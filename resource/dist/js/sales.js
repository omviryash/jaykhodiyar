$('#qty_total').css('font-weight', 'bold');
$('#total_amounts').css('font-weight', 'bold');

function add_item_get_total(){
	var new_qty = $('.quantity').val();
	var qty_total = $('#qty_total').html();
	if( qty_total == ''){ qty_total = 0; }
	$('#qty_total').html(parseInt(qty_total) + parseInt(new_qty));
	var new_total = $('.total_amount').val();
	var total_amounts = $('#total_amounts').html();
	if( total_amounts == ''){ total_amounts = 0; }
	$('#total_amounts').html(parseInt(total_amounts) + parseInt(new_total));
}

$(document).on('input','#cgst',function () {
	var disc_value = $("#disc_value").val();
	var cgst = $("#cgst").val();
	var cgst_amount = (disc_value * cgst)/100;
	$("#cgst_amount").val(parseF(cgst_amount));

	var sgst_amount = 0;
	if($("#sgst_amount").val()){ sgst_amount = $("#sgst_amount").val(); }
	var igst_amount = 0;
	if($("#igst_amount").val()){ igst_amount = $("#igst_amount").val(); }

	var amount = parseInt(disc_value) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(amount));
});

$(document).on('input','#sgst',function () {
	var disc_value = $("#disc_value").val();
	var sgst = $("#sgst").val();
	var sgst_amount = (disc_value * sgst)/100;
	$("#sgst_amount").val(parseF(sgst_amount));

	var cgst_amount = 0;
	if($("#cgst_amount").val()){ cgst_amount = $("#cgst_amount").val(); }
	var igst_amount = 0;
	if($("#igst_amount").val()){ igst_amount = $("#igst_amount").val(); }

	var amount = parseInt(disc_value) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(amount));
});

$(document).on('input','#igst',function () {
	var disc_value = $("#disc_value").val();
	var igst = $("#igst").val();
	var igst_amount = (disc_value * igst)/100;
	$("#igst_amount").val(parseF(igst_amount));

	var cgst_amount = 0;
	if($("#cgst_amount").val()){ cgst_amount = $("#cgst_amount").val(); }
	var sgst_amount = 0;
	if($("#sgst_amount").val()){ sgst_amount = $("#sgst_amount").val(); }

	var amount = parseInt(disc_value) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(amount));
});

$(document).on('input','#quantity',function () {
	input_value_get_amount();
});

$(document).on('input','#rate',function () {
	input_value_get_amount();
});

$(document).on('input','#disc_per',function () {
	input_value_get_amount();
});

$(document).on('input','#igst',function () {
	input_value_get_amount();
});

$(document).on('input','#cgst',function () {
	input_value_get_amount();
});

$(document).on('input','#sgst',function () {
	input_value_get_amount();
});

function input_value_get_amount(){
	var quantity = $("#quantity").val();
	var rate = $("#rate").val();
	var before_discount_price = quantity * rate;
	var discount = $("#disc_per").val();
	discount_amt = (before_discount_price * discount)/100;
	$("#disc_value").val(parseF(discount_amt));
	discounted_price = parseInt(before_discount_price) - parseInt(discount_amt);
	var cgst = $("#cgst").val();
	var cgst_amount = (discounted_price * cgst)/100;
	$("#cgst_amount").val(parseF(cgst_amount));
	var sgst = $("#sgst").val();
	var sgst_amount = (discounted_price * sgst)/100;
	$("#sgst_amount").val(parseF(sgst_amount));
	var igst = $("#igst").val();
	var igst_amount = (discounted_price * igst)/100;
	$("#igst_amount").val(parseF(igst_amount));

	var total_amount = parseInt(discounted_price) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(total_amount));
	set_billing_terms();
}

$(document).on("input", "#minimum", function () {
	var minumum = $(this).val();
	if(minumum != 0){
		var newdate = generateDateAfterGivenWeek(minumum);
		$('#minimum_weeks').val(newdate);
	} else {
		$('#minimum_weeks').val('');
	}
});

$(document).on("input", "#maximum", function () {
	var maximum = $(this).val();
	if(maximum != 0){
		var newdate = generateDateAfterGivenWeek(maximum);
		$('#maximum_weeks').val(newdate);
	} else {
		$('#maximum_weeks').val('');
	}
});

$('form').on('keyup keypress', function(e) {
	if($('.sales_order_item_id').val() != 0){
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) {
			e.preventDefault();
			return false;
		}
	}
});

//ADDED ON 24-JUN-2017
function convertDate(inputFormat) {
	function pad(s) { return (s < 10) ? '0' + s : s; }
	var d = new Date(inputFormat);
	return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}

function parseF(value, decimal) {
	decimal = 0;
	return value ? parseFloat(value).toFixed(decimal) : 0;
}

function generateDateAfterGivenWeek(week){
	var date = new Date();
	date.setDate(date.getDate() + 1*parseInt(week));
	var dateMsg = date.getDate()+'-'+ (parseInt(date.getMonth()+1)) +'-'+date.getFullYear();
	return dateMsg;
}

function item_details(id){
	if ($(".sales_order_item_id").attr('data-edit-item-clicked')) {
		return false;
	}
	//alert('this id value :'+id);
	if(id != '') {
		$.ajax({
			type: "POST",
			url: base_url + 'sales/ajax_load_item_details/' + id,
			data: id = 'item_id',
			async: false,
			success: function (data) {
				var json = $.parseJSON(data);
				//alert(data);
				if (json['item_name']) {
					$(".item_name").val(json['item_name']);
				} else {
					$(".item_name").val("");
				}
				if (json['uom']) {
					$(".uom").val(json['uom']);
				} else {
					$(".uom").val("");
				}

				if (json['item_code']) {
					$(".item_code").val(json['item_code']);
				} else {
					$(".itm_code").val('');
				}

				if (json['item_status']) {
					$(".item_status").val(json['item_status']).change();
				} else {
					$(".item_status").val('').change();
				}

				if (json['discount']) {
					$(".discount").val(json['discount']);
					var discountpercent = json['discount'] / 100;
					var discountprice = (rate * discountpercent );
					//alert(json['rate'] - discountprice);
					//$(".discount_amount").val(rate - discountprice);
					var discount_amount = parseInt($(".discount_amount").val());
				} else {
					$(".discount").val('');
				}
				if (json['igst']) {
					$(".igst").val(json['igst']);
					var igstpercent = json['igst'] / 100;
					var igstprice = (rate * igstpercent );
					//alert(rate + igstprice);
					//$(".igst_amount").val(discount_amount + igstprice);
				} else {
					$(".igst").val('');
				}
				if (json['cgst']) {
					$(".cgst").val(json['cgst']);
					var cgstpercent = json['cgst'] / 100;
					var cgstprice = (rate * cgstpercent );
					//alert(rate + cgstprice);
					//$(".cgst_amount").val(discount_amount + cgstprice);
				} else {
					$(".cgst").val('');
				}
				if (json['sgst']) {
					$(".sgst").val(json['sgst']);
					var sgstpercent = json['sgst'] / 100;
					var sgstprice = (rate * discountpercent );
					//alert(rate + sgstprice);
					//$(".sgst_amount").val(discount_amount + sgstprice);
				} else {
					$(".sgst").val('');
				}
				if (json['discount_amount']) {
					$(".discount_amount").val(json['discount_amount']);
				} else {
					$(".discount_amount").val('');
				}

				if (json['item_category'] && json['item_category'] != '') {
					$("#item_category_id").val(json['item_category']).change();
				} else {
					//~ $("#item_category_id").val(ITEM_CATEGORY_FINISHED_GOODS_ID).change();
				}
				if (json['item_rate']) {
					$(".rate").val(json['item_rate']);
				} else {
					$(".rate").val('');
				}
				input_value_get_amount();
			}
		});
	}
}
$(document).on('focusout','.more_items_data_qty',function () {
	var more_items_data_qty = $(this).val();
	var more_items_data_price = $(this).closest('.corporat_contact_items').find('.more_items_data_price').val();
	var more_items_data_total = 0;
	if(more_items_data_qty != '' && more_items_data_price != ''){
		more_items_data_total = more_items_data_qty * more_items_data_price;
	}
	$(this).closest('.corporat_contact_items').find('.more_items_data_total').val(more_items_data_total);
});
$(document).on('focusout','.more_items_data_price',function () {
	var more_items_data_price = $(this).val();
	var more_items_data_qty = $(this).closest('.corporat_contact_items').find('.more_items_data_qty').val();
	var more_items_data_total = 0;
	if(more_items_data_qty != '' && more_items_data_price != ''){
		more_items_data_total = more_items_data_qty * more_items_data_price;
	}
	$(this).closest('.corporat_contact_items').find('.more_items_data_total').val(more_items_data_total);
});