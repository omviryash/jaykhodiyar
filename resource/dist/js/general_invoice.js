var change_disc_value = 0;
$(document).on('input','#quantity',function () {
	input_value_get_amount();
});

$(document).on('input','#item_rate',function () {
	input_value_get_amount();
});

$(document).on('input','#disc_per',function () {
	change_disc_value = 0;
	input_value_get_amount();
});

$(document).on('input','#disc_value',function () {
	change_disc_value = 1;
	input_value_get_amount();
});

$(document).on('input','#igst',function () {
	input_value_get_amount();
});

$(document).on('input','#cgst',function () {
	input_value_get_amount();
});

$(document).on('input','#sgst',function () {
	input_value_get_amount();
});

function input_value_get_amount(){
	var quantity = $("#quantity").val();
	var rate = $("#item_rate").val();
	var before_discount_price = quantity * rate;
	$("#amount").val(before_discount_price);

	if(change_disc_value == 0){
		var discount = $("#disc_per").val();
		if(discount == ''){ discount = 0; }
		discount_amt = (before_discount_price * discount)/100;
		$("#disc_value").val(parseF(discount_amt));
		discounted_price = parseInt(before_discount_price) - parseInt(discount_amt);
	} else {
		var discount_amt = $("#disc_value").val();
		if(discount_amt == ''){ discount_amt = 0; }
		var disc_per = (discount_amt * 100) / before_discount_price;
		$("#disc_per").val(parseFloat(disc_per).toFixed(2));
		discounted_price = parseInt(before_discount_price) - parseInt(discount_amt);
	}

	var cgst = $("#cgst").val();
	var cgst_amount = (discounted_price * cgst)/100;
	$("#cgst_amount").val(parseF(cgst_amount));
	var sgst = $("#sgst").val();
	var sgst_amount = (discounted_price * sgst)/100;
	$("#sgst_amount").val(parseF(sgst_amount));
	var igst = $("#igst").val();
	var igst_amount = (discounted_price * igst)/100;
	$("#igst_amount").val(parseF(igst_amount));

	var net_amount = parseInt(discounted_price) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#net_amount").val(parseF(net_amount));
}


//ADDED ON 24-JUN-2017
function convertDate(inputFormat) {
	function pad(s) { return (s < 10) ? '0' + s : s; }
	var d = new Date(inputFormat);
	return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}

function parseF(value, decimal) {
	decimal = 0;
	return value ? parseFloat(value).toFixed(decimal) : 0;
}