// purchses.js
$(document).on("input", "#minimum", function () {
    var minumum = $(this).val();
	if(minumum != 0){
		var newdate = generateDateAfterGivenWeek(minumum);
        $('#minimum_weeks').val(newdate);
	} else {
		$('#minimum_weeks').val('');
	}
});
$(document).on("input", "#maximum", function () {
    var maximum = $(this).val();
	if(maximum != 0){
        var newdate = generateDateAfterGivenWeek(maximum);
		$('#maximum_weeks').val(newdate);
	} else {
		$('#maximum_weeks').val('');
	}
});

function generateDateAfterGivenWeek(week){
    var date = new Date();
	date.setDate(date.getDate() + 1*parseInt(week));
	var dateMsg = date.getDate()+'-'+ (parseInt(date.getMonth()+1)) +'-'+date.getFullYear();
	return dateMsg;
}

$('#qty_total').css('font-weight', 'bold');
$('#total_amounts').css('font-weight', 'bold');

function add_item_get_total(){
	var new_qty = $('[name="item_data[quantity]"]').val();
	var qty_total = $('#qty_total').html();
	if( qty_total == ''){ qty_total = 0; }
	$('#qty_total').html(parseInt(qty_total) + parseInt(new_qty));
	var new_total = $('[name="item_data[total_amount]"]').val();
	var total_amounts = $('#total_amounts').html();
	if( total_amounts == ''){ total_amounts = 0; }
	$('#total_amounts').html(parseInt(total_amounts) + parseInt(new_total));
}

$(document).on('input','#cgst',function () {
	var discount_amount = $("#discount_amount").val();
	var cgst = $("#cgst").val();
	var cgst_amount = (discount_amount * cgst)/100;
	$("#cgst_amount").val(parseF(cgst_amount));

	var sgst_amount = 0;
	if($("#sgst_amount").val()){ sgst_amount = $("#sgst_amount").val(); }
	var igst_amount = 0;
	if($("#igst_amount").val()){ igst_amount = $("#igst_amount").val(); }

	var amount = parseInt(discount_amount) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(amount));
});
$(document).on('input','#sgst',function () {
	var discount_amount = $("#discount_amount").val();
	var sgst = $("#sgst").val();
	var sgst_amount = (discount_amount * sgst)/100;
	$("#sgst_amount").val(parseF(sgst_amount));

	var cgst_amount = 0;
	if($("#cgst_amount").val()){ cgst_amount = $("#cgst_amount").val(); }
	var igst_amount = 0;
	if($("#igst_amount").val()){ igst_amount = $("#igst_amount").val(); }

	var amount = parseInt(discount_amount) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(amount));
});

$(document).on('input','#igst',function () {
	var discount_amount = $("#discount_amount").val();
	var igst = $("#igst").val();
	var igst_amount = (discount_amount * igst)/100;
	$("#igst_amount").val(parseF(igst_amount));

	var cgst_amount = 0;
	if($("#cgst_amount").val()){ cgst_amount = $("#cgst_amount").val(); }
	var sgst_amount = 0;
	if($("#sgst_amount").val()){ sgst_amount = $("#sgst_amount").val(); }

	var amount = parseInt(discount_amount) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(amount));
});

$(document).on('input','#quantity',function () {
	input_value_get_amount();
});
$(document).on('input','#item_rate',function () {
	input_value_get_amount();
});
$(document).on('input','#discount',function () {
	input_value_get_amount();
});

$(document).on('input','#igst',function () {
	input_value_get_amount();
});

$(document).on('input','#cgst',function () {
	input_value_get_amount();
});

$(document).on('input','#sgst',function () {
	input_value_get_amount();
});

function input_value_get_amount(){
	var quantity = $("#quantity").val();
	var item_rate = $("#item_rate").val();
	var before_discount_price = quantity * item_rate;
	var discount = $("#discount").val();
	discount_amt = (before_discount_price * discount)/100;
	$("#discount_amount").val(parseF(discount_amt));
	discounted_price = parseInt(before_discount_price) - parseInt(discount_amt);
	var cgst = $("#cgst").val();
	var cgst_amount = (discounted_price * cgst)/100;
	$("#cgst_amount").val(parseF(cgst_amount));
	var sgst = $("#sgst").val();
	var sgst_amount = (discounted_price * sgst)/100;
	$("#sgst_amount").val(parseF(sgst_amount));
	var igst = $("#igst").val();
	var igst_amount = (discounted_price * igst)/100;
	$("#igst_amount").val(parseF(igst_amount));

	var total_amount = parseInt(discounted_price) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#total_amount").val(parseF(total_amount));
}


function product_price_input(){
	var product_qty = $("#product_qty").val();
	var price = $("#price").val();
	apply_discount_get_amount();
	
}

function parseF(value, decimal) {
	decimal = 0;
	return value ? parseFloat(value).toFixed(decimal) : 0;
}

function item_details(id)
{
	//alert('this id value :'+id);
	if(id != '') {
		$.ajax({
			type: "POST",
			url: base_url + 'purchases/ajax_load_item_details/' + id,
			data: id = 'item_id',
			async: false,
			success: function (data) {
				var json = $.parseJSON(data);
				//alert(data);
				if (json['item_name']) {
					$(".item_name").val(json['item_name']);
				} else {
					$(".item_name").val("");
				}
				if (json['uom']) {
					setSelect2Value($("#uom_id"),base_url + 'app/set_uom_select2_val_by_id/'+json['uom']);
				} else {
					setSelect2Value($("#uom_id"),base_url + 'app/set_uom_select2_val_by_id/');
				}

				if (json['item_code']) {
					$(".item_code").val(json['item_code']);
				} else {
					$(".itm_code").val('');
				}

				if (json['item_status']) {
					$(".item_status").val(json['item_status']).change();
				} else {
					$(".item_status").val('').change();
				}

				if (json['rate']) {
					$(".item_rate").val(json['rate']);
					var rate = parseInt(json['rate']);
				} else {
					$(".item_rate").val('');
					var rate = parseInt(0);
				}
				if (json['discount']) {
					$(".discount").val(json['discount']);
					var discountpercent = json['discount'] / 100;
					var discountprice = (rate * discountpercent );
					//alert(json['rate'] - discountprice);
					//$(".discount_amount").val(rate - discountprice);
					var discount_amount = parseInt($(".discount_amount").val());
				} else {
					$(".discount").val('');
				}
				if (json['igst']) {
					$(".igst").val(json['igst']);
					var igstpercent = json['igst'] / 100;
					var igstprice = (rate * igstpercent );
					//alert(rate + igstprice);
					//$(".igst_amount").val(discount_amount + igstprice);
				} else {
					$(".igst").val('');
				}
				if (json['cgst']) {
					$(".cgst").val(json['cgst']);
					var cgstpercent = json['cgst'] / 100;
					var cgstprice = (rate * cgstpercent );
					//alert(rate + cgstprice);
					//$(".cgst_amount").val(discount_amount + cgstprice);
				} else {
					$(".cgst").val('');
				}
				if (json['sgst']) {
					$(".sgst").val(json['sgst']);
					var sgstpercent = json['sgst'] / 100;
					var sgstprice = (rate * discountpercent );
					//alert(rate + sgstprice);
					//$(".sgst_amount").val(discount_amount + sgstprice);
				} else {
					$(".sgst").val('');
				}
				if (json['discount_amount']) {
					$(".discount_amount").val(json['discount_amount']);
				} else {
					$(".discount_amount").val('');
				}

			}
		});
	}
}



function getstatedetails(id){
	//alert('this id value :'+id);
	if(id != '' && id != null){
		$.ajax({
			type: "POST",
			url: base_url + 'party/ajax_get_state/'+id,
			data: id='cat_id',
			success: function(data){
				var json = $.parseJSON(data);
				//alert(data);
				if (json['state']) {
					$("#state").html('');
					$("#state").html(json['state']);
					$("#s2id_state span:first").html($("#state option:selected").text());    
				}
				if (json['country']) {
					$("#country").html('');
					$("#country").html(json['country']);
					$("#s2id_country span:first").html($("#country option:selected").text());    
				}
			},
		});
	}
}

function supplier_details(id){
	$.ajax({
		type: "POST",
		url: base_url + 'purchases/ajax_load_supplier_with_cnt_person/'+id,
		data: id='supplier_id',
		async: false,
		success: function(data){
			var json = $.parseJSON(data);
			console.log(json);
			//agent_id  branch_id  

			if (json['address']) {
				$(".address").html(json['address']);
			}else{
				$(".address").html("");
			}

			if (json['supplier_code']) {
				$('#supplier_code').val(json['supplier_code']);
			}
			if (json['supplier_name']) {
				$('#supplier_name').val(json['supplier_name']);
			}
			if (json['supplier_id']) {
				$(".supplier_id").val(json['supplier_id']);
			}

			if (json['city_id']) {
				setSelect2Value($("#city"),base_url + 'app/set_city_select2_val_by_id/'+json['city_id']);
			}else{
				setSelect2Value($("#city"));
			}
			if (json['state_id']) {
				setSelect2Value($("#state"),base_url + 'app/set_state_select2_val_by_id/'+json['state_id']);
			}else{
				setSelect2Value($("#state"));
			}
			if (json['country_id']) {
				setSelect2Value($("#country"),base_url + 'app/set_country_select2_val_by_id/'+json['country_id']);
			}else{
				setSelect2Value($("#country"));
			}


			if (json['email_id']) {
				$(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
			}else{
				$(".email_id").val("");
			}

			if (json['pincode']) {
				$(".pincode").val(json['pincode']);
			}else{
				$(".pincode").val("");
			}
			if (json['phone_no']) {
				$(".phone_no").val(json['phone_no']);
			}else{
				$(".phone_no").val("");
			}

		}
	});
}
