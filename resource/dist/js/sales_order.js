var change_disc_value = 0;
$(document).on('input','#quantity',function () {
	input_value_get_amount();
});

$(document).on('input','#item_rate',function () {
	input_value_get_amount();
});

$(document).on('input','#reference_qty',function () {
	input_value_get_amount();
});

$(document).on('input','#disc_per',function () {
	change_disc_value = 0;
	input_value_get_amount();
});

$(document).on('input','#disc_value',function () {
	change_disc_value = 1;
	input_value_get_amount();
});

$(document).on('input','#igst',function () {
	input_value_get_amount();
});

$(document).on('input','#cgst',function () {
	input_value_get_amount();
});

$(document).on('input','#sgst',function () {
	input_value_get_amount();
});

function input_value_get_amount(){
	var quantity = $("#quantity").val();
	var rate = $("#item_rate").val();
	var reference_qty = $("#reference_qty").val();
	var before_discount_price = (quantity || 1) * (rate || 1) * (reference_qty || 1);
	$("#amount").val(before_discount_price.toFixed(0));

	if(change_disc_value == 0){
		var discount = $("#disc_per").val();
		if(discount == ''){ discount = 0; }
		discount_amt = (before_discount_price * discount)/100;
		$("#disc_value").val(parseF(discount_amt));
		discounted_price = parseInt(before_discount_price) - parseInt(discount_amt);
	} else {
		var discount_amt = $("#disc_value").val();
		if(discount_amt == ''){ discount_amt = 0; }
		var disc_per = (discount_amt * 100) / before_discount_price;
		$("#disc_per").val(parseFloat(disc_per).toFixed(2));
		discounted_price = parseInt(before_discount_price) - parseInt(discount_amt);
	}

	var cgst = $("#cgst").val();
	var cgst_amount = (discounted_price * cgst)/100;
	$("#cgst_amount").val(parseF(cgst_amount));
	var sgst = $("#sgst").val();
	var sgst_amount = (discounted_price * sgst)/100;
	$("#sgst_amount").val(parseF(sgst_amount));
	var igst = $("#igst").val();
	var igst_amount = (discounted_price * igst)/100;
	$("#igst_amount").val(parseF(igst_amount));

	var net_amount = parseInt(discounted_price) + parseInt(parseF(cgst_amount)) + parseInt(parseF(sgst_amount)) + parseInt(parseF(igst_amount));
	$("#net_amount").val(parseF(net_amount));
    
//    if(change_disc_value == 0){
//		var discount = $("#disc_per").val();
//		if(discount == ''){ discount = 0; }
//		discount_amt = (parseFloat(before_discount_price) * parseFloat(discount))/100;
//		$("#disc_value").val(parseF(discount_amt));
//		discounted_price = parseFloat(before_discount_price) - parseFloat(discount_amt);
//	} else {
//		var discount_amt = $("#disc_value").val();
//		if(discount_amt == ''){ discount_amt = 0; }
//		var disc_per = (parseFloat(discount_amt) * 100) / parseFloat(before_discount_price);
//		$("#disc_per").val(parseFloat(disc_per).toFixed(3));
//		discounted_price = parseFloat(before_discount_price) - parseFloat(discount_amt);
//	}
//
//	var cgst = $("#cgst").val();
//	var cgst_amount = (discounted_price * cgst)/100;
//	$("#cgst_amount").val(parseFloat(cgst_amount).toFixed(3));
//	var sgst = $("#sgst").val();
//	var sgst_amount = (discounted_price * sgst)/100;
//	$("#sgst_amount").val(parseFloat(sgst_amount).toFixed(3));
//	var igst = $("#igst").val();
//	var igst_amount = (discounted_price * igst)/100;
//	$("#igst_amount").val(parseFloat(igst_amount).toFixed(3));
//
//	var net_amount = parseFloat(discounted_price) + parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
//	$("#net_amount").val(parseFloat(net_amount).toFixed(3));    
    
}

$(document).on("input", "#minimum", function () {
	var minumum = $(this).val();
	if(minumum != 0){
		var newdate = generateDateAfterGivenWeek(minumum);
		$('#minimum_weeks').val(newdate);
	} else {
		$('#minimum_weeks').val('');
	}
});

$(document).on("input", "#maximum", function () {
	var maximum = $(this).val();
	if(maximum != 0){
		var newdate = generateDateAfterGivenWeek(maximum);
		$('#maximum_weeks').val(newdate);
	} else {
		$('#maximum_weeks').val('');
	}
});

$(document).on("change", "#sales_order_date,#order_date", function () {
	var minimum = $('#minimum').val();
	if(minimum){	   
	}else{
		var minimum = 0;
	}
	var maximum = $('#maximum').val();
	if(maximum){	   
	}else{
		var maximum = 0;
	}
	$('#minimum_weeks').val(generateDateAfterGivenWeek(minimum));
	$('#maximum_weeks').val(generateDateAfterGivenWeek(maximum));
});

//$('form').on('keyup keypress', function(e) {
//	if($('.sales_order_item_id').val() != 0){
//		var keyCode = e.keyCode || e.which;
//		if (keyCode === 13) {
//			e.preventDefault();
//			return false;
//		}
//	}
//});

//ADDED ON 24-JUN-2017
function convertDate(inputFormat) {
	function pad(s) { return (s < 10) ? '0' + s : s; }
	var d = new Date(inputFormat);
	return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}

function parseF(value, decimal) {
	decimal = 0;
	return value ? parseFloat(value).toFixed(decimal) : 0;
}

function generateDateAfterGivenWeek(week){
	var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
	var pathArray = window.location.pathname.split( '/' );
	var segment_1 = pathArray[2];
	if(segment_1 == 'purchase_order'){
		var sales_order_date = $("#order_date").datepicker("getDate");
	}
	if(segment_1 == 'sales_order' || segment_1 == 'proforma_invoices'){
		var sales_order_date = $("#sales_order_date").datepicker("getDate");	
	}
	if(sales_order_date == 	'Invalid Date') {
		show_notify("Please Proper Select Order Date !", false);
		return false;
	}
	var date = sales_order_date;
	//var date = new Date();
	date.setDate(date.getDate() + 1*parseInt(week));
	var dateMsg = date.getDate()+'-'+ (parseInt(date.getMonth()+1)) +'-'+date.getFullYear();
	return dateMsg;
}

//function item_details(id){
//	if ($(".sales_order_item_id").attr('data-edit-item-clicked')) {
//		return false;
//	}
//	//alert('this id value :'+id);
//	if(id != '') {
//		$.ajax({
//			type: "POST",
//			url: base_url + 'sales/ajax_load_item_details/' + id,
//			data: id = 'item_id',
//			async: false,
//			success: function (data) {
//				var json = $.parseJSON(data);
//				//alert(data);
//				if (json['item_name']) {
//					$(".item_name").val(json['item_name']);
//				} else {
//					$(".item_name").val("");
//				}
//				if (json['uom']) {
//					$(".uom").val(json['uom']);
//				} else {
//					$(".uom").val("");
//				}
//
//				if (json['item_code']) {
//					$(".item_code").val(json['item_code']);
//				} else {
//					$(".itm_code").val('');
//				}
//
//				if (json['item_status']) {
//					$(".item_status").val(json['item_status']).change();
//				} else {
//					$(".item_status").val('').change();
//				}
//
//				if (json['discount']) {
//					$(".discount").val(json['discount']);
//					var discountpercent = json['discount'] / 100;
//					var discountprice = (rate * discountpercent );
//					//alert(json['rate'] - discountprice);
//					//$(".discount_amount").val(rate - discountprice);
//					var discount_amount = parseInt($(".discount_amount").val());
//				} else {
//					$(".discount").val('');
//				}
//				if (json['igst']) {
//					$(".igst").val(json['igst']);
//					var igstpercent = json['igst'] / 100;
//					var igstprice = (rate * igstpercent );
//					//alert(rate + igstprice);
//					//$(".igst_amount").val(discount_amount + igstprice);
//				} else {
//					$(".igst").val('');
//				}
//				if (json['cgst']) {
//					$(".cgst").val(json['cgst']);
//					var cgstpercent = json['cgst'] / 100;
//					var cgstprice = (rate * cgstpercent );
//					//alert(rate + cgstprice);
//					//$(".cgst_amount").val(discount_amount + cgstprice);
//				} else {
//					$(".cgst").val('');
//				}
//				if (json['sgst']) {
//					$(".sgst").val(json['sgst']);
//					var sgstpercent = json['sgst'] / 100;
//					var sgstprice = (rate * discountpercent );
//					//alert(rate + sgstprice);
//					//$(".sgst_amount").val(discount_amount + sgstprice);
//				} else {
//					$(".sgst").val('');
//				}
//				if (json['discount_amount']) {
//					$(".discount_amount").val(json['discount_amount']);
//				} else {
//					$(".discount_amount").val('');
//				}
//
//				if (json['item_category'] && json['item_category'] != '') {
//					$("#item_category_id").val(json['item_category']).change();
//				} else {
//					//~ $("#item_category_id").val(ITEM_CATEGORY_FINISHED_GOODS_ID).change();
//				}
//				if (json['item_rate']) {
//					$(".rate").val(json['item_rate']);
//				} else {
//					$(".rate").val('');
//				}
//				input_value_get_amount();
//			}
//		});
//	}
//}