<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* ---------------- CUSTOM CONSTANCE-------------------*/
$ip_adderess_backup_lan = '192.168.29.254:8080';
$ip_adderess_backup_net = '117.240.152.122:8080';

if($_SERVER['HTTP_HOST'] == $ip_adderess_backup_lan){
	define('BASE_URL',$_SERVER["REQUEST_SCHEME"].'://'.$ip_adderess_backup_lan.'/jaykhodiyar_backup/');
}elseif($_SERVER['HTTP_HOST'] == $ip_adderess_backup_net){
	define('BASE_URL',$_SERVER["REQUEST_SCHEME"].'://'.$ip_adderess_backup_net.'/jaykhodiyar_backup/');
}else{
    define('BASE_URL',$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER['HTTP_HOST'].'/jaykhodiyar/');
}

define("CURRENT_DATABASE", 'jaykhodiyar');
define("CURRENT_LOG_DATABASE", 'jaykhodiyar_logs');

$http_host = explode(':', $_SERVER['HTTP_HOST']);
define('SERVER_REQUEST_SCHEME',$_SERVER["REQUEST_SCHEME"]);
define('HTTP_HOST',$http_host[0]);
define('PORT_NUMBER', 3000);
define('USER_TYPE_CONTESTANT',2);
define('USER_TYPE_VOTER',3);

define('bootstrap_url',BASE_URL.'resource/bootstrap/');
define('dist_url',BASE_URL.'resource/dist/');
define('plugins_url',BASE_URL.'resource/plugins/');

define('IMAGE_URL',BASE_URL.'resource/uploads/images/');
define('RESOURCE_IMAGE_URL',BASE_URL.'resource/image/');
define('IMAGE_DIR','resource/uploads/images/');
define('DEFAULT_IMAGE',BASE_URL.'resource\assets\images\default_avatar.png');


/*------------ Menu Modules ------------*/

define("DASHBOARD_MENU_ID",35); //1
define("DASHBOARD_DEMO_MENU_ID",22); //2
define("MASTER_MENU_ID",36); //3
define("COMPANY_DETAIL_MENU_ID",37); // 3.1


define("MASTER_ITEM_MENU_ID",9); //3.2
define("MASTER_ITEM_MASTER_MENU_ID",171); //3.2.1
define("MASTER_ITEM_CATEGORY_MENU_ID",172); //3.2.2
define("MASTER_ITEM_GROUP_CODE_MENU_ID",173); //3.2.3
define("MASTER_ITEM_TYPE_MENU_ID",174); //3.2.4
define("MASTER_ITEM_STATUS_MENU_ID",175); //3.2.5
define("MASTER_ITEM_MATERIAL_PROCESS_TYPE_MENU_ID",176); //3.2.6
define("MASTER_ITEM_MAIN_GROUP_MENU_ID",177); //3.2.7
define("MASTER_ITEM_SUB_GROUP_MENU_ID",178); //3.2.8
define("MASTER_ITEM_MAKE_MENU_ID",179); //3.2.9
define("MASTER_ITEM_DRAWING_NUMBER_MENU_ID",180); //3.2.10
define("MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID",181); //3.2.11
define("MASTER_ITEM_CLASS_MENU_ID",182); //3.2.12
define("MASTER_ITEM_EXCISE_MENU_ID",183); //3.2.13
define("MASTER_PRODUCT_GROUP_MENU_ID",191); //3.2.14
define("MASTER_PURCHASE_ITEM_MENU_ID",197); //3.2.15
define("MASTER_PURCHASE_PROJECT_MENU_ID",198); //3.2.16
define("MASTER_RAW_MATERIAL_GROUP_MENU_ID",255); //3.2.33
define("MASTER_SUPPLIER_MAKE_MENU_ID",256); //3.3.6

define("MASTER_BOM_MENU_ID",243); //3.3
define("MASTER_AGENT_MENU_ID",10); //3.3
define("MASTER_SALES_MENU_ID",11); //3.4
define("MASTER_SALES_REFERENCE_MENU_ID",38); //3.4.1
define("MASTER_SALES_INQUIRY_STAGE_MENU_ID",39); //3.4.2
define("MASTER_SALES_INDUSTRY_TYPE_MENU_ID",40); //3.4.3
define("MASTER_SALES_INQUIRY_STATUS_MENU_ID",41); //3.4.4
define("MASTER_SALES_PRIORITY_MENU_ID",42); //3.4.5
define("MASTER_SALES_UOM_MENU_ID",43); //3.4.6
define("MASTER_SALES_QUOTATION_TYPE_MENU_ID",44); //3.4.7
define("MASTER_SALES_SALES_TYPE_MENU_ID",45); //3.4.8
define("MASTER_SALES_QUOTATION_REASON_MENU_ID",46); //3.4.9
define("MASTER_SALES_CURRENCY_MENU_ID",47); //3.4.10
define("MASTER_SALES_SALES_MENU_ID",48); //3.4.11
define("MASTER_SALES_ACTION_MENU_ID",49); //3.4.12
define("MASTER_SALES_QUOTATION_STAGE_MENU_ID",50); //3.4.13
define("MASTER_SALES_STATUS_MENU_ID",51); //3.4.14
define("MASTER_SALES_INQUIRY_CONFIRMATION_MENU_ID",52); //3.4.15
define("MASTER_SALES_ORDER_STAGE_MENU_ID",53); //3.4.16
define("MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID",192); //3.4.17
define("MASTER_SALES_PURCHASE_ORDER_REF_MENU_ID",194); //3.4.18
define("MASTER_SALES_QUOTATION_STATUS_MENU_ID",195); //3.4.19
define("MASTER_SALES_SEA_FREIGHT_TYPE_MENU_ID",221); //3.4.20
define("MASTER_HR_MENU_ID",54); //3.5
define("MASTER_HR_DAY_MENU_ID",55); //3.5.1
define("MASTER_HR_GRADE_MENU_ID",56); //3.5.2
define("MASTER_HR_DEPARTMENT_MENU_ID",71); //3.5.3
define("MASTER_TERMS_CONDITION_MENU_ID",57); //3.6
define("MASTER_TERMS_CONDITIONS_MENU_ID",186);//3.6.5
define("MASTER_TERMS_CONDITION_GROUP_MENU_ID",58); //3.6.1
define("MASTER_TERMS_CONDITION_DETAIL_MENU_ID",59); //3.6.2
define("MASTER_DEFAULT_TERMS_CONDITION_MENU_ID",60); //3.6.3
define("MASTER_TERMS_CONDITION_TEMPLATE_MENU_ID",61); //3.6.4
define("MASTER_GENERAL_MASTER_MENU_ID",62); //3.7
define("MASTER_GENERAL_MASTER_BRANCH_MENU_ID",63); //3.7.1
define("MASTER_GENERAL_MASTER_PROJECT_MENU_ID",64); //3.7.2
define("MASTER_GENERAL_MASTER_BILLING_TEMPLATE_MENU_ID",65); //3.7.3
define("MASTER_GENERAL_MASTER_COUNTRY_MENU_ID",66); //3.7.4
define("MASTER_GENERAL_MASTER_STATE_MENU_ID",67); //3.7.5
define("MASTER_GENERAL_MASTER_CITY_MENU_ID",68); //3.7.6
define("MASTER_GENERAL_MASTER_REFERENCE_MENU_ID",69); //3.7.7
define("MASTER_GENERAL_MASTER_DESIGNATION_MENU_ID",70); //3.7.8
define("MASTER_GENERAL_MASTER_TERMS_GROUP_MENU_ID",72); //3.7.10
define("MASTER_GENERAL_MASTER_COMPANY_BANKS_MENU_ID",222); //3.7.11
define("MASTER_GENERAL_MASTER_SMS_TOPICS_MENU_ID",224); //3.7.12
define("MASTER_GENERAL_MASTER_STAFF_ROLES_MENU_ID",73); //3.8
define("MASTER_GENERAL_MASTER_CHAT_ROLES_MENU_ID",74); //3.9
define("MASTER_GENERAL_MASTER_REMINDER_MENU_ID",75); //3.10
define("MASTER_GENERAL_MASTER_BILLING_TERMS",76); //3.11
define("MASTER_CUSTOMER_REMINDER",189); //3.12
define("MASTER_CUSTOMER_REMINDER_SENT",190); //3.13
define("MASTER_GENERAL_MASTER_QUOTATION_CHAT_ROLES_MENU_ID",193); //3.14
define("MASTER_GENERAL_MASTER_PRINT_LETTER_ROLES_MENU_ID",225); //3.15
define("MASTER_HR_EMPLOYEE_WISE_INCENTIVE_MENU_ID",258); //3.8.4
define("MASTER_PAYMENT_MODE_MENU_ID",259); //3.8.5

// Master Challan
define("MASTER_CHALLAN_MENU_ID",209); //3.10
define("MASTER_CHALLAN_ITEM_USER",210); //3.10.1
define("MASTER_CHALLAN_ITEM_MAKE",211); //3.10.2
define("MASTER_CHALLAN_ITEM_HP",212); //3.10.3
define("MASTER_CHALLAN_ITEM_KW",213); //3.10.4
define("MASTER_CHALLAN_ITEM_FREQUENCY",214); //3.10.5
define("MASTER_CHALLAN_ITEM_RPM",215); //3.10.6
define("MASTER_CHALLAN_ITEM_VOLTS",216); //3.10.7
define("MASTER_CHALLAN_ITEM_GEAR_TYPE",217); //3.10.8
define("MASTER_CHALLAN_ITEM_MODEL",218); //3.10.9
define("MASTER_CHALLAN_ITEM_RATIO",219); //3.10.10
define("MASTER_CHALLAN_DEFAULT_DETAILS",237); //3.10.11

/*------------ Menu Modules ------------*/
define("PARTY_TYPE_MODULE_ID",2);
define("CHANGE_PASSWORD_MODULE_ID",21);
define("USER_DROPDOWN_HEADER_MODULE_ID",23);//1.3
define("CAN_APPROVE_SALES_ORDER",24);
define("CAN_APPROVE_PURCHASE_ORDER",199);
define("CAN_APPROVE_PROFORMA_INVOICE",188);

//define("GOOGLESHEET_ADD_VIEW",28);

/*------------ Purchase Modules constants------------*/
define("PURCHASE_MODULE_ID",15);
define("PURCHASE_SUPPLIER_MODULE_ID",28);
define("PURCHASE_INVOICE_MODULE_ID",29);
define("PURCHASE_ORDER_MODULE_ID",30);
define("PURCHASE_ORDER_ITEMS",204);

/*------------ Sales Modules constants------------*/
define("SALES_MODULE_ID",16);
define("PARTY_MODULE_ID",1); // 5.1
define("CAN_ALLOW_TRANSFER_PARTY_ID",247); // 5.1
define("SALES_PARTY_LOG_MENU_ID",93);
define("PARTY_OUTSTANDING_MODULE_ID",32);
define("ENQUIRY_MODULE_ID",4);
define("CAN_ASSIGNE_ENQUIRY_ID",220);
define("QUOTATION_MODULE_ID",5);//5.3
define("QUOTATION_FOLLOWUP",205);//5.3.1
define("SALES_ORDER_MODULE_ID",6);
define("PROFORMA_INVOICE_MODULE_ID",33);
define("START_PRODUCTION_MODULE_ID",246); //5.5.3
define("SALES_TRACK_SALE_MENU_ID",107);

/*------------ Dispatch Modules constants------------*/
define("DISPATCH_MENU_ID",108); //6
define("CHALLAN_MODULE_ID",109); //6.1
define("TESTING_REPORT_MODULE_ID",208); //6.2
define("OUR_CLIANT_MODULE_ID",110); //6.4
define("INVOICE_MODULE_ID",111);//6.3
define("CAN_APPROVE_INVOICE",207);//6.3.1

/*------------ Service Modules constants------------*/
define("SERVICE_MENU_ID",112); //7
define("INSTALLATION_MODULE_ID",223); //7.1
define("FEEDBACK_MODULE_ID",232); //7.2
define("COMPLAIN_MENU_ID",233); //7.3
define("COMPLAIN_MODULE_ID",229); //7.4
define("RESOLVE_COMPLAIN_MODULE_ID",230); //7.5
define("GENERAL_INVOICE_MENU_ID",234); //7.6
define("PARTS_MODULE_ID",231); //7.7
define("GENERAL_INVOICE_MODULE_ID",235); //7.8
define("CAN_APPROVE_GENERAL_INVOICE",236); //7.9

/*------------ Finance Modules constants------------*/
define("FINANCE_MENU_ID",113); //8
define("PAYMENT_MODULE_ID",19);//8.1
define("CAN_APPROVE_PAYMENT_RECEIPT",206);//8.1.1

define("LEDGER_MODULE_ID",79);//8.2

/*------------ Report Modules constants------------*/
define("REPORT_MENU_ID",121); //9
define("REPORT_LIST_OF_INQUIRY_MENU_ID",122); //9.1
define("REPORT_LIST_OF_QUOTATION_MENU_ID",123); //9.2
define("REPORT_SALES_LEAD_SUMMARY_MENU_ID",124); //9.3
define("REPORT_LIST_OF_SALES_ORDER_MENU_ID",125); //9.4
define("REPORT_DELAY_QUOTATION_REPORT_MENU_ID",126); //9.5
define("REPORT_LIST_OF_DUE_SALES_ORDER_MENU_ID",127); //9.6
define("REPORT_SALES_ORDER_STATUS_MENU_ID",128); //9.7
define("REPORT_SALES_SUMMARY_MENU_ID",129); //9.8
define("REPORT_DISPATCH_SUMMARY_MENU_ID",130); //9.9
define("REPORT_PAYMENT_RECEIPT_DETAILS_MENU_ID",131); //9.10
define("REPORT_PRODUCTION_SCHEDULE_MENU_ID",132); //9.11
define("REPORT_STOCK_MANAGEMENT_MENU_ID",133); //9.12
define("REPORT_QUOTATION_MANAGEMENT_MENU_ID",134); //9.13
define("REPORT_ENQUIRY_FOLLOWUP_HISTORY_MENU_ID",135); //9.14
define("REPORT_QUOTATION_FOLLOWUP_HISTORY_MENU_ID",136); //9.15
define("REPORT_PENDING_REPORT_MENU_ID",137); //9.16
define("REPORT_BOM_INFO_REPORT_MENU_ID",138); //9.17
define("SUMMARY_REPORT_MENU_ID",119); //9.17

define("ITEM_DIFF_REPORT_MODULE_ID",244); 
define("PURCHASE_ITEM_STOCK_REPORT_MODULE_ID",245); 
define("ITEM_COSTING_REPORT_MODULE_ID",257);

/*------------ General Modules constants------------*/
define("GENERAL_MENU_ID",139);//10
define("GENERAL_DAILY_WORK_ENTRY_MENU_ID",140); //10.1
define("DISPLAY_SEND_SMS_CHECKBOX",238);//10.2
define("COMMON_PLACE_SMS_MENU_ID",239);//10.3
define("COMMON_PLACE_SMS_SEND_TO_LIST_ID",240);//10.4
define("COURIER_MENU_ID",248);//10.5
define('PARTY_FRONTEND_MENU_ID', 249); //10.6
define('FEEDBACK_MENU_ID', 250); //10.7

/*------------ HR Modules constants------------*/
define("HR_MENU_ID",141); //11
define("HR_EMPLOYEE_MASTER_MENU_ID",142); //11.1
define("HR_MANAGE_EMPLOYEE_MENU_ID",143); //11.1.1
define("HR_CALCULATE_SALARY_MENU_ID",144); //11.1.2
define("HR_CHANGE_EMPLOYEE_PASSWORD_MENU_ID",21); //11.1.11
define("HR_LEAVE_MASTER_MENU_ID",145); //11.2
define("HR_TOTAL_FREE_LEAVES_MENU_ID",146); //11.2.1
define("HR_WEEKLY_LEAVES_MENU_ID",147); //11.2.2
define("HR_YEARLY_LEAVES_MENU_ID",148); //11.2.3
define("HR_EXPECTED_INTERVIEW_MENU_ID",149); //11.3
define("HR_LETTER_MANAGEMENT_MENU_ID",150); //11.4
define("HR_ADD_LETTER_MENU_ID",226); //11.4.1
define("HR_PRINT_LETTER_MENU_ID",227); //11.4.2
define("HR_ISSUED_LETTER_MENU_ID",228); //11.4.3
define("HR_LEAVE_MENU_ID",157); //11.5

/*------------ Mail System Modules constants------------*/
define("MAIL_SYSTEM_MENU_ID",158); //12
define("MAIL_SYSTEM_INBOX_MENU_ID",159); //12.1
define("MAIL_SYSTEM_COMPOSE_MENU_ID",160); //12.2
define("MAIL_SYSTEM_SEND_OUTBOX_MAILS_MENU_ID",161); //12.3

/*------------ APPLY FOR LEAVE Modules constants------------*/
define("APPLY_FOR_LEAVE_MENU_ID",164); //13
define("APPLY_FOR_LEAVE_MODULE_ID",162); //13
define("REPLY_LEAVE_MODULE_ID",25);

/*------------ Google sheet Modules constants------------*/
define("GOOGLE_SHEET_MENU_ID",163); //14
define("GOOGLE_SHEET_MODULE_ID",26); //14.1

/*------------ Cron Modules constants------------*/
define("CRON_MENU_ID",166); //15
define("CRON_AGENT_CHAT_MENU_ID",167); //15.1
define("CRON_LOAD_ALL_STAFFS_UNREAD_MAILS_MENU_ID",168); //15.2
define("CRON_LOAD_REMINDERS_MENU_ID",169); //15.3
define("CRON_REMOVE_OLD_FILES_MENU_ID",170); //15.4


define("PARTY_TYPE_EXPORT_ID",5);
define("PARTY_TYPE_DOMESTIC_ID",6);

/* Supplier constants */
define("SUPPLIER_MODULE_ID",1);
define("SUPPLIER_TYPE_MODULE_ID",2);
define("MASTER_SUPPLIER_MODULE_ID",7);
define("SUPPLIER_TYPE_EXPORT_ID",5);
define("SUPPLIER_TYPE_DOMESTIC_ID",6);

/* Chart */
define("SALES_CHART_MODULE_ID",31);
define("EMPLOYEE_PERFORMANCE_CHART_MODULE_ID",251);

/* Purchase contasts */
//define("PURCHASE_ORDER_MODULE_ID",6);


/*----------- CURRENCY ID -------------*/
define("INR_CURRENCY_ID",3);
define("USD_CURRENCY_ID",4);
/*----- Company Id ----------*/
define("COMPANY_ID",4);
define("COMPANY_DESIGNATION_CEO_ID",2);
/*-------Product Quotation -----------*/
define("ITEM_CATEGORY_FINISHED_GOODS_ID",6);
define("QUOTATION_DEFAULT_STATUS_ID",4); // Hot Status
define("QUOTATION_DEFAULT_STAGE_ID",14); // In Process Stage

define("QUOTATION_STATUS_ID_ON_ORDER_CREATE",5); //Received Status
define("QUOTATION_STATUS_HOT",4); //Hot Status
define("QUOTATION_STATUS_COLD",3); //Cold Status
define("PRODUCT_QUOTATION_TYPE_ID",5);
define("ITEM_QUOTATION_TYPE_ID",6);
define("VISITORCHATMAIL",'inquiry@jaykhodiyargroup.com,sanjay.tilala@jaykhodiyargroup.com');
//define("VISITORCHATMAIL",'omvirchirag@gmail.com');
define("AGENTCHATMAIL",'sanjay.tilala@jaykhodiyar.com');
//define("AGENTCHATMAIL",'omviravinash@gmail.com');
define("BCC_EMAIL_ADDRESS",'sanjay.tilala@jaykhodiyar.com,omviryash@gmail.com');
//define("BCC_EMAIL_ADDRESS",'omviravinash@gmail.com');
define("SMTP_EMAIL_ADDRESS",'jaykhodiyarapps@gmail.com');
define("SMTP_EMAIL_PASSWORD",'Ygdhykj@#$147*');
define("SERVICE_EMAIL_ADDRESS",'support@jaykhodiyargroup.com');
define("FROM_EMAIL_ADDRESS",'jaykhodiyarapps@gmail.com');

//Remove from DB //pending

define("OPPORTUNITIES_LEAD_MODULE_ID",3);
define("MASTER_TERMS_CONDITIONS_MODULE_ID",12);
define("MASTER_GENERAL_MASTER_MODULE_ID",13);
define("MASTER_STAFF_ROLES_MODULE_ID",14);
define("STORE_MODULE_ID",17);
define("HR_MODULE_ID",20);
define("DISPATCH_MODULE_ID",18);
define("BOM_INFO_REPORT_MODULE",34);

//log menu access role
define("LOGS_MENU",184);

//Management access role
define("MANAGEMENT_USER",196);
define("QUOTATION_MANAGEMENT_USER",200);
define("SALES_ORDER_MANAGEMENT_USER",201);
define("PROFORMA_MANAGEMENT_USER",202);

//Default Invoice Type ID
define("INVOICE_TYPE_ID",1);

// Default Challan Item Details Value :
define("FIRST_MOTOR_USER",1);
define("FIRST_MOTOR_MAKE",1);
define("FIRST_MOTOR_HP",4);
define("FIRST_MOTOR_KW",4);
define("FIRST_MOTOR_FREQUENCY",1);
define("FIRST_MOTOR_RPM",1);
define("FIRST_MOTOR_VOLTS",1);

define("SECOND_MOTOR_USER",2);
define("SECOND_MOTOR_MAKE",1);
define("SECOND_MOTOR_HP",15);
define("SECOND_MOTOR_KW",15);
define("SECOND_MOTOR_FREQUENCY",1);
define("SECOND_MOTOR_RPM",1);
define("SECOND_MOTOR_VOLTS",1);

define("THIRD_MOTOR_USER",3);
define("THIRD_MOTOR_MAKE",1);
define("THIRD_MOTOR_HP",7);
define("THIRD_MOTOR_KW",7);
define("THIRD_MOTOR_FREQUENCY",1);
define("THIRD_MOTOR_RPM",1);
define("THIRD_MOTOR_VOLTS",1);

define("FORTH_MOTOR_USER",4);
define("FORTH_MOTOR_MAKE",1);
define("FORTH_MOTOR_HP",4);
define("FORTH_MOTOR_KW",4);
define("FORTH_MOTOR_FREQUENCY",1);
define("FORTH_MOTOR_RPM",1);
define("FORTH_MOTOR_VOLTS",1);

define("FIRST_GEARBOX_USER",3);
define("FIRST_GEARBOX_MAKE",5);
define("FIRST_GEARBOX_GEAR_TYPE",1);
define("FIRST_GEARBOX_MODEL",2);
define("FIRST_GEARBOX_RATIO",2);

define("SECOND_GEARBOX_USER",4);
define("SECOND_GEARBOX_MAKE",5);
define("SECOND_GEARBOX_GEAR_TYPE",1);
define("SECOND_GEARBOX_MODEL",1);
define("SECOND_GEARBOX_RATIO",1);

//~  Customer Reminder ID
define("DELIVERY_REMINDER_ID",1);
define("PAYMENT_REMINDER_ID",2);
define("GENERAL_REMINDER_ID",3);
define("ORDER_CONFIRMATION_REMINDER_ID",4);
define("ORDER_CANCEL_REMINDER_ID",5);
define("DISPATCH_REMINDER_ID",6);
define("PRICE_UPGRADE_REMINDER_ID",7);
define("ORDER_PAYMENT_REMINDER_ID",8);
define("BALANCE_PAYMENT_REMINDER_ID",9);
define("FINANCE_BALANCE_PAYMENT_ID",10);

//~  Gujarat State ID
define("GUJARAT_STATE_ID",1);

//~  Default Party branch ID
define("DEFAULT_BRANCH_ID",3);

//~  Default Party type-2 ID
define("DEFAULT_PARTY_TYPE_2_ID",13);

//~  Default Agent ID
define("DEFAULT_AGENT_ID",7);

//~  Default Inquiry Stage ID
define("DEFAULT_INQUIRY_STAGE_ID",1);

//~  Default Industry Type ID
define("DEFAULT_INDUSTRY_TYPE_ID",15);

//~  Default Industry Type ID
define("DEFAULT_QUOTATION_TYPE_ID",23);

//~  Default Item Extra Accessories ID
define("DEFAULT_ITEM_EXTRA_ACCESSORIES_ID",1);

//~  Pending Enquiry ID
define("PENDING_ENQUIRY_ID",1);

//~  Default Purchase Item Item Group
define("DEFAULT_PURCHASE_ITEM_ITEM_GROUP_ID",5);

//~  Default Purchase Item Material Process Type
define("DEFAULT_PURCHASE_ITEM_MATERIAL_PROCESS_TYPE_ID",11);

//~  Default Purchase Item Status
define("DEFAULT_PURCHASE_ITEM_STATUS_ID",5);

//~  Default Purchase Item Status
define("TECHNICAL_DEPARTMENT_ID",12);

//~  Default Port of Loading Country
define("DEFAULT_PORT_OF_LOADING_COUNTRY_ID",1);

//~  Default Gross Weight UOM
define("DEFAULT_GROSS_WEIGHT_UOM_ID",3);

//~  Default Net Weight UOM
define("DEFAULT_NET_WEIGHT_UOM_ID",3);

//~ Letter For ID
define("LETTER_FOR_STAFF_ID",1);
define("LETTER_FOR_INTERVIEWED_PERSON_ID",2);

// INFISMS Details 
define("SEND_SMS_USERID", 'JayKhodiyar');
define("SEND_SMS_USERPASSWORD", 'khodiyar@ma123');
define("SEND_SMS_SENDERID", 'JKGRUP');
define("SEND_SMS_ACCOUNTTYPE", '2');
define("SEND_SMS_MESSAGETYPE", '0');

//~ SMS Topic IDs
define("SEND_COMPLAIN_SMS_ID",1);
define("SEND_RESOLVE_COMPLAIN_SMS_ID",2);
define("SEND_ENQUIRY_SMS_ID",3);
define("SEND_QUOTATION_SMS_ID",4);
define("SEND_SALES_ORDER_SMS_ID",5);
define("SEND_PAYMENT_RECEIPT_SMS_ID",6);
define("SEND_DISPATCH_CHALLAN_AND_INVOICE_SMS_ID",7);
define("SEND_INSTALLION_SMS_ID",8);
define("SEND_FEEDBACK_SMS_ID",9);

//~ Service Type IDs
define("FREE_SERVICE_TYPE_ID",1);
define("PAID_SERVICE_TYPE_ID",2);

//~ Sales Order stage
define("PENDING_DISPATCH_ORDER_ID",0);
define("DISPATCHED_ORDER_ID",1);
define("CANCELED_ORDER_ID",2);
define("CHALLAN_CREATED_FROM_SALES_ORDER_ID",3);
define("CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID",4);

// Default Designation ID
define("DEFAULT_DESIGNATION_ID",10);

// Default Department ID
define("DEFAULT_DEPARTMENT_ID",11);

// VISITOR CHAT NOTIFICATION AND VISITOR TAB ID
define("VISITOR_CHAT_NOTIFICATION_ID",241);
define("VISITOR_TAB_ID",242);

define("OLD_VISITOR_CHAT_DAYS", '15');

define("ALLOW_TO_CHANGE_STATUS_CANCEL_TO_PENDING",252);

// Enquiry Allow edit after Quotation
define("ENQUIRY_ALLOW_EDIT_AFTER_QUOTATION", 253); //5.2.2
// Quotation Allow edit after Sales Order
define("QUOTATION_ALLOW_EDIT_AFTER_SALES_ORDER", 254); //5.3.3

// Twilio WhatsApp Number
define("TWILIO_WHATSAPP_NUMBER", '+14155238886'); // omviravinash twilio account
