<?php
if (!function_exists('first_letters') ) {
    /**
     * @param $str
     * @return string
     */
    function first_letters($str)
    {
        $arr2 = array_filter(array_map('trim',explode(' ', $str)));
        $result='';
        foreach($arr2 as $v)
        {
            $result.=$v[0];
        }
        return $result;
    }
}
if (!function_exists('pre_die') ) {
    /**
     * @param array $array_data
     */
    function pre_die($array_data = array())
    {
        echo "<pre>";
        if (is_array($array_data) || is_object($array_data)) {
            print_r($array_data);
        } elseif(is_string($array_data)){
            echo $array_data;
        } else {
            echo "Array format not valid !";
        }
        die();
    }
}
if (!function_exists('limit_character') ) {
    /**
     * @param $string
     * @param int $character_limit
     * @return string
     */
    function limit_character($string, $character_limit = 30)
    {
        if (strlen($string) > $character_limit) {
            return substr($string, 0, $character_limit) . '...';
        } else {
            return $string;
        }
    }
}
if (!function_exists('time_ago') ) {

    /**
     * @param $time_ago
     * @return string
     */
    function time_ago($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        } //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        } //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        } //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        } //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        } //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        } //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }
}

