<?php

/**
 * Class Chatmodule
 * &@property CI_DB_active_record $db
 */
class Chatmodule extends CI_Model
{
	function __construct()
	{
		$this->load->library('session');
		parent::__construct();
	}

	function getAllAgents($id = null)
	{
		$this->db->select("*");
		$this->db->from('staff');
        if(!empty($id)){
            $where_arr = array('staff_id !=' => $id, 'active' => '1');
        } else {
            $where_arr = array('active' => '1');
        }
		$this->db->where($where_arr);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	function getAllVisitors()
	{
		$this->db->select("*");
		$this->db->from('visitors');		
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function getVisitorMessageBackend($last_id = 0)
	{
		$this->db->select("message.*,visitors.name,visitors.session_id");
		$this->db->from('message');
		$this->db->join('visitors','message.from_id = visitors.id', 'left');
		$this->db->where('message.id >',$last_id);
		$this->db->group_start(); // Open bracket
		$this->db->where('message.from_table','visitors');
		$this->db->or_where('message.to_table','visitors');
		$this->db->group_end(); // Close bracket
		$this->db->order_by('message.date_time','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$message_data = array();
			$result = $query->result_array();
			foreach ($result as $k => $v) {
				
				$to_id = $v['to_id'];
				if(!$to_id)
				{
					$result[$k]['to_name'] = 'admin';
				}
				else
				{
					$this->db->select('name');
					$this->db->from('visitors');
					$this->db->where('id', $to_id);
					$query = $this->db->get();
					$d = $query->result_array();
					$to_name = $d[0]['name'];
					if(empty($to_name) || $to_name == '')
					{
						$result[$k]['to_name'] = 'Visitor';
					}
					else
					{
						$result[$k]['to_name'] = $to_name.'(Visitor)';
					}
					
				}
			}
			$message_data['messages'] = $result;
			foreach($result as $key=>$message_row){
				$message_data['last_message_id'] = $message_data['messages'][$key]['id'];
				if($message_data['messages'][$key]['from_table'] == "visitors"){
					if($message_row['name'] == null){
						$message_data['messages'][$key]['name'] = 'Visitor';
					}else{
						$message_data['messages'][$key]['name'] = $message_row['name'].'(Visitor)';
					}
					$message_data['messages'][$key]['image'] = 'default.png';
				}else{
					$message_data['messages'][$key]['name'] = $this->get_val_by_id('staff','name','staff_id',$message_data['messages'][$key]['from_id']);
					$message_data['messages'][$key]['image'] = $this->get_val_by_id('staff','image','staff_id',$message_data['messages'][$key]['from_id']);
				}
				
			}
			return $message_data;
		} else {
			return false;
		}
	}

	public function get_visitor_history_message($visitor_id){
		$this->db->select("message.*,visitors.name");
		$this->db->from('message');
		$this->db->join('visitors','message.from_id = visitors.id', 'left');
		$this->db->where('message.from_table','visitors');
		$this->db->where('message.from_id',$visitor_id);
		$this->db->or_where('message.to_table','visitors');
		$this->db->where('message.to_id',$visitor_id);
		$this->db->order_by('message.date_time','asc');
		$this->db->limit(20);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$message_data = array();
			$result = $query->result_array();
			$response_data = '';
			$message_data['messages'] = $result;
			foreach($result as $key=>$message_row){
				$message_data['last_message_id'] = $message_data['messages'][$key]['id'];
				if($message_data['messages'][$key]['from_table'] == "visitors"){
					if($message_row['name'] == null){
						$message_data['messages'][$key]['name'] = 'Visitor';
					}else{
						$message_data['messages'][$key]['name'] = $message_row['name'].'(Visitor)';
					}
					$message_data['messages'][$key]['image'] = 'default.png';
					$response_data .= $this->load->view('shared/right_chat_html',$message_data['messages'][$key],true);
				}else{
					$message_data['messages'][$key]['name'] = $this->get_val_by_id('staff','name','staff_id',$message_data['messages'][$key]['from_id']);
					$message_data['messages'][$key]['image'] = $this->get_val_by_id('staff','image','staff_id',$message_data['messages'][$key]['from_id']);
					$response_data .= $this->load->view('shared/left_chat_html',$message_data['messages'][$key],true);
				}
			}
			return $response_data;
		} else {
			return false;
		}
	}

	public function getVisitorMessage($visitor_id,$last_message_id = 0){
		$this->db->select("message.*,staff.name,staff.image");
		$this->db->from('message');
		$this->db->join('staff', 'message.from_id = staff.staff_id', 'left');
		$this->db->where('message.id >',$last_message_id);		
		$this->db->where('message.to_table','visitors');
		$this->db->where('message.to_id',$visitor_id);
		$this->db->or_group_start(); // Open bracket
		$this->db->where('message.from_id', $visitor_id);
		$this->db->where('message.from_table', 'visitors');
		$this->db->group_end(); // Close bracket
		$this->db->order_by('message.date_time','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		if ($query->num_rows() > 0) {
			$data = $query->result_array();
			return $data;
		} else {
			return false;
		}
	}

	public function get_val_by_id($table_name,$column_name,$id_column,$id_val){
		$this->db->select($column_name);
		$this->db->from($table_name);
		$this->db->where($id_column,$id_val);
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$column_name;
		}else{
			return '';
		}
	}

	public function getMyMessage($myid, $lastid = 0, $userid = 0)
	{

		$this->db->select("message.*,staff.image,staff.name as from_name");
		$this->db->from('message');
		$this->db->join('staff', 'message.from_id = staff.staff_id', 'left');
		$this->db->where('message.id >', $lastid);
		$this->db->where('message.to_table', 'admin');
		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', $myid);
		$this->db->or_where('message.from_id', $myid);
		$this->db->group_end(); // Close bracket
		$this->db->order_by('message.date_time', 'asc');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$data = $query->result_array();
			foreach ($data as $k => $v) {
				$to_id = $v['to_id'];
				$this->db->select('name');
				$this->db->from('staff');
				$this->db->where('staff_id', $to_id);
				$query = $this->db->get();
				$result = $query->result_array();
				$to_name = $result[0]['name'];
				$data[$k]['to_name'] = $to_name;
			}
			return $data;
		} else {
			return false;
		}
	}

	public function getAllMessage($myid, $lastid = 0, $userid = 0)
	{

		$this->db->select("message.*,staff.image,staff.email,staff.birth_date,staff.contact_no,staff.name as from_name");
		$this->db->from('message');
		$this->db->join('staff', 'message.from_id = staff.staff_id', 'left');
		//$this->db->where('message.id >',$lastid);
		$this->db->where('message.to_table', 'admin');
	//	$this->db->or_where('message.to_table', 'admin,staff');
		//$this->db->or_where('message.to_id', 'all');
		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', $myid);
		$this->db->or_where('message.from_id', $myid);
		$this->db->group_end(); // Close bracket

		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', $userid);
		$this->db->or_where('message.from_id', $userid);
		$this->db->group_end(); // Close bracket
		$this->db->order_by('message.id', 'desc');
		$this->db->limit(15);

		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			//return $query->result_array();
			$data = $query->result_array();
			foreach ($data as $k => $v) {
				$to_id = $v['to_id'];
				$this->db->select('name');
				$this->db->from('staff');
				$this->db->where('staff_id', $to_id);
				$query = $this->db->get();
				$result = $query->result_array();
				$to_name = $result[0]['name'];
				$data[$k]['to_name'] = $to_name;
			}
			return $data;
		} else {
			return false;
		}
	}
	public function getScrollMessage($myid, $lastid = 0, $userid = 0,$limit,$offset)
	{

		$this->db->select("message.*,staff.image,staff.email,staff.birth_date,staff.contact_no,staff.name as from_name");
		$this->db->from('message');
		$this->db->join('staff', 'message.from_id = staff.staff_id', 'left');
		//$this->db->where('message.id >',$lastid);
		$this->db->where('message.to_table', 'admin');
	//	$this->db->or_where('message.to_table', 'admin,staff');
		//$this->db->or_where('message.to_id', 'all');
		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', $myid);
		$this->db->or_where('message.from_id', $myid);
		$this->db->group_end(); // Close bracket

		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', $userid);
		$this->db->or_where('message.from_id', $userid);
		$this->db->group_end(); // Close bracket
		$this->db->order_by('message.id', 'desc');
		$this->db->limit($limit,$offset);

		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			//return $query->result_array();
			$data = $query->result_array();
			foreach ($data as $k => $v) {
				$to_id = $v['to_id'];
				$this->db->select('name');
				$this->db->from('staff');
				$this->db->where('staff_id', $to_id);
				$query = $this->db->get();
				$result = $query->result_array();
				$to_name = $result[0]['name'];
				$data[$k]['to_name'] = $to_name;
			}
			return $data;
		} else {
			return false;
		}
	}

	public function getAllMessageForCron($myid, $lastid = 0, $userid = 0)
	{

		$this->db->select("message.*,staff.image,staff.email,staff.birth_date,staff.contact_no,staff.name as from_name");
		$this->db->from('message');
		$this->db->join('staff', 'message.from_id = staff.staff_id', 'left');
		
        $this->db->group_start(); // Open bracket
        $this->db->where('message.from_id', $myid);
        $this->db->where('message.to_id', $userid);
        $this->db->where('message.to_table', 'admin');
		$this->db->where('message.msg_email', 0);
		$this->db->group_end(); // Close bracket

        $this->db->group_start('', 'OR'); // Open bracket
        $this->db->where('message.from_id', $userid);
        $this->db->where('message.to_id', $myid);
        $this->db->where('message.to_table', 'admin');
		$this->db->where('message.msg_email', 0);
		$this->db->group_end(); // Close bracket

		$this->db->order_by('message.date_time', 'asc');

		$query = $this->db->get();
//		echo $this->db->last_query();
        
        if ($query->num_rows() > 0) {
			//return $query->result_array();
			$data = $query->result_array();
			foreach ($data as $k => $v) {
				$to_id = $v['to_id'];
				$this->db->select('name');
				$this->db->from('staff');
				$this->db->where('staff_id', $to_id);
				$query = $this->db->get();
				$result = $query->result_array();
				$to_name = $result[0]['name'];
				$data[$k]['to_name'] = $to_name;
			}
			
			$update_data = array('msg_email' => 1);
			$this->db->group_start(); // Open bracket
            $this->db->where('message.from_id', $myid);
            $this->db->where('message.to_id', $userid);
            $this->db->where('message.to_table', 'admin');
            $this->db->where('message.msg_email', 0);
            $this->db->group_end(); // Close bracket

            $this->db->group_start('', 'OR'); // Open bracket
            $this->db->where('message.from_id', $userid);
            $this->db->where('message.to_id', $myid);
            $this->db->where('message.to_table', 'admin');
            $this->db->where('message.msg_email', 0);
            $this->db->group_end(); // Close bracket
			$this->db->update('message',$update_data);
			
			return $data;
		} else {
			return false;
		}
	}
	
	public function getAllVisitorMessage($myid,$userid = 0)
	{			
		$visitor_id = $this->fetch_visitor_by_session_id($userid);
		$this->db->select("message.*,visitors.email,visitors.phone,visitors.session_id,visitors.name as from_name");
		$this->db->from('message');
		$this->db->join('visitors', 'message.from_id = visitors.id', 'left');		
		$this->db->group_start(); // Open bracket
		$this->db->where('message.from_id', $visitor_id);
		$this->db->where('message.from_table', 'visitors');
//		$this->db->where('message.msg_email', 0);
		$this->db->group_end(); // Close bracket
		
		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', '0');
		$this->db->where('message.to_table','staff,admin');
		$this->db->group_end(); // Close bracket
		
		$this->db->or_group_start(); // Open bracket
		$this->db->where('message.to_id',$visitor_id);
		$this->db->where('message.to_table','visitors');
		$this->db->group_end(); // Close bracket
		$this->db->order_by('message.date_time', 'asc');

		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			//return $query->result_array();
			$data = $query->result_array();
			//echo "<pre>";print_r($data);exit;
			foreach ($data as $k => $v) {
				$to_id = $v['to_id'];
				$this->db->select('name');
				$this->db->from('staff');
				$this->db->where('staff_id', $to_id);
				$query = $this->db->get();
				$result = $query->result_array();
				if($result)
				{
					$to_name = $result[0]['name'];				
					$data[$k]['to_name'] = $to_name;
				}				
				if($data[$k]['from_table'] == "visitors"){
					if($data[$k]['from_name'] == null || $data[$k]['from_name'] == ""){
						$data[$k]['name'] = 'Visitor';
					}else{
						$data[$k]['name'] = $data[$k]['from_name'].' (Visitor)';
					}
					$data[$k]['image'] = 'default.png';
				}else{
					$data[$k]['name'] = $this->get_val_by_id('staff','name','staff_id',$data[$k]['from_id']);
					$data[$k]['image'] = $this->get_val_by_id('staff','image','staff_id',$data[$k]['from_id']);
				}
			}
			return $data;
		} else {
			return false;
		}
	}
    
	public function getVisitorLastMessage($myid,$userid = 0)
	{
		$visitor_id = $this->fetch_visitor_by_session_id($userid);
		$this->db->select("message.*,visitors.email,visitors.phone,visitors.session_id,visitors.name as from_name");
		$this->db->from('message');
		$this->db->join('visitors', 'message.from_id = visitors.id', 'left');
		$this->db->group_start(); // Open bracket
		$this->db->where('message.from_id', $visitor_id);
		$this->db->where('message.from_table', 'visitors');
//		$this->db->where('message.msg_email', 0);
		$this->db->group_end(); // Close bracket
		
		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', '0');
		$this->db->where('message.to_table','staff,admin');
		$this->db->group_end(); // Close bracket
		
		$this->db->or_group_start(); // Open bracket
		$this->db->where('message.to_id',$visitor_id);
		$this->db->where('message.to_table','visitors');
		$this->db->group_end(); // Close bracket
        $this->db->limit(1);
		$this->db->order_by('message.date_time', 'desc');

		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			//return $query->result_array();
			$data = $query->result_array();
			//echo "<pre>";print_r($data);exit;
			foreach ($data as $k => $v) {
				$to_id = $v['to_id'];
				$this->db->select('name');
				$this->db->from('staff');
				$this->db->where('staff_id', $to_id);
				$query = $this->db->get();
				$result = $query->result_array();
				if($result)
				{
					$to_name = $result[0]['name'];
					$data[$k]['to_name'] = $to_name;
				}
				if($data[$k]['from_table'] == "visitors"){
					if($data[$k]['from_name'] == null || $data[$k]['from_name'] == ""){
						$data[$k]['name'] = 'Visitor';
					}else{
						$data[$k]['name'] = $data[$k]['from_name'].' (Visitor)';
					}
					$data[$k]['image'] = 'default.png';
				}else{
					$data[$k]['name'] = $this->get_val_by_id('staff','name','staff_id',$data[$k]['from_id']);
					$data[$k]['image'] = $this->get_val_by_id('staff','image','staff_id',$data[$k]['from_id']);
				}
			}
			return $data;
		} else {
			return false;
		}
	}
	public function getVisitorMessageForMail($userid = 0)
	{		
		$data = array();	
		$visitor_id = $this->fetch_visitor_by_session_id($userid);
		$this->db->select("message.*,visitors.email,visitors.phone,visitors.session_id,visitors.name as from_name");
		$this->db->from('message');
		$this->db->join('visitors', 'message.from_id = visitors.id', 'left');
        
        $this->db->group_start(); // Open bracket
        $this->db->where('message.from_id', $visitor_id);
        $this->db->where('message.from_table', 'visitors');
        $this->db->where('message.to_id', '0');
        $this->db->where('message.to_table','staff,admin');
        $this->db->where('message.msg_email', 0);
        $this->db->group_end(); // Close bracket

        $this->db->group_start('', 'OR'); // Open bracket
        $this->db->where('message.to_id',$visitor_id);
        $this->db->where('message.to_table','visitors');
        $this->db->where('message.msg_email', 0);
        $this->db->group_end(); // Close bracket

        $this->db->order_by('message.date_time', 'asc');

		$query = $this->db->get();
//		echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			//return $query->result_array();
			$data = $query->result_array();
			foreach ($data as $k => $v) {
				$to_id = $v['to_id'];
				$this->db->select('name');
				$this->db->from('staff');
				$this->db->where('staff_id', $to_id);
				$query = $this->db->get();
				$result = $query->result_array();
				if($result)
				{
					$to_name = $result[0]['name'];				
					$data[$k]['to_name'] = $to_name;
				}				
				if($data[$k]['from_table'] == "visitors"){
					if($data[$k]['from_name'] == null || $data[$k]['from_name'] == ""){
						$data[$k]['name'] = 'Visitor';
					}else{
						$data[$k]['name'] = $data[$k]['from_name'].' (Visitor)';
					}
					$data[$k]['image'] = 'default.png';
				}else{
					$data[$k]['name'] = $this->get_val_by_id('staff','name','staff_id',$data[$k]['from_id']);
					$data[$k]['image'] = $this->get_val_by_id('staff','image','staff_id',$data[$k]['from_id']);
				}
			}
            
            $update_data = array('msg_email' => 1);
			$this->db->group_start(); // Open bracket
			$this->db->where('from_id', $visitor_id);
			$this->db->where('from_table', 'visitors');
			$this->db->where('to_id', '0');
			$this->db->where('to_table','staff,admin');
            $this->db->where('msg_email', 0);
			$this->db->group_end(); // Close bracket

			$this->db->group_start('', 'OR'); // Open bracket
			$this->db->where('to_id',$visitor_id);
			$this->db->where('to_table','visitors');
            $this->db->where('msg_email', 0);
			$this->db->group_end(); // Close bracket

			$this->db->update('message',$update_data);
            
			return $data;
		} else {
			return 0;
		}
	}

	public function addChatMessage($data)
	{
		if ($this->db->insert('message',$data)) {
			return $this->db->insert_id();
		}
		return false;
	}

	public function getStaffById($id)
	{
		$this->db->select("*");
		$this->db->from('staff');
		$this->db->where('staff_id = ', $id);
        $this->db->order_by('name','ASC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function getVisitorById($id)
	{
		$this->db->select("*");
		$this->db->from('visitors');
		$this->db->where('session_id = ', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}


	/**
	 * @param $visitor_name
	 * @param $visitor_phone
	 * @param $visitor_email
	 * @return int
	 */
	public function register_visitor($visitor_name, $visitor_phone, $visitor_email,$session_id = 0, $chat_through = '1') // 1 = Chat Through Chat Box
	{
		if($visitor_email == "")
		{			
			$visitor_data = array(
				'name' => $visitor_name,
				'phone' => $visitor_phone,
				'email' => $visitor_email,
				'chat_through' => $chat_through,
				'session_id' => $session_id,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$this->db->insert('visitors', $visitor_data);
			return $this->db->insert_id();
		}
		else
		{
			$this->db->select('id');
			$this->db->from('visitors');
			$this->db->where('LOWER(email)',strtolower($visitor_email));
			$query = $this->db->get();
			if($query->num_rows() > 0){
				$visitor_data = array(
					'name' => $visitor_name,
					'phone' => $visitor_phone,
					'email' => $visitor_email,
				);
				$this->db->where('id',$query->row()->id);
				$this->db->update('visitors',$visitor_data);
				return $query->row()->id;
			}else{
				$this->db->select('id');
				$this->db->from('visitors');
				$this->db->where('session_id',$session_id);
				$querys = $this->db->get();
				if($querys->num_rows() > 0){
					$visitor_data = array(
						'name' => $visitor_name,
						'phone' => $visitor_phone,
						'email' => $visitor_email,
					);
					$this->db->where('id',$querys->row()->id);
					$this->db->update('visitors',$visitor_data);
					$_SESSION['is_visitor_logged_in']['name'] = $visitor_name;
					$_SESSION['is_visitor_logged_in']['phone'] = $visitor_phone;
					$_SESSION['is_visitor_logged_in']['email'] = $visitor_email;
					return $querys->row()->id;
				}
				else
				{
					$visitor_data = array(
						'name' => $visitor_name,
						'phone' => $visitor_phone,
						'email' => $visitor_email,
                                                'chat_through' => $chat_through,
						'session_id' => $session_id,
						'created_at' => date('Y-m-d H:i:s'),
					);
					$this->db->insert('visitors', $visitor_data);
					return $this->db->insert_id();
				}				
			}
		}
	}

	function fetch_visitor_by_email($visitor_email)
	{
		$this->db->select('id');
		$this->db->from('visitors');
		$this->db->where('LOWER(email)',strtolower($visitor_email));
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		}else{
			return false;
		}
	}

	function fetch_visitor_by_session_id($session_id)
	{		
		$this->db->select('id');
		$this->db->from('visitors');
		$this->db->where('session_id',$session_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		}else{
			return false;
		}
	}
	
	function fetch_session_id_by_visitor($visitor_id)
	{		
		$this->db->select('session_id');
		$this->db->from('visitors');
		$this->db->where('id',$visitor_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->session_id;
		}else{
			return false;
		}
	}
	
	function fetch_visitor_by_id($visitor_id)
	{
		$this->db->select('*');
		$this->db->from('visitors');
		$this->db->where('id',$visitor_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}else{
			return false;
		}
	}

	

	/**
	 * @param $visitor_id
	 * @param $message
	 * @return int
	 */
	public function send_visitor_message($visitor_id,$message)
	{
		$message_data['from_id'] = $visitor_id;
		$message_data['from_table'] = 'visitors';
		$message_data['to_id'] = 0;
		$message_data['to_table'] = 'staff,admin';
		$message_data['message'] = $message;
		$message_data['date_time'] = date('Y-m-d H:i:s');
		$this->db->insert('message',$message_data);
		return $this->db->insert_id();
	}

	public function updateReadMsg($userid)
	{
		$session = $this->session->all_userdata();

		$data = array('is_read' => '1');
		$this->db->where('from_id', $userid);
		$this->db->where('to_id', $session['is_logged_in']['staff_id']);
		$res = $this->db->update('message', $data);
		return $res;
	}
	public function updateVisitorReadMsg($userid)
	{		
		$visitor_id = $this->fetch_visitor_by_session_id($userid);		
		$session = $this->session->all_userdata();
		$data = array('is_read' => '1');
		$this->db->group_start(); // Open bracket
		$this->db->where('message.from_id', $visitor_id);
		$this->db->where('message.from_table', 'visitors');
		$this->db->group_end(); // Close bracket
		
		$this->db->group_start(); // Open bracket
		$this->db->where('message.to_id', '0');
		$this->db->where('message.to_table','staff,admin');
		$this->db->group_end(); // Close bracket
		
		$res = $this->db->update('message', $data);		
		return $res;
	}

	public function getUnreadMsg($id)
	{
		$this->db->select("*");
		$this->db->from('message');
		$this->db->where('to_id = ', $id);
		$this->db->where('is_read = ', '0');
		$query = $this->db->get();
		return $query->num_rows();
	}
	function checkAnyStaffLogin(){
        $this->db->select('staff_id');
        $this->db->from('staff');
        $this->db->where('is_login',1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }
    public function updateCroneMsg($msgid_in)
	{
		$data = array('msg_email' => '1');
		//$this->db->where_in('id', $msgid_in);
		 $this->db->where("id in ($msgid_in)");
		$res = $this->db->update('message', $data);
		//echo $this->db->last_query();exit;
		return $res;
	}
	function getAllAgentsForCron()
	{
		$this->db->select("*");
		$this->db->from('staff');
		$this->db->order_by('staff_id', 'asc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	public function updateDisconnectMailValue($is_disconnect_mail_sent,$visitor_id)
	{
		$data = array('is_disconnect_mail_sent' => $is_disconnect_mail_sent);
		$this->db->where("id",$visitor_id);
		$res = $this->db->update('visitors', $data);
		return $res;
	}
	public function updateFromAddress($session_id, $ip, $from_address, $http_host, $socket_id)
	{
        $data = array();
        if(empty($ip)){
            $data['is_login'] = '1';
        } else {
            $data['http_host_name'] = $http_host;
            $data['ip'] = $ip;
            $data['from_address'] = $from_address;
            $data['socket_id'] = $socket_id;

            /*Get user ip address details with geoplugin.net*/
    //		$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip;
    //		$addrDetailsArr = unserialize(file_get_contents($geopluginURL));

            /*Get data by return array*/
    //		$data['city'] = $addrDetailsArr['geoplugin_city']; 
    //		$data['currency'] = $addrDetailsArr['geoplugin_currencyCode']; 
    //		$data['country'] = $addrDetailsArr['geoplugin_countryCode'];
    //		$data['others'] = $addrDetailsArr['geoplugin_region'].",".$addrDetailsArr['geoplugin_areaCode'].",".$addrDetailsArr['geoplugin_dmaCode'].",".$addrDetailsArr['geoplugin_countryCode'].",".$addrDetailsArr['geoplugin_continentCode'].",".$addrDetailsArr['geoplugin_latitude'].",".$addrDetailsArr['geoplugin_longitude'].",".$addrDetailsArr['geoplugin_regionCode'].",".$addrDetailsArr['geoplugin_currencySymbol'].",".$addrDetailsArr['geoplugin_currencyConverter'];

            $ip_address_details = file_get_contents("http://timezoneapi.io/api/ip/?" . $ip);
            $ip_address_details = json_decode($ip_address_details, true);
            $data['city'] = $ip_address_details['data']['city'];
            $data['state'] = $ip_address_details['data']['state'];
            $data['country'] = $ip_address_details['data']['country'];
            $data['currency'] = $ip_address_details['data']['timezone']['currency_alpha_code'];
            $data['location'] = $ip_address_details['data']['location'];
            $data['timezone'] = $ip_address_details['data']['datetime']['offset_gmt'] .' '. $ip_address_details['data']['datetime']['date_time_txt'];
        }
		$this->db->where('session_id', $session_id);
		$res = $this->db->update('visitors', $data);
		return $res;
	}
	
	function fetch_visitor_by_email2($visitor_email)
	{
		$this->db->select('*');
		$this->db->from('visitors');
		$this->db->where('LOWER(email)',strtolower($visitor_email));
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}else{
			return false;
		}
	}
	
	function fetch_visitor_by_uuid($uuid)
	{
		$this->db->select('*');
		$this->db->from('visitors');
		$this->db->where('session_id',$uuid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}else{
			return false;
		}
	}
}
