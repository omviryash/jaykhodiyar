<?php

/**
 * Class Crud
 * &@property CI_DB_active_record $db
 */
class Crud extends CI_Model
{
	public $isManagement;
	public $cu_accessExport;
	public $cu_accessDomestic;
	function __construct()
	{
		parent::__construct();
		$this->isManagement = $this->applib->have_access_current_user_rights(MANAGEMENT_USER,"allow");
		$this->cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$this->cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
	}

	/**
 * @param $table_name
 * @param $data_array
 * @return bool
 */
	function insert($table_name,$data_array){
		if($this->db->insert($table_name,$data_array))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	function insertFromSql($sql)
	{
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	function execuetSQL($sql){
		$this->db->query($sql);
	}
	function getFromSQL($sql)
	{
		return $this->db->query($sql)->result();
	}

	/**
 * @param $table_name
 * @param $order_by_column
 * @param $order_by_value
 * @return bool
 */
	function get_all_records($table_name,$order_by_column,$order_by_value){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
 * @param $table_name
 * @param $order_by_column
 * @param $order_by_value
 * @param $where_array
 * @return bool
 */
	function get_all_with_where($table_name,$order_by_column,$order_by_value,$where_array)
	{
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_array);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function get_all_pending_reminder($user_id,$date = null,$onLoad = null, $created_by = null)
	{
		$this->db->select("r.*,s.name");
		$this->db->from('reminder r');
		$this->db->join('staff s','s.staff_id = r.user_id','left');
        $this->db->where('r.status',1);
		
		if($date == date('Y-m-d') && !empty($onLoad)){
			$this->db->where('DATE(r.reminder_date) <=',date('Y-m-d'));
		}else{
			$this->db->where('DATE(r.reminder_date)',$date);
		}
		
		if($this->applib->have_access_current_user_rights(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"ismanagement") == 1){
            if(!empty($created_by)){
                $this->db->where('r.user_id',$created_by);
            }
		} else {
			$this->db->group_start();
			$this->db->where('r.assigned_to',$user_id);
			$this->db->or_where('r.user_id',$user_id);
			$this->db->group_end();
		}
		$this->db->order_by('r.reminder_date','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function get_all_pending_review_lead($user_id,$date = null)
	{
		$this->db->select("l.*,s.name as assigned_to_name");
		$this->db->from('lead_details l');
		$this->db->join('staff s','s.staff_id = l.assigned_to_id','left');
		if($date != null){
			$this->db->where('DATE(l.review_date) <= ',$date);
		}else{
			$this->db->where('l.review_date <= NOW()');
		}
		$this->db->where('l.created_by',$user_id);
		$this->db->order_by('l.review_date','DESC');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		//if ($query->num_rows() > 0){
		return $query->result_array();
		/*}else{
return false;
}*/
	}


	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function get_all_due_sales_order($user_id,$date = null,$isApproved = null,$onLoad = null, $created_by = null)
	{
		$this->db->select("so.*,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('sales_order so');
		$this->db->join('staff s', 's.staff_id = so.created_by', 'left');
		$this->db->join('party p', 'p.party_id = so.sales_to_party_id', 'left');
		if ($date == date('Y-m-d') && !empty($onLoad)) {
			$this->db->where('DATE(so.committed_date) <=',date('Y-m-d'));
		} else {
			$this->db->where('DATE(so.committed_date)', $date);
		}
		$this->db->where('p.active !=', 0);
		if($isApproved == null){
			$this->db->where('so.isApproved', 0);
		}else{
			$this->db->where('so.isApproved', 1);
			$this->db->where('so.is_dispatched !=', 1);
		}
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('p.current_party_staff_id',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('so.committed_date', 'DESC');
		$query = $this->db->get();
//        echo $this->db->last_query(); exit;
		return $query->result_array();
	}
        
        function get_all_due_pending_invoice($user_id,$date = null,$isApproved = null,$onLoad = null, $created_by = null)
	{
		$this->db->select("ch.*,ch.challan_date as review_date,ch.id as challan_id,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('challans ch');
		$this->db->join('staff s', 's.staff_id = ch.created_by', 'left');
		$this->db->join('party p', 'p.party_id = ch.sales_to_party_id', 'left');
		if ($date == date('Y-m-d') && !empty($onLoad)) {
			$this->db->where('DATE(ch.challan_date) <=',date('Y-m-d'));
		} else {
			$this->db->where('DATE(ch.challan_date)', $date);
		}
		$this->db->where('p.active !=', 0);
                $this->db->where('ch.is_invoice_created', '0');
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('ch.created_by',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('ch.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('ch.challan_date', 'DESC');
		$query = $this->db->get();
//        echo $this->db->last_query(); exit;
		return $query->result_array();
	}
        
        function get_all_due_pending_testing($user_id,$date = null,$isApproved = null,$onLoad = null, $created_by = null)
	{
		$this->db->select("ch.*,ch.challan_date as review_date,ch.id as t_challan_id,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('challans ch');
		$this->db->join('staff s', 's.staff_id = ch.created_by', 'left');
		$this->db->join('party p', 'p.party_id = ch.sales_to_party_id', 'left');
		if ($date == date('Y-m-d') && !empty($onLoad)) {
			$this->db->where('DATE(ch.challan_date) <=',date('Y-m-d'));
		} else {
			$this->db->where('DATE(ch.challan_date)', $date);
		}
		$this->db->where('p.active !=', 0);
                $this->db->where('ch.is_testing_report_created', '0');
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('ch.created_by',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('ch.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('ch.challan_date', 'DESC');
		$query = $this->db->get();
//        echo $this->db->last_query(); exit;
		return $query->result_array();
	}
        
        function get_all_due_pending_install($user_id,$date = null,$isApproved = null,$onLoad = null, $created_by = null)
	{
		$this->db->select("tr.testing_report_id,tr.created_by,tr.testing_report_date as review_date,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('testing_report tr');
		$this->db->join('staff s', 's.staff_id = tr.created_by', 'left');
		$this->db->join('party p', 'p.party_id = tr.party_id', 'left');
		if ($date == date('Y-m-d') && !empty($onLoad)) {
			$this->db->where('DATE(tr.testing_report_date) <=',date('Y-m-d'));
		} else {
			$this->db->where('DATE(tr.testing_report_date)', $date);
		}
		$this->db->where('p.active !=', 0);
                $this->db->where('tr.is_installation_created', '0');
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('tr.created_by',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('tr.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('tr.testing_report_date', 'DESC');
		$query = $this->db->get();
//        echo $this->db->last_query(); exit;
		return $query->result_array();
	}

	/**
 * @param $table_name
 * @param array $where
 * @return mixed
 */
	function get_num_rows_with_where($table_name,$where = array())
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		$query = $this->db->get($table_name);
		return $query->num_rows();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function get_all_pending_inquiry($user_id,$date = null,$onLoad = null, $created_by = null)
	{
		$this->db->select("i.*,s.name as assigned_to_name,s2.name as created_by_name,p.party_name as inquiry_party_name");
		$this->db->from('inquiry i');
		$this->db->join('staff s', 's.staff_id = i.assigned_to_id', 'left');
		$this->db->join('staff s2', 's2.staff_id = i.created_by', 'left');
		$this->db->join('party p', 'p.party_id = i.party_id', 'left');

		if($this->isManagement == 1){
            if(!empty($created_by)){
                $this->db->where('p.current_party_staff_id',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}

		$this->db->where_not_in('i.inquiry_id', 'SELECT enquiry_id FROM quotations');
		$this->db->where('p.active !=', 0);
		if ($date == date('Y-m-d') && !empty($onLoad)) {
			$this->db->where('DATE(i.due_date) <=', date('Y-m-d'));
		} else {
			$this->db->where('DATE(i.due_date)', $date);
		}
        $this->db->where('i.inquiry_status_id', PENDING_ENQUIRY_ID);
		$this->db->order_by('i.created_at', 'DESC');
		$query = $this->db->get();
//        echo $this->db->last_query(); exit;
		return $query->result_array();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function get_all_pending_review_quotation($user_id,$date = null,$onLoad = null, $created_by = null)
	{
		$this->db->select("q.*,p.party_name,s.name as staff_name");
		$this->db->from('quotations q');
		$this->db->join('party p','p.party_id = q.party_id','left');
		$this->db->join('staff s','s.staff_id = q.created_by','left');
		if($this->applib->have_access_current_user_rights(QUOTATION_MANAGEMENT_USER,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('p.current_party_staff_id',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id',$user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->where_not_in('q.id', 'SELECT quotation_id FROM sales_order');
		$this->db->where('p.active !=',0);
		if($date == date('Y-m-d') && !empty($onLoad)){
			$this->db->where('q.created_at <=',date('Y-m-d'));
		}else{
			$this->db->where('DATE(q.created_at)',$date);
		}
        $status_ids = array();
        $status_ids[] = QUOTATION_STATUS_HOT;
        $status_ids[] = QUOTATION_STATUS_COLD;
		$this->db->where_in('q.quotation_status_id', $status_ids);
		$this->db->order_by('q.due_date','DESC');
		$query = $this->db->get();
//         echo $this->db->last_query(); exit;
		return $query->result_array();
	}

	/**
 * @param $tbl_name
 * @param $column_name
 * @param $where_id
 * @return mixed
 */
	function get_column_value_by_id($tbl_name,$column_name,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where_id);
		$this->db->last_query();
		$query = $this->db->get();
		return $query->row($column_name);
	}

	/**
 * @param $table_name
 * @param $where_id
 * @return mixed
 */
	function get_row_by_id($table_name,$where_id){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_id);
		$query = $this->db->get();
		return $query->result();
	}
    
	function get_row_by_where_in_ids($table_name, $column_name, $where_ids){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where_in($column_name, $where_ids);
		$query = $this->db->get();
		return $query->result();
	}

	/**
 * @param $table_name
 * @param $where_array
 * @return mixed
 */
	function delete($table_name,$where_array){
        $result = $this->db->delete($table_name,$where_array);
        $return = array();
        if ($result == '') {
            $return['error'] = "Error";
        }
        else {
            $return['success'] = 'Deleted';
        }
        return $return;
	}
    
	function delete_where_in($table_name, $column_name, $column_value_array){
        $this->db->where_in($column_name, $column_value_array);
        $result = $this->db->delete($table_name);
		return $result;
	}

	/**
 * @param $table_name
 * @param $data_array
 * @param $where_array
 * @return mixed
 */
	function update($table_name,$data_array,$where_array){
		$this->db->where($where_array);
		$rs = $this->db->update($table_name, $data_array);
		return $rs;
	}

	/**
 * @return string
 */
	function get_state()
	{
		if(isset($_POST['country_id'])){

			$countryID =  $_POST['country_id'];
			$where = array('country_id'=>$countryID);
			$result = $this->get_all_with_where('state','state','ASC',$where);
			$option = '<option value="">Select State</option>';

			if($result)
			{
				foreach ($result as $row){
					$state_id = $row->state_id;
					$state = $row->state;
					$option .= '<option value="'.$state_id.'">'.$state.'</option>';
				}
			}
			return $option;
		}
	}

	/**
 * @param $data
 * @return mixed
 */
	function search_leads($data)
	{

		$lead_source = $data['lead_source'];
		$lead_status = $data['lead_status'];
		$industry_type = $data['industry_type'];
		$lead_owner = $data['lead_owner'];
		$assigned_to = $data['assigned_to'];
		$created_by = $data['created_by'];
		$this->db->select("l.*,p.party_name");
		$this->db->from("lead_details l");
		$this->db->join("party p", "p.party_id = l.party_id");
		$accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
		$accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
		if ($accessExport == 1 && $accessDomestic == 1) {

		} else if ($accessExport == 1) {
			$this->db->where("p.party_type_1", PARTY_TYPE_EXPORT_ID);
		} else if ($accessDomestic == 1) {
			$this->db->where("p.party_type_1", PARTY_TYPE_DOMESTIC_ID);
		}

		if ($data['lead_source'] != "" && $lead_source != '') {
			$this->db->where("l.lead_source_id", $lead_source);
		}

		if ($data['lead_status'] != "" && $lead_status != '') {
			$this->db->where("l.lead_status_id", $lead_status);
		}

		if ($data['industry_type'] != "" && $industry_type != '') {
			$this->db->where("l.industry_type_id", $industry_type);
		}

		if ($data['lead_owner'] != "" && $lead_owner != '') {
			$this->db->where("l.lead_owner_id", $lead_owner);
		}

		if ($data['assigned_to'] != "" && $assigned_to != '') {
			$this->db->where("l.assigned_to_id", $assigned_to);
		}

		if ($created_by != 'all') {
			$this->db->where("l.created_by", $created_by);
		}

		$query = $this->db->get();
		return $result = $query->result();

	}

	/**
 * @param $name
 * @param $path
 * @return bool
 */
	function upload_file($name, $path)
	{
		$config['upload_path'] = $path;
		$config ['allowed_types'] = '*';
		$this->upload->initialize($config);
		if($this->upload->do_upload($name))
		{
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
		return false;
	}

	/**
 * @param $table
 * @param $column
 * @param $search_value
 * @param array $where
 * @return mixed
 */
	function getAutoCompleteData($table,$column,$search_value,$where = array())
	{
		$this->db->select('*');
		$this->db->from($table);
		if(!empty($where)) {
			$this->db->where($where);
		}
		$this->db->like($column, $search_value);
		$query = $this->db->get();
		return $query->result();
	}

	/**
 * @param $table
 * @param $id_column
 * @param $column
 * @param $column_val
 * @return null
 */
	function get_id_by_val($table,$id_column,$column,$column_val){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column,$column_val);
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}

	function get_column_value_by_quotation($where_id)
	{
		$select = "q.*,curr.currency,sales.sales,sales_type.sales_type,qs.quotation_stage,qt.quotation_type,p.party_code,
					st.state,co.country,c.city,p.party_name,p.party_type_1,p.reference_description,p.reference_id as p_reference_id,p.address,p.city_id as p_city_id,p.country_id,
					p.fax_no,p.email_id,p.project,p.pincode,p.phone_no,p.agent_id as p_agent_id,p.gst_no,p.utr_no,q.contact_person_name,q.contact_person_contact_no,
					q.contact_person_email_id,p.tin_vat_no,p.tin_cst_no,p.ecc_no,p.website,p.branch_id,p.party_name as pname, stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('quotations q');
		$this->db->join('party p','p.party_id = q.party_id','left');
		$this->db->join('quotation_stage qs','qs.id = q.quotation_stage_id','left');
		$this->db->join('quotation_type qt','qt.id = q.quotation_type_id','left');
		$this->db->join('sales_type sales_type','sales_type.id = q.sales_type_id','left');
		$this->db->join('sales sales','sales.id = p.party_type_1','left');
		$this->db->join('currency curr','curr.id = q.currency_id','left');
		$this->db->join('city c','c.city_id = p.city_id','left');
		$this->db->join('state st','st.state_id = p.state_id','left');
		$this->db->join('country co','co.country_id = p.country_id','left');
		$this->db->join('staff stf','stf.staff_id = q.created_by','left');
		$this->db->where('p.active !=',0);
		$this->db->where('q.id',$where_id);
		$query = $this->db->get();
		return $query->row(0);
	}

	/**
 * @param $id
 * @return mixed
 */
	function get_challan($id,$challan_sales_order_id){
		$select = "c.*,c.id as challan_id,party.*,";
		if(!empty($challan_sales_order_id) && $challan_sales_order_id != 0){
			$select .= "so.id as sales_order_id,so.currency_id,so.sales_id,so.sales_order_no AS sales_order_no,so.sales_order_date,so.cust_po_no,so.po_date,";
		}else{
			$select .= "pi.id as sales_order_id,pi.currency_id,pi.sales_id,pi.proforma_invoice_no AS sales_order_no,pi.sales_order_date,pi.cust_po_no,pi.po_date,";
		}
		$select .= "quo.quotation_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('challans c');
		if(!empty($challan_sales_order_id ) && $challan_sales_order_id != 0){
			$this->db->join('sales_order so', 'so.id = c.sales_order_id', 'left');
			$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
			$this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
			$this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
			$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
		}else{
			$this->db->join('proforma_invoices pi', 'pi.id = c.proforma_invoice_id', 'left');
			$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
			$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
			$this->db->join('contact_person cp', 'cp.contact_person_id = pi.kind_attn_id', 'left');
			$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
		}

		$this->db->where('c.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		return $query->row(0);
	}

	function get_only_challan_data($id)
	{
		$select = "c.*,c.id as challan_id, party.*,";
		$select .= "cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('challans c');
		$this->db->join('party party', 'party.party_id = c.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = c.kind_attn_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = c.created_by', 'left');
		$this->db->where('c.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		return $query->row(0);
	}

	function get_challan_items($challan_id){
		$this->db->select('*');
		$this->db->from('challan_items');
		$this->db->where('challan_id', $challan_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}
	function get_challan_item_with_order_item_detail($challan_id, $sales_order_id){
		$this->db->select('ci.item_id, ci.item_name, ci.item_code, ci.item_serial_no, ci.quantity, ci.item_extra_accessories_id, ci.contact_person, ci.contact_no, ci.driver_name, ci.driver_contact_no, ci.truck_container_no, ci.lr_bl_no, ci.lr_date, ci.packing_mark, ci.no_of_packing, soi.quantity as so_qty, soi.igst, soi.cgst, soi.sgst, soi.currency_id, soi.rate, soi.disc_value');
		$this->db->from('challan_items ci');
		$this->db->join('sales_order_items soi', 'soi.item_id = ci.item_id', 'left');
		$this->db->where('ci.challan_id', $challan_id);
		$this->db->where('soi.sales_order_id', $sales_order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}
	function get_challan_item_with_proforma_item_detail($challan_id, $proforma_invoice_id){
		$this->db->select('ci.item_id, ci.item_name, ci.item_code, ci.item_serial_no, ci.quantity, ci.item_extra_accessories_id, ci.contact_person, ci.contact_no, ci.driver_name, ci.driver_contact_no, ci.truck_container_no, ci.lr_bl_no, ci.lr_date, ci.packing_mark, ci.no_of_packing, pii.quantity as so_qty, pii.igst, pii.cgst, pii.sgst, pii.currency_id, pii.rate, pii.disc_value');
		$this->db->from('challan_items ci');
		$this->db->join('proforma_invoice_items pii', 'pii.item_id = ci.item_id', 'left');
		$this->db->where('ci.challan_id', $challan_id);
		$this->db->where('pii.proforma_invoice_id', $proforma_invoice_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	function challan_items_motor_details($challan_id){
		$where = array('challan_id' => $challan_id);
		$this->db->select('ci.item_id, ci.item_name, ci.item_code, ci.quantity, ci.item_extra_accessories_id, ci.contact_person, ci.contact_no, ci.driver_name, ci.driver_contact_no, ci.truck_container_no, ci.lr_bl_no, ci.lr_date, soi.quantity as so_qty, soi.igst, soi.cgst, soi.sgst, soi.currency_id, soi.rate, soi.disc_value');
		$this->db->from('challan_items ci');
		$this->db->join('sales_order_items soi', 'soi.item_id = ci.item_id', 'left');
		$this->db->where('ci.challan_id', $challan_id);
		$this->db->where('soi.sales_order_id', $sales_order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	function get_sales_order($id)
	{
		$select = "quo.quotation_no,so.id as sales_order_id,so.*,party.*,";
		$select .= "cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('sales_order so');
		$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
		$this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
		$this->db->where('so.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		return $query->row(0);
	}

	function get_proforma_invoice($id){
		$select = "quo.quotation_no,pi.id as proforma_invoice_id,pi.*,party.*,";
		$select .= "cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('proforma_invoices pi');
		$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
		$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = pi.kind_attn_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
		$this->db->where('pi.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		return $query->row(0);
	}

	function get_sales_order_challan($id)
	{
		$select = "lr_no, lr_date, truck_no ";
		$this->db->select($select);
		$this->db->from('challans');
		$this->db->where('sales_order_id', $id);
		$this->db->order_by('id','DESC');
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->row(0);
	}

	/**
 * @param $id
 * @return mixed
 */
	function get_quotation($id)
	{
		$select = "quo.quotation_no,quo.id as quotation_id,quo.*,party.*,";
		$select .= "cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('quotations quo');
		$this->db->join('party party', 'party.party_id = quo.party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = quo.kind_attn_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = quo.created_by', 'left');
		$this->db->where('quo.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		return $query->row(0);
	}

	function get_column_value_by_party($id)
	{
		$this->db->select("*");
		$this->db->from('party');
		$this->db->where('party_id',$id);
		$this->db->where('active !=',0);
		$query = $this->db->get();
		return $query->row(0);
	}

	function get_items_detail_by_quotation($where_id)
	{
		$this->db->select("i.id,i.item_id,i.item_name,i.quotation_id,i.item_code,i.item_description,i.uom_id,i.quantity,i.rate,i.amount,i.net_amount,i.disc_value,is.item_status,i.currency_id,c.currency,i.item_documents");
		$this->db->from('quotation_items i');
		$this->db->join('item_status is', 'i.item_status_id = is.id', 'left');
		$this->db->join('currency c', 'c.id = i.currency_id', 'left');
		$this->db->where('i.quotation_id',$where_id);
		$query = $this->db->get();
		return $query->result();
	}
	function get_followup_history_by_quotation($where_id)
	{
		$this->db->select("fh.*,s.name as followup_by_name");
		$this->db->from('followup_history fh');
		$this->db->join('staff s','s.staff_id = fh.followup_by');
		$this->db->where('quotation_id',$where_id);
		$query = $this->db->get();
		return $query->result();
	}

	//ADDED ON 24-JUN-2017
	function get_followup_history_by_order($where_id)
	{
		$this->db->select("fh.*,s.name as followup_by_name");
		$this->db->from('followup_order_history fh');
		$this->db->join('staff s','s.staff_id = fh.followup_by');
		$this->db->where('order_id',$where_id);
		$query = $this->db->get();
		return $query->result();
	}

	/**
 * @param $service_id
 * @return mixed
 */
	function get_service_followup_history_by_id($service_id)
	{
		$this->db->select("fh.*,s.name as followup_by_name");
		$this->db->from('service_followup_history fh');
		$this->db->join('staff s','s.staff_id = fh.followup_by');
		$this->db->where('service_id',$service_id);
		$query = $this->db->get();
		return $query->result();
	}
	function get_followup_history_by_inquiry($where_id)
	{
		$this->db->select("fh.*,s.name as followup_by_name");
		$this->db->from('inquiry_followup_history fh');
		$this->db->join('staff s','s.staff_id = fh.followup_by');
		$this->db->where('inquiry_id',$where_id);
		$query = $this->db->get();
		return $query->result();
	}
	function get_selected_items_detail_by_quotation($where_id)
	{
		$this->db->select("*");
		$this->db->from('quotation_items');
		$this->db->where('id',$where_id);
		$query = $this->db->get();
		return $query->row();
	}
	function limit_words($string, $word_limit=30){
		$words = explode(" ",$string);
		return implode(" ", array_splice($words, 0, $word_limit));
	}
	function limit_character($string, $character_limit=30){
		if (strlen($string) > $character_limit) {
			return substr($string, 0, $character_limit).'...';
		}else{
			return $string;
		}
	}
	//select data 
	function get_select_data($tbl_name)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$query = $this->db->get();
		return $query->result();
	}

	/**
 * @param $tbl_name
 * @param $where
 * @param $where_id
 * @return mixed
 */
	function get_data_row_by_id($tbl_name,$where,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where,$where_id);
		$query = $this->db->get();
		return $query->row();
	}
	function get_result_where($tbl_name,$where,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where,$where_id);
		$query = $this->db->get();
		return $query->result();
	}
	function load_item_new()
	{
		$this->db->select('t.*,is.item_status');
		$this->db->from('items t');
		$this->db->join('item_status is','is.id = t.item_status','left');
		$query = $this->db->get();
		return $query->result();
	}

	/**
 * @param $party_id
 * @return array
 */
	function get_contact_person_by_party($party_id)
	{
		$this->db->select('cp.*,dept.department,desi.designation');
		$this->db->from('contact_person cp');
		$this->db->join('department dept', 'dept.department_id = cp.department_id', 'left');
		$this->db->join('designation desi', 'desi.designation_id = cp.designation_id', 'left');
		$this->db->where('cp.party_id',$party_id);
		$this->db->where('cp.active !=',0);
		$this->db->order_by('cp.priority,cp.contact_person_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return array();
		}
	}

	function load_party_with_cnt_person_3($where_id){
		$this->db->select('p.*,pt1.type as party_type_1_name,pt2.sales_type as party_type_2_name,pt1.id as party_type_1_id,pt2.id as party_type_2_id,br.branch,ct.city,st.state,cnt.country,p.utr_no AS party_cin_no');
		$this->db->from('party p');
		$this->db->join('party_type_1 pt1','pt1.id = p.party_type_1','left');
		$this->db->join('sales_type pt2','pt2.id = p.party_type_2','left');
		$this->db->join('branch br','br.branch_id = p.branch_id','left');
		$this->db->join('city ct','ct.city_id = p.city_id','left');
		$this->db->join('state st','st.state_id = p.state_id','left');
		$this->db->join('country cnt','cnt.country_id = p.country_id','left');
		$this->db->where('p.party_id',$where_id);
		$this->db->where('p.active !=',0);

		$accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
		$accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");

		if ($accessExport == 1 && $accessDomestic == 1) {
			$query = $this->db->get();
			$return = $query->row();
		} else if ($accessExport == 1  && $accessDomestic != 1) {
			$this->db->where("party_type_1",5);
			$query = $this->db->get();
			$return = $query->row();
		} else if ($accessDomestic == 1 && $accessExport != 1) {
			$this->db->where("party_type_1",6);
			$query = $this->db->get();
			$return = $query->row();
		} else {
			$return = array();
		}
		return $return;
	}

	function load_party_with_cnt_person_2(){
		$this->db->select('p.*,pt1.type as party_type_1_name,pt2.type as party_type_2_name,br.branch,ct.city,st.state,cnt.country');
		$this->db->from('party p');
		$this->db->join('party_type_1 pt1','pt1.id = p.party_type_1','left');
		$this->db->join('party_type_2 pt2','pt2.id = p.party_type_2','left');
		$this->db->join('branch br','br.branch_id = p.branch_id','left');
		$this->db->join('city ct','ct.city_id = p.city_id','left');
		$this->db->join('state st','st.state_id = p.state_id','left');
		$this->db->join('country cnt','cnt.country_id = p.country_id','left');
		$this->db->where('p.active !=',0);

		$accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
		$accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
		if ($accessExport == 1 && $accessDomestic == 1) {
			$query = $this->db->get();
			$return = $query->result();
		} else if ($accessExport == 1) {
			$this->db->where("party_type_1",5);
			$query = $this->db->get();
			$return = $query->result();
		} else if ($accessDomestic == 1) {
			$this->db->where("party_type_1",6);
			$query = $this->db->get();
			$return = $query->result();
		} else {
			$return = array();
		}
		return $return;
	}
	function load_item_details_where($where_id){

		$this->db->select('t.*,is.item_status,is.id as item_status_id');
		$this->db->from('items t');
		$this->db->join('item_status is','is.id = t.item_status','left');
		$this->db->where('t.id',$where_id);
		$query = $this->db->get();
		return $query->row();
	}
	function load_purchase_item_details_where($where_id){

		$this->db->select('t.*,is.item_status,is.id as item_status_id');
		$this->db->from('purchase_items t');
		$this->db->join('item_status is','is.id = t.item_status','left');
		$this->db->where('t.id',$where_id);
		$query = $this->db->get();
		return $query->row();
	}
	function load_item_edit_details_where($where_id){

		$this->db->select('*');
		$this->db->from('inquiry_items');
		$this->db->where('id',$where_id);
		$query = $this->db->get();
		return $query->row();
	}
	function get_next_autoincrement($tbl_name)
	{
		$query = $this->db->query('SHOW TABLE STATUS WHERE `Name` = "'.$tbl_name.'"');
		$data = $query->result_array();
		return $data[0]['Auto_increment'];
	}

	function get_agent_list(){
		$this->db->select('agent.*,party_type_1.type');
		$this->db->from('agent');
		$this->db->order_by('agent.agent_name','asc');
		$this->db->join('party_type_1', 'agent.party_type_1 = party_type_1.id', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	/**
 * Get supplier data from supplier_id
 */
	function load_supplier_with_cnt_person_3($where_id){
		$this->db->select('p.*,ct.city,st.state,cnt.country');
		$this->db->from('supplier p');
		$this->db->join('city ct','ct.city_id = p.city_id','left');
		$this->db->join('state st','st.state_id = p.state_id','left');
		$this->db->join('country cnt','cnt.country_id = p.country_id','left');
		$this->db->where('p.supplier_id',$where_id);

		$accessExport = $this->app_model->have_access_role(SUPPLIER_TYPE_MODULE_ID, "export");
		$accessDomestic = $this->app_model->have_access_role(SUPPLIER_TYPE_MODULE_ID, "domestic");
		if ($accessExport == 1 && $accessDomestic == 1) {
			$query = $this->db->get();
			$return = $query->row();
		} else if ($accessExport == 1) {
			//$this->db->where("supplier_type_1",5);
			$query = $this->db->get();
			$return = $query->row();
		} else if ($accessDomestic == 1) {
			//$this->db->where("supplier_type_1",6);
			$query = $this->db->get();
			$return = $query->row();
		} else {
			$return = array();
		}
		return $return;
	}

	/**
 * Get supplier data from supplier_id
 */
	function load_supplier_detail($where_id){
		$this->db->select('s.*,ct.city,st.state,cnt.country');
		$this->db->from('supplier s');
		$this->db->join('city ct','ct.city_id = s.city_id','left');
		$this->db->join('state st','st.state_id = s.state_id','left');
		$this->db->join('country cnt','cnt.country_id = s.country_id','left');
		$this->db->where('s.supplier_id',$where_id);
		$query = $this->db->get();
		$return = $query->row();
		return $return;
	}

	function getUserChatRoleIDS($user_id)
	{
		$array = array();
		$sql = "SELECT * FROM chat_roles WHERE staff_id='$user_id'";
		$rows = $this->db->query($sql)->result();        
		$i = 0;        
		foreach($rows as $row)
		{
			$array[$i] = $row->allowed_staff_id;
			$i++;        
		}    
		return $array;
	}

	function get_all_reminder_data_by_staff($date = null,$staff_id = null)
	{
		if($date == null){
			$date = date('Y-m-d');
		}
		$reminders = array();


		/*--------- Fetch From Reminders --------------*/
		$this->db->select('r.*,created.name as created_by_name,assign.name as assigned_to_name');
		$this->db->from('reminder r');
		$this->db->join('staff created','created.staff_id = r.user_id','left');
		$this->db->join('staff assign','assign.staff_id = r.assigned_to','left');
		//$this->db->where('DATE(r.reminder_date) <',$date);
		if(!empty($staff_id)) {
			$this->db->group_start();
			$this->db->where('assigned_to',$staff_id);
			$this->db->or_where('user_id',$staff_id);
			$this->db->group_end();
		}
		$reminder_result = $this->db->get();
		foreach($reminder_result->result() as $reminder_row) {
			$reminders[] = array(
				'reminder_type' => 'reminder',
				'reminder_id' => $reminder_row->reminder_id,
				'reminder_date' => $reminder_row->reminder_date,
				'reminder_text' => $reminder_row->remind,
				'created_by' => $reminder_row->user_id,
				'created_by_name' => $reminder_row->created_by_name,
				'assigned_to' => $reminder_row->assigned_to,
				'assigned_to_name' => $reminder_row->assigned_to_name,
			);
		}


		/*--------- Fetch From Reminders --------------*/

	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
    function today_pending_invoice($user_id, $today = null, $up = null) {
        $this->db->select("ch.*,ch.id as challan_id,ch.challan_date as review_date,s.name as created_by_name,p.party_name as sales_order_party_name");
        $this->db->from('challans ch');
        $this->db->join('staff s', 's.staff_id = ch.created_by', 'left');
        $this->db->join('party p', 'p.party_id = ch.sales_to_party_id', 'left');

        if ($up == false) {
            if ($today == false) {
                $this->db->where('DATE(ch.challan_date) <', date('Y-m-d'));
            } else {
                $this->db->where('DATE(ch.challan_date) =', date('Y-m-d'));
            }
        } else {
            $this->db->where('DATE(ch.challan_date) >', date('Y-m-d'));
        }

        $this->db->where('p.active !=', 0);
        $this->db->where('ch.is_invoice_created ', '0');
        if ($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER, "allow") == 1) {
            if ($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1) {
                
            } elseif ($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
            } else if ($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
            }
        } else {
            $this->db->where('p.current_party_staff_id', $user_id);
            if ($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1) {
                
            } else if ($this->cu_accessExport == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
            } else if ($this->cu_accessDomestic == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
            } else {
                
            }
        }
        $this->db->order_by('ch.challan_date', 'DESC');
        $query = $this->db->get();
//        echo "<pre>"; print_r($query->result_array()); exit;
        return $query->result_array();
    }
    
    function pending_testing($user_id, $today = null, $up = null) {
        $this->db->select("ch.*,ch.id as t_challan_id,ch.challan_date as review_date,s.name as created_by_name,p.party_name as sales_order_party_name");
        $this->db->from('challans ch');
        $this->db->join('staff s', 's.staff_id = ch.created_by', 'left');
        $this->db->join('party p', 'p.party_id = ch.sales_to_party_id', 'left');

        if ($up == false) {
            if ($today == false) {
                $this->db->where('DATE(ch.challan_date) <', date('Y-m-d'));
            } else {
                $this->db->where('DATE(ch.challan_date) =', date('Y-m-d'));
            }
        } else {
            $this->db->where('DATE(ch.challan_date) >', date('Y-m-d'));
        }

        $this->db->where('p.active !=', 0);
        $this->db->where('ch.is_testing_report_created', '0');
        if ($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER, "allow") == 1) {
            if ($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1) {
                
            } elseif ($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
            } else if ($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
            }
        } else {
            $this->db->where('p.current_party_staff_id', $user_id);
            if ($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1) {
                
            } else if ($this->cu_accessExport == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
            } else if ($this->cu_accessDomestic == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
            } else {
                
            }
        }
        $this->db->order_by('ch.challan_date', 'DESC');
        $query = $this->db->get();
//        echo "<pre>"; print_r($query->result_array()); exit;
        return $query->result_array();
    }
    
    function pending_install($user_id, $today = null, $up = null) {
        $this->db->select("tr.testing_report_id,tr.created_by,tr.testing_report_date as review_date,s.name as created_by_name,p.party_name as sales_order_party_name");
        $this->db->from('testing_report tr');
        $this->db->join('staff s', 's.staff_id = tr.created_by', 'left');
        $this->db->join('party p', 'p.party_id = tr.party_id', 'left');

        if ($up == false) {
            if ($today == false) {
                $this->db->where('DATE(tr.testing_report_date) <', date('Y-m-d'));
            } else {
                $this->db->where('DATE(tr.testing_report_date) =', date('Y-m-d'));
            }
        } else {
            $this->db->where('DATE(tr.testing_report_date) >', date('Y-m-d'));
        }

        $this->db->where('p.active !=', 0);
        $this->db->where('tr.is_installation_created', '0');
        if ($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER, "allow") == 1) {
            if ($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1) {
                
            } elseif ($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
            } else if ($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
            }
        } else {
            $this->db->where('p.current_party_staff_id', $user_id);
            if ($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1) {
                
            } else if ($this->cu_accessExport == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
            } else if ($this->cu_accessDomestic == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
            } else {
                
            }
        }
        $this->db->order_by('tr.testing_report_date', 'DESC');
        $query = $this->db->get();
//        echo "<pre>"; print_r($query->result_array()); exit;
        return $query->result_array();
    }

    function today_pending_reminder($user_id,$today,$date = null) {
		$this->db->select("r.*,s.name");
		$this->db->from('reminder r');
		$this->db->join('staff s','s.staff_id = r.user_id','left');
		$this->db->where('r.status',1);
		//$this->db->where('DATE(r.reminder_date) <',date('Y-m-d'));1
		//$this->db->where('DATE(r.reminder_date) >',date('Y-m-d'));3

		if($today == false){
			$this->db->where('DATE(r.reminder_date) <',date('Y-m-d'));
		}else{
			$this->db->where('DATE(r.reminder_date) =',date('Y-m-d'));
		}
		if($this->applib->have_access_current_user_rights(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"ismanagement") == 1){
		}else{
			$this->db->group_start();
			$this->db->where('r.assigned_to',$user_id);
			$this->db->or_where('r.user_id',$user_id);
			$this->db->group_end();
		}
		$this->db->order_by('r.reminder_date','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function today_pending_inquiry($user_id,$today = null) {
		$this->db->select("i.*,s.name as assigned_to_name,s2.name as created_by_name,p.party_name as inquiry_party_name");
		$this->db->from('inquiry i');
		$this->db->join('staff s', 's.staff_id = i.assigned_to_id', 'left');
		$this->db->join('staff s2', 's2.staff_id = i.created_by', 'left');
		$this->db->join('party p', 'p.party_id = i.party_id', 'left');

		if($this->isManagement == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}

		$this->db->where_not_in('i.inquiry_id', 'SELECT enquiry_id FROM quotations');
		$this->db->where('p.active !=', 0);
		$this->db->where('i.inquiry_status_id', PENDING_ENQUIRY_ID);
		$inq = 'enquiry';
		$inq_array = array('i.lead_or_inquiry' => $inq);

		$this->db->where($inq_array);
		if ($today == false) {
			$this->db->where('DATE(i.due_date) <', date('Y-m-d'));
		} else {
			$this->db->where('DATE(i.due_date) =', date('Y-m-d'));
			//$this->db->where('DATE(i.due_date)', CURDATE(), true);
		}
		$this->db->order_by('i.created_at', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function today_pending_lead($user_id,$today = null){
		$this->db->select("i.*,s.name as assigned_to_name,s2.name as created_by_name,p.party_name as inquiry_party_name");
		$this->db->from('inquiry i');
		$this->db->join('staff s', 's.staff_id = i.assigned_to_id', 'left');
		$this->db->join('staff s2', 's2.staff_id = i.created_by', 'left');
		$this->db->join('party p', 'p.party_id = i.party_id', 'left');
		
		if($this->isManagement == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		
		$this->db->where_not_in('i.inquiry_id', 'SELECT enquiry_id FROM quotations');
		$this->db->where('p.active !=', 0);
		$this->db->where('i.inquiry_status_id', PENDING_ENQUIRY_ID);
		$inq = 'lead';
		$inq_array = array('i.lead_or_inquiry' => $inq);

		$this->db->where($inq_array);
		if ($today == false) {
			$this->db->where('DATE(i.due_date) <', date('Y-m-d'));
		} else {
			$this->db->where('DATE(i.due_date) =', date('Y-m-d'));
		}
		$this->db->order_by('i.created_at', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	function today_sales_order($user_id,$today = null,$up = null){
		$this->db->select("so.*,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('sales_order so');
		$this->db->join('staff s', 's.staff_id = so.created_by', 'left');
		$this->db->join('party p', 'p.party_id = so.sales_to_party_id', 'left');

		if($up == false){
			if ($today == false) {
				$this->db->where('DATE(so.committed_date) <', date('Y-m-d'));
			} else {
				$this->db->where('DATE(so.committed_date) =',date('Y-m-d'));
			}
		} else {
			$this->db->where('DATE(so.committed_date) >',date('Y-m-d'));
		}

		$this->db->where('p.active !=', 0);
		$this->db->where('so.isApproved !=', 0);
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		//$this->db->where_not_in('so.id', 'SELECT sales_order_id FROM challans');
		$this->db->where('so.is_dispatched', '0');
		$this->db->order_by('so.committed_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	function today_proforma_invoice($user_id,$today = null,$up = null){
		$this->db->select("pi.*,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('proforma_invoices pi');
		$this->db->join('staff s', 's.staff_id = pi.created_by', 'left');
		$this->db->join('party p', 'p.party_id = pi.sales_to_party_id', 'left');

		if($up == false){
			if ($today == false) {
				$this->db->where('DATE(pi.committed_date) <', date('Y-m-d'));
			} else {
				$this->db->where('DATE(pi.committed_date) =',date('Y-m-d'));
			}
		} else {
			$this->db->where('DATE(pi.committed_date) >',date('Y-m-d'));
		}

		$this->db->where('p.active !=', 0);
		$this->db->where('pi.isApproved !=', 0);
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('pi.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		//$this->db->where_not_in('pi.id', 'SELECT sales_order_id FROM challans');
		$this->db->where('pi.is_dispatched !=', 1);
		$this->db->order_by('pi.committed_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	function sales_order_to_approve($user_id,$today = null,$up = null){
		$this->db->select("so.*,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('sales_order so');
		$this->db->join('staff s', 's.staff_id = so.created_by', 'left');
		$this->db->join('party p', 'p.party_id = so.sales_to_party_id', 'left');

		if($up == false){
			if ($today == false) {
				$this->db->where('DATE(so.committed_date) <', date('Y-m-d'));
			} else {
				$this->db->where('DATE(so.committed_date) =',date('Y-m-d'));
			}
		} else {
			$this->db->where('DATE(so.committed_date) >',date('Y-m-d'));
		}

		$this->db->where('p.active !=', 0);
		$this->db->where('so.isApproved', 0);
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		//$this->db->where('so.id NOT IN (SELECT sales_order_id FROM challans)');
		$this->db->order_by('so.committed_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function today_unresolve_complain($user_id,$today = null,$up = null) {
		$this->db->select("c.*,s.name as created_by_name,p.party_name");
		$this->db->from('complain c');
		$this->db->join('staff s', 's.staff_id = c.created_by', 'left');
		$this->db->join('party p', 'p.party_id = c.party_id', 'left');

		if($up == false){
			if ($today == false) {
				$this->db->where('DATE(c.complain_date) <', date('Y-m-d'));
			} else {
				$this->db->where('DATE(c.complain_date) =',date('Y-m-d'));
			}
		} else {
			$this->db->where('DATE(c.complain_date) >',date('Y-m-d'));
		}

		$this->db->where('p.active !=', 0);
		$this->db->where('c.is_resolve_complain_created', 0);
		if($this->applib->have_access_current_user_rights(COMPLAIN_MENU_ID,"allow") == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('c.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('c.complain_date', 'DESC');
		$query = $this->db->get();
//        echo '<pre>'.$this->db->last_query(); exit;
		return $query->result_array();
	}
    
    function today_resolve_complain($user_id,$today = null,$up = null) {
		$this->db->select("c.*,s.name as created_by_name,p.party_name");
		$this->db->from('resolve_complain c');
		$this->db->join('staff s', 's.staff_id = c.created_by', 'left');
		$this->db->join('party p', 'p.party_id = c.party_id', 'left');

		if($up == false){
			if ($today == false) {
				$this->db->where('DATE(c.resolve_complain_date) <', date('Y-m-d'));
			} else {
				$this->db->where('DATE(c.resolve_complain_date) =',date('Y-m-d'));
			}
		} else {
			$this->db->where('DATE(c.resolve_complain_date) >',date('Y-m-d'));
		}

		$this->db->where('p.active !=', 0);
		//$this->db->where('c.is_resolve_complain_created', 0);
		if($this->applib->have_access_current_user_rights(COMPLAIN_MENU_ID,"allow") == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('c.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('c.resolve_complain_date', 'DESC');
		$query = $this->db->get();
//        echo '<pre>'.$this->db->last_query(); exit;
		return $query->result_array();
	}

	function get_all_unresolve_complain($user_id,$date = null,$onLoad = null, $created_by) {
		$this->db->select("c.*,s.name as created_by_name,p.party_name");
		$this->db->from('complain c');
		$this->db->join('staff s', 's.staff_id = c.created_by', 'left');
		$this->db->join('party p', 'p.party_id = c.party_id', 'left');
		if ($date == date('Y-m-d') && !empty($onLoad)) {
			$this->db->where('DATE(c.complain_date) <=',date('Y-m-d'));
		} else {
			$this->db->where('DATE(c.complain_date)', $date);
		}
		$this->db->where('p.active !=', 0);
        $this->db->where('c.is_resolve_complain_created', 0);
		if($this->applib->have_access_current_user_rights(COMPLAIN_MENU_ID,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('c.created_by',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('c.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('c.complain_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
    
    function get_all_resolve_complain($user_id,$date = null,$onLoad = null, $created_by) {
		$this->db->select("c.*,s.name as created_by_name,p.party_name");
		$this->db->from('resolve_complain c');
		$this->db->join('staff s', 's.staff_id = c.created_by', 'left');
		$this->db->join('party p', 'p.party_id = c.party_id', 'left');
		if ($date == date('Y-m-d') && !empty($onLoad)) {
			$this->db->where('DATE(c.resolve_complain_date) <=',date('Y-m-d'));
		} else {
			$this->db->where('DATE(c.resolve_complain_date)', $date);
		}
		$this->db->where('p.active !=', 0);
//        $this->db->where('c.is_resolve_complain_created', 0);
		if($this->applib->have_access_current_user_rights(COMPLAIN_MENU_ID,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('c.created_by',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('c.created_by', $user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->order_by('c.resolve_complain_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function today_pending_quotation($user_id,$today = null){
		$this->db->select("q.*,p.party_name,s.name as staff_name");
		$this->db->from('quotations q');
		$this->db->join('party p','p.party_id = q.party_id','left');
		$this->db->join('staff s','s.staff_id = q.created_by','left');

		if($this->applib->have_access_current_user_rights(QUOTATION_MANAGEMENT_USER,"allow") == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('p.current_party_staff_id',$user_id);
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		$this->db->where_not_in('q.id', 'SELECT quotation_id FROM sales_order');
		$this->db->where('p.active !=',0);

		if ($today == false) {
			$this->db->where('DATE(q.created_at) <', date('Y-m-d'));
		} else {
			$this->db->where('DATE(q.created_at) =',date('Y-m-d'));
		}
		//$this->db->where('p.active !=', 0);
		$this->db->where('q.quotation_status_id', QUOTATION_DEFAULT_STATUS_ID);
		$this->db->where('q.latest_revision', 1);
		$this->db->order_by('q.created_at','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	function today_pending_quotation_followup($user_id,$today = null,$date = null,$onLoad = null, $created_by = null){
                $this->db->select("f.*,q.quotation_no as f_quotation_no,s.name as staff_name,p.party_name as party_name");
		$this->db->from('followup_history f');
//		$this->db->join('(SELECT quotation_id, MAX(reminder_date) as TopDate FROM followup_history GROUP BY quotation_id) AS EachItem', 'EachItem.TopDate = f.reminder_date AND EachItem.quotation_id = f.quotation_id', 'INNER');
                if ($today == false) {
                    $this->db->join('(SELECT quotation_id, MAX(reminder_date) as TopDate FROM followup_history GROUP BY quotation_id HAVING max(DATE(reminder_date)) < "'.date('Y-m-d').'") AS EachItem', 'EachItem.TopDate = f.reminder_date AND EachItem.quotation_id = f.quotation_id', 'INNER');
		} else if (!empty($date)) {
                    if($date == date('Y-m-d') && !empty($onLoad)){
                            $this->db->join('(SELECT quotation_id, MAX(reminder_date) as TopDate FROM followup_history GROUP BY quotation_id HAVING max(DATE(reminder_date)) <= "'.date('Y-m-d').'") AS EachItem', 'EachItem.TopDate = f.reminder_date AND EachItem.quotation_id = f.quotation_id', 'INNER');
                    } else {
                            $this->db->join('(SELECT quotation_id, MAX(reminder_date) as TopDate FROM followup_history GROUP BY quotation_id HAVING max(DATE(reminder_date)) <= "'.$date.'") AS EachItem', 'EachItem.TopDate = f.reminder_date AND EachItem.quotation_id = f.quotation_id', 'INNER');
                    }
		} else {
			$this->db->join('(SELECT quotation_id, MAX(reminder_date) as TopDate FROM followup_history GROUP BY quotation_id HAVING max(DATE(reminder_date)) = "'.date('Y-m-d').'") AS EachItem', 'EachItem.TopDate = f.reminder_date AND EachItem.quotation_id = f.quotation_id', 'INNER');
		}
		$this->db->join('quotations q','q.id = f.quotation_id','left');
		$this->db->join('staff s','s.staff_id = f.followup_by','left');
		$this->db->join('party p','p.party_id = q.party_id','left');
        
		if($this->applib->have_access_current_user_rights(QUOTATION_MANAGEMENT_USER,"allow") == 1){
            if(!empty($created_by)){
                $this->db->where('p.current_party_staff_id',$created_by);
            }
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->group_start();
			$this->db->where("FIND_IN_SET( '".$user_id."', f.assigned_to) ");
			$this->db->or_where('p.current_party_staff_id',$user_id);
			$this->db->group_end();
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		
		$this->db->where_not_in('f.quotation_id', 'SELECT quotation_id FROM sales_order');
		if ($today == false) {
			$this->db->where('DATE(f.reminder_date) <', date('Y-m-d'));
		} elseif(!empty($date)) {
			if($date == date('Y-m-d') && !empty($onLoad)){
				$this->db->where('DATE(f.reminder_date) <=',date('Y-m-d'));
			}else{
				$this->db->where('DATE(f.reminder_date)',$date);
			}
		}else{
			$this->db->where('DATE(f.reminder_date)',date('Y-m-d'));
		}
		$this->db->where_in('f.quotation_id', $quotation_ids);
		$this->db->where_in('f.reminder_date', $reminder_dates);
		$this->db->where('p.active !=', 0);
		$this->db->where('q.quotation_status_id', QUOTATION_DEFAULT_STATUS_ID);
		$this->db->group_by('f.quotation_id');
		$this->db->order_by('f.reminder_date','DESC');
		$query = $this->db->get();
//        echo $this->db->last_query(); exit;
		return $query->result_array();
	}

	function pending_quotation_folloup($user_id){
		$this->db->select('q.*');
		$this->db->from('quotations q');
		$this->db->join('party p','p.party_id = q.party_id');
		$this->db->where('q.due_date <= CURDATE()');
		$this->db->where('q.quotation_status_id = '.QUOTATION_DEFAULT_STATUS_ID);
		$this->cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$this->cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if($this->applib->have_access_current_user_rights(QUOTATION_MANAGEMENT_USER,"allow") == 1){
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$this->db->where('( q.created_by = '.$user_id.' OR q.created_by IS NULL )');
			if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
			} else if($this->cu_accessExport == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
			} else if($this->cu_accessDomestic == 1){
				$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
		return $this->db->get()->num_rows();
	}

	function pending_sales_order_approved($user_id,$today = null){
		$this->db->select("so.*,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('sales_order so');
		$this->db->join('staff s', 's.staff_id = so.created_by', 'left');
		$this->db->join('party p', 'p.party_id = so.sales_to_party_id', 'left');

		if ($today == false) {
			$this->db->where('DATE(so.committed_date) <', date('Y-m-d'));
		} else {
			$this->db->where('DATE(so.committed_date) =',date('Y-m-d'));
		}

		$this->db->where('p.active !=', 0);
		$this->db->where('so.created_by', $user_id);
		//$this->db->where('so.id NOT IN (SELECT sales_order_id FROM challans)');
		$this->db->where('so.isApproved !=', 1);
		$this->db->order_by('so.committed_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
 * @param $user_id
 * @param null $date
 * @return mixed
 */
	function pending_inquiry_followup($user_id,$today = null) {
		$this->db->select("i.*,s.name as assigned_to_name,s2.name as created_by_name,p.party_name as inquiry_party_name,f.followup_date");
		$this->db->from('inquiry i');
		$this->db->join('staff s', 's.staff_id = i.assigned_to_id', 'left');
		$this->db->join('staff s2', 's2.staff_id = i.created_by', 'left');
		$this->db->join('party p', 'p.party_id = i.party_id', 'left');
		$this->db->join('inquiry_followup_history f', 'f.inquiry_id = i.inquiry_id', 'left');
		if ($today == false) {
			$this->db->where('DATE(f.followup_date) <', date('Y-m-d'));
		} else {
			$this->db->where('DATE(f.followup_date) =', date('Y-m-d'));
		}
		$this->db->where('i.assigned_to_id', $user_id);
		$this->db->where_not_in('i.inquiry_id', 'SELECT enquiry_id FROM quotations');
		$this->db->where('p.active !=', 0);
		$inq = 'enquiry';
		$inq_array = array('i.lead_or_inquiry' => $inq);
		//$this->db->where($inq_array);


		$this->db->order_by('i.created_at', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	function pending_sales_order_invoice($user_id,$today = null){
        
        $invoice_created_sales_order_ids = $this->crud->getFromSQL('SELECT `sales_order_id` FROM `challans` WHERE `id` IN (SELECT `challan_id` FROM `invoices`)');
        $invoice_created_sales_order_id_arr = array();
        foreach ($invoice_created_sales_order_ids as $invoice_created_sales_order_id){
            if(!empty($invoice_created_sales_order_id->sales_order_id)){
                if(in_array($invoice_created_sales_order_id->sales_order_id, $invoice_created_sales_order_id_arr)){ } else {
                    $invoice_created_sales_order_id_arr[] = $invoice_created_sales_order_id->sales_order_id;
                }
            }
        }
        
		$this->db->select("so.*,s.name as created_by_name,p.party_name as sales_order_party_name");
		$this->db->from('sales_order so');
		$this->db->join('staff s', 's.staff_id = so.created_by', 'left');
		$this->db->join('party p', 'p.party_id = so.sales_to_party_id', 'left');

		if ($today == false) {
			$this->db->where('DATE(so.committed_date) <', date('Y-m-d'));
		} else {
			$this->db->where('DATE(so.committed_date) =',date('Y-m-d'));
		}

		$this->db->where('p.active !=', 0);
		$this->db->where('so.created_by', $user_id);
        if(!empty($invoice_created_sales_order_id_arr)){
            $this->db->where_not_in('so.id', $invoice_created_sales_order_id_arr);
        }
		$this->db->order_by('so.committed_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	function service_data($service_id){
		$this->db->select("s.*, p.party_name, p.email_id, sf.name as service_provide_by, sf2.name as service_received_by");
		$this->db->from('service s');
		$this->db->join('party p', 'p.party_id = s.party_id', 'left');
		$this->db->join('staff sf', 'sf.staff_id = s.service_provide_by', 'left');
		$this->db->join('staff sf2', 'sf2.staff_id = s.service_received_by', 'left');
		$this->db->where('p.active !=', 0);
		$this->db->where('s.service_id', $service_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function service_item_data($service_id){
		$this->db->select("si.*, ci.item_code as item_code, ci.item_name as item_name, ci.item_code as item_code");
		$this->db->from('service_items si');
		$this->db->join('challan_items ci', 'ci.id = si.challan_item_id', 'left');
		$this->db->where('si.service_id', $service_id);
		$this->db->group_by('challan_item_id'); 
		$query = $this->db->get();
		return $query->result_array();
	}

	function get_followup_history_by_challan($where_id)
	{
		$this->db->select("fh.*,s.name as followup_by_name");
		$this->db->from('dispatch_followup_history fh');
		$this->db->join('staff s','s.staff_id = fh.followup_by');
		$this->db->where('challan_id',$where_id);
		$query = $this->db->get();
		return $query->result();
	}

	function get_max_number($tbl_name,$column_name){
		$this->db->select_max($column_name);
		$result = $this->db->get($tbl_name)->row();  
		return $result;
	}

	public function get_columns_val_by_where($table_name,$column_name,$where_array)
	{
		$this->db->select($column_name);
		$this->db->from($table_name);
		$this->db->where($where_array);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}

	public function get_column_value_implode($table_name,$column_name){
		$this->db->select($column_name);
		$this->db->from($table_name);
		$this->db->group_by($column_name);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			//return $query->result();
			foreach($query->result() as $row){
				$ids[] = $row->$column_name;
			}
			return implode(",",$ids);
		}else{
			return false;
		}
	}

	function get_challan_num_rows(){
		$this->db->select("c.*");
		$this->db->from('challans c');
		$this->db->join('party p', 'p.party_id = c.sales_to_party_id', 'left');
		if($this->cu_accessExport == 1 && $this->cu_accessDomestic == 1){
		}elseif($this->cu_accessExport == 1 && $this->cu_accessDomestic != 1){
			$this->db->where('p.party_type_1',PARTY_TYPE_EXPORT_ID);
		}else if($this->cu_accessDomestic == 1 && $this->cu_accessExport != 1){
			$this->db->where('p.party_type_1',PARTY_TYPE_DOMESTIC_ID);
		}
		$this->db->where('p.active !=', 0);
		$query = $this->db->get();
		//return $query->result_array();
		return $query->num_rows();
	}

	function get_staff_dropdown(){
		$this->db->select("*");
		$this->db->from('staff');
		$this->db->where('active',1);
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		return $query->result();
	}
    
    function get_notification_dropdown(){
        $remi_query = $this->db->query("SELECT r.*, LEFT(r.remind, 12) as remind, s.socket_id, s.name as assign_to_name, assigner.name as assign_by,last_updated_date AS f_date FROM reminder r LEFT JOIN staff s ON s.staff_id = r.assigned_to LEFT JOIN staff assigner ON assigner.staff_id = r.user_id WHERE r.reminder_date >= '".date('Y-m-d')." 00:00:00' AND r.reminder_date <= '".date('Y-m-d H:i:s')."' AND r.assigned_to = ".$this->session->userdata('is_logged_in')['staff_id']." ORDER BY `reminder_date` DESC");
        $reminder_data = $remi_query->result();
        $query = $this->db->query('SELECT `f`.*, IFNULL(p.party_name, "Jk&nbsp;Admin") AS `created_by`,f.updated_at AS f_date FROM `user_feedback` `f` LEFT JOIN `party` `p` ON `p`.`party_id` = `f`.`created_by` WHERE `f`.`feedback_id` IN (SELECT MAX(feedback_id) FROM reply WHERE assign_to_id = "0" AND `reply_from` = "2") OR `f`.`feedback_id` NOT IN (SELECT feedback_id FROM reply) ORDER BY `feedback_id` DESC');
        $feedback_data = $query->result();
        $query_quo_remi = $this->db->query("SELECT fh.id as fh_id,fh.quotation_id as re_quotation_id,fc.fc_name, q.quotation_no, fh.updated_at AS f_date FROM followup_history fh LEFT JOIN followup_category fc ON fc.fc_id = fh.reminder_category_id LEFT JOIN quotations q on q.id=fh.quotation_id  WHERE fh.reminder_date >= '".date('Y-m-d')." 00:00:00' AND fh.reminder_date <= '".date('Y-m-d H:i:s')."' AND FIND_IN_SET('".$this->session->userdata('is_logged_in')['staff_id']."', fh.assigned_to) ORDER BY fh.reminder_date DESC");
        $query_quo_remi_data = $query_quo_remi->result();
        $query_quo_remi1 = $this->db->query("SELECT fh.id as fh_id,fh.quotation_id as re_quotation_id,fc.fc_name, qo.quotation_no, fh.updated_at AS f_date FROM followup_history fh LEFT JOIN followup_category fc ON fc.fc_id = fh.reminder_category_id LEFT JOIN quotations qo ON qo.id = fh.quotation_id LEFT JOIN party p ON p.party_id = qo.party_id WHERE fh.reminder_date >= '".date('Y-m-d')." 00:00:00' AND fh.reminder_date <= '".date('Y-m-d H:i:s')."' AND p.current_party_staff_id = '".$this->session->userdata('is_logged_in')['staff_id']."' ORDER BY fh.reminder_date DESC");
        $query_quo_remi_data1 = $query_quo_remi1->result();
        $quo_arr = array_merge($query_quo_remi_data, $query_quo_remi_data1);
        if(!empty($quo_arr)){
            $quo_arr = array_map("unserialize", array_unique(array_map("serialize", $quo_arr)));
            $quo_arr = array_values($quo_arr);
        }
        if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){
            $query_sales_order = $this->db->query("SELECT so.id, p.party_type_1 AS sales_id, so.sales_order_no, so.updated_at AS f_date FROM sales_order so LEFT JOIN party p ON p.party_id = so.sales_to_party_id WHERE sales_order_date = '".date('Y-m-d')."' AND isApproved=0 ORDER BY id DESC");
            $sales_order_data = $query_sales_order->result();
        } else {
            $sales_order_data = array();
        }
        $query_sales_remi = $this->db->query("SELECT fh.id as fh_id,fh.order_id as re_sales_order_id,p.party_type_1 AS sales_id,so.sales_order_no,fc.fc_name,fh.updated_at AS f_date FROM followup_order_history fh LEFT JOIN followup_category fc ON fc.fc_id = fh.reminder_category_id LEFT JOIN sales_order so ON so.id = fh.order_id LEFT JOIN party p ON p.party_id=so.sales_to_party_id WHERE fh.reminder_date >= '".date('Y-m-d')." 00:00:00' AND fh.reminder_date <= '".date('Y-m-d H:i:s')."' AND FIND_IN_SET('".$this->session->userdata('is_logged_in')['staff_id']."', fh.assigned_to) ORDER BY fh.reminder_date DESC");
        $query_sales_remi_data = $query_sales_remi->result();
        $query_sales_remi1 = $this->db->query("SELECT fh.id as fh_id,fh.order_id as re_sales_order_id,p.party_type_1 AS sales_id,so.sales_order_no,fc.fc_name,fh.updated_at AS f_date FROM followup_order_history fh LEFT JOIN followup_category fc ON fc.fc_id = fh.reminder_category_id LEFT JOIN sales_order so ON so.id = fh.order_id LEFT JOIN party p ON p.party_id=so.sales_to_party_id WHERE fh.reminder_date >= '".date('Y-m-d')." 00:00:00' AND fh.reminder_date <= '".date('Y-m-d H:i:s')."' AND p.current_party_staff_id = '".$this->session->userdata('is_logged_in')['staff_id']."'  ORDER BY fh.reminder_date DESC");
        $query_sales_remi_data1 = $query_sales_remi1->result();
        $sales_arr = array_merge($query_sales_remi_data, $query_sales_remi_data1);
        if(!empty($sales_arr)){
            $sales_arr = array_map("unserialize", array_unique(array_map("serialize", $sales_arr)));
            $sales_arr = array_values($sales_arr);
        }
        $notify_arr = array_merge($reminder_data, $feedback_data, $quo_arr, $sales_order_data, $sales_arr);
//        echo '<pre>'; print_r($notify_arr); exit;
        return $notify_arr;
    }
    
    function get_feedback_dropdown_count(){
        $query = $this->db->query('SELECT * FROM `user_feedback` WHERE `feedback_id` IN (SELECT MAX(feedback_id) FROM reply WHERE assign_to_id = "0" AND `reply_from` = "2") OR feedback_id NOT IN (SELECT feedback_id FROM reply) ');
        return $query->num_rows();
    }

	function get_pending_dispach($today,$overdue){
		$this->db->select("SUM(quantity) AS total_quantity, SUM(dispatched_qty) AS dispatched_qty");
		$this->db->from('sales_order_items si');
		$this->db->join('sales_order s', 's.id = si.sales_order_id', 'left');
		$this->db->join('party p', 'p.party_id = s.sales_to_party_id', 'left');
		//$this->db->where_in('si.sales_order_id',$today);

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
			}
			$this->db->where('p.current_party_staff_id', $this->session->userdata('is_logged_in')['staff_id']);
		}
		$this->db->where('p.active !=', 0);
        $this->db->where('s.isApproved', 1);
        $this->db->where('s.is_dispatched', '0');
		$query = $this->db->get();
		return $query->result();
	}

	function get_pending_dispach_pi($today,$overdue){
		$this->db->select("SUM(quantity) AS total_quantity, SUM(dispatched_qty) AS dispatched_qty");
		$this->db->from('proforma_invoice_items pii');
		$this->db->join('proforma_invoices pi', 'pi.id = pii.proforma_invoice_id', 'left');
		$this->db->join('party p', 'p.party_id = pi.sales_to_party_id', 'left');
		//$this->db->where_in('si.sales_order_id',$today);

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
			}
			$this->db->where('pi.created_by', $this->session->userdata('is_logged_in')['staff_id']);
		}
		$this->db->where('p.active !=', 0);
        $this->db->where('pi.isApproved', 1);
        $this->db->where('pi.is_dispatched', '0');
		$query = $this->db->get();
		return $query->result();
	}
        
    function load_parts_details_where($where_id){
		$this->db->select('*');
		$this->db->from('parts');
		$this->db->where('id',$where_id);
		$query = $this->db->get();
		return $query->row();
	}
    
    function get_only_complain_data($id){
		$select = "c.*, party.*,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('complain c');
		$this->db->join('party party', 'party.party_id = c.party_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = c.created_by', 'left');
		$this->db->where('c.complain_id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		return $query->row(0);
	}
	
	function get_column_data_array($table_name,$column_name)
	{
		$array = array();
		$sql = "SELECT ".$column_name." FROM ".$table_name." GROUP BY ".$column_name." ORDER BY ".$column_name." ASC";
		$rows = $this->db->query($sql)->result();
		$i = 0;
		foreach ($rows as $row) {
			$array[] = $row->$column_name;
			$i++;
		}
		return $array;
	}
    
    public function get_two_columns_val_by_where($table_name,$column_name1,$column_name2,$where_array)
	{
		$this->db->select($column_name1.','.$column_name2);
		$this->db->from($table_name);
		$this->db->where($where_array);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
    
    function get_chart_data_as_per_date($from_date, $to_date, $order_status, $quotation_status_id) {
        $order_status_where = '';
        if($order_status != 'all'){
            $order_status_where = " AND so.is_dispatched = ". $order_status;
        }
        $quotation_status_where = '';
        if(!empty($quotation_status_id)){
            $quotation_status_where = " AND qo.quotation_status_id = ". $quotation_status_id;
        }
        $sql_sales = $this->db->query("SELECT  SUM(soi.`net_amount`) as net_amount, CONCAT(YEAR(so.sales_order_date),DATE_FORMAT(so.sales_order_date,'%m')) as month FROM sales_order_items soi INNER JOIN sales_order so ON so.id = soi.sales_order_id WHERE so.sales_order_date >= '" . $from_date . "' AND so.sales_order_date <= '" . $to_date . "' ". $order_status_where ." GROUP BY EXTRACT(month from so.sales_order_date)");
        $sales_order = $sql_sales->result_array();
        $sql_quotation = $this->db->query("SELECT SUM(qoi.`net_amount`) as net_amount,CONCAT(YEAR(qo.quotation_date),DATE_FORMAT(qo.quotation_date,'%m')) as month FROM `quotation_items` qoi INNER JOIN quotations qo ON qo.id = qoi.`quotation_id` WHERE qo.quotation_date >= '" . $from_date . "' AND qo.quotation_date <= '" . $to_date . "' ". $quotation_status_where ." GROUP BY EXTRACT(month from qo.quotation_date)");
        $quotation = $sql_quotation->result_array();

        $sales_order_data = array();
        foreach ($sales_order as $sales_order_row) {
            $sales_order_data[$sales_order_row['month']] = $sales_order_row['net_amount'];
        }
        $quotation_data = array();
        foreach ($quotation as $quotation_row) {
            $quotation_data[$quotation_row['month']] = $quotation_row['net_amount'];
        }
//        print_r($sales_order_data); exit;
        $response = array();
        while (strtotime($from_date) <= strtotime($to_date)) {
            $month = date('Y', strtotime($from_date)).date('m', strtotime($from_date));
            $response[] = array(
                'y' => date('Y', strtotime($from_date)).'-'.date('M', strtotime($from_date)),
                'item1' => isset($sales_order_data[$month]) ? (integer) $sales_order_data[$month] : 0,
                'item2' => isset($quotation_data[$month]) ? (integer) $quotation_data[$month] : 0,
            );
            $from_date = date('d M Y', strtotime($from_date.'+ 1 month'));
        }

        function date_compare($a, $b) {
            $t1 = strtotime($a['y']);
            $t2 = strtotime($b['y']);
            return $t1 - $t2;
        }
        usort($response, 'date_compare');
        return $response;
    }

    function autogenerate_password(){
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $password = array(); 
            $alpha_length = strlen($alphabet) - 1; 
            for ($i = 0; $i < 10; $i++){
                $n = rand(0, $alpha_length);
                $password[] = $alphabet[$n];
            }
            return implode($password); 
        }
        
    function get_all_enquiry($staff_id, $from_date, $to_date) {
        $this->db->select("count(i.inquiry_id) as enquiry");
        $this->db->from('inquiry i');
        $this->db->join('party p', 'p.party_id = i.party_id', 'left');
        $this->db->where('p.active !=', 0);
        if(!empty($staff_id)){
            $this->db->where('p.current_party_staff_id', $staff_id);
        }
        $this->db->where('i.lead_or_inquiry', 'enquiry');
        $this->db->where('DATE(i.inquiry_date) >=', $from_date);
        $this->db->where('DATE(i.inquiry_date) <=', $to_date);
        $query = $this->db->get();
        return $query->row()->enquiry;
    }
        
    function get_all_quotation($staff_id, $from_date, $to_date) {
		$this->db->select("count(q.id) as quotation");
		$this->db->from('quotations q');
		$this->db->join('party p', 'p.party_id = q.party_id', 'left');
        $this->db->where('p.active !=', 0);
        if(!empty($staff_id)){
            $this->db->where('p.current_party_staff_id', $staff_id);
        }
        $this->db->where('DATE(q.quotation_date) >=', $from_date);
        $this->db->where('DATE(q.quotation_date) <=', $to_date);
		$query = $this->db->get();
		return $query->row()->quotation;
	}
    
    function get_all_sales_order_without_cancel($staff_id, $from_date, $to_date) {
		$this->db->select("count(so.id) as sales_order");
		$this->db->from('sales_order so');
		$this->db->join('party p', 'p.party_id = so.sales_to_party_id', 'left');
        $this->db->where('p.active !=', 0);
        if(!empty($staff_id)){
            $this->db->where('p.current_party_staff_id', $staff_id);
        }
        $this->db->where('so.is_dispatched !=', CANCELED_ORDER_ID);
        $this->db->where('DATE(so.sales_order_date) >=', $from_date);
        $this->db->where('DATE(so.sales_order_date) <=', $to_date);
		$query = $this->db->get();
		return $query->row()->sales_order;
	}
    
    function get_all_sales_order($staff_id, $from_date, $to_date, $is_dispatched) {
		$this->db->select("count(so.id) as sales_order");
		$this->db->from('sales_order so');
		$this->db->join('party p', 'p.party_id = so.sales_to_party_id', 'left');
        $this->db->where('p.active !=', 0);
        if(!empty($staff_id)){
            $this->db->where('p.current_party_staff_id', $staff_id);
        }
        $this->db->where('so.is_dispatched', $is_dispatched);
        $this->db->where('DATE(so.sales_order_date) >=', $from_date);
        $this->db->where('DATE(so.sales_order_date) <=', $to_date);
		$query = $this->db->get();
		return $query->row()->sales_order;
	}
    
    function get_all_proforma_invoice($staff_id, $from_date, $to_date) {
		$this->db->select("count(pi.id) as proforma_invoice");
		$this->db->from('proforma_invoices pi');
		$this->db->join('party p', 'p.party_id = pi.sales_to_party_id', 'left');
        $this->db->where('p.active !=', 0);
        if(!empty($staff_id)){
            $this->db->where('p.current_party_staff_id', $staff_id);
        }
        $this->db->where('DATE(pi.sales_order_date) >=', $from_date);
        $this->db->where('DATE(pi.sales_order_date) <=', $to_date);
		$query = $this->db->get();
		return $query->row()->proforma_invoice;
	}
    
    function get_all_challan($staff_id, $from_date, $to_date) {
		$this->db->select("count(c.id) as challan");
		$this->db->from('challans c');
		$this->db->join('party p', 'p.party_id = c.sales_to_party_id', 'left');
        $this->db->where('p.active !=', 0);
        if(!empty($staff_id)){
            $this->db->where('p.current_party_staff_id', $staff_id);
        }
        $this->db->where('DATE(c.challan_date) >=', $from_date);
        $this->db->where('DATE(c.challan_date) <=', $to_date);
		$query = $this->db->get();
		return $query->row()->challan;
	}
    
    function get_all_invoice($staff_id, $from_date, $to_date) {
		$this->db->select("count(i.id) as invoice");
		$this->db->from('invoices i');
		$this->db->join('party p', 'p.party_id = i.sales_to_party_id', 'left');
        $this->db->where('p.active !=', 0);
        if(!empty($staff_id)){
            $this->db->where('p.current_party_staff_id', $staff_id);
        }
        $this->db->where('DATE(i.invoice_date) >=', $from_date);
        $this->db->where('DATE(i.invoice_date) <=', $to_date);
		$query = $this->db->get();
		return $query->row()->invoice;
	}
    
    function get_purchase_invoice_total_qty($item_id, $from_date, $to_date=''){
        $this->db->select('SUM(pii.quantity) AS purchase_qty');
        $this->db->from('purchase_invoice pi');
        $this->db->join('purchase_invoice_items pii', 'pii.invoice_id = pi.invoice_id', 'left');
        $this->db->where('pii.item_id', $item_id);
        if(!empty($to_date)){
            $this->db->where('pi.invoice_date >=',$from_date);
            $this->db->where('pi.invoice_date <=',$to_date);
        } else {
            $this->db->where('pi.invoice_date <',$from_date);
        }
        $query = $this->db->get();
        return $query->row()->purchase_qty;
    }
    
    function get_challan_to_sales_order_or_profoma_invoice_total_qty($item_id, $from_date, $to_date=''){
        $this->db->select('c.id, c.sales_order_id, c.sales_order_item_id, soi.item_id, soi.quantity');
        $this->db->from('challans c');
        $this->db->join('sales_order_items soi', 'soi.id = c.sales_order_item_id', 'left');
        if(!empty($to_date)){
            $this->db->where('c.challan_date >=', $from_date);
            $this->db->where('c.challan_date <=', $to_date);
        } else {
            $this->db->where('c.challan_date <', $from_date);
        }
        $this->db->where('c.sales_order_id is NOT NULL', NULL, FALSE);
        $this->db->where('c.sales_order_item_id is NOT NULL', NULL, FALSE);
        $this->db->where('soi.item_id is NOT NULL', NULL, FALSE);
        $query = $this->db->get();
        $sales_orders = $query->result();
        
        $this->db->select('c.id, c.proforma_invoice_id, c.proforma_invoice_item_id, pii.item_id, pii.quantity');
        $this->db->from('challans c');
        $this->db->join('proforma_invoice_items pii', 'pii.id = c.proforma_invoice_item_id', 'left');
        if(!empty($to_date)){
            $this->db->where('c.challan_date >=', $from_date);
            $this->db->where('c.challan_date <=', $to_date);
        } else {
            $this->db->where('c.challan_date <', $from_date);
        }
        $this->db->where('c.proforma_invoice_id is NOT NULL', NULL, FALSE);
        $this->db->where('c.proforma_invoice_item_id is NOT NULL', NULL, FALSE);
        $this->db->where('pii.item_id is NOT NULL', NULL, FALSE);
        $query1 = $this->db->get();
        $proforma_query = $query1->result();
        
        $sales_order_and_profoma_invoice = array_merge($sales_orders, $proforma_query);
        
        $total = 0;
        if(!empty($sales_order_and_profoma_invoice)){
            foreach ($sales_order_and_profoma_invoice as $sales_order){
                $this->db->select('bd.quantity,b.bom_id');
                $this->db->from('bom_master b');
                $this->db->join('bom_item_details bd', 'bd.bom_id=b.bom_id', 'left');
                $this->db->where('b.sales_item_id', $sales_order->item_id);
                $this->db->where('bd.item_id', $item_id);
                if(!empty($to_date)){
                    $this->db->where('effective_date >=',$from_date);
                    $this->db->where('effective_date <=',$to_date);
                } else {
                    $this->db->where('effective_date <',$from_date);
                }
                $this->db->order_by('effective_date', 'desc');
                $this->db->limit(1);
                $query_1 = $this->db->get();
//                echo '<pre>'. $this->db->last_query();
                $bom_qty = $query_1->row()->quantity;
                $qty = $bom_qty * $sales_order->quantity;
                $total = $total + $qty;
            }
        }
        return $total;
    }
    
    function get_terms_and_conditions_module_names(){
		$this->db->select("*");
		$this->db->from('terms_and_conditions');
		$this->db->order_by('sort_1');
		$this->db->group_by('module_name');
		$query = $this->db->get();
		return $query->result();
	}
}