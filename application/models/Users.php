<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users extends CI_Model
{
    private $table_name = 'users';            // user accounts
    private $profile_table_name = 'user_profiles';    // user profiles

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    /**
     * Check if given password key is valid and user is authenticated.
     *
     * @param	int
     * @param	string
     * @param	int
     * @return	void
     */
    function reset_password($user_id, $new_pass, $new_pass_key)
    {
        $this->db->set('password',md5($new_pass));
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key',$new_pass_key);
        $this->db->update($this->table_name);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
}