<?php
ini_set('max_execution_time', 3000);

/**
 * Class AppModel
 * @property CI_DB_active_record $db
 */
class AppModel extends CI_Model
{
    protected $currCompetition = 0;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function have_access_role($module, $role)
    {
        $status = 0;
        $user_roles = $this->session->userdata('user_roles');
        if (isset($user_roles[$module]) && in_array($role, $user_roles[$module])) {
            $status = 1;
        }
        return $status;
    }


    /**
     * ------------------- Required Methods --------------------
     */

    /**
     * @param $username
     * @param $pass
     * @return bool
     */
    function login($username, $pass)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('active !=', 0);
        $this->db->where('email', $username);
        $this->db->where('pass', md5($pass));
        $this->db->limit('staff');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getPartyContactPersons($id)
    {
        $sql_list_query = "SELECT * FROM contact_person WHERE party_id = $id";
        $query = $this->db->query($sql_list_query);
        return $query->result();
    }

    /**
     * Check unique email for party
     */
    public function check_party_email_validation($email, $id)
    {
        $duplicate = 0;
        if ($id > 0) {
            $email = trim(strtolower($email));
            $sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(email_id)) LIKE '%" . addslashes($email) . "%' AND party_id != $id";
            $query = $this->db->query($sql_list_query);
            $rows = $query->result_array();
            if (count($rows) > 0) {
                $duplicate = 1;
            }
        } else {
            $email = trim(strtolower($email));
            $sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(email_id)) LIKE '%" . addslashes($email) . "%'";
            $query = $this->db->query($sql_list_query);
            $rows = $query->result_array();
            if (count($rows) > 0) {
                $duplicate = 1;
            }
        }

        return $duplicate;
    }

    /**
     * ------------------- Required Methods
     */

    /**
     * @param $table
     * @param $column_order
     * @param $column_search
     * @param int $user_type
     */

    private function get_datatables_query($table, $column_order, $column_search, $user_type = 3)
    {
        $this->db->where('competition_id =', $this->currCompetition);
        $this->db->where('user_type !=', 1);
        if ($user_type == 2) {
            $this->db->where('user_type !=', 3);
        } else {
            $this->db->where('user_type !=', 2);
        }
        $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if (isset($_POST['search']) && $_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($column_order)) {
            $order = $column_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

    }

    function get_datatables($table, $column_order, $column_search, $user_type = 3)
    {
        $this->get_datatables_query($table, $column_order, $column_search, $user_type);
        if (isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($table, $column_order, $column_search, $user_type = 3)
    {
        $this->get_datatables_query($table, $column_order, $column_search, $user_type);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($table, $user_type = 3)
    {
        $this->db->from($table);
        $this->db->where('user_type', $user_type);
        $this->db->where('competition_id', $this->currCompetition);
        return $this->db->count_all_results();
    }

    /**
     * @param $table
     * @param $column
     * @param $search_value
     * @param array $where
     * @return mixed
     */
    function getAutoCompleteData($table, $column, $search_value, $where = array())
    {
        $this->db->select('*');
        $this->db->from($table);
        if (!empty($where)) {
            $this->db->from($where);
        }
        $this->db->like($column, $search_value);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * @param $table
     * @param $id_column
     * @param $column
     * @param $column_val
     * @return int
     */
    function get_id_by_val($table, $id_column, $column, $column_val)
    {
        $this->db->select($id_column);
        $this->db->from($table);
        $this->db->where($column, $column_val);
        $this->db->limit('1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->$id_column;
        } else {
            return null;
        }

        /* else{
            $this->db->insert($table,array($column=>$column_val));
            return $this->db->insert_id();
        } */
    }


    public function getModuleRoles_v1()
    {
        $array = array();
        $sql = "SELECT * FROM website_modules WHERE id > 34";
        $result = $this->db->query($sql)->result_array();
        $response = array();
        foreach ($result as $key => $row) {
            $modules_order = explode('.', $row['main_module']);
            if (count($modules_order) == 1) {
                $response[$modules_order[0]] = $row;
            } elseif (count($modules_order) == 2) {
                $response[$modules_order[0]]['sub_module'][] = $row;
            } elseif (count($modules_order) == 3) {
                $response[$modules_order[0]]['sub_module'][$modules_order[1]]['sub_module'][] = $row;
            }
        }
        $sorted_modules = array();
        foreach ($response as $key => $row_1) {
            $sorted_modules[] = array(
                'id' => $row_1['id'],
                'title' => $row_1['title'],
                'table_name' => $row_1['table_name'],
                'main_module' => $row_1['main_module'],
            );
            if (isset($row_1['sub_module']) && count($row_1['sub_module'])) {
                foreach ($row_1['sub_module'] as $row_2) {
                    $sorted_modules[] = array(
                        'id' => $row_2['id'],
                        'title' => $row_2['title'],
                        'table_name' => $row_2['table_name'],
                        'main_module' => $row_2['main_module'],
                    );

                    if (isset($row_2['sub_module']) && count($row_2['sub_module'])) {
                        foreach ($row_2['sub_module'] as $row_3) {
                            $sorted_modules[] = array(
                                'id' => $row_3['id'],
                                'title' => $row_3['title'],
                                'table_name' => $row_3['table_name'],
                                'main_module' => $row_3['main_module'],
                            );

                            if (isset($row_3['sub_module']) && count($row_3['sub_module'])) {
                                foreach ($row_3['sub_module'] as $row_4) {
                                    $sorted_modules[] = array(
                                        'id' => $row_4['id'],
                                        'title' => $row_4['title'],
                                        'table_name' => $row_4['table_name'],
                                        'main_module' => $row_4['main_module'],
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
        //pre_die($sorted_modules);

        foreach ($sorted_modules as $key => $row) {
            $sql = "SELECT * FROM module_roles WHERE module_id='" . $row['id'] . "'";
            $roles = $this->db->query($sql)->result_array();
            $sorted_modules[$key]['roles'] = $roles;
        }
        return $sorted_modules;
    }

    /**
     * @param $table_name
     * @param $column_nm
     * @param $column_val
     * @return bool
     */
    function check_val_exist($table_name, $column_nm, $column_val)
    {
        $this->db->where($column_nm, $column_val);
        $query = $this->db->get($table_name);
        return $query->num_rows() > 0;
    }

    /**
     * @return mixed
     */
    public function get_config()
    {
        return $this->db->get('config');
    }

    public function ActiveUserStatus($username, $pass)
    {
        $data = array(
            'is_login' => 1
        );
        $this->db->where('email', $username);
        $this->db->where('pass', md5($pass));
        $this->db->update('staff', $data);
    }

    function getUserRoleIDS($user_id)
    {
        $array = array();
        $sql = "SELECT * FROM staff_roles WHERE staff_id='$user_id'";
        $rows = $this->db->query($sql)->result();
        $i = 0;
        foreach ($rows as $row) {
            $array[$i] = $row->role_id;
            $i++;
        }
        return $array;
    }

    public function getModuleRoles()
    {
        $array = array();
        $sql = "SELECT * FROM website_modules WHERE id!='".MASTER_PRODUCT_GROUP_MENU_ID."' ORDER BY INET_ATON(REPLACE(TRIM(RPAD(main_module,8,' 0')),' ','.'))";
        $rows = $this->db->query($sql)->result();

        $i = 0;
        foreach ($rows as $row) {
            $sql = "SELECT * FROM module_roles WHERE module_id='" . $row->id . "'";
            $roles = $this->db->query($sql)->result_array();

            $array[$i]['id'] = $row->id;
            $array[$i]['title'] = $row->title;
            $array[$i]['main_module'] = $row->main_module;
            $array[$i]['roles'] = $roles;

            $i++;
        }

        return $array;
    }

    public function DeActiveUserStatus($staff_id)
    {
        $data = array(
            'is_login' => 0
        );
        $this->db->where('staff_id', $staff_id);
        $this->db->update('staff', $data);
    }

    function getUserChatRoleIDS($user_id)
    {
        $array = array();
        $sql = "SELECT * FROM chat_roles WHERE staff_id='$user_id'";
        $rows = $this->db->query($sql)->result();
        $i = 0;
        foreach ($rows as $row) {
            $array[$i] = $row->allowed_staff_id;
            $i++;
        }
        return $array;
    }
    
    function getUserQuoteReminderIDS($user_id)
    {
        $array = array();
        $sql = "SELECT * FROM quotation_reminder_roles WHERE staff_id='$user_id'";
        $rows = $this->db->query($sql)->result();
        $i = 0;
        foreach ($rows as $row) {
            $array[$i] = $row->allowed_staff_id;
            $i++;
        }
        return $array;
    }
	
	function getUserPrintLetterIDS($user_id)
    {
		$array = array();
        $sql = "SELECT * FROM `print_letter_roles` WHERE `staff_id`='$user_id' ORDER BY `letter_id` ASC";
        $rows = $this->db->query($sql)->result();
        $i = 0;
        foreach ($rows as $row) {
            $array[] = $row->letter_id;
            $i++;
        }
        return $array;
    }
    
    public function update_staff_socket_id($staff_id, $socket_id){
        $data = array(
            'is_login' => 1,
            'socket_id' => $socket_id
        );
        $this->db->where('staff_id', $staff_id);
        $this->db->update('staff', $data);
    }
}
