<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Finance
 * &@property Crud $crud
 * &@property AppLib $applib
 */
class Finance extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		if (!$this->session->userdata('is_logged_in')) {
			redirect('/auth/login/');
		}
	}

	public function expected_payment($id = 0)
	{
		set_page('finance/expected_payment');
	}

	function getLedgers()
	{

		/*$payment = $this->db->query("SELECT received_payment_date as created, total_cmount as debit, 'debit'
										FROM invoices where sales_to_party_id = ".$_POST['id']."
										UNION ALL 
										SELECT payment_date as created, amount as debit, payment_type
										FROM payment where party_id = ".$_POST['id']." 
										ORDER BY created ASC");		*/
		$payment = $this->db->query("SELECT committed_date as created, total_cmount as amount, 'debit' as payment_type, 'Invoice' as tableName
										FROM invoices where sales_to_party_id = " . $_POST['id'] . "
										UNION ALL SELECT payment_date as created, amount as amount, payment_type, 'Payment' as tableName 
										FROM payment where party_id = " . $_POST['id'] . "
										ORDER BY created ASC");
		$cnt = 0;
		foreach ($payment->result() as $row) {
			$output[$cnt] = $row;
			$cnt++;
		}
		echo json_encode($output);
	}

	function invoice_list()
	{
		if($this->app_model->have_access_role(INVOICE_MODULE_ID, "view")){
			set_page('finance/invoice_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function getContactPersonByParty($party_id, $get_table_html = false)
	{
		$this->db->select('cp.contact_person_id,cp.priority,cp.name,cp.mobile_no,cp.phone_no,dept.department,desi.designation,cp.email');
		$this->db->from('contact_person cp');
		$this->db->join('department dept', 'dept.department_id = cp.department_id', 'left');
		$this->db->join('designation desi', 'desi.designation_id = cp.designation_id', 'left');
		$this->db->where('cp.party_id', $party_id);
		$this->db->where('cp.active !=',0);
		$this->db->order_by('cp.priority,cp.contact_person_id');
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			if ($get_table_html) {
				$html_response = '';
				foreach ($query->result() as $row) {
					$html_response .= '<tr>';
					$html_response .= '<td>' . $row->priority . '</td>';
					$html_response .= '<td>' . $row->name . '</td>';
					$html_response .= '<td>' . $row->email . '</td>';
					$html_response .= '<td>' . $row->department . '</td>';
					$html_response .= '<td>' . $row->designation . '</td>';
					$html_response .= '<td>' . $row->phone_no . '</td>';
					$html_response .= '</tr>';
				}
				return $html_response;
			} else {
				foreach ($query->result() as $row) {
					$response[] = array(
						'contact_person_id' => $row->contact_person_id,
						'priority' => $row->priority,
						'name' => $row->name,
						'phone_no' => $row->phone_no,
						'mobile_no' => $row->mobile_no,
						'department' => $row->department,
						'designation' => $row->designation,
					);
				}
				return $response;
			}
		} else {
			return $response;
		}
	}

	function invoice_edit($invoice_id)
	{
		$data = array(
			'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
			'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
			'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
			'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
			'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
			'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
			'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
			'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
			'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
			'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
			'billing_terms_data' => $this->get_invoice_billing_terms_data($invoice_id),
		);
		$data['invoice_data'] = $this->get_invoice_by_id($invoice_id);
		$data['invoice_data']->invoice_no = sprintf("%04d", $data['invoice_data']->invoice_no);
		$data['invoice_items'] = $this->get_invoice_items($invoice_id, 'object');
		$data['login_data'] = $this->get_invoice_logins($invoice_id);
		$data['login_data']->created_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $data['login_data']->created_by_id));
		$data['login_data']->updated_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $data['login_data']->last_modified_by_id));
		
		$challan_data = $this->crud->get_row_by_id('challans', array('id' => $data['invoice_data']->challan_id));
		$data['invoice_data']->challan_no = $challan_data[0]->id;
		if(!empty($challan_data[0]->challan_date)){ 
			$data['invoice_data']->challan_date = date('d-m-Y',strtotime($challan_data[0]->challan_date));
		} else {
			$data['invoice_data']->challan_date = date('d-m-Y');
		}
		
		$sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $data['invoice_data']->sales_order_id));
		$data['invoice_data']->sales_order_no = $sales_order_data[0]->sales_order_no;
		if(!empty($sales_order_data[0]->sales_order_date)){ 
			$data['invoice_data']->sales_order_date = date('d-m-Y',strtotime($sales_order_data[0]->sales_order_date));
		} else {
			$data['invoice_data']->sales_order_date = date('d-m-Y');
		}
		$data['invoice_data']->cust_po_no = $sales_order_data[0]->cust_po_no;
		if(!empty($sales_order_data[0]->po_date)){ 
			$data['invoice_data']->po_date = date('d-m-Y',strtotime($sales_order_data[0]->po_date));
		} else {
			$data['invoice_data']->po_date = date('d-m-Y');
		}
		
		$quotation_data = $this->crud->get_row_by_id('quotations', array('id' => $sales_order_data[0]->quotation_id));
		$data['invoice_data']->quotation_no = $quotation_data[0]->quotation_no;
		
		$data['kind_attn_data'] = $this->getContactPersonByParty($data['invoice_data']->sales_to_party_id);
		if(!empty($data['invoice_data']->kind_attn_id)){
			$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['invoice_data']->kind_attn_id));
			$designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person'][0]->designation_id));
			$department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person'][0]->department_id));
			$data['contact_person'][0]->designation = '';
			if(!empty($designation)){
				$data['contact_person'][0]->designation = $designation[0]->designation;
			}
			$data['contact_person'][0]->department = '';
			if(!empty($department)){
				$data['contact_person'][0]->department = $department[0]->department;
			}
		}
		
		/*echo "<pre>";
		print_r($data);die();*/
		if($this->app_model->have_access_role(INVOICE_MODULE_ID, "edit")){
			set_page('finance/invoice_edit', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	
	function get_cal_code_definition($billing_terms_detail_id)
	{
		$this->db->select('ccd.is_first_element,ccd.cal_operation,ccd.cal_code_id,btd.cal_code');
		$this->db->from('cal_code_definition ccd');
		$this->db->join('billing_terms_detail btd', 'btd.id = ccd.cal_code_id');
		$this->db->where('ccd.billing_terms_detail_id', $billing_terms_detail_id);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$response[] = array(
					'is_first_element' => $row->is_first_element == 1 ? true : false,
					'cal_operation' => $row->cal_operation,
					'cal_code_id' => $row->cal_code_id,
					'cal_code' => $row->cal_code,
				);
			}
		}
		return $response;
	}

	/**
	 * @param $billing_terms_detail_id
	 * @return array
	 */
	function get_invoice_cal_code_definition($billing_terms_detail_id)
	{
		$this->db->select('ccd.is_first_element,ccd.cal_operation,ccd.cal_code_id,btd.cal_code');
		$this->db->from('invoice_cal_code_definition ccd');
		$this->db->join('invoice_billing_terms btd', 'btd.id = ccd.cal_code_id');
		$this->db->where('ccd.billing_terms_detail_id', $billing_terms_detail_id);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$response[] = array(
					'is_first_element' => $row->is_first_element == 1 ? true : false,
					'cal_operation' => $row->cal_operation,
					'cal_code_id' => $row->cal_code_id,
					'cal_code' => $row->cal_code,
				);
			}
		}
		return $response;
	}

	function get_invoice_by_id($invoice_id)
	{
		$this->db->select('pi.*,pi.id as invoice_id,p.party_code,p.party_name,p.agent_id,p.address,p.city_id,p.state_id,p.country_id,p.fax_no,p.email_id,p.website,p.gst_no,p.pan_no,p.ecc_no,p.pincode,p.phone_no');
		$this->db->from('invoices pi');
		$this->db->join('party p', 'p.party_id = pi.sales_to_party_id');
		$this->db->where('pi.id', $invoice_id);
		$this->db->where('p.active !=', 0);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_invoice_logins($invoice_id)
	{
		$this->db->select('pil.*');
		$this->db->from('invoice_logins pil');
		$this->db->where('pil.invoice_id', $invoice_id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function invoice_datatable()
	{
		$config['table'] = 'invoices pi';
		$config['select'] = 'pi.*,p.party_name,so.sales_order_no, q.quotation_no';
		$config['column_order'] = array(null, 'pi.invoice_no', 'pi.challan_id', 'so.sales_order_no', 'q.quotation_no', 'p.party_name', 'pi.invoice_date');
		$config['column_search'] = array('pi.invoice_no', 'pi.challan_id', 'so.sales_order_no', 'q.quotation_no', 'p.party_name', 'pi.invoice_date');

		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = pi.sales_to_party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = pi.sales_order_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'q.id = so.quotation_id', 'join_type' => 'left');

		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		$config['where_string'] = ' 1 = 1 ';

		if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
		} else {
			$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
			$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");

			$config['custom_where'] = '( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
			if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
			} else if ($cu_accessExport == 1) {
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if ($cu_accessDomestic == 1) {
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		$role_delete = $this->app_model->have_access_role(INVOICE_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(INVOICE_MODULE_ID, "edit");

		foreach ($list as $order_row) {
			$row = array();
			$action = '';
			if ($role_edit) {
				$action .= '<a href="' . base_url('finance/invoice_edit/' . $order_row->id) . '"
				   class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
			}
			if ($role_delete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs"
				   data-href="' . base_url('finance/delete_invoice/' . $order_row->id) . '"><i class="fa fa-trash"></i></a> ';
			}
			$action .= ' | <a href="' . base_url('finance/invoice_print/' . $order_row->id) . '" target="_blank" class="btn-primary btn-xs" ><i class="fa fa-print"></i></a> ';
			$action .= ' | <a href="' . base_url('finance/invoice_email/' . $order_row->id) . '" class="email_button">Email</a>';
			$row[] = $action;
			$row[] = '<a href="' . base_url('finance/invoice_edit/' . $order_row->id . '?view') . '" >'.sprintf("%04d", $order_row->invoice_no).'</a>';
			$row[] = '<a href="' . base_url('finance/invoice_edit/' . $order_row->id . '?view') . '" >'.$order_row->challan_id.'</a>';
			$row[] = '<a href="' . base_url('finance/invoice_edit/' . $order_row->id . '?view') . '" >'.$order_row->sales_order_no.'</a>';
			$row[] = '<a href="' . base_url('finance/invoice_edit/' . $order_row->id . '?view') . '" >'.$order_row->quotation_no.'</a>';
			$row[] = '<a href="' . base_url('finance/invoice_edit/' . $order_row->id . '?view') . '" >'.$order_row->party_name.'</a>';
			$row[] = '<a href="' . base_url('finance/invoice_edit/' . $order_row->id . '?view') . '" >'.strtotime($order_row->invoice_date) != 0?date('d-m-Y', strtotime($order_row->invoice_date)):''.'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function invoice()
	{
		$this->db->select('s.id,s.sales_order_no,s.sales_order_date,s.sales_order_status_id,p.party_name ');
		$this->db->from('sales_order s');
		$this->db->join('party p', 'p.party_id = s.sales_to_party_id', 'left');
		$this->db->where('p.active !=', 0);
		$query = $this->db->get();
		$sales_order_data = $query->result();
		$data = array(
			'sales_order_data' => $sales_order_data,
			'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
			'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
			'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
			'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
			'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
			'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
			'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
			'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
			'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
			'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
			/*'sales_invoice_payment_terms' => $this->applib->getTermsandConditions('sales_invoice_payment_terms'),
			'sales_invoice_terms_and_conditions' => $this->applib->getTermsandConditions('sales_invoice_terms_and_conditions'),*/
		);
		if($this->app_model->have_access_role(INVOICE_MODULE_ID, "add")){
			set_page('finance/invoice', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}

	}

	/**
	 * Challan DataTable
	 */
	function sales_challan_datatable(){

		$config['table'] = 'challans c';
		$config['select'] = 'c.id,c.challan_date,sales.sales,q.quotation_no,q.enquiry_no,so.id as sales_order_id,so.sales_order_no,so.sales_order_date,so.sales_order_status_id,p.party_name,p.phone_no,p.email_id';
		$config['joins'][] =  array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'quotations q', 'join_by' => 'q.id = so.quotation_id','join_type'=>'left');
		$config['joins'][] = array('join_table' => 'sales', 'join_by' => 'sales.id = so.sales_id', 'join_type' => 'left');
		$config['column_order'] = array('p.party_name', 'q.enquiry_no', 'q.quotation_no', 'so.sales_order_no', 'c.id', 'c.challan_date', 'p.phone_no', 'p.email_id', 'sales.sales');
		$config['column_search'] = array('p.party_name', 'q.enquiry_no', 'q.quotation_no', 'so.sales_order_no', 'c.id', 'c.challan_date', 'p.phone_no', 'p.email_id', 'sales.sales');
		$config['order'] = array('c.id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		if ($this->session->userdata('is_logged_in')['staff_id'] && strtolower($this->session->userdata('is_logged_in')['user_type']) != 'administrator') {
			$config['wheres'][] = array('column_name' => 'c.created_by', 'column_value' => $this->session->userdata('is_logged_in')['staff_id']);
			$accessExport = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "export");
			$accessDomestic = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "domestic");
			if ($accessExport == 1 && $accessDomestic == 1) {

			} else if ($accessExport == 1) {
				$config['wheres'][] = array('column_name' => 'so.sales_id', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if ($accessDomestic == 1) {
				$config['wheres'][] = array('column_name' => 'so.sales_id', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}
		$config['custom_where'] = "c.id NOT IN(SELECT challan_id FROM invoices)";
		
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $sales_challan) {
			$row = array();
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->party_name</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->enquiry_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->quotation_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->sales_challan_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->id</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->challan_date</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->phone_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->email_id</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-challan-data' data-sales_challan_id='$sales_challan->id' data-sales_order_id='$sales_challan->sales_order_id'>$sales_challan->sales</a>";
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
	
	/**
	 * Order DataTable
	 */
	function sales_order_datatable(){

		$config['table'] = 'sales_order s';
		$config['select'] = 's.id,sales.sales,q.quotation_no,q.enquiry_no,s.sales_order_no,s.sales_order_date,s.sales_order_status_id,p.party_name,p.phone_no,p.email_id';
		$config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales', 'join_by' => 'sales.id = s.sales_id', 'join_type' => 'left');
		$config['column_order'] = array('q.enquiry_no', 'q.quotation_no', 'sales.sales', 's.sales_order_no', 's.sales_order_date', 'p.party_name', 'p.phone_no', 'p.email_id');
		$config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 'sales.sales', 's.sales_order_no', 's.sales_order_date', 'p.party_name', 'p.phone_no', 'p.email_id');
		$config['order'] = array('s.id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		if ($this->session->userdata('is_logged_in')['staff_id'] && strtolower($this->session->userdata('is_logged_in')['user_type']) != 'administrator') {
			$config['wheres'][] = array('column_name' => 's.created_by', 'column_value' => $this->session->userdata('is_logged_in')['staff_id']);
			$accessExport = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "export");
			$accessDomestic = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "domestic");
			if ($accessExport == 1 && $accessDomestic == 1) {

			} else if ($accessExport == 1) {
				$config['wheres'][] = array('column_name' => 's.sales_id', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if ($accessDomestic == 1) {
				$config['wheres'][] = array('column_name' => 's.sales_id', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}

		if (!empty($_POST['request_from'])) {
			if ($_POST['request_from'] == 'invoice') {
				$config['custom_where'] = "s.id NOT IN(SELECT sales_order_id FROM invoices) AND s.id IN(SELECT sales_order_id FROM challans)";
			} elseif ($_POST['request_from'] == 'proforma_invoice') {
				$config['custom_where'] = "s.id NOT IN(SELECT sales_order_id FROM proforma_invoices)";
			}
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $sales_order) {
			$row = array();
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->party_name</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->enquiry_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->quotation_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->sales_order_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->phone_no</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->email_id</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->sales</a>";
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function save_invoice()
	{
		$post_data = $this->input->post();
		//~ echo "<pre>";
		//~ print_r($post_data);
		//~ exit();

		$invoice_data = $post_data['invoice_data'];
		$buyer_data = $post_data['buyer_data'];
		if(isset($buyer_data['state_id'])){ } else { $buyer_data['state_id'] = ''; }
		if(isset($buyer_data['country_id'])){ } else { $buyer_data['country_id'] = ''; }
		$login_data = $post_data['login_data'];

		$invoice_data['invoice_date'] = date('Y-m-d', strtotime(str_replace("/", "-", $invoice_data['invoice_date'])));
		$invoice_data['created_by'] = $this->session->userdata('is_logged_in')['staff_id'];
		
		/*------------ Phone No And Email ID Validation Start-------------------------*/
		// get emails
		$temp = $buyer_data['email_id'];
		$emails = explode(",", $temp);
		$email_status = 1;
		$email_msg = "";

		$email_ids = array();
		// multiple email validation
		if (is_array($emails) && count($emails) > 0) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$email_msg .= "$email is not valid email.&nbsp;";
						$email_status = 0;
					}
					$this->db->select('*');
					$this->db->from('party');
					$this->db->where('party_id !=' ,$invoice_data['sales_to_party_id']);
					$this->db->where('active !=' ,0);
					$this->db->where("FIND_IN_SET( '".$email."', email_id) ");
					$res = $this->db->get();
					$rows = $res->num_rows();
					if ($rows > 0) {
						$result = $res->result();
 						$email_msg .= "$email email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name;
						$email_status = 0;
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}
		if ($email_status == 0) {
			echo json_encode(array("status" => 0, 'message' => $email_msg, 'email_error' => 1));
			exit;
		}
		$buyer_data['email_id'] = implode(',', $email_ids);
		
		$temp = $buyer_data['phone_no'];
		if (trim($buyer_data['phone_no']) == "" && count($email_ids) == 0) {
			$email_msg = "Please enter phone number or email id !";
			echo json_encode(array("status" => 0, 'message' => $email_msg, 'email_error' => 1));
			exit;
		}
		$phone_msg = '';
		$phone_numbers = explode(",", $temp);
		$phone_status = 1;
		$phone_nos = array();
		foreach ($phone_numbers as $phone) {
			$phone = trim($phone);
			$this->db->select('*');
			$this->db->from('party');
			$this->db->where('party_id !=' ,$invoice_data['sales_to_party_id']);
			$this->db->where('active !=' ,0);
			$this->db->where("FIND_IN_SET( '".$phone."', phone_no) ");
			$res = $this->db->get();
			$rows = $res->num_rows();
			if ($rows > 0 && trim($phone) != "") {
				$result = $res->result();
				$phone_msg .= "$phone mobile already exist.&nbsp; &nbsp; <br/> Original Party : ". $result[0]->party_name;
				$phone_status = 0;
			}
		}
		if($phone_status == 0){
			echo json_encode(array("status" => 0, 'message' => $phone_msg, 'phone_error' => 1));
			exit;
		}
		$buyer_data['phone_no'] = $temp;
		/*------------ Phone No And Email ID Validation End-------------------------*/
		
		if (trim($invoice_data['kind_attn_id']) != '' && !empty(trim($invoice_data['kind_attn_id']))) {
			$contact_person = $_POST['contact_person'];
			$contact_person_update['mobile_no'] = $contact_person['contact_person_mobile_no'];
			$contact_person_update['email'] = $contact_person['contact_person_email_id'];
			$this->crud->update('contact_person', $contact_person_update, array('contact_person_id' => $invoice_data['kind_attn_id']));
		}

		$invoice_id = $invoice_data['invoice_id'];
		unset($invoice_data['invoice_id']);
		$invoice_items = array();
		$edit_delete_item_id = (int)isset($_POST['edit_delete_item_id']) ? trim($_POST['edit_delete_item_id']) : 0;
		if (trim($invoice_id) != '' && !empty(trim($invoice_id))) {

			if (!empty($invoice_data['billing_terms_id'])) {
				$billing_terms_id = $this->crud->get_id_by_val('invoices', 'billing_terms_id', 'id', $invoice_id);
				if ($billing_terms_id != $invoice_data['billing_terms_id']) {
					$this->crud->delete('invoice_billing_terms', array('invoice_id' => $invoice_id));
					$this->crud->delete('invoice_cal_code_definition', array('invoice_id' => $invoice_id));
					$this->add_invoice_billing_terms($invoice_id, $invoice_data['billing_terms_id']);
				}
			}
			$this->crud->update('invoices', $invoice_data, array('id' => $invoice_id));
			unset($buyer_data['invoice_id']);
			$this->crud->update('party', $buyer_data, array('party_id' => $buyer_data['party_id']));
			$this->applib->update_outstanding_payment($buyer_data['party_id']);
			$login_data['last_modified_by_id'] = $this->session->userdata('is_logged_in')['staff_id'];
			$login_data['modified_date_time'] = date('Y-m-d H:i:s');
			$login_data['updated_at'] = date('Y-m-d H:i:s');
			$this->crud->update('invoice_logins', $login_data, array('invoice_id' => $invoice_id));
		} else {
			$invoice_type = $this->crud->get_id_by_val('invoice_type', 'invoice_type', 'id', $invoice_data['invoice_type_id']);
			
			$invoice_no = $this->crud->get_max_number('invoices', 'invoice_no');
			$invoice_data['invoice_no'] = (int)$invoice_no->invoice_no + 1;
			
			$invoice_data['created_at'] = date('Y-m-d H:i:s');
			$invoice_id = $this->crud->insert('invoices', $invoice_data);

			if (!empty($invoice_data['billing_terms_id'])) {
				$this->add_invoice_billing_terms($invoice_id, $invoice_data['billing_terms_id']);
			}

			$login_data['invoice_id'] = $invoice_id;
			$login_data['created_by_id'] = $this->session->userdata('is_logged_in')['staff_id'];
			$login_data['created_date_time'] = date('Y-m-d H:i:s');
			$login_data['created_at'] = date('Y-m-d H:i:s');
			$login_data['last_modified_by_id'] = $this->session->userdata('is_logged_in')['staff_id'];
			$login_data['modified_date_time'] = date('Y-m-d H:i:s');
			$login_data['updated_at'] = date('Y-m-d H:i:s');
			$this->crud->insert('invoice_logins', $login_data);

			unset($buyer_data['invoice_id']);
			$this->crud->update('party', $buyer_data, array('party_id' => $buyer_data['party_id']));
			$this->applib->update_outstanding_payment($buyer_data['party_id']);


			$challan_id = $invoice_data['challan_id'];
			$this->db->select('id,item_id,item_code,item_category_id,item_name,item_description,igst,igst_amount,cgst,cgst_amount,sgst,sgst_amount,total_amount,quantity,disc_per,rate,disc_value,amount,item_serial_no,truck_container_no,lr_bl_no,lr_date,contact_person,contact_no,driver_name,driver_contact_no,item_extra_accessories_id');
			$this->db->from('challan_items');
			$this->db->where('challan_id', $challan_id);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result_array() as $key => $item_data) {
					$item_data['invoice_id'] = $invoice_id;
					$item_data['created_at'] = date('Y-m-d H:i:s');
					$item_data['updated_at'] = date('Y-m-d H:i:s');
					$challan_item_id = $item_data['id'];
					unset($item_data['id']);
					$this->db->insert('invoice_items', $item_data);
					if ($edit_delete_item_id == $challan_item_id) {
						$edit_delete_item_id = $this->db->insert_id();
					}
				}
			}
			/*-------- Sales Order Items Data ---------*/
			$this->db->select('*');
			$this->db->from('invoice_items');
			$this->db->where('invoice_id', $invoice_id);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $item_row) {
					$invoice_items[] = $item_row;
				}
			}
		}
		echo json_encode(array('success' => true, 'message' => "Invoice saved successfully!", 'invoice_id' => $invoice_id, 'invoice_items' => $invoice_items, 'edit_delete_item_id' => $edit_delete_item_id));
		exit();
	}

	/**
	 * @return int
	 */
	function get_new_invoice_no()
	{
		$query = $this->db->query("SHOW TABLE STATUS WHERE name='invoices'");
		$row = $query->row_array();
		return $row["Auto_increment"];
	}

	/**
	 * @param string $invoice_type
	 * @return null
	 */
	function get_invoice_no($invoice_type = 'export')
	{
		$invoice_type = strtolower($invoice_type);
		$invoice_cnt = $this->crud->get_id_by_val('config', 'config_value', 'LOWER(config_key)', $invoice_type . '_invoice_cnt');
		$this->db->set('config_value', 'config_value + 1', false);
		$this->db->where('LOWER(config_key)', $invoice_type . '_invoice_cnt');
		$this->db->update('config');
		return strtoupper(substr($invoice_type, 0, 1)) . $invoice_cnt;
	}

	function get_challan()
	{
		$sales_order_id = $this->input->get_post("sales_order_id");
		$challan_id = $this->input->get_post("challan_id");
		$challan_data = $this->crud->get_challan($challan_id);
		$challan_items = $this->crud->get_challan_items($challan_id);
		$party_contact_person = $this->crud->get_contact_person_by_party($challan_data->party_id);
		if(!empty($challan_data->challan_date)){ 
			$challan_data->challan_date = date('d-m-Y',strtotime($challan_data->challan_date));
		}
		$challan_data->taxes_data = ($challan_data->taxes_data) ? unserialize($challan_data->taxes_data) : '';
		echo json_encode(array("challan_data" => $challan_data, "challan_items" => $challan_items, 'party_contact_person' => $party_contact_person));
	}
	
	function get_sales_order()
	{
		$id = $this->input->get_post("id");
		$sales_order_data = $this->crud->get_sales_order($id);
		if(!empty($sales_order_data->po_date)){ 
			$sales_order_data->po_date = date('d-m-Y',strtotime($sales_order_data->po_date));
		}
		$sales_order_data_challan = $this->crud->get_sales_order_challan($id);
		if(!empty($sales_order_data_challan->lr_date)){ 
			$sales_order_data_challan->lr_date = date('d-m-Y',strtotime($sales_order_data_challan->lr_date));
		}
		$sales_order_items = $this->get_sales_order_items($id);
		$sales_order_data->email_id = $sales_order_data->email_id;
		$sales_order_data->phone_no = $sales_order_data->phone_no;
		$party_contact_person = $this->crud->get_contact_person_by_party($sales_order_data->party_id);

		$sales_order_data->taxes_data = ($sales_order_data->taxes_data) ? unserialize($sales_order_data->taxes_data) : '';
		echo json_encode(array("sales_order_data" => $sales_order_data, "sales_order_data_challan" => $sales_order_data_challan, "sales_order_items" => $sales_order_items, 'party_contact_person' => $party_contact_person));
	}
    
    function invoice_print($id)
	{
		$invoice_data = $this->get_invoice_detail($id);
		$company_details = $this->get_company_detail();
		$invoice_items = $this->get_invoice_items($id);
		$invoice_items = $invoice_items[0];
		$billing_terms_data = $this->apply_billing_terms_in_invoice($id);
		
		$challan_data = $this->crud->get_row_by_id('challans', array('id' => $invoice_data->challan_id));
		$invoice_data->challan_no = $challan_data[0]->id;
		if(!empty($challan_data[0]->challan_date)){ 
			$invoice_data->challan_date = date('d/m/Y',strtotime($challan_data[0]->challan_date));
		} else {
			$invoice_data->challan_date = '';
		}
		
		$invoice_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $invoice_data->delivery_through_id));
		$sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $invoice_data->sales_order_id));
		$invoice_data->sales_order_no = $sales_order_data[0]->sales_order_no;
		if(!empty($sales_order_data[0]->sales_order_date)){ 
			$invoice_data->sales_order_date = date('d/m/Y',strtotime($sales_order_data[0]->sales_order_date));
		} else {
			$invoice_data->sales_order_date = '';
		}
		$invoice_data->cust_po_no = $sales_order_data[0]->cust_po_no;
		if(!empty($sales_order_data[0]->po_date)){ 
			$invoice_data->po_date = date('d/m/Y',strtotime($sales_order_data[0]->po_date));
		} else {
			$invoice_data->po_date = '';
		}

		$quotation_data = $this->crud->get_row_by_id('quotations', array('id' => $sales_order_data[0]->quotation_id));
		$invoice_data->quotation_no = $quotation_data[0]->quotation_no;
		
		$invoice_items['hsn_no'] = $this->crud->get_column_value_by_id('items', 'hsn_code', array('id' => $invoice_items['item_id']));
		$invoice_items['item_extra_accessories'] = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $invoice_items['item_extra_accessories_id']));
		$invoice_items['challan_item_id'] = $this->crud->get_column_value_by_id('challan_items', 'id', array('challan_id' => $invoice_data->challan_id));
		
		$this->db->select('imd.*, u.user_name, m.make_name, h.hp_name, k.kw_name, f.frequency_name, r.rpm_name, v.volts_cycles_name');
		$this->db->from('challan_items_motor_details imd');
		$this->db->join('challan_item_user u','u.id = imd.motor_user','left');
		$this->db->join('challan_item_make m','m.id = imd.motor_make','left');
		$this->db->join('challan_item_hp h','h.id = imd.motor_hp','left');
		$this->db->join('challan_item_kw k','k.id = imd.motor_kw','left');
		$this->db->join('challan_item_frequency f','f.id = imd.motor_frequency','left');
		$this->db->join('challan_item_rpm r','r.id = imd.motor_rpm','left');
		$this->db->join('challan_item_volts_cycles v','v.id = imd.motor_volts_cycles','left');
		$this->db->where('challan_item_id', $invoice_items['challan_item_id']);
		$m_query = $this->db->get();
		$motor_data = $m_query->result();
		
		$this->db->select('igd.*, u.user_name, m.make_name, g.gear_type_name, r.ratio_name');
		$this->db->from('challan_items_gearbox_details igd');
		$this->db->join('challan_item_user u','u.id = igd.gearbox_user','left');
		$this->db->join('challan_item_make m','m.id = igd.gearbox_make','left');
		$this->db->join('challan_item_gear_type g','g.id = igd.gearbox_gear_type','left');
		$this->db->join('challan_item_ratio r','r.id = igd.gearbox_ratio','left');
		$this->db->where('challan_item_id', $invoice_items['challan_item_id']);
		$g_query = $this->db->get();
		$gearbox_data = $g_query->result();
		
		$html = $this->load->view('finance/invoice_pdf', array('invoice_data' => $invoice_data, 'company_details' => $company_details, 'invoice_item' => $invoice_items, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'billing_terms_data' => $billing_terms_data), true);
		/*pre_die($html);*/
		$this->load->library('m_pdf');
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$pdf->WriteHTML($html);
		foreach ($invoice_items as $item_row) {
			$this->db->select('*');
			$this->db->from('item_documents');
			$this->db->where('item_id', $item_row['item_id']);
			$this->db->where('document_type', strtolower($invoice_data->sales) . '_invoice');
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $key => $page_detail_row) {
					$print_export_items = $this->load->view('finance/invoice/print_price_table', array('invoice_data' => $invoice_data, 'item_row' => $item_row), true);
					$vars = array(
						'{{Price_Table}}' => $print_export_items,
					);
					$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
					$page_detail_html = $page_detail_row->detail;
					$pdf->AddPage(
						'P', //orientation
						'', //type
						'', //resetpagenum
						'', //pagenumstyle
						'', //suppress
						$margin_left, //margin-left
						$margin_right, //margin-right
						$margin_top, //margin-top
						$margin_bottom, //margin-bottom
						0, //margin-header
						0 //margin-footer
					);
					$pdf->WriteHTML($page_detail_html);
				}
			}
		}
		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
		$pdfFilePath = "invoice.pdf";
		//pre_die($pdf);

		$pdf->Output($pdfFilePath, "I");
	}

	function invoice_email($id)
	{
		$invoice_data = $this->get_invoice_detail($id);
		$company_details = $this->get_company_detail();
		$invoice_items = $this->get_invoice_items($id);
		$billing_terms_data = $this->apply_billing_terms_in_invoice($id);
		$html = $this->load->view('finance/invoice_pdf', array('invoice_data' => $invoice_data, 'company_details' => $company_details, 'invoice_items' => $invoice_items, 'billing_terms_data' => $billing_terms_data), true);
		/*pre_die($html);*/
		$this->load->library('m_pdf');
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$pdf->WriteHTML($html);
		foreach ($invoice_items as $item_row) {
			$this->db->select('*');
			$this->db->from('item_documents');
			$this->db->where('item_id', $item_row['item_id']);
			$this->db->where('document_type', strtolower($invoice_data->sales) . '_invoice');
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $key => $page_detail_row) {
					$print_export_items = $this->load->view('finance/invoice/print_price_table', array('invoice_data' => $invoice_data, 'item_row' => $item_row), true);
					$vars = array(
						'{{Price_Table}}' => $print_export_items,
					);
					$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
					$page_detail_html = $page_detail_row->detail;
					$pdf->AddPage(
						'P', //orientation
						'', //type
						'', //resetpagenum
						'', //pagenumstyle
						'', //suppress
						$margin_left, //margin-left
						$margin_right, //margin-right
						$margin_top, //margin-top
						$margin_bottom, //margin-bottom
						0, //margin-header
						0 //margin-footer
					);
					$pdf->WriteHTML($page_detail_html);
				}
			}
		}
		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
		$pdfFilePath = "./uploads/invoice_" . $id . ".pdf";
		$pdf->Output($pdfFilePath, "F");
		$this->db->where('id', $id);
		$this->db->update('invoices', array('pdf_url' => $pdfFilePath));
		redirect(base_url() . 'mail-system3/document-mail/invoice/' . $id);
	}

	function get_proforma_invoice_detail($id)
	{
		$select = "sales.sales,currency.currency,quo.quotation_no,pi.id as proforma_invoice_id,pi.*,party.party_code,party.party_name,party.address,party.email_id,party.phone_no as p_phone_no,party.fax_no,city.city,state.state,country.country,";
		$select .= "party.gst_no as p_gst_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required";
		$this->db->select($select);
		$this->db->from('proforma_invoices pi');
		$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = pi.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.party_id = pi.sales_to_party_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = pi.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = pi.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = pi.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = pi.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = pi.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = pi.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = pi.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = pi.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = pi.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = pi.for_required_id', 'left');
		$this->db->where('pi.id', $id);
		$this->db->where('party.active !=', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_invoice_detail($id)
	{
		$select = "sales.sales,currency.currency,quo.quotation_no,pi.id as invoice_id,pi.*,party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,";
		$select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.phone_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required,invoice_type.invoice_type";
		$this->db->select($select);
		$this->db->from('invoices pi');
		$this->db->join('sales_order so', 'so.id = pi.sales_order_id', 'left');
		$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = pi.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.party_id = pi.sales_to_party_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = pi.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = pi.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = pi.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = pi.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = pi.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = pi.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = pi.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = pi.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = pi.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = pi.for_required_id', 'left');
		$this->db->join('invoice_type', 'invoice_type.id = pi.invoice_type_id', 'left');
		$this->db->where('pi.id', $id);
		$this->db->where('party.active !=', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_company_detail()
	{
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id', COMPANY_ID);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row_array(0);
		} else {
			return array();
		}
	}

	function get_invoice_items($id, $return_type = 'array')
	{
		$this->db->select('pii.*, items.hsn_code');
		$this->db->from('invoice_items pii');
		$this->db->join('items','items.id = pii.item_id');
		$this->db->where('pii.invoice_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			if ($return_type == 'array') {
				return $query->result_array();
			} else {
				return $query->result();
			}
		} else {
			return array();
		}
	}

	/**
	 * @param $id
	 */
	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table, array($id_name => $id));
	}

	/**
	 * @param $id
	 */
	function delete_invoice($id)
	{
		$party_id = $this->crud->get_id_by_val('invoices', 'sales_to_party_id', 'id', $id);
		$this->applib->update_outstanding_payment($party_id);
		$this->crud->delete('invoices', array('id' => $id));
		$this->crud->delete('invoice_items', array('invoice_id' => $id));
		$this->crud->delete('invoice_billing_terms', array('invoice_id' => $id));
		$this->crud->delete('invoice_cal_code_definition', array('invoice_id' => $id));
		echo json_encode(array('success' => true, 'message' => "Invoice deleted successfully!."));
		exit();
	}

	function ledger()
	{
		$data['party_list'] = $this->crud->get_all_with_where('party', 'party_name', 'ASC', array('active !=' => 0));
		if($this->app_model->have_access_role(LEDGER_MODULE_ID, "view")){
			set_page('finance/ledger', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}

	}
	
	function get_sales_order_detail($id)
	{
		$select = "sales.sales,currency.currency,quo.quotation_no,so.id as sales_order_id,so.*,party.gst_no AS p_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.w_delivery_party_name AS delivery_party_name,party.w_address AS delivery_address,party.w_city AS delivery_city,party.w_state AS delivery_state,party.w_country AS delivery_country,party.w_phone1 AS delivery_contact_no,party.w_email AS delivery_email_id,party.oldw_pincode AS delivery_oldw_pincode,party.address_work,sob.address,sob.phone_no,city.city,state.state,country.country,";
		$select .= "sob.tin_vat_no,sob.tin_cst_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.phone_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required";
		$this->db->select($select);
		$this->db->from('sales_order so');
		$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = so.sales_id', 'left');
		$this->db->join('sales_order_buyers sob', 'sob.sales_order_id = so.id', 'left');
		$this->db->join('party party', 'party.party_id = sob.party_id', 'left');
		$this->db->join('contact_person cp', 'cp.party_id = so.sales_to_party_id', 'left');
		$this->db->join('city', 'city.city_id = sob.city_id', 'left');
		$this->db->join('state', 'state.state_id = sob.state_id', 'left');
		$this->db->join('country', 'country.country_id = sob.country_id', 'left');
		$this->db->join('currency', 'currency.id = so.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = so.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = so.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = so.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = so.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = so.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = so.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = so.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = so.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = so.for_required_id', 'left');
		$this->db->where('so.id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            //echo '<pre>';print_r($query->row(0));exit;
			return $query->row(0);
		} else {
			return array();
		}
	}
	
	function payment_reminder($id = ''){
		$customer_reminder = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$customer_reminder = $this->crud->get_all_with_where('customer_reminder_sent', 'id', 'ASC', $where_array);
		}
		$data = array();
		$data['id'] = ($customer_reminder) ? $customer_reminder[0]->id : '';
		$data['customer_reminder_id'] = ($customer_reminder) ? $customer_reminder[0]->customer_reminder_id : '';
		$data['invoice_id'] = ($customer_reminder) ? $customer_reminder[0]->invoice_id : '';
		$data['party_id'] = ($customer_reminder) ? $customer_reminder[0]->party_id : '';
		$data['subject'] = ($customer_reminder) ? $customer_reminder[0]->subject : '';
		$data['content_data'] = ($customer_reminder) ? $customer_reminder[0]->content_data : '';
		$data['letter_no'] = ($customer_reminder) ? $this->applib->get_reminder_letter_no($customer_reminder[0]->letter_no) : '';
		$data['letter_validity'] = ($customer_reminder) ? $customer_reminder[0]->letter_validity : '';
		$data['invoice_data'] = '';
		if($customer_reminder){
			$data['invoice_data'] = $this->get_invoice_detail($data['invoice_id']);
			$sales_order_data = $this->get_sales_order_detail($data['invoice_data']->sales_order_id);
			if($sales_order_data->sales_order_date != '1970-01-01' && $sales_order_data->sales_order_date != ''){
				$data['invoice_data']->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
			}
			if($sales_order_data->po_date != '1970-01-01' && $sales_order_data->po_date != ''){
				$data['invoice_data']->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
			}
			$data['invoice_data']->quotation_no = $sales_order_data->quotation_no;
			$data['invoice_data']->sales_order_no = $sales_order_data->sales_order_no;
			$data['invoice_data']->cust_po_no = $sales_order_data->cust_po_no;
			$data['invoice_data']->po_date = $sales_order_data->po_date;
			$data['invoice_data']->sales_order_pref = $sales_order_data->sales_order_pref;
		}
		set_page('reminder/customer_reminder',$data);
	}
    
    function finance_balance_payment($id = ''){
        $this->payment_reminder($id);
    }
    
    function general_notice($id = ''){
        $this->payment_reminder($id);
    }
    
    function finance_reminder_list(){
        set_page('reminder/customer_reminder_list');
    }
    
    
    
}