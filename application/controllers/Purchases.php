<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 * @property M_pdf $m_pdf
 */
class Purchases extends CI_Controller
{

	protected $staff_id = 1;

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	/**
	 *
	 */
	function index(){
		$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
		redirect("/");
	}
	function index_1()
	{
		$role_view = $this->app_model->have_access_role(OPPORTUNITIES_LEAD_MODULE_ID, "view");
		$role_add = $this->app_model->have_access_role(OPPORTUNITIES_LEAD_MODULE_ID, "add");
		$role_edit = $this->app_model->have_access_role(OPPORTUNITIES_LEAD_MODULE_ID, "edit");
		if ($role_view && $role_add && $role_edit) {
			$data = array(
				'lead_owner' => $this->crud->get_all_with_where('staff','staff_id','asc',array('active !='=>0)),
				'lead_provider' => $this->crud->get_select_data('lead_provider'),
				'industry_type' => $this->crud->get_select_data('industry_type'),
				'priority' => $this->crud->get_select_data('priority'),
				'lead_source' => $this->crud->get_select_data('lead_source'),
				'lead_stage' => $this->crud->get_select_data('lead_stage'),
				'lead_status' => $this->crud->get_select_data('lead_status'),
				'city' => $this->crud->get_select_data('city'),
				'party_code' => $this->crud->load_party_with_cnt_person_2(),
				'party_type_1' => $this->crud->get_select_data('party_type_1'),
				'party_type_2' => $this->crud->get_select_data('party_type_2'),
				'city' => $this->crud->get_select_data('city'),
				'state' => $this->crud->get_select_data('state'),
				'country' => $this->crud->get_select_data('country'),
				'lead_no' => $this->crud->get_next_autoincrement('lead_details')
				/*
                'order_stage' => $this->crud->get_select_data(''),
                'purchases' => $this->crud->get_select_data(''),
                'party' => $this->crud->get_select_data(''),
                'party_type' => $this->crud->get_select_data(''),
                'purchases_type' => $this->crud->get_select_data(''),
                'uom' => $this->crud->get_select_data(''),
                'branch1' => $this->crud->get_select_data(''),
                'branch' => $this->crud->get_select_data(''),
                'status' => $this->crud->get_select_data(''),
                'inquiry_stage' => $this->crud->get_select_data(''),
                'item_statys' => $this->crud->get_select_data(''),
                'currancy' => $this->crud->get_select_data(''),
                'project' => $this->crud->get_select_data(''),
                'quatation_stage' => $this->crud->get_select_data(''),
                'quatation_type' => $this->crud->get_select_data('')*/
			);
			set_page('purchases/opportunities_leads', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
	/**
	 * @param $purchases_order_id
	 */
	function add_purchases_order_item($purchases_order_id)
	{
		$item_id = $_POST['item_id'];
		if ($item_id != '') {
			$tempArr = $this->db->select('*')->from('purchase_items')->where('id', $item_id)->get()->result_array();
			if (count($tempArr) == 0) {
				echo json_encode(array('success' => 'false', 'message' => 'Item code not exists in item master!'));
				exit();
			}
			$item_data = $_POST;
			$item_data['purchases_order_id'] = $purchases_order_id;
			$item_data['created_at'] = date('Y-m-d H:i:s');
			$this->crud->insert('purchases_order_items', $item_data);
			$purchases_order_item_id = $this->db->insert_id();
			echo json_encode(array('success' => true, 'message' => 'Item added successfully!', 'purchases_order_item_id' => $purchases_order_item_id,));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}

	function update_purchases_order_status_to_approved()
	{
		$quotation_id = $_POST['quotation_id'];
		if ($quotation_id != '') {
			$this->db->where('quotation_id', $quotation_id);
			$this->db->update('purchases_order', array('isApproved' => 1));
			echo json_encode(array('success' => true, 'message' => 'Order approved successfully!'));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}

	function update_purchases_order_status_to_disapproved()
	{
		$quotation_id = $_POST['quotation_id'];
		if ($quotation_id != '') {
			$this->db->where('quotation_id', $quotation_id);
			$this->db->update('purchases_order', array('isApproved' => 0));
			echo json_encode(array('success' => true, 'message' => 'Order disapproved successfully!'));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}

	/**
	 * @param $purchases_order_item_id
	 */
	function edit_purchases_order_item($purchases_order_item_id)
	{
		$item_id = $_POST['item_id'];
		if ($item_id != '') {
			$tempArr = $this->db->select('*')->from('purchase_items')->where('id', $item_id)->get()->result_array();
			if (count($tempArr) == 0) {
				echo json_encode(array('success' => 'false', 'message' => 'Item code not exists in item master!'));
				exit();
			}
			$item_data = $_POST;
			$this->db->where('id', $purchases_order_item_id);
			$this->db->update('purchases_order_items', $item_data);
			echo json_encode(array('success' => true, 'message' => 'Item ced successfully!'));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}

	
	function delete_invoice($id)
	{
		$role_delete = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "delete");
		if ($role_delete) {
			$where_array = array("invoice_id" => $id);
			$this->crud->delete("purchase_invoice", $where_array);
			$this->crud->delete('purchase_invoice_items', array('invoice_id' => $id));
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this.');
			redirect("/");
		}
	}

	function delete_quotation_item($id)
	{
		//$id = $this->input->post("id");
		$where_array = array("id" => $id);
		$this->crud->delete("quotation_items", $where_array);
		echo $id;
		exit;
	}

	function delete_followup_history($id)
	{
		$where_array = array("id" => $id);
		$this->crud->delete("followup_history", $where_array);
		echo json_encode(array('status' => true, 'message' => "Follow Up Deleted!"));
		exit();
	}

	function delete_enquiry_followup_history($id)
	{
		$where_array = array("id" => $id);
		$this->crud->delete("inquiry_followup_history", $where_array);
		echo json_encode(array('status' => true, 'message' => "Follow Up Deleted!"));
		exit();
	}

	function update_inquiry_item_for_quotation_bfore_create_delete($id)
	{
		//$id = $this->input->post("id");
		$where_array = array("id" => $id);
		$this->crud->update('inquiry_items', array('quotation_flag' => 1), $where_array);
		echo $id;
		exit;
	}

	function delete_purchases_order_item($id)
	{
		//$id = $this->input->post("id");
		$where_array = array("id" => $id);
		$this->crud->delete("purchases_order_items", $where_array);
		exit;
	}

	function delete_invoice_item()
	{
		$id = $this->input->post("id");

		$status = 1;
		$msg = "Item has been deleted !";
		$where_array = array("id" => $id);
		$this->crud->delete("purchase_invoice_items", $where_array);
		echo json_encode(array("status" => $status, "msg" => $msg));
		exit;
	}

	/**
	 * Save Lead New
	 */
	function save_lead()
	{
		// echo "<pre>";print_r($_POST);exit;
		if (isset($_POST['lead_id'])) {
			$result = $this->check_party_validations_quatation($_POST['lead_data']['party_id'], $_POST['party_data']);
			if ($result['status'] == 0) {
				echo json_encode(array('status' => false, 'msg' => $result['msg']));
				exit();
			}
			$_POST['lead_data']['start_date'] = date("Y-m-d", strtotime($_POST['lead_data']['start_date']));
			$_POST['lead_data']['review_date'] = date("Y-m-d", strtotime($_POST['lead_data']['review_date']));
			$_POST['lead_data']['industry_type_id'] = !empty($_POST['lead_data']['industry_type_id']) ? $_POST['lead_data']['industry_type_id'] : NULL;
			$_POST['lead_data']['lead_owner_id'] = !empty($_POST['lead_data']['lead_owner_id']) ? $_POST['lead_data']['lead_owner_id'] : NULL;
			$_POST['lead_data']['assigned_to_id'] = !empty($_POST['lead_data']['assigned_to_id']) ? $_POST['lead_data']['assigned_to_id'] : NULL;
			$_POST['lead_data']['priority_id'] = !empty($_POST['lead_data']['priority_id']) ? $_POST['lead_data']['priority_id'] : NULL;
			$_POST['lead_data']['lead_source_id'] = !empty($_POST['lead_data']['lead_source_id']) ? $_POST['lead_data']['lead_source_id'] : NULL;
			$_POST['lead_data']['lead_provider_id'] = !empty($_POST['lead_data']['lead_provider_id']) ? $_POST['lead_data']['lead_provider_id'] : NULL;
			$_POST['lead_data']['lead_status_id'] = !empty($_POST['lead_data']['lead_status_id']) ? $_POST['lead_data']['lead_status_id'] : NULL;
			if ($_POST['lead_id'] == 0) {
				$_POST['lead_data']['created_by'] = $this->session->userdata('is_logged_in')['staff_id'];
				$_POST['lead_data']['created_at'] = date('Y-m-d H:i:s');
				$lead_id = $this->crud->insert('lead_details', $_POST['lead_data']);
				$this->crud->update('lead_details', array('lead_no' => $lead_id), array('id' => $lead_id));
			} else {
				$this->db->select('id');
				$this->db->from('lead_status');
				$this->db->where('lead_status', 'open');
				$this->db->limit(1);
				$lead_status_query = $this->db->get();
				if ($lead_status_query->num_rows() > 0) {
					$open_status_id = $lead_status_query->row()->id;
					if ($_POST['lead_data']['lead_status_id'] != $open_status_id) { //Check Lead status is close or achieved
						$this->db->select('lead_status_id');
						$this->db->from('lead_details');
						$this->db->where('id', $_POST['lead_id']);
						$this->db->limit(1);
						$lead_status_id_query = $this->db->get();
						if ($lead_status_id_query->num_rows() > 0) {
							// Check lead status is changed or not
							if ($lead_status_id_query->row()->lead_status_id != $_POST['lead_data']['lead_status_id']) {
								$_POST['lead_data']['end_date'] = date("Y-m-d");
							}
						}
					} else {
						$_POST['lead_data']['end_date'] = null;
					}
				}
				$this->crud->update('lead_details', $_POST['lead_data'], array('id' => $_POST['lead_id']));
				$lead_id = $_POST['lead_id'];
			}
			if ($_POST['lead_data']['party_id'] != 0) {
				$this->crud->update('party', $_POST['party_data'], array('party_id' => $_POST['lead_data']['party_id']));
			}
			echo json_encode(array("status" => true, "msg" => "Lead successfully saved!", "lead_id" => $lead_id));
			exit;
		} else {
			echo json_encode(array("status" => false, "msg" => "Something wrong."));
			exit;
		}
	}

	/**
	 *    Invoice
	 */
	function add_invoice(){
		$data = array( 'invoice_no' => $this->crud->get_next_autoincrement('purchase_invoice') );
		if($this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"add")) {
			set_page('purchases/add_invoice', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	/**
	 * Purchase Order DataTable
	 */
	function purchase_order_datatable(){
		$config['table'] = 'purchase_order e';
		$config['select'] = 'e.order_id, e.order_no, p.supplier_name,e.order_date,e.created_at';
		$config['column_order'] = array(null, 'e.order_no', 'p.supplier_name', 'e.order_date','p.created_by');
		$config['column_search'] = array('e.order_no', 'p.supplier_name', 'e.order_date');
		$config['joins'][1] = array('join_table' => 'supplier p', 'join_by' => 'p.supplier_id = e.supplier_id', 'join_type' => 'left');
		$config['order'] = array('e.order_id' => 'desc');
        $config['where_string'] = '1 = 1';
        $user_id = $this->input->get_post('user_id');
		if ($user_id != '' && $user_id != 'all') {
			$config['where_string'] .= " AND e.created_by = '" . $user_id . "' ";
		}
		if(isset($_POST['request_from']) && !empty($_POST['request_from'])){
			if($_POST['request_from'] == 'add_invoice'){
				$config['custom_where'] = "e.order_id NOT IN(SELECT order_id FROM purchase_invoice)";
			}	
		}
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		//echo $this->db->last_query();exit;
		$data = array();
		$role_delete = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "edit");

		foreach ($list as $order) {
			$row = array();
			$row[] = "<a href='javascript:void(0);' class='btn-feed-order-data' data-order_id='".$order->order_id."'>".$order->order_no."</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-order-data' data-order_id='".$order->order_id."'>".$order->supplier_name."</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-order-data' data-order_id='".$order->order_id."'>".date('d-m-Y', strtotime($order->order_date))."</a>";
			$row[] = "<a href='javascript:void(0);' class='btn-feed-order-data' data-order_id='".$order->order_id."'>".date('d-m-Y', strtotime($order->created_at))."</a>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
	function get_order_detail()
	{
		$order_id = $this->input->get_post("id");
		$order_data = $this->get_order_by_id($order_id);
		$order_items = $this->get_order_items($order_id);
		$data = array(
			'order_data' => $order_data,
			'order_items' => $order_items
		);
		echo json_encode($data);
	}

	function check_supplier_validations($supplier_id)
	{
		$supplier_data = $this->input->post('supplier');
		$phone_no = isset($supplier_data['phone_no']) ? $supplier_data['phone_no'] : '';
		$email_id = isset($supplier_data['email_id']) ? $supplier_data['email_id'] : '';
		$supplier_name = isset($supplier_data['supplier_name']) ? $supplier_data['supplier_name'] : '';
		$address = isset($supplier_data['address']) ? $supplier_data['address'] : '';
		$city_id = isset($supplier_data['city_id']) ? $supplier_data['city_id'] : '';
		$state_id = isset($supplier_data['state_id']) ? $supplier_data['state_id'] : '';
		$country_id = isset($supplier_data['country_id']) ? $supplier_data['country_id'] : '';
		$pincode = isset($supplier_data['pincode']) ? $supplier_data['pincode'] : '';
		
		$returnStatus = 1;
		$returnMsg = "";

		$temp = $email_id;
		$emails = explode(",", $temp);
		$email_status = 1;
		$email_msg = "";

		$email_ids = array();
		// multiple email validation
		if (is_array($emails) && count($emails) > 0) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$email_msg .= "$email is not valid email.&nbsp;";
						$email_status = 0;
					}
					$this->db->select('*');
					$this->db->from('supplier');
					$this->db->where('supplier_id !=' , $supplier_data['supplier_id']);
					$this->db->where("FIND_IN_SET( '".$email."', email_id) ");
					$res = $this->db->get();
					$rows = $res->num_rows();
					if ($rows > 0) {
						$result = $res->result();
 						$email_msg .= "$email email already exist.&nbsp; <br/> Original supplier : " . $result[0]->supplier_name;
						$email_status = 0;
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}

		if ($email_status == 0) {
			return array('status' => 0, 'msg' => $email_msg);
		}

		$supplierName = strtolower(trim($supplier_name));

		if ($supplierName == "") {
			return array('status' => 0, 'msg' => "Customer name is required.");
		}

		// supplier name unique validatoin
		$sql_list_query = "SELECT * FROM supplier WHERE TRIM(LOWER(supplier_name)) = '" . addslashes($supplierName) . "' AND supplier_id != $supplier_id";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		if (count($rows) > 0) {
			return array('status' => 0, 'msg' => "supplier name already exist!");
		}

		if (trim($phone_no) == "" && count($email_ids) == 0) {
			$email_msg = "Please enter phone number or email id !";
			return array('status' => 0, 'msg' => $email_msg);
		}

		// get phone numbers
		$temp = $phone_no;
		$phone_msg = '';
		$phone_numbers = explode(",", $temp);
		$phone_status = 1;
		$phone_nos = array();
		foreach ($phone_numbers as $phone) {
			$phone = trim($phone);
			$this->db->select('*');
			$this->db->from('supplier');
			$this->db->where('supplier_id !=' ,$supplier_data['supplier_id']);
			$this->db->where("FIND_IN_SET( '".$phone."', phone_no) ");
			$res = $this->db->get();
			$rows = $res->num_rows();
			//echo $phone.'<br>';echo $this->db->last_query().'<br>';
			if ($rows > 0 && trim($phone) != "") {
				$result = $res->result();
				$phone_msg .= "$phone mobile already exist.&nbsp; &nbsp; <br/> Original supplier : ". $result[0]->supplier_name;
				$phone_status = 0;
			}
		}
		if($phone_status == 0){
			echo json_encode(array("status" => 0, 'msg' => $phone_msg, 'phone_error' => 1));
			exit;
		}
		$phone_numbers = $temp;
		$email_ids = implode(',', $email_ids);


		$supplier_data = array(
			'phone_no' => $phone_numbers,
			'email_id' => $email_ids,
			'address' => $address,
			'city_id' => $city_id,
			'state_id' => $state_id,
			'country_id' => $country_id,
			'pincode' => $pincode,
		);

		if ($supplier_name != "")
			$supplier_data['supplier_name'] = $supplier_name;


		$this->db->where('supplier_id', $supplier_id);
		$this->db->update('supplier', $supplier_data);

		return array('status' => $returnStatus, 'msg' => $returnMsg);
	}

	function check_party_validations_quatation($party_id, $party_data)
	{
		// echo "<pre>";print_r($_POST);exit;
		// $party_data = $this->input->post('party');
		$party_code = isset($party_data['party_code']) ? $party_data['party_code'] : '';
		$phone_no = isset($party_data['phone_no']) ? $party_data['phone_no'] : '';
		$email_id = isset($party_data['email_id']) ? $party_data['email_id'] : '';
		$party_name = isset($party_data['party_name']) ? $party_data['party_name'] : '';
		$address = isset($party_data['address']) ? $party_data['address'] : '';
		$city_id = isset($party_data['city_id']) ? $party_data['city_id'] : '';
		$state_id = isset($party_data['state_id']) ? $party_data['state_id'] : '';
		$country_id = isset($party_data['country_id']) ? $party_data['country_id'] : '';
		$fax_no = isset($party_data['fax_no']) ? $party_data['fax_no'] : '';
		$pincode = isset($party_data['pincode']) ? $party_data['pincode'] : '';
		$website = isset($party_data['website']) ? $party_data['website'] : '';
		$project = isset($party_data['project']) ? $party_data['project'] : '';
		$reference_id = isset($party_data['reference_id']) ? $party_data['reference_id'] : 0;
		$agent_id = (int) isset($party_data['agent_id']) ? $party_data['agent_id'] : 0;
		$reference_description = isset($party_data['reference_description']) ? $party_data['reference_description'] : '';

		/*if ($reference_id == 0) {
			return array('status' => 0, 'msg' => 'please select reference.');
		}*/

		$returnStatus = 1;
		$returnMsg = "";

		$temp = nl2br($email_id);
		$emails = explode("<br />", $temp);

		$email_status = 1;
		$email_msg = "";

		$email_ids = array();
		// multiple email validation
		if (is_array($emails) && count($emails) > 0) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$email_msg .= "$email is not valid email.&nbsp;";
						$email_status = 0;
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}

		if ($email_status == 0) {
			return array('status' => 0, 'msg' => $email_msg);
		}

		$partyName = strtolower(trim($party_name));

		if ($partyName == "") {
			return array('status' => 0, 'msg' => "Customer name is required.");
		}

		// party name unique validation
		$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($partyName) . "' AND phone_no='".$phone_no."' AND party_id != $party_id";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		if (count($rows) > 0) {
			return array('status' => 0, 'msg' => "Party name already exist!");
		}

		if (trim($phone_no) == "" && count($email_ids) == 0) {
			$email_msg = "Please enter phone number or email id !";
			return array('status' => 0, 'msg' => $email_msg);
		}

		$temp = nl2br($phone_no);
		$phone_numbers = explode("<br />", $temp);
		$phone_numbers1 = "";
		foreach ($phone_numbers as $phone) {
			if (trim($phone) != "")
				$phone_numbers1 .= $phone . ",";
		}
		$phone_numbers = $phone_numbers1;

		$email_ids = implode(',', $email_ids);

		if ($returnStatus == 1) {
			$update_party_data = array(
				'address' => $address,
				'pincode' => $pincode,
				'phone_no' => $phone_numbers,
				'fax_no' => $fax_no,
				'email_id' => $email_ids,
				'website' => $website,
				'city_id' => $city_id,
				'state_id' => $state_id,
				'country_id' => $country_id,
				'project' => $project,
				'reference_id' => $reference_id,
				'reference_description' => $reference_description,
				'agent_id' => $agent_id
			);

			$this->db->where('party_id', $party_id);
			$this->db->update('party', $update_party_data);
		}

		return array('status' => $returnStatus, 'msg' => $returnMsg);
	}

	function invoice_edit($invoice_id)
	{
		$role_edit = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "edit");
		if ($role_edit) {
			$invoice_data = $this->get_invoice_by_id($invoice_id);
			$invoice_items = $this->get_invoice_items($invoice_id);
			$data = array(
				'invoice_data' => $invoice_data,
				'invoice_items' => $invoice_items
			);
			if($this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"edit")) {
				set_page('purchases/invoice_edit', $data);
			}else{
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function get_invoice_by_id($invoice_id)
	{
		$this->db->select("i.*,sp.supplier_name as supplier_name, st.name as assigned_by");
		$this->db->from('purchase_invoice i');
		$this->db->join('staff st', 'st.staff_id = i.created_by', 'left');
		$this->db->join('supplier sp', 'sp.supplier_id = i.supplier_id', 'left');
		$this->db->where('i.invoice_id', $invoice_id);
		return $this->db->get()->row(0);
	}

	function get_invoice_items($invoice_id)
	{
		$this->db->select("is.*");
		$this->db->from('purchase_invoice_items is');
		$this->db->where('is.invoice_id', $invoice_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$item_data = array();
			foreach ($query->result_array() as $item) {
				$item_data_array = json_decode($item['item_data'], true);
				$item_data[] = array_merge(array('id' => $item['id']), $item_data_array);
			}
			return $item_data;
		} else {
			return array();
		}
	}

	/**
	 *  Create invoice
	 */
	function create_invoice()
	{
		//echo "<pre>";print_r($_POST);exit;
		$supplier_id = $_POST['invoice']['supplier_id'];

		if ($supplier_id == "" || $supplier_id == "0") {
			echo json_encode(array("success" => 'false', 'invoice_id' => $_POST['invoice']['invoice_id'], 'item_id' => 0, 'msg' => 'Please select supplier code first!'));
			exit;
		}

		// check supplier validations
		$result = $this->check_supplier_validations($supplier_id);
		if ($result['status'] == 0) {
			echo json_encode(array("success" => 'false', 'invoice_id' => $_POST['invoice']['invoice_id'], 'item_id' => 0, 'msg' => $result['msg']));
			exit;
		}

		/*--------- Convert Date as Mysql Format -------------*/
		$_POST['invoice']['invoice_date'] = date("Y-m-d", strtotime(str_replace("/", "-", $_POST['invoice']['invoice_date'])));
		
		// Check Inquiry is new or old
		
		if (isset($_POST['invoice']['invoice_id']) && $_POST['invoice']['invoice_id'] == 0) { // New Inquiry

			$dataToInsert = $_POST['invoice'];
			$dataToInsert['created_at'] = date('Y-m-d H:i:s');
			$dataToInsert['created_by'] = $this->staff_id;
			// Unset invoice_id from Inquiry Data
			if (isset($dataToInsert['invoice_id']))
				unset($dataToInsert['invoice_id']);

			$this->db->insert('purchase_invoice', $dataToInsert);
			$invoice_id = $this->db->insert_id();
			$this->crud->update('purchase_invoice', array('invoice_no' => $invoice_id), array('invoice_id' => $invoice_id));

		} else { // Existing Inquiry
			/*if (!isset($_POST['invoice']['is_received_post'])) {
				$_POST['invoice']['is_received_post'] = 0;
			}
			if (!isset($_POST['invoice']['is_send_post'])) {
				$_POST['invoice']['is_send_post'] = 0;
			}
			if (!isset($_POST['invoice']['send_from_post'])) {
				$_POST['invoice']['send_from_post'] = 0;
			}*/

			$this->db->where('invoice_id', $_POST['invoice']['invoice_id']);
			$this->db->update('purchase_invoice', $_POST['invoice']);
			$invoice_id = $_POST['invoice']['invoice_id'];
		}

		$item_id = '';
		/*---------- Add Item If Available ------------*/
		if (empty($_POST['edit_item'])) {
			$item_id = $this->add_invoice_item($invoice_id);
		}

		/*------------ Return Response ----------------*/
		echo json_encode(array("success" => 'true', 'invoice_id' => $invoice_id, 'item_id' => $item_id));
		exit();
	}

	function get_lead_status_id_by_status($status = 'achieved')
	{
		return $this->crud->get_id_by_val('lead_status', 'id', 'lead_status', $status);
	}

	function invoice_list()
	{
		$sql_list_query = "SELECT e.invoice_id, e.invoice_no, p.supplier_name,p.supplier_id,e.invoice_date
							from purchase_invoice e
							LEFT JOIN supplier p ON p.supplier_id = e.supplier_id";

		$condition = ' WHERE 1';

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
		} else {
			//$condition .= ' AND ( p.`created_by`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.`created_by` IS NULL )';
			if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
			} else if ($cu_accessExport == 1) {
				//$condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
			} else if ($cu_accessDomestic == 1) {
				//$condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
			} else {
			}
		}
		$status = $this->input->get_post('status');
		$user_id = $this->input->get_post('user_id');
		if ($status != '' && $status != 'all') {
			//$condition .= " AND  s.inquiry_status = '" . $status . "' ";
		}

		if ($user_id != '' && $user_id != 'all') {
			$condition .= " AND  e.created_by = '" . $user_id . "' ";
		}

		$query = $this->db->query($sql_list_query . $condition);
		$invoices = $query->result_array();

		//$this->db->order_by('id');
		//$enquiry_status = $this->db->get('inquiry_status')->result();

		$this->db->order_by('name');
		$users = $this->db->where('active !=',0)->get('staff')->result();
		if($this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"view")) {
			set_page('purchases/invoice_list', array('invoices' => $invoices, 'users' => $users));
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function invoices_datatable()
	{
		$requestData = $_REQUEST;
		
		$config['table'] = 'purchase_invoice e';
		$config['select'] = 'e.invoice_id, e.invoice_no, p.supplier_name,e.invoice_date,e.created_at';
		$config['column_order'] = array(null, 'e.invoice_no', 'p.supplier_name', 'e.invoice_date',);
		$config['column_search'] = array('e.invoice_no', 'p.supplier_name', 'e.invoice_date');
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$config['likes'][] = array('e.invoice_no' => $requestData['columns'][1]['search']['value']);
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$config['likes'][] = array('p.supplier_name' => $requestData['columns'][2]['search']['value']);
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			$config['likes'][] = array('e.invoice_date' => $requestData['columns'][3]['search']['value']);
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {
			$config['likes'][] = array('e.created_at' => $requestData['columns'][4]['search']['value']);
		}
		
		$config['joins'][1] = array('join_table' => 'supplier p', 'join_by' => 'p.supplier_id = e.supplier_id', 'join_type' => 'left');
		$config['order'] = array('e.invoice_id' => 'desc');
		$config['where_string'] = ' 1 = 1 ';

		$user_id = $this->input->get_post('user_id');
		if ($user_id != '' && $user_id != 'all') {
			$config['where_string'] .= " AND e.created_by = '" . $user_id . "' ";
		}
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		$role_delete = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "edit");

		foreach ($list as $invoice) {
			$row = array();
			$action = '';
			if ($role_edit && $role_delete) {
				$action .= '<a href="' . BASE_URL . "purchases/invoice-edit/" . $invoice->invoice_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				//$action .= ' | <a onclick="return confirm(\'Are you sure ?\');"  href="' . BASE_URL . "purchases/delete_invoice/" . $invoice->invoice_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('purchases/delete_invoice/' . $invoice->invoice_id) . '"><i class="fa fa-trash"></i></a>';
			} elseif ($role_edit) {
				$action .= '<a href="' . BASE_URL . "purchases/invoice-edit/" . $invoice->invoice_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			} elseif ($role_delete) {
				//$action .= '<a onclick="return confirm(\'Are you sure ?\');"  href="' . BASE_URL . "purchases/delete_invoice/" . $invoice->invoice_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('purchases/delete_invoice/' . $invoice->invoice_id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = '<a href="' . base_url('purchases/invoice-edit/' . $invoice->invoice_id . '?view') . '" >'.$invoice->invoice_id.'</a>';
			$row[] = '<a href="' . base_url('purchases/invoice-edit/' . $invoice->invoice_id . '?view') . '" >'.$invoice->supplier_name.'</a>';
			$row[] = '<a href="' . base_url('purchases/invoice-edit/' . $invoice->invoice_id . '?view') . '" >'.date('d-m-Y', strtotime($invoice->invoice_date)).'</a>';
			$row[] = '<a href="' . base_url('purchases/invoice-edit/' . $invoice->invoice_id . '?view') . '" >'.date('d-m-Y', strtotime($invoice->created_at)).'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

		/*$this->db->select('e.invoice_id, e.inquiry_no, s.inquiry_status,p.party_name,p.party_code,e.inquiry_date');
		$this->db->from('inquiry e');
		$this->db->join('inquiry_status s', 's.id = e.inquiry_status_id', 'left');
		$this->db->join('party p', 'p.party_id = e.party_id', 'left');

		$accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
		$accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
		if ($accessExport == 1 && $accessDomestic == 1) {

		} else if ($accessExport == 1) {
			$this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
		} else if ($accessDomestic == 1) {
			$this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
		}
		$status = $this->input->get_post('status');
		$user_id = $this->input->get_post('user_id');
		if ($status != '') {
			$this->db->where('s.inquiry_status', $status);
		}

		if ($user_id != '') {
			$this->db->where('e.created_by', $user_id);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {

		}*/
	}
	
	/**
	 * @param $inquiry_id
	 * @return bool|int
	 */
	function add_invoice_item($invoice_id)
	{
		if(isset($_POST['pick_order']) && $_POST['pick_order'] == 'pick_order' ){
			$_POST['item_data'] = $_POST['item_data'][0];
		}
		$item_code = isset($_POST['item_data']['item_id']) ? $_POST['item_data']['item_id'] : '';
		if ($item_code != '') {

			// check unique validation
			$tempArr = $this->db->select('*')->from('purchase_items')->where('id', $item_code)->get()->result_array();

			if (count($tempArr) == 0) {
				if ($this->uri->segment(2) == "add-invoice-item") {
					echo json_encode(array('success' => 'false', 'msg' => 'Item code not exists in item master!'));
					exit();
				} else {
					return false;
				}
			}

			$item_data['invoice_id'] = $invoice_id;
			$item_data['created_at'] = date('Y-m-d H:i:s');
			$_POST['item_data'] = str_replace(',', '', $_POST['item_data']);
			$item_data['item_data'] = json_encode($_POST['item_data']);
			$item_data['item_id'] = $_POST['item_data']['item_id'];
			
			//echo "<pre>";print_r($_POST);exit;

			$this->db->insert('purchase_invoice_items', $item_data);
			if ($this->uri->segment(2) == "add-invoice-item") {
				echo json_encode(array(
						'success' => 'true',
						'invoice_id' => $invoice_id,
						'item_id' => $this->db->insert_id()
					)
				);
				exit();
			} else {
				return $this->db->insert_id();
			}
		} else {
			if ($this->uri->segment(2) == "add-invoice-item") {
				echo json_encode(array('success' => 'false', 'msg' => 'Please select item!'));
				exit();
			} else {
				return 'false';
			}
		}
	}

	/**
	 * @param $invoice_item_id
	 * @return int|string
	 */
	function edit_invoice_item($invoice_item_id)
	{

		$item_code = $_POST['item_data']['item_id'];
		if ($item_code != '') {

			// check unique validation
			$tempArr = $this->db->select('*')->from('purchase_items')->where('id', $item_code)->get()->result_array();

			if (count($tempArr) == 0) {
				if ($this->uri->segment(2) == "add-invoice-item") {
					echo json_encode(array('success' => 'false', 'msg' => 'Item code not exists in item master!'));
					exit();
				} else {
					return false;
				}
			}
			$_POST['item_data'] = str_replace(',', '', $_POST['item_data']);
			$item_data['item_data'] = json_encode($_POST['item_data']);
			$item_data['item_id'] = $_POST['item_data']['item_id'];
			$item_data['quantity'] = $_POST['item_data']['quantity'];
			
			$this->db->where('id', $invoice_item_id);
			$this->db->update('purchase_invoice_items', $item_data);
			echo json_encode(array(
					'success' => 'true',
					'item_id' => $invoice_item_id
				)
			);
			exit();
		} else {
			echo json_encode(array('success' => 'false', 'msg' => 'Please select item!'));
			exit();
		}
	}

	function get_inquiry()
	{
		$customer_code = "";
		$id = $this->input->get_post("id");
		$party = array();

		$status = 1;
		$msg = "OK";

		$itemsHTML = '';

		$sql = "
		SELECT 
			i.*,p.party_name as party_name, p.party_code as party_code,s.purchases
		FROM 
			inquiry i 
		LEFT JOIN party p ON i.party_id = p.party_id
		LEFT JOIN purchases s ON s.id = i.purchases_id
		WHERE i.inquiry_id = '$id'
		order by i.inquiry_id ASC
		";

		$data = $this->crud->getFromSQL($sql);
		if (count($data) > 0) {
			$data = $data[0];
			$data->inquiry_date = date('d-m-Y', strtotime($data->inquiry_date));
			$party['customer_code'] = $data->party_code;
			$party['party_name'] = $data->party_name;

			$this->db->select('p.*,pt1.type,pt2.type,br.branch,ct.city,st.state,cnt.country');
			$this->db->from('party p');
			$this->db->join('party_type_1 pt1', 'pt1.id = p.party_type_1', 'left');
			$this->db->join('party_type_2 pt2', 'pt2.id = p.party_type_2', 'left');
			$this->db->join('branch br', 'br.branch_id = p.branch_id', 'left');
			$this->db->join('city ct', 'ct.city_id = p.city_id', 'left');
			$this->db->join('state st', 'st.state_id = p.state_id', 'left');
			$this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
			$this->db->where('p.party_id', $data->party_id);
			$this->db->where('p.active !=',0);
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$phone = trim($row->phone_no);
					$emails = $row->email_id;

					$party = array(
						'party_id' => $row->party_id,
						'agent_id' => $row->agent_id,
						'party_code' => $row->party_code,
						'party_name' => $row->party_name,
						'address' => $row->address,
						'city' => $row->city,
						'city_id' => $row->city_id,
						'state' => $row->state,
						'state_id' => $row->state_id,
						'country' => $row->country,
						'country_id' => $row->country_id,
						'fax_no' => $row->fax_no,
						'email_id' => $emails,
						'website' => $row->website,
						'pincode' => $row->pincode,
						'reference_id' => $row->reference_id,
						'reference_description' => $row->reference_description,
						'phone_no' => $phone,
						'project' => $row->project,
						'purchases_id' => $row->party_type_1,
						'purchases' => $this->crud->get_id_by_val('purchases', 'purchases', 'id', $row->party_type_1),
						'contact_persons_array' => $this->getContactPersonBySupplier($row->party_id)
					);

				}

				$KindAttnData = $this->getKindAttnParty($data->party_id);
				$party['contact_person_name'] = isset($KindAttnData['name']) ? $KindAttnData['name'] : '';
				$party['contact_person_contact_no'] = isset($KindAttnData['phone_no']) ? $KindAttnData['phone_no'] : '';
				$party['contact_person_email_id'] = isset($KindAttnData['email']) ? $KindAttnData['email'] : '';
			}
		}

		$itemsHTML = '';

		$where_item['inquiry_id'] = $id;
		$items = $this->crud->get_all_with_where('inquiry_items', '', '', $where_item);

		if (!empty($items)) {
			foreach ($items as $item) {
				$detail_id = $item->id;
				$temp = json_decode($item->item_data);
				$iStatus = isset($temp->item_status) ? $temp->item_status : '';
				$itemsHTML .= '
				<tr class="item-row pending" data-item_id="' . $detail_id . '">
					<td>
						<a href="javascript:void(0);" id="delete_item_before_' . $detail_id . '" class="btn btn-danger btn-xs btn-delete-item" data-item_id="' . $detail_id . '"><i class="fa fa-remove"></i>
						</a>
						<a href="javascript:void(0);" id="edit_item_before_' . $detail_id . '" class="btn btn-primary btn-xs btn-edit-item" data-id="' . $detail_id . '"><i class="fa fa-edit"></i>
						</a>
					</td>
					<td>
					' . $temp->item_code . '
					</td>
					<td>
					' . $temp->item_name . '
					</td>
					<td>
					' . $temp->quantity . '
					</td>											
					<td>
					' . $temp->item_rate . '
					</td>
					<td>
					' . $temp->quantity * $temp->item_rate . '
					</td>
					<td>
						0
					</td>
					<td>
					' . $temp->quantity * $temp->item_rate . '
					</td>
				</tr>
			';
			}
		}

		echo json_encode(
			array(
				"status" => $status, "msg" => $msg,
				'data' => $data, 'itemsHTML' => $itemsHTML,
				'party' => $party
			)
		);
		exit;
	}

	function get_purchases_order_detail($id)
	{
		$select = "po.*,sp.supplier_name as supplier_name,sp.phone_no, sp.gstin, sp.pincode ,sp.email_id, st.name as assigned_by, city.city,state.state,country.country,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('purchase_order po');
		$this->db->join('staff st', 'st.staff_id = po.created_by', 'left');
		$this->db->join('supplier sp', 'sp.supplier_id = po.supplier_id', 'left');
		$this->db->join('city', 'city.city_id = sp.city_id', 'left');
		$this->db->join('state', 'state.state_id = sp.state_id', 'left');
		$this->db->join('country', 'country.country_id = sp.country_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = po.created_by', 'left');
		$this->db->where('po.order_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_company_detail()
	{
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id', COMPANY_ID);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row_array(0);
		} else {
			return array();
		}
	}

	function get_purchases_order_items($id)
	{
		$this->db->select('poi.*');
		$this->db->from('purchase_order_items poi');
		//~ $this->db->join('uom u', 'u.id = poi.uom_id');
		//~ $this->db->where('ic.item_category', 'Finished Goods');
		$this->db->where('poi.order_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	/**
	 * @param $id
	 * @return array
	 */
	function get_purchases_order_sub_items($id)
	{
		$this->db->select('poi.*');
		$this->db->from('purchase_order_items poi');
		//~ $this->db->join('item_category ic', 'ic.id = poi.item_category_id');
		//~ $this->db->where('ic.item_category != ', 'Finished Goods');
		$this->db->where('poi.order_id', $id);
		$query = $this->db->get();
		$other_items = array();
		if ($query->num_rows() > 0) {
			$other_items = $query->result_array();
		}
		return $other_items;
	}

	function purchases_order_print($id)
	{
		$purchases_order_data = $this->get_purchases_order_detail($id);
		$company_details = $this->get_company_detail();
		$purchases_order_items = $this->get_purchases_order_items($id);
		$terms_and_conditions = $this->crud->get_id_by_val('terms_and_conditions','detail','module','purchse_order');
		$item_data_arr = array();
		if (!empty($purchases_order_items)) {
			foreach ($purchases_order_items as $key => $item_row) {
				$item_data_arr[$key] = json_decode($item_row['item_data']);
			}
		}
		$item_array = array();
		if (!empty($item_data_arr)) {
			foreach ($item_data_arr as $key => $item_row) {
				$item_row->uom = $this->crud->get_id_by_val('uom','uom','id', $item_row->uom_id);
				$item_row->hsn = $this->crud->get_id_by_val('purchase_items','hsn_code','id', $item_row->item_id);
                $item_row->product_code = $this->crud->get_id_by_val('purchase_items','product_code','id', $item_row->item_id);
			}
		}
		//echo '<pre>';print_r($item_data_arr);exit;
		$this->load->library('m_pdf');
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		if (!empty($purchases_order_items)) {
			foreach ($purchases_order_items as $key => $item_row) {
				$param = "'utf-8','A4'";
				$pdf = $this->m_pdf->load($param);
				$pdf->pagenumPrefix = 'Page ';
				$pdf->pagenumSuffix = ' - ';
				$pdf->nbpgPrefix = ' out of ';
				$pdf->nbpgSuffix = ' pages';
				$pdf->setFooter('{PAGENO}{nbpg}');
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				if ($key == 0) {
					$other_items = $this->get_purchases_order_sub_items($id);
					$html = $this->load->view('purchases/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'item_array' => $item_data_arr, 'terms_and_conditions' => $terms_and_conditions, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
				} else {
					$html = $this->load->view('purchases/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'item_array' => $item_data_arr, 'terms_and_conditions' => $terms_and_conditions, 'company_details' => $company_details, 'is_first_item' => false), true);
				}
				$pdf->WriteHTML($html);

				$this->db->select('*');
				$this->db->from('item_documents');
				$this->db->where('item_id', $item_row['item_id']);
				$this->db->where('document_type', strtolower($purchases_order_data->purchases) . '_order');
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $key => $page_detail_row) {
						$print_export_items = $this->load->view('purchases/order_print_price_table', array('purchases_order_data' => $purchases_order_data, 'item_row' => $item_row), true);
						$vars = array(
							'{{Price_Table}}' => $print_export_items,
						);
						$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
						$page_detail_html = $page_detail_row->detail;
						$pdf->AddPage(
							'P', //orientation
							'', //type
							'', //resetpagenum
							'', //pagenumstyle
							'', //suppress
							$margin_left, //margin-left
							$margin_right, //margin-right
							$margin_top, //margin-top
							$margin_bottom, //margin-bottom
							0, //margin-header
							0 //margin-footer
						);
						$pdf->WriteHTML($page_detail_html);
					}
				}
				$pdfFilePath = "purchases_order.pdf";
				$pdf->Output($pdfFilePath, "I");

			}
		} else {
			$html = $this->load->view('purchases/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => array(), 'company_details' => $company_details, 'is_first_item' => false), true);
			$param = "'utf-8','A4'";
			$pdf = $this->m_pdf->load($param);
			$pdf->AddPage(
				'P', //orientation
				'', //type
				'', //resetpagenum
				'', //pagenumstyle
				'', //suppress
				$margin_left, //margin-left
				$margin_right, //margin-right
				$margin_top, //margin-top
				$margin_bottom, //margin-bottom
				0, //margin-header
				0 //margin-footer
			);
			$pdf->WriteHTML($html);
			$pdfFilePath = "purchases_order.pdf";
			$pdf->Output($pdfFilePath, "I");
		}
	}
    
    function purchases_order_email($id){
		$purchases_order_data = $this->get_purchases_order_detail($id);
		$company_details = $this->get_company_detail();
		$purchases_order_items = $this->get_purchases_order_items($id);
		$terms_and_conditions = $this->crud->get_id_by_val('terms_and_conditions','detail','module','purchse_order');
		$item_data_arr = array();
		if (!empty($purchases_order_items)) {
			foreach ($purchases_order_items as $key => $item_row) {
				$item_data_arr[$key] = json_decode($item_row['item_data']);
			}
		}
		$item_array = array();
		if (!empty($item_data_arr)) {
			foreach ($item_data_arr as $key => $item_row) {
				$item_row->uom = $this->crud->get_id_by_val('uom','uom','id', $item_row->uom_id);
				$item_row->hsn = $this->crud->get_id_by_val('purchase_items','hsn_code','id', $item_row->item_id);
			}
		}
		//echo '<pre>';print_r($item_data_arr);exit;
		$this->load->library('m_pdf');
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		if (!empty($purchases_order_items)) {
			foreach ($purchases_order_items as $key => $item_row) {
				$param = "'utf-8','A4'";
				$pdf = $this->m_pdf->load($param);
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				if ($key == 0) {
					$other_items = $this->get_purchases_order_sub_items($id);
					$html = $this->load->view('purchases/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'item_array' => $item_data_arr, 'terms_and_conditions' => $terms_and_conditions, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
				} else {
					$html = $this->load->view('purchases/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'item_array' => $item_data_arr, 'terms_and_conditions' => $terms_and_conditions, 'company_details' => $company_details, 'is_first_item' => false), true);
				}
				$pdf->WriteHTML($html);

				$this->db->select('*');
				$this->db->from('item_documents');
				$this->db->where('item_id', $item_row['item_id']);
				$this->db->where('document_type', strtolower($purchases_order_data->purchases) . '_order');
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $key => $page_detail_row) {
						$print_export_items = $this->load->view('purchases/order_print_price_table', array('purchases_order_data' => $purchases_order_data, 'item_row' => $item_row), true);
						$vars = array(
							'{{Price_Table}}' => $print_export_items,
						);
						$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
						$page_detail_html = $page_detail_row->detail;
						$pdf->AddPage(
							'P', //orientation
							'', //type
							'', //resetpagenum
							'', //pagenumstyle
							'', //suppress
							$margin_left, //margin-left
							$margin_right, //margin-right
							$margin_top, //margin-top
							$margin_bottom, //margin-bottom
							0, //margin-header
							0 //margin-footer
						);
						$pdf->WriteHTML($page_detail_html);
					}
				}
				
			}
		}else{
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Purchases order items not found!');
			redirect(base_url('purchases/order_list'));	
		}
		$pdfFilePath = "./uploads/purchases_order_" . $id . ".pdf";
		$pdf->Output($pdfFilePath, "F");
		$this->db->where('order_id', $id);
		$this->db->update('purchase_order', array('pdf_url' => $pdfFilePath));
		redirect(base_url() . 'mail-system3/document-mail/purchases-order/' . $id);
	}
	
	function purchases_order_email_v1($id)
	{
		$select = "currency.currency,quo.quotation_no,so.id as purchases_order_id,so.*,sob.party_code,party.party_name,sob.address,sob.phone_no,city.city,state.state,country.country,";
		$select .= "sob.tin_vat_no,sob.tin_cst_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "purchases_order_pref.purchases_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required";
		$this->db->select($select);
		$this->db->from('purchases_order so');
		$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
		$this->db->join('purchases_order_buyers sob', 'sob.purchases_order_id = so.id', 'left');
		$this->db->join('party party', 'party.party_id = sob.party_id', 'left');
		$this->db->join('contact_person cp', 'cp.party_id = so.purchases_to_party_id', 'left');
		$this->db->join('city', 'city.city_id = sob.city_id', 'left');
		$this->db->join('state', 'state.state_id = sob.state_id', 'left');
		$this->db->join('country', 'country.country_id = sob.country_id', 'left');
		$this->db->join('currency', 'currency.id = so.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
		$this->db->join('purchases_order_pref', 'purchases_order_pref.id = so.purchases_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = so.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = so.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = so.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = so.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = so.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = so.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = so.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = so.for_required_id', 'left');
		$this->db->where('so.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$purchases_order_data = $query->row(0);
			$purchases_order_id = $purchases_order_data->purchases_order_id;
			$this->db->select('*');
			$this->db->from('company');
			$this->db->where('id', COMPANY_ID);
			$this->db->limit(1);
			$query = $this->db->get();
			$company_details = array();
			if ($query->num_rows() > 0) {
				$company_details = $query->row_array(0);
			}
			/*echo "<pre>";
			print_r($purchases_order_data);
			exit();*/
			$this->db->select('soi.*');
			$this->db->from('purchases_order_items soi');
			$this->db->join('item_category ic', 'ic.id = soi.item_category_id');
			$this->db->where('ic.item_category', 'Finished Goods');
			$this->db->where('soi.purchases_order_id', $purchases_order_id);
			$query = $this->db->get();
			$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
			$margin_left = $margin_company[0]->margin_left;
			$margin_right = $margin_company[0]->margin_right;
			$margin_top = $margin_company[0]->margin_top;
			$margin_bottom = $margin_company[0]->margin_bottom;
			if ($query->num_rows() > 0) {
				$files_urls = array();
				foreach ($query->result_array() as $key => $item_row) {
					$this->load->library('m_pdf');
					$param = "'utf-8','A4'";
					$pdf = $this->m_pdf->load($param);
					if ($key == 0) {

						$this->db->select('soi.*');
						$this->db->from('purchases_order_items soi');
						$this->db->join('item_category ic', 'ic.id = soi.item_category_id');
						$this->db->where('ic.item_category != ', 'Finished Goods');
						$this->db->where('soi.purchases_order_id', $purchases_order_id);
						$query = $this->db->get();
						$other_items = array();
						if ($query->num_rows() > 0) {
							$other_items = $query->result_array();
						}
						$html = $this->load->view('purchases/order/pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
					} else {
						$html = $this->load->view('purchases/order/pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'company_details' => $company_details, 'is_first_item' => false), true);
					}
					$pdf->AddPage(
						'P', //orientation
						'', //type
						'', //resetpagenum
						'', //pagenumstyle
						'', //suppress
						$margin_left, //margin-left
						$margin_right, //margin-right
						$margin_top, //margin-top
						$margin_bottom, //margin-bottom
						0, //margin-header
						0 //margin-footer
					);
					$pdf->WriteHTML($html);
					$pdfFilePath = "./uploads/1purchases_order_" . $id . "_" . $item_row['id'] . ".pdf";
					$files_urls[] = $pdfFilePath;
					$pdf->Output($pdfFilePath, "F");
				}
				$this->db->where('id', $id);
				$this->db->update('purchases_order', array('pdf_url' => serialize($files_urls)));
				redirect(base_url() . 'mail-system3/document-mail/purchases-order/' . $id);
			}

		}
	}

	public function generate_pdf()
	{
		$this->load->library('m_pdf');
		$pdf = new mPDF();
		$pdf->WriteHTML('<p>HTML content goes here...</p>');
		$pdf->Output('suersh.pdf', 'I');
	}

	/**
	 *  Fetch Lead Owner
	 */
	function fetch_autocomplete_lead_owner()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('staff', 'name', $search,array('active !='=>0));
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->name;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Assigned To
	 */
	function fetch_autocomplete_assigned_to()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('staff', 'name', $search,array('active !='=>0));
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->name;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Lead Provider
	 */
	function fetch_autocomplete_lead_provider()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_provider', 'lead_provider', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->lead_provider,
				'value' => $row->lead_provider,
			);
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Industry Type
	 */
	function fetch_autocomplete_industry_type()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('industry_type', 'industry_type', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->industry_type;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Lead Stage
	 */
	function fetch_autocomplete_lead_stage()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_stage', 'lead_stage', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->lead_stage;
			$i++;
		}
		echo json_encode($response);
	}

	function fetch_autocomplete_order_stage()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('purchases_order_stage', 'purchases_order_stage', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->purchases_order_stage,
				'value' => $row->purchases_order_stage,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Priority
	 */
	function fetch_autocomplete_priority()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('priority', 'priority', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->priority;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Lead Source
	 */
	function fetch_autocomplete_lead_source()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_source', 'lead_source', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->lead_source;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Status
	 */
	function fetch_autocomplete_lead_status()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_status', 'lead_status', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->lead_status;
			$i++;
		}
		echo json_encode($response);
	}

	function fetch_autocomplete_order_status()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('purchases_order_status', 'purchases_order_status', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->purchases_order_status,
				'value' => $row->purchases_order_status,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Reference
	 */
	function fetch_autocomplete_reference()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('reference', 'reference', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->reference;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch purchases
	 */
	function fetch_autocomplete_purchases()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('purchases', 'purchases', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->purchases,
				'value' => $row->purchases,
			);
		}
		echo json_encode($response);
	}

	public function ajax_load_supplier_with_cnt_person($supplier_id)
	{
		$this->load->helper('url');

		$row = $this->crud->load_supplier_with_cnt_person_3($supplier_id);
		$return = array(
			'value' => $row->supplier_id,
			'label' => $row->supplier_id . ' - ' . $row->supplier_name,
			'supplier_id' => $row->supplier_id,
			'supplier_code' => $row->supplier_id,
			'supplier_name' => $row->supplier_name,
			'address' => $row->address,
			'city' => $row->city,
			'city_id' => $row->city_id,
			'state' => $row->state,
			'state_id' => $row->state_id,
			'country' => $row->country,
			'country_id' => $row->country_id,
			'email_id' => $row->email_id,
			'phone_no' => $row->phone_no,
			'pincode' => $row->pincode
		);
		//echo "<pre>";print_r($return);exit;
		print json_encode($return);
		exit;
	}

	public function ajax_load_item_details($item_id = 0)
	{
		if ($item_id != 0) {
			$this->load->helper('url');
			$row = $this->crud->load_purchase_item_details_where($item_id);

			$return = array(
				'id' => $row->id,
				'item_name' => $row->item_name,
				'item_code' => $row->item_code1,
				'item_status' => $row->item_status,
				'item_status_id' => $row->item_status_id,
				'item_category' => $row->item_status_id,
				'discount' => $row->discount,
				'igst' => $row->igst,
				'sgst' => $row->sgst,
				'cgst' => $row->cgst,
				'rate' => $row->rate,
				'uom' => $row->uom,
			);
			print json_encode($return);
			exit;
		}
	}

	function moneyFormatIndia($num)
	{
		$explrestunits = "";
		if (strlen($num) > 3) {
			$lastthree = substr($num, strlen($num) - 3, strlen($num));
			$restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);
			for ($i = 0; $i < sizeof($expunit); $i++) {
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0) {
					$explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				} else {
					$explrestunits .= $expunit[$i] . ",";
				}
			}
			$thecash = $explrestunits . $lastthree;
		} else {
			$thecash = $num;
		}
		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	public function ajax_load_item_edit_details($item_id)
	{
		$this->load->helper('url');
		
		$this->db->select('*');
		$this->db->from('purchase_invoice_items');
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		$row = $query->row();

		$iteam_data = json_decode($row->item_data);
		//echo "<pre>";print_r($iteam_data);exit;
		$return = array(
			'id' => $row->id,
			'item_id' => $iteam_data->item_id,
			'invoice_id' => $row->invoice_id,
			'item_code' => $iteam_data->item_code,
			'item_name' => $iteam_data->item_name,
			'cust_desc' => $iteam_data->cust_desc,
			'item_rate' => isset($iteam_data->item_rate) ? $this->moneyFormatIndia($iteam_data->item_rate) : 0,
			'quantity' => $iteam_data->quantity,
			'discount' => $iteam_data->discount,
			'discount_amount' => $iteam_data->discount_amount,
			'igst' => $iteam_data->igst,
			'igst_amount' => $iteam_data->igst_amount,
			'cgst' => $iteam_data->cgst,
			'cgst_amount' => $iteam_data->cgst_amount,
			'sgst' => $iteam_data->sgst,
			'sgst_amount' => $iteam_data->sgst_amount,
			'total_amount' => $iteam_data->total_amount,
			'uom_id' => $iteam_data->uom_id,
		);
		if(isset($iteam_data->total_amount)){
			$return['total_amount'] = $iteam_data->total_amount;
		}
		if(isset($iteam_data->item_category_id)){
			$return['item_category_id'] = $iteam_data->item_category_id;
		}
		if(isset($iteam_data->uom_id)){
			$return['uom_id'] = $iteam_data->uom_id;
		}
		//echo "<pre>";print_r($return);exit;
		print json_encode($return);
		exit;
	}

	public function ajax_load_order_item_edit_details($item_id)
	{
		$this->load->helper('url');

		$this->db->select('*');
		$this->db->from('purchase_order_items');
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		$row = $query->row();

		$iteam_data = json_decode($row->item_data);
		//echo "<pre>";print_r($iteam_data);exit;
		$return = array(
			'id' => $row->id,
			'item_id' => $iteam_data->item_id,
			'order_id' => $row->order_id,
			'item_code' => $iteam_data->item_code,
			'item_name' => $iteam_data->item_name,
			'cust_desc' => $iteam_data->cust_desc,
			'item_rate' => isset($iteam_data->item_rate) ? $this->moneyFormatIndia($iteam_data->item_rate) : 0,
			'quantity' => $iteam_data->quantity,
			'discount' => $iteam_data->discount,
			'discount_amount' => $iteam_data->discount_amount,
			'igst' => $iteam_data->igst,
			'igst_amount' => $iteam_data->igst_amount,
			'cgst' => $iteam_data->cgst,
			'cgst_amount' => $iteam_data->cgst_amount,
			'sgst' => $iteam_data->sgst,
			'sgst_amount' => $iteam_data->sgst_amount,
			'total_amount' => $iteam_data->total_amount,
		);
		if(isset($iteam_data->total_amount)){
			$return['total_amount'] = $iteam_data->total_amount;
		}
		if(isset($iteam_data->item_category_id)){
			$return['item_category_id'] = $iteam_data->item_category_id;
		}
		if(isset($iteam_data->uom_id)){
			$return['uom_id'] = $iteam_data->uom_id;
		}
		//echo "<pre>";print_r($return);exit;
		print json_encode($return);
		exit;
	}

	/**
	 * @param $item_id
	 */
	function get_purchases_order_item_by_id($item_id)
	{
		$this->db->select('*');
		$this->db->from('purchases_order_items');
		$this->db->where('id', $item_id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$item_data = $query->row_array(0);
			echo json_encode(array('success' => true, 'message' => "Item Data", 'item_data' => $item_data));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => "wrong!"));
			exit();
		}
	}

	/**
	 * @param $id
	 */
	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
	}

	/* ================================ORDER========================================= */

	/**
	 *   Add Order
	 */
	function add_order()
	{
		$open_status_id = $this->get_lead_status_id_by_status('open');
		$sql = "SELECT * FROM lead_details where lead_status_id = '$open_status_id'";
		$leads = $this->crud->getFromSQL($sql);
		$data = array(
			'leads' => $leads,
			'order_no' => $this->crud->get_next_autoincrement('purchase_order')
		);
		if($this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"add")) {
			set_page('purchases/add_order', $data);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}

	function order_edit($order_id)
	{
		$role_edit = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "edit");
		if ($role_edit) {
			$order_data = $this->get_order_by_id($order_id);
			$order_items = $this->get_order_items($order_id);
			$data = array(
				'order_data' => $order_data,
				'order_items' => $order_items
			);
			if($this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"edit")) {
				set_page('purchases/order_edit', $data);
			}else{
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function get_order_by_id($order_id)
	{
		$this->db->select("i.*,sp.supplier_name as supplier_name, st.name as assigned_by");
		$this->db->from('purchase_order i');
		$this->db->join('staff st', 'st.staff_id = i.created_by', 'left');
		$this->db->join('supplier sp', 'sp.supplier_id = i.supplier_id', 'left');
		$this->db->where('i.order_id', $order_id);
		return $this->db->get()->row(0);
	}

	function get_order_items($order_id)
	{
		$this->db->select("is.*");
		$this->db->from('purchase_order_items is');
		$this->db->where('is.order_id', $order_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$item_data = array();
			foreach ($query->result_array() as $item) {
				$item_data_array = json_decode($item['item_data'], true);
				$item_data[] = array_merge(array('id' => $item['id']), $item_data_array);
			}
			return $item_data;
		} else {
			return array();
		}
	}

	/**
	 *  Create order
	 */
	function create_order()
	{
		//echo '<pre>';print_r($_POST);exit;
		$supplier_id = $_POST['order']['supplier_id'];

		if ($supplier_id == "" || $supplier_id == "0") {
			echo json_encode(array("success" => 'false', 'order_id' => $_POST['order']['order_id'], 'item_id' => 0, 'msg' => 'Please select supplier code first!'));
			exit;
		}

		// check supplier validations
		$result = $this->check_supplier_validations($supplier_id);
		if ($result['status'] == 0) {
			echo json_encode(array("success" => 'false', 'order_id' => $_POST['order']['order_id'], 'item_id' => 0, 'msg' => $result['msg']));
			exit;
		}

		/*--------- Convert Date as Mysql Format -------------*/
		$_POST['order']['order_date'] = date("Y-m-d", strtotime(str_replace("/", "-", $_POST['order']['order_date'])));
		
		if(!empty($_POST['order']['mach_deli_min_weeks_date']) && isset($_POST['order']['mach_deli_min_weeks_date'])){
			$_POST['order']['mach_deli_min_weeks_date'] = date('Y-m-d', strtotime(str_replace("/", "-", $_POST['order']['mach_deli_min_weeks_date'])));
		}else{
			$_POST['order']['mach_deli_min_weeks_date'] = null;
		}
		//COMMENTED ON 24-JUN-2017

		if(!empty($_POST['order']['mach_deli_max_weeks_date']) && isset($_POST['order']['mach_deli_max_weeks_date'])){
			$_POST['order']['mach_deli_max_weeks_date'] = date('Y-m-d', strtotime(str_replace("/", "-", $_POST['order']['mach_deli_max_weeks_date'])));
		}else{
			$_POST['order']['mach_deli_max_weeks_date'] = null;
		}
		
		// Check Inquiry is new or old
		$dataToInsert = $_POST['order'];
		//echo '<pre>';print_r($dataToInsert);exit;
		if (isset($_POST['order']['order_id']) && $_POST['order']['order_id'] == 0) { // New Inquiry

			$dataToInsert['created_at'] = date('Y-m-d H:i:s');
			$dataToInsert['created_by'] = $this->staff_id;
			// Unset order_id from Inquiry Data
			if (isset($dataToInsert['order_id']))
				unset($dataToInsert['order_id']);

			$this->db->insert('purchase_order', $dataToInsert);
			$order_id = $this->db->insert_id();
			$this->crud->update('purchase_order', array('order_no' => $order_id), array('order_id' => $order_id));

		} else { // Existing Inquiry
			/*if (!isset($_POST['order']['is_received_post'])) {
				$_POST['order']['is_received_post'] = 0;
			}
			if (!isset($_POST['order']['is_send_post'])) {
				$_POST['order']['is_send_post'] = 0;
			}
			if (!isset($_POST['order']['send_from_post'])) {
				$_POST['order']['send_from_post'] = 0;
			}*/

			$this->db->where('order_id', $_POST['order']['order_id']);
			$this->db->update('purchase_order', $_POST['order']);
			$order_id = $_POST['order']['order_id'];
		}

		$item_id = '';
		/*---------- Add Item If Available ------------*/
		if (empty($_POST['edit_item'])) {
			$item_id = $this->add_order_item($order_id);
		}

		/*------------ Return Response ----------------*/
		echo json_encode(array("success" => 'true', 'order_id' => $order_id, 'item_id' => $item_id));
		exit();
	}

	/**
	 * @param $order_id
	 * @return bool|int
	 */
	function add_order_item($order_id)
	{

		$item_code = isset($_POST['item_data']['item_id']) ? $_POST['item_data']['item_id'] : '';
		if ($item_code != '') {

			// check unique validation
			$tempArr = $this->db->select('*')->from('purchase_items')->where('id', $item_code)->get()->result_array();

			if (count($tempArr) == 0) {
				if ($this->uri->segment(2) == "add-order-item") {
					echo json_encode(array('success' => 'false', 'msg' => 'Item code not exists in item master!'));
					exit();
				} else {
					return false;
				}
			}

			$item_data['order_id'] = $order_id;
			$item_data['created_at'] = date('Y-m-d H:i:s');
			$_POST['item_data'] = str_replace(',', '', $_POST['item_data']);
			$item_data['item_data'] = json_encode($_POST['item_data']);
			$item_data['item_id'] = $_POST['item_data']['item_id'];
			$item_data['quantity'] = $_POST['item_data']['quantity'];

			// echo "<pre>";print_r($_POST);exit;

			$this->db->insert('purchase_order_items', $item_data);
			if ($this->uri->segment(2) == "add-order-item") {
				echo json_encode(array(
						'success' => 'true',
						'order_id' => $order_id,
						'item_id' => $this->db->insert_id()
					)
				);
				exit();
			} else {
				return $this->db->insert_id();
			}
		} else {
			if ($this->uri->segment(2) == "add-order-item") {
				echo json_encode(array('success' => 'false', 'msg' => 'Please select item!'));
				exit();
			} else {
				return 'false';
			}
		}
	}

	/**
	 * @param $order_item_id
	 * @return int|string
	 */
	function edit_order_item($order_item_id)
	{

		$item_code = $_POST['item_data']['item_id'];
		if ($item_code != '') {

			// check unique validation
			$tempArr = $this->db->select('*')->from('purchase_items')->where('id', $item_code)->get()->result_array();

			if (count($tempArr) == 0) {
				if ($this->uri->segment(2) == "add-order-item") {
					echo json_encode(array('success' => 'false', 'msg' => 'Item code not exists in item master!'));
					exit();
				} else {
					return false;
				}
			}
			$_POST['item_data'] = str_replace(',', '', $_POST['item_data']);
			$item_data['item_data'] = json_encode($_POST['item_data']);
			$item_data['item_id'] = $_POST['item_data']['item_id'];
			$item_data['quantity'] = $_POST['item_data']['quantity'];

			$this->db->where('id', $order_item_id);
			$this->db->update('purchase_order_items', $item_data);
			echo json_encode(array(
					'success' => 'true',
					'item_id' => $order_item_id
				)
			);
			exit();
		} else {
			echo json_encode(array('success' => 'false', 'msg' => 'Please select item!'));
			exit();
		}
	}

	function delete_order_item()
	{
		$id = $this->input->post("id");

		$status = 1;
		$msg = "Item has been deleted !";
		$where_array = array("id" => $id);
		$this->crud->delete("purchase_order_items", $where_array);
		echo json_encode(array("status" => $status, "msg" => $msg));
		exit;
	}

	function order_list()
	{
		$sql_list_query = "SELECT e.order_id, e.order_no, p.supplier_name,p.supplier_id,e.order_date
							from purchase_order e
							LEFT JOIN supplier p ON p.supplier_id = e.supplier_id";

		$condition = ' WHERE 1';

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
		} else {
			//$condition .= ' AND ( p.`created_by`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.`created_by` IS NULL )';
			if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
			} else if ($cu_accessExport == 1) {
				//$condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
			} else if ($cu_accessDomestic == 1) {
				//$condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
			} else {
			}
		}
		$status = $this->input->get_post('status');
		$user_id = $this->input->get_post('user_id');
		if ($status != '' && $status != 'all') {
			//$condition .= " AND  s.inquiry_status = '" . $status . "' ";
		}

		if ($user_id != '' && $user_id != 'all') {
			$condition .= " AND  e.created_by = '" . $user_id . "' ";
		}

		$query = $this->db->query($sql_list_query . $condition);
		$orders = $query->result_array();

		//$this->db->order_by('id');
		//$enquiry_status = $this->db->get('inquiry_status')->result();
		//$this->db->order_by('name');
		
		$where_array = array('module_id' => PURCHASE_ORDER_MODULE_ID,'title' => 'add');
		$module_role_id = $this->crud->get_column_value_by_id('module_roles','id',$where_array);
		$where_array = array('module_id' => PURCHASE_ORDER_MODULE_ID,'role_id' => $module_role_id);
		$staff_ids_arr = $this->crud->get_columns_val_by_where('staff_roles','staff_id',$where_array);
		$staff_ids = array();
		foreach ($staff_ids_arr as $rows) {
			foreach ($rows as $data) {
				$staff_ids[] = $data;
			}
		}
		$users = $this->crud->getFromSQL('SELECT * FROM `staff` WHERE `staff_id` in ('.implode(",",$staff_ids).') AND `active` != 0 ');
		if($this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"view")) {
			set_page('purchases/order_list', array('orders' => $orders, 'users' => $users));
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function orders_datatable()
	{
		$requestData = $_REQUEST;
		
		//print_r($this->session->userdata('user_roles'));
        $post_data = $this->input->post();
		$user_id = $post_data['user_id'];
        
		$config['table'] = 'purchase_order e';
		$config['select'] = 'e.order_id, e.order_no, p.supplier_name,e.order_date,e.created_at';
		$config['column_order'] = array(null, 'e.order_no', 'p.supplier_name', 'e.order_date','p.created_by');
		$config['column_search'] = array('e.order_no', 'p.supplier_name', 'e.order_date');
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$config['likes'][] = array('e.order_no' => $requestData['columns'][1]['search']['value']);
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$config['likes'][] = array('p.supplier_name' => $requestData['columns'][2]['search']['value']);
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			$config['likes'][] = array('e.order_date' => $requestData['columns'][3]['search']['value']);
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {
			$config['likes'][] = array('e.created_at' => $requestData['columns'][4]['search']['value']);
		}
		
		$config['joins'][1] = array('join_table' => 'supplier p', 'join_by' => 'p.supplier_id = e.supplier_id', 'join_type' => 'left');
		$config['order'] = array('e.order_id' => 'desc');
		$config['where_string'] = ' 1 = 1 ';
		//$user_id = $this->input->get_post('user_id');
		if ($user_id != '' && $user_id != 'all') {
			$config['where_string'] .= " AND e.created_by = '" . $user_id . "' ";
		}
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		$role_delete = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "edit");

		foreach ($list as $order) {
			$row = array();
			$action = '';
			if ($role_edit && $role_delete) {
				$action .= '<a href="' . BASE_URL . "purchases/order-edit/" . $order->order_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('purchases/delete_order/' . $order->order_id) . '"><i class="fa fa-trash"></i></a>';
			} elseif ($role_edit) {
				$action .= '<a href="' . BASE_URL . "purchases/order-edit/" . $order->order_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			} elseif ($role_delete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('purchases/delete_order/' . $order->order_id) . '"><i class="fa fa-trash"></i></a>';
			}
			$action .= ' | <a href="' . BASE_URL . 'purchases/purchases_order_print/' . $order->order_id . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
			$action .= ' | <a href="' . base_url('purchases/purchases_order_email/' . $order->order_id) . '" class="email_button">Email</a>';
			
			$row[] = $action;
			$row[] = '<a href="' . base_url('purchases/order-edit/' . $order->order_id . '?view') . '" >'.$order->order_id.'</a>';
			$row[] = '<a href="' . base_url('purchases/order-edit/' . $order->order_id . '?view') . '" >'.$order->supplier_name.'</a>';
			$row[] = '<a href="' . base_url('purchases/order-edit/' . $order->order_id . '?view') . '" >'.date('d-m-Y', strtotime($order->order_date)).'</a>';
			$row[] = '<a href="' . base_url('purchases/order-edit/' . $order->order_id . '?view') . '" >'.date('d-m-Y', strtotime($order->created_at)).'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

		/*$this->db->select('e.invoice_id, e.inquiry_no, s.inquiry_status,p.party_name,p.party_code,e.inquiry_date');
		$this->db->from('inquiry e');
		$this->db->join('inquiry_status s', 's.id = e.inquiry_status_id', 'left');
		$this->db->join('party p', 'p.party_id = e.party_id', 'left');

		$accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
		$accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
		if ($accessExport == 1 && $accessDomestic == 1) {

		} else if ($accessExport == 1) {
			$this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
		} else if ($accessDomestic == 1) {
			$this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
		}
		$status = $this->input->get_post('status');
		$user_id = $this->input->get_post('user_id');
		if ($status != '') {
			$this->db->where('s.inquiry_status', $status);
		}

		if ($user_id != '') {
			$this->db->where('e.created_by', $user_id);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {

		}*/
	}

	function delete_order($id)
	{
		$role_delete = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "delete");
		if ($role_delete) {
			$where_array = array("order_id" => $id);
			$this->crud->delete("purchase_order", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
}
