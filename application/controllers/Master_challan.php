<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_challan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
		$this->load->library('user_agent');
	}
	/* function index()
	{

	} */
	function user($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$user = $this->crud->get_all_with_where('challan_item_user','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('challan_item_user', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($user) ? $user[0]->id : '';
		$return['user_name'] = ($user) ? $user[0]->user_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_USER,"view")) {
			set_page('master_challan/challan_item_user', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_user()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_user',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','User Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_user()
	{
		$post_data = $this->input->post();
		//echo  $postdata['user_id']; exit(); 
		$where_array['id'] = $postdata['user_id'];

		$result = $this->crud->update('challan_item_user', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','User Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function make($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$make = $this->crud->get_all_with_where('challan_item_make','id','ASC',$where_array);
			//echo"<pre>";
			//print_r($make); exit();
		}
		$result_data = $this->crud->get_all_records('challan_item_make', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($make) ? $make[0]->id : '';
		$return['make_name'] = ($make) ? $make[0]->make_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_MAKE,"view")) {
			set_page('master_challan/challan_item_make', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}
	
	function add_make()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_make',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Make Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_make()
	{
		$post_data = $this->input->post();
		//echo  $postdata['user_id']; exit(); 
		//$where_array['id'] = $postdata['make_id'];

		$result = $this->crud->update('challan_item_make', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Make Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function hp($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$hp = $this->crud->get_all_with_where('challan_item_hp','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('challan_item_hp', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($hp) ? $hp[0]->id : '';
		$return['hp_name'] = ($hp) ? $hp[0]->hp_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_HP,"view")) {
			set_page('master_challan/challan_item_hp', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}
	
	function add_hp()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_hp',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','HP Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_hp()
	{
		$post_data = $this->input->post();

		$result = $this->crud->update('challan_item_hp', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','HP Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function kw($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$kw = $this->crud->get_all_with_where('challan_item_kw','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('challan_item_kw', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($kw) ? $kw[0]->id : '';
		$return['kw_name'] = ($kw) ? $kw[0]->kw_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_KW,"view")) {
			set_page('master_challan/challan_item_kw', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_kw()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_kw',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','KW Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_kw()
	{
		$post_data = $this->input->post();

		$result = $this->crud->update('challan_item_kw', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','KW Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function frequency($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$frequency = $this->crud->get_all_with_where('challan_item_frequency','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('challan_item_frequency', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($frequency) ? $frequency[0]->id : '';
		$return['frequency_name'] = ($frequency) ? $frequency[0]->frequency_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_FREQUENCY,"view")) {
			set_page('master_challan/challan_item_frequency', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}	
	}
	
	function add_frequency()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_frequency',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Frequency Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_frequency()
	{
		$post_data = $this->input->post();

		$result = $this->crud->update('challan_item_frequency', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Frequency Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function rpm($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$rpm = $this->crud->get_all_with_where('challan_item_rpm','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('challan_item_rpm', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($rpm) ? $rpm[0]->id : '';
		$return['rpm_name'] = ($rpm) ? $rpm[0]->rpm_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_RPM,"view")) {
			set_page('master_challan/challan_item_rpm', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}	
	}
	
	function add_rpm()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_rpm',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','RPM Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_rpm()
	{
		$post_data = $this->input->post();

		$result = $this->crud->update('challan_item_rpm', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','RPM Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function volts($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$volts = $this->crud->get_all_with_where('challan_item_volts_cycles','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('challan_item_volts_cycles', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($volts) ? $volts[0]->id : '';
		$return['volts_cycles_name'] = ($volts) ? $volts[0]->volts_cycles_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_VOLTS,"view")) {
			set_page('master_challan/challan_item_volts', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}	
	}
	
	function add_volts()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_volts_cycles',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Volts Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_volts()
	{
		$post_data = $this->input->post();

		$result = $this->crud->update('challan_item_volts_cycles', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Volts Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function model($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$model = $this->crud->get_all_with_where('challan_item_model','id','ASC',$where_array);	
			//print_r($ratio); exit();
		}
		$result_data = $this->crud->get_all_records('challan_item_model', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($model) ? $model[0]->id : '';
		$return['model_name'] = ($model) ? $model[0]->model_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_MODEL,"view")) {
			set_page('master_challan/challan_item_model', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_model()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_model',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Model Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_model()
	{
		$post_data = $this->input->post();

		$result = $this->crud->update('challan_item_model', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Model Updated Successfully');
			echo json_encode($post_data);
		}	
	}

	function ratio($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$ratio = $this->crud->get_all_with_where('challan_item_ratio','id','ASC',$where_array);	
			//print_r($ratio); exit();
		}
		$result_data = $this->crud->get_all_records('challan_item_ratio', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($ratio) ? $ratio[0]->id : '';
		$return['ratio_name'] = ($ratio) ? $ratio[0]->ratio_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_RATIO,"view")) {
			set_page('master_challan/challan_item_ratio', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}
	
	function add_ratio()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_ratio',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Ratio Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_ratio()
	{
		$post_data = $this->input->post();
		//echo  $postdata['user_id']; exit(); 
		$where_array['id'] = $postdata['user_id'];

		$result = $this->crud->update('challan_item_ratio', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Ratio Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function gear($id="")
	{
		$day = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$gear = $this->crud->get_all_with_where('challan_item_gear_type','id','ASC',$where_array);	
			//print_r($ratio); exit();
		}
		$result_data = $this->crud->get_all_records('challan_item_gear_type', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($gear) ? $gear[0]->id : '';
		$return['gear_type_name'] = ($gear) ? $gear[0]->gear_type_name : '';
		if($this->applib->have_access_role(MASTER_CHALLAN_ITEM_GEAR_TYPE,"view")) {
			set_page('master_challan/challan_item_gear_type', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}
	
	function add_gear_type()
	{
		$post_data = $this->input->post();
		$result = $this->crud->insert('challan_item_gear_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Gear Type Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function update_gear_type()
	{
		$post_data = $this->input->post();

		$result = $this->crud->update('challan_item_gear_type', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Gear Type Updated Successfully');
			echo json_encode($post_data);
		}	
	}

	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Deleted Successfully');
	}
    function default_challan_detail() {
        if ($this->app_model->have_access_role(MASTER_CHALLAN_DEFAULT_DETAILS, "view") || $this->applib->have_access_role(MASTER_CHALLAN_DEFAULT_DETAILS,"edit")) {
            $data = array(
                'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
            );
            $data['default_challan_details'] = $this->crud->get_all_records('default_challan_details', 'id', 'asc');
            $default_challan_details = $data['default_challan_details'];
            $default_details_arr = array();
            if(isset($default_challan_details) && !empty($default_challan_details)){
                foreach($default_challan_details as $row){
                    $default_details_arr[$row->meta_key] = $row->meta_value;
                }
            }
            $data['default_details_arr'] = $default_details_arr;
            $data['motor_user'] = $this->crud->get_all_records('challan_item_user', 'user_name', 'asc');
            $data['motor_make'] = $this->crud->get_all_records('challan_item_make', 'make_name', 'asc');
            $data['motor_hp'] = $this->crud->get_all_records('challan_item_hp', 'hp_name', 'asc');
            $data['motor_kw'] = $this->crud->get_all_records('challan_item_kw', 'kw_name', 'asc');
            $data['motor_frequency'] = $this->crud->get_all_records('challan_item_frequency', 'frequency_name', 'asc');
            $data['motor_rpm'] = $this->crud->get_all_records('challan_item_rpm', 'rpm_name', 'asc');
            $data['motor_volts_cycles'] = $this->crud->get_all_records('challan_item_volts_cycles', 'volts_cycles_name', 'asc');
            $data['gearbox_user'] = $data['motor_user'];
            $data['gearbox_make'] = $data['motor_make'];
            $data['gearbox_gear_type'] = $this->crud->get_all_records('challan_item_gear_type', 'gear_type_name', 'asc');
            $data['gearbox_model'] = $this->crud->get_all_records('challan_item_model', 'model_name', 'asc');
            $data['gearbox_ratio'] = $this->crud->get_all_records('challan_item_ratio', 'ratio_name', 'asc');
            set_page('master_challan/default_challan_details', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
    
    function save_default_challan_detail(){
        $return = array();
        $post_data = $this->input->post();
        foreach ($post_data as $meta_key => $meta_value){
            $result = $this->crud->update('default_challan_details', array('meta_value' => $meta_value), array('meta_key' => $meta_key));
        }
        if ($result) {
			$return['success'] = 'Updated';
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Default Challan Details Updated Successfully');
		}
        echo json_encode($return);
        exit;
    }

}
