<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Mailbox
 * @property AppLib $applib
 */
class Mailbox extends CI_Controller
{


	/*
		private $email_address = 'info@omfortune.in';
		private $email_password = 'p@55w0rD@321';
		private $email_server = '{md-in-35.webhostbox.net}';
	*/

	private $email_address = SMTP_EMAIL_ADDRESS;
	private $email_password = SMTP_EMAIL_PASSWORD;
	private $email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';
	private $per_page = 10;

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->email_address = trim($this->session->userdata('is_logged_in')['mailbox_email']);
		$this->email_password = trim($this->session->userdata('is_logged_in')['mailbox_password']);
		$this->load->library('user_agent');
	}

	function index()
	{
		$mailbox_name = 'Inbox';
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$messageCount = imap_num_msg($mb);
		$Properties = array();
		$mail_status = imap_status($mb, $this->email_server . $mailbox_name, SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!");
		$Properties['count_unseen_mails'] = 0;
		$Properties['inbox_unseen_mails'] = $this->count_inbox_unread_mail();
		$Properties['total_mails'] = $mail_status->messages;
		$Properties['mails_range'] = $messageCount > 0 ? "1-$messageCount" : "0-$messageCount";
		$Properties['mailboxes'] = $this->get_mailboxes($mb);
		if ($mail_status->messages > 0) {
			$message_nos = imap_sort($mb, SORTDATE, 1) or die(imap_last_error() . "<br>Connection Faliure11!");
			foreach ($message_nos as $key => $message_no) {
				$EmailHeaders = imap_headerinfo($mb, $message_no);

				/*-------- Count Unread Mails -----------------*/
				if ($EmailHeaders->Unseen == 'U') {
					$Properties['count_unseen_mails'] = $Properties['count_unseen_mails'] + 1;
					$Properties['mails'][$key]['is_unread'] = true;
				} else {
					$Properties['mails'][$key]['is_unread'] = false;
				}

				/*-------- Set Attachment Status -----------------*/
				$structure = imap_fetchstructure($mb, $message_no);
				$is_attachment = 0;
				if (isset($structure->parts) && count($structure->parts)) {
					$attachments = array();
					for ($i = 0; $i < count($structure->parts); $i++) {
						$attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
						$attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
						if ($structure->parts[$i]->ifdparameters) {
							foreach ($structure->parts[$i]->dparameters as $object) {
								if (strtolower($object->attribute) == 'filename') {
									$is_attachment = 1;
								}
							}
						}
					}
				}

				if ($is_attachment == 1) {
					$Properties['mails'][$key]['AttachmentStatus'] = 1;
				} else {
					$Properties['mails'][$key]['AttachmentStatus'] = 0;
				}
				$Properties['mails'][$key]['DateReceived'] = $this->timeAgo($EmailHeaders->Date);
				$Properties['mails'][$key]['EmailHeaders'] = $EmailHeaders;
				$Properties['mails'][$key]['MID'] = $message_no;
			}
		}
		imap_close($mb);
		return $Properties;
	}

	function inbox()
	{
		$mails_data = $this->get_mail_list('inbox');
		/*echo "<pre>";
		print_r($mails_data);
		die;*/
		set_page('mail_module/mailbox', array('mails_data' => $mails_data));
	}

	function sent_mail()
	{
		$mails_data = $this->get_mail_list('[Gmail]/Sent Mail');
		set_page('mail_module/mailbox', array('mails_data' => $mails_data, 'curr_folder_name' => 'Sent Mail'));
	}

	function drafts()
	{
		$mails_data = $this->get_mail_list('[Gmail]/Drafts');
		set_page('mail_module/mailbox', array('mails_data' => $mails_data));
	}

	function trash()
	{
		$mails_data = $this->get_mail_list('[Gmail]/Trash');
		set_page('mail_module/mailbox', array('mails_data' => $mails_data));
	}

	function spam()
	{
		$mails_data = $this->get_mail_list('[Gmail]/Spam');
		set_page('mail_module/mailbox', array('mails_data' => $mails_data));
	}

	function junk()
	{
		$mails_data = $this->get_mail_list('INBOX.Junk');
		set_page('mail_module/mailbox', array('mails_data' => $mails_data));
	}

	function starred()
	{
		$mails_data = $this->get_mail_list('[Gmail]/Starred');
		set_page('mail_module/mailbox', array('mails_data' => $mails_data));
	}

	function folder_mails()
	{
		$folder_name = $org_folder_name = isset($_POST['folder_name']) ? $_POST['folder_name'] : $this->getConfigValue('folder_name');
		$folder_name = $this->getServerFolderName($folder_name);
		$mails_data = $this->get_mail_list($folder_name);
		set_page('mail_module/mailbox', array('mails_data' => $mails_data, 'curr_folder_name' => $org_folder_name));
	}

	function getServerFolderName($folder_name)
	{
		$default_folder_name = array(
			'All Mail',
			'Drafts',
			'Important',
			'Sent Mail',
			'Spam',
			'Starred',
			'Trash',
		);
		if (strtolower($folder_name) == "inbox") {
			return "INBOX";
		} elseif (in_array(ucfirst($folder_name), $default_folder_name)) {
			return "[Gmail]/" . ucfirst($folder_name);
		} else {
			$mailbox_label_array = explode('/', $folder_name);
			if (count($mailbox_label_array) > 1) {
				$folder_name = '';
				foreach ($mailbox_label_array as $key => $item) {
					if ($key > 0) {
						$folder_name .= "/";
					}
					$folder_name .= $item;
				}
			}
			return $folder_name;
		}
	}

	function read()
	{
		$folder_name = $_POST['folder_name'];
		$message_no = $_POST['message_no'];
		if ($this->input->is_ajax_request()) {
			$mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
			$mail_view = $this->load->view('mail_module/read_email', array('mail_detail' => $mail_detail, 'message_no' => $message_no, 'curr_folder_name' => $folder_name), true);
			$no_unread_mails = $this->applib->count_inbox_unread_mail();
			$unread_mails = '';
			if ($no_unread_mails > 0) {
				$unread_mails = $this->load->view('shared/unread_mails_view', array('unread_mails' => $no_unread_mails, 'unread_mails_data' => $this->applib->fetch_unread_mails()), true);
			}
			echo json_encode(array('email_detail' => $mail_view, 'no_unread_mails' => $no_unread_mails, 'unread_mails' => $unread_mails));
			exit();
		} else {
			$mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
			$mails_data = $this->getMailbox($folder_name);
			set_page('mail_module/mailbox', array('mail_detail' => $mail_detail, 'mails_data' => $mails_data, 'message_no' => $message_no, 'curr_folder_name' => $folder_name));
		}
	}

	function compose_email($action = "compose")
	{
		if (!empty($_POST['to'])) {
			$cc = isset($_POST['cc'])?$_POST['cc']:array();
			if ($this->send_mail($_POST['to'], $_POST['subject'], $_POST['message'], $_POST['attachments'],$cc)) {
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Mail sent successfully!');
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Wrong please try again.');
			}
			redirect(base_url() . "mailbox/sent-mail/");
		} else {
			$settings = $this->load_settings();
			if ($action != "compose") {
				$folder_name = $_POST['folder_name'];
				$message_no = $_POST['message_no'];
				$mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
				//$mails_data = $this->getMailbox($folder_name);
				$mails_data = array();
				set_page('mail_module/mailbox', array('mail_detail' => $mail_detail,'settings' => $settings, 'mails_data' => $mails_data, 'curr_folder_name' => $folder_name, 'action' => $action));
			} else {
				//$mails_data = $this->getMailbox();
				$mails_data = array();
				set_page('mail_module/mailbox', array('mails_data' => $mails_data,'settings' => $settings, 'action' => $action));
			}
		}
	}

	function getMailbox($folder_name = "INBOX")
	{
		$folder_name = $this->getServerFolderName($folder_name);
		$mails_data = $this->fetch_mails($folder_name);
		return $mails_data;
	}

	function fetch_mail_detail($mailbox_name, $message_no)
	{
		$message_no = (int)trim($message_no);
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$messageCount = imap_num_msg($mb);
		$mail_data = array();
		if ($messageCount > 0) {
			$email_headers = imap_headerinfo($mb, $message_no);

			/*echo "<pre>";
			print_r($email_headers);
			die();*/

			$structure = imap_fetchstructure($mb, $message_no);
			if (isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
				$part = $structure->parts[1];
				/*echo "<pre>";print_r($part);die;*/
				if ($part->subtype == "HTML") {
					if ($part->encoding == 4) {
						$body = imap_qprint(imap_fetchbody($mb, $message_no, 2));
					} else {
						$body = imap_fetchbody($mb, $message_no, 2);
					}
				} else {
					$body = imap_fetchbody($mb, $message_no, 1.2);
					if (!strlen($body) > 0) {
						$body = imap_fetchbody($mb, $message_no, 1);
					}
					$body = nl2br($body);
				}
			} else {
				$body = imap_fetchbody($mb, $message_no, 1.2);
				if (!strlen($body) > 0) {
					$body = imap_fetchbody($mb, $message_no, 1);
				}
				$body = preg_replace('/^\>/m', '', $body);
				$body = nl2br($body);
			}

			$to_email_address = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
			if ($to_email_address != $this->email_address) {
				if (isset($email_headers->to[0]->personal)) {
					$mail_data['username'] = $email_headers->to[0]->personal;
				} else {
					$mail_data['username'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
				}
				$mail_data['email_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
			} else {
				if (isset($email_headers->from[0]->personal)) {
					$mail_data['username'] = $email_headers->from[0]->personal;
				} else {
					$mail_data['username'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
				}
				$mail_data['email_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
			}

			$mail_data['mail_cc'] = array();
			if(isset($email_headers->cc) && count($email_headers->cc) > 0){
				foreach($email_headers->cc as $cc_row){
					if($mail_data['email_address'] != $cc_row->mailbox.'@'.$cc_row->host){
						$mail_data['mail_cc'][] = $cc_row->mailbox.'@'.$cc_row->host;
					}
				}
			}
			if(isset($email_headers->to) && count($email_headers->to) > 1){
				foreach($email_headers->to as $key=>$cc_row){
					if($this->email_address != $cc_row->mailbox.'@'.$cc_row->host && $mail_data['email_address'] != $cc_row->mailbox.'@'.$cc_row->host){
						$mail_data['mail_cc'][] = $cc_row->mailbox.'@'.$cc_row->host;
					}
				}
			}

			$mail_data['attachments'] = $this->get_mail_attachments($mb, $message_no);
			$mail_data['mail_subject'] = isset($email_headers->subject) ? $email_headers->subject : '';
			$mail_data['mail_bcc'] = 0;
			$mail_data['prev_mail_no'] = 0;
			$mail_data['next_mail_no'] = 0;
			$mail_data['mail_mailbox'] = $mailbox_name;
			$mail_data['mail_datetime'] = $this->timeAgo($email_headers->date);
			$mail_data['mail_body'] = $body;
			$mail_data['message_no'] = $message_no;
			$mail_data['mailboxes'] = $this->get_mailboxes($mb);
			imap_close($mb);
		}
		return $mail_data;
	}

	/**
	 * @param $mb
	 * @param $message_no
	 * @return array
	 */
	function get_mail_attachments($mb, $message_no)
	{
		$attachments = array();
		$structure = imap_fetchstructure($mb, $message_no);
		if (isset($structure->parts) && count($structure->parts)) {
			foreach ($structure->parts as $key => $parts) {
				if (isset($parts->disposition) && $parts->disposition == "ATTACHMENT") {
					$attachments[] = array(
						'filename' => $parts->parameters[0]->value,
						'file' => quoted_printable_decode(imap_fetchbody($mb, $message_no, $key + 1)),
						'subtype' => strtolower($parts->subtype) == 'x-zip' ? 'zip' : strtolower($parts->subtype),
					);
				}
			}
		}
		return $attachments;
	}

	/**
	 * @param $mb
	 * @param $message_no
	 * @return int
	 */
	function get_mail_attachment_status($mb, $message_no)
	{
		$attachments = 0;
		$structure = imap_fetchstructure($mb, $message_no);
		if (isset($structure->parts) && count($structure->parts)) {
			foreach ($structure->parts as $key => $parts) {
				if (isset($parts->disposition) && $parts->disposition == "ATTACHMENT") {
					$attachments = 1;
					break;
				}
			}
		}
		return $attachments;
	}

	function send_mail($to, $subject, $message, $attach = null,$cc = null)
	{
		/*$this->load->library('email');*/
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => "$this->email_address",
			'smtp_pass' => "$this->email_password",
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		if(isset($_POST['request_read_receipt'])){
			$this->email->set_header('Return-Receipt-To',"<$this->email_address>");
			$this->email->set_header('Read-Receipt-To',"<$this->email_address>");
			$this->email->set_header('X-Confirm-Reading-To',"<$this->email_address>");
			$this->email->set_header('Generate-Delivery-Report',"<$this->email_address>");
			$this->email->set_header('Disposition-Notification-To',"<$this->email_address>");
		}
		$this->email->set_newline("\r\n");
        $this->email->reply_to(FROM_EMAIL_ADDRESS);
		$this->email->from('JayKhodiyar', "$this->email_address");
		$this->email->to($to);

		if($cc != null && is_array($cc) && count($cc) > 0){
			$this->email->cc($cc);
		}

		$this->email->subject($subject);
		$this->email->message($message);
        $this->email->bcc(BCC_EMAIL_ADDRESS);
		/*echo "<pre>";print_r($this->email);die;*/
		if ($attach != null && $attach != '') {
			if (is_array($attach)) {
				foreach ($attach as $attach_row) {
					$this->email->attach($attach_row);
				}
			}
		}
		if ($this->email->send()) {
			if ($attach != null && $attach != '') {
				if (is_array($attach)) {
					foreach ($attach as $attach_row) {
						if (file_exists($attach_row)) {
							unlink($attach_row);
						}
					}
				}
			}
			return true;
		} else {
			if ($attach != null && $attach != '') {
				if (is_array($attach)) {
					foreach ($attach as $attach_row) {
						if (file_exists($attach_row)) {
							unlink($attach_row);
						}
					}
				}
			}
			/*echo $this->email->print_debugger();*/
			return false;
		}
	}

	function limit_words($string, $word_limit = 30)
	{
		$words = explode(" ", $string);
		return implode(" ", array_splice($words, 0, $word_limit));
	}

	function limit_character($string, $character_limit = 30)
	{
		if (strlen($string) > $character_limit) {
			return substr($string, 0, $character_limit) . '...';
		} else {
			return $string;
		}
	}

	function mail_forward($from_folder_name = '')
	{
		if (isset($_POST['mails_no']) && count($_POST['mails_no']) > 0) {
			$mails_no = $_POST['mails_no'];
			$from_folder_name = $this->getServerFolderName($from_folder_name);
			$mb = imap_open($this->email_server . $from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			if ($messageCount > 0) {
				$no = $mails_no[0];
				imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', "Mail successfully forwarded!");
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please select mail! ');
			redirect($this->agent->referrer());
		}
	}

	function mail_replay($from_folder_name = '')
	{
		if (isset($_POST['mails_no']) && count($_POST['mails_no']) > 0) {
			$mails_no = $_POST['mails_no'];
			$from_folder_name = $this->getServerFolderName($from_folder_name);
			$mb = imap_open($this->email_server . $from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			if ($messageCount > 0) {
				foreach ($mails_no as $no) {
					$uid = imap_uid($mb, $no) or die(imap_last_error() . "<br>Connection Faliure!");
					$this->move_mail($mb, $no, 'INBOX.Trash');
				}
				imap_expunge($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
				imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', "Mail successfully moved to trash!");
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please select mail! ');
			redirect($this->agent->referrer());
		}
	}

	function create_folder()
	{
		if (isset($_POST['folder_name']) && isset($_POST['parent_folder']) && $_POST['folder_name'] != '' && strlen($_POST['folder_name']) > 0) {
			$mb = imap_open($this->email_server, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$folder_name = ucfirst($_POST['folder_name']);
			$parent_folder = $_POST['parent_folder'];
			$message = '';
			error_reporting(0);
			if ($parent_folder == 1) {
				$mailbox = $this->email_server . $folder_name;
				imap_createmailbox($mb, $mailbox) or $message = imap_last_error();
				imap_subscribe($mb, $mailbox) or $message = imap_last_error();
			} else {
				$parent_folder = $this->getServerFolderName($parent_folder);
				$mailbox = $this->email_server . $parent_folder . "/" . $folder_name;
				imap_createmailbox($mb, $mailbox) or $message = imap_last_error();
				if ($message != '') {
					echo json_encode(array('success' => false, 'message' => $message));
					exit();
				}
				imap_subscribe($mb, $mailbox) or $message = imap_last_error();
			}
			if ($message == '') {
				$mailboxes = $this->get_mailboxes($mb);
				$sidebar_mail_label = $this->load->view('mail_module/sidebar_mail_label', array('mailboxes' => $mailboxes, 'current_folder_name' => $_POST['current_folder_name']), true);
				$move_to_folder = $this->load->view('mail_module/move_to_folder_view', array('mailboxes' => $mailboxes, 'current_folder_name' => $_POST['current_folder_name']), true);
				$folder_dropdown_option = $this->load->view('mail_module/folder_dropdown_view', array('mailboxes' => $mailboxes), true);
				echo json_encode(array('success' => true, 'message' => 'Folder created successfully.', 'sidebar_mail_label' => $sidebar_mail_label, 'move_to_folder' => $move_to_folder, 'folder_dropdown_option' => $folder_dropdown_option));
			} else {
				echo json_encode(array('success' => false, 'message' => $message));
			}
			imap_close($mb);
		} else {
			echo json_encode(array('success' => false, 'message' => 'Something wrong, Please try again.'));
		}
	}

	function mail_move_to()
	{
		$folder_name = $_POST['folder_name'];
		$folder_name_ori = $folder_name;
		$folder_name = $this->getServerFolderName($folder_name);
		if (isset($_POST['mails_no']) && count($_POST['mails_no']) > 0) {
			$mails_no = $_POST['mails_no'];
			$from_folder_name = $from_folder_name_ori = $_POST['from_folder_name'];
			$from_folder_name = $this->getServerFolderName($from_folder_name);
			$mb = imap_open($this->email_server . $from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			if ($messageCount > 0) {
				foreach ($mails_no as $no) {
					$uid = imap_uid($mb, $no) or die(imap_last_error() . "<br>Connection Faliure!");
					$this->move_mail($mb, $no, $folder_name);
				}
				imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', "Mail successfully moved to $folder_name_ori!");
				$this->updateConfigItem('folder_name', $from_folder_name_ori);
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
				$this->updateConfigItem('folder_name', $folder_name_ori);
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please select at least one mail! ');
			$this->updateConfigItem('folder_name', $folder_name_ori);
			redirect($this->agent->referrer());
		}
	}

	function fetch_mails($mailbox_name = 'INBOX')
	{
		$mails_data = array();
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$messageCount = imap_num_msg($mb);
		$mail_status = imap_status($mb, $this->email_server . $mailbox_name, SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!");
		$mails_data['count_unseen_mails'] = 0;
		$mails_data['inbox_unseen_mails'] = $this->count_inbox_unread_mail();
		$mails_data['total_mails'] = $mail_status->messages;
		$mails_data['mails_range'] = $messageCount > 0 ? "1-$messageCount" : "0-$messageCount";
		$mails_data['mailboxes'] = $this->get_mailboxes($mb);
		if ($mail_status->messages > 0) {
			$message_nos = imap_sort($mb, SORTDATE, 1) or die(imap_last_error() . "<br>Connection Faliure11!");
			foreach ($message_nos as $key => $message_no) {
				$email_headers = imap_headerinfo($mb, $message_no);
				/*-------- Count Unread Mails -----------------*/
				if ($email_headers->Unseen == 'U') {
					$mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
					$mails_data['mails'][$key]['is_unread'] = 1;
				} else {
					$mails_data['mails'][$key]['is_unread'] = 0;
				}
				$mails_data['mails'][$key]['attachment_status'] = $this->get_mail_attachment_status($mb, $message_no);;
				$mails_data['mails'][$key]['received_at'] = $this->timeAgo($email_headers->date);
				$mails_data['mails'][$key]['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
				$mails_data['mails'][$key]['subject'] = $email_headers->subject;
				if ($email_headers->fromaddress != $this->email_address) {
					if (isset($email_headers->from[0]->personal)) {
						$mails_data['mails'][$key]['username'] = $email_headers->from[0]->personal;
					} else {
						$mails_data['mails'][$key]['username'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
					}
				} else {
					if (isset($email_headers->to[0]->personal)) {
						$mails_data['mails'][$key]['username'] = $email_headers->to[0]->personal;
					} else {
						$mails_data['mails'][$key]['username'] = $email_headers->to[0]->mailbox . '@' . $email_headers->from[0]->host;
					}
				}
				$mails_data['mails'][$key]['mail_no'] = (int)trim($email_headers->Msgno);
			}
		}
		imap_close($mb);
		return $mails_data;
	}

	function get_mail_list($mailbox_name, $return_html = false, $curr_page = 1)
	{
		$mails_data = array();
		$mailbox_name = isset($_POST['mailbox']) ? $_POST['mailbox'] : $mailbox_name;
		$curr_page = isset($_POST['page_index']) ? $_POST['page_index'] : $curr_page;

		$current_folder_name = $mailbox_name;
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$message_count = imap_num_msg($mb);
		$mails_data['mailboxes'] = $this->get_mailboxes($mb);
		$mails_data['count_unseen_mails'] = 0;
		$mails_data['inbox_unseen_mails'] = $this->count_inbox_unread_mail();
		$mails_data['total_mails'] = $message_count;

		/*-------- Pagination -----------*/
		if ($message_count > $this->per_page) {
			$last_page = ceil($message_count / $this->per_page);
			if ($curr_page == 1) {
				$start = 1;
				$end = $start + ($this->per_page - 1);
				$mails_data['mails_range'] = "$start-$end";
				$mails_data['next_page_index'] = 2;
				$mails_data['prev_page_index'] = 0;
			} elseif ($last_page == $curr_page) {
				$start = (($curr_page - 1) * $this->per_page) + 1;
				$end = $message_count;
				$mails_data['mails_range'] = "$start-$end";
				$mails_data['next_page_index'] = 0;
				$mails_data['prev_page_index'] = $curr_page - 1;
			} else {
				$start = (($curr_page - 1) * $this->per_page) + 1;
				$end = $start + ($this->per_page - 1);
				$mails_data['mails_range'] = "$start-$end";
				$mails_data['next_page_index'] = $curr_page + 1;
				$mails_data['prev_page_index'] = $curr_page - 1;
			}
		} else {
			$start = 1;
			$end = $message_count;
			$mails_data['mails_range'] = "1-$message_count";
			$mails_data['next_page_index'] = 0;
			$mails_data['prev_page_index'] = 0;
		}
		/*-------- /Pagination -----------*/

		if ($end > 0) {
			$message_nos = imap_sort($mb, SORTDATE, 1) or die(imap_last_error() . "<br>Connection Faliure11!");
			foreach ($message_nos as $key => $message_no) {
				if (($start - 1) <= $key && $key <= ($end - 1)) {
					$email_headers = imap_headerinfo($mb, $message_no);
					/*-------- Count Unread Mails -----------------*/
					if ($email_headers->Unseen == 'U') {
						$mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
						$mails_data['mails'][$key]['is_unread'] = 1;
					} else {
						$mails_data['mails'][$key]['is_unread'] = 0;
					}
					$mails_data['mails'][$key]['attachment_status'] = $this->get_mail_attachment_status($mb, $message_no);;
					$mails_data['mails'][$key]['received_at'] = $this->timeAgo($email_headers->date);
					$mails_data['mails'][$key]['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
					$mails_data['mails'][$key]['subject'] = $email_headers->subject;
					$from_email = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
					if ($from_email != $this->email_address) {
						if (isset($email_headers->from[0]->personal)) {
							$mails_data['mails'][$key]['username'] = $email_headers->from[0]->personal;
						} else {
							$mails_data['mails'][$key]['username'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
						}
					} else {
						if (isset($email_headers->to[0]->personal)) {
							$mails_data['mails'][$key]['username'] = $email_headers->to[0]->personal;
						} else {
							$mails_data['mails'][$key]['username'] = $email_headers->to[0]->mailbox . '@' . $email_headers->from[0]->host;
						}
					}
					$mails_data['mails'][$key]['mail_no'] = (int)trim($email_headers->Msgno);
				}
			}
		}
		imap_close($mb);
		if ($return_html == true) {
			$mail_list_html = $this->load->view('mail_module/mail_list_html', array('mails_data' => $mails_data, 'current_folder_name' => $current_folder_name), true);
			$pagination_html = '';
			$pagination_html .= $mails_data['mails_range'] . '/' . $mails_data['total_mails'];
			$pagination_html .= '<div class="btn-group">';
			$pagination_html .= '<button type="button" class="btn btn-default btn-sm btn-mail-list-prev-page" data-page_index="' . $mails_data['prev_page_index'] . '"><i class="fa fa-chevron-left"></i></button>';
			$pagination_html .= '<button type="button" class="btn btn-default btn-sm btn-mail-list-next-page" data-page_index="' . $mails_data['next_page_index'] . '"><i class="fa fa-chevron-right"></i></button>';
			$pagination_html .= '</div>';
			echo json_encode(array('mail_list_html' => $mail_list_html, 'pagination_html' => $pagination_html));
			exit();
		} else {
			return $mails_data;
		}
	}

	function set_starred_flag()
	{
		$mailbox_name = $_POST['folder_name'];
		$message_no = (int)trim($_POST['message_no']);
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		/*echo $mailbox_name;die;*/
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$status = imap_setflag_full($mb, $message_no, "\\Flagged");
		echo $status;
		exit();
	}

	function remove_starred_flag()
	{
		$mailbox_name = $_POST['folder_name'];
		$message_no = (int)trim($_POST['message_no']);
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$status = imap_clearflag_full($mb, $message_no, "\\Flagged") or die(imap_last_error() . "<br>Connection Faliure!");
		echo $status;
		exit();
	}

	function timeAgo($time_ago)
	{
		$time_ago = strtotime($time_ago);
		$cur_time = time();
		$time_elapsed = $cur_time - $time_ago;
		$seconds = $time_elapsed;
		$minutes = round($time_elapsed / 60);
		$hours = round($time_elapsed / 3600);
		$days = round($time_elapsed / 86400);
		$weeks = round($time_elapsed / 604800);
		$months = round($time_elapsed / 2600640);
		$years = round($time_elapsed / 31207680);
		// Seconds
		if ($seconds <= 60) {
			return "just now";
		} //Minutes
		else if ($minutes <= 60) {
			if ($minutes == 1) {
				return "one minute ago";
			} else {
				return "$minutes minutes ago";
			}
		} //Hours
		else if ($hours <= 24) {
			if ($hours == 1) {
				return "an hour ago";
			} else {
				return "$hours hrs ago";
			}
		} //Days
		else if ($days <= 7) {
			if ($days == 1) {
				return "yesterday";
			} else {
				return "$days days ago";
			}
		} //Weeks
		else if ($weeks <= 4.3) {
			if ($weeks == 1) {
				return "a week ago";
			} else {
				return "$weeks weeks ago";
			}
		} //Months
		else if ($months <= 12) {
			if ($months == 1) {
				return "a month ago";
			} else {
				return "$months months ago";
			}
		} //Years
		else {
			if ($years == 1) {
				return "one year ago";
			} else {
				return "$years years ago";
			}
		}
	}

	function count_inbox_unread_mail()
	{
		$mb = imap_open($this->email_server . "INBOX", $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$mailbox_info = imap_mailboxmsginfo($mb);
		return $mailbox_info->Unread;
	}

	function search_mails()
	{
		$mailbox_name = $mailbox_name_ori = $_POST['mailbox_name'];
		$search = trim($_POST['search']);
		$mails_data = array();
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		if ($search != '') {
			//$search_results = imap_search($mb,"TEXT $search") or die(imap_last_error() . "<br>Connection Faliure!");
			$search_results = imap_search($mb, 'ALL') or die(imap_last_error() . "<br>Connection Faliure!");
		} else {
			$search_results = imap_search($mb, 'ALL') or die(imap_last_error() . "<br>Connection Faliure!");
		}
		$message_count = imap_num_msg($mb);
		$mails_data['count_unseen_mails'] = 0;
		$mails_data['inbox_unseen_mails'] = $this->count_inbox_unread_mail();
		$mails_data['total_mails'] = $message_count;
		$mails_data['mails_range'] = $message_count > 0 ? "1-$message_count" : "0-0";
		$mails_data['mailboxes'] = $this->get_mailboxes($mb);
		if ($message_count > 0) {
			$sort_results = imap_sort($mb, SORTDATE, 1) or die(imap_last_error() . "<br>Connection Faliure11!");
			$message_nos = $this->order_search($search_results, $sort_results);
			foreach ($message_nos as $key => $message_no) {
				$email_headers = imap_headerinfo($mb, $message_no);
				/*-------- Count Unread Mails -----------------*/
				if ($email_headers->Unseen == 'U') {
					$mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
					$mails_data['mails'][$key]['is_unread'] = 1;
				} else {
					$mails_data['mails'][$key]['is_unread'] = 0;
				}
				$mails_data['mails'][$key]['attachment_status'] = $this->get_mail_attachment_status($mb, $message_no);;
				$mails_data['mails'][$key]['received_at'] = $this->timeAgo($email_headers->date);
				$mails_data['mails'][$key]['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
				$mails_data['mails'][$key]['subject'] = $email_headers->subject;
				if ($email_headers->fromaddress != $this->email_address) {
					if (isset($email_headers->from[0]->personal)) {
						$mails_data['mails'][$key]['username'] = $email_headers->from[0]->personal;
					} else {
						$mails_data['mails'][$key]['username'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
					}
				} else {
					if (isset($email_headers->to[0]->personal)) {
						$mails_data['mails'][$key]['username'] = $email_headers->to[0]->personal;
					} else {
						$mails_data['mails'][$key]['username'] = $email_headers->to[0]->mailbox . '@' . $email_headers->from[0]->host;
					}
				}
				$mails_data['mails'][$key]['mail_no'] = (int)trim($email_headers->Msgno);
			}
		}
		imap_close($mb);
		set_page('mail_module/mailbox', array('mails_data' => $mails_data, 'search' => $search, 'curr_folder_name' => $mailbox_name_ori));
	}

	/**
	 * @param $SearchResults
	 * @param $SortResults
	 * @return array
	 */
	function order_search($SearchResults, $SortResults)
	{
		return array_values(array_intersect($SearchResults, $SortResults));
	}

	function search_mails_v1()
	{
		$mailbox_name = $mailbox_name_ori = $_POST['mailbox_name'];
		$search = $_POST['search'];
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$Properties = array();
		$Properties['count_unseen_mails'] = 0;
		$mail_status = imap_status($mb, $this->email_server . $mailbox_name, SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!");
		$Properties['total_mails'] = 0;
		$Properties['mailboxes'] = $this->get_mailboxes($mb);
		if ($search != '') {
			$emails = imap_search($mb, 'TEXT "' . $search . '"');
		} else {
			$emails = imap_search($mb, 'ALL');
		}

		foreach ($emails as $MID) {
			$Properties['total_mails'] = $Properties['total_mails'] + 1;
			$EmailHeaders = imap_headerinfo($mb, $MID);
			if ($EmailHeaders->Unseen == 'U') {
				$Properties['count_unseen_mails'] = $Properties['count_unseen_mails'] + 1;
			}
			$DateReceived = $EmailHeaders->Date;
			$Body = imap_fetchbody($mb, $MID, 1.1);
			$Properties['mails'][$MID]['main_body'] = imap_qprint(imap_fetchbody($mb, $MID, 2));
			if (!strlen($Body) > 0) {
				$Body = imap_fetchbody($mb, $MID, 1);
			}
			$structure = imap_fetchstructure($mb, $MID);
			$attachments = array();
			if (isset($structure->parts) && count($structure->parts)) {
				for ($i = 0; $i < count($structure->parts); $i++) {
					$attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
					$attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
					if ($structure->parts[$i]->ifdparameters) {
						foreach ($structure->parts[$i]->dparameters as $object) {
							if (strtolower($object->attribute) == 'filename') {
								$attachments[$i]['is_attachment'] = true;
								$attachments[$i]['filename'] = $object->value;
							}
						}
					}
					if ($attachments[$i]['is_attachment']) {
						$Properties['mails'][$MID]['AttachmentStatus'] = 1;
					} else {
						$Properties['mails'][$MID]['AttachmentStatus'] = 0;
					}
				}
			}
			$Properties['mails_range'] = $Properties['total_mails'] > 0 ? "1-" . $Properties['total_mails'] : "0-" . $Properties['total_mails'];
			$Properties['mails'][$MID]['FromEmailAddress'] = $this->parseString($Body, "[mailto:", "Sent:", 8, -4);
			$Properties['mails'][$MID]['DateReceived'] = $this->timeAgo($DateReceived);
			$Properties['mails'][$MID]['EmailTemplate'] = $Body;//$this->limit_character($Body,50);
			$Properties['mails'][$MID]['EmailHeaders'] = $EmailHeaders;
			if ($Properties['mails'][$MID]['FromEmailAddress'] == '') {
				$email_matches = array();
				$pattern = '/[A-Za-z0-9_.-]+@[A-Za-z0-9_-]+\.([A-Za-z0-9_-][A-Za-z0-9_.]+)/';
				preg_match_all($pattern, $Body, $email_matches);
				if (!empty($email_matches[0])) {
					$Properties['mails'][$MID]['FromEmailAddress'] = end($email_matches[0]);
				}
			}
			$Properties['mails'][$MID]['MID'] = $MID;
		}
		imap_close($mb);
		$mails_data = $Properties;
		set_page('mail_module/mailbox', array('mails_data' => $mails_data, 'search' => $search, 'curr_folder_name' => $mailbox_name_ori));
	}

	function delete_mail()
	{
		$from_folder_name = $_POST['folder_name'];
		$message_no = $_POST['message_no'];
		$from_folder_name = $this->getServerFolderName($from_folder_name);
		$mb = imap_open($this->email_server . $from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		$messageCount = imap_num_msg($mb);
		if ($messageCount > 0) {
			$uid = imap_uid($mb, $message_no) or die(imap_last_error() . "<br>Connection Faliure!");
			if ($from_folder_name == "[Gmail]/Trash") {
				imap_delete($mb, $message_no);
				imap_expunge($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
			} else {
				$this->move_mail($mb, $message_no, '[Gmail]/Trash');
			}
			imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', "Mail successfully moved to trash!");
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
			redirect($this->agent->referrer());
		}
	}

	function delete_multi_mails()
	{
		if (isset($_POST['mails_no']) && count($_POST['mails_no']) > 0) {
			$from_folder_name = $_POST['from_folder_name'];
			$mails_no = $_POST['mails_no'];
			/*print_r($mails_no);
			die;*/
			$from_folder_name = $this->getServerFolderName($from_folder_name);
			$mb = imap_open($this->email_server . $from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			if ($messageCount > 0) {
				$nos = array();
				foreach ($mails_no as $key => $no) {
					if ($from_folder_name == $this->email_server . "[Gmail]/Trash") {
						/*imap_delete($mb,$no);
						  imap_expunge($mb) or die(imap_last_error()."<br>Connection Faliure!");*/
					} else {
						imap_mail_move($mb, $no, '[Gmail]/Trash') or die(imap_last_error() . "<br>Connection Faliure!");
						$nos['moved'][] = $no;
					}
					$nos['all'][] = $no;
				}
				imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");
				/* echo "<pre>";
				print_r($nos);
				die; */
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', "Mail successfully moved to trash!");
				redirect("mailbox/trash");
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please select at least one mail! ');
			redirect($this->agent->referrer());
		}
	}

	function get_mailboxes($mailbox_stream = '')
	{
		$mailbox_stream_or = $mailbox_stream;
		if ($mailbox_stream == '') {
			$mailbox_stream = $mb = imap_open($this->email_server, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
		}
		$mailboxes = imap_getmailboxes($mailbox_stream, $this->email_server, '*') or die(imap_last_error() . "<br>Connection Faliure!");
		$mailbox_array = array();

		$default_mailboxes = array(
			$this->email_server . 'INBOX',
			$this->email_server . '[Gmail]',
			$this->email_server . '[Gmail]/All Mail',
			$this->email_server . '[Gmail]/Drafts',
			$this->email_server . '[Gmail]/Sent Mail',
			$this->email_server . '[Gmail]/Spam',
			$this->email_server . '[Gmail]/Starred',
			$this->email_server . '[Gmail]/Trash',
			$this->email_server . '[Gmail]/Important',
		);

		/*echo "<pre>";
		print_r($mailboxes);die;*/
		foreach ($mailboxes as $mailbox) {
			if (!in_array($mailbox->name, $default_mailboxes)) {
				$mailbox_label = str_replace($this->email_server, '', $mailbox->name);
				$mailbox_label_array = explode('/', $mailbox_label);
				if (count($mailbox_label_array) > 1) {
					$mailbox_label = '';
					foreach ($mailbox_label_array as $key => $item) {
						if ($key > 0) {
							$mailbox_label .= "/";
						}
						$mailbox_label .= $item;
					}
				}
				$mailbox_array[] = array('mailbox_label' => $mailbox_label, 'mailbox_name' => $mailbox->name);
			}
		}
		if ($mailbox_stream_or == '') {
			echo "<pre>";
			print_r($mailbox_array);

		} else {
			return $mailbox_array;
		}
	}

	/**
	 * @param $mailbox_stream
	 * @param $mail_no
	 * @param $move
	 * @return bool
	 */
	function move_mail($mailbox_stream, $mail_no, $move)
	{
		return imap_mail_move($mailbox_stream, $mail_no, $move) or die(imap_last_error() . "<br>Connection Faliure!");
	}

	function parseString($Body, $Start, $End, $LenghtVar, $EndVar = 0)
	{
		$StartPos = strpos($Body, $Start);
		$StartPos = ($StartPos + $LenghtVar);
		if ($End == 'end_mail') {
			$EndPos = strlen($Body);
		} else {
			$EndPos = strpos($Body, $End);
		}

		$Property = "";
		if (($StartPos - $LenghtVar) > 0 && $EndPos > $StartPos) {
			$EndPos = ($EndPos - $StartPos) + $EndVar;
			$Property = substr($Body, $StartPos, $EndPos);
		}
		$Property = $this->removeSpecialCharacters($Property);
		return $Property;
	}

	function getStateCode($address)
	{
		if (!empty($address)) {
			$addressArray = explode(',', $address);
			$stateWithZip = end($addressArray);
			$states = array(
				'AL' => array('name' => 'Alabama', 'id' => ''),
				'AK' => 'Alaska', array('name' => 'Alaska', 'id' => ''),
				'AZ' => array('name' => 'Arizona', 'id' => 61),
				'AR' => array('name' => 'Arkansas', 'id' => ''),
				'CA' => array('name' => 'California', 'id' => 67),
				'CO' => array('name' => 'Colorado', 'id' => 68),
				'CT' => array('name' => 'Connecticut', 'id' => ''),
				'DE' => array('name' => 'Delaware', 'id' => 388),
				'DC' => array('name' => 'District of Columbia', 'id' => ''),
				'FL' => array('name' => 'Florida', 'id' => 70),
				'GA' => array('name' => 'Georgia', 'id' => 71),
				'HI' => array('name' => 'Hawaii', 'id' => ''),
				'ID' => array('name' => 'Idaho', 'id' => ''),
				'IL' => array('name' => 'Illinois', 'id' => 63),
				'IN' => array('name' => 'Indiana', 'id' => 419),
				'IA' => array('name' => 'Iowa', 'id' => ''),
				'KS' => array('name' => 'Kansas', 'id' => ''),
				'KY' => array('name' => 'Kentucky', 'id' => ''),
				'LA' => array('name' => 'Louisiana', 'id' => ''),
				'ME' => array('name' => 'Maine', 'id' => ''),
				'MD' => array('name' => 'Maryland', 'id' => ''),
				'MA' => array('name' => 'Massachusetts', 'id' => ''),
				'MI' => array('name' => 'Michigan', 'id' => ''),
				'MN' => array('name' => 'Minnesota', 'id' => 62),
				'MS' => array('name' => 'Mississippi', 'id' => ''),
				'MO' => array('name' => 'Missouri', 'id' => ''),
				'MT' => array('name' => 'Montana', 'id' => ''),
				'NV' => array('name' => 'Nevada', 'id' => 65),
				'NE' => array('name' => 'Nebraska', 'id' => ''),
				'NH' => array('name' => 'New Hampshire', 'id' => ''),
				'NJ' => array('name' => 'New Jersey', 'id' => ''),
				'NM' => array('name' => 'New Mexico', 'id' => ''),
				'NY' => array('name' => 'New York', 'id' => ''),
				'NC' => array('name' => 'North Carolina', 'id' => 64),
				'ND' => array('name' => 'North Dakota', 'id' => ''),
				'OH' => array('name' => 'Ohio', 'id' => 450),
				'OK' => array('name' => 'Oklahoma', 'id' => ''),
				'OR' => array('name' => 'Oregon', 'id' => 481),
				'PA' => array('name' => 'Pennsylvania', 'id' => ''),
				'RI' => array('name' => 'Rhode Island', 'id' => ''),
				'SC' => array('name' => 'South Carolina', 'id' => ''),
				'SD' => array('name' => 'South Dakota', 'id' => ''),
				'TN' => array('name' => 'Tennessee', 'id' => 512),
				'TX' => array('name' => 'Texas', 'id' => 66),
				'UT' => array('name' => 'Utah', 'id' => 1889),
				'VT' => array('name' => 'Vermont', 'id' => ''),
				'VA' => array('name' => 'Virginia', 'id' => ''),
				'WA' => array('name' => 'Washington', 'id' => 69),
				'WV' => array('name' => 'West Virginia', 'id' => ''),
				'WI' => array('name' => 'Wisconsin', 'id' => ''),
				'WY' => 'Wyoming', array('name' => 'Wyoming', 'id' => ''),
			);

			if ($stateWithZip) {
				foreach ($states as $key => $val) {
					if (strstr($stateWithZip, $key)) {
						return $val['id'];
					}
				}
			}
		}
	}

	function removeSpecialCharacters($Property)
	{
		$Property = str_replace("=20", "", $Property);
		$Property = str_replace("=", "", $Property);
		$Property = trim($Property);
		return $Property;
	}

	function upload_attachment()
	{
		$this->load->library('upload');
		$config['upload_path'] = 'resource/uploads/attachments';
		$config['allowed_types'] = 'gif|jpg|png|txt|pdf|zip';
		$config['overwrite'] = TRUE;
		$this->upload->initialize($config);
		$file_data = $this->upload->data();
		if ($this->upload->do_upload('upload_attachment')) {
			$file_data = $this->upload->data();
			$file_url = $file_data['file_name'];
			echo json_encode(array('success' => true, 'message' => 'Attachment successfully uploaded.', 'attachment_url' => 'resource/uploads/attachments/' . $file_url));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Wrong with upload operation!', 'error' => $this->upload->display_errors()));
			exit();
		}
	}

	function getConfigValue($config_key)
	{
		$this->db->select('config_value');
		$this->db->from('config');
		$this->db->where('config_key', $config_key);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->config_value;
		} else {
			return '';
		}
	}

	function updateConfigItem($config_key, $config_value)
	{
		$this->db->where('config_key', $config_key);
		$this->db->update('config', array("config_value" => $config_value));
		return true;
	}

	function settings()
	{
		$this->db->select('setting_key,setting_value');
		$this->db->from('mailbox_settings');
		$query = $this->db->get();
		$setting_data = array();
		foreach ($query->result() as $setting) {
			$setting_data[$setting->setting_key] = $setting->setting_value;
		}
		$settings_html = $this->load->view('mail_module/settings',$setting_data, true);
		echo json_encode(array('success' => true, 'message' => "Successfully load settings!", 'settings_html' => $settings_html));
		exit();
	}

	function save_settings(){
		$setting_data = $_POST;
		foreach($setting_data as $setting_key=>$setting_value){
			$this->db->where('setting_key',$setting_key);
			$this->db->update('mailbox_settings',array('setting_value'=>$setting_value));
		}
		echo json_encode(array('success'=>true,'message'=>'Settings save successfully!'));
		exit();
	}

	/**
	 * @return array
	 */
	function load_settings()
	{
		$this->db->select('setting_key,setting_value');
		$this->db->from('mailbox_settings');
		$query = $this->db->get();
		$setting_data = array();
		foreach ($query->result() as $setting) {
			$setting_data[$setting->setting_key] = $setting->setting_value;
		}
		return $setting_data;
	}

	function mail_test(){
		$to = "cshailesh157@gmail.com";
		$subject = "HTML email";

		$message = "
		<html>
		<head>
		<title>HTML email</title>
		</head>
		<body>
		<p>This email contains HTML Tags!</p>
		<table>
		<tr>
		<th>Firstname</th>
		<th>Lastname</th>
		</tr>
		<tr>
		<td>John</td>
		<td>Doe</td>
		</tr>
		</table>
		</body>
		</html>
		";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <test.presntr@gmail.com>' . "\r\n";
		$headers .= "Disposition-Notification-To: test.presntr@gmail.com\r\n";
		$headers .= "X-Confirm-Reading-To: test.presntr@gmail.com\r\n";
		$mail_status = mail($to,$subject,$message,$headers);
		if($mail_status){
			echo "success";
		}else{
			echo "fail";
		}
	}
}
