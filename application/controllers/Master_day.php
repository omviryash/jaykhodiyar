<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_hr extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
	}
	
	/* function index()
	{
		
	} */
	
	function day($day_id)
	{
		$day = array();
		if(isset($day_id) && !empty($day_id)){ 
			$where_array['day_id'] = $day_id;
			$day = $this->crud->get_all_with_where('days','day_name','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('days', 'day_id', 'ASC');
		set_page('master_day/day', array('results' => $result_data, 'day_id' => $day[0]->day_id, 'day_name' => $day[0]->day_name));
	}
	
	function add_day()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('day',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Day Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_day()
	{
		$post_data = $this->input->post();
		$where_array['day_id'] = $postdata['day_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('day', $post_data, array('day_id' => $post_data['day_id']));
		if ($result) {
			$post_data['day_id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Day Updated Successfully');
			echo json_encode($post_data);
		}
	}
		
	/*function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
	}*/
	
}