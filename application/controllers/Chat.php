<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * Class Party
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 * @property Chatmodule $chatmodule
 */
class Chat extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->load->model('chatmodule');
        $this->load->model('Crud', 'crud');
        $this->load->helper('date');
        if (!$this->session->userdata('is_logged_in')) {
            redirect('/auth/login/');
        }
    }

    function getAdminChat() {
        $logged_in = $this->session->userdata("is_logged_in");
        $staff_id = $logged_in['staff_id'];
        if (isset($logged_in['lastid'])) {
            $lastid = $logged_in['lastid'];
        } else {
            $lastid = 0;
        }
        $userid = $_POST['userid'];
        $messages = $this->chatmodule->getMyMessage($staff_id, $lastid, $userid);
        if ($messages) {
            if (isset($logged_in['lastid'])) {
                $logged_in['lastid'] = $messages[count($messages) - 1]['id'];
            } else {
                $logged_in['lastid'] = $messages[count($messages) - 1]['id'];
            }
            $this->session->set_userdata('is_logged_in', $logged_in);
        }
        echo json_encode($messages);
        exit;
    }

    function getBackendSideVisitorChat() {
        $logged_in = $this->session->userdata("is_logged_in");
        if (isset($logged_in['lastid'])) {
            $last_id = $logged_in['lastid'];
        } else {
            $last_id = 0;
        }
        $message_data = $this->chatmodule->getVisitorMessageBackend($last_id);

        if (count($message_data['messages']) > 0) {
            if (isset($logged_in['lastid'])) {
                $logged_in['lastid'] = $message_data['messages'][count($message_data['messages']) - 1]['id'];
            } else {
                $logged_in['lastid'] = $message_data['messages'][count($message_data['messages']) - 1]['id'];
            }
            $this->session->set_userdata('is_logged_in', $logged_in);
        }
        $logged_in = $this->session->userdata("is_logged_in");
        echo json_encode($message_data['messages']);
        exit;
    }

    function send_visitor_msg() {
        $data = array();
        $logged_in = $this->session->userdata("is_logged_in");
        $data['from_id'] = $logged_in['staff_id'];
        $data['from_table'] = $_POST['from_table'];
        $data['to_id'] = $this->chatmodule->fetch_visitor_by_session_id($_POST['to_id']);
        $data['to_table'] = $_POST['to_table'];
        $data['message'] = $_POST['msg'] ? $_POST['msg'] : '';
        $data['date_time'] = date('Y-m-d H:i:s');
        
        if($this->session->userdata('visitor_first_message') == '1' && $_POST['to_table'] == 'visitors'){
            $visitor_last_message = $this->chatmodule->getVisitorLastMessage($logged_in['staff_id'],$_POST['to_id']);
            if(!empty($visitor_last_message)){
                $visitor_last_message_date_time = $visitor_last_message[0]['date_time'];
                $datetime1 = new DateTime($data['date_time']);
                $datetime2 = new DateTime($visitor_last_message_date_time);
                $interval = $datetime1->diff($datetime2);
                $response_time = $interval->format('%H:%i:%s');
                
                $response_arr = array();
                $response_arr['staff_id'] = $logged_in['staff_id'];
                $response_arr['visitor_id'] = $data['to_id'];
                $response_arr['response_time'] = $response_time;
                $response_arr['created_at'] = date('Y-m-d H:i:s');
                $this->crud->insert('visitor_response', $response_arr);
            }
        }
        
        $last_message_id = $this->chatmodule->addChatMessage($data);
        $logged_in['visitor_last_message_id'] = $last_message_id;
        $this->session->set_userdata('is_logged_in', $logged_in);
        $name = $logged_in['name'];
        $image = $logged_in['image'];
        $chat_html = $this->load->view('shared/right_chat_html', array('name' => $name, 'message' => $_POST['msg'], 'image' => $image), true);
        $other_agent_chat_html = $this->load->view('shared/left_chat_html', array('name' => $name, 'message' => $_POST['msg'], 'image' => $image), true);

        $to_data = $this->chatmodule->fetch_visitor_by_id($data['to_id']);
        $from_data = $this->chatmodule->getStaffById($data['from_id']);

        if($this->session->userdata('visitor_first_message') == '1' && $_POST['to_table'] == 'visitors'){
            $accepted_visitors = $from_data[0]['accepted_visitors'] + 1;
            $this->db->update('staff', array('accepted_visitors' => $accepted_visitors), array('staff_id' => $logged_in['staff_id']));
            $this->session->unset_userdata('visitor_first_message');
        }
        
        $suggested_messages_staff_id = $this->crud->get_id_by_val('text_suggestion', 'staff_id', 'text_suggestion', $data['message']);
        if (empty($suggested_messages_staff_id)) {
            if ($suggested_messages_staff_id != $logged_in['staff_id']) {
                $this->crud->insert('text_suggestion', array('staff_id' => $logged_in['staff_id'], 'text_suggestion' => $data['message']));
            }
        }
        
        $return['from_id'] = $logged_in['staff_id'];

        if ($to_data->name == '') {
            $to_name = 'Visitor';
        } else {
            $to_name = $to_data->name . ' (Visitor)';
        }
        $to_image = 'default.png';
        $to_session_id = $_POST['to_id'];
        $from_name = $from_data[0]['name'];
        $from_image = $from_data[0]['image'];

        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        if($to_data->chat_through == '2'){ // Chat Through WhatsApp
            $client->emit('send_message_chat_to_twilio_whatsapp', ['to_session_id' => $to_session_id,
                'from_id' => $data['from_id'],
                'to_name' => $to_name,
                'to_image' => $to_image,
                'other_agent_chat_html' => $other_agent_chat_html,
                'chat_html' => $chat_html,
                'date_time' => $data['date_time'],
                'msg' => $data['message'],
                'to_id' => $data['to_id'],
                'from_number' => TWILIO_WHATSAPP_NUMBER,
                'to_number' => $to_data->name,
                'msg_text' => $data['message']]);
        } else {
            $client->emit('new_message_client_to_visitor', ['to_session_id' => $to_session_id,
                'from_id' => $data['from_id'],
                'to_name' => $to_name,
                'to_image' => $to_image,
                'other_agent_chat_html' => $other_agent_chat_html,
                'chat_html' => $chat_html,
                'date_time' => $data['date_time'],
                'msg' => $data['message'],
                'to_id' => $data['to_id']]);
        }
        $client->emit('close_visitor_popup', ['to_session_id' => $to_session_id, 'from_id' => $data['from_id']]);
        $client->close();

        echo json_encode(array('success' => true, 'chat_html' => $chat_html, 'return' => $return));
        exit;
    }

    function send_chat_mail() {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => SMTP_EMAIL_ADDRESS,
            'smtp_pass' => SMTP_EMAIL_PASSWORD,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $filename = RESOURCE_IMAGE_URL . "rsz_jk_logo.png";
        $this->email->attach($filename, "inline");
        $cid = $this->email->attachment_cid($filename);
        $to = $this->config->item('adminmail');
        $logged_in = $this->session->userdata("is_logged_in");
        $userid = $_POST['id'];
        $messages = $this->chatmodule->getVisitorMessageForMail($userid);
        $visitordata = $this->chatmodule->getVisitorById($userid);
        $visitordata = $visitordata[0];
        $visitordata['unsubscribe_code'] = $this->applib->incrementalHash(10);
        $this->crud->update('visitors', array('unsubscribe_code' => $visitordata['unsubscribe_code']), array('id' => $visitordata['id']));
        $mail_html_for_admin = $this->load->view('shared/visitor_chat_mail_html', array('messages' => $messages, 'visitordata' => $visitordata, 'cid' => $cid), true);
        $mail_html_for_visitor = $this->load->view('shared/visitor_chat_mail_html', array('messages' => $messages, 'visitordata' => $visitordata, 'cid' => $cid, 'for_visitor' => 'for_visitor'), true);
//        print_r($mail_html); exit;
        if ($messages) {
            $emailmsg = '';
            foreach ($messages as $k => $v) {
                $emailmsg .= $v['name'] . ":" . $v['message'] . "<br/>";
            }
            //$to = $this->config->item('adminmail');
            //$to = $this->config->item('visitorchatmail');
            $to = VISITORCHATMAIL;
            $subject = "Chat Conversation With Visitor";
            $this->email->set_newline("\r\n");
            $this->email->reply_to(FROM_EMAIL_ADDRESS);
            $this->email->from('JayKhodiyar', FROM_EMAIL_ADDRESS);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($mail_html_for_admin);
            $this->email->bcc(BCC_EMAIL_ADDRESS);
            $result = $this->email->send();

            if(!empty($visitordata['email']) && $visitordata['unsubscribe'] == 0){
                $to = $visitordata['email'];
                $subject = "Your Chat Conversation";
                $this->email->set_newline("\r\n");
                $this->email->reply_to(FROM_EMAIL_ADDRESS);
                $this->email->from('JayKhodiyar', FROM_EMAIL_ADDRESS);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($mail_html_for_visitor);
                $this->email->bcc(BCC_EMAIL_ADDRESS);
                $result = $this->email->send();
            }
            
            var_dump($result);
            echo '<br />';
            echo $this->email->print_debugger();

            exit;
        }
    }

    function getSendRecMsg() {
        $userid = $_POST['id'];
        $logged_in = $this->session->userdata("is_logged_in");
        $staff_id = $logged_in['staff_id'];
        $messages = $this->chatmodule->getAllMsg($staff_id, $userid);
        echo json_encode($messages);
        exit;
    }

    function addAdminChat() {
        $data = array();
        //$nowdate = new DateTime('y-m-d H:i:s');
        $nowdate = date('Y-m-d H:i:s');
        $logged_in = $this->session->userdata("is_logged_in");
        $data['from_id'] = $logged_in['staff_id'];
        $data['from_table'] = $_POST['from_table'];
        $data['to_id'] = $_POST['to_id'];
        $data['to_table'] = $_POST['to_table'];
        $data['message'] = $_POST['msg'] ? $_POST['msg'] : '';
        $data['date_time'] = $nowdate;
        $messages = $this->chatmodule->addChatMessage($data);

        $suggested_messages_staff_id = $this->crud->get_id_by_val('text_suggestion', 'staff_id', 'text_suggestion', $data['message']);

        if (empty($suggested_messages_staff_id)) {
            if ($suggested_messages_staff_id != $logged_in['staff_id']) {
                $this->crud->insert('text_suggestion', array('staff_id' => $logged_in['staff_id'], 'text_suggestion' => $data['message']));
            }
        }
        $to_data = $this->chatmodule->getStaffById($data['to_id']);
        $from_data = $this->chatmodule->getStaffById($data['from_id']);
        $to_name = $to_data[0]['name'];
        $to_image = $to_data[0]['image'];
        $from_name = $from_data[0]['name'];
        $from_image = $from_data[0]['image'];

        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        $client->emit('new_message_client', ['to_id' => $data['to_id'], 'to_table' => $data['to_table'], 'to_name' => $to_name, 'to_image' => $to_image, 'from_id' => $data['from_id'], 'from_table' => $data['from_table'], 'from_name' => $from_name, 'from_image' => $from_image, 'msg' => $data['message'], 'date_time' => date('d M Y H:i:s')]);
        $client->close();

        echo json_encode($messages);
        exit;
    }

    function addAdminImage() {
        $data = array();
        //$nowdate = new DateTime('y-m-d H:i:s');
        $nowdate = date('Y-m-d H:i:s');
        $logged_in = $this->session->userdata("is_logged_in");
        $data['from_id'] = $logged_in['staff_id'];
        $data['from_table'] = $_POST['from_table'];
        $data['to_id'] = $_POST['to_id'];
        $data['to_table'] = $_POST['to_table'];
        $data['file'] = $_POST['msg'] ? $_POST['msg'] : '';
        $data['date_time'] = $nowdate;
        $messages = $this->chatmodule->addChatMessage($data);

        $to_data = $this->chatmodule->getStaffById($data['to_id']);
        $from_data = $this->chatmodule->getStaffById($data['from_id']);
        $to_name = $to_data[0]['name'];
        $to_image = $to_data[0]['image'];
        $from_name = $from_data[0]['name'];
        $from_image = $from_data[0]['image'];

        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        $client->emit('new_file_client_to_client', ['from_id' => $data['from_id'], 'from_table' => $data['from_table'], 'from_name' => $from_name, 'from_image' => $from_image, 'to_id' => $data['to_id'], 'to_table' => $data['to_table'], 'to_name' => $to_name, 'to_image' => $to_image, 'file' => $data['file'], 'date_time' => $data['date_time']]);
        $client->close();

        echo json_encode($messages);
        exit;
    }

    function addAdminToVisitorImage() {
        $data = array();
        //$nowdate = new DateTime('y-m-d H:i:s');
        $nowdate = date('Y-m-d H:i:s');
        $logged_in = $this->session->userdata("is_logged_in");
        $data['from_id'] = $logged_in['staff_id'];
        $data['from_table'] = $_POST['from_table'];
        $data['to_id'] = $this->chatmodule->fetch_visitor_by_session_id($_POST['to_id']);
        $data['to_table'] = $_POST['to_table'];
        $data['file'] = $_POST['msg'] ? $_POST['msg'] : '';
        $data['date_time'] = $nowdate;
        $last_message_id = $this->chatmodule->addChatMessage($data);
        $logged_in['visitor_last_message_id'] = $last_message_id;
        $this->session->set_userdata('is_logged_in', $logged_in);
        $name = $logged_in['name'];
        $image = $logged_in['image'];
        $file = "<a target='_blank' href=" . base_url() . "file/download/" . $data['file'] . ">" . $data['file'] . "</a>";
        $chat_html = $this->load->view('shared/right_chat_html', array('name' => $name, 'message' => $file, 'image' => $image), true);
        $other_agent_chat_html = $this->load->view('shared/left_chat_html', array('name' => $name, 'message' => $file, 'image' => $image), true);

        $to_data = $this->chatmodule->fetch_visitor_by_id($data['to_id']);
        $from_data = $this->chatmodule->getStaffById($data['from_id']);
        if ($to_data->name == '') {
            $to_name = 'Visitor';
        } else {
            $to_name = $to_data->name . 'Visitor';
        }
        $to_image = 'default.png';
        $to_session_id = $_POST['to_id'];
        $from_name = $from_data[0]['name'];
        $from_image = $from_data[0]['image'];

        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        if($to_data->chat_through == '2'){ // Chat Through WhatsApp
            $client->emit('send_file_chat_to_twilio_whatsapp', ['to_session_id' => $_POST['to_id'], 'from_id' => $data['from_id'], 'to_name' => $to_name, 'to_image' => $to_image, 'other_agent_chat_html' => $other_agent_chat_html, 'chat_html' => $chat_html, 'date_time' => $data['date_time'], 'file' => $file, 'from_number' => TWILIO_WHATSAPP_NUMBER, 'to_number' => $to_data->name, 'msg_text' => 'Attachment', 'mediaUrl' => base_url() . "uploads/" . $data['file']]);
        } else {
            $client->emit('new_file_client_to_visitor', ['to_session_id' => $_POST['to_id'], 'from_id' => $data['from_id'], 'to_name' => $to_name, 'to_image' => $to_image, 'other_agent_chat_html' => $other_agent_chat_html, 'chat_html' => $chat_html, 'date_time' => $data['date_time'], 'file' => $file]);
        }
        $client->close();

        echo json_encode(array('success' => true, 'chat_html' => $chat_html));
        exit;
    }
    
    function transfer_chat_to_other(){
        $from_staff_name = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $_POST['from_staff_id']);
        $visitor_name = $this->crud->get_id_by_val('visitors', 'name', 'session_id', $_POST['visitor_session_id']);
        if(empty($visitor_name)){ $visitor_name = 'Visitor'; }
        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        $client->emit('transfer_chat_to_other', ['from_staff_id' => $_POST['from_staff_id'], 'from_staff_name' => $from_staff_name, 'to_staff_id' => $_POST['to_staff_id'], 'visitor_session_id' => $_POST['visitor_session_id'], 'visitor_name' => $visitor_name]);
        $client->close();
    }
    function transfer_chat_to_other_response(){
        $to_staff_name = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $_POST['to_staff_id']);
        $visitor_name = $this->crud->get_id_by_val('visitors', 'name', 'session_id', $_POST['to_session_id']);
        if(empty($visitor_name)){ $visitor_name = 'Visitor'; }
        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        $client->emit('transfer_chat_to_other_response', ['from_staff_id' => $_POST['from_staff_id'], 'to_staff_name' => $to_staff_name, 'to_staff_id' => $_POST['to_staff_id'], 'visitor_session_id' => $_POST['visitor_session_id'], 'visitor_name' => $visitor_name, 'response' => $_POST['response']]);
        $client->close();
    }

}

?>
