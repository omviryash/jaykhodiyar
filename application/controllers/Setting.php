<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Setting
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 */
class Setting extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }

        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
        $this->load->model('Chatmodule','chat');
    }

    function update(){
        $status = 1;
        $msg = "Roles has been updated successfully.";
        $user_id = $this->input->post("user_id");

        if(intval($user_id) > 0){
            $roles = $this->input->post("roles");

            // delete old roles
            $sql = "DELETE FROM staff_roles WHERE staff_id='$user_id'";
            $this->crud->execuetSQL($sql);

            // add new roles
			$dataToInsert = array();
            if(is_array($roles) && count($roles) > 0)
            {
                foreach($roles as $module_id => $role_id){
                    $tmp = explode("_", $module_id);
                    $module_id = $tmp[1];
                    $data = array(
                      'staff_id' => $user_id,
                      'module_id' => $module_id,
                      'role_id' => $role_id,
                    );
					array_push($dataToInsert, $data);
                }
				$this->db->insert_batch('staff_roles', array_filter($dataToInsert)); 
            }
        }else{
            $status = 0;
            $msg = "Please select staff user.";
        }

        echo json_encode(array("status" => $status,"msg" => $msg));
        exit;
    }

    function index()
    {
        $data = array();
        $data['users'] = $this->crud->getFromSQL("SELECT * FROM `staff` WHERE `active`= 1 ORDER BY `staff`.`name` ASC");
        $user_id = isset($_GET['user_id']) ? $_GET['user_id']:0;
        $data['user_id'] = $user_id;
        $data['modules_roles'] = $this->app_model->getModuleRoles();
        $data['user_roles'] = $this->app_model->getUserRoleIDS($user_id);
        //print_r($data['modules_roles']);exit;
        if($this->applib->have_access_role(MASTER_GENERAL_MASTER_STAFF_ROLES_MENU_ID,"view")) {
            set_page('setting/index', $data);
        }else{
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function chat_access_role(){
        $data = array();
        $logged_in = $this->session->userdata("is_logged_in");
        $data['users'] = $this->crud->getFromSQL("SELECT * FROM `staff` WHERE `active`= 1 ORDER BY `staff`.`name` ASC");
        $data['agents'] = $this->chat->getAllAgents('staff_id');
        $user_id = isset($_GET['user_id']) ? $_GET['user_id']:0;
        $data['user_id'] = $user_id;
        $data['modules_roles'] = $this->app_model->getModuleRoles();
        $data['user_roles'] = $this->app_model->getUserChatRoleIDS($user_id);
        //print_r($data['logged_in']);exit;
        if($this->applib->have_access_role(MASTER_GENERAL_MASTER_CHAT_ROLES_MENU_ID,"view")) {
            set_page('setting/add_chat_access_role', $data);
        }else{
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    function save_chat_access_role(){
        $status = 1;
        $msg = "Chat Roles has been updated successfully.";
        $user_id = $this->input->post("user_id");

        if(intval($user_id) > 0)
        {
            $roles = $this->input->post("staff_ids");

            // delete old roles
            $sql = "DELETE FROM chat_roles WHERE staff_id='$user_id'";
            $this->crud->execuetSQL($sql);

            //echo "<pre>";print_r($roles);exit;

            // add new roles
            if(is_array($roles) && count($roles) > 0)
            {
                foreach($roles as $role_id)
                {
                    $dataToInsert = array(
                        'staff_id' => $user_id,
                        'allowed_staff_id' => $role_id,
                    );

                    $this->crud->insert("chat_roles", $dataToInsert);
                }
            }
        }
        else
        {
            $status = 0;
            $msg = "Please select staff user.";
        }

        echo json_encode(array("status" => $status,"msg" => $msg));
        exit;

    }
    
    
    
    function quotation_reminder_roles(){
        $data = array();
        $logged_in = $this->session->userdata("is_logged_in");
        $user_id = isset($_GET['user_id']) ? $_GET['user_id']:0;
        $data['users'] = $this->crud->getFromSQL("SELECT * FROM `staff` WHERE `active`= 1 ORDER BY `name` ASC");
        $data['agents'] = $this->chat->getAllAgents($user_id);
        $data['user_id'] = $user_id;
        $data['modules_roles'] = $this->app_model->getModuleRoles();
        $data['user_roles'] = $this->app_model->getUserQuoteReminderIDS($user_id);
        //print_r($data['logged_in']);exit;
        if($this->applib->have_access_role(MASTER_GENERAL_MASTER_QUOTATION_CHAT_ROLES_MENU_ID,"view")) {
            set_page('setting/quotation_reminder_roles', $data);
        }else{
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function save_quotation_reminder_roles(){
        $status = 1;
        $msg = "Quotation Reminder Roles has been Updated Successfully.";
        $user_id = $this->input->post("user_id");

        if(intval($user_id) > 0)
        {
            $roles = $this->input->post("staff_ids");

            // delete old roles
            $sql = "DELETE FROM quotation_reminder_roles WHERE staff_id='$user_id'";
            $this->crud->execuetSQL($sql);

            //echo "<pre>";print_r($roles);exit;

            // add new roles
            if(is_array($roles) && count($roles) > 0)
            {
                foreach($roles as $role_id)
                {
                    $dataToInsert = array(
                        'staff_id' => $user_id,
                        'allowed_staff_id' => $role_id,
                    );

                    $this->crud->insert("quotation_reminder_roles", $dataToInsert);
                }
            }
        }
        else
        {
            $status = 0;
            $msg = "Please select staff user.";
        }

        echo json_encode(array("status" => $status,"msg" => $msg));
        exit;

    }
	
	function print_letter_roles(){
		$data = array();
        $logged_in = $this->session->userdata("is_logged_in");
        $data['users'] = $this->crud->getFromSQL("SELECT * FROM `staff` WHERE `active`= 1  ORDER BY name ASC");
		$data['letters'] = $this->crud->get_all_records('letters','letter_topic','ASC');
		$user_id = isset($_GET['user_id']) ? $_GET['user_id']:0;
        $data['user_id'] = $user_id;
        $data['user_roles'] = $this->app_model->getUserPrintLetterIDS($user_id);
        //print_r($data['logged_in']);exit;
        if($this->applib->have_access_role(MASTER_GENERAL_MASTER_PRINT_LETTER_ROLES_MENU_ID,"view")) {
            set_page('setting/letter_roles', $data);
        }else{
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
	}
	
	function save_print_letter_roles(){
		$status = 1;
        $msg = "Letter Roles has been Updated Successfully.";
        $user_id = $this->input->post("user_id");
		$logged_in_id = $this->session->userdata("is_logged_in")['staff_id'];
		$now_time = date('Y-m-d H:i:s');
        if(intval($user_id) > 0) {
            $letter_ids = $this->input->post("letter_ids");
			
            // delete old roles
            $sql = "DELETE FROM print_letter_roles WHERE staff_id='$user_id'";
            $this->crud->execuetSQL($sql);

            //echo "<pre>";print_r($roles);exit;

            // add new roles
            if(is_array($letter_ids) && count($letter_ids) > 0){
                foreach($letter_ids as $letter_id){
                    $dataToInsert = array(
                        'staff_id' => $user_id,
                        'letter_id' => $letter_id,
                        'created_at' => $now_time,
                        'created_by' => $logged_in_id,
                    );
                    $this->crud->insert("print_letter_roles", $dataToInsert);
                }
            }
        } else {
            $status = 0;
            $msg = "Please select staff user.";
        }
        echo json_encode(array("status" => $status,"msg" => $msg));
        exit;
	}

}
