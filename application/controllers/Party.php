<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Party
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 * @property Crud $crud
 */
class Party extends CI_Controller
{

	function __construct(){
		parent::__construct();

		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		$this->load->model('Chatmodule','chat');
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
	}

	function check_party_mobile($search,$party_id){
		$sql = "SELECT party_id,phone_no FROM party WHERE phone_no != ''";
		if($party_id > 0 ){
			$sql .= "AND party_id != {$party_id}";
		}
		$rows = $this->db->query($sql)->result();
		$found_party_id = null;
		$i = 0;
		foreach ($rows as $row) {
			if (strpos($row->phone_no, ' ') !== false) {
				$row->phone_no = str_replace(" ","",$row->phone_no);
			}
			if (strpos($row->phone_no, '+91') !== false) {
				$row->phone_no = str_replace("+91","",$row->phone_no);
			}
			if (strpos($row->phone_no, '+') !== false) {
				$row->phone_no = str_replace("+","",$row->phone_no);
			}
			if (strpos($row->phone_no, '-') !== false) {
				$row->phone_no = str_replace('-','',$row->phone_no);
			}
			if (strpos($row->phone_no, '//') !== false) {
				$row->phone_no = str_replace('//','/',$row->phone_no);
			}
			if (strpos($row->phone_no, '/') !== false) {
				$row->phone_no = str_replace('/',',',$row->phone_no);
			}
			if (strpos($row->phone_no, '(') !== false) {
				$row->phone_no = str_replace('(','',$row->phone_no);
			}
			if (strpos($row->phone_no, ')') !== false) {
				$row->phone_no = str_replace(')','',$row->phone_no);
			}
			if (strpos($row->phone_no,$search) !== false) {
				$found_party_id = $row->party_id;
			}			
		}
		//echo $this->search_in_array("9982458691", $array);
		return $found_party_id;		
	}
	
    function check_party_fax_or_tel_no($search,$party_id){
		$sql = "SELECT party_id,fax_no FROM party WHERE fax_no != ''";
		if($party_id > 0 ){
			$sql .= "AND party_id != {$party_id}";
		}
		$rows = $this->db->query($sql)->result();
		$found_party_id = null;
		$i = 0;
		foreach ($rows as $row) {
			
			if (strpos($row->fax_no, ' ') !== false) {
				$row->phone_no = str_replace(" ","",$row->fax_no);
			}
			if (strpos($row->fax_no, '+91') !== false) {
				$row->phone_no = str_replace("+91","",$row->fax_no);
			}
			if (strpos($row->fax_no, '+') !== false) {
				$row->phone_no = str_replace("+","",$row->fax_no);
			}
			if (strpos($row->fax_no, '-') !== false) {
				$row->phone_no = str_replace('-','',$row->fax_no);
			}
			if (strpos($row->fax_no, '//') !== false) {
				$row->phone_no = str_replace('//','/',$row->fax_no);
			}
			if (strpos($row->fax_no, '/') !== false) {
				$row->phone_no = str_replace('/',',',$row->fax_no);
			}
			if (strpos($row->fax_no, '(') !== false) {
				$row->phone_no = str_replace('(','',$row->fax_no);
			}
			if (strpos($row->fax_no, ')') !== false) {
				$row->phone_no = str_replace(')','',$row->fax_no);
			}
			if (strpos($row->fax_no,$search) !== false) {
				$found_party_id = $row->party_id;
			}
		}
		//echo $this->search_in_array("9982458691", $array);
		return $found_party_id;		
	}
    
    function check_contact_person_fax_no($search,$party_id){
		$sql = "SELECT party_id,fax_no FROM contact_person WHERE fax_no != ''";
		if($party_id > 0 ){
			$sql .= "AND party_id != {$party_id}";
		}
		$rows = $this->db->query($sql)->result();
		$found_party_id = null;
		$i = 0;
		foreach ($rows as $row) {
			if (strpos($row->fax_no, ' ') !== false) {
				$row->phone_no = str_replace(" ","",$row->fax_no);
			}
			if (strpos($row->fax_no, '+91') !== false) {
				$row->phone_no = str_replace("+91","",$row->fax_no);
			}
			if (strpos($row->fax_no, '+') !== false) {
				$row->phone_no = str_replace("+","",$row->fax_no);
			}
			if (strpos($row->fax_no, '-') !== false) {
				$row->phone_no = str_replace('-','',$row->fax_no);
			}
			if (strpos($row->fax_no, '//') !== false) {
				$row->phone_no = str_replace('//','/',$row->fax_no);
			}
			if (strpos($row->fax_no, '/') !== false) {
				$row->phone_no = str_replace('/',',',$row->fax_no);
			}
			if (strpos($row->fax_no, '(') !== false) {
				$row->phone_no = str_replace('(','',$row->fax_no);
			}
			if (strpos($row->fax_no, ')') !== false) {
				$row->phone_no = str_replace(')','',$row->fax_no);
			}
			if (strpos($row->fax_no,$search) !== false) {
				$found_party_id = $row->party_id;
			}
		}
		//echo $this->search_in_array("9982458691", $array);
		return $found_party_id;		
	}
    
	function check_contact_person_mobile($search,$party_id){
		$sql = "SELECT contact_person_id,mobile_no FROM contact_person WHERE mobile_no != ''";
		if($party_id > 0 ){
			$sql .= "AND party_id != {$party_id}";
		}
		$rows = $this->db->query($sql)->result();
		$found_party_id = null;
		$i = 0;
		foreach ($rows as $row) {
			if (strpos($row->mobile_no, ' ') !== false) {
				$row->mobile_no = str_replace(" ","",$row->mobile_no);
			}
			if (strpos($row->mobile_no, '+91') !== false) {
				$row->mobile_no = str_replace("+91","",$row->mobile_no);
			}
			if (strpos($row->mobile_no, '+') !== false) {
				$row->mobile_no = str_replace("+","",$row->mobile_no);
			}
			if (strpos($row->mobile_no, '-') !== false) {
				$row->mobile_no = str_replace('-','',$row->mobile_no);
			}
			if (strpos($row->mobile_no, '//') !== false) {
				$row->mobile_no = str_replace('//','/',$row->mobile_no);
			}
			if (strpos($row->mobile_no, '/') !== false) {
				$row->mobile_no = str_replace('/',',',$row->mobile_no);
			}
			if (strpos($row->mobile_no, '(') !== false) {
				$row->mobile_no = str_replace('(','',$row->mobile_no);
			}
			if (strpos($row->mobile_no, ')') !== false) {
				$row->mobile_no = str_replace(')','',$row->mobile_no);
			}
			if (strpos($row->mobile_no,$search) !== false) {
				$found_party_id = $row->contact_person_id;
			}			
		}
		//echo $this->search_in_array("9982458691", $array);
		return $found_party_id;		
	}

	function add($party_id = ''){
        $data = array();
        $data['only_view_mode'] = 0;
		if(isset($_GET['view'])){ $data['only_view_mode'] = 1; }
		if(!empty($party_id)){
			if ($this->app_model->have_access_role(PARTY_MODULE_ID, "edit") || $data['only_view_mode'] == 1) {
				$data['party_data'] = $this->crud->get_row_by_id('party', array('party_id' => $party_id));
				$party_data = $data['party_data'][0];
				$party_data->party_created_date = substr($party_data->party_created_date, 8, 2) .'-'. substr($party_data->party_created_date, 5, 2) .'-'. substr($party_data->party_created_date, 0, 4);
				$party_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $party_data->created_by));
				$party_data->created_at = substr($party_data->created_at, 8, 2) .'-'. substr($party_data->created_at, 5, 2) .'-'. substr($party_data->created_at, 0, 4);
				$party_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $party_data->updated_by));
				$party_data->updated_at = substr($party_data->updated_at, 8, 2) .'-'. substr($party_data->updated_at, 5, 2) .'-'. substr($party_data->updated_at, 0, 4);
				$data['party_data'] = $party_data;
				$contact_persons = array();
				$where = array('party_id' => $party_id);
				$party_contact_persons = $this->crud->get_row_by_id('contact_person', $where);
				foreach($party_contact_persons as $party_contact_person){
					$party_contact_person->contact_person_name = $party_contact_person->name;
					$party_contact_person->contact_person_designation_id = $party_contact_person->designation_id;
					$party_contact_person->contact_person_department_id = $party_contact_person->department_id;
					$party_contact_person->contact_person_priority = $party_contact_person->priority;
					$party_contact_person->contact_person_mobile_no = $party_contact_person->mobile_no;
					$party_contact_person->contact_person_email = $party_contact_person->email;
					$party_contact_person->contact_person_fax_no = $party_contact_person->fax_no;
					$party_contact_person->contact_person_note = $party_contact_person->note;
					$contact_persons[] = json_encode($party_contact_person);
				}
				$data['party_contact_persons'] = implode(',', $contact_persons);
				//echo '<pre>';print_r($data); exit;
				set_page('party/add_party', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(PARTY_MODULE_ID,"add")) {
                            $password = $this->crud->autogenerate_password();
                            $data['password'] = $password;
                            set_page('party/add_party', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function check_party_name(){
        $return = array();
        
        $party_name = isset($_POST['party_name']) ? trim($_POST['party_name']) : '';
		$phone_no = isset($_POST['phone_no']) ? trim($_POST['phone_no']) : '';
		$party_id = isset($_POST['party_id']) ? $_POST['party_id'] : 0;
        $party_name = strtolower($party_name);
        if ($party_id > 0) {
			// party name unique validatoin
			$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($party_name) . "' AND phone_no='".$phone_no."' AND party_id != $party_id";
			$query = $this->db->query($sql_list_query);
			$rows = $query->result_array();
			if (count($rows) > 0) {
				$return['success'] = "false";
				$return['msg'] = "Party Name Already Exists.";
			}
		} else {
			// party name unique validatoin
			$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($party_name) . "' AND phone_no='".$phone_no."' ";
			$query = $this->db->query($sql_list_query);
			$rows = $query->result_array();
			if (count($rows) > 0) {
				$return['success'] = "false";
				$return['msg'] = "Party Name Already Exists.";
			}
		}
        print json_encode($return);
        exit;
    }
    
	function check_phone_no(){
        $return = array();
        $phone_no = isset($_POST['phone_no']) ? trim($_POST['phone_no']) : '';
		$party_id = isset($_POST['party_id']) ? $_POST['party_id'] : 0;
        // get phone numbers
		$phone_numbers = explode(",", $phone_no);
		$phone_nos = array();
		if (!empty($phone_numbers)) {
			foreach ($phone_numbers as $phone) {
				$phone = trim($phone);
				if(!empty($phone) && strlen($phone) < 10){
					$return['success'] = "false";
					$return['msg'] = "Mobile No Must be 10 digit.";
					print json_encode($return);exit;
				}
				if (strpos($phone, '-') !== false) {
					$phone = str_replace('-','',$phone);
				}
				$phone = substr($phone, -10);
				$match_no = $this->check_party_mobile($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Mobile already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_contact_person_mobile($phone,$party_id);
				if(!empty($match_no)){
					$cp_result = $this->crud->get_row_by_id('contact_person',array('contact_person_id' => $match_no));
					$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
					$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Mobile already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
			}
		}
        print json_encode($return);
        exit;
    }
    
	function check_email_id(){
        $return = array();
        $email_id = isset($_POST['email_id']) ? trim($_POST['email_id']) : '';
		$party_id = isset($_POST['party_id']) ? $_POST['party_id'] : 0;
        // get emails
		$emails = explode(",", $email_id);
		$email_ids = array();
		// multiple email validation
		if (!empty($emails)) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$return['success'] = "false";
						$return['msg'] = $email." is not valid Email.";
						print json_encode($return);
						exit;
					}
					$this->db->select('*');
					$this->db->from('party');
					$this->db->where('party_id !=', $party_id);
					$this->db->where('active !=', 0);
					$this->db->where("FIND_IN_SET( '".$email."', email_id) ");
					//~ $this->db->like('email_id',$email);
					$res = $this->db->get();
					$rows = $res->num_rows();
					
					$this->db->select('*');
					$this->db->from('contact_person');
					$this->db->where('party_id !=', $party_id);
					$this->db->where('active !=', 0);
					$this->db->where("FIND_IN_SET( '".$email."', email) ");
					//~ $this->db->like('email_id',$email);
					$res_cp = $this->db->get();
					$rows_cp = $res_cp->num_rows();
					if ($rows > 0) {
						$result = $res->result();
						$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
						$return['success'] = "false";
						$return['msg'] = $email." email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
						print json_encode($return);
						exit;
					}elseif($rows_cp > 0){
						$cp_result = $res_cp->result();
						$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
						$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
						$return['success'] = "false";
						$return['msg'] = $email." email already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
						print json_encode($return);
						exit;
						
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}
        print json_encode($return);
        exit;
    }
    
	function check_fax_no(){
        $return = array();
        $fax_or_tel_num = isset($_POST['fax_no']) ? trim($_POST['fax_no']) : '';
		$party_id = isset($_POST['party_id']) ? $_POST['party_id'] : 0;
        
        $fax_or_tel_no = explode(",", $fax_or_tel_num);
		$fax_or_tel_nos = array();
		if (!empty($fax_or_tel_no)) {
			foreach ($fax_or_tel_no as $number) {
				$number = trim($number);
				if (strpos($number, '-') !== false) {
					$number = str_replace('-','',$number);
				}
                $match_no = $this->check_party_mobile($number,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Mobile already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_party_fax_or_tel_no($number,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Telephone OR Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_contact_person_mobile($number,$party_id);
				if(!empty($match_no)){
					$cp_result = $this->crud->get_row_by_id('contact_person',array('contact_person_id' => $match_no));
					$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
					$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Mobile already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
                $match_no = $this->check_contact_person_fax_no($number,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Telephone OR Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
//                echo 'okkk'; exit;
			}
		}
        print json_encode($return);
        exit;
    }
    
	function save_party(){
		$return = array();
		$post_data = $this->input->post();
//				echo '<pre>';print_r($post_data); exit;
		$contact_person_data = json_decode('['.$post_data['contact_person_data'].']'); 
		//print_r($contact_person_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
		$post_data['party_data']['party_created_date'] = !empty(strtotime($post_data['party_data']['party_created_date'])) ? date("Y-m-d", strtotime($post_data['party_data']['party_created_date'])) : NULL;

		$party_name = isset($post_data['party_data']['party_name']) ? trim($post_data['party_data']['party_name']) : '';
		$email_id = isset($post_data['party_data']['email_id']) ? trim($post_data['party_data']['email_id']) : '';
		$phone_no = isset($post_data['party_data']['phone_no']) ? trim($post_data['party_data']['phone_no']) : '';
		$party_id = isset($post_data['party_data']['party_id']) ? $post_data['party_data']['party_id'] : 0;
		$post_data['party_data']['agent_id'] = (isset($post_data['party_data']['agent_id']) && !empty($post_data['party_data']['agent_id'])) ? $post_data['party_data']['agent_id'] : NULL;

		if (empty($email_id) && empty($phone_no)) {
			$return['success'] = "false";
			$return['msg'] = "Please Enter Phone Number Or Email Id!";
			print json_encode($return);
			exit;
		}

		$party_name = strtolower($party_name);
		// check unique validatation
		if ($party_id > 0) {
			// party name unique validatoin
			$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($party_name) . "' AND phone_no='".$phone_no."' AND party_id != $party_id";
			$query = $this->db->query($sql_list_query);
			$rows = $query->result_array();
			if (count($rows) > 0) {
				$return['success'] = "false";
				$return['msg'] = "Party Name Already Exists.";
				print json_encode($return);
				exit;
			}
		} else {
			// party name unique validatoin
			$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($party_name) . "' AND phone_no='".$phone_no."' ";
			$query = $this->db->query($sql_list_query);
			$rows = $query->result_array();
			if (count($rows) > 0) {
				$return['success'] = "false";
				$return['msg'] = "Party Name Already Exists.";
				print json_encode($return);
				exit;
			}
		}

		// get emails
		$emails = explode(",", $email_id);
		$email_ids = array();
		// multiple email validation
		if (!empty($emails)) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$return['success'] = "false";
						$return['msg'] = $email." is not valid Email.";
						print json_encode($return);
						exit;
					}
					$this->db->select('*');
					$this->db->from('party');
					$this->db->where('party_id !=', $party_id);
					$this->db->where('active !=', 0);
					$this->db->where("FIND_IN_SET( '".$email."', email_id) ");
					//~ $this->db->like('email_id',$email);
					$res = $this->db->get();
					$rows = $res->num_rows();
					
					$this->db->select('*');
					$this->db->from('contact_person');
					$this->db->where('party_id !=', $party_id);
					$this->db->where('active !=', 0);
					$this->db->where("FIND_IN_SET( '".$email."', email) ");
					//~ $this->db->like('email_id',$email);
					$res_cp = $this->db->get();
					$rows_cp = $res_cp->num_rows();
					if ($rows > 0) {
						$result = $res->result();
						$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
						$return['success'] = "false";
						$return['msg'] = $email." email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
						print json_encode($return);
						exit;
					}elseif($rows_cp > 0){
						$cp_result = $res_cp->result();
						$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
						$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
						$return['success'] = "false";
						$return['msg'] = $email." email already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
						print json_encode($return);
						exit;
						
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}

		// get phone numbers
		$phone_numbers = explode(",", $phone_no);
		$phone_nos = array();
		if (!empty($phone_numbers)) {
			foreach ($phone_numbers as $phone) {
				$phone = trim($phone);
				if(!empty($phone) && strlen($phone) < 10){
					$return['success'] = "false";
					$return['msg'] = "Mobile No Must be 10 digit.";
					print json_encode($return);exit;
				}
				if (strpos($phone, '-') !== false) {
					$phone = str_replace('-','',$phone);
				}
				$phone = substr($phone, -10);
				$match_no = $this->check_party_mobile($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Mobile already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_contact_person_mobile($phone,$party_id);
				if(!empty($match_no)){
					$cp_result = $this->crud->get_row_by_id('contact_person',array('contact_person_id' => $match_no));
					$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
					$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Mobile already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
			}
		}
        
        $fax_or_tel_num = isset($post_data['party_data']['fax_no']) ? trim($post_data['party_data']['fax_no']) : '';
        $fax_or_tel_no = explode(",", $fax_or_tel_num);
		$fax_or_tel_nos = array();
		if (!empty($fax_or_tel_no)) {
			foreach ($fax_or_tel_no as $number) {
				$number = trim($number);
				if (strpos($number, '-') !== false) {
					$number = str_replace('-','',$number);
				}
                $match_no = $this->check_party_mobile($number,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Mobile already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_party_fax_or_tel_no($number,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Telephone OR Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_contact_person_mobile($number,$party_id);
				if(!empty($match_no)){
					$cp_result = $this->crud->get_row_by_id('contact_person',array('contact_person_id' => $match_no));
					$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
					$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Mobile already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
                $match_no = $this->check_contact_person_fax_no($number,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $number." Telephone OR Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
//                echo 'okkk'; exit;
			}
		}
        
		if(isset($post_data['party_data']['party_id']) && !empty($post_data['party_data']['party_id'])){
			$party_id = $post_data['party_data']['party_id'];
			if (isset($post_data['party_data']['party_id']))
				unset($post_data['party_data']['party_id']);

			$post_data['party_data']['updated_by'] = $this->staff_id;
			$post_data['party_data']['updated_at'] = $this->now_time;
			$this->db->where('party_id', $party_id);
			$result = $this->db->update('party', $post_data['party_data']);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Party Updated Successfully');

				if(isset($post_data['deleted_contact_person_id'])){
					$this->db->where_in('contact_person_id', $post_data['deleted_contact_person_id']);
					$this->db->delete('contact_person');
				}
				$val_inc = 0;
				foreach($contact_person_data[0] as $contact_person){
					$add_contact_person = array();
					$add_contact_person['party_id'] = $party_id;
					$add_contact_person['name'] = $contact_person->contact_person_name;
					if(isset($contact_person->contact_person_designation_id) && !empty($contact_person->contact_person_designation_id)){
						$add_contact_person['designation_id'] = $contact_person->contact_person_designation_id;
					}
					if(isset($contact_person->contact_person_department_id) && !empty($contact_person->contact_person_department_id)){
						$add_contact_person['department_id'] = $contact_person->contact_person_department_id;
					}
					$add_contact_person['priority'] = $contact_person->contact_person_priority;
					$add_contact_person['mobile_no'] = $contact_person->contact_person_mobile_no;	
					$add_contact_person['email'] = $contact_person->contact_person_email;
					$add_contact_person['fax_no'] = $contact_person->contact_person_fax_no;
					$add_contact_person['note'] = $contact_person->contact_person_note;
					$add_contact_person['updated_by'] = $this->staff_id;
					$add_contact_person['updated_at'] = $this->now_time;
					if(isset($contact_person->contact_person_id) && !empty($contact_person->contact_person_id)){
						$this->db->where('contact_person_id', $contact_person->contact_person_id);
						$this->db->update('contact_person', $add_contact_person);
					} else {
						$add_contact_person['created_by'] = $this->staff_id;
						$add_contact_person['created_at'] = $this->now_time;
						$this->crud->insert('contact_person',$add_contact_person);
					}
				}
			}
		} else {
			$post_data['party_data']['party_code'] = $this->crud->get_next_autoincrement('party');
			$dataToInsert = $post_data['party_data'];
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_by'] = $this->staff_id;
			$dataToInsert['updated_at'] = $this->now_time;
            $dataToInsert['current_party_staff_id'] = $this->staff_id;

			// Unset party_id from party_data
			if (isset($dataToInsert['party_id']))
				unset($dataToInsert['party_id']);

			$result = $this->db->insert('party', $dataToInsert);
			$party_id = $this->db->insert_id();
			if($result){
				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Party Added Successfully');
				foreach($contact_person_data[0] as $contact_person){
					$add_contact_person = array();
					$add_contact_person['party_id'] = $party_id;
					$add_contact_person['name'] = $contact_person->contact_person_name;
					if(isset($contact_person->contact_person_designation_id) && !empty($contact_person->contact_person_designation_id)){
						$add_contact_person['designation_id'] = $contact_person->contact_person_designation_id;
					}
					if(isset($contact_person->contact_person_department_id) && !empty($contact_person->contact_person_department_id)){
						$add_contact_person['department_id'] = $contact_person->contact_person_department_id;
					}
					$add_contact_person['priority'] = $contact_person->contact_person_priority;
					$add_contact_person['mobile_no'] = $contact_person->contact_person_mobile_no;
					$add_contact_person['email'] = $contact_person->contact_person_email;
					$add_contact_person['fax_no'] = $contact_person->contact_person_fax_no;
					$add_contact_person['note'] = $contact_person->contact_person_note;
					$add_contact_person['created_by'] = $this->staff_id;
					$add_contact_person['created_at'] = $this->now_time;
					$add_contact_person['updated_by'] = $this->staff_id;
					$add_contact_person['updated_at'] = $this->now_time;
					$this->crud->insert('contact_person',$add_contact_person);
				}
			}
//            
		}
//        echo $this->db->last_query(); exit;
		print json_encode($return);
		exit;
	}
	
	function check_ajax_cp_email(){
		//pre_die($_POST);
        $party_id = $_POST['party_id'];
		$email_id = trim($_POST['contact_person_email']);
        
        // get emails
		$emails = explode(",", $email_id);
		$email_ids = array();
		// multiple email validation
		if (!empty($emails)) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$return['success'] = "false";
						$return['msg'] = $email." is not valid Contect Person Email.";
						print json_encode($return);
						exit;
					}
					$this->db->select('*');
					$this->db->from('party');
					$this->db->where('party_id !=', $party_id);
					$this->db->where('active !=', 0);
					$this->db->where("FIND_IN_SET( '".$email."', email_id) ");
					//~ $this->db->like('email_id',$email);
					$res = $this->db->get();
					$rows = $res->num_rows();
					
					$this->db->select('*');
					$this->db->from('contact_person');
					$this->db->where('party_id !=', $party_id);
					$this->db->where('active !=', 0);
					$this->db->where("FIND_IN_SET( '".$email."', email) ");
					//~ $this->db->like('email_id',$email);
					$res_cp = $this->db->get();
					$rows_cp = $res_cp->num_rows();
					if ($rows > 0) {
						$result = $res->result();
						$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
						$return['success'] = "false";
						$return['msg'] = $email." email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
						print json_encode($return);
						exit;
					}elseif($rows_cp > 0){
						$cp_result = $res_cp->result();
						$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
						$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
						$return['success'] = "false";
						$return['msg'] = $email." email already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
						print json_encode($return);
						exit;
						
					} else {
						$email_ids[] = $email;
					}
				}
			}
		} else {
			$return['success'] = true;
			print json_encode($return);
			exit;
		}
        
	}
	
	function check_cp_phone(){
		$party_id = $_POST['party_id'];
		$phone_no = trim($_POST['contact_person_mobile_no']);
        
        // get phone numbers
		$phone_numbers = explode(",", $phone_no);
		$phone_nos = array();
		if (!empty($phone_numbers)) {
			foreach ($phone_numbers as $phone) {
				$phone = trim($phone);
				if(!empty($phone) && strlen($phone) < 10){
					$return['success'] = "false";
					$return['msg'] = "Mobile No Must be 10 digit.";
					print json_encode($return);exit;
				}
				if (strpos($phone, '-') !== false) {
					$phone = str_replace('-','',$phone);
				}
				$phone = substr($phone, -10);
				$match_no = $this->check_party_mobile($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Contect Person Mobile already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_contact_person_mobile($phone,$party_id);
				if(!empty($match_no)){
					$cp_result = $this->crud->get_row_by_id('contact_person',array('contact_person_id' => $match_no));
					$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
					$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Contect Person Mobile already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
                $match_no = $this->check_party_fax_or_tel_no($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Telephone OR Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
                $match_no = $this->check_contact_person_fax_no($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
			}
		} else {
			$return['success'] = true;
			print json_encode($return);
			exit;
		}
	}
    
    function check_cp_fax_no(){
		$party_id = $_POST['party_id'];
		$phone_no = trim($_POST['contact_person_fax_no']);
        
        // get phone numbers
		$phone_numbers = explode(",", $phone_no);
		$phone_nos = array();
		if (!empty($phone_numbers)) {
			foreach ($phone_numbers as $phone) {
				$phone = trim($phone);
				if (strpos($phone, '-') !== false) {
					$phone = str_replace('-','',$phone);
				}
//				$phone = substr($phone, -10);
				$match_no = $this->check_party_mobile($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Contect Person Mobile already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
				$match_no = $this->check_contact_person_mobile($phone,$party_id);
				if(!empty($match_no)){
					$cp_result = $this->crud->get_row_by_id('contact_person',array('contact_person_id' => $match_no));
					$party_name = $this->crud->get_id_by_val('party','party_name','party_id',$cp_result[0]->party_id);
					$created_by = empty($cp_result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $cp_result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Contect Person Mobile already exist.&nbsp; <br/> Original Contect Person Name : " . $cp_result[0]->name. "<br/> Original Party : " . $party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
                $match_no = $this->check_party_fax_or_tel_no($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Telephone OR Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
                $match_no = $this->check_contact_person_fax_no($phone,$party_id);
				if(!empty($match_no)){
					$result = $this->crud->get_row_by_id('party',array('party_id' => $match_no));
					$created_by = empty($result[0]->created_by) ? 'Admin' : $this->crud->get_id_by_val('staff', 'name', 'staff_id', $result[0]->created_by);
					$return['success'] = "false";
					$return['msg'] = $phone." Fax Number already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name. "&nbsp; <br/> Created By : " . $created_by;
					print json_encode($return);
					exit;
				}
			}
		} else {
			$return['success'] = true;
			print json_encode($return);
			exit;
		}
	}

	function party_list() {
        $data = array();
		if($this->app_model->have_access_role(PARTY_MODULE_ID, "view")){
            $data['agents'] = $this->chat->getAllAgents($this->staff_id);
            set_page('party/party_list', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	/**
	 * Party List DataTable
	 */

	function party_list_datatable() {
//            echo '<pre>'; print_r($_POST); exit;
		//print_r($this->session->userdata('user_roles'));
		$config['table'] = 'party p';

		$config['select'] = 'p.party_id,p.party_created_date,p.party_code,p.party_name,p.phone_no,p.w_phone1,p.com_address_phone1,p.email_id,p.active as party_status,p.current_party_staff_id,c.city,s.state,cy.country,p.username,p.password,cre_s.name AS created_person,cur_s.name AS current_person';
		$config['column_search'] = array('DATE_FORMAT(p.party_created_date,"%d-%m-%Y")','p.party_code','p.party_name','p.phone_no','p.w_phone1','p.com_address_phone1','p.email_id','c.city','s.state','cy.country');
		$config['column_order'] = array(null,'p.party_created_date','p.party_code','p.party_name','p.phone_no','p.email_id','p.username','p.password','c.city','s.state','cy.country');
		$config['joins'][] = array('join_table'=>'city c','join_by'=>'c.city_id = p.city_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'state s','join_by'=>'s.state_id = p.state_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'country cy','join_by'=>'cy.country_id = p.country_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'staff cre_s','join_by'=>'cre_s.staff_id = p.created_by','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');
		if (isset($_POST['party_status']) && $_POST['party_status'] != 'all') {
			$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => $_POST['party_status']);
		}
        $custom_where = ' 1 = 1 ';
        if (isset($_POST['party_mobile']) && !empty($_POST['party_mobile'])) {
            $party_id = $this->crud->getFromSQL("SELECT `p`.`party_id` FROM `party` `p` LEFT JOIN `contact_person` `cp` ON `cp`.`party_id` = `p`.`party_id` WHERE ( `p`.`phone_no` LIKE '%".$_POST['party_mobile']."%' ESCAPE '!' OR  `p`.`w_phone1` LIKE '%".$_POST['party_mobile']."%' ESCAPE '!' OR  `p`.`com_address_phone1` LIKE '%".$_POST['party_mobile']."%' ESCAPE '!' OR  `cp`.`phone_no` LIKE '%".$_POST['party_mobile']."%' ESCAPE '!' OR  `cp`.`mobile_no` LIKE '%".$_POST['party_mobile']."%' ESCAPE '!' )");
            $party_where_id = '';
            if(!empty($party_id)){
                $party_ids = array();
                foreach ($party_id as $party){
                    $party_ids[] = $party->party_id;
                }
                $party_where_id = implode(',', $party_ids);
            }
            if(!empty($party_where_id)){
                $custom_where .= " AND p.party_id IN(".$party_where_id.")";
            } else {
                $custom_where .= " AND p.party_id IN(-1)";
            }
        }
        if (isset($_POST['party_email']) && !empty($_POST['party_email'])) {
            $party_id = $this->crud->getFromSQL("SELECT `p`.`party_id` FROM `party` `p` LEFT JOIN `contact_person` `cp` ON `cp`.`party_id` = `p`.`party_id` WHERE ( `p`.`email_id` LIKE '%".$_POST['party_email']."%' ESCAPE '!' OR  `cp`.`email` LIKE '%".$_POST['party_email']."%' ESCAPE '!')");
            $party_where_id = '';
            if(!empty($party_id)){
                $party_ids = array();
                foreach ($party_id as $party){
                    $party_ids[] = $party->party_id;
                }
                $party_where_id = implode(',', $party_ids);
            }
            if(!empty($party_where_id)){
                $custom_where .= " AND p.party_id IN(".$party_where_id.")";
            } else {
                $custom_where .= " AND p.party_id IN(-1)";
            }
        }

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		
        if($this->app_model->have_access_role(PARTY_MODULE_ID, "is_management")){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$custom_where .= ' AND ( p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL )';
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if($cu_accessDomestic == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
        $config['custom_where'] = $custom_where;
		$config['order'] = array('party_id' => 'DESC');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
//        echo $this->db->last_query(); exit;
		foreach ($list as $party) {
			$row = array();
			$action = '';
			$isEdit = $this->app_model->have_access_role(PARTY_MODULE_ID, "edit");
			$isDelete = $this->app_model->have_access_role(PARTY_MODULE_ID, "delete");
			if ($isEdit) {
				$action .= '<a href="' . base_url("party/add/".$party->party_id) . '" class="btn btn-xs btn-primary btn-edit-party" data-party_id="' . $party->party_id . '"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$inquiry_id = $this->crud->get_id_by_val('inquiry', 'inquiry_id', 'party_id', $party->party_id);
				if(empty($inquiry_id) && $party->party_status == 1){
					$action .= ' <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_button" data-href="' . base_url('party/delete_party/' . $party->party_id) . '"><i class="fa fa-trash"></i></a>';
				}
			}
            if($this->app_model->have_access_role(PARTY_MODULE_ID, "is_management") == 1 && $party->party_status == 0){
                $action .= ' <a href="javascript:void(0);" class="btn btn-xs btn-info restore_button" data-href="' . base_url('party/restore_party/' . $party->party_id) . '"> Restore </a>';
            }
            if($this->app_model->have_access_role(CAN_ALLOW_TRANSFER_PARTY_ID, "allow") == 1){
                $action .= ' <a href="javascript:void(0);" class="btn btn-xs btn-warning transfer_button open_transfer_party_modal" data-transfer_party_id="' . $party->party_id . '" data-transfer_from="' . $party->current_party_staff_id . '" > Transfer To </a>';
            }
			$row[] = $action;
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.substr($party->party_created_date, 8, 2) .'-'. substr($party->party_created_date, 5, 2) .'-'. substr($party->party_created_date, 0, 4).'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->party_code.'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->party_name.'</a>';
            $party->phone_no = $party->phone_no.', '.$party->w_phone1.', '.$party->com_address_phone1;
			$phone_no_arr = preg_split('/[\\n\,]+/', $party->phone_no);
			$phone_nos = '';
			$p_inc = 1;
            $phone_no_check_unique_arr = array();
			foreach($phone_no_arr as $phone_no){
				if(!empty(trim($phone_no))){
                    if(in_array($phone_no, $phone_no_check_unique_arr)){ } else {
                        $phone_nos .= $phone_no.', ';
                        $phone_no_check_unique_arr[] = $phone_no;
                        if($p_inc%2 == 0){ $phone_nos .= '<br />'; }
                        $p_inc++;
                    }
				}
			}
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$phone_nos.'</a>';
			$email_id_arr = explode(',', $party->email_id);
			$email_ids = '';
			foreach($email_id_arr as $email_id){
				if(!empty(trim($email_id))){
					$email_ids .= $email_id.', ';
					$email_ids .= '<br />';
				}
			}
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$email_ids.'</a>';
                        $row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->username.'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->password.'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->city.'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->state.'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->country.'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->created_person.'</a>';
			$row[] = '<a href="' . base_url("party/add/".$party->party_id."?view") . '" >'.$party->current_person.'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function delete_party($party_id){
		$this->crud->update('party',array('active'=>0),array('party_id'=>$party_id));
		$this->crud->update('contact_person',array('active'=>0),array('party_id'=>$party_id));
		echo json_encode(array('success'=>true,'message'=>"Party deleted successfully!"));
		exit();
	}
    
    function restore_party($party_id){
		$this->crud->update('party',array('active'=>1),array('party_id'=>$party_id));
		$this->crud->update('contact_person',array('active'=>1),array('party_id'=>$party_id));
		echo json_encode(array('success'=>true,'message'=>"Party deleted successfully!"));
		exit();
	}
    
    function transfer_one_party_to_other(){
		if ($_POST['transfer_party_id'] != '' && $_POST['transfer_from'] != '' && $_POST['transfer_to'] != '' ) {
			$transfer_party_id = $_POST['transfer_party_id'];
            $transfer_from = $_POST['transfer_from'];
			$transfer_to = $_POST['transfer_to'];
            $transfer_party_name = $this->crud->get_id_by_val('party', 'party_name', 'party_id', $transfer_party_id);
            $transfer_from_name = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $transfer_from);
            $transfer_to_name = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $transfer_to);
            
            $result = $this->transfer_party_modules($transfer_party_id, $transfer_from, $transfer_to);
            if($result){
                echo json_encode(array('success' => true, 'message' => 'Party : <b>"'.$transfer_party_name.'"</b> of <u>'.$transfer_from_name.'</u> <b>Transfered To</b> <u>'.$transfer_to_name.'</u> Successfully! '));
            } else {
                echo json_encode(array('success' => false, 'message' => 'some error occurred !'));
            }
			exit();
		} else {
			redirect("party/party_list"); exit;
			exit();
		}
    }
    
    function transfer_all_party_to_other(){
		if ($_POST['transfer_from'] != '' && $_POST['transfer_to'] != '' ) {
			$transfer_from = $_POST['transfer_from'];
			$transfer_to = $_POST['transfer_to'];
            $transfer_from_name = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $transfer_from);
            $transfer_to_name = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $transfer_to);
            
            $party_ids = $this->crud->get_row_by_id('party',array('current_party_staff_id' => $transfer_from));
            if(!empty($party_ids)){
                foreach($party_ids as $party_id){
                    $result = $this->transfer_party_modules($party_id->party_id, $transfer_from, $transfer_to);
                }
            }
            
            if($result){
                echo json_encode(array('success' => true, 'message' => 'All Party of <u>'.$transfer_from_name.'</u> <b>Transfered To</b> <u>'.$transfer_to_name.'</u> Successfully! '));
            } else {
                echo json_encode(array('success' => false, 'message' => 'Not Find Party of <u>'.$transfer_from_name.'</u>!'));
            }
			exit();
		} else {
			redirect("party/party_list"); exit;
			exit();
		}
    }
            
    function transfer_party_modules($transfer_party_id, $transfer_from, $transfer_to){
        $data_array = array('updated_by' => $this->staff_id, 'updated_at' => $this->now_time, 'current_party_staff_id' => $transfer_to);
        $where_array = array('party_id' => $transfer_party_id);
        $this->crud->update('party', $data_array, $where_array);

        $dataToInsert = array();
        $dataToInsert['module_table_name'] = 'party';
        $dataToInsert['module_table_id'] = $transfer_party_id;
        $dataToInsert['party_id'] = $transfer_party_id;
        $dataToInsert['transfer_from'] = $transfer_from;
        $dataToInsert['transfer_to'] = $transfer_to;
        $dataToInsert['created_by'] = $this->staff_id;
        $dataToInsert['created_at'] = $this->now_time;
        return $this->db->insert('party_transfer_history', $dataToInsert);
	}

	function party_log_report(){
		set_page('party/party_log_report');
	}

	function party_log_view(){
		$id = $this->input->get("id");
		$data['party_id'] = $party_id = $id;
		$this->db->select('p.*,ref.reference,sales.sales,sales_type.sales_type,branch.branch,ct.city,st.state,cnt.country,wct.city as wcity,wst.state as wstate,wcnt.country as wcountry');
		$this->db->from('party_log p');
		$this->db->join('reference ref','ref.id = p.reference_id','left');
		$this->db->join('sales sales','sales.id = p.party_type_1','left');
		$this->db->join('sales_type sales_type','sales_type.id = p.party_type_2','left');
		$this->db->join('branch branch','branch.branch_id = p.branch_id','left');
		$this->db->join('city ct','ct.city_id = p.city_id','left');
		$this->db->join('state st','st.state_id = p.state_id','left');
		$this->db->join('country cnt','cnt.country_id = p.country_id','left');
		$this->db->join('city wct','wct.city_id = p.w_city','left');
		$this->db->join('state wst','wst.state_id = p.w_state','left');
		$this->db->join('country wcnt','wcnt.country_id = p.w_country','left');
		$this->db->where('p.party_id',$party_id);
		$this->db->order_by('p.id','DESC');
		$query = $this->db->get();
		$party_log_data = array();
		if($query->num_rows() > 0) {
			$party_log_data = $query->result_array();
		}
		$data['party_log_data'] = $party_log_data;
		set_page('party/party_log_report_detail', $data);
	}

	function party_log_datatable(){
		$party_id = $this->input->post('party_id');
		if($party_id != ''){
			$config['wheres'][] = array('column_name'=>'party.party_id','column_value'=>$party_id);
		}
		$config['table'] = 'party_log';
		$config['joins'][0]['join_table'] = "party";
		$config['joins'][0]['join_by'] = " party_log.party_id=party.party_id ";
		$config['joins'][0]['join_type'] = "left";

		$config['joins'][1]['join_table'] = "staff";
		$config['joins'][1]['join_by'] = "party_log.created_by=staff.staff_id";
		$config['joins'][1]['join_type'] = "left";

		$config['select'] = 'party_log.id,party_log.operation_type,party_log.party_id,party_log.party_code,party_log.party_name,party_log.created_by,party_log.created_at,staff.name';
		$config['column_search'] = array('party_log.id', 'party_log.party_id', 'party.party_code', 'party.party_name', 'party_log.operation_type');
		$config['column_order'] = array('party_log.id', 'party_log.operation_type', 'party_log.party_id', 'party.party_code', 'party.party_name', 'party_log.created_by', 'party_log.created_at');
		$config['order'] = array('party_log.party_id,party_log.id ' => 'ASC');
		if(isset($_POST['party_id'])) {
			$config['wheres'][] = array('column_name'=>'party_log.party_id','column_value'=>$_POST['party_id']);
		}
		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($this->app_model->have_access_role(PARTY_MODULE_ID, "is_management")){
		} else {
			$config['where_string'] = '( `party`.`created_by`='. $this->session->userdata('is_logged_in')['staff_id'].' OR `party`.`created_by` IS NULL )';
			if($cu_accessExport = 1 && $cu_accessDomestic = 1){
			} else if($cu_accessExport = 1){
				$config['where_string'] .= ' AND `party`.`party_type_1` = '. PARTY_TYPE_EXPORT_ID;
			} else if($cu_accessDomestic = 1){
				$config['where_string'] .= ' AND `party`.`party_type_1` = '. PARTY_TYPE_DOMESTIC_ID;
			} else {}
		}
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		foreach ($list as $party) {
			$row = array();
			$row[] = $party->id;
			if($party->operation_type == 'insert') {

			}
			$row[] = $party->operation_type;
			$row[] = $party->party_id;
			$row[] = $party->party_code;
			$row[] = $party->party_name;
			$row[] = $party->name;
			$row[] = $party->created_at;
			$row[] = '<a href="' . base_url() . 'party/party_log_view?id=' . $party->party_id . '">View</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);

	}

	function party_list_outstanding(){
		//set_page('party/party_log_report');
		set_page('party/party_list_outstanding');
	}

	function party_list_outstanding_datatable(){

		$config['table'] = 'party p';
		/*echo "<pre>";
		print_r($_POST);
		echo "</pre>";*/
		$config['select'] = 'p.party_code,p.party_name,p.outstanding_balance';
		$config['column_search'] = array('p.party_code', 'p.party_name', 'p.outstanding_balance');
		$config['column_order'] = array('p.party_code', 'p.party_name', 'p.outstanding_balance');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		$config['order'] = array('p.party_name' => 'ASC');

		if ($_POST['outstanding_sort'] == 0) {
		} elseif ($_POST['outstanding_sort'] == 1) {
			$config['wheres'][] = array('column_name' => 'p.outstanding_balance <', 'column_value' => "0");

		} elseif ($_POST['outstanding_sort'] == 2) {
			$config['wheres'][] = array('column_name' => 'p.outstanding_balance <=', 'column_value' => "0");
		} elseif ($_POST['outstanding_sort'] == 3) {
			$config['wheres'][] = array('column_name' => 'p.outstanding_balance =', 'column_value' => "0");
		} elseif ($_POST['outstanding_sort'] == 4) {
			$config['wheres'][] = array('column_name' => 'p.outstanding_balance >', 'column_value' => "0");
		}

		if(!empty($_POST['enquiry_created'])) {
			$config['joins'][] = array('join_table' => 'inquiry i', 'join_by' => 'i.party_id = p.party_id');
		}

		if(!empty($_POST['quotation_created'])) {
			$config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'q.party_id = p.party_id');
		}

		if(!empty($_POST['proforma_invoice_created'])) {
			$config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.sales_to_party_id = p.party_id');
		}

		if(!empty($_POST['order_created'])) {
			$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.sales_to_party_id = p.party_id');
		}

		if(!empty($_POST['challan_created'])) {
			$config['joins'][] = array('join_table' => 'challans ch', 'join_by' => 'ch.sales_to_party_id = p.party_id');
		}

		if(!empty($_POST['invoice_created'])) {
			$config['joins'][] = array('join_table' => 'invoices in', 'join_by' => 'in.sales_to_party_id = p.party_id');
		}

		$config['group_by'] = "p.party_id";
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$cnt = 0;
		foreach ($list as $party) {
			$row = array();
			//$row[] = $action;
			$row[] = $party->party_code;
			$row[] = $party->party_name;
			$row[] = $party->outstanding_balance;
			//$row[] = $party->email_id;
			//$row[] = $party->website;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function party_log_detail_datatable(){
		$config['table'] = 'party_log p';

		$config['joins'][1]['join_table'] = "staff s";
		$config['joins'][1]['join_by'] = "p.created_by=s.staff_id";
		$config['joins'][1]['join_type'] = "left";

		$config['select'] = 'p.*,s.name as created_by_name';
		$config['column_search'] = array();
		$config['column_order'] = array();
		$config['order'] = array('p.party_id,p.id ' => 'ASC');
		$config['wheres'][] = array('column_name'=>'p.party_id','column_value'=>$_POST['party_id']);

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		foreach ($list as $party) {
			$row = array();
			if($party->operation_type == 'insert') {}
			$row[] = $party->id;
			$row[] = $party->operation_type;
			$row[] = $party->party_id;
			$row[] = $party->party_code;
			$row[] = $party->party_name;
			$row[] = $party->reference_id;
			$row[] = $party->reference_description;
			$row[] = $party->party_type_1;
			$row[] = $party->party_type_2;
			$row[] = $party->branch_id;
			$row[] = $party->project;
			$row[] = $party->address;
			$row[] = $party->area;
			$row[] = $party->pincode;
			$row[] = $party->city_id;
			$row[] = $party->state_id;
			$row[] = $party->country_id;
			$row[] = $party->phone_no;
			$row[] = $party->fax_no;
			$row[] = $party->email_id;
			$row[] = $party->website;
			$row[] = $party->opening_bal;
			$row[] = $party->credit_limit;
			$row[] = $party->active;
			$row[] = $party->tin_vat_no;
			$row[] = $party->tin_cst_no;
			$row[] = $party->ecc_no;
			$row[] = $party->pan_no;
			$row[] = $party->range;
			$row[] = $party->division;
			$row[] = $party->service_tax_no;
			$row[] = $party->utr_no;
			$row[] = $party->w_address;
			$row[] = $party->w_city;
			$row[] = $party->w_state;
			$row[] = $party->w_country;
			$row[] = $party->w_email;
			$row[] = $party->w_web;
			$row[] = $party->w_phone1;
			$row[] = $party->w_phone2;
			$row[] = $party->w_phone3;
			$row[] = $party->w_note;
			$row[] = $party->created_by_name;
			$row[] = date('d-m-Y',strtotime($party->created_at));
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);

	}

	function contact_person_log_detail_datatable(){
		$config['table'] = 'contact_person_log cp';

		$config['joins'][0]['join_table'] = "department dept";
		$config['joins'][0]['join_by'] = " dept.department_id=cp.department_id ";
		$config['joins'][0]['join_type'] = "left";

		$config['joins'][1]['join_table'] = "designation desi";
		$config['joins'][1]['join_by'] = "desi.designation_id=cp.designation_id";
		$config['joins'][1]['join_type'] = "left";

		$config['select'] = 'cp.*,dept.department,desi.designation';
		$config['column_search'] = array('cp.name','cp.operation_type','cp.phone_no','cp.mobile_no','cp.fax_no','cp.email','cp.priority','cp.note','dept.department','desi.designation');
		$config['column_order'] = array('cp.name','cp.operation_type','cp.phone_no','cp.mobile_no','cp.fax_no','cp.email','cp.priority','cp.note','dept.department','desi.designation');
		$config['order'] = array('cp.contact_person_id,cp.id ' => 'ASC');
		$config['wheres'][] = array('column_name'=>'cp.party_id','column_value'=>$_POST['party_id']);
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		foreach ($list as $party) {
			$row = array();
			if($party->operation_type == 'insert') {

			}
			$row[] = $party->id;
			$row[] = $party->created_at;
			$row[] = $party->operation_type;
			$row[] = $party->priority;
			$row[] = $party->name;
			$row[] = $party->designation;
			$row[] = $party->department;
			$row[] = $party->mobile_no;
			$row[] = $party->phone_no;
			$row[] = $party->email;
			$row[] = $party->fax_no;
			$row[] = $party->note;
			$row[] = $party->created_by;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	/**
	 * @param $party_id
	 * @return int
	 */
	function get_max_priority($party_id){
		$this->db->select_max('priority');
		$this->db->where('party_id',$party_id);
		$this->db->where('active !=',0);
		$query = $this->db->get('contact_person');
		if ($query->num_rows() > 0)
		{
			$priority = (int) $query->row()->priority + 1;
			echo $priority;
			exit();
		}
		echo 1;
		exit();
	}
	/**
	 * @param $party_id
	 * @return mixed
	 */
	function get_party_by_id($party_id, $viewpage = ''){
		$select = 'p.*,r.reference,sales.sales as party_type_1_name,sales_type.sales_type as party_type_2_name,b.branch,c.city,s.state,cnt.country';
		$this->db->select($select);
		$this->db->from('party p');
		$this->db->join('reference r', 'r.id = p.reference_id', 'left');
		$this->db->join('sales', 'sales.id = p.party_type_1', 'left');
		$this->db->join('sales_type', 'sales_type.id = p.party_type_2', 'left');
		$this->db->join('branch b', 'b.branch_id = p.branch_id', 'left');
		$this->db->join('city c', 'c.city_id = p.city_id', 'left');
		$this->db->join('state s', 's.state_id = p.state_id', 'left');
		$this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
		$this->db->where('p.party_id', $party_id);
		$this->db->where('p.active !=', 0);
		$this->db->limit(1);
		if ($this->input->is_ajax_request()) {
			$party_data = $this->db->get()->row_array(0);
			$party_data['contact_persons_html'] = '';

			$this->db->select('cp.contact_person_id,cp.name,cp.email,cp.phone_no,cp.mobile_no,cp.priority,d.department,de.designation');
			$this->db->from('contact_person cp');
			$this->db->join('department d', 'd.department_id = cp.department_id', 'left');
			$this->db->join('designation de', 'de.designation_id = cp.designation_id', 'left');
			$this->db->where('cp.party_id', $party_id);
			$this->db->where('cp.active !=',0);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $contact_persons_row) {
					$party_data['contact_persons_html'] .= "<tr>";
					$action = '';
					if($viewpage == 'view'){ 
					} else {
						$action .= '<a class="btn btn-xs btn-danger btn-delete-contact-person" href="javascript:void(0);" data-id="' . $contact_persons_row->contact_person_id . '"><i class="fa fa-remove"></i></a>';
						$action .= ' <a class="btn btn-xs btn-primary btn-edit-contact-person" href="javascript:void(0);" data-id="' . $contact_persons_row->contact_person_id . '"><i class="fa fa-edit"></i></a>';
					}
					$party_data['contact_persons_html'] .= "<td>" . $action . "</td>";
					$party_data['contact_persons_html'] .= "<td>" . $contact_persons_row->priority . "</td>";
					$party_data['contact_persons_html'] .= "<td>" . $contact_persons_row->name . "</td>";
					$party_data['contact_persons_html'] .= "<td>" . $contact_persons_row->designation . "</td>";
					$party_data['contact_persons_html'] .= "<td>" . $contact_persons_row->department . "</td>";
					$party_data['contact_persons_html'] .= "<td>" . $contact_persons_row->mobile_no . "</td>";
					$party_data['contact_persons_html'] .= "<td>" . $contact_persons_row->email . "</td>";
					$party_data['contact_persons_html'] .= "</tr>";
				}
			}
			$party_data['created_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $party_data['created_by']));
			$party_data['created_at'] = substr($party_data['created_at'], 8, 2) .'-'. substr($party_data['created_at'], 5, 2) .'-'. substr($party_data['created_at'], 0, 4);
			$party_data['updated_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $party_data['updated_by']));
			$party_data['updated_at'] = substr($party_data['updated_at'], 8, 2) .'-'. substr($party_data['updated_at'], 5, 2) .'-'. substr($party_data['updated_at'], 0, 4);
			$party_data['party_created_date'] = substr($party_data['party_created_date'], 8, 2) .'-'. substr($party_data['party_created_date'], 5, 2) .'-'. substr($party_data['party_created_date'], 0, 4);
			echo json_encode($party_data);
			exit();
		} else {
			return $this->db->get()->row(0);
		}
	}

	function print_envelope($party_id){
		$this->db->select('p.party_name,p.address,p.phone_no,p.email_id,c.city,s.state,cnt.country,cp.name as cp_name,cp.mobile_no as cp_mobile_no,');
		$this->db->from('party p');
		$this->db->join('city c', 'c.city_id = p.city_id', 'left');
		$this->db->join('state s', 's.state_id = p.state_id', 'left');
		$this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
		$this->db->join('contact_person cp', 'cp.party_id = p.party_id', 'left');
		$this->db->where('p.party_id', $party_id);
		$this->db->where('p.active !=',0);
		$this->db->order_by('cp.priority', 'asc');
		$this->db->limit(1);
		$data = $this->db->get()->row_array();
		$data['phone_no'] = explode(',', $data['phone_no']);
		$data['phone_no'] = $data['phone_no'][0];
		$data['email_id'] = explode(',', $data['email_id']);
		$data['email_id'] = $data['email_id'][0];

		if (strtolower(trim($data['party_name'])) == strtolower(trim($data['cp_name']))) {
			$html = '<table style="width:100%;">';
			$html .= '<tr>';
			$html .= '<td style="width:50%;"><strong>To : ' . $data['party_name'] . '</strong></td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['address'] . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['city'] . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['state'] . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['country'] . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>Contact No. : ' . $data['phone_no'] . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>Email ID : ' . $data['email_id'] . '</td>';
			$html .= '</tr></table>';
		} else {
			$html = '<table style="width:100%;">';
			$html .= '<tr>';
			$html .= '<td style="width:50%;"><strong>To : ' . $data['party_name'] . '</strong></td>';
			$html .= '<td style="width:50%;">Kind Attn. : ' . $data['cp_name'] . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['address'] . '</td>';
			if ($data['cp_mobile_no'] != '') {
				$html .= '<td>Contact No. : ' . $data['cp_mobile_no'] . '</td>';
			} else {
				$html .= '<td>&nbsp;</td>';
			}
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['city'] . '</td>';
			$html .= '<td>&nbsp;</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['state'] . '</td>';
			$html .= '<td>&nbsp;</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>' . $data['country'] . '</td>';
			$html .= '<td>&nbsp;</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>Contact No. : ' . $data['phone_no'] . '</td>';
			$html .= '<td>&nbsp;</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td>Email ID : ' . $data['email_id'] . '</td>';
			$html .= '<td>&nbsp;</td>';
			$html .= '</tr></table>';
		}
		/*echo $html;die;*/
		$this->load->library('m_pdf');
		$pdf = new mPDF();
		$pdf->WriteHTML($html);
		$pdf->Output('envelope.pdf', 'I');
	}

	function fetch_party_contact_person($party_id)
	{
		$this->db->select('*');
		$this->db->where("party_id", $party_id);
		$this->db->where("active !=",0);
		$table_result = $this->db->get('contact_person')->result_array();
		return $table_result;
	}

	public function fetch_field_by_table_name($table_name = '', $field_name = '', $where_field = '', $where_value = 0)
	{
		$this->db->select($field_name);
		$this->db->where($where_field, $where_value);
		$result = $this->db->get($table_name)->result_array();
		if (count($result) > 0) {
			return $result[0][$field_name];
		} else {
			return '';
		}
	}

	function fetch_state_country_by_city()
	{
		$city_id = $this->input->get_post("city_id");

		$stateName = "";
		$countryName = "";

		if ($city_id > 0) {
			$sql = "SELECT * FROM city WHERE city_id='$city_id'";
			$data = $this->crud->getFromSQL($sql);
			if (count($data) > 0) {
				$data = $data[0];
				$state_id = $data->state_id;
				$country_id = $data->country_id;

				$sql = "SELECT * FROM state WHERE state_id='$state_id'";
				$data = $this->crud->getFromSQL($sql);

				$sql = "SELECT * FROM country WHERE country_id='$country_id'";
				$data2 = $this->crud->getFromSQL($sql);

				if (count($data) > 0) {
					$data = $data[0];
					$stateName = $data->state;
				}

				if (count($data2) > 0) {
					$data = $data2[0];
					$countryName = $data->country;
				}

			}
		}

		echo json_encode(array("stateName" => $stateName, 'countryName' => $countryName));
		exit;
	}

	function fetch_autocomplete_city()
	{
		$search = $_GET['term'];
		$city = $this->app_model->getAutoCompleteData('city', 'city', $search);
		$response = array();
		$i = 0;
		foreach ($city as $cityRow) {
			$response[$i]['label'] = $cityRow->city;
			$response[$i]['value'] = $cityRow->city;
			$response[$i]['id'] = $cityRow->city_id;
			$i++;
		}
		echo json_encode($response);
	}

	function load_party_detail($party_id)
	{
		echo $party_id;
	}

	function fetch_autocomplete_state()
	{
		$search = $_GET['term'];
		$state = $this->app_model->getAutoCompleteData('state', 'state', $search);
		$response = array();
		$i = 0;
		foreach ($state as $stateRow) {
			$response[$i] = $stateRow->state;
			$i++;
		}
		echo json_encode($response);
	}

	function fetch_autocomplete_country()
	{
		$search = $_GET['term'];
		$country = $this->app_model->getAutoCompleteData('country', 'country', $search);
		$response = array();
		$i = 0;
		foreach ($country as $countryRow) {
			$response[$i] = $countryRow->country;
			$i++;
		}
		echo json_encode($response);
	}

	/************************ Start For Country, State, City *************************/

	function country($country_id="")
	{
		$country = array();
		if (isset($country_id) && !empty($country_id)) {
			$where_array['country_id'] = $country_id;
			$country = $this->crud->get_all_with_where('country', 'country', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('country', 'country', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['country_id'] = ($country) ? $country[0]->country_id : '';
		$return['country_name'] = ($country) ? $country[0]->country : '';

		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_COUNTRY_MENU_ID,"view")) {
			set_page('party/country', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_country()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('country', $post_data);
		if ($result) {
			$post_data['country_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Country Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_country()
	{
		$post_data = $this->input->post();
		$where_array['country_id'] = $post_data['country_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('country', $post_data, array('country_id' => $post_data['country_id']));
		if ($result) {
			$post_data['country_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Country Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function state($state_id="")
	{
		$state = array();
		if (isset($state_id) && !empty($state_id)) {
			$where_array['state_id'] = $state_id;
			$state = $this->crud->get_all_with_where('state', 'state', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('state', 'state', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['state_id'] = ($state) ? $state[0]->state_id : '';
		$return['country_id'] = ($state) ? $state[0]->country_id : '';
		$return['state_name'] = ($state) ? $state[0]->state : '';

		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID,"view")) {
			set_page('party/state', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_state()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('state', $post_data);
		if ($result) {
			$post_data['state_id'] = $result;
			$post_data['country'] = $this->crud->get_column_value_by_id('country', 'country', array('country_id' => $post_data['country_id']));
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'State Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_state()
	{
		$post_data = $this->input->post();
		$where_array['state_id'] = $post_data['state_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('state', $post_data, array('state_id' => $post_data['state_id']));
		if ($result) {
			$post_data['state_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'State Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function get_state()
	{
		echo $this->crud->get_state();
	}

	function city($city_id="")
	{
		$city = array();
		if (isset($city_id) && !empty($city_id)) {
			$where_array['city_id'] = $city_id;
			$city = $this->crud->get_all_with_where('city', 'city', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('city', 'city', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['city_id'] = ($city) ? $city[0]->city_id : '';
		$return['state_id'] = ($city) ? $city[0]->state_id : '';
		$return['country_id'] = ($city) ? $city[0]->country_id : '';
		$return['city_name'] = ($city) ? $city[0]->city : '';

		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID,"view")) {
			set_page('party/city', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function city_datatable()
	{
		$config['table'] = 'city ct';
		$config['select'] = 'ct.*,s.state,c.country';
		$config['column_order'] = array(null, 'ct.city', 's.state', 'c.country',);
		$config['column_search'] = array('ct.city', 's.state', 'c.country');
		$config['joins'][0] = array('join_table' => 'state s', 'join_by' => 's.state_id = ct.state_id');
		$config['joins'][1] = array('join_table' => 'country c', 'join_by' => 'c.country_id = ct.country_id');
		$config['order'] = array('ct.city' => 'asc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "delete");
		$data = array();
		foreach ($list as $city_row) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '<a href="' . base_url('party/city/' . $city_row->city_id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> | ';	
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('party/delete/' . $city_row->city_id) . '"><i class="fa fa-trash"></i></a>';	
			}
			$row[] = $action;
			$row[] = $city_row->city;
			$row[] = $city_row->state;
			$row[] = $city_row->country;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function state_datatable()
	{
		$config['table'] = 'state s';
		$config['select'] = 's.*,c.country';
		$config['column_order'] = array(null, 's.state', 'c.country',);
		$config['column_search'] = array('s.state', 'c.country');
		$config['joins'][1] = array('join_table' => 'country c', 'join_by' => 'c.country_id = s.country_id');
		$config['order'] = array('s.state' => 'asc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "delete");
		$data = array();
		foreach ($list as $state_row) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '<a href="' . base_url('party/state/' . $state_row->state_id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> | ';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('party/delete/' . $state_row->state_id) . '"><i class="fa fa-trash"></i></a>';	
			}
			$row[] = $action;
			$row[] = $state_row->state;
			$row[] = $state_row->country;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function add_city()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('city', $post_data);
		if ($result) {
			$post_data['city_id'] = $result;
			$post_data['country'] = $this->crud->get_column_value_by_id('country', 'country', array('country_id' => $post_data['country_id']));
			$post_data['state'] = $this->crud->get_column_value_by_id('state', 'state', array('state_id' => $post_data['state_id']));
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'City Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_city()
	{
		$post_data = $this->input->post();
		$where_array['city_id'] = $post_data['city_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('city', $post_data, array('city_id' => $post_data['city_id']));
		if ($result) {
			$post_data['city_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'City Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function party_type1($id)
	{
		$party_type1 = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$type = $this->crud->get_all_with_where('party_type_1', 'TYPE', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('party_type_1', 'id', 'ASC');
		set_page('party/party_type1', array('results' => $result_data, 'id' => $type[0]->id, 'type' => $type[0]->type));
	}

	function add_party_type1()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('party_type_1', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Party Type 1 Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_party_type1()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('party_type_1', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Party Type 1 Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function party_type2($id)
	{
		$party_type2 = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$type = $this->crud->get_all_with_where('party_type_2', 'TYPE', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('party_type_2', 'id', 'ASC');
		set_page('party/party_type2', array('results' => $result_data, 'id' => $type[0]->id, 'type' => $type[0]->type));
	}

	function add_party_type2()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('party_type_2', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Party Type 2 Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_party_type2()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('party_type_2', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Party Type 2 Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function reference($id="")
	{
		$reference = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$reference = $this->crud->get_all_with_where('reference', 'reference', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('reference', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($reference) ? $reference[0]->id : '';
		$return['reference'] = ($reference) ? $reference[0]->reference : '';

		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_REFERENCE_MENU_ID,"view")) {
			set_page('party/reference', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_reference()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('reference', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Reference Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_reference()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('reference', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Reference Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function designation($designation_id="")
	{
		$designation = array();
		if (isset($designation_id) && !empty($designation_id)) {
			$where_array['designation_id'] = $designation_id;
			$designation = $this->crud->get_all_with_where('designation', 'designation', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('designation', 'designation', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['designation_id'] = ($designation) ? $designation[0]->designation_id : '';
		$return['designation_name'] = ($designation) ? $designation[0]->designation : '';

		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_DESIGNATION_MENU_ID,"view")) {
			set_page('party/designation', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_designation()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('designation', $post_data);
		if ($result) {
			$post_data['designation_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Designation Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_designation()
	{
		$post_data = $this->input->post();
		$where_array['designation_id'] = $post_data['designation_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('designation', $post_data, array('designation_id' => $post_data['designation_id']));
		if ($result) {
			$post_data['designation_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Designation Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function terms_group($id="")
	{
		$terms_group = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$terms_group = $this->crud->get_all_with_where('terms_group', 'terms_group', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('terms_group', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($terms_group) ? $terms_group[0]->id : '';
		$return['terms_group'] = ($terms_group) ? $terms_group[0]->terms_group : '';
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_TERMS_GROUP_MENU_ID,"view")) {
			set_page('party/terms_group', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_terms_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('terms_group', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Terms Condition Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_terms_group()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('terms_group', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Terms Condition Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function company_banks($id = '') {
		$company_banks = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$company_banks = $this->crud->get_all_with_where('company_banks', 'id', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('company_banks', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($company_banks) ? $company_banks[0]->id : '';
		$return['company_banks'] = ($company_banks) ? $company_banks[0]->detail : '';
		$return['for_domestic'] = ($company_banks) ? $company_banks[0]->for_domestic : '';
		$return['for_export'] = ($company_banks) ? $company_banks[0]->for_export : '';
		if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_COMPANY_BANKS_MENU_ID, "view")) {
			set_page('party/company_banks', $return);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function save_company_banks() {
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$post_data = $this->input->post();
        $post_data['for_domestic'] = isset($_POST['for_domestic']) ? $_POST['for_domestic'] : '0';
        $post_data['for_export'] = isset($_POST['for_export']) ? $_POST['for_export'] : '0';
		if(isset($post_data['id']) && !empty($post_data['id'])){
			$post_data['updated_by'] = $this->staff_id;
			$post_data['updated_at'] = $this->now_time;
			$result = $this->crud->update('company_banks', $post_data, array('id' => $post_data['id']));
			if ($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Company Bank Updated Successfully');
			}
		} else {
			$post_data['created_by'] = $this->staff_id;
			$post_data['created_at'] = $this->now_time;
			$result = $this->crud->insert('company_banks', $post_data);
			if ($result){
				$return['success'] = "Added";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Company Bank Added Successfully');
			}
		}
		echo json_encode($return);
		exit;
	}

	function sms_topic($id='')
	{
		$sms_topic = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$sms_topic = $this->crud->get_all_with_where('sms_topic','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('sms_topic', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sms_topic) ? $sms_topic[0]->id : '';
		$return['sms_topic'] = ($sms_topic) ? $sms_topic[0]->topic_name : '';
		$return['message'] = ($sms_topic) ? $sms_topic[0]->message : '';
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_SMS_TOPICS_MENU_ID,"view")) {
			set_page('party/sms_topic', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_sms_topic()
	{
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$post_data = $this->input->post();
		$post_data['created_by'] = $this->staff_id;
		$post_data['created_at'] = $this->now_time;
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->insert('sms_topic',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sms Topic Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_sms_topic()
	{
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->update('sms_topic', $post_data, array('id' => $post_data['id']));
		if ($result) { 
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sms Topic Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table, array($id_name => $id));
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Deleted Successfully');
	}

	public function ajax_get_state($city_id)
	{
		$this->load->helper('url');
		$row = $this->crud->get_data_row_by_id('city', 'city_id', $city_id);

		$state = $this->crud->get_data_row_by_id('state', 'state_id', $row->state_id);
		$country = $this->crud->get_data_row_by_id('country', 'country_id', $row->country_id);

		$return['state_id'] = $row->state_id;
		$return['country_id'] = $row->country_id;
		if(!empty($state)){
			$return['state'] = '<option value="' . $state->state_id . '" selected>' . $state->state . '</option>';	
		}
		//print json_encode($return);
		if(!empty($country)){
			$return['country'] = '<option value="' . $country->country_id . '" selected>' . $country->country . '</option>';	
		}
		print json_encode($return);
		exit;
		echo '<option value="' . $state->state_id . '" selected disabled>' . $state->state . '</option>';
		//$this->load->view('ajax_get_finish',$data);
	}

	public function ajax_get_country($state_id){
		$this->load->helper('url');
		$row = $this->crud->get_data_row_by_id('state', 'state_id', $state_id);
		$country = $this->crud->get_data_row_by_id('country', 'country_id', $row->country_id);
		$return['country_id'] = $row->country_id;
		if(!empty($country)){
			$return['country'] = '<option value="' . $country->country_id . '" selected>' . $country->country . '</option>';	
		}		
		print json_encode($return);
		exit;		
	}

	function delete_contact_person($id)
	{
		/*$this->db->where('contact_person_id', $id);
		$this->db->delete('contact_person');*/
		$this->crud->update('contact_person',array('active'=>0),array('contact_person_id'=>$id));
		echo json_encode(array('success' => true, 'message' => 'Deleted successfully!'));
		exit();
	}

	function get_contact_person_by_id($id)
	{
		$this->db->select('cp.*,d.department,de.designation');
		$this->db->from('contact_person cp');
		$this->db->join('department d', 'd.department_id = cp.department_id', 'left');
		$this->db->join('designation de', 'de.designation_id = cp.designation_id', 'left');
		$this->db->where('contact_person_id', $id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			echo json_encode(array('success' => true, 'message' => 'Contact person data loaded', 'contact_person_data' => $query->row_array(0)));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Contact persons not found!'));
			exit();
		}
	}

	function edit_contact_person($id)
	{
		$post_data = array();
		if (isset($_POST['contact_person_name'])) {
			$post_data['name'] = $_POST['contact_person_name'];
		}
		if (isset($_POST['contact_person_phone_no'])) {
			$post_data['phone_no'] = $_POST['contact_person_phone_no'];
		}
		if (isset($_POST['contact_person_mobile_no'])) {
			$post_data['mobile_no'] = $_POST['contact_person_mobile_no'];
		}
		if (isset($_POST['contact_person_fax_no'])) {
			$post_data['fax_no'] = $_POST['contact_person_fax_no'];
		}
		if (isset($_POST['contact_person_email'])) {
			$post_data['email'] = $_POST['contact_person_email'];
		}
		if (isset($_POST['contact_person_designation_id'])) {
			$post_data['designation_id'] = $_POST['contact_person_designation_id'];
		}
		if (isset($_POST['contact_person_department_id'])) {
			$post_data['department_id'] = $_POST['contact_person_department_id'];
		}
		if (isset($_POST['contact_person_note'])) {
			$post_data['note'] = $_POST['contact_person_note'];
		}
		if (isset($_POST['contact_person_priority'])) {
			$post_data['priority'] = $_POST['contact_person_priority'];
		}

		$post_data['updated_by'] = $this->session->userdata('is_logged_in')['staff_id'];

		$this->db->where('contact_person_id', $id);
		$this->db->update('contact_person', $post_data);
		echo json_encode(array('success' => true, 'message' => 'Saved successfully!'));
		exit();
	}

	function check_is_unique()
	{
		//echo '<pre>';print_r($_POST); exit;
		$table_name = trim($_POST['table_name']);
		$column_name = trim($_POST['column_name']);
		$column_value = trim($_POST['column_value']);
		$id_column_name1 = isset($_POST['id_column_name1']) ? trim($_POST['id_column_name1']) : '';
		$id_column_value1 = isset($_POST['id_column_value1']) ? trim($_POST['id_column_value1']) : '';
		$id_column_name2 = isset($_POST['id_column_name2']) ? trim($_POST['id_column_name2']) : '';
		$id_column_value2 = isset($_POST['id_column_value2']) ? trim($_POST['id_column_value2']) : '';

		$this->db->select($column_name);
		$this->db->from($table_name);
		if ($id_column_name1 != '' && $id_column_value1 != '') {
			$this->db->where("$id_column_name1 !=", $id_column_value1);
		}
		if ($id_column_name2 != '' && $id_column_value2 != '') {
			$this->db->where("$id_column_name2 =", $id_column_value2);
		}
		$this->db->where("LOWER($column_name)", strtolower($column_value));
		if ($this->db->get()->num_rows() > 0) {
			echo "0";
		} else {
			echo "1";
		}
		//ECHO $this->db->last_query();
		exit();
	}

	function check_priority_is_unique()
	{
		$party_id = $_POST['party_id'];
		$priority = $_POST['priority'];
		$contact_person_id = isset($_POST['contact_person_id']) ? $_POST['contact_person_id'] : '';
		$this->db->select('party_id');
		$this->db->from('contact_person');
		if ($contact_person_id != '') {
			$this->db->where("contact_person_id !=", $contact_person_id);
		}
		$this->db->where("active !=",0);
		$this->db->where("party_id", $party_id);
		$this->db->where("priority", $priority);
		if ($this->db->get()->num_rows() > 0) {
			echo "0";
		} else {
			echo "1";
		}
		exit();
	}
	/************************ End For Country *************************/

    function transfer_report() {
        $data = array();
		if($this->app_model->have_access_role(PARTY_MODULE_ID, "is_management") == 1){
			set_page('party/transfer_report', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function transfer_report_list_datatable() {

        $post_data = $this->input->post();
        $config['table'] = 'party_transfer_history pth';
        $config['select'] = 'pth.id,pth.module_table_name,pth.module_table_id,pth.party_id,p.party_code,pth.transfer_from,pth.transfer_to,pth.created_by,pth.created_at,p.party_name,st.name as from_name,stf.name as to_name,p.phone_no,p.email_id,c.city,s.state,cy.country';
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'pth.party_id = p.party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff st', 'join_by' => 'pth.transfer_from = st.staff_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff stf', 'join_by' => 'pth.transfer_to = stf.staff_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table'=>'city c','join_by'=>'c.city_id = p.city_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'state s','join_by'=>'s.state_id = p.state_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'country cy','join_by'=>'cy.country_id = p.country_id','join_type'=>'left');
        $config['column_search'] = array('pth.id','DATE_FORMAT(pth.created_at,"%d-%m-%Y")' , 'pth.party_id', 'p.party_name','p.phone_no','p.email_id','c.city','s.state','cy.country','st.name','stf.name');
        $config['column_order'] = array(null,'pth.created_at' , 'pth.party_id', 'p.party_name','st.name','stf.name','p.phone_no','p.email_id','c.city','s.state','cy.country');
        $config['wheres'][] = array('column_name'=> 'pth.module_table_name', 'column_value' => 'party');
        $config['order'] = array('DATE_FORMAT(pth.created_at,"%d-%m-%Y")' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();

        foreach ($list as $transfer_list) {
            $row = array();
            $action = '';
            $action .= '<a href="' . base_url("party/transfer_report_view" . $transfer_list->party_id) . '"><span class="fa fa-eye" style="color : #419bf4" >&nbsp;</span></a>';
//            $row[] = $action;
            $row[] = (!empty(strtotime($transfer_list->created_at))) ? date('d-m-Y h:i A', strtotime($transfer_list->created_at)) : '';
            $row[] = $transfer_list->party_code;
            $row[] = $transfer_list->party_name;
            $row[] = $transfer_list->from_name;
            $row[] = $transfer_list->to_name;
            $phone_no_arr = preg_split('/[\\n\,]+/', $transfer_list->phone_no);
			$phone_nos = '';
			$p_inc = 1;
			foreach($phone_no_arr as $phone_no){
				if(!empty(trim($phone_no))){
					$phone_nos .= $phone_no.', ';
					if($p_inc%2 == 0){ $phone_nos .= '<br />'; }
					$p_inc++;
				}
			}
            $row[] = $phone_nos;
            $email_id_arr = explode(',', $transfer_list->email_id);
			$email_ids = '';
			foreach($email_id_arr as $email_id){
				if(!empty(trim($email_id))){
					$email_ids .= $email_id.', ';
					$email_ids .= '<br />';
				}
			}
			$row[] = $email_ids;
            $row[] = $transfer_list->city;
            $row[] = $transfer_list->state;
            $row[] = $transfer_list->country;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
}
