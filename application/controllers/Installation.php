<?php

ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Installation extends CI_Controller {

	protected $staff_id = 1;

	function __construct() {
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}
	
	function pick_testing_report_datatable() {
		$config['table'] = 'testing_report tr';
		$config['select'] = 'tr.testing_report_id,tr.testing_report_no,tr.testing_report_no_year,tr.challan_id,c.challan_no,c.id,ci.item_code,p.party_name,p.phone_no,p.email_id,so.sales_order_no,pi.proforma_invoice_no,qso.quotation_no AS qso_quotation_no,qso.enquiry_no AS qso_enquiry_no,qpi.quotation_no AS qpi_quotation_no,qpi.enquiry_no AS qpi_enquiry_no,city.city,state.state,country.country';
		$config['joins'][] = array('join_table' => 'challans c', 'join_by' => 'tr.challan_id = c.id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id', 'join_type' => 'left');

		$config['joins'][] = array('join_table' => 'quotations qso', 'join_by' => 'qso.id = so.quotation_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotations qpi', 'join_by' => 'qpi.id = pi.quotation_id', 'join_type' => 'left');

		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

		$config['column_order'] = array(null, 'tr.testing_report_no', 'c.challan_no', null, null, 'ci.item_code', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('tr.testing_report_no', 'c.challan_no', 'so.sales_order_no', 'pi.proforma_invoice_no', 'qso.quotation_no', 'qso.enquiry_no', 'ci.item_code', 'qpi.quotation_no', 'qpi.enquiry_no', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['order'] = array('tr.testing_report_id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '1');
		$config['wheres'][] = array('column_name' => 'tr.is_installation_created', 'column_value' => '0');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");

		if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {

		} elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$role_delete = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "edit");
		foreach ($list as $testing_report) {
			$row = array();
			$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->testing_report_no_year . '</a>';
			$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->challan_no . '</a>';

			if (!empty($testing_report->sales_order_no)) {
				$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->sales_order_no . '</a>';
				$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->qso_quotation_no . '</a>';
				$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->qso_enquiry_no . '</a>';
			} else {
				$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->proforma_invoice_no . '</a>';
				$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->qpi_quotation_no . '</a>';
				$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->qpi_enquiry_no . '</a>';
			}

			$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->item_code . '</a>';
            $row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->party_name . '</a>';
            $phone_no = substr($testing_report->phone_no,0,10);
            $testing_report->phone_no = str_replace(',', ', ', $testing_report->phone_no);
            $row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" data-toggle="tooltip" data-placement="bottom" title="'.$testing_report->phone_no.'">' . $phone_no . '</a>';
			$email_id = substr($testing_report->email_id,0,14);
            $testing_report->email_id = str_replace(',', ', ', $testing_report->email_id);
            $row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" data-toggle="tooltip" data-placement="bottom" title="'.$testing_report->email_id.'">' . $email_id . '</a>';
			$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->city . '</a>';
			$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->state . '</a>';
			$row[] = '<a href="javascript:void(0);" class="testing_report_row" data-testing_report_id="' . $testing_report->testing_report_id . '" data-challan_id="' . $testing_report->challan_id . '" >' . $testing_report->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function add($installation_id = '') {
		$data = array();
		$data['problem_resolutions'] = $this->crud->get_all_records('problem_resolutions', 'label', 'asc');
		if (!empty($installation_id)) {
			if ($this->app_model->have_access_role(INSTALLATION_MODULE_ID, "edit")) {
				$installation_data = $this->crud->get_row_by_id('installation', array('installation_id' => $installation_id));
				if (empty($installation_data)) {
					redirect("/installation/installation_list/");
				}
				$installation_data = $installation_data[0];
				$installation_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $installation_data->created_by));
				$installation_data->created_at = substr($installation_data->created_at, 8, 2) . '-' . substr($installation_data->created_at, 5, 2) . '-' . substr($installation_data->created_at, 0, 4);
				$installation_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $installation_data->updated_by));
				$installation_data->updated_at = substr($installation_data->updated_at, 8, 2) . '-' . substr($installation_data->updated_at, 5, 2) . '-' . substr($installation_data->updated_at, 0, 4);
				$installation_data->party_id = $this->crud->get_column_value_by_id('challans', 'sales_to_party_id', array('id' => $installation_data->challan_id));

				$data['party_contact_person'] = $this->crud->get_contact_person_by_party($installation_data->party_id);
				$data['installation_data'] = $installation_data;
				$where = array('installation_id' => $data['installation_data']->installation_id);
				$daily_working_report_data = $this->crud->get_row_by_id('installation_daily_working_report', $where);
				foreach ($daily_working_report_data as $key => $daily_working_report) {
					$daily_working_report_data[$key]->work_date = (!empty(strtotime($daily_working_report->work_date))) ? date("d-m-Y", strtotime($daily_working_report->work_date)) : null;
				}
				$data['daily_working_report_data'] = json_encode($daily_working_report_data);
				$problem_resolution_data = $this->crud->get_row_by_id('installation_problem_resolutions', $where);
				$data['problem_resolution_data'] = json_encode($problem_resolution_data);

				if (!empty($data['installation_data']->kind_attn_id)) {
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['installation_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if (!empty($designation)) {
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if (!empty($department)) {
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
				//echo '<pre>';print_r($data); exit;
				set_page('service/installation/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if ($this->applib->have_access_role(INSTALLATION_MODULE_ID, "add")) {
				set_page('service/installation/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function get_testing_report() {
		$data = array();
		$testing_report_id = $this->input->get_post("testing_report_id");
		$challan_id = $this->input->get_post("challan_id");

		$where = array('testing_report_id' => $testing_report_id);
		$testing_report_data = $this->crud->get_row_by_id('testing_report', $where);
		$data['testing_report_data'] = $testing_report_data[0];
		if (!empty($data['testing_report_data']->testing_report_date)) {
			$data['testing_report_data']->testing_report_date = date('d-m-Y', strtotime($data['testing_report_data']->testing_report_date));
		}
		if (!empty($data['testing_report_data']->assembling_equipment)) {
			$data['testing_report_data']->assembling_equipment = date('d-m-Y', strtotime($data['testing_report_data']->assembling_equipment));
		}
		if (!empty($data['testing_report_data']->equipment_testing_date)) {
			$data['testing_report_data']->equipment_testing_date = date('d-m-Y', strtotime($data['testing_report_data']->equipment_testing_date));
		}
		if (!empty($data['testing_report_data']->equipment_testing_date)) {
			$data['testing_report_data']->equipment_testing_date = date('d-m-Y', strtotime($data['testing_report_data']->equipment_testing_date));
		}
		if (!empty($data['testing_report_data']->delivery_of_equipment)) {
			$data['testing_report_data']->delivery_of_equipment = date('d-m-Y', strtotime($data['testing_report_data']->delivery_of_equipment));
		}
		$data['testing_report_data']->quality_checked_by_value = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->quality_checked_by);
		$data['testing_report_data']->equipment_inspection_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->equipment_inspection_by);
		$data['testing_report_data']->equipment_testing_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->equipment_testing_by);
		$data['testing_report_data']->our_site_incharge_person_value = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->our_site_incharge_person);

		$challan_sales_order_id = $this->crud->get_id_by_val('challans', 'sales_order_id', 'id', $challan_id);
		$challan_proforma_invoice_id = $this->crud->get_id_by_val('challans', 'proforma_invoice_id', 'id', $challan_id);
        if(!empty($challan_sales_order_id ) && $challan_sales_order_id != 0){
            $sales_order_id = $this->crud->get_id_by_val('sales_order', 'id', 'id', $challan_sales_order_id);
            if(empty($sales_order_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else if (!empty($challan_proforma_invoice_id ) && $challan_proforma_invoice_id != 0){
            $proforma_invoice_id = $this->crud->get_id_by_val('proforma_invoices', 'id', 'id', $challan_proforma_invoice_id);
            if(empty($proforma_invoice_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else {
            $challan_data = $this->crud->get_only_challan_data($challan_id);
        }
		if (empty($challan_sales_order_id)) {
			$challan_data->proforma_invoice_no = $challan_data->sales_order_no;
			unset($challan_data->sales_order_no);
			$challan_data->proforma_invoice_date = date('d-m-Y', strtotime($challan_data->sales_order_date));
			unset($challan_data->sales_order_date);
		}
		if (!empty($challan_data->po_date)) {
			$challan_data->po_date = date('d-m-Y', strtotime($challan_data->po_date));
		}
		if (!empty($challan_data->sales_order_date)) {
			$challan_data->sales_order_date = date('d-m-Y', strtotime($challan_data->sales_order_date));
		}
		if (!empty($challan_data->challan_date)) {
			$challan_data->challan_date = date('d-m-Y', strtotime($challan_data->challan_date));
		}
		$data['challan_data'] = $challan_data;

		$where = array('challan_id' => $challan_id);
		$challan_item_data = $this->crud->get_row_by_id('challan_items', $where);
		$data['challan_item_data'] = $challan_item_data[0];
		if (!empty($data['challan_item_data']->lr_date)) {
			$data['challan_item_data']->lr_date = date('d-m-Y', strtotime($data['challan_item_data']->lr_date));
		}
		$data['challan_item_data']->item_extra_accessories = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $data['challan_item_data']->item_extra_accessories_id));
		$data['party_contact_person'] = $this->crud->get_contact_person_by_party($challan_data->sales_to_party_id);

		$where = array('challan_item_id' => $data['challan_item_data']->id);
		$motor_data = $this->crud->get_row_by_id('challan_items_motor_details', $where);
		//pre_die($motor_data);
		foreach ($motor_data as $motor) {
			$motor->motor_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $motor->motor_user);
			$motor->motor_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $motor->motor_make);
			$motor->motor_hp = $this->crud->get_id_by_val('challan_item_hp', 'hp_name', 'id', $motor->motor_hp);
			$motor->motor_kw = $this->crud->get_id_by_val('challan_item_kw', 'kw_name', 'id', $motor->motor_kw);
			$motor->motor_frequency = $this->crud->get_id_by_val('challan_item_frequency', 'frequency_name', 'id', $motor->motor_frequency);
			$motor->motor_rpm = $this->crud->get_id_by_val('challan_item_rpm', 'rpm_name', 'id', $motor->motor_rpm);
			$motor->motor_volts_cycles = $this->crud->get_id_by_val('challan_item_volts_cycles', 'volts_cycles_name', 'id', $motor->motor_volts_cycles);
		}
		$data['motor_data'] = $motor_data;

		$gearbox_data = $this->crud->get_row_by_id('challan_items_gearbox_details', $where);
		foreach ($gearbox_data as $gearbox) {
			$gearbox->gearbox_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $gearbox->gearbox_user);
			$gearbox->gearbox_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $gearbox->gearbox_make);
			$gearbox->gearbox_gear_type = $this->crud->get_id_by_val('challan_item_gear_type', 'gear_type_name', 'id', $gearbox->gearbox_gear_type);
			$gearbox->gearbox_model = $this->crud->get_id_by_val('challan_item_model', 'model_name', 'id', $gearbox->gearbox_model);
			$gearbox->gearbox_ratio = $this->crud->get_id_by_val('challan_item_ratio', 'ratio_name', 'id', $gearbox->gearbox_ratio);
		}
		$data['gearbox_data'] = $gearbox_data;

		if (!empty($data['challan_data']->kind_attn_id)) {
			$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['challan_data']->kind_attn_id));
            if(!empty($data['contact_person'])){
                $data['contact_person'] = $data['contact_person'][0];
                $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                $data['contact_person']->designation = '';
                if (!empty($designation)) {
                    $data['contact_person']->designation = $designation[0]->designation;
                }
                $data['contact_person']->department = '';
                if (!empty($department)) {
                    $data['contact_person']->department = $department[0]->department;
                }
            }
		}
		//echo '<pre>';print_r($data); exit;
		echo json_encode($data);
		exit;
	}

	function save_installation() {
        $return = array();
		$post_data = $this->input->post();
		//		echo '<pre>';print_r($post_data); exit;
        $send_sms = 0;
        if(isset($post_data['installation_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['installation_data']['send_sms']);
        }
		/* --------- Convert Date as Mysql Format ------------- */
		$post_data['installation_data']['installation_date'] = (!empty(strtotime($post_data['installation_data']['installation_date']))) ? date("Y-m-d", strtotime($post_data['installation_data']['installation_date'])) : null;
		$post_data['installation_data']['jk_technical_person'] = !empty($post_data['installation_data']['jk_technical_person']) ? $post_data['installation_data']['jk_technical_person'] : NULL;
		$post_data['installation_data']['installation_commissioning_by'] = (isset($post_data['installation_data']['installation_commissioning_by'])) ? $post_data['installation_data']['installation_commissioning_by'] : null;
		$post_data['installation_data']['project_training_provide_by'] = (isset($post_data['installation_data']['project_training_provide_by'])) ? $post_data['installation_data']['project_training_provide_by'] : null;
		$post_data['installation_data']['party_id'] = !empty($post_data['party']['party_id']) ? $post_data['party']['party_id'] : NULL;
		$post_data['installation_data']['unloading_date'] = (!empty(strtotime($post_data['installation_data']['unloading_date']))) ? date("Y-m-d", strtotime($post_data['installation_data']['unloading_date'])) : null;
		$post_data['installation_data']['filling_of_foundation_boxes'] = (!empty(strtotime($post_data['installation_data']['filling_of_foundation_boxes']))) ? date("Y-m-d", strtotime($post_data['installation_data']['filling_of_foundation_boxes'])) : null;
		$post_data['installation_data']['installation_start'] = (!empty(strtotime($post_data['installation_data']['installation_start']))) ? date("Y-m-d", strtotime($post_data['installation_data']['installation_start'])) : null;
		$post_data['installation_data']['installation_finished'] = (!empty(strtotime($post_data['installation_data']['installation_finished']))) ? date("Y-m-d", strtotime($post_data['installation_data']['installation_finished'])) : null;
		$post_data['installation_data']['electric_power'] = (isset($post_data['installation_data']['electric_power'])) ? $post_data['installation_data']['electric_power'] : null;
		$post_data['installation_data']['elec_rm2_panel_board_cable'] = (isset($post_data['installation_data']['elec_rm2_panel_board_cable'])) ? $post_data['installation_data']['elec_rm2_panel_board_cable'] : null;
		$post_data['installation_data']['connection_cable_1'] = (isset($post_data['installation_data']['connection_cable_1'])) ? $post_data['installation_data']['connection_cable_1'] : null;
		$post_data['installation_data']['water_tank_chiller_system_1'] = (isset($post_data['installation_data']['water_tank_chiller_system_1'])) ? $post_data['installation_data']['water_tank_chiller_system_1'] : null;
		$post_data['installation_data']['water_plumbing_chiller_conn_1'] = (isset($post_data['installation_data']['water_plumbing_chiller_conn_1'])) ? $post_data['installation_data']['water_plumbing_chiller_conn_1'] : null;
		$post_data['installation_data']['lubrication_gear_oil_1'] = (isset($post_data['installation_data']['lubrication_gear_oil_1'])) ? $post_data['installation_data']['lubrication_gear_oil_1'] : null;
		$post_data['installation_data']['gear_box_oil_1'] = (isset($post_data['installation_data']['gear_box_oil_1'])) ? $post_data['installation_data']['gear_box_oil_1'] : null;
		$post_data['installation_data']['raw_material_1'] = (isset($post_data['installation_data']['raw_material_1'])) ? $post_data['installation_data']['raw_material_1'] : null;
		$post_data['installation_data']['run_start_date'] = (!empty(strtotime($post_data['installation_data']['run_start_date']))) ? date("Y-m-d", strtotime($post_data['installation_data']['run_start_date'])) : null;
		$post_data['installation_data']['production_start'] = (!empty(strtotime($post_data['installation_data']['production_start']))) ? date("Y-m-d", strtotime($post_data['installation_data']['production_start'])) : null;
		$post_data['installation_data']['product_quality'] = (isset($post_data['installation_data']['product_quality'])) ? $post_data['installation_data']['product_quality'] : null;
		$post_data['installation_data']['technician_of_jk_installed'] = (isset($post_data['installation_data']['technician_of_jk_installed'])) ? $post_data['installation_data']['technician_of_jk_installed'] : null;
		$post_data['installation_data']['technician_from_jk_accompanied'] = (isset($post_data['installation_data']['technician_from_jk_accompanied'])) ? $post_data['installation_data']['technician_from_jk_accompanied'] : null;
		$post_data['installation_data']['technician_from_jk_helped'] = (isset($post_data['installation_data']['technician_from_jk_helped'])) ? $post_data['installation_data']['technician_from_jk_helped'] : null;
		$post_data['installation_data']['performance_of_our_equipment'] = (isset($post_data['installation_data']['performance_of_our_equipment'])) ? $post_data['installation_data']['performance_of_our_equipment'] : null;
		$post_data['installation_data']['party_id'] = (isset($post_data['party']['party_id'])) ? $post_data['party']['party_id'] : null;
		$post_data['installation_data']['tractor_loader'] = (isset($post_data['installation_data']['tractor_loader']) && !empty($post_data['installation_data']['tractor_loader'])) ? $post_data['installation_data']['tractor_loader'] : null;

		if (isset($_POST['installation_data']['installation_id']) && !empty($_POST['installation_data']['installation_id'])) {
			$installation_id = $_POST['installation_data']['installation_id'];
			if (isset($post_data['installation_data']['installation_id'])) {
				unset($post_data['installation_data']['installation_id']);
			}

			$post_data['installation_data']['updated_at'] = $this->now_time;
			$post_data['installation_data']['updated_by'] = $this->staff_id;
			$this->db->where('installation_id', $installation_id);
			$result = $this->db->update('installation', $post_data['installation_data']);
			if ($result) {
				$return['success'] = "Updated";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Installation Updated Successfully');

				$val_inc = 0;
				if (!empty($post_data['daily_working_report_data']['work_date'])) {
					if (isset($post_data['deleted_dwr_id'])) {
						$this->db->where_in('id', $post_data['deleted_dwr_id']);
						$this->db->delete('installation_daily_working_report');
					}
					foreach ($post_data['daily_working_report_data']['work_date'] as $work_date) {
						$work_date = (!empty(strtotime($work_date))) ? date("Y-m-d", strtotime($work_date)) : null;
						$daily_working_report_data = array();
						if (!empty($post_data['daily_working_report_data']['dwr_id'][$val_inc])) {
							$daily_working_report_data['installation_id'] = $installation_id;
							$daily_working_report_data['work_date'] = $work_date;
							$daily_working_report_data['oper_person'] = $post_data['daily_working_report_data']['oper_person'][$val_inc];
							$daily_working_report_data['dwr_incharge_person'] = $post_data['daily_working_report_data']['dwr_incharge_person'][$val_inc];
							$daily_working_report_data['use_raw_mater'] = $post_data['daily_working_report_data']['use_raw_mater'][$val_inc];
							$daily_working_report_data['moisture'] = $post_data['daily_working_report_data']['moisture'][$val_inc];
							$daily_working_report_data['produ_hour'] = $post_data['daily_working_report_data']['produ_hour'][$val_inc];
							$daily_working_report_data['after_mois'] = $post_data['daily_working_report_data']['after_mois'][$val_inc];
							$daily_working_report_data['working_hour'] = $post_data['daily_working_report_data']['working_hour'][$val_inc];
							$daily_working_report_data['remark'] = $post_data['daily_working_report_data']['remark'][$val_inc];
							$daily_working_report_data['updated_at'] = $this->now_time;
							$daily_working_report_data['updated_by'] = $this->staff_id;
							$this->db->where('id', $post_data['daily_working_report_data']['dwr_id'][$val_inc]);
							$this->db->update('installation_daily_working_report', $daily_working_report_data);
						} else {
							$daily_working_report_data['installation_id'] = $installation_id;
							$daily_working_report_data['work_date'] = $work_date;
							$daily_working_report_data['oper_person'] = $post_data['daily_working_report_data']['oper_person'][$val_inc];
							$daily_working_report_data['dwr_incharge_person'] = $post_data['daily_working_report_data']['dwr_incharge_person'][$val_inc];
							$daily_working_report_data['use_raw_mater'] = $post_data['daily_working_report_data']['use_raw_mater'][$val_inc];
							$daily_working_report_data['moisture'] = $post_data['daily_working_report_data']['moisture'][$val_inc];
							$daily_working_report_data['produ_hour'] = $post_data['daily_working_report_data']['produ_hour'][$val_inc];
							$daily_working_report_data['after_mois'] = $post_data['daily_working_report_data']['after_mois'][$val_inc];
							$daily_working_report_data['working_hour'] = $post_data['daily_working_report_data']['working_hour'][$val_inc];
							$daily_working_report_data['remark'] = $post_data['daily_working_report_data']['remark'][$val_inc];
							$daily_working_report_data['created_at'] = $this->now_time;
							$daily_working_report_data['updated_at'] = $this->now_time;
							$daily_working_report_data['created_by'] = $this->staff_id;
							$daily_working_report_data['updated_by'] = $this->staff_id;
							$this->db->insert('installation_daily_working_report', $daily_working_report_data);
						}
						$val_inc++;
					}
				}

				$val_inc = 0;
				if (!empty($post_data['problem_resolution_details']['problem_resolution_option'])) {
					if (isset($post_data['deleted_problem_resolution_id'])) {
						$this->db->where_in('id', $post_data['deleted_problem_resolution_id']);
						$this->db->delete('installation_problem_resolutions');
					}
					foreach ($post_data['problem_resolution_details']['problem_resolution_option'] as $problem_resolution_option) {
						$problem_resolution_details = array();
						if (!empty($post_data['problem_resolution_details']['problem_resolution_id'][$val_inc])) {
							$problem_resolution_details['installation_id'] = $installation_id;
							$problem_resolution_details['problem_resolution_option'] = $problem_resolution_option;
							$problem_resolution_details['problem_resolution_text'] = $post_data['problem_resolution_details']['problem_resolution_text'][$val_inc];
							$problem_resolution_details['updated_at'] = $this->now_time;
							$problem_resolution_details['updated_by'] = $this->staff_id;
							$this->db->where('id', $post_data['problem_resolution_details']['problem_resolution_id'][$val_inc]);
							$this->db->update('installation_problem_resolutions', $problem_resolution_details);
						} else {
							$problem_resolution_details['installation_id'] = $installation_id;
							$problem_resolution_details['problem_resolution_option'] = $problem_resolution_option;
							$problem_resolution_details['problem_resolution_text'] = $post_data['problem_resolution_details']['problem_resolution_text'][$val_inc];
							$problem_resolution_details['created_at'] = $this->now_time;
							$problem_resolution_details['updated_at'] = $this->now_time;
							$problem_resolution_details['created_by'] = $this->staff_id;
							$problem_resolution_details['updated_by'] = $this->staff_id;
							$this->db->insert('installation_problem_resolutions', $problem_resolution_details);
						}
						$val_inc++;
					}
				}
			}
		} else {
			$dataToInsert = $post_data['installation_data'];
			$testing_report_id = $dataToInsert['testing_report_id'];
			
			$max_installation_no = $this->crud->get_max_number('installation','installation_no');
			$dataToInsert['installation_no'] = $max_installation_no->installation_no + 1;
			$dataToInsert['installation_no_year'] = $this->applib->get_challan_no($dataToInsert['installation_no'], $post_data['installation_data']['installation_date']);
			
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
			$result = $this->db->insert('installation', $dataToInsert);
            $installation_id = $this->db->insert_id();
			if ($result) {
				$return['success'] = "Added";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Installation Data Added Successfully');
				$this->crud->update('testing_report', array('is_installation_created' => 1), array('testing_report_id' => $testing_report_id));

				$val_inc = 0;
				if (!empty($post_data['daily_working_report_data']['work_date'])) {
					foreach ($post_data['daily_working_report_data']['work_date'] as $work_date) {
						$work_date = (!empty(strtotime($work_date))) ? date("Y-m-d", strtotime($work_date)) : null;
						$daily_working_report_data = array();
						$daily_working_report_data['installation_id'] = $installation_id;
						$daily_working_report_data['work_date'] = $work_date;
						$daily_working_report_data['oper_person'] = $post_data['daily_working_report_data']['oper_person'][$val_inc];
						$daily_working_report_data['dwr_incharge_person'] = $post_data['daily_working_report_data']['dwr_incharge_person'][$val_inc];
						$daily_working_report_data['use_raw_mater'] = $post_data['daily_working_report_data']['use_raw_mater'][$val_inc];
						$daily_working_report_data['moisture'] = $post_data['daily_working_report_data']['moisture'][$val_inc];
						$daily_working_report_data['produ_hour'] = $post_data['daily_working_report_data']['produ_hour'][$val_inc];
						$daily_working_report_data['after_mois'] = $post_data['daily_working_report_data']['after_mois'][$val_inc];
						$daily_working_report_data['working_hour'] = $post_data['daily_working_report_data']['working_hour'][$val_inc];
						$daily_working_report_data['remark'] = $post_data['daily_working_report_data']['remark'][$val_inc];
						$daily_working_report_data['created_at'] = $this->now_time;
						$daily_working_report_data['updated_at'] = $this->now_time;
						$daily_working_report_data['created_by'] = $this->staff_id;
						$daily_working_report_data['updated_by'] = $this->staff_id;
						$this->db->insert('installation_daily_working_report', $daily_working_report_data);
						$val_inc++;
					}
				}
				$val_inc = 0;
				if (!empty($post_data['problem_resolution_details']['problem_resolution_option'])) {
					foreach ($post_data['problem_resolution_details']['problem_resolution_option'] as $problem_resolution_option) {
						$problem_resolution_details = array();
						$problem_resolution_details['installation_id'] = $installation_id;
						$problem_resolution_details['problem_resolution_option'] = $problem_resolution_option;
						$problem_resolution_details['problem_resolution_text'] = $post_data['problem_resolution_details']['problem_resolution_text'][$val_inc];
						$problem_resolution_details['created_at'] = $this->now_time;
						$problem_resolution_details['updated_at'] = $this->now_time;
						$problem_resolution_details['created_by'] = $this->staff_id;
						$problem_resolution_details['updated_by'] = $this->staff_id;
						$this->db->insert('installation_problem_resolutions', $problem_resolution_details);
						$val_inc++;
					}
				}
			}
		}
		if (isset($post_data['challan_items_motor_details']) && !empty($post_data['challan_items_motor_details'])) {
			$challan_items_motor_details = $post_data['challan_items_motor_details'];
			foreach ($challan_items_motor_details as $key => $row) {
				$data[] = array(
					'id' => $key,
					'normal_amps_c' => $row['normal_amps_c'],
					'working_amps_c' => $row['working_amps_c'],
					'total_workinghours_c' => $row['total_workinghours_c'],
				);
			}
			$result = $this->db->update_batch('challan_items_motor_details', $data, 'id');
		}
		if (isset($post_data['challan_items_gearbox_details']) && !empty($post_data['challan_items_gearbox_details'])) {
			$challan_items_gearbox_details = $post_data['challan_items_gearbox_details'];
			foreach ($challan_items_gearbox_details as $key => $row) {
				$data[] = array(
					'id' => $key,
					'normal_amps_c' => $row['normal_amps_c'],
					'working_amps_c' => $row['working_amps_c'],
					'total_workinghours_c' => $row['total_workinghours_c'],
				);
			}
			$result = $this->db->update_batch('challan_items_gearbox_details', $data, 'id');
		}
		if (isset($post_data['testing_report_data']) && !empty($post_data['testing_report_data'])) {
			$testing_report_data = $post_data['testing_report_data'];
			$this->db->where('testing_report_id', $post_data['installation_data']['testing_report_id']);
			$this->db->update('testing_report', $testing_report_data);
		}
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['party']['party_id']);
            $this->applib->send_sms('installation', $installation_id, $party_phone_no, SEND_INSTALLION_SMS_ID);
        }
		print json_encode($return);
		exit;
	}

	function installation_list() {
		if ($this->app_model->have_access_role(INSTALLATION_MODULE_ID, "view")) {
			set_page('service/installation/installation_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function installation_datatable() {
		$config['table'] = 'installation i';
		$config['select'] = 'i.installation_id,i.installation_no,i.installation_no_year,tr.testing_report_no,tr.testing_report_no_year,c.challan_no,c.id,ci.item_code,p.party_name,p.phone_no,p.email_id,so.sales_order_no,pi.proforma_invoice_no,qso.quotation_no AS qso_quotation_no,qso.enquiry_no AS qso_enquiry_no,qpi.quotation_no AS qpi_quotation_no,qpi.enquiry_no AS qpi_enquiry_no,city.city,state.state,country.country,i.is_feedback_created,i.is_complain_created';
		$config['joins'][] = array('join_table' => 'testing_report tr', 'join_by' => 'i.testing_report_id = tr.testing_report_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'challans c', 'join_by' => 'i.challan_id = c.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id', 'join_type' => 'left');

		$config['joins'][] = array('join_table' => 'quotations qso', 'join_by' => 'qso.id = so.quotation_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotations qpi', 'join_by' => 'qpi.id = pi.quotation_id', 'join_type' => 'left');

		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

		$config['column_order'] = array(null, 'i.installation_no', 'tr.testing_report_no', 'c.challan_no', null, null, null, 'ci.item_code', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('i.installation_no_year', 'tr.testing_report_no', 'c.challan_no', 'so.sales_order_no', 'pi.proforma_invoice_no', 'qso.quotation_no', 'qso.enquiry_no', 'qpi.quotation_no', 'qpi.enquiry_no', 'ci.item_code', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['order'] = array('i.installation_id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '1');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");

		if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {

		} elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$role_delete = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "edit");
		foreach ($list as $installation) {
			$row = array();
			$action = '';
			if ($role_edit) {
				$action .= '<a href="' . base_url('installation/add/' . $installation->installation_id) . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			}
			if ($role_delete) {
                if ($installation->is_feedback_created != 1 && $installation->is_complain_created != 1) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('installation/delete_installation/' . $installation->installation_id) . '"><i class="fa fa-trash"></i></a>';				
                }
            }
			$action .= ' | <a href="' . base_url('installation/installation_print/' . $installation->installation_id) . '" target="_blank" class="btn-primary btn-xs" title="Print Testing"><i class="fa fa-print"></i> Print</a>';
			$action .= ' | <a href="' . base_url('installation/installation_email/' . $installation->installation_id) . '" target="_blank" class="" title="Email">Email</a>';
			$row[] = $action;
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->installation_no_year. '</a>';
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->testing_report_no_year . '</a>';
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->challan_no . '</a>';

			if (!empty($installation->sales_order_no)) {
				$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->sales_order_no . '</a>';
			} else {
				$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->proforma_invoice_no . '</a>';
			}
			if (!empty($installation->qso_quotation_no)) {
				$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->qso_quotation_no . '</a>';
			} else {
				$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->qpi_quotation_no . '</a>';
			}

			if (!empty($installation->qso_enquiry_no)) {
				$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->qso_enquiry_no . '</a>';
			} else {
				$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->qpi_enquiry_no . '</a>';
			}

			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->item_code . '</a>';
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->party_name . '</a>';
            $phone_no = substr($installation->phone_no,0,10);
            $installation->phone_no = str_replace(',', ', ', $installation->phone_no);
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$installation->phone_no.'">' . $phone_no . '</a>';
            $email_id = substr($installation->email_id,0,14);
            $installation->email_id = str_replace(',', ', ', $installation->email_id);
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$installation->email_id.'">' . $email_id . '</a>';
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->city . '</a>';
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->state . '</a>';
			$row[] = '<a href="' . base_url('installation/add/' . $installation->installation_id . '?view') . '" >' . $installation->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function delete_installation($installation_id) {
		$role_delete = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "delete");
		if ($role_delete) {

			//---Start Update Testing Report Table
			$testing_report_id = $this->crud->get_id_by_val('installation', 'testing_report_id', 'installation_id', $installation_id);
			$testing_report_update_data = array(
				'is_installation_created' => '0',
				'c_testing_normal_capacity_hrs' => '',
				'c_testing_normal_test_raw_material' => '',
				'c_testing_normal_moisture_raw_material' => '',
				'c_testing_normal_after_procces_moisture' => '',
				'c_testing_full_capacity_hrs' => '',
				'c_testing_full_test_raw_material' => '',
				'c_testing_full_moisture_raw_material' => '',
				'c_testing_full_after_procces_moisture' => '',
				'c_temp_oli' => '',
				'c_temp_eqipment' => '',
				'c_temp_head' => '',
				'c_temp_kiln' => '',
				'c_temp_hot_air' => '',
				'c_temp_weather' => '',
				'c_et_equipment_rpm' => '',
				'c_et_testing' => '',
				'c_et_stop_run_time' => '',
				'c_cpb_testing' => '',
				'c_cpb_working_hours' => ''
			);
			$this->crud->update('testing_report', $testing_report_update_data, array('testing_report_id' => $testing_report_id));
			//---Ent Update Testing Report Table

			$where_array = array("installation_id" => $installation_id);
			$this->crud->delete("installation", $where_array);
			$this->crud->delete("installation_daily_working_report", $where_array);
			$this->crud->delete("installation_problem_resolutions", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function installation_print($id) {
		$this->print_pdf($id,'print');
	}
	
	function installation_email($id) {
		$this->print_pdf($id,'email');
	}

	function print_pdf($id, $for){
		$company_details = $this->applib->get_company_detail();
		$challan_id = $this->crud->get_id_by_val('installation', 'challan_id', 'installation_id', $id);
		$sales_order_id = $this->crud->get_id_by_val('challans', 'sales_order_id', 'id', $challan_id);
		$proforma_invoice_id = $this->crud->get_id_by_val('challans', 'proforma_invoice_id', 'id', $challan_id);
		$from = !empty($sales_order_id) ? 'sales_order' : (!empty($proforma_invoice_id) ? 'proforma_invoices' : '');
		$installation_data = $this->get_installation_detail($id, $from);

		$installation_data['installation_date'] = (!empty(strtotime($installation_data['installation_date']))) ? date("d/m/Y", strtotime($installation_data['installation_date'])) : '';
		$installation_data['item_serial_no'] = $this->crud->get_id_by_val('challan_items', 'item_serial_no', 'challan_id', $installation_data['challan_id']);
		if (empty($installation_data['sales_order_no'])) {
			$installation_data['proforma_invoice_no'] = $this->crud->get_id_by_val('proforma_invoices', 'proforma_invoice_no', 'id', $installation_data['proforma_invoice_id']);
			$installation_data['proforma_invoice_date'] = $this->crud->get_id_by_val('proforma_invoices', 'sales_order_date', 'id', $installation_data['proforma_invoice_id']);
			$installation_data['cust_po_no'] = $this->crud->get_id_by_val('proforma_invoices', 'cust_po_no', 'id', $installation_data['proforma_invoice_id']);
			$installation_data['po_date'] = $this->crud->get_id_by_val('proforma_invoices', 'po_date', 'id', $installation_data['proforma_invoice_id']);
		} else {
			$installation_data['cust_po_no'] = $this->crud->get_id_by_val('sales_order', 'cust_po_no', 'id', $installation_data['sales_order_id']);
			$installation_data['po_date'] = $this->crud->get_id_by_val('sales_order', 'po_date', 'id', $installation_data['sales_order_id']);
		}

		//pre_die($installation_data);
		$where = array('installation_id' => $id);
		$daily_working_report_data = $this->crud->get_row_by_id('installation_daily_working_report', $where);
		$problem_resolution_data = $this->get_problem_resolution_data($id);

		$installation_data['installation_commissioning_by'] = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $installation_data['installation_commissioning_by']);
		$installation_data['jk_technical_person'] = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $installation_data['jk_technical_person']);
		$installation_data['project_training_provide_by'] = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $installation_data['project_training_provide_by']);
		$installation_data['electric_power'] = $this->crud->get_id_by_val('electric_power', 'label', 'id', $installation_data['electric_power']);
		$installation_data['elec_rm2_panel_board_cable'] = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $installation_data['elec_rm2_panel_board_cable']);
		$installation_data['connection_cable_1'] = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $installation_data['connection_cable_1']);
		$installation_data['water_tank_chiller_system_1'] = $this->crud->get_id_by_val('water_tank_or_chiller_system', 'label', 'id', $installation_data['water_tank_chiller_system_1']);
		$installation_data['water_plumbing_chiller_conn_1'] = $this->crud->get_id_by_val('water_plumbing_or_chiller_conn', 'label', 'id', $installation_data['water_plumbing_chiller_conn_1']);
		$installation_data['lubrication_gear_oil_1'] = $this->crud->get_id_by_val('lubrication_gear_oil', 'label', 'id', $installation_data['lubrication_gear_oil_1']);
		$installation_data['gear_box_oil_1'] = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $installation_data['gear_box_oil_1']);
		$installation_data['raw_material_1'] = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $installation_data['raw_material_1']);
		$installation_data['product_quality'] = $this->crud->get_id_by_val('customer_feedback', 'label', 'id', $installation_data['product_quality']);
		$installation_data['technician_of_jk_installed'] = $this->crud->get_id_by_val('customer_feedback', 'label', 'id', $installation_data['technician_of_jk_installed']);
		$installation_data['technician_from_jk_accompanied'] = $this->crud->get_id_by_val('customer_feedback', 'label', 'id', $installation_data['technician_from_jk_accompanied']);
		$installation_data['technician_from_jk_helped'] = $this->crud->get_id_by_val('customer_feedback', 'label', 'id', $installation_data['technician_from_jk_helped']);
		$installation_data['performance_of_our_equipment'] = $this->crud->get_id_by_val('customer_feedback', 'label', 'id', $installation_data['performance_of_our_equipment']);

		$installation_data['installation_declaration'] = $this->crud->get_id_by_val('terms_and_conditions', 'detail', 'module', 'installation_declaration');

		$where = array('challan_id' => $challan_id);
		$challan_item_data = $this->crud->get_row_by_id('challan_items', $where);
		$challan_item_data = $challan_item_data[0];
		if (!empty($challan_item_data->lr_date)) {
			$challan_item_data->lr_date = date('d/m/Y', strtotime($challan_item_data->lr_date));
		}
		$challan_id = $installation_data['challan_id'];
		$challan_item_id = $this->crud->get_id_by_val('challan_items', 'id', 'challan_id', $challan_id);

		$motor_data = $this->get_motor_data($challan_item_id);
		$gearbox_data = $this->get_gearbox_data($challan_item_id);
		$invoice_data = $this->crud->get_row_by_id('invoices', array('challan_id' => $challan_id));
		if (!empty($invoice_data)) {
			$installation_data['invoice_no'] = $invoice_data[0]->invoice_no;
			$installation_data['invoice_date'] = $invoice_data[0]->invoice_date;
		}

		$this->load->library('m_pdf');
		$margin_left = $company_details['margin_left'];
        $margin_right = $company_details['margin_right'];
        $margin_top = $company_details['margin_top'];
        $margin_bottom = $company_details['margin_bottom'];
		if (!empty($installation_data)) {
			$param = "'utf-8','A4'";
			$pdf = $this->m_pdf->load($param);
			$pdf->AddPage(
				'P', //orientation
				'', //type
				'', //resetpagenum
				'', //pagenumstyle
				'', //suppress
				$margin_left, //margin-left
				$margin_right, //margin-right
				$margin_top, //margin-top
				$margin_bottom, //margin-bottom
				0, //margin-header
				0 //margin-footer
			);
			if ($key == 0) {
				$html = $this->load->view('service/installation/installation_print', array('installation_data' => $installation_data, 'daily_working_report_data' => $daily_working_report_data, 'problem_resolution_data' => $problem_resolution_data, 'challan_item_data' => $challan_item_data, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => true), true);
			} else {
				$html = $this->load->view('service/installation/installation_print', array('installation_data' => $installation_data, 'daily_working_report_data' => $daily_working_report_data, 'problem_resolution_data' => $problem_resolution_data, 'challan_item_data' => $challan_item_data, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => false), true);
			}
			$pdf->WriteHTML($html);
			
			if($for == 'print'){
				$pdfFilePath = "installation_" . $id . ".pdf";
				$pdf->Output($pdfFilePath, "I");	
			}elseif($for == 'email'){
				$party_name  = str_replace(' ', '_', $this->crud->get_id_by_val('party','party_name','party_id',$quotation_data->party_id));
				$pdfFilePath = "./uploads/installation_" . $id . ".pdf";
				$files_urls[] = $pdfFilePath;
				$pdf->Output($pdfFilePath, "F");
			}
			
		}
		if($for == 'email'){
			$this->db->where('installation_id', $id);
			$this->db->update('installation', array('pdf_url' => serialize($files_urls)));
			redirect(base_url() . 'mail-system3/document-mail/installation/' . $id);	
		}
		
	}

	function get_installation_detail($installation_id, $from) {
		if ($from == '') {
			$this->session->set_flashdata('error_message', 'Sales Order id and Proforma Invoice id empty in challans.');
			redirect("/");
		}
		$select = "i.*, tr.*, ch.*, ch.id as challan_id, sales.sales,quo.quotation_no,";
		$select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,party.party_type_1,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,party.utr_no AS party_cin_no,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		if($from == 'sales_order'){			
			$select .= "so.sales_order_no,so.sales_order_date,";
		}else{
			$select .= "pi.proforma_invoice_no,pi.sales_order_date,";
		}
		$this->db->select($select);
		$this->db->from('installation i');
		$this->db->join('testing_report tr', 'tr.testing_report_id = i.testing_report_id', 'left');
		$this->db->join('challans ch', 'ch.id = tr.challan_id', 'left');
		if ($from == 'sales_order') {
			$this->db->join('sales_order so', 'so.id = ch.sales_order_id', 'left');
			$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
			$this->db->join('sales', 'sales.id = so.sales_id', 'left');
			$this->db->join('currency', 'currency.id = so.currency_id', 'left');
			$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
			$this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
			$this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
		} else {
			$this->db->join('proforma_invoices pi', 'pi.id = ch.proforma_invoice_id', 'left');
			$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
			$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
			$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
			$this->db->join('currency', 'currency.id = pi.currency_id', 'left');
			$this->db->join('sales', 'sales.id = pi.sales_id', 'left');
			$this->db->join('contact_person cp', 'cp.contact_person_id = pi.kind_attn_id', 'left');
		}
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');

		$this->db->where('i.installation_id', $installation_id);
		$this->db->limit('1');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array()[0];
		} else {
			return array();
		}
	}

	function get_problem_resolution_data($installation_id) {
		$where = array('installation_id' => $installation_id);
		$problem_resolution_data = $this->crud->get_row_by_id('installation_problem_resolutions', $where);
		foreach ($problem_resolution_data as $problem_resolution) {
			$problem_resolution->problem_resolution_option = $this->crud->get_id_by_val('problem_resolutions', 'label', 'id', $problem_resolution->problem_resolution_option);
		}
		return $problem_resolution_data;
	}

	function get_motor_data($challan_item_id) {
		$where = array('challan_item_id' => $challan_item_id);
		$motor_data = $this->crud->get_row_by_id('challan_items_motor_details', $where);
		//pre_die($motor_data);
		foreach ($motor_data as $motor) {
			$motor->motor_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $motor->motor_user);
			$motor->motor_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $motor->motor_make);
			$motor->motor_hp = $this->crud->get_id_by_val('challan_item_hp', 'hp_name', 'id', $motor->motor_hp);
			$motor->motor_kw = $this->crud->get_id_by_val('challan_item_kw', 'kw_name', 'id', $motor->motor_kw);
			$motor->motor_frequency = $this->crud->get_id_by_val('challan_item_frequency', 'frequency_name', 'id', $motor->motor_frequency);
			$motor->motor_rpm = $this->crud->get_id_by_val('challan_item_rpm', 'rpm_name', 'id', $motor->motor_rpm);
			$motor->motor_volts_cycles = $this->crud->get_id_by_val('challan_item_volts_cycles', 'volts_cycles_name', 'id', $motor->motor_volts_cycles);
		}
		return $motor_data;
	}

	function get_gearbox_data($challan_item_id) {
		$where = array('challan_item_id' => $challan_item_id);
		$gearbox_data = $this->crud->get_row_by_id('challan_items_gearbox_details', $where);
		foreach ($gearbox_data as $gearbox) {
			$gearbox->gearbox_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $gearbox->gearbox_user);
			$gearbox->gearbox_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $gearbox->gearbox_make);
			$gearbox->gearbox_gear_type = $this->crud->get_id_by_val('challan_item_gear_type', 'gear_type_name', 'id', $gearbox->gearbox_gear_type);
			$gearbox->gearbox_model = $this->crud->get_id_by_val('challan_item_model', 'model_name', 'id', $gearbox->gearbox_model);
			$gearbox->gearbox_ratio = $this->crud->get_id_by_val('challan_item_ratio', 'ratio_name', 'id', $gearbox->gearbox_ratio);
		}
		return $gearbox_data;
	}

	function get_challan_detail($id) {
		$select = "ch.*, ch.id as challan_id, ch.notes as challan_notes, ch.loading_at as challan_loading_at, sales.sales,currency.currency,quo.quotation_no,so.*,party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,";
		$select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,party.party_type_1,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required";
		$this->db->select($select);
		$this->db->from('challans ch');
		$this->db->join('sales_order so', 'so.id = ch.sales_order_id', 'left');
		$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = so.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = so.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = so.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = so.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = so.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = so.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = so.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = so.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = so.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = so.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = so.for_required_id', 'left');
		$this->db->where('ch.id', $id);
		$this->db->where('party.active !=', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_challan_items($id) {
		$this->db->select('soi.*');
		$this->db->from('challan_items soi');
		//        $this->db->join('item_category ic','ic.id = soi.item_category_id');
		//        $this->db->where('ic.item_category','Finished Goods');
		$this->db->where('soi.challan_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

}
