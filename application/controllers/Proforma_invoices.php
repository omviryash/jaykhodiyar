<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Proforma_invoices extends CI_Controller
{

	protected $staff_id = 1;

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

	}

	function get_quotation(){
		$quotation_id = $this->input->get_post("id");
		$quotation_data = $this->crud->get_row_by_id('quotations', array('id' => $quotation_id));
		$quotation_data = $quotation_data[0];
		$quotation_data->party_type_1 = $this->crud->get_id_by_val('party', 'party_type_1', 'party_id', $quotation_data->party_id);
		$quotation_data->enquiry_date = $this->crud->get_id_by_val('inquiry', 'inquiry_date', 'inquiry_id', $quotation_data->enquiry_id);
		$quotation_data->enquiry_date = substr($quotation_data->enquiry_date, 8, 2) .'-'. substr($quotation_data->enquiry_date, 5, 2) .'-'. substr($quotation_data->enquiry_date, 0, 4);
		$data['quotation_data'] = $quotation_data;
		$state_id = $this->crud->get_column_value_by_id('party', 'state_id', array('party_id' => $quotation_data->party_id));
		$lineitems = array();
		$where = array('quotation_id' => $quotation_id);
		$quotation_lineitems = $this->crud->get_row_by_id('quotation_items', $where);
		foreach($quotation_lineitems as $quotation_lineitem){
			$quotation_lineitem->disc_per = !empty($quotation_lineitem->disc_per)? $quotation_lineitem->disc_per : '0';
			$quotation_lineitem->disc_value = !empty($quotation_lineitem->disc_value)? $quotation_lineitem->disc_value : '0';
			$quotation_lineitem->quantity = !empty($quotation_lineitem->quantity)? $quotation_lineitem->quantity : '0';
			$quotation_lineitem->item_rate = !empty($quotation_lineitem->rate)? $quotation_lineitem->rate : '0';

            if($quotation_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
                if($state_id == GUJARAT_STATE_ID){
                    $quotation_lineitem->igst = 0;
                    $cgst = $this->crud->get_column_value_by_id('items', 'cgst', array('id' => $quotation_lineitem->item_id));
                    $sgst = $this->crud->get_column_value_by_id('items', 'sgst', array('id' => $quotation_lineitem->item_id));
                    $quotation_lineitem->cgst = !empty($cgst)? $cgst : '0';
                    $quotation_lineitem->sgst = !empty($sgst)? $sgst : '0';
                } else {
                    $igst = $this->crud->get_column_value_by_id('items', 'igst', array('id' => $quotation_lineitem->item_id));
                    $quotation_lineitem->igst = !empty($igst)? $igst : '0';
                    $quotation_lineitem->cgst = 0;
                    $quotation_lineitem->sgst = 0;
                }
            } else {
                $quotation_lineitem->cgst = 0;
                $quotation_lineitem->sgst = 0;
                $quotation_lineitem->igst = 0;
            }

			$pure_amount = $quotation_lineitem->quantity * $quotation_lineitem->item_rate;

			//$discount_amount = $pure_amount * $quotation_lineitem->disc_per / 100;
            $discount_amount = $quotation_lineitem->disc_value;
			$taxable_amount = $pure_amount - $discount_amount;
			$igst_amount = $taxable_amount * $quotation_lineitem->igst / 100;
			$cgst_amount = $taxable_amount * $quotation_lineitem->cgst / 100;
			$sgst_amount = $taxable_amount * $quotation_lineitem->sgst / 100;
			$net_amount = $taxable_amount + $igst_amount + $cgst_amount + $sgst_amount;

			$quotation_lineitem->igst_amount = $igst_amount;
			$quotation_lineitem->cgst_amount = $cgst_amount;
			$quotation_lineitem->sgst_amount = $sgst_amount;
			$quotation_lineitem->net_amount = $net_amount;
			$lineitems[] = $quotation_lineitem;
		}
		$data['quotation_lineitems'] = $lineitems;
		echo json_encode($data);
		exit;
	}

	function quotation_datatable_for_proforma_invoices_create(){
        
        $challan_created_quotation_ids = $this->crud->getFromSQL('SELECT `quotation_id` FROM `sales_order` WHERE `id` IN (SELECT `sales_order_id` FROM `challans`)');
        $challan_created_quotation_id_arr = array();
        foreach ($challan_created_quotation_ids as $challan_created_quotation_id){
            if(!empty($challan_created_quotation_id->quotation_id)){
                if(in_array($challan_created_quotation_id->quotation_id, $challan_created_quotation_id_arr)){ } else {
                    $challan_created_quotation_id_arr[] = $challan_created_quotation_id->quotation_id;
                }
            }
        }
            
		$requestData = $_REQUEST; 
		$config['select'] = 'q.id as quotation_id,q.quotation_no,q.updated_at,q.quotation_date,q.quotation_status_id,q.rev,p.party_name,qs.quotation_status,city.city,state.state,country.country';
		$config['table'] = 'quotations q';
		$config['column_order'] = array('q.updated_at','q.id', null, 'q.quotation_date','q.party_name','qs.quotation_status');
		$config['column_search'] = array('q.quotation_no','DATE_FORMAT(q.quotation_date,"%d-%m-%Y")','p.party_name','qs.quotation_status');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = q.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotation_status qs', 'join_by' => 'qs.id = q.quotation_status_id', 'join_type' => 'left');
		//$config['wheres'][] = array('column_name' => 'q.id  !=', 'column_value' => 0);
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => 0);
		$config['wheres'][] = array('column_name' => 'LOWER(qs.quotation_status)');

		if (!empty($_POST['proforma_invoices_for']) && $_POST['proforma_invoices_for'] == 'export') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if (!empty($_POST['proforma_invoices_for']) && $_POST['proforma_invoices_for'] == 'domestic') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($this->applib->have_access_current_user_rights(QUOTATION_MANAGEMENT_USER,"allow") == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){					
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$config['custom_where'] = '( p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL )';
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if($cu_accessDomestic == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
				
			} else {}
		}
		$config['wheres'][] = array('column_name' => 'q.is_proforma_created !=', 'column_value' => 1);
        if(!empty($challan_created_quotation_id_arr)){
            $config['where_not_in'] = array('column_name' => 'q.id', 'array' => $challan_created_quotation_id_arr);
        }
		$config['order'] = array('q.created_at' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $quotation_data) {
			$row = array();
			$row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->updated_at . '</a>';
			$row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->quotation_no . '</a>';
			$row[] = !empty($quotation_data->rev) ? $quotation_data->rev . '  &nbsp; &nbsp;' : '1 &nbsp; &nbsp;';
			$row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . date('d-m-Y', strtotime($quotation_data->quotation_date)) . '</a>';
			$row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->party_name . '</a>';
			$row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->city . '</a>';
			$row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->state . '</a>';
			$row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	/**
	 *    Proforma Invoice
	 */
	function add($proforma_invoice_id = ''){
		$data = array();
		if(!empty($proforma_invoice_id)){
			if ($this->app_model->have_access_role(PROFORMA_INVOICE_MODULE_ID, "edit")) {
				$proforma_invoices_data = $this->crud->get_row_by_id('proforma_invoices', array('id' => $proforma_invoice_id));
				if(empty($proforma_invoices_data)){
					redirect("proforma_invoices/proforma_invoices_list"); exit;
				}
				$proforma_invoices_data = $proforma_invoices_data[0];
				$login_data = $this->crud->get_row_by_id('proforma_invoice_logins', array('proforma_invoice_id' => $proforma_invoice_id));
				$login_data = $login_data[0];
				$login_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->created_by_id));
				$login_data->created_at = substr($login_data->created_at, 8, 2) .'-'. substr($login_data->created_at, 5, 2) .'-'. substr($login_data->created_at, 0, 4);
				$login_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->last_modified_by_id));
				$login_data->updated_at = substr($login_data->updated_at, 8, 2) .'-'. substr($login_data->updated_at, 5, 2) .'-'. substr($login_data->updated_at, 0, 4);
				$login_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->approved_by_id));
				$login_data->approved_at = substr($login_data->approved_date_time, 8, 2) .'-'. substr($login_data->approved_date_time, 5, 2) .'-'. substr($login_data->approved_date_time, 0, 4);
				$data = array(
					'proforma_invoices_data' => $proforma_invoices_data,
					'login_data' => $login_data,
					'party_contact_person' => $this->crud->get_contact_person_by_party($proforma_invoices_data->sales_to_party_id),
					'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
					'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
					'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
					'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
					'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
					'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
					'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
					'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
					'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
					'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
				);
				if(!empty($data['proforma_invoices_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['proforma_invoices_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if(!empty($designation)){
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if(!empty($department)){
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
                $data['proforma_invoices_data']->not_any_dispatched = 0;
				$lineitems = array();
				$where = array('proforma_invoice_id' => $proforma_invoice_id);
				$proforma_invoices_lineitems = $this->crud->get_row_by_id('proforma_invoice_items', $where);
				foreach($proforma_invoices_lineitems as $proforma_invoices_lineitem){
					$proforma_invoices_lineitem->disc_per = !empty($proforma_invoices_lineitem->disc_per)? $proforma_invoices_lineitem->disc_per : '0';
					$proforma_invoices_lineitem->disc_value = !empty($proforma_invoices_lineitem->disc_value)? $proforma_invoices_lineitem->disc_value : '0';
					$proforma_invoices_lineitem->quantity = !empty($proforma_invoices_lineitem->quantity)? $proforma_invoices_lineitem->quantity : '0';
					$proforma_invoices_lineitem->item_rate = !empty($proforma_invoices_lineitem->rate)? $proforma_invoices_lineitem->rate : '0';

					$proforma_invoices_lineitem->igst = !empty($proforma_invoices_lineitem->igst)? $proforma_invoices_lineitem->igst : '0';
					$proforma_invoices_lineitem->cgst = !empty($proforma_invoices_lineitem->cgst)? $proforma_invoices_lineitem->cgst : '0';
					$proforma_invoices_lineitem->sgst = !empty($proforma_invoices_lineitem->sgst)? $proforma_invoices_lineitem->sgst : '0';

					$pure_amount = $proforma_invoices_lineitem->quantity * $proforma_invoices_lineitem->item_rate;

					//$discount_amount = $pure_amount * $proforma_invoices_lineitem->disc_per / 100;
					$discount_amount = $proforma_invoices_lineitem->disc_value;
					$taxable_amount = $pure_amount - $discount_amount;
					$igst_amount = $taxable_amount * $proforma_invoices_lineitem->igst / 100;
					$cgst_amount = $taxable_amount * $proforma_invoices_lineitem->cgst / 100;
					$sgst_amount = $taxable_amount * $proforma_invoices_lineitem->sgst / 100;
					$net_amount = $taxable_amount + $igst_amount + $cgst_amount + $sgst_amount;

					$proforma_invoices_lineitem->igst_amount = $igst_amount;
					$proforma_invoices_lineitem->cgst_amount = $cgst_amount;
					$proforma_invoices_lineitem->sgst_amount = $sgst_amount;
					$proforma_invoices_lineitem->net_amount = $net_amount;
					$lineitems[] = json_encode($proforma_invoices_lineitem);
                    
                    if($proforma_invoices_lineitem->dispatched_qty != 0){ $data['proforma_invoices_data']->not_any_dispatched = 1; }
				}
				$data['proforma_invoices_lineitems'] = implode(',', $lineitems);
				$data['challan_id'] = $this->crud->get_id_by_val('challans','id','proforma_invoice_id',$proforma_invoice_id);
				//                echo '<pre>';print_r($data); exit;
				set_page('proforma_invoices/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"add")) {
				$data = array(
					'proforma_invoice_no' => 'PI'.$this->get_new_proforma_invoice_no(),
					'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
					'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
					'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
					'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
					'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
					'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
					'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
					'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
					'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
					'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
				);
				set_page('proforma_invoices/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function save_proforma_invoices(){
		$post_data = $this->input->post();
		//echo '<pre>';print_r($post_data['line_items_data']);exit;
		// check party validations
		$party_id = $post_data['party']['party_id'];
		if ($party_id == "" || $party_id == "0") {
			echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
			exit;
		}

		//echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
		//		print_r($line_items_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
		$post_data['proforma_invoices_data']['sales_order_date'] = date("Y-m-d", strtotime($post_data['proforma_invoices_data']['sales_order_date']));
		$post_data['proforma_invoices_data']['committed_date'] = date("Y-m-d", strtotime($post_data['proforma_invoices_data']['committed_date']));
		$post_data['proforma_invoices_data']['po_date'] = date("Y-m-d", strtotime($post_data['proforma_invoices_data']['po_date']));
        $post_data['proforma_invoices_data']['sales_order_pref_id'] = !empty($post_data['proforma_invoices_data']['sales_order_pref_id']) ? $post_data['proforma_invoices_data']['sales_order_pref_id'] : NULL;

		if(!empty($post_data['proforma_invoices_data']['received_payment_date']) && isset($post_data['proforma_invoices_data']['received_payment_date'])){
			$post_data['proforma_invoices_data']['received_payment_date'] = date('Y-m-d', strtotime($post_data['proforma_invoices_data']['received_payment_date']));	
		} else {
			$post_data['proforma_invoices_data']['received_payment_date'] = null;
		}

		if(!empty($post_data['proforma_invoices_data']['mach_deli_min_weeks_date']) && isset($post_data['proforma_invoices_data']['mach_deli_min_weeks_date'])){
			$post_data['proforma_invoices_data']['mach_deli_min_weeks_date'] = date('Y-m-d', strtotime($post_data['proforma_invoices_data']['mach_deli_min_weeks_date']));	
		} else {
			$post_data['proforma_invoices_data']['mach_deli_min_weeks_date'] = null;
		}

		if(!empty($post_data['proforma_invoices_data']['mach_deli_max_weeks_date']) && isset($post_data['proforma_invoices_data']['mach_deli_max_weeks_date'])){
			$post_data['proforma_invoices_data']['mach_deli_max_weeks_date'] = date('Y-m-d', strtotime($post_data['proforma_invoices_data']['mach_deli_max_weeks_date']));	
		} else {
			$post_data['proforma_invoices_data']['mach_deli_max_weeks_date'] = null;
		}

		if(isset($post_data['proforma_invoices_data']['quotation_id'])){
			$post_data['proforma_invoices_data']['quotation_no'] = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $post_data['proforma_invoices_data']['quotation_id']);
		}

		$post_data['proforma_invoices_data']['kind_attn_id'] = !empty($post_data['proforma_invoices_data']['kind_attn_id']) ? $post_data['proforma_invoices_data']['kind_attn_id'] : NULL;
		$post_data['proforma_invoices_data']['branch_id'] = !empty($post_data['proforma_invoices_data']['branch_id']) ? $post_data['proforma_invoices_data']['branch_id'] : NULL;
		$post_data['proforma_invoices_data']['agent_id'] = !empty($post_data['proforma_invoices_data']['agent_id']) ? $post_data['proforma_invoices_data']['agent_id'] : NULL;
		$post_data['proforma_invoices_data']['sales_order_status_id'] = !empty($post_data['proforma_invoices_data']['sales_order_status_id']) ? $post_data['proforma_invoices_data']['sales_order_status_id'] : NULL;
		$post_data['proforma_invoices_data']['sales_id'] = !empty($post_data['proforma_invoices_data']['sales_id']) ? $post_data['proforma_invoices_data']['sales_id'] : NULL;
		$post_data['proforma_invoices_data']['currency_id'] = !empty($post_data['proforma_invoices_data']['currency_id']) ? $post_data['proforma_invoices_data']['currency_id'] : NULL;
		$post_data['proforma_invoices_data']['delivery_through_id'] = !empty($post_data['proforma_invoices_data']['delivery_through_id']) ? $post_data['proforma_invoices_data']['delivery_through_id'] : NULL;
		$post_data['proforma_invoices_data']['sales_to_party_id'] = $party_id;

		if(isset($post_data['proforma_invoices_data']['proforma_invoice_id']) && !empty($post_data['proforma_invoices_data']['proforma_invoice_id'])){

			$proforma_invoice_no = $post_data['proforma_invoices_data']['proforma_invoice_no'];
			$proforma_invoices_result = $this->crud->get_id_by_val('proforma_invoices', 'id', 'proforma_invoice_no', $proforma_invoice_no);
			if(!empty($proforma_invoices_result) && $proforma_invoices_result != $post_data['proforma_invoices_data']['proforma_invoice_id']){
				echo json_encode(array("success" => 'false', 'msg' => 'Proforma Invoice No. Already Exist!'));
				exit;
			}

			$proforma_invoice_id = $post_data['proforma_invoices_data']['proforma_invoice_id'];
			if (isset($post_data['proforma_invoices_data']['proforma_invoice_id']))
				unset($post_data['proforma_invoices_data']['proforma_invoice_id']);

			$this->db->where('id', $proforma_invoice_id);
			$result = $this->db->update('proforma_invoices', $post_data['proforma_invoices_data']);
			if($result){

				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->update('proforma_invoice_logins', $login_data, array('proforma_invoice_id' => $proforma_invoice_id));

				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Proforma Invoice Updated Successfully');
				$where_array = array("proforma_invoice_id" => $proforma_invoice_id);
				$this->crud->delete("proforma_invoice_items", $where_array);
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['proforma_invoice_id'] = $proforma_invoice_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					$add_lineitem['item_description'] = $lineitem->item_description;
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['cgst'] = $lineitem->cgst;
					$add_lineitem['sgst'] = $lineitem->sgst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('proforma_invoice_items',$add_lineitem);
				}				
			}
		} else {
			$post_data['proforma_invoices_data']['proforma_invoice_no'] = 'PI'.$this->get_new_proforma_invoice_no();
			$dataToInsert = $post_data['proforma_invoices_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			// Unset quotation_id from Inquiry Data
			if (isset($dataToInsert['proforma_invoice_id']))
				unset($dataToInsert['proforma_invoice_id']);

			$result = $this->db->insert('proforma_invoices', $dataToInsert);
			$proforma_invoice_id = $this->db->insert_id();
			if($result){

				$login_data['proforma_invoice_id'] = $proforma_invoice_id;
				$login_data['created_by_id'] = $this->staff_id;
				$login_data['created_date_time'] = $this->now_time;
				$login_data['created_at'] = $this->now_time;
				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->insert('proforma_invoice_logins', $login_data);

				/*-- Update Qoutation Status --*/
				$quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $post_data['proforma_invoices_data']['quotation_id']);
				//print_r($quotation_no);exit;
				if (!empty($quotation_no)){
					$this->crud->update('quotations', array('is_proforma_created' => 1), array('quotation_no' => $quotation_no));
				}

				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Proforma Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['proforma_invoice_id'] = $proforma_invoice_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					$add_lineitem['item_description'] = $lineitem->item_description;
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['cgst'] = $lineitem->cgst;
					$add_lineitem['sgst'] = $lineitem->sgst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('proforma_invoice_items',$add_lineitem);
				}
			}
		}
		print json_encode($return);
		exit;
	}

	function add_export($proforma_invoice_id = ''){
		$data = array();
		if(!empty($proforma_invoice_id)){
			if ($this->app_model->have_access_role(PROFORMA_INVOICE_MODULE_ID, "edit")) {
				$proforma_invoices_data = $this->crud->get_row_by_id('proforma_invoices', array('id' => $proforma_invoice_id));
				if(empty($proforma_invoices_data)){
					redirect("proforma_invoices/proforma_invoices_list"); exit;
				}
				$proforma_invoices_data = $proforma_invoices_data[0];

				/*$proforma_invoices_data->port_of_loading_country = $this->crud->get_id_by_val('country', 'country_id', 'country', $proforma_invoices_data->port_of_loading_country);
				$proforma_invoices_data->port_of_discharge_country = $this->crud->get_id_by_val('country', 'country_id', 'country', $proforma_invoices_data->port_of_discharge_country);
				$proforma_invoices_data->place_of_delivery_country = $this->crud->get_id_by_val('country', 'country_id', 'country', $proforma_invoices_data->place_of_delivery_country);*/

				$login_data = $this->crud->get_row_by_id('proforma_invoice_logins', array('proforma_invoice_id' => $proforma_invoice_id));
				$login_data = $login_data[0];
				$login_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->created_by_id));
				$login_data->created_at = substr($login_data->created_at, 8, 2) .'-'. substr($login_data->created_at, 5, 2) .'-'. substr($login_data->created_at, 0, 4);
				$login_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->last_modified_by_id));
				$login_data->updated_at = substr($login_data->updated_at, 8, 2) .'-'. substr($login_data->updated_at, 5, 2) .'-'. substr($login_data->updated_at, 0, 4);
				$login_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->approved_by_id));
				$login_data->approved_at = substr($login_data->approved_date_time, 8, 2) .'-'. substr($login_data->approved_date_time, 5, 2) .'-'. substr($login_data->approved_date_time, 0, 4);
				$data = array(
					'proforma_invoices_data' => $proforma_invoices_data,
					'login_data' => $login_data,
					'party_contact_person' => $this->crud->get_contact_person_by_party($proforma_invoices_data->sales_to_party_id),
					'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
					'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
					'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
					'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
					'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
					'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
					'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
					'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
					'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
					'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
                    'company_banks' => $this->crud->get_all_with_where('company_banks', 'id', 'ASC',array('for_export' => '1')),
				);
				if(!empty($data['proforma_invoices_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['proforma_invoices_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if(!empty($designation)){
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if(!empty($department)){
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
                $data['proforma_invoices_data']->not_any_dispatched = 0;
				$lineitems = array();
				$where = array('proforma_invoice_id' => $proforma_invoice_id);
				$proforma_invoices_lineitems = $this->crud->get_row_by_id('proforma_invoice_items', $where);
				foreach($proforma_invoices_lineitems as $proforma_invoices_lineitem){
					$proforma_invoices_lineitem->disc_per = !empty($proforma_invoices_lineitem->disc_per)? $proforma_invoices_lineitem->disc_per : '0';
					$proforma_invoices_lineitem->disc_value = !empty($proforma_invoices_lineitem->disc_value)? $proforma_invoices_lineitem->disc_value : '0';
					$proforma_invoices_lineitem->quantity = !empty($proforma_invoices_lineitem->quantity)? $proforma_invoices_lineitem->quantity : '0';
					$proforma_invoices_lineitem->item_rate = !empty($proforma_invoices_lineitem->rate)? $proforma_invoices_lineitem->rate : '0';

					$pure_amount = $proforma_invoices_lineitem->quantity * $proforma_invoices_lineitem->item_rate;

					//$discount_amount = $pure_amount * $proforma_invoices_lineitem->disc_per / 100;
					$discount_amount = $proforma_invoices_lineitem->disc_value;
					$taxable_amount = $pure_amount - $discount_amount;
					$net_amount = $taxable_amount;

					$proforma_invoices_lineitem->net_amount = $net_amount;
					$lineitems[] = json_encode($proforma_invoices_lineitem);
                    
                    if($proforma_invoices_lineitem->dispatched_qty != 0){ $data['proforma_invoices_data']->not_any_dispatched = 1; }
				}
				$data['proforma_invoices_lineitems'] = implode(',', $lineitems);
				$where = array('proforma_invoice_id' => $proforma_invoice_id);
				$data['challan_id'] = $this->crud->get_id_by_val('challans','id','proforma_invoice_id',$proforma_invoice_id);
				//                echo '<pre>';print_r($data); exit;
				set_page('proforma_invoices/add_export', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"add")) {
				$data = array(
					'proforma_invoice_no' => 'PI'.$this->get_new_proforma_invoice_no(),
					'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
					'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
					'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
					'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
					'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
					'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
					'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
					'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
					'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
					'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
					'company_banks' => $this->crud->get_all_with_where('company_banks', 'id', 'ASC',array('for_export' => '1')),
				);
				set_page('proforma_invoices/add_export', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function save_proforma_invoices_export(){
		$post_data = $this->input->post();
		//echo '<pre>';print_r($post_data);exit;
		//echo '<pre>';print_r($post_data['proforma_invoices_data']);exit;
		// check party validations
		$party_id = $post_data['party']['party_id'];
		if ($party_id == "" || $party_id == "0") {
			echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
			exit;
		}
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
		//		print_r($line_items_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
		$post_data['proforma_invoices_data']['sales_order_date'] = date("Y-m-d", strtotime($post_data['proforma_invoices_data']['sales_order_date']));
		$post_data['proforma_invoices_data']['committed_date'] = date("Y-m-d", strtotime($post_data['proforma_invoices_data']['committed_date']));
		$post_data['proforma_invoices_data']['po_date'] = date("Y-m-d", strtotime($post_data['proforma_invoices_data']['po_date']));

		if(!empty($post_data['proforma_invoices_data']['received_payment_date']) && isset($post_data['proforma_invoices_data']['received_payment_date'])){
			$post_data['proforma_invoices_data']['received_payment_date'] = date('Y-m-d', strtotime($post_data['proforma_invoices_data']['received_payment_date']));	
		} else {
			$post_data['proforma_invoices_data']['received_payment_date'] = null;
		}
		
		$post_data['proforma_invoices_data']['kind_attn_id'] = !empty($post_data['proforma_invoices_data']['kind_attn_id']) ? $post_data['proforma_invoices_data']['kind_attn_id'] : NULL;
		$post_data['proforma_invoices_data']['branch_id'] = !empty($post_data['proforma_invoices_data']['branch_id']) ? $post_data['proforma_invoices_data']['branch_id'] : NULL;
		$post_data['proforma_invoices_data']['agent_id'] = !empty($post_data['proforma_invoices_data']['agent_id']) ? $post_data['proforma_invoices_data']['agent_id'] : NULL;
		$post_data['proforma_invoices_data']['sales_order_status_id'] = !empty($post_data['proforma_invoices_data']['sales_order_status_id']) ? $post_data['proforma_invoices_data']['sales_order_status_id'] : NULL;
		$post_data['proforma_invoices_data']['sales_id'] = !empty($post_data['proforma_invoices_data']['sales_id']) ? $post_data['proforma_invoices_data']['sales_id'] : NULL;
		$post_data['proforma_invoices_data']['currency_id'] = !empty($post_data['proforma_invoices_data']['currency_id']) ? $post_data['proforma_invoices_data']['currency_id'] : NULL;
        $post_data['proforma_invoices_data']['delivery_through_id'] = !empty($post_data['proforma_invoices_data']['delivery_through_id']) ? $post_data['proforma_invoices_data']['delivery_through_id'] : NULL;
		$post_data['proforma_invoices_data']['bank_detail'] = !empty($post_data['proforma_invoices_data']['bank_detail']) ? $post_data['proforma_invoices_data']['bank_detail'] : NULL;
		$post_data['proforma_invoices_data']['sales_to_party_id'] = $party_id;
		if(isset($post_data['proforma_invoices_data']['port_of_loading_country_id'])){
			$post_data['proforma_invoices_data']['port_of_loading_country'] = $this->crud->get_id_by_val('country', 'country', 'country_id', $post_data['proforma_invoices_data']['port_of_loading_country_id']);	
		}
		if(isset($post_data['proforma_invoices_data']['port_of_discharge_country_id'])){
			$post_data['proforma_invoices_data']['port_of_discharge_country'] = $this->crud->get_id_by_val('country', 'country', 'country_id', $post_data['proforma_invoices_data']['port_of_discharge_country_id']);
		}
		if(isset($post_data['proforma_invoices_data']['place_of_delivery_country_id'])){
			$post_data['proforma_invoices_data']['place_of_delivery_country'] = $this->crud->get_id_by_val('country', 'country', 'country_id', $post_data['proforma_invoices_data']['place_of_delivery_country_id']);
		}		
		if(isset($post_data['proforma_invoices_data']['quotation_id'])){
			$post_data['proforma_invoices_data']['quotation_no'] = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $post_data['proforma_invoices_data']['quotation_id']);
		}

		if(isset($post_data['proforma_invoices_data']['proforma_invoice_id']) && !empty($post_data['proforma_invoices_data']['proforma_invoice_id'])){
			$proforma_invoice_no = $post_data['proforma_invoices_data']['proforma_invoice_no'];
			$proforma_invoices_result = $this->crud->get_id_by_val('proforma_invoices', 'id', 'proforma_invoice_no', $proforma_invoice_no);
			if(!empty($proforma_invoices_result) && $proforma_invoices_result != $post_data['proforma_invoices_data']['proforma_invoice_id']){
				echo json_encode(array("success" => 'false', 'msg' => 'Proforma Invoice No. Already Exist!'));
				exit;
			}

			$proforma_invoice_id = $post_data['proforma_invoices_data']['proforma_invoice_id'];
			if (isset($post_data['proforma_invoices_data']['proforma_invoice_id']))
				unset($post_data['proforma_invoices_data']['proforma_invoice_id']);

			$this->db->where('id', $proforma_invoice_id);
			$result = $this->db->update('proforma_invoices', $post_data['proforma_invoices_data']);
			if($result){

				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->update('proforma_invoice_logins', $login_data, array('proforma_invoice_id' => $proforma_invoice_id));

				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Proforma Invoice Updated Successfully');
				$where_array = array("proforma_invoice_id" => $proforma_invoice_id);
				$this->crud->delete("proforma_invoice_items", $where_array);
                $is_dispatched_arr = array();
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['proforma_invoice_id'] = $proforma_invoice_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					$add_lineitem['item_description'] = $lineitem->item_description;
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
                    $add_lineitem['dispatched_qty'] = $lineitem->dispatched_qty;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('proforma_invoice_items',$add_lineitem);
                    if($add_lineitem['quantity'] == $add_lineitem['dispatched_qty']){
                        $is_dispatched_arr[] = 1;
                    }
				}
                $is_dispatched_count = count($is_dispatched_arr);
                $line_items_count = count($line_items_data[0]);
                if($is_dispatched_count == $line_items_count){
                    $this->crud->update('proforma_invoices', array("is_dispatched" => DISPATCHED_ORDER_ID), array('id' => $proforma_invoice_id));
                } else if(isset($post_data['proforma_invoices_data']['is_dispatched']) && $post_data['proforma_invoices_data']['is_dispatched'] == CANCELED_ORDER_ID){
                    $this->crud->update('proforma_invoices', array("is_dispatched" => CANCELED_ORDER_ID), array('id' => $proforma_invoice_id));
                } else if(isset($post_data['proforma_invoices_data']['is_dispatched']) && $post_data['proforma_invoices_data']['is_dispatched'] == CHALLAN_CREATED_FROM_SALES_ORDER_ID){
                    $this->crud->update('proforma_invoices', array("is_dispatched" => CHALLAN_CREATED_FROM_SALES_ORDER_ID), array('id' => $proforma_invoice_id));
                } else {
                    $this->crud->update('proforma_invoices', array("is_dispatched" => PENDING_DISPATCH_ORDER_ID), array('id' => $proforma_invoice_id));
                }
			}
		} else {
			$post_data['proforma_invoices_data']['proforma_invoice_no'] = 'PI'.$this->get_new_proforma_invoice_no();
			$dataToInsert = $post_data['proforma_invoices_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			// Unset quotation_id from Inquiry Data
			if (isset($dataToInsert['proforma_invoice_id']))
				unset($dataToInsert['proforma_invoice_id']);

			$result = $this->db->insert('proforma_invoices', $dataToInsert);
			$proforma_invoice_id = $this->db->insert_id();
			if($result){

				$login_data['proforma_invoice_id'] = $proforma_invoice_id;
				$login_data['created_by_id'] = $this->staff_id;
				$login_data['created_date_time'] = $this->now_time;
				$login_data['created_at'] = $this->now_time;
				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->insert('proforma_invoice_logins', $login_data);

				/*-- Update Qoutation Status --*/
				$quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $post_data['proforma_invoices_data']['quotation_id']);
				//print_r($quotation_no);exit;
				if (!empty($quotation_no)){
					$this->crud->update('quotations', array('is_proforma_created' => 1), array('quotation_no' => $quotation_no));
				}

				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Proforma Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['proforma_invoice_id'] = $proforma_invoice_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					$add_lineitem['item_description'] = $lineitem->item_description;
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    if(isset($lineitem->dispatched_qty) && !empty($lineitem->dispatched_qty)){
						$add_lineitem['dispatched_qty'] = $lineitem->dispatched_qty;
					}
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('proforma_invoice_items',$add_lineitem);
				}
			}
		}
		print json_encode($return);
		exit;
	}

	function proforma_invoices_list($staff_id='', $from_date='', $to_date=''){
		$role_view = $this->app_model->have_access_role(PROFORMA_INVOICE_MODULE_ID, "view");
		if ($role_view) {
			$data = array();
                        $users = $lead_owner = $this->crud->get_staff_dropdown();
                        $data['users'] = $users;
                        $data['staff_id'] = $staff_id;
                        $data['from_date'] = $from_date;
                        $data['to_date'] = $to_date;
			set_page('proforma_invoices/proforma_invoices_list', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function proforma_invoices_datatable(){
		$config['table'] = 'proforma_invoices pi';
		$config['select'] = 'pi.*,p.party_name,p.party_type_1,city.city,state.state,country.country,sf.name AS sales_person,cur_s.name AS party_current_person';
		$config['column_order'] = array(null, 'pi.id', 'pi.quotation_no', 'p.party_name', 'pi.sales_order_date', 'pi.committed_date');
		$config['column_search'] = array('pi.proforma_invoice_no', 'pi.quotation_no', 'p.party_name', 'pi.sales_order_date', 'pi.committed_date');

		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = pi.sales_to_party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff sf', 'join_by' => 'sf.staff_id = pi.created_by', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => 0);
		if (isset($_POST['is_approved']) && $_POST['is_approved'] != 'all') {
			$config['wheres'][] = array('column_name' => 'pi.isApproved', 'column_value' => $_POST['is_approved']);
		}

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($this->applib->have_access_current_user_rights(PROFORMA_MANAGEMENT_USER,"allow") == 1){
			if (isset($_POST['invoice_not_created']) && $_POST['invoice_not_created'] == 'true') {
				$config['custom_where'] = " pi.id NOT IN(SELECT proforma_invoice_id FROM invoices)";
			}
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			if (isset($_POST['invoice_not_created']) && $_POST['invoice_not_created'] == 'true') {
				$config['custom_where'] = ' pi.id NOT IN(SELECT proforma_invoice_id FROM invoices) AND (p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL)';
			} else {
			}

			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if($cu_accessDomestic == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			} else {}
			$config['custom_where'] = '(p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL)';
		}

		if (!empty($_POST['party_type']) && $_POST['party_type'] == 'all') {
		} else if (isset($_POST['party_type']) && $_POST['party_type'] == 'export') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if (isset($_POST['party_type']) && $_POST['party_type'] == 'domestic') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}
        
        if (!empty($_POST['pi_status']) && $_POST['pi_status'] == 'all') {
		} else {
			$config['wheres'][] = array('column_name' => 'pi.is_dispatched', 'column_value' => $_POST['pi_status']);
		}
                if (isset($_POST['from_date']) && !empty($_POST['from_date'])) {
                    $from_date = date('Y-m-d', strtotime($_POST['from_date']));
                    $config['wheres'][] = array('column_name' => 'pi.sales_order_date >=', 'column_value' => $from_date);
                }
                if (isset($_POST['to_date']) && !empty($_POST['to_date'])) {
                    $to_date = date('Y-m-d', strtotime($_POST['to_date']));
                    $config['wheres'][] = array('column_name' => 'pi.sales_order_date <=', 'column_value' => $to_date);
                }
                if (isset($_POST['user_id']) && $_POST['user_id'] != 'all') {
                    $config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' => $_POST['user_id']);
                }
        
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
//                echo $this->db->last_query(); exit;
		$data = array();
		$role_delete = $this->app_model->have_access_role(PROFORMA_INVOICE_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(PROFORMA_INVOICE_MODULE_ID, "edit");

		foreach ($list as $order_row) {
			$row = array();
			$action = '';
                        if($order_row->is_dispatched != CHALLAN_CREATED_FROM_SALES_ORDER_ID){
			if ($role_edit) {
				if($order_row->party_type_1 == PARTY_TYPE_EXPORT_ID){
					$action .= '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
				} elseif ($order_row->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
					$action .= '<a href="' . base_url('proforma_invoices/add/' . $order_row->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
				}
			}
			if ($role_delete) {
                if ($order_row->delete_not_allow != 1 ) {
                    $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('proforma_invoices/delete_proforma_invoice/' . $order_row->id) . '"><i class="fa fa-trash"></i></a> ';
                }
			}
                        }
			if ($order_row->isApproved == 1 ) {
				$action .= ' | <a href="' . base_url('proforma_invoices/proforma_invoice_print/' . $order_row->id) . '" target="_blank" class="btn-primary btn-xs" ><i class="fa fa-print"></i></a> ';
				$action .= ' | <a href="' . base_url('proforma_invoices/proforma_invoice_email/' . $order_row->id) . '" class="email_button">Email</a>';
			}
			$row[] = $action;
            if($order_row->party_type_1 == PARTY_TYPE_EXPORT_ID){
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->proforma_invoice_no.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->quotation_no.'</a>';
                
                if ($order_row->is_dispatched == DISPATCHED_ORDER_ID ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >Dispatched</a>';
                } else if ($order_row->is_dispatched == CANCELED_ORDER_ID ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >Canceled Order</a>';
                } else if ($order_row->is_dispatched == CHALLAN_CREATED_FROM_SALES_ORDER_ID ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >Challan Created from Sales Order</a>';
                } else {
                    $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >Pending Dispatch</a>';
                }
                
                if ($order_row->isApproved == 1 ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >Approved</a>';
                } else {
                    $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >Not Approved</a>';
                }
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->party_name.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->sales_person.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.date('d-m-Y', strtotime($order_row->sales_order_date)).'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->city.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->state.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->country.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add_export/' . $order_row->id . '?view') . '" >'.$order_row->party_current_person.'</a>';
            } elseif ($order_row->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->proforma_invoice_no.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->quotation_no.'</a>';
                
                if ($order_row->is_dispatched == DISPATCHED_ORDER_ID ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >Dispatched</a>';
                } else if ($order_row->is_dispatched == CANCELED_ORDER_ID ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >Canceled Order</a>';
                } else if ($order_row->is_dispatched == CHALLAN_CREATED_FROM_SALES_ORDER_ID ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >Challan Created from Sales Order</a>';
                } else {
                    $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >Pending Dispatch</a>';
                }
                
                if ($order_row->isApproved == 1 ) {
                    $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >Approved</a>';
                } else {
                    $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >Not Approved</a>';
                }
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->party_name.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->sales_person.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.date('d-m-Y', strtotime($order_row->sales_order_date)).'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->city.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->state.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->country.'</a>';
                $row[] = '<a href="' . base_url('proforma_invoices/add/' . $order_row->id . '?view') . '" >'.$order_row->party_current_person.'</a>';
            }
			
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function delete_proforma_invoice($id){
		$role_delete = $this->app_model->have_access_role(PROFORMA_INVOICE_MODULE_ID, "delete");
		if ($role_delete) {

			$quotation_id = $this->crud->get_id_by_val('proforma_invoices','quotation_id','id',$id);
			$this->crud->update('quotations',array('is_proforma_created'=> 0),array('id'=>$quotation_id));

			$where_array = array("id" => $id);
			$this->crud->delete("proforma_invoices", $where_array);
			$where_array = array("proforma_invoice_id" => $id);
			$this->crud->delete("proforma_invoice_items", $where_array);
			$this->crud->delete("proforma_invoice_logins", $where_array);

			$session_data = array(
				'success_message' => "Proforma Invoice has been deleted !"
			);
			$this->session->set_userdata($session_data);
			redirect(BASE_URL . "proforma_invoices/proforma_invoices_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function proforma_invoice_print($id){
		$this->print_pdf($id,'print');
	}

	function proforma_invoice_email($id){
		$this->print_pdf($id,'email');
	}

	function print_pdf($id,$type){
		$proforma_invoice_data = $this->get_proforma_invoice_detail($id);
                $proforma_invoice_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $proforma_invoice_data->delivery_through_id));
//		echo '<pre>';print_r($proforma_invoice_data);exit;
		if(isset($proforma_invoice_data->delivery_city)){
			$proforma_invoice_data->delivery_city = $this->crud->get_id_by_val('city','city','city_id', $proforma_invoice_data->delivery_city);
		}
		if(isset($proforma_invoice_data->delivery_state)){
			$proforma_invoice_data->delivery_state = $this->crud->get_id_by_val('state','state','state_id', $proforma_invoice_data->delivery_state);
		}
		if(isset($proforma_invoice_data->delivery_country)){
			$proforma_invoice_data->delivery_country = $this->crud->get_id_by_val('country','country','country_id', $proforma_invoice_data->delivery_country);
		}
		$company_details = $this->applib->get_company_detail();
		$proforma_invoice_items = $this->get_proforma_invoice_items($id);
        $letterpad_details = array();
        if($proforma_invoice_data->party_type_1 == PARTY_TYPE_EXPORT_ID){
			$company_banks = $this->crud->get_all_with_where('company_banks','','',array('for_export' => '1'));
            $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'export_letter_pad_content');
		}elseif($proforma_invoice_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
			$company_banks = $this->crud->get_all_with_where('company_banks','','',array('for_domestic' => '1'));
            $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
		}
        //echo '<pre>';print_r($proforma_invoice_data);exit;	
		$proforma_invoice_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $proforma_invoice_data->delivery_through_id));
		if (!empty($proforma_invoice_items)) {
			foreach ($proforma_invoice_items as $key => $item_row) {
				$proforma_invoice_items[$key]['hsn'] = $this->crud->get_id_by_val('items','hsn_code','id', $item_row['item_id']);
				$proforma_invoice_items[$key]['item_extra_accessories'] = $this->crud->get_id_by_val('item_extra_accessories','item_extra_accessories_label','item_extra_accessories_id', $item_row['item_extra_accessories_id']);
			}
		}
//                echo '<pre>'; print_r($proforma_invoice_data); exit;
		$html = $this->load->view('proforma_invoices/proforma_invoice_pdf', array('proforma_invoice_data' => $proforma_invoice_data, 'company_details' => $company_details, 'proforma_invoice_items' => $proforma_invoice_items, 'company_banks' => $company_banks, 'letterpad_details' => $letterpad_details), true);
		$this->load->library('m_pdf');
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$margin_left = $company_details['margin_left'];
        $margin_right = $company_details['margin_right'];
        $margin_top = $company_details['margin_top'];
        $margin_bottom = $company_details['margin_bottom'];
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$pdf->WriteHTML($html);
        $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: '. $letterpad_details->header_logo_alignment .';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>');
        $pdf->SetHTMLFooter($letterpad_details->footer_detail);
//		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/ProformaInvoice_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');

		// Terms and conditions Start				
		if($proforma_invoice_data->party_type_1 == PARTY_TYPE_EXPORT_ID){
			$t_and_c_id = $this->crud->get_id_by_val('terms_and_conditions','id','module','proforma_invoice_terms_and_conditions_for_export');
		}elseif($proforma_invoice_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
			$t_and_c_id = $this->crud->get_id_by_val('terms_and_conditions','id','module','proforma_invoice_terms_and_conditions_for_domestic');
		}
		$t_and_c_result_array = $this->crud->get_columns_val_by_where('terms_and_conditions_sub','*',array('module_id' => $t_and_c_id));

		$replacements = array(0 => array('detail' => $proforma_invoice_data->terms_condition_purchase));
		$t_and_c_result_array = array_replace($t_and_c_result_array, $replacements);
		$terms_conditionss = '';
		$tc_flag_inc = 1;
		if (!empty($t_and_c_result_array)) {
            $tc_count = count($t_and_c_result_array);
            $tc_count = $tc_count + 1;
			//                    $first = reset($query->result_array());
			//                    $last = end($query->result_array());
			foreach ($t_and_c_result_array as $key => $page_detail_row) {
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				if($tc_flag_inc == 1){
					$terms_conditionss[0] .= '<div align="center" style="font-size: 18px;"><b>Terms And Conditions</b></div>';    
				} $tc_flag_inc++;
                $terms_conditionss[$key] .= nl2br($page_detail_row['detail']);
                if($tc_flag_inc == $tc_count){
                if($proforma_invoice_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
                //                        if(end($query->result_array())){
				$terms_conditionss[$key] .= '<div>
                                                <table>
												    <tr>
                                                        <td valign="top" class="text-bold" rowspan="3" width="65%">
                                                            &nbsp;
                                                        </td>
                                                        <td align="center" class="text-bold" colspan="4" width="" style="font-size: 12px;">
                                                            <strong>For, '.$company_details['name'].'</strong><br/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="text-bold no-border-top"  colspan="4" >
                                                            <img src="'.BASE_URL.'/resource/image/jk_symbol.jpg" style="margin-bottom:5px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="text-bold no-border-top" colspan="4" 	>
                                                            AUTHORISED SIGNATORY
                                                        </td>
                                                    </tr>
												</table>
                                            </div>';
				//}
                }
                if($proforma_invoice_data->party_type_1 == PARTY_TYPE_EXPORT_ID){
                $terms_conditionss[$key] .= '<div>
                                                <table class="">
                                                    <tr class="">
                                                        <td valign="top" class="text-bold" rowspan="2" width="65%">
                                                            &nbsp;
                                                        </td>
                                                        <td align="center" class="footer-detail-area">
                                                            <strong>For, '.$company_details['name'].'</strong>
                                                        </td>
                                                    </tr>
                                                    <tr class="no-border">
                                                        <td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                                            <img src="'.BASE_URL.'/resource/image/jk_symbol.jpg" style="margin-bottom:5px"><br />
                                                            '.$company_details['m_d_name'].'<br/>
                                                            Managing Director
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>';
				}
                }
				$pdf->WriteHTML($terms_conditionss[$key]);
                $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: ' . $letterpad_details->header_logo_alignment . ';"><img src="' . base_url() . image_dir('letterpad_header_logo/' . $letterpad_details->header_logo) . '" width="100px"></div>');
                $pdf->SetHTMLFooter($letterpad_details->footer_detail);
//                $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/ProformaInvoice_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
			}
		}//exit;
		// Terms and conditions End
        if($type == 'print'){
            $pdfFilePath = "proforma_invoice.pdf";
		$pdf->Output($pdfFilePath, "I");
        } elseif($type == 'email'){
            $pdfFilePath = "./uploads/proforma_invoice_" . $id . ".pdf";
            $pdf->Output($pdfFilePath, "F");
        }
		if($type == 'email'){
            $this->db->where('id', $id);
            $this->db->update('proforma_invoices', array('pdf_url' => $pdfFilePath));
            redirect(base_url() . 'mail-system3/document-mail/proforma_invoice/' . $id);
        }
	}

	function get_proforma_invoice_detail($id)
	{
		$select = "sales.sales,currency.currency,quo.quotation_no,quo.quotation_date,pi.id as proforma_invoice_id,pi.*,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.address,party.email_id,party.phone_no as p_phone_no,party.fax_no,party.pincode,party.party_type_1,city.city,state.state,country.country,party.w_delivery_party_name AS delivery_party_name,party.w_address AS delivery_address,party.w_city AS delivery_city,party.w_state AS delivery_state,party.w_country AS delivery_country,party.w_phone1 AS delivery_contact_no,party.w_email AS delivery_email_id,party.oldw_pincode AS delivery_oldw_pincode,party.address_work,party.utr_no AS party_cin_no,";
		$select .= "party.gst_no as p_gst_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required,";
		$select .= "pi.bank_detail,company_banks.id,company_banks.detail AS banks_detail,sea_freight_type.name AS sea_freight_type,";
		$this->db->select($select);
		$this->db->from('proforma_invoices pi');
		$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = pi.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = pi.kind_attn_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = pi.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = pi.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = pi.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = pi.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = pi.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = pi.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = pi.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = pi.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = pi.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = pi.for_required_id', 'left');
		$this->db->join('company_banks', 'company_banks.id = pi.bank_detail', 'left');
		$this->db->join('sea_freight_type', 'sea_freight_type.id = pi.sea_freight_type', 'left');
		$this->db->where('pi.id', $id);
		$this->db->where('party.active !=', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}


	function get_proforma_invoice_items($id){
		$this->db->select('soi.*');
		$this->db->from('proforma_invoice_items soi');
		$this->db->join('item_category ic', 'ic.id = soi.item_category_id');
		$this->db->where('ic.item_category', 'Finished Goods');
		$this->db->where('soi.proforma_invoice_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	function get_proforma_invoices_sub_items($id){
		$this->db->select('soi.*');
		$this->db->from('proforma_invoice_items soi');
		$this->db->join('item_category ic', 'ic.id = soi.item_category_id');
		$this->db->where('ic.item_category != ', 'Finished Goods');
		$this->db->where('soi.proforma_invoice_id', $id);
		$query = $this->db->get();
		$other_items = array();
		if ($query->num_rows() > 0) {
			$other_items = $query->result_array();
		}
		return $other_items;
	}

	function get_new_proforma_invoice_no(){
		$this->db->select_max('id');
		$this->db->from('proforma_invoices');
		$this->db->limit(1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id + 1;
		} else {
			return 1;
		}
	}

	function update_proforma_invoices_status_to_approved(){
		if ($_POST['proforma_invoice_id'] != '') {
			$proforma_invoice_id = $_POST['proforma_invoice_id'];
			$this->db->where('id', $proforma_invoice_id);
			$this->db->update('proforma_invoices', array('isApproved' => 1));
			$login_data['approved_by_id'] = $this->staff_id;
			$login_data['approved_date_time'] = $this->now_time;
			$login_data['updated_at'] = $this->now_time;
			$this->crud->update('proforma_invoice_logins', $login_data, array('proforma_invoice_id' => $proforma_invoice_id));
			echo json_encode(array('success' => true, 'message' => 'Proforma Invoice approved successfully!'));
			exit();
		} else {
			redirect("proforma_invoices/proforma_invoices_list"); exit;
			exit();
		}
	}

	function update_proforma_invoices_status_to_disapproved(){
		if ($_POST['proforma_invoice_id'] != '') {
			$proforma_invoice_id = $_POST['proforma_invoice_id'];
			$this->db->where('id', $proforma_invoice_id);
			$this->db->update('proforma_invoices', array('isApproved' => 0));
			$login_data['approved_by_id'] = null;
			$login_data['approved_date_time'] = null;
			$login_data['updated_at'] = $this->now_time;
			$this->crud->update('proforma_invoice_logins', $login_data, array('proforma_invoice_id' => $proforma_invoice_id));
			echo json_encode(array('success' => true, 'message' => 'Proforma Invoice disapproved successfully!'));
			exit();
		} else {
			redirect("proforma_invoices/proforma_invoices_list"); exit;
			exit();
		}
	}

}
