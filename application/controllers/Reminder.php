<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Reminder
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 * @property Crud $crud
 */
class Reminder extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}

		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
	}

	function update()
	{
		$status = 1;
		$msg = "Roles has been updated successfully.";
		$user_id = $this->input->post("user_id");

		if(intval($user_id) > 0)
		{
			$roles = $this->input->post("roles");

			// delete old roles
			$sql = "DELETE FROM staff_roles WHERE staff_id='$user_id'";
			$this->crud->execuetSQL($sql);

			// echo "<pre>";print_r($roles);exit;

			// add new roles
			if(is_array($roles) && count($roles) > 0)
			{                
				foreach($roles as $module_id => $role_id)
				{
					$tmp = explode("_", $module_id);
					$module_id = $tmp[1];

					$dataToInsert = array(
						'staff_id' => $user_id,
						'module_id' => $module_id,  
						'role_id' => $role_id,  
					);

					$this->crud->insert("staff_roles", $dataToInsert);
				}    
			}    
		}
		else
		{
			$status = 0;
			$msg = "Please staff user.";
		}

		echo json_encode(array("status" => $status,"msg" => $msg));
		exit;
	}

	function index() 
	{
		$status = 0;
		$msg = "Reminder added fail.";

		if($this->input->post('remind'))
		{
			//echo "<pre>";print_r($_POST);exit;
			$post_data = $this->input->post();
			$logged_in = $this->session->userdata("is_logged_in");
			$user_id = $logged_in['staff_id'];
			$created_date = date('Y-m-d H:i:s');
			$remind = $post_data['remind'];
			$reminder_date = $post_data['reminder_date'];
			$reminder_time = $post_data['reminder_time'];
			$reminder_date = date('Y-m-d H:i:s',strtotime($reminder_date.' '.$reminder_time));
			$assigned_by = $post_data['assigned_by'];
			$assigned_to = $post_data['assigned_to'];
			$data = array();

			$data['user_id'] = $user_id;
			$data['remind'] = $remind;
			$data['reminder_date'] = $reminder_date;
			$data['assigned_by'] = $assigned_by;
			$data['assigned_to'] = $assigned_to;
			$data['created_date'] = $created_date;
			$data['last_updated_date'] = $created_date;
			$this->crud->insert("reminder", $data);
			$status = 1;
			$msg = "Reminder added successfully.";
			echo json_encode(array("status" => $status,"msg" => $msg));
			exit;
		}
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"add")) {
            $data['agent_list'] = $this->crud->get_all_with_where('staff','name','asc',array('active !='=>0));
//            echo '<pre>'; print_r($data); exit;
			set_page('reminder/index', $data);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to add reminder.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function add(){
		$reminder_date_arr = $_REQUEST['reminder_date'];
		$reminder_time_arr = $_REQUEST['reminder_time'];
		$assigned_to_arr = $_REQUEST['assigned_to'];
		$status_arr = $_REQUEST['status'];
		$priority_arr = $_REQUEST['priority'];
		$created_date = date('Y-m-d H:i:s');
		$logged_in = $this->session->userdata("is_logged_in");
		$user_id = $logged_in['staff_id'];


		$rec_arr = $_REQUEST;
		$details = array();
		//echo "<pre>";print_r($rec_arr);exit;
		$details['user_id'] = $user_id;
		$details['remind'] = $rec_arr['remind'];
		foreach ($rec_arr['reminder_date'] as $key => $reminder_date) {
			$reminder_date = substr($reminder_date, 6, 4).'-'.substr($reminder_date, 3, 2).'-'.substr($reminder_date, 0, 2);
			$reminder_datetime = $reminder_date.' '.$rec_arr['reminder_time'][$key];
			$details['reminder_date'] = $reminder_datetime;
			$details['assigned_to'] = $rec_arr['assigned_to'][$key];
			$details['status'] = $rec_arr['status'][$key];
			$details['priority'] = $rec_arr['priority'][$key];
			$details['created_date'] = $created_date;
			$details['last_updated_date'] =  $created_date;
			//echo "<pre>";print_r($details);exit;
			$result = $this->crud->insert("reminder", $details);
		}
		$status = 0;
		$msg = "Reminder added fail.";
		if($result){
			$status = 1;
			$msg = "Reminder added successfully.";
		}
		echo json_encode(array("status" => $status,"msg" => $msg));
		exit;
	}

	function edit($id = 0) 
	{
		if(!$this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"edit")) {
			$this->session->set_flashdata('error_message', 'You have not permission to edit reminder.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$data = array();
		if($id == 0)
		{
			redirect("/");
		}
		else
		{   
			$status = 0;
			$msg = "Reminder update fail.";
			if($this->input->post())
			{
				$post_data = $this->input->post();
				$logged_in = $this->session->userdata("is_logged_in");
				$user_id = $logged_in['staff_id'];
				$updated_date = date('Y-m-d H:i:s');

				$remind = $post_data['remind'];
				$reminder_id = $post_data['reminder_id'];

				$reminder_time = isset($post_data['reminder_time'])?$post_data['reminder_time']:date('H:i:s');
				$reminder_date = $post_data['reminder_date'];

				$reminder_date = date('Y-m-d H:i:s',strtotime($reminder_date.' '.$reminder_time));
				$status = $post_data['status'];
				$priority = $post_data['priority'];
				$data = array();

				$data['remind'] = $remind;
				$data['reminder_date'] = $reminder_date;
				$data['assigned_to'] = $post_data['assigned_to'];
				$data['last_updated_date'] = $updated_date;
				$data['status'] = $status;
				$data['priority'] = $priority;
				if(strtotime($reminder_date) > strtotime(date('Y-m-d H:i:s'))) {
					$data['is_notified'] = 0;
				}

				//echo "<pre>";print_r($data);exit;
				//$this->crud->update("reminder", $data,array('reminder_id'=>$reminder_id,'user_id'=>$user_id));
				$this->crud->update("reminder", $data,array('reminder_id'=>$reminder_id));
				$status = 1;
				$msg = "Reminder Updated Successfully!";
				echo json_encode(array("status" => $status,"msg" => $msg));
				exit;
			}
			else
			{
				$reminder = $this->crud->get_all_with_where('reminder','','',array('reminder_id'=>$id));
				$data['reminder'] = $reminder;
				//echo "<pre>";print_r($reminder);exit;
				$date = DateTime::createFromFormat('Y-m-d H:i:s', $reminder[0]->reminder_date);
				$data['reminder_date_1'] = $date->format('d-m-Y');
				$data['reminder_time_1'] = $date->format('H:i');
				//echo $reminder[0]->reminder_date;exit;
				$data['agent_list'] = $this->crud->get_all_with_where('staff','staff_id','asc',array('active !='=>0));
				set_page('reminder/edit', $data);
			}
		}
	}
	function delete($id = 0) 
	{
		if(!$this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"delete")) {
			$this->session->set_flashdata('error_message', 'You have not permission to delete reminder.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$status = 0;
		$msg = "Reminder Delete fail.";
		if($id != 0)
		{
			$this->crud->delete("reminder", array('reminder_id'=>$id));
			$status = 1;
			$msg = "Reminder Delete successfully.";
		}
		echo json_encode(array("status" => $status,"msg" => $msg));
		exit;
	}

	function customer_reminder($id = ''){
		$customer_reminder = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$customer_reminder = $this->crud->get_all_with_where('customer_reminder', 'topic', 'ASC', $where_array);
		}
		$data = array();
		$data['id'] = ($customer_reminder) ? $customer_reminder[0]->id : '';
		$data['topic'] = ($customer_reminder) ? $customer_reminder[0]->topic : '';
		$data['subject'] = ($customer_reminder) ? $customer_reminder[0]->subject : '';
		$data['content_data'] = ($customer_reminder) ? $customer_reminder[0]->content : '';
		set_page('reminder/reminder_master', $data);
	}
	
	function customer_reminder_datatable(){
		$config['table'] = 'customer_reminder';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'topic','subject');
		$config['column_search'] = array('topic','subject');
		$config['order'] = array('topic' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_CUSTOMER_REMINDER, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_CUSTOMER_REMINDER, "delete");
		foreach ($list as $customer_reminders) {
			$row = array();
			$action = '';
			if($isEdit){
				$action .= '<a href="' . base_url('reminder/customer_reminder/' . $customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			} 
			if($isDelete){
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'.base_url('master/delete/'.$customer_reminders->id).'"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $customer_reminders->topic; 
			$row[] = $customer_reminders->subject;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}	
	
	function add_customer_reminder()
	{
		if(!$this->applib->have_access_role(MASTER_CUSTOMER_REMINDER,"add")) {
			$this->session->set_flashdata('error_message', 'You have not permission to add Reminder.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$post_data['created_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$post_data['updated_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);

		$result = $this->crud->insert('customer_reminder', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			$post_data['created_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Reminder Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_customer_reminder()
	{
		if(!$this->applib->have_access_role(MASTER_CUSTOMER_REMINDER,"edit")) {
			$this->session->set_flashdata('error_message', 'You have not permission to edit Reminder.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$post_data['updated_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);
		$result = $this->crud->update('customer_reminder', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Reminder Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	public function ajax_load_customer_reminder($id){
		$where_array['id'] = $id;
		$customer_reminder = $this->crud->get_all_with_where('customer_reminder', 'topic', 'ASC', $where_array);
		$data = array();
		$data['id'] = ($customer_reminder) ? $customer_reminder[0]->id : '';
		$data['party_id'] = ($customer_reminder) ? $customer_reminder[0]->party_id : '';
		$data['subject'] = ($customer_reminder) ? $customer_reminder[0]->subject : '';
		$data['content_data'] = ($customer_reminder) ? $customer_reminder[0]->content : '';
		print json_encode($data);
		exit;
	}
	
	function customer_reminder_sent(){
		$post_data = $this->input->post();
		$save_print_button = $post_data['save_print_button'];
		unset($post_data['save_print_button']);
		
		$where_array['party_id'] = $post_data['party_id'];
		$party_detail = $this->crud->get_all_with_where('party', 'party_id', 'ASC', $where_array);
		$party_detail = $party_detail[0];
		/*$vars = array(
			'{{Party_Name}}' => $party_detail->party_name,
			'{{Address}}' => nl2br($party_detail->address),
			'{{City}}' => $this->crud->get_id_by_val('city','city','city_id',$party_detail->city_id),
			'{{Pin}}' => !empty($party_detail->pincode)?' - '.$party_detail->pincode:'',
			'{{State}}' => $this->crud->get_id_by_val('state','state','state_id',$party_detail->state_id),
			'{{Country}}' => $this->crud->get_id_by_val('country','country','country_id',$party_detail->country_id),
			'{{Email}}' => $party_detail->email_id,
			'{{Tel_No}}' => $party_detail->fax_no,
			'{{Contact_No}}' => $party_detail->phone_no,
			'{{Contact_Person}}' => $this->crud->get_id_by_val('contact_person','name','party_id',$party_detail->party_id),
			'{{GST_No}}' => $party_detail->gst_no,
		);*/
		if($party_detail->email_id == ''){
			echo json_encode(array('empty' => 'empty' ));
			exit;
		}
		
		$id = $post_data['id'];
		unset($post_data['id']);
		/*$post_data['subject'] = strtr($post_data['subject'], $vars);
		$post_data['content_data'] = strtr($post_data['content_data'], $vars);*/
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$post_data['updated_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);
		
		if (trim($id) != '' && !empty(trim($id))) {
			$result = $this->crud->update('customer_reminder_sent', $post_data, array('id' => $id));
		} else {
			$letter_no = $this->crud->get_max_number('customer_reminder_sent', 'letter_no');
			$post_data['letter_no'] = $letter_no->letter_no + 1;
			$post_data['created_at'] = date('Y-m-d H:i:s');
			$post_data['created_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);
			$result = $this->crud->insert('customer_reminder_sent', $post_data);
			$id = $this->db->insert_id();
		}
		if ($result) {
			$post_data['id'] = $result;
			$post_data['created_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);
			$from = $this->session->userdata('is_logged_in')['email'];
			$to = $party_detail->email_id; 
			
			if(isset($save_print_button) && $save_print_button == 1){
				$post_data['last_insert_id'] = $id;
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Reminder Save Successfully! ');
			} else {
				//~ if($this->sendemail($from, $to, $post_data['subject'], $post_data['content_data'])){
					//~ $this->session->set_flashdata('success', true);
					//~ $this->session->set_flashdata('message', 'Reminder Sent to '.$party_detail->email_id);
				//~ } else {
					//~ $this->session->set_flashdata('success', false);
					//~ $this->session->set_flashdata('message', 'Mail not Send to '.$party_detail->email_id);
				//~ }
				
				$customer_reminder_detail = $this->get_customer_reminder_detail($id);
				$customer_reminder_detail->created_name = $this->crud->get_id_by_val('staff','name','staff_id',$customer_reminder_detail->created_by);
				$customer_reminder_detail->received_payment_currency_id = $this->crud->get_id_by_val('currency','currency','id',$customer_reminder_detail->received_payment_currency_id);
				
                if($customer_reminder_detail->sales_order_id != 0 || !empty($customer_reminder_detail->sales_order_id)){
                    $sales_order_data = $this->get_sales_order_detail($customer_reminder_detail->sales_order_id);
                }elseif($customer_reminder_detail->invoice_id != 0 || !empty($customer_reminder_detail->invoice_id)){
                    $sales_order_data = $this->get_invoice_detail($customer_reminder_detail->invoice_id);
                }
                
				$company_details = $this->get_company_detail();
				
				$this->load->library('m_pdf');
				$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
				$margin_left = $margin_company[0]->margin_left;
				$margin_right = $margin_company[0]->margin_right;
				$margin_top = $margin_company[0]->margin_top;
				$margin_bottom = $margin_company[0]->margin_bottom;
					
				$param = "'utf-8','A4'";
				$pdf = $this->m_pdf->load($param);
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
                
                $where_array['party_id'] = $customer_reminder_detail->party_id;
                $party_detail = $this->crud->get_all_with_where('party', 'party_id', 'ASC', $where_array);
                $party_detail = $party_detail[0];
                $vars = array(
                    '{{Party_Name}}' => $party_detail->party_name,
                    '{{Address}}' => nl2br($party_detail->address),
                    '{{City}}' => $this->crud->get_id_by_val('city','city','city_id',$party_detail->city_id),
                    '{{Pin}}' => !empty($party_detail->pincode)?' - '.$party_detail->pincode:'',
                    '{{State}}' => $this->crud->get_id_by_val('state','state','state_id',$party_detail->state_id),
                    '{{Country}}' => $this->crud->get_id_by_val('country','country','country_id',$party_detail->country_id),
                    '{{Email}}' => $party_detail->email_id,
                    '{{Tel_No}}' => $party_detail->fax_no,
                    '{{Contact_No}}' => $party_detail->phone_no,
                    '{{Contact_Person}}' => $this->crud->get_id_by_val('contact_person','name','party_id',$party_detail->party_id),
                    '{{GST_No}}' => $party_detail->gst_no,
                );
                $customer_reminder_detail->content_data = strtr($customer_reminder_detail->content_data, $vars);
                
				$html = $this->load->view('reminder/pdf', array('customer_reminder_detail' => $customer_reminder_detail, 'sales_order_data' => $sales_order_data, 'company_details' => $company_details), true);
				$pdf->WriteHTML($html);
				$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
				$pdfFilePath = "./uploads/Customer_Reminder_" . $id . ".pdf";
				$pdf->Output($pdfFilePath, "F");
				$this->db->where('id', $id);
				$this->db->update('customer_reminder_sent', array('pdf_url' => $pdfFilePath));
				$post_data['send_mail'] = base_url() . 'mail-system3/document-mail/customer_reminder_sent/' . $id;
			}
		}
		echo json_encode($post_data);
		exit;
	}
	
	function sendemail($from, $to, $subject, $content_data){
        // configure the email setting
        $this->load->library('email');
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = SMTP_EMAIL_ADDRESS;
		$config['smtp_pass']    = SMTP_EMAIL_PASSWORD;
		$config['charset']    = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html'; // or html
		$config['validation'] = TRUE; // bool whether to validate email or not
		$this->email->initialize($config);
        $this->email->reply_to(FROM_EMAIL_ADDRESS);
        $this->email->from($from, 'Jaykhodiyar');
        $this->email->to($to); 
        $this->email->bcc(BCC_EMAIL_ADDRESS); 
        $this->email->subject($subject);
        $this->email->message($content_data);
        return $this->email->send();
    }
	
	function customer_reminder_sent_datatable(){
		$config['table'] = 'customer_reminder_sent crs';
		$config['select'] = 'crs.*, p.party_name, cr.topic, city.city, state.state';
		$config['column_order'] = array(null, 'cr.topic','crs.subject','p.party_name');
		$config['column_search'] = array('cr.topic','crs.subject','p.party_name');
		$config['joins'][] = array('join_table' => 'customer_reminder cr', 'join_by' => 'cr.id = crs.customer_reminder_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = crs.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['order'] = array('crs.id' => 'DESC');
		if(!empty($_REQUEST['customer_reminder_id'])){
			$config['custom_where'] = ' crs.customer_reminder_id ="'.$_REQUEST['customer_reminder_id'].'" ';
        } else if($_REQUEST['reminder'] == 'delivery_reminder_list'){
			$config['custom_where'] = ' crs.customer_reminder_id IN ('.DELIVERY_REMINDER_ID.', '.ORDER_CONFIRMATION_REMINDER_ID.', '.ORDER_CANCEL_REMINDER_ID.', '.DISPATCH_REMINDER_ID.', '.PRICE_UPGRADE_REMINDER_ID.', '.ORDER_PAYMENT_REMINDER_ID.', '.BALANCE_PAYMENT_REMINDER_ID.') ';
        } elseif($_REQUEST['reminder'] == 'finance_reminder_list'){
            $config['custom_where'] = ' crs.customer_reminder_id IN ('.FINANCE_BALANCE_PAYMENT_ID.') ';
		} else {
			$config['wheres'][] = array('column_name' => 'crs.customer_reminder_id', 'column_value' => 2);
		} 
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "delete");
		foreach ($list as $customer_reminders) {
			$row = array();
			$action = '';
			if($isEdit){
				if($customer_reminders->customer_reminder_id == DELIVERY_REMINDER_ID){
					$action .= '<a href="' . base_url('sales_order/delivery_reminder/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				} else if($customer_reminders->customer_reminder_id == ORDER_CONFIRMATION_REMINDER_ID){
					$action .= '<a href="' . base_url('sales_order/order_confirmation_reminder/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				} else if($customer_reminders->customer_reminder_id == ORDER_CANCEL_REMINDER_ID){
					$action .= '<a href="' . base_url('sales_order/order_cancel_reminder/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				} else if($customer_reminders->customer_reminder_id == DISPATCH_REMINDER_ID){
					$action .= '<a href="' . base_url('sales_order/dispatch_reminder/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				} else if($customer_reminders->customer_reminder_id == PRICE_UPGRADE_REMINDER_ID){
					$action .= '<a href="' . base_url('sales_order/price_upgrade_reminder/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				} else if($customer_reminders->customer_reminder_id == ORDER_PAYMENT_REMINDER_ID){
					$action .= '<a href="' . base_url('sales_order/order_payment_reminder/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				} else if($customer_reminders->customer_reminder_id == BALANCE_PAYMENT_REMINDER_ID){
					$action .= '<a href="' . base_url('sales_order/balance_payment_reminder/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                } else if($customer_reminders->customer_reminder_id == FINANCE_BALANCE_PAYMENT_ID){
                    $action .= '<a href="' . base_url('finance/finance_balance_payment/'.$customer_reminders->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				}
			}
			if($isDelete){
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'.base_url('master/delete/'.$customer_reminders->id).'"><i class="fa fa-trash"></i></a>';
			}
			$action .= ' | <a href="' . base_url('reminder/customer_reminder_print/' . $customer_reminders->id) . '" target="_blank" class="btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
			$action .= ' | <a href="' . base_url('reminder/customer_reminder_email/' . $customer_reminders->id) . '" target="_blank" class="btn-primary btn-xs" data-href="#">Email</a>';
			$row[] = $action;
			$row[] = $this->applib->get_reminder_letter_no($customer_reminders->letter_no); 
			$row[] = $customer_reminders->topic;
			$row[] = $customer_reminders->subject;
            if($customer_reminders->sales_order_id != 0 || !empty($customer_reminders->sales_order_id)){
                $row[] = $this->crud->get_id_by_val('sales_order','sales_order_no','id',$customer_reminders->sales_order_id);    
            }elseif($customer_reminders->invoice_id != 0 || !empty($customer_reminders->invoice_id)){
                $row[] = $this->crud->get_id_by_val('invoices','invoice_no','id',$customer_reminders->invoice_id);    
            }
			$row[] = $customer_reminders->party_name;
			$row[] = $customer_reminders->city;
			$row[] = $customer_reminders->state;
			$row[] = substr($customer_reminders->created_at, 8, 2) .'-'. substr($customer_reminders->created_at, 5, 2) .'-'. substr($customer_reminders->created_at, 0, 4);
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	function get_company_detail(){
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id', COMPANY_ID);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row_array(0);
		} else {
			return array();
		}
	}
	
	function get_sales_order_detail($id){
		$select = "sales.sales,currency.currency,quo.quotation_no,so.id as sales_order_id,so.*,party.gst_no AS p_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.w_delivery_party_name AS delivery_party_name,party.w_address AS delivery_address,party.w_city AS delivery_city,party.w_state AS delivery_state,party.w_country AS delivery_country,party.w_phone1 AS delivery_contact_no,party.w_email AS delivery_email_id,party.oldw_pincode AS delivery_oldw_pincode,party.address_work,party.address,party.phone_no,city.city,state.state,country.country,party.utr_no AS party_cin_no,";
		$select .= "cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required";
		$this->db->select($select);
		$this->db->from('sales_order so');
		$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = so.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = so.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = so.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = so.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = so.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = so.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = so.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = so.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = so.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = so.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = so.for_required_id', 'left');
		$this->db->where('so.id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            //echo '<pre>';print_r($query->row(0));exit;
			return $query->row(0);
		} else {
			return array();
		}
	}
    
    function customer_reminder_print($id){
		$customer_reminder_detail = $this->get_customer_reminder_detail($id);
		$customer_reminder_detail->created_name = $this->crud->get_id_by_val('staff','name','staff_id',$customer_reminder_detail->created_by);
		$customer_reminder_detail->received_payment_currency_id = $this->crud->get_id_by_val('currency','currency','id',$customer_reminder_detail->received_payment_currency_id);
        
        if($customer_reminder_detail->sales_order_id != 0 || !empty($customer_reminder_detail->sales_order_id)){
            $sales_order_data = $this->get_sales_order_detail($customer_reminder_detail->sales_order_id);
        }elseif($customer_reminder_detail->invoice_id != 0 || !empty($customer_reminder_detail->invoice_id)){
            $sales_order_data = $this->get_invoice_detail($customer_reminder_detail->invoice_id);
        }
		
        $company_details = $this->get_company_detail();
        /*echo '<pre>';
        print_r($customer_reminder_detail);
        print_r($sales_order_data);
		print_r($company_details);
        exit;*/
		
		$this->load->library('m_pdf');
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
			
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
        
        $where_array['party_id'] = $customer_reminder_detail->party_id;
        $party_detail = $this->crud->get_all_with_where('party', 'party_id', 'ASC', $where_array);
        $party_detail = $party_detail[0];
        $vars = array(
            '{{Party_Name}}' => $party_detail->party_name,
            '{{Address}}' => nl2br($party_detail->address),
            '{{City}}' => $this->crud->get_id_by_val('city','city','city_id',$party_detail->city_id),
            '{{Pin}}' => !empty($party_detail->pincode)?' - '.$party_detail->pincode:'',
            '{{State}}' => $this->crud->get_id_by_val('state','state','state_id',$party_detail->state_id),
            '{{Country}}' => $this->crud->get_id_by_val('country','country','country_id',$party_detail->country_id),
            '{{Email}}' => $party_detail->email_id,
            '{{Tel_No}}' => $party_detail->fax_no,
            '{{Contact_No}}' => $party_detail->phone_no,
            '{{Contact_Person}}' => $this->crud->get_id_by_val('contact_person','name','party_id',$party_detail->party_id),
            '{{GST_No}}' => $party_detail->gst_no,
        );
        $customer_reminder_detail->content_data = strtr($customer_reminder_detail->content_data, $vars);

		$html = $this->load->view('reminder/pdf', array('customer_reminder_detail' => $customer_reminder_detail, 'sales_order_data' => $sales_order_data, 'company_details' => $company_details), true);
		$pdf->WriteHTML($html);
		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
		$pdfFilePath = "Customer_Reminder.pdf";
		$pdf->Output($pdfFilePath, "I");
	}
	
	function customer_reminder_email($id){
		$customer_reminder_detail = $this->get_customer_reminder_detail($id);
		$customer_reminder_detail->created_name = $this->crud->get_id_by_val('staff','name','staff_id',$customer_reminder_detail->created_by);
		$customer_reminder_detail->received_payment_currency_id = $this->crud->get_id_by_val('currency','currency','id',$customer_reminder_detail->received_payment_currency_id);
		
        if($customer_reminder_detail->sales_order_id != 0 || !empty($customer_reminder_detail->sales_order_id)){
            $sales_order_data = $this->get_sales_order_detail($customer_reminder_detail->sales_order_id);
        }elseif($customer_reminder_detail->invoice_id != 0 || !empty($customer_reminder_detail->invoice_id)){
            $sales_order_data = $this->get_invoice_detail($customer_reminder_detail->invoice_id);
        }
		$company_details = $this->get_company_detail();
		
		$this->load->library('m_pdf');
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
			
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
        
        $where_array['party_id'] = $customer_reminder_detail->party_id;
        $party_detail = $this->crud->get_all_with_where('party', 'party_id', 'ASC', $where_array);
        $party_detail = $party_detail[0];
        $vars = array(
            '{{Party_Name}}' => $party_detail->party_name,
            '{{Address}}' => nl2br($party_detail->address),
            '{{City}}' => $this->crud->get_id_by_val('city','city','city_id',$party_detail->city_id),
            '{{Pin}}' => !empty($party_detail->pincode)?' - '.$party_detail->pincode:'',
            '{{State}}' => $this->crud->get_id_by_val('state','state','state_id',$party_detail->state_id),
            '{{Country}}' => $this->crud->get_id_by_val('country','country','country_id',$party_detail->country_id),
            '{{Email}}' => $party_detail->email_id,
            '{{Tel_No}}' => $party_detail->fax_no,
            '{{Contact_No}}' => $party_detail->phone_no,
            '{{Contact_Person}}' => $this->crud->get_id_by_val('contact_person','name','party_id',$party_detail->party_id),
            '{{GST_No}}' => $party_detail->gst_no,
        );
        $customer_reminder_detail->content_data = strtr($customer_reminder_detail->content_data, $vars);
        
		$html = $this->load->view('reminder/pdf', array('customer_reminder_detail' => $customer_reminder_detail, 'sales_order_data' => $sales_order_data, 'company_details' => $company_details), true);
		$pdf->WriteHTML($html);
		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
		$pdfFilePath = "./uploads/Customer_Reminder_" . $id . ".pdf";
		$pdf->Output($pdfFilePath, "F");
		$this->db->where('id', $id);
		$this->db->update('customer_reminder_sent', array('pdf_url' => $pdfFilePath));
		redirect(base_url() . 'mail-system3/document-mail/customer_reminder_sent/' . $id);
	}
	
	function get_customer_reminder_detail($id){
		$select = "crs.*, cr.topic, p.party_name, p.email_id, cr.topic, city.city, state.state, country.country";
		$this->db->select($select);
		$this->db->from('customer_reminder_sent crs');
		$this->db->join('customer_reminder cr', 'cr.id = crs.customer_reminder_id', 'left');
		$this->db->join('party p', 'p.party_id = crs.party_id', 'left');
		$this->db->join('city', 'city.city_id = p.city_id', 'left');
		$this->db->join('state', 'state.state_id = p.state_id', 'left');
		$this->db->join('country', 'country.country_id = p.country_id', 'left');
		$this->db->where('crs.id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            return $query->row(0);
		} else {
			return array();
		}
	}
	
	function get_sales_order_datatable()
	{
//		$challan_sales_order_ids_db = $this->db->select('sales_order_id')->from('challans')->group_by('sales_order_id')->get()->result_array();
//		$challan_sales_order_ids = array();
//		foreach ($challan_sales_order_ids_db as $row) {
//			$challan_sales_order_ids[] = $row['sales_order_id'];
//		}
		$config['table'] = 'sales_order s';
		$config['select'] = 's.*,q.quotation_no,p.party_name, city.city, state.state';
		$config['select'] .= ',GROUP_CONCAT(soi.item_code) AS so_item_code';
		$config['column_order'] = array('q.quotation_no', 's.sales_order_no', 'soi.item_code', 'p.party_name', 'city.city', 'state.state', 's.sales_order_date');
		$config['column_search'] = array('q.quotation_no', 's.sales_order_no', 'p.party_name', 'DATE_FORMAT(s.sales_order_date,"%d-%m-%Y")', 'DATE_FORMAT(s.committed_date,"%d-%m-%Y")');
		$config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id');
		$config['joins'][] = array('join_table' => 'city city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales_order_items soi', 'join_by' => 'soi.sales_order_id = s.id', 'join_type' => 'left');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		$config['wheres'][] = array('column_name' => 's.isApproved', 'column_value' => '1');
		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow") == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		} else {
            $config['custom_where'] = '(s.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR s.created_by IS NULL)';
			if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
			} else if ($cu_accessExport == 1) {
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if ($cu_accessDomestic == 1) {
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			} else {}
            $config['wheres'][] = array('column_name' => 's.created_by', 'column_value' => $this->session->userdata('is_logged_in')['staff_id']);
		}
		$config['custom_where'] = ' `is_dispatched` = "'.PENDING_DISPATCH_ORDER_ID.'" ';
		$config['group_by'] = 's.id';
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
//		echo '<pre>';print_r($this->db->last_query());exit;
		foreach ($list as $order_row) {
//			if (in_array($order_row->id, $challan_sales_order_ids)) {
//			} else {
				$row = array();
				$so_item_code = explode(",", $order_row->so_item_code);
				$so_item_code = (count($so_item_code) > 1 ? 'Multiple' : $so_item_code[0]);
				$order_row->so_item_code = str_replace(',', ', ', $order_row->so_item_code);
				$row[] = "<a href='javascript:void(0);' class='sales_order_select_link' data-sales_order_id='".$order_row->id."' data-sales_to_party_id='".$order_row->sales_to_party_id."' data-sales_order_data='".$order_row->sales_order_no .' - '. $order_row->party_name."'>".$order_row->quotation_no."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_select_link' data-sales_order_id='".$order_row->id."' data-sales_to_party_id='".$order_row->sales_to_party_id."' data-sales_order_data='".$order_row->sales_order_no .' - '. $order_row->party_name."'>".$order_row->sales_order_no."</a>";
				
			$row[] = "<a href='javascript:void(0);' data-toggle='tooltip' data-placement='bottom' title='".$order_row->so_item_code."' class='sales_order_select_link' data-sales_order_id='".$order_row->id."' data-sales_to_party_id='".$order_row->sales_to_party_id."' data-sales_order_data='".$order_row->sales_order_no .' - '. $order_row->party_name."' >".$so_item_code."</a>";
				
				$row[] = "<a href='javascript:void(0);' class='sales_order_select_link' data-sales_order_id='".$order_row->id."' data-sales_to_party_id='".$order_row->sales_to_party_id."' data-sales_order_data='".$order_row->sales_order_no .' - '. $order_row->party_name."'>".$order_row->party_name."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_select_link' data-sales_order_id='".$order_row->id."' data-sales_to_party_id='".$order_row->sales_to_party_id."' data-sales_order_data='".$order_row->sales_order_no .' - '. $order_row->party_name."'>".$order_row->city."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_select_link' data-sales_order_id='".$order_row->id."' data-sales_to_party_id='".$order_row->sales_to_party_id."' data-sales_order_data='".$order_row->sales_order_no .' - '. $order_row->party_name."'>".$order_row->state."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_select_link' data-sales_order_id='".$order_row->id."' data-sales_to_party_id='".$order_row->sales_to_party_id."' data-sales_order_data='".$order_row->sales_order_no .' - '. $order_row->party_name."'>".date('d-m-Y', strtotime($order_row->sales_order_date))."</a>";
				$data[] = $row;
//			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	function ajax_load_sales_order_detail($sales_order_id){
		$sales_order_data = $this->get_sales_order_detail($sales_order_id);
		$sales_order_data->address = nl2br($sales_order_data->address);
		if($sales_order_data->received_payment_date != '1970-01-01' && $sales_order_data->received_payment_date != ''){
			$sales_order_data->received_payment_date = substr($sales_order_data->received_payment_date, 8, 2) .'/'. substr($sales_order_data->received_payment_date, 5, 2) .'/'. substr($sales_order_data->received_payment_date, 0, 4);
		}
		if($sales_order_data->sales_order_date != '1970-01-01' && $sales_order_data->sales_order_date != ''){
			$sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
		}
		if($sales_order_data->po_date != '1970-01-01' && $sales_order_data->po_date != ''){
			$sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
		}
		echo json_encode($sales_order_data);
		exit;
	}
	
	function get_invoice_detail($id){
		$select = "sales.sales,currency.currency,quo.quotation_no,pi.id as invoice_id,pi.*,party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,";
		$select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required,invoice_type.invoice_type";
		$this->db->select($select);
		$this->db->from('invoices pi');
		$this->db->join('sales_order so', 'so.id = pi.sales_order_id', 'left');
		$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = pi.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = pi.kind_attn_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = pi.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = pi.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = pi.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = pi.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = pi.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = pi.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = pi.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = pi.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = pi.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = pi.for_required_id', 'left');
		$this->db->join('invoice_type', 'invoice_type.id = pi.invoice_type_id', 'left');
		$this->db->where('pi.id', $id);
		$this->db->where('party.active !=', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}
	
	function get_invoice_datatable(){
		$config['table'] = 'invoices pi';
		$config['select'] = 'pi.*,p.party_name,so.sales_order_no, q.quotation_no';
		$config['column_order'] = array('pi.invoice_no', 'pi.challan_id', 'so.sales_order_no', 'q.quotation_no', 'p.party_name', 'pi.invoice_date');
		$config['column_search'] = array('pi.invoice_no', 'pi.challan_id', 'so.sales_order_no', 'q.quotation_no', 'p.party_name', 'pi.invoice_date');

		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = pi.sales_to_party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = pi.sales_order_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'q.id = so.quotation_id', 'join_type' => 'left');

		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		$config['where_string'] = ' 1 = 1 ';

		if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
		} else {
			$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
			$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");

			$config['custom_where'] = '( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
			if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
			} else if ($cu_accessExport == 1) {
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if ($cu_accessDomestic == 1) {
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		$role_delete = $this->app_model->have_access_role(INVOICE_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(INVOICE_MODULE_ID, "edit");

		foreach ($list as $order_row) {
			$row = array();
			$row[] = "<a href='javascript:void(0);' class='invoice_select_link' data-invoice_id='".$order_row->id."' data-party_id='".$order_row->sales_to_party_id."' >".sprintf("%04d", $order_row->invoice_no)."</a>";
			$row[] = "<a href='javascript:void(0);' class='invoice_select_link' data-invoice_id='".$order_row->id."' data-party_id='".$order_row->sales_to_party_id."' >".$order_row->challan_id."</a>";
			$row[] = "<a href='javascript:void(0);' class='invoice_select_link' data-invoice_id='".$order_row->id."' data-party_id='".$order_row->sales_to_party_id."' >".$order_row->sales_order_no."</a>";
			$row[] = "<a href='javascript:void(0);' class='invoice_select_link' data-invoice_id='".$order_row->id."' data-party_id='".$order_row->sales_to_party_id."' >".$order_row->quotation_no."</a>";
			$row[] = "<a href='javascript:void(0);' class='invoice_select_link' data-invoice_id='".$order_row->id."' data-party_id='".$order_row->sales_to_party_id."' >".$order_row->party_name."</a>";
			$row[] = "<a href='javascript:void(0);' class='invoice_select_link' data-invoice_id='".$order_row->id."' data-party_id='".$order_row->sales_to_party_id."' >".strtotime($order_row->invoice_date) != 0?date('d-m-Y', strtotime($order_row->invoice_date)):''."</a>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	function ajax_load_invoice_detail($invoice_id){
		$invoice_data = $this->get_invoice_detail($invoice_id);
		$invoice_data->address = nl2br($invoice_data->address);
		if($invoice_data->invoice_date != '1970-01-01' && $invoice_data->invoice_date != ''){
			$invoice_data->invoice_date = substr($invoice_data->invoice_date, 8, 2) .'-'. substr($invoice_data->invoice_date, 5, 2) .'-'. substr($invoice_data->invoice_date, 0, 4);
		}
		$sales_order_data = $this->get_sales_order_detail($invoice_data->sales_order_id);
		if($sales_order_data->sales_order_date != '1970-01-01' && $sales_order_data->sales_order_date != ''){
			$invoice_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
		}
		if($sales_order_data->po_date != '1970-01-01' && $sales_order_data->po_date != ''){
			$invoice_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
		}
		$invoice_data->quotation_no = $sales_order_data->quotation_no;
		$invoice_data->sales_order_no = $sales_order_data->sales_order_no;
		$invoice_data->cust_po_no = $sales_order_data->cust_po_no;
		$invoice_data->sales_order_pref = $sales_order_data->sales_order_pref;
		echo json_encode($invoice_data);
		exit;
	}
    
    function update_reminder_onshow_notify(){
        $reminder_id = $_POST['reminder_id'];
        if(!empty($reminder_id)){
            $result = $this->crud->update('reminder', array('is_notified' => 1), array('reminder_id' => $reminder_id));
        }
    }
	
    function update_quotation_reminder_onshow_notify(){
        $id = $_POST['fh_id'];
        if(!empty($id)){
            $this->crud->update('followup_history', array('is_notified' => 1), array('id' => $id));
        }
    }
	
}
