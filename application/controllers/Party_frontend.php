<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
/**
 * Class Party_frontend
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 * @property Chatmodule $chat_module
 */
class Party_frontend extends CI_Controller
{
	private $email_address = SMTP_EMAIL_ADDRESS;
	private $email_password = SMTP_EMAIL_PASSWORD;
	private $email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("Appmodel", "app_model");
        $this->load->model("Crud", "crud");
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
    }
    
    function files($file_id = '') {
        //$file_type_id = $this->crud->get_row_by_id('party_frontend_files', array('file_id' => $file_id));
//       $file_type_id = $this->crud->get_column_value_by_id('party_frontend_files', 'file_type_id', array('file_id' => $file_id));
//        $data['file_type_id'] = $file_type_id;
//        echo '<pre>'; print_r( $data['file_type_id']); exit;
        $data = array();
        if (isset($file_id) && !empty($file_id)) {
//            if ($this->app_model->have_access_role(PARTY_FRONTEND_MENU_ID, "view")) {
//                $where_array['file_id'] = $file_id;
//                $files = $this->crud->get_row_by_id('party_frontend_files', $where_array);
//                $data['created_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $files[0]->created_by));
//                $data['created_at'] = substr($files[0]->created_at, 8, 2) . '-' . substr($files[0]->created_at, 5, 2) . '-' . substr($files[0]->created_at, 0, 4);
//                $data['updated_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $files[0]->updated_by));
//                $data['updated_at'] = substr($files[0]->updated_at, 8, 2) . '-' . substr($files[0]->updated_at, 5, 2) . '-' . substr($files[0]->updated_at, 0, 4);
//                $data['file_id'] = $files[0]->file_id;
//                $data['item_id'] = $files[0]->item_id;
//                $data['caption'] = $files[0]->caption;
//                $data['file_type_id'] = $files[0]->file_type_id;
//                $data['file_name'] = $files[0]->file_name;
//                $data['party_id'] = $files[0]->party_id;
//                set_page('party_frontend/add_files', $data);
//            } else {
//                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
//                redirect("/");
//            }
        } else {
            if ($this->app_model->have_access_role(PARTY_FRONTEND_MENU_ID, "view")) {
                set_page('party_frontend/add_files');
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }
    
    function save_file(){
        $post_data = $this->input->post();
        unset($post_data['party_list_datatable_length']);
//        echo '<pre>';print_r($post_data);exit;
        if (isset($post_data['file_id']) && !empty($post_data['file_id'])) {
//            $post_data['updated_at'] = $this->now_time;
//            $post_data['updated_by'] = $this->staff_id;
//            $where_array['file_id'] = $post_data['file_id'];
//            $result = $this->crud->update('party_frontend_files', $post_data, $where_array);
//            if ($result) {
//                $return['success'] = "Updated";
//                $this->session->set_flashdata('success', true);
//                $this->session->set_flashdata('message', 'File Updated Successfully');
//            }
        } else {
            if(isset($post_data['selected_party_id']) && !empty($post_data['selected_party_id'])){
                $party_ids = $post_data['selected_party_id'];
                unset($post_data['selected_party_id']);
                foreach($party_ids as $party_id){
                    $post_data['party_id'] = $party_id;
                    $post_data['created_at'] = $this->now_time;
                    $post_data['created_by'] = $this->staff_id;
                    
                    $upload_flag = 0;
                    if($post_data['exist_file_name'] != ""){
                        $upload_flag = 1;
                        $post_data['file_name'] = $post_data['exist_file_name'];
                        unset($post_data['exist_file_name']);
                    } else {
                        unset($post_data['exist_file_name']);
                    }
                    $result = $this->crud->insert('party_frontend_files', $post_data);
                    $last_query_id = $this->db->insert_id();
                    
                    if($upload_flag  == 0){
                        $upload_path = '';
                        if($post_data['file_type_id'] == '1'){
                            $upload_path = './uploads/party_frontend/files';
                        } else if($post_data['file_type_id'] == '2'){
                            $upload_path = './uploads/party_frontend/videos';
                        } else if($post_data['file_type_id'] == '3'){
                            $upload_path = './uploads/party_frontend/photos';
                        }
                        if ($result) {
                            $file_element_name = 'file';
                            $config['upload_path'] = $upload_path;
                            $config['allowed_types'] = '*';
                            $config['overwrite'] = TRUE;
                            //$config['encrypt_name'] = FALSE;
                            $config['remove_spaces'] = TRUE;
                            $newFileName = $_FILES['file']['name'];
                            $tmp = explode('.', $newFileName);
                            $file_extension = end($tmp);
                            $filename = $newFileName;

                            $config['file_name'] = $filename;
                            if (!is_dir($config['upload_path'])) {
                                mkdir($config['upload_path'], 0777, TRUE);
                            }
                            $config['max_size'] = 0;
                            $this->upload->initialize($config);
                            if (!$this->upload->do_upload($file_element_name)) {
                                $return['Uploaderror'] = $this->upload->display_errors();
                            }
                            $file_data = $this->upload->data();
                            if (!empty($file_data['file_name']) && !empty($file_extension)) {
                                $f_data['file_name'] = $file_data['file_name'];
                                $f_where_array['file_id'] = $last_query_id;
                                $file_id = $this->crud->update('party_frontend_files', $f_data, $f_where_array);
                            }
                            @unlink($_FILES[$file_element_name]);
                        }
                    }
                }
            } else {
                $post_data['created_at'] = $this->now_time;
                $post_data['created_by'] = $this->staff_id;
                $upload_flag = 0;
                if($post_data['exist_file_name'] != ""){
                    $upload_flag = 1;
                    $post_data['file_name'] = $post_data['exist_file_name'];
                    unset($post_data['exist_file_name']);
                } else {
                    unset($post_data['exist_file_name']);
                }
                $result = $this->crud->insert('party_frontend_files', $post_data);
                $last_query_id = $this->db->insert_id();
                if($upload_flag  == 0){
                    $upload_path = '';
                    if($post_data['file_type_id'] == '1'){
                        $upload_path = './uploads/party_frontend/files';
                    } else if($post_data['file_type_id'] == '2'){
                        $upload_path = './uploads/party_frontend/videos';
                    } else if($post_data['file_type_id'] == '3'){
                        $upload_path = './uploads/party_frontend/photos';
                    } else if($post_data['file_type_id'] == '4'){
                        $upload_path = './uploads/party_frontend/foundation_detail';
                    }
                    if ($result) {
                        $file_element_name = 'file';
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = '*';
                        $config['overwrite'] = TRUE;
                        //$config['encrypt_name'] = FALSE;
                        $config['remove_spaces'] = TRUE;
                        $newFileName = $_FILES['file']['name'];
                        $tmp = explode('.', $newFileName);
                        $file_extension = end($tmp);
                        $filename = $newFileName;

                        $config['file_name'] = $filename;
                        if (!is_dir($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, TRUE);
                        }
                        $config['max_size'] = 0;
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload($file_element_name)) {
                            $return['Uploaderror'] = $this->upload->display_errors();
                        }
                        $file_data = $this->upload->data();
                        if (!empty($file_data['file_name']) && !empty($file_extension)) {
                            $f_data['file_name'] = $file_data['file_name'];
                            $f_where_array['file_id'] = $last_query_id;
                            $file_id = $this->crud->update('party_frontend_files', $f_data, $f_where_array);
                        }
                        @unlink($_FILES[$file_element_name]);
                    }
                }
            }
            
            if ($result) {
                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'File Added Successfully');
            }
        }
        //echo '<pre>';print_r($data);exit;
        print json_encode($return);
        exit;
    }
    
    function file_list(){
        if ($this->app_model->have_access_role(PARTY_FRONTEND_MENU_ID, "view")) {
            set_page('party_frontend/file_list');
        } else {
            redirect("/");
        }
    }
        
    function file_list_datatable() {
        $config['table'] = 'party_frontend_files pf';
        $config['select'] = 'pf.*, it.item_name, pt.party_name';
        $config['column_order'] = array(null, 'it.item_name', 'pf.caption', 'pt.party_name', 'pf.file_type_id', 'pf.created_at');
		$config['column_search'] = array('it.item_name', 'pf.caption','pt.party_name', 'DATE_FORMAT(pf.created_at,"%d-%m-%Y")');
        $config['joins'][] = array('join_table' => 'items it', 'join_by' => 'it.id = pf.item_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party pt', 'join_by' => 'pt.party_id = pf.party_id', 'join_type' => 'left');
        if (isset($_POST['item_id']) && !empty($_POST['item_id'])) {
			$config['wheres'][] = array('column_name' => 'pf.item_id', 'column_value' => $_POST['item_id']);
		}
        if (isset($_POST['file_type_id']) && $_POST['file_type_id'] != 0) {
			$config['wheres'][] = array('column_name' => 'pf.file_type_id', 'column_value' => $_POST['file_type_id']);
		}
        if (isset($_POST['party_id']) && !empty($_POST['party_id'])) {
			$config['wheres'][] = array('column_name' => 'pf.party_id', 'column_value' => $_POST['party_id']);
		}
        $config['order'] = array('pf.file_id' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $isDelete = $this->app_model->have_access_role(PARTY_FRONTEND_MENU_ID, "view");
        foreach ($list as $file) {
            $folder_name = '';
            $file_type = '';
            if ($file->file_type_id == '1') {
                $file_type = 'General Files';
                $folder_name = 'files';
            } else if ($file->file_type_id == '2') {
                $file_type = 'Videos';
                $folder_name = 'videos';
            } else if ($file->file_type_id == '3') {
                $file_type = 'Photos';
                $folder_name = 'photos';
            } else if ($file->file_type_id == '4') {
                $file_type = 'Foundation Detail';
                $folder_name = 'foundation_detail';
            }
            $row = array();
            $action = '';
            if ($isDelete) {
                $action .= '&nbsp;<a href="'. BASE_URL.'uploads/party_frontend/'.$folder_name.'/'.$file->file_name.'" class="btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a> &nbsp;';
                $action .= ' | &nbsp;<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('party_frontend/delete_file/' . $file->file_id) . '"><i class="fa fa-trash"></i></a> &nbsp;';
            }
            $row[] = $action;
            $row[] = $file->item_name;
            $row[] = $file->caption;
            $row[] = $file_type;
            if(isset($file->party_name) && !empty($file->party_name)){
                $row[] = $file->party_name;
            } else {
                $row[] = "All";
            }
            $row[] = date('d-m-Y h:i', strtotime($file->created_at));
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function fileupload_datatable() {
        $folder_name = "photos";       
        $file_type = 'Photos';
        if (isset($_POST['file_type_id']) && $_POST['file_type_id'] == '1') {
            $file_type = 'Files';
            $folder_name = 'files';
        } else if (isset($_POST['file_type_id']) && $_POST['file_type_id'] == '2') {
            $file_type = 'Videos';
            $folder_name = 'videos';
        } else if (isset($_POST['file_type_id']) && $_POST['file_type_id'] == '4') {
            $file_type = 'Foundation Detail';
            $folder_name = 'foundation_detail';
        }
        $this->load->helper('directory');       
        $map = directory_map('./uploads/party_frontend/'.$folder_name.'/');  
//        echo "<pre>";
//        print_r($map);exit;
        //$isDelete = $this->app_model->have_access_role(PARTY_FRONTEND_MENU_ID, "view");
        $data = array();
        foreach ($map as $file_key => $file) {          
            $row = array();
            $action = '';
            $row[] = '<input type="radio" id="fileupload_id_'.$file_key.'" name="file_name_radio_btn" data-file_val="val" class="fileupload_id" value="'.$file.'" />';
            if( $folder_name == "photos"){
                $row[] = '<label for="fileupload_id_'.$file_key.'" ><img src="' . base_url("/uploads/party_frontend/".$folder_name."/" . $file) . '" alt="" height="170" class="uploaded_file"></label>';
            } else {
                $row[] = '<label for="fileupload_id_'.$file_key.'" >'.$file.'</label>';
            }            

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($map),
            "recordsFiltered" => count($map),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    
    function delete_file($file_id){
        $role_delete = $this->app_model->have_access_role(PARTY_FRONTEND_MENU_ID, "view");
        if ($role_delete) {
            $file_name = $this->crud->get_id_by_val('party_frontend_files','file_name','file_id', $file_id);
            $file_type = $this->crud->get_id_by_val('party_frontend_files', 'file_type_id','file_id', $file_id);
            if($file_type == 1){
                $folder_name = 'files';
            }else if($file_type == 2){
                $folder_name = 'videos';
            }else if($file_type == 3){
                $folder_name = 'photos';
            }else if($file_type == 4){
                $folder_name = 'foundation_detail';
            }
            $repeat_name = $this->db->query('SELECT COUNT( file_id ) as count FROM party_frontend_files WHERE  `file_name` =  "'. $file_name .'"');
            $repeat = $repeat_name->row();
            if($repeat->count > 1){
            } else {
                $delete_file_path = './uploads/party_frontend/'.$folder_name.'/'.$file_name;
                unlink($delete_file_path);
            }
            $where_array = array("file_id" => $file_id);
            $this->crud->delete("party_frontend_files", $where_array);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
    
    function party_list_datatable() {
		//print_r($this->session->userdata('user_roles'));
		$config['table'] = 'party p';

		$config['select'] = 'p.party_id,p.party_created_date,p.party_code,p.party_name,p.phone_no,p.email_id,p.active as party_status,p.created_by as party_created_by,c.city,s.state,cy.country,p.username,p.password,p.party_type_1';
		$config['column_search'] = array('DATE_FORMAT(p.party_created_date,"%d-%m-%Y")','p.party_code','p.party_name','p.phone_no','p.email_id','c.city','s.state','cy.country');
		$config['column_order'] = array(null,'p.party_created_date','p.party_code','p.party_name','p.phone_no','p.email_id','p.username','p.password','c.city','s.state','cy.country');
		$config['joins'][] = array('join_table'=>'city c','join_by'=>'c.city_id = p.city_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'state s','join_by'=>'s.state_id = p.state_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'country cy','join_by'=>'cy.country_id = p.country_id','join_type'=>'left');
		if (isset($_POST['party_status']) && $_POST['party_status'] != 'all') {
			$config['wheres'][] = array('column_name' => 'active', 'column_value' => $_POST['party_status']);
		}

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
        if($this->app_model->have_access_role(PARTY_MODULE_ID, "is_management")){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$config['custom_where'] = '( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if($cu_accessDomestic == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}

		$config['order'] = array('party_name' => 'ASC');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $party) {
			$row = array();
			$action = '<input type="checkbox" value="'.$party->party_id.'" class="party_id" />';
			$row[] = $action;
			$row[] = $party->party_code;
			$row[] = $party->party_name;
			$phone_no_arr = preg_split('/[\\n\,]+/', $party->phone_no);
			$phone_nos = '';
			$p_inc = 1;
			foreach($phone_no_arr as $phone_no){
				if(!empty(trim($phone_no))){
					$phone_nos .= $phone_no.', ';
					if($p_inc%2 == 0){ $phone_nos .= '<br />'; }
					$p_inc++;
				}
			}
			$row[] = $phone_nos;
			$email_id_arr = explode(',', $party->email_id);
			$email_ids = '';
			foreach($email_id_arr as $email_id){
				if(!empty(trim($email_id))){
					$email_ids .= $email_id.', ';
					$email_ids .= '<br />';
				}
			}
			$row[] = $email_ids;
            $party_type = '';
            if($party->party_type_1 == PARTY_TYPE_EXPORT_ID){
                $party_type = 'Export';
            } else if($party->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
                $party_type = 'Domestic';
            }
			$row[] = $party_type;
			$row[] = $party->city;
			$row[] = $party->state;
			$row[] = $party->country;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
    
}
