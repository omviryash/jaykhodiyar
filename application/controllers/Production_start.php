<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 * @property M_pdf $m_pdf
 */
class Production_start extends CI_Controller {

    protected $staff_id = 1;

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
        $this->now_time = date('Y-m-d H:i:s');
    }

    function sales_order_itemqtywise_datatable() {
        $select = 'si.id as sales_order_item_id,si.item_name,si.quantity, si.dispatched_qty, si.pro_started_qty, s.id,sales.sales,q.quotation_no,q.enquiry_no,s.sales_order_no,s.sales_order_date,s.sales_order_status_id,p.party_name,p.phone_no,p.email_id,';
        $select .= 'p.party_type_1,city.city,state.state,country.country';
        $config['table'] = 'sales_order_items si';
        $config['select'] = $select;
        $config['joins'][] = array('join_table' => 'sales_order s', 'join_by' => 'si.sales_order_id = s.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'sales', 'join_by' => 'sales.id = s.sales_id', 'join_type' => 'left');
        $config['column_order'] = array('q.enquiry_no', 'q.quotation_no', 's.sales_order_no', 'p.party_name', 'si.item_name', 'p.phone_no', 'p.email_id', 'sales.sales', 'city.city', 'state.state', 'country.country');
        $config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 's.sales_order_no', 'p.party_name', 'si.item_name', 'p.phone_no', 'p.email_id', 'sales.sales', 'city.city', 'state.state', 'country.country');
        $config['order'] = array('s.id' => 'desc');

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
            
        } elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        } else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
        }

        $config['wheres'][] = array('column_name' => 'p.active != ', 'column_value' => '0');
        $config['wheres'][] = array('column_name' => 's.isApproved ', 'column_value' => '1');
        $config['wheres'][] = array('column_name' => 's.is_dispatched != ', 'column_value' => CANCELED_ORDER_ID);
        $config['wheres'][] = array('column_name' => 's.is_dispatched != ', 'column_value' => CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID);
        $config['custom_where'] = 'si.quantity != si.dispatched_qty';

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $sales_order) {
            $quantity = $sales_order->quantity - $sales_order->dispatched_qty - $sales_order->pro_started_qty;
            for ($q_inc = 1; $q_inc <= $quantity; $q_inc++) {
                $row = array();
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->enquiry_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->quotation_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->sales_order_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->party_name . "</a>";
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->item_name . "</a>";
                $phone_no = substr($sales_order->phone_no, 0, 10);
                $sales_order->phone_no = str_replace(',', ', ', $sales_order->phone_no);
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-toggle='tooltip' data-placement='bottom' title='" . $sales_order->phone_no . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $phone_no . "</a>";
                $email_id = substr($sales_order->email_id, 0, 14);
                $sales_order->email_id = str_replace(',', ', ', $sales_order->email_id);
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'data-toggle='tooltip' data-placement='bottom' title='" . $sales_order->email_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $email_id . "</a>";
                $party_type = '';
                if ($sales_order->party_type_1 == 5) {
                    $party_type = 'Export';
                } elseif ($sales_order->party_type_1 == 6) {
                    $party_type = 'Domestic';
                }
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $party_type . "</a>";
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->city . "</a>";
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->state . "</a>";
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_no='" . $sales_order->sales_order_no . "'>" . $sales_order->country . "</a>";
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //echo $this->db->last_query();
        echo json_encode($output);
    }

    function export_proforma_invoice_itemqtywise_datatable() {
        $select = 'pii.id as proforma_invoice_item_id,pii.item_name,pii.quantity, pii.dispatched_qty, pii.pro_started_qty, pi.id,sales.sales,q.quotation_no,q.enquiry_no,pi.proforma_invoice_no,pi.sales_order_date,p.party_name,p.phone_no,p.email_id,';
        $select .= 'p.party_type_1,city.city,state.state,country.country';
        $config['table'] = 'proforma_invoice_items pii';
        $config['select'] = $select;
        $config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pii.proforma_invoice_id = pi.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'pi.quotation_id = q.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = pi.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'sales', 'join_by' => 'sales.id = pi.sales_id', 'join_type' => 'left');
        $config['column_order'] = array('q.enquiry_no', 'q.quotation_no', 'pi.proforma_invoice_no', 'p.party_name', 'pii.item_name', 'p.phone_no', 'p.email_id', 'sales.sales', 'city.city', 'state.state', 'country.country');
        $config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 'pi.proforma_invoice_no', 'p.party_name', 'pii.item_name', 'p.phone_no', 'p.email_id', 'sales.sales', 'city.city', 'state.state', 'country.country');
        $config['order'] = array('pi.id' => 'desc');

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        if ($cu_accessExport == 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        }
        $config['wheres'][] = array('column_name' => 'p.active != ', 'column_value' => '0');
        $config['wheres'][] = array('column_name' => 'pi.isApproved ', 'column_value' => '1');
        $config['wheres'][] = array('column_name' => 'pi.is_dispatched != ', 'column_value' => CANCELED_ORDER_ID);
        $config['wheres'][] = array('column_name' => 'pi.is_dispatched != ', 'column_value' => CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID);

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $proforma_invoice) {
            $quantity = $proforma_invoice->quantity - $proforma_invoice->dispatched_qty - $proforma_invoice->pro_started_qty;
            for ($q_inc = 1; $q_inc <= $quantity; $q_inc++) {
                $row = array();
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->enquiry_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->quotation_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->proforma_invoice_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->party_name . "</a>";
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->item_name . "</a>";
                $phone_no = substr($proforma_invoice->phone_no, 0, 10);
                $proforma_invoice->phone_no = str_replace(',', ', ', $proforma_invoice->phone_no);
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-toggle='tooltip' data-placement='bottom' title='" . $proforma_invoice->phone_no . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $phone_no . "</a>";
                $email_id = substr($proforma_invoice->email_id, 0, 14);
                $proforma_invoice->email_id = str_replace(',', ', ', $proforma_invoice->email_id);
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "'data-toggle='tooltip' data-placement='bottom' title='" . $proforma_invoice->email_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $email_id . "</a>";
                $party_type = '';
                if ($proforma_invoice->party_type_1 == 5) {
                    $party_type = 'Export';
                } elseif ($proforma_invoice->party_type_1 == 6) {
                    $party_type = 'Domestic';
                }
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $party_type . "</a>";
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->city . "</a>";
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->state . "</a>";
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='" . $proforma_invoice->id . "' data-proforma_invoice_item_id='" . $proforma_invoice->proforma_invoice_item_id . "' data-proforma_invoice_no='" . $proforma_invoice->proforma_invoice_no . "'>" . $proforma_invoice->country . "</a>";
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //echo $this->db->last_query();
        echo json_encode($output);
    }

    function production_item_detail($production_id = '') {
        if ($this->app_model->have_access_role(START_PRODUCTION_MODULE_ID, "view")) {
            $data = array();
            if (isset($production_id) && !empty($production_id)) {
                $production_data = $this->crud->get_row_by_id('production', array('production_id' => $production_id));
                $production_data = $production_data[0];
                $data['production_data'] = $production_data;
                
                $data['item_name'] = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $production_data->item_id));
                $data['item_code'] = $this->crud->get_column_value_by_id('items', 'item_code1', array('id' => $production_data->item_id));
                
                if (!empty($production_data->sales_order_id) && $production_data->sales_order_id != 0) {
                    $data['production_for'] = $this->crud->get_id_by_val('sales_order', 'sales_order_no', 'id', $production_data->sales_order_id);
                } else if (!empty($production_data->proforma_invoice_id) && $production_data->proforma_invoice_id != 0) {
                    $data['production_for'] = $this->crud->get_id_by_val('proforma_invoices', 'proforma_invoice_no', 'id', $production_data->proforma_invoice_id);
                }

                $where = array('production_id' => $production_id);
                
                $data['production_item_expenses'] = $this->crud->get_row_by_id('production_item_expenses', $where);
				$data['production_projects'] = $this->crud->get_row_by_id('production_projects', $where);
                
                $project_lineitems = array();
                $production_projects = $this->crud->get_row_by_id('production_projects', $where);
                foreach($production_projects as $production_project){
                    $production_project->production_id = $production_project->bom_project_id;
                    $production_project->project_id = $production_project->project_id;
                    $production_project->project_name = $this->crud->get_column_value_by_id('purchase_project', 'project_name', array('project_id' => $production_project->project_id));
                    $production_project->project_qty = $production_project->project_qty;
					$project_lineitems[] = $production_project;
				}
				$data['production_projects'] = $project_lineitems;
                
                $lineitems = array();
                $production_item_details = $this->crud->get_row_by_id('production_item_details', $where);
                foreach($production_item_details as $production_item_detail){
                    $production_item_detail->lineitem_id = $production_item_detail->id;
                    $production_item_detail->item_id = $production_item_detail->purchase_item_id;
                    $production_item_detail->item_code = $this->crud->get_column_value_by_id('purchase_items', 'item_code', array('id' => $production_item_detail->purchase_item_id));
                    $production_item_detail->item_name = $this->crud->get_column_value_by_id('purchase_items', 'item_name', array('id' => $production_item_detail->purchase_item_id));
                    $production_item_detail->effective_stock = $this->crud->get_column_value_by_id('purchase_items', 'effective_stock', array('id' => $production_item_detail->purchase_item_id));
                    if($production_item_detail->quantity > $production_item_detail->effective_stock){
                        $production_item_detail->inout_stock = '2';
                    } else {
                        $production_item_detail->inout_stock = '1';
                    }
                    $production_item_detail->project_name = (!empty($production_item_detail->project_id)) ? $this->crud->get_column_value_by_id('purchase_project', 'project_name', array('project_id' => $production_item_detail->project_id)) : '';
                    if(empty($production_item_detail->project_name)){
                        $production_item_detail->item_group = $this->crud->get_column_value_by_id('items', 'item_group', array('id' => $production_data->sales_item_id));
                    }
                    $production_item_detail->set_qty = $production_item_detail->basic_qty;
                    $production_item_detail->uom_id = $production_item_detail->uom_id;
                    $production_item_detail->purchase_project_qty = $production_item_detail->project_qty;
                    $production_item_detail->loss = isset($production_item_detail->loss) && !empty($production_item_detail->loss) ? $production_item_detail->loss : '';
                    $production_item_detail->job_expense = isset($production_item_detail->job_expense) && !empty($production_item_detail->job_expense) ? $production_item_detail->job_expense : '';
                    if(empty($production_item_detail->uom_id)){
                        if(!empty($production_item_detail->project_id)){
                            $production_item_detail->uom_id = $this->crud->get_column_value_by_id('purchase_project_items', 'uom_id', array('project_id' => $production_item_detail->project_id, 'item_id' => $production_item_detail->item_id));
                        } else {
                            $production_item_detail->uom_id = $this->crud->get_column_value_by_id('purchase_items', 'uom', array('id' => $production_item_detail->item_id));
                        }
                    }    
                    $production_item_detail->uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $production_item_detail->uom_id));
					$lineitems[] = json_encode($production_item_detail);
				}
				$data['production_item_details'] = implode(',', $lineitems);
            }

//            echo '<pre>'; print_r($data); exit;
            set_page('sales/production_item_detail', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function start_production() {
        $data = array();
        $post_data = $this->input->post();
        $line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
        
        $production['sales_order_id'] = $post_data['ps_sales_order_id'];
        $production['proforma_invoice_id'] = $post_data['ps_proforma_invoice_id'];
        $production['bom_id'] = isset($post_data['bom_id']) && !empty($post_data['bom_id']) ? $post_data['bom_id'] : NULL;
        if ($post_data['ps_sales_order_id'] == 0) {
            $production['item_id'] = $this->crud->get_id_by_val('proforma_invoice_items', 'item_id', 'id', $post_data['ps_item_id']);
        } else {
            $production['item_id'] = $this->crud->get_id_by_val('sales_order_items', 'item_id', 'id', $post_data['ps_item_id']);
        }
        $production['created_at'] = date('Y-m-d H:i:s');
        $production['created_by'] = $this->staff_id;
        $production['updated_by'] = $this->staff_id;
        $production['updated_at'] = $this->now_time;
        $result = $this->db->insert('production', $production);
        $production_id = $this->db->insert_id();
        if ($result) {
            
            if(isset($post_data['bom_item_expenses']['item_expense'][0]) && !empty($post_data['bom_item_expenses']['item_expense'][0])){
                    foreach($post_data['bom_item_expenses']['item_expense'] as $kay => $item_expense){
                        $add_project = array();
                        $add_project['production_id'] = $production_id;
                        $add_project['item_expense'] = $item_expense;
                        $add_project['expense'] = $post_data['bom_item_expenses']['expense'][$kay];
                        $add_project['created_by'] = $this->staff_id;
                        $add_project['created_at'] = $this->now_time;
                        $add_project['updated_by'] = $this->staff_id;
                        $add_project['updated_at'] = $this->now_time;
                        $this->crud->insert('production_item_expenses',$add_project);
                    }
                }
                
                if(isset($post_data['bom_projets']['project_id'][0]) && !empty($post_data['bom_projets']['project_id'][0])){
                    foreach($post_data['bom_projets']['project_id'] as $key => $project_line_item){
                        $production_projects = array();
                        $production_projects['production_id'] = $production_id;
                        $production_projects['project_id'] = $project_line_item;
                        $production_projects['project_qty'] = $post_data['bom_projets']['project_qty'][$key];
                        $production_projects['created_by'] = $this->staff_id;
                        $production_projects['created_at'] = $this->now_time;
                        $production_projects['updated_by'] = $this->staff_id;
                        $production_projects['updated_at'] = $this->now_time;
                        $this->crud->insert('production_projects',$production_projects);
                    }
                }
                
                if(isset($line_items_data[0]) && !empty($line_items_data[0])){
                    foreach($line_items_data[0] as $lineitem){
                        $add_lineitem = array();
                        $add_lineitem['production_id'] = $production_id;
                        $add_lineitem['purchase_item_id'] = $lineitem->item_id;
                        if(isset($lineitem->project_id) && !empty($lineitem->project_id)){
                            $add_lineitem['project_id'] = $lineitem->project_id;
                        } else {
                            $add_lineitem['project_id'] = NULL;
                        }
                        $add_lineitem['basic_qty'] = isset($lineitem->set_qty) && !empty($lineitem->set_qty) ? $lineitem->set_qty : 0;
                        if(isset($lineitem->purchase_project_qty) && !empty($lineitem->purchase_project_qty)){
                            $add_lineitem['project_qty'] = $lineitem->purchase_project_qty;
                        } else {
                            $add_lineitem['project_qty'] = NULL;
                        }
                        $add_lineitem['uom_id'] = $lineitem->uom_id;
                        $add_lineitem['loss'] = isset($lineitem->loss) && !empty($lineitem->loss) ? $lineitem->loss : NULL;
                        $add_lineitem['job_expense'] = isset($lineitem->job_expense) && !empty($lineitem->job_expense) ? $lineitem->job_expense : NULL;
                        $add_lineitem['quantity'] = isset($lineitem->quantity) && !empty($lineitem->quantity) ? $lineitem->quantity : 0;
                        $add_lineitem['created_by'] = $this->staff_id;
                        $add_lineitem['created_at'] = $this->now_time;
                        $add_lineitem['updated_by'] = $this->staff_id;
                        $add_lineitem['updated_at'] = $this->now_time;
                        $this->crud->insert('production_item_details',$add_lineitem);
                    }
                }

            /* --- Start Update Sales Order to Production Started ----------- */
            if (!empty($production['sales_order_id'])) {
                $sales_order_item_data = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $production['sales_order_id'], 'item_id' => $production['item_id']));
//                echo $this->db->last_query(); exit;
//                echo '<pre>'; print_r($sales_order_item_data); exit;
                foreach ($sales_order_item_data as $sales_order_item) {
                    if ($sales_order_item->quantity == $sales_order_item->pro_started_qty) {
                        continue;
                    }
                    if($sales_order_item->quantity > $sales_order_item->dispatched_qty){
                        $this->applib->update_purchase_stock_from_production_by_sales_item_id($production_id, '+');
//                        echo $this->db->last_query(); exit;
                    }
                    $sales_order_pro_started_qty = $sales_order_item->pro_started_qty + 1;
                    $this->crud->update('sales_order_items', array("pro_started_qty" => $sales_order_pro_started_qty), array('id' => $sales_order_item->id));
                    break;
                }
            }
            /* --- End Update Sales Order to Production Started ----------- */

            /* --- Start Update Proforma Invoice to Production Started ----------- */
            if (!empty($production['proforma_invoice_id'])) {
                $proforma_invoice_item_data = $this->crud->get_row_by_id('proforma_invoice_items', array('proforma_invoice_id' => $production['proforma_invoice_id'], 'item_id' => $production['item_id']));
                foreach ($proforma_invoice_item_data as $proforma_invoice_item) {
                    if ($proforma_invoice_item->quantity == $proforma_invoice_item->pro_started_qty) {
                        continue;
                    }
                    if($proforma_invoice_item->quantity > $proforma_invoice_item->dispatched_qty){
                       $this->applib->update_purchase_stock_from_production_by_sales_item_id($production_id, '+');
                    }
                    $proforma_invoice_pro_started_qty = $proforma_invoice_item->pro_started_qty + 1;
                    $this->crud->update('proforma_invoice_items', array("pro_started_qty" => $proforma_invoice_pro_started_qty), array('id' => $proforma_invoice_item->id));
                    break;
                }
            }
            /* --- End Update Proforma Invoice to Production Started ----------- */

            $return['success'] = "Added";
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Production Started...');
        }
        print json_encode($return);
        exit;
    }

    function get_item_bom_detail($sales_order_item_id = '', $proforma_invoice_item_id = '') {
//        echo '<pre>'; print_r($sales_order_item_id);
//        echo '<pre>'; print_r($proforma_invoice_item_id); exit;
        if (!empty($sales_order_item_id) && $sales_order_item_id != 0) {
            $item_id = $this->crud->get_id_by_val('sales_order_items', 'item_id', 'id', $sales_order_item_id);
        } else if (!empty($proforma_invoice_item_id) && $proforma_invoice_item_id != 0) {
            $item_id = $this->crud->get_id_by_val('proforma_invoice_items', 'item_id', 'id', $proforma_invoice_item_id);
        }
        $data = array();
        $data['item_name'] = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $item_id));
        $data['item_code'] = $this->crud->get_column_value_by_id('items', 'item_code1', array('id' => $item_id));
        $data['item_id'] = $item_id;

        $bom_data = $this->applib->get_max_effective_date_bom_id_by_item_id($item_id);
//        echo '<pre>'; print_r($bom_data); exit;
        if(!empty($bom_data)){
            $bom_id = $bom_data->bom_id;
            $bom_data->project_item_group = $this->crud->get_column_value_by_id('items', 'item_group', array('id' => $bom_data->sales_item_id));
            $bom_data->item_code = $this->crud->get_column_value_by_id('items', 'item_code1', array('id' => $bom_data->sales_item_id));
            $bom_data->effective_date = substr($bom_data->effective_date, 8, 2) .'-'. substr($bom_data->effective_date, 5, 2) .'-'. substr($bom_data->effective_date, 0, 4);
            $bom_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $bom_data->created_by));
            $bom_data->created_at = substr($bom_data->created_at, 8, 2) .'-'. substr($bom_data->created_at, 5, 2) .'-'. substr($bom_data->created_at, 0, 4);
            $bom_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $bom_data->updated_by));
            $bom_data->updated_at = substr($bom_data->updated_at, 8, 2) .'-'. substr($bom_data->updated_at, 5, 2) .'-'. substr($bom_data->updated_at, 0, 4);
            $data['bom_data'] = $bom_data;

            $data['bom_id'] = $bom_data->bom_id;
            $where = array('bom_id' => $bom_id);
            $data['expense'] = $this->crud->get_row_by_id('bom_item_expenses', $where);

            $project_lineitems = array();
            $bom_projects = $this->crud->get_row_by_id('bom_projects', $where);
            foreach($bom_projects as $bom_project){
                $bom_project->bom_project_id = $bom_project->bom_project_id;
                $bom_project->project_id = $bom_project->project_id;
                $bom_project->project_name = $this->crud->get_column_value_by_id('purchase_project', 'project_name', array('project_id' => $bom_project->project_id));
                $bom_project->project_qty = $bom_project->project_qty;
                $project_lineitems[] = $bom_project;
            }
            $data['projects'] = $project_lineitems;

            $lineitems = array();
            $bom_item_details = $this->crud->get_row_by_id('bom_item_details', $where);
            foreach($bom_item_details as $bom_item_detail){
                $bom_item_detail->lineitem_id = $bom_item_detail->id;

                $bom_purchase_item = $this->crud->get_data_row_by_id('purchase_items', array('id' => $bom_item_detail->item_id));
                $bom_item_detail->item_code = $bom_purchase_item->item_code;
                $bom_item_detail->item_name = $bom_purchase_item->item_name;
                $bom_item_detail->effective_stock = $bom_purchase_item->effective_stock;

                $bom_item_detail->project_name = (!empty($bom_item_detail->project_id)) ? $this->crud->get_column_value_by_id('purchase_project', 'project_name', array('project_id' => $bom_item_detail->project_id)) : '';
                if(empty($bom_item_detail->project_name)){
                    $bom_item_detail->item_group = $this->crud->get_column_value_by_id('items', 'item_group', array('id' => $bom_data->sales_item_id));
                }
                $bom_item_detail->set_qty = $bom_item_detail->basic_qty;
                $bom_item_detail->uom_id = $bom_item_detail->uom_id;
                $bom_item_detail->purchase_project_qty = $bom_item_detail->project_qty;
                $bom_item_detail->loss = isset($bom_item_detail->loss) && !empty($bom_item_detail->loss) ? $bom_item_detail->loss : '';
                $bom_item_detail->job_expense = isset($bom_item_detail->job_expense) && !empty($bom_item_detail->job_expense) ? $bom_item_detail->job_expense : '';
                if(empty($bom_item_detail->uom_id)){
                    if(!empty($bom_item_detail->project_id)){
                        $bom_item_detail->uom_id = $this->crud->get_column_value_by_id('purchase_project_items', 'uom_id', array('project_id' => $bom_item_detail->project_id, 'item_id' => $bom_item_detail->item_id));
                    } else {
                        $bom_item_detail->uom_id = $this->crud->get_column_value_by_id('purchase_items', 'uom', array('id' => $bom_item_detail->item_id));
                    }
                }
                $bom_item_detail->uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $bom_item_detail->uom_id));
                $lineitems[] = $bom_item_detail;
            }
            $data['purchase_items'] = $lineitems;
        } else {
            $data['error_exist'] = 'bom_not_exist';
        }
        
//        echo '<pre>'; print_r($data); exit;
        print json_encode($data);
        exit;
    }

    function production_start_list() {
        if ($this->app_model->have_access_role(START_PRODUCTION_MODULE_ID, "view")) {
            set_page('sales/production_start_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function production_list_datatable() {

        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
        $config['table'] = 'production p';
        $config['select'] = 'p.*,so.sales_order_no,pi.proforma_invoice_no,i.item_name,i.item_code1';
        $config['column_order'] = array(null, 'p.created_at', 'so.sales_order_no', 'pi.proforma_invoice_no', 'i.item_name', 'i.item_code1', 'p.pro_status');
        $config['column_search'] = array('p.created_at', 'so.sales_order_no', 'pi.proforma_invoice_no', 'i.item_name', 'i.item_code1', 'p.pro_status');

        $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = p.sales_order_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = p.proforma_invoice_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'items i', 'join_by' => 'i.id = p.item_id', 'join_type' => 'left');
        $config['order'] = array('p.production_id' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
//        echo '<pre>'; print_r($list); exit;
        foreach ($list as $production) {
            $row = array();
            $action = '';
            if ($production->pro_status == 1 ) {
                $action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('production_start/delete_production/' . $production->production_id) . '" title="Click to Delete Started Production" ><i class="fa fa-trash"></i></a> ';
                $action .= '<a href="javascript:void(0);" class="finish_button btn-primary btn-xs" data-href="' . base_url('production_start/finish_production/' . $production->production_id) . '" title="Click to Finish Production" ><i class="fa fa-thumbs-up"></i></a> ';
            }
            $row[] = $action;
            $row[] = '<a href="' . base_url('production_start/production_item_detail/' . $production->production_id . '?view') . '" >' . date('d-m-Y', strtotime($production->created_at)) . '</a>';
            $row[] = '<a href="' . base_url('production_start/production_item_detail/' . $production->production_id . '?view') . '" >' . $production->sales_order_no . $production->proforma_invoice_no . '</a>';
            $row[] = '<a href="' . base_url('production_start/production_item_detail/' . $production->production_id . '?view') . '" >' . $production->item_name . '</a>';
            $row[] = '<a href="' . base_url('production_start/production_item_detail/' . $production->production_id . '?view') . '" >' . $production->item_code1 . '</a>';
            if ($production->pro_status == "1") {
                $production->pro_status = 'Production';
            } else {
                $production->pro_status = 'Finished';
            }
            $row[] = '<a href="' . base_url('production_start/production_item_detail/' . $production->production_id . '?view') . '" >' . $production->pro_status . '</a>';
            $finished_at = '';
            if(!empty($production->finished_at)){
                $finished_at = date('d-m-Y', strtotime($production->finished_at));
            }
            $row[] = '<a href="' . base_url('production_start/production_item_detail/' . $production->production_id . '?view') . '" >' . $finished_at . '</a>';
            $data[] = $row;
        }
//                echo '<pre>'; print_r($data); exit;
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function delete_production($production_id) {
        $role_delete = $this->app_model->have_access_role(START_PRODUCTION_MODULE_ID, "view");
		if ($role_delete) {
            $production_data = $this->crud->get_row_by_id('production', array('production_id' => $production_id));
            $production_data = $production_data[0];
            
            /*--- Start Update Sales Order to Pending Production Started -----------*/
            if(!empty($production_data->sales_order_id) && $production_data->sales_order_id != 0){
                $sales_order_item_data = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $production_data->sales_order_id, 'item_id' => $production_data->item_id));
//                echo '<pre>'; print_r($sales_order_item_data); exit;
                foreach ($sales_order_item_data as $sales_order_item){
                    if($sales_order_item->pro_started_qty == 0){
                        continue;
                    }
                    if($sales_order_item->quantity > $sales_order_item->dispatched_qty){
                       $this->applib->update_purchase_stock_from_production_by_sales_item_id($production_id, '-');
                    }
                    $sales_order_pro_started_qty = $sales_order_item->pro_started_qty - 1;
                    $this->crud->update('sales_order_items', array("pro_started_qty" => $sales_order_pro_started_qty), array('id' => $sales_order_item->id));
                    break;
                }
            }
            /*--- End Update Sales Order to Production Started -----------*/

            /*--- Start Update Proforma Invoice to Production Started -----------*/
            if(!empty($production_data->proforma_invoice_id) && $production_data->proforma_invoice_id != 0){
                $proforma_invoice_item_data = $this->crud->get_row_by_id('proforma_invoice_items', array('proforma_invoice_id' => $production_data->proforma_invoice_id, 'item_id' => $production_data->item_id));
                foreach ($proforma_invoice_item_data as $proforma_invoice_item){
                    if($proforma_invoice_item->pro_started_qty == 0){
                        continue;
                    }
                    if($proforma_invoice_item->quantity > $proforma_invoice_item->dispatched_qty){
                       $this->applib->update_purchase_stock_from_production_by_sales_item_id($production_id, '-');
                    }
                    $proforma_invoice_pro_started_qty = $proforma_invoice_item->pro_started_qty - 1;
                    $this->crud->update('proforma_invoice_items', array("pro_started_qty" => $proforma_invoice_pro_started_qty), array('id' => $proforma_invoice_item->id));
                    break;
                }
            }
            /*--- End Update Proforma Invoice to Production Started -----------*/
            
            $this->crud->delete('production_item_details', array('production_id' => $production_id));
            $this->crud->delete('production_item_expenses', array('production_id' => $production_id));
            $this->crud->delete('production_projects', array('production_id' => $production_id));
            $result = $this->crud->delete('production', array('production_id' => $production_id));
            $return = array();
            $return['success'] = "Deleted";
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Started Production deleted');
            print json_encode($return);
            exit;
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
    }

    function finish_production($production_id) {
        $role_delete = $this->app_model->have_access_role(START_PRODUCTION_MODULE_ID, "view");
		if ($role_delete) {
            $production_data = $this->crud->get_row_by_id('production', array('production_id' => $production_id));
            $production_data = $production_data[0];
//            print_r($production_data); exit;
            
            /*--- Start Update Sales Order to Pending Production Started -----------*/
            if(!empty($production_data->sales_order_id) && $production_data->sales_order_id != 0){
                $sales_order_item_data = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $production_data->sales_order_id, 'item_id' => $production_data->item_id));
                foreach ($sales_order_item_data as $sales_order_item){
                    if($sales_order_item->pro_started_qty > $sales_order_item->pro_ended_qty){
                        $this->applib->update_purchase_stock_on_production_finish_by_sales_item_id($production_id, '-');
                    }
                    $sales_order_pro_ended_qty = $sales_order_item->pro_ended_qty + 1;
                    $this->crud->update('sales_order_items', array("pro_ended_qty" => $sales_order_pro_ended_qty), array('id' => $sales_order_item->id));
                    break;
                }
            }
            /*--- End Update Sales Order to Production Started -----------*/

            /*--- Start Update Proforma Invoice to Production Started -----------*/
            if(!empty($production_data->proforma_invoice_id) && $production_data->proforma_invoice_id != 0){
                $proforma_invoice_item_data = $this->crud->get_row_by_id('proforma_invoice_items', array('proforma_invoice_id' => $production_data->proforma_invoice_id, 'item_id' => $production_data->item_id));
                foreach ($proforma_invoice_item_data as $proforma_invoice_item){
                    if($proforma_invoice_item_data->pro_started_qty > $proforma_invoice_item_data->pro_ended_qty){
                        $this->applib->update_purchase_stock_on_production_finish_by_sales_item_id($production_id, '-');
                    }
                    $proforma_invoice_pro_ended_qty = $proforma_invoice_item->pro_ended_qty + 1;
                    $this->crud->update('proforma_invoice_items', array("pro_ended_qty" => $proforma_invoice_pro_ended_qty), array('id' => $proforma_invoice_item->id));
                    break;
                }
            }
            /*--- End Update Proforma Invoice to Production Started -----------*/
            
            $result = $this->crud->update('production', array('pro_status' => 2, 'finished_at' => date('Y-m-d H:i:s'), 'finished_by' => $this->staff_id), array('production_id' => $production_id));
            
            $return = array();
            $return['success'] = "Finished";
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Production Finished');
            print json_encode($return);
            exit;
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
//			redirect("/");
		}
    }
    
    function get_effective_amt(){
        $data = array();
        $item_id = $_POST['purchase_item_id'];
        if(!empty($item_id)){
            $data['effective_stock'] = $this->crud->get_column_value_by_id('purchase_items', 'effective_stock', array('id' => $item_id));
        }
        print json_encode($data);
        exit;
    }
    
    public function ajax_load_purchase_item_details(){
        if (isset($_POST['purchase_item_id']) && !empty($_POST['purchase_item_id'])) {
			$return = $this->crud->load_purchase_item_details_where($_POST['purchase_item_id']);
			print json_encode($return);
			exit;
		}
	}
}
