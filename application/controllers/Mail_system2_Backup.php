<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
/**
 * Class Mailbox_system1
 * &@property Datatables $datatable
 */
class Mail_system2 extends CI_Controller
{
    private $email_address = SMTP_EMAIL_ADDRESS;
    private $email_password = SMTP_EMAIL_PASSWORD;
    private $email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->email_address = trim($this->session->userdata('is_logged_in')['mailbox_email']);
        $this->email_password = trim($this->session->userdata('is_logged_in')['mailbox_password']);
        $this->load->library('user_agent');
    }

    function check_internet_conn()
    {
        return checkdnsrr('php.net') ? true : false;
    }

    function inbox()
    {
        $mails_data = $this->fetch_mails();
        set_page('mail_system2/mailbox', array('mails_data' => $mails_data));
    }

    function drafts()
    {
        $mails_data = $this->fetch_mails('Drafts');
        set_page('mail_system2/mailbox', array('mails_data' => $mails_data));
    }

    function getServerFolderName($folder_name)
    {
        $default_folder_name = array(
            'All Mail',
            'Drafts',
            'Important',
            'Sent Mail',
            'Spam',
            'Starred',
            'Trash',
        );
        if (strtolower($folder_name) == "inbox") {
            return "INBOX";
        } elseif (in_array(ucfirst($folder_name), $default_folder_name)) {
            return "[Gmail]/" . ucfirst($folder_name);
        } else {
            $mailbox_label_array = explode('/', $folder_name);
            if (count($mailbox_label_array) > 1) {
                $folder_name = '';
                foreach ($mailbox_label_array as $key => $item) {
                    if ($key > 0) {
                        $folder_name .= "/";
                    }
                    $folder_name .= $item;
                }
            }
            return $folder_name;
        }
    }

    function read()
    {
        $folder_name = $_POST['folder_name'];
        $message_no = $_POST['message_no'];

        if ($this->input->is_ajax_request()) {
            $mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
            $mail_view = $this->load->view('mail_system2/read_email', array('mail_detail' => $mail_detail, 'message_no' => $message_no, 'curr_folder_name' => $folder_name), true);
            $no_unread_mails = 0;
            $unread_mails = '';
            echo json_encode(array('email_detail' => $mail_view, 'no_unread_mails' => $no_unread_mails, 'unread_mails' => $unread_mails));
            exit();
        } else {
            $mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
            $mails_data = $this->getMailbox($folder_name);
            set_page('mail_system2/mailbox', array('mail_detail' => $mail_detail, 'mails_data' => $mails_data, 'message_no' => $message_no, 'curr_folder_name' => $folder_name));
        }
    }

    function compose_email($action = "compose")
    {
        if (!empty($_POST['to'])) {
            $attach = $_POST['attachments'];
            if (isset($_POST['mail_id'])) {
                $this->db->select('attachments');
                $this->db->from('outbox');
                $this->db->where('mail_id', $_POST['mail_id']);
                $this->db->limit(1);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $attachments = $query->row()->attachments;
                    if ($attachments != null) {
                        $attachments = unserialize($attachments);
                        foreach ($attachments as $attachment_row) {
                            $attach[] = $attachment_row['url'];
                        }
                    }
                }
            }

            if ($this->send_mail($_POST['to'], $_POST['subject'], $_POST['message'], $attach)) {
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Mail sent successfully!');
            } else {
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'Wrong please try again.');
            }
            redirect(base_url() . "mail_system2/drafts/");
        } else {
            $settings = $this->load_settings();
            if ($action != "compose") {
                $folder_name = $_POST['folder_name'];
                $message_no = $_POST['message_no'];
                $mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
                $mails_data = $this->getMailbox($folder_name);
                /*print_r($mails_data);die;*/
                set_page('mail_system2/mailbox', array('mail_detail' => $mail_detail, 'settings' => $settings, 'mails_data' => $mails_data, 'curr_folder_name' => $folder_name, 'action' => $action));
            } else {
                $mails_data = $this->getMailbox();
                set_page('mail_system2/mailbox', array('mails_data' => $mails_data, 'settings' => $settings, 'action' => $action));
            }
        }
    }

    function getMailbox($folder_name = "INBOX")
    {
        $folder_name = $this->getServerFolderName($folder_name);
        $mails_data = $this->fetch_mails($folder_name);
        return $mails_data;
    }

    function fetch_mail_detail($mailbox_name, $message_no)
    {
        $mailbox_name = $this->getServerFolderName($mailbox_name);
        $mail_data = array();
        $this->db->select('*');
        $this->db->from('inbox');
        $this->db->where('folder_name', $this->email_server . $mailbox_name);
        $this->db->where('mail_no', $message_no);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $mail_data['mail_id'] = $query->row()->mail_id;
            $mail_data['message_no'] = $query->row()->mail_no;
            if ($query->row()->from_name != $this->email_address) {
                $mail_data['username'] = $query->row()->from_name;
                $mail_data['email'] = $query->row()->from_address;
            } else {
                $mail_data['username'] = $query->row()->to_name;
                $mail_data['email'] = $query->row()->to_address;
            }
            $mail_data['subject'] = $query->row()->subject;
            $mail_data['body'] = $query->row()->body;
            if ($query->row()->attachments != null) {
                $attachments = unserialize($query->row()->attachments);
                $mail_data['attachments'] = $attachments;
            } else {
                $mail_data['attachments'] = null;
            }
            $mail_data['received_at'] = $this->timeAgo($query->row()->received_at);
            $mail_data['cc'] = $query->row()->cc;
            $mail_data['bcc'] = $query->row()->bcc;
        }
        return $mail_data;
    }

    function send_mail($to, $subject, $message, $attach = null)
    {
        if ($this->check_internet_conn()) {
            /*$this->load->library('email');*/
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => "$this->email_address",
                'smtp_pass' => "$this->email_password",
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('JayKhodiyar', "$this->email_address");
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            if ($attach != null && $attach != '') {
                if (is_array($attach) && count($attach) > 0) {
                    foreach ($attach as $attach_row) {
                        if ($attach_row != '') {
                            $this->email->attach($attach_row);
                        }
                    }
                }
            }
            if ($this->email->send()) {
                if ($attach != null && $attach != '') {
                    if (is_array($attach)) {
                        foreach ($attach as $attach_row) {
                            if (file_exists($attach_row)) {
                                unlink($attach_row);
                            }
                        }
                    }
                }
                return true;
            } else {
                if ($attach != null && $attach != '') {
                    if (is_array($attach)) {
                        foreach ($attach as $attach_row) {
                            if (file_exists($attach_row)) {
                                unlink($attach_row);
                            }
                        }
                    }
                }
                /*echo $this->email->print_debugger();*/
                echo $this->email->print_debugger();
                die;
                return false;
            }
        } else {
            $attachments = array();
            if (is_array($attach) && count($attach) > 0) {
                foreach ($attach as $attach_row) {
                    if ($attach_row != '') {
                        $attachments[] = array(
                            'filename' => basename($attach_row, PATHINFO_FILENAME),
                            'url' => $attach_row,
                            'subtype' => pathinfo($attach_row, PATHINFO_EXTENSION),
                        );
                    }
                }
            }
            /*echo "<pre>";print_r($attachments);die;*/
            $email_data = array(
                'from_address' => $this->email_address,
                'from_name' => $this->email_address,
                'to_address' => $to,
                'to_name' => $to,
                'subject' => $subject,
                'body' => $message,
                'attachments' => count($attachments) > 0 ? serialize($attachments) : null,
                'is_attachment' => count($attachments) > 0 ? 1 : 0,
                'mail_no' => 1 + $this->get_max_mail_no_in_folder('Drafts'),
                'received_at' => date('Y-m-d H:i:s'),
                'folder_label' => "Drafts",
                'folder_name' => $this->email_server . "[Gmail]/Drafts",
                'is_sent' => 0,
            );
            $this->db->insert('inbox', $email_data);
            return $this->db->insert_id() > 0 ? true : false;

        }
    }

    /**
     * @param $folder_label
     * @return mixed
     */
    function get_max_mail_no_in_folder($folder_label)
    {
        $this->db->select('MAX(mail_no) as mail_no');
        $this->db->from('inbox');
        $this->db->where('LOWER(folder_label)', strtolower($folder_label));
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row()->mail_no : 0;
    }

    function limit_words($string, $word_limit = 30)
    {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit));
    }

    function limit_character($string, $character_limit = 30)
    {
        if (strlen($string) > $character_limit) {
            return substr($string, 0, $character_limit) . '...';
        } else {
            return $string;
        }
    }

    function fetch_mails($mailbox_name = 'INBOX')
    {
        $mails_data = array();
        $mailbox_name = $this->getServerFolderName($mailbox_name);
        $this->db->select('i.mail_id,i.folder_name,i.mail_no,i.from_name,i.to_name,i.subject,i.is_starred,i.received_at,i.is_attachment,i.is_unread');
        $this->db->from('inbox i');
        if ($mailbox_name == '[Gmail]/Starred') {
            $this->db->where('i.is_starred', 1);
        } else {
            $this->db->where('i.folder_name', $this->email_server . $mailbox_name);
        }
        $this->db->order_by('i.received_at', 'DESC');
        $query = $this->db->get();
        $total_mails = $query->num_rows();
        $mails_data['count_unseen_mails'] = 0;
        $mails_data['inbox_unseen_mails'] = 0;//$this->count_inbox_unread_mail();
        $mails_data['total_mails'] = $total_mails;
        $mails_data['mails_range'] = $total_mails > 0 ? "1-$total_mails" : "0-$total_mails";
        $mails_data['mailboxes'] = $this->get_mailboxes();
        if ($total_mails > 0) {
            foreach ($query->result() as $key => $mail_row) {
                if ($mail_row->is_unread == 1) {
                    $mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
                }
                $mails_data['mails'][$key]['is_unread'] = $mail_row->is_unread;
                $mails_data['mails'][$key]['attachment_status'] = $mail_row->is_attachment;
                $mails_data['mails'][$key]['received_at'] = $this->timeAgo($mail_row->received_at);
                $mails_data['mails'][$key]['is_starred'] = $mail_row->is_starred;
                $mails_data['mails'][$key]['subject'] = $this->limit_character($mail_row->subject, 20);
                if ($mail_row->from_name != $this->email_address) {
                    $mails_data['mails'][$key]['username'] = $mail_row->from_name;
                } else {
                    $mails_data['mails'][$key]['username'] = $mail_row->to_name;
                }
                $mails_data['mails'][$key]['mail_no'] = $mail_row->mail_no;
                $mails_data['mails'][$key]['mail_id'] = $mail_row->mail_id;
                $mails_data['mails'][$key]['folder_name'] = str_replace("{$this->email_server}", '', $mail_row->folder_name);
            }
        }
        return $mails_data;
    }


    function mails_datatable()
    {

        $config['table'] = 'inbox i';
        $config['column_order'] = array('i.mail_id', 'i.folder_name', 'i.mail_no', 'i.from_name', 'i.to_name', 'i.subject', 'i.is_starred', 'i.received_at', 'i.is_attachment', 'i.is_unread');
        $config['column_search'] = array('i.from_name', 'i.to_name', 'i.subject', 'i.body');
        $config['order'] = array('i.mail_id' => 'desc');
        $config['wheres'] = array();
        $folder_name = $this->getServerFolderName($_POST['folder_name']);
        $config['wheres'][] = array('column_name' => 'folder_name', 'column_value' => $this->email_server . $folder_name);

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        foreach ($list as $mail_row) {
            $mail['is_unread'] = $mail_row->is_unread;
            $mail['attachment_status'] = $mail_row->is_attachment;
            $mail['received_at'] = $this->timeAgo($mail_row->received_at);
            $mail['is_starred'] = $mail_row->is_starred;
            $mail['subject'] = $mail_row->subject;
            if ($mail_row->from_name != $this->email_address) {
                $mail['username'] = $mail_row->from_name;
            } else {
                $mail['username'] = $mail_row->to_name;
            }
            $mail['mail_no'] = $mail_row->mail_no;
            $mail['mail_id'] = $mail_row->mail_id;
            $mail['folder_name'] = str_replace("{$this->email_server}", '', $mail_row->folder_name);

            $row = array();
            $row[] = '';
            $row[] = $this->load->view('shared/row_mail_datatable', array('mail' => $mail), true);
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        } //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        } //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        } //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        } //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        } //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        } //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    function count_inbox_unread_mail()
    {
        $unread_emails = 0;
        if ($this->check_internet_conn()) {
            $mb = imap_open($this->email_server . "INBOX", $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
            $messageCount = imap_num_msg($mb);
            for ($MID = $messageCount; $MID >= 1; $MID--) {
                $email_headers = imap_headerinfo($mb, $MID);
                /*-------- Count Unread Mails -----------------*/
                if ($email_headers->Unseen == 'U') {
                    $unread_emails = $unread_emails + 1;
                }
            }
        }
        return $unread_emails;
    }

    function get_mailboxes($mailbox_stream = '')
    {
        $this->db->select('folder_label as mailbox_label,folder_name as mailbox_name');
        $this->db->from('mailbox_folders');
        $this->db->where('is_system_folder', 0);
        $query = $this->db->get();
        $mailbox_array = $query->result_array();
        return $mailbox_array;
    }

    function upload_attachment()
    {
        $this->load->library('upload');
        $config['upload_path'] = 'resource/uploads/attachments';
        $config['allowed_types'] = 'gif|jpg|png|txt|pdf|zip';
        $config['overwrite'] = TRUE;
        $this->upload->initialize($config);
        if ($this->upload->do_upload('upload_attachment')) {
            $file_data = $this->upload->data();
            $file_url = $file_data['file_name'];
            echo json_encode(array('success' => true, 'message' => 'Attachment successfully uploaded.', 'attachment_url' => 'resource/uploads/attachments/' . $file_url));
            exit();
        } else {
            echo json_encode(array('success' => false, 'message' => 'Wrong with upload operation!', 'error' => $this->upload->display_errors()));
            exit();
        }
    }

    /**
     * @param $mb
     * @param $message_no
     * @return mixed|string
     */
    function get_mail_body($mb, $message_no)
    {
        $body = imap_fetchbody($mb, $message_no, 1.2);
        if (!strlen($body) > 0) {
            $body = imap_fetchbody($mb, $message_no, 1);
        }
        $body = preg_replace('/^\>/m', '', $body);
        return $body;
    }

    /**
     * @param $SearchResults
     * @param $SortResults
     * @return array
     */
    function order_search($SearchResults, $SortResults)
    {
        return array_values(array_intersect($SearchResults, $SortResults));
    }

    /**
     * @param $mb
     * @param $message_no
     * @return array
     */
    function get_mail_attachments($mb, $message_no)
    {
        $attachments = array();
        $structure = imap_fetchstructure($mb, $message_no);
        if (isset($structure->parts) && count($structure->parts)) {
            for ($i = 0; $i < count($structure->parts); $i++) {
                $attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
                $attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                        }
                    }
                }
                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }
                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($mb, $message_no, $i + 1);
                    if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                    } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }
        }
        $response_data = array();
        if (count($attachments) != 0) {
            foreach ($attachments as $at) {
                if ($at['is_attachment'] == 1) {
                    $response_data[] = array(
                        'filename' => $at['name'],
                        'file' => $at['attachment'],
                        'subtype' => $at['subtype'],
                    );
                }
            }
        }
        return $response_data;
    }

    function save_unread_mails()
    {
        if ($this->check_internet_conn()) {
            $mailbox = $this->email_server . "INBOX";
            $mb = imap_open($mailbox, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!3");
            $mail_status = imap_status($mb, $this->email_server . "INBOX", SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!4");
            if ($mail_status->messages > 0) {
                $sort_results = imap_sort($mb, SORTDATE, 1);
                $search_results = imap_search($mb, 'UNSEEN');
                if (is_array($search_results)) {
                    $message_nos = $this->order_search($search_results, $sort_results);
                    foreach ($message_nos as $key => $message_no) {
                        $email_headers = imap_headerinfo($mb, $message_no);
                        $mail_data = array();
                        $mail_data['mail_no'] = $email_headers->Msgno;
                        $mail_data['uid'] = imap_uid($mb, $message_no);
                        if (isset($email_headers->from[0]->mailbox) && $email_headers->from[0]->mailbox != '') {
                            $mail_data['from_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
                            if (isset($email_headers->from[0]->personal)) {
                                $mail_data['from_name'] = $email_headers->from[0]->personal;
                            } else {
                                $mail_data['from_name'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
                            }
                        }

                        if (isset($email_headers->to[0]->mailbox) && $email_headers->to[0]->mailbox != '') {
                            $mail_data['to_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
                            if (isset($email_headers->to[0]->personal)) {
                                $mail_data['to_name'] = $email_headers->to[0]->personal;
                            } else {
                                $mail_data['to_name'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
                            }
                        }

                        if (isset($email_headers->reply_to[0]->mailbox) && $email_headers->reply_to[0]->mailbox != '') {
                            $mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
                            if (isset($email_headers->reply_to[0]->personal)) {
                                $mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
                            } else {
                                $mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
                            }
                        }

                        if (isset($email_headers->sender[0]->mailbox) && $email_headers->sender[0]->mailbox != '') {
                            $mail_data['sender_address'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
                            if (isset($email_headers->sender[0]->personal)) {
                                $mail_data['sender_name'] = $email_headers->sender[0]->personal;
                            } else {
                                $mail_data['sender_name'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
                            }
                        }

                        $mail_data['cc'] = '';
                        $mail_data['bcc'] = '';
                        $mail_data['subject'] = $email_headers->subject;
                        $mail_data['body'] = $this->get_mail_body($mb, $message_no);
                        $attachments = $this->get_mail_attachments($mb, $message_no);
                        if (count($attachments) > 0) {
                            $mail_data['is_attachment'] = 1;
                            $attachment_url = array();
                            $time = time();
                            mkdir('resource/uploads/attachments/' . $time);
                            foreach ($attachments as $attachment) {
                                $file = "resource/uploads/attachments/$time/" . $attachment['filename'];
                                file_put_contents($file, base64_decode($attachment['file']));
                                $attachment_url[] = array(
                                    'filename' => $attachment['filename'],
                                    'url' => $file,
                                    'subtype' => $attachment['subtype'],
                                );
                            }
                            $mail_data['attachments'] = serialize($attachment_url);
                        }
                        $mail_data['received_at'] = $email_headers->date;
                        $mail_data['folder_label'] = 'Inbox';
                        $mail_data['folder_name'] = $mailbox;
                        $mail_data['is_unread'] = $email_headers->Unseen == 'U' ? 1 : 0;
                        $mail_data['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
                        $mail_data['created_at'] = date('Y-m-d H:i:s');
                        $this->db->insert('inbox', $mail_data);
                    }
                }
            }
            imap_close($mb);
            echo json_encode(array('success' => true, 'message' => 'Mailbox updated successfully!'));
        } else {
            echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
        }
        exit();
    }


    public function save_unread_mails_for_all()
    {
		//$this->db->select('*');
		//$this->db->from('staff');
		//$this->db->where('mail_id', $_POST['mail_id']);
		//$this->db->limit(1);
		$count =0;
		$usercount_mail=0;
		//$prev_staffid=0;
		//$current_staff_id=0;
		$staffs = $this->db->query("SELECT * FROM staff WHERE active != '0'");
		//print_r($staffs);
		if ($staffs->num_rows() > 0) {
			foreach ($staffs->result() as $staff) {

				//echo "<pre>";
				//print_r($staff);
				//echo "</pre>";
				$massages[$count]['staff_id'] = $staff->staff_id;
				$massages[$count]['mail_count']=0;
				$usercount_mail=0;
				if ($this->check_internet_conn()) {
					$mailbox = $this->email_server . "INBOX";
					$mb = imap_open($mailbox, $staff->mailbox_email, $staff->mailbox_password) or die(imap_last_error() . "<br>Connection Faliure!3");
					$mail_status = imap_status($mb, $this->email_server . "INBOX", SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!4");
					if ($mail_status->messages > 0) {
						$sort_results = imap_sort($mb, SORTDATE, 1);
						$search_results = imap_search($mb, 'UNSEEN');
						if (is_array($search_results)) {
							$message_nos = $this->order_search($search_results, $sort_results);
							foreach ($message_nos as $key => $message_no) {
								$usercount_mail++;
								$email_headers = imap_headerinfo($mb, $message_no);
								$mail_data = array();
								$mail_data['mail_no'] = $email_headers->Msgno;
								$mail_data['uid'] = imap_uid($mb, $message_no);
								if (isset($email_headers->from[0]->mailbox) && $email_headers->from[0]->mailbox != '') {
									$mail_data['from_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
									if (isset($email_headers->from[0]->personal)) {
										$mail_data['from_name'] = $email_headers->from[0]->personal;
									} else {
										$mail_data['from_name'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
									}
								}

								if (isset($email_headers->to[0]->mailbox) && $email_headers->to[0]->mailbox != '') {
									$mail_data['to_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
									if (isset($email_headers->to[0]->personal)) {
										$mail_data['to_name'] = $email_headers->to[0]->personal;
									} else {
										$mail_data['to_name'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
									}
								}

								if (isset($email_headers->reply_to[0]->mailbox) && $email_headers->reply_to[0]->mailbox != '') {
									$mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
									if (isset($email_headers->reply_to[0]->personal)) {
										$mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
									} else {
										$mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
									}
								}

								if (isset($email_headers->sender[0]->mailbox) && $email_headers->sender[0]->mailbox != '') {
									$mail_data['sender_address'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
									if (isset($email_headers->sender[0]->personal)) {
										$mail_data['sender_name'] = $email_headers->sender[0]->personal;
									} else {
										$mail_data['sender_name'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
									}
								}

								$mail_data['cc'] = '';
								$mail_data['bcc'] = '';
								$mail_data['subject'] = $email_headers->subject;
								$mail_data['body'] = $this->get_mail_body($mb, $message_no);
								$attachments = $this->get_mail_attachments($mb, $message_no);
								if (count($attachments) > 0) {
									$mail_data['is_attachment'] = 1;
									$attachment_url = array();
									$time = time();
									mkdir('resource/uploads/attachments/' . $time);
									foreach ($attachments as $attachment) {
										$file = "resource/uploads/attachments/$time/" . $attachment['filename'];
										file_put_contents($file, base64_decode($attachment['file']));
										$attachment_url[] = array(
											'filename' => $attachment['filename'],
											'url' => $file,
											'subtype' => $attachment['subtype'],
										);
									}
									$mail_data['attachments'] = serialize($attachment_url);
								}
								$mail_data['received_at'] = $email_headers->date;
								$mail_data['folder_label'] = 'Inbox';
								$mail_data['staff_id'] = $staff->staff_id;
								$mail_data['folder_name'] = $mailbox;
								$mail_data['is_unread'] = $email_headers->Unseen == 'U' ? 1 : 0;
								$mail_data['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
								$mail_data['created_at'] = date('Y-m-d H:i:s');
								$this->db->insert('inbox', $mail_data);
							}
							$massages[$count]['mail_count']=$usercount_mail;
							$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
							$client->initialize();
							$client->emit('cron_mail_notification', ['staff_id'=> $staff->staff_id,'count_mail' => $usercount_mail]);
							$client->close();

						}
					}
					imap_close($mb);
					//echo json_encode(array('success' => true, 'message' => 'Mailbox updated successfully!'));
					$massages[$count]['success']=true;
					$massages[$count]['message']="Mailbox updated successfully!";
				} else {
					$massages[$count]['success']=false;
					$massages[$count]['message']="No internet connection available";
					//echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
				}
				$count++;
			}
		}
		echo json_encode($massages);
		exit();
    }

    /**
     * @return bool
     */
    function sent_drafts_mails()
    {
        $this->db->select('*');
        $this->db->from('inbox');
        $this->db->where('folder_name', $this->email_server . '[Gmail]/Drafts');
        $this->db->where('is_sent', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $mail_row) {
                $attach = array();
                if ($mail_row->attachments != null) {
                    $attachments = unserialize($mail_row->attachments);
                    foreach ($attachments as $attachment_row) {
                        $attach[] = $attachment_row['url'];
                    }
                }
                if ($this->send_mail($mail_row->to_address, $mail_row->subject, $mail_row->body, $attach)) {
                    $update_data = array('is_sent' => 1, 'mail_no' => 1 + $this->get_max_mail_no_in_folder('Sent mail'), 'folder_label' => 'Sent mail', 'folder_name' => $this->email_server . $this->getServerFolderName('Sent Mail'));
                    $this->db->where('mail_id', $mail_row->mail_id);
                    $this->db->update('inbox', $update_data);
                }
            }
        }
        return true;
    }

    function update_mailbox()
    {
        if ($this->check_internet_conn()) {
            $this->save_unread_mails();
            $this->sent_drafts_mails();
        } else {
            echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
        }
        exit();
    }

    function settings()
    {
        $this->db->select('setting_key,setting_value');
        $this->db->from('mailbox_settings');
        $query = $this->db->get();
        $setting_data = array();
        foreach ($query->result() as $setting) {
            $setting_data[$setting->setting_key] = $setting->setting_value;
        }
        $settings_html = $this->load->view('mail_system2/settings', $setting_data, true);
        echo json_encode(array('success' => true, 'message' => "Successfully load settings!", 'settings_html' => $settings_html));
        exit();
    }

    function save_settings()
    {
        $setting_data = $_POST;
        foreach ($setting_data as $setting_key => $setting_value) {
            $this->db->where('setting_key', $setting_key);
            $this->db->update('mailbox_settings', array('setting_value' => $setting_value));
        }
        echo json_encode(array('success' => true, 'message' => 'Settings save successfully!'));
        exit();
    }

    /**
     * @return array
     */
    function load_settings()
    {
        $this->db->select('setting_key,setting_value');
        $this->db->from('mailbox_settings');
        $query = $this->db->get();
        $setting_data = array();
        foreach ($query->result() as $setting) {
            $setting_data[$setting->setting_key] = $setting->setting_value;
        }
        return $setting_data;
    }
}
