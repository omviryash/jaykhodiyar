<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_sales extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
		$this->load->library('user_agent');
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
	}

	/* function index()
	{

	} */

	function reference($id='')
	{
		$reference = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$reference = $this->crud->get_all_with_where('reference','reference','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('reference', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($reference) ? $reference[0]->id : '';
		$return['reference'] = ($reference) ? $reference[0]->reference : '';
		if($this->applib->have_access_role(MASTER_SALES_REFERENCE_MENU_ID,"view")) {
			set_page('master_sales/reference', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_reference()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('reference',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Reference Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_reference()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('reference', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Reference Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function inquiry_stage($id="")
	{
		$inquiry_stage = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$inquiry_stage = $this->crud->get_all_with_where('inquiry_stage','inquiry_stage','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('inquiry_stage', 'inquiry_stage', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($inquiry_stage) ? $inquiry_stage[0]->id : '';
		$return['inquiry_stage'] = ($inquiry_stage) ? $inquiry_stage[0]->inquiry_stage : '';
		if($this->applib->have_access_role(MASTER_SALES_INQUIRY_STAGE_MENU_ID,"view")) {
			set_page('master_sales/inquiry_stage', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_inquiry_stage()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('inquiry_stage',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Inquiry stage Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_inquiry_stage()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('inquiry_stage', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Inquiry stage Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function industry_type($id="")
	{
		$industry_type = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$industry_type = $this->crud->get_all_with_where('industry_type','industry_type','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('industry_type', 'industry_type', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($industry_type) ? $industry_type[0]->id : '';
		$return['industry_type'] = ($industry_type) ? $industry_type[0]->industry_type : '';
		if($this->applib->have_access_role(MASTER_SALES_INDUSTRY_TYPE_MENU_ID,"view")) {
			set_page('master_sales/industry_type', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_industry_type()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('industry_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Industry Type Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_industry_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('industry_type', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Industry Type Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function inquiry_status($id='')
	{
		$inquiry_status = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$inquiry_status = $this->crud->get_all_with_where('inquiry_status','inquiry_status','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('inquiry_status', 'inquiry_status', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($inquiry_status) ? $inquiry_status[0]->id : '';
		$return['inquiry_status'] = ($inquiry_status) ? $inquiry_status[0]->inquiry_status : '';
		if($this->applib->have_access_role(MASTER_SALES_INQUIRY_STATUS_MENU_ID,"view")) {
			set_page('master_sales/inquiry_status', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_inquiry_status()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('inquiry_status',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Inquiry Status Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_inquiry_status()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('inquiry_status', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Inquiry Status Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function priority($id="")
	{
		$priority = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$priority = $this->crud->get_all_with_where('priority','priority','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('priority', 'priority', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($priority) ? $priority[0]->id : '';
		$return['priority'] = ($priority) ? $priority[0]->priority : '';
		if($this->applib->have_access_role(MASTER_SALES_PRIORITY_MENU_ID,"view")) {
			set_page('master_sales/priority', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_priority()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('priority',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Priority Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_priority()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('priority', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Priority Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function lead_provider($id="")
	{
		$lead_provider = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$lead_provider = $this->crud->get_all_with_where('lead_provider','lead_provider','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('lead_provider', 'lead_provider', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($lead_provider) ? $lead_provider[0]->id : '';
		$return['lead_provider'] = ($lead_provider) ? $lead_provider[0]->lead_provider : '';
		if($this->applib->have_access_role(MASTER_SALES_REFERENCE_MENU_ID,"view")) {
			set_page('master_sales/lead_provider', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_lead_provider()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('lead_provider',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Provider Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_lead_provider()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('lead_provider', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Provider Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function uom($id="")
	{
		$uom = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$uom = $this->crud->get_all_with_where('uom','uom','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('uom', 'uom', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($uom) ? $uom[0]->id : '';
		$return['uom'] = ($uom) ? $uom[0]->uom : '';
		if($this->applib->have_access_role(MASTER_SALES_UOM_MENU_ID,"view")) {
			set_page('master_sales/uom', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_uom()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('uom',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','UOM Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_uom()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('uom', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','UOM Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function quotation_type($id="")
	{
		$quotation_type = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$quotation_type = $this->crud->get_all_with_where('quotation_type','quotation_type','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('quotation_type', 'quotation_type', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($quotation_type) ? $quotation_type[0]->id : '';
		$return['quotation_type'] = ($quotation_type) ? $quotation_type[0]->quotation_type : '';
		if($this->applib->have_access_role(MASTER_SALES_QUOTATION_TYPE_MENU_ID,"view")) {
			set_page('master_sales/quotation_type', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_quotation_type()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('quotation_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Quotation Type Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_quotation_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('quotation_type', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Quotation Type Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function sales_type($id="")
	{
		$sales_type = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$sales_type = $this->crud->get_all_with_where('sales_type','sales_type','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('sales_type', 'sales_type', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sales_type) ? $sales_type[0]->id : '';
		$return['sales_type'] = ($sales_type) ? $sales_type[0]->sales_type : '';
		if($this->applib->have_access_role(MASTER_SALES_SALES_TYPE_MENU_ID,"view")) {
			set_page('master_sales/sales_type', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_sales_type()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('sales_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sales Type Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_sales_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('sales_type', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sales Type Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function quotation_reason($id="")
	{
		$quotation_reason = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$quotation_reason = $this->crud->get_all_with_where('quotation_reason','quotation_reason','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('quotation_reason', 'quotation_reason', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($quotation_reason) ? $quotation_reason[0]->id : '';
		$return['quotation_reason'] = ($quotation_reason) ? $quotation_reason[0]->quotation_reason : '';
		if($this->applib->have_access_role(MASTER_SALES_QUOTATION_REASON_MENU_ID,"view")) {
			set_page('master_sales/quotation_reason', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_quotation_reason()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('quotation_reason',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message',' Reason Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_quotation_reason()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('quotation_reason', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message',' Reason Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function currency($id="")
	{
		
		//~ $amount=1;

		//~ $from='USD';

		//~ $to='INR';

		//~ $a = $this->currencyConverter($from,$to,$amount);
		
		//~ //$a = $this->currency_cal("USD","INR",1);
		
		//~ echo $a;		
		//~ exit();

		$currency = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$currency = $this->crud->get_all_with_where('currency','currency','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('currency', 'currency', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($currency) ? $currency[0]->id : '';
		$return['currency'] = ($currency) ? $currency[0]->currency : '';
		$return['inr_amt'] = ($currency) ? $currency[0]->inr_amt : '';
		
		//print_r($return);exit();
		if($this->applib->have_access_role(MASTER_SALES_CURRENCY_MENU_ID,"view")) {
			set_page('master_sales/currency', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}
	
	//~ function currencyConverter($from_currency, $to_currency, $amount) {
	//~ $amount    = urlencode($amount);
	//~ $from    = urlencode($from_currency);
	//~ $to        = urlencode($to_currency);
	//~ $url    = "http://www.google.com/ig/calculator?hl=en&q=$amount$from=?$to";
	//~ $ch     = @curl_init();
	//~ $timeout= 0;
	 
	//~ curl_setopt ($ch, CURLOPT_URL, $url);
	//~ curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	//~ curl_setopt ($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
	//~ curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	 
	//~ $rawdata = curl_exec($ch);
	//~ curl_close($ch);
	//~ $data = explode('"', $rawdata);
	//~ $data = explode(' ', $data['3']);
	//~ $var = $data['0'];
	//~ return round($var,3);
	//~ }

	function add_currency()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('currency',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Currency Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_currency()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('currency', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Currency Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function sales($id="")
	{
		$sales = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$sales = $this->crud->get_all_with_where('sales','sales','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('sales', 'sales', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sales) ? $sales[0]->id : '';
		$return['sales'] = ($sales) ? $sales[0]->sales : '';
		if($this->applib->have_access_role(MASTER_SALES_SALES_MENU_ID,"view")) {
			set_page('master_sales/sales', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_sales()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('sales',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sales Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_sales()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('sales', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sales Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function action($id="")
	{
		$sales_action = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$sales_action = $this->crud->get_all_with_where('sales_action','sales_action','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('sales_action', 'sales_action', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sales_action) ? $sales_action[0]->id : '';
		$return['sales_action'] = ($sales_action) ? $sales_action[0]->sales_action : '';
		if($this->applib->have_access_role(MASTER_SALES_ACTION_MENU_ID,"view")) {
			set_page('master_sales/action', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_action()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('sales_action',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Action Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_sales_action()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('sales_action', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Action Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function stage($id="")
	{
		$quotation_stage = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$quotation_stage = $this->crud->get_all_with_where('quotation_stage','quotation_stage','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('quotation_stage', 'quotation_stage', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($quotation_stage) ? $quotation_stage[0]->id : '';
		$return['quotation_stage'] = ($quotation_stage) ? $quotation_stage[0]->quotation_stage : '';
		if($this->applib->have_access_role(MASTER_SALES_QUOTATION_STAGE_MENU_ID,"view")) {
			set_page('master_sales/stage', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_stage()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('quotation_stage',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Stage Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_stage()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('quotation_stage', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Stage Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function status($id="")
	{
		$sales_status = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$sales_status = $this->crud->get_all_with_where('sales_status','sales_status','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('sales_status', 'sales_status', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sales_status) ? $sales_status[0]->id : '';
		$return['sales_status'] = ($sales_status) ? $sales_status[0]->sales_status : '';
		if($this->applib->have_access_role(MASTER_SALES_STATUS_MENU_ID,"view")) {
			set_page('master_sales/status', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_status()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('sales_status',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Status Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_status()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('sales_status', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Status Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function inquiry_confirmation($id="")
	{
		$inquiry_confirmation = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$inquiry_confirmation = $this->crud->get_all_with_where('inquiry_confirmation','inquiry_confirmation','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('inquiry_confirmation', 'inquiry_confirmation', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($inquiry_confirmation) ? $inquiry_confirmation[0]->id : '';
		$return['inquiry_confirmation'] = ($inquiry_confirmation) ? $inquiry_confirmation[0]->inquiry_confirmation : '';
		if($this->applib->have_access_role(MASTER_SALES_INQUIRY_CONFIRMATION_MENU_ID,"view")) {
			set_page('master_sales/inquiry_confirmation', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_inquiry_confirmation()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('inquiry_confirmation',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Inquiry Confirmation Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_inquiry_confirmation()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('inquiry_confirmation', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Inquiry Confirmation Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function lead_stage($id="")
	{
		$lead_stage = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$lead_stage = $this->crud->get_all_with_where('lead_stage','lead_stage','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('lead_stage', 'lead_stage', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($lead_stage) ? $lead_stage[0]->id : '';
		$return['lead_stage'] = ($lead_stage) ? $lead_stage[0]->lead_stage : '';
		if($this->applib->have_access_role(MASTER_SALES_REFERENCE_MENU_ID,"view")) {
			set_page('master_sales/lead_stage', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}


	}

	function add_lead_stage()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('lead_stage',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Stage Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_lead_stage()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('lead_stage', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Stage Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function lead_source($id="")
	{
		$lead_source = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$lead_source = $this->crud->get_all_with_where('lead_source','lead_source','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('lead_source', 'lead_source', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($lead_source) ? $lead_source[0]->id : '';
		$return['lead_source'] = ($lead_source) ? $lead_source[0]->lead_source : '';
		if($this->applib->have_access_role(MASTER_SALES_REFERENCE_MENU_ID,"view")) {
			set_page('master_sales/lead_source', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_lead_source()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('lead_source',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Source Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_lead_source()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('lead_source', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Source Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function lead_status($id="")
	{
		$lead_status = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$lead_status = $this->crud->get_all_with_where('lead_status','lead_status','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('lead_status', 'lead_status', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($lead_status) ? $lead_status[0]->id : '';
		$return['lead_status'] = ($lead_status) ? $lead_status[0]->lead_status : '';
		if($this->applib->have_access_role(MASTER_SALES_REFERENCE_MENU_ID,"view")) {
			set_page('master_sales/lead_status', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_lead_status()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('lead_status',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Status Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_lead_status()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('lead_status', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Lead Status Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Deleted Successfully');
	}

	function order_stage($id="")
	{
		$order_stage = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$order_stage = $this->crud->get_all_with_where('order_stage','order_stage','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('order_stage', 'order_stage', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($order_stage) ? $order_stage[0]->id : '';
		$return['order_stage'] = ($order_stage) ? $order_stage[0]->order_stage : '';
		if($this->applib->have_access_role(MASTER_SALES_ORDER_STAGE_MENU_ID,"view")) {
			set_page('master_sales/order_stage', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function add_order_stage()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('order_stage',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Order stage Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_order_stage()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('order_stage', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Order stage Updated Successfully');
			echo json_encode($post_data);
		}
	}
	function order_payment_terms(){
		if (!empty($_POST)) {
			$this->db->where('module', 'order_payment_terms');
			$data['detail'] = $_POST['details'];
			$data['updated_at'] = $this->now_time;
			$data['updated_by'] = $this->logged_in_id;
			$this->db->update('terms_and_conditions', $data);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Sales Order Payment Terms successfully saved!');
			redirect('master_sales/order_payment_terms');
		} else {
			$terms_detail = $this->applib->getTermsandConditions('order_payment_terms');
			if($this->applib->have_access_role(PURCHASE_ORDER_TERMS_MENU_ID,"view")) {
				set_page('master_sales/order_payment_terms', array('terms_detail' => $terms_detail));
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}

	function terms_and_conditions_of_purchase(){

	}


	function followup_category($fc_id = ''){ 
		$followup_category = array();
		if(isset($fc_id) && !empty($fc_id)){
			$where_array['fc_id'] = $fc_id;
			$followup_category = $this->crud->get_all_with_where('followup_category','fc_id','ASC',$where_array);
		}										
		$result_data = $this->crud->get_all_records('followup_category','fc_id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['fc_id'] = ($followup_category) ? $followup_category[0]->fc_id : '';
		$return['fc_name'] = ($followup_category) ? $followup_category[0]->fc_name : '';
		if($this->applib->have_access_role(MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID,"view")) {
			set_page('master_sales/followup_category', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		} 

	}

	function followup_category_datatable(){
		$config['select'] = 'fc_id, fc_name';
		$config['table'] = 'followup_category';
		$config['column_order'] = array(null, 'fc_name');
		$config['column_search'] = array('fc_name');

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID, "delete");
		foreach ($list as $followup_category_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' <a href="' . base_url('master_sales/followup_category/' . $followup_category_list->fc_id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> | ';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('master_sales/delete/' . $followup_category_list->fc_id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $followup_category_list->fc_name;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

	}

	function update_followup_category()
	{
		$post_data = $this->input->post();
		$where_array['fc_id'] = $post_data['fc_id'];
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->update('followup_category', $post_data, array('fc_id' => $post_data['fc_id']));

		if ($result) {

			$post_data['fc_id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Followup Category Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_followup_category()
	{
		$post_data = $this->input->post();
		$post_data['created_by'] = $this->staff_id;
		$post_data['created_at'] = $this->now_time;
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->insert('followup_category',$post_data);
		if($result)
		{
			$post_data['fc_id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Followup Category Added Successfully');
			echo json_encode($post_data);
		}
	}

	function purchase_order_ref($id='')
	{
		$sales_order_pref = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$sales_order_pref = $this->crud->get_all_with_where('sales_order_pref','sales_order_pref','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('sales_order_pref', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sales_order_pref) ? $sales_order_pref[0]->id : '';
		$return['sales_order_pref'] = ($sales_order_pref) ? $sales_order_pref[0]->sales_order_pref : '';
		if($this->applib->have_access_role(MASTER_SALES_PURCHASE_ORDER_REF_MENU_ID,"view")) {
			set_page('master_sales/purchase_order_ref', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_sales_order_pref()
	{
		$post_data = $this->input->post();
		$post_data['created_by'] = $this->staff_id;
		$post_data['created_at'] = $this->now_time;
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->insert('sales_order_pref',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Purchase Order Reference Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_sales_order_pref()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->update('sales_order_pref', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Purchase Order Reference Updated Successfully');
			echo json_encode($post_data);
		}
	}


	function quotation_status($id='')
	{
		$quotation_statusf = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$quotation_status = $this->crud->get_all_with_where('quotation_status','quotation_status','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('quotation_status', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($quotation_status) ? $quotation_status[0]->id : '';
		$return['quotation_status'] = ($quotation_status) ? $quotation_status[0]->quotation_status : '';
		if($this->applib->have_access_role(MASTER_SALES_QUOTATION_STATUS_MENU_ID,"view")) {
			set_page('master_sales/quotation_status', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_quotation_status()
	{
		$post_data = $this->input->post();
		$post_data['created_by'] = $this->staff_id;
		$post_data['created_at'] = $this->now_time;
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->insert('quotation_status',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Quotation Status Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_quotation_status()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->update('quotation_status', $post_data, array('id' => $post_data['id']));
		if ($result) { 
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Quotation Status Updated Successfully');
			echo json_encode($post_data);
		}
	}
        
        function sea_freight_type($id='')
	{
		$sea_freight_type = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$sea_freight_type = $this->crud->get_all_with_where('sea_freight_type','id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('sea_freight_type', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sea_freight_type) ? $sea_freight_type[0]->id : '';
		$return['sea_freight_type'] = ($sea_freight_type) ? $sea_freight_type[0]->name : '';
		if($this->applib->have_access_role(MASTER_SALES_SEA_FREIGHT_TYPE_MENU_ID,"view")) {
			set_page('master_sales/sea_freight_type', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_sea_freight_type()
	{
		$post_data = $this->input->post();
		$post_data['created_by'] = $this->staff_id;
		$post_data['created_at'] = $this->now_time;
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->insert('sea_freight_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sea Freight Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_sea_freight_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_by'] = $this->staff_id;
		$post_data['updated_at'] = $this->now_time;
		$result = $this->crud->update('sea_freight_type', $post_data, array('id' => $post_data['id']));
		if ($result) { 
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sea Freight Updated Successfully');
			echo json_encode($post_data);
		}
	}



}