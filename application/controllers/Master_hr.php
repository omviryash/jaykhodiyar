<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_hr extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
		$this->load->library('user_agent');
	}
	
	/* function index()
	{
		
	} */
	
	function day($day_id="")
	{
		$day = array();
		if(isset($day_id) && !empty($day_id)){ 
			$where_array['day_id'] = $day_id;
			$day = $this->crud->get_all_with_where('days','day_id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('days', 'day_id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['day_id'] = ($day) ? $day[0]->day_id : '';
		$return['day_name'] = ($day) ? $day[0]->day_name : '';
		if($this->applib->have_access_role(MASTER_HR_DAY_MENU_ID,"view")) {
			set_page('master_day/day', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}

	function add_day()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('days',$post_data);
		if($result)
		{
			$post_data['day_id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Day Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_day()
	{
		$post_data = $this->input->post();
		$where_array['day_id'] = $postdata['day_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('days', $post_data, array('day_id' => $post_data['day_id']));
		if ($result) {
			$post_data['day_id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Day Updated Successfully');
			echo json_encode($post_data);
		}	
	}
	
	function grade($grade_id="")
	{
		$grade = array();
		if(isset($grade_id) && !empty($grade_id)){ 
			$where_array['grade_id'] = $grade_id;
			$grade = $this->crud->get_all_with_where('grade','grade','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('grade', 'grade', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['grade_id'] = ($grade) ? $grade[0]->grade_id : '';
		$return['grade'] = ($grade) ? $grade[0]->grade : '';
		if($this->applib->have_access_role(MASTER_HR_GRADE_MENU_ID,"view")) {
			set_page('master_day/grade', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}

	function add_grade()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('grade',$post_data);
		if($result)
		{
			$post_data['grade_id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Grade Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_grade()
	{
		$post_data = $this->input->post();
		$where_array['grade_id'] = $postdata['grade_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('grade', $post_data, array('grade_id' => $post_data['grade_id']));
		if ($result) {
			$post_data['grade_id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Grade Updated Successfully');
			echo json_encode($post_data);
		}	
	}
        
        function department($department_id="")
	{
		$department = array();
		if (isset($department_id) && !empty($department_id)) {
			$where_array['department_id'] = $department_id;
			$department = $this->crud->get_all_with_where('department', 'department', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('department', 'department', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['department_id'] = ($department) ? $department[0]->department_id : '';
		$return['department_name'] = ($department) ? $department[0]->department : '';
		if($this->applib->have_access_role(MASTER_HR_DEPARTMENT_MENU_ID,"view")) {
			set_page('master_day/department', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function add_department()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('department', $post_data);
		if ($result) {
			$post_data['department_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Department Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_department()
	{
		$post_data = $this->input->post();
		$where_array['department_id'] = $post_data['department_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('department', $post_data, array('department_id' => $post_data['department_id']));
		if ($result) {
			$post_data['department_id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Department Updated Successfully');
			echo json_encode($post_data);
		}
	}


	function delete($day_id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$day_id));
		$this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
	}
    
    function employee_wise_incentive(){
		if($this->applib->have_access_role(MASTER_HR_EMPLOYEE_WISE_INCENTIVE_MENU_ID,"view")) {
            $data = array();
            $employee_wise_incentive = $this->crud->get_row_by_id('employee_wise_incentive', array('staff_id' => '0'));
//                echo '<pre>'; print_r($employee_wise_incentive); exit;
            $lineitems = array();
            foreach($employee_wise_incentive as $employee_wise_inc){
                $employee_wise_inc->item_name = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $employee_wise_inc->item_id));
                if($employee_wise_inc->no_of_item == '1'){
                    $employee_wise_inc->no_of_item_data = '1st';
                } elseif ($employee_wise_inc->no_of_item == '2') {
                    $employee_wise_inc->no_of_item_data = '2nd';
                } elseif ($employee_wise_inc->no_of_item == '3') {
                    $employee_wise_inc->no_of_item_data = '3rd';
                } elseif ($employee_wise_inc->no_of_item == '4') {
                    $employee_wise_inc->no_of_item_data = '4 & 4+';
                }
                $lineitems[] = json_encode($employee_wise_inc);
            }
            $data['employee_wise_incentive'] = implode(',', $lineitems);
			set_page('master_day/employee_wise_incentive', $data);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
    function save_employee_wise_incentive(){        
        $post_data = $this->input->post();
        if(!empty($post_data['employee_id'])){
            $employee_id = $post_data['employee_id'];
            array_push($employee_id, '0');
            foreach ($employee_id as $emp_id){
                $this->crud->delete('employee_wise_incentive', array('staff_id' => $emp_id));
            }
        } else {
            $employee_id = $this->crud->get_column_data_array('staff', 'staff_id');
            array_push($employee_id, '0');
            foreach ($employee_id as $emp_id){
                $this->crud->delete('employee_wise_incentive', array('staff_id' => $emp_id));
            }
        }
        
//        echo '<pre>'; print_r($employee_id); exit;
        $line_items_data = json_decode($post_data['line_items_data']);
        foreach($employee_id as $emp){
            foreach ($line_items_data as $line_item){            
                $insert_arr['item_id'] = $line_item->item_id;
                $insert_arr['no_of_item'] = $line_item->no_of_item;
                $insert_arr['incentive'] = $line_item->incentive;
                $insert_arr['staff_id'] = $emp;
                $insert_arr['created_at'] = $this->now_time;
                $insert_arr['created_by'] = $this->staff_id;
                $insert_arr['updated_at'] = $this->now_time;
                $insert_arr['updated_by'] = $this->staff_id;
                $this->crud->insert('employee_wise_incentive',$insert_arr);
            }
        }
        $return['success'] = "Added";
        $this->session->set_flashdata('success',TRUE);
        $this->session->set_flashdata('message','Incentive Data Saved Successfully.');
        print json_encode($return);
		exit;
    }
    
    function payment_mode($id = "") {
        $payment_mode = array();
        if (isset($id) && !empty($id)) {
            $where_array['id'] = $id;
            $payment_mode = $this->crud->get_all_with_where('payment_by', 'name', 'ASC', $where_array);
        }
        $result_data = $this->crud->get_all_records('payment_by', 'name', 'ASC');

        $return = array();
        $return['results'] = $result_data;
        $return['id'] = ($payment_mode) ? $payment_mode[0]->id : '';
        $return['name'] = ($payment_mode) ? $payment_mode[0]->name : '';
        if ($this->applib->have_access_role(MASTER_PAYMENT_MODE_MENU_ID, "view")) {
            set_page('master_day/payment_mode', $return);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function add_payment_mode() {
        $post_data = $this->input->post();
        $post_data['created_at'] = $this->now_time;
        $post_data['created_by'] = $this->staff_id;
        $post_data['updated_at'] = $this->now_time;
        $post_data['updated_by'] = $this->staff_id;
        $result = $this->crud->insert('payment_by', $post_data);
        if ($result) {
            $post_data['id'] = $result;
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Payment Mode Added Successfully');
            echo json_encode($post_data);
        }
    }

    function update_payment_mode() {
        $post_data = $this->input->post();
        $where_array['id'] = $post_data['id'];
        $post_data['updated_at'] = $this->now_time;
        $post_data['updated_by'] = $this->staff_id;
        $result = $this->crud->update('payment_by', $post_data, array('id' => $post_data['id']));
        if ($result) {
            $post_data['id'] = $result;
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Payment Mode Updated Successfully');
            echo json_encode($post_data);
        }
    }

}