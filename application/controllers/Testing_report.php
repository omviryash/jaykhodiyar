<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Testing_report extends CI_Controller
{
	protected $staff_id = 1;
	function __construct(){
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	function pick_challan_datatable(){
		$config['table'] = 'challans c';
		$config['select'] = 'c.id,c.challan_no,c.is_invoice_created,ci.item_code,p.party_name,p.phone_no,p.email_id,so.sales_order_no,pi.proforma_invoice_no,qso.quotation_no AS qso_quotation_no,qso.enquiry_no AS qso_enquiry_no,qpi.quotation_no AS qpi_quotation_no,qpi.enquiry_no AS qpi_enquiry_no,city.city,state.state,country.country';
		$config['joins'][] =  array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id','join_type'=>'left');

		$config['joins'][] =  array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id','join_type'=>'left');

		$config['joins'][] =  array('join_table' => 'quotations qso', 'join_by' => 'qso.id = so.quotation_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'quotations qpi', 'join_by' => 'qpi.id = pi.quotation_id','join_type'=>'left');

		$config['joins'][]  = array('join_table'=>'city','join_by'=>'city.city_id = p.city_id','join_type'=>'left');
		$config['joins'][]  = array('join_table'=>'state','join_by'=>'state.state_id = p.state_id','join_type'=>'left');
		$config['joins'][]  = array('join_table'=>'country','join_by'=>'country.country_id = p.country_id','join_type'=>'left');
		$config['column_order'] = array('c.challan_no',null,null,null,'ci.item_code','p.party_name','p.phone_no','p.email_id','city.city','state.state','country.country');
		$config['column_search'] = array('c.challan_no','qso.enquiry_no','qso.quotation_no','qpi.enquiry_no','qpi.quotation_no','so.sales_order_no','pi.proforma_invoice_no','p.party_name','ci.item_code','p.phone_no','p.email_id','city.city','state.state','country.country');
		$config['order'] = array('c.id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active','column_value'=>'1');
		$config['wheres'][] = array('column_name' => 'c.is_testing_report_created','column_value' =>'0');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");

		if($cu_accessExport == 1 && $cu_accessDomestic == 1){
		}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);	
		}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$role_delete = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "edit");
		foreach ($list as $challan) {
			$row = array();
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->challan_no . '</a>';
			if(!empty($challan->sales_order_no)){
				$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->sales_order_no . '</a>';	
			}else{
				$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->proforma_invoice_no . '</a>';	
			}
			if(!empty($challan->qso_quotation_no)){
				$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->qso_quotation_no . '</a>';	
			}else{
				$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->qpi_quotation_no . '</a>';
			}
			if(!empty($challan->qso_enquiry_no)){
				$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->qso_enquiry_no . '</a>';	
			}else{
				$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->qpi_enquiry_no . '</a>';
			}
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->item_code . '</a>';
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->party_name . '</a>';
            $phone_no = substr($challan->phone_no,0,10);
            $challan->phone_no = str_replace(',', ', ', $challan->phone_no);
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '" data-toggle="tooltip" data-placement="bottom" title="'.$challan->phone_no.'">'.$phone_no.'</a>';
            $email_id = substr($challan->email_id,0,14);
            $challan->email_id = str_replace(',', ', ', $challan->email_id);
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '" data-toggle="tooltip" data-placement="bottom" title="'.$challan->email_id.'">'.$email_id.'</a>';
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->city . '</a>';
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->state . '</a>';
			$row[] = '<a href="javascript:void(0);" class="challan_row" data-challan_id="' . $challan->id . '">' . $challan->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function add($testing_report_id = ''){
		$data = array();
		if(!empty($testing_report_id)){
			if ($this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "edit")) {
				$testing_report_data = $this->crud->get_row_by_id('testing_report', array('testing_report_id' => $testing_report_id));
				$testing_report_data = $testing_report_data[0];
				$testing_report_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $testing_report_data->created_by));
				$testing_report_data->created_at = substr($testing_report_data->created_at, 8, 2) .'-'. substr($testing_report_data->created_at, 5, 2) .'-'. substr($testing_report_data->created_at, 0, 4);
				$testing_report_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $testing_report_data->updated_by));
				$testing_report_data->updated_at = substr($testing_report_data->updated_at, 8, 2) .'-'. substr($testing_report_data->updated_at, 5, 2) .'-'. substr($testing_report_data->updated_at, 0, 4);
				$testing_report_data->sales_to_party_id = $this->crud->get_column_value_by_id('challans', 'sales_to_party_id', array('id' => $testing_report_data->challan_id));
                
                $testing_report_data->quality_checked_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $testing_report_data->quality_checked_by));
                $testing_report_data->equipment_inspection_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $testing_report_data->equipment_inspection_by));
                $testing_report_data->equipment_testing_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $testing_report_data->equipment_testing_by));
                $testing_report_data->our_site_incharge_person_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $testing_report_data->our_site_incharge_person));

				$data['party_contact_person'] = $this->crud->get_contact_person_by_party($testing_report_data->sales_to_party_id);
				$data['testing_report_data'] = $testing_report_data;

				if(!empty($data['testing_report_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['testing_report_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if(!empty($designation)){
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if(!empty($department)){
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
				//echo '<pre>';print_r($data); exit;
				set_page('dispatch/testing_report/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(TESTING_REPORT_MODULE_ID,"add")) {
				set_page('dispatch/testing_report/add');
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function get_challan(){
		$challan_id = $this->input->get_post("challan_id");
		$challan_sales_order_id = $this->crud->get_id_by_val('challans', 'sales_order_id', 'id', $challan_id);
		$challan_proforma_invoice_id = $this->crud->get_id_by_val('challans', 'proforma_invoice_id', 'id', $challan_id);
        if(!empty($challan_sales_order_id ) && $challan_sales_order_id != 0){
            $sales_order_id = $this->crud->get_id_by_val('sales_order', 'id', 'id', $challan_sales_order_id);
            if(empty($sales_order_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else if (!empty($challan_proforma_invoice_id ) && $challan_proforma_invoice_id != 0){
            $proforma_invoice_id = $this->crud->get_id_by_val('proforma_invoices', 'id', 'id', $challan_proforma_invoice_id);
            if(empty($proforma_invoice_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else {
            $challan_data = $this->crud->get_only_challan_data($challan_id);
        }
		if(empty($challan_sales_order_id)){
			$challan_data->proforma_invoice_no = $challan_data->sales_order_no;
			unset($challan_data->sales_order_no);
			$challan_data->proforma_invoice_date = date('d-m-Y',strtotime($challan_data->sales_order_date));
			unset($challan_data->sales_order_date);
		}
		if(!empty($challan_data->po_date)){
			$challan_data->po_date = date('d-m-Y',strtotime($challan_data->po_date));
		}
		if(!empty($challan_data->sales_order_date)){
			$challan_data->sales_order_date = date('d-m-Y',strtotime($challan_data->sales_order_date));
		}
		if(!empty($challan_data->challan_date)){
			$challan_data->challan_date = date('d-m-Y',strtotime($challan_data->challan_date));
		}
		$data['challan_data'] = $challan_data;

		$where = array('challan_id' => $challan_id);
		$challan_item_data = $this->crud->get_row_by_id('challan_items', $where);
		$data['challan_item_data'] = $challan_item_data[0];
                $data['challan_item_data']->item_extra_accessories = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $data['challan_item_data']->item_extra_accessories_id));
                $data['party_contact_person'] = $this->crud->get_contact_person_by_party($challan_data->sales_to_party_id);

		$where = array('challan_item_id' => $data['challan_item_data']->id);
		$motor_data = $this->crud->get_row_by_id('challan_items_motor_details', $where);
		//pre_die($motor_data);
		foreach($motor_data as $motor){
			$motor->motor_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $motor->motor_user);
			$motor->motor_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $motor->motor_make);
			$motor->motor_hp = $this->crud->get_id_by_val('challan_item_hp', 'hp_name', 'id', $motor->motor_hp);
			$motor->motor_kw = $this->crud->get_id_by_val('challan_item_kw', 'kw_name', 'id', $motor->motor_kw);
			$motor->motor_frequency = $this->crud->get_id_by_val('challan_item_frequency','frequency_name', 'id', $motor->motor_frequency);
			$motor->motor_rpm = $this->crud->get_id_by_val('challan_item_rpm','rpm_name', 'id', $motor->motor_rpm);
			$motor->motor_volts_cycles = $this->crud->get_id_by_val('challan_item_volts_cycles','volts_cycles_name', 'id', $motor->motor_volts_cycles);	
		}
		$data['motor_data'] = $motor_data;

		$gearbox_data = $this->crud->get_row_by_id('challan_items_gearbox_details', $where);
		foreach($gearbox_data as $gearbox){
			$gearbox->gearbox_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $gearbox->gearbox_user);
			$gearbox->gearbox_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $gearbox->gearbox_make);
			$gearbox->gearbox_gear_type = $this->crud->get_id_by_val('challan_item_gear_type','gear_type_name', 'id', $gearbox->gearbox_gear_type);
			$gearbox->gearbox_model = $this->crud->get_id_by_val('challan_item_model','model_name', 'id', $gearbox->gearbox_model);
			$gearbox->gearbox_ratio = $this->crud->get_id_by_val('challan_item_ratio','ratio_name', 'id', $gearbox->gearbox_ratio);	
		}
		$data['gearbox_data'] = $gearbox_data;

		if(!empty($data['challan_data']->kind_attn_id)){
			$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['challan_data']->kind_attn_id));
            if(!empty($data['contact_person'])){
                $data['contact_person'] = $data['contact_person'][0];
                $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                $data['contact_person']->designation = '';
                if(!empty($designation)){
                    $data['contact_person']->designation = $designation[0]->designation;
                }
                $data['contact_person']->department = '';
                if(!empty($department)){
                    $data['contact_person']->department = $department[0]->department;
                }
            }
		}
		//echo '<pre>';print_r($data); exit;
		echo json_encode($data);
		exit;
	}
    
    function get_staff_contact_no($id){
        if(!empty($id) && $id != 'null'){
            echo $this->applib->get_staff_contact_no($id);
        }
    }

	function save_testing_report(){
        $return = array();
		$post_data = $this->input->post();
		//echo '<pre>';print_r($post_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
		$post_data['testing_report_data']['testing_report_date'] = (!empty(strtotime($post_data['testing_report_data']['testing_report_date']))) ? date("Y-m-d", strtotime($post_data['testing_report_data']['testing_report_date'])) : null;
		$post_data['testing_report_data']['assembling_equipment'] = (!empty(strtotime($post_data['testing_report_data']['assembling_equipment']))) ? date("Y-m-d", strtotime($post_data['testing_report_data']['assembling_equipment'])) : null;
		$post_data['testing_report_data']['equipment_testing_date'] = (!empty(strtotime($post_data['testing_report_data']['equipment_testing_date']))) ? date("Y-m-d", strtotime($post_data['testing_report_data']['equipment_testing_date'])) : null;
		$post_data['testing_report_data']['delivery_of_equipment'] = (!empty(strtotime($post_data['testing_report_data']['delivery_of_equipment']))) ? date("Y-m-d", strtotime($post_data['testing_report_data']['delivery_of_equipment'])) : null;
        $post_data['testing_report_data']['quality_checked_by'] = (!empty($post_data['testing_report_data']['quality_checked_by'])) ? $post_data['testing_report_data']['quality_checked_by'] : null;
        $post_data['testing_report_data']['equipment_inspection_by'] = (!empty($post_data['testing_report_data']['equipment_inspection_by'])) ? $post_data['testing_report_data']['equipment_inspection_by'] : null;
        $post_data['testing_report_data']['equipment_testing_by'] = (!empty($post_data['testing_report_data']['equipment_testing_by'])) ? $post_data['testing_report_data']['equipment_testing_by'] : null;
        $post_data['testing_report_data']['our_site_incharge_person'] = (!empty($post_data['testing_report_data']['our_site_incharge_person'])) ? $post_data['testing_report_data']['our_site_incharge_person'] : null;
		if(isset($_POST['testing_report_data']['testing_report_id']) && !empty($_POST['testing_report_data']['testing_report_id'])){
			$testing_report_id = $_POST['testing_report_data']['testing_report_id'];
			if (isset($post_data['testing_report_data']['testing_report_id'])){
				unset($post_data['testing_report_data']['testing_report_id']);
			}

			$post_data['testing_report_data']['updated_at'] = $this->now_time;
			$post_data['testing_report_data']['updated_by'] = $this->staff_id;
			$this->db->where('testing_report_id', $testing_report_id);
			$result = $this->db->update('testing_report', $post_data['testing_report_data']);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Testing Report Updated Successfully');
			}
		} else {
			$dataToInsert = $post_data['testing_report_data'];
			$challan_id = $dataToInsert['challan_id'];
			
			$max_testing_report_no = $this->crud->get_max_number('testing_report','testing_report_no');
			$dataToInsert['testing_report_no'] = $max_testing_report_no->testing_report_no + 1;
			$dataToInsert['testing_report_no_year'] = $this->applib->get_challan_no($dataToInsert['testing_report_no'], $post_data['testing_report_data']['testing_report_date']);
			
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
			$result = $this->db->insert('testing_report', $dataToInsert);
			$testing_report_id = $this->db->insert_id();
			if($result){
				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Testing Report Added Successfully');
				$this->crud->update('challans',array('is_testing_report_created' => 1),array('id' => $challan_id));				
			}
		}
		$challan_items_motor_details = $post_data['challan_items_motor_details'];
        foreach($challan_items_motor_details as $key => $row){
			$data[] = array(				 
				'id' => $key,
				'normal_amps_jk' => $row['normal_amps_jk'],
				'working_amps_jk' => $row['working_amps_jk'],
				'total_workinghours_jk' => $row['total_workinghours_jk'],
			);			
		}
		$result = $this->db->update_batch('challan_items_motor_details', $data, 'id');
        if(isset($post_data['challan_items_gearbox_details']) && !empty($post_data['challan_items_gearbox_details'])){
            $challan_items_gearbox_details = $post_data['challan_items_gearbox_details'];
            foreach($challan_items_gearbox_details as $key => $row){
                $data[] = array(				 
                    'id' => $key,
                    'normal_amps_jk' => $row['normal_amps_jk'],
                    'working_amps_jk' => $row['working_amps_jk'],
                    'total_workinghours_jk' => $row['total_workinghours_jk'],
                );			
            }
            $result = $this->db->update_batch('challan_items_gearbox_details', $data, 'id');
        }
		print json_encode($return);
		exit;
	}

	function testing_report_list(){
		if ($this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "view")) {
			set_page('dispatch/testing_report/testing_report_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function testing_report_datatable(){
		$config['table'] = 'testing_report tr';
		$config['select'] = 'tr.testing_report_id,tr.testing_report_no,tr.testing_report_no_year,tr.is_installation_created,c.challan_no,c.id,ci.item_code,p.party_name,p.phone_no,p.email_id,so.sales_order_no,pi.proforma_invoice_no,qso.quotation_no AS qso_quotation_no,qso.enquiry_no AS qso_enquiry_no,qpi.quotation_no AS qpi_quotation_no,qpi.enquiry_no AS qpi_enquiry_no,city.city,state.state,country.country,cur_s.name AS party_current_person';
		$config['joins'][] = array('join_table' => 'challans c', 'join_by' => 'tr.challan_id = c.id','join_type'=>'left');
        $config['joins'][] = array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id','join_type'=>'left');
		$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id','join_type'=>'left');
		$config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id','join_type'=>'left');

		$config['joins'][] = array('join_table' => 'quotations qso', 'join_by' => 'qso.id = so.quotation_id','join_type'=>'left');
		$config['joins'][] = array('join_table' => 'quotations qpi', 'join_by' => 'qpi.id = pi.quotation_id','join_type'=>'left');

		$config['joins'][] = array('join_table'=>'city','join_by'=>'city.city_id = p.city_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'state','join_by'=>'state.state_id = p.state_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'country','join_by'=>'country.country_id = p.country_id','join_type'=>'left');
        $config['joins'][] = array('join_table'=> 'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');

		$config['column_order'] = array(null,'tr.testing_report_no','c.challan_no',null,null,null,'ci.item_code','p.party_name','p.phone_no','p.email_id','city.city','state.state','country.country');
		$config['column_search'] = array('tr.testing_report_no_year','c.challan_no','so.sales_order_no','pi.proforma_invoice_no','qso.quotation_no','qso.enquiry_no','qpi.quotation_no','qpi.enquiry_no','ci.item_code','p.party_name','p.phone_no','p.email_id','city.city','state.state','country.country');
		$config['order'] = array('tr.testing_report_id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active','column_value'=>'1');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");

		if($cu_accessExport == 1 && $cu_accessDomestic == 1){
		}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);	
		}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$role_delete = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "edit");
		foreach ($list as $testing_report) {
			$row = array();
			$action = '';
			if($role_edit) {
				$action .= '<a href="'.base_url('testing_report/add/'.$testing_report->testing_report_id).'" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			}
			if($role_delete) {
                if ($testing_report->is_installation_created != 1) {
                    $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'.base_url('testing_report/delete_testing_report/'.$testing_report->testing_report_id).'"><i class="fa fa-trash"></i></a>';
                }
			}
			$action .= ' | <a href="'.base_url('testing_report/testing_print/'.$testing_report->testing_report_id).'" target="_blank" class="btn-primary btn-xs" title="Print Testing"><i class="fa fa-print"></i> Print</a>';
			$action .= ' | <a href="'.base_url('testing_report/plat_print/'.$testing_report->testing_report_id).'" target="_blank" class="btn-primary btn-xs" title="Plat Print"><i class="fa fa-print"></i> Plat Print</a>';
			$row[] = $action;
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->testing_report_no_year.'</a>';
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->challan_no.'</a>';

			if(!empty($testing_report->sales_order_no)){
				$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->sales_order_no.'</a>';	
			}else{				
				$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->proforma_invoice_no.'</a>';
			}
			if(!empty($testing_report->qso_quotation_no)){
				$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->qso_quotation_no.'</a>';
			}else{				
				$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->qpi_quotation_no.'</a>';
			}

			if(!empty($testing_report->qso_enquiry_no)){
				$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->qso_enquiry_no.'</a>';
			}else{				
				$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->qpi_enquiry_no.'</a>';
			}

			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->item_code.'</a>';
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->party_name.'</a>';
            $phone_no = substr($testing_report->phone_no,0,10);
            $testing_report->phone_no = str_replace(',', ', ', $testing_report->phone_no);
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$testing_report->phone_no.'">'.$phone_no.'</a>';
            $email_id = substr($testing_report->email_id,0,14);
            $testing_report->email_id = str_replace(',', ', ', $testing_report->email_id);
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$testing_report->email_id.'">'.$email_id.'</a>';
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->city.'</a>';
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->state.'</a>';
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->country.'</a>';
			$row[] = '<a href="' . base_url('testing_report/add/' . $testing_report->testing_report_id . '?view') . '" >'.$testing_report->party_current_person.'</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function delete_testing_report($testing_report_id){
		$role_delete = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "delete");
		if ($role_delete) {
			$where_array = array("testing_report_id" => $testing_report_id);			
			//---Start Update Challan Table
			$challan_id = $this->crud->get_id_by_val('testing_report','challan_id','testing_report_id',$testing_report_id);			
			$this->crud->update('challans',array('is_testing_report_created' => '0'),array('id' => $challan_id));			
			//---Ent Update Challan Table

			$this->crud->delete("testing_report", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function testing_print($id){
		$company_details = $this->applib->get_company_detail();
		$challan_id = $this->crud->get_id_by_val('testing_report','challan_id','testing_report_id',$id);
		$sales_order_id = $this->crud->get_id_by_val('challans','sales_order_id','id',$challan_id);
		$proforma_invoice_id = $this->crud->get_id_by_val('challans','proforma_invoice_id','id',$challan_id);
		$from = !empty($sales_order_id) ? 'sales_order' : (!empty($proforma_invoice_id) ? 'proforma_invoices' : '');
		$testing_report_data = $this->get_testing_report_detail($id,$from);
        $testing_report_data['item_serial_no'] = $this->crud->get_id_by_val('challan_items','item_serial_no','challan_id',$testing_report_data['challan_id']);

		if(empty($testing_report_data['sales_order_no'])){
			$testing_report_data['proforma_invoice_no'] = $this->crud->get_id_by_val('proforma_invoices','proforma_invoice_no','id',$testing_report_data['proforma_invoice_id']);
			$testing_report_data['proforma_invoice_date'] = $this->crud->get_id_by_val('proforma_invoices','sales_order_date','id',$testing_report_data['proforma_invoice_id']);
			$testing_report_data['cust_po_no'] = $this->crud->get_id_by_val('proforma_invoices','cust_po_no','id',$testing_report_data['proforma_invoice_id']);
			$testing_report_data['po_date'] = $this->crud->get_id_by_val('proforma_invoices','po_date','id',$testing_report_data['proforma_invoice_id']);
		}else{
			$testing_report_data['cust_po_no'] = $this->crud->get_id_by_val('sales_order','cust_po_no','id',$testing_report_data['sales_order_id']);
			$testing_report_data['po_date'] = $this->crud->get_id_by_val('sales_order','po_date','id',$testing_report_data['sales_order_id']);
		}
		//pre_die($testing_report_data);
		$challan_id =  $testing_report_data['challan_id'];
		$challan_item_id = $this->crud->get_id_by_val('challan_items','id','challan_id',$challan_id);
		$testing_report_data['item_code'] = $this->crud->get_id_by_val('items','item_code1','id',$testing_report_data['item_id']);

		$testing_report_data['our_site_incharge_person_contect_staff'] = $this->crud->get_id_by_val('staff','contact_no','staff_id',$testing_report_data['our_site_incharge_person']);
		$testing_report_data['quality_checked_by'] = $this->crud->get_id_by_val('staff','name','staff_id',$testing_report_data['quality_checked_by']);
		$testing_report_data['equipment_testing_by'] = $this->crud->get_id_by_val('staff','name','staff_id',$testing_report_data['equipment_testing_by']);
		$testing_report_data['equipment_inspection_by'] = $this->crud->get_id_by_val('staff','name','staff_id',$testing_report_data['equipment_inspection_by']);
		$testing_report_data['our_site_incharge_person'] = $this->crud->get_id_by_val('staff','name','staff_id',$testing_report_data['our_site_incharge_person']);
		$testing_report_data['test_declaration'] = $this->crud->get_id_by_val('terms_and_conditions','detail','module','testing_declaration');

		$motor_data = $this->get_motor_data($challan_item_id);
		$gearbox_data = $this->get_gearbox_data($challan_item_id);

		//pre_die($testing_report_data);

		$this->load->library('m_pdf');
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;

		$invoice_data = $this->crud->get_row_by_id('invoices', array('sales_order_no' => $challan_data->sales_order_no));
		if(!empty($invoice_data)){
			//$challan_data->invoice_no = $invoice_data[0]->invoice_no;
			//$challan_data->invoice_date = date('d-m-Y',strtotime($invoice_data[0]->sales_order_date));
		}

		if (!empty($testing_report_data)) {
			$param = "'utf-8','A4'";
			$pdf = $this->m_pdf->load($param);
			$pdf->AddPage(
				'P', //orientation
				'', //type
				'', //resetpagenum
				'', //pagenumstyle
				'', //suppress
				$margin_left, //margin-left
				$margin_right, //margin-right
				$margin_top, //margin-top
				$margin_bottom, //margin-bottom
				0, //margin-header
				0 //margin-footer
			);
			if ($key == 0) {
				$other_items = $this->get_challan_sub_items($id);
				$html = $this->load->view('dispatch/testing_report/testing_pdf', array('testing_report_data' => $testing_report_data, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => true), true);
			} else {
				$html = $this->load->view('dispatch/testing_report/testing_pdf', array('testing_report_data' => $testing_report_data, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => false), true);
			}
			$pdf->WriteHTML($html);

			$pdfFilePath = "testing_".$id.".pdf";
			$pdf->Output($pdfFilePath, "I");

		}
	}

	function get_testing_report_detail($testing_report_id,$from){
		if($from == ''){
			$this->session->set_flashdata('error_message', 'Sales Order id and Proforma Invoice id empty in challans.');
			redirect("/");
		}
		$select = "tr.*,ch.*, ch.id as challan_id, sales.sales,quo.quotation_no,";
		$select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,party.party_type_1,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,party.utr_no AS party_cin_no,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		if($from == 'sales_order'){			
			$select .= "so.sales_order_no,so.sales_order_date,";
		}else{
			$select .= "pi.proforma_invoice_no,pi.sales_order_date,";
		}
		$this->db->select($select);
		$this->db->from('testing_report tr');
		$this->db->join('challans ch', 'ch.id = tr.challan_id', 'left');
		if($from == 'sales_order'){
			$this->db->join('sales_order so', 'so.id = ch.sales_order_id', 'left');
			$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
			$this->db->join('sales', 'sales.id = so.sales_id', 'left');
			$this->db->join('currency', 'currency.id = so.currency_id', 'left');
			$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
			$this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
			$this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
		}else{
			$this->db->join('proforma_invoices pi', 'pi.id = ch.proforma_invoice_id', 'left');
			$this->db->join('quotations quo', 'quo.id = pi.quotation_id', 'left');
			$this->db->join('party party', 'party.party_id = pi.sales_to_party_id', 'left');
			$this->db->join('staff stf', 'stf.staff_id = pi.created_by', 'left');
			$this->db->join('currency', 'currency.id = pi.currency_id', 'left');
			$this->db->join('sales', 'sales.id = pi.sales_id', 'left');
			$this->db->join('contact_person cp', 'cp.contact_person_id = pi.kind_attn_id', 'left');
		}
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');

		$this->db->where('tr.testing_report_id', $testing_report_id);
		$this->db->where('party.active !=',0);
		$this->db->limit('1');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array()[0];
		} else {
			return array();
		}
	}

	function get_motor_data($challan_item_id){
		$where = array('challan_item_id' => $challan_item_id);
		$motor_data = $this->crud->get_row_by_id('challan_items_motor_details', $where);
		//pre_die($motor_data);
		foreach($motor_data as $motor){
			$motor->motor_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $motor->motor_user);
			$motor->motor_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $motor->motor_make);
			$motor->motor_hp = $this->crud->get_id_by_val('challan_item_hp', 'hp_name', 'id', $motor->motor_hp);
			$motor->motor_kw = $this->crud->get_id_by_val('challan_item_kw', 'kw_name', 'id', $motor->motor_kw);
			$motor->motor_frequency = $this->crud->get_id_by_val('challan_item_frequency','frequency_name', 'id', $motor->motor_frequency);
			$motor->motor_rpm = $this->crud->get_id_by_val('challan_item_rpm','rpm_name', 'id', $motor->motor_rpm);
			$motor->motor_volts_cycles = $this->crud->get_id_by_val('challan_item_volts_cycles','volts_cycles_name', 'id', $motor->motor_volts_cycles);	
		}
		return $motor_data;
	}

	function get_gearbox_data($challan_item_id){
		$where = array('challan_item_id' => $challan_item_id);
		$gearbox_data = $this->crud->get_row_by_id('challan_items_gearbox_details', $where);
		foreach($gearbox_data as $gearbox){
			$gearbox->gearbox_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $gearbox->gearbox_user);
			$gearbox->gearbox_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $gearbox->gearbox_make);
			$gearbox->gearbox_gear_type = $this->crud->get_id_by_val('challan_item_gear_type','gear_type_name', 'id', $gearbox->gearbox_gear_type);
			$gearbox->gearbox_model = $this->crud->get_id_by_val('challan_item_model','model_name', 'id', $gearbox->gearbox_model);
			$gearbox->gearbox_ratio = $this->crud->get_id_by_val('challan_item_ratio','ratio_name', 'id', $gearbox->gearbox_ratio);	
		}
		return $gearbox_data;
	}

	function get_challan_detail($id){
		$select = "ch.*, ch.id as challan_id, ch.notes as challan_notes, ch.loading_at as challan_loading_at, sales.sales,currency.currency,quo.quotation_no,so.*,party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,";
		$select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,party.party_type_1,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required";
		$this->db->select($select);
		$this->db->from('challans ch');
		$this->db->join('sales_order so', 'so.id = ch.sales_order_id', 'left');
		$this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = so.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = so.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = so.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = so.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = so.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = so.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = so.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = so.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = so.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = so.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = so.for_required_id', 'left');
		$this->db->where('ch.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_challan_items($id){
		$this->db->select('soi.*');
		$this->db->from('challan_items soi');
		//        $this->db->join('item_category ic','ic.id = soi.item_category_id');
		//        $this->db->where('ic.item_category','Finished Goods');
		$this->db->where('soi.challan_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	function get_challan_sub_items($id){
		$this->db->select('soi.*');
		$this->db->from('challan_items soi');
		$this->db->join('item_category ic','ic.id = soi.item_category_id');
		$this->db->where('ic.item_category != ','Finished Goods');
		$this->db->where('soi.challan_id', $id);
		$query = $this->db->get();
		$other_items = array();
		if ($query->num_rows() > 0) {
			$other_items = $query->result_array();
		}
		return $other_items;
	}

}
