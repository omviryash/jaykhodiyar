<?php

ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Feedback extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    function add($feedback_id = '') {
        $data = array();
		if (!empty($feedback_id)) {
			if ($this->app_model->have_access_role(FEEDBACK_MODULE_ID, "edit")) {
				$feedback_data = $this->crud->get_row_by_id('feedback', array('feedback_id' => $feedback_id));
				if (empty($feedback_data)) {
					redirect("/feedback/feedback_list/");
				}
				$feedback_data = $feedback_data[0];
				$feedback_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $feedback_data->created_by));
				$feedback_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $feedback_data->created_by));
				$feedback_data->created_at = substr($feedback_data->created_at, 8, 2) . '-' . substr($feedback_data->created_at, 5, 2) . '-' . substr($feedback_data->created_at, 0, 4);
				$feedback_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $feedback_data->updated_by));
				$feedback_data->updated_at = substr($feedback_data->updated_at, 8, 2) . '-' . substr($feedback_data->updated_at, 5, 2) . '-' . substr($feedback_data->updated_at, 0, 4);
				
				$data['feedback_data'] = $feedback_data;
                $data['feedback_items_data'] = $this->crud->get_row_by_id('feedback_items', array('feedback_id' => $feedback_id));
                
				//echo '<pre>';print_r($data); exit;
				set_page('service/feedback/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
            if ($this->applib->have_access_role(FEEDBACK_MODULE_ID, "add")) {
				set_page('service/feedback/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}
    
    function save_feedback() {
        $return = array();
		$post_data = $this->input->post();
//        echo '<pre>';print_r($post_data); exit;
        $send_sms = 0;
        if(isset($post_data['feedback_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['feedback_data']['send_sms']);
        }
        
		/* --------- Convert Date as Mysql Format ------------- */
		$post_data['feedback_data']['feedback_date'] = (!empty(strtotime($post_data['feedback_data']['feedback_date']))) ? date("Y-m-d", strtotime($post_data['feedback_data']['feedback_date'])) : null;
        $post_data['feedback_data']['party_id'] = (isset($post_data['feedback_data']['party_id'])) ? $post_data['feedback_data']['party_id'] : null;
        $post_data['feedback_data']['feedback_call_by'] = (isset($post_data['feedback_data']['feedback_call_by'])) ? $post_data['feedback_data']['feedback_call_by'] : null;
        $post_data['feedback_data']['how_are_you'] = (isset($post_data['feedback_data']['how_are_you']) && !empty($post_data['feedback_data']['how_are_you'])) ? $post_data['feedback_data']['how_are_you'] : null;
        $post_data['feedback_data']['can_we_talk'] = (isset($post_data['feedback_data']['can_we_talk']) && !empty($post_data['feedback_data']['can_we_talk'])) ? $post_data['feedback_data']['can_we_talk'] : null;
        $post_data['feedback_data']['satisfied_with_quality'] = (isset($post_data['feedback_data']['satisfied_with_quality']) && !empty($post_data['feedback_data']['satisfied_with_quality'])) ? $post_data['feedback_data']['satisfied_with_quality'] : null;
        $post_data['feedback_data']['delivery_on_time'] = (isset($post_data['feedback_data']['delivery_on_time']) && !empty($post_data['feedback_data']['delivery_on_time'])) ? $post_data['feedback_data']['delivery_on_time'] : null;
        $post_data['feedback_data']['technician_come_on_time'] = (isset($post_data['feedback_data']['technician_come_on_time']) && !empty($post_data['feedback_data']['technician_come_on_time'])) ? $post_data['feedback_data']['technician_come_on_time'] : null;
        $post_data['feedback_data']['technician_properly_explain'] = (isset($post_data['feedback_data']['technician_properly_explain']) && !empty($post_data['feedback_data']['technician_properly_explain'])) ? $post_data['feedback_data']['technician_properly_explain'] : null;
        $post_data['feedback_data']['ask_money_for_install'] = (isset($post_data['feedback_data']['ask_money_for_install']) && !empty($post_data['feedback_data']['ask_money_for_install'])) ? $post_data['feedback_data']['ask_money_for_install'] : null;
        $post_data['feedback_data']['install_machine_properly'] = (isset($post_data['feedback_data']['install_machine_properly']) && !empty($post_data['feedback_data']['install_machine_properly'])) ? $post_data['feedback_data']['install_machine_properly'] : null;
        $post_data['feedback_data']['raw_material_using'] = (isset($post_data['feedback_data']['raw_material_using']) && !empty($post_data['feedback_data']['raw_material_using'])) ? $post_data['feedback_data']['raw_material_using'] : null;
        $post_data['feedback_data']['give_proper_production'] = (isset($post_data['feedback_data']['give_proper_production']) && !empty($post_data['feedback_data']['give_proper_production'])) ? $post_data['feedback_data']['give_proper_production'] : null;
        $post_data['feedback_data']['properly_done_service'] = (isset($post_data['feedback_data']['properly_done_service']) && !empty($post_data['feedback_data']['properly_done_service'])) ? $post_data['feedback_data']['properly_done_service'] : null;
        $post_data['feedback_data']['visit_on_time'] = (isset($post_data['feedback_data']['visit_on_time']) && !empty($post_data['feedback_data']['visit_on_time'])) ? $post_data['feedback_data']['visit_on_time'] : null;
        $post_data['feedback_data']['were_problems_arised'] = (isset($post_data['feedback_data']['were_problems_arised']) && !empty($post_data['feedback_data']['were_problems_arised'])) ? $post_data['feedback_data']['were_problems_arised'] : null;
        $post_data['feedback_data']['idea_regarding_problem'] = (isset($post_data['feedback_data']['idea_regarding_problem']) && !empty($post_data['feedback_data']['idea_regarding_problem'])) ? $post_data['feedback_data']['idea_regarding_problem'] : null;
        $post_data['feedback_data']['behaviour_of_technician'] = (isset($post_data['feedback_data']['behaviour_of_technician']) && !empty($post_data['feedback_data']['behaviour_of_technician'])) ? $post_data['feedback_data']['behaviour_of_technician'] : null;
        $post_data['feedback_data']['ask_for_facility'] = (isset($post_data['feedback_data']['ask_for_facility']) && !empty($post_data['feedback_data']['ask_for_facility'])) ? $post_data['feedback_data']['ask_for_facility'] : null;
        $post_data['feedback_data']['ask_for_money'] = (isset($post_data['feedback_data']['ask_for_money']) && !empty($post_data['feedback_data']['ask_for_money'])) ? $post_data['feedback_data']['ask_for_money'] : null;
        $post_data['feedback_data']['pick_up_facility'] = (isset($post_data['feedback_data']['pick_up_facility']) && !empty($post_data['feedback_data']['pick_up_facility'])) ? $post_data['feedback_data']['pick_up_facility'] : null;
        $post_data['feedback_data']['work_perfomance'] = (isset($post_data['feedback_data']['work_perfomance']) && !empty($post_data['feedback_data']['work_perfomance'])) ? $post_data['feedback_data']['work_perfomance'] : null;
        $post_data['feedback_data']['when_technician_came'] = (isset($post_data['feedback_data']['when_technician_came']) && !empty($post_data['feedback_data']['when_technician_came'])) ? $post_data['feedback_data']['when_technician_came'] : null;
        $post_data['feedback_data']['support_of_staff'] = (isset($post_data['feedback_data']['support_of_staff']) && !empty($post_data['feedback_data']['support_of_staff'])) ? $post_data['feedback_data']['support_of_staff'] : null;
        $post_data['feedback_data']['understand_technical_problem'] = (isset($post_data['feedback_data']['understand_technical_problem']) && !empty($post_data['feedback_data']['understand_technical_problem'])) ? $post_data['feedback_data']['understand_technical_problem'] : null;
        $post_data['feedback_data']['able_to_solve'] = (isset($post_data['feedback_data']['able_to_solve']) && !empty($post_data['feedback_data']['able_to_solve'])) ? $post_data['feedback_data']['able_to_solve'] : null;
        $post_data['feedback_data']['satisfied_our_product'] = (isset($post_data['feedback_data']['satisfied_our_product']) && !empty($post_data['feedback_data']['satisfied_our_product'])) ? $post_data['feedback_data']['satisfied_our_product'] : null;
        $post_data['feedback_data']['business_with_jk'] = (isset($post_data['feedback_data']['business_with_jk']) && !empty($post_data['feedback_data']['business_with_jk'])) ? $post_data['feedback_data']['business_with_jk'] : null;
        $post_data['feedback_data']['say_something_for_product'] = (isset($post_data['feedback_data']['say_something_for_product']) && !empty($post_data['feedback_data']['say_something_for_product'])) ? $post_data['feedback_data']['say_something_for_product'] : null;
        $post_data['feedback_data']['say_something_for_team'] = (isset($post_data['feedback_data']['say_something_for_team']) && !empty($post_data['feedback_data']['say_something_for_team'])) ? $post_data['feedback_data']['say_something_for_team'] : null;
        $post_data['feedback_data']['behaviour_of_jk'] = (isset($post_data['feedback_data']['behaviour_of_jk']) && !empty($post_data['feedback_data']['behaviour_of_jk'])) ? $post_data['feedback_data']['behaviour_of_jk'] : null;
        
		if (isset($_POST['feedback_data']['feedback_id']) && !empty($_POST['feedback_data']['feedback_id'])) {
			$feedback_id = $_POST['feedback_data']['feedback_id'];
			if (isset($post_data['feedback_data']['feedback_id'])) {
				unset($post_data['feedback_data']['feedback_id']);
			}

			$post_data['feedback_data']['updated_at'] = $this->now_time;
			$post_data['feedback_data']['updated_by'] = $this->staff_id;
			$this->db->where('feedback_id', $feedback_id);
			$result = $this->db->update('feedback', $post_data['feedback_data']);
			if ($result) {
				$return['success'] = "Updated";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Feedback Updated Successfully');
                    
                $this->db->where_in('feedback_id', $feedback_id);
                $this->db->delete('feedback_items');
                foreach ($post_data['feedback_items'] as $feedback_items) {
                    $feedback_items_details = array();
                    $feedback_items_details['feedback_id'] = $feedback_id;
                    $feedback_items_details['installation_id'] = $feedback_items['installation_id'];
                    $feedback_items_details['testing_report_id'] = $feedback_items['testing_report_id'];
                    $feedback_items_details['challan_id'] = $feedback_items['challan_id'];
                    $feedback_items_details['created_at'] = $this->now_time;
                    $feedback_items_details['updated_at'] = $this->now_time;
                    $feedback_items_details['created_by'] = $this->staff_id;
                    $feedback_items_details['updated_by'] = $this->staff_id;
                    $this->db->insert('feedback_items', $feedback_items_details);
                }

			}
		} else {
			$dataToInsert = $post_data['feedback_data'];
			
            $max_feedback_no = $this->crud->get_max_number('feedback','feedback_no');
			$dataToInsert['feedback_no'] = $max_feedback_no->feedback_no + 1;
			$dataToInsert['feedback_no_year'] = $this->applib->get_feedback_no($dataToInsert['feedback_no'], $post_data['feedback_data']['feedback_date']);
			
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
			$result = $this->db->insert('feedback', $dataToInsert);
            $feedback_id = $this->db->insert_id();
			if ($result) {
				$return['success'] = "Added";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Feedback Added Successfully');
				
                foreach ($post_data['feedback_items'] as $feedback_items) {
                    $this->crud->update('installation',array('is_feedback_created'=> 1),array('installation_id'=>$feedback_items['installation_id']));
                    $feedback_items_details = array();
                    $feedback_items_details['feedback_id'] = $feedback_id;
                    $feedback_items_details['installation_id'] = $feedback_items['installation_id'];
                    $feedback_items_details['testing_report_id'] = $feedback_items['testing_report_id'];
                    $feedback_items_details['challan_id'] = $feedback_items['challan_id'];
                    $feedback_items_details['created_at'] = $this->now_time;
                    $feedback_items_details['updated_at'] = $this->now_time;
                    $feedback_items_details['created_by'] = $this->staff_id;
                    $feedback_items_details['updated_by'] = $this->staff_id;
                    $this->db->insert('feedback_items', $feedback_items_details);
                }
			}
		}
        
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['feedback_data']['party_id']);
            $this->applib->send_sms('feedback', $feedback_id, $party_phone_no, SEND_FEEDBACK_SMS_ID);
        }
        
		print json_encode($return);
		exit;
	}
    
    function feedback_list() {
		if ($this->app_model->have_access_role(FEEDBACK_MODULE_ID, "view")) {
			set_page('service/feedback/feedback_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function feedback_datatable() {
		$config['table'] = 'feedback f';
		$config['select'] = 'f.feedback_id,f.feedback_no,f.feedback_no_year,f.feedback_date,p.party_name,p.phone_no,p.email_id,city.city,state.state,country.country';
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = f.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

		$config['column_order'] = array(null, 'f.feedback_no', 'f.feedback_date', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('f.feedback_no_year', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['order'] = array('f.feedback_id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '1');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
		} elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
        $role_delete = $this->app_model->have_access_role(FEEDBACK_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(FEEDBACK_MODULE_ID, "edit");
		foreach ($list as $feedback) {
			$row = array();
			$action = '';
			if ($role_edit) {
				$action .= '<a href="' . base_url('feedback/add/' . $feedback->feedback_id) . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			}
			if ($role_delete) {
                $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('feedback/delete_feedback/' . $feedback->feedback_id) . '"><i class="fa fa-trash"></i></a>';				
			}
            $action .= ' | <a href="' . base_url('feedback/feedback_print/' . $feedback->feedback_id) . '" target="_blank" class="btn-primary btn-xs" title="Feedback Print"><i class="fa fa-print"></i> Print</a>';
            //$action .= ' | <a href="' . base_url('feedback/feedback_email/' . $feedback->feedback_id) . '" target="_blank" class="" title="Email">Email</a>';
			$row[] = $action;
			$row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" >' . $feedback->feedback_no_year . '</a>';
            $feedback_date = (!empty(strtotime($feedback->feedback_date))) ? date("d-m-Y", strtotime($feedback->feedback_date)) : '';
			$row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" >' . $feedback_date . '</a>';
			$row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" >' . $feedback->party_name . '</a>';
            $phone_no = substr($feedback->phone_no,0,14);
            $feedback->phone_no = str_replace(',', ', ', $feedback->phone_no);
            $row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$feedback->phone_no.'" >'.$phone_no.'</a>';
            $email_id = substr($feedback->email_id,0,14);
            $feedback->email_id = str_replace(',', ', ', $feedback->email_id);
            $row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$feedback->email_id.'" >'.$email_id.'</a>';
			$row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" >' . $feedback->city . '</a>';
			$row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" >' . $feedback->state . '</a>';
			$row[] = '<a href="' . base_url('feedback/add/' . $feedback->feedback_id . '?view') . '" >' . $feedback->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
    
    function feed_feedback_items($feedback_id){
        $this->db->select('fi.*, ci.item_name, i.installation_no, i.installation_no_year');
		$this->db->from('feedback_items fi');
        $this->db->join('challan_items ci', 'ci.challan_id = fi.challan_id', 'left');
        $this->db->join('installation i', 'i.installation_id = fi.installation_id', 'left');
		$this->db->where('fi.feedback_id', $feedback_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$item_data = $query->result();
			echo json_encode(array('success' => true, 'message' => 'Item loaded successfully!', 'item_data' => $item_data));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}
	
    function delete_feedback($feedback_id) {
        $role_delete = $this->app_model->have_access_role(FEEDBACK_MODULE_ID, "delete");
		if ($role_delete) {
            $feedback_items = $this->crud->get_row_by_id('feedback_items', array('feedback_id' => $feedback_id));
            foreach ($feedback_items as $feedback_item) {
                    $this->crud->update('installation',array('is_feedback_created'=> 0),array('installation_id'=>$feedback_item->installation_id));
            }
			$where_array = array("feedback_id" => $feedback_id);
			$this->crud->delete("feedback", $where_array);
			$this->crud->delete("feedback_items", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function feedback_print($feedback_id) {
        
        $company_details = $this->applib->get_company_detail();
        $data['company_details'] = $company_details;
        $feedback_data = $this->crud->get_row_by_id('feedback', array('feedback_id' => $feedback_id));
        $feedback_data = $feedback_data[0];
        $feedback_data->feedback_date = (!empty(strtotime($feedback_data->feedback_date))) ? date("d/m/Y", strtotime($feedback_data->feedback_date)) : null;
        $feedback_data->feedback_call_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $feedback_data->feedback_call_by));
        $feedback_data->behaviour_of_technician = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $feedback_data->behaviour_of_technician));
        $feedback_data->work_perfomance = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $feedback_data->work_perfomance));
        $feedback_data->support_of_staff = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $feedback_data->support_of_staff));
        $feedback_data->understand_technical_problem = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $feedback_data->understand_technical_problem));
        $feedback_data->behaviour_of_jk = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $feedback_data->behaviour_of_jk));
        $data['feedback_data'] = $feedback_data;
        $data['feedback_items_data'] = $this->crud->get_row_by_id('feedback_items', array('feedback_id' => $feedback_id));
        $data['party_data'] = (array) $this->crud->load_party_with_cnt_person_3($feedback_data->party_id);
        $cti_data = $this->get_challan_testing_report_installation_details($data['feedback_items_data'][0]->challan_id, $data['feedback_items_data'][0]->testing_report_id, $data['feedback_items_data'][0]->installation_id);
        $data = array_merge($cti_data,$data);
        
        $challan_items_data = array();
        if(!empty($data['feedback_items_data'])){
            foreach ($data['feedback_items_data'] as $f_key => $feedback_item){
                $where = array('challan_id' => $feedback_item->challan_id);
                $challan_item_data = $this->crud->get_row_by_id('challan_items', $where);
                $challan_items_data[] = $challan_item_data[0];
            }
        }
        $data['challan_items_data'] = $challan_items_data;
		
        $margin_left = $company_details['margin_left'];
        $margin_right = $company_details['margin_right'];
        $margin_top = $company_details['margin_top'];
        $margin_bottom = $company_details['margin_bottom'];
        $this->load->library('m_pdf');
        $param = "'utf-8','A4'";
        $pdf = $this->m_pdf->load($param);
        $pdf->pagenumPrefix = 'Page ';
        $pdf->pagenumSuffix = ' - ';
        $pdf->nbpgPrefix = ' out of ';
        $pdf->nbpgSuffix = ' pages';
        $pdf->setFooter('{PAGENO}{nbpg}');
        $pdf->AddPage(
            'P', //orientation
            '', //type
            '', //resetpagenum
            '', //pagenumstyle
            '', //suppress
            $margin_left, //margin-left
            $margin_right, //margin-right
            $margin_top, //margin-top
            $margin_bottom, //margin-bottom
            0, //margin-header
            0 //margin-footer
        );
//        echo '<pre>'; print_r($data); exit;
        $html = $this->load->view('service/feedback/feedback_print', $data, true);
        $pdf->WriteHTML($html);
        $pdf->SetHTMLFooter('');
        $pdf->Output($pdfFilePath, "I");
    }
    
    function get_challan_testing_report_installation_details($challan_id, $testing_report_id, $installation_id) {
        $where = array('installation_id' => $installation_id);
		$installation_data = $this->crud->get_row_by_id('installation', $where);
		$data['installation_data'] = $installation_data[0];
		if (!empty($data['installation_data']->installation_date)) {
			$data['installation_data']->installation_date = date('d/m/Y', strtotime($data['installation_data']->installation_date));
		}
        
		$where = array('testing_report_id' => $testing_report_id);
		$testing_report_data = $this->crud->get_row_by_id('testing_report', $where);
		$data['testing_report_data'] = $testing_report_data[0];
		if (!empty($data['testing_report_data']->testing_report_date)) {
			$data['testing_report_data']->testing_report_date = date('d/m/Y', strtotime($data['testing_report_data']->testing_report_date));
		}
		
		$challan_sales_order_id = $this->crud->get_id_by_val('challans', 'sales_order_id', 'id', $challan_id);
		$challan_proforma_invoice_id = $this->crud->get_id_by_val('challans', 'proforma_invoice_id', 'id', $challan_id);
        if(!empty($challan_sales_order_id ) && $challan_sales_order_id != 0){
            $sales_order_id = $this->crud->get_id_by_val('sales_order', 'id', 'id', $challan_sales_order_id);
            if(empty($sales_order_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else if (!empty($challan_proforma_invoice_id ) && $challan_proforma_invoice_id != 0){
            $proforma_invoice_id = $this->crud->get_id_by_val('proforma_invoices', 'id', 'id', $challan_proforma_invoice_id);
            if(empty($proforma_invoice_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else {
            $challan_data = $this->crud->get_only_challan_data($challan_id);
        }
		if (empty($challan_sales_order_id)) {
			$challan_data->proforma_invoice_no = $challan_data->sales_order_no;
			unset($challan_data->sales_order_no);
			$challan_data->proforma_invoice_date = date('d/m/Y', strtotime($challan_data->sales_order_date));
			unset($challan_data->sales_order_date);
		}
		if (!empty($challan_data->po_date)) {
			$challan_data->po_date = date('d/m/Y', strtotime($challan_data->po_date));
		}
		if (!empty($challan_data->sales_order_date)) {
			$challan_data->sales_order_date = date('d/m/Y', strtotime($challan_data->sales_order_date));
		}
		if (!empty($challan_data->challan_date)) {
			$challan_data->challan_date = date('d/m/Y', strtotime($challan_data->challan_date));
		}
        
        $challan_data->invoice_no = '';
        $challan_data->invoice_date = '';
        $invoice_data = $this->crud->get_row_by_id('invoices', array('challan_id' => $challan_id));
		if(!empty($invoice_data)){
			$challan_data->invoice_no = $invoice_data[0]->invoice_no;
			$challan_data->invoice_date = date('d/m/Y',strtotime($invoice_data[0]->invoice_date));
		}
        $challan_data->quotation_date = '';
        $challan_data->quotation_date = $this->crud->get_id_by_val('quotations', 'quotation_date', 'quotation_no', $challan_data->quotation_no);
        if (!empty($challan_data->quotation_date)) {
			$challan_data->quotation_date = date('d/m/Y', strtotime($challan_data->quotation_date));
		}
        $data['challan_data'] = $challan_data;
        return $data;
    }

}
