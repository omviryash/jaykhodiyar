<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	class File extends CI_Controller {

		 public function __construct() { 
			 parent::__construct(); 
			 $this->load->helper(array('form', 'url','download')); 
		  }
	
		public function download($file){
			$data = file_get_contents('uploads/' . $file); // Read the file's contents
			$name = $file;
			force_download($name, $data);
		}
	} 
?>
