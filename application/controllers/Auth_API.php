<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * Class Auth
 * @property AppModel $app_model
 */
class Auth_API extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Appmodel", "app_model");
        $this->load->model('chatmodule');
    }

    function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            set_page('welcome');
        } else {
            redirect('/auth/login/');
        }
    }

    /**
     * Login user on the site
     *
     * @return void
     */
    function login()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
       
		$this->form_validation->set_rules('email','email', 'trim|required|valid_email');
		$this->form_validation->set_rules('pass', 'password', 'trim|required');
		//$this->form_validation->set_rules('remember', 'Remember me', 'integer');
		
		
		if ($this->form_validation->run()) {
			$email = $_POST['email'];
			$pass = $_POST['pass'];
			$response = $this->app_model->login($email,$pass);
			
			if ($response) {
				
				
				$staff_id = $response[0]['staff_id'];
				
				$sql = "SELECT sr.staff_id,sr.module_id,sr.role_id, r.title as role, m.title as module
					FROM staff_roles sr
					INNER JOIN website_modules m ON sr.module_id = m.id
					INNER JOIN module_roles r ON sr.role_id = r.id WHERE sr.staff_id = $staff_id;";
				
				$results = $this->crud->getFromSQL($sql);
				$roles = array();
				
				foreach($results as $row)
				{
					$roles[$row->module_id][] = $row->role;
				}
				$this->session->set_userdata('is_logged_in',$response[0]);
				$this->session->set_userdata('logged_in_as',$response[0]);
				$this->session->set_userdata('user_roles',$roles);
				$this->session->set_userdata('logged_in_as_user_roles',$roles);
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','You have successfully login.');
				$this->app_model->ActiveUserStatus($email,$pass);
				$data['success']= true;
				$data['data']= $response;
				echo json_encode($data);
				exit;
				//$response);exit;
				//redirect('');
			} else {
				$data['success']= false;
				$data['errors']['invalid'] = 'Invalid email or password!';
			}
		} else {
			if (validation_errors()) {
				$error_messages = $this->form_validation->error_array();
				$data['errors'] = $error_messages;
				$data['success']= false;
			}
		}
		//$this->load->view('auth/login_form', $data);
		echo json_encode($data);
		exit;
        
    }

    /**
     *
     */
    function logout()
    {
        $logged_in = $this->session->userdata("is_logged_in");
        $this->app_model->DeActiveUserStatus($logged_in['staff_id']);
        if (isset($logged_in['visitor_last_message_id'])) {
            $this->db->where('staff_id',$logged_in['staff_id']);
            $this->db->update('staff',array('visitor_last_message_id'=>$logged_in['visitor_last_message_id']));
        }
        $this->session->unset_userdata('is_logged_in');
        session_destroy();
        redirect('auth/login');
    }

	function getAllVisitors()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		$visitors = $this->chatmodule->getAllVisitors();
		echo json_encode($visitors);
		exit;
	}
	
	function getAllAgents()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		$agent_id = $_POST['staff_id'];
		$staff = $this->chatmodule->getAllAgents($agent_id);
		echo json_encode($staff);
		exit;
	}
    /**
     *
     */
    function register()
    {
        if ($this->session->userdata('is_logged_in')) {                                    // logged in
            redirect('');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
            if ($this->form_validation->run()) {
                $response = $this->api->register($_POST);
                if ($response != false) {
                    if ($response === 'exist_username') {
                        $data['errors']['username'] = 'Username Already Exist.';
                    } elseif ($response === 'exist_email') {
                        $data['errors']['email'] = 'Username Already Exist.';
                    } else {
                        $this->session->set_userdata('is_logged_in', $response);
                        $this->session->set_flashdata('success',true);
                        $this->session->set_flashdata('message','You have successfully register.');
                        redirect('');
                    }
                } else {
                    echo 'false';
                    die;
                }
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            set_page('auth/register_form', $data);
        }
    }

    /**
     *
     */
    function profile()
    {
        if(!$this->session->userdata('is_logged_in')) {                                    // logged in
            redirect('');
        }
        $data = array();
        if (!empty($_POST)) {
            $profile_data = $_POST;
            $this->form_validation->set_rules('name', 'username', 'trim|required|alpha_numeric');
            /*$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');*/
            $this->form_validation->set_rules('contact_no', 'contact no', 'trim|required');
            if ($this->form_validation->run()) {
                if(!empty($_FILES['image']['name'])){
                    $config['upload_path'] = image_dir('staff/');
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('image')){
                        print_r($this->upload->display_errors());
                    }else{
                        $image_data = $this->upload->data();
                        $profile_data['image'] = $image_data['file_name'];
                    }
                }
                if(isset($profile_data['email'])) {
                    unset($profile_data['email']);
                }
                $this->db->where('staff_id',$profile_data['staff_id']);
                $this->db->update('staff',$profile_data);
                $query = $this->db->get_where('staff',array('staff_id',$this->session->userdata('is_logged_in')['staff_id']));
                $response = $query->row_array();
                $this->session->set_userdata('is_logged_in',$response);
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            if(!empty($data)){
                set_page('profile', $data);
            }else{
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','You have successfully save profile.');
                redirect('auth/profile');
            }
        } else {
            $query = $this->db->get_where('staff',array('staff_id',$this->session->userdata('is_logged_in')['staff_id']));
            set_page('profile',$query->row());
        }
    }

    function change_password()
    {
        $data = array();
        //print_r($_POST
		$staff_id = $_POST['staff_id'];
		$query = $this->db->get_where('staff',array('staff_id'=>$staff_id,'pass'=>md5($_POST['old_pass'])));
		if($query->num_rows() > 0){
			//return true;				
			$this->db->where('staff_id',$_POST['staff_id']);
			$this->db->update('staff',array('pass'=>md5($_POST['new_pass'])));
			//$this->session->set_flashdata('success',true);
			//$this->session->set_flashdata('message','You have successfully changed password!');
			//redirect('auth/profile');
			$message['success'] = true;
			$message['msg'] = 'Update password successfully';
		}else{
			$message['success'] = false;
			$message['msg'] = 'Please enter correct current password';
		} 
		echo json_encode($message);
		exit;       
    }

    function check_old_password($old_pass){
        $staff_id = $_POST['staff_id'];
        $query = $this->db->get_where('staff',array('staff_id'=>$staff_id,'pass'=>md5($old_pass)));
        if($query->num_rows() > 0){
            return true;
        }else{
            $this->form_validation->set_message('check_old_password', 'wrong old password.');
            return false;
        }
    }

    function change_profile_pic(){
        if(!empty($_FILES['profile_pic']['name'])){
            $profile_pic = $this->api->upload_image('profile_pic',image_dir('profile-pics/'));
            if ($profile_pic != false) {
                redirect('');
            } else {
                $data['errors']['profile_pic'] = 'Plz select valid image.';
            }
            set_page('profile',$data);
        }else{
            $data['errors']['profile_pic'] = 'Plz select image.';
            set_page('profile',$data);
        }
    }

    function forgot_password(){
        $data = array();
        if(!empty($_POST)){
            $this->form_validation->set_rules('email', 'email', 'trim|required');
            if ($this->form_validation->run()) {
                $response_data = $this->api->forgot_password($_POST);
                if ($response_data == false) {
                    $data['errors']['email'] = 'Incorrect email plz try again.';
                } elseif ($response_data === 'not_send') {
                    $data['errors']['email'] = 'Please try again mail not sanded.';
                }
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            if(!empty($data)){
                set_page('auth/forgot_password_form',$data);
            }else{
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Plz check inbox.');
                redirect('');
            }
        }else{
            set_page('auth/forgot_password_form');
        }
    }
	
	public function change_seesion_staff() {
		$data = array();
		$query = $this->db->query("select * from staff where staff_id='".$_POST['staff_id']."' AND active !='0'");
		$response = $query->result_array();
		if ($response) {
			$this->session->set_userdata('is_logged_in',$response[0]);
			$staff_id = $response[0]['staff_id'];
                    
			$sql = "
				SELECT 
						sr.staff_id,sr.module_id,sr.role_id, r.title as role, m.title as module
				FROM staff_roles sr
				INNER JOIN website_modules m ON sr.module_id = m.id
				INNER JOIN module_roles r ON sr.role_id = r.id WHERE sr.staff_id = $staff_id;    
			";
			
			$results = $this->crud->getFromSQL($sql);
			$roles = array();
			foreach($results as $row)
			{
				$roles[$row->module_id][] = $row->role;
			}
			$this->session->set_userdata('user_roles',$roles);
			$data['success'] = true;
		} else {
			$data['success'] = false;
		}
		echo json_encode($data);
		exit;
	}
	
	function send_visitor_msg(){
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
        $data = array();
       // $logged_in = $this->session->userdata("is_logged_in");
        $data['from_id'] = $_POST['staff_id'];
        $data['from_table'] = $_POST['from_table'];
        $data['to_id'] = $_POST['to_id'];
        $data['to_table'] = $_POST['to_table'];
        $data['message'] = $_POST['msg'] ? $_POST['msg'] : '';
        $data['date_time'] = date('Y-m-d H:i:s');
       
        $last_message_id = $this->chatmodule->addChatMessage($data);
        //echo $last_message_id;exit;
        $logged_in['visitor_last_message_id'] = $last_message_id;       
        $name = $logged_in['name'];
        $image = $logged_in['image'];
        $chat_html = array('name'=>$name,'message'=>$_POST['msg'],'image'=>$image);
        $other_agent_chat_html = array('name'=>$name,'message'=>$_POST['msg'],'image'=>$image);

        $to_data = $this->chatmodule->fetch_visitor_by_id($data['to_id']);
        $from_data = $this->chatmodule->getStaffById($data['from_id']);

        if($to_data->name == '')
        {
			$to_name = 'Visitor';
		}
		else
		{
			$to_name = $to_data->name.' (Visitor)';
		}
        $to_image = 'default.png';
        $to_session_id = $this->chatmodule->fetch_session_id_by_visitor($_POST['to_id']);;
        $from_name = $from_data[0]['name'];
        $from_image = $from_data[0]['image'];

        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
		$client->initialize();
        $client->emit('new_message_client_to_visitor', ['to_session_id'=>$to_session_id,
														'from_id' => $data['from_id'],
														'to_name' => $to_name,
														'to_image' => $to_image,
														'other_agent_chat_html' => $other_agent_chat_html,
														'chat_html' => $chat_html,
														'date_time' => $data['date_time'],
														'msg' => $data['message'],
														'to_id'=> $data['to_id']]);
        $client->close();
        
        echo json_encode(array('success' => true,'chat_html' => $chat_html));
        exit;
    }
    
    function getAdminChat()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		
        $staff_id = $_POST['logged_in'];
        /*if (isset($logged_in['lastid'])) {
            $lastid = $logged_in['lastid'];
        } else {*/
            $lastid = 0;
        //}
        $userid = $_POST['from_id'];
        
        
        $query = $this->db->query("SELECT `message`.*, `staff`.`image`, `staff`.`name` as `from_name` 
									FROM `message` LEFT JOIN `staff` ON `message`.`from_id` = `staff`.`staff_id` 
									WHERE `message`.`id` >0 AND `message`.`to_table` = 'admin' 
									AND `message`.`to_id` = $userid AND `message`.`from_id` = $staff_id 
									OR `message`.`to_id` = $staff_id  AND `message`.`from_id` = $userid
									ORDER BY `message`.`date_time` ASC");
									
		$row = $query->row();
		$cnt=0;
		if ($query->num_rows()>0)
		{
			foreach ($query->result() as $row)
			{
				$messages[$cnt] = $row;
				/*echo $row->name;
				echo $row->body;*/
				$cnt++;
			}
		}
		
        //$messages = $this->chatmodule->getMyMessage($staff_id, $lastid, $userid);
        /*if ($messages) {
            if (isset($logged_in['lastid'])) {
                $logged_in['lastid'] = $messages[count($messages) - 1]['id'];
            } else {
                $logged_in['lastid'] = $messages[count($messages) - 1]['id'];
            }
           // $this->session->set_userdata('is_logged_in', $logged_in);
        }*/
        echo json_encode($messages);
        exit;
    }
    
    function getAdminChat23()
    {      
		$messages = $this->chatmodule->getMyMessage(1, 4, 5);
		print_r($messages);
    }
    
    function addAdminChat()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		
        $data = array();
        //$nowdate = new DateTime('y-m-d H:i:s');
        $nowdate = date('Y-m-d H:i:s');
        //$logged_in = $_POST["is_logged_in"];
        $data['from_id'] = $_POST['staff_id'];
        $data['from_table'] = $_POST['from_table'];
        $data['to_id'] = $_POST['to_id'];
        $data['to_table'] = $_POST['to_table'];
        $data['message'] = $_POST['msg'] ? $_POST['msg'] : '';
        $data['date_time'] = $nowdate;
        $messages = $this->chatmodule->addChatMessage($data);

        $to_data = $this->chatmodule->getStaffById($data['to_id']);
        $from_data = $this->chatmodule->getStaffById($data['from_id']);
        $to_name = $to_data[0]['name'];
        $to_image = $to_data[0]['image'];
        $from_name = $from_data[0]['name'];
        $from_image = $from_data[0]['image'];
		//print_r($data);
        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
		$client->initialize();
        $client->emit('new_message_client', ['to_id'=>$data['to_id'],
											'to_table'=>$data['to_table'],
											'to_name'=>$to_name,
											'to_image'=>$to_image,
											'from_id' => $data['from_id'], 
											'from_table' => $data['from_table'], 
											'from_name' => $from_name, 
											'from_image' => $from_image,
											'msg' => $data['message'], 
											'date_time' => date('d M Y H:i:s')]);
        $client->close();
        
        echo json_encode($data);
        exit;
    }
}
