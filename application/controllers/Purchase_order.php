<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 * @property M_pdf $m_pdf
 */
class Purchase_order extends CI_Controller
{

	protected $staff_id = 1;
	function __construct(){
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}
    
    /**
	 *    Purchase Order
	 */
    function add($order_id = ''){
        $data = array();
        if(!empty($order_id)){
            if ($this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "edit")) {
                $purchase_order_data = $this->crud->get_row_by_id('purchase_order', array('order_id' => $order_id));
                $purchase_order_data = $purchase_order_data[0];
                $purchase_order_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_order_data->created_by));
                $purchase_order_data->created_at = substr($purchase_order_data->created_at, 8, 2) .'-'. substr($purchase_order_data->created_at, 5, 2) .'-'. substr($purchase_order_data->created_at, 0, 4);
                $purchase_order_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_order_data->updated_by));
                $purchase_order_data->updated_at = substr($purchase_order_data->updated_at, 8, 2) .'-'. substr($purchase_order_data->updated_at, 5, 2) .'-'. substr($purchase_order_data->updated_at, 0, 4);
                $purchase_order_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_order_data->approved_by));
                $purchase_order_data->approved_at = substr($purchase_order_data->approved_at, 8, 2) .'-'. substr($purchase_order_data->approved_at, 5, 2) .'-'. substr($purchase_order_data->approved_at, 0, 4);
                $data['purchase_order_data'] = $purchase_order_data;
                $lineitems = array();
                $where = array('order_id' => $order_id);
                $purchase_order_lineitems = $this->crud->get_row_by_id('purchase_order_items', $where);
                foreach($purchase_order_lineitems as $purchase_order_lineitem){
                    $lineitems[] = $purchase_order_lineitem->item_data;
                }
                $data['purchase_order_lineitems'] = implode(',', $lineitems);
//                echo '<pre>';print_r($data); exit;
                set_page('purchases/order/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
		} else {
            if($this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"add")) {
                $data['order_no'] = $this->crud->get_next_autoincrement('purchase_order');
                set_page('purchases/order/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
	}
    
    function save_purchase_order(){
		$post_data = $this->input->post();
//		echo '<pre>';print_r($post_data); exit;
    
        $line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
//		print_r($line_items_data); exit;
		
        /*--------- Convert Date as Mysql Format -------------*/
		$_POST['purchase_order_data']['order_date'] = date("Y-m-d", strtotime($_POST['purchase_order_data']['order_date']));
		$_POST['purchase_order_data']['purchase_quote_ref'] = !empty($_POST['purchase_order_data']['purchase_quote_ref']) ? $_POST['purchase_order_data']['purchase_quote_ref'] : NULL;
		$_POST['purchase_order_data']['round_off'] = !empty($_POST['purchase_order_data']['round_off']) ? $_POST['purchase_order_data']['round_off'] : NULL;
		$_POST['purchase_order_data']['purchase_project_id'] = !empty($_POST['purchase_order_data']['purchase_project_id']) ? $_POST['purchase_order_data']['purchase_project_id'] : NULL;
		$_POST['purchase_order_data']['purchase_project_qty'] = !empty($_POST['purchase_order_data']['purchase_project_qty']) ? $_POST['purchase_order_data']['purchase_project_qty'] : NULL;
		$_POST['purchase_order_data']['note'] = !empty($_POST['purchase_order_data']['note']) ? $_POST['purchase_order_data']['note'] : NULL;
        $_POST['purchase_order_data']['mach_deli_min_weeks_date'] = date("Y-m-d", strtotime($_POST['purchase_order_data']['mach_deli_min_weeks_date']));
		$_POST['purchase_order_data']['mach_deli_max_weeks_date'] = date("Y-m-d", strtotime($_POST['purchase_order_data']['mach_deli_max_weeks_date']));
		if(isset($_POST['purchase_order_data']['item_group_id']) && !empty($_POST['purchase_order_data']['item_group_id'])){
            unset($_POST['purchase_order_data']['item_group_id']);
        }
		if(isset($_POST['purchase_order_data']['order_id']) && !empty($_POST['purchase_order_data']['order_id'])){
            
            $order_no = $_POST['purchase_order_data']['order_no'];
            $purchase_order_result = $this->crud->get_id_by_val('purchase_order', 'order_id', 'order_no', $order_no);
            if(!empty($purchase_order_result) && $purchase_order_result != $_POST['purchase_order_data']['order_id']){
                echo json_encode(array("success" => 'false', 'msg' => 'Purchase Order No. Already Exist!'));
                exit;
            }
            
			$_POST['purchase_order_data']['updated_at'] = $this->now_time;
			$_POST['purchase_order_data']['updated_by'] = $this->staff_id;
			$this->db->where('order_id', $_POST['purchase_order_data']['order_id']);
			$result = $this->db->update('purchase_order', $_POST['purchase_order_data']);
			$order_id = $_POST['purchase_order_data']['order_id'];
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Purchase Order Updated Successfully');
                $where_array = array("order_id" => $order_id);
                $this->crud->delete("purchase_order_items", $where_array);
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
                    $add_lineitem['order_id'] = $order_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['item_data'] = json_encode($lineitem);
                    $add_lineitem['updated_at'] = $this->now_time;
                    $add_lineitem['created_at'] = $this->now_time;
                    $this->crud->insert('purchase_order_items',$add_lineitem);
				}
			}
		} else {
            
            $order_no = $_POST['purchase_order_data']['order_no'];
            $purchase_order_result = $this->crud->get_id_by_val('purchase_order', 'order_id', 'order_no', $order_no);
            if(!empty($purchase_order_result)){
                echo json_encode(array("success" => 'false', 'msg' => 'Purchase Order No. Already Exist!'));
                exit;
            }
            
            $dataToInsert = $_POST['purchase_order_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
			$result = $this->db->insert('purchase_order', $dataToInsert);
            $order_id = $this->db->insert_id();
			if($result){
				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Purchase Order Added Successfully');
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
                    $add_lineitem['order_id'] = $order_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['item_data'] = json_encode($lineitem);
                    $add_lineitem['updated_at'] = $this->now_time;
                    $add_lineitem['created_at'] = $this->now_time;
                    $this->crud->insert('purchase_order_items',$add_lineitem);
				}
			}
		}
		print json_encode($return);
		exit;
	}
    
    function order_list(){
		$where_array = array('module_id' => PURCHASE_ORDER_MODULE_ID,'title' => 'add');
		$module_role_id = $this->crud->get_column_value_by_id('module_roles','id',$where_array);
		$where_array = array('module_id' => PURCHASE_ORDER_MODULE_ID,'role_id' => $module_role_id);
		$staff_ids_arr = $this->crud->get_columns_val_by_where('staff_roles','staff_id',$where_array);
		$staff_ids = array();
		foreach ($staff_ids_arr as $rows) {
			foreach ($rows as $data) {
				$staff_ids[] = $data;
			}
		}
		$users = $this->crud->getFromSQL('SELECT * FROM `staff` WHERE `staff_id` in ('.implode(",",$staff_ids).') AND `active` != 0 ');
		if($this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"view")) {
			set_page('purchases/order/order_list', array('users' => $users));
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
    
    function orders_datatable(){
		$requestData = $_REQUEST;
		$config['table'] = 'purchase_order e';
		$config['select'] = 'e.order_id, e.order_no, p.supplier_name,e.order_date,e.created_at,e.is_approved';
		$config['column_order'] = array(null, 'e.order_no', 'p.supplier_name', 'e.order_date','e.created_at');
		$config['column_search'] = array('e.order_no', 'p.supplier_name', 'DATE_FORMAT(e.order_date,"%d-%m-%Y")', 'DATE_FORMAT(e.created_at,"%d-%m-%Y")');
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$config['likes'][] = array('e.order_no' => $requestData['columns'][1]['search']['value']);
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$config['likes'][] = array('p.supplier_name' => $requestData['columns'][2]['search']['value']);
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			$config['likes'][] = array('e.order_date' => $requestData['columns'][3]['search']['value']);
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {
			$config['likes'][] = array('e.created_at' => $requestData['columns'][4]['search']['value']);
		}
		
		$config['joins'][1] = array('join_table' => 'supplier p', 'join_by' => 'p.supplier_id = e.supplier_id', 'join_type' => 'left');
		$config['order'] = array('e.order_id' => 'desc');
		$config['where_string'] = ' 1 = 1 ';
		//$user_id = $this->input->get_post('user_id');
//		if ($user_id != '' && $user_id != 'all') {
//			$config['where_string'] .= " AND e.created_by = '" . $user_id . "' ";
//		}
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		$role_delete = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "edit");

		foreach ($list as $order) {
			$row = array();
			$action = '';
			if ($role_edit && $role_delete) {
				$action .= '<a href="' . BASE_URL . "purchase_order/add/" . $order->order_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
				$action .= ' | <a onclick="return confirm(\'Are you sure ?\');" href="' . BASE_URL . "purchase_order/delete_purchase_order/" . $order->order_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
			} elseif ($role_edit) {
				$action .= '<a href="' . BASE_URL . "purchase_order/add/" . $order->order_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			} elseif ($role_delete) {
				$action .= ' | <a onclick="return confirm(\'Are you sure ?\');" href="' . BASE_URL . "purchase_order/delete_purchase_order/" . $order->order_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
			}
            if ($order->is_approved == 1 ) {
                $action .= ' | <a href="' . BASE_URL . 'purchase_order/purchases_order_print/' . $order->order_id . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
                $action .= ' | <a href="' . base_url('purchase_order/purchases_order_email/' . $order->order_id) . '" class="email_button">Email</a>';
            }
			
			$row[] = $action;
			$row[] = '<a href="' . base_url('purchase_order/add/' . $order->order_id . '?view') . '" >'.$order->order_no.'</a>';
			$row[] = '<a href="' . base_url('purchase_order/add/' . $order->order_id . '?view') . '" >'.$order->supplier_name.'</a>';
			$row[] = '<a href="' . base_url('purchase_order/add/' . $order->order_id . '?view') . '" >'.date('d-m-Y', strtotime($order->order_date)).'</a>';
			$row[] = '<a href="' . base_url('purchase_order/add/' . $order->order_id . '?view') . '" >'.date('d-m-Y', strtotime($order->created_at)).'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
    }
    
    function delete_purchase_order($id){
		$role_delete = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "delete");
		if ($role_delete) {
			$where_array = array("order_id" => $id);
			$result = $this->crud->delete("purchase_order", $where_array);
            if($result['error'] == 'Error'){
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message','You cannot delete this Item. This Item has been used.');
            } elseif($result['success'] == 'Deleted') {
                $this->crud->delete("purchase_order_items", $where_array);
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Purchase Order has been deleted');
            }
			redirect(BASE_URL . "purchase_order/order_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function update_purchase_order_status_to_approved(){
		if ($_POST['order_id'] != '') {
            $order_id = $_POST['order_id'];
			$data = array();
            $data['is_approved'] = 1;
            $data['approved_by'] = $this->staff_id;
			$data['approved_at'] = $this->now_time;
            $this->db->where('order_id', $order_id);
			$this->db->update('purchase_order', $data);
			echo json_encode(array('success' => true, 'message' => 'Purchase Order approved successfully!'));
			exit();
		} else {
			redirect("purchase_order/order_list"); exit;
			exit();
		}
	}

	function update_purchase_order_status_to_disapproved(){
		if ($_POST['order_id'] != '') {
            $order_id = $_POST['order_id'];
            $data = array();
            $data['is_approved'] = 0;
            $data['approved_by'] = null;
			$data['approved_at'] = null;
			$this->db->where('order_id', $order_id);
			$this->db->update('purchase_order', $data);
			echo json_encode(array('success' => true, 'message' => 'Purchase Order disapproved successfully!'));
			exit();
		} else {
			redirect("purchase_order/order_list"); exit;
			exit();
		}
	}
    
    function purchases_order_print($id){
        $this->print_pdf($id,'print');
    }
    
    function purchases_order_email($id){
        $this->print_pdf($id,'email');
	}
    
    function print_pdf($id,$type){
		$purchases_order_data = $this->get_purchases_order_detail($id);
		$company_details = $this->applib->get_company_detail();
		$purchases_order_items = $this->get_purchases_order_items($id);
        $purchases_order_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchases_order_data->approved_by));
		$item_data_arr = array();
		if (!empty($purchases_order_items)) {
			foreach ($purchases_order_items as $key => $item_row) {
				$item_data_arr[$key] = json_decode($item_row['item_data']);
			}
		}
		$item_array = array();
		if (!empty($item_data_arr)) {
			foreach ($item_data_arr as $key => $item_row) {
                $item_row->uom = $item_row->rate_uom_name;
				$item_row->hsn = $this->crud->get_id_by_val('purchase_items','hsn_code','id', $item_row->item_id);
                $item_row->item_code = $this->crud->get_id_by_val('purchase_items','item_code','id', $item_row->item_id);
                $item_row->required_uom = $item_row->uom_name;
                if(isset($item_row->po_reference_qty_uom) && !empty($item_row->po_reference_qty_uom)){
                    $item_row->po_reference_qty_uom = $this->crud->get_id_by_val('uom','uom','id', $item_row->po_reference_qty_uom);
                }
			}
		}
		//echo '<pre>';print_r($item_data_arr);exit;
		$this->load->library('m_pdf');
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		if (!empty($purchases_order_items)) {
			foreach ($purchases_order_items as $key => $item_row) {
				$param = "'utf-8','A4'";
				$pdf = $this->m_pdf->load($param);
				$pdf->pagenumPrefix = 'Page ';
				$pdf->pagenumSuffix = ' - ';
				$pdf->nbpgPrefix = ' out of ';
				$pdf->nbpgSuffix = ' pages';
                $pdf->SetHTMLFooter('
                <div style="height:50px; vertical-align: bottom; font-family: serif; font-size: 11px; color: #000000; font-weight: bold; border-top: 1px solid #000;">
                <div style="float: left; width:66%;">This is a System Generated Document / Approved PO. Manual Signature Not Required.</div>
                <div style="float: right; width:20%;">{PAGENO}/{nbpg}</div>
                </div>');
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				if ($key == 0) {
					$other_items = $this->get_purchases_order_sub_items($id);
					$html = $this->load->view('purchases/order/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'item_array' => $item_data_arr, 'terms_and_conditions' => $terms_and_conditions, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
				} else {
					$html = $this->load->view('purchases/order/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => $item_row, 'item_array' => $item_data_arr, 'terms_and_conditions' => $terms_and_conditions, 'company_details' => $company_details, 'is_first_item' => false), true);
				}
				$pdf->WriteHTML($html);

				$this->db->select('*');
				$this->db->from('item_documents');
				$this->db->where('item_id', $item_row['item_id']);
				$this->db->where('document_type', strtolower($purchases_order_data->purchases) . '_order');
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $key => $page_detail_row) {
						$print_export_items = $this->load->view('purchases/order_print_price_table', array('purchases_order_data' => $purchases_order_data, 'item_row' => $item_row), true);
						$vars = array(
							'{{Price_Table}}' => $print_export_items,
						);
						$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
						$page_detail_html = $page_detail_row->detail;
						$pdf->AddPage(
							'P', //orientation
							'', //type
							'', //resetpagenum
							'', //pagenumstyle
							'', //suppress
							$margin_left, //margin-left
							$margin_right, //margin-right
							$margin_top, //margin-top
							$margin_bottom, //margin-bottom
							0, //margin-header
							0 //margin-footer
						);
						$pdf->WriteHTML($page_detail_html);
					}
				}
                if($type == 'print'){
                    $pdfFilePath = "purchases_order.pdf";
                    $pdf->Output($pdfFilePath, "I");
                } elseif($type == 'email'){
                    $pdfFilePath = "./uploads/purchases_order_" . $id . ".pdf";
                    $pdf->Output($pdfFilePath, "F");
                    break;
                }
			}
		} else {
			$html = $this->load->view('purchases/order/order_pdf', array('purchases_order_data' => $purchases_order_data, 'purchases_order_item' => array(), 'company_details' => $company_details, 'is_first_item' => false), true);
			$param = "'utf-8','A4'";
			$pdf = $this->m_pdf->load($param);
            $pdf->pagenumPrefix = 'Page ';
            $pdf->pagenumSuffix = ' - ';
            $pdf->nbpgPrefix = ' out of ';
            $pdf->nbpgSuffix = ' pages';
            $pdf->SetHTMLFooter('
                <div style="height:50px; vertical-align: bottom; font-family: serif; font-size: 11px; color: #000000; font-weight: bold; border-top: 1px solid #000;">
                <div style="float: left; width:66%;">This is a System Generated Document / Approved PO. Manual Signature Not Required.</div>
                <div style="float: right; width:20%;">{PAGENO}/{nbpg}</div>
                </div>');
			$pdf->AddPage(
				'P', //orientation
				'', //type
				'', //resetpagenum
				'', //pagenumstyle
				'', //suppress
				$margin_left, //margin-left
				$margin_right, //margin-right
				$margin_top, //margin-top
				$margin_bottom, //margin-bottom
				0, //margin-header
				0 //margin-footer
			);
			$pdf->WriteHTML($html);
            if($type == 'print'){
                $pdfFilePath = "purchases_order.pdf";
                $pdf->Output($pdfFilePath, "I");
            } elseif($type == 'email'){
                $pdfFilePath = "./uploads/purchases_order_" . $id . ".pdf";
                $pdf->Output($pdfFilePath, "F");
            }
		}
        if($type == 'email'){
            $this->db->where('order_id', $id);
            $this->db->update('purchase_order', array('pdf_url' => $pdfFilePath));
            redirect(base_url() . 'mail-system3/document-mail/purchases-order/' . $id);
        }
	}

    function get_purchases_order_detail($id){
		$select = "po.*,sp.supplier_name as supplier_name, sp.address, sp.tel_no, sp.contact_no, sp.cont_person, sp.gstin, sp.pincode ,sp.email_id, st.name as assigned_by, sp.cin_no, city.city,state.state,country.country,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('purchase_order po');
		$this->db->join('staff st', 'st.staff_id = po.created_by', 'left');
		$this->db->join('supplier sp', 'sp.supplier_id = po.supplier_id', 'left');
		$this->db->join('city', 'city.city_id = sp.city_id', 'left');
		$this->db->join('state', 'state.state_id = sp.state_id', 'left');
		$this->db->join('country', 'country.country_id = sp.country_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = po.created_by', 'left');
		$this->db->where('po.order_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}
    
    function get_purchases_order_items($id){
		$this->db->select('poi.*');
		$this->db->from('purchase_order_items poi');
		//~ $this->db->join('uom u', 'u.id = poi.uom_id');
		//~ $this->db->where('ic.item_category', 'Finished Goods');
		$this->db->where('poi.order_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}
    
    function get_purchases_order_sub_items($id){
		$this->db->select('poi.*');
		$this->db->from('purchase_order_items poi');
		//~ $this->db->join('item_category ic', 'ic.id = poi.item_category_id');
		//~ $this->db->where('ic.item_category != ', 'Finished Goods');
		$this->db->where('poi.order_id', $id);
		$query = $this->db->get();
		$other_items = array();
		if ($query->num_rows() > 0) {
			$other_items = $query->result_array();
		}
		return $other_items;
	}
    
    public function ajax_load_supplier($supplier_id){
		$this->load->helper('url');
		$row = $this->crud->load_supplier_detail($supplier_id);
		$return = array(
			'supplier_id' => $row->supplier_id,
			'supplier_name' => $row->supplier_name,
			'address' => $row->address,
			'city' => $row->city,
			'city_id' => $row->city_id,
			'state' => $row->state,
			'state_id' => $row->state_id,
			'country' => $row->country,
			'country_id' => $row->country_id,
			'email_id' => $row->email_id,
			'tel_no' => $row->tel_no,
			'contact_no' => $row->contact_no,
			'pincode' => $row->pincode,
			'cont_person' => $row->cont_person
		);
		//echo "<pre>";print_r($return);exit;
		print json_encode($return);
		exit;
	}
    
    public function ajax_load_purchase_item_details($item_id = 0){
		if ($item_id != 0) {
			$this->load->helper('url');
			$row = $this->crud->load_purchase_item_details_where($item_id);

			$return = array(
				'id' => $row->id,
				'item_name' => $row->item_name,
				'item_group' => $row->item_group,
				'item_code' => $row->item_code,
				'item_make_id' => $row->item_make_id,
				'uom' => $row->required_uom,
//                'set_qty'=> $row->required_qty,
				'quantity' => $row->required_qty,
				'reference_qty' => $row->reference_qty,
				'reference_qty_uom' => $row->reference_qty_uom,
				'rate' => $row->rate,
				'sales_rate' => $row->sales_rate,
				'igst' => $row->igst,
				'sgst' => $row->sgst,
				'cgst' => $row->cgst,
				'rate_uom' => $row->uom,

			);
			print json_encode($return);
			exit;
		}
	}
    
    function get_purchase_project_items($purchase_project_id, $purchase_project_qty = '1', $state_id = ''){
        if ($purchase_project_id != 0) {
			$purchase_project_items = $this->crud->get_row_by_id('purchase_project_items', array('project_id' => $purchase_project_id));
            $project_name = $this->crud->get_id_by_val('purchase_project', 'project_name', 'project_id', $purchase_project_id);
            $item_group_id = $this->crud->get_column_value_by_id('purchase_project', 'item_group_id', array('project_id' => $purchase_project_id));
            $purchase_project_item_arr = array();
            if(!empty($purchase_project_items)){
                foreach ($purchase_project_items as $purchase_project_item ){
                    $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $purchase_project_item->item_id));
                    $purchase_item = $purchase_item[0];
                    $item_arr = array();
                    $item_arr['project_name'] = $project_name;
                    $item_arr['item_group_id'] = $item_group_id;
                    $item_arr['purchase_project_qty'] = $purchase_project_qty;
                    $item_arr['item_id'] = $purchase_project_item->item_id;
                    $item_arr['uom_id'] = $purchase_project_item->uom_id;
                    $item_arr['uom_name'] = (!empty($purchase_project_item->uom_id)) ? $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $purchase_project_item->uom_id)) : '';
                    $item_arr['id'] = '';
                    $item_arr['item_code'] = $purchase_item->item_code;
                    $item_arr['item_name'] = $purchase_item->item_name;
                    $item_arr['item_make_id'] = (!empty($purchase_item->item_make_id)) ? $purchase_item->item_make_id : '';
                    $item_arr['item_make_name'] = (!empty($purchase_item->item_make_id)) ? $this->crud->get_id_by_val('challan_item_make','make_name','id', $purchase_item->item_make_id) : '';
                    $item_arr['item_description'] = '';

                    $item_arr['set_qty'] = $purchase_project_item->qty;
                    $item_arr['quantity'] = $purchase_project_item->qty * $purchase_project_qty;
                    $item_arr['reference_qty'] = $purchase_item->reference_qty;
                    $item_arr['rate_uom'] = $purchase_item->uom;
                    $item_arr['rate_uom_name'] = (!empty($purchase_item->uom)) ? $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $purchase_item->uom)) : '';
//                    $item_arr['po_reference_qty'] = $purchase_project_item->project_reference_qty * $purchase_project_qty;
//                    $item_arr['po_reference_qty_uom'] = $purchase_project_item->project_reference_qty_uom;
//                    $item_arr['po_reference_qty_uom_name'] = !empty($purchase_project_item->project_reference_qty_uom_name) ? $purchase_project_item->project_reference_qty_uom_name : '';
                    $item_arr['item_rate'] = $purchase_item->rate;
                    $item_arr['amount'] = $item_arr['quantity'] * $item_arr['item_rate'] * $item_arr['reference_qty'];

                    $item_arr['disc_per'] = $purchase_item->discount;
                    $item_arr['disc_value'] = $item_arr['amount'] * $item_arr['disc_per'] / 100;

                    $taxable_value = $item_arr['amount'] - $item_arr['disc_value'];
                    $item_arr['igst'] = '0';
                    $item_arr['igst_amount'] = '0';
                    $item_arr['cgst'] = '0';
                    $item_arr['cgst_amount'] = '0';
                    $item_arr['sgst'] = '0';
                    $item_arr['sgst_amount'] = '0';
                    if($state_id == '' || $state_id == GUJARAT_STATE_ID){
                        $item_arr['cgst'] = !empty($purchase_item->cgst) ? $purchase_item->cgst : '0';
                        $item_arr['cgst_amount'] = $taxable_value * $item_arr['cgst'] / 100;
                        $item_arr['sgst'] = !empty($purchase_item->sgst) ? $purchase_item->sgst : '0';
                        $item_arr['sgst_amount'] = $taxable_value * $item_arr['sgst'] / 100;
                    } else {
                        $item_arr['igst'] = !empty($purchase_item->igst) ? $purchase_item->igst : '0';
                        $item_arr['igst_amount'] = $taxable_value * $item_arr['igst'] / 100;
                    }

                    $item_arr['net_amount'] = $taxable_value + $item_arr['cgst_amount'] + $item_arr['sgst_amount'] + $item_arr['igst_amount'];

                    //{"item_id":"23","uom_id":"5","id":"","item_code":"","
                    //item_name":"1NC Auxiliary contact block for 3TF30 to 3TF35 \t\t\t",
                    //"item_description":"","quantity":"1","item_rate":"68","disc_per":"0","disc_value":"0",
                    //"igst":"0","igst_amount":"0","cgst":"9","cgst_amount":"6","sgst":"9","sgst_amount":"6",
                    //"amount":"68","net_amount":"80"}
                    $purchase_project_item_arr[] = json_encode($item_arr);
                }
            }
//            echo '<pre>'; print_r($purchase_project_item_arr); exit;
            echo $project_item_arr = '['.implode(',', $purchase_project_item_arr).']';
            exit;
		}
	}

    
}
