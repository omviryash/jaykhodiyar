<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Feedback_reply extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');

	}
    
    function add($feedback_id = '',$view = ''){
        $data = array();
        if (isset($feedback_id)) {
            if ($this->app_model->have_access_role(FEEDBACK_MENU_ID, "edit") || $this->app_model->have_access_role(FEEDBACK_MENU_ID, "view")) {
                $feedback_data = $this->crud->get_row_by_id('user_feedback', array('feedback_id' => $feedback_id));
                $feedback_data = $feedback_data[0];
                $data['feedback_data'] = $feedback_data;
                $data['view'] = $view;
//                if($feedback_data->created_by != 1){
                    $party = $this->crud->get_row_by_id('party', array('party_id' => $feedback_data->created_by));
                    $party = $party[0];
                    $data['party_data'] = $party;
//                }
//                echo '<pre>'; print_r($data); exit;
                set_page('feedback/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            if ($this->app_model->have_access_role(FEEDBACK_MENU_ID, "add") || $this->app_model->have_access_role(FEEDBACK_MENU_ID, "view")) {
                set_page('feedback/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }
    
    function save_feedback() {
        $return = array();
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
        $post_data['feedback_date'] = (isset($post_data['feedback_date']) && !empty($post_data['feedback_date'])) ? date('Y-m-d', strtotime($post_data['feedback_date'])) : NULL;
        if (isset($post_data['feedback_id']) && !empty($post_data['feedback_id'])) {
            $post_data['updated_at'] = $this->now_time;
            $post_data['updated_by'] = $this->staff_id;
            $post_data['feedback_from'] = '1';
            $where_array['feedback_id'] = $post_data['feedback_id'];
            $result = $this->crud->update('user_feedback', $post_data, $where_array);
            if ($result) {
                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Feedback Updated Successfully');
            }
        } else {
            $post_data['created_at'] = $this->now_time;
            $post_data['created_by'] = $this->staff_id;
            $post_data['feedback_from'] = '1';
            $result = $this->crud->insert('user_feedback', $post_data);
            if ($result) {
                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Feedback Added Successfully');
            } else {
                $return['error'] = "errorAdded";
            }
        }
        print json_encode($return);
        exit;
    }
    
    function feedback_datatable() {
        $post_data = $this->input->post();
        $config['table'] = 'user_feedback f';
        $config['select'] = 'f.*, IFNULL(`p`.`party_name`, "Jk&nbsp;Admin") AS `created_by`,pp.party_id AS f_party_id,pp.party_name AS f_party_name';
        $config['joins'][] = array('join_table' => 'staff s', 'join_by' => 's.staff_id = f.feedback_created', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = f.feedback_created', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party pp', 'join_by' => 'pp.party_id = f.created_by', 'join_type' => 'left');
        $config['column_search'] = array('p.party_name', 'f.feedback_date', 'f.note');
        $config['column_order'] = array('f.feedback_id', 'p.party_name', 'f.feedback_date', 'f.note');
        $config['order'] = array('f.feedback_id' => 'desc');
        $config['group_by'] = 'f.feedback_id';
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
//        echo $this->db->last_query(); exit;
        $isEdit = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "delete");
        foreach ($list as $feedback) {
            $assigned_name = '';
            $feedback_data = $this->crud->getFromSQL('SELECT r.reply_id,IFNULL(`p`.`party_name`, "Jk&nbsp;Admin") AS `assign_name` FROM reply r LEFT JOIN party p ON p.party_id=r.assign_to_id WHERE `r`.`reply_id` IN (SELECT MAX(rr.reply_id) FROM reply rr GROUP BY rr.feedback_id) AND r.feedback_id='. $feedback->feedback_id);
            if(!empty($feedback_data)){
                $assigned_name = $feedback_data[0]->assign_name;
            }
//            echo $this->db->last_query(); exit;
            $row = array();
            $action = '';
//            if($isEdit){ 
//                $action .= '<a href="' . base_url("feedback_reply/add/" . $feedback->feedback_id) . '" class="edit_button btn-primary btn-xs" data-href="#" ><i class="fa fa-edit"></i></a>';
//            }
            if($isDelete){
                $action .= ' <a href="javascript:void(0);" class="delete_feedback btn-danger btn-xs" data-href="' . base_url('feedback_reply/delete_feedback/' . $feedback->feedback_id) . '"><i class="fa fa-trash"></i></a>';
            }
            $feedback_date = (!empty(strtotime($feedback->feedback_date))) ? date('d-m-Y', strtotime($feedback->feedback_date)) : '';
            $row[] = $action;
            if(empty($feedback->created_by)){
                $created_by = 'JK India';
            } else {
                $created_by = $feedback->created_by;
            }
            $row[] = '<a href="javascript:void(0);" class="feedback_row" data-feedback_id="' . $feedback->feedback_id . '" data-party_id="' . $feedback->f_party_id . '" data-party_name="' . $feedback->f_party_name . '">' . $created_by . '</a>';
            $row[] = '<a href="javascript:void(0);" class="feedback_row" data-feedback_id="' . $feedback->feedback_id . '" data-party_id="' . $feedback->f_party_id . '" data-party_name="' . $feedback->f_party_name . '">' . $assigned_name . '</a>';
            $row[] = '<a href="javascript:void(0);" class="feedback_row" data-feedback_id="' . $feedback->feedback_id . '" data-party_id="' . $feedback->f_party_id . '" data-party_name="' . $feedback->f_party_name . '">' . $feedback_date . '</a>';
            $row[] = '<a href="javascript:void(0);" class="feedback_row" data-feedback_id="' . $feedback->feedback_id . '" data-party_id="' . $feedback->f_party_id . '" data-party_name="' . $feedback->f_party_name . '">' . $feedback->note . '</a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function delete_feedback($id = '') {
        $role_delete = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "delete");
        if ($role_delete) {
           $this->crud->delete('reply', array('feedback_id' => $id));
           $this->crud->delete('user_feedback', array('feedback_id' => $id));
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
    
    function reply($reply_id = '') {
        $data = array();
        
        if (isset($reply_id) && !empty($reply_id)) {
            if ($this->app_model->have_access_role(FEEDBACK_MENU_ID, "edit")) {
                $reply_data = $this->crud->get_row_by_id('reply', array('reply_id' => $reply_id));
                $reply_data = $reply_data[0];
                $data['reply_data'] = $reply_data;
                $feedback_data = $this->crud->get_row_by_id('user_feedback', array('feedback_id' => $reply_data->feedback_id));
                $feedback_data = $feedback_data[0];
                $data['feedback_data'] = $feedback_data;
//                echo '<pre>'; print_r($data); exit;
                set_page('feedback/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            if ($this->app_model->have_access_role(FEEDBACK_MENU_ID, "add")) {
                set_page('feedback/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }
    
    function save_reply() {
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
//        $return = array();
        $post_data['reply_date'] = (isset($post_data['reply_date']) && !empty($post_data['reply_date'])) ? date('Y-m-d', strtotime($post_data['reply_date'])) : NULL;
        if (isset($post_data['reply_id']) && !empty($post_data['reply_id'])) {
            $post_data['updated_at'] = $this->now_time;
            $post_data['updated_by'] = $this->staff_id;
            $post_data['reply_from'] = '1';
            $where_array['reply_id'] = $post_data['reply_id'];
            $result = $this->crud->update('reply', $post_data, $where_array);
            if ($result) {
                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Reply Updated Successfully');
            }
        } else {
            $post_data['created_at'] = $this->now_time;
            $post_data['created_by'] = $this->staff_id;
            $post_data['reply_from'] = '1';
            $result = $this->crud->insert('reply', $post_data);
            if ($result) {
                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Reply Added Successfully');
            }
        }
        print json_encode($return);
        exit;
    }

    function reply_datatable() {
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
        $config['table'] = 'reply r';
        $config['select'] = 'r.*, IFNULL(`p`.`party_name`, "Jk&nbsp;Admin") AS `created_by`';
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = r.assign_to_id', 'join_type' => 'left');
        $config['column_search'] = array('DATE_FORMAT(r.reply_date, "%d-%m-%Y")', 'r.note');
        $config['column_order'] = array(null, 'r.reply_date', 'r.note');
        $config['order'] = array('r.reply_id' => 'desc');
        if (isset($post_data['feedback_id']) && !empty($post_data['feedback_id'])) {
            $config['wheres'][] = array('column_name' => 'r.feedback_id', 'column_value' => $post_data['feedback_id']);
        }
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
//        echo $this->db->last_query();
        $isEdit = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "delete");
        foreach ($list as $reply) {
            $row = array();
            $action = '';
            if($isEdit){ 
                $action .= '<a href="' . base_url("feedback_reply/reply/" . $reply->reply_id) . '" class="edit_button btn-primary btn-xs" data-href="#" ><i class="fa fa-edit"></i></a>';
            }
            if($isDelete){
                $action .= ' | <a href="javascript:void(0);" class="delete_reply btn-danger btn-xs" data-href="' . base_url('feedback_reply/delete_row/' . $reply->reply_id) . '"><i class="fa fa-trash"></i></a>';
            }
            $row[] = $action;
            $row[] = $reply->created_by;
            $row[] = (!empty(strtotime($reply->reply_date))) ? date('d-m-Y', strtotime($reply->reply_date)) : '';
            $row[] = $reply->note;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function delete_row($id = '') {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
    }
}
