<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Item
 * &@property AppLib $applib
 */
class Item extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
		$this->load->library('user_agent');
	}

	/* function index()
	{

	} */

	function item_master(){
		set_page('item/item_master');
	}
	function item_master_datatable(){
		$config['select'] = 'id, item_name, item_code1, item_for_domestic, item_for_export';
		$config['table'] = 'items';
		$config['column_order'] = array(null, 'item_name', 'item_code1');
		$config['column_search'] = array('item_name', 'item_code1');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_MASTER_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_MASTER_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' | <a href="' . base_url('item/add_item_master/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->item_name;
			$row[] = $item_list->item_code1;
            $item_for = '';
            if($item_list->item_for_domestic == 1 && $item_list->item_for_export == 1){
                $item_for = 'Domestic, Export';
            } else if ($item_list->item_for_domestic == 1 && $item_list->item_for_export == 0){
                $item_for = 'Domestic';
            } else if ($item_list->item_for_export == 1 && $item_list->item_for_domestic == 0){
                $item_for = 'Export';
            }
			$row[] = $item_for;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function file_upload_demo(){
		set_page('item/file_upload_demo');
	}

	function add_item_master($id = 0)
	{
		$data = array();
		//$data['item_categories'] = $this->get_item_categories();
        $data['rates_in'] = $this->crud->getFromSQL('SELECT `currency`.`id`,`currency`.`currency` FROM currency');
        if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$item = $this->crud->get_all_with_where('items', 'item_category', 'ASC', $where_array);
			$data['id'] = $item[0]->id;
			$data['item_category'] = $item[0]->item_category;
			$data['item_code1'] = $item[0]->item_code1;
			$data['item_code2'] = $item[0]->item_code2;
			$data['uom'] = $item[0]->uom;
			$data['qty'] = $item[0]->qty;
			$data['item_type'] = $item[0]->item_type;
			$data['stock'] = $item[0]->stock;
			$data['document'] = $item[0]->document;
			$data['hsn_code'] = $item[0]->hsn_code;
			$data['rate'] = $item[0]->rate;
            $data['rates_in_val'] = json_decode($item[0]->rate_in);
			$data['sgst'] = $item[0]->sgst;
			$data['cgst'] = $item[0]->cgst;
			$data['igst'] = $item[0]->igst;
			$data['item_group'] = $item[0]->item_group;
			$data['item_active'] = $item[0]->item_active;
			$data['item_name'] = $item[0]->item_name;
			$data['cfactor'] = $item[0]->cfactor;
			$data['conv_uom'] = $item[0]->conv_uom;
			$data['item_mpt'] = $item[0]->item_mpt;
			$data['item_status'] = $item[0]->item_status;
			$data['image'] = $item[0]->image;
			$data['item_for_domestic'] = $item[0]->item_for_domestic;
			$data['item_for_export'] = $item[0]->item_for_export;
			$data['domestic_quotation_detail_pages'] = array();
			$data['export_quotation_detail_pages'] = array();
			$data['domestic_proforma_invoice_detail_pages'] = array();
			$data['export_proforma_invoice_detail_pages'] = array();
			$data['domestic_order_detail_pages'] = array();
			$data['export_order_detail_pages'] = array();
			$data['domestic_dispatch_detail_pages'] = array();
			$data['export_dispatch_detail_pages'] = array();
			$data['domestic_invoice_detail_pages'] = array();
			$data['export_invoice_detail_pages'] = array();
			$data['domestic_allow_to_edit'] = array();
			$data['export_allow_to_edit'] = array();

			$this->db->select('allow_to_edit,detail,document_type');
			$this->db->from('item_documents');
			$this->db->where('item_id',$id);
			$query = $this->db->get();
			if($query->num_rows() > 0) {
				foreach ($query->result() as $page_row) {
					if ($page_row->document_type == 'domestic_quotation') {
						$data['domestic_quotation_detail_pages'][] = $page_row->detail;
						$data['domestic_allow_to_edit'][] = $page_row->allow_to_edit;
					} elseif ($page_row->document_type == 'export_quotation') {
						$data['export_quotation_detail_pages'][] = $page_row->detail;
						$data['export_allow_to_edit'][] = $page_row->allow_to_edit;
					} elseif ($page_row->document_type == 'domestic_proforma_invoice') {
						$data['domestic_proforma_invoice_detail_pages'][] = $page_row->detail;
					} elseif ($page_row->document_type == 'export_proforma_invoice') {
						$data['export_proforma_invoice_detail_pages'][] = $page_row->detail;
					} elseif ($page_row->document_type == 'domestic_order') {
						$data['domestic_order_detail_pages'][] = $page_row->detail;
					} elseif ($page_row->document_type == 'export_order') {
						$data['export_order_detail_pages'][] = $page_row->detail;
					} elseif ($page_row->document_type == 'domestic_dispatch') {
						$data['domestic_dispatch_detail_pages'][] = $page_row->detail;
					} elseif ($page_row->document_type == 'export_dispatch') {
						$data['export_dispatch_detail_pages'][] = $page_row->detail;
					} elseif ($page_row->document_type == 'domestic_invoice') {
						$data['domestic_invoice_detail_pages'][] = $page_row->detail;
					} elseif ($page_row->document_type == 'export_invoice') {
						$data['export_invoice_detail_pages'][] = $page_row->detail;
					}
				}
			}
			$data['domestic_item_files'] = array();
			$data['export_item_files'] = array();
			$this->db->select('id,file_url,item_type');
			$this->db->from('item_files');
			$this->db->where('item_id',$id);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result_array() as $file_row) {
					if($file_row['item_type'] == 'Domestic'){
						$data['domestic_item_files'][] = $file_row;
					}
					if($file_row['item_type'] == 'Export'){
						$data['export_item_files'][] = $file_row;
					}
				}
			}
			$data['item_bom_data'] = array();
			$this->db->select('ib.id,ib.qty,ib.total_amount,ic.item_category,i.item_name,uom.uom');
			$this->db->from('item_bom ib');
			$this->db->join('items i','i.id = ib.item_id');
			$this->db->join('item_category ic','ic.id = ib.item_category_id');
			$this->db->join('uom','uom.id = ib.uom_id');
			$this->db->where('ib.item_master_id',$id);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result_array() as $file_row) {
					$data['item_bom_data'][] = $file_row;
				}
			}
            $new_array = array();
            foreach ($data['rates_in_val'] as $key => $rate_in_val) {
                $new_array[] = array(
                    'id' => $key,
                    'value' => $rate_in_val
                );
            }
            $object = json_decode(json_encode($data['rates_in']), TRUE);
            $data['rates_in_val'] = array_replace_recursive($object,$new_array);
            /*echo "<pre>";
            print_r($data['rates_in_val']);
            die;*/
		}
//        echo "<pre>";print_r($data); exit;
		set_page('item/add_item_master', $data);
	}
    
    function save_item_master()
    {
        $post_data = $this->input->post();

        /*echo "<pre>";
		print_r($_FILES);
		print_r($_POST);
		die;*/
//        echo "<pre>"; print_r($post_data); exit;
        $post_data['created_at'] = date('Y-m-d H:i:s');
        $post_data['updated_at'] = date('Y-m-d H:i:s');
        $post_data['document'] = '';
        $post_data['image'] = '';
        $post_data['igst'] = isset($_POST['igst']) ? $_POST['igst'] : '0';
        $post_data['cgst'] = isset($_POST['cgst']) ? $_POST['cgst'] : '0';
        $post_data['sgst'] = isset($_POST['sgst']) ? $_POST['sgst'] : '0';
        $post_data['rate'] = isset($_POST['rate']) ? $_POST['rate'] : '0';
        $post_data['item_active'] = isset($_POST['item_active']) ? $_POST['item_active'] : '0';
        $post_data['item_for_domestic'] = isset($_POST['item_for_domestic']) ? $_POST['item_for_domestic'] : '0';
        $post_data['item_for_export'] = isset($_POST['item_for_export']) ? $_POST['item_for_export'] : '0';
        if (isset($_FILES['document'])) {
            $nxt_id = $this->crud->get_next_autoincrement('items');
            $_FILES['document']['name'] = $nxt_id.$_FILES['document']['name'];
            $post_data['document'] = $this->crud->upload_file('document', 'resource/uploads/document');
            $post_data['document'] = str_replace(' ', '_', $post_data['document']);
        }
        if (isset($_FILES['image'])) {
            $_FILES['image']['name'] = $nxt_id.$_FILES['image']['name'];
            $post_data['image'] = $this->crud->upload_file('image', 'resource/uploads/images/items');
            $post_data['image'] = str_replace(' ', '_', $post_data['image']);
        }
        $item_documents = $post_data['item_documents'];
        $domestic_allow_to_edit_array = $post_data['domestic_allow_to_edit'];
        $export_allow_to_edit_array = $post_data['export_allow_to_edit'];
        $item_id = $post_data['id'];
        unset($post_data['item_documents']);
        unset($post_data['id']);
        unset($post_data['domestic_allow_to_edit']);
        unset($post_data['export_allow_to_edit']);
        $post_data['rate_in'] = json_encode($post_data['rate_in']);
        if ($item_id == '' || $item_id == null) {
//            echo '<pre>'; print_r($post_data); exit;
            $item_id = $this->crud->insert('items', $post_data);
            if (!empty($item_documents)) {
				foreach ($item_documents as $document_type=>$documents) {
					$d_page_inc = 1;
					$e_page_inc = 1;
					foreach($documents as $key => $document_detail) {
						if ($document_detail != '') {
							$allow_to_edit = 0;
							if($document_type == "domestic_quotation"){
								if(in_array($d_page_inc, $domestic_allow_to_edit_array)){
									$allow_to_edit = 1;
								}
								$insert_data = array('item_id' => $item_id,
													 'detail' => $document_detail,
													 'document_type'=>$document_type,
													 'allow_to_edit'=>$allow_to_edit,
													 'sequence'=>$key + 1,
													 'created_at'=>date('Y-m-d H:i:s')
													);
								$this->crud->insert('item_documents', $insert_data);
								$d_page_inc++;
							} else if ($document_type == "export_quotation") {
								if(in_array($e_page_inc, $export_allow_to_edit_array)){
									$allow_to_edit = 1;
								}
								$insert_data = array('item_id' => $item_id,
													 'detail' => $document_detail,
													 'document_type'=>$document_type,
													 'allow_to_edit'=>$allow_to_edit,
													 'sequence'=>$key + 1,
													 'created_at'=>date('Y-m-d H:i:s')
													);
								$this->crud->insert('item_documents',$insert_data);
								$e_page_inc++;
							}
						}
					}
                }
            }

            /*if (!empty($item_quotation_detail)) {
				foreach ($item_quotation_detail as $item_quotation_detail_row) {
					if ($item_quotation_detail_row != '') {
						$this->crud->insert('item_quotation_detail', array('item_id' => $item_id, 'page_detail' => $item_quotation_detail_row));
					}
				}
			}*/
        } else {
            $this->db->where('id', $item_id);
            $this->db->update('items', $post_data);
            if (!empty($item_documents)) {
                $this->db->where('item_id', $item_id);
                $this->db->delete('item_documents');
				foreach ($item_documents as $document_type=>$documents) {
					$d_page_inc = 1;
					$e_page_inc = 1;
					foreach($documents as  $key =>  $document_detail) {
						if ($document_detail != '') {
							$allow_to_edit = 0;
							if($document_type == "domestic_quotation"){
								if(in_array($d_page_inc, $domestic_allow_to_edit_array)){
									$allow_to_edit = 1;
								}
								$insert_data = array('item_id' => $item_id,
													 'detail' => $document_detail,
													 'document_type'=>$document_type,
													 'allow_to_edit'=>$allow_to_edit,
													 'sequence'=>$key + 1,
													 'created_at'=>date('Y-m-d H:i:s'));
								$this->crud->insert('item_documents', $insert_data);
								$d_page_inc++;
							} else if ($document_type == "export_quotation") {
								if(in_array($e_page_inc, $export_allow_to_edit_array)){
									$allow_to_edit = 1;
								}
								$insert_data = array('item_id' => $item_id,
													 'detail' => $document_detail,
													 'document_type'=>$document_type,
													 'allow_to_edit'=>$allow_to_edit,
													 'sequence'=>$key + 1,
													 'created_at'=>date('Y-m-d H:i:s'));
								$this->crud->insert('item_documents', $insert_data);
								$e_page_inc++;
							}
						}
					}
				}
            }
            /*
			$this->db->where('id', $item_id);
			$this->db->update('items', $post_data);
			if (!empty($item_quotation_detail)) {
				$this->db->where('item_id', $item_id);
				$this->db->delete('item_quotation_detail');

				foreach ($item_quotation_detail as $item_quotation_detail_row) {
					if ($item_quotation_detail_row != '') {
						$this->crud->insert('item_quotation_detail', array('item_id' => $item_id, 'page_detail' => $item_quotation_detail_row));
					}
				}
			}*/
        }

        if ($item_id > 0) {
            $data['success'] = true;
            $data['item_id'] = $item_id;
            $data['message'] = "Item successfully saved!";
            $data['item_id'] = $this->db->insert_id();
            echo json_encode($data);
        } else {
            echo json_encode(array('success' => false, 'message' => "Something wrong!"));
        }
        exit();
    }


    function update_item_master()
    {
//		print_r($this->input->post());exit;
        $post_data['updated_at'] = date('Y-m-d H:i:s');
        $post_data['item_category'] = $this->input->post('item_category');
        $post_data['item_code1'] = $this->input->post('item_code1');
        $post_data['uom'] = $this->input->post('uom');
        $post_data['qty'] = $this->input->post('qty');
        $post_data['item_type'] = $this->input->post('item_type');
        $post_data['stock'] = $this->input->post('stock');
        $post_data['item_group'] = $this->input->post('item_group');
        $post_data['item_active'] = isset($_POST['item_active']) ? $_POST['item_active'] : '0';
        $post_data['item_name'] = $this->input->post('item_name');
        $post_data['cfactor'] = $this->input->post('cfactor');
        $post_data['conv_uom'] = $this->input->post('conv_uom');
        $post_data['item_mpt'] = $this->input->post('item_mpt');
        $post_data['item_status'] = $this->input->post('item_status');
        $post_data['hsn_code'] = $this->input->post('hsn_code');
        $post_data['rate'] = $this->input->post('rate');
        $post_data['rate_in'] = json_encode($this->input->post('rate_in'));
        $post_data['cgst'] = $this->input->post('cgst');
        $post_data['sgst'] = $this->input->post('sgst');
        $post_data['igst'] = $this->input->post('igst');
        $post_data['item_for_domestic'] = isset($_POST['item_for_domestic']) ? $_POST['item_for_domestic'] : '0';
        $post_data['item_for_export'] = isset($_POST['item_for_export']) ? $_POST['item_for_export'] : '0';
        $domestic_allow_to_edit_array = $this->input->post('domestic_allow_to_edit');
        $export_allow_to_edit_array = $this->input->post('export_allow_to_edit');
        $where_array['id'] = $i_id = $this->input->post('id');
        $post_data['image'] = $this->crud->get_column_value_by_id('items', 'image', $where_array);
        if (isset($_FILES['document'])) {
            if (!empty($_FILES['document']['name'])) {
                $doc = $this->crud->get_all_with_where('items','','',$where_array);
                $doc = $doc[0]->document;
                $_FILES['document']['name'] = $i_id.$_FILES['document']['name'];
                $doc = BASEPATH."../resource/uploads/document/".$doc;
                unlink($doc);
                $this->crud->upload_file('document', 'resource/uploads/document');
                $post_data['document'] = $_FILES['document']['name'];
                $post_data['document'] = str_replace(' ', '_', $post_data['document']);
            } else {
                $post_data['document'] = $this->crud->get_column_value_by_id('items', 'document', $where_array);
            }
        }
        if (isset($_FILES['image'])) {
            if (!empty($_FILES['image']['name'])) {
                $i_img = $this->crud->get_all_with_where('items','','',$where_array);
                $i_img = $i_img[0]->image;
                $_FILES['image']['name'] = $i_id.$_FILES['image']['name'];
                $i_img = BASEPATH."../resource/uploads/images/items/".$i_img;
                unlink($i_img);
                $this->crud->upload_file('image', 'resource/uploads/images/items');
                $post_data['image'] = $_FILES['image']['name'];
                $post_data['image'] = str_replace(' ', '_', $post_data['image']);
            } else {
                $post_data['image'] = $this->crud->get_column_value_by_id('items', 'image', $where_array);
            }
        }
        $result = $this->crud->update('items', $post_data, $where_array);

        $item_id = $this->input->post('id');
        $this->db->where('id', $item_id);
        $this->db->update('items', $post_data);

        $item_documents = $_POST['item_documents'];
        if (!empty($item_documents)) {

            $this->db->where('item_id', $item_id);
            $this->db->delete('item_documents');

            foreach ($item_documents as $document_type=>$documents) {
                $d_page_inc = 1;
                $e_page_inc = 1;
				foreach($documents as $key => $document_detail) {
					if ($document_detail != '') {
                        $allow_to_edit = 0;
                        if($document_type == "domestic_quotation"){
                            if(in_array($d_page_inc, $domestic_allow_to_edit_array)){
                                $allow_to_edit = 1;
                            }
							$insert_data = array('item_id' => $item_id,
												 'detail' => $document_detail,
												 'document_type'=>$document_type,
												 'allow_to_edit'=>$allow_to_edit,
												 'sequence'=>$key + 1,
												 'created_at'=>date('Y-m-d H:i:s'));
							$this->crud->insert('item_documents', $insert_data);
                            $d_page_inc++;
                        } else if ($document_type == "export_quotation") {
                            if(in_array($e_page_inc, $export_allow_to_edit_array)){
                                $allow_to_edit = 1;
                            }
							$insert_data = array('item_id' => $item_id,
												 'detail' => $document_detail,
												 'document_type'=>$document_type,
												 'allow_to_edit'=>$allow_to_edit,
												 'sequence'=>$key + 1,
												 'created_at'=>date('Y-m-d H:i:s'));
							$this->crud->insert('item_documents', $insert_data);
                            $e_page_inc++;
                        }
                    }
                }
            }
        }

        /*$item_quotation_detail = $_POST['item_quotation_detail'];
		if (!empty($item_quotation_detail)) {
			$this->db->where('item_id', $item_id);
			$this->db->delete('item_quotation_detail');

			foreach ($item_quotation_detail as $item_quotation_detail_row) {
				if ($item_quotation_detail_row != '') {
					$this->crud->insert('item_quotation_detail', array('item_id' => $item_id, 'page_detail' => $item_quotation_detail_row));
				}
			}
		}*/

        if ($result) {
            $data['success'] = $result;
            $data['message'] = 'Item Updated Successfully';
            $data['item_id'] = $item_id;
            /*$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Item Updated Successfully');*/
            echo json_encode($data);
        }
    }


	function upload_domestic_item_file($item_id = 0){
		if ($item_id != 0) {
			$config['upload_path'] = './resource/uploads/document/';
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('item_file')) {
				return false;
			}
			$data = $this->upload->data();
			$this->db->insert('item_files',array('item_id'=>$item_id,'file_url'=>'./resource/uploads/document/'.$data['file_name'],'item_type'=>'Domestic'));
			$file_id = $this->db->insert_id();
			$file_data['files'][] = array(
				'name' => $data['file_name'],
				'size' => $data['file_size'] * 1024,
				'type' => $data['file_type'],
				'url' => base_url().'uploads/'.$data['file_name'],
				'deleteType' => 'POST',
				'deleteUrl' => base_url().'item/remove_item_file/'.$file_id,
			);
			echo json_encode($file_data);
			exit();
		}
	}
	function upload_export_item_file($item_id = 0){
		if ($item_id != 0) {
			$config['upload_path'] = './resource/uploads/document/';
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('item_file')) {
				return false;
			}
			$data = $this->upload->data();
			$this->db->insert('item_files',array('item_id'=>$item_id,'file_url'=>'./resource/uploads/document/'.$data['file_name'],'item_type'=>'Export'));
			$file_id = $this->db->insert_id();
			$file_data['files'][] = array(
				'name' => $data['file_name'],
				'size' => $data['file_size'] * 1024,
				'type' => $data['file_type'],
				'url' => base_url().'uploads/'.$data['file_name'],
				'deleteType' => 'POST',
				'deleteUrl' => base_url().'item/remove_item_file/'.$file_id,
			);
			echo json_encode($file_data);
			exit();
		}
	}
    
    /**
	 * @param $file_id
	 */
	function remove_item_file($file_id)
	{
		$file_url = $this->crud->get_all_with_where('item_files','','',array('id' => $file_id));
		$file_url = BASEPATH."../".ltrim($file_url[0]->file_url, './');
		if(file_exists($file_url)){
			unlink($file_url);
		}
		$this->db->where('id',$file_id);
		$this->db->delete('item_files');
		echo json_encode(array('success'=>true,'message'=>"File removed successfully!"));
	}

	/**
	 * @return mixed
	 */
	function get_item_categories()
	{
		$this->db->select('*');
		$this->db->from('item_category');
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @param $category_id
	 * @return mixed
	 */
	function get_items_by_category($category_id)
	{
		$this->db->select('id,item_name');
		$this->db->from('items');
		$this->db->where('item_category', $category_id);
		$query = $this->db->get();
		echo json_encode($query->result());
	}

	/**
	 * @param $item_id
	 */
	function save_item_bom($item_id)
	{
		$item_bom_data = $this->input->post();
		$item_bom_data['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('item_bom',$item_bom_data);
		$item_bom_id = $this->db->insert_id();
		echo json_encode(array('success'=>true,'message'=>"Item BOM successfully added!.",'item_bom_id'=>$item_bom_id));
		exit();
	}

	//~ Purchase Item
	function purchase_item(){
		if($this->applib->have_access_role(MASTER_PURCHASE_ITEM_MENU_ID,"view")) {
			set_page('item/purchase_item');	
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	function purchase_item_datatable(){
		$config['select'] = 'pi.id, i_cat.item_category, ig.ig_code, sm.supplier_make_name, pi.item_name, pi.item_code, cim.make_name, rmg.group_name';
		$config['table'] = 'purchase_items pi';
		$config['column_order'] = array(null, 'i_cat.item_category', 'ig.ig_code', 'pi.item_name', 'pi.item_code', 'sm.supplier_make_name', 'cim.make_name', 'rmg.group_name');
		$config['column_search'] = array('i_cat.item_category', 'ig.ig_code', 'pi.item_name', 'pi.item_code', 'sm.supplier_make_name', 'cim.make_name', 'rmg.group_name');
        $config['joins'][1] = array('join_table' => 'item_category i_cat', 'join_by' => 'i_cat.id = pi.item_category', 'join_type' => 'left');
        $config['joins'][2] = array('join_table' => 'item_group_code ig', 'join_by' => 'ig.id = pi.item_group', 'join_type' => 'left');
        $config['joins'][3] = array('join_table' => 'supplier_make sm', 'join_by' => 'sm.id = pi.supplier_make_id', 'join_type' => 'left');
        $config['joins'][4] = array('join_table' => 'challan_item_make cim', 'join_by' => 'cim.id = pi.item_make_id', 'join_type' => 'left');
        $config['joins'][5] = array('join_table' => 'raw_material_group rmg', 'join_by' => 'rmg.id = pi.raw_material_group_id', 'join_type' => 'left');
        if (isset($_POST['item_group']) && !empty($_POST['item_group'])) {
            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => $_POST['item_group']);
        } else {
//            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => '-1');
        }
		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_PURCHASE_ITEM_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_PURCHASE_ITEM_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '<a href="' . base_url('item/add_purchase_item/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete_purchase_items/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
				$action .= ' | <input type="checkbox" class="check_purchase_item" name="checked_purchase_items[]" value="'.$item_list->id.'">';
			}
			$row[] = $action;
			$row[] = $item_list->item_category;
			$row[] = $item_list->ig_code;
			$row[] = $item_list->item_name;
			$row[] = $item_list->item_code;
			$row[] = $item_list->supplier_make_name;
			$row[] = $item_list->make_name;
			$row[] = $item_list->group_name;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	function add_purchase_item($id = 0, $item_group_id = 0){
		$data = array();
        $data['item_categories'] = $this->get_item_categories();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$item = $this->crud->get_all_with_where('purchase_items', 'item_category', 'ASC', $where_array);
            $item_group_code = $this->crud->get_column_value_by_id('item_group_code', 'ig_description', array('id' => $item[0]->item_group));
            $data['item_group_code'] = $item_group_code;
			$data['id'] = $item[0]->id;
			$data['item_category'] = $item[0]->item_category;
			$data['item_code'] = $item[0]->item_code;
			$data['item_make_id'] = $item[0]->item_make_id;
			$data['uom'] = $item[0]->uom;
			$data['reference_qty'] = !empty($item[0]->reference_qty) ? $item[0]->reference_qty : NULL;
			$data['reference_qty_uom'] = !empty($item[0]->reference_qty_uom) ? $item[0]->reference_qty_uom : NULL;
            $data['qty_2'] = !empty($item[0]->required_qty) ? $item[0]->required_qty : NULL;
            $data['uom_2'] = !empty($item[0]->required_uom) ? $item[0]->required_uom : NULL;
			$data['stock'] = $item[0]->stock;
			$data['document'] = $item[0]->document;
			$data['hsn_code'] = $item[0]->hsn_code;
			$data['item_group'] = $item[0]->item_group;
            $data['item_active'] = $item[0]->item_active;
			$data['item_name'] = $item[0]->item_name;
			$data['cfactor'] = $item[0]->cfactor;
			$data['item_mpt'] = $item[0]->item_mpt;
			$data['item_status'] = $item[0]->item_status;
			$data['image'] = $item[0]->image;
			$data['igst'] = $item[0]->igst;
			$data['cgst'] = $item[0]->cgst;
			$data['sgst'] = $item[0]->sgst;
			$data['rate'] = $item[0]->rate;
			$data['sales_rate'] = $item[0]->sales_rate;
			$data['discount'] = $item[0]->discount;
            $data['raw_material_group_id'] = $item[0]->raw_material_group_id;
            $data['supplier_make_id'] = $item[0]->supplier_make_id;
        } else {
            if (isset($item_group_id) && !empty($item_group_id)) {
                $item_group_code = $this->crud->get_column_value_by_id('item_group_code', 'ig_description', array('id' => $item_group_id));
                $data['item_group_id'] = $item_group_id;
                $data['item_group_code'] = $item_group_code;
            }
        }
//        echo "<pre>"; print_r($data); exit;
		if($this->applib->have_access_role(MASTER_PURCHASE_ITEM_MENU_ID,"add")) {
			set_page('item/add_purchase_item',$data);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
    function get_item_group_code($item_group_id){
        $data = array();
        $item_group_code = $this->crud->get_column_value_by_id('item_group_code', 'ig_description', array('id' => $item_group_id));
        $data['item_group_code'] = $item_group_code;
        echo json_encode($data);
        exit;
    }
        
	function save_purchase_item(){
		$post_data = $this->input->post();

//		echo "<pre>";
//		print_r($_FILES);
//		print_r($post_data);
//		exit;

		$post_data['created_at'] = $this->now_time;
		$post_data['document'] = '';
		$post_data['image'] = '';
        $post_data['item_category'] = !empty($this->input->post('item_category')) ? $this->input->post('item_category') : NULL;
        $post_data['item_code'] = !empty($this->input->post('item_code')) ? $this->input->post('item_code') : NULL;
        $post_data['item_make_id'] = !empty($this->input->post('item_make_id')) ? $this->input->post('item_make_id') : NULL;
        $post_data['uom'] = !empty($this->input->post('uom')) ? $this->input->post('uom') : NULL;
        $post_data['reference_qty'] = !empty($this->input->post('reference_qty')) ? $this->input->post('reference_qty') : NULL;
        $post_data['reference_qty_uom'] = !empty($this->input->post('reference_qty_uom')) ? $this->input->post('reference_qty_uom') : NULL;
        $post_data['required_qty'] = !empty($this->input->post('qty_2')) ? $this->input->post('qty_2') : NULL;
        $post_data['required_uom'] = !empty($this->input->post('uom_2')) ? $this->input->post('uom_2') : NULL;
        $post_data['item_mpt'] = !empty($this->input->post('item_mpt')) ? $this->input->post('item_mpt') : NULL;
        $post_data['item_status'] = !empty($this->input->post('item_status')) ? $this->input->post('item_status') : NULL;
        $post_data['discount'] = !empty($this->input->post('discount')) ? $this->input->post('discount') : NULL;
        $post_data['igst'] = !empty($this->input->post('igst')) ? $this->input->post('igst') : NULL;
        $post_data['cgst'] = !empty($this->input->post('cgst')) ? $this->input->post('cgst') : NULL;
        $post_data['sgst'] = !empty($this->input->post('sgst')) ? $this->input->post('sgst') : NULL;
        $post_data['sales_rate'] = !empty($this->input->post('sales_rate')) ? $this->input->post('sales_rate') : '0';
		$post_data['item_active'] = isset($_POST['item_active']) ? $_POST['item_active'] : '0';
        $post_data['raw_material_group_id'] = !empty($this->input->post('raw_material_group_id')) ? $this->input->post('raw_material_group_id') : NULL;
        $post_data['supplier_make_id'] = !empty($this->input->post('supplier_make_id')) ? $this->input->post('supplier_make_id') : NULL;
		if (isset($_FILES['document'])) {
			$nxt_id = $this->crud->get_next_autoincrement('purchase_items');
			$_FILES['document']['name'] = $nxt_id.$_FILES['document']['name'];
			$post_data['document'] = $this->crud->upload_file('document', 'resource/uploads/purchase_document');
			$post_data['document'] = str_replace(' ', '_', $post_data['document']);
		}
		if (isset($_FILES['image'])) {
			$_FILES['image']['name'] = $nxt_id.$_FILES['image']['name'];
			$post_data['image'] = $this->crud->upload_file('image', 'resource/uploads/images/purchase_items');
			$post_data['image'] = str_replace(' ', '_', $post_data['image']);
		}
		$item_id = $post_data['id'];
		unset($post_data['id']);
		unset($post_data['reference_qty_uom']);
		unset($post_data['qty_2']);
		unset($post_data['uom_2']);
		if ($item_id == '' || $item_id == null) {
			$item_id = $this->crud->insert('purchase_items', $post_data);
		} else {
			$this->db->where('id', $item_id);
			$this->db->update('purchase_items', $post_data);
		}
		if ($item_id > 0) {
			$data['success'] = true;
			$data['item_id'] = $item_id;
			$data['message'] = "Purchase Item successfully saved!";
			$data['item_id'] = $this->db->insert_id();
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Purchase Item Added Successfully');
			echo json_encode($data);
		} else {
			echo json_encode(array('success' => false, 'message' => "Something wrong!"));
		}
		exit();
	}

	function update_purchase_item(){
//        print_r($this->input->post()); exit;
        $item_id = $this->input->post('id');
        $where_array = array('id' => $item_id);
		$post_data = array();
		$post_data['updated_at'] = $this->now_time;
		$post_data['item_category'] = !empty($this->input->post('item_category')) ? $this->input->post('item_category') : NULL;
        $post_data['item_code'] = !empty($this->input->post('item_code')) ? $this->input->post('item_code') : NULL;
        $post_data['item_make_id'] = !empty($this->input->post('item_make_id')) ? $this->input->post('item_make_id') : NULL;
        $post_data['uom'] = !empty($this->input->post('uom')) ? $this->input->post('uom') : NULL;
		$post_data['reference_qty'] = !empty($this->input->post('reference_qty')) ? $this->input->post('reference_qty') : NULL;
        $post_data['reference_qty_uom'] = !empty($this->input->post('reference_qty_uom')) ? $this->input->post('reference_qty_uom') : NULL;
        $post_data['required_qty'] = !empty($this->input->post('qty_2')) ? $this->input->post('qty_2') : NULL;
        $post_data['required_uom'] = !empty($this->input->post('uom_2')) ? $this->input->post('uom_2') : NULL;
		$post_data['stock'] = $this->input->post('stock');
		$post_data['item_group'] = $this->input->post('item_group');
        $post_data['item_active'] = isset($_POST['item_active']) ? $_POST['item_active'] : '0';
		$post_data['item_name'] = $this->input->post('item_name');
		$post_data['cfactor'] = $this->input->post('cfactor');
        $post_data['item_mpt'] = !empty($this->input->post('item_mpt')) ? $this->input->post('item_mpt') : NULL;
        $post_data['item_status'] = !empty($this->input->post('item_status')) ? $this->input->post('item_status') : NULL;
		$post_data['hsn_code'] = $this->input->post('hsn_code');
        $post_data['igst'] = !empty($this->input->post('igst')) ? $this->input->post('igst') : NULL;
        $post_data['cgst'] = !empty($this->input->post('cgst')) ? $this->input->post('cgst') : NULL;
        $post_data['sgst'] = !empty($this->input->post('sgst')) ? $this->input->post('sgst') : NULL;
		$post_data['rate'] = $this->input->post('rate');
        $post_data['sales_rate'] = !empty($this->input->post('sales_rate')) ? $this->input->post('sales_rate') : '0';
		$post_data['discount'] = !empty($this->input->post('discount')) ? $this->input->post('discount') : NULL;
        $post_data['image'] = $this->crud->get_column_value_by_id('purchase_items', 'image', $where_array);
        $post_data['raw_material_group_id'] = !empty($this->input->post('raw_material_group_id')) ? $this->input->post('raw_material_group_id') : NULL;
        $post_data['supplier_make_id'] = !empty($this->input->post('supplier_make_id')) ? $this->input->post('supplier_make_id') : NULL;
		if (isset($_FILES['document'])) {
			if (!empty($_FILES['document']['name'])) {
				$doc = $this->crud->get_all_with_where('purchase_items','','',$where_array);
				$doc = $doc[0]->document;
				$_FILES['document']['name'] = $item_id.$_FILES['document']['name'];
				$doc = BASEPATH."../resource/uploads/purchase_document/".$doc;
				unlink($doc);
				$this->crud->upload_file('document', 'resource/uploads/purchase_document');
				$post_data['document'] = $_FILES['document']['name'];
				$post_data['document'] = str_replace(' ', '_', $post_data['document']);
			} else {
				$post_data['document'] = $this->crud->get_column_value_by_id('purchase_items', 'document', $where_array);
			}
		}
		if (isset($_FILES['image'])) {
			if (!empty($_FILES['image']['name'])) {
				$i_img = $this->crud->get_all_with_where('purchase_items','','',$where_array);
				$i_img = $i_img[0]->image;
				$_FILES['image']['name'] = $item_id.$_FILES['image']['name'];
				$i_img = BASEPATH."../resource/uploads/images/purchase_items/".$i_img;
				unlink($i_img);
				$this->crud->upload_file('image', 'resource/uploads/images/purchase_items');
				$post_data['image'] = $_FILES['image']['name'];
				$post_data['image'] = str_replace(' ', '_', $post_data['image']);
			} else {
				$post_data['image'] = $this->crud->get_column_value_by_id('purchase_items', 'image', $where_array);
			}
		}
		$result = $this->crud->update('purchase_items', $post_data, $where_array);
		if ($result) {
			$data['success'] = $result;
			$data['message'] = 'Purchase Item Updated Successfully';
			$data['item_id'] = $item_id;
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Purchase Item Updated Successfully');
			echo json_encode($data);
		}
	}
    
    function delete_purchase_items($id){
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$return = $this->crud->delete($table, array($id_name => $id));
        print json_encode($return);
        exit;
	}
    
    function delete_checked_purchase_items(){
		$checked_purchase_item_ids = $_POST['checked_purchase_item_ids'];
        if(!empty($checked_purchase_item_ids)){
//            $this->crud->delete_where_in('purchase_items', 'id', $checked_purchase_item_ids);
//            $this->session->set_flashdata('success',true);
//            $this->session->set_flashdata('message','Deleted Successfully');
            foreach ($checked_purchase_item_ids as $checked_purchase_item_id){
                $this->crud->delete('purchase_items', array('id' => $checked_purchase_item_id));
            }
        }
	}
    
    function download_demo_for_import_purchase_item(){
        $output_head = array('Item Category', 'Item Group', 'Item Name', 'Item Code', 'Supplier Make', 'Rate', 'UOM', 'Relation', 'Required Qty', 'Required UOM', 'Item Make', 'HSN Code', 'Sales Rate', 'Material Process Type', 'Discount', 'IGST', 'CGST', 'SGST', 'Status', 'Batch Wise Stock', 'Raw Material Group');
        $filename = "Purchase_item_import_demo.csv";
        header("Content-type: application/csv");
        header('Content-Disposition: attachment; filename='.$filename);
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        fputcsv($handle, $output_head);
        fclose($handle);
        exit;
	}
    
    function import_purchase_item(){
		$count = 0;
        $fp = fopen($_FILES['import_file']['tmp_name'],'r') or die("can't open file");
		while($csv_line = fgetcsv($fp,1024)){
            
            $insert_csv = array();
			$count++;
			if($count == 1){
				continue;
			}//keep this if condition if you want to remove the first row
            
            // - Item Category
			$field_value = trim($csv_line[0]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('item_category', 'id', 'item_category', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['item_category'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $insert_new['updated_by'] = $this->logged_in_id;
                    $insert_new['created_by'] = $this->logged_in_id;
                    $result = $this->crud->insert('item_category', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['item_category'] = $field_value;
            
            // - Item Group
			$field_value = trim($csv_line[1]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('item_group_code', 'id', 'ig_code', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['ig_code'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $result = $this->crud->insert('item_group_code', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['item_group'] = $field_value;
            
            $insert_csv['item_name'] = (!empty($csv_line[2])) ? $csv_line[2] : NULL;
            $insert_csv['item_code'] = (!empty($csv_line[3])) ? $csv_line[3] : NULL;
            
            // - Supplier Make
			$field_value = trim($csv_line[4]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('supplier_make', 'id', 'supplier_make_name', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['supplier_make_name'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $result = $this->crud->insert('supplier_make', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['supplier_make_id'] = $field_value;
            
            $insert_csv['rate'] = (!empty($csv_line[5])) ? $csv_line[5] : NULL;
            
            // - UOM
			$field_value = trim($csv_line[6]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('uom', 'id', 'uom', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['uom'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $result = $this->crud->insert('uom', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['uom'] = $field_value;
            
            $insert_csv['reference_qty'] = (!empty($csv_line[7])) ? $csv_line[7] : NULL;
            $insert_csv['required_qty'] = (!empty($csv_line[8])) ? $csv_line[8] : NULL;
            
            // - Required Qty UOM
			$field_value = trim($csv_line[9]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('uom', 'id', 'uom', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['uom'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $result = $this->crud->insert('uom', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['required_uom'] = $field_value;
            
            // - Item Make
			$field_value = trim($csv_line[10]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('challan_item_make', 'id', 'make_name', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['make_name'] = $field_value;
                    $result = $this->crud->insert('challan_item_make', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['item_make_id'] = $field_value;
            
            $insert_csv['hsn_code'] = (!empty($csv_line[11])) ? $csv_line[11] : NULL;
            $insert_csv['sales_rate'] = (!empty($csv_line[12])) ? $csv_line[12] : NULL;
            
            // - Material Process Type
			$field_value = trim($csv_line[13]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('material_process_type', 'id', 'material_process_type', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['material_process_type'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $result = $this->crud->insert('material_process_type', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['item_mpt'] = $field_value;
            
            $insert_csv['discount'] = (!empty($csv_line[14])) ? $csv_line[14] : NULL;
            $insert_csv['igst'] = (!empty($csv_line[15])) ? $csv_line[15] : NULL;
            $insert_csv['cgst'] = (!empty($csv_line[16])) ? $csv_line[16] : NULL;
            $insert_csv['sgst'] = (!empty($csv_line[17])) ? $csv_line[17] : NULL;
            
            // - Item Status
			$field_value = trim($csv_line[18]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('item_status', 'id', 'item_status', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['item_status'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $result = $this->crud->insert('item_status', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['item_status'] = $field_value;
            
            // - Batch Wise Stock ?
			$field_value = trim($csv_line[19]);
            if(!empty($field_value)){
                $field_value = strtolower($field_value);
                if($field_value == 'yes'){
                    $field_value = 0;
                } else if($field_value == 'no'){
                    $field_value = 1;
                }
            } else {
                $field_value = NULL;
            }
            $insert_csv['stock'] = $field_value;
            
            // - Raw Material Group
			$field_value = trim($csv_line[20]);
            if(!empty($field_value)){
                $field_value_id = $this->crud->get_id_by_val('raw_material_group', 'id', 'group_name', $field_value);
                if(empty($field_value_id)){
                    $insert_new = array();
                    $insert_new['group_name'] = $field_value;
                    $insert_new['created_at'] = $this->now_time;
                    $insert_new['updated_at'] = $this->now_time;
                    $result = $this->crud->insert('raw_material_group', $insert_new);
                    $field_value_id = $this->db->insert_id();
                }
                $field_value = $field_value_id;
            } else {
                $field_value = NULL;
            }
            $insert_csv['raw_material_group_id'] = $field_value;
            
			$i++;
			$check_product_arr = array();
			if(!empty($insert_csv['item_name'])){ $check_product_arr['item_name'] = $insert_csv['item_name']; }
			if(!empty($insert_csv['item_group'])){ $check_product_arr['item_group'] = $insert_csv['item_group']; }
            $result = $this->crud->get_row_by_id('purchase_items', $check_product_arr);
            if(empty($result)){
                $insert_csv['created_at'] = $this->now_time;
                $insert_csv['updated_at'] = $this->now_time;
                $this->crud->insert('purchase_items', $insert_csv);
            }
		}
		fclose($fp) or die("can't close file");
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Purchase Items Import Successfully!');
        redirect('item/purchase_item/');
	}
	
	/**
	 * @param $id
	 */
	function item_category($id = ''){
		$category = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$category = $this->crud->get_all_with_where('item_category','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_category','id','ASC');
		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($category) ? $category[0]->id : '';
		$return['category_name'] = ($category) ? $category[0]->item_category : '';
		if($this->applib->have_access_role(MASTER_ITEM_CATEGORY_MENU_ID,"view")) {
			set_page('item/item_category',$return);	
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	function item_cat_datatable(){
		$config['select'] = 'id, item_category';
		$config['table'] = 'item_category';
		$config['column_order'] = array(null, 'item_category');
		$config['column_search'] = array('item_category');

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_CATEGORY_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_CATEGORY_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' | <a href="' . base_url('item/item_category/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->item_category;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

	}

	function update_item_category()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$post_data['updated_by'] = $this->session->userdata('is_logged_in')['staff_id'];
		$result = $this->crud->update('item_category', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Category Updated Successfully');
			echo json_encode($post_data);
		}
	}


	function add_item_category()
	{
		$post_data = $this->input->post();
		$post_data['created_by'] = $this->session->userdata('is_logged_in')['staff_id'];
		$post_data['updated_by'] = $this->session->userdata('is_logged_in')['staff_id'];
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_category',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Category Added Successfully');
			echo json_encode($post_data);
		}
	}

	function item_group_code($id = ''){
		$item_group = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$item_group = $this->crud->get_all_with_where('item_group_code','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_group_code','id','ASC');
		
		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($item_group) ? $item_group[0]->id : '';
		$return['ig_code'] = ($item_group) ? $item_group[0]->ig_code : '';
		$return['ig_description'] = ($item_group) ? $item_group[0]->ig_description : '';
		if($this->applib->have_access_role(MASTER_ITEM_GROUP_CODE_MENU_ID,"view")) {
			set_page('item/item_group_code', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}

	function item_group_datatable(){
		$config['select'] = 'id, ig_code, ig_description';
		$config['table'] = 'item_group_code';
		$config['column_order'] = array(null, 'ig_code', 'ig_description');
		$config['column_search'] = array('ig_code', 'ig_description');

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_GROUP_CODE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_GROUP_CODE_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' <a href="' . base_url('item/item_group_code/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> | ';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = '<a href="' . base_url('item/add_purchase_item/0/' . $item_list->id) . '" target="_blank" >'.$item_list->ig_code.'</a>';
			$row[] = $item_list->ig_description;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

	}



	function update_item_group_codes()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_group_code', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Group Codes Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_item_group_code()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_group_code',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Group Codes Added Successfully');
			echo json_encode($post_data);
		}
	}

	function item_type($id ='')
	{
		$item_type = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$item_type = $this->crud->get_all_with_where('item_type', 'id', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_all_records('item_type', 'id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($item_type) ? $item_type[0]->id : '';
		$return['item_type'] = ($item_type) ? $item_type[0]->item_type : '';
		if($this->applib->have_access_role(MASTER_ITEM_TYPE_MENU_ID,"view")) {
			set_page('item/item_type', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}

	function item_type_datatable(){
		$config['select'] = 'id, item_type';
		$config['table'] = 'item_type';
		$config['column_order'] = array(null, 'item_type');
		$config['column_search'] = array('item_type');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_TYPE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_TYPE_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '  <a href="' . base_url('item/item_type/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> | ';
			}
			if ($isDelete) {
				$action .= '  <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->item_type;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);


	}

	function update_item_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_type', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Type Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_item_type()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Type Added Successfully');
			echo json_encode($post_data);
		}
	}

	function item_status($id=''){
		$items_status = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$items_status = $this->crud->get_all_with_where('item_status','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_status','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($items_status) ? $items_status[0]->id : '';
		$return['item_status'] = ($items_status) ? $items_status[0]->item_status : '';
		if($this->applib->have_access_role(MASTER_ITEM_STATUS_MENU_ID,"view")) {
			set_page('item/item_status', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	function item_status_datatable(){
		$config['select'] = 'id, item_status';
		$config['table'] = 'item_status';
		$config['column_order'] = array(null, 'item_status');
		$config['column_search'] = array('item_status');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_STATUS_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_STATUS_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' | <a href="' . base_url('item/item_status/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->item_status;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function update_item_status()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_status', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Status Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_item_status()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_status',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Status Added Successfully');
			echo json_encode($post_data);
		}
	}

	function material_process_type($id=''){
		$material_process_types = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$material_process_types = $this->crud->get_all_with_where('material_process_type','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('material_process_type','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($material_process_types) ? $material_process_types[0]->id : '';
		$return['material_process_type'] = ($material_process_types) ? $material_process_types[0]->material_process_type : '';
		if($this->applib->have_access_role(MASTER_ITEM_MATERIAL_PROCESS_TYPE_MENU_ID,"view")) {
			set_page('item/material_process_type', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	function material_process_datatables(){
		$config['select'] = 'id, material_process_type';
		$config['table'] = 'material_process_type';
		$config['column_order'] = array(null, 'material_process_type');
		$config['column_search'] = array('material_process_type');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_MATERIAL_PROCESS_TYPE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_MATERIAL_PROCESS_TYPE_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' | <a href="' . base_url('item/material_process_type/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->material_process_type;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function update_material_process_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('material_process_type', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Material Process Type Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_material_process_type()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('material_process_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Material Process Type Added Successfully');
			echo json_encode($post_data);
		}
	}

	function main_group($id=''){
		$main_groups = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$main_groups = $this->crud->get_all_with_where('main_group','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('main_group','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($main_groups) ? $main_groups[0]->id : '';
		$return['code'] = ($main_groups) ? $main_groups[0]->code : '';
		$return['item_group'] = ($main_groups) ? $main_groups[0]->item_group : '';
		$return['description'] = ($main_groups) ? $main_groups[0]->description : '';
		if($this->applib->have_access_role(MASTER_ITEM_MAIN_GROUP_MENU_ID,"view")) {
			set_page('item/main_group', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function main_group_datatables(){
		$config['select'] = 'id, item_group, code, description';
		$config['table'] = 'main_group';
		$config['column_order'] = array(null, 'item_group', 'code', 'description');
		$config['column_search'] = array('item_group', 'code', 'description');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_MAIN_GROUP_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_MAIN_GROUP_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' | <a href="' . base_url('item/main_group/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->item_group;
			$row[] = $item_list->code;
			$row[] = $item_list->description;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	function update_main_group()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('main_group', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Group Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_main_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('main_group',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Item Group Added Successfully');
			echo json_encode($post_data);
		}
	}

	function sub_group($id=''){
		$sub_groups = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$sub_groups = $this->crud->get_all_with_where('sub_group','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('sub_group','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($sub_groups) ? $sub_groups[0]->id : '';
		$return['sub_item_group'] = ($sub_groups) ? $sub_groups[0]->sub_item_group : '';
		if($this->applib->have_access_role(MASTER_ITEM_SUB_GROUP_MENU_ID,"view")) {
			set_page('item/sub_group', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function sub_group_datatable(){
		$config['select'] = 'id, sub_item_group';
		$config['table'] = 'sub_group';
		$config['column_order'] = array(null, 'sub_item_group');
		$config['column_search'] = array('sub_item_group');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_SUB_GROUP_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_SUB_GROUP_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' | <a href="' . base_url('item/sub_group/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->sub_item_group;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	function update_sub_group()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('sub_group', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sub Item Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_sub_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('sub_group',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Sub Item Added Successfully');
			echo json_encode($post_data);
		}
	}

	function make($id=''){
		$make = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$make = $this->crud->get_all_with_where('item_make','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_make','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($make) ? $make[0]->id : '';
		$return['item_make'] = ($make) ? $make[0]->item_make : '';
		if($this->applib->have_access_role(MASTER_ITEM_MAKE_MENU_ID,"view")) {
			set_page('item/make', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function item_make_datatable(){
		$config['select'] = 'id, item_make';
		$config['table'] = 'item_make';
		$config['column_order'] = array(null, 'item_make');
		$config['column_search'] = array('item_make');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_MAKE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_MAKE_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '  <a href="' . base_url('item/make/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->item_make;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function add_make()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_make',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Make Item Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_make()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_make', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Make Item Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function drawing_number($id=''){
		$drawing_numbers = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$drawing_numbers = $this->crud->get_all_with_where('drawing_number','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('drawing_number','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($drawing_numbers) ? $drawing_numbers[0]->id : '';
		$return['drawing_number'] = ($drawing_numbers) ? $drawing_numbers[0]->drawing_number : '';
		if($this->applib->have_access_role(MASTER_ITEM_DRAWING_NUMBER_MENU_ID,"view")) {
			set_page('item/drawing_number', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	function drawing_number_datatable(){
		$config['select'] = 'id, drawing_number';
		$config['table'] = 'drawing_number';
		$config['column_order'] = array(null, 'drawing_number');
		$config['column_search'] = array('drawing_number');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_DRAWING_NUMBER_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_DRAWING_NUMBER_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '  <a href="' . base_url('item/drawing_number/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->drawing_number;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function update_drawing_number()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('drawing_number', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Drawing Number Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_drawing_number()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('drawing_number',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Drawing Number Added Successfully');
			echo json_encode($post_data);
		}
	}

	function material_specification($id=''){
		$material_specifications = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$material_specifications = $this->crud->get_all_with_where('material_specification','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('material_specification','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($material_specifications) ? $material_specifications[0]->id : '';
		$return['material'] = ($material_specifications) ? $material_specifications[0]->material : '';
		$return['material_specification'] = ($material_specifications) ? $material_specifications[0]->material_specification : '';
		if($this->applib->have_access_role(MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID,"view")) {
			set_page('item/material_specification', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	function material_specification_datatable(){
		$config['select'] = 'id, material, material_specification';
		$config['table'] = 'material_specification';
		$config['column_order'] = array(null, 'material', 'material_specification');
		$config['column_search'] = array('material', 'material_specification');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '  <a href="' . base_url('item/material_specification/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->material;
			$row[] = $item_list->material_specification;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function update_material_specification()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('material_specification', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Material Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_material_specification()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('material_specification',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Material Added Successfully');
			echo json_encode($post_data);
		}
	}

	function item_class($id=''){
		$items_class = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$items_class = $this->crud->get_all_with_where('item_class','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_class','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($items_class) ? $items_class[0]->id : '';
		$return['item_class'] = ($items_class) ? $items_class[0]->item_class : '';
		if($this->applib->have_access_role(MASTER_ITEM_CLASS_MENU_ID,"view")) {
			set_page('item/item_class', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	function item_class_datatable(){
		$config['select'] = 'id, item_class';
		$config['table'] = 'item_class';
		$config['column_order'] = array(null, 'item_class');
		$config['column_search'] = array('item_class');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_CLASS_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_CLASS_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' | <a href="' . base_url('item/item_class/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->item_class;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}


	function update_item_class()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_class', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Class Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_item_class()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_class',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Class Added Successfully');
			echo json_encode($post_data);
		}
	}

	function excise($id=''){
		$excises = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$excises = $this->crud->get_all_with_where('excise','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('excise','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($excises) ? $excises[0]->id : '';
		$return['description'] = ($excises) ? $excises[0]->description : '';
		$return['sub_heading'] = ($excises) ? $excises[0]->sub_heading : '';
		if($this->applib->have_access_role(MASTER_ITEM_EXCISE_MENU_ID,"view")) {
			set_page('item/excise', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function product_group($id = ''){
		$product_group = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$product_group = $this->crud->get_all_with_where('product_group','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('product_group','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($product_group) ? $product_group[0]->id : '';
		$return['name'] = ($product_group) ? $product_group[0]->name : '';
		if($this->applib->have_access_role(MASTER_PRODUCT_GROUP_MENU_ID,"view")) {
			set_page('item/product_group', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	function product_group_datatable(){
		$config['select'] = 'id, name';
		$config['table'] = 'product_group';
		$config['column_order'] = array(null, 'name');
		$config['column_search'] = array('name');

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_PRODUCT_GROUP_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_PRODUCT_GROUP_MENU_ID, "delete");
		foreach ($list as $product_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' <a href="' . base_url('item/product_group/' . $product_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> | ';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $product_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $product_list->name;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

	}

	function update_product_groups()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('product_group', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Product Group Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_product_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('product_group',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Product Group Added Successfully');
			echo json_encode($post_data);
		}
	}

	function excise_datatable(){
		$config['select'] = 'id, description, sub_heading';
		$config['table'] = 'excise';
		$config['column_order'] = array(null, 'id', 'description', 'sub_heading');
		$config['column_search'] = array('id','description', 'sub_heading');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_ITEM_EXCISE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_ITEM_EXCISE_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '  <a href="' . base_url('item/excise/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->id;
			$row[] = $item_list->description;
			$row[] = $item_list->sub_heading;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}


	function update_excise()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('excise', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Excise Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function add_excise()
	{
		$post_data = $this->input->post();
		unset($post_data['id']);
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('excise',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Excise Added Successfully');
			echo json_encode($post_data);
		}
	}
    
    //~ Purchase Project

	function purchase_project(){
		if($this->applib->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID,"view")) {
			set_page('item/purchase_project');	
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function purchase_project_datatable(){
		$config['select'] = 'project_id, project_name';
		$config['table'] = 'purchase_project';
		$config['column_order'] = array(null, 'project_name');
		$config['column_search'] = array('project_name');

		//$config['order'] = array('item_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID, "delete");
		foreach ($list as $project_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= '<a href="' . base_url('item/add_purchase_project/' . $project_list->project_id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' | <a onclick="return confirm(\'Are you sure ?\');" href="' . BASE_URL . "item/delete_purchase_project/" . $project_list->project_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $project_list->project_name;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function add_purchase_project($project_id = 0) {
        $data = array();
        $item_groups = $this->crud->get_all_records('item_group_code', '', '');
        $data['item_groups'] = $item_groups;
        $purchase_items = $this->crud->get_all_records('purchase_items', '', '');
        $data['purchase_items'] = $purchase_items;
        $uom = $this->crud->get_all_records('uom', '', '');
        $data['uom'] = $uom;
        if (!empty($project_id)) {
            if ($this->app_model->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID, "edit")) {
                $purchase_project = $this->crud->get_row_by_id('purchase_project', array('project_id' => $project_id));
                $data['purchase_project'] = $purchase_project[0];
                $data['purchase_project']->ig_description = $this->crud->get_column_value_by_id('item_group_code', 'ig_description', array('id' => $data['purchase_project']->item_group_id));
                $purchase_project_lineitems = $this->crud->get_row_by_id('purchase_project_items', array('project_id' => $project_id));
                $data['purchase_project_lineitems'] = json_encode($purchase_project_lineitems);
                $items = $this->db->query('SELECT pi.id as item_id, pi.item_name as item_name,pi.item_code FROM purchase_items pi WHERE pi.item_group="'. $data['purchase_project']->item_group_id .'"');
                $item = $items->result();
                $data['group_wise_items'] = json_encode($item);
                set_page('item/add_purchase_project', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            if ($this->applib->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID, "add")) {
                set_page('item/add_purchase_project', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }
    
    function get_items_uom_select_from_item_group(){
        $this->db->select('pi.id as item_id, pi.item_name as item_name,pi.item_code,required_qty AS qty_id,pi.required_uom as uom_id');
        $this->db->from('purchase_items pi');
        $this->db->where('pi.item_group="'. $_POST['item_group_id'] . '"');
        if(!empty($_POST['item_make_id']) || !empty($_POST['raw_material_group_id']) || !empty($_POST['supplier_make_id'])){
            $this->db->group_start();
                $this->db->or_where_in('pi.item_make_id', $_POST['item_make_id']);
                $this->db->or_where_in('pi.raw_material_group_id', $_POST['raw_material_group_id']);
                $this->db->or_where_in('pi.supplier_make_id', $_POST['supplier_make_id']);
                $this->db->where('1', '1');
            $this->db->group_end();
        }
        $query = $this->db->get();
        $item = $query->result();
//        echo '<pre>'.$this->db->last_query(); exit;
        echo json_encode($item);
        exit();
    }
        
    function save_purchase_project() {
        $post_data = $this->input->post();
        $line_items_data = $post_data['line_items_data'];
//        echo '<pre>'; print_r($post_data); exit;
        if (isset($post_data['purchase_project']['project_id']) && !empty($post_data['purchase_project']['project_id'])) {
            $project_id = $post_data['purchase_project']['project_id'];
            unset($post_data['purchase_project']['project_id']);
            $post_data['purchase_project']['updated_at'] = $this->now_time;
            $post_data['purchase_project']['updated_by'] = $this->staff_id;            
            $result = $this->db->where('project_id',$project_id)
                                ->update('purchase_project', $post_data['purchase_project']);
            if ($result) {
                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Project Updated Successfully');
                
                foreach ($post_data['deleted_ppi_id'] as $pp_id){
                    $this->crud->delete('purchase_project_items', array('ppi_id' => $pp_id));
                }
                
                if(!empty($post_data['line_items_data'])){
                    $lineitem_key = 0;
                    foreach($post_data['line_items_data']['item_id'] as $value) {
                        $add_lineitem = array();
                        $add_lineitem['project_id'] = $project_id;
                        $add_lineitem['item_id'] = $value;
                        $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $post_data['line_items_data']['item_id'][$lineitem_key]));
                        $add_lineitem['item_name'] = $purchase_item[0]->item_name;
                        $add_lineitem['item_code'] = $purchase_item[0]->item_code;
                        $add_lineitem['item_group_id'] = $purchase_item[0]->item_group;                        
                        $add_lineitem['qty'] = isset($post_data['line_items_data']['qty'][$lineitem_key]) && !empty($post_data['line_items_data']['qty'][$lineitem_key]) ? $post_data['line_items_data']['qty'][$lineitem_key] : NULL;
                        $add_lineitem['uom_id'] = isset($post_data['line_items_data']['uom'][$lineitem_key]) && !empty($post_data['line_items_data']['uom'][$lineitem_key]) ? $post_data['line_items_data']['uom'][$lineitem_key] : NULL;
                        if($post_data['line_items_data']['ppi_id'][$lineitem_key] == ""){
                            $add_lineitem['created_at'] = $this->now_time;
                            $add_lineitem['created_by'] = $this->staff_id;
                            $this->crud->insert('purchase_project_items', $add_lineitem);
                        } else {
                            $add_lineitem['updated_at'] = $this->now_time;
                            $add_lineitem['updated_by'] = $this->staff_id;
                            $this->db->where('ppi_id',$post_data['line_items_data']['ppi_id'][$lineitem_key])
                                    ->update('purchase_project_items', $add_lineitem);
                        }
                        $lineitem_key++;
                    }
                }
            }
        } else {
            if(!empty($line_items_data['item_make_id'])){
                $item_make_id = implode(',', $line_items_data['item_make_id']);
            }
            if(!empty($line_items_data['raw_material_group_id'])){
                $raw_material_group_id = implode(',', $line_items_data['raw_material_group_id']);
            }
            if(!empty($line_items_data['supplier_make_id'])){
                $supplier_make_id = implode(',', $line_items_data['supplier_make_id']);
            }
            $add_project['project_name'] = $post_data['purchase_project']['project_name'];
            $add_project['item_group_id'] = $line_items_data['item_group_id'];
            $add_project['item_make_id'] = $item_make_id;
            $add_project['raw_material_group_id'] = $raw_material_group_id;
            $add_project['supplier_make_id'] = $supplier_make_id;
            $add_project['created_by'] = $this->staff_id;
            $add_project['updated_by'] = $this->staff_id;
            $add_project['created_at'] = $this->now_time;
            $add_project['updated_at'] = $this->now_time;
            $result = $this->crud->insert('purchase_project', $add_project);
            $project_id = $this->db->insert_id();
            if ($result) {
                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Project Added Successfully');
                foreach ($line_items_data['item_id'] as $lineitem_key => $lineitem_id) {
                    $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $lineitem_id));
                    if(!empty($purchase_item)){
                        $purchase_item = $purchase_item[0];
                        $add_lineitem = array();
                        $add_lineitem['project_id'] = $project_id;
                        $add_lineitem['item_id'] = $lineitem_id;
                        $add_lineitem['item_name'] = $purchase_item->item_name;
                        $add_lineitem['item_code'] = $purchase_item->item_code;
                        $add_lineitem['item_group_id'] = $purchase_item->item_group;
                        $add_lineitem['qty'] = isset($line_items_data['qty'][$lineitem_key]) && !empty($line_items_data['qty'][$lineitem_key]) ? $line_items_data['qty'][$lineitem_key] : NULL;
                        $add_lineitem['uom_id'] = isset($line_items_data['uom'][$lineitem_key]) && !empty($line_items_data['uom'][$lineitem_key]) ? $line_items_data['uom'][$lineitem_key] : NULL;
//                        $add_lineitem['uom_name'] = $lineitem->uom_name;
//                        $add_lineitem['project_reference_qty'] = !empty($lineitem->project_reference_qty) ? $lineitem->project_reference_qty : NULL;
//                        $add_lineitem['project_reference_qty_uom'] = !empty($lineitem->project_reference_qty_uom) ? $lineitem->project_reference_qty_uom : NULL;
//                        $add_lineitem['project_reference_qty_uom_name'] = !empty($lineitem->project_reference_qty_uom_name) ? $lineitem->project_reference_qty_uom_name : NULL;
                        $add_lineitem['created_at'] = $this->now_time;
                        $add_lineitem['updated_at'] = $this->now_time;
                        $add_lineitem['created_by'] = $this->staff_id;
                        $add_lineitem['updated_by'] = $this->staff_id;
                        $this->crud->insert('purchase_project_items', $add_lineitem);
                        if ($result) {
                            $return['success'] = "Updated";
                            $this->session->set_flashdata('success', true);
                            $this->session->set_flashdata('message', 'Project Added Successfully');
                        }
                    }
                }
            }
        }
        print json_encode($return);
        exit;
    }

    function delete_purchase_project($id){
		$role_delete = $this->app_model->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID, "delete");
		if ($role_delete) {
            $where_array = array("project_id" => $id);
            $result = $this->crud->delete("purchase_project", $where_array);
            if($result['error'] == 'Error'){
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message','You cannot delete this Item. This Item has been used.');
            } elseif($result['success'] == 'Deleted') {
                $this->crud->delete("purchase_project_items", $where_array);
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Purchase Project has been deleted');
            }
            redirect(BASE_URL . "item/purchase_project");
        } else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Deleted Successfully');
	}
    
    function raw_material_group($id = ''){
		$item_group = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$row_material_group = $this->crud->get_all_with_where('raw_material_group','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('raw_material_group','id','ASC');
		
		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($row_material_group) ? $row_material_group[0]->id : '';
		$return['group_name'] = ($row_material_group) ? $row_material_group[0]->group_name : '';
		$return['group_desc'] = ($row_material_group) ? $row_material_group[0]->group_desc : '';
		if($this->applib->have_access_role(MASTER_RAW_MATERIAL_GROUP_MENU_ID,"view")) {
			set_page('item/raw_material_group', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}

	function raw_material_group_datatable(){
		$config['select'] = 'id, group_name, group_desc';
		$config['table'] = 'raw_material_group';
		$config['column_order'] = array(null, 'group_name', 'group_desc');
		$config['column_search'] = array('group_name', 'group_desc');

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_RAW_MATERIAL_GROUP_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_RAW_MATERIAL_GROUP_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' <a href="' . base_url('item/raw_material_group/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> | ';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->group_name;
			$row[] = $item_list->group_desc;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

	}
//
//
//
	function update_raw_material_group()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('raw_material_group', $post_data, array('id' => $post_data['id']));

		if ($result) {

			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Raw Material Group Updated Successfully');
			echo json_encode($post_data);
		}
	}
//
	function add_raw_material_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('raw_material_group',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Raw Material Group Added Successfully');
			echo json_encode($post_data);
		}
	}
    
    function supplier_make($id = ''){
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$supplier_make = $this->crud->get_all_with_where('supplier_make','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('supplier_make','id','ASC');
		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($supplier_make) ? $supplier_make[0]->id : '';
		$return['supplier_make_name'] = ($supplier_make) ? $supplier_make[0]->supplier_make_name : '';
		if($this->applib->have_access_role(MASTER_SUPPLIER_MAKE_MENU_ID,"view")) {
			set_page('item/supplier_make', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function supplier_make_datatable(){
        $config = array();
		$config['select'] = 'id, supplier_make_name';
		$config['table'] = 'supplier_make';
		$config['column_order'] = array(null, 'supplier_make_name');
		$config['column_search'] = array('supplier_make_name');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_SUPPLIER_MAKE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_SUPPLIER_MAKE_MENU_ID, "delete");
		foreach ($list as $item_list) {
			$row = array();
			$action = '';
			if ($isEdit) {
				$action .= ' <a href="' . base_url('item/supplier_make/' . $item_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> | ';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('item/delete/' . $item_list->id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $item_list->supplier_make_name;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);

	}
    
	function update_supplier_make(){
		$post_data = $this->input->post();
        $where_array = array();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('supplier_make', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Supplier Make Updated Successfully');
			echo json_encode($post_data);
		}
	}
    
	function add_supplier_make(){
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('supplier_make',$post_data);
		if($result){
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message','Supplier Make Added Successfully');
			echo json_encode($post_data);
		}
	}
    
    function get_items_group_code($item_group_id) {
        $data = array();
        if(!empty($item_group_id)){
            $ig_description = $this->crud->get_column_value_by_id('item_group_code','ig_description', array('id' => $item_group_id));
            $data['ig_description'] = $ig_description;
        }
//        echo '<pre>'; print_r($data); exit;
        print json_encode($data);
        exit;
    }
}
