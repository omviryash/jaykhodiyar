<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Invoice extends CI_Controller
{

	protected $staff_id = 1;
	function __construct(){
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

	}

	function get_challan(){
		$challan_id = $this->input->get_post("challan_id");
		$challan_data = $this->crud->get_only_challan_data($challan_id);
        $challan_data->challan_date = substr($challan_data->challan_date, 8, 2) .'-'. substr($challan_data->challan_date, 5, 2) .'-'. substr($challan_data->challan_date, 0, 4);
        
        if(!empty($challan_data->sales_order_id)){
            $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $challan_data->sales_order_id));
            $sales_order_data = $sales_order_data[0];
            $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
            $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
            $sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
            
            $challan_items = $this->crud->get_challan_item_with_order_item_detail($challan_id, $challan_data->sales_order_id);
            
        } else if(!empty($challan_data->proforma_invoice_id)){
            $sales_order_data = $this->crud->get_row_by_id('proforma_invoices', array('id' => $challan_data->proforma_invoice_id));
            $sales_order_data = $sales_order_data[0];
            $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
            $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
            $sales_order_data->proforma_invoice_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
            unset($sales_order_data->sales_order_date);
            $challan_items = $this->crud->get_challan_item_with_proforma_item_detail($challan_id, $challan_data->proforma_invoice_id);
        }
        
        $challan_item = $challan_items[0];
        $challan_item['challan_id'] = $challan_data->challan_id;
        $challan_item['challan_no'] = $this->crud->get_column_value_by_id('challans', 'challan_no', array('id' => $challan_data->challan_id));
        $challan_item['item_rate'] = $challan_item['rate'];
        $challan_item['amount'] = $challan_item['rate'];
        if(!empty($challan_item['disc_value'])){
            $challan_item['disc_value'] = $challan_item['disc_value'] / $challan_item['so_qty'];
            $challan_item['disc_per'] = ($challan_item['disc_value'] * 100 ) / $challan_item['rate'];
        } else {
            $challan_item['disc_per'] = 0;
            $challan_item['disc_value'] = 0;
        }
        $taxable_value = $challan_item['amount'] - $challan_item['disc_value'];
        if($challan_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
            $challan_item['igst_amount'] = ($challan_item['igst'] * $taxable_value ) / 100;
            $challan_item['cgst_amount'] = ($challan_item['cgst'] * $taxable_value ) / 100;
            $challan_item['sgst_amount'] = ($challan_item['sgst'] * $taxable_value ) / 100;
        } else {
            $challan_item['igst'] = 0;
            $challan_item['cgst'] = 0;
            $challan_item['sgst'] = 0;
            $challan_item['igst_amount'] = 0;
            $challan_item['cgst_amount'] = 0;
            $challan_item['sgst_amount'] = 0;
        }
        $challan_item['net_amount'] = $taxable_value + $challan_item['igst_amount'] + $challan_item['cgst_amount'] + $challan_item['sgst_amount'];
        $challan_item['lr_date'] = substr($challan_item['lr_date'], 8, 2) .'-'. substr($challan_item['lr_date'], 5, 2) .'-'. substr($challan_item['lr_date'], 0, 4);
        echo json_encode(array("sales_order_data" => $sales_order_data, "challan_data" => $challan_data, "challan_item" => $challan_item));
	}
    
	/**
	 * Challan DataTable
	 */
	function challan_datatable(){
		$select = 'c.id,c.sales_order_id,c.proforma_invoice_id,c.challan_no,c.challan_date,ci.item_code,';
		$select .= 'so.id as sales_order_id,qso.quotation_no AS qso_quotation_no,qso.enquiry_no AS qso_enquiry_no,qpi.quotation_no AS qpi_quotation_no,qpi.enquiry_no AS qpi_enquiry_no,';
		$select .= 'so.sales_order_no,so.sales_order_date,so.sales_order_status_id,pi.proforma_invoice_no,pi.sales_order_date,pi.sales_order_status_id,';
		$select .= 'p.party_name,p.phone_no,p.email_id';
		$config['table'] = 'challans c';
		$config['select'] = $select;
		$config['joins'][] =  array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'quotations qso', 'join_by' => 'qso.id = so.quotation_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'quotations qpi', 'join_by' => 'qpi.id = pi.quotation_id','join_type'=>'left');
		$config['order'] = array('c.id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
        
        if (!empty($_POST['invoice_for']) && $_POST['invoice_for'] == 'export') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			$config['column_order'] = array(null, null, null, 'c.challan_date', 'p.party_name', 'ci.item_code', 'p.phone_no', 'p.email_id');
			$config['column_search'] = array('qso.enquiry_no', 'qso.quotation_no','qpi.enquiry_no', 'qpi.quotation_no', 'pi.proforma_invoice_no','so.sales_order_no', 'c.challan_no','DATE_FORMAT(c.challan_date,"%d-%m-%Y")', 'p.party_name', 'ci.item_code', 'p.phone_no', 'p.email_id');
		} else if (!empty($_POST['invoice_for']) && $_POST['invoice_for'] == 'domestic') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			$config['column_order'] = array('qso.enquiry_no', 'qso.quotation_no', 'so.sales_order_no', 'c.challan_no', 'c.challan_date', 'p.party_name', 'ci.item_code', 'p.phone_no', 'p.email_id');
			$config['column_search'] = array('qso.enquiry_no', 'qso.quotation_no', 'so.sales_order_no', 'c.challan_no', 'DATE_FORMAT(c.challan_date,"%d-%m-%Y")', 'p.party_name', 'ci.item_code', 'p.phone_no', 'p.email_id');
		}
        $config['wheres'][] = array('column_name' => 'c.is_invoice_created !=', 'column_value' => 1);
		
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $sales_challan) {
			$row = array();
			if(!empty($sales_challan->sales_order_id)){
                $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->qso_enquiry_no</a>";
                $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->qso_quotation_no</a>";
                $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->sales_order_no</a>";       
            } else if(!empty($sales_challan->proforma_invoice_id)){
                $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->qpi_enquiry_no</a>";
                $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->qpi_quotation_no</a>";
                $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->proforma_invoice_no</a>";                
            }
			$row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->challan_no</a>";
			$row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >".date('d-m-Y', strtotime($sales_challan->challan_date))."</a>";
            $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->party_name</a>";
            $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' >$sales_challan->item_code</a>";
            $phone_no = substr($sales_challan->phone_no,0,10);
            $sales_challan->phone_no = str_replace(',', ', ', $sales_challan->phone_no);
            $row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' data-toggle='tooltip' data-placement='bottom' title='".$sales_challan->phone_no."'>$phone_no</a>";
            $email_id = substr($sales_challan->email_id,0,14);
            $sales_challan->email_id = str_replace(',', ', ', $sales_challan->email_id);
			$row[] = "<a href='javascript:void(0);' class='challan_row' data-challan_id='$sales_challan->id' data-toggle='tooltip' data-placement='bottom' title='".$sales_challan->email_id."'>$email_id</a>";
			
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
    
    function ajax_load_challan_details($challan_id = 0){
        if ($challan_id != 0) {
            $sales_order_id = $this->crud->get_id_by_val('challans','sales_order_id','id',$challan_id);
            $proforma_invoice_id = $this->crud->get_id_by_val('challans','proforma_invoice_id','id',$challan_id);
            if(!empty($sales_order_id)){
                $challan_items = $this->crud->get_challan_item_with_order_item_detail($challan_id, $sales_order_id);   
            } else if(!empty($proforma_invoice_id)){
                $challan_items = $this->crud->get_challan_item_with_proforma_item_detail($challan_id, $proforma_invoice_id);
            }
            $challan_data = $this->crud->get_challan($challan_id, $sales_order_id);
            $challan_item = $challan_items[0];
            $challan_item['challan_id'] = $challan_data->challan_id;
            $challan_item['challan_no'] = $challan_data->challan_no;
            $challan_item['item_rate'] = $challan_item['rate'];
            $challan_item['amount'] = $challan_item['rate'];
            if(!empty($challan_item['disc_value'])){
                $challan_item['disc_value'] = $challan_item['disc_value'] / $challan_item['so_qty'];
                $challan_item['disc_per'] = ($challan_item['disc_value'] * 100 ) / $challan_item['rate'];
            } else {
                $challan_item['disc_per'] = 0;
                $challan_item['disc_value'] = 0;
            }
            $taxable_value = $challan_item['amount'] - $challan_item['disc_value'];
            if($challan_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
                $challan_item['igst_amount'] = ($challan_item['igst'] * $taxable_value ) / 100;
                $challan_item['cgst_amount'] = ($challan_item['cgst'] * $taxable_value ) / 100;
                $challan_item['sgst_amount'] = ($challan_item['sgst'] * $taxable_value ) / 100;
            } else {
                $challan_item['igst'] = 0;
                $challan_item['cgst'] = 0;
                $challan_item['sgst'] = 0;
                $challan_item['igst_amount'] = 0;
                $challan_item['cgst_amount'] = 0;
                $challan_item['sgst_amount'] = 0;
            }
            $challan_item['net_amount'] = $taxable_value + $challan_item['igst_amount'] + $challan_item['cgst_amount'] + $challan_item['sgst_amount'];
            echo json_encode(array("challan_data" => $challan_data, "challan_item" => $challan_item));
			exit;
		}
	}

	/**
	 *    Invoice
	 */
	function add_domestic($invoice_id = ''){
		$data = array();
		if(!empty($invoice_id)){
			if ($this->app_model->have_access_role(INVOICE_MODULE_ID, "edit")) {
				$invoice_data = $this->crud->get_row_by_id('invoices', array('id' => $invoice_id));
				if(empty($invoice_data)){
					redirect("invoice/invoice_list"); exit;
				}
				$invoice_data = $invoice_data[0];
                
                $challan_data = $this->crud->get_row_by_id('challans', array('id' => $invoice_data->challan_id));
                $challan_data = $challan_data[0];
                $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $challan_data->sales_order_id));
                $sales_order_data = $sales_order_data[0];
                $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
                $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
                $sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
                $sales_order_data->challan_no = $challan_data->challan_no;
                $sales_order_data->challan_date = substr($challan_data->challan_date, 8, 2) .'-'. substr($challan_data->challan_date, 5, 2) .'-'. substr($challan_data->challan_date, 0, 4);
                
				$login_data = $this->crud->get_row_by_id('invoice_logins', array('invoice_id' => $invoice_id));
				$login_data = $login_data[0];
				$login_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->created_by_id));
				$login_data->created_at = substr($login_data->created_at, 8, 2) .'-'. substr($login_data->created_at, 5, 2) .'-'. substr($login_data->created_at, 0, 4);
				$login_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->last_modified_by_id));
				$login_data->updated_at = substr($login_data->updated_at, 8, 2) .'-'. substr($login_data->updated_at, 5, 2) .'-'. substr($login_data->updated_at, 0, 4);
				$login_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->approved_by_id));
				$login_data->approved_at = substr($login_data->approved_date_time, 8, 2) .'-'. substr($login_data->approved_date_time, 5, 2) .'-'. substr($login_data->approved_date_time, 0, 4);
				$data = array(
					'sales_order_data' => $sales_order_data,
					'invoice_data' => $invoice_data,
					'login_data' => $login_data,
					'party_contact_person' => $this->crud->get_contact_person_by_party($invoice_data->sales_to_party_id),
				);
				if(!empty($data['invoice_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['invoice_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if(!empty($designation)){
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if(!empty($department)){
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
				$lineitems = array();
				$where = array('invoice_id' => $invoice_id);
				$invoice_lineitems = $this->crud->get_row_by_id('invoice_items', $where);
				foreach($invoice_lineitems as $invoice_lineitem){
                    $invoice_lineitem->challan_no = $this->crud->get_column_value_by_id('challans', 'challan_no', array('id' => $invoice_lineitem->challan_id));
					$invoice_lineitem->disc_per = !empty($invoice_lineitem->disc_per)? $invoice_lineitem->disc_per : '0';
					$invoice_lineitem->disc_value = !empty($invoice_lineitem->disc_value)? $invoice_lineitem->disc_value : '0';
					$invoice_lineitem->quantity = !empty($invoice_lineitem->quantity)? $invoice_lineitem->quantity : '0';
					$invoice_lineitem->item_rate = !empty($invoice_lineitem->rate)? $invoice_lineitem->rate : '0';

					$invoice_lineitem->igst = !empty($invoice_lineitem->igst)? $invoice_lineitem->igst : '0';
					$invoice_lineitem->cgst = !empty($invoice_lineitem->cgst)? $invoice_lineitem->cgst : '0';
					$invoice_lineitem->sgst = !empty($invoice_lineitem->sgst)? $invoice_lineitem->sgst : '0';

					$pure_amount = $invoice_lineitem->quantity * $invoice_lineitem->item_rate;

//					$discount_amount = $pure_amount * $invoice_lineitem->disc_per / 100;
                    $discount_amount = $invoice_lineitem->disc_value;
					$taxable_amount = $pure_amount - $discount_amount;
					$igst_amount = $taxable_amount * $invoice_lineitem->igst / 100;
					$cgst_amount = $taxable_amount * $invoice_lineitem->cgst / 100;
					$sgst_amount = $taxable_amount * $invoice_lineitem->sgst / 100;
					$net_amount = $taxable_amount + $igst_amount + $cgst_amount + $sgst_amount;

					$invoice_lineitem->igst_amount = $igst_amount;
					$invoice_lineitem->cgst_amount = $cgst_amount;
					$invoice_lineitem->sgst_amount = $sgst_amount;
					$invoice_lineitem->net_amount = $net_amount;
					$lineitems[] = json_encode($invoice_lineitem);
				}
				$data['invoice_lineitems'] = implode(',', $lineitems);
//				echo '<pre>';print_r($data); exit;
				set_page('dispatch/invoices/add_domestic', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(INVOICE_MODULE_ID,"add")) {
				set_page('dispatch/invoices/add_domestic', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function save_domestic_invoice(){
		$post_data = $this->input->post();
        $return = array();
        
		// check party validations
		$party_id = $post_data['party']['party_id'];
		if ($party_id == "" || $party_id == "0") {
			echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
			exit;
		}

//		echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
		//		print_r($line_items_data); exit;
        $send_sms = 0;
        if(isset($post_data['invoice_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['invoice_data']['send_sms']);
        }
		/*--------- Convert Date as Mysql Format -------------*/
        $post_data['invoice_data']['invoice_date'] = !empty(strtotime($post_data['invoice_data']['invoice_date'])) ? date("Y-m-d", strtotime($post_data['invoice_data']['invoice_date'])) : NULL;
		$post_data['invoice_data']['kind_attn_id'] = !empty($post_data['invoice_data']['kind_attn_id']) ? $post_data['invoice_data']['kind_attn_id'] : NULL;
		$post_data['invoice_data']['invoice_type_id'] = !empty($post_data['invoice_data']['invoice_type_id']) ? $post_data['invoice_data']['invoice_type_id'] : NULL;
		$post_data['invoice_data']['delivery_through_id'] = !empty($post_data['invoice_data']['delivery_through_id']) ? $post_data['invoice_data']['delivery_through_id'] : NULL;
        $post_data['invoice_data']['lr_date'] = !empty(strtotime($post_data['invoice_data']['lr_date'])) ? date("Y-m-d", strtotime($post_data['invoice_data']['lr_date'])) : NULL;
        $post_data['invoice_data']['currency_id'] = !empty($post_data['invoice_data']['currency_id']) ? $post_data['invoice_data']['currency_id'] : NULL;
        $post_data['invoice_data']['packing_forwarding_cgst'] = !empty($post_data['invoice_data']['packing_forwarding_cgst']) ? $post_data['invoice_data']['packing_forwarding_cgst'] : NULL;
        $post_data['invoice_data']['packing_forwarding_sgst'] = !empty($post_data['invoice_data']['packing_forwarding_sgst']) ? $post_data['invoice_data']['packing_forwarding_sgst'] : NULL;
        $post_data['invoice_data']['packing_forwarding_igst'] = !empty($post_data['invoice_data']['packing_forwarding_igst']) ? $post_data['invoice_data']['packing_forwarding_igst'] : NULL;
        $post_data['invoice_data']['transit_insurance_cgst'] = !empty($post_data['invoice_data']['transit_insurance_cgst']) ? $post_data['invoice_data']['transit_insurance_cgst'] : NULL;
        $post_data['invoice_data']['transit_insurance_sgst'] = !empty($post_data['invoice_data']['transit_insurance_sgst']) ? $post_data['invoice_data']['transit_insurance_sgst'] : NULL;
        $post_data['invoice_data']['transit_insurance_igst'] = !empty($post_data['invoice_data']['transit_insurance_igst']) ? $post_data['invoice_data']['transit_insurance_igst'] : NULL;
        $post_data['invoice_data']['port_of_discharge'] = !empty($post_data['invoice_data']['port_of_discharge']) ? $post_data['invoice_data']['port_of_discharge'] : NULL;
        $post_data['invoice_data']['port_place_of_delivery'] = !empty($post_data['invoice_data']['port_place_of_delivery']) ? $post_data['invoice_data']['port_place_of_delivery'] : NULL;
        $post_data['invoice_data']['name_of_shipment'] = !empty($post_data['invoice_data']['name_of_shipment']) ? $post_data['invoice_data']['name_of_shipment'] : NULL;
        $post_data['invoice_data']['vehicle_no'] = !empty($post_data['invoice_data']['vehicle_no']) ? $post_data['invoice_data']['vehicle_no'] : NULL;
        $post_data['invoice_data']['lr_no'] = !empty($post_data['invoice_data']['lr_no']) ? $post_data['invoice_data']['lr_no'] : NULL;
        $post_data['invoice_data']['lr_date'] = !empty($post_data['invoice_data']['lr_date']) ? $post_data['invoice_data']['lr_date'] : NULL;
        $post_data['invoice_data']['sales_to_party_id'] = !empty($post_data['invoice_data']['sales_to_party_id']) ? $post_data['invoice_data']['sales_to_party_id'] : NULL;
		$post_data['invoice_data']['sales_to_party_id'] = $party_id;

		if(isset($post_data['invoice_data']['invoice_id']) && !empty($post_data['invoice_data']['invoice_id'])){

			$invoice_no = $post_data['invoice_data']['invoice_no'];
			$invoice_result = $this->crud->get_id_by_val('invoices', 'id', 'invoice_no', $invoice_no);
			if(!empty($invoice_result) && $invoice_result != $post_data['invoice_data']['invoice_id']){
				echo json_encode(array("success" => 'false', 'msg' => 'Invoice No. Already Exist!'));
				exit;
			}

			$invoice_id = $post_data['invoice_data']['invoice_id'];
			if (isset($post_data['invoice_data']['invoice_id']))
				unset($post_data['invoice_data']['invoice_id']);

			$this->db->where('id', $invoice_id);
			$result = $this->db->update('invoices', $post_data['invoice_data']);
			if($result){

				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->update('invoice_logins', $login_data, array('invoice_id' => $invoice_id));

				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Invoice Updated Successfully');
				$where_array = array("invoice_id" => $invoice_id);
                
                /*-- Update Challan Status --*/
                $invoice_lineitems = $this->crud->get_row_by_id('invoice_items', $where_array);
				foreach($invoice_lineitems as $invoice_lineitem){
                    $this->crud->update('challans',array('is_invoice_created'=> 0),array('id'=>$invoice_lineitem->challan_id));
                }
				$this->crud->delete("invoice_items", $where_array);
				foreach($line_items_data[0] as $lineitem){
                    
                    /*-- Update Challan Status --*/
                    $this->crud->update('challans',array('is_invoice_created'=> 1),array('id'=>$lineitem->challan_id));
                    
					$add_lineitem = array();
					$add_lineitem['invoice_id'] = $invoice_id;
					$add_lineitem['challan_id'] = $lineitem->challan_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					if(isset($lineitem->item_description) && !empty($lineitem->item_description)){
                        $add_lineitem['item_description'] = $lineitem->item_description;
                    }
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['cgst'] = $lineitem->cgst;
					$add_lineitem['sgst'] = $lineitem->sgst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('invoice_items',$add_lineitem);
				}				
			}
		} else {
			$post_data['invoice_data']['invoice_no'] = sprintf("%04d", $this->applib->get_new_invoice_no($post_data['invoice_data']['invoice_date']));
			$post_data['invoice_data']['invoice_no'] = $this->applib->get_invoice_no($post_data['invoice_data']['invoice_no'], $post_data['invoice_data']['invoice_date']);
            
            $dataToInsert = $post_data['invoice_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			// Unset quotation_id from Inquiry Data
			if (isset($dataToInsert['invoice_id']))
				unset($dataToInsert['invoice_id']);

			$result = $this->db->insert('invoices', $dataToInsert);
//            echo $this->db->last_query(); exit;
			$invoice_id = $this->db->insert_id();
			if($result){

				$login_data['invoice_id'] = $invoice_id;
				$login_data['created_by_id'] = $this->staff_id;
				$login_data['created_date_time'] = $this->now_time;
				$login_data['created_at'] = $this->now_time;
				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->insert('invoice_logins', $login_data);
                
				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
                    
                    /*-- Update Challan Status --*/
                    $this->crud->update('challans',array('is_invoice_created'=> 1),array('id'=>$lineitem->challan_id));
                    
					$add_lineitem = array();
					$add_lineitem['invoice_id'] = $invoice_id;
                    $add_lineitem['challan_id'] = $lineitem->challan_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					if(isset($lineitem->item_description) && !empty($lineitem->item_description)){
                        $add_lineitem['item_description'] = $lineitem->item_description;
                    }
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['cgst'] = $lineitem->cgst;
					$add_lineitem['sgst'] = $lineitem->sgst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('invoice_items',$add_lineitem);
				}
			}
		}
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['party']['party_id']);
            $this->applib->send_sms('invoices', $invoice_id, $party_phone_no, SEND_DISPATCH_CHALLAN_AND_INVOICE_SMS_ID);
        }
		print json_encode($return);
		exit;
	}
	
    function add_export($invoice_id = ''){
		$data = array();
		if(!empty($invoice_id)){
			if ($this->app_model->have_access_role(INVOICE_MODULE_ID, "edit")) {
				$invoice_data = $this->crud->get_row_by_id('invoices', array('id' => $invoice_id));
				if(empty($invoice_data)){
					redirect("invoice/invoice_list"); exit;
				}
				$invoice_data = $invoice_data[0];
                
                $challan_data = $this->crud->get_row_by_id('challans', array('id' => $invoice_data->challan_id));
                $challan_data = $challan_data[0];
                if(!empty($challan_data->sales_order_id)){
                    $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $challan_data->sales_order_id));
                    $sales_order_data = $sales_order_data[0];
                    if(isset($sales_order_data->bank_detail)){
                        unset($sales_order_data->bank_detail);
                    }
                    $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
                    $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
                    $sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
                } else if(!empty($challan_data->proforma_invoice_id)){
                    $sales_order_data = $this->crud->get_row_by_id('proforma_invoices', array('id' => $challan_data->proforma_invoice_id));
                    $sales_order_data = $sales_order_data[0];
                    if(isset($sales_order_data->bank_detail)){
                        unset($sales_order_data->bank_detail);
                    }
                    $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
                    $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
                    $sales_order_data->proforma_invoice_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
                    unset($sales_order_data->sales_order_date);
                }
                
                $sales_order_data->challan_no = $challan_data->challan_no;
                $sales_order_data->challan_date = substr($challan_data->challan_date, 8, 2) .'-'. substr($challan_data->challan_date, 5, 2) .'-'. substr($challan_data->challan_date, 0, 4);
                
				$login_data = $this->crud->get_row_by_id('invoice_logins', array('invoice_id' => $invoice_id));
				$login_data = $login_data[0];
				$login_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->created_by_id));
				$login_data->created_at = substr($login_data->created_at, 8, 2) .'-'. substr($login_data->created_at, 5, 2) .'-'. substr($login_data->created_at, 0, 4);
				$login_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->last_modified_by_id));
				$login_data->updated_at = substr($login_data->updated_at, 8, 2) .'-'. substr($login_data->updated_at, 5, 2) .'-'. substr($login_data->updated_at, 0, 4);
				$login_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->approved_by_id));
				$login_data->approved_at = substr($login_data->approved_date_time, 8, 2) .'-'. substr($login_data->approved_date_time, 5, 2) .'-'. substr($login_data->approved_date_time, 0, 4);
				$data = array(
					'sales_order_data' => $sales_order_data,
					'invoice_data' => $invoice_data,
					'login_data' => $login_data,
					'party_contact_person' => $this->crud->get_contact_person_by_party($invoice_data->sales_to_party_id),
                    'company_banks' => $this->crud->get_all_with_where('company_banks', 'id', 'ASC',array('for_export' => '1')),
				);
				if(!empty($data['invoice_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['invoice_data']->kind_attn_id));
					if(!empty($data['contact_person'])){
						$data['contact_person'] = $data['contact_person'][0];
						$designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
						$department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
						$data['contact_person']->designation = '';
						if(!empty($designation)){
							$data['contact_person']->designation = $designation[0]->designation;
						}
						$data['contact_person']->department = '';
						if(!empty($department)){
							$data['contact_person']->department = $department[0]->department;
						}
					}
				}
				$lineitems = array();
				$where = array('invoice_id' => $invoice_id);
				$invoice_lineitems = $this->crud->get_row_by_id('invoice_items', $where);
				foreach($invoice_lineitems as $invoice_lineitem){
                    $invoice_lineitem->challan_no = $this->crud->get_column_value_by_id('challans', 'challan_no', array('id' => $invoice_lineitem->challan_id));
					$invoice_lineitem->disc_per = !empty($invoice_lineitem->disc_per)? $invoice_lineitem->disc_per : '0';
					$invoice_lineitem->disc_value = !empty($invoice_lineitem->disc_value)? $invoice_lineitem->disc_value : '0';
					$invoice_lineitem->quantity = !empty($invoice_lineitem->quantity)? $invoice_lineitem->quantity : '0';
					$invoice_lineitem->item_rate = !empty($invoice_lineitem->rate)? $invoice_lineitem->rate : '0';

					$invoice_lineitem->igst = !empty($invoice_lineitem->igst)? $invoice_lineitem->igst : '0';
					$invoice_lineitem->cgst = !empty($invoice_lineitem->cgst)? $invoice_lineitem->cgst : '0';
					$invoice_lineitem->sgst = !empty($invoice_lineitem->sgst)? $invoice_lineitem->sgst : '0';

					$pure_amount = $invoice_lineitem->quantity * $invoice_lineitem->item_rate;

//					$discount_amount = $pure_amount * $invoice_lineitem->disc_per / 100;
                    $discount_amount = $invoice_lineitem->disc_value;
					$taxable_amount = $pure_amount - $discount_amount;
					$igst_amount = $taxable_amount * $invoice_lineitem->igst / 100;
					$cgst_amount = $taxable_amount * $invoice_lineitem->cgst / 100;
					$sgst_amount = $taxable_amount * $invoice_lineitem->sgst / 100;
					$net_amount = $taxable_amount + $igst_amount + $cgst_amount + $sgst_amount;

					$invoice_lineitem->igst_amount = $igst_amount;
					$invoice_lineitem->cgst_amount = $cgst_amount;
					$invoice_lineitem->sgst_amount = $sgst_amount;
					$invoice_lineitem->net_amount = $net_amount;
					$lineitems[] = json_encode($invoice_lineitem);
				}
				$data['invoice_lineitems'] = implode(',', $lineitems);				
//                echo '<pre>';print_r($data); exit;
				set_page('dispatch/invoices/add_export', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(INVOICE_MODULE_ID,"add")) {
                $data['company_banks'] = $this->crud->get_all_with_where('company_banks', 'id', 'ASC',array('for_export' => '1'));
				set_page('dispatch/invoices/add_export', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function save_export_invoice(){
		$post_data = $this->input->post();
		// check party validations
		$party_id = $post_data['party']['party_id'];
		if ($party_id == "" || $party_id == "0") {
			echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
			exit;
		}

//		echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
		//		print_r($line_items_data); exit;
        $send_sms = 0;
        if(isset($post_data['invoice_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['invoice_data']['send_sms']);
        }
		/*--------- Convert Date as Mysql Format -------------*/
		$post_data['invoice_data']['invoice_date'] = !empty(strtotime($post_data['invoice_data']['invoice_date'])) ? date("Y-m-d", strtotime($post_data['invoice_data']['invoice_date'])) : NULL;
		$post_data['invoice_data']['kind_attn_id'] = !empty($post_data['invoice_data']['kind_attn_id']) ? $post_data['invoice_data']['kind_attn_id'] : NULL;
		$post_data['invoice_data']['invoice_type_id'] = !empty($post_data['invoice_data']['invoice_type_id']) ? $post_data['invoice_data']['invoice_type_id'] : NULL;
		$post_data['invoice_data']['delivery_through_id'] = !empty($post_data['invoice_data']['delivery_through_id']) ? $post_data['invoice_data']['delivery_through_id'] : NULL;
        $post_data['invoice_data']['port_of_loading_country_id'] = !empty($post_data['invoice_data']['port_of_loading_country_id']) ? $post_data['invoice_data']['port_of_loading_country_id'] : NULL;
		$post_data['invoice_data']['port_of_discharge_country_id'] = !empty($post_data['invoice_data']['port_of_discharge_country_id']) ? $post_data['invoice_data']['place_of_delivery_country_id'] : NULL;
		$post_data['invoice_data']['place_of_delivery_country_id'] = !empty($post_data['invoice_data']['place_of_delivery_country_id']) ? $post_data['invoice_data']['place_of_delivery_country_id'] : NULL;
		$post_data['invoice_data']['sea_freight_type'] = !empty($post_data['invoice_data']['sea_freight_type']) ? $post_data['invoice_data']['sea_freight_type'] : NULL;
		$post_data['invoice_data']['origin_of_goods_country_id'] = !empty($post_data['invoice_data']['origin_of_goods_country_id']) ? $post_data['invoice_data']['origin_of_goods_country_id'] : NULL;
		$post_data['invoice_data']['final_destination_country_id'] = !empty($post_data['invoice_data']['final_destination_country_id']) ? $post_data['invoice_data']['final_destination_country_id'] : NULL;
        $post_data['invoice_data']['lc_date'] = !empty(strtotime($post_data['invoice_data']['lc_date'])) ? date("Y-m-d", strtotime($post_data['invoice_data']['lc_date'])) : NULL;
		$post_data['invoice_data']['currency_id'] = !empty($post_data['invoice_data']['currency_id']) ? $post_data['invoice_data']['currency_id'] : NULL;
		$post_data['invoice_data']['bank_detail'] = !empty($post_data['invoice_data']['bank_detail']) ? $post_data['invoice_data']['bank_detail'] : NULL;
		$post_data['invoice_data']['sales_to_party_id'] = $party_id;

		if(isset($post_data['invoice_data']['invoice_id']) && !empty($post_data['invoice_data']['invoice_id'])){

			$invoice_no = $post_data['invoice_data']['invoice_no'];
			$invoice_result = $this->crud->get_id_by_val('invoices', 'id', 'invoice_no', $invoice_no);
			if(!empty($invoice_result) && $invoice_result != $post_data['invoice_data']['invoice_id']){
				echo json_encode(array("success" => 'false', 'msg' => 'Invoice No. Already Exist!'));
				exit;
			}

			$invoice_id = $post_data['invoice_data']['invoice_id'];
			if (isset($post_data['invoice_data']['invoice_id']))
				unset($post_data['invoice_data']['invoice_id']);

			$this->db->where('id', $invoice_id);
			$result = $this->db->update('invoices', $post_data['invoice_data']);
			if($result){

				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->update('invoice_logins', $login_data, array('invoice_id' => $invoice_id));

				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Invoice Updated Successfully');
				$where_array = array("invoice_id" => $invoice_id);
                
                /*-- Update Challan Status --*/
                $invoice_lineitems = $this->crud->get_row_by_id('invoice_items', $where_array);
				foreach($invoice_lineitems as $invoice_lineitem){
                    $this->crud->update('challans',array('is_invoice_created'=> 0),array('id'=>$invoice_lineitem->challan_id));
                }
				$this->crud->delete("invoice_items", $where_array);
				foreach($line_items_data[0] as $lineitem){
                    
                    /*-- Update Challan Status --*/
                    $this->crud->update('challans',array('is_invoice_created'=> 1),array('id'=>$lineitem->challan_id));
                    
					$add_lineitem = array();
					$add_lineitem['invoice_id'] = $invoice_id;
					$add_lineitem['challan_id'] = $lineitem->challan_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_serial_no'] = $lineitem->item_serial_no;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					if(isset($lineitem->item_description) && !empty($lineitem->item_description)){
                        $add_lineitem['item_description'] = $lineitem->item_description;
                    }
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
					$add_lineitem['packing_mark'] = $lineitem->packing_mark;
					$add_lineitem['no_of_packing'] = $lineitem->no_of_packing;
                    $add_lineitem['gross_weight'] = $lineitem->gross_weight;
                    if(isset($lineitem->gross_weight_uom_id) && !empty($lineitem->gross_weight_uom_id)){
						$add_lineitem['gross_weight_uom_id'] = $lineitem->gross_weight_uom_id;
					}
                    $add_lineitem['net_weight'] = $lineitem->net_weight;
                    if(isset($lineitem->net_weight_uom_id) && !empty($lineitem->net_weight_uom_id)){
						$add_lineitem['net_weight_uom_id'] = $lineitem->net_weight_uom_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['cgst'] = $lineitem->cgst;
					$add_lineitem['sgst'] = $lineitem->sgst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('invoice_items',$add_lineitem);
				}				
			}
		} else {
			$post_data['invoice_data']['invoice_no'] = sprintf("%04d", $this->applib->get_new_invoice_no($post_data['invoice_data']['invoice_date']));
			$post_data['invoice_data']['invoice_no'] = $this->applib->get_invoice_no($post_data['invoice_data']['invoice_no'], $post_data['invoice_data']['invoice_date']);
			$dataToInsert = $post_data['invoice_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			// Unset quotation_id from Inquiry Data
			if (isset($dataToInsert['invoice_id']))
				unset($dataToInsert['invoice_id']);

			$result = $this->db->insert('invoices', $dataToInsert);
			$invoice_id = $this->db->insert_id();
			if($result){

				$login_data['invoice_id'] = $invoice_id;
				$login_data['created_by_id'] = $this->staff_id;
				$login_data['created_date_time'] = $this->now_time;
				$login_data['created_at'] = $this->now_time;
				$login_data['last_modified_by_id'] = $this->staff_id;
				$login_data['modified_date_time'] = $this->now_time;
				$login_data['updated_at'] = $this->now_time;
				$this->crud->insert('invoice_logins', $login_data);

				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
                    
                    /*-- Update Challan Status --*/
                    $this->crud->update('challans',array('is_invoice_created'=> 1),array('id'=>$lineitem->challan_id));
                    
					$add_lineitem = array();
					$add_lineitem['invoice_id'] = $invoice_id;
                    $add_lineitem['challan_id'] = $lineitem->challan_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					$add_lineitem['item_serial_no'] = $lineitem->item_serial_no;
					$add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
					if(isset($lineitem->item_description) && !empty($lineitem->item_description)){
                        $add_lineitem['item_description'] = $lineitem->item_description;
                    }
					if(isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)){
						$add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
					}
                    $add_lineitem['packing_mark'] = $lineitem->packing_mark;
                    $add_lineitem['no_of_packing'] = $lineitem->no_of_packing;
                    if(isset($lineitem->gross_weight) && !empty($lineitem->gross_weight)){
						$add_lineitem['gross_weight'] = $lineitem->gross_weight;
					}
                    if(isset($lineitem->gross_weight_uom_id) && !empty($lineitem->gross_weight_uom_id)){
						$add_lineitem['gross_weight_uom_id'] = $lineitem->gross_weight_uom_id;
					}
                    if(isset($lineitem->net_weight) && !empty($lineitem->net_weight)){
						$add_lineitem['net_weight'] = $lineitem->net_weight;
					}
                    if(isset($lineitem->net_weight_uom_id) && !empty($lineitem->net_weight_uom_id)){
						$add_lineitem['net_weight_uom_id'] = $lineitem->net_weight_uom_id;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					$add_lineitem['currency_id'] = $lineitem->currency_id;
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['cgst'] = $lineitem->cgst;
					$add_lineitem['sgst'] = $lineitem->sgst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('invoice_items',$add_lineitem);
				}
			}
		}
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['party']['party_id']);
            $this->applib->send_sms('invoices', $invoice_id, $party_phone_no, SEND_DISPATCH_CHALLAN_AND_INVOICE_SMS_ID);
        }
		print json_encode($return);
		exit;
	}

	function invoice_list($staff_id='',$from_date='',$to_date=''){
		$role_view = $this->app_model->have_access_role(INVOICE_MODULE_ID, "view");
		if ($role_view) {
			$data = array();
                        $users = $lead_owner = $this->crud->get_staff_dropdown();
                        $data['users'] = $users;
                        $data['staff_id']=$staff_id;
                        $data['from_date']=$from_date;
                        $data['to_date']=$to_date;
			set_page('dispatch/invoices/invoice_list', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function invoice_datatable(){
		//pre_die($_POST['party_type']);
		$select = 'i.*,c.sales_order_id,c.proforma_invoice_id,c.challan_no,';
		$select .= 'so.sales_order_no,pi.proforma_invoice_no, qso.quotation_no AS qso_quotation_no,qpi.quotation_no AS qpi_quotation_no,';
		$select .= 'p.party_name,p.party_type_1,sf.name AS sales_person,cur_s.name AS party_current_person';
		$config['table'] = 'invoices i';
		$config['select'] = $select;

		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = i.sales_to_party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'challans c', 'join_by' => 'c.id = i.challan_id', 'join_type' => 'left');
		
		$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id', 'join_type' => 'left');
		
		$config['joins'][] = array('join_table' => 'quotations qso', 'join_by' => 'qso.id = so.quotation_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotations qpi', 'join_by' => 'qpi.id = pi.quotation_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff sf', 'join_by' => 'sf.staff_id = pi.created_by', 'join_type' => 'left');
        $config['joins'][] = array('join_table'=> 'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		
		$config['column_order'] = array(null, 'i.invoice_no', 'c.challan_no', 'so.sales_order_no,pi.proforma_invoice_no', 'qso.quotation_no,qpi.quotation_no', 'p.party_name', 'i.invoice_date');
		$config['column_search'] = array('i.invoice_no', 'c.challan_no', 'so.sales_order_no','pi.proforma_invoice_no', 'qso.quotation_no','qpi.quotation_no', 'p.party_name', 'DATE_FORMAT(i.invoice_date,"%d-%m-%Y")');
		
        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
        } else if ($cu_accessExport == 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        } else if ($cu_accessDomestic == 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
        }
		
		if (!empty($_POST['party_type']) && $_POST['party_type'] == 'all') {
		} else if (isset($_POST['party_type']) && $_POST['party_type'] == 'export') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if (isset($_POST['party_type']) && $_POST['party_type'] == 'domestic') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}
        if (isset($_POST['from_date']) && !empty($_POST['from_date'])) {
            $from_date = date('Y-m-d', strtotime($_POST['from_date']));
            $config['wheres'][] = array('column_name' => 'i.invoice_date >=', 'column_value' => $from_date);
        }
        if (isset($_POST['to_date']) && !empty($_POST['to_date'])) {
            $to_date = date('Y-m-d', strtotime($_POST['to_date']));
            $config['wheres'][] = array('column_name' => 'i.invoice_date <=', 'column_value' => $to_date);
        }
        if (isset($_POST['user_id']) && $_POST['user_id'] != 'all') {
            $config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' => $_POST['user_id']);
        }

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
        $data = array();
		$role_delete = $this->app_model->have_access_role(INVOICE_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(INVOICE_MODULE_ID, "edit");

		foreach ($list as $invoice_row) {
			$row = array();
			$action = '';
			if ($role_edit) {
                if($invoice_row->party_type_1 == PARTY_TYPE_EXPORT_ID){
					$action .= '<a href="' . base_url('invoice/add_export/' . $invoice_row->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
				} elseif ($invoice_row->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
					$action .= '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
				}
			}
			if ($role_delete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs"
				   data-href="' . base_url('invoice/delete_invoice/' . $invoice_row->id) . '"><i class="fa fa-trash"></i></a> ';
			}
            if ($invoice_row->isApproved == 1 ) {
//                $action .= ' | <a href="' . base_url('invoice/invoice_print/' . $invoice_row->id) . '" target="_blank" class="btn-primary btn-xs" ><i class="fa fa-print"></i></a> ';
                $action .= ' | <a href="javascript:void(0);" class="btn-primary btn-xs with_without_letterpad" data-invoice_id="' . $invoice_row->id . '" ><i class="fa fa-print"></i></a> ';
                $action .= ' | <a href="' . base_url('invoice/invoice_email/' . $invoice_row->id) . '" class="email_button" target="_blank">Email</a>';
            }
			$row[] = $action;
            if($invoice_row->party_type_1 == PARTY_TYPE_EXPORT_ID){
                $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'.$invoice_row->invoice_no.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'.$invoice_row->challan_no.'</a>';
                if(!empty($invoice_row->sales_order_id)){
                    $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'.$invoice_row->sales_order_no.'</a>';
                    $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'.$invoice_row->qso_quotation_no.'</a>';
                } else if(!empty($invoice_row->proforma_invoice_id)){
                    $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'.$invoice_row->proforma_invoice_no.'</a>';
                    $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'.$invoice_row->qpi_quotation_no.'</a>';
                }
                $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'.$invoice_row->party_name.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->sales_person.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_export/' . $invoice_row->id . '?view') . '" >'. date('d-m-Y', strtotime($invoice_row->invoice_date)).'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->party_current_person.'</a>';
            } elseif ($invoice_row->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->invoice_no.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->challan_no.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->sales_order_no.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->qso_quotation_no.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->party_name.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->sales_person.'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.date('d-m-Y', strtotime($invoice_row->invoice_date)).'</a>';
                $row[] = '<a href="' . base_url('invoice/add_domestic/' . $invoice_row->id . '?view') . '" >'.$invoice_row->party_current_person.'</a>';
            }
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function delete_invoice($id){
		$role_delete = $this->app_model->have_access_role(INVOICE_MODULE_ID, "delete");
		if ($role_delete) {

            $invoice_items = $this->crud->get_row_by_id('invoice_items', array('invoice_id' => $id));
			foreach ($invoice_items as $invoice_item){
                $this->crud->update('challans',array('is_invoice_created'=> 0),array('id'=>$invoice_item->challan_id));
            }
			$where_array = array("id" => $id);
			$this->crud->delete("invoices", $where_array);
			$where_array = array("invoice_id" => $id);
			$this->crud->delete("invoice_items", $where_array);
			$this->crud->delete("invoice_logins", $where_array);

			$session_data = array(
				'success_message' => "Invoice has been deleted!"
			);
			$this->session->set_userdata($session_data);
			redirect(BASE_URL . "invoice/invoice_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function invoice_print($id, $with_without_letterpad = ''){
		$this->print_pdf($id, $with_without_letterpad, 'print');
	}

	function invoice_email($id, $with_without_letterpad = ''){
		$this->print_pdf($id, $with_without_letterpad, 'email');
	}

	function print_pdf($id, $with_without_letterpad = '', $type){
		$invoice_data = $this->get_invoice_detail($id);
//        echo '<pre>'; print_r($invoice_data); exit;
        $challan_data = $this->crud->get_row_by_id('challans', array('id' => $invoice_data->challan_id));
        $challan_data = $challan_data[0];
        
        $invoice_data->sales_order_id = $challan_data->sales_order_id;
        $invoice_data->proforma_invoice_id = $challan_data->proforma_invoice_id;
        if(!empty($challan_data->sales_order_id)){
            $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $challan_data->sales_order_id));
            $sales_order_data = $sales_order_data[0];
            $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = $this->crud->get_id_by_val('quotations', 'quotation_date', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = substr($sales_order_data->quotation_date, 8, 2) .'/'. substr($sales_order_data->quotation_date, 5, 2) .'/'. substr($sales_order_data->quotation_date, 0, 4);
            $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'/'. substr($sales_order_data->po_date, 5, 2) .'/'. substr($sales_order_data->po_date, 0, 4);
            $sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'/'. substr($sales_order_data->sales_order_date, 5, 2) .'/'. substr($sales_order_data->sales_order_date, 0, 4);
        } else if(!empty($challan_data->proforma_invoice_id)){
            $sales_order_data = $this->crud->get_row_by_id('proforma_invoices', array('id' => $challan_data->proforma_invoice_id));
            $sales_order_data = $sales_order_data[0];
            $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = $this->crud->get_id_by_val('quotations', 'quotation_date', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = substr($sales_order_data->quotation_date, 8, 2) .'/'. substr($sales_order_data->quotation_date, 5, 2) .'/'. substr($sales_order_data->quotation_date, 0, 4);
            $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'/'. substr($sales_order_data->po_date, 5, 2) .'/'. substr($sales_order_data->po_date, 0, 4);
            $sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'/'. substr($sales_order_data->sales_order_date, 5, 2) .'/'. substr($sales_order_data->sales_order_date, 0, 4);
        }
        
        $sales_order_data->challan_no = $challan_data->challan_no;
        $sales_order_data->challan_date = substr($challan_data->challan_date, 8, 2) .'/'. substr($challan_data->challan_date, 5, 2) .'/'. substr($challan_data->challan_date, 0, 4);

        $company_details = $this->applib->get_company_detail();
        $invoice_data->lr_date = (!empty(strtotime($invoice_data->lr_date))) ? substr($invoice_data->lr_date, 8, 2) .'/'. substr($invoice_data->lr_date, 5, 2) .'/'. substr($invoice_data->lr_date, 0, 4) : '';
        if(!empty($invoice_data->lc_no)){
            $invoice_data->lc_date = (!empty(strtotime($invoice_data->lc_date))) ? substr($invoice_data->lc_date, 8, 2) .'/'. substr($invoice_data->lc_date, 5, 2) .'/'. substr($invoice_data->lc_date, 0, 4) : '';
        } else {
            $invoice_data->lc_date = '';
        }
        $invoice_data->invoice_terms_and_conditions = $this->crud->get_id_by_val('terms_and_conditions','detail','module','sales_invoice_terms_and_conditions_for_domestic');
        $invoice_data->sales_invoice_declaration_for_export = $this->crud->get_id_by_val('terms_and_conditions','detail','module','sales_invoice_declaration_for_export');
		$invoice_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $invoice_data->delivery_through_id));
		$invoice_data->invoice_type = $this->crud->get_column_value_by_id('invoice_type', 'invoice_type', array('id' => $invoice_data->invoice_type_id));
        $invoice_data->port_of_discharge_country = $this->crud->get_id_by_val('country','country','country_id',$invoice_data->port_of_discharge_country_id);
        $invoice_data->port_of_loading_country = $this->crud->get_id_by_val('country','country','country_id',$invoice_data->port_of_loading_country_id);
        $invoice_data->place_of_delivery_country = $this->crud->get_id_by_val('country','country','country_id',$invoice_data->place_of_delivery_country_id);
        $invoice_data->origin_of_goods_country = $this->crud->get_id_by_val('country','country','country_id',$invoice_data->origin_of_goods_country_id);
        $invoice_data->final_destination_country = $this->crud->get_id_by_val('country','country','country_id',$invoice_data->final_destination_country_id);
        
        $invoice_items = $this->get_invoice_items($id);
        if (!empty($invoice_items)) {
			foreach ($invoice_items as $key => $item_row) {
                $invoice_items[$key]['hsn_no'] = $this->crud->get_column_value_by_id('items', 'hsn_code', array('id' => $item_row['item_id']));
                $invoice_items[$key]['item_extra_accessories'] = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $item_row['item_extra_accessories_id']));
                $invoice_items[$key]['challan_item_id'] = $this->crud->get_column_value_by_id('challan_items', 'id', array('challan_id' => $invoice_data->challan_id));
                $invoice_items[$key]['gross_weight_uom'] = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $item_row['gross_weight_uom_id']));
                $invoice_items[$key]['net_weight_uom'] = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $item_row['net_weight_uom_id']));
            }
        }
		if ($invoice_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
            $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
        } else {
            $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'export_letter_pad_content');
        }
        if($with_without_letterpad == 'wlp'){
            $html = $this->load->view('dispatch/invoices/invoice_pdf', array('sales_order_data' => $sales_order_data, 'invoice_data' => $invoice_data, 'company_details' => $company_details, 'invoice_items' => $invoice_items, 'letterpad_details' => $letterpad_details), true);
        } else {
            $html = $this->load->view('dispatch/invoices/invoice_pdf', array('sales_order_data' => $sales_order_data, 'invoice_data' => $invoice_data, 'company_details' => $company_details, 'invoice_items' => $invoice_items), true);
        }
		/*pre_die($html);*/
		$this->load->library('m_pdf');
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$margin_left = $company_details['margin_left'];
        $margin_right = $company_details['margin_right'];
        $margin_top = $company_details['margin_top'];
        $margin_bottom = $company_details['margin_bottom'];
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$pdf->WriteHTML($html);
		if($with_without_letterpad == 'wlp'){
            $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: '. $letterpad_details->header_logo_alignment .';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>');
            $pdf->SetHTMLFooter($letterpad_details->footer_detail);
//            $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
        }
        
        if($type == 'print'){
            $pdfFilePath = "invoice.pdf";
            $pdf->Output($pdfFilePath, "I");
        } elseif ($type == 'email'){
            $pdfFilePath = "./uploads/invoice_" . $id . ".pdf";
            $files_urls[] = $pdfFilePath;
            $pdf->Output($pdfFilePath, "F");
            $this->db->where('id', $id);
			$this->db->update('invoices', array('pdf_url' => serialize($files_urls)));
			redirect(base_url() . 'mail-system3/document-mail/invoice/' . $id);    
        }
	}

	function get_invoice_detail($id){
		$select = "sales.sales,currency.currency,quo.quotation_no,i.id as invoice_id,i.*,party.party_code,party.party_name,party.address,party.email_id as party_email_id,party.phone_no as p_phone_no,party.fax_no,party.pincode,party.party_type_1,party.gst_no as party_gst_no,city.city,state.state,country.country,party.utr_no AS party_cin_no,party.utr_no AS party_cin_no,";
		$select .= "party.gst_no as p_gst_no,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
		$select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required,";
		$select .= "i.bank_detail,company_banks.id,company_banks.detail AS banks_detail,sea_freight_type.name AS sea_freight_type,";
		$this->db->select($select);
		$this->db->from('invoices i');
		$this->db->join('quotations quo', 'quo.id = i.quotation_id', 'left');
		$this->db->join('sales', 'sales.id = i.sales_id', 'left');
		$this->db->join('party party', 'party.party_id = i.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.party_id = i.sales_to_party_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('currency', 'currency.id = i.currency_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = i.created_by', 'left');
		$this->db->join('sales_order_pref', 'sales_order_pref.id = i.sales_order_pref_id', 'left');
		$this->db->join('transportation_by', 'transportation_by.id = i.transportation_by_id', 'left');
		$this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = i.foundation_drawing_required_id', 'left');
		$this->db->join('loading_by', 'loading_by.id = i.loading_by_id', 'left');
		$this->db->join('inspection_required', 'inspection_required.id = i.inspection_required_id', 'left');
		$this->db->join('unloading_by', 'unloading_by.id = i.unloading_by_id', 'left');
		$this->db->join('erection_commissioning', 'erection_commissioning.id = i.erection_commissioning_id', 'left');
		$this->db->join('road_insurance_by', 'road_insurance_by.id = i.road_insurance_by_id', 'left');
		$this->db->join('for_required', 'for_required.id = i.for_required_id', 'left');
		$this->db->join('company_banks', 'company_banks.id = i.bank_detail', 'left');
		$this->db->join('sea_freight_type', 'sea_freight_type.id = i.sea_freight_type', 'left');
		$this->db->where('i.id', $id);
		$this->db->where('party.active !=', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_invoice_items($id){
		$this->db->select('soi.*');
		$this->db->from('invoice_items soi');
		$this->db->join('item_category ic', 'ic.id = soi.item_category_id');
		$this->db->where('ic.item_category', 'Finished Goods');
		$this->db->where('soi.invoice_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	function get_invoice_sub_items($id){
		$this->db->select('soi.*');
		$this->db->from('invoice_items soi');
		$this->db->join('item_category ic', 'ic.id = soi.item_category_id');
		$this->db->where('ic.item_category != ', 'Finished Goods');
		$this->db->where('soi.invoice_id', $id);
		$query = $this->db->get();
		$other_items = array();
		if ($query->num_rows() > 0) {
			$other_items = $query->result_array();
		}
		return $other_items;
	}
    
    function update_invoice_status_to_approved(){
		if ($_POST['invoice_id'] != '') {
			$invoice_id = $_POST['invoice_id'];
			$this->db->where('id', $invoice_id);
			$this->db->update('invoices', array('isApproved' => 1));
			$login_data['approved_by_id'] = $this->staff_id;
			$login_data['approved_date_time'] = $this->now_time;
			$login_data['updated_at'] = $this->now_time;
			$this->crud->update('invoice_logins', $login_data, array('invoice_id' => $invoice_id));
			echo json_encode(array('success' => true, 'message' => 'Invoice approved successfully!'));
			exit();
		} else {
			redirect("invoice/invoice_list"); exit;
			exit();
		}
	}

	function update_invoice_status_to_disapproved(){
		if ($_POST['invoice_id'] != '') {
			$invoice_id = $_POST['invoice_id'];
			$this->db->where('id', $invoice_id);
			$this->db->update('invoices', array('isApproved' => 0));
            $login_data['approved_by_id'] = null;
			$login_data['approved_date_time'] = null;
			$login_data['updated_at'] = $this->now_time;
			$this->crud->update('invoice_logins', $login_data, array('invoice_id' => $invoice_id));
			echo json_encode(array('success' => true, 'message' => 'Invoice disapproved successfully!'));
			exit();
		} else {
			redirect("invoice/invoice_list"); exit;
			exit();
		}
	}

}
