<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Letterpad_content extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
    }

    function index() {
        if ($this->applib->have_access_role(MASTER_TERMS_CONDITIONS_MENU_ID, "view")) {
            $data = array();
            $data['modules'] = $this->crud->get_all_records('letterpad_content', 'module_label', 'ASC');
            if (isset($_POST) && !empty($_POST)) {
                $data['module_data'] = $this->crud->get_data_row_by_id('letterpad_content', 'id', $_POST['modules']);
                $data['module_id'] = $_POST['modules'];
            } else {
                $data['module_data'] = $this->crud->get_data_row_by_id('letterpad_content', 'id', '1');
                $data['module_id'] = '1';
            }
            set_page('letterpad_content/new_letterpad_content', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function save_letterpad_content() {
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($_FILES); exit;
        $header_logo_alignment = $post_data['header_logo_alignment'];
        $footer_detail = $post_data['footer_detail'];
        $module_id = $post_data['module_id'];

        if (!empty($module_id)) {
            if (!empty($_FILES['header_logo']['name'])) {
                $header_logo = $this->applib->upload_image('header_logo', image_dir('letterpad_header_logo/'), false);
                if (isset($post_data['header_logo']['error'])) {
                    $header_logo = null;
                }
            } else if (!empty($post_data['header_logo_old'])) {
                $header_logo = $post_data['header_logo_old'];
            } else {
                $header_logo = null;
            }
            $this->db->where('id', $module_id);
            $this->db->update('letterpad_content', array('header_logo' => $header_logo, 'header_logo_alignment' => $header_logo_alignment, 'footer_detail' => $footer_detail, 'updated_by' => $this->staff_id, 'updated_at' => $this->now_time));
        }
        if ($module_id > 0) {
            $data['success'] = true;
            $data['module_id'] = $module_id;
            $data['message'] = "Letter pad Content Successfully Saved!";
            //$data['module_id'] = $this->db->insert_id();
            echo json_encode($data);
        } else {
            echo json_encode(array('success' => false, 'message' => "Something wrong!"));
        }
        exit();
    }

}
