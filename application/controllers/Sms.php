<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Payment
 * &@property Crud $crud
 */
class Sms extends CI_Controller
{	

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel','app_model'); 
		$this->load->model('Crud','crud');       
	}

	function add(){
		
        if($this->app_model->have_access_role(COMMON_PLACE_SMS_MENU_ID, "add")){
            set_page('sms/add');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }	 
	}
    
    function get_message(){
        $return = array();
		if(isset($_POST['sms_topic'])){ 
			$return = $this->crud->get_column_value_by_id('sms_topic', 'message', array('id' => $_POST['sms_topic']));
		}
		echo json_encode($return);
		exit;
    }
        
    function save_send_sms(){
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data['party_id']); exit;
        $sms_topic_id = $post_data['sms_topic'];
        if (isset($post_data['all_party'])){
            $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
            $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
            $isManagement = $this->app_model->have_access_role(PARTY_MODULE_ID, "is_management");
            $where = array();
            if ($isManagement == 1) {
                if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                    
                } elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
                    $where = '`party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
                } else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
                    $where = '`party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
                }
                $party_details = $this->crud->get_two_columns_val_by_where('party','party_id','phone_no',$where);
                $receiver_type = 'party';
                foreach ($party_details as $party_detail) {
                    $party_id = $party_detail['party_id'];
                    $party_phone_no = $party_detail['phone_no'];
                    $phone_no = $party_detail['phone_no'];
                    $party_phone = explode(",", $phone_no);
                    $party_phone_no = $party_phone[0];
                    $this->applib->common_place_to_send_sms('common_place', NULL, $party_phone_no, $sms_topic_id,$party_id,$receiver_type);
                }
            } else {
                $where = '( `created_by`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR `created_by` IS NULL )';
                if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                } else if ($cu_accessExport == 1) {
                    $where .= ' AND `party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
                } else if ($cu_accessDomestic == 1) {
                    $where .= ' AND `party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
                }
                $party_details = $this->crud->get_two_columns_val_by_where('party','party_id','phone_no',$where);
                $receiver_type = 'party';
                foreach ($party_details as $party_detail) {
                    $party_id = $party_detail['party_id'];
                    $phone_no = $party_detail['phone_no'];
                    $party_phone = explode(",", $phone_no);
                    $party_phone_no = $party_phone[0];
                    $this->applib->common_place_to_send_sms('common_place', NULL, $party_phone_no, $sms_topic_id,$party_id,$receiver_type);
                }
            }
        } else if (isset($post_data['party_id'])){
            $party_ids = $post_data['party_id'];
            $receiver_type = 'party';
            foreach ($party_ids as $party_id) {
                $phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $party_id);
                $party_phone = explode(",", $phone_no);
                $party_phone_no = $party_phone[0];
                $this->applib->common_place_to_send_sms('common_place', NULL, $party_phone_no, $sms_topic_id,$party_id,$receiver_type);
            }
        }
        if (isset($post_data['all_supplier'])){
            $where_array = array();
            $supplier_details = $this->crud->get_two_columns_val_by_where('supplier','supplier_id','contact_no',$where_array);
            $receiver_type = 'supplier';
            foreach ($supplier_details as $supplier_detail) {
                $supplier_id = $supplier_detail['supplier_id'];
                $phone_no = $supplier_detail['contact_no'];
                $supplier_phone = explode(",", $phone_no);
                $supplier_phone_no = $supplier_phone[0];
                $this->applib->common_place_to_send_sms('common_place', NULL, $supplier_phone_no, $sms_topic_id,$supplier_id,$receiver_type);
            }
        } else if (isset($post_data['supplier'])){
            $supplier_ids = $post_data['supplier'];
            $receiver_type = 'supplier';
            foreach ($supplier_ids as $supplier_id) {
                $phone_no = $this->crud->get_id_by_val('supplier', 'contact_no', 'supplier_id', $supplier_id);
                $supplier_phone = explode(",", $phone_no);
                $supplier_phone_no = $supplier_phone[0];
                $this->applib->common_place_to_send_sms('common_place', NULL, $supplier_phone_no, $sms_topic_id,$supplier_id,$receiver_type);
            }
        }
        if (isset($post_data['all_staff'])){
            $where_array = array();
            $staff_details = $this->crud->get_two_columns_val_by_where('staff','staff_id','company_contact',$where_array);
            $receiver_type = 'staff';
            foreach ($staff_details as $staff_detail) {
                $staff_id = $staff_detail['staff_id'];
                $phone_no = $staff_detail['company_contact'];
                $staff_phone = explode(",", $phone_no);
                $staff_phone_no = $staff_phone[0];
                $this->applib->common_place_to_send_sms('common_place', NULL, $staff_phone_no, $sms_topic_id,$staff_id,$receiver_type);
            }
        } else if (isset($post_data['staff'])){
            $staff_ids = $post_data['staff'];
            $receiver_type = 'staff';
            foreach ($staff_ids as $staff_id) {
                $phone_no = $this->crud->get_id_by_val('staff', 'company_contact', 'staff_id', $staff_id);
                $staff_phone = explode(",", $phone_no);
                $staff_phone_no = $staff_phone[0];
                $this->applib->common_place_to_send_sms('common_place', NULL, $staff_phone_no, $sms_topic_id,$staff_id,$receiver_type);
            }
        }
        $return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Sms Send Successfully');
        print json_encode($return);
		exit;    
    }
    
    function sent_sms_list(){
        $data = array();
        if($this->applib->have_access_role(COMMON_PLACE_SMS_MENU_ID,"view")) {
            $data['modules'] = $this->crud->get_column_data_array('sent_sms','module_name');
			set_page('sms/sent_sms_list', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
    }
    
    function sent_sms_list_datatable(){
		$config['table'] = 'sent_sms ss';
		$config['select'] = 'ss.id,ss.module_name,ss.send_to,ss.sms_topic_id,st.topic_name,ss.message,ss.created_by,stf.name,ss.created_at,ss.party_id,ss.supplier_id,ss.staff_id,pt.party_name,sup.supplier_name,stff.name as staff_name';
        $config['joins'][] = array('join_table' => 'sms_topic st', 'join_by' => 'st.id = ss.sms_topic_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff stf', 'join_by' => 'stf.staff_id = ss.created_by', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party pt', 'join_by' => 'pt.party_id = ss.party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'supplier sup', 'join_by' => 'sup.supplier_id = ss.supplier_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff stff', 'join_by' => 'stff.staff_id = ss.staff_id', 'join_type' => 'left');
        
		$config['column_order'] = array('ss.module_name','pt.party_name,sup.supplier_name,stff.name as staff_name','ss.send_to','st.topic_name','ss.message','stf.name','ss.created_at');
		$config['column_search'] = array('ss.module_name','ss.send_to','st.topic_name','ss.message','stf.name','DATE_FORMAT(ss.created_at,"%d-%m-%Y")');
		$config['order'] = array('ss.id' => 'desc');
        if (!empty($_POST['module_name']) && $_POST['module_name'] != 'all') {
            $config['wheres'][] = array('column_name'=> 'module_name','column_value' => $_POST['module_name']);
		}
		$config['group_by'] = 'ss.id';
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $sent_sms) {
			$row = array();
			$row[] = $sent_sms->module_name;
            if(isset($sent_sms->party_name) && !empty($sent_sms->party_name)){
                $row[] = 'Party : '. $sent_sms->party_name;
            } else if(isset($sent_sms->supplier_name) && !empty($sent_sms->supplier_name)){
                $row[] = 'Supllier : '.$sent_sms->supplier_name;
            } else if(isset($sent_sms->staff_name) && !empty($sent_sms->staff_name)){
                $row[] = 'Staff : '.$sent_sms->staff_name;
            } else {
                $row[] = '';
            }
			$row[] = $sent_sms->send_to;
			$row[] = $sent_sms->topic_name;
			$row[] = $sent_sms->message;
			$row[] = $sent_sms->name;
			$row[] = date('d-m-Y h:i',strtotime($sent_sms->created_at));
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

}