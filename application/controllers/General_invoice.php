<?php

ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class General_invoice extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    function add_parts($id = '') {
        $data = array();
        $data['rates_in'] = $this->crud->getFromSQL('SELECT `currency`.`id`,`currency`.`currency` FROM currency');

        if (isset($id) && !empty($id)) {
            if ($this->app_model->have_access_role(PARTS_MODULE_ID, "edit")) {
                $where_array['id'] = $id;
                $parts = $this->crud->get_all_with_where('parts', 'id', 'ASC', $where_array);
                $parts[0]->created_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $parts[0]->created_by));
                $parts[0]->created_at = substr($parts[0]->created_at, 8, 2) . '-' . substr($parts[0]->created_at, 5, 2) . '-' . substr($parts[0]->created_at, 0, 4);
                $parts[0]->updated_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $parts[0]->updated_by));
                $parts[0]->updated_at = substr($parts[0]->updated_at, 8, 2) . '-' . substr($parts[0]->updated_at, 5, 2) . '-' . substr($parts[0]->updated_at, 0, 4);
                $data['created_by'] = $parts[0]->created_by;
                $data['created_at'] = $parts[0]->created_at;
                $data['updated_by'] = $parts[0]->updated_by;
                $data['updated_at'] = $parts[0]->updated_at;
                $data['id'] = $parts[0]->id;
                $data['part_name'] = $parts[0]->part_name;
                $data['part_code'] = $parts[0]->part_code;
                $data['uom'] = $parts[0]->uom;
                $data['stock'] = $parts[0]->stock;
                $data['hsn_code'] = $parts[0]->hsn_code;
                $data['rates_in_val'] = json_decode($parts[0]->rate_in);
                $data['sgst'] = $parts[0]->sgst;
                $data['cgst'] = $parts[0]->cgst;
                $data['igst'] = $parts[0]->igst;
                $new_array = array();
                foreach ($data['rates_in_val'] as $key => $rate_in_val) {
                    $new_array[] = array(
                        'id' => $key,
                        'value' => $rate_in_val
                    );
                }
                $object = json_decode(json_encode($data['rates_in']), TRUE);
                $data['rates_in_val'] = array_replace_recursive($object, $new_array);
                set_page('service/general_invoice/add_parts', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            if ($this->applib->have_access_role(PARTS_MODULE_ID, "add")) {
                set_page('service/general_invoice/add_parts', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }

    function save_parts() {
        $post_data = $this->input->post();
        //print_r($post_data); exit;
        if (isset($post_data['id']) && !empty($post_data['id'])) {
            $post_data['updated_by'] = $this->staff_id;
            $post_data['updated_at'] = $this->now_time;
            $post_data['rate_in'] = json_encode($post_data['rate_in']);
            //print_r($post_data); exit;
            $result = $this->crud->update('parts', $post_data, array('id' => $post_data['id']));
            if ($result) {
                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Parts Updated Successfully');
            }
        } else {
            $post_data['created_by'] = $this->staff_id;
            $post_data['created_at'] = $this->now_time;
            $post_data['rate_in'] = json_encode($post_data['rate_in']);
            $result = $this->crud->insert('parts', $post_data);
            if ($result) {
                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Parts Added Successfully');
            }
        }
        echo json_encode($return);
        exit;
    }

    function parts_list() {
        if ($this->applib->have_access_role(PARTS_MODULE_ID, "view")) {
            set_page('service/general_invoice/parts_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function part_datatable() {
        $config['select'] = 'id, part_name, part_code';
        $config['table'] = 'parts';
        $config['column_order'] = array(null, 'part_name', 'part_code');
        $config['column_search'] = array('part_name', 'part_code');
        $config['order'] = array('id' => 'DESC');

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $isEdit = $this->app_model->have_access_role(PARTS_MODULE_ID, "edit");
        $isDelete = $this->app_model->have_access_role(PARTS_MODULE_ID, "delete");
        foreach ($list as $parts_list) {
            $row = array();
            $action = '';
            if ($isEdit) {
                $action .= '<a href="' . base_url('general_invoice/add_parts/' . $parts_list->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
            }
            if ($isDelete) {
                $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('general_invoice/delete/' . $parts_list->id) . '"><i class="fa fa-trash"></i></a>';
            }
            $row[] = $action;
            $row[] = $parts_list->part_name;
            $row[] = $parts_list->part_code;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function delete($id) {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
        $this->session->set_flashdata('success', true);
        $this->session->set_flashdata('message', 'Deleted Successfully');
    }
    
    function pick_receive_complain_datatable() {
        $config['table'] = 'complain com';
		$config['select'] = 'com.complain_id,com.complain_no,com.complain_no_year,com.complain_date,com.party_id,p.party_name,p.phone_no,p.email_id,city.city,state.state,country.country';
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = com.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

		$config['column_order'] = array(null, 'com.complain_no', 'com.complain_date', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('com.complain_no_year', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '1');
		$config['wheres'][] = array('column_name' => 'com.service_type', 'column_value' => PAID_SERVICE_TYPE_ID);
        $config['order'] = array('com.complain_id' => 'desc');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
		} elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
        $role_delete = $this->app_model->have_access_role(COMPLAIN_MENU_ID, "delete");
        $role_edit = $this->app_model->have_access_role(COMPLAIN_MENU_ID, "edit");
		foreach ($list as $complain) {
			$row = array();
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->complain_no_year . '</a>';
            $complain_date = (!empty(strtotime($complain->complain_date))) ? date("d-m-Y", strtotime($complain->complain_date)) : '';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain_date . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->party_name . '</a>';
            $phone_no = substr($complain->phone_no,0,10); 
            $complain->phone_no = str_replace(',', ', ', $complain->phone_no);
            $row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" data-toggle="tooltip" data-placement="bottom" title="'.$complain->phone_no.'" >' . $phone_no . '</a>';
            $email_id = substr($complain->email_id,0,10); 
            $complain->email_id = str_replace(',', ', ', $complain->email_id);
            $row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" data-toggle="tooltip" data-placement="bottom" title="'.$complain->email_id.'" >' . $email_id . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->city . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->state . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
    
    function get_complain(){
		$complain_id = $this->input->get_post("complain_id");
		$complain_data = $this->crud->get_only_complain_data($complain_id);
        $complain_data->complain_date = substr($complain_data->complain_date, 8, 2) .'-'. substr($complain_data->complain_date, 5, 2) .'-'. substr($complain_data->complain_date, 0, 4);
        
        $complain_item = array();
        if($complain_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
            $complain_item['currency_id'] = INR_CURRENCY_ID;
        } else if($complain_data->party_type_1 == PARTY_TYPE_EXPORT_ID){
            $complain_item['currency_id'] = USD_CURRENCY_ID;
        }
        $complain_item['complain_id'] = $complain_data->complain_id;
        $complain_item['complain_no_year'] = $complain_data->complain_no_year;
        $complain_item['quantity'] = '1';
        $complain_item['item_rate'] = $complain_data->service_charge;
        $complain_item['amount'] = $complain_data->service_charge;
        $complain_item['disc_per'] = 0;
        $complain_item['disc_value'] = 0;
        
        $taxable_value = $complain_item['amount'] - $complain_item['disc_value'];
        $complain_item['igst'] = 0;
        $complain_item['cgst'] = 0;
        $complain_item['sgst'] = 0;
        $complain_item['igst_amount'] = 0;
        $complain_item['cgst_amount'] = 0;
        $complain_item['sgst_amount'] = 0;
        $complain_item['net_amount'] = $taxable_value + $complain_item['igst_amount'] + $complain_item['cgst_amount'] + $complain_item['sgst_amount'];
        echo json_encode(array("complain_data" => $complain_data, "complain_item" => $complain_item));
	}
    
    function ajax_load_complain_details($complain_id = 0){
        if ($complain_id != 0) {
            $complain_data = $this->crud->get_only_complain_data($complain_id);
            $complain_item = array();
            $complain_item['complain_no'] = $complain_data->complain_no;
            $complain_item['quantity'] = '1';
            $complain_item['item_rate'] = $complain_data->service_charge;
            $complain_item['amount'] = $complain_data->service_charge;
            $complain_item['disc_per'] = 0;
            $complain_item['disc_value'] = 0;

            $taxable_value = $complain_item['amount'] - $complain_item['disc_value'];
            $complain_item['igst'] = 0;
            $complain_item['cgst'] = 0;
            $complain_item['sgst'] = 0;
            $complain_item['igst_amount'] = 0;
            $complain_item['cgst_amount'] = 0;
            $complain_item['sgst_amount'] = 0;
            $complain_item['net_amount'] = $taxable_value + $complain_item['igst_amount'] + $complain_item['cgst_amount'] + $complain_item['sgst_amount'];
            echo json_encode(array("complain_data" => $complain_data, "complain_item" => $complain_item));
		}
	}
    
    function pick_dispatch_party_datatable(){
        $select = 'c.id,p.party_id,p.party_name,p.phone_no,p.email_id,city.city,state.state,country.country';
		$config['table'] = 'challans c';
		$config['select'] = $select;
		$config['joins'][] =  array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id','join_type'=>'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
		$config['column_order'] = array('p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
        
        if($this->applib->have_access_current_user_rights(PARTS_MODULE_ID,"is_management")){
            if($cu_accessExport == 1 && $cu_accessDomestic == 1){
            }elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            }else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            }
        }else{
            $config['custom_where'] = '( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
            if($cu_accessExport == 1 && $cu_accessDomestic == 1){
            } else if($cu_accessExport == 1){
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if($cu_accessDomestic == 1){
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            } else {}
        }
        
        $config['order'] = array('c.id' => 'desc');
        $config['group_by'] = array('p.party_id');
        
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $dispatch_party_row) {
			$row = array();
			$row[] = '<a href="javascript:void(0);" class="dispatch_party_row" data-party_id="' . $dispatch_party_row->party_id . '" >' . $dispatch_party_row->party_name . '</a>';
            $phone_no = substr($dispatch_party_row->phone_no,0,10); 
            $dispatch_party_row->phone_no = str_replace(',', ', ', $dispatch_party_row->phone_no);
            $row[] = '<a href="javascript:void(0);" class="dispatch_party_row" data-party_id="' . $dispatch_party_row->party_id . '" data-toggle="tooltip" data-placement="bottom" title="'.$dispatch_party_row->phone_no.'" >' . $phone_no . '</a>';
            $email_id = substr($dispatch_party_row->email_id,0,14); 
            $dispatch_party_row->email_id = str_replace(',', ', ', $dispatch_party_row->email_id);
			$row[] = '<a href="javascript:void(0);" class="dispatch_party_row" data-party_id="' . $dispatch_party_row->party_id . '" data-toggle="tooltip" data-placement="bottom" title="'.$dispatch_party_row->email_id.'" >' . $email_id . '</a>';
			$row[] = '<a href="javascript:void(0);" class="dispatch_party_row" data-party_id="' . $dispatch_party_row->party_id . '" >' . $dispatch_party_row->city . '</a>';
			$row[] = '<a href="javascript:void(0);" class="dispatch_party_row" data-party_id="' . $dispatch_party_row->party_id . '" >' . $dispatch_party_row->state . '</a>';
			$row[] = '<a href="javascript:void(0);" class="dispatch_party_row" data-party_id="' . $dispatch_party_row->party_id . '" >' . $dispatch_party_row->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

    function add_invoice_from_complain($general_invoice_id = '') {
        $data = array();
		if(!empty($general_invoice_id)){
			if ($this->app_model->have_access_role(GENERAL_INVOICE_MODULE_ID, "edit")) {
				$general_invoice_data = $this->crud->get_row_by_id('general_invoice', array('general_invoice_id' => $general_invoice_id));
				if(empty($general_invoice_data)){
					redirect("general_invoice/general_invoice_list"); exit;
				}
				$general_invoice_data = $general_invoice_data[0];
                $general_invoice_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->created_by));
				$general_invoice_data->created_at = substr($general_invoice_data->created_at, 8, 2) .'-'. substr($general_invoice_data->created_at, 5, 2) .'-'. substr($general_invoice_data->created_at, 0, 4);
				$general_invoice_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->updated_by));
				$general_invoice_data->updated_at = substr($general_invoice_data->updated_at, 8, 2) .'-'. substr($general_invoice_data->updated_at, 5, 2) .'-'. substr($general_invoice_data->updated_at, 0, 4);
				$general_invoice_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->approved_by));
				$general_invoice_data->approved_at = substr($general_invoice_data->approved_at, 8, 2) .'-'. substr($general_invoice_data->approved_at, 5, 2) .'-'. substr($general_invoice_data->approved_at, 0, 4);
                $data['general_invoice_data'] = $general_invoice_data;
                
				$lineitems = array();
				$where = array('general_invoice_id' => $general_invoice_id);
				$general_invoice_lineitems = $this->crud->get_row_by_id('general_invoice_details', $where);
				foreach($general_invoice_lineitems as $general_invoice_lineitem){
                    $general_invoice_lineitem->lineitem_id = $general_invoice_lineitem->id;
                    $general_invoice_lineitem->complain_no_year = $this->crud->get_column_value_by_id('complain', 'complain_no_year', array('complain_id' => $general_invoice_lineitem->complain_id));
                    
					$general_invoice_lineitem->disc_per = !empty($general_invoice_lineitem->disc_per)? $general_invoice_lineitem->disc_per : '0';
					$general_invoice_lineitem->disc_value = !empty($general_invoice_lineitem->disc_value)? $general_invoice_lineitem->disc_value : '0';
					$general_invoice_lineitem->quantity = !empty($general_invoice_lineitem->quantity)? $general_invoice_lineitem->quantity : '0';
					$general_invoice_lineitem->item_rate = !empty($general_invoice_lineitem->item_rate)? $general_invoice_lineitem->item_rate : '0';

					$general_invoice_lineitem->igst = !empty($general_invoice_lineitem->igst)? $general_invoice_lineitem->igst : '0';
					$general_invoice_lineitem->cgst = !empty($general_invoice_lineitem->cgst)? $general_invoice_lineitem->cgst : '0';
					$general_invoice_lineitem->sgst = !empty($general_invoice_lineitem->sgst)? $general_invoice_lineitem->sgst : '0';
					$lineitems[] = json_encode($general_invoice_lineitem);
				}
				$data['general_invoice_lineitems'] = implode(',', $lineitems);
//				echo '<pre>';print_r($data); exit;
				set_page('service/general_invoice/add_invoice_from_complain', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID,"add")) {
				set_page('service/general_invoice/add_invoice_from_complain', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
    }

    function save_invoice_from_complain(){
		$post_data = $this->input->post();
//		echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
//        print_r($line_items_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
        $post_data['general_invoice_data']['general_invoice_date'] = !empty(strtotime($post_data['general_invoice_data']['general_invoice_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['general_invoice_date'])) : NULL;
        $post_data['general_invoice_data']['purchase_order_date'] = !empty(strtotime($post_data['general_invoice_data']['purchase_order_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['purchase_order_date'])) : NULL;
        $post_data['general_invoice_data']['sales_order_date'] = !empty(strtotime($post_data['general_invoice_data']['sales_order_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['sales_order_date'])) : NULL;
		$post_data['general_invoice_data']['delivery_through_id'] = !empty($post_data['general_invoice_data']['delivery_through_id']) ? $post_data['general_invoice_data']['delivery_through_id'] : NULL;
        $post_data['general_invoice_data']['lr_date'] = !empty(strtotime($post_data['general_invoice_data']['lr_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['lr_date'])) : NULL;
        $post_data['general_invoice_data']['created_from'] = '1';
        $post_data['general_invoice_data']['party_id'] = $post_data['party']['party_party_id'];

		if(isset($post_data['general_invoice_data']['general_invoice_id']) && !empty($post_data['general_invoice_data']['general_invoice_id'])){

			$general_invoice_id = $post_data['general_invoice_data']['general_invoice_id'];
			if (isset($post_data['general_invoice_data']['general_invoice_id']))
				unset($post_data['general_invoice_data']['general_invoice_id']);

			$this->db->where('general_invoice_id', $general_invoice_id);
			$result = $this->db->update('general_invoice', $post_data['general_invoice_data']);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','General Invoice Updated Successfully');
				
                if(isset($post_data['deleted_lineitem_id'])){
                    $this->db->where_in('id', $post_data['deleted_lineitem_id']);
                    $this->db->delete('general_invoice_details');
                }
                $val_inc = 0;
				foreach($line_items_data[0] as $lineitem){
                    $add_lineitem = array();
                    $add_lineitem['general_invoice_id'] = $general_invoice_id;
                    $add_lineitem['complain_id'] = $lineitem->complain_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    if(isset($lineitem->currency_id) && !empty($lineitem->currency_id)){
                        $add_lineitem['currency_id'] = $lineitem->currency_id;
                    }
                    $add_lineitem['item_rate'] = $lineitem->item_rate;
                    $add_lineitem['disc_per'] = $lineitem->disc_per;
                    $add_lineitem['disc_value'] = $lineitem->disc_value;
                    $add_lineitem['cgst'] = $lineitem->cgst;
                    $add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
                    $add_lineitem['sgst'] = $lineitem->sgst;
                    $add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['igst'] = $lineitem->igst;
                    $add_lineitem['igst_amount'] = $lineitem->igst_amount;
                    $add_lineitem['amount'] = $lineitem->amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    $add_lineitem['updated_by'] = $this->staff_id;
                    $add_lineitem['updated_at'] = $this->now_time;
                    if(isset($lineitem->lineitem_id) && !empty($lineitem->lineitem_id)){
                        $this->db->where('id', $lineitem->lineitem_id);
                        $this->db->update('general_invoice_details', $add_lineitem);
                    } else {
                        $add_lineitem['created_by'] = $this->staff_id;
                        $add_lineitem['created_at'] = $this->now_time;
                        $this->crud->insert('general_invoice_details',$add_lineitem);
                    }
				}
			}
		} else {
			$post_data['general_invoice_data']['general_invoice_no'] = sprintf("%04d", $this->applib->get_new_invoice_no($post_data['general_invoice_data']['general_invoice_date']));
			$post_data['general_invoice_data']['general_invoice_no'] = $this->applib->get_invoice_no($post_data['general_invoice_data']['general_invoice_no'], $post_data['general_invoice_data']['general_invoice_date']);
            $dataToInsert = $post_data['general_invoice_data'];
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_by'] = $this->staff_id;
			$dataToInsert['updated_at'] = $this->now_time;
			
            // Unset general_invoice_id from general_invoice_data
			if (isset($dataToInsert['general_invoice_id']))
				unset($dataToInsert['general_invoice_id']);

			$result = $this->db->insert('general_invoice', $dataToInsert);
			$general_invoice_id = $this->db->insert_id();
			if($result){
                $return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','General Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
                    $add_lineitem = array();
					$add_lineitem['general_invoice_id'] = $general_invoice_id;
					$add_lineitem['complain_id'] = $lineitem->complain_id;
					$add_lineitem['quantity'] = $lineitem->quantity;
					if(isset($lineitem->currency_id) && !empty($lineitem->currency_id)){
						$add_lineitem['currency_id'] = $lineitem->currency_id;
					}
					$add_lineitem['item_rate'] = $lineitem->item_rate;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['cgst'] = $lineitem->cgst;
                    $add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst'] = $lineitem->sgst;
                    $add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_by'] = $this->staff_id;
					$add_lineitem['created_at'] = $this->now_time;
					$add_lineitem['updated_by'] = $this->staff_id;
					$add_lineitem['updated_at'] = $this->now_time;
					$this->crud->insert('general_invoice_details',$add_lineitem);
				}
			}
		}
		print json_encode($return);
		exit;
	}
    
    function general_invoice_list() {
        if ($this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID, "view")) {
            set_page('service/general_invoice/general_invoice_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function general_invoice_datatable() {
    	//pre_die($_POST['party_type']);
		$select = 'gi.*, p.party_id,p.party_name,p.phone_no,p.email_id,city.city,state.state,country.country';
		$config['table'] = 'general_invoice gi';
		$config['select'] = $select;
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = gi.party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
        $config['column_order'] = array(null, 'general_invoice_no', 'general_invoice_date', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('general_invoice_no', 'general_invoice_date', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		
        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
        } else if ($cu_accessExport == 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        } else if ($cu_accessDomestic == 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
        }
		
		if (!empty($_POST['created_from']) && $_POST['created_from'] == 'all') {
		} else if (isset($_POST['created_from']) && $_POST['created_from'] == '1') {
			$config['wheres'][] = array('column_name' => 'gi.created_from', 'column_value' => 1);
		} else if (isset($_POST['created_from']) && $_POST['created_from'] == '2') {
			$config['wheres'][] = array('column_name' => 'gi.created_from', 'column_value' => 2);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
        $data = array();
		$role_delete = $this->app_model->have_access_role(GENERAL_INVOICE_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(GENERAL_INVOICE_MODULE_ID, "edit");

		foreach ($list as $invoice_row) {
			$row = array();
            $add_form_name = '';
            if($invoice_row->created_from == 1){
                $add_form_name = 'add_invoice_from_complain';
            } else if($invoice_row->created_from == 2){
                $add_form_name = 'add_invoice_from_party';
            } else if($invoice_row->created_from == 3){
                $add_form_name = 'add_invoice_from_party_pitem';
            }
			$action = '';
			if ($role_edit) {
                $action .= '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
			}
			if ($role_delete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs"
				   data-href="' . base_url('general_invoice/delete_general_invoice/' . $invoice_row->general_invoice_id) . '"><i class="fa fa-trash"></i></a> ';
			}
            if(!empty($invoice_row->is_approved)){
                $action .= ' | <a href="javascript:void(0);" class="btn-primary btn-xs with_without_letterpad" data-invoice_id="' . $invoice_row->general_invoice_id . '" ><i class="fa fa-print"></i></a> ';
                //$action .= ' | <a href="' . base_url('general_invoice/invoice_print/' . $invoice_row->general_invoice_id) . '" class="btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>';
                $action .= ' | <a href="' . base_url('general_invoice/invoice_email/' . $invoice_row->general_invoice_id) . '" class="email_button" target="_blank">Email</a>';
            }
			$row[] = $action;
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" >'.$invoice_row->general_invoice_no.'</a>';
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" >'. date('d-m-Y', strtotime($invoice_row->general_invoice_date)).'</a>';
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" >'.$invoice_row->party_name.'</a>';
            $phone_no = substr($invoice_row->phone_no,0,10);
            $invoice_row->phone_no = str_replace(',', ', ', $invoice_row->phone_no);
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$invoice_row->phone_no.'" >'.$phone_no.'</a>';
            $email_id = substr($invoice_row->email_id,0,14);
            $invoice_row->email_id = str_replace(',', ', ', $invoice_row->email_id);
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$invoice_row->email_id.'" >'.$email_id.'</a>';
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" >'.$invoice_row->city.'</a>';
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" >'.$invoice_row->state.'</a>';
            $row[] = '<a href="' . base_url('general_invoice/'.$add_form_name.'/' . $invoice_row->general_invoice_id . '?view') . '" >'.$invoice_row->country.'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
    
    function delete_general_invoice($general_invoice_id){
		$role_delete = $this->app_model->have_access_role(GENERAL_INVOICE_MODULE_ID, "delete");
		if ($role_delete) {
            
            /* Update Purchase Item Stock by General Invoice ID */
            $created_from = $this->crud->get_id_by_val('general_invoice','created_from','general_invoice_id',$general_invoice_id);
            if($created_from == 3){
                $this->applib->update_purchase_stock_by_general_invoice_id($general_invoice_id, '+');
            }
            
            $where_array = array("general_invoice_id" => $general_invoice_id);
			$this->crud->delete("general_invoice_details", $where_array);
            $this->crud->delete("general_invoice", $where_array);

			$session_data = array( 'success_message' => "General Invoice has been deleted!");
			$this->session->set_userdata($session_data);
			redirect(BASE_URL . "general_invoice/general_invoice_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function update_general_invoice_status_to_approved(){
		if ($_POST['general_invoice_id'] != '') {
            $general_invoice_id = $_POST['general_invoice_id'];
			$data = array();
            $data['is_approved'] = 1;
            $data['approved_by'] = $this->staff_id;
			$data['approved_at'] = $this->now_time;
            $this->db->where('general_invoice_id', $general_invoice_id);
			$this->db->update('general_invoice', $data);
			echo json_encode(array('success' => true, 'message' => 'General Invoice approved successfully!'));
			exit();
		} else {
			redirect("general_invoice/general_invoice_list"); exit;
			exit();
		}
	}

	function update_general_invoice_status_to_disapproved(){
		if ($_POST['general_invoice_id'] != '') {
            $general_invoice_id = $_POST['general_invoice_id'];
            $data = array();
            $data['is_approved'] = 0;
            $data['approved_by'] = null;
			$data['approved_at'] = null;
			$this->db->where('general_invoice_id', $general_invoice_id);
			$this->db->update('general_invoice', $data);
			echo json_encode(array('success' => true, 'message' => 'General Invoice disapproved successfully!'));
			exit();
		} else {
			redirect("general_invoice/general_invoice_list"); exit;
			exit();
		}
	}
    
    function add_invoice_from_party($general_invoice_id = '') {
        $data = array(
            'company_banks' => $this->crud->get_all_records('company_banks', 'id', 'asc'),
        );
        if(!empty($general_invoice_id)){
			if ($this->app_model->have_access_role(GENERAL_INVOICE_MODULE_ID, "edit")) {
				$general_invoice_data = $this->crud->get_row_by_id('general_invoice', array('general_invoice_id' => $general_invoice_id));
				if(empty($general_invoice_data)){
					redirect("general_invoice/general_invoice_list"); exit;
				}
				$general_invoice_data = $general_invoice_data[0];
                $general_invoice_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->created_by));
				$general_invoice_data->created_at = substr($general_invoice_data->created_at, 8, 2) .'-'. substr($general_invoice_data->created_at, 5, 2) .'-'. substr($general_invoice_data->created_at, 0, 4);
				$general_invoice_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->updated_by));
				$general_invoice_data->updated_at = substr($general_invoice_data->updated_at, 8, 2) .'-'. substr($general_invoice_data->updated_at, 5, 2) .'-'. substr($general_invoice_data->updated_at, 0, 4);
				$general_invoice_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->approved_by));
				$general_invoice_data->approved_at = substr($general_invoice_data->approved_at, 8, 2) .'-'. substr($general_invoice_data->approved_at, 5, 2) .'-'. substr($general_invoice_data->approved_at, 0, 4);
                $data['general_invoice_data'] = $general_invoice_data;
                
				$lineitems = array();
				$where = array('general_invoice_id' => $general_invoice_id);
				$general_invoice_lineitems = $this->crud->get_row_by_id('general_invoice_details', $where);
				foreach($general_invoice_lineitems as $general_invoice_lineitem){
                    $general_invoice_lineitem->lineitem_id = $general_invoice_lineitem->id;
                    $general_invoice_lineitem->part_name = $this->crud->get_column_value_by_id('parts', 'part_name', array('id' => $general_invoice_lineitem->part_id));
                    $general_invoice_lineitem->part_code = $this->crud->get_column_value_by_id('parts', 'part_code', array('id' => $general_invoice_lineitem->part_id));
                    $general_invoice_lineitem->uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $general_invoice_lineitem->uom_id));
                    
					$general_invoice_lineitem->disc_per = !empty($general_invoice_lineitem->disc_per)? $general_invoice_lineitem->disc_per : '0';
					$general_invoice_lineitem->disc_value = !empty($general_invoice_lineitem->disc_value)? $general_invoice_lineitem->disc_value : '0';
					$general_invoice_lineitem->quantity = !empty($general_invoice_lineitem->quantity)? $general_invoice_lineitem->quantity : '0';
					$general_invoice_lineitem->item_rate = !empty($general_invoice_lineitem->item_rate)? $general_invoice_lineitem->item_rate : '0';

					$general_invoice_lineitem->igst = !empty($general_invoice_lineitem->igst)? $general_invoice_lineitem->igst : '0';
					$general_invoice_lineitem->cgst = !empty($general_invoice_lineitem->cgst)? $general_invoice_lineitem->cgst : '0';
					$general_invoice_lineitem->sgst = !empty($general_invoice_lineitem->sgst)? $general_invoice_lineitem->sgst : '0';
					$lineitems[] = json_encode($general_invoice_lineitem);
				}
				$data['general_invoice_lineitems'] = implode(',', $lineitems);
//				echo '<pre>';print_r($data); exit;
				set_page('service/general_invoice/add_invoice_from_party', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID,"add")) {
				set_page('service/general_invoice/add_invoice_from_party', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
    }
    
    function save_invoice_from_party(){
        $return = array();
		$post_data = $this->input->post();
//		echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
//        print_r($line_items_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
        $post_data['general_invoice_data']['general_invoice_date'] = !empty(strtotime($post_data['general_invoice_data']['general_invoice_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['general_invoice_date'])) : NULL;
        $post_data['general_invoice_data']['purchase_order_date'] = !empty(strtotime($post_data['general_invoice_data']['purchase_order_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['purchase_order_date'])) : NULL;
        $post_data['general_invoice_data']['sales_order_date'] = !empty(strtotime($post_data['general_invoice_data']['sales_order_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['sales_order_date'])) : NULL;
		$post_data['general_invoice_data']['delivery_through_id'] = !empty($post_data['general_invoice_data']['delivery_through_id']) ? $post_data['general_invoice_data']['delivery_through_id'] : NULL;
		$post_data['general_invoice_data']['transport'] = !empty($post_data['general_invoice_data']['transport']) ? $post_data['general_invoice_data']['transport'] : NULL;
		$post_data['general_invoice_data']['insurance'] = !empty($post_data['general_invoice_data']['insurance']) ? $post_data['general_invoice_data']['insurance'] : NULL;
		$post_data['general_invoice_data']['p_and_f'] = !empty($post_data['general_invoice_data']['p_and_f']) ? $post_data['general_invoice_data']['p_and_f'] : NULL;
        $post_data['general_invoice_data']['lr_date'] = !empty(strtotime($post_data['general_invoice_data']['lr_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['lr_date'])) : NULL;
        $post_data['general_invoice_data']['lc_date'] = (isset($post_data['general_invoice_data']['lc_date']) && !empty(strtotime($post_data['general_invoice_data']['lc_date']))) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['lc_date'])) : NULL;
        $post_data['general_invoice_data']['created_from'] = '2';
        $post_data['general_invoice_data']['party_id'] = $post_data['party']['party_party_id'];

		if(isset($post_data['general_invoice_data']['general_invoice_id']) && !empty($post_data['general_invoice_data']['general_invoice_id'])){

			$general_invoice_id = $post_data['general_invoice_data']['general_invoice_id'];
			if (isset($post_data['general_invoice_data']['general_invoice_id']))
				unset($post_data['general_invoice_data']['general_invoice_id']);

			$this->db->where('general_invoice_id', $general_invoice_id);
			$result = $this->db->update('general_invoice', $post_data['general_invoice_data']);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','General Invoice Updated Successfully');
				
                if(isset($post_data['deleted_lineitem_id'])){
                    $this->db->where_in('id', $post_data['deleted_lineitem_id']);
                    $this->db->delete('general_invoice_details');
                }
                $val_inc = 0;
				foreach($line_items_data[0] as $lineitem){
                    $add_lineitem = array();
                    $add_lineitem['general_invoice_id'] = $general_invoice_id;
                    $add_lineitem['part_id'] = $lineitem->part_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    if(isset($lineitem->uom_id) && !empty($lineitem->uom_id)){
                        $add_lineitem['uom_id'] = $lineitem->uom_id;
                    }
                    if(isset($lineitem->currency_id) && !empty($lineitem->currency_id)){
                        $add_lineitem['currency_id'] = $lineitem->currency_id;
                    }
                    $add_lineitem['item_rate'] = $lineitem->item_rate;
                    $add_lineitem['disc_per'] = $lineitem->disc_per;
                    $add_lineitem['disc_value'] = $lineitem->disc_value;
                    $add_lineitem['cgst'] = $lineitem->cgst;
                    $add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
                    $add_lineitem['sgst'] = $lineitem->sgst;
                    $add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['igst'] = $lineitem->igst;
                    $add_lineitem['igst_amount'] = $lineitem->igst_amount;
                    $add_lineitem['amount'] = $lineitem->amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    $add_lineitem['updated_by'] = $this->staff_id;
                    $add_lineitem['updated_at'] = $this->now_time;
                    if(isset($lineitem->lineitem_id) && !empty($lineitem->lineitem_id)){
                        $this->db->where('id', $lineitem->lineitem_id);
                        $this->db->update('general_invoice_details', $add_lineitem);
                    } else {
                        $add_lineitem['created_by'] = $this->staff_id;
                        $add_lineitem['created_at'] = $this->now_time;
                        $this->crud->insert('general_invoice_details',$add_lineitem);
                    }
				}
			}
		} else {
			$post_data['general_invoice_data']['general_invoice_no'] = sprintf("%04d", $this->applib->get_new_invoice_no($post_data['general_invoice_data']['general_invoice_date']));
			$post_data['general_invoice_data']['general_invoice_no'] = $this->applib->get_invoice_no($post_data['general_invoice_data']['general_invoice_no'], $post_data['general_invoice_data']['general_invoice_date']);
            $dataToInsert = $post_data['general_invoice_data'];
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_by'] = $this->staff_id;
			$dataToInsert['updated_at'] = $this->now_time;
			
            // Unset general_invoice_id from general_invoice_data
			if (isset($dataToInsert['general_invoice_id']))
				unset($dataToInsert['general_invoice_id']);

			$result = $this->db->insert('general_invoice', $dataToInsert);
//            echo $this->db->last_query(); exit;
			$general_invoice_id = $this->db->insert_id();
			if($result){
                $return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','General Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
                    $add_lineitem = array();
					$add_lineitem['general_invoice_id'] = $general_invoice_id;
					$add_lineitem['part_id'] = $lineitem->part_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    if(isset($lineitem->uom_id) && !empty($lineitem->uom_id)){
                        $add_lineitem['uom_id'] = $lineitem->uom_id;
                    }
					if(isset($lineitem->currency_id) && !empty($lineitem->currency_id)){
						$add_lineitem['currency_id'] = $lineitem->currency_id;
					}
					$add_lineitem['item_rate'] = $lineitem->item_rate;
					$add_lineitem['disc_per'] = $lineitem->disc_per;
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['cgst'] = $lineitem->cgst;
                    $add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					$add_lineitem['sgst'] = $lineitem->sgst;
                    $add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['igst'] = $lineitem->igst;
					$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_by'] = $this->staff_id;
					$add_lineitem['created_at'] = $this->now_time;
					$add_lineitem['updated_by'] = $this->staff_id;
					$add_lineitem['updated_at'] = $this->now_time;
					$this->crud->insert('general_invoice_details',$add_lineitem);
				}
			}
		}
		print json_encode($return);
		exit;
	}

    function add_invoice_from_party_pitem($general_invoice_id = '') {
        $data = array(
            'company_banks' => $this->crud->get_all_records('company_banks', 'id', 'asc'),
        );
        if(!empty($general_invoice_id)){
			if ($this->app_model->have_access_role(GENERAL_INVOICE_MODULE_ID, "edit")) {
				$general_invoice_data = $this->crud->get_row_by_id('general_invoice', array('general_invoice_id' => $general_invoice_id));
				if(empty($general_invoice_data)){
					redirect("general_invoice/general_invoice_list"); exit;
				}
				$general_invoice_data = $general_invoice_data[0];
                $general_invoice_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->created_by));
				$general_invoice_data->created_at = substr($general_invoice_data->created_at, 8, 2) .'-'. substr($general_invoice_data->created_at, 5, 2) .'-'. substr($general_invoice_data->created_at, 0, 4);
				$general_invoice_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->updated_by));
				$general_invoice_data->updated_at = substr($general_invoice_data->updated_at, 8, 2) .'-'. substr($general_invoice_data->updated_at, 5, 2) .'-'. substr($general_invoice_data->updated_at, 0, 4);
				$general_invoice_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $general_invoice_data->approved_by));
				$general_invoice_data->approved_at = substr($general_invoice_data->approved_at, 8, 2) .'-'. substr($general_invoice_data->approved_at, 5, 2) .'-'. substr($general_invoice_data->approved_at, 0, 4);
                $data['general_invoice_data'] = $general_invoice_data;
                
				$lineitems = array();
				$where = array('general_invoice_id' => $general_invoice_id);
				$general_invoice_lineitems = $this->crud->get_row_by_id('general_invoice_details', $where);
				foreach($general_invoice_lineitems as $general_invoice_lineitem){
                    $general_invoice_lineitem->lineitem_id = $general_invoice_lineitem->id;
                    $general_invoice_lineitem->item_name = $this->crud->get_column_value_by_id('purchase_items', 'item_name', array('id' => $general_invoice_lineitem->purchase_item_id));
                    $general_invoice_lineitem->item_code = $this->crud->get_column_value_by_id('purchase_items', 'item_code', array('id' => $general_invoice_lineitem->purchase_item_id));
                    $general_invoice_lineitem->uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $general_invoice_lineitem->uom_id));
                    
					$general_invoice_lineitem->disc_per = !empty($general_invoice_lineitem->disc_per)? $general_invoice_lineitem->disc_per : '0';
					$general_invoice_lineitem->disc_value = !empty($general_invoice_lineitem->disc_value)? $general_invoice_lineitem->disc_value : '0';
					$general_invoice_lineitem->quantity = !empty($general_invoice_lineitem->quantity)? $general_invoice_lineitem->quantity : '0';
					$general_invoice_lineitem->item_rate = !empty($general_invoice_lineitem->item_rate)? $general_invoice_lineitem->item_rate : '0';

					$general_invoice_lineitem->igst = !empty($general_invoice_lineitem->igst)? $general_invoice_lineitem->igst : '0';
					$general_invoice_lineitem->cgst = !empty($general_invoice_lineitem->cgst)? $general_invoice_lineitem->cgst : '0';
					$general_invoice_lineitem->sgst = !empty($general_invoice_lineitem->sgst)? $general_invoice_lineitem->sgst : '0';
					$lineitems[] = json_encode($general_invoice_lineitem);
				}
				$data['general_invoice_lineitems'] = implode(',', $lineitems);
//				echo '<pre>';print_r($data); exit;
				set_page('service/general_invoice/add_invoice_from_party_pitem', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID,"add")) {
				set_page('service/general_invoice/add_invoice_from_party_pitem', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
    }
    
    function save_invoice_from_party_pitem(){
        $return = array();
		$post_data = $this->input->post();
//		echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
//        print_r($line_items_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
        $post_data['general_invoice_data']['general_invoice_date'] = !empty(strtotime($post_data['general_invoice_data']['general_invoice_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['general_invoice_date'])) : NULL;
        $post_data['general_invoice_data']['purchase_order_date'] = !empty(strtotime($post_data['general_invoice_data']['purchase_order_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['purchase_order_date'])) : NULL;
        $post_data['general_invoice_data']['sales_order_date'] = !empty(strtotime($post_data['general_invoice_data']['sales_order_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['sales_order_date'])) : NULL;
		$post_data['general_invoice_data']['delivery_through_id'] = !empty($post_data['general_invoice_data']['delivery_through_id']) ? $post_data['general_invoice_data']['delivery_through_id'] : NULL;
        $post_data['general_invoice_data']['lr_date'] = !empty(strtotime($post_data['general_invoice_data']['lr_date'])) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['lr_date'])) : NULL;
        $post_data['general_invoice_data']['lc_date'] = (isset($post_data['general_invoice_data']['lc_date']) && !empty(strtotime($post_data['general_invoice_data']['lc_date']))) ? date("Y-m-d", strtotime($post_data['general_invoice_data']['lc_date'])) : NULL;
        $post_data['general_invoice_data']['created_from'] = '3';
        $post_data['general_invoice_data']['party_id'] = $post_data['party']['party_party_id'];
        $post_data['general_invoice_data']['transport'] = !empty($post_data['general_invoice_data']['transport']) ? $post_data['general_invoice_data']['transport'] : NULL;
        $post_data['general_invoice_data']['p_and_f'] = !empty($post_data['general_invoice_data']['p_and_f']) ? $post_data['general_invoice_data']['p_and_f'] : NULL;
        $post_data['general_invoice_data']['insurance'] = !empty($post_data['general_invoice_data']['insurance']) ? $post_data['general_invoice_data']['insurance'] : NULL;

		if(isset($post_data['general_invoice_data']['general_invoice_id']) && !empty($post_data['general_invoice_data']['general_invoice_id'])){

			$general_invoice_id = $post_data['general_invoice_data']['general_invoice_id'];
			if (isset($post_data['general_invoice_data']['general_invoice_id']))
				unset($post_data['general_invoice_data']['general_invoice_id']);

			$this->db->where('general_invoice_id', $general_invoice_id);
			$result = $this->db->update('general_invoice', $post_data['general_invoice_data']);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','General Invoice Updated Successfully');
				
                if(isset($post_data['deleted_lineitem_id'])){
                    $general_invoice_details = $this->crud->get_row_by_where_in_ids('general_invoice_details', 'id', $post_data['deleted_lineitem_id']);
                    if(!empty($general_invoice_details)){
                        foreach ($general_invoice_details as $general_invoice_detail){
                            $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $general_invoice_detail->purchase_item_id));
                            if(!empty($purchase_item)){
                                $purchase_item = $purchase_item[0];
                                $this->applib->purchase_item_fifo_from_delete_general_invoice($general_invoice_id, $general_invoice_detail->purchase_item_id, $general_invoice_detail->quantity, $purchase_item->uom, $purchase_item->reference_qty);
                            }
                        }
                    }
                    $this->db->where_in('id', $post_data['deleted_lineitem_id']);
                    $this->db->delete('general_invoice_details');
                }
                $val_inc = 0;
				foreach($line_items_data[0] as $lineitem){
                    $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $lineitem->purchase_item_id));
                    
                    $add_lineitem = array();
                    $add_lineitem['general_invoice_id'] = $general_invoice_id;
                    $add_lineitem['purchase_item_id'] = $lineitem->purchase_item_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    if(isset($lineitem->uom_id) && !empty($lineitem->uom_id)){
                        $add_lineitem['uom_id'] = $lineitem->uom_id;
                    }
                    if(isset($lineitem->currency_id) && !empty($lineitem->currency_id)){
                        $add_lineitem['currency_id'] = $lineitem->currency_id;
                    }
                    $add_lineitem['item_rate'] = $lineitem->item_rate;
                    if(isset($lineitem->disc_per) && !empty($lineitem->disc_per)){
						$add_lineitem['disc_per'] = $lineitem->disc_per;
					}
                    if(isset($lineitem->disc_value) && !empty($lineitem->disc_value)){
						$add_lineitem['disc_value'] = $lineitem->disc_value;
					}
                    if(isset($lineitem->cgst) && !empty($lineitem->cgst)){
						$add_lineitem['cgst'] = $lineitem->cgst;
						$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					}
                    if(isset($lineitem->sgst) && !empty($lineitem->sgst)){
						$add_lineitem['sgst'] = $lineitem->sgst;
						$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
					}
                    if(isset($lineitem->igst) && !empty($lineitem->igst)){
						$add_lineitem['igst'] = $lineitem->igst;
						$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					}
                    $add_lineitem['amount'] = $lineitem->amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    $add_lineitem['updated_by'] = $this->staff_id;
                    $add_lineitem['updated_at'] = $this->now_time;
                    if(isset($lineitem->lineitem_id) && !empty($lineitem->lineitem_id)){
                        
                        if(!empty($purchase_item)){
                            $purchase_item = $purchase_item[0];
                            
                            /* Update stock in Purchase Item Master */
                            $quantity = $this->crud->get_id_by_val('general_invoice_details', 'quantity', 'id', $lineitem->lineitem_id);
                            $current_item_stock = $purchase_item->current_item_stock + $quantity - $lineitem->quantity;
                            $effective_stock = $purchase_item->effective_stock + $quantity - $lineitem->quantity;
                            $this->crud->update('purchase_items', array('current_item_stock' => $current_item_stock, 'effective_stock' => $effective_stock), array('id' => $lineitem->purchase_item_id));
                            $updated_quantity = $lineitem->quantity - $quantity;
                            $this->applib->purchase_item_fifo_from_edit_general_invoice($general_invoice_id, $lineitem->purchase_item_id, $updated_quantity, $purchase_item->uom, $purchase_item->reference_qty);
                        }
                        
                        $this->db->where('id', $lineitem->lineitem_id);
                        $this->db->update('general_invoice_details', $add_lineitem);
                    } else {
                        
                        if(!empty($purchase_item)){
                            $purchase_item = $purchase_item[0];
                            
                            /* Update stock in Purchase Item Master */
                            $current_item_stock = $purchase_item->current_item_stock - $lineitem->quantity;
                            $effective_stock = $purchase_item->effective_stock - $lineitem->quantity;
                            $this->crud->update('purchase_items', array('current_item_stock' => $current_item_stock, 'effective_stock' => $effective_stock), array('id' => $lineitem->purchase_item_id));
                            
                            $this->applib->purchase_item_fifo_from_add_general_invoice($general_invoice_id, $lineitem->purchase_item_id, $lineitem->quantity, $purchase_item->uom, $purchase_item->reference_qty);
                        }
                        $add_lineitem['created_by'] = $this->staff_id;
                        $add_lineitem['created_at'] = $this->now_time;
                        $this->crud->insert('general_invoice_details',$add_lineitem);
                    }
				}
			}
		} else {
			$post_data['general_invoice_data']['general_invoice_no'] = sprintf("%04d", $this->applib->get_new_invoice_no($post_data['general_invoice_data']['general_invoice_date']));
			$post_data['general_invoice_data']['general_invoice_no'] = $this->applib->get_invoice_no($post_data['general_invoice_data']['general_invoice_no'], $post_data['general_invoice_data']['general_invoice_date']);
            $dataToInsert = $post_data['general_invoice_data'];
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_by'] = $this->staff_id;
			$dataToInsert['updated_at'] = $this->now_time;
			
            // Unset general_invoice_id from general_invoice_data
			if (isset($dataToInsert['general_invoice_id']))
				unset($dataToInsert['general_invoice_id']);

			$result = $this->db->insert('general_invoice', $dataToInsert);
//            echo $this->db->last_query(); exit;
			$general_invoice_id = $this->db->insert_id();
			if($result){
                $return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','General Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
                    $add_lineitem = array();
					$add_lineitem['general_invoice_id'] = $general_invoice_id;
					$add_lineitem['purchase_item_id'] = $lineitem->purchase_item_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    if(isset($lineitem->uom_id) && !empty($lineitem->uom_id)){
                        $add_lineitem['uom_id'] = $lineitem->uom_id;
                    }
					if(isset($lineitem->currency_id) && !empty($lineitem->currency_id)){
						$add_lineitem['currency_id'] = $lineitem->currency_id;
					}
					$add_lineitem['item_rate'] = $lineitem->item_rate;
                    if(isset($lineitem->disc_per) && !empty($lineitem->disc_per)){
						$add_lineitem['disc_per'] = $lineitem->disc_per;
					}
                    if(isset($lineitem->disc_value) && !empty($lineitem->disc_value)){
						$add_lineitem['disc_value'] = $lineitem->disc_value;
					}
                    if(isset($lineitem->cgst) && !empty($lineitem->cgst)){
						$add_lineitem['cgst'] = $lineitem->cgst;
						$add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
					}
                    if(isset($lineitem->sgst) && !empty($lineitem->sgst)){
						$add_lineitem['sgst'] = $lineitem->sgst;
						$add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
					}
                    if(isset($lineitem->igst) && !empty($lineitem->igst)){
						$add_lineitem['igst'] = $lineitem->igst;
						$add_lineitem['igst_amount'] = $lineitem->igst_amount;
					}
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_by'] = $this->staff_id;
					$add_lineitem['created_at'] = $this->now_time;
					$add_lineitem['updated_by'] = $this->staff_id;
					$add_lineitem['updated_at'] = $this->now_time;
					$this->crud->insert('general_invoice_details',$add_lineitem);
//                    echo $this->db->last_query(); exit;
				}
			}
            
            /* Update Purchase Item Stock by General Invoice ID */
            $this->applib->update_purchase_stock_by_general_invoice_id($general_invoice_id, '-');
		}
		print json_encode($return);
		exit;
	}

    function ajax_load_parts_details($part_id = 0) {
        if ($part_id != 0) {
            $this->load->helper('url');
            $row = $this->crud->load_parts_details_where($part_id);

            $return = array(
                'id' => $row->id,
                'part_name' => $row->part_name,
                'part_code' => $row->part_code,
                'uom' => $row->uom,
                'rate_in' => $row->rate_in,
                'igst' => $row->igst,
                'sgst' => $row->sgst,
                'cgst' => $row->cgst
            );
            print json_encode($return);
            exit;
        }
    }

    function invoice_print($general_invoice_id, $with_without_letterpad = '') {
        $this->print_pdf($general_invoice_id, $with_without_letterpad, 'print');
	}

    function invoice_email($general_invoice_id, $with_without_letterpad = ''){
		$this->print_pdf($general_invoice_id, 'wlp', 'email');
	}

    function print_pdf($general_invoice_id, $with_without_letterpad = '', $type){
        
        $general_invoice_data = $this->crud->get_row_by_id('general_invoice', array('general_invoice_id' => $general_invoice_id));
        
        if(empty($general_invoice_data)){
            redirect("general_invoice/general_invoice_list"); exit;
        }
        $general_invoice_data = $general_invoice_data[0];
        $general_invoice_data->delivery_through_id = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $general_invoice_data->delivery_through_id));
        $general_invoice_data->general_invoice_date = substr($general_invoice_data->general_invoice_date, 8, 2) .'/'. substr($general_invoice_data->general_invoice_date, 5, 2) .'/'. substr($general_invoice_data->general_invoice_date, 0, 4);
        if(!empty($general_invoice_data->purchase_order_date)){
            $general_invoice_data->purchase_order_date = substr($general_invoice_data->purchase_order_date, 8, 2) .'/'. substr($general_invoice_data->purchase_order_date, 5, 2) .'/'. substr($general_invoice_data->purchase_order_date, 0, 4);
        }
        if(!empty($general_invoice_data->sales_order_date)){
            $general_invoice_data->sales_order_date = substr($general_invoice_data->sales_order_date, 8, 2) .'/'. substr($general_invoice_data->sales_order_date, 5, 2) .'/'. substr($general_invoice_data->sales_order_date, 0, 4);
        }
        if(!empty($general_invoice_data->lr_date)){
            $general_invoice_data->lr_date = substr($general_invoice_data->lr_date, 8, 2) .'/'. substr($general_invoice_data->lr_date, 5, 2) .'/'. substr($general_invoice_data->lr_date, 0, 4);
        }
        if(!empty($general_invoice_data->lc_date)){
            $general_invoice_data->lc_date = substr($general_invoice_data->lc_date, 8, 2) .'/'. substr($general_invoice_data->lc_date, 5, 2) .'/'. substr($general_invoice_data->lc_date, 0, 4);
        }
        $general_invoice_data->bank_detail = $this->crud->get_id_by_val('company_banks','detail','id',$general_invoice_data->bank_detail);
        $general_invoice_data->sea_freight_type = $this->crud->get_id_by_val('sea_freight_type','name','id',$general_invoice_data->sea_freight_type);
        
        $general_invoice_data->port_of_loading_country_id = $this->crud->get_id_by_val('country','country','country_id',$general_invoice_data->port_of_loading_country_id);
        //$general_invoice_data->port_of_discharge_country_id = $this->crud->get_id_by_val('country','country','country_id',$general_invoice_data->port_of_discharge_country_id);
        //$general_invoice_data->place_of_delivery_country_id	 = $this->crud->get_id_by_val('country','country','country_id',$general_invoice_data->place_of_delivery_country_id);
        $general_invoice_data->origin_of_goods_country_id	 = $this->crud->get_id_by_val('country','country','country_id',$general_invoice_data->origin_of_goods_country_id);
        $general_invoice_data->final_destination_country_id	 = $this->crud->get_id_by_val('country','country','country_id',$general_invoice_data->final_destination_country_id);
        $general_invoice_data->declaration	 = $this->crud->get_id_by_val('terms_and_conditions','detail','module','general_invoice_declaration_for_export');
        
        $data['general_invoice_data'] = $general_invoice_data;
        //echo '<pre>';print_r($general_invoice_data);exit;
        $lineitems = array();
        $where = array('general_invoice_id' => $general_invoice_id);
        $general_invoice_lineitems = $this->crud->get_row_by_id('general_invoice_details', $where);
        foreach($general_invoice_lineitems as $general_invoice_lineitem){
            $general_invoice_lineitem->lineitem_id = $general_invoice_lineitem->id;
            $general_invoice_lineitem->complain_no_year = $this->crud->get_column_value_by_id('complain', 'complain_no_year', array('complain_id' => $general_invoice_lineitem->complain_id));
            $general_invoice_lineitem->part_name = $this->crud->get_column_value_by_id('parts', 'part_name', array('id' => $general_invoice_lineitem->part_id));
            $general_invoice_lineitem->hsn_code = $this->crud->get_column_value_by_id('parts', 'hsn_code', array('id' => $general_invoice_lineitem->part_id));
            $general_invoice_lineitem->item_name = $this->crud->get_column_value_by_id('purchase_items', 'item_name', array('id' => $general_invoice_lineitem->purchase_item_id));
            $general_invoice_lineitem->pi_hsn_code = $this->crud->get_column_value_by_id('purchase_items', 'hsn_code', array('id' => $general_invoice_lineitem->purchase_item_id));
            $general_invoice_lineitem->uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $general_invoice_lineitem->uom_id));
            $general_invoice_lineitem->currency = $this->crud->get_column_value_by_id('currency', 'currency', array('id' => $general_invoice_lineitem->currency_id));

            $general_invoice_lineitem->disc_per = !empty($general_invoice_lineitem->disc_per)? $general_invoice_lineitem->disc_per : '0';
            $general_invoice_lineitem->disc_value = !empty($general_invoice_lineitem->disc_value)? $general_invoice_lineitem->disc_value : '0';
            $general_invoice_lineitem->quantity = !empty($general_invoice_lineitem->quantity)? $general_invoice_lineitem->quantity : '0';
            $general_invoice_lineitem->item_rate = !empty($general_invoice_lineitem->item_rate)? $general_invoice_lineitem->item_rate : '0';

            $general_invoice_lineitem->igst = !empty($general_invoice_lineitem->igst)? $general_invoice_lineitem->igst : '0';
            $general_invoice_lineitem->cgst = !empty($general_invoice_lineitem->cgst)? $general_invoice_lineitem->cgst : '0';
            $general_invoice_lineitem->sgst = !empty($general_invoice_lineitem->sgst)? $general_invoice_lineitem->sgst : '0';
            $lineitems[] = $general_invoice_lineitem;
        }
        $data['invoice_items'] = $lineitems;
        $data['party_data'] = (array) $this->crud->load_party_with_cnt_person_3($general_invoice_data->party_id);
        $company_details = $this->applib->get_company_detail();
        $margin_left = $company_details['margin_left'];
        $margin_right = $company_details['margin_right'];
        $margin_top = $company_details['margin_top'];
        $margin_bottom = $company_details['margin_bottom'];
        
        $data['company_details'] = $company_details;
        $this->load->library('m_pdf');
        $param = "'utf-8','A4'";
        $pdf = $this->m_pdf->load($param);
        $pdf->pagenumPrefix = 'Page ';
        $pdf->pagenumSuffix = ' - ';
        $pdf->nbpgPrefix = ' out of ';
        $pdf->nbpgSuffix = ' pages';
        $pdf->setFooter('{PAGENO}{nbpg}');
        $pdf->AddPage(
                'P', //orientation
                '', //type
                '', //resetpagenum
                '', //pagenumstyle
                '', //suppress
                $margin_left, //margin-left
                $margin_right, //margin-right
                $margin_top, //margin-top
                $margin_bottom, //margin-bottom
                0, //margin-header
                0 //margin-footer
        );
//        pre_die($data);
        if ($data['party_data']['party_type_1'] == PARTY_TYPE_DOMESTIC_ID) {
            $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
        } else {
            $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'export_letter_pad_content');
        }
        if($with_without_letterpad == 'wlp'){
            $data['letterpad_details'] = $letterpad_details;
        }
        $html = $this->load->view('service/general_invoice/invoice_print', $data, true);
        $pdf->WriteHTML($html);
        if($with_without_letterpad == 'wlp'){
            $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: '. $letterpad_details->header_logo_alignment .';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>');
            $pdf->SetHTMLFooter($letterpad_details->footer_detail);
//            $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
        }
        
        if($type == 'print'){
            $pdfFilePath = "general_invoice.pdf";
            $pdf->Output($pdfFilePath, "I");
        } elseif ($type == 'email'){
            $pdfFilePath = "./uploads/general_invoice_" . $general_invoice_id . ".pdf";
            $files_urls[] = $pdfFilePath;
            $pdf->Output($pdfFilePath, "F");
            $this->db->where('general_invoice_id', $general_invoice_id);
			$this->db->update('general_invoice', array('pdf_url' => serialize($files_urls)));
			redirect(base_url() . 'mail-system3/document-mail/general_invoice/' . $general_invoice_id);    
        }
    }

}
