<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Hr
 * &@property AppModel $app_model
 * &@property AppLib $applib
 * &@property Crud $crud
 * &@property Datatables $datatable
 */
class Hr extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
        $this->load->model('Chatmodule','chat');
	}

	/**
	 * Total Free Leaves
	 */
	function total_free_leaves()
	{
		$this->db->select('*');
		$query = $this->db->get('leave_for_employee');
		$leaves = $query->row(0);
		if($this->applib->have_access_role(HR_TOTAL_FREE_LEAVES_MENU_ID,"view")) {
			set_page('hr/leaves/total_free_leaves', array('leaves' => $leaves));
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}

	}

	/**
	 * Weekly Leaves
	 */
	function weekly_leaves()
	{
		if (!empty($_POST)) {
			$WeekendHolidayDates = $this->applib->getWeekendHolidayDates();
			foreach ($WeekendHolidayDates as $DateRow) {
				if (date('Y-m-d', strtotime($DateRow)) > date('m/d/Y')) {
					$this->db->where('date', date('Y-m-d', strtotime($DateRow)));
					$this->db->delete('yearly_leaves');
				}
			}
			$this->db->update('weekly_holiday', array('day' => $_POST['weekly_holiday_day']));
			if($this->applib->have_access_role(HR_WEEKLY_LEAVES_MENU_ID,"view")) {
				set_page('hr/leaves/weekly_leaves');
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		} else {
			if($this->applib->have_access_role(HR_WEEKLY_LEAVES_MENU_ID,"view")) {
				set_page('hr/leaves/weekly_leaves');
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}

	/**
	 * Yearly Leaves
	 */
	function yearly_leaves()
	{
		$YearlyHoliday = $this->applib->getYearlyHoliday();
		if (!empty($_POST['dates'])) {
			$this->db->where('date >', date('m/d/Y'));
			$this->db->delete('yearly_leaves');
			$WeekendHolidayDates = $this->applib->getWeekendHolidayDates();
			foreach ($_POST['dates'] as $DateRow) {
				if (strtotime($DateRow) > strtotime(date('m/d/Y')) && !in_array($DateRow, $WeekendHolidayDates)) {
					$this->db->insert('yearly_leaves', array('user_id' => $this->session->userdata('is_logged_in')['staff_id'], 'date' => date('Y-m-d', strtotime($DateRow))));
				}
			}
			echo "success";
		} else {
			if($this->applib->have_access_role(HR_YEARLY_LEAVES_MENU_ID,"view")) {
				set_page('hr/leaves/yearly_leaves', array('YearlyHoliday' => $YearlyHoliday));
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}

	/**
	 * Apply Leave
	 */
	function apply_leave()
	{
		$data = array(
			'days' => $this->crud->get_select_data('days'),
		);
		if (!empty($_POST)) {
			$leave_data = $_POST;
			$leave_data['from_date'] = date('Y-m-d', strtotime($leave_data['from_date']));
			$leave_data['to_date'] = date('Y-m-d', strtotime($leave_data['to_date']));
			$leave_data['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert('employee_leaves', $leave_data);
			if ($this->db->insert_id() > 0) {
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Leave successfully added!');
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Something wrong, Please try again!');
			}
			redirect('hr/apply-leave', $data);
		} else {
			//pre_die($data);
			set_page('hr/leaves/apply_leave', $data);
		}
	}

	/**
	 * Reply Leave
	 */
	function reply_leave($leave_id)
	{
		if(!$this->applib->have_access_role(REPLY_LEAVE_MODULE_ID,"allow")) {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
		if (!empty($_POST)) {
			$leave_data = $_POST;
			$leave_data['from_date'] = date('Y-m-d', strtotime($leave_data['from_date']));
			$leave_data['to_date'] = date('Y-m-d', strtotime($leave_data['to_date']));
			$this->db->where(array('leave_id' => $leave_id));
			$this->db->update('employee_leaves', $leave_data);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Leave successfully replied!');
			redirect('hr/apply-leave');
		} else {
			$days = $this->crud->get_select_data('days');
			$leave = $this->getLeaveDetail($leave_id);
			set_page('hr/leaves/reply_leave', array('leave' => $leave, 'days' => $days));
		}
	}

	/**
	 * Edit Leave
	 */
	function edit_leave($leave_id)
	{
		if(!$this->applib->have_access_role(APPLY_FOR_LEAVE_MODULE_ID,"edit")) {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
		if (!empty($_POST)) {
			$leave_data = $_POST;
			$leave_data['from_date'] = date('Y-m-d', strtotime($leave_data['from_date']));
			$leave_data['to_date'] = date('Y-m-d', strtotime($leave_data['to_date']));
			$this->db->where(array('leave_id' => $leave_id));
			$this->db->update('employee_leaves', $leave_data);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Leave successfully saved!');
			redirect('hr/apply-leave');
		} else {
			$days = $this->crud->get_select_data('days');
			$leave = $this->getLeaveDetail($leave_id);
			set_page('hr/leaves/edit_leave', array('leave' => $leave, 'days' => $days));

		}
	}

	function getEmployeeLeaves()
	{
		$this->db->select('el.*,e.name,e.email');
		$this->db->from('employee_leaves el');
		$this->db->join('staff e', 'e.staff_id = el.employee_id');
		$this->db->where('e.active !=', 0);
		return $this->db->get()->result();
	}

	function getLeaveDetail($leave_id)
	{
		$this->db->select('el.*,e.name,e.email');
		$this->db->from('employee_leaves el');
		$this->db->join('staff e', 'e.staff_id = el.employee_id');
		$this->db->where('el.leave_id', $leave_id);
		$this->db->where('e.active !=', 0);
		$this->db->limit(1);
		$leave_data = $this->db->get()->result();
		return $leave_data[0];
	}

	/**
	 * Expected Interview
	 */
	function expected_interview()
	{
		if (!empty($_POST)) {
			$interview_data = $_POST;
			if (!empty($interview_data['birth_date'])) {
				$interview_data['birth_date'] = date('Y-m-d', strtotime($interview_data['birth_date']));
			} else {
				$interview_data['birth_date'] = '0000-00-00';
			}
			if (!empty($_FILES['image']['name'])) {
				$interview_data['image'] = $this->applib->upload_image('image', image_dir('employee/'));
			}
			$interview_data['birth_date'] = $this->applib->to_sql_date($interview_data['birth_date']);
			$this->db->insert('interview', $interview_data);
			if ($this->db->insert_id() > 0) {
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Interview successfully added!');
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Something wrong, Please try again!');
			}
			redirect('hr/expected-interview');
		} else {
			$interviews = $this->db->get('interview')->result();
			if($this->applib->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID,"view") || $this->applib->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID,"add")) {
				set_page('hr/interview/expected_interview', array('interviews' => $interviews));
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}

	/**
	 * @param $interview_id
	 */
	function interview_result($interview_id)
	{
		if (!empty($_POST)) {
			$staff_data = $interview_data = $_POST;
			$staff_data['birth_date'] = $interview_data['birth_date'] = date('Y-m-d', strtotime($staff_data['birth_date']));
			$staff_data['date_of_joining'] = $interview_data['date_of_joining'] = date('Y-m-d', strtotime($staff_data['date_of_joining']));
			$staff_data['date_of_leaving'] = $interview_data['date_of_leaving'] = date('Y-m-d', strtotime($staff_data['date_of_leaving']));

			if (!empty($_FILES['image']['name'])) {
				$interview_data['image'] = $this->applib->upload_image('image', image_dir('employee/'));
				$this->applib->unlink_file($_POST['image_old']);
			}
			$interview_data['mailbox_email'] = $interview_data['email'];
			if ($interview_data['pass'] != '') {
				$interview_data['pass'] = md5($interview_data['pass']);
			} else {
				unset($interview_data['pass']);
			}
			if ($interview_data['mailbox_password'] != '') {

			} else {
				unset($interview_data['mailbox_password']);
			}
			unset($interview_data['image_old']);
			$this->db->where(array('interview_id' => $interview_id));
			$this->db->update('interview', $interview_data);

			/*---------------------*/
			if ($interview_data['interview_decision'] == 1) { //interview_decision == 1 [employee selected]
				if (!$this->app_model->check_val_exist('staff', 'interview_id', $interview_id)) {
					if (!empty($_FILES['image']['name'])) {
						$staff_data['image'] = $this->applib->upload_image('image', image_dir('staff/'), false);
					} else {
						$file_info = pathinfo($staff_data['image_old']);
						$file_name = md5(date('Y-m-d H:i:s') . rand(00000, 99999)) . "." . $file_info['extension'];
						$to = image_dir("staff/$file_name");
						if (copy($staff_data['image_old'], $to)) {
							$staff_data['image'] = $file_name;
						}
					}
					unset($staff_data['image_old']);
					unset($staff_data['interview_review']);
					unset($staff_data['interview_decision']);
					$staff_data['interview_id'] = $interview_id;
					$staff_data['mailbox_email'] = $staff_data['email'];
					$staff_data['pass'] = md5($staff_data['pass']);
					$staff_data['created_at'] = date('Y-m-d H:i:s');
					$this->db->insert('staff', $staff_data);
				}
			}
			/*---------------------*/

			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Interview successfully saved!');
			redirect('hr/expected-interview');
		} else {
			$interview = $this->crud->get_row_by_id('interview', array('interview_id' => $interview_id));
			$interview = $interview[0];
			$data = array(
				'designation' => $this->crud->get_select_data('designation'),
				'grade' => $this->crud->get_select_data('grade'),
				'department' => $this->crud->get_select_data('department'),
				'interview' => $interview
			);
			if($this->applib->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID,"edit")) {
				set_page('hr/interview/interview_result', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}


	/**
	 * @param $interview_id
	 */
	function interview_delete($interview_id)
	{
		$image = $this->crud->get_id_by_val('interview', 'image', 'interview_id', $interview_id);
		$this->applib->unlink_file($image);
		$this->db->where(array('interview_id' => $interview_id));
		$this->db->delete('interview');
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Interview successfully deleted!');
		redirect('hr/expected-interview');
	}

	/**
	 * Expected Interview
	 */
	function employee_master(){
        $data = array();
		if($this->applib->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID,"view")) {
            $data['employees'] = $this->crud->get_all_with_where('staff', 'staff_id', 'desc', array('active !=' => 0));
            $data['agents'] = $this->chat->getAllAgents($this->staff_id);
			set_page('hr/employee/employee_master', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}

	}

	/**
	 * Employee DataTable
	 */
	function employee_datatable()
	{
		$config['table'] = 'staff';
		$config['column_order'] = array('updated_at', null, 'name', 'contact_no', 'email','salary', 'image', 'birth_date', 'qualification', 'date_of_joining');
		$config['column_search'] = array('name', 'contact_no', 'email', 'salary', 'image', 'DATE_FORMAT(birth_date,"%d-%m-%Y")', 'qualification', 'DATE_FORMAT(date_of_joining,"%d-%m-%Y")');
		$config['wheres'][] = array('column_name' => 'active !=', 'column_value' => '0');
        if(isset($_POST['have_party_or_not']) && !empty($_POST['have_party_or_not'])){
            if($_POST['have_party_or_not'] == '1'){
                $config['custom_where'] = "staff_id NOT IN(SELECT current_party_staff_id FROM party)";
            } else if($_POST['have_party_or_not'] == '2'){
                $config['custom_where'] = "staff_id IN(SELECT current_party_staff_id FROM party)";
            }
        }
		$config['order'] = array('staff_id' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$access_edit  = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "edit");
		$access_del  = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "delete");
		$access_change_password  = $this->app_model->have_access_role(HR_CHANGE_EMPLOYEE_PASSWORD_MENU_ID, "allow");
        $current_party_staff_id = $this->crud->get_column_data_array('party','current_party_staff_id');
        
		$data = array();
		foreach ($list as $employee) {
			$row = array();
			$action = '';
			if($access_edit){
				$action .= '<a href="' . base_url() . 'hr/add/' . $employee->staff_id . '" class=" btn-primary btn-xs"><i class="fa fa-edit"></i></a> | ';	
			}
			if($access_del){
				$action .= '<a onclick="return confirm(\'Are you sure delete this record?\');" href="' . base_url() . 'hr/delete-employee/' . $employee->staff_id . '" class=" btn-danger btn-xs"><i class="fa fa-trash"></i></a> | ';	
			}
			if($access_change_password){
				$action .= '<a href="' . base_url() . 'hr/change-employee-password/' . $employee->staff_id . '">Change Password</a> | ';
			}
            if($this->app_model->have_access_role(PARTY_MODULE_ID, "is_management") == 1){
                if(in_array($employee->staff_id, $current_party_staff_id)){
                    $action .= '<a href="javascript:void(0);" class="btn btn-xs btn-warning transfer_button open_transfer_party_modal" data-transfer_from="' . $employee->staff_id . '" > All Party Transfer To </a> | ';
                }
            }
			$action .= '<a href="' . base_url('hr/print_employee/' . $employee->staff_id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
			$row[] = $employee->updated_at;
			$row[] = $action;
			$row[] = $employee->name;
			$row[] = 'JK'.$employee->employee_code;
			$row[] = $employee->contact_no;
			$row[] = $employee->email;
			$row[] = $employee->salary;
			if (!empty($employee->image)) {
				$row[] = '<img height="80px" width="80px" src="' . image_url('staff/' . $employee->image) . '">';
			} else {
				$row[] = '<img height="80px" width="80px" src="' . base_url() . 'resource/dist/img/default-user.png">';
			}
			if (!empty($employee->birth_date) && $employee->birth_date != '0000-00-00') {
				$row[] = date('d-m-Y', strtotime($employee->birth_date));
			} else {
				$row[] = '';
			}
			$row[] = $employee->qualification;
			if (!empty($employee->date_of_joining) && strtotime($employee->date_of_joining) != 0) {
				$row[] = date('d-m-Y', strtotime($employee->date_of_joining));
			} else {
				$row[] = '';
			}
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	/**
	 * Expected Interview DataTable
	 */
	function expected_interview_datatable()
	{
		$config['table'] = 'interview';
		$config['column_order'] = array(null, 'name', 'qualification', 'created_at');
		$config['column_search'] = array('name', 'qualification', 'created_at');
		$config['order'] = array('interview_id' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$access_del = $this->app_model->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID, "delete");
		$access_edit  = $this->app_model->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID, "edit");
		$data = array();
		foreach ($list as $interview) {
			$row = array();
			$action = '';
			if($access_del){
				$action .= '<a onclick="return confirm(\'Are you sure delete this record?\');" href="' . base_url() . 'hr/interview-delete/' . $interview->interview_id . '"class=" btn-danger btn-xs"><i class="fa fa-trash"></i></a> | ';	
			}
			if($access_edit){
				$action .= '<a href="' . base_url() . 'hr/interview-result/' . $interview->interview_id . '">Interview Result</a>';	
			}
			$row[] = $action;
			$row[] = $interview->name;
			$row[] = $interview->qualification;
			$row[] = $interview->created_at;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	/**
	 * Employees Leaves DataTable
	 */
	function employees_leaves_datatable()
	{
		$logged_in = $this->session->userdata("is_logged_in");
		$id = $logged_in['staff_id'];
		$config['select'] = 'el.*,s.name,s.email';
		$config['table'] = 'employee_leaves el';
		$config['column_order'] = array(null, 'name', 'email', 'from_date', 'to_date', 'leave_status');
		$config['column_search'] = array('name', 'email', 'from_date', 'to_date', 'leave_status');
		$config['order'] = array('leave_id' => 'desc');
		if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){
		} else {
			$config['wheres'][] = array('column_name' => 'employee_id', 'column_value' => $id);
		}
		$config['joins'][0] = array('join_table' => 'staff s', 'join_by' => 's.staff_id = el.employee_id');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $employee_leave) {
			$row = array();
			$action = '';
			if ($this->applib->have_access_role(APPLY_FOR_LEAVE_MODULE_ID, 'edit')) {
				$action .= '<a href="' . base_url() . 'hr/edit-leave/' . $employee_leave->leave_id . '"class=" btn-primary btn-xs"><i class="fa fa-edit"></i></a>';	
			}
			if ($this->applib->have_access_role(REPLY_LEAVE_MODULE_ID, 'allow')) {
				$action .= ' <a href="' . base_url() . 'hr/reply-leave/' . $employee_leave->leave_id . '"class=" btn-primary btn-xs"><i class="fa fa-reply"></i></a>';
			}

			$row[] = $action;
			$row[] = $employee_leave->name;
			$row[] = $employee_leave->email;
			$row[] = date('d-m-Y', strtotime($employee_leave->from_date));
			$row[] = date('d-m-Y', strtotime($employee_leave->to_date));
			$row[] = $employee_leave->leave_status;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	/**
	 * Add Employee
	 */
	
	function add($staff_id = '') {
		$data = array();
		if (!empty($staff_id)) {
			if($this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "edit")){
				$employee_data = $this->crud->get_row_by_id('staff', array('staff_id' => $staff_id));

				if(empty($employee_data)){
					redirect("hr/employee_list");
				}
				$employee_data                  = $employee_data[0];
				$employee_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $employee_data->created_by));
				$employee_data->created_at = substr($employee_data->created_at, 8, 2) . '-' . substr($employee_data->created_at, 5, 2) . '-' . substr($employee_data->created_at, 0, 4);
				$employee_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $employee_data->updated_by));
				$employee_data->updated_at  = substr($employee_data->updated_at, 8, 2) . '-' . substr($employee_data->updated_at, 5, 2) . '-' . substr($employee_data->updated_at, 0, 4);
				$data['employee_data']      = $employee_data;
//                $data['incentive_data'] = $this->crud->get_column_value_by_id('employee_wise_incentive', 'name', array('staff_id' => $employee_data->updated_by));
                $employee_wise_incentive = $this->crud->get_row_by_id('employee_wise_incentive', array('staff_id' => $staff_id));
//                echo '<pre>'; print_r($employee_wise_incentive); exit;
                $lineitems = array();
                foreach($employee_wise_incentive as $employee_wise_inc){
                    $employee_wise_inc->item_name = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $employee_wise_inc->item_id));
                    if($employee_wise_inc->no_of_item == '1'){
                        $employee_wise_inc->no_of_item_data = '1st';
                    } elseif ($employee_wise_inc->no_of_item == '2') {
                        $employee_wise_inc->no_of_item_data = '2nd';
                    } elseif ($employee_wise_inc->no_of_item == '3') {
                        $employee_wise_inc->no_of_item_data = '3rd';
                    } elseif ($employee_wise_inc->no_of_item == '4') {
                        $employee_wise_inc->no_of_item_data = '4 & 4+';
                    }
                    $lineitems[] = json_encode($employee_wise_inc);
                }
                $data['employee_wise_incentive'] = implode(',', $lineitems);
                
//				echo "<pre>";  print_r($data); exit;
				set_page('hr/employee/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if ($this->applib->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "add")) {
                $data['c_pf_per'] = $this->crud->get_column_value_by_id('company', 'c_pf_per', array('id' => COMPANY_ID));
                $data['c_esi_per'] = $this->crud->get_column_value_by_id('company', 'c_esi_per', array('id' => COMPANY_ID));
				set_page('hr/employee/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

    function save_employee(){
		$msg = 'Employee successfully added!';
		$return['msg'] = true;
		$post_data = $this->input->post();
        $line_items_data = json_decode($post_data['line_items_data']);
//		echo "<pre>";  print_r($post_data); exit;
		//echo "<pre>";  print_r($_FILES); exit;

		if (!empty($_FILES['image']['name'])) {
			$post_data['employee_data']['image'] = $this->applib->upload_image('image', image_dir('staff/'), false);				
			if(isset($post_data['employee_data']['image']['error'])){
				$msg = $post_data['employee_data']['image']['error'];
				$return['msg'] = false;
				unset($post_data['employee_data']['image']);
			}
		}else if (!empty($post_data['employee_data']['image_old'])) {
			$post_data['employee_data']['image'] = $post_data['employee_data']['image_old'];
		}else {
			if($post_data['employee_data']['gender'] == 'm'){
				$post_data['employee_data']['image'] = "default_m.png";	
			}elseif($post_data['employee_data']['gender'] == 'f'){
				$post_data['employee_data']['image'] = "default_f.png";
			}else{
				$post_data['employee_data']['image'] = "default-user.png";
			}
		}
		unset($post_data['employee_data']['image_old']);

		if (!empty($_FILES['signature']['name'])) {
			$post_data['employee_data']['signature'] = $this->applib->upload_image('signature', image_dir('staff/signature/'), false);				
			//echo '<pre>';print_r($post_data['employee_data']['signature']);exit;
			if(isset($post_data['employee_data']['signature']['error'])){
				$error_msg = $post_data['employee_data']['signature']['error'];
				if(strcmp($error_msg,"The filetype you are attempting to upload is not allowed.")){
					$msg .= ', Please upload .jpg file for signature.';
				}else{
					$msg .= $error_msg;	
				}
				$return['msg'] = false;
				unset($post_data['employee_data']['signature']);
			}
		}else if (!empty($post_data['employee_data']['signature_old'])) {
			$post_data['employee_data']['signature'] = $post_data['employee_data']['signature_old'];
		} else {
			$post_data['employee_data']['signature'] = "default-user.png";
		}
		unset($post_data['employee_data']['signature_old']);

		$post_data['employee_data']['birth_date'] = (!empty(strtotime($post_data['employee_data']['birth_date']))) ? date("Y-m-d", strtotime($post_data['employee_data']['birth_date'])) : null;
		$post_data['employee_data']['marriage_date'] = (!empty(strtotime($post_data['employee_data']['marriage_date']))) ? date("Y-m-d", strtotime($post_data['employee_data']['marriage_date'])) : null;
		$post_data['employee_data']['passport_issue_date'] = (!empty(strtotime($post_data['employee_data']['passport_issue_date']))) ? date("Y-m-d", strtotime($post_data['employee_data']['passport_issue_date'])) : null;
		$post_data['employee_data']['passport_expired_date'] = (!empty(strtotime($post_data['employee_data']['passport_expired_date']))) ? date("Y-m-d", strtotime($post_data['employee_data']['passport_expired_date'])) : null;
		$post_data['employee_data']['date_of_joining'] = (!empty(strtotime($post_data['employee_data']['date_of_joining']))) ? date("Y-m-d", strtotime($post_data['employee_data']['date_of_joining'])) : null;
		$post_data['employee_data']['date_of_leaving'] = (!empty(strtotime($post_data['employee_data']['date_of_leaving']))) ? date("Y-m-d", strtotime($post_data['employee_data']['date_of_leaving'])) : null;
		$post_data['employee_data']['house_rent'] = (!empty($post_data['employee_data']['house_rent'])) ? $post_data['employee_data']['house_rent'] : null;
		$post_data['employee_data']['pf_per'] = (!empty($post_data['employee_data']['pf_per'])) ? $post_data['employee_data']['pf_per'] : null;
		$post_data['employee_data']['pf_amount'] = (!empty($post_data['employee_data']['pf_amount'])) ? $post_data['employee_data']['pf_amount'] : null;
		$post_data['employee_data']['other_allotment'] = (!empty($post_data['employee_data']['other_allotment'])) ? $post_data['employee_data']['other_allotment'] : null;
		$post_data['employee_data']['professional_tax'] = (!empty($post_data['employee_data']['professional_tax'])) ? $post_data['employee_data']['professional_tax'] : null;
		$post_data['employee_data']['salary'] = (!empty($post_data['employee_data']['salary'])) ? $post_data['employee_data']['salary'] : null;
		$post_data['employee_data']['traveling_allowance'] = (!empty($post_data['employee_data']['traveling_allowance'])) ? $post_data['employee_data']['traveling_allowance'] : null;
		$post_data['employee_data']['bonus_amount'] = (!empty($post_data['employee_data']['bonus_amount'])) ? $post_data['employee_data']['bonus_amount'] : null;
		$post_data['employee_data']['monthly_gross'] = (!empty($post_data['employee_data']['monthly_gross'])) ? $post_data['employee_data']['monthly_gross'] : null;
		$post_data['employee_data']['cost_to_company'] = (!empty($post_data['employee_data']['cost_to_company'])) ? $post_data['employee_data']['cost_to_company'] : null;
		$post_data['employee_data']['basic_pay'] = (!empty($post_data['employee_data']['basic_pay'])) ? $post_data['employee_data']['basic_pay'] : null;
		$post_data['employee_data']['education_allowance'] = (!empty($post_data['employee_data']['education_allowance'])) ? $post_data['employee_data']['education_allowance'] : null;
		$post_data['employee_data']['medical_reimbursement'] = (!empty($post_data['employee_data']['medical_reimbursement'])) ? $post_data['employee_data']['medical_reimbursement'] : null;
		$post_data['employee_data']['pf_employee'] = (!empty($post_data['employee_data']['pf_employee'])) ? $post_data['employee_data']['pf_employee'] : null;
		$post_data['employee_data']['intensive_per'] = (!empty($post_data['employee_data']['intensive_per'])) ? $post_data['employee_data']['intensive_per'] : null;
		$post_data['employee_data']['esi_per'] = (!empty($post_data['employee_data']['esi_per'])) ? $post_data['employee_data']['esi_per'] : null;
		$post_data['employee_data']['esi_amount'] = (!empty($post_data['employee_data']['esi_amount'])) ? $post_data['employee_data']['esi_amount'] : null;
                $post_data['employee_data']['allow_pf'] = isset($post_data['allow_pf']) && !empty($post_data['allow_pf']) ? $post_data['allow_pf'] : NULL;
                $post_data['employee_data']['allow_esi'] = isset($post_data['allow_esi']) && !empty($post_data['allow_esi']) ? $post_data['allow_esi'] : NULL;
                $post_data['employee_data']['designation_id'] = isset($post_data['designation_id']) && !empty($post_data['designation_id']) ? $post_data['designation_id'] : NULL;
                $post_data['employee_data']['department_id'] = isset($post_data['department_id']) && !empty($post_data['department_id']) ? $post_data['department_id'] : NULL;
                $post_data['employee_data']['grade_id'] = isset($post_data['grade_id']) && !empty($post_data['grade_id']) ? $post_data['grade_id'] : NULL;
                
		if(isset($post_data['employee_data']['email']) && !empty($post_data['employee_data']['email'])){
			$post_data['employee_data']['mailbox_email'] = $post_data['employee_data']['email'];
		}
		if(isset($post_data['employee_data']['pass']) && !empty($post_data['employee_data']['pass'])){
			$post_data['employee_data']['pass'] = md5($post_data['employee_data']['pass']);
		}else{
			unset($post_data['employee_data']['pass']);
		}
		if(isset($post_data['employee_data']['mailbox_password']) && !empty($post_data['employee_data']['mailbox_password'])){
		}else{
			unset($post_data['employee_data']['mailbox_password']);
		}
		if(isset($post_data['staff_id']) && !empty($post_data['staff_id'])){
			$post_data['employee_data']['updated_at'] = $this->now_time;
			$post_data['employee_data']['updated_by'] = $this->staff_id;

			$this->db->where('staff_id', $post_data['staff_id']);
			$dataToInsert = $post_data['employee_data'];
			if (isset($post_data['staff_id'])){
//				unset($post_data['staff_id']);
			}
			$result = $this->db->update('staff', $dataToInsert);
//            echo $this->db->last_query(); exit;
			if($result){
                
                if(!empty($post_data['deleted_lineitem_id'])) {
                    foreach($post_data['deleted_lineitem_id'] as $id){
                        $this->crud->delete('employee_wise_incentive', array('id' => $id));
                    }
                }
                
                if (!empty($line_items_data)) {
                    foreach ($line_items_data as $lineitem) {
                        $update_item = array();
                        $update_item['staff_id'] = $post_data['staff_id'];
                        $update_item['item_id'] = $lineitem->item_id;
                        $update_item['no_of_item'] = $lineitem->no_of_item;
                        $update_item['incentive'] = $lineitem->incentive;
                        $update_item['updated_at'] = $this->now_time;
                        $update_item['updated_by'] = $this->logged_in_id;

                        if($lineitem->id != ''){
                            $where = array("id" => $lineitem->id);
                            $this->crud->update("employee_wise_incentive", $update_item, $where);
                        } else {
                            $update_item['created_at'] = $this->now_time;
                            $update_item['created_by'] = $this->logged_in_id;
                            $this->crud->insert('employee_wise_incentive', $update_item);
                        }
                    }
                }
                
				$return['success'] = "Updated";
				$msg = 'Employee Updated Successfully';
				$this->session->set_flashdata('success',$return['msg']);
				$this->session->set_flashdata('message', $msg);
			}

		} else {
            $emp_code = $this->crud->get_max_number('staff', 'employee_code');
            $employee_code = 101;
            if ($emp_code->employee_code > 0) {
                $employee_code = $emp_code->employee_code + 1;
            }
			$post_data['employee_data']['employee_code'] = $employee_code;
			$post_data['employee_data']['created_at'] = $this->now_time;
			$post_data['employee_data']['created_by'] = $this->staff_id;
			$post_data['employee_data']['updated_at'] = $this->now_time;
			$post_data['employee_data']['updated_by'] = $this->staff_id;
			$dataToInsert = $post_data['employee_data'];
			$result = $this->db->insert('staff', $dataToInsert);
			$staff_id = $this->db->insert_id();
            
			if($result){
                
//                echo "<pre>";
//                print_r($line_items_data);die;
                
                $item_inc = 1;
                if (!empty($line_items_data)) {
                    
                    foreach ($line_items_data as $lineitem) {
                        $insert_item = array();
                        $insert_item['staff_id'] = $staff_id;
                        $insert_item['item_id'] = $lineitem->item_id;
                        $insert_item['no_of_item'] = $lineitem->no_of_item;
                        $insert_item['incentive'] = $lineitem->incentive;
                        $insert_item['created_at'] = $this->now_time;
                        $insert_item['created_by'] = $this->logged_in_id;
                        $insert_item['updated_at'] = $this->now_time;
                        $insert_item['updated_by'] = $this->logged_in_id;
                        $this->crud->insert('employee_wise_incentive', $insert_item);
                    }
                }
                
				$return['success'] = "Added";
				$this->session->set_flashdata('success',$return['msg']);
				$this->session->set_flashdata('message','Employee Added Successfully');
			}
		}
		print json_encode($return);
		exit;

	}

	function print_employee($id){
		$employee_detail = $this->crud->get_all_with_where('staff','','',array('staff_id' => $id));

		$employee_detail = $employee_detail[0];
		//echo '<pre>';print_r($employee_detail->designation_id);exit;
		$employee_detail->designation_id = $this->crud->get_id_by_val('designation', 'designation', 'designation_id', $employee_detail->designation_id);
		$employee_detail->department_id = $this->crud->get_id_by_val('department', 'department', 'department_id', $employee_detail->department_id);
		//echo '<pre>';print_r($this->db->last_query());exit;

		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		$this->load->library('m_pdf');
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$pdf->pagenumPrefix = 'Page ';
		$pdf->pagenumSuffix = ' - ';
		$pdf->nbpgPrefix = ' out of ';
		$pdf->nbpgSuffix = ' pages';
		$pdf->setFooter('{PAGENO}{nbpg}');
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$html = $this->load->view('hr/employee/pdf', array('employee_detail' => $employee_detail, 'is_first_item' => true), true);
		$pdf->WriteHTML($html);
		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
		$pdfFilePath = "purchases_order.pdf";
		$pdf->Output($pdfFilePath, "I");
	}

	function check_email_password()
	{
		$mailbox_email = trim($_POST['mailbox_email']);
		$mailbox_password = trim($_POST['mailbox_password']);
		$mb = imap_open('{imap.gmail.com:993/imap/ssl}INBOX', $mailbox_email, $mailbox_password) or die(json_encode(array('success' => false, 'massage' => 'Password is false.', 'error' => imap_last_error())));
		echo json_encode(array('success' => true, 'massage' => 'Password is true.'));
	}

	function check_email_address()
	{
		if (!empty($_POST)) {
			$this->db->where('email', $_POST['email']);
			$this->db->where('active !=', 0);
			$query = $this->db->get('staff');
			$count_row = $query->num_rows();
			if ($count_row > 0) {
				echo json_encode(array("status" => FALSE, "msg" => 'Email address already exist!'));
				exit;
			} else {
				echo json_encode(array("status" => TRUE));
				exit;
			}
		}
	}

	function check_email_address_edit()
	{
		if (!empty($_POST)) {
			$ignore = array($_POST['id']);
			$this->db->where('email', $_POST['email']);
			$this->db->where('active !=', 0);
			$this->db->where_not_in('staff_id', $ignore);
			$query = $this->db->get('staff');
			$count_row = $query->num_rows();
			if ($count_row > 0) {
				echo json_encode(array("status" => FALSE, "msg" => 'Email address allready exist!'));
				exit;
			} else {
				echo json_encode(array("status" => TRUE));
				exit;
			}
		}
	}

	/**
	 * @param $staff_id
	 */
	function change_employee_password($staff_id)
	{
		if(!$this->applib->have_access_role(HR_CHANGE_EMPLOYEE_PASSWORD_MENU_ID,"allow")) {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
		$data = array();
		if (isset($_POST['ac_pass'])) {
			$this->form_validation->set_rules('staff_id', 'User ID', 'trim|required');
			//$this->form_validation->set_rules('old_pass', 'old password', 'trim|required|callback_check_old_password');
			$this->form_validation->set_rules('new_pass', 'new password', 'trim|required');
			$this->form_validation->set_rules('confirm_pass', 'confirm Password', 'trim|required|matches[new_pass]');
			if ($this->form_validation->run()) {
				$this->db->where('staff_id', $_POST['staff_id']);
				$this->db->update('staff', array('pass' => md5($_POST['new_pass'])));
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Employee password changed successfully!');
				redirect('hr/employee_master');
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Old password not matched!');
				redirect('hr/change-employee-password/' . $staff_id);
			}
		} else if (isset($_POST['email_pass'])) {
			$this->form_validation->set_rules('staff_id', 'User ID', 'trim|required');
			//$this->form_validation->set_rules('old_pass_email', 'old password', 'trim|required|callback_check_old_password1');
			$this->form_validation->set_rules('new_pass_email', 'new password', 'trim|required');
			$this->form_validation->set_rules('confirm_pass_email', 'confirm Password', 'trim|required|matches[new_pass_email]');
			if ($this->form_validation->run()) {
				$this->db->where('staff_id', $_POST['staff_id']);
				//$this->db->update('staff', array('mailbox_password' => md5($_POST['new_pass_email'])));
				$this->db->update('staff', array('mailbox_password' => $_POST['new_pass_email']));
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Employee Email password changed successfully!');
				redirect('hr/employee_master');
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Old password not matched!');
				redirect('hr/change-employee-password/' . $staff_id);
			}
		} else {
			$employee = $this->crud->get_row_by_id('staff', array('staff_id' => $staff_id));
			$employee = $employee[0];
			set_page('hr/employee/change_employee_password', array('employee' => $employee));
		}
	}

	function check_old_password($old_pass)
	{
		$staff_id = $_POST['staff_id'];
		$query = $this->db->get_where('staff', array('staff_id' => $staff_id, 'pass' => md5($old_pass)));
		if ($query->num_rows() > 0) {
			return true;
		} else {
			$this->form_validation->set_message('check_old_password', 'wrong old password.');
			return false;
		}
	}

	function check_old_password1($old_pass)
	{
		$staff_id = $_POST['staff_id'];
		$query = $this->db->get_where('staff', array('staff_id' => $staff_id, 'mailbox_password' => md5($old_pass)));
		if ($query->num_rows() > 0) {
			return true;
		} else {
			$this->form_validation->set_message('check_old_password', 'wrong old password.');
			return false;
		}
	}

	/**
	 * @param $staff_id
	 */
	function delete_employee($staff_id)
	{
		/*$image = $this->crud->get_id_by_val('staff', 'image', 'staff_id', $staff_id);
		$this->applib->unlink_file(image_dir('staff/' . $image));
		$this->db->where(array('staff_id' => $staff_id));
		$this->db->delete('staff');*/
		$this->crud->update('staff', array('active' => 0), array('staff_id' => $staff_id));
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Employee successfully deleted!');
		redirect('hr/employee_master');
	}

	/**
	 * Offer Letter
	 */
	function letter($id='')
	{
		$letter = array();
		if(isset($id) && !empty($id)){ 
			$where_array['letter_id'] = $id;
			$letter = $this->crud->get_all_with_where('letters','letter_id','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('letters', 'letter_id', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['letter_id'] = ($letter) ? $letter[0]->letter_id : '';
		$return['letter_topic'] = ($letter) ? $letter[0]->letter_topic : '';
		$return['letter_for'] = ($letter) ? $letter[0]->letter_for : '';
		$return['letter_template'] = ($letter) ? $letter[0]->letter_template : '';

		if($this->applib->have_access_role(HR_ADD_LETTER_MENU_ID,"view")) {
			set_page('hr/letters/add_letter', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function save_letter() {
		$post_data = $this->input->post();
		if(isset($post_data['letter_id']) && !empty($post_data['letter_id'])){
			$where_array['letter_id'] = $postdata['letter_id'];
			$post_data['updated_by'] = $this->staff_id;
			$post_data['updated_at'] = $this->now_time;
			$result = $this->crud->update('letters', $post_data, array('letter_id' => $post_data['letter_id']));
			if ($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Letter Updated Successfully');
			}
		} else { 
			$post_data['created_by'] = $this->staff_id;
			$post_data['created_at'] = $this->now_time;
			$post_data['updated_by'] = $this->staff_id;
			$post_data['updated_at'] = $this->now_time;
			//echo"<pre>"; print_r($post_data); exit;
			$result = $this->crud->insert('letters',$post_data);
			if ($result){
				$return['success'] = "Added";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Letter Added Successfully');
			}
		}
		echo json_encode($return);
		exit;
	} 

	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Deleted Successfully');
	}

	function print_letter($id='') { 
            $print_letter = array();
            if(isset($id) && !empty($id)){
                if($this->app_model->have_access_role(HR_ISSUED_LETTER_MENU_ID, "edit")){
                    $where_array['id'] = $id;
                    $print_letter = $this->crud->get_all_with_where('print_letter','id','ASC',$where_array);	
                    $return = array();
                    $return['id'] = ($print_letter) ? $print_letter[0]->id : '';
                    $return['letter_for'] = $this->crud->get_column_value_by_id('letter_for', 'label', array('id' => $print_letter[0]->letter_for_id));
                    $return['staff'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $print_letter[0]->person_id));
                    $return['letter_topic'] = $this->crud->get_column_value_by_id('letters', 'letter_topic', array('letter_id' => $print_letter[0]->letter_topic_id));
                    $return['letter_template'] = ($print_letter) ? $print_letter[0]->letter_template : '';
                    set_page('hr/letters/print_letter', $return);
                } else {
                    $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                    redirect("/");
                }
            } else {
                if($this->applib->have_access_role(HR_PRINT_LETTER_MENU_ID,"print")) {
                    set_page('hr/letters/print_letter');
                } else {
                    $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
	}

	function get_letter_template(){
		$return = array();
		if(isset($_POST['letter'])){ 
			$return = $this->crud->get_column_value_by_id('letters', 'letter_template', array('letter_id' => $_POST['letter']));
		}
		echo json_encode($return);
		exit;
	}

	function save_print_letter()
        {   
            $post_data = $this->input->post();
             
            if(isset($post_data['id']) && !empty($post_data['id'])){
                $post_data['updated_by'] = $this->staff_id;
                $post_data['updated_at'] = $this->now_time;
                $result = $this->crud->update('print_letter', $post_data, array('id' => $post_data['id']));
            
                if ($result){
                    $return['success'] = "Updated";
                    $return['print_letter_id'] = $post_data['id'];
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', 'Letter Updated Successfully');
                }
            } else {
		$post_data = $this->input->post();
		$post_data['created_by'] = $this->staff_id;
		$post_data['created_at'] = $this->now_time;
                $result = $this->crud->insert('print_letter',$post_data);
                
                    if ($result){
                        $return['success'] = "Added";
                        $return['print_letter_id'] = $result;
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', 'Letter Added Successfully');
                }
            }
            echo json_encode($return);
            exit;
        } 

	function letter_pdf($print_letter_id){
		$this->print_letter_pdf($print_letter_id,'print');
	}
	
	function letter_email($print_letter_id){
		$this->print_letter_pdf($print_letter_id,'email');
	}
	
	function print_letter_pdf($print_letter_id ,$for){
		$letter_row = $this->crud->get_row_by_id('print_letter',array('id' => $print_letter_id));
		$print_letter_row = $letter_row[0];
		if($print_letter_row->letter_for_id == LETTER_FOR_STAFF_ID){
			$this->db->select('s.*,desig.designation,dept.department,grade.grade');
			$this->db->from('staff s');
			$this->db->join('department dept', 'dept.department_id = s.department_id', 'left');
			$this->db->join('designation desig', 'desig.designation_id = s.designation_id', 'left');
			$this->db->join('grade', 'grade.grade_id = s.grade_id', 'left');
			$this->db->where('s.staff_id', $print_letter_row->person_id);
			$employee_data = $this->db->get()->row(0);
			$vars = array(
				'{(Employee Name)}' => $employee_data->name,
				'{(Designation)}' => $employee_data->designation,
				'{{Current Date}}' => date('d-m-Y'),
				'{{Date of Joining}}' => date('d F Y', strtotime($employee_data->date_of_joining)),
				'{{Date of relieving}}' => date('d F Y', strtotime($employee_data->date_of_leaving)),
                                '{{Reference No}}' => $print_letter_row->id,
			);
		}elseif($print_letter_row->letter_for_id == LETTER_FOR_INTERVIEWED_PERSON_ID){
			$this->db->select('i.*,desig.designation,dept.department,grade.grade');
			$this->db->from('interview i');
			$this->db->join('department dept', 'dept.department_id = i.department_id', 'left');
			$this->db->join('designation desig', 'desig.designation_id = i.designation_id', 'left');
			$this->db->join('grade', 'grade.grade_id = i.grade_id', 'left');
			$this->db->where('i.interview_id', $print_letter_row->person_id);
			$employee_data = $this->db->get()->row(0);
			$vars = array(
				'{(Employee Name)}' => $employee_data->name,
				'{(Designation)}' => $employee_data->designation,
				'{{Current Date}}' => date('d-m-Y'),
				'{{Date of Joining}}' => $employee_data->date_of_joining,
				'{{Date of relieving}}' => $employee_data->date_of_leaving,
				'{{Reference No}}' => $print_letter_row->id,
			);			
		}else{}
		$html = $print_letter_row->letter_template;
		$html = strtr($html, $vars);
		$this->load->library('m_pdf');
		$param = "'utf-8','A4'";
		$pdf = $this->m_pdf->load($param);
		$letter_topic = $this->crud->get_id_by_val('letters','letter_topic','letter_id',$print_letter_row->letter_topic_id);
		$pdf->SetTitle($letter_topic);
		$margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
		$letter_code = $this->crud->get_id_by_val('letters','letter_code','letter_id',$print_letter_row->letter_topic_id);			
		
		if($for == 'print'){
			$pdfFilePath = $letter_code.'_'.$print_letter_id.".pdf";
			$pdf->WriteHTML($html);
			$pdf->Output($pdfFilePath, "I");
		}elseif($for == 'email'){
			$pdfFilePath = "./uploads/" . $letter_code . "_" . $print_letter_id . ".pdf";
			$files_urls[] = $pdfFilePath;
			$pdf->WriteHTML($html);
			$pdf->Output($pdfFilePath, "F");
			$this->db->where('id', $print_letter_id);
			$this->db->update('print_letter', array('pdf_url' => serialize($files_urls)));
			redirect(base_url() . 'mail-system3/document-mail/print_letter/' . $print_letter_id);
		}
	}

	function issued_letter_list(){
		if($this->app_model->have_access_role(HR_ISSUED_LETTER_MENU_ID, "view")){
			set_page('hr/letters/issued_letter_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function issued_letter_list_datatable(){
		$post_data = $this->input->post();
		$select = 'pl.*';
		$select .= ',lf.label AS lf_label,s.name AS s_name,st.name AS st_name,i.name AS i_name,l.letter_topic AS l_letter_topic';
		$config['table'] = 'print_letter pl';
		$config['select'] = $select;
		
		$config['column_order'] = array(null,'lf.label','s.name,i.name','l.letter_topic','st.name','created_at');
		$config['column_search'] = array('lf.label','s.name','i.name','l.letter_topic','st.name','DATE_FORMAT(pl.created_at,"%d-%m-%Y")');
		
		$config['joins'][] = array('join_table' => 'letter_for lf', 'join_by' => 'lf.id = pl.letter_for_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'staff s', 'join_by' => 's.staff_id = pl.person_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'interview i', 'join_by' => 'i.interview_id = pl.person_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'letters l', 'join_by' => 'l.letter_id = pl.letter_topic_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'staff st', 'join_by' => 'st.staff_id = pl.created_by', 'join_type' => 'left');
		
		$config['order'] = array('id' => 'desc');
                
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
                $access_edit  = $this->app_model->have_access_role(HR_ISSUED_LETTER_MENU_ID, "edit");
                $access_delete  = $this->app_model->have_access_role(HR_ISSUED_LETTER_MENU_ID, "delete");
		foreach ($list as $print_letter) {
			$row = array();
			$action = '';
                        if ($access_edit){
                            $action .= '<a href="' . base_url() . 'hr/print_letter/' . $print_letter->id . '" class=" btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                        }
                        if ($access_delete){
                            $action .= ' | <a href="javascript:void(0);" class="btn-danger btn-xs delete_button" data-href="' . base_url('hr/delete/'.$print_letter->id) . '"><i class="fa fa-trash"></i></a>';
                        }
			$action .= ' | <a href="' . base_url('hr/letter_pdf/'. $print_letter->id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
			$action .= ' | <a href="' . base_url('hr/letter_email/'. $print_letter->id) . '" target="_blank" class="btn-primary btn-xs" data-href="#"> Email </a>';

			$row[] = $action;
			$row[] = $print_letter->id;
			$row[] = $print_letter->lf_label;
			if ($print_letter->letter_for_id == 1){
				$row[] = $print_letter->s_name;
			} else if ($print_letter->letter_for_id == 2) {
				$row[] = $print_letter->i_name;
			}
			$row[] = $print_letter->l_letter_topic;
			$row[] = $print_letter->st_name;
			$row[] = date('d-m-Y h:i',strtotime($print_letter->created_at));
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
        /**
	 * @param string $letter_code
	 */
	function download_letter($letter_code = 'noc_letter', $interview_id = 0)
	{
		if ($interview_id != 0) {
			if (in_array($letter_code, array('experience_letter', 'noc_letter',))) {
				$this->db->select('s.*,desig.designation,dept.department,grade.grade');
				$this->db->from('staff s');
				$this->db->join('department dept', 'dept.department_id = s.department_id', 'left');
				$this->db->join('designation desig', 'desig.designation_id = s.designation_id', 'left');
				$this->db->join('grade', 'grade.grade_id = s.grade_id', 'left');
				$this->db->where('s.staff_id', $interview_id);
				$employee_data = $this->db->get()->row(0);
				$html = $this->applib->getLetterTemplate($letter_code);
				$vars = array(
					'{(Employee Name)}' => $employee_data->name,
					'{(Designation)}' => $employee_data->designation,
					'{{Current Date}}' => date('d-m-Y'),
					'{{Date of Joining}}' => date('d F Y', strtotime($employee_data->date_of_joining)),
					'{{Date of relieving}}' => date('d F Y', strtotime($employee_data->date_of_leaving)),
				);
				$html = strtr($html, $vars);
				$this->load->library('m_pdf');
				$param = "'utf-8','A4'";
				$pdf = $this->m_pdf->load($param);
				$margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
				$margin_left = $margin_company[0]->margin_left;
				$margin_right = $margin_company[0]->margin_right;
				$margin_top = $margin_company[0]->margin_top;
				$margin_bottom = $margin_company[0]->margin_bottom;
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				$pdf->SetHTMLFooter('');
				$pdfFilePath = "$letter_code.pdf";
				$pdf->WriteHTML($html);
				$pdf->Output($pdfFilePath, "D");
			} elseif (in_array($letter_code, array('appointment_letter', 'confirmation_letter', 'offer_letter'))) {
				$this->db->select('i.*,desig.designation,dept.department,grade.grade');
				$this->db->from('interview i');
				$this->db->join('department dept', 'dept.department_id = i.department_id', 'left');
				$this->db->join('designation desig', 'desig.designation_id = i.designation_id', 'left');
				$this->db->join('grade', 'grade.grade_id = i.grade_id', 'left');
				$this->db->where('i.interview_id', $interview_id);
				$employee_data = $this->db->get()->row(0);
				$html = $this->applib->getLetterTemplate($letter_code);
				$vars = array(
					'{(Employee Name)}' => $employee_data->name,
					'{(Designation)}' => $employee_data->designation,
					'{{Current Date}}' => date('d-m-Y'),
					'{{Date of Joining}}' => $employee_data->date_of_joining,
					'{{Date of relieving}}' => $employee_data->date_of_leaving,
				);
				$html = strtr($html, $vars);
				$this->load->library('m_pdf');
				$param = "'utf-8','A4'";
				$pdf = $this->m_pdf->load($param);
				$margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
				$margin_left = $margin_company[0]->margin_left;
				$margin_right = $margin_company[0]->margin_right;
				$margin_top = $margin_company[0]->margin_top;
				$margin_bottom = $margin_company[0]->margin_bottom;
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				$pdf->SetHTMLFooter('');
				$pdfFilePath = "$letter_code.pdf";
				$pdf->WriteHTML($html);
				$pdf->Output($pdfFilePath, "D");
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Please try again, Something wrong!');
				redirect('hr/edit_employee/', $interview_id);
				exit();
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please try again, Something wrong!');
			redirect('hr/employee_master/');
			exit();
		}
	}

	/**
	 * Calculate Salary
	 */
	function salary_list(){
		if($this->applib->have_access_role(HR_CALCULATE_SALARY_MENU_ID,"view")) {
			set_page('hr/salary/list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	/**
	 * @param $employee_id
	 */
    function salary_report($salary_id = ''){
        $data = array();
        if (isset($_POST['employee_id']) && !empty($_POST['employee_id'])) {
            $employee_id = $_POST['employee_id'];
        }
        if ($this->app_model->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "manage")) {
            if(!empty($salary_id)){
                $salary_data = $this->crud->get_row_by_id('employee_salary', array('employee_salary_id' => $salary_id));
                $salary_data = $salary_data[0];
                if(!empty($salary_data)){
                    $employee_id = $salary_data->staff_id;
                    $_POST['month'] = $salary_data->month;
                    $_POST['year'] = $salary_data->year;
                } else {
                    redirect(base_url() . 'hr/salary_report');
                }
            }
        } else {
            $employee_id = $this->staff_id;
        }
        if(!empty($salary_id)){
            if ($this->app_model->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "manage")) {} else {
                redirect(base_url() . 'hr/salary_report');
            }
        }
        
		$this->db->select('s.*,desig.designation,dept.department,grade.grade');
		$this->db->from('staff s');
		$this->db->join('department dept', 'dept.department_id = s.department_id', 'left');
		$this->db->join('designation desig', 'desig.designation_id = s.designation_id', 'left');
		$this->db->join('grade', 'grade.grade_id = s.grade_id', 'left');
		$this->db->where('active !=', 0);
		$this->db->where('staff_id', $employee_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$employee_data = $query->row(0);
		if (isset($_POST['month']) && isset($_POST['year'])) {
			$month = $_POST['month'];
			$year = $_POST['year'];
//            echo '<pre>'; print_r($_POST); exit;
//            echo '<pre>'; print_r($year); exit;
			if (strtotime($employee_data->date_of_joining) < strtotime("$year-$month-01")) {
                
                $pre_month = $month - 1;
                $this->db->select('c.challan_date, so.rate AS sales_rate, pi.rate AS profoma_rate, ci.item_id');
                $this->db->from('challans c');
                $this->db->join('challan_items ci', 'ci.challan_id = c.id', 'left');
                $this->db->join('party p', 'p.party_id = c.sales_to_party_id', 'left');
                $this->db->join('sales_order_items so', 'so.id = c.sales_order_item_id', 'left');
                $this->db->join('proforma_invoice_items pi', 'pi.id = c.proforma_invoice_item_id', 'left');
                $this->db->where('p.current_party_staff_id', $employee_id);
                $this->db->where('c.challan_date >=', date('Y-m-d', strtotime("$year-$pre_month-01")));
                $this->db->where('c.challan_date <=', date('Y-m-t', strtotime("$year-$pre_month-01")));
                $this->db->order_by('c.challan_date', 'ASC');
                $query1 = $this->db->get();
                $challan_data = $query1->result();
//                    echo '<pre>'. $this->db->last_query(); exit;
//                    echo '<pre>'; print_r($challan_data); exit;
                $incentive_item_arr = array();
                $incentive_amount = 0;
                $incentive = '';
                if(!empty($challan_data)){
                    foreach ($challan_data as $key => $challan){
                        if($challan->sales_rate != ''){
                            $rate = $challan->sales_rate;
                        } else {
                            $rate = $challan->profoma_rate;
                        }

                        $incentive_amt = 0;
                        $incentive = 0;
//                            
//                            if (in_array($challan->item_id, $item_arr)) {
//                                $count_items = array_count_values($item_arr);
                        if($key == '0'){
                            $incentive = $this->crud->get_column_value_by_id('employee_wise_incentive', 'incentive', array('staff_id' => $employee_id, 'item_id' => $challan->item_id, 'no_of_item' => 1));
                        } else if($key == '1'){
                            $incentive = $this->crud->get_column_value_by_id('employee_wise_incentive', 'incentive', array('staff_id' => $employee_id, 'item_id' => $challan->item_id, 'no_of_item' => 2));
                        } else if($key == '2'){
                            $incentive = $this->crud->get_column_value_by_id('employee_wise_incentive', 'incentive', array('staff_id' => $employee_id, 'item_id' => $challan->item_id, 'no_of_item' => 3));
                        } else if($key >= '3'){
                            $incentive = $this->crud->get_column_value_by_id('employee_wise_incentive', 'incentive', array('staff_id' => $employee_id, 'item_id' => $challan->item_id, 'no_of_item' => 4));
                        } 
//                            } else {
//                                $incentive = $this->crud->get_column_value_by_id('employee_wise_incentive', 'incentive', array('staff_id' => $employee_id, 'item_id' => $challan->item_id, 'no_of_item' => 1));
//                            }
                        if(!empty($incentive)){
                            $incentive_amt = ($rate * $incentive) / 100;

                        }
                        $incentive_amount = $incentive_amount + $incentive_amt;
                        $incentive_item_arr[$key]->item_name = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $challan->item_id));
                        $incentive_item_arr[$key]->incentive_pr_amt = $incentive_amt;
                        $incentive_item_arr[$key]->incentive_pr = isset($incentive) && !empty($incentive) ? number_format($incentive, '2', '.', '') : 'NF';
                        $incentive_item_arr[$key]->item_rate = $rate;
                    } 
                } 
//                echo '<pre>'; print_r($incentive_item_arr); exit;
                $staff_data = $this->crud->get_all_with_where('employee_salary','employee_salary_id','asc',array('staff_id' => $employee_id, 'month' => $month, 'year' => $year));
                if(!empty($staff_data)){
                    $staff_data = $staff_data[0];
                    $staff_data->pf_amount = $staff_data->ctc_pf;
                    $staff_data->esi_amount = $staff_data->esi;
                    $staff_data->incentive_amount = $staff_data->incentive;
                    $staff_data->net_pay = $staff_data->salary - $staff_data->esi_amount - $staff_data->pf_amount - $staff_data->professional_tax + $staff_data->incentive_amount - $staff_data->tds_on_incentive;
                    $employee_data = (object) array_merge((array) $employee_data, (array) $staff_data);
                    $data['only_view_mode'] = 1;
                } else {
                    $data['only_view_mode'] = 0;
                    $leaves_data = $this->applib->employee_month_leaves($employee_id, $month, $year);
                    $start_date = date('Y-m-d', strtotime("$year-$month-01"));
                    $last_day = date('t', strtotime($start_date));
                    $end_date = date('Y-m-d', strtotime("$year-$month-$last_day"));
                    $month_total_leaves = 0;
                    $leaves_dates = array();
                    $week_off = $this->db->where('id', 1)->get('weekly_holiday')->row(0)->day;
                    for ($i = $start_date; $i <= $end_date; $i = date('Y-m-d', strtotime("+1 day", strtotime($i)))) {
                        foreach ($leaves_data as $leave_row) {
                            if (strtolower($week_off) != strtolower(date('l', strtotime($i))) && $leave_row['from_date'] <= $i && $i <= $leave_row['to_date'] && !in_array($i, $leaves_dates)) {
                                $leaves_dates[] = $i;
                                $month_total_leaves++;
                            }
                        }
                    }
                    
                    $tds_per = $this->crud->get_column_value_by_id('company', 'tds_per', array('id' => COMPANY_ID));
                    $employee_data->actual_salary = $employee_data->salary;
                    $employee_data->incentive_amount = $incentive_amount;
                    if(!empty($employee_data->incentive_amount) && $employee_data->incentive_amount >= '10000'){
                        $employee_data->tds_on_incentive = $incentive_amount * $tds_per / 100;
                    } else {
                        $employee_data->tds_on_incentive = '0';
                    }
                    $employee_data->salary = $employee_data->actual_salary - $employee_data->pf_amount;

                    if($employee_data->actual_salary <= 5999){ 
                        $professional_tax = '0';
                    } elseif ($employee_data->actual_salary >= 6000 && $employee_data->actual_salary <= 8999) {
                        $professional_tax = '80';
                    } elseif ($employee_data->actual_salary >= 9000 && $employee_data->actual_salary <= 11999) {
                        $professional_tax = '150';
                    } elseif ($employee_data->actual_salary >= 12000) {
                        $professional_tax = '200';
                    } 
                    $employee_data->professional_tax = $professional_tax;
                    $employee_data->net_pay = $employee_data->salary - $employee_data->esi_amount - $employee_data->pf_amount - $professional_tax + $employee_data->incentive_amount - $employee_data->tds_on_incentive;
                }
//                echo '<pre>';print_r($month);
//                echo '<pre>';print_r($year); exit;
				if($this->applib->have_access_role(HR_CALCULATE_SALARY_MENU_ID,"view")) {
					set_page('hr/salary/salary_report', array('employee_data' => $employee_data, 'employee_id' => $employee_id, 'month' => $month, 'year' => $year, 'month_total_leaves' => $month_total_leaves, 'data' => $data, 'incentive_item_arr' => $incentive_item_arr));
				} else {
					$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
					redirect("/");
				}
			} else {
				$this->session->set_flashdata('success', false);
				$date_of_joining = date('d F Y', strtotime($employee_data->date_of_joining));
				$this->session->set_flashdata('message', "Please select valid month and year, Your joining date is $date_of_joining !");
				redirect(base_url() . 'hr/salary_report');
				exit();
			}
		} else {
			if($this->applib->have_access_role(HR_CALCULATE_SALARY_MENU_ID,"view")) {
				set_page('hr/salary/salary_report', array('employee_data' => $employee_data));
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}

	function print_salary_report($employee_id, $year, $month)
	{
		$this->db->select('s.*,desig.designation,dept.department,grade.grade');
		$this->db->from('staff s');
		$this->db->join('department dept', 'dept.department_id = s.department_id', 'left');
		$this->db->join('designation desig', 'desig.designation_id = s.designation_id', 'left');
		$this->db->join('grade', 'grade.grade_id = s.grade_id', 'left');
		$this->db->where('s.active !=', 0);
		$this->db->where('staff_id', $employee_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$employee_data = $query->row(0);
		$leaves_data = $this->applib->employee_month_leaves($employee_id, $month, $year);
		$start_date = date('Y-m-d', strtotime("$year-$month-01"));
		$last_day = date('t', strtotime($start_date));
		$end_date = date('Y-m-d', strtotime("$year-$month-$last_day"));
		$month_total_leaves = 0;
		$leaves_dates = array();
		$week_off = $this->db->where('id', 1)->get('weekly_holiday')->row(0)->day;
		for ($i = $start_date; $i <= $end_date; $i = date('Y-m-d', strtotime("+1 day", strtotime($i)))) {
			foreach ($leaves_data as $leave_row) {
				if (strtolower($week_off) != strtolower(date('l', strtotime($i))) && $leave_row['from_date'] <= $i && $i <= $leave_row['to_date'] && !in_array($i, $leaves_dates)) {
					$leaves_dates[] = $i;
					$month_total_leaves++;
				}
			}
		}
//        if (strtotime($employee_data->date_of_joining) < strtotime("$year-$month-01")) {
            $staff_data = $this->crud->get_all_with_where('employee_salary','employee_salary_id','asc',array('staff_id' => $employee_id, 'month' => $month, 'year' => $year));
//            if(!empty($staff_data)){
                $staff_data = $staff_data[0];
                $staff_data->pf_amount = $staff_data->ctc_pf;
                $staff_data->esi_amount = $staff_data->esi;
                $staff_data->incentive_amount = $staff_data->incentive;
                $employee_data = (object) array_merge((array) $employee_data, (array) $staff_data);
                
                $employee_data->other = $employee_data->other_deduction + $employee_data->late_coming_charge;
                $employee_data->incentive_after_tds = $employee_data->incentive_amount - $employee_data->tds_on_incentive;
                $employee_data->basic_pay = !empty($employee_data->salary) ? $employee_data->salary : '0';
                $employee_data->hra = !empty($employee_data->hra) ? $employee_data->hra : '0';
                $employee_data->conveyance = !empty($employee_data->conveyance) ? $employee_data->conveyance : '0';
                $employee_data->mediclaim = !empty($employee_data->mediclaim) ? $employee_data->mediclaim : '0';
                $employee_data->incentive_after_tds = !empty($employee_data->incentive_after_tds) ? $employee_data->incentive_after_tds : '0';
                $employee_data->allow_pf = !empty($staff_data->allow_pf) ? $staff_data->allow_pf : NULL;
                $employee_data->allow_esi = !empty($staff_data->allow_esi) ? $staff_data->allow_esi : NULL;
                $employee_data->pf_per = !empty($staff_data->pf_per) ? $staff_data->pf_per : '0';
                $employee_data->pf_amount = !empty($employee_data->pf_amount) ? $employee_data->pf_amount : '0';
                $employee_data->esi_per = !empty($staff_data->esi_per) ? $staff_data->esi_per : '0';
                $employee_data->esi_amount = !empty($employee_data->esi_amount) ? $employee_data->esi_amount : '0';
                $employee_data->professional_tax = !empty($employee_data->professional_tax) ? $employee_data->professional_tax : '0';
                $employee_data->other = !empty($employee_data->other) ? $employee_data->other : '0';
                
                $employee_data->left_total = $employee_data->basic_pay + $employee_data->hra + $employee_data->conveyance + $employee_data->mediclaim + $employee_data->incentive_after_tds;
                $employee_data->right_total = $employee_data->pf_amount + $employee_data->esi_amount + $employee_data->professional_tax + $employee_data->other;
                $employee_data->final_total = $employee_data->left_total - $employee_data->right_total;
//            }
//        }
//        echo '<pre>'; print_r($employee_data); exit;
        $employee_data->pay_mode = $this->crud->get_id_by_val('payment_by', 'name', 'id', $employee_data->pay_mode);
        $margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
        $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
		$salary_report_html = $this->load->view('hr/salary/salary_report_print', array('employee_data' => $employee_data, 'month' => $month, 'year' => $year, 'month_total_leaves' => $month_total_leaves, 'company_pf_no' => $margin_company[0]->pf_no, 'letterpad_details' => $letterpad_details), true);
		$this->load->library('m_pdf');
		
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		$pdf = new mPDF();
		$pdf->allow_charset_conversion = true;
		$pdf->charset_in = 'UTF-8';
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			'0', //margin-left
			'0', //margin-right
			$margin_top, //margin-top
			'0', //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$pdf->WriteHTML($salary_report_html);
//		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
		$filename = date('FY', strtotime("$year-$month-01"));
		$pdf->Output("SalaryReport_$filename", 'I');
	}
    
    function salary_list_datatable(){
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
        $config['table'] = 'employee_salary es';
        $config['select'] = 'es.*,s.name';
		$config['column_search'] = array('s.name', 'es.month', 'es.salary', 'es.esi', 'es.pf', 'es.professional_tax', 'es.other_deduction', 'es.hra', 'es.conveyance', 'es.mediclaim', 'es.incentive', 'es.net_pay');
		$config['column_order'] = array('s.name', 'es.month', 'es.salary', 'es.esi', 'es.pf', 'es.professional_tax', 'es.other_deduction', 'es.hra', 'es.conveyance', 'es.mediclaim', 'es.incentive', 'es.net_pay');
		$config['joins'][] = array('join_table'=>'staff s','join_by'=>'s.staff_id = es.staff_id','join_type'=>'left');
        if(isset($post_data['staff_id']) && !empty($post_data['staff_id'])){
            $config['wheres'][] = array('column_name' => 'es.staff_id', 'column_value' => $post_data['staff_id']);
        }
        if(isset($post_data['month']) && !empty($post_data['month'])){
            $config['wheres'][] = array('column_name' => 'es.month', 'column_value' => $post_data['month']);
        }
        if(isset($post_data['year']) && !empty($post_data['year'])){
            $config['wheres'][] = array('column_name' => 'es.year', 'column_value' => $post_data['year']);
        }
        $config['order'] = array('employee_salary_id' => 'DESC');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $access_edit_delete  = $this->app_model->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "manage");
//        echo $this->db->last_query(); exit;
        foreach ($list as $salary) {
                $row = array();
                $action = '';
                if($access_edit_delete){
                    $action .= '<a href="' . base_url() . 'hr/salary_report/' . $salary->employee_salary_id . '" class=" btn-primary btn-xs"><i class="fa fa-edit"></i></a> | ';
                    $action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('hr/delete_salary/' . $salary->employee_salary_id) . '" Title="Delete"><i class="fa fa-trash"></i></a>';
                }
                $action .= ' | <a href="' . base_url() . 'hr/print_salary_report/' . $salary->staff_id . '/'. $salary->year . '/' .$salary->month.'" target="_blank" class=" btn-primary btn-xs"><i class="fa fa-print"></i></a>';
                $row[] = $action;
                $row[] = $salary->name;
                $row[] = date('F', mktime(0, 0, 0, $salary->month)).' - '.$salary->year;
                $row[] = number_format($salary->salary, '2', '.', '');
                $row[] = number_format($salary->esi, '2', '.', '');
                $row[] = number_format($salary->pf, '2', '.', '');
                $row[] = number_format($salary->professional_tax, '2', '.', '');
                $row[] = number_format($salary->other_deduction + $salary->late_coming_charge, '2', '.', '');
                $row[] = number_format($salary->hra, '2', '.', '');
                $row[] = number_format($salary->conveyance, '2', '.', '');
                $row[] = number_format($salary->mediclaim, '2', '.', '');
                $row[] = number_format($salary->incentive - $salary->tds_on_incentive, '2', '.', '');
                $row[] = number_format($salary->net_pay, '2', '.', '');
                $data[] = $row;
        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->datatable->count_all(),
                "recordsFiltered" => $this->datatable->count_filtered(),
                "data" => $data,
        );
        echo json_encode($output);
    }
    
    function delete_salary($id){
        $this->crud->delete('employee_salary',array('employee_salary_id' => $id));
    }

	function email_salary_report($employee_id, $year, $month)
	{
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->where('staff_id', $employee_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$employee_data = $query->row(0);
		$leaves_data = $this->applib->employee_month_leaves($employee_id, $month, $year);
		$start_date = date('Y-m-d', strtotime("$year-$month-01"));
		$last_day = date('t', strtotime($start_date));
		$end_date = date('Y-m-d', strtotime("$year-$month-$last_day"));
		$month_total_leaves = 0;
		$leaves_dates = array();
		$week_off = $this->db->where('id', 1)->get('weekly_holiday')->row(0)->day;
		for ($i = $start_date; $i <= $end_date; $i = date('Y-m-d', strtotime("+1 day", strtotime($i)))) {
			foreach ($leaves_data as $leave_row) {
				if (strtolower($week_off) != strtolower(date('l', strtotime($i))) && $leave_row['from_date'] <= $i && $i <= $leave_row['to_date'] && !in_array($i, $leaves_dates)) {
					$leaves_dates[] = $i;
					$month_total_leaves++;
				}
			}
		}
		$salary_report_html = $this->load->view('hr/salary/salary_report_print', array('employee_data' => $employee_data, 'month' => $month, 'year' => $year, 'month_total_leaves' => $month_total_leaves), true);
		$margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		$this->load->library('m_pdf');
		$pdf = new mPDF();
		$pdf->allow_charset_conversion = true;
		$pdf->charset_in = 'UTF-8';
		$pdf->AddPage(
			'P', //orientation
			'', //type
			'', //resetpagenum
			'', //pagenumstyle
			'', //suppress
			$margin_left, //margin-left
			$margin_right, //margin-right
			$margin_top, //margin-top
			$margin_bottom, //margin-bottom
			0, //margin-header
			0 //margin-footer
		);
		$pdf->WriteHTML($salary_report_html);
		$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
		$filename = date('FY', strtotime("$year-$month-01"));
		$upload_folder_path = "./uploads/";
		$pdf_url = $upload_folder_path . "SalaryReport-$filename" . ".pdf";
		$pdf->Output($pdf_url, 'F');
		$this->db->where('staff_id', $employee_id);
		$this->db->update('staff', array('pdf_url' => $pdf_url));
		redirect(base_url() . 'mail-system3/document-mail/salary-report/' . $employee_id);
	}

	function update_total_leave()
	{

		$total_sl = isset($_POST['total_sl']) ? trim($_POST['total_sl']) : 0;
		$total_cl = isset($_POST['total_cl']) ? trim($_POST['total_cl']) : 0;
		$total_el_pl = isset($_POST['total_el_pl']) ? trim($_POST['total_el_pl']) : 0;
		$query = $this->db->get('leave_for_employee');
		if ($query->num_rows() > 0) {
			$this->db->update('leave_for_employee', array('total_sl' => $total_sl, 'total_cl' => $total_cl, 'total_el_pl' => $total_el_pl));
		} else {
			$this->db->insert('leave_for_employee', array('total_sl' => $total_sl, 'total_cl' => $total_cl, 'total_el_pl' => $total_el_pl));
		}
		echo json_encode(array("status" => 1));
	}

	function leave()
	{
		if (!empty($_POST)) {
			$month = array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
			$filter_employee_id = $_POST['employee_id'];
			$filter_month = $_POST['month'];
			$filter_year = $_POST['year'];
			$data = array();
			$recordsFiltered = 1;
			$leave_dates = array();

			$this->db->select('*');
			$this->db->from('real_leave');
			$this->db->where('employee_id', $filter_employee_id);
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$leave_dates[] = array(
						'from_date' => $row->from_date,
						'to_date' => $row->to_date,
						'leave_type' => $row->leave_type,
					);
				}
			}
			$week_off = $this->db->where('id', 1)->get('weekly_holiday')->row(0)->day;
			for ($i = 1; $i < 13; $i++) {
				if ($filter_month == 'all' || $filter_month == $i) {
					$row = array();
					$row[] = $month[$i];
					for ($j = 1; $j < 32; $j++) {
						$month_index = str_pad($i, 2, '0', STR_PAD_LEFT);
						$day_index = str_pad($j, 2, '0', STR_PAD_LEFT);
						if (checkdate($month_index, $day_index, $filter_year)) {
							if (strtolower($week_off) == strtolower(date('l', strtotime("$filter_year-$month_index-$day_index")))) {
								$row[] = 'WH';
							} elseif (!empty($leave_dates)) {
								$leave_status = 'P';
								foreach ($leave_dates as $leave_dates_row) {
									if ($leave_dates_row['from_date'] <= "$filter_year-$month_index-$day_index" && "$filter_year-$month_index-$day_index" <= $leave_dates_row['to_date']) {
										$leave_status = "<span class='text-danger'>" . $leave_dates_row['leave_type'] . "</span>";
										break;
									}
								}
								$row[] = $leave_status;
							} else {
								$row[] = 'P';;
							}
						} else {
							$row[] = '-';
						}
					}
					$data[] = $row;
					$recordsFiltered++;
				}
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => 12,
				"recordsFiltered" => $recordsFiltered - 1,
				"data" => $data,
			);
			echo json_encode($output);
		} else {
			if($this->applib->have_access_role(HR_LEAVE_MENU_ID,"view")) {
				set_page('hr/leave_list/leave');
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}

	function add_real_leave()
	{
		if (!empty($_POST)) {
			$real_leave_data = array(
				'employee_id' => $_POST['employee_id'],
				'from_date' => date('Y-m-d', strtotime($_POST['from_date'])),
				'to_date' => date('Y-m-d', strtotime($_POST['to_date'])),
				'leave_type' => $_POST['leave_type'],
			);
			if ($this->crud->insert('real_leave', $real_leave_data)) {
				echo json_encode(array('status' => true, 'msg' => "Leave added successfully!"));
				exit();
			} else {
				echo json_encode(array('status' => false, 'msg' => "Something wrong!"));
				exit();
			}
		} else {
			echo json_encode(array('status' => false, 'msg' => "Something wrong!"));
			exit();
		}
	}
    
    function  save_salary_data(){
        $return = array();
        $post_data = $this->input->post();
        $post_data['hra'] = isset($post_data['hra']) && !empty($post_data['hra']) ? $post_data['hra'] : NULL;
        $post_data['conveyance'] = isset($post_data['conveyance']) && !empty($post_data['conveyance']) ? $post_data['conveyance'] : NULL;
        $post_data['mediclaim'] = isset($post_data['mediclaim']) && !empty($post_data['mediclaim']) ? $post_data['mediclaim'] : NULL;
        $post_data['other_deduction'] = isset($post_data['other_deduction']) && !empty($post_data['other_deduction']) ? $post_data['other_deduction'] : NULL;
        $post_data['late_coming_charge'] = isset($post_data['late_coming_charge']) && !empty($post_data['late_coming_charge']) ? $post_data['late_coming_charge'] : NULL;
        $post_data['allow_pf'] = isset($post_data['allow_pf']) && !empty($post_data['allow_pf']) ? $post_data['allow_pf'] : NULL;
        $post_data['allow_esi'] = isset($post_data['allow_esi']) && !empty($post_data['allow_esi']) ? $post_data['allow_esi'] : NULL;
        $post_data['pf'] = isset($post_data['pf']) && !empty($post_data['pf']) ? $post_data['pf'] : NULL;
        $post_data['pf_per'] = isset($post_data['pf_per']) && !empty($post_data['pf_per']) ? $post_data['pf_per'] : NULL;
        $post_data['esi'] = isset($post_data['esi']) && !empty($post_data['esi']) ? $post_data['esi'] : NULL;
        $post_data['esi_per'] = isset($post_data['esi_per']) && !empty($post_data['esi_per']) ? $post_data['esi_per'] : NULL;
        $post_data['actual_salary'] = isset($post_data['actual_salary']) && !empty($post_data['actual_salary']) ? $post_data['actual_salary'] : NULL;
        $post_data['basic_salary'] = isset($post_data['basic_salary']) && !empty($post_data['basic_salary']) ? $post_data['basic_salary'] : NULL;
        $post_data['ctc_pf'] = isset($post_data['ctc_pf']) && !empty($post_data['ctc_pf']) ? $post_data['ctc_pf'] : NULL;
        $post_data['salary'] = isset($post_data['salary']) && !empty($post_data['salary']) ? $post_data['salary'] : NULL;
        $post_data['professional_tax'] = isset($post_data['professional_tax']) && !empty($post_data['professional_tax']) ? $post_data['professional_tax'] : NULL;
        $post_data['incentive'] = isset($post_data['incentive']) && !empty($post_data['incentive']) ? $post_data['incentive'] : NULL;
        $post_data['tds_on_incentive'] = isset($post_data['tds_on_incentive']) && !empty($post_data['tds_on_incentive']) ? $post_data['tds_on_incentive'] : NULL;
        $post_data['net_pay'] = isset($post_data['net_pay']) && !empty($post_data['net_pay']) ? $post_data['net_pay'] : NULL;
        $post_data['pay_mode'] = isset($post_data['pay_mode']) && !empty($post_data['pay_mode']) ? $post_data['pay_mode'] : NULL;
        if(isset($post_data['employee_salary_id']) && !empty($post_data['employee_salary_id'])){
            $post_data['updated_at'] = $this->now_time;
            $post_data['updated_by'] = $this->staff_id;
            $result = $this->crud->update('employee_salary',$post_data, array('employee_salary_id' => $post_data['employee_salary_id']));
        } else {
            $post_data['created_at'] = $this->now_time;
            $post_data['updated_at'] = $this->now_time;
            $post_data['created_by'] = $this->staff_id;
            $post_data['updated_by'] = $this->staff_id;
    //        echo '<pre>'; print_r($post_data); exit;
            $result = $this->crud->insert('employee_salary',$post_data);
        }
        if($result){
            $return['success'] = "Added";
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Salary Calculated');
        }
        print json_encode($return);
        exit;
    }
}
