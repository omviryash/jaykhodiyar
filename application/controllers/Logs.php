<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 */
class Logs extends CI_Controller
{
	private $staff_id = 1;
	function __construct()
	{
		parent::__construct();
		$this->db_b = $this->load->database('log', TRUE);
		$this->load->helper('file');
		
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		if(!$this->applib->have_access_role(LOGS_MENU,"view")) {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect('');
		}
		$this->staff_id = trim($this->session->userdata('is_logged_in')['staff_id']);
	}

	function index(){
		if ($this->applib->have_access_role(LOGS_MENU,"view")) {
			set_page('logs/log_list');
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect('');
		}
	}
	
	function log_dt(){
		$config['table'] = 'INFORMATION_SCHEMA.TABLES t';
		$config['select'] = 't.TABLE_NAME';
		$config['column_order'] = array(null,'t.TABLE_NAME');
		$config['column_search'] = array('t.TABLE_NAME');
		$config['order'] = array('t.TABLE_NAME' => 'desc');
		$config['wheres'] = array();
		$config['wheres'][] = array('column_name' => 't.TABLE_SCHEMA', 'column_value' => 'jaykhodiyar_logs');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$no = 1;
		foreach ($list as $agents) {
			$row = array();
			$field = str_replace('_', ' ', $agents->TABLE_NAME);
			$link = str_replace('_log', ' ', $agents->TABLE_NAME);
			$row[] = $no;
			if($agents->TABLE_NAME == 'proforma_invoice_logins_log'){
				$row[] = '<a href="' . base_url() . 'logs/proforma_invoice_logins" target="_blank" class="" data-href="">'.ucwords('proforma invoice logins').'</a>';
			}elseif($agents->TABLE_NAME == 'sales_order_logins_log'){
				$row[] = '<a href="' . base_url() . 'logs/sales_order_logins" target="_blank" class="" data-href="">'.ucwords('sales order logins').'</a>';
			}elseif($agents->TABLE_NAME == 'invoice_logins_log'){
				$row[] = '<a href="' . base_url() . 'logs/invoice_logins" target="_blank" class="" data-href="">'.ucwords('invoice logins').'</a>';
			}else{
				$row[] = '<a href="' . base_url() . 'logs/'.$link.'" target="_blank" class="" data-href="">'.ucwords($field).'</a>';
			}
			
			$data[] = $row;
			$no++;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	/*function agent_datatable(){
		$config['table'] = 'agent_log a';
		$config['select'] = 'a.*, p.type,s.name';
		$config['column_order'] = array('');
		$config['column_order'] = array(null, 'a.operation_type');
		//$config['column_order'] = array(null, 'a.agent_name','a.company_name','a.address','a.phone_no','a.email' ,'a.note','p.type' ,'a.fix_commission','a.commission','s.name');
		//$config['column_search'] = array('a.agent_name','a.company_name','a.address','a.phone_no','a.email' ,'a.note','p.type' ,'a.fix_commission','a.commission','s.name');
		$config['joins'][] = array('join_table' => 'party_type_1_log p', 'join_by' => 'p.id = a.party_type_1', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'staff_log s', 'join_by' => 's.staff_id = a.created_by', 'join_type' => 'left');
		$config['order'] = array('a.updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $agents) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $agents->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;
			$row[] = $agents->operation_type;
			$row[] = $agents->id;
			$row[] = $agents->type;
			$row[] = $agents->address;
			$row[] = $agents->company_name;
			$row[] = $agents->phone_no;
			$row[] = $agents->email;
			$row[] = $agents->note;
			$row[] = $agents->fix_commission;
			$row[] = $agents->agent_name;
			$row[] = $agents->commission;
			$row[] = $agents->name;
			$row[] = $agents->created_at;
			$row[] = $agents->updated_at;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function billing_template(){
		$data['fields'] = $this->get_field('billing_template_log');
		set_page('logs/billing_template',$data);
	}

	function billing_template_datatable(){
		$config['table'] = 'billing_template_log';
		$config['select'] = '*';
		$config['column_order'] = array('');
		$config['column_order'] = array(null, 'operation_type');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;

			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->template;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
*/
	function agent(){
		$data['fields'] = $this->get_field('agent_log');
		set_page('logs/agent',$data);
	}

	function agent_datatable(){
		$config['table'] = 'agent_log a';
		$config['select'] = 'a.*, p.type,s.name';
		$config['column_order'] = array(null, 'a.operation_type','a.id','p.type','a.address','a.company_name','a.phone_no','a.email' ,'a.fix_commission','a.agent_name','a.commission','a.updated_at','a.updated_at');
		$config['column_search'] = array('a.operation_type','a.id','p.type','a.address','a.company_name','a.phone_no','a.email' ,'a.fix_commission','a.agent_name','a.commission','a.updated_at','a.updated_at');
		$config['joins'][] = array('join_table' => 'staff_log s', 'join_by' => 's.staff_id = a.created_by', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'party_type_1_log p', 'join_by' => 'p.id = a.party_type_1', 'join_type' => 'left');
		$config['order'] = array('a.updated_at' => 'desc');
		$config['group_by'] = 'a.log_id';
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $agents) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $agents->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;
			$row[] = $agents->operation_type;
			$row[] = $agents->id;
			$row[] = $agents->type;
			$row[] = $agents->address;
			$row[] = $agents->company_name;
			$row[] = $agents->phone_no;
			$row[] = $agents->email;
			$row[] = $agents->note;
			$row[] = $agents->fix_commission;
			$row[] = $agents->agent_name;
			$row[] = $agents->commission;
			$row[] = $agents->name;
			$row[] = $agents->created_at;
			$row[] = $agents->updated_at;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function billing_template(){
		$data['fields'] = $this->get_field('billing_template_log');
		set_page('logs/billing_template',$data);
	}

	function billing_template_datatable(){
		$config['table'] = 'billing_template_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'template', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->template;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function billing_terms_detail(){
		$data['fields'] = $this->get_field('billing_terms_detail_log');
		set_page('logs/billing_terms_detail',$data);
	}

	function billing_terms_detail_datatable(){
		$config['table'] = 'billing_terms_detail_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'billing_terms_id', 'cal_code', 'narration', 'cal_definition', 'percentage', 'value', 'gl_acc', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;
			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->billing_terms_id;
			$row[] = $tbl_row->cal_code;
			$row[] = $tbl_row->narration;
			$row[] = $tbl_row->cal_definition;
			$row[] = $tbl_row->percentage;
			$row[] = $tbl_row->value;
			$row[] = $tbl_row->gl_acc;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function billing_terms(){
		$data['fields'] = $this->get_field('billing_terms_log');
		set_page('logs/billing_terms',$data);
	}

	function billing_terms_datatable(){
		$config['table'] = 'billing_terms_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'billing_terms_name', 'billing_terms_date', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->billing_terms_name;
			$row[] = $tbl_row->billing_terms_date;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function branch(){
		$data['fields'] = $this->get_field('branch_log');
		set_page('logs/branch',$data);
	}

	function branch_datatable(){
		$config['table'] = 'branch_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'branch_id', 'branch_code', 'branch', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->branch_id;
			$row[] = $tbl_row->branch_code;
			$row[] = $tbl_row->branch;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function cal_code_definition(){
		$data['fields'] = $this->get_field('cal_code_definition_log');
		set_page('logs/cal_code_definition',$data);
	}

	function cal_code_definition_datatable(){
		$config['table'] = 'cal_code_definition_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'billing_terms_detail_id', 'is_first_element', 'cal_operation', 'cal_code_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->billing_terms_detail_id;
			$row[] = $tbl_row->is_first_element;
			$row[] = $tbl_row->cal_operation;
			$row[] = $tbl_row->cal_code_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function challan_items(){
		$data['fields'] = $this->get_field('challan_items_log');
		set_page('logs/challan_items',$data);
	}

	function challan_items_datatable(){
		$config['table'] = 'challan_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_id', 'challan_id', 'item_code', 'item_category_id', 'item_name', 'item_desc', 'item_description', 'add_description', 'detail_description', 'drawing_number', 'drawing_revision', 'uom_id', 'uom', 'total_amount', 'item_note', 'quantity', 'disc_per', 'rate', 'disc_value', 'amount', 'net_amount', 'item_status_id', 'item_status', 'cust_part_no', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->challan_id;
			$row[] = $tbl_row->item_code;
			$row[] = $tbl_row->item_category_id;
			$row[] = $tbl_row->item_name;
			$row[] = $tbl_row->item_desc;
			$row[] = $tbl_row->item_description;
			$row[] = $tbl_row->add_description;
			$row[] = $tbl_row->detail_description;
			$row[] = $tbl_row->drawing_number;
			$row[] = $tbl_row->drawing_revision;
			$row[] = $tbl_row->uom_id;
			$row[] = $tbl_row->uom;
			$row[] = $tbl_row->total_amount;
			$row[] = $tbl_row->item_note;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->disc_per;
			$row[] = $tbl_row->rate;
			$row[] = $tbl_row->disc_value;
			$row[] = $tbl_row->amount;
			$row[] = $tbl_row->net_amount;
			$row[] = $tbl_row->item_status_id;
			$row[] = $tbl_row->item_status;
			$row[] = $tbl_row->cust_part_no;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function challans(){
		$data['fields'] = $this->get_field('challans_log');
		set_page('logs/challans',$data);
	}

	function challans_datatable(){
		$config['table'] = 'challans_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_id', 'sales_to_party_id', 'truck_no', 'gear_no_5', 'gear_no_3_54', 'lr_no', 'lr_date', 'trademark_name', 'full_load_current', 'serial_no', 'serial_no_1', 'serial_no_2', 'serial_no_3', 'serial_no_4', 'motor_75_hp', 'motor_50_hp', 'rated_voltage', 'phases_no', 'short_circuit_rating', 'capacity', 'operating_manual_reference_no', 'gross_weight', 'manufacturing_monthYear', 'challan_date', 'transport_name', 'transport_charges', 'created_by', 'created_at', 'updated_by', 'rounding', 'challan_no', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->sales_to_party_id;
			$row[] = $tbl_row->truck_no;
			$row[] = $tbl_row->gear_no_5;
			$row[] = $tbl_row->gear_no_3_54;
			$row[] = $tbl_row->lr_no;
			$row[] = $tbl_row->lr_date;
			$row[] = $tbl_row->trademark_name;
			$row[] = $tbl_row->full_load_current;
			$row[] = $tbl_row->serial_no;
			$row[] = $tbl_row->serial_no_1;
			$row[] = $tbl_row->serial_no_2;
			$row[] = $tbl_row->serial_no_3;
			$row[] = $tbl_row->serial_no_4;
			$row[] = $tbl_row->motor_75_hp;
			$row[] = $tbl_row->motor_50_hp;
			$row[] = $tbl_row->rated_voltage;
			$row[] = $tbl_row->phases_no;
			$row[] = $tbl_row->short_circuit_rating;
			$row[] = $tbl_row->capacity;
			$row[] = $tbl_row->operating_manual_reference_no;
			$row[] = $tbl_row->gross_weight;
			$row[] = $tbl_row->manufacturing_monthYear;
			$row[] = $tbl_row->challan_date;
			$row[] = $tbl_row->transport_name;
			$row[] = $tbl_row->transport_charges;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = $tbl_row->updated_by;
			$row[] = $tbl_row->rounding;
			$row[] = $tbl_row->challan_no;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function chat_roles(){
		$data['fields'] = $this->get_field('chat_roles_log');
		set_page('logs/chat_roles',$data);
	}

	function chat_roles_datatable(){
		$config['table'] = 'chat_roles_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'staff_id', 'allowed_staff_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->staff_id;
			$row[] = $tbl_row->allowed_staff_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function city(){
		$data['fields'] = $this->get_field('city_log');
		set_page('logs/city',$data);
	}

	function city_datatable(){
		$config['table'] = 'city_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'city_id', 'state_id', 'country_id', 'city', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->city_id;
			$row[] = $tbl_row->state_id;
			$row[] = $tbl_row->country_id;
			$row[] = $tbl_row->city;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function company(){
		$data['fields'] = $this->get_field('company_log');
		set_page('logs/company',$data);
	}

	function company_datatable(){
		$config['table'] = 'company_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'name', 'ceo_name', 'm_d_name', 'address', 'city', 'state', 'pincode', 'country', 'contact_no', 'cell_no', 'fax_no', 'email_id', 'tin_no', 'pan_no', 'tan_no', 'pf_no', 'esic_no', 'pt_no', 'service_tax_no', 'vat_no', 'cst_no', 'code', 'bank_domestic', 'bank_export', 'margin_left', 'margin_right', 'margin_top', 'margin_bottom', 'invoice_terms', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->name;
			$row[] = $tbl_row->ceo_name;
			$row[] = $tbl_row->m_d_name;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->city;
			$row[] = $tbl_row->state;
			$row[] = $tbl_row->pincode;
			$row[] = $tbl_row->country;
			$row[] = $tbl_row->contact_no;
			$row[] = $tbl_row->cell_no;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->tin_no;
			$row[] = $tbl_row->pan_no;
			$row[] = $tbl_row->tan_no;
			$row[] = $tbl_row->pf_no;
			$row[] = $tbl_row->esic_no;
			$row[] = $tbl_row->pt_no;
			$row[] = $tbl_row->service_tax_no;
			$row[] = $tbl_row->vat_no;
			$row[] = $tbl_row->cst_no;
			$row[] = $tbl_row->code;
			$row[] = $tbl_row->bank_domestic;
			$row[] = $tbl_row->bank_export;
			$row[] = $tbl_row->margin_left;
			$row[] = $tbl_row->margin_right;
			$row[] = $tbl_row->margin_top;
			$row[] = $tbl_row->margin_bottom;
			$row[] = $tbl_row->invoice_terms;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function config(){
		$data['fields'] = $this->get_field('config_log');
		set_page('logs/config',$data);
	}

	function config_datatable(){
		$config['table'] = 'config_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'config_key', 'config_value', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->config_key;
			$row[] = $tbl_row->config_value;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function contact_person(){
		$data['fields'] = $this->get_field('contact_person_log');
		set_page('logs/contact_person',$data);
	}

	function contact_person_datatable(){
		$config['table'] = 'contact_person_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'contact_person_id', 'party_id', 'active', 'name', 'phone_no', 'mobile_no', 'fax_no', 'email', 'designation_id', 'department_id', 'note', 'priority', 'created_by', 'updated_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->contact_person_id;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->active;
			$row[] = $tbl_row->name;
			$row[] = $tbl_row->phone_no;
			$row[] = $tbl_row->mobile_no;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->email;
			$row[] = $tbl_row->designation_id;
			$row[] = $tbl_row->department_id;
			$row[] = $tbl_row->note;
			$row[] = $tbl_row->priority;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function country(){
		$data['fields'] = $this->get_field('country_log');
		set_page('logs/country',$data);
	}

	function country_datatable(){
		$config['table'] = 'country_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'country_id', 'country', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->country_id;
			$row[] = $tbl_row->country;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function currency(){
		$data['fields'] = $this->get_field('currency_log');
		set_page('logs/currency',$data);
	}

	function currency_datatable(){
		$config['table'] = 'currency_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'currency', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->currency;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function daily_work_entry(){
		$data['fields'] = $this->get_field('daily_work_entry_log');
		set_page('logs/daily_work_entry',$data);
	}

	function daily_work_entry_datatable(){
		$config['table'] = 'daily_work_entry_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'userid', 'username', 'entry_date', 'activity_details', 'created_date', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->userid;
			$row[] = $tbl_row->username;
			$row[] = $tbl_row->entry_date;
			$row[] = $tbl_row->activity_details;
			$row[] = $tbl_row->created_date;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function days(){
		$data['fields'] = $this->get_field('days_log');
		set_page('logs/days',$data);
	}

	function days_datatable(){
		$config['table'] = 'days_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'day_id', 'day_name', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->day_id;
			$row[] = $tbl_row->day_name;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function default_terms_condition(){
		$data['fields'] = $this->get_field('default_terms_condition_log');
		set_page('logs/default_terms_condition',$data);
	}

	function default_terms_condition_datatable(){
		$config['table'] = 'default_terms_condition_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'template_name', 'group_name', 'terms_condition', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->template_name;
			$row[] = $tbl_row->group_name;
			$row[] = $tbl_row->terms_condition;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function department(){
		$data['fields'] = $this->get_field('department_log');
		set_page('logs/department',$data);
	}

	function department_datatable(){
		$config['table'] = 'department_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'department_id', 'department', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->department_id;
			$row[] = $tbl_row->department;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function designation(){
		$data['fields'] = $this->get_field('designation_log');
		set_page('logs/designation',$data);
	}

	function designation_datatable(){
		$config['table'] = 'designation_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'designation_id', 'designation', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->designation_id;
			$row[] = $tbl_row->designation;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function dispatch_followup_history(){
		$data['fields'] = $this->get_field('dispatch_followup_history_log');
		set_page('logs/dispatch_followup_history',$data);
	}

	function dispatch_followup_history_datatable(){
		$config['table'] = 'dispatch_followup_history_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'challan_id', 'followup_date', 'history', 'followup_by', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->challan_id;
			$row[] = $tbl_row->followup_date;
			$row[] = $tbl_row->history;
			$row[] = $tbl_row->followup_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function drawing_number(){
		$data['fields'] = $this->get_field('drawing_number_log');
		set_page('logs/drawing_number',$data);
	}

	function drawing_number_datatable(){
		$config['table'] = 'drawing_number_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'drawing_number', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->drawing_number;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function employee_leaves(){
		$data['fields'] = $this->get_field('employee_leaves_log');
		set_page('logs/employee_leaves',$data);
	}

	function employee_leaves_datatable(){
		$config['table'] = 'employee_leaves_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'leave_id', 'employee_id', 'from_date', 'to_date', 'day', 'leave_type', 'leave_detail', 'leave_status', 'leave_note', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->leave_id;
			$row[] = $tbl_row->employee_id;
			$row[] = $tbl_row->from_date;
			$row[] = $tbl_row->to_date;
			$row[] = $tbl_row->day;
			$row[] = $tbl_row->leave_type;
			$row[] = $tbl_row->leave_detail;
			$row[] = $tbl_row->leave_status;
			$row[] = $tbl_row->leave_note;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function erection_commissioning(){
		$data['fields'] = $this->get_field('erection_commissioning_log');
		set_page('logs/erection_commissioning',$data);
	}

	function erection_commissioning_datatable(){
		$config['table'] = 'erection_commissioning_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'erection_commissioning', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->erection_commissioning;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function excise(){
		$data['fields'] = $this->get_field('excise_log');
		set_page('logs/excise',$data);
	}

	function excise_datatable(){
		$config['table'] = 'excise_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'description', 'sub_heading', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->description;
			$row[] = $tbl_row->sub_heading;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function followup_history(){
		$data['fields'] = $this->get_field('followup_history_log');
		set_page('logs/followup_history',$data);
	}

	function followup_history_datatable(){
		$config['table'] = 'followup_history_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'quotation_id', 'followup_date', 'history', 'followup_by', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->quotation_id;
			$row[] = $tbl_row->followup_date;
			$row[] = $tbl_row->history;
			$row[] = $tbl_row->followup_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function followup_order_history(){
		$data['fields'] = $this->get_field('followup_order_history_log');
		set_page('logs/followup_order_history',$data);
	}

	function followup_order_history_datatable(){
		$config['table'] = 'followup_order_history_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'order_id', 'followup_date', 'history', 'followup_by', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->order_id;
			$row[] = $tbl_row->followup_date;
			$row[] = $tbl_row->history;
			$row[] = $tbl_row->followup_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function for_required(){
		$data['fields'] = $this->get_field('for_required_log');
		set_page('logs/for_required',$data);
	}

	function for_required_datatable(){
		$config['table'] = 'for_required_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'for_required', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->for_required;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function foundation_drawing_required(){
		$data['fields'] = $this->get_field('foundation_drawing_required_log');
		set_page('logs/foundation_drawing_required',$data);
	}

	function foundation_drawing_required_datatable(){
		$config['table'] = 'foundation_drawing_required_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'foundation_drawing_required', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->foundation_drawing_required;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function google_sheet_staff(){
		$data['fields'] = $this->get_field('google_sheet_staff_log');
		set_page('logs/google_sheet_staff',$data);
	}

	function google_sheet_staff_datatable(){
		$config['table'] = 'google_sheet_staff_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'google_sheet_id', 'staff_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->google_sheet_id;
			$row[] = $tbl_row->staff_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function google_sheets(){
		$data['fields'] = $this->get_field('google_sheets_log');
		set_page('logs/google_sheets',$data);
	}

	function google_sheets_datatable(){
		$config['table'] = 'google_sheets_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'name', 'url', 'sequence', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->name;
			$row[] = $tbl_row->url;
			$row[] = $tbl_row->sequence;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function grade(){
		$data['fields'] = $this->get_field('grade_log');
		set_page('logs/grade',$data);
	}

	function grade_datatable(){
		$config['table'] = 'grade_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'grade_id', 'grade', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->grade_id;
			$row[] = $tbl_row->grade;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inbox(){
		$data['fields'] = $this->get_field('inbox_log');
		set_page('logs/inbox',$data);
	}

	function inbox_datatable(){
		$config['table'] = 'inbox_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'mail_id', 'uid', 'mail_no', 'from_address', 'from_name', 'to_address', 'to_name', 'reply_to', 'reply_to_name', 'sender_address', 'sender_name', 'cc', 'bcc', 'subject', 'body', 'attachments', 'received_at', 'folder_label', 'folder_name', 'is_sent', 'is_unread', 'is_starred', 'is_attachment', 'is_created', 'is_updated', 'is_deleted', 'staff_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->mail_id;
			$row[] = $tbl_row->uid;
			$row[] = $tbl_row->mail_no;
			$row[] = $tbl_row->from_address;
			$row[] = $tbl_row->from_name;
			$row[] = $tbl_row->to_address;
			$row[] = $tbl_row->to_name;
			$row[] = $tbl_row->reply_to;
			$row[] = $tbl_row->reply_to_name;
			$row[] = $tbl_row->sender_address;
			$row[] = $tbl_row->sender_name;
			$row[] = $tbl_row->cc;
			$row[] = $tbl_row->bcc;
			$row[] = $tbl_row->subject;
			$row[] = $tbl_row->body;
			$row[] = $tbl_row->attachments;
			$row[] = $tbl_row->received_at;
			$row[] = $tbl_row->folder_label;
			$row[] = $tbl_row->folder_name;
			$row[] = $tbl_row->is_sent;
			$row[] = $tbl_row->is_unread;
			$row[] = $tbl_row->is_starred;
			$row[] = $tbl_row->is_attachment;
			$row[] = $tbl_row->is_created;
			$row[] = $tbl_row->is_updated;
			$row[] = $tbl_row->is_deleted;
			$row[] = $tbl_row->staff_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function industry_type(){
		$data['fields'] = $this->get_field('industry_type_log');
		set_page('logs/industry_type',$data);
	}

	function industry_type_datatable(){
		$config['table'] = 'industry_type_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'industry_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->industry_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inquiry_confirmation(){
		$data['fields'] = $this->get_field('inquiry_confirmation_log');
		set_page('logs/inquiry_confirmation',$data);
	}

	function inquiry_confirmation_datatable(){
		$config['table'] = 'inquiry_confirmation_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'inquiry_confirmation', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->inquiry_confirmation;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inquiry_followup_history(){
		$data['fields'] = $this->get_field('inquiry_followup_history_log');
		set_page('logs/inquiry_followup_history',$data);
	}

	function inquiry_followup_history_datatable(){
		$config['table'] = 'inquiry_followup_history_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'inquiry_id', 'followup_date', 'history', 'followup_by', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->inquiry_id;
			$row[] = $tbl_row->followup_date;
			$row[] = $tbl_row->history;
			$row[] = $tbl_row->followup_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inquiry_items(){
		$data['fields'] = $this->get_field('inquiry_items_log');
		set_page('logs/inquiry_items',$data);
	}

	function inquiry_items_datatable(){
		$config['table'] = 'inquiry_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'inquiry_id', 'item_data', 'inquiry_flag', 'quotation_flag', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->inquiry_id;
			$row[] = $tbl_row->item_data;
			$row[] = $tbl_row->inquiry_flag;
			$row[] = $tbl_row->quotation_flag;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inquiry(){
		$data['fields'] = $this->get_field('inquiry_log');
		set_page('logs/inquiry',$data);
	}

	function inquiry_datatable(){
		$config['table'] = 'inquiry_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'inquiry_id', 'inquiry_no', 'due_date', 'reference_date', 'inquiry_date', 'kind_attn_id', 'follow_up_by_id', 'assigned_to_id', 'industry_type_id', 'inquiry_stage_id', 'branch_id', 'inquiry_status_id', 'sales_id', 'sales_type_id', 'party_id', 'received_email', 'received_fax', 'received_verbal', 'is_received_post', 'send_email', 'send_fax', 'is_send_post', 'send_from_post', 'send_from_post_name', 'lead_or_inquiry', 'lead_id', 'history', 'created_by', 'created_at', 'updated_by', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->inquiry_id;
			$row[] = $tbl_row->inquiry_no;
			$row[] = $tbl_row->due_date;
			$row[] = $tbl_row->reference_date;
			$row[] = $tbl_row->inquiry_date;
			$row[] = $tbl_row->kind_attn_id;
			$row[] = $tbl_row->follow_up_by_id;
			$row[] = $tbl_row->assigned_to_id;
			$row[] = $tbl_row->industry_type_id;
			$row[] = $tbl_row->inquiry_stage_id;
			$row[] = $tbl_row->branch_id;
			$row[] = $tbl_row->inquiry_status_id;
			$row[] = $tbl_row->sales_id;
			$row[] = $tbl_row->sales_type_id;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->received_email;
			$row[] = $tbl_row->received_fax;
			$row[] = $tbl_row->received_verbal;
			$row[] = $tbl_row->is_received_post;
			$row[] = $tbl_row->send_email;
			$row[] = $tbl_row->send_fax;
			$row[] = $tbl_row->is_send_post;
			$row[] = $tbl_row->send_from_post;
			$row[] = $tbl_row->send_from_post_name;
			$row[] = $tbl_row->lead_or_inquiry;
			$row[] = $tbl_row->lead_id;
			$row[] = $tbl_row->history;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inquiry_stage(){
		$data['fields'] = $this->get_field('inquiry_stage_log');
		set_page('logs/inquiry_stage',$data);
	}

	function inquiry_stage_datatable(){
		$config['table'] = 'inquiry_stage_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'inquiry_stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->inquiry_stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inquiry_status(){
		$data['fields'] = $this->get_field('inquiry_status_log');
		set_page('logs/inquiry_status',$data);
	}

	function inquiry_status_datatable(){
		$config['table'] = 'inquiry_status_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'inquiry_status', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->inquiry_status;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function inspection_required(){
		$data['fields'] = $this->get_field('inspection_required_log');
		set_page('logs/inspection_required',$data);
	}

	function inspection_required_datatable(){
		$config['table'] = 'inspection_required_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'inspection_required', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->inspection_required;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function interview(){
		$data['fields'] = $this->get_field('interview_log');
		set_page('logs/interview',$data);
	}

	function interview_datatable(){
		$config['table'] = 'interview_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'interview_id', 'name', 'email', 'pass', 'mailbox_email', 'mailbox_password', 'image', 'birth_date', 'qualification', 'father_name', 'mother_name', 'husband_wife_name', 'blood_group', 'contact_no', 'married_status', 'gender', 'address', 'permanent_address', 'interview_review', 'interview_decision', 'designation_id', 'department_id', 'grade_id', 'date_of_joining', 'date_of_leaving', 'allow_pf', 'salary', 'basic_pay', 'house_rent', 'traveling_allowance', 'education_allowance', 'pf_amount', 'bonus_amount', 'other_allotment', 'medical_reimbursement', 'professional_tax', 'monthly_gross', 'cost_to_company', 'pf_employee', 'bank_name', 'esic_no', 'emp_id', 'pan_no', 'bank_acc_no', 'uan_id', 'pf_no', 'emp_no', 'esic_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->interview_id;
			$row[] = $tbl_row->name;
			$row[] = $tbl_row->email;
			$row[] = $tbl_row->pass;
			$row[] = $tbl_row->mailbox_email;
			$row[] = $tbl_row->mailbox_password;
			$row[] = $tbl_row->image;
			$row[] = $tbl_row->birth_date;
			$row[] = $tbl_row->qualification;
			$row[] = $tbl_row->father_name;
			$row[] = $tbl_row->mother_name;
			$row[] = $tbl_row->husband_wife_name;
			$row[] = $tbl_row->blood_group;
			$row[] = $tbl_row->contact_no;
			$row[] = $tbl_row->married_status;
			$row[] = $tbl_row->gender;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->permanent_address;
			$row[] = $tbl_row->interview_review;
			$row[] = $tbl_row->interview_decision;
			$row[] = $tbl_row->designation_id;
			$row[] = $tbl_row->department_id;
			$row[] = $tbl_row->grade_id;
			$row[] = $tbl_row->date_of_joining;
			$row[] = $tbl_row->date_of_leaving;
			$row[] = $tbl_row->allow_pf;
			$row[] = $tbl_row->salary;
			$row[] = $tbl_row->basic_pay;
			$row[] = $tbl_row->house_rent;
			$row[] = $tbl_row->traveling_allowance;
			$row[] = $tbl_row->education_allowance;
			$row[] = $tbl_row->pf_amount;
			$row[] = $tbl_row->bonus_amount;
			$row[] = $tbl_row->other_allotment;
			$row[] = $tbl_row->medical_reimbursement;
			$row[] = $tbl_row->professional_tax;
			$row[] = $tbl_row->monthly_gross;
			$row[] = $tbl_row->cost_to_company;
			$row[] = $tbl_row->pf_employee;
			$row[] = $tbl_row->bank_name;
			$row[] = $tbl_row->esic_no;
			$row[] = $tbl_row->emp_id;
			$row[] = $tbl_row->pan_no;
			$row[] = $tbl_row->bank_acc_no;
			$row[] = $tbl_row->uan_id;
			$row[] = $tbl_row->pf_no;
			$row[] = $tbl_row->emp_no;
			$row[] = $tbl_row->esic_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function invoice_billing_terms(){
		$data['fields'] = $this->get_field('invoice_billing_terms_log');
		set_page('logs/invoice_billing_terms',$data);
	}

	function invoice_billing_terms_datatable(){
		$config['table'] = 'invoice_billing_terms_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'invoice_id', 'cal_code', 'narration', 'cal_definition', 'percentage', 'value', 'gl_acc', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->invoice_id;
			$row[] = $tbl_row->cal_code;
			$row[] = $tbl_row->narration;
			$row[] = $tbl_row->cal_definition;
			$row[] = $tbl_row->percentage;
			$row[] = $tbl_row->value;
			$row[] = $tbl_row->gl_acc;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function invoice_cal_code_definition(){
		$data['fields'] = $this->get_field('invoice_cal_code_definition_log');
		set_page('logs/invoice_cal_code_definition',$data);
	}

	function invoice_cal_code_definition_datatable(){
		$config['table'] = 'invoice_cal_code_definition_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'billing_terms_detail_id', 'invoice_id', 'is_first_element', 'cal_operation', 'cal_code_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->billing_terms_detail_id;
			$row[] = $tbl_row->invoice_id;
			$row[] = $tbl_row->is_first_element;
			$row[] = $tbl_row->cal_operation;
			$row[] = $tbl_row->cal_code_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function invoice_items(){
		$data['fields'] = $this->get_field('invoice_items_log');
		set_page('logs/invoice_items',$data);
	}

	function invoice_items_datatable(){
		$config['table'] = 'invoice_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_id', 'invoice_id', 'item_code', 'item_category_id', 'item_name', 'item_desc', 'item_description', 'add_description', 'detail_description', 'drawing_number', 'drawing_revision', 'uom_id', 'uom', 'total_amount', 'item_note', 'quantity', 'disc_per', 'rate', 'disc_value', 'amount', 'net_amount', 'item_status_id', 'item_status', 'cust_part_no', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->invoice_id;
			$row[] = $tbl_row->item_code;
			$row[] = $tbl_row->item_category_id;
			$row[] = $tbl_row->item_name;
			$row[] = $tbl_row->item_desc;
			$row[] = $tbl_row->item_description;
			$row[] = $tbl_row->add_description;
			$row[] = $tbl_row->detail_description;
			$row[] = $tbl_row->drawing_number;
			$row[] = $tbl_row->drawing_revision;
			$row[] = $tbl_row->uom_id;
			$row[] = $tbl_row->uom;
			$row[] = $tbl_row->total_amount;
			$row[] = $tbl_row->item_note;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->disc_per;
			$row[] = $tbl_row->rate;
			$row[] = $tbl_row->disc_value;
			$row[] = $tbl_row->amount;
			$row[] = $tbl_row->net_amount;
			$row[] = $tbl_row->item_status_id;
			$row[] = $tbl_row->item_status;
			$row[] = $tbl_row->cust_part_no;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}


	function invoice_type(){
		$data['fields'] = $this->get_field('invoice_type_log');
		set_page('logs/invoice_type',$data);
	}

	function invoice_type_datatable(){
		$config['table'] = 'invoice_type_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'invoice_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->invoice_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function invoices(){
		$data['fields'] = $this->get_field('invoices_log');
		set_page('logs/invoices',$data);
	}

	function invoices_datatable(){
		$config['table'] = 'invoices_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'invoice_type_id', 'sales_order_id', 'total_cmount', 'quatation_id', 'quotation_id', 'invoice_no', 'sales_order_no', 'sales_to_party_id', 'ship_to_party_id', 'branch_id', 'billing_terms_id', 'sales_order_date', 'committed_date', 'currency_id', 'project_id', 'kind_attn_id', 'contact_no', 'cust_po_no', 'email_id', 'po_date', 'sales_order_pref_id', 'conversation_rate', 'sales_id', 'sales_order_stage_id', 'sales_order_status_id', 'order_type', 'lead_provider_id', 'buyer_detail_id', 'note_detail_id', 'login_detail_id', 'transportation_by_id', 'foundation_drawing_required_id', 'loading_by_id', 'inspection_required_id', 'unloading_by_id', 'erection_commissioning_id', 'road_insurance_by_id', 'for_required_id', 'review_date', 'mach_deli_min_weeks', 'mach_deli_max_weeks', 'mach_deli_min_weeks_date', 'mach_deli_max_weeks_date', 'sales_order_validaty', 'sales_order_quotation_delivery', 'mode_of_shipment_name', 'mode_of_shipment_truck_number', 'supplier_payment_terms', 'received_payment', 'received_payment_date', 'received_payment_type', 'terms_condition_purchase', 'isApproved', 'pdf_url', 'lr_no', 'lr_date', 'vehicle_no', 'against_form', 'taxes_data', 'created_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->invoice_type_id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->total_cmount;
			$row[] = $tbl_row->quatation_id;
			$row[] = $tbl_row->quotation_id;
			$row[] = $tbl_row->invoice_no;
			$row[] = $tbl_row->sales_order_no;
			$row[] = $tbl_row->sales_to_party_id;
			$row[] = $tbl_row->ship_to_party_id;
			$row[] = $tbl_row->branch_id;
			$row[] = $tbl_row->billing_terms_id;
			$row[] = $tbl_row->sales_order_date;
			$row[] = $tbl_row->committed_date;
			$row[] = $tbl_row->currency_id;
			$row[] = $tbl_row->project_id;
			$row[] = $tbl_row->kind_attn_id;
			$row[] = $tbl_row->contact_no;
			$row[] = $tbl_row->cust_po_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->po_date;
			$row[] = $tbl_row->sales_order_pref_id;
			$row[] = $tbl_row->conversation_rate;
			$row[] = $tbl_row->sales_id;
			$row[] = $tbl_row->sales_order_stage_id;
			$row[] = $tbl_row->sales_order_status_id;
			$row[] = $tbl_row->order_type;
			$row[] = $tbl_row->lead_provider_id;
			$row[] = $tbl_row->buyer_detail_id;
			$row[] = $tbl_row->note_detail_id;
			$row[] = $tbl_row->login_detail_id;
			$row[] = $tbl_row->transportation_by_id;
			$row[] = $tbl_row->foundation_drawing_required_id;
			$row[] = $tbl_row->loading_by_id;
			$row[] = $tbl_row->inspection_required_id;
			$row[] = $tbl_row->unloading_by_id;
			$row[] = $tbl_row->erection_commissioning_id;
			$row[] = $tbl_row->road_insurance_by_id;
			$row[] = $tbl_row->for_required_id;
			$row[] = $tbl_row->review_date;
			$row[] = $tbl_row->mach_deli_min_weeks;
			$row[] = $tbl_row->mach_deli_max_weeks;
			$row[] = $tbl_row->mach_deli_min_weeks_date;
			$row[] = $tbl_row->mach_deli_max_weeks_date;
			$row[] = $tbl_row->sales_order_validaty;
			$row[] = $tbl_row->sales_order_quotation_delivery;
			$row[] = $tbl_row->mode_of_shipment_name;
			$row[] = $tbl_row->mode_of_shipment_truck_number;
			$row[] = $tbl_row->supplier_payment_terms;
			$row[] = $tbl_row->received_payment;
			$row[] = $tbl_row->received_payment_date;
			$row[] = $tbl_row->received_payment_type;
			$row[] = $tbl_row->terms_condition_purchase;
			$row[] = $tbl_row->isApproved;
			$row[] = $tbl_row->pdf_url;
			$row[] = $tbl_row->lr_no;
			$row[] = $tbl_row->lr_date;
			$row[] = $tbl_row->vehicle_no;
			$row[] = $tbl_row->against_form;
			$row[] = $tbl_row->taxes_data;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_bom(){
		$data['fields'] = $this->get_field('item_bom_log');
		set_page('logs/item_bom',$data);
	}

	function item_bom_datatable(){
		$config['table'] = 'item_bom_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_master_id', 'item_category_id', 'item_id', 'qty', 'uom_id', 'total_amount', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_master_id;
			$row[] = $tbl_row->item_category_id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->qty;
			$row[] = $tbl_row->uom_id;
			$row[] = $tbl_row->total_amount;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_category(){
		$data['fields'] = $this->get_field('item_category_log');
		set_page('logs/item_category',$data);
	}

	function item_category_datatable(){
		$config['table'] = 'item_category_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_category', 'created_by', 'updated_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_category;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_class(){
		$data['fields'] = $this->get_field('item_class_log');
		set_page('logs/item_class',$data);
	}

	function item_class_datatable(){
		$config['table'] = 'item_class_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_class', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_class;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_details(){
		$data['fields'] = $this->get_field('item_details_log');
		set_page('logs/item_details',$data);
	}

	function item_details_datatable(){
		$config['table'] = 'item_details_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'inquiry_id', 'item_code', 'item_description', 'item_desc', 'add_description', 'detail_description', 'drawing_number', 'rev', 'uom', 'quantity', 'cust_part_no', 'item_note', 'lead_no1', 'lead_no2', 'item_status', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->inquiry_id;
			$row[] = $tbl_row->item_code;
			$row[] = $tbl_row->item_description;
			$row[] = $tbl_row->item_desc;
			$row[] = $tbl_row->add_description;
			$row[] = $tbl_row->detail_description;
			$row[] = $tbl_row->drawing_number;
			$row[] = $tbl_row->rev;
			$row[] = $tbl_row->uom;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->cust_part_no;
			$row[] = $tbl_row->item_note;
			$row[] = $tbl_row->lead_no1;
			$row[] = $tbl_row->lead_no2;
			$row[] = $tbl_row->item_status;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_documents(){
		$data['fields'] = $this->get_field('item_documents_log');
		set_page('logs/item_documents',$data);
	}

	function item_documents_datatable(){
		$config['table'] = 'item_documents_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_id', 'detail', 'document_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->detail;
			$row[] = $tbl_row->document_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_files(){
		$data['fields'] = $this->get_field('item_files_log');
		set_page('logs/item_files',$data);
	}

	function item_files_datatable(){
		$config['table'] = 'item_files_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_id', 'file_url', 'item_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->file_url;
			$row[] = $tbl_row->item_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_group_code(){
		$data['fields'] = $this->get_field('item_group_code_log');
		set_page('logs/item_group_code',$data);
	}

	function item_group_code_datatable(){
		$config['table'] = 'item_group_code_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'ig_code', 'ig_description', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->ig_code;
			$row[] = $tbl_row->ig_description;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_make(){
		$data['fields'] = $this->get_field('item_make_log');
		set_page('logs/item_make',$data);
	}

	function item_make_datatable(){
		$config['table'] = 'item_make_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_make', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_make;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_status(){
		$data['fields'] = $this->get_field('item_status_log');
		set_page('logs/item_status',$data);
	}

	function item_status_datatable(){
		$config['table'] = 'item_status_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_status', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_status;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function item_type(){
		$data['fields'] = $this->get_field('item_type_log');
		set_page('logs/item_type',$data);
	}

	function item_type_datatable(){
		$config['table'] = 'item_type_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function items(){
		$data['fields'] = $this->get_field('items_log');
		set_page('logs/items',$data);
	}

	function items_datatable(){
		$config['table'] = 'items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_category', 'item_code1', 'item_code2', 'uom', 'qty', 'item_type', 'stock', 'document', 'hsn_code', 'item_group', 'item_active', 'item_name', 'cfactor', 'conv_uom', 'item_mpt', 'item_status', 'image', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_category;
			$row[] = $tbl_row->item_code1;
			$row[] = $tbl_row->item_code2;
			$row[] = $tbl_row->uom;
			$row[] = $tbl_row->qty;
			$row[] = $tbl_row->item_type;
			$row[] = $tbl_row->stock;
			$row[] = $tbl_row->document;
			$row[] = $tbl_row->hsn_code;
			$row[] = $tbl_row->item_group;
			$row[] = $tbl_row->item_active;
			$row[] = $tbl_row->item_name;
			$row[] = $tbl_row->cfactor;
			$row[] = $tbl_row->conv_uom;
			$row[] = $tbl_row->item_mpt;
			$row[] = $tbl_row->item_status;
			$row[] = $tbl_row->image;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function lead_contact_person(){
		$data['fields'] = $this->get_field('lead_contact_person_log');
		set_page('logs/lead_contact_person',$data);
	}

	function lead_contact_person_datatable(){
		$config['table'] = 'lead_contact_person_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'person_name', 'department', 'designation', 'phone_no', 'mobile_no', 'fax_no', 'email_id', 'division', 'address', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->person_name;
			$row[] = $tbl_row->department;
			$row[] = $tbl_row->designation;
			$row[] = $tbl_row->phone_no;
			$row[] = $tbl_row->mobile_no;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->division;
			$row[] = $tbl_row->address;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function lead_details(){
		$data['fields'] = $this->get_field('lead_details_log');
		set_page('logs/lead_details',$data);
	}

	function lead_details_datatable(){
		$config['table'] = 'lead_details_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'party_id', 'lead_no', 'lead_title', 'start_date', 'end_date', 'lead_detail', 'industry_type_id', 'lead_owner_id', 'assigned_to_id', 'priority_id', 'lead_source_id', 'lead_stage_id', 'lead_provider_id', 'lead_status_id', 'review_date', 'created_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->lead_no;
			$row[] = $tbl_row->lead_title;
			$row[] = $tbl_row->start_date;
			$row[] = $tbl_row->end_date;
			$row[] = $tbl_row->lead_detail;
			$row[] = $tbl_row->industry_type_id;
			$row[] = $tbl_row->lead_owner_id;
			$row[] = $tbl_row->assigned_to_id;
			$row[] = $tbl_row->priority_id;
			$row[] = $tbl_row->lead_source_id;
			$row[] = $tbl_row->lead_stage_id;
			$row[] = $tbl_row->lead_provider_id;
			$row[] = $tbl_row->lead_status_id;
			$row[] = $tbl_row->review_date;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function lead_provider(){
		$data['fields'] = $this->get_field('lead_provider_log');
		set_page('logs/lead_provider',$data);
	}

	function lead_provider_datatable(){
		$config['table'] = 'lead_provider_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'lead_provider', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->lead_provider;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function lead_source(){
		$data['fields'] = $this->get_field('lead_source_log');
		set_page('logs/lead_source',$data);
	}

	function lead_source_datatable(){
		$config['table'] = 'lead_source_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'lead_source', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->lead_source;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function lead_stage(){
		$data['fields'] = $this->get_field('lead_stage_log');
		set_page('logs/lead_stage',$data);
	}

	function lead_stage_datatable(){
		$config['table'] = 'lead_stage_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'lead_stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->lead_stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function lead_status(){
		$data['fields'] = $this->get_field('lead_status_log');
		set_page('logs/lead_status',$data);
	}

	function lead_status_datatable(){
		$config['table'] = 'lead_status_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'lead_status', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->lead_status;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function leave_for_employee(){
		$data['fields'] = $this->get_field('leave_for_employee_log');
		set_page('logs/leave_for_employee',$data);
	}

	function leave_for_employee_datatable(){
		$config['table'] = 'leave_for_employee_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'total_sl', 'total_cl', 'total_el_pl', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->total_sl;
			$row[] = $tbl_row->total_cl;
			$row[] = $tbl_row->total_el_pl;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function leaves(){
		$data['fields'] = $this->get_field('leaves_log');
		set_page('logs/leaves',$data);
	}

	function leaves_datatable(){
		$config['table'] = 'leaves_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'leave_id', 'employee_id', 'name', 'email', 'from_date', 'to_date', 'leave_type', 'day', 'leave_detail', 'leave_count', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->leave_id;
			$row[] = $tbl_row->employee_id;
			$row[] = $tbl_row->name;
			$row[] = $tbl_row->email;
			$row[] = $tbl_row->from_date;
			$row[] = $tbl_row->to_date;
			$row[] = $tbl_row->leave_type;
			$row[] = $tbl_row->day;
			$row[] = $tbl_row->leave_detail;
			$row[] = $tbl_row->leave_count;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function letters(){
		$data['fields'] = $this->get_field('letters_log');
		set_page('logs/letters',$data);
	}

	function letters_datatable(){
		$config['table'] = 'letters_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'letter_id', 'letter_code', 'letter_template', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->letter_id;
			$row[] = $tbl_row->letter_code;
			$row[] = $tbl_row->letter_template;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function loading_by(){
		$data['fields'] = $this->get_field('loading_by_log');
		set_page('logs/loading_by',$data);
	}

	function loading_by_datatable(){
		$config['table'] = 'loading_by_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'loading_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->loading_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function mail_system_folder(){
		$data['fields'] = $this->get_field('mail_system_folder_log');
		set_page('logs/mail_system_folder',$data);
	}

	function mail_system_folder_datatable(){
		$config['table'] = 'mail_system_folder_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'folder_id', 'folder', 'staff_id', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->folder_id;
			$row[] = $tbl_row->folder;
			$row[] = $tbl_row->staff_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function mail_system(){
		$data['fields'] = $this->get_field('mail_system_log');
		set_page('logs/mail_system',$data);
	}

	function mail_system_datatable(){
		$config['table'] = 'mail_system_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'mail_id', 'mail_uid', 'staff_id', 'folder', 'from_address', 'from_name', 'to_address', 'to_name', 'reply_to', 'reply_to_name', 'sender_address', 'sender_name', 'cc', 'bcc', 'subject', 'body', 'attachments', 'received_at', 'is_unread', 'is_starred', 'is_attachment', 'is_read_receipt_req', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->mail_id;
			$row[] = $tbl_row->mail_uid;
			$row[] = $tbl_row->staff_id;
			$row[] = $tbl_row->folder;
			$row[] = $tbl_row->from_address;
			$row[] = $tbl_row->from_name;
			$row[] = $tbl_row->to_address;
			$row[] = $tbl_row->to_name;
			$row[] = $tbl_row->reply_to;
			$row[] = $tbl_row->reply_to_name;
			$row[] = $tbl_row->sender_address;
			$row[] = $tbl_row->sender_name;
			$row[] = $tbl_row->cc;
			$row[] = $tbl_row->bcc;
			$row[] = $tbl_row->subject;
			$row[] = $tbl_row->body;
			$row[] = $tbl_row->attachments;
			$row[] = $tbl_row->received_at;
			$row[] = $tbl_row->is_unread;
			$row[] = $tbl_row->is_starred;
			$row[] = $tbl_row->is_attachment;
			$row[] = $tbl_row->is_read_receipt_req;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function mailbox_folders(){
		$data['fields'] = $this->get_field('mailbox_folders_log');
		set_page('logs/mailbox_folders',$data);
	}

	function mailbox_folders_datatable(){
		$config['table'] = 'mailbox_folders_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'parent_id', 'folder_label', 'folder_name', 'old_parent_id', 'is_system_folder', 'is_created', 'is_updated', 'is_deleted', 'deleted_at', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->parent_id;
			$row[] = $tbl_row->folder_label;
			$row[] = $tbl_row->folder_name;
			$row[] = $tbl_row->old_parent_id;
			$row[] = $tbl_row->is_system_folder;
			$row[] = $tbl_row->is_created;
			$row[] = $tbl_row->is_updated;
			$row[] = $tbl_row->is_deleted;
			$row[] = $tbl_row->deleted_at;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function mailbox_mails(){
		$data['fields'] = $this->get_field('mailbox_mails_log');
		set_page('logs/mailbox_mails',$data);
	}

	function mailbox_mails_datatable(){
		$config['table'] = 'mailbox_mails_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'mail_id', 'uid', 'folder_id', 'mail_no', 'from_address', 'from_name', 'to_address', 'to_name', 'reply_to', 'reply_to_name', 'sender_address', 'sender_name', 'cc', 'bcc', 'subject', 'body', 'attachments', 'received_at', 'folder_label', 'folder_name', 'is_unread', 'is_starred', 'is_attachment', 'is_created', 'is_updated', 'is_deleted', 'deleted_at', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->mail_id;
			$row[] = $tbl_row->uid;
			$row[] = $tbl_row->folder_id;
			$row[] = $tbl_row->mail_no;
			$row[] = $tbl_row->from_address;
			$row[] = $tbl_row->from_name;
			$row[] = $tbl_row->to_address;
			$row[] = $tbl_row->to_name;
			$row[] = $tbl_row->reply_to;
			$row[] = $tbl_row->reply_to_name;
			$row[] = $tbl_row->sender_address;
			$row[] = $tbl_row->sender_name;
			$row[] = $tbl_row->cc;
			$row[] = $tbl_row->bcc;
			$row[] = $tbl_row->subject;
			$row[] = $tbl_row->body;
			$row[] = $tbl_row->attachments;
			$row[] = $tbl_row->received_at;
			$row[] = $tbl_row->folder_label;
			$row[] = $tbl_row->folder_name;
			$row[] = $tbl_row->is_unread;
			$row[] = $tbl_row->is_starred;
			$row[] = $tbl_row->is_attachment;
			$row[] = $tbl_row->is_created;
			$row[] = $tbl_row->is_updated;
			$row[] = $tbl_row->is_deleted;
			$row[] = $tbl_row->deleted_at;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function mailbox_settings(){
		$data['fields'] = $this->get_field('mailbox_settings_log');
		set_page('logs/mailbox_settings',$data);
	}

	function mailbox_settings_datatable(){
		$config['table'] = 'mailbox_settings_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'staff_id', 'setting_key', 'setting_value', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->staff_id;
			$row[] = $tbl_row->setting_key;
			$row[] = $tbl_row->setting_value;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function main_group(){
		$data['fields'] = $this->get_field('main_group_log');
		set_page('logs/main_group',$data);
	}

	function main_group_datatable(){
		$config['table'] = 'main_group_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_group', 'code', 'description', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_group;
			$row[] = $tbl_row->code;
			$row[] = $tbl_row->description;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function material_process_type(){
		$data['fields'] = $this->get_field('material_process_type_log');
		set_page('logs/material_process_type',$data);
	}

	function material_process_type_datatable(){
		$config['table'] = 'material_process_type_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'material_process_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->material_process_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function material_specification(){
		$data['fields'] = $this->get_field('material_specification_log');
		set_page('logs/material_specification',$data);
	}

	function material_specification_datatable(){
		$config['table'] = 'material_specification_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'material', 'material_specification', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->material;
			$row[] = $tbl_row->material_specification;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function module_roles(){
		$data['fields'] = $this->get_field('module_roles_log');
		set_page('logs/module_roles',$data);
	}

	function module_roles_datatable(){
		$config['table'] = 'module_roles_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'title', 'role_name', 'module_id', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->title;
			$row[] = $tbl_row->role_name;
			$row[] = $tbl_row->module_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function order_stage(){
		$data['fields'] = $this->get_field('order_stage_log');
		set_page('logs/order_stage',$data);
	}

	function order_stage_datatable(){
		$config['table'] = 'order_stage_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'order_stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->order_stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function outbox(){
		$data['fields'] = $this->get_field('outbox_log');
		set_page('logs/outbox',$data);
	}

	function outbox_datatable(){
		$config['table'] = 'outbox_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'mail_id', 'from_address', 'from_name', 'to_address', 'to_name', 'reply_to', 'reply_to_name', 'sender_address', 'sender_name', 'cc', 'bcc', 'subject', 'body', 'attachments', 'received_at', 'is_attachment', 'is_sent', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->mail_id;
			$row[] = $tbl_row->from_address;
			$row[] = $tbl_row->from_name;
			$row[] = $tbl_row->to_address;
			$row[] = $tbl_row->to_name;
			$row[] = $tbl_row->reply_to;
			$row[] = $tbl_row->reply_to_name;
			$row[] = $tbl_row->sender_address;
			$row[] = $tbl_row->sender_name;
			$row[] = $tbl_row->cc;
			$row[] = $tbl_row->bcc;
			$row[] = $tbl_row->subject;
			$row[] = $tbl_row->body;
			$row[] = $tbl_row->attachments;
			$row[] = $tbl_row->received_at;
			$row[] = $tbl_row->is_attachment;
			$row[] = $tbl_row->is_sent;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function party_detail(){
		$data['fields'] = $this->get_field('party_detail_log');
		set_page('logs/party_detail',$data);
	}

	function party_detail_datatable(){
		$config['table'] = 'party_detail_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'party_detail_id', 'party_id', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->party_detail_id;
			$row[] = $tbl_row->party_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function party(){
		$data['fields'] = $this->get_field('party_log');
		set_page('logs/party',$data);
	}

	function party_datatable(){
		$config['table'] = 'party_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'party_id', 'party_code', 'party_name', 'reference_id', 'reference_description', 'party_type_1', 'party_type_2', 'branch_id', 'project', 'address', 'area', 'pincode', 'city_id', 'state_id', 'country_id', 'phone_no', 'fax_no', 'email_id', 'website', 'opening_bal', 'credit_limit', 'active', 'tin_vat_no', 'gst_no', 'tin_cst_no', 'ecc_no', 'pan_no', 'range', 'division', 'service_tax_no', 'utr_no', 'address_work', 'w_address', 'w_city', 'w_state', 'w_country', 'oldw_pincode', 'w_email', 'w_web', 'w_phone1', 'w_phone2', 'w_phone3', 'w_note', 'oldPartyType', 'oldHoliday', 'oldST_No', 'oldVendorCode', 'oldw_fax', 'oldacct_no', 'oldReason', 'oldAllowMultipalBill', 'oldAddCity', 'oldCreatedBy', 'oldCreateDate', 'oldModifyBy', 'oldModiDate', 'oldPrintFlag', 'oldPT', 'oldCPerson', 'oldOpType', 'oldCompProfile', 'oldBranchCode', 'oldAreaName', 'oldCreditLimit', 'oldAcctGrpCode', 'oldOpBalDr', 'oldApprovedBy', 'oldApprovedDate', 'oldPStage', 'agent_id', 'outstanding_balance', 'created_by', 'updated_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->party_code;
			$row[] = $tbl_row->party_name;
			$row[] = $tbl_row->reference_id;
			$row[] = $tbl_row->reference_description;
			$row[] = $tbl_row->party_type_1;
			$row[] = $tbl_row->party_type_2;
			$row[] = $tbl_row->branch_id;
			$row[] = $tbl_row->project;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->area;
			$row[] = $tbl_row->pincode;
			$row[] = $tbl_row->city_id;
			$row[] = $tbl_row->state_id;
			$row[] = $tbl_row->country_id;
			$row[] = $tbl_row->phone_no;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->website;
			$row[] = $tbl_row->opening_bal;
			$row[] = $tbl_row->credit_limit;
			$row[] = $tbl_row->active;
			$row[] = $tbl_row->tin_vat_no;
			$row[] = $tbl_row->gst_no;
			$row[] = $tbl_row->tin_cst_no;
			$row[] = $tbl_row->ecc_no;
			$row[] = $tbl_row->pan_no;
			$row[] = $tbl_row->range;
			$row[] = $tbl_row->division;
			$row[] = $tbl_row->service_tax_no;
			$row[] = $tbl_row->utr_no;
			$row[] = $tbl_row->address_work;
			$row[] = $tbl_row->w_address;
			$row[] = $tbl_row->w_city;
			$row[] = $tbl_row->w_state;
			$row[] = $tbl_row->w_country;
			$row[] = $tbl_row->oldw_pincode;
			$row[] = $tbl_row->w_email;
			$row[] = $tbl_row->w_web;
			$row[] = $tbl_row->w_phone1;
			$row[] = $tbl_row->w_phone2;
			$row[] = $tbl_row->w_phone3;
			$row[] = $tbl_row->w_note;
			$row[] = $tbl_row->oldPartyType;
			$row[] = $tbl_row->oldHoliday;
			$row[] = $tbl_row->oldST_No;
			$row[] = $tbl_row->oldVendorCode;
			$row[] = $tbl_row->oldw_fax;
			$row[] = $tbl_row->oldacct_no;
			$row[] = $tbl_row->oldReason;
			$row[] = $tbl_row->oldAllowMultipalBill;
			$row[] = $tbl_row->oldAddCity;
			$row[] = $tbl_row->oldCreatedBy;
			$row[] = $tbl_row->oldCreateDate;
			$row[] = $tbl_row->oldModifyBy;
			$row[] = $tbl_row->oldModiDate;
			$row[] = $tbl_row->oldPrintFlag;
			$row[] = $tbl_row->oldPT;
			$row[] = $tbl_row->oldCPerson;
			$row[] = $tbl_row->oldOpType;
			$row[] = $tbl_row->oldCompProfile;
			$row[] = $tbl_row->oldBranchCode;
			$row[] = $tbl_row->oldAreaName;
			$row[] = $tbl_row->oldCreditLimit;
			$row[] = $tbl_row->oldAcctGrpCode;
			$row[] = $tbl_row->oldOpBalDr;
			$row[] = $tbl_row->oldApprovedBy;
			$row[] = $tbl_row->oldApprovedDate;
			$row[] = $tbl_row->oldPStage;
			$row[] = $tbl_row->agent_id;
			$row[] = $tbl_row->outstanding_balance;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function party_old(){
		$data['fields'] = $this->get_field('party_old_log');
		set_page('logs/party_old',$data);
	}

	function party_old_datatable(){
		$config['table'] = 'party_old_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'party_code', 'party_name', 'address', 'pincode', 'phone_no', 'fax_no', 'website', 'email_id', 'tin_cst_no', 'service_tax_no', 'ecc_no', 'pan_no', 'active', 'range', 'division', 'w_address', 'w_phone1', 'w_email', 'w_web', 'opening_bal', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->party_code;
			$row[] = $tbl_row->party_name;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->pincode;
			$row[] = $tbl_row->phone_no;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->website;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->tin_cst_no;
			$row[] = $tbl_row->service_tax_no;
			$row[] = $tbl_row->ecc_no;
			$row[] = $tbl_row->pan_no;
			$row[] = $tbl_row->active;
			$row[] = $tbl_row->range;
			$row[] = $tbl_row->division;
			$row[] = $tbl_row->w_address;
			$row[] = $tbl_row->w_phone1;
			$row[] = $tbl_row->w_email;
			$row[] = $tbl_row->w_web;
			$row[] = $tbl_row->opening_bal;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function party_type_1(){
		$data['fields'] = $this->get_field('party_type_1_log');
		set_page('logs/party_type_1',$data);
	}

	function party_type_1_datatable(){
		$config['table'] = 'party_type_1_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function party_type_2(){
		$data['fields'] = $this->get_field('party_type_2_log');
		set_page('logs/party_type_2',$data);
	}

	function party_type_2_datatable(){
		$config['table'] = 'party_type_2_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function payment(){
		$data['fields'] = $this->get_field('payment_log');
		set_page('logs/payment',$data);
	}

	function payment_datatable(){
		$config['table'] = 'payment_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'payment_id', 'party_id', 'payment_date', 'amount', 'payment_type', 'payment_note', 'created_by', 'updated_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->payment_id;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->payment_date;
			$row[] = $tbl_row->amount;
			$row[] = $tbl_row->payment_type;
			$row[] = $tbl_row->payment_note;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function priority(){
		$data['fields'] = $this->get_field('priority_log');
		set_page('logs/priority',$data);
	}

	function priority_datatable(){
		$config['table'] = 'priority_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'priority', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->priority;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function proforma_invoice_billing_terms(){
		$data['fields'] = $this->get_field('proforma_invoice_billing_terms_log');
		set_page('logs/proforma_invoice_billing_terms',$data);
	}

	function proforma_invoice_billing_terms_datatable(){
		$config['table'] = 'proforma_invoice_billing_terms_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'proforma_invoice_id', 'cal_code', 'narration', 'cal_definition', 'percentage', 'value', 'gl_acc', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->proforma_invoice_id;
			$row[] = $tbl_row->cal_code;
			$row[] = $tbl_row->narration;
			$row[] = $tbl_row->cal_definition;
			$row[] = $tbl_row->percentage;
			$row[] = $tbl_row->value;
			$row[] = $tbl_row->gl_acc;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function proforma_invoice_cal_code_definition(){
		$data['fields'] = $this->get_field('proforma_invoice_cal_code_definition_log');
		set_page('logs/proforma_invoice_cal_code_definition',$data);
	}

	function proforma_invoice_cal_code_definition_datatable(){
		$config['table'] = 'proforma_invoice_cal_code_definition_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'billing_terms_detail_id', 'proforma_invoice_id', 'is_first_element', 'cal_operation', 'cal_code_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->billing_terms_detail_id;
			$row[] = $tbl_row->proforma_invoice_id;
			$row[] = $tbl_row->is_first_element;
			$row[] = $tbl_row->cal_operation;
			$row[] = $tbl_row->cal_code_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function proforma_invoice_items(){
		$data['fields'] = $this->get_field('proforma_invoice_items_log');
		set_page('logs/proforma_invoice_items',$data);
	}

	function proforma_invoice_items_datatable(){
		$config['table'] = 'proforma_invoice_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_id', 'proforma_invoice_id', 'item_code', 'item_category_id', 'item_name', 'item_desc', 'item_description', 'add_description', 'detail_description', 'drawing_number', 'drawing_revision', 'uom_id', 'uom', 'total_amount', 'item_note', 'quantity', 'disc_per', 'rate', 'disc_value', 'amount', 'net_amount', 'item_status_id', 'item_status', 'cust_part_no', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->proforma_invoice_id;
			$row[] = $tbl_row->item_code;
			$row[] = $tbl_row->item_category_id;
			$row[] = $tbl_row->item_name;
			$row[] = $tbl_row->item_desc;
			$row[] = $tbl_row->item_description;
			$row[] = $tbl_row->add_description;
			$row[] = $tbl_row->detail_description;
			$row[] = $tbl_row->drawing_number;
			$row[] = $tbl_row->drawing_revision;
			$row[] = $tbl_row->uom_id;
			$row[] = $tbl_row->uom;
			$row[] = $tbl_row->total_amount;
			$row[] = $tbl_row->item_note;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->disc_per;
			$row[] = $tbl_row->rate;
			$row[] = $tbl_row->disc_value;
			$row[] = $tbl_row->amount;
			$row[] = $tbl_row->net_amount;
			$row[] = $tbl_row->item_status_id;
			$row[] = $tbl_row->item_status;
			$row[] = $tbl_row->cust_part_no;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function proforma_invoice_logins(){
		$data['fields'] = $this->get_field('proforma_invoice_logins_log');
		set_page('logs/proforma_invoice_logins',$data);
	}

	function proforma_invoice_logins_datatable(){
		$config['table'] = 'proforma_invoice_logins_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'proforma_invoice_id', 'created_by', 'created_by_id', 'created_date_time', 'last_modified_by', 'last_modified_by_id', 'modified_date_time', 'prepared_by', 'prepared_by_id', 'prepared_date_time', 'approved_by', 'approved_by_id', 'approved_date_time', 'stage_id', 'stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->proforma_invoice_id;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->created_by_id;
			$row[] = $tbl_row->created_date_time;
			$row[] = $tbl_row->last_modified_by;
			$row[] = $tbl_row->last_modified_by_id;
			$row[] = $tbl_row->modified_date_time;
			$row[] = $tbl_row->prepared_by;
			$row[] = $tbl_row->prepared_by_id;
			$row[] = $tbl_row->prepared_date_time;
			$row[] = $tbl_row->approved_by;
			$row[] = $tbl_row->approved_by_id;
			$row[] = $tbl_row->approved_date_time;
			$row[] = $tbl_row->stage_id;
			$row[] = $tbl_row->stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function proforma_invoices(){
		$data['fields'] = $this->get_field('proforma_invoices_log');
		set_page('logs/proforma_invoices',$data);
	}

	function proforma_invoices_datatable(){
		$config['table'] = 'proforma_invoices_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'quotation_id', 'proforma_invoice_no', 'quotation_no', 'sales_to_party_id', 'ship_to_party_id', 'branch_id', 'billing_terms_id', 'sales_order_date', 'committed_date', 'currency_id', 'project_id', 'kind_attn_id', 'contact_no', 'cust_po_no', 'email_id', 'po_date', 'sales_order_pref_id', 'conversation_rate', 'sales_id', 'sales_order_stage_id', 'sales_order_status_id', 'order_type', 'lead_provider_id', 'buyer_detail_id', 'note_detail_id', 'login_detail_id', 'transportation_by_id', 'foundation_drawing_required_id', 'loading_by_id', 'inspection_required_id', 'unloading_by_id', 'erection_commissioning_id', 'road_insurance_by_id', 'for_required_id', 'review_date', 'mach_deli_min_weeks', 'mach_deli_max_weeks', 'mach_deli_min_weeks_date', 'mach_deli_max_weeks_date', 'sales_order_validaty', 'sales_order_quotation_delivery', 'mode_of_shipment_name', 'mode_of_shipment_truck_number', 'supplier_payment_terms', 'received_payment', 'received_payment_date', 'received_payment_type', 'terms_condition_purchase', 'note', 'isApproved', 'pdf_url', 'created_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->quotation_id;
			$row[] = $tbl_row->proforma_invoice_no;
			$row[] = $tbl_row->quotation_no;
			$row[] = $tbl_row->sales_to_party_id;
			$row[] = $tbl_row->ship_to_party_id;
			$row[] = $tbl_row->branch_id;
			$row[] = $tbl_row->billing_terms_id;
			$row[] = $tbl_row->sales_order_date;
			$row[] = $tbl_row->committed_date;
			$row[] = $tbl_row->currency_id;
			$row[] = $tbl_row->project_id;
			$row[] = $tbl_row->kind_attn_id;
			$row[] = $tbl_row->contact_no;
			$row[] = $tbl_row->cust_po_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->po_date;
			$row[] = $tbl_row->sales_order_pref_id;
			$row[] = $tbl_row->conversation_rate;
			$row[] = $tbl_row->sales_id;
			$row[] = $tbl_row->sales_order_stage_id;
			$row[] = $tbl_row->sales_order_status_id;
			$row[] = $tbl_row->order_type;
			$row[] = $tbl_row->lead_provider_id;
			$row[] = $tbl_row->buyer_detail_id;
			$row[] = $tbl_row->note_detail_id;
			$row[] = $tbl_row->login_detail_id;
			$row[] = $tbl_row->transportation_by_id;
			$row[] = $tbl_row->foundation_drawing_required_id;
			$row[] = $tbl_row->loading_by_id;
			$row[] = $tbl_row->inspection_required_id;
			$row[] = $tbl_row->unloading_by_id;
			$row[] = $tbl_row->erection_commissioning_id;
			$row[] = $tbl_row->road_insurance_by_id;
			$row[] = $tbl_row->for_required_id;
			$row[] = $tbl_row->review_date;
			$row[] = $tbl_row->mach_deli_min_weeks;
			$row[] = $tbl_row->mach_deli_max_weeks;
			$row[] = $tbl_row->mach_deli_min_weeks_date;
			$row[] = $tbl_row->mach_deli_max_weeks_date;
			$row[] = $tbl_row->sales_order_validaty;
			$row[] = $tbl_row->sales_order_quotation_delivery;
			$row[] = $tbl_row->mode_of_shipment_name;
			$row[] = $tbl_row->mode_of_shipment_truck_number;
			$row[] = $tbl_row->supplier_payment_terms;
			$row[] = $tbl_row->received_payment;
			$row[] = $tbl_row->received_payment_date;
			$row[] = $tbl_row->received_payment_type;
			$row[] = $tbl_row->terms_condition_purchase;
			$row[] = $tbl_row->note;
			$row[] = $tbl_row->isApproved;
			$row[] = $tbl_row->pdf_url;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function project(){
		$data['fields'] = $this->get_field('project_log');
		set_page('logs/project',$data);
	}

	function project_datatable(){
		$config['table'] = 'project_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'project', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->project;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function purchase_invoice_items(){
		$data['fields'] = $this->get_field('purchase_invoice_items_log');
		set_page('logs/purchase_invoice_items',$data);
	}

	function purchase_invoice_items_datatable(){
		$config['table'] = 'purchase_invoice_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'invoice_id', 'item_id', 'quantity', 'item_data', 'invoice_flag', 'quotation_flag', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->invoice_id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->item_data;
			$row[] = $tbl_row->invoice_flag;
			$row[] = $tbl_row->quotation_flag;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function purchase_invoice(){
		$data['fields'] = $this->get_field('purchase_invoice_log');
		set_page('logs/purchase_invoice',$data);
	}

	function purchase_invoice_datatable(){
		$config['table'] = 'purchase_invoice_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'invoice_id', 'invoice_no', 'invoice_date', 'supplier_id', 'address', 'note', 'created_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->invoice_id;
			$row[] = $tbl_row->invoice_no;
			$row[] = $tbl_row->invoice_date;
			$row[] = $tbl_row->supplier_id;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->note;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function purchase_order_items(){
		$data['fields'] = $this->get_field('purchase_order_items_log');
		set_page('logs/purchase_order_items',$data);
	}

	function purchase_order_items_datatable(){
		$config['table'] = 'purchase_order_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'order_id', 'item_id', 'quantity', 'item_data', 'invoice_flag', 'quotation_flag', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->order_id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->item_data;
			$row[] = $tbl_row->invoice_flag;
			$row[] = $tbl_row->quotation_flag;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function purchase_order(){
		$data['fields'] = $this->get_field('purchase_order_log');
		set_page('logs/purchase_order',$data);
	}

	function purchase_order_datatable(){
		$config['table'] = 'purchase_order_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'order_id', 'order_no', 'order_date', 'supplier_id', 'address', 'note', 'created_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->order_id;
			$row[] = $tbl_row->order_no;
			$row[] = $tbl_row->order_date;
			$row[] = $tbl_row->supplier_id;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->note;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function quotation_items(){
		$data['fields'] = $this->get_field('quotation_items_log');
		set_page('logs/quotation_items',$data);
	}

	function quotation_items_datatable(){
		$config['table'] = 'quotation_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_id', 'quotation_id', 'item_code', 'item_name', 'item_description', 'item_desc', 'add_description', 'detail_description', 'drawing_number', 'drawing_revision', 'uom_id', 'uom', 'total_amount', 'item_note', 'quantity', 'disc_per', 'rate', 'disc_value', 'amount', 'net_amount', 'item_status_id', 'item_status', 'cust_part_no', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->quotation_id;
			$row[] = $tbl_row->item_code;
			$row[] = $tbl_row->item_name;
			$row[] = $tbl_row->item_description;
			$row[] = $tbl_row->item_desc;
			$row[] = $tbl_row->add_description;
			$row[] = $tbl_row->detail_description;
			$row[] = $tbl_row->drawing_number;
			$row[] = $tbl_row->drawing_revision;
			$row[] = $tbl_row->uom_id;
			$row[] = $tbl_row->uom;
			$row[] = $tbl_row->total_amount;
			$row[] = $tbl_row->item_note;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->disc_per;
			$row[] = $tbl_row->rate;
			$row[] = $tbl_row->disc_value;
			$row[] = $tbl_row->amount;
			$row[] = $tbl_row->net_amount;
			$row[] = $tbl_row->item_status_id;
			$row[] = $tbl_row->item_status;
			$row[] = $tbl_row->cust_part_no;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function quotation_reason(){
		$data['fields'] = $this->get_field('quotation_reason_log');
		set_page('logs/quotation_reason',$data);
	}

	function quotation_reason_datatable(){
		$config['table'] = 'quotation_reason_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'quotation_reason', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->quotation_reason;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function quotation_stage(){
		$data['fields'] = $this->get_field('quotation_stage_log');
		set_page('logs/quotation_stage',$data);
	}

	function quotation_stage_datatable(){
		$config['table'] = 'quotation_stage_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'quotation_stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->quotation_stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function quotation_status(){
		$data['fields'] = $this->get_field('quotation_status_log');
		set_page('logs/quotation_status',$data);
	}

	function quotation_status_datatable(){
		$config['table'] = 'quotation_status_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'quotation_status', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->quotation_status;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function quotation_type(){
		$data['fields'] = $this->get_field('quotation_type_log');
		set_page('logs/quotation_type',$data);
	}

	function quotation_type_datatable(){
		$config['table'] = 'quotation_type_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'quotation_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->quotation_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function quotations(){
		$data['fields'] = $this->get_field('quotations_log');
		set_page('logs/quotations',$data);
	}

	function quotations_datatable(){
		$config['table'] = 'quotations_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'enquiry_id', 'revised_from_id', 'party_id', 'enquiry_no', 'quotation_no', 'quotation_stage_id', 'quotation_type_id', 'quotation_status_id', 'sales_type_id', 'sales_id', 'action', 'currency_id', 'rev', 'rev_date', 'quotation_note', 'reason', 'reason_remark', 'review_date_check', 'review_date', 'enquiry_date', 'quotation_date', 'due_date_check', 'oldEnquiryNo', 'oldSubject', 'oldPreparedBy', 'oldApprovedBy', 'oldSrNo', 'oldExpr1', 'oldExpr2', 'oldEnquiryDate', 'oldKindAttn', 'oldEnquiryStatus', 'oldEnqRef', 'due_date', 'conv_rate', 'reference_date', 'kind_attn_id', 'party_code', 'party_name', 'reference_id', 'reference_description', 'address', 'agent_id', 'city_id', 'state_id', 'country_id', 'fax_no', 'email_id', 'project', 'phone_no', 'pincode', 'website', 'contact_person_name', 'contact_person_contact_no', 'contact_person_email_id', 'subject', 'header', 'footer', 'cc_to', 'authorised_by', 'reference_note', 'item_dec_title', 'addon_title', 'specification_1', 'specification_2', 'specification_3', 'send_from_post', 'send_from_post_name', 'pdf_url', 'created_by', 'updated_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->enquiry_id;
			$row[] = $tbl_row->revised_from_id;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->enquiry_no;
			$row[] = $tbl_row->quotation_no;
			$row[] = $tbl_row->quotation_stage_id;
			$row[] = $tbl_row->quotation_type_id;
			$row[] = $tbl_row->quotation_status_id;
			$row[] = $tbl_row->sales_type_id;
			$row[] = $tbl_row->sales_id;
			$row[] = $tbl_row->action;
			$row[] = $tbl_row->currency_id;
			$row[] = $tbl_row->rev;
			$row[] = $tbl_row->rev_date;
			$row[] = $tbl_row->quotation_note;
			$row[] = $tbl_row->reason;
			$row[] = $tbl_row->reason_remark;
			$row[] = $tbl_row->review_date_check;
			$row[] = $tbl_row->review_date;
			$row[] = $tbl_row->enquiry_date;
			$row[] = $tbl_row->quotation_date;
			$row[] = $tbl_row->due_date_check;
			$row[] = $tbl_row->oldEnquiryNo;
			$row[] = $tbl_row->oldSubject;
			$row[] = $tbl_row->oldPreparedBy;
			$row[] = $tbl_row->oldApprovedBy;
			$row[] = $tbl_row->oldSrNo;
			$row[] = $tbl_row->oldExpr1;
			$row[] = $tbl_row->oldExpr2;
			$row[] = $tbl_row->oldEnquiryDate;
			$row[] = $tbl_row->oldKindAttn;
			$row[] = $tbl_row->oldEnquiryStatus;
			$row[] = $tbl_row->oldEnqRef;
			$row[] = $tbl_row->due_date;
			$row[] = $tbl_row->conv_rate;
			$row[] = $tbl_row->reference_date;
			$row[] = $tbl_row->kind_attn_id;
			$row[] = $tbl_row->party_code;
			$row[] = $tbl_row->party_name;
			$row[] = $tbl_row->reference_id;
			$row[] = $tbl_row->reference_description;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->agent_id;
			$row[] = $tbl_row->city_id;
			$row[] = $tbl_row->state_id;
			$row[] = $tbl_row->country_id;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->project;
			$row[] = $tbl_row->phone_no;
			$row[] = $tbl_row->pincode;
			$row[] = $tbl_row->website;
			$row[] = $tbl_row->contact_person_name;
			$row[] = $tbl_row->contact_person_contact_no;
			$row[] = $tbl_row->contact_person_email_id;
			$row[] = $tbl_row->subject;
			$row[] = $tbl_row->header;
			$row[] = $tbl_row->footer;
			$row[] = $tbl_row->cc_to;
			$row[] = $tbl_row->authorised_by;
			$row[] = $tbl_row->reference_note;
			$row[] = $tbl_row->item_dec_title;
			$row[] = $tbl_row->addon_title;
			$row[] = $tbl_row->specification_1;
			$row[] = $tbl_row->specification_2;
			$row[] = $tbl_row->specification_3;
			$row[] = $tbl_row->send_from_post;
			$row[] = $tbl_row->send_from_post_name;
			$row[] = $tbl_row->pdf_url;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function real_leave(){
		$data['fields'] = $this->get_field('real_leave_log');
		set_page('logs/real_leave',$data);
	}

	function real_leave_datatable(){
		$config['table'] = 'real_leave_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'leave_id', 'employee_id', 'from_date', 'to_date', 'leave_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->leave_id;
			$row[] = $tbl_row->employee_id;
			$row[] = $tbl_row->from_date;
			$row[] = $tbl_row->to_date;
			$row[] = $tbl_row->leave_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function reference(){
		$data['fields'] = $this->get_field('reference_log');
		set_page('logs/reference',$data);
	}

	function reference_datatable(){
		$config['table'] = 'reference_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'reference', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->reference;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function reminder(){
		$data['fields'] = $this->get_field('reminder_log');
		set_page('logs/reminder',$data);
	}

	function reminder_datatable(){
		$config['table'] = 'reminder_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'reminder_id', 'user_id', 'remind', 'reminder_date', 'is_notified', 'assigned_to', 'status', 'priority', 'created_date', 'last_updated_date', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->reminder_id;
			$row[] = $tbl_row->user_id;
			$row[] = $tbl_row->remind;
			$row[] = $tbl_row->reminder_date;
			$row[] = $tbl_row->is_notified;
			$row[] = $tbl_row->assigned_to;
			$row[] = $tbl_row->status;
			$row[] = $tbl_row->priority;
			$row[] = $tbl_row->created_date;
			$row[] = $tbl_row->last_updated_date;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function road_insurance_by(){
		$data['fields'] = $this->get_field('road_insurance_by_log');
		set_page('logs/road_insurance_by',$data);
	}

	function road_insurance_by_datatable(){
		$config['table'] = 'road_insurance_by_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'road_insurance_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->road_insurance_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_action(){
		$data['fields'] = $this->get_field('sales_action_log');
		set_page('logs/sales_action',$data);
	}

	function sales_action_datatable(){
		$config['table'] = 'sales_action_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_action', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_action;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales(){
		$data['fields'] = $this->get_field('sales_log');
		set_page('logs/sales',$data);
	}

	function sales_datatable(){
		$config['table'] = 'sales_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_billing_terms(){
		$data['fields'] = $this->get_field('sales_order_billing_terms_log');
		set_page('logs/sales_order_billing_terms',$data);
	}

	function sales_order_billing_terms_datatable(){
		$config['table'] = 'sales_order_billing_terms_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_id', 'cal_code', 'narration', 'cal_definition', 'percentage', 'value', 'gl_acc', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->cal_code;
			$row[] = $tbl_row->narration;
			$row[] = $tbl_row->cal_definition;
			$row[] = $tbl_row->percentage;
			$row[] = $tbl_row->value;
			$row[] = $tbl_row->gl_acc;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_buyers(){
		$data['fields'] = $this->get_field('sales_order_buyers_log');
		set_page('logs/sales_order_buyers',$data);
	}

	function sales_order_buyers_datatable(){
		$config['table'] = 'sales_order_buyers_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_id', 'party_id', 'party_code', 'party_name', 'address', 'city_id', 'state_id', 'country_id', 'fax_no', 'email_id', 'website', 'gst_no', 'pan_no', 'tin_vat_no', 'tin_cst_no', 'ecc_no', 'pincode', 'phone_no', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->party_code;
			$row[] = $tbl_row->party_name;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->city_id;
			$row[] = $tbl_row->state_id;
			$row[] = $tbl_row->country_id;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->website;
			$row[] = $tbl_row->gst_no;
			$row[] = $tbl_row->pan_no;
			$row[] = $tbl_row->tin_vat_no;
			$row[] = $tbl_row->tin_cst_no;
			$row[] = $tbl_row->ecc_no;
			$row[] = $tbl_row->pincode;
			$row[] = $tbl_row->phone_no;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_cal_code_definition(){
		$data['fields'] = $this->get_field('sales_order_cal_code_definition_log');
		set_page('logs/sales_order_cal_code_definition',$data);
	}

	function sales_order_cal_code_definition_datatable(){
		$config['table'] = 'sales_order_cal_code_definition_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'billing_terms_detail_id', 'sales_order_id', 'is_first_element', 'cal_operation', 'cal_code_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->billing_terms_detail_id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->is_first_element;
			$row[] = $tbl_row->cal_operation;
			$row[] = $tbl_row->cal_code_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_item(){
		$data['fields'] = $this->get_field('sales_order_item_log');
		set_page('logs/sales_order_item',$data);
	}

	function sales_order_item_datatable(){
		$config['table'] = 'sales_order_item_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_id', 'party_name', 'party_type', 'address', 'city', 'state', 'country', 'fax_no', 'pin_code', 'phone_no', 'email_id', 'website', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->party_name;
			$row[] = $tbl_row->party_type;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->city;
			$row[] = $tbl_row->state;
			$row[] = $tbl_row->country;
			$row[] = $tbl_row->fax_no;
			$row[] = $tbl_row->pin_code;
			$row[] = $tbl_row->phone_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->website;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_items(){
		$data['fields'] = $this->get_field('sales_order_items_log');
		set_page('logs/sales_order_items',$data);
	}

	function sales_order_items_datatable(){
		$config['table'] = 'sales_order_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'item_id', 'sales_order_id', 'item_code', 'item_category_id', 'item_name', 'item_desc', 'item_description', 'add_description', 'detail_description', 'drawing_number', 'drawing_revision', 'uom_id', 'uom', 'total_amount', 'item_note', 'quantity', 'disc_per', 'rate', 'disc_value', 'amount', 'net_amount', 'item_status_id', 'item_status', 'cust_part_no', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->item_id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->item_code;
			$row[] = $tbl_row->item_category_id;
			$row[] = $tbl_row->item_name;
			$row[] = $tbl_row->item_desc;
			$row[] = $tbl_row->item_description;
			$row[] = $tbl_row->add_description;
			$row[] = $tbl_row->detail_description;
			$row[] = $tbl_row->drawing_number;
			$row[] = $tbl_row->drawing_revision;
			$row[] = $tbl_row->uom_id;
			$row[] = $tbl_row->uom;
			$row[] = $tbl_row->total_amount;
			$row[] = $tbl_row->item_note;
			$row[] = $tbl_row->quantity;
			$row[] = $tbl_row->disc_per;
			$row[] = $tbl_row->rate;
			$row[] = $tbl_row->disc_value;
			$row[] = $tbl_row->amount;
			$row[] = $tbl_row->net_amount;
			$row[] = $tbl_row->item_status_id;
			$row[] = $tbl_row->item_status;
			$row[] = $tbl_row->cust_part_no;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order(){
		$data['fields'] = $this->get_field('sales_order_log');
		set_page('logs/sales_order',$data);
	}

	function sales_order_datatable(){
		$config['table'] = 'sales_order_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'quatation_id', 'quotation_id', 'sales_order_no', 'sales_to_party_id', 'ship_to_party_id', 'branch_id', 'billing_terms_id', 'sales_order_date', 'committed_date', 'currency_id', 'project_id', 'kind_attn_id', 'contact_no', 'cust_po_no', 'email_id', 'po_date', 'sales_order_pref_id', 'conversation_rate', 'sales_id', 'sales_order_stage_id', 'sales_order_status_id', 'order_type', 'lead_provider_id', 'buyer_detail_id', 'note_detail_id', 'login_detail_id', 'transportation_by_id', 'transport_amount', 'foundation_drawing_required_id', 'loading_by_id', 'inspection_required_id', 'unloading_by_id', 'erection_commissioning_id', 'road_insurance_by_id', 'for_required_id', 'review_date', 'mach_deli_min_weeks', 'mach_deli_max_weeks', 'mach_deli_min_weeks_date', 'mach_deli_max_weeks_date', 'sales_order_validaty', 'sales_order_quotation_delivery', 'mode_of_shipment_name', 'mode_of_shipment_truck_number', 'supplier_payment_terms', 'received_payment', 'received_payment_date', 'received_payment_type', 'terms_condition_purchase', 'isApproved', 'pdf_url', 'taxes_data', 'more_items_data', 'due_date', 'created_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->quatation_id;
			$row[] = $tbl_row->quotation_id;
			$row[] = $tbl_row->sales_order_no;
			$row[] = $tbl_row->sales_to_party_id;
			$row[] = $tbl_row->ship_to_party_id;
			$row[] = $tbl_row->branch_id;
			$row[] = $tbl_row->billing_terms_id;
			$row[] = $tbl_row->sales_order_date;
			$row[] = $tbl_row->committed_date;
			$row[] = $tbl_row->currency_id;
			$row[] = $tbl_row->project_id;
			$row[] = $tbl_row->kind_attn_id;
			$row[] = $tbl_row->contact_no;
			$row[] = $tbl_row->cust_po_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->po_date;
			$row[] = $tbl_row->sales_order_pref_id;
			$row[] = $tbl_row->conversation_rate;
			$row[] = $tbl_row->sales_id;
			$row[] = $tbl_row->sales_order_stage_id;
			$row[] = $tbl_row->sales_order_status_id;
			$row[] = $tbl_row->order_type;
			$row[] = $tbl_row->lead_provider_id;
			$row[] = $tbl_row->buyer_detail_id;
			$row[] = $tbl_row->note_detail_id;
			$row[] = $tbl_row->login_detail_id;
			$row[] = $tbl_row->transportation_by_id;
			$row[] = $tbl_row->transport_amount;
			$row[] = $tbl_row->foundation_drawing_required_id;
			$row[] = $tbl_row->loading_by_id;
			$row[] = $tbl_row->inspection_required_id;
			$row[] = $tbl_row->unloading_by_id;
			$row[] = $tbl_row->erection_commissioning_id;
			$row[] = $tbl_row->road_insurance_by_id;
			$row[] = $tbl_row->for_required_id;
			$row[] = $tbl_row->review_date;
			$row[] = $tbl_row->mach_deli_min_weeks;
			$row[] = $tbl_row->mach_deli_max_weeks;
			$row[] = $tbl_row->mach_deli_min_weeks_date;
			$row[] = $tbl_row->mach_deli_max_weeks_date;
			$row[] = $tbl_row->sales_order_validaty;
			$row[] = $tbl_row->sales_order_quotation_delivery;
			$row[] = $tbl_row->mode_of_shipment_name;
			$row[] = $tbl_row->mode_of_shipment_truck_number;
			$row[] = $tbl_row->supplier_payment_terms;
			$row[] = $tbl_row->received_payment;
			$row[] = $tbl_row->received_payment_date;
			$row[] = $tbl_row->received_payment_type;
			$row[] = $tbl_row->terms_condition_purchase;
			$row[] = $tbl_row->isApproved;
			$row[] = $tbl_row->pdf_url;
			$row[] = $tbl_row->taxes_data;
			$row[] = $tbl_row->more_items_data;
			$row[] = $tbl_row->due_date;
			$row[] = $tbl_row->created_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_logins(){
		$data['fields'] = $this->get_field('sales_order_logins_log');
		set_page('logs/sales_order_logins',$data);
	}

	function sales_order_logins_datatable(){
		$config['table'] = 'sales_order_logins_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_id', 'created_by', 'created_by_id', 'created_date_time', 'last_modified_by', 'last_modified_by_id', 'modified_date_time', 'prepared_by', 'prepared_by_id', 'prepared_date_time', 'approved_by', 'approved_by_id', 'approved_date_time', 'stage_id', 'stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->created_by_id;
			$row[] = $tbl_row->created_date_time;
			$row[] = $tbl_row->last_modified_by;
			$row[] = $tbl_row->last_modified_by_id;
			$row[] = $tbl_row->modified_date_time;
			$row[] = $tbl_row->prepared_by;
			$row[] = $tbl_row->prepared_by_id;
			$row[] = $tbl_row->prepared_date_time;
			$row[] = $tbl_row->approved_by;
			$row[] = $tbl_row->approved_by_id;
			$row[] = $tbl_row->approved_date_time;
			$row[] = $tbl_row->stage_id;
			$row[] = $tbl_row->stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_notes(){
		$data['fields'] = $this->get_field('sales_order_notes_log');
		set_page('logs/sales_order_notes',$data);
	}

	function sales_order_notes_datatable(){
		$config['table'] = 'sales_order_notes_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_id', 'header', 'note', 'note2', 'internal_note', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_id;
			$row[] = $tbl_row->header;
			$row[] = $tbl_row->note;
			$row[] = $tbl_row->note2;
			$row[] = $tbl_row->internal_note;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_pref(){
		$data['fields'] = $this->get_field('sales_order_pref_log');
		set_page('logs/sales_order_pref',$data);
	}

	function sales_order_pref_datatable(){
		$config['table'] = 'sales_order_pref_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_pref', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_pref;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_stage(){
		$data['fields'] = $this->get_field('sales_order_stage_log');
		set_page('logs/sales_order_stage',$data);
	}

	function sales_order_stage_datatable(){
		$config['table'] = 'sales_order_stage_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_order_status(){
		$data['fields'] = $this->get_field('sales_order_status_log');
		set_page('logs/sales_order_status',$data);
	}

	function sales_order_status_datatable(){
		$config['table'] = 'sales_order_status_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_order_status', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_order_status;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_status(){
		$data['fields'] = $this->get_field('sales_status_log');
		set_page('logs/sales_status',$data);
	}

	function sales_status_datatable(){
		$config['table'] = 'sales_status_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_status', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_status;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sales_type(){
		$data['fields'] = $this->get_field('sales_type_log');
		set_page('logs/sales_type',$data);
	}

	function sales_type_datatable(){
		$config['table'] = 'sales_type_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sales_type', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sales_type;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function service_followup_history(){
		$data['fields'] = $this->get_field('service_followup_history_log');
		set_page('logs/service_followup_history',$data);
	}

	function service_followup_history_datatable(){
		$config['table'] = 'service_followup_history_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'service_id', 'followup_date', 'history', 'followup_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->service_id;
			$row[] = $tbl_row->followup_date;
			$row[] = $tbl_row->history;
			$row[] = $tbl_row->followup_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function service_items(){
		$data['fields'] = $this->get_field('service_items_log');
		set_page('logs/service_items',$data);
	}

	function service_items_datatable(){
		$config['table'] = 'service_items_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'service_id', 'challan_id', 'challan_item_id', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->service_id;
			$row[] = $tbl_row->challan_id;
			$row[] = $tbl_row->challan_item_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function service(){
		$data['fields'] = $this->get_field('service_log');
		set_page('logs/service',$data);
	}

	function service_datatable(){
		$config['table'] = 'service_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'service_id', 'party_id', 'service_date', 'note', 'paid_free', 'charge', 'service_provide_by', 'service_received_by', 'service_type', 'created_by', 'updated_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->service_id;
			$row[] = $tbl_row->party_id;
			$row[] = $tbl_row->service_date;
			$row[] = $tbl_row->note;
			$row[] = $tbl_row->paid_free;
			$row[] = $tbl_row->charge;
			$row[] = $tbl_row->service_provide_by;
			$row[] = $tbl_row->service_received_by;
			$row[] = $tbl_row->service_type;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function staff(){
		$data['fields'] = $this->get_field('staff_log');
		set_page('logs/staff',$data);
	}

	function staff_datatable(){
		$config['table'] = 'staff_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'staff_id', 'interview_id', 'name', 'birth_date', 'gender', 'blood_group', 'father_name', 'mother_name', 'married_status', 'husband_wife_name', 'marriage_date', 'contact_no', 'email', 'pass', 'image', 'address', 'permanent_address', 'qualification', 'designation_id', 'department_id', 'grade_id', 'date_of_joining', 'date_of_leaving', 'salary', 'allow_pf', 'basic_pay', 'house_rent', 'traveling_allowance', 'education_allowance', 'pf_amount', 'bonus_amount', 'other_allotment', 'medical_reimbursement', 'professional_tax', 'monthly_gross', 'cost_to_company', 'pf_employee', 'bank_name', 'bank_acc_no', 'esic_no', 'pan_no', 'uan_id', 'pf_no', 'emp_no', 'emp_id', 'esic_id', 'visitor_last_message_id', 'user_type', 'mailbox_email', 'mailbox_password', 'pdf_url', 'active', 'is_login', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->staff_id;
			$row[] = $tbl_row->interview_id;
			$row[] = $tbl_row->name;
			$row[] = $tbl_row->birth_date;
			$row[] = $tbl_row->gender;
			$row[] = $tbl_row->blood_group;
			$row[] = $tbl_row->father_name;
			$row[] = $tbl_row->mother_name;
			$row[] = $tbl_row->married_status;
			$row[] = $tbl_row->husband_wife_name;
			$row[] = $tbl_row->marriage_date;
			$row[] = $tbl_row->contact_no;
			$row[] = $tbl_row->email;
			$row[] = $tbl_row->pass;
			$row[] = $tbl_row->image;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->permanent_address;
			$row[] = $tbl_row->qualification;
			$row[] = $tbl_row->designation_id;
			$row[] = $tbl_row->department_id;
			$row[] = $tbl_row->grade_id;
			$row[] = $tbl_row->date_of_joining;
			$row[] = $tbl_row->date_of_leaving;
			$row[] = $tbl_row->salary;
			$row[] = $tbl_row->allow_pf;
			$row[] = $tbl_row->basic_pay;
			$row[] = $tbl_row->house_rent;
			$row[] = $tbl_row->traveling_allowance;
			$row[] = $tbl_row->education_allowance;
			$row[] = $tbl_row->pf_amount;
			$row[] = $tbl_row->bonus_amount;
			$row[] = $tbl_row->other_allotment;
			$row[] = $tbl_row->medical_reimbursement;
			$row[] = $tbl_row->professional_tax;
			$row[] = $tbl_row->monthly_gross;
			$row[] = $tbl_row->cost_to_company;
			$row[] = $tbl_row->pf_employee;
			$row[] = $tbl_row->bank_name;
			$row[] = $tbl_row->bank_acc_no;
			$row[] = $tbl_row->esic_no;
			$row[] = $tbl_row->pan_no;
			$row[] = $tbl_row->uan_id;
			$row[] = $tbl_row->pf_no;
			$row[] = $tbl_row->emp_no;
			$row[] = $tbl_row->emp_id;
			$row[] = $tbl_row->esic_id;
			$row[] = $tbl_row->visitor_last_message_id;
			$row[] = $tbl_row->user_type;
			$row[] = $tbl_row->mailbox_email;
			$row[] = $tbl_row->mailbox_password;
			$row[] = $tbl_row->pdf_url;
			$row[] = $tbl_row->active;
			$row[] = $tbl_row->is_login;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function staff_roles(){
		$data['fields'] = $this->get_field('staff_roles_log');
		set_page('logs/staff_roles',$data);
	}

	function staff_roles_datatable(){
		$config['table'] = 'staff_roles_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'staff_id', 'module_id', 'role_id', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->staff_id;
			$row[] = $tbl_row->module_id;
			$row[] = $tbl_row->role_id;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function state(){
		$data['fields'] = $this->get_field('state_log');
		set_page('logs/state',$data);
	}

	function state_datatable(){
		$config['table'] = 'state_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'state_id', 'country_id', 'state', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->state_id;
			$row[] = $tbl_row->country_id;
			$row[] = $tbl_row->state;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function sub_group(){
		$data['fields'] = $this->get_field('sub_group_log');
		set_page('logs/sub_group',$data);
	}

	function sub_group_datatable(){
		$config['table'] = 'sub_group_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'sub_item_group', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->sub_item_group;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function supplier(){
		$data['fields'] = $this->get_field('supplier_log');
		set_page('logs/supplier',$data);
	}

	function supplier_datatable(){
		$config['table'] = 'supplier_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'supplier_id', 'supplier_name', 'address', 'city_id', 'state_id', 'country_id', 'phone_no', 'email_id', 'created_by', 'updated_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->supplier_id;
			$row[] = $tbl_row->supplier_name;
			$row[] = $tbl_row->address;
			$row[] = $tbl_row->city_id;
			$row[] = $tbl_row->state_id;
			$row[] = $tbl_row->country_id;
			$row[] = $tbl_row->phone_no;
			$row[] = $tbl_row->email_id;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->updated_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function terms_condition_detail(){
		$data['fields'] = $this->get_field('terms_condition_detail_log');
		set_page('logs/terms_condition_detail',$data);
	}

	function terms_condition_detail_datatable(){
		$config['table'] = 'terms_condition_detail_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'code', 'group_name1', 'group_name2', 'description', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->code;
			$row[] = $tbl_row->group_name1;
			$row[] = $tbl_row->group_name2;
			$row[] = $tbl_row->description;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function terms_condition_group(){
		$data['fields'] = $this->get_field('terms_condition_group_log');
		set_page('logs/terms_condition_group',$data);
	}

	function terms_condition_group_datatable(){
		$config['table'] = 'terms_condition_group_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'code', 'description', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->code;
			$row[] = $tbl_row->description;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function terms_condition_template(){
		$data['fields'] = $this->get_field('terms_condition_template_log');
		set_page('logs/terms_condition_template',$data);
	}

	function terms_condition_template_datatable(){
		$config['table'] = 'terms_condition_template_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'template_name', 'group_name', 'terms_condition', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->template_name;
			$row[] = $tbl_row->group_name;
			$row[] = $tbl_row->terms_condition;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function terms_group(){
		$data['fields'] = $this->get_field('terms_group_log');
		set_page('logs/terms_group',$data);
	}

	function terms_group_datatable(){
		$config['table'] = 'terms_group_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'terms_group', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->terms_group;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function transportation_by(){
		$data['fields'] = $this->get_field('transportation_by_log');
		set_page('logs/transportation_by',$data);
	}

	function transportation_by_datatable(){
		$config['table'] = 'transportation_by_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'transportation_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->transportation_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function unloading_by(){
		$data['fields'] = $this->get_field('unloading_by_log');
		set_page('logs/unloading_by',$data);
	}

	function unloading_by_datatable(){
		$config['table'] = 'unloading_by_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'unloading_by', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->unloading_by;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function uom(){
		$data['fields'] = $this->get_field('uom_log');
		set_page('logs/uom',$data);
	}

	function uom_datatable(){
		$config['table'] = 'uom_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'uom', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->uom;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function visitors(){
		$data['fields'] = $this->get_field('visitors_log');
		set_page('logs/visitors',$data);
	}

	function visitors_datatable(){
		$config['table'] = 'visitors_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'name', 'phone', 'email', 'ip', 'from_address', 'city', 'country', 'currency', 'others', 'session_id', 'is_disconnect_mail_sent', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->name;
			$row[] = $tbl_row->phone;
			$row[] = $tbl_row->email;
			$row[] = $tbl_row->ip;
			$row[] = $tbl_row->from_address;
			$row[] = $tbl_row->city;
			$row[] = $tbl_row->country;
			$row[] = $tbl_row->currency;
			$row[] = $tbl_row->others;
			$row[] = $tbl_row->session_id;
			$row[] = $tbl_row->is_disconnect_mail_sent;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function website_modules(){
		$data['fields'] = $this->get_field('website_modules_log');
		set_page('logs/website_modules',$data);
	}

	function website_modules_datatable(){
		$config['table'] = 'website_modules_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'title', 'table_name', 'main_module', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->title;
			$row[] = $tbl_row->table_name;
			$row[] = $tbl_row->main_module;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function weekly_holiday(){
		$data['fields'] = $this->get_field('weekly_holiday_log');
		set_page('logs/weekly_holiday',$data);
	}

	function weekly_holiday_datatable(){
		$config['table'] = 'weekly_holiday_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'day', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->day;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function yearly_leaves(){
		$data['fields'] = $this->get_field('yearly_leaves_log');
		set_page('logs/yearly_leaves',$data);
	}

	function yearly_leaves_datatable(){
		$config['table'] = 'yearly_leaves_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'leave_id', 'user_id', 'date', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->leave_id;
			$row[] = $tbl_row->user_id;
			$row[] = $tbl_row->date;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function invoice_logins(){
		$data['fields'] = $this->get_field('invoice_logins_log');
		set_page('logs/invoice_logins',$data);
	}

	function invoice_logins_datatable(){
		$config['table'] = 'invoice_logins_log';
		$config['select'] = '*';
		$config['column_order'] = array(null, 'operation_type', 'id', 'invoice_id', 'created_by', 'created_by_id', 'created_date_time', 'last_modified_by', 'last_modified_by_id', 'modified_date_time', 'prepared_by', 'prepared_by_id', 'prepared_date_time', 'approved_by', 'approved_by_id', 'approved_date_time', 'stage_id', 'stage', 'created_at', 'updated_at');
		$config['order'] = array('updated_at' => 'desc');
		$this->load->library('log_datatable', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $tbl_row) {
			$row = array();
			$action = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$action .= '<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('logs/delete/' . $tbl_row->log_id) . '"><i class="fa fa-trash"></i></a>';
			$row[] = $action;


			$row[] = $tbl_row->operation_type;
			$row[] = $tbl_row->id;
			$row[] = $tbl_row->invoice_id;
			$row[] = $tbl_row->created_by;
			$row[] = $tbl_row->created_by_id;
			$row[] = $tbl_row->created_date_time;
			$row[] = $tbl_row->last_modified_by;
			$row[] = $tbl_row->last_modified_by_id;
			$row[] = $tbl_row->modified_date_time;
			$row[] = $tbl_row->prepared_by;
			$row[] = $tbl_row->prepared_by_id;
			$row[] = $tbl_row->prepared_date_time;
			$row[] = $tbl_row->approved_by;
			$row[] = $tbl_row->approved_by_id;
			$row[] = $tbl_row->approved_date_time;
			$row[] = $tbl_row->stage_id;
			$row[] = $tbl_row->stage;
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->created_at));
			$row[] = date('d-m-Y H:i:s', strtotime($tbl_row->updated_at));

			$data[] = $row;
		}
		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_filtered(),
			'data' => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	function get_field($tbl_name){
		$data = array();
		$result = $this->db_b->list_fields($tbl_name);
		foreach($result as $key => $val){
			if($val == 'log_id'){
				$val = 'Action';
			}
			$account_field = str_replace('_', ' ', $val);
			$data[]['field'] = ucwords($account_field);
		}
		return $data;
	}

	function delete($id){
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$result = $this->db_b->delete($table,array($id_name => $id));
		if($result){
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Deleted Successfully');
		}
	}

}
