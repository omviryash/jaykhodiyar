<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

class Twilio_whatsapp_demo extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('Crud', 'crud');
        $this->load->model('chatmodule', 'chat_module');
        $this->now_time = date('Y-m-d H:i:s');
    }
    
    function test(){
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $data = array();
        set_page('twilio_whatsapp_demo', $data);
    }
    
    function whatsapp(){
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $data = array();
        $from_numbers = $this->crud->getFromSQL("SELECT `message_from` FROM `twilio_webhook_demo` WHERE `webhook_type` = 'message_comes' AND `message_from` != '' GROUP BY `message_from`");
        $from_numbers_arr = array();
        foreach ($from_numbers as $from_number){
            $message_from = str_replace('+91', '', $from_number->message_from);
            $account_name = $this->crud->getFromSQL("SELECT `account_name` FROM `account` WHERE `account_mobile` LIKE '%".$message_from."%'");
            $from_number->account_name = '';
            if(!empty($account_name)){
                $from_number->account_name = $account_name[0]->account_name;
            }
            $from_numbers_arr[] = $from_number;
        }
        $data['from_numbers'] = $from_numbers_arr;
        set_page('twilio_whatsapp', $data);
    }
    
    function get_conversations($conversation_text = ''){
        $data = array();
        $sql_query = "SELECT `message_from` FROM `twilio_webhook_demo` ";
        $sql_query .= " WHERE `webhook_type` = 'message_comes' AND `message_from` != '' ";
        $sql_query .= " GROUP BY `message_from`";
        $from_numbers = $this->crud->getFromSQL($sql_query);
        $from_numbers_arr = array();
        foreach ($from_numbers as $from_number){
            $message_from = str_replace('+91', '', $from_number->message_from);
            $account_name = $this->crud->getFromSQL("SELECT `account_name` FROM `account` WHERE `account_mobile` LIKE '%".$message_from."%'");
            $from_number->account_name = '';
            if(!empty($account_name)){
                $from_number->account_name = $account_name[0]->account_name;
            }
            $from_numbers_arr[] = $from_number;
        }
        echo json_encode($from_numbers_arr);
        exit;
    }
    
    function pre_webhook(){
        $webhook_content = json_encode($_REQUEST);
        $data_array = array();
        $data_array['webhook_type'] = 'pre';
        $data_array['webhook_content'] = $webhook_content;
        $data_array['created_at'] = $this->now_time;
        $data_array['created_by'] = '1';
        $data_array['updated_at'] = $this->now_time;
        $data_array['updated_by'] = '1';
        $this->crud->insert('twilio_webhook_demo', $data_array);
    }
    
    function post_webhook(){
        $webhook_content = json_encode($_REQUEST);
        $data_array = array();
        $data_array['webhook_type'] = 'post';
        $data_array['webhook_content'] = $webhook_content;
        $data_array['created_at'] = $this->now_time;
        $data_array['created_by'] = '1';
        $data_array['updated_at'] = $this->now_time;
        $data_array['updated_by'] = '1';
        $this->crud->insert('twilio_webhook_demo', $data_array);
    }
    
    function message_comes_in_webhook(){
        $message_from = null;
        if(isset($_REQUEST['From'])){
            $message_from = $_REQUEST['From'];
            $message_from = explode(':', $message_from);
            $message_from = $message_from[1];
        }
        $message_to = null;
        if(isset($_REQUEST['To'])){
            $message_to = $_REQUEST['To'];
            $message_to = explode(':', $message_to);
            $message_to = $message_to[1];
        }
        $message_body = null;
        if(isset($_REQUEST['Body'])){
            $message_body = $_REQUEST['Body'];
        }
        $webhook_content = json_encode($_REQUEST);
        $data_array = array();
        $data_array['webhook_type'] = 'message_comes';
        $data_array['webhook_content'] = $webhook_content;
        $data_array['message_from'] = $message_from;
        $data_array['message_to'] = $message_to;
        $data_array['message_body'] = $message_body;
        $data_array['created_at'] = $this->now_time;
        $data_array['created_by'] = '1';
        $data_array['updated_at'] = $this->now_time;
        $data_array['updated_by'] = '1';
        $result = $this->crud->insert('twilio_webhook_demo', $data_array);
        if($result){
            $client = new Client(new Version1X(SERVER_REQUEST_SCHEME . '://' . HTTP_HOST . ':' . PORT_NUMBER));
            $client->initialize();
            $client->emit('message_comes_in_webhook', []);
            $client->close();
        }
    }
    
    function whatsapp_to_chat_message_comes_in_webhook(){
        $message_from = null;
        if(isset($_REQUEST['From'])){
            $message_from = $_REQUEST['From'];
            $message_from = explode(':', $message_from);
            $message_from = $message_from[1];
        }
        $message_to = null;
        if(isset($_REQUEST['To'])){
            $message_to = $_REQUEST['To'];
            $message_to = explode(':', $message_to);
            $message_to = $message_to[1];
        }
        $message_body = null;
        if(isset($_REQUEST['Body'])){
            $message_body = $_REQUEST['Body'];
        }

        $visitor_name = $message_from;
        $visitor_phone = $message_from;
        $visitor_email = $message_from;
        $session_id = $this->session->userdata('__ci_last_regenerate');
        $chat_through = '2'; // Chat Through WhatsApp
        $visitor_id = $this->chat_module->register_visitor($visitor_name, $visitor_phone, $visitor_email, $session_id, $chat_through);
        $message = $message_body;
        if (!$this->session->userdata('is_visitor_logged_in')) {
            $vis_array = array(
                'name' => $visitor_name,
                'email' => $visitor_email,
                'phone' => $visitor_phone,
                'visitor_id' => $visitor_id
            );
            $this->session->set_userdata('is_visitor_logged_in', $vis_array);
        }
        if(isset($_REQUEST['NumMedia']) && $_REQUEST['NumMedia'] > 0){

            $NumMedia = $_REQUEST['NumMedia'];
            for($i = 0; $i < $NumMedia; $i++){
                $message = '<a href="' . $_REQUEST['MediaUrl' . $i] .'" target="_blank">Attachment</a>';
                $lastmsgid = $this->chat_module->send_visitor_message($visitor_id, $message);
            }
        } else {
            $lastmsgid = $this->chat_module->send_visitor_message($visitor_id, $message);
        }
        $visitor_name = $visitor_name == '' ? 'Visitor' : $visitor_name;

        $visitor_data = $this->chat_module->fetch_visitor_by_id($visitor_id);
        $from_session_id = $visitor_data->session_id;
        //$from_name = $visitor_data->name ? $visitor_data->name.'(Visitor)' : 'Visitor';
        if ($visitor_data->name == '') {
            $from_name = 'Visitor';
        } else {
            $from_name = $visitor_data->name . ' (Visitor)';
        }
        $from_image = 'default.png';
        $date_time = $this->chat_module->get_val_by_id('message', 'date_time', 'id', $lastmsgid);

        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME . '://' . HTTP_HOST . ':' . PORT_NUMBER));
        $client->initialize();
        $client->emit('new_message_visitor_to_client', ['to_id' => 0, 'from_id' => $visitor_id, 'from_session_id' => $from_session_id, 'from_table' => 'visitor', 'from_name' => $from_name, 'from_image' => $from_image, 'msg' => $message, 'date_time' => $date_time, 'chat_html' => $chat_html]);
        $client->close();
    }

}
