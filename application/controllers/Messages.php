<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Messages
 */
class Messages extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
        $this->load->model('Chatmodule','chatmodule');
		$this->load->model('Crud', 'crud');
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
	}
	public function index($from_session_id = 0)
	{	$staff = $this->chatmodule->getAllAgents($_SESSION['is_logged_in']['staff_id']);
//		$visitors = $this->chatmodule->getAllVisitors();
        $currunt_date = strtotime(date('Y-m-d'));
        $prev_date = strtotime(' -'.OLD_VISITOR_CHAT_DAYS.' days', $currunt_date);
        $prev_date = date('Y-m-d', $prev_date);
        $visitors = $this->crud->get_all_with_where('visitors', 'created_at', 'DESC', array('created_at >=' => $prev_date));
	 	$chat_roles = $this->crud->getUserChatRoleIDS($_SESSION['is_logged_in']['staff_id']);
		set_page('messages',array('agents'=>$staff,'visitors'=>$visitors,'from_session_id'=>$from_session_id,'chat_roles'=>$chat_roles));
	}
	public function getUserMsg()
	{
		$logged_in = $this->session->userdata("is_logged_in");
		$staff_id = $logged_in['staff_id'];
		if(isset($logged_in['lastid']))
		{
		  $lastid = $logged_in['lastid'];
		}
		else
		{
		  $lastid = 0;
		}
		$userid = $_POST['userid'];
		
		$messages = $this->chatmodule->getAllMessage($staff_id,$lastid,$userid);
		if($messages)
		{
		  if(isset($logged_in['lastid']))
		  {
			$logged_in['lastid'] = $messages[count($messages) - 1]['id'];
		  }
		  else
		  {
			$logged_in['lastid'] = $messages[count($messages) - 1]['id'];
		  }
		  $this->session->set_userdata('is_logged_in', $logged_in);
		}
		echo json_encode($messages);exit;
	}

	public function getScrollUserMsg()
	{
		$logged_in = $this->session->userdata("is_logged_in");
		$staff_id = $logged_in['staff_id'];
		if(isset($logged_in['lastid']))
		{
		  $lastid = $logged_in['lastid'];
		}
		else
		{
		  $lastid = 0;
		}
		$userid = $_POST['userid'];
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		
		$messages = $this->chatmodule->getScrollMessage($staff_id,$lastid,$userid,$limit,$offset);
		if($messages)
		{
		  if(isset($logged_in['lastid']))
		  {
			$logged_in['lastid'] = $messages[count($messages) - 1]['id'];
		  }
		  else
		  {
			$logged_in['lastid'] = $messages[count($messages) - 1]['id'];
		  }
		  $this->session->set_userdata('is_logged_in', $logged_in);
		}
		echo json_encode($messages);exit;
	}
	
	public function getVisitorMsg()
	{
		$logged_in = $this->session->userdata("is_logged_in");
		$staff_id = $logged_in['staff_id'];		
		$userid = $_POST['session_id'];
		
		$messages = $this->chatmodule->getAllVisitorMessage($staff_id,$userid);
		if($messages)
		{
		  if(isset($logged_in['lastid']))
		  {
			$logged_in['lastid'] = $messages[count($messages) - 1]['id'];
		  }
		  else
		  {
			$logged_in['lastid'] = $messages[count($messages) - 1]['id'];
		  }
		  $this->session->set_userdata('is_logged_in', $logged_in);
		}
		echo json_encode($messages);exit;
	}

	function agent($agent_id){
		$staff = $this->chatmodule->getAllAgents($_SESSION['is_logged_in']['staff_id']);
		$visitors = $this->chatmodule->getAllVisitors();
		$agent_name = $this->db->select('name')->from('staff')->where('staff_id',$agent_id)->get()->row()->name;
		set_page('messages',array('agents'=>$staff,'visitors'=>$visitors,'agent_id'=>$agent_id,'agent_name'=>$agent_name));
	}

	public function getStaffById()
	{		
		$staffdata = $this->chatmodule->getStaffById($_POST['userid']);
		echo json_encode($staffdata);exit;
	}
	public function getVisitorById()
	{		
		$visitordata = $this->chatmodule->getVisitorById($_POST['userid']);
		echo json_encode($visitordata);exit;
	}
	public function updateReadMsg()
	{		
		$res = $this->chatmodule->updateReadMsg($_POST['userid']);
		echo $res;exit;
	}
	public function updateVisitorReadMsg()
	{		
		$res = $this->chatmodule->updateVisitorReadMsg($_POST['userid']);
		echo $res;exit;
	}
	public function getUnreadMsg()
	{		
		$res = $this->chatmodule->getUnreadMsg($_POST['userid']);
		echo $res;exit;
	}
    
    function visitor_on_site(){
        set_page('visitor_on_site');
    }
    function staff_chat_report(){
        $data = array();
        $agents = $this->chatmodule->getAllAgents();
//        echo '<pre>'; print_r($agents); exit;
        foreach ($agents as $key => $agent){
            $agents[$key]['response_time'] = '';
            $visitor_response = $this->crud->get_columns_val_by_where('visitor_response', 'response_time', array('staff_id' => $agent['staff_id']));
            if(!empty($visitor_response)){
                $response_time = array_column($visitor_response, 'response_time');
                $average = date('H:i:s', array_sum(array_map('strtotime', $response_time)) / count($response_time));
                $agents[$key]['response_time'] = $average;
            }
        }
        $data['agents'] = $agents;
        $data['chat_roles'] = $this->crud->getUserChatRoleIDS($this->staff_id);
//        echo '<pre>'; print_r($data); exit;
        set_page('staff_chat_report', $data);
    }
    function chat_window(){
        set_page('chat_window');
    }
    
    function old_visitor_chat_by_date(){
        $data = array();
        $old_visitor_chat_date = date('Y-m-d', strtotime($_POST['old_visitor_chat_date']));
        $data['visitors'] = $this->crud->get_all_with_where('visitors', 'created_at', 'DESC', array('created_at >=' => $old_visitor_chat_date));
        $visitor_list_by_date_html = $this->load->view('shared/visitor_list_by_date_html', $data, true);
        echo json_encode(array('visitor_list_by_date_html' => $visitor_list_by_date_html));
        exit;
    }
}
