<?php

ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Dispatch extends CI_Controller {

    protected $staff_id = 1;

    function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud');
    }

    function challan() {
        
    }

    /**
     * Sales Order DataTable
     */
    function sales_order_datatable() {
        $config['table'] = 'sales_order s';
        $config['select'] = 's.id,sales.sales,q.quotation_no,q.enquiry_no,s.sales_order_no,s.sales_order_date,s.sales_order_status_id,p.party_name,p.phone_no,p.email_id';
        $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'sales', 'join_by' => 'sales.id = s.sales_id', 'join_type' => 'left');
        $config['column_order'] = array('q.enquiry_no', 'q.quotation_no', 'sales.sales', 's.sales_order_no', 's.sales_order_date', 'p.party_name', 'p.phone_no', 'p.email_id');
        $config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 'sales.sales', 's.sales_order_no', 's.sales_order_date', 'p.party_name', 'p.phone_no', 'p.email_id');
        $config['order'] = array('s.id' => 'desc');

        $config['wheres'][] = array('column_name' => 'p.active != ', 'column_value' => '0');

        if ($this->session->userdata('is_logged_in')['staff_id'] && strtolower($this->session->userdata('is_logged_in')['user_type']) != 'administrator') {
            $config['wheres'][] = array('column_name' => 's.created_by', 'column_value' => $this->session->userdata('is_logged_in')['staff_id']);
            $accessExport = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "export");
            $accessDomestic = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "domestic");
            if ($accessExport == 1 && $accessDomestic == 1) {
                
            } else if ($accessExport == 1) {
                $config['wheres'][] = array('column_name' => 's.sales_id', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 's.sales_id', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            }
        }

        if (!empty($_POST['request_from'])) {
            if ($_POST['request_from'] == 'challan') {
                $config['custom_where'] = "s.id NOT IN(SELECT sales_order_id FROM challans)";
            }
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        foreach ($list as $sales_order) {
            $row = array();
            $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->party_name</a>";
            $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->enquiry_no</a>";
            $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->quotation_no</a>";
            $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->sales_order_no</a>";
            $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->phone_no</a>";
            $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->email_id</a>";
            $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->sales</a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //echo $this->db->last_query();
        echo json_encode($output);
    }

    /**
     * Sales Order DataTable : Item Qty Wise
     */
    function sales_order_itemqtywise_datatable() {
        $config['table'] = 'sales_order_items si';
        $config['select'] = 'si.id as sales_order_item_id,si.item_name,si.quantity, s.id,sales.sales,q.quotation_no,q.enquiry_no,s.sales_order_no,s.sales_order_date,s.sales_order_status_id,p.party_name,p.phone_no,p.email_id';
        $config['joins'][] = array('join_table' => 'sales_order s', 'join_by' => 'si.sales_order_id = s.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'sales', 'join_by' => 'sales.id = s.sales_id', 'join_type' => 'left');
        $config['column_order'] = array('q.enquiry_no', 'q.quotation_no', 'sales.sales', 's.sales_order_no', 's.sales_order_date', 'p.party_name', 'p.phone_no', 'p.email_id');
        $config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 'sales.sales', 's.sales_order_no', 's.sales_order_date', 'p.party_name', 'p.phone_no', 'p.email_id');
        $config['order'] = array('s.id' => 'desc');

        $config['wheres'][] = array('column_name' => 'p.active != ', 'column_value' => '0');
        $config['wheres'][] = array('column_name' => 's.isApproved ', 'column_value' => '1');

        /* if($this->session->userdata('is_logged_in')['staff_id'] && strtolower($this->session->userdata('is_logged_in')['user_type']) != 'administrator') {
          $config['wheres'][] = array('column_name' => 's.created_by','column_value'=>$this->session->userdata('is_logged_in')['staff_id']);
          $accessExport = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "export");
          $accessDomestic = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "domestic");
          if ($accessExport == 1 && $accessDomestic == 1) {

          } else if ($accessExport == 1) {
          $config['wheres'][] = array('column_name' => 's.sales_id','column_value'=>PARTY_TYPE_EXPORT_ID);
          } else if ($accessDomestic == 1) {
          $config['wheres'][] = array('column_name' => 's.sales_id','column_value'=>PARTY_TYPE_DOMESTIC_ID);
          }
          }

          if(!empty($_POST['request_from'])) {
          if($_POST['request_from'] == 'challan') {
          $config['custom_where'] = "s.id NOT IN(SELECT sales_order_item_id FROM challans)";
          }
          } */

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $sales_order) {
            $return_data = $this->crud->get_result_where('challans', 'sales_order_item_id', $sales_order->sales_order_item_id);
            $return_data_count = count($return_data);
            $quantity = $sales_order->quantity - $return_data_count;
            for ($q_inc = 1; $q_inc <= $quantity; $q_inc++) {
                $row = array();
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->party_name . "</a>";
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->enquiry_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->quotation_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->sales_order_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->item_name . "</a>";
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->phone_no . "</a>";
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->email_id . "</a>";
                $row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='" . $sales_order->id . "' data-sales_order_item_id='" . $sales_order->sales_order_item_id . "'>" . $sales_order->sales . "</a>";
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //echo $this->db->last_query();
        echo json_encode($output);
    }

    function challan_edit($challan_id) {
        $challan_data = $this->get_challan_by_id($challan_id);
        $challan_data['created_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data['created_by']));
        $challan_data['created_at'] = substr($challan_data['created_at'], 8, 2) . '-' . substr($challan_data['created_at'], 5, 2) . '-' . substr($challan_data['created_at'], 0, 4);
        $challan_data['updated_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data['updated_by']));
        $challan_data['updated_at'] = substr($challan_data['updated_at'], 8, 2) . '-' . substr($challan_data['updated_at'], 5, 2) . '-' . substr($challan_data['updated_at'], 0, 4);
        $challan_data['challan_items'] = $this->get_challan_items($challan_id);
        $challan_data['item_category'] = $this->crud->get_all_records('item_category', 'id', 'asc');
        $challan_data['followup_history_data'] = $this->crud->get_followup_history_by_challan($challan_id);
        $challan_data['party_detail'] = $this->crud->get_column_value_by_party($challan_data['sales_to_party_id']);
        $challan_data['kind_attn_data'] = $this->getContactPersonByParty($challan_data['sales_to_party_id']);
        $challan_data['invoice_no'] = '';
        $challan_data['invoice_date'] = '';
        $invoice_data = $this->crud->get_row_by_id('invoices', array('challan_id' => $challan_data['id']));
        if (!empty($invoice_data)) {
            $challan_data['invoice_no'] = sprintf("%04d", $invoice_data[0]->invoice_no);
            $challan_data['invoice_date'] = date('d-m-Y', strtotime($invoice_data[0]->invoice_date));
        }

        if (!empty($challan_data['kind_attn_id'])) {
            $challan_data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $challan_data['kind_attn_id']));
            $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $challan_data['contact_person'][0]->designation_id));
            $department = $this->crud->get_row_by_id('department', array('department_id' => $challan_data['contact_person'][0]->department_id));
            $challan_data['contact_person'][0]->designation = '';
            if (!empty($designation)) {
                $challan_data['contact_person'][0]->designation = $designation[0]->designation;
            }
            $challan_data['contact_person'][0]->department = '';
            if (!empty($department)) {
                $challan_data['contact_person'][0]->department = $department[0]->department;
            }
        }

        $challan_data['motor_user'] = $this->crud->get_all_records('challan_item_user', 'user_name', 'asc');
        $challan_data['motor_make'] = $this->crud->get_all_records('challan_item_make', 'make_name', 'asc');
        $challan_data['motor_hp'] = $this->crud->get_all_records('challan_item_hp', 'hp_name', 'asc');
        $challan_data['motor_kw'] = $this->crud->get_all_records('challan_item_kw', 'kw_name', 'asc');
        $challan_data['motor_frequency'] = $this->crud->get_all_records('challan_item_frequency', 'frequency_name', 'asc');
        $challan_data['motor_rpm'] = $this->crud->get_all_records('challan_item_rpm', 'rpm_name', 'asc');
        $challan_data['motor_volts_cycles'] = $this->crud->get_all_records('challan_item_volts_cycles', 'volts_cycles_name', 'asc');
        $challan_data['gearbox_user'] = $challan_data['motor_user'];
        $challan_data['gearbox_make'] = $challan_data['motor_make'];
        $challan_data['gearbox_gear_type'] = $this->crud->get_all_records('challan_item_gear_type', 'gear_type_name', 'asc');
        $challan_data['gearbox_model'] = $this->crud->get_all_records('challan_item_model', 'model_name', 'asc');
        $challan_data['gearbox_ratio'] = $this->crud->get_all_records('challan_item_ratio', 'ratio_name', 'asc');

        //~ echo "<pre>";print_r($challan_data);die();
        if ($this->app_model->have_access_role(CHALLAN_MODULE_ID, "edit")) {
            set_page('dispatch/challan_edit', $challan_data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function challan_datatable() {
        $config['table'] = 'challans c';
        $config['select'] = 'c.id,p.party_name,p.phone_no,p.email_id,so.sales_order_no,q.quotation_no,q.enquiry_no, (select count(*) from challan_items where c.id = challan_id group by challan_id) as count_items,city.city,state.state,country.country';
        //$config['select'] = 'c.id,c.challan_no,p.party_name,p.phone_no,p.email_id,so.sales_order_no,q.quotation_no,q.enquiry_no';
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'q.id = so.quotation_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

        $config['column_order'] = array(null, 'c.id', 'so.id', 'q.id', 'q.enquiry_no', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
        $config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 'so.sales_order_no', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
        $config['order'] = array('c.id' => 'desc');
        $config['wheres'][] = array('column_name' => 'p.active != ', 'column_value' => '0');

        if (!empty($_POST['city_id']) && $_POST['city_id'] != 'null') {
            $config['wheres'][] = array('column_name' => 'p.city_id', 'column_value' => trim($_POST['city_id']));
        }

        if (!empty($_POST['state_id']) && $_POST['state_id'] != 'null') {
            $config['wheres'][] = array('column_name' => 'p.state_id', 'column_value' => trim($_POST['state_id']));
        }

        if (!empty($_POST['country_id']) && $_POST['country_id'] != 'null') {
            $config['wheres'][] = array('column_name' => 'p.country_id', 'column_value' => trim($_POST['country_id']));
        }

        if ($this->session->userdata('is_logged_in')['staff_id'] && strtolower($this->session->userdata('is_logged_in')['user_type']) != 'administrator') {
            $config['wheres'][] = array('column_name' => 's.created_by', 'column_value', $this->session->userdata('is_logged_in')['staff_id']);
            $accessExport = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "export");
            $accessDomestic = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "domestic");
            if ($accessExport == 1 && $accessDomestic == 1) {
                
            } else if ($accessExport == 1) {
                $config['wheres'][] = array('column_name' => 's.sales_id', 'column_value', PARTY_TYPE_EXPORT_ID);
            } else if ($accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 's.sales_id', 'column_value', PARTY_TYPE_DOMESTIC_ID);
            }
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();

        $role_delete = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "edit");
        foreach ($list as $sales_order) {
            $row = array();
            $action = '';

            if ($role_edit) {
                $action .= '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id) . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
            }

            if ($role_delete) {
                $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs"
				   data-href="' . base_url('dispatch/delete/' . $sales_order->id) . '"><i class="fa fa-trash"></i></a>';
            }

            if ($sales_order->count_items > 1) {
                $action .= ' | <a href="#" class="print_button btn-primary btn-xs show_itemprint_modal" id="' . $sales_order->id . '" title="Print Challan"><i class="fa fa-print"></i> Challan</a>';
                $action .= ' | <a href="#" class="print_button btn-primary btn-xs show_itemprintplat_modal" id="' . $sales_order->id . '" title="Print Plat"><i class="fa fa-print"></i> Print</a>';
            } else {
                $action .= ' | <a href="' . base_url('dispatch/challan_print/' . $sales_order->id) . '" target="_blank" class="btn-primary btn-xs" title="Print Challan"><i class="fa fa-print"></i> Challan</a>';
                $action .= ' | <a href="' . base_url('dispatch/testing_print/' . $sales_order->id) . '" target="_blank" class="btn-primary btn-xs" title="Print Testing"><i class="fa fa-print"></i> Testing</a>';
                $action .= ' | <a href="' . base_url('dispatch/challan_plat_print/' . $sales_order->id) . '" target="_blank" class="btn-primary btn-xs" title="Print Plat"><i class="fa fa-print"></i> Print</a>';
            }

            $row[] = $action;
            $invoice_no = $this->crud->get_id_by_val('invoices', 'invoice_no', 'challan_id', $sales_order->id);
            if (!empty($invoice_no)) {
                $invoice_no = sprintf("%04d", $invoice_no);
            }
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $invoice_no . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->id . '</a>';
            //$row[] = "<a href='javascript:void(0);' class='btn-feed-sales-order-data' data-sales_order_id='$sales_order->id'>$sales_order->challan_no</a>";
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->sales_order_no . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->quotation_no . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->enquiry_no . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->party_name . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->phone_no . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->email_id . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->city . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->state . '</a>';
            $row[] = '<a href="' . base_url('dispatch/challan_edit/' . $sales_order->id . '?view') . '" >' . $sales_order->country . '</a>';
            if ($sales_order->count_items > 0) {
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    /**
     * @param $challan_id
     */
    function challan_items($challan_id) {
        $this->db->select('*');
        $this->db->from('challan_items');
        $this->db->where('challan_id', $challan_id);
        $this->db->where('item_category_id', ITEM_CATEGORY_FINISHED_GOODS_ID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $item_data = $query->result();
            echo json_encode(array('success' => true, 'message' => 'Item loaded successfully!', 'item_data' => $item_data));
            exit();
        } else {
            echo json_encode(array('success' => false, 'message' => 'Please select item!'));
            exit();
        }
    }

    function getContactPersonByParty($party_id, $get_table_html = false) {
        $this->db->select('cp.contact_person_id,cp.priority,cp.name,cp.mobile_no,cp.phone_no,dept.department,desi.designation,cp.email');
        $this->db->from('contact_person cp');
        $this->db->join('department dept', 'dept.department_id = cp.department_id', 'left');
        $this->db->join('designation desi', 'desi.designation_id = cp.designation_id', 'left');
        $this->db->where('cp.party_id', $party_id);
        $this->db->where('cp.active !=', 0);
        $this->db->order_by('cp.priority,cp.contact_person_id');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            if ($get_table_html) {
                $html_response = '';
                foreach ($query->result() as $row) {
                    $html_response .= '<tr>';
                    $html_response .= '<td>' . $row->priority . '</td>';
                    $html_response .= '<td>' . $row->name . '</td>';
                    $html_response .= '<td>' . $row->email . '</td>';
                    $html_response .= '<td>' . $row->department . '</td>';
                    $html_response .= '<td>' . $row->designation . '</td>';
                    $html_response .= '<td>' . $row->phone_no . '</td>';
                    $html_response .= '</tr>';
                }
                return $html_response;
            } else {
                foreach ($query->result() as $row) {
                    $response[] = array(
                        'contact_person_id' => $row->contact_person_id,
                        'priority' => $row->priority,
                        'name' => $row->name,
                        'phone_no' => $row->phone_no,
                        'mobile_no' => $row->mobile_no,
                        'department' => $row->department,
                        'designation' => $row->designation,
                    );
                }
                return $response;
            }
        } else {
            return $response;
        }
    }

    function get_sales_order() {
        $id = $this->input->get_post("id");
        $sales_order_data = $this->crud->get_sales_order($id);
        if (!empty($sales_order_data->po_date)) {
            $sales_order_data->po_date = date('d-m-Y', strtotime($sales_order_data->po_date));
        }
        if (!empty($sales_order_data->sales_order_date)) {
            $sales_order_data->sales_order_date = date('d-m-Y', strtotime($sales_order_data->sales_order_date));
        }
        $sales_order_items = $this->get_sales_order_items($id);
        $sales_order_data->email_id = $sales_order_data->email_id;
        $sales_order_data->phone_no = $sales_order_data->phone_no;
        $party_contact_person = $this->crud->get_contact_person_by_party($sales_order_data->party_id);
        echo json_encode(array("sales_order_data" => $sales_order_data, "sales_order_items" => $sales_order_items, 'party_contact_person' => $party_contact_person));
    }

    function get_sales_order_item_for_challan() {
        $id = $this->input->get_post("id");
        $sales_order_item_id = $this->input->get_post("sales_order_item_id");
        $sales_order_data = $this->crud->get_sales_order($id);
        if (!empty($sales_order_data->po_date)) {
            $sales_order_data->po_date = date('d-m-Y', strtotime($sales_order_data->po_date));
        }
        if (!empty($sales_order_data->sales_order_date)) {
            $sales_order_data->sales_order_date = date('d-m-Y', strtotime($sales_order_data->sales_order_date));
        }
        $sales_order_item = array();
        $this->db->select('*');
        $this->db->from('sales_order_items');
        $this->db->where('id', $sales_order_item_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $sales_order_item = $query->result_array();
        }
        $sales_order_data->sales_order_item_id = $sales_order_item_id;
        $party_contact_person = $this->crud->get_contact_person_by_party($sales_order_data->party_id);
        echo json_encode(array("sales_order_data" => $sales_order_data, "sales_order_item" => $sales_order_item, 'party_contact_person' => $party_contact_person));
    }

    /**
     * @param $sales_order_id
     * @return array
     */
    function get_sales_order_items($sales_order_id) {
        $this->db->select('*');
        $this->db->from('sales_order_items');
        $this->db->where('sales_order_id', $sales_order_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function save_challan() {
        $post_data = $this->input->post();
        /* echo "<pre>";
          print_r($post_data);
          exit(); */

        $challan_data = $post_data['challan_data'];
        $buyer_data = $post_data['buyer_data'];
        if (isset($buyer_data['state_id'])) {
            
        } else {
            $buyer_data['state_id'] = '';
        }
        if (isset($buyer_data['country_id'])) {
            
        } else {
            $buyer_data['country_id'] = '';
        }

        $challan_data['challan_date'] = date('Y-m-d', strtotime(str_replace("/", "-", $challan_data['challan_date'])));
        $challan_data['lr_date'] = date('Y-m-d', strtotime(str_replace("/", "-", $challan_data['lr_date'])));

        /* ------------ Phone No And Email ID Validation Start------------------------- */
        // get emails
        $temp = $buyer_data['email_id'];
        $emails = explode(",", $temp);
        $email_status = 1;
        $email_msg = "";

        $email_ids = array();
        // multiple email validation
        if (is_array($emails) && count($emails) > 0) {
            foreach ($emails as $email) {
                if (trim($email) != "") {
                    $email = trim($email);
                    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                        $email_msg .= "$email is not valid email.&nbsp;";
                        $email_status = 0;
                    }
                    $this->db->select('*');
                    $this->db->from('party');
                    $this->db->where('party_id !=', $challan_data['sales_to_party_id']);
                    $this->db->where('active !=', 0);
                    $this->db->where("FIND_IN_SET( '" . $email . "', email_id) ");
                    $res = $this->db->get();
                    $rows = $res->num_rows();
                    if ($rows > 0) {
                        $result = $res->result();
                        $email_msg .= "$email email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name;
                        $email_status = 0;
                    } else {
                        $email_ids[] = $email;
                    }
                }
            }
        }
        if ($email_status == 0) {
            echo json_encode(array("status" => 0, 'message' => $email_msg, 'email_error' => 1));
            exit;
        }
        $buyer_data['email_id'] = implode(',', $email_ids);

        $temp = $buyer_data['phone_no'];
        if (trim($buyer_data['phone_no']) == "" && count($email_ids) == 0) {
            $email_msg = "Please enter phone number or email id !";
            echo json_encode(array("status" => 0, 'message' => $email_msg, 'email_error' => 1));
            exit;
        }
        $phone_msg = '';
        $phone_numbers = explode(",", $temp);
        $phone_status = 1;
        $phone_nos = array();
        foreach ($phone_numbers as $phone) {
            $phone = trim($phone);
            $this->db->select('*');
            $this->db->from('party');
            $this->db->where('party_id !=', $challan_data['sales_to_party_id']);
            $this->db->where('active !=', 0);
            $this->db->where("FIND_IN_SET( '" . $phone . "', phone_no) ");
            $res = $this->db->get();
            $rows = $res->num_rows();
            if ($rows > 0 && trim($phone) != "") {
                $result = $res->result();
                $phone_msg .= "$phone mobile already exist.&nbsp; &nbsp; <br/> Original Party : " . $result[0]->party_name;
                $phone_status = 0;
            }
        }
        if ($phone_status == 0) {
            echo json_encode(array("status" => 0, 'message' => $phone_msg, 'phone_error' => 1));
            exit;
        }
        $buyer_data['phone_no'] = $temp;
        /* ------------ Phone No And Email ID Validation End------------------------- */

        if (trim($challan_data['kind_attn_id']) != '' && !empty(trim($challan_data['kind_attn_id']))) {
            $contact_person = $_POST['contact_person'];
            $contact_person_update['mobile_no'] = $contact_person['contact_person_mobile_no'];
            $contact_person_update['email'] = $contact_person['contact_person_email_id'];
            $this->crud->update('contact_person', $contact_person_update, array('contact_person_id' => $challan_data['kind_attn_id']));
        }


        $challan_id = $challan_data['challan_id'];
        $logged_in = $this->session->userdata("is_logged_in");
        $challan_data['updated_by'] = $logged_in['staff_id'];
        unset($challan_data['challan_id']);
        $challan_items = array();
        $edit_delete_item_id = (int) isset($_POST['edit_delete_item_id']) ? trim($_POST['edit_delete_item_id']) : 0;
        if (trim($challan_id) != '' && !empty(trim($challan_id))) {
            $this->crud->update('challans', $challan_data, array('id' => $challan_id));
            $this->crud->update('party', $buyer_data, array('party_id' => $buyer_data['party_id']));
        } else {
            $challan_data['created_by'] = $this->session->userdata('is_logged_in')['staff_id'];
            $challan_data['created_at'] = date('Y-m-d H:i:s');
            $challan_id = $this->crud->insert('challans', $challan_data);
            $this->crud->update('party', $buyer_data, array('party_id' => $buyer_data['party_id']));
            $sales_order_item_id = $challan_data['sales_order_item_id'];
            $this->db->select('*');
            $this->db->from('sales_order_items');
            $this->db->where('id', $sales_order_item_id);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $key => $item_data) {
                    $item_data['quantity'] = '1';
                    $pure_amount = $item_data['quantity'] * $item_data['rate'];
                    $item_data['disc_value'] = $pure_amount * $item_data['disc_per'] / 100;
                    $taxable_amount = $pure_amount - $item_data['disc_value'];
                    $item_data['igst_amount'] = $taxable_amount * $item_data['igst'] / 100;
                    $item_data['cgst_amount'] = $taxable_amount * $item_data['cgst'] / 100;
                    $item_data['sgst_amount'] = $taxable_amount * $item_data['sgst'] / 100;
                    $item_data['total_amount'] = $taxable_amount + $item_data['igst_amount'] + $item_data['cgst_amount'] + $item_data['sgst_amount'];

                    $item_data['challan_id'] = $challan_id;
                    $item_data['created_at'] = date('Y-m-d H:i:s');
                    $sales_order_item_id = $item_data['id'];
                    unset($item_data['sales_order_id']);
                    unset($item_data['id']);
                    $this->db->insert('challan_items', $item_data);
                    if ($edit_delete_item_id == $sales_order_item_id) {
                        $edit_delete_item_id = $this->db->insert_id();
                    }
                }
            }
            /* -------- Sales Order Items Data --------- */
            $this->db->select('*');
            $this->db->from('challan_items');
            $this->db->where('challan_id', $challan_id);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $item_row) {
                    $challan_items[] = $item_row;
                }
            }
        }
        echo json_encode(array('success' => true, 'message' => "Challan saved successfully!", 'challan_id' => $challan_id, 'challan_items' => $challan_items, 'edit_delete_item_id' => $edit_delete_item_id));
        exit();
    }

    /**
     * @param $challan_id
     */
    function add_challan_item($challan_id) {
        $item_id = $_POST['item_id'];
        if ($item_id != '') {
            $tempArr = $this->db->select('*')->from('items')->where('id', $item_id)->get()->result_array();
            if (count($tempArr) == 0) {
                echo json_encode(array('success' => 'false', 'message' => 'Item code not exists in item master!'));
                exit();
            }
            $item_data = $_POST;
            $motor_user = array();
            if (!empty($item_data['motor_user'])) {
                $motor_user = explode(',', $item_data['motor_user']);
            }
            unset($item_data['motor_user']);
            $motor_make = array();
            if (!empty($item_data['motor_make'])) {
                $motor_make = explode(',', $item_data['motor_make']);
            }
            unset($item_data['motor_make']);
            $motor_hp = array();
            if (!empty($item_data['motor_hp'])) {
                $motor_hp = explode(',', $item_data['motor_hp']);
            }
            unset($item_data['motor_hp']);
            $motor_kw = array();
            if (!empty($item_data['motor_kw'])) {
                $motor_kw = explode(',', $item_data['motor_kw']);
            }
            unset($item_data['motor_kw']);
            $motor_frequency = array();
            if (!empty($item_data['motor_frequency'])) {
                $motor_frequency = explode(',', $item_data['motor_frequency']);
            }
            unset($item_data['motor_frequency']);
            $motor_rpm = array();
            if (!empty($item_data['motor_rpm'])) {
                $motor_rpm = explode(',', $item_data['motor_rpm']);
            }
            unset($item_data['motor_rpm']);
            $motor_volts_cycles = array();
            if (!empty($item_data['motor_volts_cycles'])) {
                $motor_volts_cycles = explode(',', $item_data['motor_volts_cycles']);
            }
            unset($item_data['motor_volts_cycles']);
            $motor_serial_no = array();
            if (!empty($item_data['motor_serial_no'])) {
                $motor_serial_no = explode(',', $item_data['motor_serial_no']);
            }
            unset($item_data['motor_serial_no']);
            $gearbox_user = array();
            if (!empty($item_data['gearbox_user'])) {
                $gearbox_user = explode(',', $item_data['gearbox_user']);
            }
            unset($item_data['gearbox_user']);
            $gearbox_make = array();
            if (!empty($item_data['gearbox_make'])) {
                $gearbox_make = explode(',', $item_data['gearbox_make']);
            }
            unset($item_data['gearbox_make']);
            $gearbox_gear_type = array();
            if (!empty($item_data['gearbox_gear_type'])) {
                $gearbox_gear_type = explode(',', $item_data['gearbox_gear_type']);
            }
            unset($item_data['gearbox_gear_type']);
            $gearbox_model = array();
            if (!empty($item_data['gearbox_model'])) {
                $gearbox_model = explode(',', $item_data['gearbox_model']);
            }
            unset($item_data['gearbox_model']);
            $gearbox_ratio = array();
            if (!empty($item_data['gearbox_ratio'])) {
                $gearbox_ratio = explode(',', $item_data['gearbox_ratio']);
            }
            unset($item_data['gearbox_ratio']);
            $gearbox_serial_no = array();
            if (!empty($item_data['gearbox_serial_no'])) {
                $gearbox_serial_no = explode(',', $item_data['gearbox_serial_no']);
            }
            unset($item_data['gearbox_serial_no']);

            $val_inc = 0;
            if (!empty($motor_user)) {
                $this->db->where('challan_item_id', $challan_item_id);
                $this->db->delete('challan_items_motor_details');
                foreach ($motor_user as $motor_user_val) {
                    $challan_items_motor_details = array();
                    $challan_items_motor_details['challan_item_id'] = $challan_item_id;
                    $challan_items_motor_details['motor_user'] = $motor_user_val;
                    $challan_items_motor_details['motor_make'] = $motor_make[$val_inc];
                    $challan_items_motor_details['motor_hp'] = $motor_hp[$val_inc];
                    $challan_items_motor_details['motor_kw'] = $motor_kw[$val_inc];
                    $challan_items_motor_details['motor_frequency'] = $motor_frequency[$val_inc];
                    $challan_items_motor_details['motor_rpm'] = $motor_rpm[$val_inc];
                    $challan_items_motor_details['motor_volts_cycles'] = $motor_volts_cycles[$val_inc];
                    $challan_items_motor_details['motor_serial_no'] = $motor_serial_no[$val_inc];
                    $this->db->insert('challan_items_motor_details', $challan_items_motor_details);
                    $val_inc++;
                }
            }

            $val_inc = 0;
            if (!empty($gearbox_user)) {
                $this->db->where('challan_item_id', $challan_item_id);
                $this->db->delete('challan_items_gearbox_details');
                foreach ($gearbox_user as $gearbox_user_val) {
                    $challan_items_gearbox_details = array();
                    $challan_items_gearbox_details['challan_item_id'] = $challan_item_id;
                    $challan_items_gearbox_details['gearbox_user'] = $gearbox_user_val;
                    $challan_items_gearbox_details['gearbox_make'] = $gearbox_make[$val_inc];
                    $challan_items_gearbox_details['gearbox_gear_type'] = $gearbox_gear_type[$val_inc];
                    $challan_items_gearbox_details['gearbox_model'] = $gearbox_model[$val_inc];
                    $challan_items_gearbox_details['gearbox_ratio'] = $gearbox_ratio[$val_inc];
                    $challan_items_gearbox_details['gearbox_serial_no'] = $gearbox_serial_no[$val_inc];
                    $this->db->insert('challan_items_gearbox_details', $challan_items_gearbox_details);
                    $val_inc++;
                }
            }

            $item_data['challan_id'] = $challan_id;
            $item_data['created_at'] = date('Y-m-d H:i:s');
            $challan_item_id = $this->crud->insert('challan_items', $item_data);
            echo json_encode(array('success' => true, 'message' => 'Item added successfully!', 'challan_item_id' => $challan_item_id,));
            exit();
        } else {
            echo json_encode(array('success' => false, 'message' => 'Please select item!'));
            exit();
        }
    }

    /**
     * @param $challan_item_id
     */
    function edit_challan_item($challan_item_id) {
        $item_id = $_POST['item_id'];
        if ($item_id != '') {
            $tempArr = $this->db->select('*')->from('items')->where('id', $item_id)->get()->result_array();
            if (count($tempArr) == 0) {
                echo json_encode(array('success' => 'false', 'message' => 'Item code not exists in item master!'));
                exit();
            }
            $item_data = $_POST;
            $motor_user = array();
            if (!empty($item_data['motor_user'])) {
                $motor_user = explode(',', $item_data['motor_user']);
            }
            unset($item_data['motor_user']);
            $motor_make = array();
            if (!empty($item_data['motor_make'])) {
                $motor_make = explode(',', $item_data['motor_make']);
            }
            unset($item_data['motor_make']);
            $motor_hp = array();
            if (!empty($item_data['motor_hp'])) {
                $motor_hp = explode(',', $item_data['motor_hp']);
            }
            unset($item_data['motor_hp']);
            $motor_kw = array();
            if (!empty($item_data['motor_kw'])) {
                $motor_kw = explode(',', $item_data['motor_kw']);
            }
            unset($item_data['motor_kw']);
            $motor_frequency = array();
            if (!empty($item_data['motor_frequency'])) {
                $motor_frequency = explode(',', $item_data['motor_frequency']);
            }
            unset($item_data['motor_frequency']);
            $motor_rpm = array();
            if (!empty($item_data['motor_rpm'])) {
                $motor_rpm = explode(',', $item_data['motor_rpm']);
            }
            unset($item_data['motor_rpm']);
            $motor_volts_cycles = array();
            if (!empty($item_data['motor_volts_cycles'])) {
                $motor_volts_cycles = explode(',', $item_data['motor_volts_cycles']);
            }
            unset($item_data['motor_volts_cycles']);
            $motor_serial_no = array();
            if (!empty($item_data['motor_serial_no'])) {
                $motor_serial_no = explode(',', $item_data['motor_serial_no']);
            }
            unset($item_data['motor_serial_no']);
            $gearbox_user = array();
            if (!empty($item_data['gearbox_user'])) {
                $gearbox_user = explode(',', $item_data['gearbox_user']);
            }
            unset($item_data['gearbox_user']);
            $gearbox_make = array();
            if (!empty($item_data['gearbox_make'])) {
                $gearbox_make = explode(',', $item_data['gearbox_make']);
            }
            unset($item_data['gearbox_make']);
            $gearbox_gear_type = array();
            if (!empty($item_data['gearbox_gear_type'])) {
                $gearbox_gear_type = explode(',', $item_data['gearbox_gear_type']);
            }
            unset($item_data['gearbox_gear_type']);
            $gearbox_model = array();
            if (!empty($item_data['gearbox_model'])) {
                $gearbox_model = explode(',', $item_data['gearbox_model']);
            }
            unset($item_data['gearbox_model']);
            $gearbox_ratio = array();
            if (!empty($item_data['gearbox_ratio'])) {
                $gearbox_ratio = explode(',', $item_data['gearbox_ratio']);
            }
            unset($item_data['gearbox_ratio']);
            $gearbox_serial_no = array();
            if (!empty($item_data['gearbox_serial_no'])) {
                $gearbox_serial_no = explode(',', $item_data['gearbox_serial_no']);
            }
            unset($item_data['gearbox_serial_no']);

            $val_inc = 0;
            if (!empty($motor_user)) {
                $this->db->where('challan_item_id', $challan_item_id);
                $this->db->delete('challan_items_motor_details');
                foreach ($motor_user as $motor_user_val) {
                    $challan_items_motor_details = array();
                    $challan_items_motor_details['challan_item_id'] = $challan_item_id;
                    $challan_items_motor_details['motor_user'] = $motor_user_val;
                    $challan_items_motor_details['motor_make'] = $motor_make[$val_inc];
                    $challan_items_motor_details['motor_hp'] = $motor_hp[$val_inc];
                    $challan_items_motor_details['motor_kw'] = $motor_kw[$val_inc];
                    $challan_items_motor_details['motor_frequency'] = $motor_frequency[$val_inc];
                    $challan_items_motor_details['motor_rpm'] = $motor_rpm[$val_inc];
                    $challan_items_motor_details['motor_volts_cycles'] = $motor_volts_cycles[$val_inc];
                    $challan_items_motor_details['motor_serial_no'] = $motor_serial_no[$val_inc];
                    $this->db->insert('challan_items_motor_details', $challan_items_motor_details);
                    $val_inc++;
                }
            }

            $val_inc = 0;
            if (!empty($gearbox_user)) {
                $this->db->where('challan_item_id', $challan_item_id);
                $this->db->delete('challan_items_gearbox_details');
                foreach ($gearbox_user as $gearbox_user_val) {
                    $challan_items_gearbox_details = array();
                    $challan_items_gearbox_details['challan_item_id'] = $challan_item_id;
                    $challan_items_gearbox_details['gearbox_user'] = $gearbox_user_val;
                    $challan_items_gearbox_details['gearbox_make'] = $gearbox_make[$val_inc];
                    $challan_items_gearbox_details['gearbox_gear_type'] = $gearbox_gear_type[$val_inc];
                    $challan_items_gearbox_details['gearbox_model'] = $gearbox_model[$val_inc];
                    $challan_items_gearbox_details['gearbox_ratio'] = $gearbox_ratio[$val_inc];
                    $challan_items_gearbox_details['gearbox_serial_no'] = $gearbox_serial_no[$val_inc];
                    $this->db->insert('challan_items_gearbox_details', $challan_items_gearbox_details);
                    $val_inc++;
                }
            }

            $this->db->where('id', $challan_item_id);
            $this->db->update('challan_items', $item_data);
            echo json_encode(array('success' => true, 'message' => 'Item saved successfully!'));
            exit();
        } else {
            echo json_encode(array('success' => false, 'message' => 'Please select item!'));
            exit();
        }
    }

    /**
     * @param $challan_item_id
     */
    function delete_challan_item($challan_item_id) {
        $where_array = array("id" => $challan_item_id);
        $this->crud->delete("challan_items", $where_array);
        $this->crud->delete('challan_items_motor_details', array('challan_item_id' => $challan_item_id));
        $this->crud->delete('challan_items_gearbox_details', array('challan_item_id' => $challan_item_id));
        exit;
    }

    /**
     * @param $item_id
     */
    function get_challan_item_by_id($item_id) {
        $this->db->select('*');
        $this->db->from('challan_items');
        $this->db->where('id', $item_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $item_data = $query->row_array(0);
            $item_data['quantity'] = '1';

            $this->db->select('*');
            $this->db->from('challan_items_motor_details');
            $this->db->where('challan_item_id', $item_data['id']);
            $m_query = $this->db->get();
            $motor_data = $m_query->result();

            $this->db->select('*');
            $this->db->from('challan_items_gearbox_details');
            $this->db->where('challan_item_id', $item_data['id']);
            $g_query = $this->db->get();
            $gearbox_data = $g_query->result();

            echo json_encode(array('success' => true, 'message' => "Item Data", 'item_data' => $item_data, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data));
            exit();
        } else {
            echo json_encode(array('success' => false, 'message' => "wrong!"));
            exit();
        }
    }

    function challan_list() {
        if ($this->app_model->have_access_role(CHALLAN_MODULE_ID, "view")) {
            set_page('dispatch/challan_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    /**
     * @return mixed
     */
    function get_challan() {
        $this->db->select('dc.id,dc.id,dc.challan_date,p.party_name as sales_to_party');
        //$this->db->select('dc.id,dc.challan_no,dc.challan_date,p.party_name as sales_to_party');
        $this->db->from('challans dc');
        $this->db->join('party p', 'p.party_id = dc.sales_to_party_id', 'left');
        $this->db->where('p.active !=', 0);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * @param $challan_id
     * @return array
     */
    function get_challan_by_id($challan_id) {
        $this->db->select('c.*');
        $this->db->from('challans c');
        $this->db->where('c.id', $challan_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array(0);
        } else {
            return array();
        }
    }

    function challan_print($id) {
        $select_item = (isset($_GET['item']) && $_GET['item']) ? $_GET['item'] : '';
        $challan_data = $this->get_challan_detail($id);
        $company_details = $this->get_company_detail();
        $challan_items = $this->get_challan_items($id);
        $this->load->library('m_pdf');
        $margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
        $margin_left = $margin_company[0]->margin_left;
        $margin_right = $margin_company[0]->margin_right;
        $margin_top = $margin_company[0]->margin_top;
        $margin_bottom = $margin_company[0]->margin_bottom;

        $challan_data->invoice_no = '';
        $challan_data->invoice_date = '';
        $challan_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $challan_data->delivery_through_id));
        $challan_data->prepared_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->prepared_by));
        $challan_data->checked_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->checked_by));
        $invoice_data = $this->crud->get_row_by_id('invoices', array('sales_order_no' => $challan_data->sales_order_no));
        if (!empty($invoice_data)) {
            $challan_data->invoice_no = $invoice_data[0]->invoice_no;
            $challan_data->invoice_date = date('d-m-Y', strtotime($invoice_data[0]->sales_order_date));
        }

        if (!empty($challan_items)) {
            foreach ($challan_items as $key => $item_row) {
                if ($select_item && $item_row['id'] != $select_item) {
                    continue;
                }
                $item_row['hsn_no'] = $this->crud->get_column_value_by_id('items', 'hsn_code', array('id' => $item_row['item_id']));
                $item_row['item_extra_accessories'] = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $item_row['item_extra_accessories_id']));

                $this->db->select('imd.*, u.user_name, m.make_name, h.hp_name, k.kw_name, f.frequency_name, r.rpm_name, v.volts_cycles_name');
                $this->db->from('challan_items_motor_details imd');
                $this->db->join('challan_item_user u', 'u.id = imd.motor_user', 'left');
                $this->db->join('challan_item_make m', 'm.id = imd.motor_make', 'left');
                $this->db->join('challan_item_hp h', 'h.id = imd.motor_hp', 'left');
                $this->db->join('challan_item_kw k', 'k.id = imd.motor_kw', 'left');
                $this->db->join('challan_item_frequency f', 'f.id = imd.motor_frequency', 'left');
                $this->db->join('challan_item_rpm r', 'r.id = imd.motor_rpm', 'left');
                $this->db->join('challan_item_volts_cycles v', 'v.id = imd.motor_volts_cycles', 'left');
                $this->db->where('challan_item_id', $item_row['id']);
                $m_query = $this->db->get();
                $motor_data = $m_query->result();

                $this->db->select('igd.*, u.user_name, m.make_name, g.gear_type_name, mo.model_name, r.ratio_name');
                $this->db->from('challan_items_gearbox_details igd');
                $this->db->join('challan_item_user u', 'u.id = igd.gearbox_user', 'left');
                $this->db->join('challan_item_make m', 'm.id = igd.gearbox_make', 'left');
                $this->db->join('challan_item_gear_type g', 'g.id = igd.gearbox_gear_type', 'left');
                $this->db->join('challan_item_model mo', 'mo.id = igd.gearbox_model', 'left');
                $this->db->join('challan_item_ratio r', 'r.id = igd.gearbox_ratio', 'left');
                $this->db->where('challan_item_id', $item_row['id']);
                $g_query = $this->db->get();
                $gearbox_data = $g_query->result();
                if ($challan_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
                    $item_row['challan_declaration'] = $this->applib->getTermsandConditions('challan_declaration_for_domestic');
                } elseif ($challan_data->party_type_1 == PARTY_TYPE_EXPORT_ID) {
                    $item_row['challan_declaration'] = $this->applib->getTermsandConditions('challan_declaration_for_export');
                } else {
                    $item_row['challan_declaration'] = null;
                }

                $param = "'utf-8','A4'";
                $pdf = $this->m_pdf->load($param);
                $pdf->AddPage(
                    'P', //orientation
                    '', //type
                    '', //resetpagenum
                    '', //pagenumstyle
                    '', //suppress
                    $margin_left, //margin-left
                    $margin_right, //margin-right
                    $margin_top, //margin-top
                    $margin_bottom, //margin-bottom
                    0, //margin-header
                    0 //margin-footer
                );
                if ($key == 0) {
                    $other_items = $this->get_challan_sub_items($id);
                    $html = $this->load->view('dispatch/challan_pdf', array('challan_data' => $challan_data, 'challan_item' => $item_row, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
                } else {
                    $html = $this->load->view('dispatch/challan_pdf', array('challan_data' => $challan_data, 'challan_item' => $item_row, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => false), true);
                }
                $pdf->WriteHTML($html);

                $pdfFilePath = "challan_" . $id . ".pdf";
                $pdf->Output($pdfFilePath, "I");
            }
        }
    }

    function challan_plat_print($id) {
        $select_item = (isset($_GET['item']) && $_GET['item']) ? $_GET['item'] : '';
        $challan_data = $this->get_challan_detail($id);
        $company_details = $this->get_company_detail();
        $challan_items = $this->get_challan_items($id);
        $this->load->library('m_pdf');

        if ($challan_data) {
            $param = "'utf-8','A4'";
            $pdf = $this->m_pdf->load($param);
            $margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
            $margin_left = $margin_company[0]->margin_left;
            $margin_right = $margin_company[0]->margin_right;
            $margin_top = $margin_company[0]->margin_top;
            $margin_bottom = $margin_company[0]->margin_bottom;
            if (!empty($challan_items)) {
                foreach ($challan_items as $key => $item_row) {
                    if ($select_item && $item_row['id'] != $select_item) {
                        continue;
                    }

                    $pdf->AddPage(
                        'P', //orientation
                        '', //type
                        '', //resetpagenum
                        '', //pagenumstyle
                        '', //suppress
                        $margin_left, //margin-left
                        $margin_right, //margin-right
                        $margin_top, //margin-top
                        $margin_bottom, //margin-bottom
                        0, //margin-header
                        0 //margin-footer
                    );

                    $html = $this->load->view('dispatch/challan_plat_pdf', array('challan_data' => $challan_data, 'company_details' => $company_details, 'is_first_item' => true, 'item_row' => $item_row), true);

                    $pdf->WriteHTML($html);
                }
            }

            $pdfFilePath = "work_order.pdf";
            $pdf->Output($pdfFilePath, "I");
        }
    }

    function get_company_detail() {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('id', COMPANY_ID);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array(0);
        } else {
            return array();
        }
    }

    /**
     * @param $id
     * @return array
     */
    function get_challan_items($id) {
        $this->db->select('soi.*');
        $this->db->from('challan_items soi');
        $this->db->join('item_category ic', 'ic.id = soi.item_category_id');
        $this->db->where('ic.item_category', 'Finished Goods');
        $this->db->where('soi.challan_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    /**
     * @param $id
     * @return array
     */
    function get_challan_sub_items($id) {
        $this->db->select('soi.*');
        $this->db->from('challan_items soi');
        $this->db->join('item_category ic', 'ic.id = soi.item_category_id');
        $this->db->where('ic.item_category != ', 'Finished Goods');
        $this->db->where('soi.challan_id', $id);
        $query = $this->db->get();
        $other_items = array();
        if ($query->num_rows() > 0) {
            $other_items = $query->result_array();
        }
        return $other_items;
    }

    /**
     * @param $id
     * @return array
     */
    function get_challan_detail($id) {
        $select = "ch.*, ch.id as challan_id, ch.notes as challan_notes, sales.sales,currency.currency,quo.quotation_no,so.*,party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,party.utr_no AS party_cin_no,";
        $select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,party.party_type_1,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
        $select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
        $select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
        $select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required";
        $this->db->select($select);
        $this->db->from('challans ch');
        $this->db->join('sales_order so', 'so.id = ch.sales_order_id', 'left');
        $this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
        $this->db->join('sales', 'sales.id = so.sales_id', 'left');
        $this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
        $this->db->join('contact_person cp', 'cp.party_id = so.sales_to_party_id', 'left');
        $this->db->join('city', 'city.city_id = party.city_id', 'left');
        $this->db->join('state', 'state.state_id = party.state_id', 'left');
        $this->db->join('country', 'country.country_id = party.country_id', 'left');
        $this->db->join('currency', 'currency.id = so.currency_id', 'left');
        $this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
        $this->db->join('sales_order_pref', 'sales_order_pref.id = so.sales_order_pref_id', 'left');
        $this->db->join('transportation_by', 'transportation_by.id = so.transportation_by_id', 'left');
        $this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = so.foundation_drawing_required_id', 'left');
        $this->db->join('loading_by', 'loading_by.id = so.loading_by_id', 'left');
        $this->db->join('inspection_required', 'inspection_required.id = so.inspection_required_id', 'left');
        $this->db->join('unloading_by', 'unloading_by.id = so.unloading_by_id', 'left');
        $this->db->join('erection_commissioning', 'erection_commissioning.id = so.erection_commissioning_id', 'left');
        $this->db->join('road_insurance_by', 'road_insurance_by.id = so.road_insurance_by_id', 'left');
        $this->db->join('for_required', 'for_required.id = so.for_required_id', 'left');
        $this->db->where('ch.id', $id);
        $this->db->where('party.active !=', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row(0);
        } else {
            return array();
        }
    }

    function our_cliant() {
        if ($this->app_model->have_access_role(OUR_CLIANT_MODULE_ID, "view")) {
            set_page('dispatch/our_cliant');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function cliant_datatable() {
        if (!$this->app_model->have_access_role(OUR_CLIANT_MODULE_ID, "view")) {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }

        $config['table'] = 'challans c';
        $config['select'] = 'c.id as challan_id, c.serial_no, c.challan_date, ci.id, ci.item_serial_no, p.party_name, ci.item_name, so.sales_order_no, q.quotation_no, q.enquiry_no, city.city, state.state, country.country,p.party_name,p.phone_no,p.email_id';
        //challan_items ci, party p WHERE c.id = ci.challan_id
        $config['joins'][] = array('join_table' => 'challan_items ci', 'join_by' => 'c.id = ci.challan_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'q.id = so.quotation_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['column_order'] = array(null, 'so.sales_order_no', 'q.quotation_no', 'q.enquiry_no', 'p.party_name', 'city.city', 'state.state', 'country.country', 'ci.item_serial_no', 'c.challan_date', 'ci.item_name');
        $config['column_search'] = array('so.sales_order_no', 'q.quotation_no', 'q.enquiry_no', 'p.party_name', 'city.city', 'state.state', 'country.country', 'ci.item_serial_no', 'DATE_FORMAT(c.challan_date,"%d-%m-%Y")', 'ci.item_name');

        if (!empty($_POST['party_type']) && $_POST['party_type'] == 'all') {
            
        } else if (isset($_POST['party_type']) && $_POST['party_type'] == 'export') {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        } else if (isset($_POST['party_type']) && $_POST['party_type'] == 'domestic') {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
        }

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
            
        } elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        } else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
        }else{}

		$config['order'] = array('c.id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active != ','column_value'=>'0');

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $role_edit = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "edit");
        $role_view = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "view");
        foreach ($list as $cliant) {
            $row = array();
            $action = '';

            if ($role_view) {
                $action .= '<a href="' . base_url('/challan/add/' . $cliant->challan_id . '?view') . '" class="view_button btn-primary btn-xs"><i class="fa fa-eye"></i></a>';
            }
            $row[] = $action;
            $row[] = $cliant->sales_order_no;
            $row[] = $cliant->quotation_no;
            $row[] = $cliant->enquiry_no;
            $row[] = $cliant->party_name;
            $row[] = $cliant->city;
            $row[] = $cliant->state;
            $row[] = $cliant->country;
            $row[] = $cliant->item_serial_no;
            $row[] = date('d-m-Y', strtotime($cliant->challan_date));
            $row[] = $cliant->item_name;


            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    /**
     * @param $id
     */
    function delete($id) {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
    }

    /**
     * Dispatch Followup histiory
     */
    function add_followup_history() {
        //echo "<pre>";print_r($_POST);exit;
        $data = array();
        $data['challan_id'] = $_POST['followup_history_challan_id'];
        $data['followup_date'] = date("Y-m-d", strtotime(str_replace("/", "-", $_POST['followup_history_date'])));
        $data['history'] = $_POST['followup_history_history'];
        $data['followup_by'] = $this->staff_id;

        if (isset($_POST['followup_history_id']) && !empty($_POST['followup_history_id'])) {
            /* unset($data['followup_by']); */
            $followup_history_id = $_POST['followup_history_id'];
            $this->db->where('id', $followup_history_id);
            $rs = $this->db->update('dispatch_followup_history', $data);
            if ($rs) {
                $return_data = $this->db->get_where('dispatch_followup_history', array('id' => $followup_history_id));
                $return_data = $return_data->result_array();
                $return_data[0]['followup_by'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $return_data[0]['followup_by']));
                echo json_encode(array('status' => true, 'message' => "Follow Up Edited!", 'data' => $return_data[0]));
            } else {
                echo json_encode(array('status' => true, 'message' => "Follow Up Edit Fail!"));
            }
            exit();
        } else {
            $this->db->insert('dispatch_followup_history', $data);
            $followup_history_id = $this->db->insert_id();
        }

        /* ------------ Return Response ---------------- */
        echo json_encode(array('status' => true, 'message' => "Follow Up Added!", 'followup_history_id' => $followup_history_id));
        exit();
    }

    function delete_followup_history($id) {
        $where_array = array("id" => $id);
        $this->crud->delete("dispatch_followup_history", $where_array);
        echo json_encode(array('status' => true, 'message' => "Follow Up Deleted!"));
        exit();
    }

    function get_followup_history_by_id($id) {
        $this->db->select('*');
        $this->db->from('dispatch_followup_history');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $followup_h_data = $query->row_array(0);
            echo json_encode(array('success' => true, 'followup_h_data' => $followup_h_data));
            exit();
        } else {
            echo json_encode(array('success' => false));
            exit();
        }
    }

    function dispatch_followup_history_datatable($id) {
        $config['table'] = 'dispatch_followup_history fh';
        $config['select'] = 'fh.*,s.name as followup_by_name';
        $config['column_order'] = array(null, 'fh.followup_date', 'fh.history', 's.name');
        $config['column_search'] = array('DATE_FORMAT(fh.followup_date,"%d-%m-%Y")', 'fh.history', 's.name');
        $config['joins'][] = array('join_table' => 'staff s', 'join_by' => 's.staff_id = fh.followup_by');
        //$config['order'] = array('e.challan_id' => 'desc');
        $config['wheres'][] = array('column_name' => 'fh.challan_id', 'column_value' => $id);
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        foreach ($list as $followup_history_data_row) {
            $row = array();
            $action = '';

            $action .= '<a href="javascript:void(0);" class="followup_history_edit_button btn-primary btn-xs" data-id="' . $followup_history_data_row->id . '"><i class="fa fa-edit"></i></a>';
            $action .= '| <a href="javascript:void(0);" class="followup_history_delete_button btn-danger btn-xs" data-id="' . $followup_history_data_row->id . '"><i class="fa fa-trash"></i></a>';
            $row[] = $action;
            $row[] = date('d-m-Y', strtotime($followup_history_data_row->followup_date));
            $row[] = $followup_history_data_row->history;
            $row[] = $followup_history_data_row->followup_by_name;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function testing_print($id) {
        $select_item = (isset($_GET['item']) && $_GET['item']) ? $_GET['item'] : '';
        $challan_data = $this->get_challan_detail($id);
        $company_details = $this->get_company_detail();
        $challan_items = $this->get_challan_items($id);
        $this->load->library('m_pdf');
        $margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
        $margin_left = $margin_company[0]->margin_left;
        $margin_right = $margin_company[0]->margin_right;
        $margin_top = $margin_company[0]->margin_top;
        $margin_bottom = $margin_company[0]->margin_bottom;

        $challan_data->invoice_no = '';
        $challan_data->invoice_date = '';
        $challan_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $challan_data->delivery_through_id));
        $challan_data->prepared_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->prepared_by));
        $challan_data->checked_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->checked_by));
        $invoice_data = $this->crud->get_row_by_id('invoices', array('sales_order_no' => $challan_data->sales_order_no));
        if (!empty($invoice_data)) {
            $challan_data->invoice_no = $invoice_data[0]->invoice_no;
            $challan_data->invoice_date = date('d-m-Y', strtotime($invoice_data[0]->sales_order_date));
        }

        if (!empty($challan_items)) {
            foreach ($challan_items as $key => $item_row) {
                if ($select_item && $item_row['id'] != $select_item) {
                    continue;
                }
                $item_row['hsn_no'] = $this->crud->get_column_value_by_id('items', 'hsn_code', array('id' => $item_row['item_id']));
                $item_row['item_extra_accessories'] = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $item_row['item_extra_accessories_id']));

                $this->db->select('imd.*, u.user_name, m.make_name, h.hp_name, k.kw_name, f.frequency_name, r.rpm_name, v.volts_cycles_name');
                $this->db->from('challan_items_motor_details imd');
                $this->db->join('challan_item_user u', 'u.id = imd.motor_user', 'left');
                $this->db->join('challan_item_make m', 'm.id = imd.motor_make', 'left');
                $this->db->join('challan_item_hp h', 'h.id = imd.motor_hp', 'left');
                $this->db->join('challan_item_kw k', 'k.id = imd.motor_kw', 'left');
                $this->db->join('challan_item_frequency f', 'f.id = imd.motor_frequency', 'left');
                $this->db->join('challan_item_rpm r', 'r.id = imd.motor_rpm', 'left');
                $this->db->join('challan_item_volts_cycles v', 'v.id = imd.motor_volts_cycles', 'left');
                $this->db->where('challan_item_id', $item_row['id']);
                $m_query = $this->db->get();
                $motor_data = $m_query->result();

                $this->db->select('igd.*, u.user_name, m.make_name, g.gear_type_name, mo.model_name, r.ratio_name');
                $this->db->from('challan_items_gearbox_details igd');
                $this->db->join('challan_item_user u', 'u.id = igd.gearbox_user', 'left');
                $this->db->join('challan_item_make m', 'm.id = igd.gearbox_make', 'left');
                $this->db->join('challan_item_gear_type g', 'g.id = igd.gearbox_gear_type', 'left');
                $this->db->join('challan_item_model mo', 'mo.id = igd.gearbox_model', 'left');
                $this->db->join('challan_item_ratio r', 'r.id = igd.gearbox_ratio', 'left');
                $this->db->where('challan_item_id', $item_row['id']);
                $g_query = $this->db->get();
                $gearbox_data = $g_query->result();
                if ($challan_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
                    $item_row['challan_declaration'] = $this->applib->getTermsandConditions('challan_declaration_for_domestic');
                } elseif ($challan_data->party_type_1 == PARTY_TYPE_EXPORT_ID) {
                    $item_row['challan_declaration'] = $this->applib->getTermsandConditions('challan_declaration_for_export');
                } else {
                    $item_row['challan_declaration'] = null;
                }

                $param = "'utf-8','A4'";
                $pdf = $this->m_pdf->load($param);
                $pdf->AddPage(
                    'P', //orientation
                    '', //type
                    '', //resetpagenum
                    '', //pagenumstyle
                    '', //suppress
                    $margin_left, //margin-left
                    $margin_right, //margin-right
                    $margin_top, //margin-top
                    $margin_bottom, //margin-bottom
                    0, //margin-header
                    0 //margin-footer
                );
                if ($key == 0) {
                    $other_items = $this->get_challan_sub_items($id);
                    $html = $this->load->view('dispatch/testing_pdf', array('challan_data' => $challan_data, 'challan_item' => $item_row, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
                } else {
                    $html = $this->load->view('dispatch/testing_pdf', array('challan_data' => $challan_data, 'challan_item' => $item_row, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => false), true);
                }
                $pdf->WriteHTML($html);

                $pdfFilePath = "testing_" . $id . ".pdf";
                $pdf->Output($pdfFilePath, "I");
            }
        }
    }

}
