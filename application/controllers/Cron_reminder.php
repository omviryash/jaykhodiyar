<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * Class Party
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 * @property Chatmodule $chatmodule
 */
class Cron_reminder extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('chatmodule');
        $this->load->helper('date');
		$this->load->model("Appmodel", "app_model");
    }


	function reminderStatus()
    {
		$query = $this->db->query("SELECT reminder.* from reminder,staff where TIMESTAMPDIFF(MINUTE, reminder.reminder_date, NOW()) > 0 AND staff.staff_id = reminder.user_id AND reminder.is_notified = 0 AND staff.is_login = 1");

		if($query->num_rows())
		{
			$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
			$client->initialize();
			foreach ($query->result() as $row)
			{
				//echo $row->title;
//				$data1['is_notified'] = 1;
//				$result = $this->crud->update('reminder', $data1, array('reminder_id' => $row->reminder_id));
//				if($result){
					$client->emit('reminder_to_user', ['user_id'=>$row->assigned_to,'reminder' => $row->remind, 'reminder_id' => $row->reminder_id]);
//				}
			}
			$client->close();
		}
        //echo json_encode(array('success' => true,'chat_html' => $chat_html));
        //exit;
	}
}	
?>
		
