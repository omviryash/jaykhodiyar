<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 * @property M_pdf $m_pdf
 */
class Sales extends CI_Controller
{

	protected $staff_id = 1;

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

	}

	function index(){
		$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
		redirect("/");
	}

	function get_lead()
	{
		$status = 1;
		$msg = "OK";
		$data = array();
		$id = $this->input->post("id");
		$lead_data = array();

		$sql =
			"
              SELECT ld.*, p.party_code,p.party_type_1,p.party_type_2,i.industry_type,s1.name as lead_owner,s2.name as assigned_to,
              pr.priority,ls.lead_source,ls2.lead_stage,lp.lead_provider,ls3.lead_status
              FROM lead_details ld 
              LEFT JOIN party p ON p.party_id = ld.party_id 
              LEFT JOIN industry_type i ON i.id = ld.industry_type_id 
              LEFT JOIN staff s1 ON s1.staff_id = ld.lead_owner_id 
              LEFT JOIN staff s2 ON s2.staff_id = ld.assigned_to_id 
              LEFT JOIN priority pr ON pr.id = ld.priority_id 
              LEFT JOIN lead_source ls ON ls.id = ld.lead_source_id 
              LEFT JOIN lead_stage ls2 ON ls2.id = ld.lead_stage_id 
              LEFT JOIN lead_provider lp ON lp.id = ld.lead_provider_id 
              LEFT JOIN lead_status ls3 ON ls3.id = ld.lead_status_id 
              WHERE ld.id = '$id'
            ";

		$rows = $this->crud->getFromSQL($sql);

		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$rowJson = json_decode(json_encode($row), true);

				$rowJson['start_date'] = $rowJson['start_date'] == "0000-00-00" ? '' : date("d-m-Y", strtotime($rowJson['start_date']));
				$rowJson['end_date'] = $rowJson['end_date'] == "0000-00-00" ? '' : date("d-m-Y", strtotime($rowJson['end_date']));
				$rowJson['review_date'] = $rowJson['end_date'] == "0000-00-00" ? '' : date("d-m-Y", strtotime($rowJson['review_date']));

				$lead_data = $rowJson;
				$data['party_code'] = $row->party_code;

				$this->db->select('p.*,pt1.type,pt2.type,br.branch,ct.city,st.state,cnt.country');
				$this->db->from('party p');
				$this->db->join('party_type_1 pt1', 'pt1.id = p.party_type_1', 'left');
				$this->db->join('party_type_2 pt2', 'pt2.id = p.party_type_2', 'left');
				$this->db->join('branch br', 'br.branch_id = p.branch_id', 'left');
				$this->db->join('city ct', 'ct.city_id = p.city_id', 'left');
				$this->db->join('state st', 'st.state_id = p.state_id', 'left');
				$this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
				$this->db->where('p.active !=',0);
				$this->db->where('p.party_id', $row->party_id);
				$query = $this->db->get();

				//echo "<pre>";print_r($query->result());exit;

				foreach ($query->result() as $row) {
					$data = array('id' => $row->party_id,
								  'value' => $row->party_code,
								  'label' => $row->party_code . ' - ' . $row->party_name,
								  'party_id' => $row->party_id,
								  'party_code' => $row->party_code,
								  'party_type_1' => $row->party_type_1,
								  'party_type_2' => $row->party_type_2,
								  'party_name' => $row->party_name,
								  'address' => $row->address,
								  'city' => $row->city,
								  'city_id' => $row->city_id,
								  'state' => $row->state,
								  'state_id' => $row->state_id,
								  'country' => $row->country,
								  'country_id' => $row->country_id,
								  'fax_no' => $row->fax_no,
								  'email_id' => $row->email_id,
								  'website' => $row->website,
								  'pincode' => $row->pincode,
								  'phone_no' => $row->phone_no,
								  'reference_id' => $row->reference_id,
								  'contact_persons' => $this->getContactPersonByParty($row->party_id, true)
								 );
				}

			}
		} else {
			$status = 0;
			$msg = "Lead not found !";
		}
		//echo "<pre>";print_r($data);exit;
		echo json_encode(array("status" => $status, "msg" => $msg, 'party' => $data, 'lead_data' => $lead_data));
		exit;
	}

	function search_lead()
	{
		$post_data = $this->input->post();
		$result = $this->crud->search_leads($post_data);
		set_page_ajax('sales/search_result_ajax', array("result" => $result));
	}

	function billing_terms_datatable()
	{
		$config['table'] = 'billing_terms';
		$config['select'] = 'id,billing_terms_name,billing_terms_date';
		$config['column_search'] = array('billing_terms_name','billing_terms_date');
		$config['column_order'] = array('billing_terms_name','billing_terms_date');
		$config['order'] = array('id' => 'DESC');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$cnt = 1;
		foreach ($list as $billing_terms) {
			$row = array();
			$row[] = "<a href='javascript:void(0);' class='btn_set_billing_terms' data-billing_terms_id='".$billing_terms->id."'>".$cnt."</a>";
			$row[] = "<a href='javascript:void(0);' class='btn_set_billing_terms' data-billing_terms_id='".$billing_terms->id."'>".$billing_terms->billing_terms_name."</a>";
			$row[] = "<a href='javascript:void(0);' class='btn_set_billing_terms' data-billing_terms_id='".$billing_terms->id."'>".date('d-m-Y',strtotime($billing_terms->billing_terms_date))."</a>";
			$data[] = $row;
			$cnt++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	/**
	 * @param $billing_terms_id
	 */
	function set_billing_terms_in_sales_order($billing_terms_id)
	{
		$this->db->select('*');
		$this->db->from('billing_terms_detail');
		$this->db->where('billing_terms_id', $billing_terms_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response = array();
			$sr_no = 1;
			foreach($query->result() as $row) {
				$response[] = array(
					'sr_no'=>$sr_no,
					'cal_code_id'=>$row->id,
					'cal_code'=>$row->cal_code,
					'description'=>$row->narration,
					'cal_definition_label'=>$row->cal_definition,
					'percentage'=>$row->percentage,
					'cal_definition_data'=>$this->get_cal_code_definition($row->id),
				);
				$sr_no++;
			}
			echo json_encode(array('success' => true, 'message' => 'Billing terms template data loaded.','template_data'=>$response));
		} else {
			echo json_encode(array('success' => 'no_cal_code', 'message' => 'No any cal code in this template.'));
		}
		exit();
	}

	/**
	 * @param $sales_order_id
	 * @return array
	 */
	function get_sales_order_billing_terms_data($sales_order_id)
	{
		$this->db->select('*');
		$this->db->from('sales_order_billing_terms');
		$this->db->where('sales_order_id', $sales_order_id);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			$sr_no = 1;
			foreach($query->result() as $row) {
				$response[] = array(
					'sr_no'=>$sr_no,
					'cal_code_id'=>$row->id,
					'cal_code'=>$row->cal_code,
					'description'=>$row->narration,
					'cal_definition_label'=>$row->cal_definition,
					'percentage'=>$row->percentage,
					'cal_definition_data'=>$this->get_sales_order_cal_code_definition($row->id),
				);
				$sr_no++;
			}
		}
		return $response;
	}

	/**
	 * @param $billing_terms_detail_id
	 * @return array
	 */
	function get_sales_order_cal_code_definition($billing_terms_detail_id)
	{
		$this->db->select('ccd.is_first_element,ccd.cal_operation,ccd.cal_code_id,btd.cal_code');
		$this->db->from('sales_order_cal_code_definition ccd');
		$this->db->join('sales_order_billing_terms btd', 'btd.id = ccd.cal_code_id');
		$this->db->where('ccd.billing_terms_detail_id', $billing_terms_detail_id);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$response[] = array(
					'is_first_element' => $row->is_first_element == 1?true:false,
					'cal_operation' => $row->cal_operation,
					'cal_code_id' => $row->cal_code_id,
					'cal_code' => $row->cal_code,
				);
			}
		}
		return $response;
	}

	function get_cal_code_definition($billing_terms_detail_id)
	{
		$this->db->select('ccd.is_first_element,ccd.cal_operation,ccd.cal_code_id,btd.cal_code');
		$this->db->from('cal_code_definition ccd');
		$this->db->join('billing_terms_detail btd', 'btd.id = ccd.cal_code_id');
		$this->db->where('ccd.billing_terms_detail_id', $billing_terms_detail_id);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$response[] = array(
					'is_first_element' => $row->is_first_element == 1?true:false,
					'cal_operation' => $row->cal_operation,
					'cal_code_id' => $row->cal_code_id,
					'cal_code' => $row->cal_code,
				);
			}
		}
		return $response;
	}

	/**
	 * @param $sales_order_id
	 * @param $billing_terms_id
	 */
	function add_sales_order_billing_terms($sales_order_id,$billing_terms_id){

		$billing_terms_details = $this->db->select('id,cal_code,narration,cal_definition,percentage,value,gl_acc')->from('billing_terms_detail')->where('billing_terms_id',$billing_terms_id)->order_by('id')->get()->result();
		$cal_cod_ids = array();
		foreach($billing_terms_details as $billing_terms_detail_row) {
			$billing_term_data = array(
				'sales_order_id' => $sales_order_id,
				'cal_code' => $billing_terms_detail_row->cal_code,
				'narration' => $billing_terms_detail_row->narration,
				'cal_definition' => $billing_terms_detail_row->cal_definition,
				'percentage' => $billing_terms_detail_row->percentage,
				'value' => $billing_terms_detail_row->value,
				'gl_acc' => $billing_terms_detail_row->gl_acc,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$billing_terms_detail_id = $this->crud->insert('sales_order_billing_terms',$billing_term_data);
			$cal_cod_ids["$billing_terms_detail_row->cal_code"] = $billing_terms_detail_id;
			$cal_code_definition = $this->get_cal_code_definition($billing_terms_detail_row->id);
			foreach($cal_code_definition as $cal_code_definition_row) {
				$cal_code_definition_data = array(
					'billing_terms_detail_id' => $billing_terms_detail_id,
					'sales_order_id' => $sales_order_id,
					'is_first_element' => $cal_code_definition_row['is_first_element'],
					'cal_operation' => $cal_code_definition_row['cal_operation'],
					'cal_code_id' => $cal_cod_ids[$cal_code_definition_row['cal_code']],
					'created_at' => date('Y-m-d H:i:s'),
				);
				$this->crud->insert('sales_order_cal_code_definition', $cal_code_definition_data);
			}
		}
	}

	/**
	 * Save Lead New
	 */
	function save_lead()
	{
		// echo "<pre>";print_r($_POST);exit;
		if (isset($_POST['lead_id'])) {
			$result = $this->check_party_validations_quatation($_POST['lead_data']['party_id'], $_POST['party_data']);
			if ($result['status'] == 0) {
				echo json_encode(array('status' => false, 'msg' => $result['msg']));
				exit();
			}
			$_POST['lead_data']['start_date'] = date("Y-m-d", strtotime($_POST['lead_data']['start_date']));
			$_POST['lead_data']['review_date'] = date("Y-m-d", strtotime($_POST['lead_data']['review_date']));
			$_POST['lead_data']['industry_type_id'] = !empty($_POST['lead_data']['industry_type_id']) ? $_POST['lead_data']['industry_type_id'] : NULL;
			$_POST['lead_data']['lead_owner_id'] = !empty($_POST['lead_data']['lead_owner_id']) ? $_POST['lead_data']['lead_owner_id'] : NULL;
			$_POST['lead_data']['assigned_to_id'] = !empty($_POST['lead_data']['assigned_to_id']) ? $_POST['lead_data']['assigned_to_id'] : NULL;
			$_POST['lead_data']['priority_id'] = !empty($_POST['lead_data']['priority_id']) ? $_POST['lead_data']['priority_id'] : NULL;
			$_POST['lead_data']['lead_source_id'] = !empty($_POST['lead_data']['lead_source_id']) ? $_POST['lead_data']['lead_source_id'] : NULL;
			$_POST['lead_data']['lead_provider_id'] = !empty($_POST['lead_data']['lead_provider_id']) ? $_POST['lead_data']['lead_provider_id'] : NULL;
			$_POST['lead_data']['lead_status_id'] = !empty($_POST['lead_data']['lead_status_id']) ? $_POST['lead_data']['lead_status_id'] : NULL;
			if ($_POST['lead_id'] == 0) {
				$_POST['lead_data']['created_by'] = $this->session->userdata('is_logged_in')['staff_id'];
				$_POST['lead_data']['created_at'] = date('Y-m-d H:i:s');
				$lead_id = $this->crud->insert('lead_details', $_POST['lead_data']);
				$this->crud->update('lead_details', array('lead_no' => $lead_id), array('id' => $lead_id));
			} else {
				$this->db->select('id');
				$this->db->from('lead_status');
				$this->db->where('lead_status', 'open');
				$this->db->limit(1);
				$lead_status_query = $this->db->get();
				if ($lead_status_query->num_rows() > 0) {
					$open_status_id = $lead_status_query->row()->id;
					if ($_POST['lead_data']['lead_status_id'] != $open_status_id) { //Check Lead status is close or achieved
						$this->db->select('lead_status_id');
						$this->db->from('lead_details');
						$this->db->where('id', $_POST['lead_id']);
						$this->db->limit(1);
						$lead_status_id_query = $this->db->get();
						if ($lead_status_id_query->num_rows() > 0) {
							// Check lead status is changed or not
							if ($lead_status_id_query->row()->lead_status_id != $_POST['lead_data']['lead_status_id']) {
								$_POST['lead_data']['end_date'] = date("Y-m-d");
							}
						}
					} else {
						$_POST['lead_data']['end_date'] = null;
					}
				}
				$this->crud->update('lead_details', $_POST['lead_data'], array('id' => $_POST['lead_id']));
				$lead_id = $_POST['lead_id'];
			}
			if ($_POST['lead_data']['party_id'] != 0) {
				$this->crud->update('party', $_POST['party_data'], array('party_id' => $_POST['lead_data']['party_id']));
			}
			echo json_encode(array("status" => true, "msg" => "Lead successfully saved!", "lead_id" => $lead_id));
			exit;
		} else {
			echo json_encode(array("status" => false, "msg" => "Something wrong."));
			exit;
		}
	}

	function check_party_validations($party_id)
	{
		// echo "<pre>";print_r($_POST);exit;
		$party_data = $this->input->post('party');
		$party_code = isset($party_data['party_code']) ? $party_data['party_code'] : '';
		$phone_no = isset($party_data['phone_no']) ? $party_data['phone_no'] : '';
		$email_id = isset($party_data['email_id']) ? $party_data['email_id'] : '';
		$party_name = isset($party_data['party_name']) ? $party_data['party_name'] : '';
		$address = isset($party_data['address']) ? $party_data['address'] : '';
		$city_id = isset($party_data['city_id']) ? $party_data['city_id'] : '';
		$state_id = isset($party_data['state_id']) ? $party_data['state_id'] : '';
		$country_id = isset($party_data['country_id']) ? $party_data['country_id'] : '';
		$fax_no = isset($party_data['fax_no']) ? $party_data['fax_no'] : '';
		$pincode = isset($party_data['pincode']) ? $party_data['pincode'] : '';
		$website = isset($party_data['website']) ? $party_data['website'] : '';
		$project = isset($party_data['project']) ? $party_data['project'] : '';
		$reference_description = isset($party_data['reference_description']) ? $party_data['reference_description'] : '';

		$returnStatus = 1;
		$returnMsg = "";

		$temp = $email_id;
		$emails = explode(",", $temp);
		$email_status = 1;
		$email_msg = "";

		$email_ids = array();
		// multiple email validation
		if (is_array($emails) && count($emails) > 0) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$email_msg .= "$email is not valid email.&nbsp;";
						$email_status = 0;
					} 
					$this->db->select('*');
					$this->db->from('party');
					$this->db->where('party_id !=' ,$party_id);
					$this->db->where('active !=' ,0);
					$this->db->where("FIND_IN_SET( '".$email."', email_id) ");
					$res = $this->db->get();
					$rows = $res->num_rows();
					if ($rows > 0) {
						$result = $res->result();
						$email_msg .= "$email email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name;
						$email_status = 0;
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}

		if ($email_status == 0) {
			return array('status' => 0, 'msg' => $email_msg);
		}

		$partyName = strtolower(trim($party_name));

		if ($partyName == "") {
			return array('status' => 0, 'msg' => "Customer name is required.");
		}

		// party name unique validation
		$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($partyName) . "' AND phone_no='".$phone_no."' AND party_id != $party_id";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		if (count($rows) > 0) {
			return array('status' => 0, 'msg' => "Party name already exist!");
		}

		if (trim($phone_no) == "" && count($email_ids) == 0) {
			$email_msg = "Please enter phone number or email id !";
			return array('status' => 0, 'msg' => $email_msg);
		}

		// get phone numbers
		$phone_msg = '';
		$phone_numbers = explode(",", $phone_no);
		$phone_status = 1;
		$phone_nos = array();
		foreach ($phone_numbers as $phone) {
			$phone = trim($phone);
			$this->db->select('*');
			$this->db->from('party');
			$this->db->where('party_id !=' ,$party_id);
			$this->db->where('active !=' ,0);
			$this->db->where("FIND_IN_SET( '".$phone."', phone_no) ");
			$res = $this->db->get();
			$rows = $res->num_rows();
			//echo $phone.'<br>';echo $this->db->last_query().'<br>';
			if ($rows > 0 && trim($phone) != "") {
				$result = $res->result();
				$phone_msg .= "$phone mobile already exist.&nbsp; &nbsp; <br/> Original Party : ". $result[0]->party_name;
				$phone_status = 0;
			}
		}
		if($phone_status == 0){
			echo json_encode(array("status" => 0, 'msg' => $phone_msg, 'phone_error' => 1));
			exit;
		}
		$phone_numbers = $phone_no;
		$email_ids = implode(',', $email_ids);

		$party_data = array(
			'address' => $address,
			'pincode' => $pincode,
			'phone_no' => $phone_numbers,
			'fax_no' => $fax_no,
			'email_id' => $email_ids,
			'website' => $website,
			'city_id' => $city_id,
			'state_id' => $state_id,
			'country_id' => $country_id,
			'project' => $project,
			'reference_description' => $reference_description,
			'oldApprovedDate' => '2001-01-01'
		);

		if ($party_code != "")
			$party_data['party_code'] = $party_code;

		if ($party_name != "")
			$party_data['party_name'] = $party_name;

		//echo '<pre>';print_r($party_data);exit;
		$this->db->where('party_id', $party_id);
		$this->db->update('party', $party_data);

		return array('status' => $returnStatus, 'msg' => $returnMsg);
	}

	function check_party_validations_quatation($party_id, $party_data)
	{
		// echo "<pre>";print_r($_POST);exit;
		// $party_data = $this->input->post('party');
		$party_code = isset($party_data['party_code']) ? $party_data['party_code'] : '';
		$phone_no = isset($party_data['phone_no']) ? $party_data['phone_no'] : '';
		$email_id = isset($party_data['email_id']) ? $party_data['email_id'] : '';
		$party_name = isset($party_data['party_name']) ? $party_data['party_name'] : '';
		$address = isset($party_data['address']) ? $party_data['address'] : '';
		$city_id = isset($party_data['city_id']) ? $party_data['city_id'] : '';
		$state_id = isset($party_data['state_id']) ? $party_data['state_id'] : '';
		$country_id = isset($party_data['country_id']) ? $party_data['country_id'] : '';
		$fax_no = isset($party_data['fax_no']) ? $party_data['fax_no'] : '';
		$pincode = isset($party_data['pincode']) ? $party_data['pincode'] : '';
		$website = isset($party_data['website']) ? $party_data['website'] : '';
		$project = isset($party_data['project']) ? $party_data['project'] : '';
		$reference_id = isset($party_data['reference_id']) ? $party_data['reference_id'] : 0;
		$agent_id = (int) isset($party_data['agent_id']) ? $party_data['agent_id'] : 0;
		$reference_description = isset($party_data['reference_description']) ? $party_data['reference_description'] : '';

		/*if ($reference_id == 0) {
			return array('status' => 0, 'msg' => 'please select reference.');
		}*/

		$returnStatus = 1;
		$returnMsg = "";

		$temp = $email_id;
		$emails = explode(",", $temp);
		$email_status = 1;
		$email_msg = "";

		$email_ids = array();
		// multiple email validation
		if (is_array($emails) && count($emails) > 0) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$email_msg .= "$email is not valid email.&nbsp;";
						$email_status = 0;
					}
					$this->db->select('*');
					$this->db->from('party');
					$this->db->where('party_id !=' ,$party_id);
					$this->db->where('active !=' ,0);
					$this->db->where("FIND_IN_SET( '".$email."', email_id) ");
					$res = $this->db->get();
					$rows = $res->num_rows();
					if ($rows > 0) {
						$result = $res->result();
						$email_msg .= "$email email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name;
						$email_status = 0;
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}

		if ($email_status == 0) {
			return array('status' => 0, 'msg' => $email_msg);
		}

		$partyName = strtolower(trim($party_name));

		if ($partyName == "") {
			return array('status' => 0, 'msg' => "Customer name is required.");
		}

		// party name unique validation
		$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($partyName) . "' AND phone_no='".$phone_no."' AND party_id != $party_id";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		if (count($rows) > 0) {
			return array('status' => 0, 'msg' => "Party name already exist!");
		}

		if (trim($phone_no) == "" && count($email_ids) == 0) {
			$email_msg = "Please enter phone number or email id !";
			return array('status' => 0, 'msg' => $email_msg);
		}

		// get phone numbers
		$phone_msg = '';
		$phone_numbers = explode(",", $phone_no);
		$phone_status = 1;
		$phone_nos = array();
		foreach ($phone_numbers as $phone) {
			$phone = trim($phone);
			$this->db->select('*');
			$this->db->from('party');
			$this->db->where('party_id !=' ,$party_id);
			$this->db->where('active !=' ,0);
			$this->db->where("FIND_IN_SET( '".$phone."', phone_no) ");
			$res = $this->db->get();
			$rows = $res->num_rows();
			//echo $phone.'<br>';echo $this->db->last_query().'<br>';
			if ($rows > 0 && trim($phone) != "") {
				$result = $res->result();
				$phone_msg .= "$phone mobile already exist.&nbsp; &nbsp; <br/> Original Party : ". $result[0]->party_name;
				$phone_status = 0;
			}
		}
		if($phone_status == 0){
			echo json_encode(array("status" => 0, 'msg' => $phone_msg, 'phone_error' => 1));
			exit;
		}
		$phone_numbers = $phone_no;
		$email_ids = implode(',', $email_ids);

		if ($returnStatus == 1) {
			$update_party_data = array(
				'address' => $address,
				'pincode' => $pincode,
				'phone_no' => $phone_numbers,
				'fax_no' => $fax_no,
				'email_id' => $email_ids,
				'website' => $website,
				'city_id' => $city_id,
				'state_id' => $state_id,
				'country_id' => $country_id,
				'project' => $project,
				'reference_id' => $reference_id,
				'reference_description' => $reference_description,
				'agent_id' => $agent_id
			);

			$this->db->where('party_id', $party_id);
			$this->db->update('party', $update_party_data);
		}

		return array('status' => $returnStatus, 'msg' => $returnMsg);
	}

	function get_lead_status_id_by_status($status = 'achieved'){
		return $this->crud->get_id_by_val('lead_status', 'id', 'lead_status', $status);
	}

	/**
	 * @param $id
	 * @return int
	 */
	function get_quotation_no_revisions($id)
	{
		$get_last_parent_revision_id = $this->get_last_parent_revision_id($id);
		$revised_quotation_data = $this->get_all_revision($get_last_parent_revision_id);
		return count($revised_quotation_data) - 1;
	}

	/**
	 * @return mixed
	 */
	function get_quotation_stage()
	{
		$this->db->select('id,quotation_stage');
		$this->db->from('quotation_stage');
		return $this->db->get()->result();
	}

	/**
	 * @return mixed
	 */
	function get_quotation_type()
	{
		$this->db->select('id,quotation_type');
		$this->db->from('quotation_type');
		return $this->db->get()->result();
	}

	public function generate_pdf()
	{
		$this->load->library('m_pdf');
		$pdf = new mPDF();
		$pdf->WriteHTML('<p>HTML content goes here...</p>');
		$pdf->Output('suersh.pdf', 'I');
	}

	/**
	 * @param $id
	 */
	function quotation_revise($id)
	{
		$this->db->select('*');
		$this->db->from('quotations');
		$this->db->where('id', $id);
		$this->db->limit(1);
		$query = $this->db->get();
		$quotation_data = $query->row_array();
		if($quotation_data['revised_from_id'] == '0'){
			$quotation_data['revised_from_id'] = $id;
		}
		$get_last_parent_revision_id = $this->get_last_parent_revision_id($id);
		$revised_quotation_data = $this->get_all_revision($quotation_data['revised_from_id']);
		$quotation_data['rev'] = count($revised_quotation_data) + 1;
		$quotation_data['rev_date'] = date('Y-m-d');;
		$quotation_data['created_at'] = date('Y-m-d H:i:s');
		$quotation_data['created_by'] = $this->staff_id;
		unset($quotation_data['id']);
		//pre_die($quotation_data);
		$this->db->insert('quotations', $quotation_data);
		$quotation_id = $this->db->insert_id();

		/*-------- Create Quotation Items -----------*/
		$this->db->select('*');
		$this->db->from('quotation_items');
		$this->db->where('quotation_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $item_data) {
				$item_data['quotation_id'] = $quotation_id;
				$item_data['created_at'] = date('Y-m-d H:i:s');
				/*--------Unset ID ---------*/
				unset($item_data['id']);
				$this->db->insert('quotation_items', $item_data);
			}
		}
		/*-------- Create Follow up History -----------*/
		$this->db->select('*');
		$this->db->from('followup_history');
		$this->db->where('quotation_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $followup_history_data) {
				$followup_history_data['quotation_id'] = $quotation_id;
				unset($followup_history_data['id']);
				$this->db->insert('followup_history', $followup_history_data);
			}
		}
		redirect(base_url() . "sales/quotation_edit/" . $quotation_id);
		/*echo json_encode(array('success' => true, 'message' => "Quotation saved successfully!", 'quotation_id' => $quotation_id));
		exit();*/
	}

	function get_item_status()
	{
		$this->db->select('*');
		$this->db->from('item_status');
		return $this->db->get()->result();
	}

	/**
	 * @param $id
	 */
	function get_all_revised_quotation($id)
	{
		$this->db->select('*');
		$this->db->from('quotations');
		$this->db->where('revised_from_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {

		} else {

		}
		$data = $this->db->get()->result_array();
		$return[] = array('id' => $data[0]->id, "children" => $this->get_all_revised_quotation($data[0]->id));
	}

	/**
	 * @param $id
	 * @param array $table_data
	 * @param array $result_array
	 * @return array
	 */
	function get_all_child_revision($id, $table_data = array(), &$result_array = array())
	{
		if (empty($table_data)) {
			$this->db->select('*');
			$this->db->from('quotations');
			$table_data = $this->db->get()->result();
		}
		foreach ($table_data as $row) {
			if ($id == $row->revised_from_id) {
				$result_array[] = $row;
				$this->get_all_child_revision($row->id, $table_data, $result_array);
			}
		}
		return $result_array;
	}

	/**
	 * @param $id
	 * @param array $table_data
	 * @param array $result_array
	 * @return array
	 */
	function get_all_parent_revision($id, $table_data = array(), &$result_array = array())
	{
		if (empty($table_data)) {
			$this->db->select('*');
			$this->db->from('quotations');
			$table_data = $this->db->get()->result();
		}
		foreach ($table_data as $row) {
			if ($id == $row->id) {
				$result_array[] = $row;
				$this->get_all_parent_revision($row->revised_from_id, $table_data, $result_array);
			}
		}
		return $result_array;
	}

	/**
	 * @param $id
	 * @param array $table_data
	 * @param int $parent_id
	 * @return int
	 */
	function get_last_parent_revision_id($id, $table_data = array(), &$parent_id = 0)
	{
		if (empty($table_data)) {
			$this->db->select('*');
			$this->db->from('quotations');
			$this->db->where("id",$id);
			$table_data = $this->db->get()->result();
		}
		foreach ($table_data as $row) {
			if ($id == $row->id) {
				$parent_id = $row->id;
				$this->get_last_parent_revision_id($row->revised_from_id, $table_data, $parent_id);
			}
		}
		return $parent_id;
	}

	/**
	 * @param $id
	 * @param array $table_data
	 * @param array $result_array
	 * @return array
	 */
	function get_all_revision($id, $table_data = array(), &$result_array = array())
	{
		if (empty($table_data)) {
			$this->db->select('q.id,q.revised_from_id,q.enquiry_no,q.quotation_no,q.party_name,q.rev,p.party_name as pname');
			$this->db->from('quotations q');
			$this->db->join('party p', 'p.party_id = q.party_id');
			$table_data = $this->db->get()->result();
		}
		foreach ($table_data as $row) {
			if (empty($result_array)) {
				if ($id == $row->id) {
					$result_array[] = $row;
				}
			}
			if ($id == $row->revised_from_id) {
				$result_array[] = $row;
				$this->get_all_revision($row->id, $table_data, $result_array);
			}
		}
		return $result_array;
	}

	function have_child_layer($id)
	{
		$this->db->where(array('revised_from_id' => $id));
		$query = $this->db->get('quotations');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *  Fetch Lead Owner
	 */
	function fetch_autocomplete_lead_owner()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('staff', 'name', $search,array('active !='=>0));
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->name;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Assigned To
	 */
	function fetch_autocomplete_assigned_to()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('staff', 'name', $search,array('active !='=>0));
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->name;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Lead Provider
	 */
	function fetch_autocomplete_lead_provider()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_provider', 'lead_provider', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->lead_provider,
				'value' => $row->lead_provider,
			);
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Industry Type
	 */
	function fetch_autocomplete_industry_type()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('industry_type', 'industry_type', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->industry_type;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Lead Stage
	 */
	function fetch_autocomplete_lead_stage()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_stage', 'lead_stage', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->lead_stage;
			$i++;
		}
		echo json_encode($response);
	}

	function fetch_autocomplete_order_stage()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('sales_order_stage', 'sales_order_stage', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->sales_order_stage,
				'value' => $row->sales_order_stage,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Priority
	 */
	function fetch_autocomplete_priority()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('priority', 'priority', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->priority;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Lead Source
	 */
	function fetch_autocomplete_lead_source()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_source', 'lead_source', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->lead_source;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Status
	 */
	function fetch_autocomplete_lead_status()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('lead_status', 'lead_status', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->lead_status;
			$i++;
		}
		echo json_encode($response);
	}

	function fetch_autocomplete_order_status()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('sales_order_status', 'sales_order_status', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->sales_order_status,
				'value' => $row->sales_order_status,
			);
		}
		echo json_encode($response);
	}


	/**
	 *  Fetch Reference
	 */
	function fetch_autocomplete_reference()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('reference', 'reference', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->reference;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Sales
	 */
	function fetch_autocomplete_sales()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('sales', 'sales', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->sales,
				'value' => $row->sales,
			);
		}
		echo json_encode($response);
	}

	/* Fetch Party */
	function fetch_autocomplete_party()
	{
		$search = $_GET['term'];
		$this->db->select('p.*,pt1.type,pt2.type,br.branch,ct.city,st.state,cnt.country');
		$this->db->from('party p');
		$this->db->join('party_type_1 pt1', 'pt1.id = p.party_type_1', 'left');
		$this->db->join('party_type_2 pt2', 'pt2.id = p.party_type_2', 'left');
		$this->db->join('branch br', 'br.branch_id = p.branch_id', 'left');
		$this->db->join('city ct', 'ct.city_id = p.city_id', 'left');
		$this->db->join('state st', 'st.state_id = p.state_id', 'left');
		$this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
		$this->db->where('p.active !=', 0);
		$this->db->like('p.party_name', $search);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->result();
			foreach ($result as $row) {
				$response[] = array(
					'id' => $row->party_id,
					'label' => $row->party_name,
					'value' => $row->party_name,
					'party_code' => $row->party_code,
					'address' => $row->address,
					'city' => $row->city,
					'state' => $row->state,
					'country' => $row->country,
					'fax_no' => $row->fax_no,
					'email_id' => $row->email_id,
					'website' => $row->website,
					'tin_vat_no' => $row->tin_vat_no,
					'tin_cst_no' => $row->tin_cst_no,
					'ecc_no' => $row->ecc_no,
					'pincode' => $row->pincode,
					'phone_no' => $row->phone_no,
				);
			}
			echo json_encode($response);
		}
		exit();
	}

	function fetch_autocomplete_party_type()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('party_type_1', 'type', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->type;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Sales Type
	 */
	function fetch_autocomplete_sales_type()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('sales_type', 'sales_type', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->sales_type,
				'value' => $row->sales_type,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch UOM
	 */
	function fetch_autocomplete_uom()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('uom', 'uom', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->uom,
				'value' => $row->uom,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Branch1
	 */
	function fetch_autocomplete_branch1()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('branch', 'branch', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->branch_id,
				'label' => $row->branch,
				'value' => $row->branch,
			);
		}
		echo json_encode($response);
	}

	function fetch_autocomplete_branch()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('branch', 'branch', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->branch_id,
				'label' => $row->branch,
				'value' => $row->branch,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Inquiry Status
	 */
	function fetch_autocomplete_status()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('inquiry_status', 'inquiry_status', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->inquiry_status;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Inquiry Stage
	 */
	function fetch_autocomplete_inquiry_stage()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('inquiry_stage', 'inquiry_stage', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $staffRow) {
			$response[$i] = $staffRow->inquiry_stage;
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Item Status
	 */
	function fetch_autocomplete_item_status()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('item_status', 'item_status', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->item_status,
				'value' => $row->item_status,
			);
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Currency
	 */
	function fetch_autocomplete_currency()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('currency', 'currency', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->currency,
				'value' => $row->currency,
			);
		}
		echo json_encode($response);
	}

	function fetch_autocomplete_project()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('project', 'project', $search);
		$response = array();
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->project,
				'value' => $row->project,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Quotation Stage
	 */
	function fetch_autocomplete_quotation_stage()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('quotation_stage', 'quotation_stage', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->quotation_stage,
				'value' => $row->quotation_stage,
			);
		}
		echo json_encode($response);
	}

	/**
	 *  Fetch Quotation Type
	 */
	function fetch_autocomplete_quotation_type()
	{
		$search = $_GET['term'];
		$staff = $this->crud->getAutoCompleteData('quotation_type', 'quotation_type', $search);
		$response = array();
		$i = 0;
		foreach ($staff as $row) {
			$response[] = array(
				'id' => $row->id,
				'label' => $row->quotation_type,
				'value' => $row->quotation_type,
			);
			$i++;
		}
		echo json_encode($response);
	}

	/**
	 * Fetch Enquiry Data
	 * @param string $enquiry
	 */
	function getEnquiryData($enquiry = "")
	{
		$this->db->select('i.*');
		$this->db->from('inquiry.*');
		$this->db->where(array('i.id' => $enquiry));
	}

	function viewrec()
	{
		if ($_POST['maid'] != "") {
			$myid = $_POST['maid'];
			$quotation_items = $this->crud->get_selected_items_detail_by_quotation($myid);
			echo $quotation_items->item_code . "|" . $quotation_items->item_description . "|" . $quotation_items->item_desc . "|" . $quotation_items->add_description . "|" . $quotation_items->detail_description . "|" . $quotation_items->drawing_number . "|" . $quotation_items->drawing_revision . "|" . $quotation_items->uom_id . "|" . $quotation_items->total_amount . "|" . $quotation_items->item_note . "|" . $quotation_items->quantity . "|" . $quotation_items->disc_per . "|" . $quotation_items->rate . "|" . $quotation_items->disc_value . "|" . $quotation_items->amount . "|" . $quotation_items->net_amount . "|" . $quotation_items->item_status_id . "|" . $quotation_items->cust_part_no . "|" . $quotation_items->id;
		}
	}

	function updateinfo()
	{
		if (isset($_POST['reid']) && $_POST['reid'] != "") {
			$post_quotation_item_id = $_POST['reid'];
			$quotation_item_data['item_code'] = $_POST['itemcode'];
			$quotation_item_data['item_description'] = $_POST['itemdesc'];
			$quotation_item_data['item_desc'] = $_POST['item_desc'];
			$quotation_item_data['add_description'] = $_POST['add_desc'];
			$quotation_item_data['detail_description'] = $_POST['det_desc'];
			$quotation_item_data['drawing_number'] = $_POST['draw_no'];
			$quotation_item_data['drawing_revision'] = $_POST['drga_rev'];
			$quotation_item_data['uom_id'] = $_POST['uom_id'];
			$quotation_item_data['total_amount'] = $_POST['tot_amo'];
			$quotation_item_data['item_note'] = $_POST['itnote'];
			$quotation_item_dinquiryata['quantity'] = $_POST['quant'];
			$quotation_item_data['disc_per'] = $_POST['discper'];
			$quotation_item_data['rate'] = $_POST['rate'];
			$quotation_item_data['disc_value'] = $_POST['disval'];
			$quotation_item_data['amount'] = $_POST['amount'];
			$quotation_item_data['net_amount'] = $_POST['netamo'];
			$quotation_item_data['item_status_id'] = $_POST['itstatus'];
			$quotation_item_data['cust_part_no'] = $_POST['custpart'];
			$quotation_item_data['updated_at'] = date('Y-m-d H:i:s');
			$this->crud->update('quotation_items', $quotation_item_data, array('id' => $post_quotation_item_id));
			$quotation_item_id = $post_quotation_item_id;
			$quotation_items = $this->crud->get_selected_items_detail_by_quotation($quotation_item_id);
			echo $quotation_items->id . "|" . $quotation_items->item_code . "|" . $quotation_items->item_description . "|" . $quotation_items->item_desc . "|" . $quotation_items->add_description . "|" . $quotation_items->detail_description . "|" . $quotation_items->drawing_number . "|" . $quotation_items->drawing_revision . "|" . $quotation_items->uom_id . "|" . $quotation_items->total_amount . "|" . $quotation_items->item_note . "|" . $quotation_items->quantity . "|" . $quotation_items->disc_per . "|" . $quotation_items->rate . "|" . $quotation_items->disc_value . "|" . $quotation_items->amount . "|" . $quotation_items->net_amount . "|" . $quotation_items->item_status_id . "|" . $quotation_items->cust_part_no;
		}

	}

	function getContactPersonByParty($party_id, $get_table_html = false)
	{
		$this->db->select('cp.contact_person_id,cp.priority,cp.name,cp.mobile_no,cp.phone_no,dept.department,desi.designation,cp.email');
		$this->db->from('contact_person cp');
		$this->db->join('department dept', 'dept.department_id = cp.department_id', 'left');
		$this->db->join('designation desi', 'desi.designation_id = cp.designation_id', 'left');
		$this->db->where('cp.party_id', $party_id);
		$this->db->where('cp.active !=',0);
		$this->db->order_by('cp.priority,cp.contact_person_id');
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			if ($get_table_html) {
				$html_response = '';
				foreach ($query->result() as $row) {
					$html_response .= '<tr>';
					$html_response .= '<td>' . $row->priority . '</td>';
					$html_response .= '<td>' . $row->name . '</td>';
					$html_response .= '<td>' . $row->email . '</td>';
					$html_response .= '<td>' . $row->department . '</td>';
					$html_response .= '<td>' . $row->designation . '</td>';
					$html_response .= '<td>' . $row->phone_no . '</td>';
					$html_response .= '</tr>';
				}
				return $html_response;
			} else {
				foreach ($query->result() as $row) {
					$response[] = array(
						'contact_person_id' => $row->contact_person_id,
						'priority' => $row->priority,
						'name' => $row->name,
						'phone_no' => $row->phone_no,
						'mobile_no' => $row->mobile_no,
						'email' => $row->email,
						'department' => $row->department,
						'designation' => $row->designation,
					);
				}
				return $response;
			}
		} else {
			return $response;
		}
	}

	function getKindAttnParty($party_id)
	{
		$this->db->select('cp.contact_person_id,cp.priority,cp.name,cp.mobile_no,cp.phone_no,dept.department,desi.designation,cp.email');
		$this->db->from('contact_person cp');
		$this->db->join('department dept', 'dept.department_id = cp.department_id', 'left');
		$this->db->join('designation desi', 'desi.designation_id = cp.designation_id', 'left');
		$this->db->where('cp.party_id', $party_id);
		$this->db->where('cp.active !=',0);
		$this->db->order_by('cp.priority,cp.contact_person_id');
		$this->db->limit(1);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			$response = $query->row_array(0);
		}
		return $response;
	}

	public function ajax_load_party_with_cnt_person($party_id = ''){
		$this->load->helper('url');
        $return = array();
        if(!empty($party_id)){
            $row = $this->crud->load_party_with_cnt_person_3($party_id);
            $return = array(
                'value' => $row->party_code,
                'label' => $row->party_code . ' - ' . $row->party_name,
                'party_id' => $row->party_id,
                'party_code' => $row->party_code,
                'party_name' => $row->party_name,
                'branch_id' => $row->branch_id,
                'address' => $row->address,
                'city' => $row->city,
                'city_id' => $row->city_id,
                'state' => $row->state,
                'state_id' => $row->state_id,
                'country' => $row->country,
                'country_id' => $row->country_id,
                'fax_no' => $row->fax_no,
                'email_id' => $row->email_id,
                'website' => $row->website,
                'pincode' => $row->pincode,
                'phone_no' => $row->phone_no,
                'whatsapp_no' => $row->whatsapp_no,
                'project' => $row->project,
                'reference_id' => $row->reference_id,
                'agent_id' => $row->agent_id,
                'branch_id' => $row->branch_id,
                'reference_description' => $row->reference_description,
                'party_type_1_name' => $row->party_type_1_name,
                'party_type_2_name' => $row->party_type_2_name,
                'party_type_1_id' => $row->party_type_1_id,
                'party_type_2_id' => $row->party_type_2_id,
                'contact_persons' => $this->getContactPersonByParty($row->party_id, true),
                'contact_persons_array' => $this->getContactPersonByParty($row->party_id),
            );
        }
		//echo "<pre>";print_r($return);exit;
		print json_encode($return);
		exit;
	}

	public function ajax_load_item_details($item_id = 0)
	{
		if ($item_id != 0) {
			$this->load->helper('url');
			$row = $this->crud->load_item_details_where($item_id);

			$return = array(
				'id' => $row->id,
				'item_name' => $row->item_name,
				'item_code' => $row->item_code1,
				'item_status' => $row->item_status,
				'item_status_id' => $row->item_status_id,
				'item_category' => $row->item_status_id,
				'uom' => $row->uom,
				'item_rate' => $row->rate,
				'item_rate_in' => $row->rate_in,
				'igst' => $row->igst,
				'sgst' => $row->sgst,
				'cgst' => $row->cgst,

			);
			print json_encode($return);
			exit;
		}
	}

	function moneyFormatIndia($num)
	{
		$explrestunits = "";
		if (strlen($num) > 3) {
			$lastthree = substr($num, strlen($num) - 3, strlen($num));
			$restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);
			for ($i = 0; $i < sizeof($expunit); $i++) {
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0) {
					$explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				} else {
					$explrestunits .= $expunit[$i] . ",";
				}
			}
			$thecash = $explrestunits . $lastthree;
		} else {
			$thecash = $num;
		}
		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	public function ajax_load_item_edit_details($item_id)
	{
		$this->load->helper('url');
		$row = $this->crud->load_item_edit_details_where($item_id);
		$iteam_data = json_decode($row->item_data);
		//echo "<pre>";print_r($test);exit;
		$return = array(
			'id' => $row->id,
			'item_id' => $iteam_data->item_id,
			'inquiry_id' => $row->inquiry_id,
			'item_code' => $iteam_data->item_code,
			'item_name' => $iteam_data->item_name,
			'cust_desc' => $iteam_data->cust_desc,
			//'item_rate' => isset($iteam_data->item_rate) ? $this->moneyFormatIndia($iteam_data->item_rate) : 0,
			'item_rate' => isset($iteam_data->item_rate) ? $iteam_data->item_rate : 0,
			'quantity' => $iteam_data->quantity
		);
		//echo "<pre>";print_r($return);exit;
		print json_encode($return);
		exit;
	}

	function get_enquiry_received_by_detail($party_id)
	{
		$this->db->select('p.email_id,p.fax_no,cp.name as verbal');
		$this->db->from('party p');
		$this->db->join('contact_person cp', 'cp.party_id = p.party_id', 'left');
		$this->db->where('p.party_id', $party_id);
		$this->db->where('cp.active !=',0);
		$this->db->order_by('cp.priority,cp.contact_person_id');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$received_by_detail = $query->row_array(0);
			$received_by_detail['email_id'] = explode(',', $received_by_detail['email_id']);
			$received_by_detail['email_id'] = count($received_by_detail['email_id']) > 0 ? $received_by_detail['email_id'][0] : $received_by_detail['email_id'];
			echo json_encode(array('success' => true, 'message' => "received_by_detail", 'received_by_detail' => $received_by_detail));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => "wrong!"));
			exit();
		}
	}

	function get_enquiry_send_by_detail($party_id)
	{
		$this->db->select('p.email_id,p.fax_no,cp.name as verbal');
		$this->db->from('party p');
		$this->db->join('contact_person cp', 'cp.party_id = p.party_id', 'left');
		$this->db->where('p.party_id', $party_id);
		$this->db->where('cp.active !=',0);
		$this->db->order_by('cp.priority,cp.contact_person_id');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$send_by_detail = $query->row_array(0);
			$send_by_detail['email_id'] = explode(',', $send_by_detail['email_id']);
			$send_by_detail['email_id'] = count($send_by_detail['email_id']) > 0 ? $send_by_detail['email_id'][0] : $send_by_detail['email_id'];
			echo json_encode(array('success' => true, 'message' => "send_by_detail", 'send_by_detail' => $send_by_detail));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => "wrong!"));
			exit();
		}
	}

	/**
	 * @param $party_id
	 */
	function get_party_by_id($party_id)
	{
		$this->db->select('p.*');
		$this->db->from('party p');
		$this->db->where('p.party_id', $party_id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$party_data = $query->row_array(0);
			$party_data['email_id'] = $party_data['email_id'];
			$party_data['phone_no'] = $party_data['phone_no'];
			$party_data['party_contact_person'] = $this->getContactPersonByParty($party_id);
			echo json_encode(array('success' => true, 'message' => "Party Data", 'party_data' => $party_data));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => "wrong!"));
			exit();
		}
	}

	function get_contact_no_of_kinn_attn($contact_person_id)
	{
		$this->db->select('phone_no,mobile_no');
		$this->db->from('contact_person');
		$this->db->where('contact_person_id', $contact_person_id);
		$this->db->where('active !=',0);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			if ($query->row()->mobile_no != '') {
				echo $query->row()->mobile_no;
			} else {
				echo $query->row()->phone_no;
			}
		} else {
			echo '';
		}
	}

	function track_sale()
	{
		$data = array();
		$data['users'] = $this->db->select('staff_id,name')->from('staff')->where('active !=',0 )->order_by('name','asc')->get()->result();
		if($this->applib->have_access_role(SALES_TRACK_SALE_MENU_ID,"view")) {
			set_page('sales/track_sale', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}

	}

	function track_sale_datetable()
	{
		$requestData = $_REQUEST;

		$config['table'] = 'inquiry e';
		$config['select'] = 'e.inquiry_id, e.inquiry_no, s.inquiry_status,p.party_name,p.party_code,p.outstanding_balance,e.inquiry_date,q.quotation_date,so.sales_order_date,pi.po_date,ch.challan_date,inc.committed_date as invoice_date';
		$config['column_order'] = array(null, 'e.inquiry_no', 's.inquiry_status', 'p.party_name', 'p.party_code', 'e.inquiry_date','q.quotation_date','so.sales_order_date','pi.po_date','ch.challan_date','invoice_date');
		$config['column_search'] = array('e.inquiry_no', 's.inquiry_status', 'p.party_name', 'p.party_code', 'e.inquiry_date');
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$config['likes'][] = array('p.party_name' => $requestData['columns'][1]['search']['value']);
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$config['likes'][] = array('e.inquiry_no' => $requestData['columns'][2]['search']['value']);
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			$config['likes'][] = array('p.party_code' => $requestData['columns'][3]['search']['value']);
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {
			$config['likes'][] = array('s.inquiry_status' => $requestData['columns'][4]['search']['value']);
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {
			$config['likes'][] = array('e.inquiry_date' => $requestData['columns'][5]['search']['value']);
		}
		$config['joins'][0] = array('join_table' => 'inquiry_status s', 'join_by' => 's.id = e.inquiry_status_id', 'join_type' => 'left');
		$config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = e.party_id', 'join_type' => 'left');
		$config['joins'][2] = array('join_table' => 'quotations q', 'join_by' => 'q.enquiry_id = e.inquiry_id', 'join_type' => 'left');
		$config['joins'][3] = array('join_table' => 'sales_order so', 'join_by' => 'so.quotation_id = q.id', 'join_type' => 'left');
		$config['joins'][4] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.quotation_id = q.id', 'join_type' => 'left');
		$config['joins'][5] = array('join_table' => 'challans ch', 'join_by' => 'ch.sales_order_id = so.id', 'join_type' => 'left');
		$config['joins'][6] = array('join_table' => 'invoices inc', 'join_by' => 'inc.sales_order_id = so.id', 'join_type' => 'left');
		$config['order'] = array('e.inquiry_id' => 'desc');
		$config['wheres'][] = array('column_name','p.active !=','column_value'=>'0');
		$config['where_string'] = ' 1 = 1 ';

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		
		if($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER, "allow") == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$config['where_string'] .= ' AND ( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if($cu_accessDomestic == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}

		$inquiry_status = $this->input->get_post('inquiry_status');
		$user_id = $this->input->get_post('user_id');
		if ($inquiry_status != '' && $inquiry_status != 'all') {
			$inquiry_status_id = $this->crud->get_id_by_val('inquiry_status', 'id', 'inquiry_status', $inquiry_status);
			$config['where_string'] .= " AND e.inquiry_status_id = '" . $inquiry_status_id . "' ";
		}
		if ($user_id != '' && $user_id != 'all') {
			$config['where_string'] .= " AND e.assigned_to_id = '" . $user_id . "' ";
		}
		//echo '<pre>'; print_r($config);die;
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		$role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");

		//e.inquiry_id, e.inquiry_no, s.inquiry_status,p.party_name,p.party_code,e.inquiry_date,q.quotation_date,so.sales_order_date,pi.po_date,ch.challan_date
		foreach ($list as $enquiry_row) {
			$row = array();
			$action = '';
			$row[] = $enquiry_row->party_name.'('.$enquiry_row->party_code.')';
			$row[] = date('d-m-Y', strtotime($enquiry_row->inquiry_date));
			$row[] = ($enquiry_row->quotation_date) ? date('d-m-Y', strtotime($enquiry_row->quotation_date)) : '-';
			$row[] = ($enquiry_row->po_date) ? date('d-m-Y', strtotime($enquiry_row->po_date)) : '-';
			$row[] = ($enquiry_row->sales_order_date) ? date('d-m-Y', strtotime($enquiry_row->sales_order_date)) : '-';
			$row[] = ($enquiry_row->challan_date) ? date('d-m-Y', strtotime($enquiry_row->challan_date)) : '-';
			$row[] = ($enquiry_row->invoice_date) ? date('d-m-Y', strtotime($enquiry_row->invoice_date)) : '-';
			$row[] = $enquiry_row->outstanding_balance;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	/**
	 * @param $id
	 */
	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
	}
	function company_detail(){
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id',COMPANY_ID);
		$this->db->limit(1);
		$query = $this->db->get();
		$results = $query->result();
		return $results;

	}

}
