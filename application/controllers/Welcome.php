<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * Class Welcome
 * @property AppLib $app_lib
 * @property Crud $crud
 * @property Chat $chat
 */
class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Chatmodule','chat');
		$this->load->model('Crud', 'crud');
	}

	public function index_v1()
	{
		/*----- Log ----------*/
		$this->load->helper('log4php');
		log_info('Test log message');
		$logged_in = $this->session->userdata("is_logged_in");
		log_info(print_r($logged_in,true));
		/*----- Log ----------*/

		$staff_id = $logged_in['staff_id'];
		$data['agents'] = $this->chat->getAllAgents($staff_id);
		$data['reminder_data'] = $this->crud->get_all_reminder_data_by_staff($staff_id);
		$data = array();
		if ($this->session->userdata('is_logged_in')) {
			set_page('welcome', $data);
		} else {
			$this->load->view('auth/login_form');
		}
	}

	public function index()
	{
		$this->load->helper('log4php');
		log_info('Test log message');
		$data = array();
		$logged_in = $this->session->userdata("is_logged_in");
		log_info(print_r($logged_in,true));

		$id = $logged_in['staff_id'];
		$data['agents'] = $this->chat->getAllAgents($id);
        $data['users'] = $this->crud->getFromSQL("SELECT * FROM `staff` WHERE `active`= 1 ORDER BY `staff`.`name` ASC");
        $currunt_date = strtotime(date('Y-m-d'));
        $prev_date = strtotime(' -'.OLD_VISITOR_CHAT_DAYS.' days', $currunt_date);
        $prev_date = date('Y-m-d', $prev_date);
        $data['visitors'] = $this->crud->get_all_with_where('visitors', 'created_at', 'DESC', array('created_at >=' => $prev_date));
        $data['chat_roles'] = $this->crud->getUserChatRoleIDS($id);
        
//		$where_array['reminder_date'] = '<= CURDATE()';
//		$data['reminder'] = $this->crud->get_all_pending_reminder($id);
//		$data['current_user_id'] = $id;
//		$data['chart_data'] = $this->crud->get_chart_data();
//		$where_staff_id_array['staff_id'] = $id;
		
		if($this->session->userdata('is_logged_in')){
			set_page('welcome',$data);
		}else{
			$this->load->view('auth/login_form');
		}
	}

	function get_modules_num_of_count(){
		$data = array();
		$logged_in = $this->session->userdata("is_logged_in");
		$id = $logged_in['staff_id'];
                
                $today_pending_install = $this->crud->pending_install($id,true);
		$data['today_pending_install'] = count($today_pending_install);
                $overdue_pending_install = $this->crud->pending_install($id);
		$data['overdue_pending_install'] = count($overdue_pending_install);
                
                $today_pending_testing = $this->crud->pending_testing($id,true);
		$data['today_pending_testing'] = count($today_pending_testing);
                $overdue_pending_testing = $this->crud->pending_testing($id);
		$data['overdue_pending_testing'] = count($overdue_pending_testing);
                
                $today_pending_invoice = $this->crud->today_pending_invoice($id,true);
		$data['today_pending_invoice'] = count($today_pending_invoice);
                $overdue_pending_invoice = $this->crud->today_pending_invoice($id);
		$data['overdue_pending_invoice'] = count($overdue_pending_invoice);
                
                $today_reminder = $this->crud->today_pending_reminder($id,true);
		$data['today_reminder_no'] = count($today_reminder);
		$overdue_reminder = $this->crud->today_pending_reminder($id,false);
		$data['overdue_reminder_no'] = count($overdue_reminder);

		$today_pending_inquiry = $this->crud->today_pending_inquiry($id,true);
		$data['today_pending_inquiry'] = count($today_pending_inquiry);
		$overdue_pending_inquiry = $this->crud->today_pending_inquiry($id);
		$data['overdue_pending_inquiry'] = count($overdue_pending_inquiry);

		$today_pending_lead = $this->crud->today_pending_lead($id,true);
		$data['today_pending_lead'] = count($today_pending_lead);
		$overdue_pending_lead = $this->crud->today_pending_lead($id);
		$data['overdue_pending_lead'] = count($overdue_pending_lead);

		$today_pending_quotation = $this->crud->today_pending_quotation($id,true);
		$data['today_pending_quotation'] = count($today_pending_quotation);
		$overdue_pending_quotation = $this->crud->today_pending_quotation($id);
		$data['overdue_pending_quotation'] = count($overdue_pending_quotation);

//		$today_pending_quotation_followup = $this->crud->today_pending_quotation_followup($id,true);
//		$data['today_pending_quotation_followup'] = count($today_pending_quotation_followup);
//		$overdue_pending_quotation_followup = $this->crud->today_pending_quotation_followup($id,false);
//		$data['overdue_pending_quotation_followup'] = count($overdue_pending_quotation_followup);

		$today_dispatch = $this->crud->today_sales_order($id,true);
		$data['today_pending_dispatch'] = count($today_dispatch);
		$overdue_dispatch = $this->crud->today_sales_order($id);
		$data['overdue_pending_dispatch'] = count($overdue_dispatch);

		$today_sales_order_to_approve = $this->crud->sales_order_to_approve($id,true);
		$data['today_sales_order_to_approve'] = count($today_sales_order_to_approve);
		$overdue_sales_order_to_approve = $this->crud->sales_order_to_approve($id);
		$data['overdue_sales_order_to_approve'] = count($overdue_sales_order_to_approve);

		$today_unresolve_complain = $this->crud->today_unresolve_complain($id,true);
		$data['today_unresolve_complain'] = count($today_unresolve_complain);
		$overdue_unresolve_complain = $this->crud->today_unresolve_complain($id);
		$data['overdue_unresolve_complain'] = count($overdue_unresolve_complain);
        
        $today_resolve_complain = $this->crud->today_resolve_complain($id,true);
		$data['today_resolve_complain'] = count($today_resolve_complain);
		$overdue_resolve_complain = $this->crud->today_resolve_complain($id);
		$data['overdue_resolve_complain'] = count($overdue_resolve_complain);
//        $staff_id = $logged_in['staff_id'];
//        $data['total_pending_enquiry'] = $this->crud->get_num_rows_with_where('inquiry',array('assigned_to_id'=>$staff_id,'inquiry_status_id'=>$this->crud->get_id_by_val('inquiry_status','id','LOWER(inquiry_status)','pending')));
		$data['total_pending_enquiry_leads'] = $data['today_pending_inquiry'] + $data['overdue_pending_inquiry'] + $data['today_pending_lead'] + $data['overdue_pending_lead'];
		$data['total_pending_quatation'] = $data['today_pending_quotation'] + $data['overdue_pending_quotation'];
		$data['dispatched'] = $this->crud->get_challan_num_rows();
        
        $data['total_pending_dispatch'] = 0;
        $today_dispatch_so_id = array_values(array_column($today_dispatch, 'id'));
		$overdue_dispatch_so_id = array_values(array_column($overdue_dispatch, 'id'));
		$dispatch_so_id = array_merge($today_dispatch_so_id,$overdue_dispatch_so_id);
		$total_pending_dispatch = $this->crud->get_pending_dispach($dispatch_so_id,$overdue_dispatch_so_id);
        if(!empty($total_pending_dispatch)){
            $total_pending_dispatch = $total_pending_dispatch[0];
            $data['total_pending_dispatch'] = $total_pending_dispatch->total_quantity - $total_pending_dispatch->dispatched_qty;
        }
        
//        $today_dispatch_pi = $this->crud->today_proforma_invoice($id,true);
//		$overdue_dispatch_pi = $this->crud->today_proforma_invoice($id);
//        $data['total_pending_dispatch_pi'] = 0;
//        $today_dispatch_pi_id = array_values(array_column($today_dispatch_pi, 'id'));
//		$overdue_dispatch_pi_id = array_values(array_column($overdue_dispatch_pi, 'id'));
//		$dispatch_pi_id = array_merge($today_dispatch_pi_id,$overdue_dispatch_pi_id);
//		$total_pending_dispatch_pi = $this->crud->get_pending_dispach_pi($dispatch_pi_id,$overdue_dispatch_pi_id);
//        if(!empty($total_pending_dispatch_pi)){
//            $total_pending_dispatch_pi = $total_pending_dispatch_pi[0];
//            $data['total_pending_dispatch_pi'] = $total_pending_dispatch_pi->total_quantity - $total_pending_dispatch_pi->dispatched_qty;
//        }
        
//        $data['total_pending_dispatch'] = $data['total_pending_dispatch'] + $data['total_pending_dispatch_pi'];
        $data['total_pending_dispatch'] = $data['total_pending_dispatch'];
        
		$data['success'] = true;
        echo json_encode($data);
        exit;
	}
	function get_reminder_by_date($created_by = ''){
        if($created_by == 'null'){
            $created_by = '';
        }
//        echo "<pre>"; print_r($_POST);
//        echo $created_by;
        session_write_close();
		$selected_date = date('Y-m-d',strtotime($_POST['date']));
		$onLoad = $_POST['from'];
		$data = array();
		$logged_in = $this->session->userdata("is_logged_in");
		$id = $logged_in['staff_id'];
		//$data['agents'] = $this->chat->getAllAgents($id);
		$data['reminder'] = array();
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"view")) {
			$data['reminder'] = $this->crud->get_all_pending_reminder($id, $selected_date, $onLoad, $created_by);
//             echo "<pre>"; print_r($data['reminder']);
			foreach($data['reminder'] as $k => $v)
			{
				$data['reminder'][$k]['review_date'] = date("Y-m-d", strtotime($data['reminder'][$k]['reminder_date']));
				$data['reminder'][$k]['assigned_name'] =  $this->crud->get_id_by_val('staff','name','staff_id',$data['reminder'][$k]['assigned_to']);
			}
		}

		$data['quotation_followup'] = array();
        $today_pending_quotation_followup = 0;
		$overdue_pending_quotation_followup = 0;
		
        if(isset($_POST['from']) && $_POST['from'] == 'onLoad'){
            
            if ($this->applib->have_access_role(QUOTATION_MODULE_ID, "view")) {
                $quotation_followup_overdue = $this->crud->today_pending_quotation_followup($id ,true ,$selected_date, $onLoad, $created_by);
                //print_r($this->db->last_query());exit;
                $data['quotation_followup'] = $quotation_followup_overdue;
                foreach($data['quotation_followup'] as $k => $v){
                    $current_date = date('Y-m-d');
                    $data['quotation_followup'][$k]['review_date'] = date("Y-m-d", strtotime($data['quotation_followup'][$k]['reminder_date']));

                    if($current_date == $data['quotation_followup'][$k]['review_date']){
                        $today_pending_quotation_followup++;
                    } else {
                        $overdue_pending_quotation_followup++;
                    }

                    $assigned_to_ids = $data['quotation_followup'][$k]['assigned_to'];
                    if(!empty($assigned_to_ids)){
                        $assigned_to_arr = explode(',', $assigned_to_ids);
                        $assigned_to_names = array();
                        foreach ($assigned_to_arr as $assigned_to){
                            $assigned_to_names[] = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $assigned_to);
                        }
                        $data['quotation_followup'][$k]['assigned_to'] = implode(', ',$assigned_to_names);
                    }
                }
            }
            
            $data['combine_array'] = array_merge($data['reminder'],$data['quotation_followup']);
        } else {
            
            if ($this->applib->have_access_role(QUOTATION_MODULE_ID, "view")) {
                $quotation_followup_overdue = $this->crud->today_pending_quotation_followup($id ,true ,$selected_date, $onLoad, $created_by);
                //print_r($this->db->last_query());exit;
                $data['quotation_followup'] = $quotation_followup_overdue;
                foreach($data['quotation_followup'] as $k => $v){
                    $data['quotation_followup'][$k]['review_date'] = date("Y-m-d", strtotime($data['quotation_followup'][$k]['reminder_date']));
                    $assigned_to_ids = $data['quotation_followup'][$k]['assigned_to'];
                    if(!empty($assigned_to_ids)){
                        $assigned_to_arr = explode(',', $assigned_to_ids);
                        $assigned_to_names = array();
                        foreach ($assigned_to_arr as $assigned_to){
                            $assigned_to_names[] = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $assigned_to);
                        }
                        $data['quotation_followup'][$k]['assigned_to'] = implode(', ',$assigned_to_names);
                    }
                }
            }
            
            $data['quotation_review'] = array();
            if ($this->applib->have_access_role(QUOTATION_MODULE_ID, "view")) {
                $data['quotation_review'] = $this->crud->get_all_pending_review_quotation($id,$selected_date, $onLoad, $created_by);
                //print_r($this->db->last_query());exit;
                foreach($data['quotation_review'] as $k => $v)
                {
                    $data['quotation_review'][$k]['review_date'] = date("Y-m-d", strtotime($data['quotation_review'][$k]['due_date']));
                }
            }
            $data['sales_order_due'] = array();
            if ($this->applib->have_access_role(SALES_ORDER_MODULE_ID, "view")) {
                $data['sales_order_due'] = $this->crud->get_all_due_sales_order($id,$selected_date,1,$onLoad, $created_by);
                foreach($data['sales_order_due'] as $k => $v)
                {
                    $data['sales_order_due'][$k]['review_date'] = date("Y-m-d", strtotime($data['sales_order_due'][$k]['committed_date']));
                    $data['sales_order_due'][$k]['approve'] = 1;
                }
            }
            
            $data['pending_invoice'] = array();
            if ($this->applib->have_access_role(CHALLAN_MODULE_ID, "view")) {
                $data['pending_invoice'] = $this->crud->get_all_due_pending_invoice($id,$selected_date,1,$onLoad, $created_by);
            }
            $data['pending_testing'] = array();
            if ($this->applib->have_access_role(CHALLAN_MODULE_ID, "view")) {
                $data['pending_testing'] = $this->crud->get_all_due_pending_testing($id,$selected_date,1,$onLoad, $created_by);
            }
            $data['pending_install'] = array();
            if ($this->applib->have_access_role(TESTING_REPORT_MODULE_ID, "view")) {
                $data['pending_install'] = $this->crud->get_all_due_pending_install($id,$selected_date,1,$onLoad, $created_by);
            }
//            print_r($data['pending_invoice']); exit;
            $data['sales_order_to_approve'] = array();
            if ($this->applib->have_access_role(SALES_ORDER_MODULE_ID, "view")) {
                $data['sales_order_to_approve'] = $this->crud->get_all_due_sales_order($id,$selected_date,null,$onLoad, $created_by);
                foreach($data['sales_order_to_approve'] as $k => $v)
                {
                    $data['sales_order_to_approve'][$k]['review_date'] = date("Y-m-d", strtotime($data['sales_order_to_approve'][$k]['committed_date']));
                    $data['sales_order_to_approve'][$k]['approve'] = 0;
                }
            }

            $data['pending_inquiry'] = array();
            if ($this->applib->have_access_role(ENQUIRY_MODULE_ID, "view")) {
                $data['pending_inquiry'] = $this->crud->get_all_pending_inquiry($id,$selected_date,$onLoad, $created_by);
                foreach($data['pending_inquiry'] as $k => $v)
                {
                    $data['pending_inquiry'][$k]['review_date'] = date("Y-m-d", strtotime($data['pending_inquiry'][$k]['due_date']));
                }
            }

            $data['unresolve_complain'] = array();
            if ($this->applib->have_access_role(COMPLAIN_MODULE_ID, "view")) {
                $data['unresolve_complain'] = $this->crud->get_all_unresolve_complain($id,$selected_date,$onLoad, $created_by);
                foreach($data['unresolve_complain'] as $k => $v)
                {
                    $data['unresolve_complain'][$k]['review_date'] = date("Y-m-d", strtotime($data['unresolve_complain'][$k]['complain_date']));
                }
            }
            
            $data['resolve_complain'] = array();
            if ($this->applib->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "view")) {
                $data['resolve_complain'] = $this->crud->get_all_resolve_complain($id,$selected_date,$onLoad, $created_by);
                foreach($data['resolve_complain'] as $k => $v)
                {
                    $data['resolve_complain'][$k]['review_date'] = date("Y-m-d", strtotime($data['unresolve_complain'][$k]['resolve_complain_date']));
                }
            }
            
            $data['combine_array'] = array_merge($data['reminder'],$data['quotation_review'],$data['sales_order_due'],$data['sales_order_to_approve'],$data['pending_inquiry'],$data['unresolve_complain'],$data['resolve_complain'],$data['quotation_followup'], $data['pending_invoice'], $data['pending_testing']);	
//		echo "<pre>";print_r($data['combine_array']);exit;
            
                }
		$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
		//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
		$data['current_user_id'] = $id;
		$reminder_count = count($data['combine_array']);
		$reminder_html = $this->load->view('shared/reminder_html',$data,true);
//                echo "<pre>";print_r($reminder_html);exit;
		echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html, 'reminder_count'=>$reminder_count, 'today_pending_quotation_followup'=>$today_pending_quotation_followup, 'overdue_pending_quotation_followup'=>$overdue_pending_quotation_followup));
	}

	function get_reminder_by_anchor(){
		$today = $_POST['today'];
		$type = $_REQUEST['typename'];
		//echo "<pre>";print_r($_REQUEST);exit;
		$data = array();
		$logged_in = $this->session->userdata("is_logged_in");
		$id = $logged_in['staff_id'];
		//$data['agents'] = $this->chat->getAllAgents($id);
		//$data['reminder'] = $this->crud->get_all_pending_reminder($id,$selected_date);

		if($type == 'reminder'){
			if($today == 'true'){
				$data['reminder'] = $this->crud->today_pending_reminder($id,true);
			}else {
				$data['reminder'] = $this->crud->today_pending_reminder($id,false);
			}

			foreach($data['reminder'] as $k => $v) {
				$data['reminder'][$k]['review_date'] = date("Y-m-d", strtotime($data['reminder'][$k]['reminder_date']));
				$data['reminder'][$k]['assigned_name'] =  $this->crud->get_id_by_val('staff','name','staff_id',$data['reminder'][$k]['assigned_to']);
			}
			$data['combine_array'] = array_merge($data['reminder']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		if($type == 'lead'){
			if($today == 'true'){
				$data['pending_lead'] = $this->crud->today_pending_lead($id,true);
			}else {
				$data['pending_lead'] = $this->crud->today_pending_lead($id);
			}

			foreach($data['pending_lead'] as $k => $v)
			{
				$data['pending_lead'][$k]['review_date'] = date("Y-m-d", strtotime($data['pending_lead'][$k]['due_date']));
				$data['pending_lead'][$k]['lead_or_inquiry'] = 'lead';
			}


			$data['combine_array'] = array_merge($data['pending_lead']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		if($type == 'inquiry'){
			if($today == 'true'){
				$data['pending_inquiry'] = $this->crud->today_pending_inquiry($id,true);
			}else {
				$data['pending_inquiry'] = $this->crud->today_pending_inquiry($id);
			}

			foreach($data['pending_inquiry'] as $k => $v)
			{
				$data['pending_inquiry'][$k]['review_date'] = date("Y-m-d", strtotime($data['pending_inquiry'][$k]['due_date']));
				$data['pending_inquiry'][$k]['lead_or_inquiry'] = 'inquiry';
			}

			$data['combine_array'] = array_merge($data['pending_inquiry']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		if($type == 'quotation'){
			if($today == 'true'){
				$data['pending_quotation'] = $this->crud->today_pending_quotation($id,true);
			}else {
				$data['pending_quotation'] = $this->crud->today_pending_quotation($id);
			}

			foreach($data['pending_quotation'] as $k => $v)
			{
				$data['pending_quotation'][$k]['review_date'] = date("Y-m-d", strtotime($data['pending_quotation'][$k]['created_at']));
				$data['pending_quotation'][$k]['lead_or_inquiry'] = 'inquiry';
			}

			$data['combine_array'] = array_merge($data['pending_quotation']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		if($type == 'quotation_followup'){
			if($today == 'true'){
				$data['quotation_followup'] = $this->crud->today_pending_quotation_followup($id,true);
			} else {
				$data['quotation_followup'] = $this->crud->today_pending_quotation_followup($id);
			}

			foreach($data['quotation_followup'] as $k => $v){
				$data['quotation_followup'][$k]['review_date'] = date("Y-m-d", strtotime($data['quotation_followup'][$k]['reminder_date']));
				$assigned_to_ids = $data['quotation_followup'][$k]['assigned_to'];
				if(!empty($assigned_to_ids)){
					$assigned_to_arr = explode(',', $assigned_to_ids);
					$assigned_to_names = array();
					foreach ($assigned_to_arr as $assigned_to){
						$assigned_to_names[] = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $assigned_to);
					}
					$data['quotation_followup'][$k]['assigned_to'] = implode(', ',$assigned_to_names);
				}
			}

			$data['combine_array'] = array_merge($data['quotation_followup']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		if($type == 'dispatch'){
			if($today == 'true'){
				$data['sales_order_due'] = $this->crud->today_sales_order($id,true);
			}else {
				$data['sales_order_due'] = $this->crud->today_sales_order($id);
			}

			//$data['sales_order_due'] = $this->crud->get_all_due_sales_order($id);
			foreach($data['sales_order_due'] as $k => $v)
			{
				$data['sales_order_due'][$k]['review_date'] = date("Y-m-d", strtotime($data['sales_order_due'][$k]['committed_date']));
				$data['sales_order_due'][$k]['approve'] = 1;
			}

			$data['combine_array'] = array_merge($data['sales_order_due']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		if($type == 'order_to_approve'){
			if($today == 'true'){
				$data['sales_order_to_approve'] = $this->crud->sales_order_to_approve($id,true);
			}else {
				$data['sales_order_to_approve'] = $this->crud->sales_order_to_approve($id);
			}

			//$data['sales_order_due'] = $this->crud->get_all_due_sales_order($id);
			foreach($data['sales_order_to_approve'] as $k => $v)
			{
				$data['sales_order_to_approve'][$k]['review_date'] = date("Y-m-d", strtotime($data['sales_order_to_approve'][$k]['committed_date']));
				$data['sales_order_to_approve'][$k]['approve'] = 0;
			}

			$data['combine_array'] = array_merge($data['sales_order_to_approve']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		if($type == 'unresolve_complain'){
			if($today == 'true'){
				$data['unresolve_complain'] = $this->crud->today_unresolve_complain($id,true);
			}else {
				$data['unresolve_complain'] = $this->crud->today_unresolve_complain($id);
			}
			//echo "<pre>";print_r($data['unresolve_complain']);exit;
			foreach($data['unresolve_complain'] as $k => $v)
			{
				$data['unresolve_complain'][$k]['review_date'] = date("Y-m-d", strtotime($data['unresolve_complain'][$k]['complain_date']));
			}

			$data['combine_array'] = array_merge($data['unresolve_complain']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
			//echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}
        
        if($type == 'resolve_complain'){
			if($today == 'true'){
				$data['resolve_complain'] = $this->crud->today_resolve_complain($id,true);
			}else {
				$data['resolve_complain'] = $this->crud->today_resolve_complain($id);
			}
			//echo "<pre>";print_r($data['unresolve_complain']);exit;
			foreach($data['resolve_complain'] as $k => $v)
			{
				$data['resolve_complain'][$k]['review_date'] = date("Y-m-d", strtotime($data['resolve_complain'][$k]['resolve_complain_date']));
			}

			$data['combine_array'] = array_merge($data['resolve_complain']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			//$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC,'created_at'=>SORT_DESC));
			$data['current_user_id'] = $id;
//			echo "<pre>";print_r($data);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}
        
		if($type == 'pending_invoice'){
			if($today == 'true'){
				$data['pending_invoice'] = $this->crud->today_pending_invoice($id,true);
			}else {
				$data['pending_invoice'] = $this->crud->today_pending_invoice($id);
			}
			$data['combine_array'] = array_merge($data['pending_invoice']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			$data['current_user_id'] = $id;
//			echo "<pre>";print_r($data['pending_invoice']);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}
                
		if($type == 'pending_testing'){
			if($today == 'true'){
				$data['pending_testing'] = $this->crud->pending_testing($id,true);
			}else {
				$data['pending_testing'] = $this->crud->pending_testing($id);
			}
			$data['combine_array'] = array_merge($data['pending_testing']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			$data['current_user_id'] = $id;
//			echo "<pre>";print_r($data['pending_invoice']);exit;
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}
		if($type == 'pending_install'){
			if($today == 'true'){
				$data['pending_install'] = $this->crud->pending_install($id,true);
			}else {
				$data['pending_install'] = $this->crud->pending_install($id);
			}
			$data['combine_array'] = array_merge($data['pending_install']);
			$data['combine_array'] = $this->array_msort($data['combine_array'], array('review_date'=>SORT_DESC));
			$data['current_user_id'] = $id;
			
			$reminder_count = count($data['combine_array']);
			$reminder_html = $this->load->view('shared/reminder_html',$data,true);
			echo json_encode(array('success'=>true,'reminder_html'=>$reminder_html,'reminder_count'=>$reminder_count));
			exit;
		}

		/*if($today == 'true'){
            $today_dispatch = $this->crud->today_sales_order($id,true);
		}else {
			$overdue_dispatch = $this->crud->today_sales_order($id);
		}

		$data['quotation_review'] = $this->crud->get_all_pending_review_quotation($id,$selected_date);
		foreach($data['quotation_review'] as $k => $v)
		{
			$data['quotation_review'][$k]['review_date'] = date("Y-m-d", strtotime($data['quotation_review'][$k]['due_date']));
		}

        $data['sales_order_due'] = $this->crud->get_all_due_sales_order($id,$selected_date);
		foreach($data['sales_order_due'] as $k => $v)
		{
			$data['sales_order_due'][$k]['review_date'] = date("Y-m-d", strtotime($data['sales_order_due'][$k]['committed_date']));
		}

		$data['pending_inquiry'] = $this->crud->get_all_pending_inquiry($id,$selected_date);
		foreach($data['pending_inquiry'] as $k => $v)
		{
			$data['pending_inquiry'][$k]['review_date'] = date("Y-m-d", strtotime($data['pending_inquiry'][$k]['due_date']));
		}*/

	}


	function array_msort($array, $cols)
	{
		$colarr = array();
		foreach ($cols as $col => $order) {
			$colarr[$col] = array();
			foreach ($array as $k => $row) {
				if(isset($row[$col]))
					$colarr[$col]['_'.$k] = strtolower($row[$col]);
			}
		}
		$eval = 'array_multisort(';
		foreach ($cols as $col => $order) {
			$eval .= '$colarr[\''.$col.'\'],'.$order.',';
		}
		$eval = substr($eval,0,-1).');';
		/*pre_die($colarr['review_date']);
		pre_die($eval);*/
		eval($eval);
		$ret = array();
		foreach ($colarr as $col => $arr) {
			foreach ($arr as $k => $v) {
				$k = substr($k,1);
				if (!isset($ret[$k])) $ret[$k] = $array[$k];
				$ret[$k][$col] = $array[$k][$col];
			}
		}
		return $ret;

	}

	public function dashboarddemo()
	{
		$this->load->helper('log4php');
		log_info('Test log message');
		$data = array();
		$logged_in = $this->session->userdata("is_logged_in");
		log_info(print_r($logged_in,true));
		$id = $logged_in['staff_id'];
		$data['agents'] = $this->chat->getAllAgents($id);
		$where_array['reminder_date'] = '<= CURDATE()';
		$data['reminder'] = $this->crud->get_all_pending_reminder($id);

		$data['chart_data'] = $this->crud->get_chart_data();

		if($this->session->userdata('is_logged_in')){
			set_page('dashboarddemo',$data);
		}else{
			$this->load->view('auth/login_form');
		}
	}

	function get_chart_data()
	{
		$data = $this->crud->get_chart_data();
		echo '<pre>';print_r($data);exit;
		//return $data;
		echo json_encode($data);
		exit;
	}

	public function testemit()
	{
		$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));

		$client->initialize();
		$client->emit('new_message', ['name'=>"Naitik",'email'=>"nbbd@gmail.com",'subject' => "Massage form jay khodiayar welcom controller index action", 'created_at' => "NOW()",'id' => 1]);
		$client->emit('new_message_client', ['to_id'=>1,'to_table'=>"admin",'from_id' => 2, 'from_table' => "admin",'msg' => "Massage form jay khodiayar welcom controller index action"]);
		$client->close();
	}
	public function getVisitors() {
		$query = $this->db->query("select count(*) AS total,country from visitors GROUP BY country");
		$results = $query->result_array();
		$json = array();
		foreach($results as $result) {
			$json[$result['country']]['total'] = $result['total'];
			$json[$result['country']]['country'] = $result['country'];
		}
		echo json_encode($json);
		exit;
	}

	public function getReminder() {
		$query = $this->db->query("select reminder_date from reminder where user_id ='".$this->session->userdata('logged_in_as')['staff_id']."'");
		$results = $query->result_array();
		$json = array();
		foreach($results as $result) {
			$json[date('D M d Y H:i:s O', strtotime($result['reminder_date'])). ' (India Standard Time)'] = date('D M d Y H:i:s O', strtotime($result['reminder_date'])). ' (India Standard Time)';
		}
		echo json_encode($json);
		exit;
	}

	public function getReminderByDate() {
		$query = $this->db->query("select * from reminder where reminder_date = '123'");
		$results = $query->result_array();
		$json = array();
		foreach($results as $result) {
			$json[$result['reminder_date']] = $result['reminder_date'];
		}
		echo json_encode($json);
		exit;
	}

	function notify_sales_order_for_approve() {
		$today_sales_order_to_approve = $this->crud->sales_order_to_approve($this->session->userdata('logged_in_as')['staff_id'], true);
		$overdue_sales_order_to_approve = $this->crud->sales_order_to_approve($this->session->userdata('logged_in_as')['staff_id']);
		echo count($today_sales_order_to_approve) + count($overdue_sales_order_to_approve);
		exit;
	}
        
        function get_notification_data(){
            $feedbacks = $this->crud->get_notification_dropdown();
            echo json_encode($feedbacks);
            exit;
        }

}
