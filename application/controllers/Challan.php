<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Challan extends CI_Controller
{
	protected $staff_id = 1;
	function __construct(){
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	function sales_order_itemqtywise_datatable(){
		$select = 'si.id as sales_order_item_id,si.item_name,si.quantity,si.dispatched_qty, s.id,sales.sales,q.quotation_no,q.enquiry_no,s.sales_order_no,s.sales_order_date,s.sales_order_status_id,p.party_name,p.phone_no,p.email_id,';
		$select .= 'p.party_type_1,city.city,state.state,country.country';
		$config['table'] = 'sales_order_items si';
		$config['select'] = $select;
		$config['joins'][] = array('join_table' => 'sales_order s', 'join_by' => 'si.sales_order_id = s.id' ,'join_type'=>'left'    );
		$config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id' ,'join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'sales', 'join_by' => 'sales.id = s.sales_id','join_type'=>'left');
		$config['column_order'] = array('q.enquiry_no','q.quotation_no','s.sales_order_no','p.party_name','si.item_name','p.phone_no','p.email_id','sales.sales','city.city','state.state','country.country');
		$config['column_search'] = array('q.enquiry_no','q.quotation_no','s.sales_order_no','p.party_name','si.item_name','p.phone_no','p.email_id','sales.sales','city.city','state.state','country.country');
		$config['order'] = array('s.id' => 'desc');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($cu_accessExport == 1 && $cu_accessDomestic == 1){
		}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}
		
		$config['wheres'][] = array('column_name' => 'p.active != ','column_value'=>'0');
		$config['wheres'][] = array('column_name' => 's.isApproved ','column_value'=>'1');
		$config['wheres'][] = array('column_name' => 's.is_dispatched != ','column_value'=> CANCELED_ORDER_ID);
		$config['wheres'][] = array('column_name' => 's.is_dispatched != ','column_value'=> CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID);
		$config['custom_where'] = 'si.quantity != si.dispatched_qty';

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		foreach ($list as $sales_order) {
			$quantity = $sales_order->quantity - $sales_order->dispatched_qty;
			for($q_inc=1; $q_inc<=$quantity; $q_inc++){
				$row = array();
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->enquiry_no."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->quotation_no."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->sales_order_no."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->party_name."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->item_name ."</a>";
                $phone_no = substr($sales_order->phone_no,0,10);
                $sales_order->phone_no = str_replace(',', ', ', $sales_order->phone_no);
                $row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."' data-toggle='tooltip' data-placement='bottom' title='".$sales_order->phone_no."'>".$phone_no."</a>";
                $email_id = substr($sales_order->email_id,0,14);
                $sales_order->email_id = str_replace(',', ', ', $sales_order->email_id);
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'data-toggle='tooltip' data-placement='bottom' title='".$sales_order->email_id."'>".$email_id."</a>";
				$party_type = '';
				if($sales_order->party_type_1 == 5){
					$party_type = 'Export';
				}elseif($sales_order->party_type_1 == 6){
					$party_type = 'Domestic';
				}
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$party_type."</a>";				
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->city."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->state."</a>";
				$row[] = "<a href='javascript:void(0);' class='sales_order_row' data-sales_order_id='".$sales_order->id."' data-sales_order_item_id='".$sales_order->sales_order_item_id."'>".$sales_order->country."</a>";
				$data[] = $row;
			}
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//echo $this->db->last_query();
		echo json_encode($output);
	}
	
	function get_sales_order(){
		$sales_order_id = $this->input->get_post("sales_order_id");
		$sales_order_item_id = $this->input->get_post("sales_order_item_id");
		$sales_order_data = $this->crud->get_sales_order($sales_order_id);
		if(!empty($sales_order_data->po_date)){ 
			$sales_order_data->po_date = date('d-m-Y',strtotime($sales_order_data->po_date));
		}
		if(!empty($sales_order_data->sales_order_date)){ 
			$sales_order_data->sales_order_date = date('d-m-Y',strtotime($sales_order_data->sales_order_date));
		}
		$data['sales_order_data'] = $sales_order_data;
		$lineitems = array();
		$where = array('id' => $sales_order_item_id);
		$sales_order_item = $this->crud->get_row_by_id('sales_order_items', $where);
		$data['sales_order_item'] = $sales_order_item[0];
		//        echo '<pre>';print_r($data); exit;
		echo json_encode($data);
		exit;
	}
	
    function export_proforma_invoice_itemqtywise_datatable(){
		$select = 'pii.id as proforma_invoice_item_id,pii.item_name,pii.quantity,pii.dispatched_qty, pi.id,sales.sales,q.quotation_no,q.enquiry_no,pi.proforma_invoice_no,pi.sales_order_date,p.party_name,p.phone_no,p.email_id,';
		$select .= 'p.party_type_1,city.city,state.state,country.country';
		$config['table'] = 'proforma_invoice_items pii';
		$config['select'] = $select;
		$config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pii.proforma_invoice_id = pi.id' ,'join_type'=>'left'    );
		$config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'pi.quotation_id = q.id' ,'join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'party p', 'join_by' => 'p.party_id = pi.sales_to_party_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'sales', 'join_by' => 'sales.id = pi.sales_id','join_type'=>'left');
		$config['column_order'] = array('q.enquiry_no','q.quotation_no','pi.proforma_invoice_no','p.party_name','pii.item_name','p.phone_no','p.email_id','sales.sales','city.city','state.state','country.country');
		$config['column_search'] = array('q.enquiry_no','q.quotation_no','pi.proforma_invoice_no','p.party_name','pii.item_name','p.phone_no','p.email_id','sales.sales','city.city','state.state','country.country');
		$config['order'] = array('pi.id' => 'desc');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		if($cu_accessExport == 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		}
		$config['wheres'][] = array('column_name' => 'p.active != ','column_value'=>'0');
		$config['wheres'][] = array('column_name' => 'pi.isApproved ','column_value'=>'1');
        $config['wheres'][] = array('column_name' => 'pi.is_dispatched != ','column_value'=> CANCELED_ORDER_ID);
        $config['wheres'][] = array('column_name' => 'pi.is_dispatched != ','column_value'=> CHALLAN_CREATED_FROM_SALES_ORDER_ID);

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();

		$data = array();
		foreach ($list as $proforma_invoice) {
			$quantity = $proforma_invoice->quantity - $proforma_invoice->dispatched_qty;
			for($q_inc=1; $q_inc<=$quantity; $q_inc++){
				$row = array();
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->enquiry_no."</a>";
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->quotation_no."</a>";
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->proforma_invoice_no."</a>";
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->party_name."</a>";
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->item_name ."</a>";
                $phone_no = substr($proforma_invoice->phone_no,0,10);
                $proforma_invoice->phone_no = str_replace(',', ', ', $proforma_invoice->phone_no);
                $row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."' data-toggle='tooltip' data-placement='bottom' title='".$proforma_invoice->phone_no."'>".$phone_no."</a>";
                $email_id = substr($proforma_invoice->email_id,0,14);
                $proforma_invoice->email_id = str_replace(',', ', ', $proforma_invoice->email_id);
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'data-toggle='tooltip' data-placement='bottom' title='".$proforma_invoice->email_id."'>".$email_id."</a>";
				$party_type = '';
				if($proforma_invoice->party_type_1 == 5){
					$party_type = 'Export';
				}elseif($proforma_invoice->party_type_1 == 6){
					$party_type = 'Domestic';
				}
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$party_type."</a>";				
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->city."</a>";
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->state."</a>";
				$row[] = "<a href='javascript:void(0);' class='proforma_invoice_row' data-proforma_invoice_id='".$proforma_invoice->id."' data-proforma_invoice_item_id='".$proforma_invoice->proforma_invoice_item_id."'>".$proforma_invoice->country."</a>";
				$data[] = $row;
			}
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//echo $this->db->last_query();
		echo json_encode($output);
	}
    
    function get_proforma_invoice(){
		$proforma_invoice_id = $this->input->get_post("proforma_invoice_id");
		$proforma_invoice_item_id = $this->input->get_post("proforma_invoice_item_id");
		$proforma_invoice_data = $this->crud->get_proforma_invoice($proforma_invoice_id);
		if(!empty($proforma_invoice_data->po_date)){ 
			$proforma_invoice_data->po_date = date('d-m-Y',strtotime($proforma_invoice_data->po_date));
		}
		if(!empty($proforma_invoice_data->proforma_invoice_date)){ 
			$proforma_invoice_data->proforma_invoice_date = date('d-m-Y',strtotime($proforma_invoice_data->proforma_invoice_date));
		}
		$data['proforma_invoice_data'] = $proforma_invoice_data;
		$lineitems = array();
		$where = array('id' => $proforma_invoice_item_id);
		$proforma_invoice_item = $this->crud->get_row_by_id('proforma_invoice_items', $where);
		$data['proforma_invoice_item'] = $proforma_invoice_item[0];
		//        echo '<pre>';print_r($data); exit;
		echo json_encode($data);
		exit;
	}
    
	function add($challan_id = ''){
		$data = array();
		$data['motor_user'] = $this->crud->get_all_records('challan_item_user','user_name','asc');
		$data['motor_make'] = $this->crud->get_all_records('challan_item_make','make_name','asc');
		$data['motor_hp'] = $this->crud->get_all_records('challan_item_hp','hp_name','asc');
		$data['motor_kw'] = $this->crud->get_all_records('challan_item_kw','kw_name','asc');
		$data['motor_frequency'] = $this->crud->get_all_records('challan_item_frequency','frequency_name','asc');
		$data['motor_rpm'] = $this->crud->get_all_records('challan_item_rpm','rpm_name','asc');
		$data['motor_volts_cycles'] = $this->crud->get_all_records('challan_item_volts_cycles','volts_cycles_name','asc');
		$data['gearbox_user'] = $data['motor_user'];
		$data['gearbox_make'] = $data['motor_make'];
		$data['gearbox_gear_type'] = $this->crud->get_all_records('challan_item_gear_type','gear_type_name','asc');
		$data['gearbox_model'] = $this->crud->get_all_records('challan_item_model','model_name','asc');
		$data['gearbox_ratio'] = $this->crud->get_all_records('challan_item_ratio','ratio_name','asc');
		$default_challan_details = $this->crud->get_all_records('default_challan_details','id','asc');
		$default_details_arr = array();
		if(isset($default_challan_details) && !empty($default_challan_details)){
			foreach($default_challan_details as $row){
				$default_details_arr[$row->meta_key] = $row->meta_value;
			}
		}
		$data['default_details_arr'] = $default_details_arr;
		if(!empty($challan_id)){
			if ($this->app_model->have_access_role(CHALLAN_MODULE_ID, "edit")) {
				$challan_data = $this->crud->get_row_by_id('challans', array('id' => $challan_id));
				$challan_data = $challan_data[0];
				$challan_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->created_by));
				$challan_data->created_at = substr($challan_data->created_at, 8, 2) .'-'. substr($challan_data->created_at, 5, 2) .'-'. substr($challan_data->created_at, 0, 4);
				$challan_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->updated_by));
				$challan_data->updated_at = substr($challan_data->updated_at, 8, 2) .'-'. substr($challan_data->updated_at, 5, 2) .'-'. substr($challan_data->updated_at, 0, 4);
				$data['challan_data'] = $challan_data;
                
                if(!empty($challan_data->sales_order_id)){
                    $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $challan_data->sales_order_id));
                    if(!empty($sales_order_data)){
                        $sales_order_data = $sales_order_data[0];
                        $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
                        $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'-'. substr($sales_order_data->po_date, 5, 2) .'-'. substr($sales_order_data->po_date, 0, 4);
                        $sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'-'. substr($sales_order_data->sales_order_date, 5, 2) .'-'. substr($sales_order_data->sales_order_date, 0, 4);
                        $data['sales_order_data'] = $sales_order_data;
                    }
                }
                
                if(!empty($challan_data->proforma_invoice_id)){
                    $proforma_invoice_data = $this->crud->get_row_by_id('proforma_invoices', array('id' => $challan_data->proforma_invoice_id));
                    if(!empty($proforma_invoice_data)){
                        $proforma_invoice_data = $proforma_invoice_data[0];
                        $proforma_invoice_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $proforma_invoice_data->quotation_id);
                        $proforma_invoice_data->po_date = substr($proforma_invoice_data->po_date, 8, 2) .'-'. substr($proforma_invoice_data->po_date, 5, 2) .'-'. substr($proforma_invoice_data->po_date, 0, 4);
                        $proforma_invoice_data->proforma_invoice_date = substr($proforma_invoice_data->sales_order_date, 8, 2) .'-'. substr($proforma_invoice_data->sales_order_date, 5, 2) .'-'. substr($proforma_invoice_data->sales_order_date, 0, 4);
                        unset($proforma_invoice_data->sales_order_date);
                        $data['sales_order_data'] = $proforma_invoice_data;
                    }
                }
                
				$where = array('challan_id' => $challan_id);
				$challan_item_data = $this->crud->get_row_by_id('challan_items', $where);
				$data['challan_item_data'] = $challan_item_data[0];
                $data['party_contact_person'] = $this->crud->get_contact_person_by_party($challan_data->sales_to_party_id);
				$where = array('challan_item_id' => $data['challan_item_data']->id);
				$motor_data = $this->crud->get_row_by_id('challan_items_motor_details', $where);
				$data['motor_data'] = json_encode($motor_data);
				$gearbox_data = $this->crud->get_row_by_id('challan_items_gearbox_details', $where);
				$data['gearbox_data'] = json_encode($gearbox_data);
                
                if(!empty($data['challan_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['challan_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if(!empty($designation)){
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if(!empty($department)){
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
                
				$challan_data->invoice_no = '';
				$challan_data->invoice_date = '';
				$invoice_data = $this->crud->get_row_by_id('invoices', array('challan_id' => $challan_data->id));
                if(!empty($invoice_data)){
                    $challan_data->invoice_no = $invoice_data[0]->invoice_no;
                    $challan_data->invoice_date = $invoice_data[0]->invoice_date;
                }
				//echo '<pre>';print_r($data); exit;
				set_page('dispatch/challan/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(CHALLAN_MODULE_ID,"add")) {
				$data['order_no'] = $this->crud->get_next_autoincrement('purchase_order');
				set_page('dispatch/challan/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}
    
    function check_item_bom_created_or_not($item_id){
        $return = array();
        $bom_data = $this->crud->get_row_by_id('bom_master', array('sales_item_id' => $item_id));
        if(empty($bom_data)){
            $return['status'] = '0';
        } else {
            $return['status'] = '1';
        }
		print json_encode($return);
		exit;
    }
    function save_challan(){
		$post_data = $this->input->post();
//				echo '<pre>';print_r($post_data); exit;
        $send_sms = 0;
        if(isset($post_data['challan_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['challan_data']['send_sms']);
        }
		/*--------- Convert Date as Mysql Format -------------*/
		$post_data['challan_data']['challan_date'] = date("Y-m-d", strtotime($post_data['challan_data']['challan_date']));
		$post_data['challan_data']['delivery_through_id'] = !empty($post_data['challan_data']['delivery_through_id']) ? $post_data['challan_data']['delivery_through_id'] : NULL;
		$post_data['challan_data']['prepared_by'] = !empty($post_data['challan_data']['prepared_by']) ? $post_data['challan_data']['prepared_by'] : NULL;
		$post_data['challan_data']['checked_by'] = !empty($post_data['challan_data']['checked_by']) ? $post_data['challan_data']['checked_by'] : NULL;
		$post_data['challan_data']['notes'] = !empty($post_data['challan_data']['notes']) ? $post_data['challan_data']['notes'] : NULL;
		$post_data['challan_data']['sales_to_party_id'] = !empty($post_data['party']['party_id']) ? $post_data['party']['party_id'] : NULL;
		
		$post_data['challan_data']['sales_order_id'] = !empty($post_data['challan_data']['sales_order_id']) ? $post_data['challan_data']['sales_order_id'] : NULL;
		$post_data['challan_data']['sales_order_item_id'] = !empty($post_data['challan_data']['sales_order_item_id']) ? $post_data['challan_data']['sales_order_item_id'] : NULL;
		$post_data['challan_data']['proforma_invoice_id'] = !empty($post_data['challan_data']['proforma_invoice_id']) ? $post_data['challan_data']['proforma_invoice_id'] : NULL;
		$post_data['challan_data']['proforma_invoice_item_id'] = !empty($post_data['challan_data']['proforma_invoice_item_id']) ? $post_data['challan_data']['proforma_invoice_item_id'] : NULL;
		
        if(isset($_POST['challan_data']['challan_id']) && !empty($_POST['challan_data']['challan_id'])){
			$challan_id = $_POST['challan_data']['challan_id'];
			if (isset($post_data['challan_data']['challan_id'])){
				unset($post_data['challan_data']['challan_id']);
			}
			$challan_no = $post_data['challan_data']['challan_no'];
			$challan_result = $this->crud->get_id_by_val('challans', 'id', 'challan_no', $challan_no);
			if(!empty($challan_result) && $challan_result != $challan_id){
				echo json_encode(array("success" => 'false', 'msg' => 'Challan No. Already Exist!'));
				exit;
			}

			$post_data['challan_data']['updated_at'] = $this->now_time;
			$post_data['challan_data']['updated_by'] = $this->staff_id;
			$this->db->where('id', $challan_id);
			$result = $this->db->update('challans', $post_data['challan_data']);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Challan Updated Successfully');
				$challan_item_id = $post_data['challan_item_data']['challan_item_id'];
				$add_lineitem = array();
				$add_lineitem['challan_id'] = $challan_id;
				$add_lineitem['item_id'] = $post_data['challan_item_data']['item_id'];
				$add_lineitem['item_name'] = $post_data['challan_item_data']['item_name'];
				$add_lineitem['item_code'] = $post_data['challan_item_data']['item_code'];
				$add_lineitem['item_description'] = $post_data['challan_item_data']['item_description'];
				$add_lineitem['contact_person'] = $post_data['challan_item_data']['contact_person'];
				$add_lineitem['contact_no'] = $post_data['challan_item_data']['contact_no'];
				$add_lineitem['driver_name'] = $post_data['challan_item_data']['driver_name'];
				$add_lineitem['driver_contact_no'] = $post_data['challan_item_data']['driver_contact_no'];
				$add_lineitem['quantity'] = $post_data['challan_item_data']['quantity'];
				if(isset($post_data['challan_item_data']['item_extra_accessories_id']) && !empty($post_data['challan_item_data']['item_extra_accessories_id'])){
					$add_lineitem['item_extra_accessories_id'] = $post_data['challan_item_data']['item_extra_accessories_id'];
				}
				$add_lineitem['item_serial_no'] = $post_data['challan_item_data']['item_serial_no'];
				$add_lineitem['truck_container_no'] = $post_data['challan_item_data']['truck_container_no'];
				$add_lineitem['lr_bl_no'] = $post_data['challan_item_data']['lr_bl_no'];
				$add_lineitem['lr_date'] = date("Y-m-d", strtotime($post_data['challan_item_data']['lr_date']));
				$add_lineitem['packing_mark'] = $post_data['challan_item_data']['packing_mark'];
				$add_lineitem['no_of_packing'] = $post_data['challan_item_data']['no_of_packing'];
				$add_lineitem['no_of_unit'] = $post_data['challan_item_data']['no_of_unit'];
				$add_lineitem['updated_at'] = $this->now_time;
				$add_lineitem['created_at'] = $this->now_time;
				$this->db->where('id', $challan_item_id);
				$result = $this->db->update('challan_items', $add_lineitem);

				$val_inc = 0;
				if(!empty($post_data['item_details']['motor_user'])){
                    
                    if(isset($post_data['deleted_challan_motor_id'])){
                        $this->db->where_in('id', $post_data['deleted_challan_motor_id']);
                        $this->db->delete('challan_items_motor_details');
                    }
					foreach($post_data['item_details']['motor_user'] as $motor_user){
                        $challan_items_motor_details = array();
                        if(!empty($post_data['item_details']['challan_motor_id'][$val_inc])){
                            $challan_items_motor_details['challan_item_id'] = $challan_item_id;
                            $challan_items_motor_details['motor_user'] = $motor_user;
                            $challan_items_motor_details['motor_make'] = $post_data['item_details']['motor_make'][$val_inc];
                            $challan_items_motor_details['motor_hp'] = $post_data['item_details']['motor_hp'][$val_inc];
                            $challan_items_motor_details['motor_kw'] = $post_data['item_details']['motor_kw'][$val_inc];
                            $challan_items_motor_details['motor_frequency'] = $post_data['item_details']['motor_frequency'][$val_inc];
                            $challan_items_motor_details['motor_rpm'] = $post_data['item_details']['motor_frequency'][$val_inc];
                            $challan_items_motor_details['motor_volts_cycles'] = $post_data['item_details']['motor_volts_cycles'][$val_inc];
                            $challan_items_motor_details['motor_serial_no'] = $post_data['item_details']['motor_serial_no'][$val_inc];
                            $this->db->where('id', $post_data['item_details']['challan_motor_id'][$val_inc]);
                            $this->db->update('challan_items_motor_details', $challan_items_motor_details);
                        } else {
                            $challan_items_motor_details['challan_item_id'] = $challan_item_id;
                            $challan_items_motor_details['motor_user'] = $motor_user;
                            $challan_items_motor_details['motor_make'] = $post_data['item_details']['motor_make'][$val_inc];
                            $challan_items_motor_details['motor_hp'] = $post_data['item_details']['motor_hp'][$val_inc];
                            $challan_items_motor_details['motor_kw'] = $post_data['item_details']['motor_kw'][$val_inc];
                            $challan_items_motor_details['motor_frequency'] = $post_data['item_details']['motor_frequency'][$val_inc];
                            $challan_items_motor_details['motor_rpm'] = $post_data['item_details']['motor_frequency'][$val_inc];
                            $challan_items_motor_details['motor_volts_cycles'] = $post_data['item_details']['motor_volts_cycles'][$val_inc];
                            $challan_items_motor_details['motor_serial_no'] = $post_data['item_details']['motor_serial_no'][$val_inc];
                            $this->db->insert('challan_items_motor_details', $challan_items_motor_details);
                        }
						$val_inc++;
					}
				}
				$val_inc = 0;
				if(!empty($post_data['item_details']['gearbox_user'])){
                    
                    if(isset($post_data['deleted_challan_gearbox_id'])){
                        $this->db->where_in('id', $post_data['deleted_challan_gearbox_id']);
                        $this->db->delete('challan_items_gearbox_details');
                    }
                    if(isset($post_data['challan_gearbox_empty'])){
                    } else {
                        foreach($post_data['item_details']['gearbox_user'] as $gearbox_user){
                            $challan_items_gearbox_details = array();
                            if(!empty($post_data['item_details']['challan_gearbox_id'][$val_inc])){
                                $challan_items_gearbox_details['challan_item_id'] = $challan_item_id;
                                $challan_items_gearbox_details['gearbox_user'] = $gearbox_user;
                                $challan_items_gearbox_details['gearbox_make'] = $post_data['item_details']['gearbox_make'][$val_inc];
                                $challan_items_gearbox_details['gearbox_gear_type'] = $post_data['item_details']['gearbox_gear_type'][$val_inc];
                                $challan_items_gearbox_details['gearbox_model'] = $post_data['item_details']['gearbox_model'][$val_inc];
                                $challan_items_gearbox_details['gearbox_ratio'] = $post_data['item_details']['gearbox_ratio'][$val_inc];
                                $challan_items_gearbox_details['gearbox_serial_no'] = $post_data['item_details']['gearbox_serial_no'][$val_inc];
                                $this->db->where('id', $post_data['item_details']['challan_gearbox_id'][$val_inc]);
                                $this->db->update('challan_items_gearbox_details', $challan_items_gearbox_details);
                            } else {
                                $challan_items_gearbox_details['challan_item_id'] = $challan_item_id;
                                $challan_items_gearbox_details['gearbox_user'] = $gearbox_user;
                                $challan_items_gearbox_details['gearbox_make'] = $post_data['item_details']['gearbox_make'][$val_inc];
                                $challan_items_gearbox_details['gearbox_gear_type'] = $post_data['item_details']['gearbox_gear_type'][$val_inc];
                                $challan_items_gearbox_details['gearbox_model'] = $post_data['item_details']['gearbox_model'][$val_inc];
                                $challan_items_gearbox_details['gearbox_ratio'] = $post_data['item_details']['gearbox_ratio'][$val_inc];
                                $challan_items_gearbox_details['gearbox_serial_no'] = $post_data['item_details']['gearbox_serial_no'][$val_inc];
                                $this->db->insert('challan_items_gearbox_details', $challan_items_gearbox_details);
                            }
                            $val_inc++;
                        }
                    }
				}
			}
		} else {

            $post_data['challan_data']['challan_no'] = $this->crud->get_next_autoincrement('challans');
			$post_data['challan_data']['challan_no'] = $this->applib->get_challan_no($post_data['challan_data']['challan_no'], $post_data['challan_data']['challan_date']);
			$dataToInsert = $post_data['challan_data'];
//            $dataToInsert['challan_no'] = $this->crud->get_next_autoincrement('challans');
            $dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
			$result = $this->db->insert('challans', $dataToInsert);
			$challan_id = $this->db->insert_id();
			if($result){
				
				/*--- Start Update Sales Order to Dispatched -----------*/
                if(!empty($post_data['challan_data']['sales_order_id'])){
                    $this->crud->update('sales_order', array("delete_not_allow" => 1), array('id' => $post_data['challan_data']['sales_order_id']));
                    $sales_order_item_data = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $post_data['challan_data']['sales_order_id'], 'item_id' => $post_data['challan_item_data']['item_id']));
//                    echo '<pre>'; print_r($sales_order_item_data); exit;
                    foreach ($sales_order_item_data as $sales_order_item){
                        if($sales_order_item->quantity == $sales_order_item->dispatched_qty){
                            continue;
                        }
                        if($sales_order_item->quantity >= $sales_order_item->pro_started_qty){
                            if($sales_order_item->pro_started_qty >= $sales_order_item->pro_ended_qty){
                                $production_data = $this->crud->getFromSQL("SELECT * FROM `production` WHERE `sales_order_id` = '".$post_data['challan_data']['sales_order_id']."' AND `item_id` = '".$post_data['challan_item_data']['item_id']."'");
//                                echo '<pre>'; print_r($production_data); exit;
                                if(!empty($production_data)){
                                    $production_data = $production_data[0];
                                    if($production_data->pro_status == '1'){
                                        $this->applib->update_purchase_stock_on_production_finish_by_sales_item_id($production_data->production_id, '-');
                                        $this->crud->update('production', array('pro_status' => '2', 'finished_at' => date('Y-m-d H:i:s'), 'finished_by' => $this->staff_id), array('production_id' => $production_data->production_id));
                                        $pro_ended_qty = $sales_order_item->pro_ended_qty + 1;
                                        $this->crud->update('sales_order_items', array('pro_ended_qty' => $pro_ended_qty), array('id' => $sales_order_item->id));
                                    }
                                    /* Update Purchase Item FIFO in Challan add And Production already started */
                                    $this->applib->purchase_item_fifo_from_add_challan_and_production_already_started($production_data->production_id, $challan_id);
                                }
                            } else {
                                $add_production = array();
                                $add_production['sales_order_id'] = $post_data['challan_data']['sales_order_id'];
                                $add_production['item_id'] = $post_data['challan_item_data']['item_id'];
                                $add_production['pro_status'] = '2';
                                $add_production['created_at'] = $this->now_time;
                                $add_production['created_by'] = $this->staff_id;
                                $add_production['finished_at'] = $this->now_time;
                                $add_production['finished_by'] = $this->staff_id;
                                $this->crud->insert('production', $add_production);
                                $inserted_production_id = $this->db->insert_id();
                                $this->applib->update_purchase_stock_on_direct_challan_by_sales_item_id($inserted_production_id, $challan_id, $post_data['challan_item_data']['item_id'], '-');
                                $pro_started_qty = $sales_order_item->pro_started_qty + 1;
                                $pro_ended_qty = $sales_order_item->pro_ended_qty + 1;
                                $this->crud->update('sales_order_items', array('pro_started_qty' => $pro_started_qty, 'pro_ended_qty' => $pro_ended_qty), array('id' => $sales_order_item->id));
                            }
                        }
                        $sales_order_dispatched_qty = $sales_order_item->dispatched_qty + 1;
                        $this->crud->update('sales_order_items', array("dispatched_qty" => $sales_order_dispatched_qty), array('id' => $sales_order_item->id));
                        break;
                    }
                    $is_dispatched_arr = array();
                    $sales_order_item_data = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $post_data['challan_data']['sales_order_id']));
                    foreach($sales_order_item_data as $sales_order_item){
                        if($sales_order_item->quantity == $sales_order_item->dispatched_qty){
                            $is_dispatched_arr[] = 1;
                        }
                    }
                    $is_dispatched_count = count($is_dispatched_arr);
                    $line_items_count = count($sales_order_item_data);
                    if($is_dispatched_count == $line_items_count){
						$this->crud->update('sales_order', array("is_dispatched" => DISPATCHED_ORDER_ID), array('id' => $post_data['challan_data']['sales_order_id']));
                    } else {
						$this->crud->update('sales_order', array("is_dispatched" => PENDING_DISPATCH_ORDER_ID), array('id' => $post_data['challan_data']['sales_order_id']));
                    }
                }
				/*--- End Update Sales Order to Dispatched -----------*/
                
                /*--- Start Update Proforma Invoice to Dispatched -----------*/
                if(!empty($post_data['challan_data']['proforma_invoice_id'])){
                    $this->crud->update('proforma_invoices', array("delete_not_allow" => 1), array('id' => $post_data['challan_data']['proforma_invoice_id']));
                    $proforma_invoice_item_data = $this->crud->get_row_by_id('proforma_invoice_items', array('proforma_invoice_id' => $post_data['challan_data']['proforma_invoice_id'], 'item_id' => $post_data['challan_item_data']['item_id']));
                    foreach ($proforma_invoice_item_data as $proforma_invoice_item){
                        if($proforma_invoice_item->quantity == $proforma_invoice_item->dispatched_qty){
                            continue;
                        }
                        if($proforma_invoice_item->quantity >= $proforma_invoice_item->pro_started_qty){
                            if($proforma_invoice_item->pro_started_qty >= $proforma_invoice_item->pro_ended_qty){
                                $production_data = $this->crud->getFromSQL("SELECT * FROM `production` WHERE `proforma_invoice_id` = '".$post_data['challan_data']['proforma_invoice_id']."' AND `item_id` = '".$post_data['challan_item_data']['item_id']."'");
                                if(!empty($production_data)){
                                    $production_data = $production_data[0];
                                    if($production_data->pro_status == '1'){
                                        $this->applib->update_purchase_stock_on_production_finish_by_sales_item_id($production_data->production_id, '-');
                                        $this->crud->update('production', array('pro_status' => '2', 'finished_at' => date('Y-m-d H:i:s'), 'finished_by' => $this->staff_id), array('production_id' => $production_data->production_id));
                                        $pro_ended_qty = $proforma_invoice_item->pro_ended_qty + 1;
                                        $this->crud->update('proforma_invoice_items', array('pro_ended_qty' => $pro_ended_qty), array('id' => $proforma_invoice_item->id));
                                    }
                                    /* Update Purchase Item FIFO in Challan add And Production already started */
                                    $this->applib->purchase_item_fifo_from_add_challan_and_production_already_started($production_data->production_id, $challan_id);
                                }
                            } else {
                                $add_production = array();
                                $add_production['proforma_invoice_id'] = $post_data['challan_data']['proforma_invoice_id'];
                                $add_production['item_id'] = $post_data['challan_item_data']['item_id'];
                                $add_production['pro_status'] = '2';
                                $add_production['created_at'] = $this->now_time;
                                $add_production['created_by'] = $this->staff_id;
                                $add_production['finished_at'] = $this->now_time;
                                $add_production['finished_by'] = $this->staff_id;
                                $this->crud->insert('production', $add_production);
                                $inserted_production_id = $this->db->insert_id();
                                $this->applib->update_purchase_stock_on_direct_challan_by_sales_item_id($inserted_production_id, $challan_id, $post_data['challan_item_data']['item_id'], '-');
                                $pro_started_qty = $proforma_invoice_item->pro_started_qty + 1;
                                $pro_ended_qty = $proforma_invoice_item->pro_ended_qty + 1;
                                $this->crud->update('proforma_invoice_items', array('pro_started_qty' => $pro_started_qty, 'pro_ended_qty' => $pro_ended_qty), array('id' => $proforma_invoice_item->id));
                            }
                        }
                        $proforma_invoice_dispatched_qty = $proforma_invoice_item->dispatched_qty + 1;
                        $this->crud->update('proforma_invoice_items', array("dispatched_qty" => $proforma_invoice_dispatched_qty), array('id' => $proforma_invoice_item->id));
                        break;
                    }
                    $is_dispatched_arr = array();
                    $proforma_invoice_item_data = $this->crud->get_row_by_id('proforma_invoice_items', array('proforma_invoice_id' => $post_data['challan_data']['proforma_invoice_id']));
                    foreach($proforma_invoice_item_data as $proforma_invoice_item){
                        if($proforma_invoice_item->quantity == $proforma_invoice_item->dispatched_qty){
                            $is_dispatched_arr[] = 1;
                        }
                    }
                    $is_dispatched_count = count($is_dispatched_arr);
                    $line_items_count = count($proforma_invoice_item_data);
                    if($is_dispatched_count == $line_items_count){
						$this->crud->update('proforma_invoices', array("is_dispatched" => DISPATCHED_ORDER_ID), array('id' => $post_data['challan_data']['proforma_invoice_id']));
                    } else {
						$this->crud->update('proforma_invoices', array("is_dispatched" => PENDING_DISPATCH_ORDER_ID), array('id' => $post_data['challan_data']['proforma_invoice_id']));
                    }
                    $this->crud->update('quotations', array('quotation_status_id' => QUOTATION_STATUS_ID_ON_ORDER_CREATE), array('quotation_no' => $post_data['quotation_data']['quotation_no']));
                }
				/*--- End Update Proforma Invoice to Dispatched -----------*/

				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Challan Added Successfully');
				$add_lineitem = array();
				$add_lineitem['challan_id'] = $challan_id;
				$add_lineitem['item_id'] = $post_data['challan_item_data']['item_id'];
				$add_lineitem['item_name'] = $post_data['challan_item_data']['item_name'];
				$add_lineitem['item_code'] = $post_data['challan_item_data']['item_code'];
				$add_lineitem['item_description'] = $post_data['challan_item_data']['item_description'];
				$add_lineitem['contact_person'] = $post_data['challan_item_data']['contact_person'];
				$add_lineitem['contact_no'] = $post_data['challan_item_data']['contact_no'];
				$add_lineitem['driver_name'] = $post_data['challan_item_data']['driver_name'];
				$add_lineitem['driver_contact_no'] = $post_data['challan_item_data']['driver_contact_no'];
				$add_lineitem['quantity'] = $post_data['challan_item_data']['quantity'];
				if(isset($post_data['challan_item_data']['item_extra_accessories_id']) && !empty($post_data['challan_item_data']['item_extra_accessories_id'])){
					$add_lineitem['item_extra_accessories_id'] = $post_data['challan_item_data']['item_extra_accessories_id'];
				}
				$add_lineitem['item_serial_no'] = $post_data['challan_item_data']['item_serial_no'];
				$add_lineitem['truck_container_no'] = $post_data['challan_item_data']['truck_container_no'];
				$add_lineitem['lr_bl_no'] = $post_data['challan_item_data']['lr_bl_no'];
				$add_lineitem['lr_date'] = date("Y-m-d", strtotime($post_data['challan_item_data']['lr_date']));
				$add_lineitem['packing_mark'] = $post_data['challan_item_data']['packing_mark'];
				$add_lineitem['no_of_packing'] = $post_data['challan_item_data']['no_of_packing'];
				$add_lineitem['no_of_unit'] = $post_data['challan_item_data']['no_of_unit'];
				$add_lineitem['updated_at'] = $this->now_time;
				$add_lineitem['created_at'] = $this->now_time;
				$this->crud->insert('challan_items',$add_lineitem);
				$challan_item_id = $this->db->insert_id();
                
                $val_inc = 0;
				if(!empty($post_data['item_details']['motor_user'])){
					foreach($post_data['item_details']['motor_user'] as $motor_user){
						$challan_items_motor_details = array();
						$challan_items_motor_details['challan_item_id'] = $challan_item_id;
						$challan_items_motor_details['motor_user'] = $motor_user;
						$challan_items_motor_details['motor_make'] = $post_data['item_details']['motor_make'][$val_inc];
						$challan_items_motor_details['motor_hp'] = $post_data['item_details']['motor_hp'][$val_inc];
						$challan_items_motor_details['motor_kw'] = $post_data['item_details']['motor_kw'][$val_inc];
						$challan_items_motor_details['motor_frequency'] = $post_data['item_details']['motor_frequency'][$val_inc];
						$challan_items_motor_details['motor_rpm'] = $post_data['item_details']['motor_frequency'][$val_inc];
						$challan_items_motor_details['motor_volts_cycles'] = $post_data['item_details']['motor_volts_cycles'][$val_inc];
						$challan_items_motor_details['motor_serial_no'] = $post_data['item_details']['motor_serial_no'][$val_inc];
						$this->db->insert('challan_items_motor_details', $challan_items_motor_details);
						$val_inc++;
					}
				}
				$val_inc = 0;
                if(isset($post_data['challan_gearbox_empty'])){
                } else {
                    if(!empty($post_data['item_details']['gearbox_user'])){
                        foreach($post_data['item_details']['gearbox_user'] as $gearbox_user){
                            $challan_items_gearbox_details = array();
                            $challan_items_gearbox_details['challan_item_id'] = $challan_item_id;
                            $challan_items_gearbox_details['gearbox_user'] = $gearbox_user;
                            $challan_items_gearbox_details['gearbox_make'] = $post_data['item_details']['gearbox_make'][$val_inc];
                            $challan_items_gearbox_details['gearbox_gear_type'] = $post_data['item_details']['gearbox_gear_type'][$val_inc];
                            $challan_items_gearbox_details['gearbox_model'] = $post_data['item_details']['gearbox_model'][$val_inc];
                            $challan_items_gearbox_details['gearbox_ratio'] = $post_data['item_details']['gearbox_ratio'][$val_inc];
                            $challan_items_gearbox_details['gearbox_serial_no'] = $post_data['item_details']['gearbox_serial_no'][$val_inc];
                            $this->db->insert('challan_items_gearbox_details', $challan_items_gearbox_details);
                            $val_inc++;
                        }
                    }
                }
			}
		}
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['party']['party_id']);
            $this->applib->send_sms('challans', $challan_id, $party_phone_no, SEND_DISPATCH_CHALLAN_AND_INVOICE_SMS_ID);
        }
		print json_encode($return);
		exit;
	}

	function challan_list($staff_id='',$from_date='',$to_date=''){
            $data = array();
		if ($this->app_model->have_access_role(CHALLAN_MODULE_ID, "view")) {
                    $users = $lead_owner = $this->crud->get_staff_dropdown();
                    $data['users'] = $users;
                    $data['staff_id']=$staff_id;
                    $data['from_date']=$from_date;
                    $data['to_date']=$to_date;
//                    echo '<pre>'; print_r($data); exit;
                    set_page('dispatch/challan/challan_list', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function challan_datatable(){
		$select = 'c.id,c.challan_no,c.sales_order_id,c.proforma_invoice_id,c.is_invoice_created,c.is_testing_report_created,ci.item_code,c.challan_date,';
		$select .= 'so.sales_order_no,pi.proforma_invoice_no,qso.quotation_no AS qso_quotation_no,qso.enquiry_no AS qso_enquiry_no,qpi.quotation_no AS qpi_quotation_no,qpi.enquiry_no AS qpi_enquiry_no,';
		$select .= 'inv.invoice_no,';
		$select .= 'p.party_name,p.phone_no,p.email_id,';
		$select .= 'city.city,state.state,country.country,sf.name AS sales_person,cur_s.name AS party_current_person';
		$config['table'] = 'challans c';
		$config['select'] = $select;
		$config['joins'][] =  array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'party p', 'join_by' => 'p.party_id = c.sales_to_party_id','join_type'=>'left');
		
		$config['joins'][] =  array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id','join_type'=>'left');		
		$config['joins'][] =  array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id','join_type'=>'left');
		
		$config['joins'][] =  array('join_table' => 'invoices inv', 'join_by' => 'inv.challan_id = c.id','join_type'=>'left');
		
		$config['joins'][] =  array('join_table' => 'quotations qso', 'join_by' => 'qso.id = so.quotation_id','join_type'=>'left');
		$config['joins'][] =  array('join_table' => 'quotations qpi', 'join_by' => 'qpi.id = pi.quotation_id','join_type'=>'left');
		
		$config['joins'][]  = array('join_table'=>'city','join_by'=>'city.city_id = p.city_id','join_type'=>'left');
		$config['joins'][]  = array('join_table'=>'state','join_by'=>'state.state_id = p.state_id','join_type'=>'left');
		$config['joins'][]  = array('join_table'=>'country','join_by'=>'country.country_id = p.country_id','join_type'=>'left');
        $config['joins'][] = array('join_table' => 'staff sf', 'join_by' => 'sf.staff_id = c.created_by', 'join_type' => 'left');
        $config['joins'][] = array('join_table'=> 'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');

		$config['column_order'] = array(null,'inv.invoice_no','c.challan_no',null,null,null,'ci.item_code','p.party_name','p.phone_no','p.email_id','city.city','state.state','country.country');
		$config['column_search'] = array('c.challan_no','qso.enquiry_no','qso.quotation_no','qpi.enquiry_no','qpi.quotation_no','inv.invoice_no','so.sales_order_no','pi.proforma_invoice_no','ci.item_code','p.phone_no','p.party_name','p.email_id','city.city','state.state','country.country');
		$config['order'] = array('c.id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active','column_value'=>'1');

		if(!empty($_POST['city_id']) && $_POST['city_id'] != 'null') {
			$config['wheres'][] = array('column_name' => 'p.city_id', 'column_value' => trim($_POST['city_id']));
		}

		if(!empty($_POST['state_id']) && $_POST['state_id'] != 'null') {
			$config['wheres'][] = array('column_name' => 'p.state_id', 'column_value' => trim($_POST['state_id']));
		}

		if(!empty($_POST['country_id']) && $_POST['country_id'] != 'null') {
			$config['wheres'][] = array('column_name' => 'p.country_id', 'column_value' => trim($_POST['country_id']));
		}
		
		if (!empty($_POST['party_type']) && $_POST['party_type'] == 'all') {
		} else if (isset($_POST['party_type']) && $_POST['party_type'] == 'export') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if (isset($_POST['party_type']) && $_POST['party_type'] == 'domestic') {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($cu_accessExport == 1 && $cu_accessDomestic == 1){
		}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);	
		}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}
		if (isset($_POST['from_date']) && !empty($_POST['from_date'])) {
            $from_date = date('Y-m-d', strtotime($_POST['from_date']));
            $config['wheres'][] = array('column_name' => 'c.challan_date >=', 'column_value' => $from_date);
        }
        if (isset($_POST['to_date']) && !empty($_POST['to_date'])) {
            $to_date = date('Y-m-d', strtotime($_POST['to_date']));
            $config['wheres'][] = array('column_name' => 'c.challan_date <=', 'column_value' => $to_date);
        }
        if (isset($_POST['user_id']) && $_POST['user_id'] != 'all') {
            $config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' => $_POST['user_id']);
        }
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
//                echo '<pre>'. $this->db->last_query(); exit;
		$data = array();
		$role_delete = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "edit");
		foreach ($list as $challan) {
			$row = array();
			$action = '';
			if($role_edit) {
				$action .= '<a href="'.base_url('challan/add/'.$challan->id).'" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			}
			if($role_delete) {
                if ($challan->is_invoice_created != 1 && $challan->is_testing_report_created != 1) {
                    $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'.base_url('challan/delete_challan/'.$challan->id).'"><i class="fa fa-trash"></i></a>';
                }
			}
			$action .= ' | <a href="'.base_url('challan/challan_print/'.$challan->id).'" target="_blank" class="btn-primary btn-xs" title="Print Challan"><i class="fa fa-print"></i> Challan</a>';
			$row[] = $action;

			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->invoice_no.'</a>';
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->challan_no.'</a>';
            if(!empty($challan->sales_order_no)){
                $row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->sales_order_no.'</a>';
                $row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->qso_quotation_no.'</a>';
                $row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->qso_enquiry_no.'</a>';
            } else{
                $row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->proforma_invoice_no.'</a>';
				$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->qpi_quotation_no.'</a>';
                $row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->qpi_enquiry_no.'</a>';
            }
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->item_code.'</a>';
            $row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->party_name.'</a>';
            $phone_no = substr($challan->phone_no,0,10);
            $challan->phone_no = str_replace(',', ', ', $challan->phone_no);
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$challan->phone_no.'">'.$phone_no.'</a>';
            $email_id = substr($challan->email_id,0,14);
            $challan->email_id = str_replace(',', ', ', $challan->email_id);
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$challan->email_id.'">'.$email_id.'</a>';
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->sales_person.'</a>';
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.date('d-m-Y', strtotime($challan->challan_date)).'</a>';
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->city.'</a>';
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->state.'</a>';
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->country.'</a>';
			$row[] = '<a href="' . base_url('challan/add/' . $challan->id . '?view') . '" >'.$challan->party_current_person.'</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function delete_challan($challan_id){
		$role_delete = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "delete");
		if ($role_delete) {

			$challan_data = $this->crud->get_row_by_id('challans', array('id' => $challan_id));
			$challan_data = $challan_data[0];
			$challan_item_data = $this->crud->get_row_by_id('challan_items', array('challan_id' => $challan_id));
			$challan_item_data = $challan_item_data[0];

			/*--- Start Update Sales Order to Pending Dispatched -----------*/
            if(!empty($challan_data->sales_order_id)){
                $sales_order_item_data = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $challan_data->sales_order_id, 'item_id' => $challan_item_data->item_id));
                foreach ($sales_order_item_data as $sales_order_item){
                    if($sales_order_item->dispatched_qty == 0){
                        continue;
                    }
                    $sales_order_dispatched_qty = $sales_order_item->dispatched_qty - 1;
                    $this->crud->update('sales_order_items', array("dispatched_qty" => $sales_order_dispatched_qty), array('id' => $sales_order_item->id));
                    break;
                }
                
                $is_dispatched_arr = array();
                $delete_not_allow_arr = array();
                $sales_order_item_data = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $challan_data->sales_order_id));
                foreach($sales_order_item_data as $sales_order_item){
                    if($sales_order_item->quantity == $sales_order_item->dispatched_qty){
                        $is_dispatched_arr[] = 1;
                    }
                    if($sales_order_item->dispatched_qty != 0){
                        $delete_not_allow_arr[] = 1;
                    }
                }
                $is_dispatched_count = count($is_dispatched_arr);
                $delete_not_allow_count = count($delete_not_allow_arr);
                $line_items_count = count($sales_order_item_data);
                if($is_dispatched_count == $line_items_count){
					$this->crud->update('sales_order', array("is_dispatched" => DISPATCHED_ORDER_ID), array('id' => $challan_data->sales_order_id));
                } else {
					$this->crud->update('sales_order', array("is_dispatched" => PENDING_DISPATCH_ORDER_ID), array('id' => $challan_data->sales_order_id));
                }
                if(!empty($delete_not_allow_count)){
                    $this->crud->update('sales_order', array("delete_not_allow" => 1), array('id' => $challan_data->sales_order_id));
                } else {
                    $this->crud->update('sales_order', array("delete_not_allow" => 0), array('id' => $challan_data->sales_order_id));
                }
            }
			/*--- End Update Sales Order to Pending Dispatched -----------*/
            
            /*--- Start Update Proforma Invoice to Pending Dispatched -----------*/
            if(!empty($challan_data->proforma_invoice_id)){
                $proforma_invoice_item_data = $this->crud->get_row_by_id('proforma_invoice_items', array('proforma_invoice_id' => $challan_data->proforma_invoice_id, 'item_id' => $challan_item_data->item_id));
                foreach ($proforma_invoice_item_data as $proforma_invoice_item){
                    if($proforma_invoice_item->dispatched_qty == 0){
                        continue;
                    }
                    $proforma_invoice_dispatched_qty = $proforma_invoice_item->dispatched_qty - 1;
                    $this->crud->update('proforma_invoice_items', array("dispatched_qty" => $proforma_invoice_dispatched_qty), array('id' => $proforma_invoice_item->id));
                    break;
                }

                $is_dispatched_arr = array();
                $delete_not_allow_arr = array();
                $proforma_invoice_item_data = $this->crud->get_row_by_id('proforma_invoice_items', array('proforma_invoice_id' => $challan_data->proforma_invoice_id));
                foreach($proforma_invoice_item_data as $proforma_invoice_item){
                    if($proforma_invoice_item->quantity == $proforma_invoice_item->dispatched_qty){
                        $is_dispatched_arr[] = 1;
                    }
                    if($proforma_invoice_item->dispatched_qty != 0){
                        $delete_not_allow_arr[] = 1;
                    }
                }
                $is_dispatched_count = count($is_dispatched_arr);
                $delete_not_allow_count = count($delete_not_allow_arr);
                $line_items_count = count($proforma_invoice_item_data);
                if($is_dispatched_count == $line_items_count){
					$this->crud->update('proforma_invoices', array("is_dispatched" => DISPATCHED_ORDER_ID), array('id' => $challan_data->proforma_invoice_id));
                } else {
					$this->crud->update('proforma_invoices', array("is_dispatched" => PENDING_DISPATCH_ORDER_ID), array('id' => $challan_data->proforma_invoice_id));
                }
                if(!empty($delete_not_allow_count)){
                    $this->crud->update('proforma_invoices', array("delete_not_allow" => 1), array('id' => $challan_data->proforma_invoice_id));
                } else {
                    $this->crud->update('proforma_invoices', array("delete_not_allow" => 0), array('id' => $challan_data->proforma_invoice_id));
                }
                $quotation_id = $this->crud->get_id_by_val('proforma_invoices', 'quotation_id', 'id', $challan_data->proforma_invoice_id);
                $this->crud->update('quotations', array('quotation_status_id' => QUOTATION_DEFAULT_STATUS_ID), array('id' => $quotation_id));
            }
			/*--- End Update Proforma Invoice to Pending Dispatched -----------*/
            
            /* Update Purchase Item FIFO in Challan delete */
            $this->applib->purchase_item_fifo_from_delete_challan($challan_id);
            
            $where_array = array("id" => $challan_id);
			$this->crud->delete("challans", $where_array);

			$where_array = array("challan_id" => $challan_id);
			$this->crud->delete("challan_items", $where_array);
			$where_array = array("challan_item_id" => $challan_item_data->id);
			$this->crud->delete("challan_items_motor_details", $where_array);
			$this->crud->delete("challan_items_gearbox_details", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
        exit;
	}

	function challan_print($id){
		$challan_data = $this->get_challan_detail($id);
		$company_details = $this->applib->get_company_detail();
		$challan_items = $this->get_challan_items($id);
		$this->load->library('m_pdf');
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;

        if(!empty($challan_data->sales_order_id)){
            $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $challan_data->sales_order_id));
            $sales_order_data = $sales_order_data[0];
            $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = $this->crud->get_id_by_val('quotations', 'quotation_date', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = substr($sales_order_data->quotation_date, 8, 2) .'/'. substr($sales_order_data->quotation_date, 5, 2) .'/'. substr($sales_order_data->quotation_date, 0, 4);
            $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'/'. substr($sales_order_data->po_date, 5, 2) .'/'. substr($sales_order_data->po_date, 0, 4);
            $sales_order_data->sales_order_date = substr($sales_order_data->sales_order_date, 8, 2) .'/'. substr($sales_order_data->sales_order_date, 5, 2) .'/'. substr($sales_order_data->sales_order_date, 0, 4);
        }

        if(!empty($challan_data->proforma_invoice_id)){
            $proforma_invoice_data = $this->crud->get_row_by_id('proforma_invoices', array('id' => $challan_data->proforma_invoice_id));
            $sales_order_data = $proforma_invoice_data[0];
            $sales_order_data->quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = $this->crud->get_id_by_val('quotations', 'quotation_date', 'id', $sales_order_data->quotation_id);
            $sales_order_data->quotation_date = substr($sales_order_data->quotation_date, 8, 2) .'/'. substr($sales_order_data->quotation_date, 5, 2) .'/'. substr($sales_order_data->quotation_date, 0, 4);
            $sales_order_data->po_date = substr($sales_order_data->po_date, 8, 2) .'/'. substr($sales_order_data->po_date, 5, 2) .'/'. substr($sales_order_data->po_date, 0, 4);
            $sales_order_data->proforma_invoice_date = substr($sales_order_data->sales_order_date, 8, 2) .'/'. substr($sales_order_data->sales_order_date, 5, 2) .'/'. substr($sales_order_data->sales_order_date, 0, 4);
            unset($sales_order_data->sales_order_date);
        }
        
        $challan_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $challan_data->challan_delivery_through_id));
		$challan_data->prepared_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->prepared_by));
		$challan_data->checked_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $challan_data->checked_by));
        $challan_data->port_of_discharge_country = $this->crud->get_id_by_val('country','country','country_id',$challan_data->port_of_discharge_country_id);
        $challan_data->port_of_loading_country = $this->crud->get_id_by_val('country','country','country_id',$challan_data->port_of_loading_country_id);
        $challan_data->place_of_delivery_country = $this->crud->get_id_by_val('country','country','country_id',$challan_data->place_of_delivery_country_id);
		$challan_data->invoice_no = '';
		$challan_data->invoice_date = '';
        $invoice_data = $this->crud->get_row_by_id('invoices', array('challan_id' => $challan_data->id));
		if(!empty($invoice_data)){
			$challan_data->invoice_no = $invoice_data[0]->invoice_no;
			$challan_data->invoice_date = $invoice_data[0]->invoice_date;
		}

		if (!empty($challan_items)) {
			foreach ($challan_items as $key => $item_row) {
				$item_row['hsn_no'] = $this->crud->get_column_value_by_id('items', 'hsn_code', array('id' => $item_row['item_id']));
				$item_row['item_extra_accessories'] = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $item_row['item_extra_accessories_id']));

				$this->db->select('imd.*, u.user_name, m.make_name, h.hp_name, k.kw_name, f.frequency_name, r.rpm_name, v.volts_cycles_name');
				$this->db->from('challan_items_motor_details imd');
				$this->db->join('challan_item_user u','u.id = imd.motor_user','left');
				$this->db->join('challan_item_make m','m.id = imd.motor_make','left');
				$this->db->join('challan_item_hp h','h.id = imd.motor_hp','left');
				$this->db->join('challan_item_kw k','k.id = imd.motor_kw','left');
				$this->db->join('challan_item_frequency f','f.id = imd.motor_frequency','left');
				$this->db->join('challan_item_rpm r','r.id = imd.motor_rpm','left');
				$this->db->join('challan_item_volts_cycles v','v.id = imd.motor_volts_cycles','left');
				$this->db->where('challan_item_id', $item_row['id']);
				$m_query = $this->db->get();
				$motor_data = $m_query->result();

				$this->db->select('igd.*, u.user_name, m.make_name, g.gear_type_name, mo.model_name, r.ratio_name');
				$this->db->from('challan_items_gearbox_details igd');
				$this->db->join('challan_item_user u','u.id = igd.gearbox_user','left');
				$this->db->join('challan_item_make m','m.id = igd.gearbox_make','left');
				$this->db->join('challan_item_gear_type g','g.id = igd.gearbox_gear_type','left');
				$this->db->join('challan_item_model mo','mo.id = igd.gearbox_model','left');
				$this->db->join('challan_item_ratio r','r.id = igd.gearbox_ratio','left');
				$this->db->where('challan_item_id', $item_row['id']);
				$g_query = $this->db->get();
				$gearbox_data = $g_query->result();
				if($challan_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID){
					$item_row['challan_declaration'] = $this->applib->getTermsandConditions('challan_declaration_for_domestic');
				}elseif($challan_data->party_type_1 == PARTY_TYPE_EXPORT_ID){
					$item_row['challan_declaration'] = $this->applib->getTermsandConditions('challan_declaration_for_export');
				}else{
					$item_row['challan_declaration'] = null;
				}

				$param = "'utf-8','A4'";
				$pdf = $this->m_pdf->load($param);
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				if ($key == 0) {
					$other_items = $this->get_challan_sub_items($id);
					$html = $this->load->view('dispatch/challan/challan_pdf', array('sales_order_data' => $sales_order_data, 'challan_data' => $challan_data, 'challan_item' => $item_row, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
				} else {
					$html = $this->load->view('dispatch/challan/challan_pdf', array('sales_order_data' => $sales_order_data, 'challan_data' => $challan_data, 'challan_item' => $item_row, 'motor_data' => $motor_data, 'gearbox_data' => $gearbox_data, 'company_details' => $company_details, 'is_first_item' => false), true);
				}
				$pdf->WriteHTML($html);

				$pdfFilePath = "challan_".$id.".pdf";
				$pdf->Output($pdfFilePath, "I");
			}
		}
	}

	function get_challan_detail($id){
		$select = "ch.*, ch.id as challan_id, ch.notes as challan_notes, ch.delivery_through_id as challan_delivery_through_id, ch.loading_at as challan_loading_at, party.gst_no AS party_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.address,city.city,state.state,country.country,party.utr_no AS party_cin_no,";
		$select .= "party.tin_vat_no,party.tin_cst_no,party.pan_no,party.party_type_1,cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
		$this->db->select($select);
		$this->db->from('challans ch');
		$this->db->join('party party', 'party.party_id = ch.sales_to_party_id', 'left');
		$this->db->join('contact_person cp', 'cp.contact_person_id = ch.kind_attn_id', 'left');
		$this->db->join('city', 'city.city_id = party.city_id', 'left');
		$this->db->join('state', 'state.state_id = party.state_id', 'left');
		$this->db->join('country', 'country.country_id = party.country_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = ch.created_by', 'left');
		$this->db->where('ch.id', $id);
		$this->db->where('party.active !=',0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}

	function get_challan_items($id){
		$this->db->select('soi.*');
		$this->db->from('challan_items soi');
		//        $this->db->join('item_category ic','ic.id = soi.item_category_id');
		//        $this->db->where('ic.item_category','Finished Goods');
		$this->db->where('soi.challan_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	function get_challan_sub_items($id){
		$this->db->select('soi.*');
		$this->db->from('challan_items soi');
		$this->db->join('item_category ic','ic.id = soi.item_category_id');
		$this->db->where('ic.item_category != ','Finished Goods');
		$this->db->where('soi.challan_id', $id);
		$query = $this->db->get();
		$other_items = array();
		if ($query->num_rows() > 0) {
			$other_items = $query->result_array();
		}
		return $other_items;
	}
    
}
