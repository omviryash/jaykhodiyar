<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Service
 * &@property Crud $crud
 */
class Service extends CI_Controller
{
    private $smtp_email_address = SMTP_EMAIL_ADDRESS;
    private $smtp_email_password = SMTP_EMAIL_PASSWORD;
    private $smtp_email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
    }

    function index()
    {
        //echo $email_address;
        $data['service_list'] = $this->crud->get_all_records('service', 'service_id', 'ASC');
        $data['party_list'] = $this->crud->get_all_with_where('party', 'party_id', 'ASC', array('active !=' => 0));
        if ($this->app_model->have_access_role(SERVICE_MODULE_ID, "view")) {
            set_page('service/service_list', $data);
        }else{
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
        
    }

    function getSalesOrderWithItems($id = 0)
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        }
        $this->db->select('ch.sales_order_id,ch.sales_to_party_id,ch.challan_date,so.sales_order_no,chi.*,chi.id as challan_item_id,invoices.invoice_no as bill_no');
        $this->db->from('challans ch');
        $this->db->join('sales_order so','so.id = ch.sales_order_id');
        $this->db->join('invoices','invoices.sales_order_id = ch.sales_order_id','left');
        $this->db->join('challan_items chi','chi.challan_id = ch.id');
        $this->db->where('ch.sales_to_party_id',$id);
        $query = $this->db->get();
        $challan_items = $query->result();
        $challan_items_table_html = $this->load->view('shared/dispatch_challan_items',array('challan_items'=>$challan_items),true);
        echo json_encode(array('challan_items_table_html'=>$challan_items_table_html));
    }

    function save_service()
    {
        $post_data = $this->input->post();
        $party_data = $post_data['party'];
        $party_id = $party_data['party_id'];

        $result = $this->check_party_validations_service($party_id, $party_data);
        if ($result['status'] == 0) {
            echo json_encode(array('success' => false, 'message' => $result['msg']));
            exit();
        }

        if (!empty($post_data['service_id'])) {
            $service_id = $post_data['service_id'];
            $service_data = array(
                'party_id' => $party_id,
                'service_date' => strtotime($post_data['service_date']) > $post_data['service_date'] ? date('Y-m-d', strtotime($post_data['service_date'])) : date('Y-m-d'),
                'note' => $post_data['note'],
                'paid_free' => $post_data['paid_free'],
                'charge' => $post_data['charge'],
                'service_provide_by' => $post_data['service_provide_by'],
                'service_received_by' => $post_data['service_received_by'],
                'service_type' => $post_data['service_type'],
                'updated_by' => $this->session->userdata('is_logged_in')['staff_id'],
            );
            $this->crud->update('service', $service_data, array('service_id' => $service_id));
        } else {
            $service_data = array(
                'party_id' => $party_id,
                'service_date' => strtotime($post_data['service_date']) > $post_data['service_date'] ? date('Y-m-d', strtotime($post_data['service_date'])) : date('Y-m-d'),
                'note' => $post_data['note'],
                'paid_free' => $post_data['paid_free'],
                'charge' => $post_data['charge'],
                'service_provide_by' => $post_data['service_provide_by'],
                'service_received_by' => $post_data['service_received_by'],
                'service_type' => $post_data['service_type'],
                'created_by' => $this->session->userdata('is_logged_in')['staff_id'],
                'updated_by' => $this->session->userdata('is_logged_in')['staff_id'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $service_id = $this->crud->insert('service', $service_data);
            
        }

        if (!empty($post_data['service_items'])) {
            $this->crud->delete('service_items', array('service_id' => $service_id));
            foreach ($post_data['service_items'] as $service_item_row) {
                $service_item_data = array(
                    'service_id' => $service_id,
                    'challan_id' => $service_item_row['challan_id'],
                    'challan_item_id' => $service_item_row['challan_item_id'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->crud->insert('service_items', $service_item_data);
            }
        }
        if($service_id){
                $mail_details = $this->crud->service_data($service_id);
                $mail_item_details = $this->crud->service_item_data($service_id);
                //echo "<pre>";print_r($mail_item_details);die;

                $table= "<table width='' cellspacing='0' cellpadding='0'>";
                $table .="<tr><td>Party Name : </td><td>". $mail_details[0]['party_name'] ."</td></tr>";
                $table .="<tr><td>Service Date : </td><td>". $mail_details[0]['service_date'] ."</td></tr>";
                $table .="<tr><td>Note : </td><td>". $mail_details[0]['note'] ."</td></tr>";
                $table .="<tr><td>Paid Free : </td><td>". $mail_details[0]['paid_free'] ."</td></tr>";
                if($mail_details[0]['charge'] != ''){
                    $table .="<tr><td>Charge : </td><td>". $mail_details[0]['charge'] ."</td></tr>";
                }
                $table .="<tr><td>Service Provide By : </td><td>". $mail_details[0]['service_provide_by'] ."</td></tr>";
                $table .="<tr><td>Service Received By : </td><td>". $mail_details[0]['service_received_by'] ."</td></tr>";
                if($mail_details[0]['service_type'] == 'parts_service'){
                    $service_type = 'Parts Service';
                }else if($mail_details[0]['service_type'] =='technical_service'){
                    $service_type = 'Technical Service';
                }
                $table .="<tr><td>Service Type : </td><td>". $service_type ."</td></tr>";
                $table .="<tr><td clospan='2' align='center'><br><b>Service Items</b></td></tr>";
                $table .="</table>";
                $table .="<br><table>";
                foreach ($mail_item_details as $mail_item_row) {
                    $table .="<tr>";
                    $table .="<td><b>". $mail_item_row['item_code'] ."</b> - ". $mail_item_row['item_name'] ."</td>";
                    $table .="</tr>";
                }
                $table .="</table>";
                
                $email_to = $mail_details[0]['email_id'] . "," . SERVICE_EMAIL_ADDRESS;
                $sent_mail = $this->send_smtp_mail($email_to, 'Service', $table, $attach = null);
            }
        echo json_encode(array('success'=>true,'message'=>'Service saved successfully!','service_id'=>$service_id));
        exit();
    }
    
    public function send_smtp_mail($to, $subject, $message, $attach = null)
    {
        $this->load->library('email');
		$this->email->set_smtp_user(SMTP_EMAIL_ADDRESS);
		$this->email->set_smtp_pass(SMTP_EMAIL_PASSWORD);
        $this->email->set_newline("\r\n");
        $this->email->from(SMTP_EMAIL_ADDRESS, "Jaykhodiyar ERP e-mailer");
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($attach != null && $attach != '') {
            $this->email->attach($attach);
        }
		$this->email->bcc(BCC_EMAIL_ADDRESS);
        if ($this->email->send()) {
            return true;
        } else {
            /*echo $this->email->print_debugger();*/
            return false;
        }
    }

    function check_party_validations_service($party_id,$party_data)
    {
        if(empty($party_id)) {
            return array('status' => 0, 'msg' => "Please select party.");
        }

        if(empty($party_data['email_id']) && empty($party_data['phone_no'])) {
            $email_msg = "Please enter phone number or email id.";
            return array('status' => 0, 'msg' => $email_msg);
        }

        $update_data = array();
        if(!empty($party_data['email_id'])) {
            $email_id = isset($party_data['email_id']) ? $party_data['email_id'] : '';
            $temp = nl2br($email_id);
            $emails = explode("<br />", $temp);

            $email_status = 1;
            $email_msg = "";

            $email_ids = array();
            if (is_array($emails) && count($emails) > 0) {
                foreach ($emails as $email) {
                    if (trim($email) != "") {
                        $email = trim($email);
                        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                            $email_msg .= "$email is not valid email.&nbsp;";
                            $email_status = 0;
                        } else {
                            $email_ids[] = $email;
                        }
                    }
                }
            }

            if ($email_status == 0) {
                return array('status' => 0, 'msg' => $email_msg);
            }
            $update_data['email_id'] = implode(',', $email_ids);
        }

        if(!empty($party_data['phone_no'])) {
            $temp = nl2br($party_data['phone_no']);
            $phone_numbers = explode("<br />", $temp);
            $phone_numbers1 = "";
            foreach ($phone_numbers as $phone) {
                if (trim($phone) != "")
                    $phone_numbers1 .= $phone . ",";
            }
            $update_data['phone_no'] = $phone_numbers1;
        }

        if(!empty($party_data['address'])) {
            $update_data['address'] = $party_data['address'];
        }

        if(!empty($party_data['city_id'])) {
            $update_data['city_id'] = $party_data['city_id'];
        }

        if(!empty($party_data['state_id'])) {
            $update_data['state_id'] = $party_data['state_id'];
        }

        if(!empty($party_data['country_id'])) {
            $update_data['country_id'] = $party_data['country_id'];
        }

        if(!empty($party_data['fax_no'])) {
            $update_data['fax_no'] = $party_data['fax_no'];
        }

        if(!empty($party_data['website'])) {
            $update_data['website'] = $party_data['website'];
        }

        if(!empty($party_data['pincode'])) {
            $update_data['pincode'] = $party_data['pincode'];
        }

        $this->db->where('party_id', $party_id);
        $this->db->update('party', $update_data);
        return array('status' =>1, 'msg' =>'Success');
    }

    function updateService()
    {
        //$arr = array('3:2','4:2','12:6');

        $sales_item = preg_split("/[,]+/", $_POST['cdata']);
        $service_id = $_POST['service_id'];
        if (isset($_POST['paid_free'])) {
            $data['paid_free'] = 1;
            // Checkbox is selected
        } else {
            $data['paid_free'] = 0;
            // Alternate code
        }
        //print_r($_POST);exit;
        $service_date = date("Y-m-d", strtotime($_POST['service_date']));
        $date = $_POST['service_date'];
        $date = str_replace('/', '-', $date);
        $service_date = date('Y-m-d', strtotime($date));
        //echo $service_date."---".strtotime($_POST['service_date'])."---------".$_POST['service_date'];exit;
        $date = date('Y-m-d H:i:s');
        $staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        if ($this->crud->update('service', array('party_id' => $_POST['party_id'], 'note' => $_POST['notes'], 'paid_free' => $data['paid_free'],'charge' => $data['charge'], 'service_provide_by' => $data['service_provide_by'],'service_received_by' => $data['service_received_by'], 'updated_by' => $staff_id, 'updated_at' => $date, 'service_date' => $service_date), array('service_id' => $service_id))) {
            //$service_id=$this->db->insert_id();
            if ($this->crud->delete('service_items', array('service_items_id' => $service_id))) {
                foreach ($sales_item as $item) {
                    $item_array = preg_split("/[:]+/", $item);

                    $this->crud->insert('service_items', array('service_items_id' => $service_id, 'sales_order_id' => $item_array[1], 'sales_order_item_id' => $item_array[0]));
                }
            }
            echo json_encode(array("status" => TRUE, "msg" => 'Service updated successfully'));
        } else {
            echo json_encode(array("status" => FALSE, "msg" => 'Failed to add services'));
        }
    }

    function service()
    {
		$data['users'] = $this->crud->getFromSQL("SELECT * FROM staff WHERE active != '0'");
        $data['service_list'] = $this->crud->get_all_records('service', 'service_id', 'ASC');
        if ($this->app_model->have_access_role(SERVICE_MODULE_ID, "add")) {
            set_page('service/service', $data);
        }else{
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function service_edit($service_id)
    {
        $data['service_data'] =  $this->db->select('*')->from('service')->where('service_id',$service_id)->limit(1)->get()->row();
		$data['users'] = $this->crud->getFromSQL("SELECT * FROM staff WHERE active != '0'");
        $service_item_result = $this->db->select('*')->from('service_items')->where('service_id',$service_id)->get()->result();
        $challan_item_id = array();
        foreach($service_item_result as $service_item_row) {
            $challan_item_id[] = $service_item_row->challan_item_id;
        }
        $data['challan_item_id'] = $challan_item_id;
        $data['followup_history_data'] = $this->crud->get_service_followup_history_by_id($service_id);
        //pre_die($data);
        if ($this->app_model->have_access_role(SERVICE_MODULE_ID, "edit")) {
            set_page('service/service',$data);
        }else{
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
        
    }

    function add_service_followup_history()
    {
        //echo "<pre>";print_r($_POST);exit;
        $data = array();
        $data['service_id'] = $_POST['service_followup_history_id'];
        $data['followup_date'] = date("Y-m-d", strtotime(str_replace("/", "-", $_POST['followup_history_date'])));
        $data['history'] = $_POST['followup_history_history'];
        $data['followup_by'] = $this->session->userdata('is_logged_in')['staff_id'];

        if (isset($_POST['followup_history_id']) && !empty($_POST['followup_history_id'])) {
            $followup_history_id = $_POST['followup_history_id'];
            $this->db->where('id', $followup_history_id);
            $rs = $this->db->update('service_followup_history', $data);
            if ($rs) {
                $return_data = $this->db->get_where('service_followup_history', array('id' => $followup_history_id));
                $return_data = $return_data->result_array();
                $return_data[0]['followup_by'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $return_data[0]['followup_by']));
                echo json_encode(array('status' => true, 'message' => "Follow Up Edited!", 'data' => $return_data[0]));
            } else {
                echo json_encode(array('status' => true, 'message' => "Follow Up Edit Fail!"));
            }
            exit();
        } else {
            $data['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert('service_followup_history', $data);
            $followup_history_id = $this->db->insert_id();
        }

        /*------------ Return Response ----------------*/
        echo json_encode(array('status' => true, 'message' => "Follow Up Added!", 'followup_history_id' => $followup_history_id));
        exit();
    }

    function delete_service_followup_history($id)
    {
        $where_array = array("id" => $id);
        $this->crud->delete("service_followup_history", $where_array);
        echo json_encode(array('status' => true, 'message' => "Follow Up Deleted!"));
        exit();
    }



    function get_service_followup_history_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('service_followup_history');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $followup_h_data = $query->row_array(0);
            echo json_encode(array('success' => true, 'followup_h_data' => $followup_h_data));
            exit();
        } else {
            echo json_encode(array('success' => false));
            exit();
        }
    }

    function service_list_datatable()
    {
        $post_data = $this->input->post();
        $config['table'] = 'service';
        $config['select'] = 'service_id,party.party_name,note,paid_free,service_date';
        $config['column_search'] = array('party.party_name', 'note');
        $config['column_order'] = array('party.party_name', 'note');
        $config['joins'][0]['join_table'] = "party";
        $config['joins'][0]['join_by'] = " service.party_id=party.party_id ";
        $config['joins'][0]['join_type'] = "left";
        $config['wheres'][] = array('column_name' => 'party.active !=', 'column_value' => '0');

        if (!empty($post_data['party_id'])) {
            $config['wheres'][] = array('column_name' => 'service.party_id', 'column_value' => $post_data['party_id']);
        }

        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
        } else {
            $config['custom_where'][] = '( `created_by`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR `created_by` IS NULL )';
        }
        $config['order'] = array('party.party_name' => 'ASC', 'service_date' => 'DESC');
        $this->load->library('datatables', $config, 'datatable');
        $role_edit = $this->app_model->have_access_role(SERVICE_MODULE_ID, "edit");
        $role_delete = $this->app_model->have_access_role(SERVICE_MODULE_ID, "delete");
        $list = $this->datatable->get_datatables();
        $data = array();

        foreach ($list as $service) {
            $row = array();
            $action = '';
            if($role_edit){
                $action .= '<a href="' . base_url("service/service_edit/" . $service->service_id) . '" class="btn btn-xs btn-primary btn-edit-service" data-service_id="' . $service->service_id . '"><i class="fa fa-edit"></i></a>';    
            }
            if($role_delete){
                $action .= ' | <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_button" data-href="' . base_url('service/delete/' . $service->service_id) . '"><i class="fa fa-trash"></i></a>';
            }
            $row[] = $action;
            $row[] = '<a href="' . base_url('service/service_edit/' . $service->service_id . '?view') . '" >'.$service->party_name.'</a>';
            $row[] = '<a href="' . base_url('service/service_edit/' . $service->service_id . '?view') . '" >'.date('d-m-Y', strtotime($service->service_date)).'</a>';
            $row[] = '<a href="' . base_url('service/service_edit/' . $service->service_id . '?view') . '" >'.ucfirst($service->paid_free).'</a>';
            $row[] = '<a href="' . base_url('service/service_edit/' . $service->service_id . '?view') . '" >'.$service->note.'</a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function saving_service_data()
    {

        $post_data = $this->input->post();

        //echo '<pre>'; print_r($post_data); echo '</pre>'; exit;

        $data = array();
        $data['party_id'] = isset($_POST['party_id']) ? $_POST['party_id'] : '';

        $data['note'] = isset($_POST['note']) ? $_POST['note'] : '';
        //0=free,1=paid
        //$data['paid_free'] = isset($_POST['paid_free']) ? $_POST['paid_free'] : '';
        if (isset($_POST['paid_free'])) {
            $data['paid_free'] = 1;
            // Checkbox is selected
        } else {
            $data['paid_free'] = 0;
            // Alternate code
        }
        //echo  $data['paid_free'];exit;
        if ($data['party_id'] == '' && $data['note'] == '') {
            redirect('service/index', 'refresh');
        }

        if (isset($post_data['service_id']) AND $post_data['service_id'] != '' AND $post_data['service_edit'] == 'service_edit') {

            $where_array['service_id'] = $post_data['service_id'];

            $post_data['updated_at'] = date('Y-m-d H:i:s');

            $result = $this->crud->update('service', $data, array('service_id' => $post_data['service_id']));

            if ($result) {
                $post_data['service_id'] = $result;
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'service Updated Successfully');
            }

        } else {

            $data['created_at'] = date("Y-m-d H:i:s");

            $data['created_by'] = $this->session->userdata('is_logged_in')['staff_id'];

            $this->db->insert('service', $data);
            $rslt = $this->db->insert_id();

            if ($rslt > 0) {
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Service Added Successfully');
            } else {
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'Somthing Wrong');
            }
        }
        redirect('service/index', 'refresh');
    }
	

    function delete($id)
    {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
        $this->session->set_flashdata('success', true);
        $this->session->set_flashdata('message', 'Deleted Successfully');
    }

    function general_reminder(){
        $data = array();
        set_page('reminder/customer_reminder',$data);
    }
    function general_reminder_list(){
		set_page('reminder/customer_reminder_list');
	}
    
}
