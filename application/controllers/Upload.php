<?php
  
   class Upload extends CI_Controller {
	
      public function __construct() { 
         parent::__construct(); 
         $this->load->helper(array('form', 'url')); 
      }
		
      public function index() { 
         $this->load->view('uploadForm', array('error' => ' ','image_name' => ' ')); 
      }
      public function frontupload() { 
         $this->load->view('uploadFormFront', array('error' => ' ','image_name' => ' ')); 
      }
      public function fileuploadtovisitor() { 
         $this->load->view('uploadFormadmintovisitor', array('error' => ' ','image_name' => ' ')); 
      } 
		
      public function do_upload() { 
		 //$config['upload_path']   =  base_url()."uploads/";
		 $config['upload_path']   =  "uploads/";
		 $config["allowed_types"] ="*";
         $this->upload->initialize($config);	
         $this->load->library('upload', $config);
			
         if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors(),'image_name' => ' ');
            $this->load->view('uploadForm', $error);
        }
		else {
            $data = array('upload_data' => $this->upload->data()); 
            $image_name = $data['upload_data']['file_name'];
			$this->load->view('uploadForm', array('error' => ' ','image_name' => $image_name));
        } 
      }

      public function upload_front() { 
		 //$config['upload_path']   =  base_url()."uploads/";
		 $config['upload_path']   =  "uploads/";
		 $config["allowed_types"] ="*";
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, TRUE);
            }
         $this->upload->initialize($config);	
         $this->load->library('upload', $config);
		 
         if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors(),'image_name' => ' ');
            $this->load->view('uploadFormFront', $error);
        }
		else {
            $data = array('upload_data' => $this->upload->data()); 
            $image_name = $data['upload_data']['file_name'];
			$this->load->view('uploadFormFront', array('error' => ' ','image_name' => $image_name));
        } 
      }

      public function upload_admin_to_front() { 
		 //$config['upload_path']   =  base_url()."uploads/";
		 $config['upload_path']   =  "uploads/";
		 $config["allowed_types"] ="*";
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, TRUE);
            }
         $this->upload->initialize($config);	
         $this->load->library('upload', $config);
         if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors(),'image_name' => ' ');
            $this->load->view('uploadFormadmintovisitor', $error);
        }
		else {
            $data = array('upload_data' => $this->upload->data()); 
            $image_name = $data['upload_data']['file_name'];
			$this->load->view('uploadFormadmintovisitor', array('error' => ' ','image_name' => $image_name));
        } 
      } 
   } 
?>
