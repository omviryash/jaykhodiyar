<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_condition extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
	}
	
	function terms_condition_group($id="")
	{
		$terms_condition_group = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$terms_condition_group = $this->crud->get_all_with_where('terms_condition_group','code','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('terms_condition_group', 'code', 'ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($terms_condition_group) ? $terms_condition_group[0]->id : '';
		$return['code'] = ($terms_condition_group) ? $terms_condition_group[0]->code : '';
		$return['description'] = ($terms_condition_group) ? $terms_condition_group[0]->description : '';
		if($this->applib->have_access_role(MASTER_TERMS_CONDITION_GROUP_MENU_ID,"view")) {
			set_page('terms_condition/terms_condition_group', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	
	function add_terms_condition_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('terms_condition_group',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Terms Condition Group Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_terms_condition_group()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('terms_condition_group', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Terms Condition Group Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function terms_condition_detail($id = 0 )
	{
		$terms_condition_detail = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$terms_condition_detail = $this->crud->get_all_with_where('terms_condition_detail','code','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('terms_condition_detail', 'code', 'ASC');
		$modal_data = $this->crud->get_all_records('terms_condition_group','code','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['modal_results'] = $modal_data;
		$return['id'] = ($terms_condition_detail) ? $terms_condition_detail[0]->id : '';
		$return['code'] = ($terms_condition_detail) ? $terms_condition_detail[0]->code : '';
		$return['group_name1'] = ($terms_condition_detail) ? $terms_condition_detail[0]->group_name1 : '';
		$return['group_name2'] = ($terms_condition_detail) ? $terms_condition_detail[0]->group_name2 : '';
		$return['description'] = ($terms_condition_detail) ? $terms_condition_detail[0]->description : '';
		if($this->applib->have_access_role(MASTER_TERMS_CONDITION_DETAIL_MENU_ID,"view")) {
			set_page('terms_condition/terms_condition_detail', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	
	function add_terms_condition_detail()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('terms_condition_detail',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Terms Condition Detail Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_terms_condition_detail()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('terms_condition_detail', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Terms Condition Detail Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function default_terms_condition($id=""){
		$default_terms_condition = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$default_terms_condition = $this->crud->get_all_with_where('default_terms_condition','template_name','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('default_terms_condition','template_name','ASC');
		$modal_data = $this->crud->get_all_records('terms_condition_group','code','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['modal_results'] = $modal_data;
		$return['id'] = ($default_terms_condition) ? $default_terms_condition[0]->id : '';
		$return['template_name'] = ($default_terms_condition) ? $default_terms_condition[0]->template_name : '';
		$return['group_name'] = ($default_terms_condition) ? $default_terms_condition[0]->group_name : '';
		$return['terms_condition'] = ($default_terms_condition) ? $default_terms_condition[0]->terms_condition : '';
		if($this->applib->have_access_role(MASTER_DEFAULT_TERMS_CONDITION_MENU_ID,"view")) {
			set_page('terms_condition/default_terms_condition', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	
	function add_default_terms_condition()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('default_terms_condition',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Default Terms Condition Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_default_terms_condition()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('default_terms_condition', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Default Terms Condition Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function terms_condition_template($id = 0 )
	{
		$terms_condition_template = array();
		if(isset($id) && !empty($id)){ 
			$where_array['id'] = $id;
			$terms_condition_template = $this->crud->get_all_with_where('terms_condition_template','template_name','ASC',$where_array);	
		}
		$result_data = $this->crud->get_all_records('terms_condition_template', 'template_name', 'ASC');
		$modal_data = $this->crud->get_all_records('terms_condition_group','code','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['modal_results'] = $modal_data;
		$return['id'] = ($terms_condition_template) ? $terms_condition_template[0]->id : '';
		$return['template_name'] = ($terms_condition_template) ? $terms_condition_template[0]->template_name : '';
		$return['group_name'] = ($terms_condition_template) ? $terms_condition_template[0]->group_name : '';
		$return['terms_condition'] = ($terms_condition_template) ? $terms_condition_template[0]->terms_condition : '';
		if($this->applib->have_access_role(MASTER_TERMS_CONDITION_TEMPLATE_MENU_ID,"view")) {
			set_page('terms_condition/terms_condition_template', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
		
	}
	
	function add_terms_condition_template()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('terms_condition_template',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Terms Condition Template Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_terms_condition_template()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $postdata['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('terms_condition_template', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Terms Condition Template Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
	}
    
    function terms_conditions(){
        if(isset($_POST) && !empty($_POST)) {
            //echo '<pre>';print_r($_POST);
            $main_detail = $this->crud->get_data_row_by_id('terms_and_conditions','id',$_POST['modules']);
            $data_arr['module_details'] = $main_detail;
            $data_arr['modules'] = $this->crud->get_all_records('terms_and_conditions', 'sort_1,sort_2', 'ASC');
            $data_arr['related_modules'] = $this->crud->get_all_with_where('terms_and_conditions','sort_2','ASC',array('sort_1' => $main_detail->sort_1));
            $data_arr['module_id'] = $_POST['modules'];
            
            $this->db->select('detail');
            $this->db->from('terms_and_conditions_sub');
            $this->db->where('module_id',$_POST['modules']);
            $query = $this->db->get();
            if($query->num_rows() > 0) {
                foreach ($query->result() as $page_row) {
                    $data_arr['terms_and_condition_detail_pages'][0] = $main_detail->detail;
                    if($main_detail->detail == $page_row->detail)continue;
                    $data_arr['terms_and_condition_detail_pages'][] = $page_row->detail;
                    
                }
            }else{
                $data_arr['terms_and_condition_detail_pages'][0] = $main_detail->detail;
            }
            set_page('terms_condition/new_terms_conditions', $data_arr);
            /*$data_array['detail'] = $_POST['details'];
            $data_array['updated_by'] = $this->now_time;
			$data_array['updated_at'] = $this->logged_in_id;
            $where_array['id'] = $_POST['modules'];
            $result = $this->crud->update('terms_and_conditions',$data_array,$where_array);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Terms and Condition successfully saved!');
			redirect('terms_condition/terms_conditions');*/
        } else {
            $data['modules'] = $this->crud->get_all_records('terms_and_conditions', 'label', 'ASC');
            if($this->applib->have_access_role(MASTER_TERMS_CONDITIONS_MENU_ID,"view")) {
				set_page('terms_condition/new_terms_conditions', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
        }
    }
    
    function terms_conditions_table(){
        $data['terms_and_conditions_modules'] = $this->crud->get_terms_and_conditions_module_names();
        if($this->applib->have_access_role(MASTER_TERMS_CONDITIONS_MENU_ID,"view")) {
			set_page('terms_condition/terms_conditions_table', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
    }
    
    function save_tandc_master(){
        $post_data = $this->input->post();

        /*echo "<pre>";
		print_r($_POST);
		die;*/
        
        $tandc_documents = $post_data['tandc_documents'];
        $module_id = $post_data['module_id'];
        
        if (!empty($tandc_documents)) {
            $this->db->where('module_id', $module_id);
            $this->db->delete('terms_and_conditions_sub');
            foreach ($tandc_documents as $terms_and_conditions) {
                if($terms_and_conditions[0] != ''){
                    $this->db->where('id', $module_id);
                    $this->db->update('terms_and_conditions', array('detail'=>$terms_and_conditions[0],'updated_at'=>date('Y-m-d H:i:s')));
                }
                foreach($terms_and_conditions as $detail) {
                    if ($detail != '') {
                        $this->crud->insert('terms_and_conditions_sub', array('module_id' => $module_id, 'detail' => $detail,'updated_by'=>'1'));
                    }
                }
                
            }
        }
        if ($module_id > 0) {
            $data['success'] = true;
            $data['module_id'] = $module_id;
            $data['message'] = "Terms and Condition successfully saved!";
            //$data['module_id'] = $this->db->insert_id();
            echo json_encode($data);
        } else {
            echo json_encode(array('success' => false, 'message' => "Something wrong!"));
        }
        exit();
    }
}
