<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
/**
 * Class Frontend
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 * @property Chatmodule $chat_module
 */
class Frontend_API extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('chatmodule', 'chat_module');
        //header('Access-Control-Allow-Origin: *');  
        //header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    }

    public function index()
    {
		//echo "<pre>";print_r($this->session->userdata());exit;
		$data['anystaff_login'] = $this->chat_module->checkAnyStaffLogin();
		$data['visitor_id'] = 0;
		$data['session_id'] = 0;
		if($data['anystaff_login'] > 0)
		{
			if(!$this->session->userdata('is_visitor_logged_in'))
			{
				$session_id = $this->session->userdata('__ci_last_regenerate');
				$visitor_name = "";
				$visitor_phone = "";
				$visitor_email = "";
				$visitor_id = $this->chat_module->register_visitor($visitor_name, $visitor_phone, $visitor_email,$session_id);
				$vis_array = array(
					'name' => $visitor_name,
					'email' => $visitor_email,
					'phone' => $visitor_phone,
					'visitor_id' => $visitor_id,
					'session_id' => $session_id
				);
				$this->session->set_userdata('is_visitor_logged_in',$vis_array);
				$data['visitor_id'] = $visitor_id;
				$data['session_id'] = $session_id;
			}
			else
			{
				$session_data = $this->session->userdata('is_visitor_logged_in');
				$data['visitor_id'] = $session_data['visitor_id'];
				$data['session_id'] = $session_data['session_id'];
				/*set 'is_disconnect_mail_sent' to 0, to send again chat mail on disconnect*/
				$is_disconnect_mail_sent = 0;
				$this->chat_module->updateDisconnectMailValue($is_disconnect_mail_sent,$data['visitor_id']);
			}
		}
		$popup_flag = $this->config->item('opennewvisitorpopup_admin');
		if($popup_flag)
		{
			$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
			$client->initialize();
			$client->emit('new_visitor_come', ['session_id'=>$data['session_id'],'visitor_name' => 'Visitor']);
			$client->close();
		}
        //$this->load->view('frontend/home',$data);
        echo json_encode($data);
    }

	public function chatbox()
	{
		$data['anystaff_login'] = $this->chat_module->checkAnyStaffLogin();
		$data['visitor_id'] = 0;
		$data['session_id'] = 0;
		if($data['anystaff_login'] > 0)
		{
			if(!$this->session->userdata('is_visitor_logged_in'))
			{
				$session_id = $this->session->userdata('__ci_last_regenerate');
				$visitor_name = "";
				$visitor_phone = "";
				$visitor_email = "";
				$visitor_id = $this->chat_module->register_visitor($visitor_name, $visitor_phone, $visitor_email,$session_id);
				$vis_array = array(
					'name' => $visitor_name,
					'email' => $visitor_email,
					'phone' => $visitor_phone,
					'visitor_id' => $visitor_id,
					'session_id' => $session_id
				);
				$this->session->set_userdata('is_visitor_logged_in',$vis_array);
				$data['visitor_id'] = $visitor_id;
				$data['session_id'] = $session_id;
			}
			else
			{
				$session_data = $this->session->userdata('is_visitor_logged_in');
				$data['visitor_id'] = $session_data['visitor_id'];
				$data['session_id'] = $session_data['session_id'];
				/*set 'is_disconnect_mail_sent' to 0, to send again chat mail on disconnect*/
				$is_disconnect_mail_sent = 0;
				$this->chat_module->updateDisconnectMailValue($is_disconnect_mail_sent,$data['visitor_id']);
			}
		}
		$popup_flag = $this->config->item('opennewvisitorpopup_admin');
		if($popup_flag)
		{
			$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
			$client->initialize();
			$client->emit('new_visitor_come', ['session_id'=>$data['session_id'],'visitor_name' => 'Visitor']);
			$client->close();
		}
		//$this->load->view('frontend/chatbox',$data);
		echo json_encode($data);
	}

	function iframe()
	{
		$this->load->view('frontend/iframe');
	}
	
	public function check_visitor()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		
        $visitor_email = $_POST['visitor_email'];
        $visitor_name = $_POST['visitor_name'];
        $visitor_id = $_POST['visitor_id'];
        $visitor_phone = $_POST['visitor_phone'];
        $data['success'] = true;
        //echo  $visitor_email;
        //print_r($_POST);exit;
		if($visitor_email != "null"){
			$data['visitor_id'] = $this->chat_module->fetch_visitor_by_email($visitor_email);
			$data['visitor_email'] = $visitor_email;
			$data['visitor_name'] = $visitor_name;
			$data['visitor_phone'] = $visitor_phone;
			$data['else'] = "if";
		}else{
			$visitor_data = array(
				'name' => $visitor_name,
				'phone' => $visitor_phone,
				'email' => $visitor_email,
				'ip' => $ip_address,
				'from_address' => $from_address,
				'currency' => $currancy,
				'country' => $country,
				'city' => $city,
				'others' => $others,
				'session_id' => $visitor_id,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$this->db->insert('visitors', $visitor_data);
			$data['visitor_id'] = $this->db->insert_id();
			$data['else'] = "in else";
			//$visitor_id = $this->chat_module->fetch_visitor_by_session_id($session_id);
		}
        echo json_encode($data);
	}
	
	public function update_visitor_profile()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		
        $visitor_email = $_POST['visitor_email'];
        $visitor_name = $_POST['visitor_name'];
        $visitor_id = $_POST['visitor_id'];
        $visitor_phone = $_POST['visitor_phone'];
        $data['name'] = $_POST['visitor_name'];
        $data['phone'] = $_POST['visitor_phone'];
        $data['email'] = $_POST['visitor_email'];
        
        //$data = array('is_disconnect_mail_sent' => $is_disconnect_mail_sent);
		$this->db->where("id",$visitor_id);
		$res = $this->db->update('visitors', $data);
				
        echo json_encode($data);
	}
	
	public function getVisitorByEmail()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		
		$visitor_email = $_POST['visitor_email'];
		$visitor = $this->chat_module->fetch_visitor_by_email2($visitor_email);
		if(!$visitor)
		{
			$data['visitor_id'] = $visitor;
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}
		echo json_encode($data);
	}
	
	public function getVisitorByUUID()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		
		$uuid = $_POST['uuid'];
		$visitor = $this->chat_module->fetch_visitor_by_uuid($uuid);
		//print_r($visitor);
		if($visitor->id > 0)
		{
			$data['visitor_id'] = $visitor->id;
			$data['visitor_email'] = $visitor->email;
			$data['visitor_name'] = $visitor->name;
			$data['visitor_phone'] = $visitor->phone;
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}
		echo json_encode($data);
	}
	
/*	public function check_visitor()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		
        $visitor_email = $_POST['visitor_email'];
        $visitor_name = $_POST['visitor_name'];
        $visitor_id = $_POST['visitor_id'];
        $visitor_phone = $_POST['visitor_phone'];
        $data['success'] = true;
        //echo  $visitor_email;
        //print_r($_POST);exit;
		if($visitor_email != "null"){
			$data['visitor_id'] = $this->chat_module->fetch_visitor_by_email($visitor_email);
			$data['visitor_email'] = $visitor_email;
			$data['visitor_name'] = $visitor_name;
			$data['visitor_phone'] = $visitor_phone;
			$data['else'] = "if";
		}else{
			$visitor_data = array(
				'name' => $visitor_name,
				'phone' => $visitor_phone,
				'email' => $visitor_email,
				'ip' => $ip_address,
				'from_address' => $from_address,
				'currency' => $currancy,
				'country' => $country,
				'city' => $city,
				'others' => $others,
				'session_id' => $visitor_id,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$this->db->insert('visitors', $visitor_data);
			$data['visitor_id'] = $this->db->insert_id();
			$data['else'] = "in else";
			//$visitor_id = $this->chat_module->fetch_visitor_by_session_id($session_id);
		}
        echo json_encode($data);
	}
	*/
    public function send_message()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		//print_r($_POST);exit;
        //$visitor_email = $_POST['visitor_email'];
        $visitor_name = $_POST['visitor_name'];
        $visitor_id = $_POST['visitor_id'];
        $message = $_POST['message'];
		// $visitor_phone = $_POST['visitor_phone'];
        //dprint_r($_POST);
        $lastmsgid = $this->chat_module->send_visitor_message($visitor_id, $message);
        $visitor_name = $visitor_name == ''?'Visitor':$visitor_name;
        //$chat_html = $this->load->view('shared/right_chat_html', array('message' =>$message,'name' =>$visitor_name,'image'=>'default.png'), true);

        $visitor_data = $this->chat_module->fetch_visitor_by_id($visitor_id);
		$from_session_id = $visitor_data->session_id;
        //$from_name = $visitor_data->name ? $visitor_data->name.'(Visitor)' : 'Visitor';
        if($visitor_data->name == '')
        {
			$from_name = 'Visitor';
		}
		else
		{
			$from_name = $visitor_data->name.' (Visitor)';
		}
		
		$chat_html[] =  array('name' =>$visitor_name, 'type'=>'Visitor', 'date_time'=> date(),'message' =>$message,'image'=>'default.png');
		
        $from_image = 'default.png';
		$date_time = $this->chat_module->get_val_by_id('message','date_time','id',$lastmsgid);
		
        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        $client->emit('new_message_visitor_to_client', ['to_id'=>0,
														'from_id' => $visitor_id,
														'from_session_id' => $from_session_id, 
														'from_table' => 'visitor', 
														'from_name' => $from_name, 
														'from_image' => $from_image,
														'msg' => $message, 
														'date_time' => $date_time, 
														'chat_html' => $chat_html]);		
        $client->close();
        echo json_encode(array('success' => true, 'chat_html' => $chat_html,'visitor_id'=>$visitor_id));
    }

    public function save_visitor_detail()
    {
        $visitor_name = $_POST['visitor_name'];
        $visitor_phone = $_POST['visitor_phone'];
        $visitor_email = $_POST['visitor_email'];
        $session_id = $_POST['session_id'];
        $visitor_id = $this->chat_module->register_visitor($visitor_name, $visitor_phone, $visitor_email,$session_id);
        $message_data = $this->chat_module->get_visitor_history_message($visitor_id);
		
		$visitor_data = $this->chat_module->fetch_visitor_by_id($visitor_id);		
		$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();        
		$client->emit('add_visitor_data', ['name'=>$visitor_data->name ,'phone' =>$visitor_data->phone,'email' => $visitor_data->email, 'ip' => $visitor_data->ip, 'currency' => $visitor_data->currency, 'country' => $visitor_data->country,'city' => $visitor_data->city, 'others' => $visitor_data->others,'session_id' => $visitor_data->session_id,'date' => $visitor_data->created_at]);
        $client->close();
		
        echo json_encode(array('success' => true,'visitor_name' => $visitor_name,'visitor_id'=>$visitor_id,'messages'=>$message_data));
    }

    public function send_mail()
    {
        $contact_us_email_html = $this->load->view('shared/contact_us_email',$_POST, true);
        $visitor_email_html = $this->load->view('shared/visitor_contact_us_email',$_POST, true);
        $messages = array();
        if($this->send_smtp_mail($_POST['visitor_email'],'Jaykhodiyar inquiry',$visitor_email_html)){
			$messages['customer_success'] = true;
			$messages['customer_message'] = 'Mail sent successfully!';
        }else{
			$messages['customer_success'] = false;
			$messages['customer_message'] = 'Please try again, Something wrong!';
        }
        if($this->send_smtp_mail($this->config->item('adminmail'),'Inquiry from visitor',$contact_us_email_html)){
			$messages['admin_success'] = true;
			$messages['admin_message'] = 'Mail sent successfully!';
        }else{
			$messages['admin_success'] = false;
			$messages['admin_message'] = 'Please try again, Something wrong!';
        }
        echo json_encode($messages);
        exit();
    }

    public function send_smtp_mail($to, $subject, $message, $attach = null)
    {
        /*$this->load->library('email');*/
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => SMTP_EMAIL_ADDRESS,
            'smtp_pass' => SMTP_EMAIL_PASSWORD,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->reply_to(FROM_EMAIL_ADDRESS);
        $this->email->from('JayKhodiyar', "$this->email_address");
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($attach != null && $attach != '') {
            $this->email->attach($attach);
        }
		$this->email->bcc(BCC_EMAIL_ADDRESS);
        if ($this->email->send()) {
            return true;
        } else {
            /*echo $this->email->print_debugger();*/
            return false;
        }
    }

    function getVisitorChat(){
		//header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Requested-With');
		//print_r($_POST);
        $visitor_id = $_POST['visitor_id'];
        if($this->session->userdata('last_message_id')){
            $last_message_id = $this->session->userdata('last_message_id');
        }else{
            $last_message_id = 0;
        }
        $last_message_id = 0;
        $message_data = $this->chat_module->getVisitorMessage($visitor_id,$last_message_id);
        if($message_data != false){
            $chat_html = '';
            foreach($message_data as $data){
				if($data['from_table'] == 'visitors')
				{
					$visitordata= $this->chat_module->fetch_visitor_by_id($data['from_id']);
					if($visitordata->name == ""){
						$data['name'] = 'Visitor';
						$data['type'] = 'Visitor';
					}else{
						$data['name'] = $visitordata->name.'(Visitor)';
						$data['type'] = 'Visitor';
					}
					if($data['message'] == "" || $data['message'] == null)
					{
						//message = "<a target='_blank' href='uploads/"+file+"'>"+file+"</a>";
						$message = $data['file'];
						$chat_html[] =array('name'=>$data['name'], 'type'=>'Visitor', 'date_time'=>$data['date_time'],'message'=>$message,'image'=>$data['image']);
					}
					else
					{
						$chat_html[] = array('name'=>$data['name'], 'type'=>'Visitor', 'date_time'=>$data['date_time'],'message'=>$data['message'],'image'=>$data['image']);
					}
				}
				else
				{
					if($data['message'] == "" || $data['message'] == null)
					{
						$message = $data['file'];
						$chat_html[] = array('name'=>$data['name'], 'type'=>'agent' ,'date_time'=>$data['date_time'],'message'=>$message,'image'=>$data['image']);
					}
					else
					{
						$chat_html[] = array('name'=>$data['name'], 'type'=> 'agent' ,'date_time'=>$data['date_time'],'message'=>$data['message'],'image'=>$data['image']);
					}
				}
                $last_message_id = $data['id'];
            }
            $this->session->set_userdata('last_message_id',$last_message_id);
            echo json_encode(array('success'=>true,'chat_html'=>$chat_html));
            exit;
        }else{
            echo json_encode(array('success'=>false));
            exit;
        }
    }

    function addFrontImage()
    {
        $data = array();
        //$nowdate = new DateTime('y-m-d H:i:s');
        $nowdate = date('Y-m-d H:i:s');
        $data['from_id'] = $_POST['from_id'];
        $data['from_table'] = $_POST['from_table'];
        $data['to_id'] = 0;
        $data['to_table'] = $_POST['to_table'];
        $data['file'] = $_POST['msg'] ? $_POST['msg'] : '';
        $data['date_time'] = $nowdate;
        $messages = $this->chat_module->addChatMessage($data);
        $from_data = $visitordata = $this->chat_module->fetch_visitor_by_id($_POST['from_id']);;
        $to_name = 'admin';
        if($from_data->name == '')
        {
			$from_name = 'Visitor';
		}
		else
		{
			$from_name = $from_data->name.'Visitor';
		}
        $from_image = 'default.png';
        $from_session_id = $from_data->session_id;
        $to_session_id = $_POST['to_id'];

		$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
		$client->initialize();
        $client->emit('new_file_visitor_to_client', ['from_id'=>$data['from_id'],'from_session_id'=>$from_session_id,'from_table'=>$data['from_table'],'from_name'=>$from_name,'from_image'=>$from_image,'to_id' => $data['to_id'], 'to_table' => $data['to_table'], 'to_name' => $to_name,'file' => $data['file'], 'date_time' => $data['date_time']]);
        $client->close();
        
        echo json_encode($messages);
        exit;
    }
    
    public function sendEmailToadminForOldChatOfVisitor()
	{
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => SMTP_EMAIL_ADDRESS,
			'smtp_pass' => SMTP_EMAIL_PASSWORD,
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		$userid = $_POST['userid'];
		$vdata = $this->chat_module->fetch_visitor_by_session_id($userid);
		$visitordata = $this->chat_module->fetch_visitor_by_id($vdata);
		
		
		if($visitordata->is_disconnect_mail_sent == 0)
		{

			$messages = $this->chat_module->getVisitorMessageForMail($userid);
			if($messages != 0)
			{
				$is_disconnect_mail_sent = 1;
				$this->chat_module->updateDisconnectMailValue($is_disconnect_mail_sent,$visitordata->id);
				$emailmsg = '';
				//$mail_html = $this->load->view('shared/agent_chat_mail_html',array('messages' => $messages,'user_1'=>$userid,'user_2'=>0),true);
				$mail_html = $this->load->view('shared/visitor_chat_mail_html',array('messages' => $messages,'user_1'=>$staff_id,'user_2'=>0),true);
				foreach($messages as $k=>$v)
				{
					$emailmsg.= $v['name'].":".$v['message']."<br/>";
				}
				//echo $mail_html;die;
				//$to = $this->config->item('adminmail');
				//$to = $this->config->item('visitorchatmail');
				$to = VISITORCHATMAIL;
				$subject = "Chat Conversation With Visitor";                  
				$this->email->set_newline("\r\n");
                $this->email->reply_to(FROM_EMAIL_ADDRESS);
				$this->email->from('JayKhodiyar', FROM_EMAIL_ADDRESS);
				$this->email->to($to);
				$this->email->subject($subject);
				$this->email->message($mail_html);
				$this->email->bcc(BCC_EMAIL_ADDRESS);
				$result = $this->email->send();

				if(!empty($visitordata->email))
				{
					$to = $visitordata->email;
					$subject = "Your Chat Conversation";
					$this->email->set_newline("\r\n");
                    $this->email->reply_to(FROM_EMAIL_ADDRESS);
					$this->email->from('JayKhodiyar', FROM_EMAIL_ADDRESS);
					$this->email->to($to);
					$this->email->subject($subject);
					$this->email->message($mail_html);
					$this->email->bcc(BCC_EMAIL_ADDRESS);
					$result = $this->email->send();
				}

				var_dump($result);
				echo '<br />';
				echo $this->email->print_debugger();

				exit;
			}
		}
	}
	public function DayCroneForAgentToAgentChat()
	{
		$allagents = $this->chat_module->getAllAgentsForCron();
		if($allagents)
		{
			$agent_id = array();
			foreach($allagents as $k=>$v)
			{
				$agent_id[] = $v['staff_id'];
			}
			$total_agents = $agent_id;
			$result = $this->combination($total_agents,2);
			
			foreach($result as $k=>$v)
			{
				$users = explode(',',$v);
				$user_1 = $users[0];
				$user_2 = $users[1];
				if($user_1 === $user_2)
				{
					unset($result[$user_1.$user_2]);
					continue;
				}
				$this->sendAgentToAgentChatMail($user_1,$user_2);
			}
		}
		redirect('/');
	}
	
	function combination($chars, $size, $combinations = array())
	{
		# if it's the first iteration, the first set 
		# of combinations is the same as the set of characters
		if (empty($combinations)) {
			$combinations = $chars;
		}

		# we're done if we're at size 1
		if ($size == 1) {
			return $combinations;
		}

		# initialise array to put new values in
		$new_combinations = array();

		# loop through existing combinations and character set to create strings
		foreach ($combinations as $combination) {
			foreach ($chars as $char) {
				$new_combinations[$combination.$char] = $combination.','.$char;
			}
		}

		# call same function again for the next iteration
		return $this->combination($chars, $size - 1, $new_combinations);

	}
	
	public function sendAgentToAgentChatMail($user_1,$user_2)
	{
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => SMTP_EMAIL_ADDRESS,
			'smtp_pass' => SMTP_EMAIL_PASSWORD,
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		$lastid = 0;
		$messages = $this->chat_module->getAllMessageForCron($user_1,$lastid,$user_2);
		if($messages)
		{
			$emailmsg = '';
			$msg_id = array();
			$mail_html = $this->load->view('shared/agent_chat_mail_html',array('messages' => $messages,'user_1'=>$user_1,'user_2'=>$user_2),true);
			foreach($messages as $k=>$v)
			{
				$emailmsg.= $v['from_name'].":".$v['message']."<br/>";
				$msg_id[] = $v['id'];
			}
			/*echo $mail_html;die;*/
			//$to = $this->config->item('adminmail');
			//$to = $this->config->item('agentchatmail');
			$to = AGENTCHATMAIL;
			$subject = "Chat Conversation Of Agent";                  
			$this->email->set_newline("\r\n");
            $this->email->reply_to(FROM_EMAIL_ADDRESS);
			$this->email->from('JayKhodiyar', FROM_EMAIL_ADDRESS);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($mail_html);
            $this->email->bcc(BCC_EMAIL_ADDRESS);
			$result = $this->email->send();
			/*$msgid_in = implode(',',$msg_id);
			$messages = $this->chat_module->updateCroneMsg($msgid_in);*/
			return;
		}
	}
	
	public function update_from_address()
	{
		if (!empty($_POST)) 
		{
			$session_id = $_POST['session_id'];
			$from_address = $_POST['from_address'];
			$data = $this->chat_module->updateFromAddress($session_id,$from_address);
		}
		exit;
	}	
}
