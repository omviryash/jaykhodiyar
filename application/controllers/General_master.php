<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class General_master
 * &@property Crud crud
 */
class General_master extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
	}
	
	/* function index()
	{
		
	} */
	
	function branch($id=""){
		$branches = array();
		if(isset($id) && !empty($id)){
			$where_array['branch_id'] = $id;
			$branches = $this->crud->get_all_with_where('branch','branch_id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('branch','branch_id','desc');

		$return = array();
		$return['results'] = $result_data;
		$return['branch_id'] = ($branches) ? $branches[0]->branch_id : '';
		$return['branch'] = ($branches) ? $branches[0]->branch : '';
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_BRANCH_MENU_ID,"view")) {
			set_page('general_master/branch', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
		
	}
	
	function update_branch()
	{
		$post_data = $this->input->post();
		$where_array['branch_id'] = $post_data['branch_id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('branch', $post_data,$where_array);
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Branch Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_branch()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('branch',$post_data);
		if($result)
		{
			$post_data['branch_id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Branch Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	
	function project($id=""){
		$projects = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$projects = $this->crud->get_all_with_where('project','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('project','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($projects) ? $projects[0]->id : '';
		$return['project'] = ($projects) ? $projects[0]->project : '';
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_PROJECT_MENU_ID,"view")) {
			set_page('general_master/project', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	
	function update_project()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('project', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Project Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_project()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('project',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Project Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function billing_template($id=""){
		$billing_templates = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$billing_templates = $this->crud->get_all_with_where('billing_template','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('billing_template','id','ASC');

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($billing_templates) ? $billing_templates[0]->id : '';
		$return['template'] = ($billing_templates) ? $billing_templates[0]->template : '';
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_BILLING_TEMPLATE_MENU_ID,"view")) {
			set_page('general_master/billing_template', $return);
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
	
	function update_billing_template()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('billing_template', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Template Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_billing_template()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('billing_template',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Template Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
	}
	
}
