<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Complain
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Complain extends CI_Controller
{
	protected $staff_id = 1;
	function __construct(){
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

    function get_installation_with_item_list($id = 0){
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        }
        $this->db->select('i.installation_id,i.installation_no,i.installation_no_year,i.testing_report_id,i.challan_id,i.party_id,i.installation_date,c.challan_no,ci.*,ci.id as challan_item_id');
        $this->db->from('installation i');
        $this->db->join('challans c','c.id = i.challan_id');
        $this->db->join('challan_items ci','ci.challan_id = c.id');
        $this->db->where('i.party_id',$id);
        $query = $this->db->get();
        $installation_with_items = $query->result();
        
        $this->db->select('com.*, rc.resolve_complain_no, rc.resolve_complain_date, s.name as created_by_name');
        $this->db->from('complain com');
        $this->db->join('resolve_complain rc','rc.complain_id = com.complain_id','left');
        $this->db->join('staff s','s.staff_id = com.created_by');
        $this->db->where('com.party_id',$id);
        $this->db->order_by('com.complain_id','DESC');
        $query = $this->db->get();
        $previous_complain_data = $query->result();
        foreach ($previous_complain_data as $key => $previous_complain){
            if (!empty($previous_complain_data[$key]->complain_date)) {
                $previous_complain_data[$key]->complain_date = date('d-m-Y', strtotime($previous_complain_data[$key]->complain_date));
            }
            if (!empty($previous_complain_data[$key]->resolve_complain_date)) {
                $previous_complain_data[$key]->resolve_complain_date = date('d-m-Y', strtotime($previous_complain_data[$key]->resolve_complain_date));
            }
        }
//        print_r($previous_complain); exit;
        $installation_with_item_html = $this->load->view('shared/installation_with_item',array('installation_with_items'=>$installation_with_items),true);
        $installation_wise_et_tp_html = $this->load->view('shared/installation_wise_et_tp_html',array('installation_with_items'=>$installation_with_items),true);
        echo json_encode(array('installation_with_item_html' => $installation_with_item_html, 'installation_wise_et_tp_html' => $installation_wise_et_tp_html, 'previous_complain_data' => $previous_complain_data));
    }
    
    function get_installation_details() {
		$data = array();
		$installation_id = $this->input->get_post("installation_id");
		$testing_report_id = $this->input->get_post("testing_report_id");
		$challan_id = $this->input->get_post("challan_id");
        $data = $this->get_challan_testing_report_installation_details($challan_id, $testing_report_id, $installation_id);
		//echo '<pre>';print_r($data); exit;
		echo json_encode($data);
		exit;
	}
    
    function get_challan_testing_report_installation_details($challan_id, $testing_report_id, $installation_id) {
        $where = array('installation_id' => $installation_id);
		$installation_data = $this->crud->get_row_by_id('installation', $where);
		$data['installation_data'] = $installation_data[0];
		if (!empty($data['installation_data']->installation_date)) {
			$data['installation_data']->installation_date = date('d-m-Y', strtotime($data['installation_data']->installation_date));
		}
        $data['installation_data']->installation_commissioning_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['installation_data']->installation_commissioning_by);
        if(!empty($data['installation_data']->installation_commissioning_by_name)){
            $data['installation_data']->installation_commissioning_by = $data['installation_data']->installation_commissioning_by_name;
        }
		$data['installation_data']->jk_technical_person = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['installation_data']->jk_technical_person);
        if(!empty($data['installation_data']->jk_technical_person_name)){
            $data['installation_data']->jk_technical_person = $data['installation_data']->jk_technical_person_name;
        }
        $data['installation_data']->project_training_provide_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['installation_data']->project_training_provide_by);
        if(!empty($data['installation_data']->project_training_provide_by_name)){
            $data['installation_data']->project_training_provide_by = $data['installation_data']->project_training_provide_by_name;
        }
		$data['installation_data']->electric_power = $this->crud->get_id_by_val('electric_power', 'label', 'id', $data['installation_data']->electric_power);
		$data['installation_data']->elec_rm2_panel_board_cable = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $data['installation_data']->elec_rm2_panel_board_cable);
		$data['installation_data']->connection_cable_1 = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $data['installation_data']->connection_cable_1);
		$data['installation_data']->water_tank_chiller_system_1 = $this->crud->get_id_by_val('water_tank_or_chiller_system', 'label', 'id', $data['installation_data']->water_tank_chiller_system_1);
		$data['installation_data']->water_plumbing_chiller_conn_1 = $this->crud->get_id_by_val('water_plumbing_or_chiller_conn', 'label', 'id', $data['installation_data']->water_plumbing_chiller_conn_1);
		$data['installation_data']->lubrication_gear_oil_1 = $this->crud->get_id_by_val('lubrication_gear_oil', 'label', 'id', $data['installation_data']->lubrication_gear_oil_1);
		$data['installation_data']->gear_box_oil_1 = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $data['installation_data']->gear_box_oil_1);
		$data['installation_data']->raw_material_1 = $this->crud->get_id_by_val('available_or_not', 'label', 'id', $data['installation_data']->raw_material_1);
		$data['installation_data']->product_quality = $this->crud->get_id_by_val('customer_feedback', 'label', 'id', $data['installation_data']->product_quality);
		
        if (!empty($data['installation_data']->unloading_date)) {
			$data['installation_data']->unloading_date = date('d-m-Y', strtotime($data['installation_data']->unloading_date));
		}
        if (!empty($data['installation_data']->filling_of_foundation_boxes)) {
			$data['installation_data']->filling_of_foundation_boxes = date('d-m-Y', strtotime($data['installation_data']->filling_of_foundation_boxes));
		}
        if (!empty($data['installation_data']->installation_start)) {
			$data['installation_data']->installation_start = date('d-m-Y', strtotime($data['installation_data']->installation_start));
		}
        if (!empty($data['installation_data']->installation_finished)) {
			$data['installation_data']->installation_finished = date('d-m-Y', strtotime($data['installation_data']->installation_finished));
		}
        if (!empty($data['installation_data']->run_start_date)) {
			$data['installation_data']->run_start_date = date('d-m-Y', strtotime($data['installation_data']->run_start_date));
		}
        if (!empty($data['installation_data']->production_start)) {
			$data['installation_data']->production_start = date('d-m-Y', strtotime($data['installation_data']->production_start));
		}
        $data['installation_data']->water_tank_chiller_system_label = ($data['installation_data']->water_tank_chiller_system_label == 1) ? 'Water Tank' : 'Chiller System';
        $data['installation_data']->water_plumbing_chiller_conn_label = ($data['installation_data']->water_plumbing_chiller_conn_label == 1) ? 'Water Plumbing' : 'Chiller Conn';
        $data['installation_data']->tractor_loader = ($data['installation_data']->tractor_loader == 1) ? 'Yes' : 'No';
		
		$where = array('testing_report_id' => $testing_report_id);
		$testing_report_data = $this->crud->get_row_by_id('testing_report', $where);
		$data['testing_report_data'] = $testing_report_data[0];
		if (!empty($data['testing_report_data']->testing_report_date)) {
			$data['testing_report_data']->testing_report_date = date('d-m-Y', strtotime($data['testing_report_data']->testing_report_date));
		}
		if (!empty($data['testing_report_data']->assembling_equipment)) {
			$data['testing_report_data']->assembling_equipment = date('d-m-Y', strtotime($data['testing_report_data']->assembling_equipment));
		}
		if (!empty($data['testing_report_data']->equipment_testing_date)) {
			$data['testing_report_data']->equipment_testing_date = date('d-m-Y', strtotime($data['testing_report_data']->equipment_testing_date));
		}
		if (!empty($data['testing_report_data']->equipment_testing_date)) {
			$data['testing_report_data']->equipment_testing_date = date('d-m-Y', strtotime($data['testing_report_data']->equipment_testing_date));
		}
		if (!empty($data['testing_report_data']->delivery_of_equipment)) {
			$data['testing_report_data']->delivery_of_equipment = date('d-m-Y', strtotime($data['testing_report_data']->delivery_of_equipment));
		}
		$data['testing_report_data']->quality_checked_by_value = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->quality_checked_by);
		$data['testing_report_data']->equipment_inspection_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->equipment_inspection_by);
		$data['testing_report_data']->equipment_testing_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->equipment_testing_by);
		$data['testing_report_data']->our_site_incharge_person_value = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $data['testing_report_data']->our_site_incharge_person);

		$challan_sales_order_id = $this->crud->get_id_by_val('challans', 'sales_order_id', 'id', $challan_id);
		$challan_proforma_invoice_id = $this->crud->get_id_by_val('challans', 'proforma_invoice_id', 'id', $challan_id);
        if(!empty($challan_sales_order_id ) && $challan_sales_order_id != 0){
            $sales_order_id = $this->crud->get_id_by_val('sales_order', 'id', 'id', $challan_sales_order_id);
            if(empty($sales_order_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else if (!empty($challan_proforma_invoice_id ) && $challan_proforma_invoice_id != 0){
            $proforma_invoice_id = $this->crud->get_id_by_val('proforma_invoices', 'id', 'id', $challan_proforma_invoice_id);
            if(empty($proforma_invoice_id)){
                $challan_data = $this->crud->get_only_challan_data($challan_id);
            } else {
                $challan_data = $this->crud->get_challan($challan_id, $challan_sales_order_id);
            }
        } else {
            $challan_data = $this->crud->get_only_challan_data($challan_id);
        }
		if (empty($challan_sales_order_id)) {
			$challan_data->proforma_invoice_no = $challan_data->sales_order_no;
			unset($challan_data->sales_order_no);
			$challan_data->proforma_invoice_date = date('d-m-Y', strtotime($challan_data->sales_order_date));
			unset($challan_data->sales_order_date);
		}
		if (!empty($challan_data->po_date)) {
			$challan_data->po_date = date('d-m-Y', strtotime($challan_data->po_date));
		}
		if (!empty($challan_data->sales_order_date)) {
			$challan_data->sales_order_date = date('d-m-Y', strtotime($challan_data->sales_order_date));
		}
		if (!empty($challan_data->challan_date)) {
			$challan_data->challan_date = date('d-m-Y', strtotime($challan_data->challan_date));
		}
        
        $challan_data->invoice_no = '';
        $challan_data->invoice_date = '';
        $invoice_data = $this->crud->get_row_by_id('invoices', array('challan_id' => $challan_id));
		if(!empty($invoice_data)){
			$challan_data->invoice_no = $invoice_data[0]->invoice_no;
			$challan_data->invoice_date = date('d-m-Y',strtotime($invoice_data[0]->invoice_date));
		}
        $challan_data->quotation_date = '';
        $challan_data->quotation_date = $this->crud->get_id_by_val('quotations', 'quotation_date', 'quotation_no', $challan_data->quotation_no);
        if (!empty($challan_data->quotation_date)) {
			$challan_data->quotation_date = date('d-m-Y', strtotime($challan_data->quotation_date));
		}
        $data['challan_data'] = $challan_data;

		$where = array('challan_id' => $challan_id);
		$challan_item_data = $this->crud->get_row_by_id('challan_items', $where);
		$data['challan_item_data'] = $challan_item_data[0];
		if (!empty($data['challan_item_data']->lr_date)) {
			$data['challan_item_data']->lr_date = date('d-m-Y', strtotime($data['challan_item_data']->lr_date));
		}
		$data['challan_item_data']->item_extra_accessories = $this->crud->get_column_value_by_id('item_extra_accessories', 'item_extra_accessories_label', array('item_extra_accessories_id' => $data['challan_item_data']->item_extra_accessories_id));
		$data['party_contact_person'] = $this->crud->get_contact_person_by_party($challan_data->sales_to_party_id);

		$where = array('challan_item_id' => $data['challan_item_data']->id);
		$motor_data = $this->crud->get_row_by_id('challan_items_motor_details', $where);
		//pre_die($motor_data);
		foreach ($motor_data as $motor) {
			$motor->motor_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $motor->motor_user);
			$motor->motor_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $motor->motor_make);
			$motor->motor_hp = $this->crud->get_id_by_val('challan_item_hp', 'hp_name', 'id', $motor->motor_hp);
			$motor->motor_kw = $this->crud->get_id_by_val('challan_item_kw', 'kw_name', 'id', $motor->motor_kw);
			$motor->motor_frequency = $this->crud->get_id_by_val('challan_item_frequency', 'frequency_name', 'id', $motor->motor_frequency);
			$motor->motor_rpm = $this->crud->get_id_by_val('challan_item_rpm', 'rpm_name', 'id', $motor->motor_rpm);
			$motor->motor_volts_cycles = $this->crud->get_id_by_val('challan_item_volts_cycles', 'volts_cycles_name', 'id', $motor->motor_volts_cycles);
		}
		$data['motor_data'] = $motor_data;

		$gearbox_data = $this->crud->get_row_by_id('challan_items_gearbox_details', $where);
		foreach ($gearbox_data as $gearbox) {
			$gearbox->gearbox_user = $this->crud->get_id_by_val('challan_item_user', 'user_name', 'id', $gearbox->gearbox_user);
			$gearbox->gearbox_make = $this->crud->get_id_by_val('challan_item_make', 'make_name', 'id', $gearbox->gearbox_make);
			$gearbox->gearbox_gear_type = $this->crud->get_id_by_val('challan_item_gear_type', 'gear_type_name', 'id', $gearbox->gearbox_gear_type);
			$gearbox->gearbox_model = $this->crud->get_id_by_val('challan_item_model', 'model_name', 'id', $gearbox->gearbox_model);
			$gearbox->gearbox_ratio = $this->crud->get_id_by_val('challan_item_ratio', 'ratio_name', 'id', $gearbox->gearbox_ratio);
		}
		$data['gearbox_data'] = $gearbox_data;

		if (!empty($data['challan_data']->kind_attn_id)) {
			$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['challan_data']->kind_attn_id));
            if(!empty($data['contact_person'])){
                $data['contact_person'] = $data['contact_person'][0];
                $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                $data['contact_person']->designation = '';
                if (!empty($designation)) {
                    $data['contact_person']->designation = $designation[0]->designation;
                }
                $data['contact_person']->department = '';
                if (!empty($department)) {
                    $data['contact_person']->department = $department[0]->department;
                }
            }
		}
        return $data;
    }
    
	function add($complain_id = '') {
        $data = array();
		if (!empty($complain_id)) {
			if ($this->app_model->have_access_role(COMPLAIN_MODULE_ID, "edit")) {
				$complain_data = $this->crud->get_row_by_id('complain', array('complain_id' => $complain_id));
				if (empty($complain_data)) {
					redirect("/complain/complain_list/");
				}
				$complain_data = $complain_data[0];
				$complain_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->created_by));
				$complain_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $complain_data->created_by));
				$complain_data->created_at = substr($complain_data->created_at, 8, 2) . '-' . substr($complain_data->created_at, 5, 2) . '-' . substr($complain_data->created_at, 0, 4);
				$complain_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->updated_by));
				$complain_data->updated_at = substr($complain_data->updated_at, 8, 2) . '-' . substr($complain_data->updated_at, 5, 2) . '-' . substr($complain_data->updated_at, 0, 4);
				
				$data['complain_data'] = $complain_data;
                
                $data['complain_items_data'] = $this->crud->get_row_by_id('complain_items', array('complain_id' => $complain_id));
                
				$where = array('complain_id' => $data['complain_data']->complain_id);
				$complain_type_details = $this->crud->get_row_by_id('complain_type_details', $where);
				foreach ($complain_type_details as $key => $complain_type) {
					$complain_type_details[$key]->complain_type_date = (!empty(strtotime($complain_type->complain_type_date))) ? date("d-m-Y", strtotime($complain_type->complain_type_date)) : null;
				}
				$data['complain_type_details'] = json_encode($complain_type_details);
				
				//echo '<pre>';print_r($data); exit;
				set_page('service/complain/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
            if ($this->applib->have_access_role(COMPLAIN_MODULE_ID, "add")) {
				set_page('service/complain/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function save_complain() {
        $return = array();
		$post_data = $this->input->post();
//        echo '<pre>';print_r($post_data); exit;
        $send_sms = 0;
        if(isset($post_data['complain_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['complain_data']['send_sms']);
        }
        
		/* --------- Convert Date as Mysql Format ------------- */
		$post_data['complain_data']['complain_date'] = (!empty(strtotime($post_data['complain_data']['complain_date']))) ? date("Y-m-d", strtotime($post_data['complain_data']['complain_date'])) : null;
        $post_data['complain_data']['party_id'] = (isset($post_data['complain_data']['party_id'])) ? $post_data['complain_data']['party_id'] : null;
        $post_data['complain_data']['received_by'] = (isset($post_data['complain_data']['received_by'])) ? $post_data['complain_data']['received_by'] : null;
        $post_data['complain_data']['technical_support_by'] = (isset($post_data['complain_data']['technical_support_by'])) ? $post_data['complain_data']['technical_support_by'] : null;
        $post_data['complain_data']['service_type'] = (isset($post_data['complain_data']['service_type'])) ? $post_data['complain_data']['service_type'] : null;
        $post_data['complain_data']['service_charge'] = (isset($post_data['complain_data']['service_charge']) && !empty($post_data['complain_data']['service_charge'])) ? $post_data['complain_data']['service_charge'] : null;

		if (isset($_POST['complain_data']['complain_id']) && !empty($_POST['complain_data']['complain_id'])) {
			$complain_id = $_POST['complain_data']['complain_id'];
			if (isset($post_data['complain_data']['complain_id'])) {
				unset($post_data['complain_data']['complain_id']);
			}

			$post_data['complain_data']['updated_at'] = $this->now_time;
			$post_data['complain_data']['updated_by'] = $this->staff_id;
			$this->db->where('complain_id', $complain_id);
			$result = $this->db->update('complain', $post_data['complain_data']);
			if ($result) {
				$return['success'] = "Updated";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Complain Updated Successfully');

				$val_inc = 0;
				if (!empty($post_data['complain_type_details']['complain_type_date'])) {
					if (isset($post_data['deleted_complain_type_id'])) {
						$this->db->where_in('complain_type_id', $post_data['deleted_complain_type_id']);
						$this->db->delete('complain_type_details');
					}
					foreach ($post_data['complain_type_details']['complain_type_date'] as $complain_type_date) {
						$complain_type_date = (!empty(strtotime($complain_type_date))) ? date("Y-m-d", strtotime($complain_type_date)) : null;
						$complain_type_details = array();
						if (!empty($post_data['complain_type_details']['complain_type_id'][$val_inc])) {
							$complain_type_details['complain_id'] = $complain_id;
							$complain_type_details['complain_type_date'] = $complain_type_date;
							$complain_type_details['type_of_complain'] = $post_data['complain_type_details']['type_of_complain'][$val_inc];
							$complain_type_details['complain_box'] = $post_data['complain_type_details']['complain_box'][$val_inc];
							$complain_type_details['updated_at'] = $this->now_time;
							$complain_type_details['updated_by'] = $this->staff_id;
							$this->db->where('complain_type_id', $post_data['complain_type_details']['complain_type_id'][$val_inc]);
							$this->db->update('complain_type_details', $complain_type_details);
						} else {
							$complain_type_details['complain_id'] = $complain_id;
							$complain_type_details['complain_type_date'] = $complain_type_date;
							$complain_type_details['type_of_complain'] = $post_data['complain_type_details']['type_of_complain'][$val_inc];
							$complain_type_details['complain_box'] = $post_data['complain_type_details']['complain_box'][$val_inc];
							$complain_type_details['created_at'] = $this->now_time;
							$complain_type_details['updated_at'] = $this->now_time;
							$complain_type_details['created_by'] = $this->staff_id;
							$complain_type_details['updated_by'] = $this->staff_id;
							$this->db->insert('complain_type_details', $complain_type_details);
						}
						$val_inc++;
					}
                }
                    
                $this->db->where_in('complain_id', $complain_id);
                $this->db->delete('complain_items');
                foreach ($post_data['complain_items'] as $complain_items) {
                    $complain_item_details = array();
                    $complain_item_details['complain_id'] = $complain_id;
                    $complain_item_details['installation_id'] = $complain_items['installation_id'];
                    $complain_item_details['testing_report_id'] = $complain_items['testing_report_id'];
                    $complain_item_details['challan_id'] = $complain_items['challan_id'];
                    $installation_id = $complain_items['installation_id'];
                    if(isset($post_data['equipment_testing_data'])){
                        $complain_item_details['c_testing_normal_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_normal_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_normal_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_normal_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_testing_full_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_full_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_full_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_full_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_temp_oli'] = $post_data['temperature_parameters_data']['c_temp_oli'][$installation_id];
                        $complain_item_details['c_temp_eqipment'] = $post_data['temperature_parameters_data']['c_temp_eqipment'][$installation_id];
                        $complain_item_details['c_temp_head'] = $post_data['temperature_parameters_data']['c_temp_head'][$installation_id];
                        $complain_item_details['c_temp_kiln'] = $post_data['temperature_parameters_data']['c_temp_kiln'][$installation_id];
                        $complain_item_details['c_temp_hot_air'] = $post_data['temperature_parameters_data']['c_temp_hot_air'][$installation_id];
                        $complain_item_details['c_temp_weather'] = $post_data['temperature_parameters_data']['c_temp_weather'][$installation_id];
                    }
                    $complain_item_details['created_at'] = $this->now_time;
                    $complain_item_details['updated_at'] = $this->now_time;
                    $complain_item_details['created_by'] = $this->staff_id;
                    $complain_item_details['updated_by'] = $this->staff_id;
                    $this->db->insert('complain_items', $complain_item_details);
                }
			}
		} else {
			$dataToInsert = $post_data['complain_data'];
			
            $max_complain_no = $this->crud->get_max_number('complain','complain_no');
			$dataToInsert['complain_no'] = $max_complain_no->complain_no + 1;
			$dataToInsert['complain_no_year'] = $this->applib->get_complain_no($dataToInsert['complain_no'], $post_data['complain_data']['complain_date']);
			
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
            $result = $this->db->insert('complain', $dataToInsert);
			$complain_id = $this->db->insert_id();
            if ($result) {
				$return['success'] = "Added";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Complain Added Successfully');
//				$this->crud->update('testing_report', array('is_installation_created' => 1), array('testing_report_id' => $testing_report_id));

				$val_inc = 0;
				if (!empty($post_data['complain_type_details']['complain_type_date'])) {
					foreach ($post_data['complain_type_details']['complain_type_date'] as $complain_type_date) {
						$complain_type_date = (!empty(strtotime($complain_type_date))) ? date("Y-m-d", strtotime($complain_type_date)) : null;
						$complain_type_details = array();
						$complain_type_details['complain_id'] = $complain_id;
                        $complain_type_details['complain_type_date'] = $complain_type_date;
                        $complain_type_details['type_of_complain'] = $post_data['complain_type_details']['type_of_complain'][$val_inc];
                        $complain_type_details['complain_box'] = $post_data['complain_type_details']['complain_box'][$val_inc];
                        $complain_type_details['created_at'] = $this->now_time;
                        $complain_type_details['updated_at'] = $this->now_time;
                        $complain_type_details['created_by'] = $this->staff_id;
                        $complain_type_details['updated_by'] = $this->staff_id;
						$this->db->insert('complain_type_details', $complain_type_details);
						$val_inc++;
					}
				}
                foreach ($post_data['complain_items'] as $complain_items) {
                    $this->crud->update('installation',array('is_complain_created'=> 1),array('installation_id'=>$complain_items['installation_id']));
                    $complain_item_details = array();
                    $complain_item_details['complain_id'] = $complain_id;
                    $complain_item_details['installation_id'] = $complain_items['installation_id'];
                    $complain_item_details['testing_report_id'] = $complain_items['testing_report_id'];
                    $complain_item_details['challan_id'] = $complain_items['challan_id'];
                    if(isset($post_data['equipment_testing_data'])){
                        $installation_id = $complain_items['installation_id'];
                        $complain_item_details['c_testing_normal_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_normal_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_normal_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_normal_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_testing_full_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_full_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_full_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_full_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_temp_oli'] = $post_data['temperature_parameters_data']['c_temp_oli'][$installation_id];
                        $complain_item_details['c_temp_eqipment'] = $post_data['temperature_parameters_data']['c_temp_eqipment'][$installation_id];
                        $complain_item_details['c_temp_head'] = $post_data['temperature_parameters_data']['c_temp_head'][$installation_id];
                        $complain_item_details['c_temp_kiln'] = $post_data['temperature_parameters_data']['c_temp_kiln'][$installation_id];
                        $complain_item_details['c_temp_hot_air'] = $post_data['temperature_parameters_data']['c_temp_hot_air'][$installation_id];
                        $complain_item_details['c_temp_weather'] = $post_data['temperature_parameters_data']['c_temp_weather'][$installation_id];
                    }
                    $complain_item_details['created_at'] = $this->now_time;
                    $complain_item_details['updated_at'] = $this->now_time;
                    $complain_item_details['created_by'] = $this->staff_id;
                    $complain_item_details['updated_by'] = $this->staff_id;
                    $this->db->insert('complain_items', $complain_item_details);
                }
			}
		}
        
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['complain_data']['party_id']);
            $this->applib->send_sms('complain', $complain_id, $party_phone_no, SEND_COMPLAIN_SMS_ID);
        }
        
		print json_encode($return);
		exit;
	}

	function complain_list() {
		if ($this->app_model->have_access_role(COMPLAIN_MODULE_ID, "view")) {
			set_page('service/complain/complain_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function complain_datatable() {
		$config['table'] = 'complain com';
		$config['select'] = 'com.complain_id,com.complain_no,com.complain_no_year,com.complain_date,com.is_resolve_complain_created,p.party_name,p.phone_no,p.email_id,city.city,state.state,country.country';
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = com.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

		$config['column_order'] = array(null, 'com.complain_no', 'com.complain_date', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('com.complain_no_year', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['order'] = array('com.complain_id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '1');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
		} elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
        $role_delete = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "edit");
		foreach ($list as $complain) {
			$row = array();
			$action = '';
			if ($role_edit) {
				$action .= '<a href="' . base_url('complain/add/' . $complain->complain_id) . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			}
			if ($role_delete) {
                if ($complain->is_resolve_complain_created != 1) {
                    $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('complain/delete_complain/' . $complain->complain_id) . '"><i class="fa fa-trash"></i></a>';				
                }
			}
            $complain_items = $this->crud->get_row_by_id('complain_items', array('complain_id' => $complain->complain_id));
            if (count($complain_items) > 1) {
                $action .= ' | <a href="#" class="print_button btn-primary btn-xs show_itemprint_modal" id="' . $complain->complain_id . '"><i class="fa fa-print"></i> Print</a>';
            } else {
                $action .= ' | <a href="' . base_url('complain/received_complain/' . $complain->complain_id) . '" target="_blank" class="btn-primary btn-xs" title="Received Complain Print"><i class="fa fa-print"></i> Print</a>';
            }
			$action .= ' | <a href="' . base_url('complain/received_complain_email/' . $complain->complain_id) . '" target="_blank" class="" title="Email">Email</a>';
			$row[] = $action;
			$row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" >' . $complain->complain_no_year . '</a>';
            $complain_date = (!empty(strtotime($complain->complain_date))) ? date("d-m-Y", strtotime($complain->complain_date)) : '';
			$row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" >' . $complain_date . '</a>';
			$row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" >' . $complain->party_name . '</a>';
            $phone_no = substr($complain->phone_no,0,14);
            $complain->phone_no = str_replace(',', ', ', $complain->phone_no);
            $row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$complain->phone_no.'" >'.$phone_no.'</a>';
            $email_id = substr($complain->email_id,0,14);
            $complain->email_id = str_replace(',', ', ', $complain->email_id);
            $row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$complain->email_id.'" >'.$email_id.'</a>';
			$row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" >' . $complain->city . '</a>';
			$row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" >' . $complain->state . '</a>';
			$row[] = '<a href="' . base_url('complain/add/' . $complain->complain_id . '?view') . '" >' . $complain->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
    
    function feed_complain_items($complain_id){
        $this->db->select('comi.*, ci.item_name, i.installation_no, i.installation_no_year');
		$this->db->from('complain_items comi');
        $this->db->join('challan_items ci', 'ci.challan_id = comi.challan_id', 'left');
        $this->db->join('installation i', 'i.installation_id = comi.installation_id', 'left');
		$this->db->where('comi.complain_id', $complain_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$item_data = $query->result();
			echo json_encode(array('success' => true, 'message' => 'Item loaded successfully!', 'item_data' => $item_data));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}
	
    function delete_complain($complain_id) {
        $role_delete = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "delete");
		if ($role_delete) {

			//---Start Update Testing Report Table
//			$this->crud->update('testing_report', $testing_report_update_data, array('testing_report_id' => $testing_report_id));
			//---Ent Update Testing Report Table
            $complain_items = $this->crud->get_row_by_id('complain_items', array('complain_id' => $complain_id));
            foreach ($complain_items as $complain_item) {
                    $this->crud->update('installation',array('is_complain_created'=> 0),array('installation_id'=>$complain_item->installation_id));
            }
			$where_array = array("complain_id" => $complain_id);
			$this->crud->delete("complain", $where_array);
			$this->crud->delete("complain_type_details", $where_array);
			$this->crud->delete("complain_items", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
	function received_complain($complain_id){
		$this->print_pdf($complain_id, 'received');
	}

	function resolving_complain($resolve_complain_id){
		$this->print_pdf($resolve_complain_id, 'resolving');
	}

	function print_pdf($id, $for){
        $data = array();
        
        if($for == 'received'){
            $complain_id = $id;
        } elseif($for == 'resolving'){
            $resolve_complain_id = $id;
            $resolve_complain_data = $this->crud->get_row_by_id('resolve_complain', array('resolve_complain_id' => $resolve_complain_id));
            if (empty($resolve_complain_data)) {
                redirect("/complain/resolve_complain_list/");
            }
            $resolve_complain_data = $resolve_complain_data[0];
            $resolve_complain_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->created_by));
            $resolve_complain_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $resolve_complain_data->created_by));
            $resolve_complain_data->created_at = substr($resolve_complain_data->created_at, 8, 2) . '-' . substr($resolve_complain_data->created_at, 5, 2) . '-' . substr($resolve_complain_data->created_at, 0, 4);
            $resolve_complain_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->updated_by));
            $resolve_complain_data->updated_at = substr($resolve_complain_data->updated_at, 8, 2) . '-' . substr($resolve_complain_data->updated_at, 5, 2) . '-' . substr($resolve_complain_data->updated_at, 0, 4);
            
            $resolve_complain_data->resolve_complain_date = substr($resolve_complain_data->resolve_complain_date, 8, 2) . '-' . substr($resolve_complain_data->resolve_complain_date, 5, 2) . '-' . substr($resolve_complain_data->resolve_complain_date, 0, 4);
            $resolve_complain_data->rc_jk_technical_person = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->rc_jk_technical_person));
            $resolve_complain_data->rank_of_providing_service = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $resolve_complain_data->rank_of_providing_service));
            $resolve_complain_data->technician_of_jk_accompanied = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $resolve_complain_data->technician_of_jk_accompanied));
            $resolve_complain_data->rank_our_technician_kbs = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $resolve_complain_data->rank_our_technician_kbs));
            $data['resolve_complain_data'] = $resolve_complain_data;

            $where = array('resolve_complain_id' => $data['resolve_complain_data']->resolve_complain_id);
            $reason_customer_complain_details = $this->crud->get_row_by_id('reason_customer_complain_details', $where);
            foreach ($reason_customer_complain_details as $key => $reason_customer_complain) {
                $reason_customer_complain_details[$key]->reason_checking_person = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $reason_customer_complain->reason_checking_person));
                $reason_customer_complain_details[$key]->complain_start_date = (!empty(strtotime($reason_customer_complain->complain_start_date))) ? date("d-m-Y", strtotime($reason_customer_complain->complain_start_date)) : null;
                $reason_customer_complain_details[$key]->complain_end_date = (!empty(strtotime($reason_customer_complain->complain_end_date))) ? date("d-m-Y", strtotime($reason_customer_complain->complain_end_date)) : null;
            }
            $data['reason_customer_complain_details'] = $reason_customer_complain_details;

            $resolving_complain_details = $this->crud->get_row_by_id('resolving_complain_details', $where);
            foreach ($resolving_complain_details as $key => $resolving_complain) {
                $resolving_complain_details[$key]->resolving_person = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolving_complain->resolving_person));
                $resolving_complain_details[$key]->resolving_start_date = (!empty(strtotime($resolving_complain->resolving_start_date))) ? date("d-m-Y", strtotime($resolving_complain->resolving_start_date)) : null;
                $resolving_complain_details[$key]->resolving_end_date = (!empty(strtotime($resolving_complain->resolving_end_date))) ? date("d-m-Y", strtotime($resolving_complain->resolving_end_date)) : null;
            }
            $data['resolving_complain_details'] = $resolving_complain_details;
            
            $complain_id = $resolve_complain_data->complain_id;
        }
        
		$company_details = $this->applib->get_company_detail();
        $select_item = (isset($_GET['item']) && $_GET['item']) ? $_GET['item'] : '';
        
        $complain_data = $this->crud->get_row_by_id('complain', array('complain_id' => $complain_id));
        $complain_data = $complain_data[0];
        $complain_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->created_by));
        $complain_data->created_at = substr($complain_data->created_at, 8, 2) . '-' . substr($complain_data->created_at, 5, 2) . '-' . substr($complain_data->created_at, 0, 4);
        $complain_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->updated_by));
        $complain_data->updated_at = substr($complain_data->updated_at, 8, 2) . '-' . substr($complain_data->updated_at, 5, 2) . '-' . substr($complain_data->updated_at, 0, 4);
        $complain_data->complain_date = (!empty(strtotime($complain_data->complain_date))) ? date("d-m-Y", strtotime($complain_data->complain_date)) : null;
        $complain_data->received_by = $this->crud->get_column_value_by_id('reference', 'reference', array('id' => $complain_data->received_by));
        $complain_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $complain_data->created_by));
        $complain_data->technical_support_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->technical_support_by));
        $complain_data->service_type = $this->crud->get_column_value_by_id('service_type', 'label', array('id' => $complain_data->service_type));
        
        $where = array('complain_id' => $complain_id);
        $complain_type_details = $this->crud->get_row_by_id('complain_type_details', $where);
        foreach ($complain_type_details as $key => $complain_type) {
            $complain_type_details[$key]->complain_type_date = (!empty(strtotime($complain_type->complain_type_date))) ? date("d-m-Y", strtotime($complain_type->complain_type_date)) : null;
        }
        if($for == 'received'){
            $complain_items = $this->crud->get_row_by_id('complain_items', $where);
        }elseif($for == 'resolving'){
            $where = array('resolve_complain_id' => $resolve_complain_id);
            $complain_items = $this->crud->get_row_by_id('resolve_complain_items', $where);
        }
        
		$this->load->library('m_pdf');
		$margin_left = $company_details['margin_left'];
        $margin_right = $company_details['margin_right'];
        $margin_top = $company_details['margin_top'];
        $margin_bottom = $company_details['margin_bottom'];
		foreach ($complain_items as $complain_item_data) {
			if ($select_item && $complain_item_data->id != $select_item) {
				continue;
			}
            $cid_where = array('complain_id' => $complain_id, 'installation_id' => $complain_item_data->installation_id, 'testing_report_id' => $complain_item_data->testing_report_id, 'challan_id' => $complain_item_data->challan_id);
            $complain_item_et_tp = $this->crud->get_row_by_id('complain_items', $cid_where);
            $cti_data = $this->get_challan_testing_report_installation_details($complain_item_data->challan_id, $complain_item_data->testing_report_id, $complain_item_data->installation_id);
            $return_data = array_merge($cti_data,$data);
            $return_data['company_details'] = $company_details;
            $return_data['complain_data'] = $complain_data;
            $return_data['complain_type_details'] = $complain_type_details;
            $return_data['complain_item_data'] = $complain_item_data;
            $return_data['complain_item_et_tp'] = $complain_item_et_tp[0];
            $return_data['party_data'] = (array) $this->crud->load_party_with_cnt_person_3($complain_data->party_id);
            
			$param = "'utf-8','A4'";
			$pdf = $this->m_pdf->load($param);
			$pdf->use_kwt = true; 
			$pdf->AddPage(
				'P', //orientation
				'', //type
				'', //resetpagenum
				'', //pagenumstyle
				'', //suppress
				$margin_left, //margin-left
				$margin_right, //margin-right
				$margin_top, //margin-top
				$margin_bottom, //margin-bottom
				0, //margin-header
				0 //margin-footer
			);
//            echo '<pre>'; print_r($data); exit;
            if($for == 'received'){
                $html = $this->load->view('service/complain/received_print', $return_data, true);
            }elseif($for == 'resolving'){
                $html = $this->load->view('service/complain/resolving_print', $return_data, true);
            }
			$pdf->WriteHTML($html);
			if($for == 'received'){
				$pdfFilePath = "complain_received.pdf";
			}elseif($for == 'resolving'){
				$pdfFilePath = "complain_resolved.pdf";
			}
			$pdf->Output($pdfFilePath, "I");

		}
	}
    
    function received_complain_email($complain_id){
		$this->complain_email($complain_id, 'received');
	}

	function resolving_complain_email($resolve_complain_id){
		$this->complain_email($resolve_complain_id, 'resolving');
	}
    
    function complain_email($id, $for){
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$data = array();
        
        if($for == 'received'){
            $complain_id = $id;
        } elseif($for == 'resolving'){
            $resolve_complain_id = $id;
            $resolve_complain_data = $this->crud->get_row_by_id('resolve_complain', array('resolve_complain_id' => $resolve_complain_id));
            if (empty($resolve_complain_data)) {
                redirect("/complain/resolve_complain_list/");
            }
            $resolve_complain_data = $resolve_complain_data[0];
            $resolve_complain_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->created_by));
            $resolve_complain_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $resolve_complain_data->created_by));
            $resolve_complain_data->created_at = substr($resolve_complain_data->created_at, 8, 2) . '-' . substr($resolve_complain_data->created_at, 5, 2) . '-' . substr($resolve_complain_data->created_at, 0, 4);
            $resolve_complain_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->updated_by));
            $resolve_complain_data->updated_at = substr($resolve_complain_data->updated_at, 8, 2) . '-' . substr($resolve_complain_data->updated_at, 5, 2) . '-' . substr($resolve_complain_data->updated_at, 0, 4);

            $resolve_complain_data->resolve_complain_date = substr($resolve_complain_data->resolve_complain_date, 8, 2) . '-' . substr($resolve_complain_data->resolve_complain_date, 5, 2) . '-' . substr($resolve_complain_data->resolve_complain_date, 0, 4);
            $resolve_complain_data->rc_jk_technical_person = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->rc_jk_technical_person));
            $resolve_complain_data->rank_of_providing_service = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $resolve_complain_data->rank_of_providing_service));
            $resolve_complain_data->technician_of_jk_accompanied = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $resolve_complain_data->technician_of_jk_accompanied));
            $resolve_complain_data->rank_our_technician_kbs = $this->crud->get_column_value_by_id('customer_feedback', 'label', array('id' => $resolve_complain_data->rank_our_technician_kbs));
            $data['resolve_complain_data'] = $resolve_complain_data;

            $where = array('resolve_complain_id' => $data['resolve_complain_data']->resolve_complain_id);
            $reason_customer_complain_details = $this->crud->get_row_by_id('reason_customer_complain_details', $where);
            foreach ($reason_customer_complain_details as $key => $reason_customer_complain) {
                $reason_customer_complain_details[$key]->reason_checking_person = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $reason_customer_complain->reason_checking_person));
                $reason_customer_complain_details[$key]->complain_start_date = (!empty(strtotime($reason_customer_complain->complain_start_date))) ? date("d-m-Y", strtotime($reason_customer_complain->complain_start_date)) : null;
                $reason_customer_complain_details[$key]->complain_end_date = (!empty(strtotime($reason_customer_complain->complain_end_date))) ? date("d-m-Y", strtotime($reason_customer_complain->complain_end_date)) : null;
            }
            $data['reason_customer_complain_details'] = $reason_customer_complain_details;

            $resolving_complain_details = $this->crud->get_row_by_id('resolving_complain_details', $where);
            foreach ($resolving_complain_details as $key => $resolving_complain) {
                $resolving_complain_details[$key]->resolving_person = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolving_complain->resolving_person));
                $resolving_complain_details[$key]->resolving_start_date = (!empty(strtotime($resolving_complain->resolving_start_date))) ? date("d-m-Y", strtotime($resolving_complain->resolving_start_date)) : null;
                $resolving_complain_details[$key]->resolving_end_date = (!empty(strtotime($resolving_complain->resolving_end_date))) ? date("d-m-Y", strtotime($resolving_complain->resolving_end_date)) : null;
            }
            $data['resolving_complain_details'] = $resolving_complain_details;
            
            $complain_id = $resolve_complain_data->complain_id;
        }
        
		$company_details = $this->applib->get_company_detail();
        $select_item = (isset($_GET['item']) && $_GET['item']) ? $_GET['item'] : '';
        
        $complain_data = $this->crud->get_row_by_id('complain', array('complain_id' => $complain_id));
        $complain_data = $complain_data[0];
        $complain_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->created_by));
        $complain_data->created_at = substr($complain_data->created_at, 8, 2) . '-' . substr($complain_data->created_at, 5, 2) . '-' . substr($complain_data->created_at, 0, 4);
        $complain_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->updated_by));
        $complain_data->updated_at = substr($complain_data->updated_at, 8, 2) . '-' . substr($complain_data->updated_at, 5, 2) . '-' . substr($complain_data->updated_at, 0, 4);
        $complain_data->complain_date = (!empty(strtotime($complain_data->complain_date))) ? date("d-m-Y", strtotime($complain_data->complain_date)) : null;
        $complain_data->received_by = $this->crud->get_column_value_by_id('reference', 'reference', array('id' => $complain_data->received_by));
        $complain_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $complain_data->created_by));
        $complain_data->technical_support_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->technical_support_by));
        $complain_data->service_type = $this->crud->get_column_value_by_id('service_type', 'label', array('id' => $complain_data->service_type));
        
        $where = array('complain_id' => $complain_id);
        $complain_type_details = $this->crud->get_row_by_id('complain_type_details', $where);
        foreach ($complain_type_details as $key => $complain_type) {
            $complain_type_details[$key]->complain_type_date = (!empty(strtotime($complain_type->complain_type_date))) ? date("d-m-Y", strtotime($complain_type->complain_type_date)) : null;
        }
        if($for == 'received'){
            $complain_items = $this->crud->get_row_by_id('complain_items', $where);
        }elseif($for == 'resolving'){
            $where = array('resolve_complain_id' => $resolve_complain_id);
            $complain_items = $this->crud->get_row_by_id('resolve_complain_items', $where);
        }
        
		$this->load->library('m_pdf');
		$margin_left = $company_details['margin_left'];
        $margin_right = $company_details['margin_right'];
        $margin_top = $company_details['margin_top'];
        $margin_bottom = $company_details['margin_bottom'];
		foreach ($complain_items as $key => $complain_item_data) {
            $cid_where = array('complain_id' => $complain_id, 'installation_id' => $complain_item_data->installation_id, 'testing_report_id' => $complain_item_data->testing_report_id, 'challan_id' => $complain_item_data->challan_id);
            $complain_item_et_tp = $this->crud->get_row_by_id('complain_items', $cid_where);
            $cti_data = $this->get_challan_testing_report_installation_details($complain_item_data->challan_id, $complain_item_data->testing_report_id, $complain_item_data->installation_id);
            $return_data = array_merge($cti_data,$data);
            $return_data['company_details'] = $company_details;
            $return_data['complain_data'] = $complain_data;
            $return_data['complain_type_details'] = $complain_type_details;
            $return_data['complain_item_data'] = $complain_item_data;
            $return_data['complain_item_et_tp'] = $complain_item_et_tp[0];
            $return_data['party_data'] = (array) $this->crud->load_party_with_cnt_person_3($complain_data->party_id);
            
            $param = "'utf-8','A4'";
			$pdf = $this->m_pdf->load($param);
			$pdf->use_kwt = true; 
			$pdf->AddPage(
				'P', //orientation
				'', //type
				'', //resetpagenum
				'', //pagenumstyle
				'', //suppress
				$margin_left, //margin-left
				$margin_right, //margin-right
				$margin_top, //margin-top
				$margin_bottom, //margin-bottom
				0, //margin-header
				0 //margin-footer
			);
//            echo '<pre>'; print_r($data); exit;
            if($for == 'received'){
                $html = $this->load->view('service/complain/received_print', $return_data, true);
            }elseif($for == 'resolving'){
                $html = $this->load->view('service/complain/resolving_print', $return_data, true);
            }
            $pdf->WriteHTML($html);
			if($for == 'received'){
				$pdfFilePath = "./uploads/complain_received_" . $key . ".pdf";
			} elseif($for == 'resolving'){
				$pdfFilePath = "./uploads/complain_resolved_" . $key . ".pdf";
			}
			$files_urls[] = $pdfFilePath;
			$pdf->Output($pdfFilePath, "F");
		}
        if($for == 'received'){
            $this->db->where('complain_id', $complain_id);
            $this->db->update('complain', array('pdf_url' => serialize($files_urls)));
            redirect(base_url() . 'mail-system3/document-mail/complain/' . $complain_id);
        } elseif($for == 'resolving'){
            $this->db->where('resolve_complain_id', $resolve_complain_id);
            $this->db->update('resolve_complain', array('pdf_url' => serialize($files_urls)));
            redirect(base_url() . 'mail-system3/document-mail/resolve_complain/' . $resolve_complain_id);
        }
	}
    
    function pick_receive_complain_datatable() {
        $config['table'] = 'complain com';
		$config['select'] = 'com.complain_id,com.complain_no,com.complain_no_year,com.complain_date,com.party_id,p.party_name,p.phone_no,p.email_id,city.city,state.state,country.country';
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = com.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

		$config['column_order'] = array(null, 'com.complain_no', 'com.complain_date', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('com.complain_no_year', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '1');
		$config['wheres'][] = array('column_name' => 'com.is_resolve_complain_created', 'column_value' => '0');
        $config['order'] = array('com.complain_id' => 'desc');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
		} elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
        $role_delete = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "edit");
		foreach ($list as $complain) {
			$row = array();
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->complain_no_year . '</a>';
            $complain_date = (!empty(strtotime($complain->complain_date))) ? date("d-m-Y", strtotime($complain->complain_date)) : '';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain_date . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->party_name . '</a>';
                        $phone_no = substr($complain->phone_no,0,14);
                        $complain->phone_no = str_replace(',', ', ', $complain->phone_no);
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" data-toggle="tooltip" data-placement="bottom" title="'. $complain->phone_no .'" >' . $phone_no . '</a>';
                        $email_id = substr($complain->email_id,0,14);
                        $complain->email_id = str_replace(',', ', ', $complain->email_id);
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" data-toggle="tooltip" data-placement="bottom" title="'. $complain->email_id .'" >' . $email_id . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->city . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->state . '</a>';
			$row[] = '<a href="javascript:void(0);" class="complain_row" data-complain_id="' . $complain->complain_id . '" data-party_id="' . $complain->party_id . '" >' . $complain->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

    function get_complain_details() {
        $data = array();
        $complain_id = $this->input->get_post("complain_id");
        $complain_data = $this->crud->get_row_by_id('complain', array('complain_id' => $complain_id));
        $complain_data = $complain_data[0];
        $complain_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->created_by));
        $complain_data->created_at = substr($complain_data->created_at, 8, 2) . '-' . substr($complain_data->created_at, 5, 2) . '-' . substr($complain_data->created_at, 0, 4);
        $complain_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->updated_by));
        $complain_data->updated_at = substr($complain_data->updated_at, 8, 2) . '-' . substr($complain_data->updated_at, 5, 2) . '-' . substr($complain_data->updated_at, 0, 4);
        $complain_data->complain_date = (!empty(strtotime($complain_data->complain_date))) ? date("d-m-Y", strtotime($complain_data->complain_date)) : null;
        $complain_data->received_by = $this->crud->get_column_value_by_id('reference', 'reference', array('id' => $complain_data->received_by));
        $complain_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $complain_data->created_by));
        $complain_data->technical_support_by = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $complain_data->technical_support_by));
        $complain_data->service_type = $this->crud->get_column_value_by_id('service_type', 'label', array('id' => $complain_data->service_type));
        $data['complain_data'] = $complain_data;
        
        $where = array('complain_id' => $complain_id);
        $data['complain_items'] = $this->crud->get_row_by_id('complain_items', $where);
        $complain_type_details = $this->crud->get_row_by_id('complain_type_details', $where);
        foreach ($complain_type_details as $key => $complain_type) {
            $complain_type_details[$key]->complain_type_date = (!empty(strtotime($complain_type->complain_type_date))) ? date("d-m-Y", strtotime($complain_type->complain_type_date)) : null;
        }
        $data['complain_type_details'] = $complain_type_details;
        echo json_encode($data);
		exit;
    }
    
    function resolve_complain($resolve_complain_id = '') {
        $data = array();
        $data['users'] = $this->crud->getFromSQL("SELECT * FROM `staff` WHERE `active`= 1 ORDER BY `staff`.`name` ASC");
		if (!empty($resolve_complain_id)) {
			if ($this->app_model->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "edit")) {
				$resolve_complain_data = $this->crud->get_row_by_id('resolve_complain', array('resolve_complain_id' => $resolve_complain_id));
				if (empty($resolve_complain_data)) {
					redirect("/complain/resolve_complain_list/");
				}
				$resolve_complain_data = $resolve_complain_data[0];
				$resolve_complain_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->created_by));
				$resolve_complain_data->created_by_contact = $this->crud->get_column_value_by_id('staff', 'contact_no', array('staff_id' => $resolve_complain_data->created_by));
				$resolve_complain_data->created_at = substr($resolve_complain_data->created_at, 8, 2) . '-' . substr($resolve_complain_data->created_at, 5, 2) . '-' . substr($resolve_complain_data->created_at, 0, 4);
				$resolve_complain_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $resolve_complain_data->updated_by));
				$resolve_complain_data->updated_at = substr($resolve_complain_data->updated_at, 8, 2) . '-' . substr($resolve_complain_data->updated_at, 5, 2) . '-' . substr($resolve_complain_data->updated_at, 0, 4);
				
				$data['resolve_complain_data'] = $resolve_complain_data;
                $data['resolve_complain_items_data'] = $this->crud->get_row_by_id('resolve_complain_items', array('resolve_complain_id' => $data['resolve_complain_data']->resolve_complain_id));
                
                $where = array('resolve_complain_id' => $data['resolve_complain_data']->resolve_complain_id);
				$reason_customer_complain_details = $this->crud->get_row_by_id('reason_customer_complain_details', $where);
				foreach ($reason_customer_complain_details as $key => $reason_customer_complain) {
					$reason_customer_complain_details[$key]->complain_start_date = (!empty(strtotime($reason_customer_complain->complain_start_date))) ? date("d-m-Y", strtotime($reason_customer_complain->complain_start_date)) : null;
					$reason_customer_complain_details[$key]->complain_end_date = (!empty(strtotime($reason_customer_complain->complain_end_date))) ? date("d-m-Y", strtotime($reason_customer_complain->complain_end_date)) : null;
				}
				$data['reason_customer_complain_details'] = json_encode($reason_customer_complain_details);
				
				$resolving_complain_details = $this->crud->get_row_by_id('resolving_complain_details', $where);
				foreach ($resolving_complain_details as $key => $resolving_complain) {
					$resolving_complain_details[$key]->resolving_start_date = (!empty(strtotime($resolving_complain->resolving_start_date))) ? date("d-m-Y", strtotime($resolving_complain->resolving_start_date)) : null;
					$resolving_complain_details[$key]->resolving_end_date = (!empty(strtotime($resolving_complain->resolving_end_date))) ? date("d-m-Y", strtotime($resolving_complain->resolving_end_date)) : null;
				}
				$data['resolving_complain_details'] = json_encode($resolving_complain_details);
				
				//echo '<pre>';print_r($data); exit;
				set_page('service/complain/resolve_complain', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
            if ($this->applib->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "add")) {
				set_page('service/complain/resolve_complain', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}
    
    function save_resolve_complain() {
        $return = array();
		$post_data = $this->input->post();
//        echo '<pre>';print_r($post_data); exit;
        $send_sms = 0;
        if(isset($post_data['resolve_complain_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['resolve_complain_data']['send_sms']);
        }
        
		/* --------- Convert Date as Mysql Format ------------- */
		$post_data['resolve_complain_data']['resolve_complain_date'] = (!empty(strtotime($post_data['resolve_complain_data']['resolve_complain_date']))) ? date("Y-m-d", strtotime($post_data['resolve_complain_data']['resolve_complain_date'])) : null;
        $post_data['resolve_complain_data']['party_id'] = (isset($post_data['resolve_complain_data']['party_id']) && !empty($post_data['resolve_complain_data']['party_id'])) ? $post_data['resolve_complain_data']['party_id'] : null;
        $post_data['resolve_complain_data']['rc_jk_technical_person'] = (isset($post_data['resolve_complain_data']['rc_jk_technical_person']) && !empty($post_data['resolve_complain_data']['rc_jk_technical_person'])) ? $post_data['resolve_complain_data']['rc_jk_technical_person'] : null;
        $post_data['resolve_complain_data']['rank_of_providing_service'] = (isset($post_data['resolve_complain_data']['rank_of_providing_service']) && !empty($post_data['resolve_complain_data']['rank_of_providing_service'])) ? $post_data['resolve_complain_data']['rank_of_providing_service'] : null;
        $post_data['resolve_complain_data']['technician_of_jk_accompanied'] = (isset($post_data['resolve_complain_data']['technician_of_jk_accompanied']) && !empty($post_data['resolve_complain_data']['technician_of_jk_accompanied'])) ? $post_data['resolve_complain_data']['technician_of_jk_accompanied'] : null;
        $post_data['resolve_complain_data']['rank_our_technician_kbs'] = (isset($post_data['resolve_complain_data']['rank_our_technician_kbs']) && !empty($post_data['resolve_complain_data']['rank_our_technician_kbs'])) ? $post_data['resolve_complain_data']['rank_our_technician_kbs'] : null;
        $post_data['resolve_complain_data']['num_of_equipments'] = (isset($post_data['resolve_complain_data']['num_of_equipments']) && !empty($post_data['resolve_complain_data']['num_of_equipments'])) ? $post_data['resolve_complain_data']['num_of_equipments'] : null;

		if (isset($_POST['resolve_complain_data']['resolve_complain_id']) && !empty($_POST['resolve_complain_data']['resolve_complain_id'])) {
			$resolve_complain_id = $_POST['resolve_complain_data']['resolve_complain_id'];
			if (isset($post_data['resolve_complain_data']['resolve_complain_id'])) {
				unset($post_data['resolve_complain_data']['resolve_complain_id']);
			}

			$post_data['resolve_complain_data']['updated_at'] = $this->now_time;
			$post_data['resolve_complain_data']['updated_by'] = $this->staff_id;
			$this->db->where('resolve_complain_id', $resolve_complain_id);
			$result = $this->db->update('resolve_complain', $post_data['resolve_complain_data']);
			if ($result) {
				$return['success'] = "Updated";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Resolve Complain Updated Successfully');

				$val_inc = 0;
				if (!empty($post_data['reason_customer_complain_details']['reason_for_customer_complain_text'])) {
					if (isset($post_data['deleted_reason_customer_complain_id'])) {
						$this->db->where_in('reason_customer_complain_id', $post_data['deleted_reason_customer_complain_id']);
						$this->db->delete('reason_customer_complain_details');
					}
					foreach ($post_data['reason_customer_complain_details']['reason_for_customer_complain_text'] as $reason_for_customer_complain_text) {
						$reason_customer_complain_details = array();
						if (!empty($post_data['reason_customer_complain_details']['reason_customer_complain_id'][$val_inc])) {
							$reason_customer_complain_details['resolve_complain_id'] = $resolve_complain_id;
							$reason_customer_complain_details['reason_for_customer_complain_text'] = $reason_for_customer_complain_text;
                            if(isset($post_data['reason_customer_complain_details']['reason_checking_person'][$val_inc])){
                                $reason_customer_complain_details['reason_checking_person'] = $post_data['reason_customer_complain_details']['reason_checking_person'][$val_inc];
                            }
							$reason_customer_complain_details['complain_start_date'] = (!empty(strtotime($post_data['reason_customer_complain_details']['complain_start_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['reason_customer_complain_details']['complain_start_date'][$val_inc])) : null;
							$reason_customer_complain_details['complain_end_date'] = (!empty(strtotime($post_data['reason_customer_complain_details']['complain_end_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['reason_customer_complain_details']['complain_end_date'][$val_inc])) : null;
							$reason_customer_complain_details['complain_hours'] = $post_data['reason_customer_complain_details']['complain_hours'][$val_inc];
							$reason_customer_complain_details['updated_at'] = $this->now_time;
							$reason_customer_complain_details['updated_by'] = $this->staff_id;
							$this->db->where('reason_customer_complain_id', $post_data['reason_customer_complain_details']['reason_customer_complain_id'][$val_inc]);
							$this->db->update('reason_customer_complain_details', $reason_customer_complain_details);
						} else {
							$reason_customer_complain_details['resolve_complain_id'] = $resolve_complain_id;
							$reason_customer_complain_details['reason_for_customer_complain_text'] = $reason_for_customer_complain_text;
                            if(isset($post_data['reason_customer_complain_details']['reason_checking_person'][$val_inc])){
                                $reason_customer_complain_details['reason_checking_person'] = $post_data['reason_customer_complain_details']['reason_checking_person'][$val_inc];
                            }
							$reason_customer_complain_details['complain_start_date'] = (!empty(strtotime($post_data['reason_customer_complain_details']['complain_start_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['reason_customer_complain_details']['complain_start_date'][$val_inc])) : null;
							$reason_customer_complain_details['complain_end_date'] = (!empty(strtotime($post_data['reason_customer_complain_details']['complain_end_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['reason_customer_complain_details']['complain_end_date'][$val_inc])) : null;
							$reason_customer_complain_details['complain_hours'] = $post_data['reason_customer_complain_details']['complain_hours'][$val_inc];
							$reason_customer_complain_details['created_at'] = $this->now_time;
							$reason_customer_complain_details['updated_at'] = $this->now_time;
							$reason_customer_complain_details['created_by'] = $this->staff_id;
							$reason_customer_complain_details['updated_by'] = $this->staff_id;
							$this->db->insert('reason_customer_complain_details', $reason_customer_complain_details);
						}
						$val_inc++;
					}
                }

				$val_inc = 0;
				if (!empty($post_data['resolving_complain_details']['resolving_complain_text'])) {
					if (isset($post_data['deleted_resolving_complain_id'])) {
						$this->db->where_in('resolving_complain_id', $post_data['deleted_resolving_complain_id']);
						$this->db->delete('resolving_complain_details');
					}
					foreach ($post_data['resolving_complain_details']['resolving_complain_text'] as $resolving_complain_text) {
						$resolving_complain_details = array();
						if (!empty($post_data['resolving_complain_details']['resolving_complain_id'][$val_inc])) {
							$resolving_complain_details['resolve_complain_id'] = $resolve_complain_id;
                            $resolving_complain_details['resolving_complain_text'] = $resolving_complain_text;
                            if(isset($post_data['resolving_complain_details']['resolving_person'][$val_inc])){
                                $resolving_complain_details['resolving_person'] = $post_data['resolving_complain_details']['resolving_person'][$val_inc];
                            }
                            $resolving_complain_details['resolving_start_date'] = (!empty(strtotime($post_data['resolving_complain_details']['resolving_start_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['resolving_complain_details']['resolving_start_date'][$val_inc])) : null;
                            $resolving_complain_details['resolving_end_date'] = (!empty(strtotime($post_data['resolving_complain_details']['resolving_end_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['resolving_complain_details']['resolving_end_date'][$val_inc])) : null;
                            $resolving_complain_details['resolving_hours'] = $post_data['resolving_complain_details']['resolving_hours'][$val_inc];
							$resolving_complain_details['updated_at'] = $this->now_time;
							$resolving_complain_details['updated_by'] = $this->staff_id;
							$this->db->where('resolving_complain_id', $post_data['resolving_complain_details']['resolving_complain_id'][$val_inc]);
							$this->db->update('resolving_complain_details', $resolving_complain_details);
						} else {
							$resolving_complain_details['resolve_complain_id'] = $resolve_complain_id;
                            $resolving_complain_details['resolving_complain_text'] = $resolving_complain_text;
                            if(isset($post_data['resolving_complain_details']['resolving_person'][$val_inc])){
                                $resolving_complain_details['resolving_person'] = $post_data['resolving_complain_details']['resolving_person'][$val_inc];
                            }
                            $resolving_complain_details['resolving_start_date'] = (!empty(strtotime($post_data['resolving_complain_details']['resolving_start_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['resolving_complain_details']['resolving_start_date'][$val_inc])) : null;
                            $resolving_complain_details['resolving_end_date'] = (!empty(strtotime($post_data['resolving_complain_details']['resolving_end_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['resolving_complain_details']['resolving_end_date'][$val_inc])) : null;
                            $resolving_complain_details['resolving_hours'] = $post_data['resolving_complain_details']['resolving_hours'][$val_inc];
							$resolving_complain_details['created_at'] = $this->now_time;
							$resolving_complain_details['updated_at'] = $this->now_time;
							$resolving_complain_details['created_by'] = $this->staff_id;
							$resolving_complain_details['updated_by'] = $this->staff_id;
							$this->db->insert('resolving_complain_details', $resolving_complain_details);
						}
						$val_inc++;
					}
                }
                
                if(isset($post_data['equipment_testing_data'])){
                    foreach ( $post_data['equipment_testing_data']['c_testing_normal_capacity_hrs'] as $installation_id => $c_testing_normal_capacity_hrs){
                        $complain_item_details = array();
                        $complain_item_details['c_testing_normal_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_normal_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_normal_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_normal_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_testing_full_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_full_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_full_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_full_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_temp_oli'] = $post_data['temperature_parameters_data']['c_temp_oli'][$installation_id];
                        $complain_item_details['c_temp_eqipment'] = $post_data['temperature_parameters_data']['c_temp_eqipment'][$installation_id];
                        $complain_item_details['c_temp_head'] = $post_data['temperature_parameters_data']['c_temp_head'][$installation_id];
                        $complain_item_details['c_temp_kiln'] = $post_data['temperature_parameters_data']['c_temp_kiln'][$installation_id];
                        $complain_item_details['c_temp_hot_air'] = $post_data['temperature_parameters_data']['c_temp_hot_air'][$installation_id];
                        $complain_item_details['c_temp_weather'] = $post_data['temperature_parameters_data']['c_temp_weather'][$installation_id];
                        $this->db->where('complain_id', $post_data['resolve_complain_data']['complain_id']);
                        $this->db->where('installation_id', $installation_id);
                        $this->db->update('complain_items', $complain_item_details);
                    }
                }
                
                $this->db->where_in('resolve_complain_id', $resolve_complain_id);
                $this->db->delete('resolve_complain_items');
                foreach ($post_data['resolve_complain_items'] as $resolve_complain_items) {
                    $resolve_complain_item_details = array();
                    $resolve_complain_item_details['resolve_complain_id'] = $resolve_complain_id;
                    $resolve_complain_item_details['complain_id'] = $resolve_complain_items['complain_id'];
                    $resolve_complain_item_details['installation_id'] = $resolve_complain_items['installation_id'];
                    $resolve_complain_item_details['testing_report_id'] = $resolve_complain_items['testing_report_id'];
                    $resolve_complain_item_details['challan_id'] = $resolve_complain_items['challan_id'];
                    $resolve_complain_item_details['created_at'] = $this->now_time;
                    $resolve_complain_item_details['updated_at'] = $this->now_time;
                    $resolve_complain_item_details['created_by'] = $this->staff_id;
                    $resolve_complain_item_details['updated_by'] = $this->staff_id;
                    $this->db->insert('resolve_complain_items', $resolve_complain_item_details);
                }
            }
		} else {
			$dataToInsert = $post_data['resolve_complain_data'];
            $complain_id = $dataToInsert['complain_id'];
            
            $max_resolve_complain_no = $this->crud->get_max_number('resolve_complain','resolve_complain_no');
			$dataToInsert['resolve_complain_no'] = $max_resolve_complain_no->resolve_complain_no + 1;
			$dataToInsert['resolve_complain_no_year'] = $this->applib->get_resolve_complain_no($dataToInsert['resolve_complain_no'], $post_data['resolve_complain_data']['resolve_complain_date']);
			
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
			$result = $this->db->insert('resolve_complain', $dataToInsert);
            
			$resolve_complain_id = $this->db->insert_id();
			if ($result) {
				$return['success'] = "Added";
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Resolve Complain Added Successfully');
                //---Start Update Complain Table
                $this->crud->update('complain', array('is_resolve_complain_created' => '1'), array('complain_id' => $complain_id));
                //---End Update Complain Table
                

				$val_inc = 0;
				if (!empty($post_data['reason_customer_complain_details']['reason_for_customer_complain_text'])) {
					foreach ($post_data['reason_customer_complain_details']['reason_for_customer_complain_text'] as $reason_for_customer_complain_text) {
						$reason_customer_complain_details = array();
						$reason_customer_complain_details['resolve_complain_id'] = $resolve_complain_id;
                        $reason_customer_complain_details['reason_for_customer_complain_text'] = $reason_for_customer_complain_text;
                        if(isset($post_data['reason_customer_complain_details']['reason_checking_person'][$val_inc])){
                            $reason_customer_complain_details['reason_checking_person'] = $post_data['reason_customer_complain_details']['reason_checking_person'][$val_inc];
                        }
                        $reason_customer_complain_details['complain_start_date'] = (!empty(strtotime($post_data['reason_customer_complain_details']['complain_start_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['reason_customer_complain_details']['complain_start_date'][$val_inc])) : null;
                        $reason_customer_complain_details['complain_end_date'] = (!empty(strtotime($post_data['reason_customer_complain_details']['complain_end_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['reason_customer_complain_details']['complain_end_date'][$val_inc])) : null;
                        $reason_customer_complain_details['complain_hours'] = $post_data['reason_customer_complain_details']['complain_hours'][$val_inc];
                        $reason_customer_complain_details['created_at'] = $this->now_time;
                        $reason_customer_complain_details['updated_at'] = $this->now_time;
                        $reason_customer_complain_details['created_by'] = $this->staff_id;
                        $reason_customer_complain_details['updated_by'] = $this->staff_id;
						$this->db->insert('reason_customer_complain_details', $reason_customer_complain_details);
						$val_inc++;
					}
				}
				$val_inc = 0;
				if (!empty($post_data['resolving_complain_details']['resolving_complain_text'])) {
					foreach ($post_data['resolving_complain_details']['resolving_complain_text'] as $resolving_complain_text) {
						$resolving_complain_details = array();
						$resolving_complain_details['resolve_complain_id'] = $resolve_complain_id;
                        $resolving_complain_details['resolving_complain_text'] = $resolving_complain_text;
                        if(isset($post_data['resolving_complain_details']['resolving_person'][$val_inc])){
                            $resolving_complain_details['resolving_person'] = $post_data['resolving_complain_details']['resolving_person'][$val_inc];
                        }
                        $resolving_complain_details['resolving_start_date'] = (!empty(strtotime($post_data['resolving_complain_details']['resolving_start_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['resolving_complain_details']['resolving_start_date'][$val_inc])) : null;
                        $resolving_complain_details['resolving_end_date'] = (!empty(strtotime($post_data['resolving_complain_details']['resolving_end_date'][$val_inc]))) ? date("Y-m-d", strtotime($post_data['resolving_complain_details']['resolving_end_date'][$val_inc])) : null;
                        $resolving_complain_details['resolving_hours'] = $post_data['resolving_complain_details']['resolving_hours'][$val_inc];
                        $resolving_complain_details['created_at'] = $this->now_time;
                        $resolving_complain_details['updated_at'] = $this->now_time;
                        $resolving_complain_details['created_by'] = $this->staff_id;
                        $resolving_complain_details['updated_by'] = $this->staff_id;
						$this->db->insert('resolving_complain_details', $resolving_complain_details);
						$val_inc++;
					}
				}
                if(isset($post_data['equipment_testing_data'])){
                    foreach ( $post_data['equipment_testing_data']['c_testing_normal_capacity_hrs'] as $installation_id => $c_testing_normal_capacity_hrs){
                        $complain_item_details = array();
                        $complain_item_details['c_testing_normal_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_normal_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_normal_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_normal_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_normal_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_normal_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_testing_full_capacity_hrs'] = $post_data['equipment_testing_data']['c_testing_full_capacity_hrs'][$installation_id];
                        $complain_item_details['c_testing_full_test_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_test_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_moisture_raw_material'] = $post_data['equipment_testing_data']['c_testing_full_moisture_raw_material'][$installation_id];
                        $complain_item_details['c_testing_full_after_procces_moisture'] = $post_data['equipment_testing_data']['c_testing_full_after_procces_moisture'][$installation_id];
                        $complain_item_details['c_temp_oli'] = $post_data['temperature_parameters_data']['c_temp_oli'][$installation_id];
                        $complain_item_details['c_temp_eqipment'] = $post_data['temperature_parameters_data']['c_temp_eqipment'][$installation_id];
                        $complain_item_details['c_temp_head'] = $post_data['temperature_parameters_data']['c_temp_head'][$installation_id];
                        $complain_item_details['c_temp_kiln'] = $post_data['temperature_parameters_data']['c_temp_kiln'][$installation_id];
                        $complain_item_details['c_temp_hot_air'] = $post_data['temperature_parameters_data']['c_temp_hot_air'][$installation_id];
                        $complain_item_details['c_temp_weather'] = $post_data['temperature_parameters_data']['c_temp_weather'][$installation_id];
                        $this->db->where('complain_id', $post_data['resolve_complain_data']['complain_id']);
                        $this->db->where('installation_id', $installation_id);
                        $this->db->update('complain_items', $complain_item_details);
                    }
                }
                foreach ($post_data['resolve_complain_items'] as $resolve_complain_items) {
                    $resolve_complain_item_details = array();
                    $resolve_complain_item_details['resolve_complain_id'] = $resolve_complain_id;
                    $resolve_complain_item_details['complain_id'] = $resolve_complain_items['complain_id'];
                    $resolve_complain_item_details['installation_id'] = $resolve_complain_items['installation_id'];
                    $resolve_complain_item_details['testing_report_id'] = $resolve_complain_items['testing_report_id'];
                    $resolve_complain_item_details['challan_id'] = $resolve_complain_items['challan_id'];
                    $resolve_complain_item_details['created_at'] = $this->now_time;
                    $resolve_complain_item_details['updated_at'] = $this->now_time;
                    $resolve_complain_item_details['created_by'] = $this->staff_id;
                    $resolve_complain_item_details['updated_by'] = $this->staff_id;
                    $this->db->insert('resolve_complain_items', $resolve_complain_item_details);
                }
			}
		}
        
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['resolve_complain_data']['party_id']);
            $this->applib->send_sms('resolve_complain', $resolve_complain_id, $party_phone_no, SEND_RESOLVE_COMPLAIN_SMS_ID);
        }
        
		print json_encode($return);
		exit;
	}

	function resolve_complain_list() {
        if ($this->app_model->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "view")) {
			set_page('service/complain/resolve_complain_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function resolve_complain_datatable() {
		$config['table'] = 'resolve_complain r_com';
		$config['select'] = 'r_com.resolve_complain_id,r_com.resolve_complain_no,r_com.resolve_complain_no_year,r_com.resolve_complain_date,com.complain_id,com.complain_no,com.complain_no_year,com.complain_date,p.party_name,p.phone_no,p.email_id,city.city,state.state,country.country';
		$config['joins'][] = array('join_table' => 'complain com', 'join_by' => 'com.complain_id = r_com.complain_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = com.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');

		$config['column_order'] = array(null, 'r_com.resolve_complain_no', 'r_com.resolve_complain_date', 'com.complain_no', 'com.complain_date', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['column_search'] = array('r_com.resolve_complain_no_year', 'r_com.resolve_complain_date', 'com.complain_no_year', 'p.party_name', 'p.phone_no', 'p.email_id', 'city.city', 'state.state', 'country.country');
		$config['order'] = array('r_com.resolve_complain_id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '1');

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
		if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
		} elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
		} else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
			$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
        $role_delete = $this->app_model->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "edit");
		foreach ($list as $resolve_complain) {
			$row = array();
			$action = '';
			if ($role_edit) {
				$action .= '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id) . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			}
			if ($role_delete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('complain/delete_resolve_complain/' . $resolve_complain->resolve_complain_id) . '"><i class="fa fa-trash"></i></a>';				
			}
            $complain_items = $this->crud->get_row_by_id('complain_items', array('complain_id' => $resolve_complain->complain_id));
            if (count($complain_items) > 1) {
                $action .= ' | <a href="#" class="print_button btn-primary btn-xs show_itemprint_modal" id="' . $resolve_complain->resolve_complain_id . '" data-complain_id="' . $resolve_complain->complain_id . '" ><i class="fa fa-print"></i> Print</a>';
            } else {
                $action .= ' | <a href="' . base_url('complain/resolving_complain/' . $resolve_complain->resolve_complain_id) . '" target="_blank" class="btn-primary btn-xs" title="Resolving Complain Print"><i class="fa fa-print"></i> Print</a>';
            }
			$action .= ' | <a href="' . base_url('complain/resolving_complain_email/' . $resolve_complain->resolve_complain_id) . '" target="_blank" class="" title="Email">Email</a>';
			$row[] = $action;
			$row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' . $resolve_complain->resolve_complain_no_year . '</a>';
			$resolve_complain_date = (!empty(strtotime($resolve_complain->resolve_complain_date))) ? date("d-m-Y", strtotime($resolve_complain->resolve_complain_date)) : '';
            $row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' . $resolve_complain_date . '</a>';
			$row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' . $resolve_complain->complain_no_year . '</a>';
            $complain_date = (!empty(strtotime($resolve_complain->complain_date))) ? date("d-m-Y", strtotime($resolve_complain->complain_date)) : '';
			$row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' .  $complain_date . '</a>';
			$row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' . $resolve_complain->party_name . '</a>';
            $phone_no = substr($resolve_complain->phone_no,0,14);
            $resolve_complain->phone_no = str_replace(',', ', ', $resolve_complain->phone_no);
            $row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$resolve_complain->phone_no.'" >'.$phone_no.'</a>';
            $email_id = substr($resolve_complain->email_id,0,14);
            $resolve_complain->email_id = str_replace(',', ', ', $resolve_complain->email_id);
            $row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$resolve_complain->email_id.'" >'.$email_id.'</a>';
			$row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' . $resolve_complain->city . '</a>';
			$row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' . $resolve_complain->state . '</a>';
			$row[] = '<a href="' . base_url('complain/resolve_complain/' . $resolve_complain->resolve_complain_id . '?view') . '" >' . $resolve_complain->country . '</a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
    
    function feed_resolve_complain_items($resolve_complain_id){
        $this->db->select('rcomi.*, ci.item_name, i.installation_no, i.installation_no_year');
		$this->db->from('resolve_complain_items rcomi');
        $this->db->join('challan_items ci', 'ci.challan_id = rcomi.challan_id', 'left');
        $this->db->join('installation i', 'i.installation_id = rcomi.installation_id', 'left');
		$this->db->where('rcomi.resolve_complain_id', $resolve_complain_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$item_data = $query->result();
			echo json_encode(array('success' => true, 'message' => 'Item loaded successfully!', 'item_data' => $item_data));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}
    
    function delete_resolve_complain($resolve_complain_id) {
        $role_delete = $this->app_model->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "delete");
		if ($role_delete) {
            
            //---Start Update Complain Table
            $complain_id = $this->crud->get_id_by_val('resolve_complain', 'complain_id', 'resolve_complain_id', $resolve_complain_id);
            $this->crud->update('complain', array('is_resolve_complain_created' => '0'), array('complain_id' => $complain_id));
			//---End Update Complain Table
            
			$where_array = array("resolve_complain_id" => $resolve_complain_id);
			$this->crud->delete("resolve_complain", $where_array);
			$this->crud->delete("reason_customer_complain_details", $where_array);
			$this->crud->delete("resolving_complain_details", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
}