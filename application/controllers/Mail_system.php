<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Mailbox_system
 */
class Mail_system extends CI_Controller
{
	private $email_address = SMTP_EMAIL_ADDRESS;
	private $email_password = SMTP_EMAIL_PASSWORD;
	private $email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->library('user_agent');
	}

	function check_internet_conn(){
		return checkdnsrr('php.net')?true:false;
	}

	function inbox()
	{
		/*echo $this->get_max_mail_no_in_folder('INBOX');die;*/
		$mails_data = $this->fetch_mails();
		/*echo "<pre>";
		print_r($mails_data);
		die;*/
		set_page('mail_system/mailbox', array('mails_data' => $mails_data));
	}

	function sent_mail()
	{
		$mails_data = $this->fetch_mails('Sent Mail');
		set_page('mail_system/mailbox', array('mails_data' => $mails_data,'curr_folder_name'=>'Sent Mail'));
	}

	function drafts()
	{
		$mails_data = $this->fetch_mails('Drafts');
		set_page('mail_system/mailbox', array('mails_data' => $mails_data));
	}

	function trash()
	{
		$mails_data = $this->fetch_mails('Trash');
		set_page('mail_system/mailbox', array('mails_data' => $mails_data));
	}

	function spam()
	{
		$mails_data = $this->fetch_mails('Spam');
		set_page('mail_system/mailbox', array('mails_data' => $mails_data));
	}

	function junk()
	{
		$mails_data = $this->fetch_mails('INBOX.Junk');
		set_page('mail_system/mailbox', array('mails_data' => $mails_data));
	}

	function starred()
	{
		$mails_data = $this->fetch_mails('Starred');
		set_page('mail_system/mailbox', array('mails_data' => $mails_data));
	}

	function folder_mails()
	{
		$folder_name = $org_folder_name = isset($_POST['folder_name'])?$_POST['folder_name']:$this->getConfigValue('folder_name');
		$folder_name = $this->getServerFolderName($folder_name);
		$mails_data = $this->fetch_mails($folder_name);
		set_page('mail_system/mailbox', array('mails_data'=>$mails_data,'curr_folder_name'=>$org_folder_name));
	}

	function getServerFolderName($folder_name){
		$default_folder_name = array(
			'All Mail',
			'Drafts',
			'Important',
			'Sent Mail',
			'Spam',
			'Starred',
			'Trash',
		);
		if(strtolower($folder_name) == "inbox"){
			return "INBOX";
		}elseif(in_array(ucfirst($folder_name),$default_folder_name)){
			return "[Gmail]/".ucfirst($folder_name);
		}else{
			$mailbox_label_array = explode('/', $folder_name);
			if (count($mailbox_label_array) > 1) {
				$folder_name = '';
				foreach ($mailbox_label_array as $key => $item) {
					if ($key > 0) {
						$folder_name .= "/";
					}
					$folder_name .= $item;
				}
			}
			return $folder_name;
		}
	}

	function read()
	{
		$folder_name = $_POST['folder_name'];
		$message_no = $_POST['message_no'];

		if ($this->input->is_ajax_request()) {
			$mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
			$mail_view = $this->load->view('mail_system/read_email',array('mail_detail' => $mail_detail,'message_no'=>$message_no,'curr_folder_name'=>$folder_name),true);
			$no_unread_mails = $this->applib->count_inbox_unread_mail();
			$unread_mails = '';
			if($no_unread_mails > 0){
				$unread_mails = $this->load->view('shared/unread_mails_view',array('unread_mails'=>$no_unread_mails,'unread_mails_data'=>$this->applib->fetch_unread_mails()),true);
			}
			echo json_encode(array('email_detail'=>$mail_view,'no_unread_mails'=>$no_unread_mails,'unread_mails'=>$unread_mails));
			exit();
		}else{
			$mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
			$mails_data = $this->getMailbox($folder_name);
			set_page('mail_system/mailbox', array('mail_detail' => $mail_detail, 'mails_data' => $mails_data,'message_no'=>$message_no,'curr_folder_name'=>$folder_name));
		}
	}

	function compose_email($action = "compose"){
		if (!empty($_POST['to'])) {
			$attach = $_POST['attachments'];
			if(isset($_POST['mail_id'])){
				$this->db->select('attachments');
				$this->db->from('mailbox_mails');
				$this->db->where('mail_id',$_POST['to']);
				$this->db->limit(1);
				$query = $this->db->get();
				if($query->num_rows() > 0){
					$attachments = $query->row()->attachments;
					if($attachments != null){
						$attachments = unserialize($attachments);
						foreach($attachments as $attachment_row){
							$attach[] = $attachment_row['url'];
						}
					}
				}
			}

			if ($this->send_mail($_POST['to'], $_POST['subject'], $_POST['message'],$attach)) {
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Mail sent successfully!');
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Wrong please try again.');
			}
			redirect(base_url()."mail-system/sent-mail/");
		} else {
			if($action != "compose"){
				$folder_name = $_POST['folder_name'];
				$message_no = $_POST['message_no'];
				$mail_detail = $this->fetch_mail_detail($folder_name, $message_no);
				$mails_data = $this->getMailbox($folder_name);
				/*print_r($mails_data);die;*/
				set_page('mail_system/mailbox', array('mail_detail' => $mail_detail,'mails_data' => $mails_data,'curr_folder_name'=>$folder_name,'action'=>$action));
			}else{
				$mails_data = $this->getMailbox();
				set_page('mail_system/mailbox',array('mails_data' => $mails_data,'action'=>$action));
			}
		}
	}

	function getMailbox($folder_name = "INBOX")
	{
		$folder_name = $this->getServerFolderName($folder_name);
		$mails_data = $this->fetch_mails($folder_name);
		return $mails_data;
	}

	function fetch_mail_detail($mailbox_name,$message_no)
	{
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		$mail_data = array();
		if($this->check_internet_conn()){
			$mb = imap_open($this->email_server.$mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			if ($messageCount > 0) {
				$email_headers = imap_headerinfo($mb, $message_no);
				$mail_data['message_no'] = $message_no;
				$mail_data['username'] = $email_headers->fromaddress;
				$mail_data['subject'] = $email_headers->subject;
				$mail_data['body'] = $this->get_mail_body($mb, $message_no);
				$mail_data['attachments'] = $this->get_mail_attachments($mb, $message_no);
				$mail_data['received_at'] = $this->timeAgo($email_headers->date);
				$mail_data['cc'] = 0;
				$mail_data['bcc'] = 0;
				$mail_data['prev_mail_no'] = 0;
				$mail_data['next_mail_no'] = 0;
				imap_close($mb);
			}
		}else{
			$this->db->select('*');
			$this->db->from('mailbox_mails');
			$this->db->where('folder_name',$this->email_server.$mailbox_name);
			$this->db->where('mail_no',$message_no);
			$this->db->limit(1);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$mail_data['mail_id'] = $query->row()->mail_id;
				$mail_data['message_no'] = $query->row()->mail_no;
				if($query->row()->from_name != $this->email_address){
					$mail_data['username'] =  $query->row()->from_name;
					$mail_data['email'] =  $query->row()->from_address;
				}else{
					$mail_data['username'] =  $query->row()->to_name;
					$mail_data['email'] =  $query->row()->to_address;
				}
				$mail_data['subject'] = $query->row()->subject;
				$mail_data['body'] = $query->row()->body;
				if($query->row()->attachments != null){
					$attachments = unserialize($query->row()->attachments);
					$mail_data['attachments'] = $attachments;
				}else{
					$mail_data['attachments'] = null;
				}
				$mail_data['received_at'] = $query->row()->received_at;
				$mail_data['cc'] = $query->row()->cc;
				$mail_data['bcc'] = $query->row()->bcc;
			}
		}
		return $mail_data;
	}

	function getAttachments($imap, $mailNum, $part, $partNum)
	{
		$attachments = array();

		if (isset($part->parts)) {
			foreach ($part->parts as $key => $subpart) {
				if ($partNum != "") {
					$newPartNum = $partNum . "." . ($key + 1);
				} else {
					$newPartNum = ($key + 1);
				}
				$result = getAttachments($imap, $mailNum, $subpart,
					$newPartNum);
				if (count($result) != 0) {
					array_push($attachments, $result);
				}
			}
		} else if (isset($part->disposition)) {
			if ($part->disposition == "ATTACHMENT") {
				$partStruct = imap_bodystruct($imap, $mailNum,
					$partNum);
				$attachmentDetails = array(
					"name" => $part->dparameters[0]->value,
					"partNum" => $partNum,
					"enc" => $partStruct->encoding
				);
				return $attachmentDetails;
			}
		}

		return $attachments;
	}

	function send_mail($to, $subject, $message, $attach = null)
	{
		if($this->check_internet_conn()){
			/*$this->load->library('email');*/
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => "$this->email_address",
				'smtp_pass' => "$this->email_password",
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('JayKhodiyar', "$this->email_address");
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			if ($attach != null && $attach != '') {
				if (is_array($attach) && count($attach) > 0) {
					foreach ($attach as $attach_row) {
						$this->email->attach($attach_row);
					}
				}
			}
			if ($this->email->send()) {
				if ($attach != null && $attach != '') {
					if (is_array($attach)) {
						foreach ($attach as $attach_row) {
							if(file_exists($attach_row)){
								unlink($attach_row);
							}
						}
					}
				}
				return true;
			} else {
				if ($attach != null && $attach != '') {
					if (is_array($attach)) {
						foreach ($attach as $attach_row) {
							if(file_exists($attach_row)){
								unlink($attach_row);
							}
						}
					}
				}
				/*echo $this->email->print_debugger();*/
				return false;
			}
		}else{
			$folder_detail = $this->get_folder_detail_by_label('Drafts');
			$attachments = array();
			if(is_array($attach) && count($attach) > 0){
				foreach($attach as $attach_row){
					$attachments[] = array(
						'filename'=>basename($attach_row,PATHINFO_FILENAME),
						'url'=>$attach_row,
						'subtype'=>pathinfo($attach_row,PATHINFO_EXTENSION),
					);
				}
			}
			$email_data = array(
				'from_address'=>$this->email_address,
				'from_name'=>$this->email_address,
				'to_address'=>$to,
				'to_name'=>$to,
				'subject'=>$subject,
				'body'=>$message,
				'attachments'=>count($attachments)>0?serialize($attachments):null,
				'is_attachment'=>count($attachments)>0?1:0,
				'mail_no'=>1 + $this->get_max_mail_no_in_folder('Drafts'),
				'folder_id'=>$folder_detail['id'],
				'folder_label'=>$folder_detail['folder_label'],
				'folder_name'=>$folder_detail['folder_name'],
				'received_at'=>date('Y-m-d H:i:s'),
				'is_created'=>1,
			);
			$this->db->insert('mailbox_mails',$email_data);
			$mail_id = $this->db->insert_id();
			$this->db->insert('mailbox_changes_logs',array('action'=>'compose','mail_id'=>$mail_id,'created_at'=>date('Y-m-d H:i:s')));
			return $this->db->insert_id() > 0?true:false;

		}
	}

	function get_folder_detail_by_label($folder_label){
		$this->db->select('*');
		$this->db->from('mailbox_folders');
		$this->db->where('LOWER(folder_label)',strtolower($folder_label));
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}

	/**
	 * @param $folder_label
	 * @return mixed
	 */
	function get_max_mail_no_in_folder($folder_label){
		$this->db->select('MAX(mail_no) as mail_no');
		$this->db->from('mailbox_mails');
		$this->db->where('LOWER(folder_label)',strtolower($folder_label));
		$query = $this->db->get();
		return $query->num_rows() > 0?$query->row()->mail_no:0;
	}

	function limit_words($string, $word_limit = 30)
	{
		$words = explode(" ", $string);
		return implode(" ", array_splice($words, 0, $word_limit));
	}

	function limit_character($string, $character_limit = 30)
	{
		if (strlen($string) > $character_limit) {
			return substr($string, 0, $character_limit) . '...';
		} else {
			return $string;
		}
	}

	/**
	 *
     */
	function create_folder()
	{
		if (isset($_POST['folder_name']) && isset($_POST['parent_folder']) && $_POST['folder_name'] != '' && strlen($_POST['folder_name']) > 0) {
			$folder_name = ucfirst($_POST['folder_name']);
			$parent_folder = $_POST['parent_folder'];
			$message = '';
			error_reporting(0);
			if($this->check_internet_conn()){
				$mb = imap_open($this->email_server,$this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
				if ($parent_folder == 1) {
					$mailbox = $this->email_server . $folder_name;
					imap_createmailbox($mb, $mailbox) or $message = imap_last_error();
					imap_subscribe($mb, $mailbox) or $message = imap_last_error();
				} else {
					$parent_folder = $this->getServerFolderName($parent_folder);
					$mailbox = $this->email_server . $parent_folder . "/" . $folder_name;
					imap_createmailbox($mb, $mailbox) or $message = imap_last_error();
					if ($message != '') {
						echo json_encode(array('success' => false, 'message' => $message));
						exit();
					}
					imap_subscribe($mb, $mailbox) or $message = imap_last_error();
				}
				if ($message == '') {
					$mailboxes = $this->get_mailboxes($mb);
					$sidebar_mail_label = $this->load->view('mail_system/sidebar_mail_label', array('mailboxes' => $mailboxes,'current_folder_name'=>$_POST['current_folder_name']), true);
					$move_to_folder = $this->load->view('mail_system/move_to_folder_view', array('mailboxes' => $mailboxes,'current_folder_name'=>$_POST['current_folder_name']), true);
					$folder_dropdown_option = $this->load->view('mail_system/folder_dropdown_view', array('mailboxes' => $mailboxes), true);
					echo json_encode(array('success' => true, 'message' => 'Folder created successfully.', 'sidebar_mail_label' => $sidebar_mail_label, 'move_to_folder' => $move_to_folder,'folder_dropdown_option'=>$folder_dropdown_option));
				} else {
					echo json_encode(array('success' => false, 'message' => $message));
				}
				imap_close($mb);
			}else{
				if ($parent_folder == 1) {
					$folder_data = array(
						'folder_label'=>$folder_name,
						'folder_name'=>$this->email_server.$folder_name,
						'is_created'=>1,
						'created_at'=>date('Y-m-d H:i:s'),
					);

				} else {
					$parent_folder = $this->getServerFolderName($parent_folder);
					$folder_data = array(
						'folder_label'=>$parent_folder . "/" . $folder_name,
						'folder_name'=>$this->email_server . $parent_folder . "/" . $folder_name,
						'is_created'=>1,
						'created_at'=>date('Y-m-d H:i:s'),
					);
				}
				$this->db->insert('mailbox_folders',$folder_data);
				$mailbox_changes_logs = array(
					'action'=>'create_folder',
					'folder_name'=>$folder_data['folder_name'],
				);
				$this->db->insert('mailbox_changes_logs',$mailbox_changes_logs);
				if ($this->db->insert_id() > 0) {
					$mailboxes = $this->get_mailboxes('');
					$sidebar_mail_label = $this->load->view('mail_system/sidebar_mail_label', array('mailboxes' => $mailboxes,'current_folder_name'=>$_POST['current_folder_name']), true);
					$move_to_folder = $this->load->view('mail_system/move_to_folder_view', array('mailboxes' => $mailboxes,'current_folder_name'=>$_POST['current_folder_name']), true);
					$folder_dropdown_option = $this->load->view('mail_system/folder_dropdown_view', array('mailboxes' => $mailboxes), true);
					echo json_encode(array('success' => true, 'message' => 'Folder created successfully.', 'sidebar_mail_label' => $sidebar_mail_label, 'move_to_folder' => $move_to_folder,'folder_dropdown_option'=>$folder_dropdown_option));
				} else {
					echo json_encode(array('success' => false, 'message' => 'Please try again, Something wrong!'));
				}
			}
		} else {
			echo json_encode(array('success' => false, 'message' => 'Something wrong, Please try again.'));
		}
	}

	function mail_move_to()
	{
		$folder_name = $_POST['folder_name'];
		$folder_name_ori = $folder_name;
		$folder_name = $this->getServerFolderName($folder_name);
		if (isset($_POST['mails_no']) && count($_POST['mails_no']) > 0) {
			$mails_no = $_POST['mails_no'];
			$from_folder_name = $from_folder_name_ori = $_POST['from_folder_name'];
			$from_folder_name = $this->getServerFolderName($from_folder_name);
			if($this->check_internet_conn()){
				$mb = imap_open($this->email_server.$from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
				$messageCount = imap_num_msg($mb);
				if ($messageCount > 0) {
					foreach ($mails_no as $no) {
						$this->move_mail($mb, $no,$folder_name);
					}
					imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
					$this->session->set_flashdata('success', true);
					$this->session->set_flashdata('message', "Mail successfully moved to $folder_name_ori!");
					$this->updateConfigItem('folder_name',$from_folder_name_ori);
					redirect($this->agent->referrer());
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
					$this->updateConfigItem('folder_name',$folder_name_ori);
					redirect($this->agent->referrer());
				}
			}else{
				foreach ($mails_no as $no) {
					$this->db->select('mail_id,mail_no');
					$this->db->from('mailbox_mails');
					$this->db->where('folder_name',$this->email_server.$from_folder_name);
					$this->db->where('mail_no',$no);
					$this->db->limit(1);
					$query = $this->db->get();
					if($query->num_rows() > 0) {
						$mail_id = $query->row()->mail_id;
						$from_mail_no = $query->row()->mail_no;
						$no = 1 + $this->get_max_mail_no_in_folder($folder_name);
						$email_data = array(
							'mail_no'=>$no,
							'folder_label'=>str_replace('[Gmail]/','',$folder_name),
							'folder_name'=>$this->email_server.$folder_name,
							'is_updated'=>1,
						);
						$this->db->where('mail_id',$mail_id);
						$this->db->update('mailbox_mails',$email_data);

						$email_change_log = array(
							'uid'=>$this->get_mail_id($no,$folder_name),
							'mail_id'=>$mail_id,
							'action'=>'move_to',
							'from_folder_name'=>$this->email_server.$from_folder_name,
							'from_mail_no'=>$from_mail_no,
							'to_folder_name'=>$this->email_server.$folder_name,
							'to_mail_no'=>$no,
						);
						$this->db->insert('mailbox_changes_logs',$email_change_log);
					}
				}
				if ($this->db->insert_id() > 0) {
					$this->session->set_flashdata('success', true);
					$this->session->set_flashdata('message', "Mail successfully moved to $folder_name_ori!");
					redirect($this->agent->referrer());
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
					redirect($this->agent->referrer());
				}
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please select at least one mail! ');
			$this->updateConfigItem('folder_name',$folder_name_ori);
			redirect($this->agent->referrer());
		}
	}

	function fetch_mails($mailbox_name = 'INBOX')
	{
		$mails_data = array();
		if($this->check_internet_conn()){
			$mailbox_name = $this->getServerFolderName($mailbox_name);
			$mb = imap_open($this->email_server.$mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			$mail_status = imap_status($mb, $this->email_server . $mailbox_name, SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!");
			$mails_data['count_unseen_mails'] = 0;
			$mails_data['inbox_unseen_mails'] = $this->count_inbox_unread_mail();
			$mails_data['total_mails'] = $mail_status->messages;
			$mails_data['mails_range'] = $messageCount > 0 ? "1-$messageCount" : "0-$messageCount";
			$mails_data['mailboxes'] = $this->get_mailboxes($mb);
			if($mail_status->messages > 0){
				$message_nos = imap_sort($mb,SORTDATE,1) or die(imap_last_error()."<br>Connection Faliure11!");
				foreach($message_nos as $key=>$message_no){
					$email_headers = imap_headerinfo($mb,$message_no);
					/*-------- Count Unread Mails -----------------*/
					if($email_headers->Unseen == 'U') {
						$mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
						$mails_data['mails'][$key]['is_unread'] = 1;
					}else{
						$mails_data['mails'][$key]['is_unread'] = 0;
					}
					$mails_data['mails'][$key]['attachment_status'] = $this->get_mail_attachment_status($mb,$message_no);;
					$mails_data['mails'][$key]['received_at'] = $this->timeAgo($email_headers->date);
					$mails_data['mails'][$key]['is_starred'] = $email_headers->Flagged == 'F'?1:0;
					$mails_data['mails'][$key]['subject'] = $email_headers->subject;
					if($email_headers->fromaddress != $this->email_address){
						if(isset($email_headers->from[0]->personal)){
							$mails_data['mails'][$key]['username'] = $email_headers->from[0]->personal;
						}else{
							$mails_data['mails'][$key]['username'] = $email_headers->from[0]->mailbox.'@'.$email_headers->from[0]->host;
						}
					}else{
						if(isset($email_headers->to[0]->personal)){
							$mails_data['mails'][$key]['username'] = $email_headers->to[0]->personal;
						}else{
							$mails_data['mails'][$key]['username'] = $email_headers->to[0]->mailbox.'@'.$email_headers->from[0]->host;
						}
					}
					$mails_data['mails'][$key]['mail_no'] = $email_headers->Msgno;
				}
			}
			imap_close($mb);
		}else{
			$mailbox_name = $this->getServerFolderName($mailbox_name);
			$this->db->select('m.mail_id,m.folder_name,m.mail_no,m.from_name,m.to_name,m.subject,m.is_starred,m.received_at,m.is_attachment,m.is_unread');
			$this->db->from('mailbox_mails m');
			if($mailbox_name == '[Gmail]/Starred'){
				$this->db->where('m.is_starred',1);
			}else{
				$this->db->where('m.folder_name',$this->email_server.$mailbox_name);
			}
			$query = $this->db->get();
			$total_mails = $query->num_rows();
			$mails_data['count_unseen_mails'] = 0;
			$mails_data['inbox_unseen_mails'] = 0;//$this->count_inbox_unread_mail();
			$mails_data['total_mails'] = $total_mails;
			$mails_data['mails_range'] = $total_mails > 0 ? "1-$total_mails" : "0-$total_mails";
			$mails_data['mailboxes'] = $this->get_mailboxes();
			if($total_mails > 0){
				foreach($query->result() as $key=>$mail_row){
					if($mail_row->is_unread == 1) {
						$mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
					}
					$mails_data['mails'][$key]['is_unread'] = $mail_row->is_unread;
					$mails_data['mails'][$key]['attachment_status'] = $mail_row->is_attachment;
					$mails_data['mails'][$key]['received_at'] = $this->timeAgo($mail_row->received_at);
					$mails_data['mails'][$key]['is_starred'] = $mail_row->is_starred;
					$mails_data['mails'][$key]['subject'] = $mail_row->subject;
					if($mail_row->from_name != $this->email_address){
						$mails_data['mails'][$key]['username'] = $mail_row->from_name;
					}else{
						$mails_data['mails'][$key]['username'] = $mail_row->to_name;
					}
					$mails_data['mails'][$key]['mail_no'] = $mail_row->mail_no;
					$mails_data['mails'][$key]['mail_id'] = $mail_row->mail_id;
					$mails_data['mails'][$key]['folder_name'] = str_replace("{$this->email_server}",'',$mail_row->folder_name);
				}
			}
		}
		return $mails_data;
	}

	function get_mail_attachment_status($mb,$message_no){
		$structure = imap_fetchstructure($mb, $message_no);
		$is_attachment = 0;
		if (isset($structure->parts) && count($structure->parts)) {
			$attachments = array();
			for ($i = 0; $i < count($structure->parts); $i++) {
				$attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
				$attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
				if ($structure->parts[$i]->ifdparameters) {
					foreach ($structure->parts[$i]->dparameters as $object) {
						if (strtolower($object->attribute) == 'filename') {
							$is_attachment = 1;
							break;
						}
					}
				}
			}
		}
		return $is_attachment;
	}

	function set_starred_flag()
	{
		$mailbox_name = $_POST['folder_name'];
		$message_no = $_POST['message_no'];
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		if ($this->check_internet_conn()) {
			$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$status = imap_setflag_full($mb, $message_no, "\\Flagged");
		} else {
			$this->db->where('mail_no', $message_no);
			$this->db->where('folder_name', $this->email_server . $mailbox_name);
			$this->db->update('mailbox_mails', array('is_starred' => 1));
			$status = $this->db->affected_rows() > 0 ? true : false;
			$mailbox_changes_logs = array(
				'uid'=>$this->get_mail_id($message_no,$mailbox_name),
				'action'=>'set_starred',
				'from_folder_name'=>$this->email_server . $mailbox_name,
				'from_mail_no'=>$message_no,
			);
			$this->db->insert('mailbox_changes_logs',$mailbox_changes_logs);
		}
		echo $status;
		exit();
	}

	function remove_starred_flag()
	{
		$mailbox_name = $_POST['folder_name'];
		$message_no = $_POST['message_no'];
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		if ($this->check_internet_conn()) {
			$mb = imap_open($this->email_server . $mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$status = imap_clearflag_full($mb, $message_no, "\\Flagged");
		} else {
			$this->db->where('mail_no', $message_no);
			$this->db->where('folder_name', $this->email_server . $mailbox_name);
			$this->db->update('mailbox_mails', array('is_starred' => 0));
			$status = $this->db->affected_rows() > 0 ? true : false;
			$mailbox_changes_logs = array(
				'uid' => $this->get_mail_id($message_no, $mailbox_name),
				'action' => 'remove_starred',
				'from_folder_name' => $this->email_server . $mailbox_name,
				'from_mail_no' => $message_no,
			);
			$this->db->insert('mailbox_changes_logs', $mailbox_changes_logs);
		}
		echo $status;
		exit();
	}

	function timeAgo($time_ago)
	{
		$time_ago = strtotime($time_ago);
		$cur_time = time();
		$time_elapsed = $cur_time - $time_ago;
		$seconds = $time_elapsed;
		$minutes = round($time_elapsed / 60);
		$hours = round($time_elapsed / 3600);
		$days = round($time_elapsed / 86400);
		$weeks = round($time_elapsed / 604800);
		$months = round($time_elapsed / 2600640);
		$years = round($time_elapsed / 31207680);
		// Seconds
		if ($seconds <= 60) {
			return "just now";
		} //Minutes
		else if ($minutes <= 60) {
			if ($minutes == 1) {
				return "one minute ago";
			} else {
				return "$minutes minutes ago";
			}
		} //Hours
		else if ($hours <= 24) {
			if ($hours == 1) {
				return "an hour ago";
			} else {
				return "$hours hrs ago";
			}
		} //Days
		else if ($days <= 7) {
			if ($days == 1) {
				return "yesterday";
			} else {
				return "$days days ago";
			}
		} //Weeks
		else if ($weeks <= 4.3) {
			if ($weeks == 1) {
				return "a week ago";
			} else {
				return "$weeks weeks ago";
			}
		} //Months
		else if ($months <= 12) {
			if ($months == 1) {
				return "a month ago";
			} else {
				return "$months months ago";
			}
		} //Years
		else {
			if ($years == 1) {
				return "one year ago";
			} else {
				return "$years years ago";
			}
		}
	}

	function count_inbox_unread_mail(){
		$unread_emails = 0;
		if($this->check_internet_conn()){
			$mb = imap_open($this->email_server."INBOX", $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			for ($MID = $messageCount; $MID >= 1; $MID--) {
				$email_headers = imap_headerinfo($mb, $MID);
				/*-------- Count Unread Mails -----------------*/
				if ($email_headers->Unseen == 'U') {
					$unread_emails = $unread_emails + 1;
				}
			}
		}
		return $unread_emails;
	}

	function search_mails()
	{
		$mailbox_name = $mailbox_name_ori = $_POST['mailbox_name'];
		$search = $_POST['search'];
		$mailbox_name = $this->getServerFolderName($mailbox_name);
		if($this->check_internet_conn()){
			$mb = imap_open($this->email_server.$mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$mails_data = array();
			if($search != ''){
				$emails = imap_search($mb,'TEXT "'.$search.'"');
			}else{
				$emails = imap_search($mb,'ALL');
			}
			if(count($emails)){
				$total_mails = count($emails);
				$mails_data['inbox_unseen_mails'] = $this->count_inbox_unread_mail();
				$mails_data['total_mails'] = $total_mails;
				$mails_data['mails_range'] = $total_mails > 0 ? "1-$total_mails" : "0-$total_mails";
				$mails_data['mailboxes'] = $this->get_mailboxes($mb);
				foreach($emails as $key=>$message_no){
					$email_headers = imap_headerinfo($mb,$message_no);
					/*-------- Count Unread Mails -----------------*/
					if($email_headers->Unseen == 'U') {
						$mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
						$mails_data['mails'][$key]['is_unread'] = 1;
					}else{
						$mails_data['mails'][$key]['is_unread'] = 0;
					}
					$mails_data['mails'][$key]['attachment_status'] = $this->get_mail_attachment_status($mb,$message_no);;
					$mails_data['mails'][$key]['received_at'] = $this->timeAgo($email_headers->date);
					$mails_data['mails'][$key]['is_starred'] = $email_headers->Flagged == 'F'?1:0;
					$mails_data['mails'][$key]['subject'] = $email_headers->subject;
					if($email_headers->fromaddress != $this->email_address){
						if(isset($email_headers->from[0]->personal)){
							$mails_data['mails'][$key]['username'] = $email_headers->from[0]->personal;
						}else{
							$mails_data['mails'][$key]['username'] = $email_headers->from[0]->mailbox.'@'.$email_headers->from[0]->host;
						}
					}else{
						if(isset($email_headers->to[0]->personal)){
							$mails_data['mails'][$key]['username'] = $email_headers->to[0]->personal;
						}else{
							$mails_data['mails'][$key]['username'] = $email_headers->to[0]->mailbox.'@'.$email_headers->from[0]->host;
						}
					}
					$mails_data['mails'][$key]['mail_no'] = $email_headers->Msgno;
				}
			}
			imap_close($mb);
		}else{
			$this->db->select('m.mail_no,m.from_name,m.to_name,m.subject,m.body,m.is_starred,m.received_at,m.is_attachment,m.is_unread');
			$this->db->from('mailbox_mails m');
			$this->db->where('m.folder_name',$this->email_server.$mailbox_name);
			$this->db->group_start();
			$this->db->like('m.from_name',$search);
			$this->db->or_like('m.to_name',$search);
			$this->db->or_like('m.subject',$search);
			$this->db->or_like('m.body',$search);
			$this->db->group_end();
			$query = $this->db->get();
			$total_mails = $query->num_rows();
			$mails_data['count_unseen_mails'] = 0;
			$mails_data['inbox_unseen_mails'] = $this->count_inbox_unread_mail();
			$mails_data['total_mails'] = $total_mails;
			$mails_data['mails_range'] = $total_mails > 0 ? "1-$total_mails" : "0-$total_mails";
			$mails_data['mailboxes'] = $this->get_mailboxes();
			if($total_mails > 0){
				foreach($query->result() as $key=>$mail_row){
					if($mail_row->is_unread == 1) {
						$mails_data['count_unseen_mails'] = $mails_data['count_unseen_mails'] + 1;
					}
					$mails_data['mails'][$key]['is_unread'] = $mail_row->is_unread;
					$mails_data['mails'][$key]['attachment_status'] = $mail_row->is_attachment;
					$mails_data['mails'][$key]['received_at'] = $this->timeAgo($mail_row->received_at);
					$mails_data['mails'][$key]['is_starred'] = $mail_row->is_starred;
					$mails_data['mails'][$key]['subject'] = $mail_row->subject;
					if($mail_row->from_name != $this->email_address){
						$mails_data['mails'][$key]['username'] = $mail_row->from_name;
					}else{
						$mails_data['mails'][$key]['username'] = $mail_row->to_name;
					}
					$mails_data['mails'][$key]['mail_no'] = $mail_row->mail_no;
				}
			}
		}
		set_page('mail_system/mailbox', array('mails_data' => $mails_data,'search'=>$search,'curr_folder_name'=>$mailbox_name_ori));
	}

	function delete_mail()
	{
		$from_folder_name = $_POST['folder_name'];
		$message_no = $_POST['message_no'];
		$from_folder_name = $this->getServerFolderName($from_folder_name);
		if($this->check_internet_conn()){
			$mb = imap_open($this->email_server . $from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			$messageCount = imap_num_msg($mb);
			if ($messageCount > 0) {
				$uid = imap_uid($mb, $message_no) or die(imap_last_error() . "<br>Connection Faliure!");
				if ($from_folder_name == "[Gmail]/Trash") {
					imap_delete($mb, $message_no);
					imap_expunge($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
				} else {
					$this->move_mail($mb, $message_no, '[Gmail]/Trash');
				}
				imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");;
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', "Mail successfully moved to trash!");
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
				redirect($this->agent->referrer());
			}
		}else{
			$this->db->select('mail_id,mail_no');
			$this->db->from('mailbox_mails');
			$this->db->where('folder_name',$this->email_server.$from_folder_name);
			$this->db->where('mail_no',$message_no);
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0) {
				$mail_id = $query->row()->mail_id;
				$from_mail_no = $query->row()->mail_no;
				$no = 1 + $this->get_max_mail_no_in_folder('Trash');
				$email_data = array(
					'mail_no'=>$no,
					'folder_label'=>'Trash',
					'folder_name'=>'{imap.gmail.com:993/imap/ssl/novalidate-cert}[Gmail]/Trash',
					'is_updated'=>1,
				);
				$this->db->where('mail_id',$mail_id);
				$this->db->update('mailbox_mails',$email_data);

				$email_change_log = array(
					'uid'=>$this->get_mail_id($no,'[Gmail]/Trash'),
					'mail_id'=>$mail_id,
					'action'=>'move_to',
					'from_folder_name'=>$this->email_server.$from_folder_name,
					'from_mail_no'=>$from_mail_no,
					'to_folder_name'=>'{imap.gmail.com:993/imap/ssl/novalidate-cert}[Gmail]/Trash',
					'to_mail_no'=>$no,
				);
				$this->db->insert('mailbox_changes_logs',$email_change_log);
				if ($this->db->insert_id() > 0) {
					$this->session->set_flashdata('success', true);
					$this->session->set_flashdata('message', "Mail successfully moved to trash!");
					redirect("mail-system/trash");
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
					redirect($this->agent->referrer());
				}
			}else{
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
				redirect($this->agent->referrer());
			}
		}
	}

	function get_mail_id($mail_no,$folder_name){
		$this->db->select('uid');
		$this->db->from('mailbox_mails');
		$this->db->where('mail_no',$mail_no);
		$this->db->where('folder_name',$this->email_server.$folder_name);
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->uid;
		}else{
			return false;
		}
	}

	function delete_multi_mails()
	{
		if (isset($_POST['mails_no']) && count($_POST['mails_no']) > 0) {
			$from_folder_name = $_POST['from_folder_name'];
			$mails_no = $_POST['mails_no'];
			$from_folder_name = $this->getServerFolderName($from_folder_name);
			if($this->check_internet_conn()){
				$mb = imap_open($this->email_server.$from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
				$messageCount = imap_num_msg($mb);
				if ($messageCount > 0) {
					foreach ($mails_no as $key=>$no) {
						if ($from_folder_name == $this->email_server."[Gmail]/Trash") {
							imap_delete($mb,$no);
							imap_expunge($mb) or die(imap_last_error()."<br>Connection Faliure!");
						} else {
							$this->move_mail($mb,$no,'[Gmail]/Trash') or die(imap_last_error()."<br>Connection Faliure!");
						}
					}
					imap_close($mb) or die(imap_last_error() . "<br>Connection Faliure!");
					$this->session->set_flashdata('success', true);
					$this->session->set_flashdata('message', "Mail successfully moved to trash!");
					redirect("mail-system/trash");
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
					redirect($this->agent->referrer());
				}
			}else{
				foreach ($mails_no as $key=>$no) {
					$this->db->select('mail_id,mail_no');
					$this->db->from('mailbox_mails');
					$this->db->where('folder_name',$this->email_server.$from_folder_name);
					$this->db->where('mail_no',$no);
					$this->db->limit(1);
					$query = $this->db->get();
					if($query->num_rows() > 0) {
						$mail_id = $query->row()->mail_id;
						$from_mail_no = $query->row()->mail_no;
						$no = 1 + $this->get_max_mail_no_in_folder('Trash');
						$email_data = array(
							'mail_no'=>$no,
							'folder_label'=>'Trash',
							'folder_name'=>'{imap.gmail.com:993/imap/ssl/novalidate-cert}[Gmail]/Trash',
							'is_updated'=>1,
						);
						$this->db->where('mail_id',$mail_id);
						$this->db->update('mailbox_mails',$email_data);

						$email_change_log = array(
							'uid'=>$this->get_mail_id($no,'[Gmail]/Trash'),
							'mail_id'=>$mail_id,
							'action'=>'move_to',
							'from_folder_name'=>$this->email_server.$from_folder_name,
							'from_mail_no'=>$from_mail_no,
							'to_folder_name'=>'{imap.gmail.com:993/imap/ssl/novalidate-cert}[Gmail]/Trash',
							'to_mail_no'=>$no,
						);
						$this->db->insert('mailbox_changes_logs',$email_change_log);
					}
				}
				if ($this->db->insert_id() > 0) {
					$this->session->set_flashdata('success', true);
					$this->session->set_flashdata('message', "Mail successfully moved to trash!");
					redirect("mail-system/trash");
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Please try again, Mail not found in mailbox.');
					redirect($this->agent->referrer());
				}
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please select at least one mail! ');
			redirect($this->agent->referrer());
		}
	}

	function get_mailboxes($mailbox_stream = '')
	{
		$mailbox_array = array();
		if($this->check_internet_conn()){
			$mailbox_stream_or = $mailbox_stream;
			if ($mailbox_stream == '') {
				$mailbox_stream = $mb = imap_open($this->email_server, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
			}
			$mailboxes = imap_getmailboxes($mailbox_stream, $this->email_server, '*') or die(imap_last_error() . "<br>Connection Faliure!");
			$default_mailboxes = array(
				$this->email_server . 'INBOX',
				$this->email_server . '[Gmail]',
				$this->email_server . '[Gmail]/All Mail',
				$this->email_server . '[Gmail]/Drafts',
				$this->email_server . '[Gmail]/Sent Mail',
				$this->email_server . '[Gmail]/Spam',
				$this->email_server . '[Gmail]/Starred',
				$this->email_server . '[Gmail]/Trash',
				$this->email_server . '[Gmail]/Important',
			);
			foreach ($mailboxes as $mailbox) {
				if (!in_array($mailbox->name, $default_mailboxes)) {
					$mailbox_label = str_replace($this->email_server,'',$mailbox->name);
					$delimiter = $mailbox->delimiter;
				 	$mailbox_label_array = explode($delimiter, $mailbox_label);
					if (count($mailbox_label_array) > 1) {
						$mailbox_label = '';
						foreach ($mailbox_label_array as $key => $item) {
							if ($key > 0) {
								$mailbox_label .= "/";
							}
							$mailbox_label .= $item;
						}
					}
					$mailbox_array[] = array('mailbox_label' => $mailbox_label, 'mailbox_name' => $mailbox->name);
				}
			}
		}else{
			$this->db->select('folder_label as mailbox_label,folder_name as mailbox_name');
			$this->db->from('mailbox_folders');
			$this->db->where('is_system_folder',0);
			$query = $this->db->get();
			$mailbox_array = $query->result_array();
		}
		return $mailbox_array;
	}

	function move_mail($mailbox_stream, $mail_no, $move)
	{
		return imap_mail_move($mailbox_stream, $mail_no, $move) or die(imap_last_error() . "<br>Connection Faliure!");
	}

	function parseString($Body, $Start, $End, $LenghtVar, $EndVar = 0)
	{
		$StartPos = strpos($Body, $Start);
		$StartPos = ($StartPos + $LenghtVar);
		$EndPos = $StartPos + 1;
		if ($End == 'end_mail') {
			$EndPos = strlen($Body);
		} else {
			$EndPos = strpos($Body, $End);
		}

		$Property = "";
		if (($StartPos - $LenghtVar) > 0 && $EndPos > $StartPos) {
			$EndPos = ($EndPos - $StartPos) + $EndVar;
			$Property = substr($Body, $StartPos, $EndPos);
		}
		$Property = $this->removeSpecialCharacters($Property);
		return $Property;
	}



	function removeSpecialCharacters($Property)
	{
		$Property = str_replace("=20", "", $Property);
		$Property = str_replace("=", "", $Property);
		$Property = trim($Property);
		return $Property;
	}

	function upload_attachment()
	{
		$this->load->library('upload');
		$config['upload_path'] = 'resource/uploads/attachments';
		$config['allowed_types'] = 'gif|jpg|png|txt|pdf|zip';
		$config['overwrite'] = TRUE;
		$this->upload->initialize($config);
		if ($this->upload->do_upload('upload_attachment')) {
			$file_data = $this->upload->data();
			$file_url = $file_data['file_name'];
			echo json_encode(array('success' => true, 'message' => 'Attachment successfully uploaded.', 'attachment_url' => 'resource/uploads/attachments/'.$file_url));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Wrong with upload operation!', 'error' => $this->upload->display_errors()));
			exit();
		}
	}

	function getConfigValue($config_key){
		$this->db->select('config_value');
		$this->db->from('config');
		$this->db->where('config_key',$config_key);
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->config_value;
		}else{
			return '';
		}
	}

	function updateConfigItem($config_key,$config_value){
		$this->db->where('config_key',$config_key);
		$this->db->update('config',array("config_value"=>$config_value));
		return true;
	}

	function save_account_to_local(){
		$mb = imap_open($this->email_server, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!1");
		$mailboxes = imap_getmailboxes($mb,$this->email_server,'*') or die(imap_last_error() . "<br>Connection Faliure!2");
		imap_close($mb);
		$default_mailboxes = array(
			$this->email_server . 'INBOX',
			$this->email_server . '[Gmail]',
			$this->email_server . '[Gmail]/All Mail',
			$this->email_server . '[Gmail]/Drafts',
			$this->email_server . '[Gmail]/Sent Mail',
			$this->email_server . '[Gmail]/Spam',
			$this->email_server . '[Gmail]/Starred',
			$this->email_server . '[Gmail]/Trash',
			$this->email_server . '[Gmail]/Important',
		);
		foreach ($mailboxes as $mailbox) {

			$this->db->select('id,folder_label');
			$this->db->from('mailbox_folders');
			$this->db->where('folder_name',$mailbox->name);
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				$folder_id = $query->row()->id;
				$mailbox_label = $query->row()->folder_label;
			}else{
				/*----------- Insert Mailbox Folder ------------*/
				if (in_array($mailbox->name, $default_mailboxes)) {
					if($this->email_server.'[Gmail]' != $mailbox->name && $this->email_server.'[Gmail]/All Mail' != $mailbox->name){
						if($this->email_server.'INBOX' == $mailbox->name){
							$mailbox_label = 'Inbox';
						}else{
							$mailbox_label = str_replace($this->email_server.'[Gmail]/','',$mailbox->name);
						}
						$mailbox_data = array(
							'folder_label'=>$mailbox_label,
							'folder_name'=>$mailbox->name,
							'is_system_folder'=>1,
							'created_at'=>date("Y-m-d H:i:s"),
						);
						$this->db->insert('mailbox_folders',$mailbox_data);
					}
				}else{
					$mailbox_label = str_replace($this->email_server,'',$mailbox->name);
					$mailbox_data = array(
						'folder_label'=>$mailbox_label,
						'folder_name'=>$mailbox->name,
						'is_system_folder'=>0,
						'created_at'=>date("Y-m-d H:i:s"),
					);
					$this->db->insert('mailbox_folders',$mailbox_data);
				}
				/*----------- Insert Mailbox Folder ------------*/

				$folder_id = $this->db->insert_id();
			}
			if($this->email_server.'[Gmail]' != $mailbox->name && $this->email_server.'[Gmail]/All Mail' != $mailbox->name && $this->email_server.'[Gmail]/Starred' != $mailbox->name){
				$mailbox_name = trim($mailbox->name);
				$mb = imap_open($mailbox_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!3");
				$mail_status = imap_status($mb,$mailbox_name, SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!4");
				if($mail_status->messages > 0){
					$message_nos = imap_sort($mb,SORTDATE,1) or die(imap_last_error()."<br>Connection Faliure!5");
					foreach($message_nos as $key=>$message_no){
						$email_headers = imap_headerinfo($mb,$message_no);
						$this->db->select('mail_id');
						$this->db->from('mailbox_mails');
						$this->db->where('uid',$email_headers->uid);
						$this->db->where('folder_id',$folder_id);
						$this->db->limit(1);
						$query = $this->db->get();
						if($query->num_rows() > 0){

						}else{
							$mail_data = array();
							$mail_data['folder_id'] = $folder_id;
							$mail_data['mail_no'] = $email_headers->Msgno;
							$mail_data['uid'] = imap_uid ($mb,$message_no );
							if($email_headers->from[0]->mailbox != ''){
								$mail_data['from_address'] = $email_headers->from[0]->mailbox.'@'.$email_headers->from[0]->host;
								if(isset($email_headers->from[0]->personal)){
									$mail_data['from_name'] = $email_headers->from[0]->personal;
								}else{
									$mail_data['from_name'] = $email_headers->from[0]->mailbox.'@'.$email_headers->from[0]->host;
								}
							}


							if($email_headers->to[0]->mailbox != ''){
								$mail_data['to_address'] = $email_headers->to[0]->mailbox.'@'.$email_headers->to[0]->host;
								if(isset($email_headers->to[0]->personal)){
									$mail_data['to_name'] = $email_headers->to[0]->personal;
								}else{
									$mail_data['to_name'] = $email_headers->to[0]->mailbox.'@'.$email_headers->to[0]->host;
								}
							}

							if($email_headers->reply_to[0]->mailbox != ''){
								$mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox.'@'.$email_headers->reply_to[0]->host;
								if(isset($email_headers->reply_to[0]->personal)){
									$mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
								}else{
									$mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox.'@'.$email_headers->reply_to[0]->host;
								}
							}

							if($email_headers->sender[0]->mailbox != ''){
								$mail_data['sender_address'] = $email_headers->sender[0]->mailbox.'@'.$email_headers->sender[0]->host;
								if(isset($email_headers->sender[0]->personal)){
									$mail_data['sender_name'] = $email_headers->sender[0]->personal;
								}else{
									$mail_data['sender_name'] = $email_headers->sender[0]->mailbox.'@'.$email_headers->sender[0]->host;
								}
							}

							$mail_data['cc'] = '';
							$mail_data['bcc'] = '';
							$mail_data['subject'] = $email_headers->subject;
							$mail_data['body'] = $this->get_mail_body($mb,$message_no);
							$attachments = $this->get_mail_attachments($mb,$message_no);
							if(count($attachments) > 0){
								$mail_data['is_attachment'] = 1;
								$attachment_url = array();
								$time = time();
								mkdir('resource/uploads/attachments/'.$time);
								foreach($attachments as $attachment){
									$file = "resource/uploads/attachments/$time/".$attachment['filename'];
									file_put_contents($file,base64_decode($attachment['file']));
									$attachment_url[] = array(
										'filename'=>$attachment['filename'],
										'url'=>$file,
										'subtype'=>$attachment['subtype'],
									);
								}
								$mail_data['attachments'] = serialize($attachment_url);
							}
							$mail_data['received_at'] = $email_headers->date;
							$mail_data['folder_label'] = $mailbox_label;
							$mail_data['folder_name'] = $mailbox->name;
							$mail_data['is_unread'] = $email_headers->Unseen == 'U'?1:0;
							$mail_data['is_starred'] = $email_headers->Flagged == 'F'?1:0;
							$mail_data['created_at'] = date('Y-m-d H:i:s');
							$this->db->insert('mailbox_mails',$mail_data);
						}
					}
				}
				imap_close($mb);
			}
		}
		echo "Success !!";
	}

	/**
	 * @param $mb
	 * @param $message_no
	 * @return mixed|string
	 */
	function get_mail_body($mb,$message_no){
		$body = imap_fetchbody($mb,$message_no,1.2);
		if (!strlen($body) > 0) {
			$body = imap_fetchbody($mb,$message_no,1);
		}
		$body = preg_replace('/^\>/m', '', $body);
		return $body;
	}

	/**
	 * @param $mb
	 * @param $message_no
	 * @return array
	 */
	function get_mail_attachments($mb,$message_no){
		$attachments = array();
		$structure = imap_fetchstructure($mb, $message_no);
		if (isset($structure->parts) && count($structure->parts)) {
			for ($i = 0; $i < count($structure->parts); $i++) {
				$attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
				$attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
				if ($structure->parts[$i]->ifdparameters) {
					foreach ($structure->parts[$i]->dparameters as $object) {
						if (strtolower($object->attribute) == 'filename') {
							$attachments[$i]['is_attachment'] = true;
							$attachments[$i]['filename'] = $object->value;
						}
					}
				}
				if ($structure->parts[$i]->ifparameters) {
					foreach ($structure->parts[$i]->parameters as $object) {
						if (strtolower($object->attribute) == 'name') {
							$attachments[$i]['is_attachment'] = true;
							$attachments[$i]['name'] = $object->value;
						}
					}
				}
				if ($attachments[$i]['is_attachment']) {
					$attachments[$i]['attachment'] = imap_fetchbody($mb, $message_no, $i + 1);
					if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
					} elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
						$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
					}
				}
			}
		}
		$response_data = array();
		if (count($attachments) != 0) {
			foreach ($attachments as $at) {
				if ($at['is_attachment'] == 1) {
					$response_data[] = array(
						'filename' => $at['name'],
						'file' => $at['attachment'],
						'subtype' => $at['subtype'],
					);
				}
			}
		}
		return $response_data;
	}

	function local_data_to_live_account()
	{
		$this->db->select('*');
		$this->db->from('mailbox_changes_logs');
		$this->db->where('is_done', 0);
		$this->db->order_by('id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $change_log) {
				if ($change_log->action == 'compose') {
					$this->db->select('to_address,subject,body,attachments');
					$this->db->from('mailbox_mails');
					$this->db->where('mail_id', $change_log->mail_id);
					$this->db->limit(1);
					$query = $this->db->get();
					if ($query->num_rows() > 0) {
						$attach = array();
						if ($query->row()->attachments != null) {
							$attachments = unserialize($query->row()->attachments);
							foreach ($attachments as $attachment_row) {
								$attach[] = $attachment_row['url'];
							}
						}
						if ($this->send_mail($query->row()->to_address, $query->row()->subject, $query->row()->body, $attach)) {
							$this->db->where('id', $change_log->id);
							$this->db->update('mailbox_changes_logs', array('is_done' => 1));
						}
					}

				} elseif ($change_log->action == 'move_to') {
					$mb = imap_open($change_log->from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure in mail move to 0!");
					if ($change_log->uid != null) {
						$uid = imap_uid($mb, $change_log->from_mail_no) or die(imap_last_error() . "<br>Connection Faliure  in mail move to 1!");
						$no = imap_msgno($mb, $change_log->uid) or die(imap_last_error() . "<br>Connection Faliure in mail move to 2!");
					} else {
						$no = $change_log->from_mail_no;
					}
					$status = $this->move_mail($mb,$no,trim(str_replace($this->email_server,'',$change_log->to_folder_name)));
					imap_close($mb);
					if ($status) {
						$this->db->where('id', $change_log->id);
						$this->db->update('mailbox_changes_logs', array('is_done' => 1));
					}
				} elseif ($change_log->action == 'set_starred') {
					$mb = imap_open($change_log->from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure in mail set starred!");
					if ($change_log->uid != null) {
						$mail_no = imap_msgno($mb, $change_log->uid) or die(imap_last_error() . "<br>Connection Faliure!");
					} else {
						$mail_no = $change_log->from_mail_no;
					}
					$status = imap_setflag_full($mb, $mail_no, "\\Flagged");
					imap_close($mb);
					if ($status) {
						$this->db->where('id', $change_log->id);
						$this->db->update('mailbox_changes_logs', array('is_done' => 1));
					}
				} elseif ($change_log->action == 'remove_starred') {
					$mb = imap_open($change_log->from_folder_name, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure in mail remove starred!");
					if ($change_log->uid != null) {
						$mail_no = imap_msgno($mb, $change_log->uid) or die(imap_last_error() . "<br>Connection Faliure!");
					} else {
						$mail_no = $change_log->from_mail_no;
					}
					$status = imap_clearflag_full($mb, $mail_no, "\\Flagged");
					imap_close($mb);
					if ($status) {
						$this->db->where('id', $change_log->id);
						$this->db->update('mailbox_changes_logs', array('is_done' => 1));
					}
				} elseif ($change_log->action == 'create_folder') {
					$mb = imap_open($this->email_server, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure in create folder!");
					imap_createmailbox($mb, $change_log->folder_name);
					$status = imap_subscribe($mb, $change_log->folder_name);
					imap_close($mb);
					if ($status) {
						$this->db->where('id', $change_log->id);
						$this->db->update('mailbox_changes_logs', array('is_done' => 1));
					}
				}
			}
			echo "Local changes successfully saved to live.";
		}else{
			echo "No changes found!";
		}
	}
}
