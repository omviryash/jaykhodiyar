<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Report
 * &@property Crud $crud
 */
class Report extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
    }

    function index() {
        // @todo
    }

    /**
     * Give "From Date" and "To Date" filter
     * Give below columns :
     * "Quotations", "Quotations Qty", "Quotations Amount", "Orders", "Orders Qty", "Orders Amount", "Dispatch", "Dispatch Qty", "Dispatch Amount"
     * @return [type] [description]
     */
    function sales_summary_v1() {
        $data = array();

        $data['from_date'] = (isset($_REQUEST['from_date']) && $_REQUEST['from_date']) ? date('Y-m-d', strtotime($_REQUEST['from_date'])) : date('Y-m-d', strtotime(' -1 day'));
        $data['to_date'] = (isset($_REQUEST['to_date']) && $_REQUEST['to_date']) ? date('Y-m-d', strtotime($_REQUEST['to_date'])) : date('Y-m-d');


        $sql = "SELECT chls.id as challan_id, so.id as sale_order_id, qt.id as quotation_id
            FROM challans chls
            INNER JOIN sales_order so ON chls.sales_order_id = so.id
            INNER JOIN quotations qt ON so.quotation_id = qt.id
            WHERE chls.challan_date >= '" . $data['from_date'] . "' AND chls.challan_date <= '" . $data['to_date'] . "'
                ORDER BY chls.challan_date DESC";

        $query = $this->db->query($sql);
        $list = $query->result_array();

        $quotation_ids = $sale_order_ids = $challan_ids = array();
        foreach ($list as $d) {
            $challan_ids[] = $d['challan_id'];
            $sale_order_ids[] = $d['sale_order_id'];
            $quotation_ids[] = $d['quotation_id'];
        }

        if ($challan_ids) {
            $challan_items = $this->get_challan_items($challan_ids);
        }
        if ($sale_order_ids) {
            $sale_order_items = $this->get_sales_order_items($sale_order_ids);
        }
        if ($quotation_ids) {
            $quotation_items = $this->get_quotation_items($quotation_ids);
        }

        $i = 1;
        $data['sales_data'] = array();
        foreach ($list as $d) {
            $row = array();

            $challan_items_show = '';
            $challan_items_total = 0;
            if (array_key_exists($d['challan_id'], $challan_items)) {
                foreach ($challan_items[$d['challan_id']] as $value) {
                    $challan_items_show .= $value->item_name . ' (' . $value->item_code . ') (' . $value->quantity . ')<br>';
                    $challan_items_total += $value->total_amount;
                }
            }
            $d['challan_items'] = $challan_items_show;
            $d['challan_items_total'] = $challan_items_total;

            $sale_order_items_show = '';
            $sale_order_items_total = 0;
            if (array_key_exists($d['sale_order_id'], $sale_order_items)) {
                foreach ($sale_order_items[$d['sale_order_id']] as $value) {
                    $sale_order_items_show .= $value->item_name . ' (' . $value->item_code . ') (' . $value->quantity . ')<br>';
                    $sale_order_items_total += $value->total_amount;
                }
            }
            $d['sale_order_items'] = $sale_order_items_show;
            $d['sale_order_items_total'] = $sale_order_items_total;


            $quotation_items_show = '';
            $quotation_items_total = 0;
            if (array_key_exists($d['quotation_id'], $quotation_items)) {
                foreach ($quotation_items[$d['quotation_id']] as $value) {
                    $quotation_items_show .= $value->item_name . ' (' . $value->item_code . ') (' . $value->quantity . ')<br>';
                    $quotation_items_total += $value->total_amount;
                }
            }
            $d['quotation_items'] = $quotation_items_show;
            $d['quotation_items_total'] = $quotation_items_total;


            $data['sales_data'][] = $d;

            $i++;
        }
        if ($this->applib->have_access_role(REPORT_SALES_SUMMARY_MENU_ID, "view")) {
            set_page('report/sales_summary', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    /**
     * Give "From Date" and "To Date" filter
     * Give below columns :
     * "Quotations", "Quotations Qty", "Quotations Amount", "Orders", "Orders Qty", "Orders Amount", "Dispatch", "Dispatch Qty", "Dispatch Amount"
     */
    function sales_summary() {
        $from_date = (isset($_REQUEST['from_date']) && $_REQUEST['from_date']) ? date('Y-m-d', strtotime($_REQUEST['from_date'])) : date('Y-m-d', strtotime(' -1 day'));
        $to_date = (isset($_REQUEST['to_date']) && $_REQUEST['to_date']) ? date('Y-m-d', strtotime($_REQUEST['to_date'])) : date('Y-m-d');
        $data = $this->sales_summary_data($from_date, $to_date);
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        //pre_die($data);
        if ($this->applib->have_access_role(REPORT_SALES_SUMMARY_MENU_ID, "view")) {
            set_page('report/sales_summary', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    /**
     * @param null $from_date
     * @param null $to_date
     * @return array
     */
    function sales_summary_data($from_date = NULL, $to_date = NULL) {
        if ($from_date == NULL) {
            $from_date = date('Y-m-d');
        }
        if ($to_date == NULL) {
            $to_date = date('Y-m-d');
        }

        $response = array();
        /* ------ Quotation ------- */
        $this->db->select('id');
        $this->db->from('quotations');
        $this->db->where('quotation_date >=', $from_date);
        $this->db->where('quotation_date <=', $to_date);
        $query = $this->db->get();
        $response['total_quotation'] = $query->num_rows();
        if ($query->num_rows() > 0) {
            $quotation_ids = array();
            foreach ($query->result() as $row) {
                $quotation_ids[] = $row->id;
            }
            $this->db->select('SUM(quantity) as quantity,SUM(amount) as amount,item_code,item_name,quotation_id');
            $this->db->from('quotation_items');
            $this->db->where_in('quotation_id', $quotation_ids);
            $this->db->group_by('item_id');
            $query = $this->db->get();
            $isEdit_quotation = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
            if ($query->num_rows() > 0) {
                $total_amount = 0;
                foreach ($query->result() as $row) {
                    if ($isEdit_quotation) {
                        $action = ' <a href="' . base_url('quotation/add/' . $row->quotation_id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
                    }
                    $response['quotation_qty'][] = array(
                        'quotation_id' => $action,
                        'item_code' => $row->item_code,
                        'item_name' => $row->item_name,
                        'quantity' => $row->quantity,
                        'amount' => $row->amount,
                    );
                    $total_amount = $total_amount + $row->amount;
                }
                $response['quotation_amount'] = $total_amount;
            } else {
                $response['quotation_qty'] = "";
                $response['quotation_amount'] = 0;
            }
        } else {
            $response['quotation_qty'] = "";
            $response['quotation_amount'] = 0;
        }

        /* ------ Sales Order ------- */
        $this->db->select('id');
        $this->db->from('sales_order');
        $this->db->where('sales_order_date >=', $from_date);
        $this->db->where('sales_order_date <=', $to_date);
        $query = $this->db->get();
        $response['total_sales_order'] = $query->num_rows();
        $role_edit_enquiry = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");
        if ($query->num_rows() > 0) {
            $sales_order_ids = array();
            foreach ($query->result() as $row) {
                $sales_order_ids[] = $row->id;
            }
            $this->db->select('SUM(quantity) as quantity,SUM(amount) as amount,item_code,item_name,sales_order_id');
            $this->db->from('sales_order_items');
            $this->db->where_in('sales_order_id', $sales_order_ids);
            $this->db->group_by('item_id');
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $total_amount = 0;
                foreach ($query->result() as $row) {
                    if ($role_edit_enquiry) {
                        $action = '<a href="' . base_url('sales_order/add/' . $row->sales_order_id) . '"
				   target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
                    }
                    $response['sales_order_qty'][] = array(
                        'sales_order_id' => $action,
                        'item_code' => $row->item_code,
                        'item_name' => $row->item_name,
                        'quantity' => $row->quantity,
                        'amount' => $row->amount,
                    );
                    $total_amount = $total_amount + $row->amount;
                }
                $response['sales_order_amount'] = $total_amount;
            } else {
                $response['sales_order_qty'] = "";
                $response['sales_order_amount'] = 0;
            }
        } else {
            $response['sales_order_qty'] = "";
            $response['sales_order_amount'] = 0;
        }

        /* ------ Dispatch Challan ------- */
        $this->db->select('id');
        $this->db->from('challans');
        $this->db->where('challan_date >=', $from_date);
        $this->db->where('challan_date <=', $to_date);
        $query = $this->db->get();
        $response['total_challans'] = $query->num_rows();
        $role_edit_dispatch = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "edit");
        if ($query->num_rows() > 0) {
            $challan_ids = array();
            foreach ($query->result() as $row) {
                $challan_ids[] = $row->id;
            }
            $this->db->select('SUM(quantity) as quantity,SUM(amount) as amount,item_code,item_name,challan_id');
            $this->db->from('challan_items');
            $this->db->where_in('challan_id', $challan_ids);
            $this->db->group_by('item_id');
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $total_amount = 0;
                foreach ($query->result() as $row) {
                    if ($role_edit_dispatch) {
                        $action = ' <a href="' . base_url('dispatch/challan_edit/' . $row->challan_id) . '" target="_blank" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                    }
                    $response['challan_qty'][] = array(
                        'challan_id' => $action,
                        'item_code' => $row->item_code,
                        'item_name' => $row->item_name,
                        'quantity' => $row->quantity,
                        'amount' => $row->amount,
                    );
                    $total_amount = $total_amount + $row->amount;
                }
                $response['challan_amount'] = $total_amount;
            } else {
                $response['challan_qty'] = "";
                $response['challan_amount'] = 0;
            }
        } else {
            $response['challan_qty'] = "";
            $response['challan_amount'] = 0;
        }

        return $response;
        //pre_die($response);
    }

    /**
     * - Display Sales Order Items for which Dispatch Challan is not created
     * Display columns
     * Date, Party, Item, Qty
     * @return [type] [description]
     */
    function production_schedule() {
        $data = array();
        if ($this->applib->have_access_role(REPORT_PRODUCTION_SCHEDULE_MENU_ID, "view")) {
            set_page('report/production_schedule', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function production_schedule_datatable() {

        $requestData = $_REQUEST;
        $config['table'] = 'sales_order s';
        $config['select'] = 's.*,p.party_name, (select count(*) from sales_order_items where s.id = sales_order_id) as count_items, soi.item_name, soi.item_code, soi.quantity';
        $config['column_order'] = array(null, null, 's.sales_order_date', 'p.party_name', 'soi.item_name', 'soi.quantity');
        $config['column_search'] = array('s.sales_order_no', 'p.party_name', 's.sales_order_date', 's.committed_date');

        $config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][2] = array('join_table' => 'sales_order_items soi', 'join_by' => 'soi.sales_order_id = s.id', 'join_type' => 'right');
        $config['wheres'][] = array('column_name' => 'p.active', 'column_value' => '0');
        $config['where_string'] = ' 1 = 1 AND  s.id NOT IN (SELECT sales_order_id FROM challans) ';

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            $config['where_string'] .= ' AND ( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_EXPORT_ID;
            } else if ($cu_accessDomestic == 1) {
                $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_DOMESTIC_ID;
            } else {
                
            }
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        //echo $this->db->last_query(); exit;
        $data = array();
        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");

        $i = 1;
        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");
        foreach ($list as $d) {
            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . base_url('sales_order/add/' . $d->id) . '"
				    target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
            }
            $row[] = date('d-m-Y', strtotime($d->sales_order_date));
            $row[] = $action;
            $row[] = date('d-m-Y', strtotime($d->sales_order_date));
            $row[] = $d->party_name;
            $row[] = $d->item_name . ' (' . $d->item_code . ') ';
            $row[] = $d->quantity;
            $data[] = $row;
            $i++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /**
     * Enquiry Followup History
     * @return array
     */
    function inquiry_followup_history() {
        $data = array();
        $data['enquiry_status_data'] = $this->db->get('inquiry_status')->result();
        $data['staff_data'] = $this->db->select('staff_id,name')->where('name !=','')->order_by('name','asc')->get('staff')->result();
        if ($this->applib->have_access_role(REPORT_ENQUIRY_FOLLOWUP_HISTORY_MENU_ID, "view")) {
            set_page('report/inquiry_followup_history', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    /**
     * Enquiry Followup History
     * @return array
     */
    function inquiry_followup_history_v1() {
        $data = array();

        $data['from_date'] = (isset($_REQUEST['from_date']) && $_REQUEST['from_date']) ? date('Y-m-d', strtotime($_REQUEST['from_date'])) : date('Y-m-d', strtotime(' -1 day'));
        $data['to_date'] = (isset($_REQUEST['to_date']) && $_REQUEST['to_date']) ? date('Y-m-d', strtotime($_REQUEST['to_date'])) : date('Y-m-d');


        $sql = "SELECT e.*,p.party_name, p.party_code, p.address,p.reference_description, p.reference_id, e.inquiry_date, ifh.*, cp.name as contact_person_name, cp.phone_no as contact_person_phone, cp.mobile_no as contact_person_mobile, cp.email as contact_person_email,ct.city,st.state,cnt.country,stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno
            FROM inquiry_followup_history ifh
            LEFT JOIN inquiry e ON ifh.inquiry_id = e.inquiry_id
            LEFT JOIN inquiry_status s ON s.id = e.inquiry_status_id
            LEFT JOIN party p ON p.party_id = e.party_id
            LEFT JOIN city ct ON ct.city_id = p.city_id
            LEFT JOIN state st ON st.state_id = p.state_id
            LEFT JOIN country cnt ON cnt.country_id = p.country_id
            LEFT JOIN contact_person cp ON e.kind_attn_id = cp.contact_person_id
            LEFT JOIN staff stf ON stf.staff_id = e.created_by
            WHERE p.active != '0'
                AND ifh.followup_date >= '" . $data['from_date'] . "' AND ifh.followup_date <= '" . $data['to_date'] . "'
                ORDER BY ifh.followup_date DESC";

        $query = $this->db->query($sql);
        $list = $query->result_array();

        $inquiry_ids = array();
        foreach ($list as $d) {
            $inquiry_ids[] = $d['inquiry_id'];
        }

        if ($inquiry_ids) {
            $items = $this->get_inquiry_items($inquiry_ids);
        }

        $i = 1;
        $data['followup_history'] = array();
        foreach ($list as $d) {
            $row = array();

            $item_str = '';
            if (array_key_exists($d['inquiry_id'], $items)) {
                foreach ($items[$d['inquiry_id']] as $value) {
                    $item_data = json_decode($value->item_data);
                    $item_str .= $item_data->item_name . ' (' . $item_data->item_code . ') (' . $item_data->quantity . ')<br>';
                }
            }
            $d['items'] = $item_str;
            $data['followup_history'][] = $d;

            $i++;
        }
        if ($this->applib->have_access_role(REPORT_BOM_INFO_REPORT_MENU_ID, "view")) {
            set_page('report/inquiry_followup_history', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function quotation_summary() {
        $role_view = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "view");
        if ($role_view) {
            $this->db->select('q.id,q.enquiry_no,q.quotation_no,q.party_name,p.party_name as pname');
            $this->db->from('quotations q');
            $this->db->join('party p', 'p.party_id = q.party_id');
            $where_string = ' 1 = 1 ';
            $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
            $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
            if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
                
            } else {
                $where_string .= ' AND ( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
                if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                    
                } else if ($cu_accessExport == 1) {
                    $where_string .= ' AND p.party_type_1 = ' . PARTY_TYPE_EXPORT_ID;
                } else if ($cu_accessDomestic == 1) {
                    $where_string .= ' AND p.party_type_1 = ' . PARTY_TYPE_DOMESTIC_ID;
                } else {
                    
                }
            }

            $this->db->where('p.active !=', 0);
            $this->db->where($where_string);
            $query = $this->db->get();
            $result_data = $query->result();
            $data['results'] = $result_data;
            $data['item_status_data'] = $this->get_item_status();
            $data['qtn_stage_data'] = $this->get_quotation_stage();
            $data['qtn_type_data'] = $this->get_quotation_type();

            if ($this->applib->have_access_role(REPORT_QUOTATION_MANAGEMENT_MENU_ID, "view")) {
                set_page('report/quotation_summary', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function get_challan_items($challan_ids) {
        $this->db->select('*');
        $this->db->where_in('challan_id', $challan_ids);
        $this->db->from('challan_items');
        $data = $this->db->get()->result();
        $return = array();
        if ($data) {
            foreach ($data as $key => $value) {
                $return[$value->challan_id][] = $value;
            }
        }
        return $return;
    }

    function get_sales_order_items($sales_order_ids) {
        $this->db->select('*');
        $this->db->where_in('sales_order_id', $sales_order_ids);
        $this->db->from('sales_order_items');
        $data = $this->db->get()->result();
        $return = array();
        if ($data) {
            foreach ($data as $key => $value) {
                $return[$value->sales_order_id][] = $value;
            }
        }
        return $return;
    }

    function get_inquiry_items($inquiry_ids) {
        $this->db->select('*');
        $this->db->where_in('inquiry_id', $inquiry_ids);
        $this->db->from('inquiry_items');
        $data = $this->db->get()->result();
        $return = array();
        if ($data) {
            foreach ($data as $key => $value) {
                $return[$value->inquiry_id][] = $value;
            }
        }
        return $return;
    }

    function quotation_datatable() {
        $requestData = $_REQUEST;

        $config['select'] = 'q.email_id,q.phone_no,q.updated_at,q.quotation_date,q.id,q.enquiry_no,q.quotation_no,q.party_name,q.rev,p.party_name as pname, city.city, country.country,state.state, q.id,(select count(*) from quotation_items where q.id = quotation_id group by quotation_id) as count_items ';
        $config['table'] = 'quotations q';
        $config['column_order'] = array('q.updated_at', null, null, 'q.quotation_date', 'p.party_name', null, 'city.city', 'state.state', 'country.country');
        $config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 'p.party_name', 'country.country', 'city.city');

        if (!empty($requestData['columns'][1]['search']['value']) && $requestData['columns'][1]['search']['value'] != 'null') {
            $config['likes'][] = array('q.quotation_no' => trim($requestData['columns'][1]['search']['value']));
        }
        if (!empty($requestData['columns'][2]['search']['value']) && $requestData['columns'][2]['search']['value'] != 'null') {
            $config['likes'][] = array('q.enquiry_no' => trim($requestData['columns'][2]['search']['value']));
        }
        if (!empty($requestData['columns'][3]['search']['value']) && $requestData['columns'][3]['search']['value'] != 'null') {
            $config['likes'][] = array('p.party_name' => trim($requestData['columns'][3]['search']['value']));
        }
        if (!empty($requestData['columns'][5]['search']['value']) && $requestData['columns'][5]['search']['value'] != 'null') {
            //$config['likes'][] = array('city.city_id' => trim($requestData['columns'][5]['search']['value']));
            $config['wheres'][] = array('column_name' => 'city.city_id', 'column_value' => trim($requestData['columns'][5]['search']['value']));
        }
        if (!empty($requestData['columns'][6]['search']['value']) && $requestData['columns'][6]['search']['value'] != 'null') {
            //$config['likes'][] = array('state.state_id' => trim($requestData['columns'][6]['search']['value']));
            $config['wheres'][] = array('column_name' => 'state.state_id', 'column_value' => trim($requestData['columns'][6]['search']['value']));
        }
        if (!empty($requestData['columns'][7]['search']['value']) && $requestData['columns'][7]['search']['value'] != 'null') {
            //$config['likes'][] = array('country.country_id' => trim($requestData['columns'][7]['search']['value']));
            $config['wheres'][] = array('column_name' => 'country.country_id', 'column_value' => trim($requestData['columns'][7]['search']['value']));
        }

        $config['order'] = array('q.updated_at' => 'desc');
        $config['joins'][0] = array('join_table' => 'party p', 'join_by' => 'p.party_id = q.party_id');
        $config['joins'][1] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][2] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][3] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => 0);

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            $config['custom_where'] = '(p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1 ', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1 ', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            } else {
                
            }
        }

        $this->load->library('datatables', $config, 'datatable');
        /* echo "<pre>";print_r($this->datatable);die(); */
        $list = $this->datatable->get_datatables();
        //      echo $this->db->last_query();
        $data = array();
        $isEdit = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
        $isDelete = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "delete");
        $data_quotation = array();
        foreach ($list as $employee_leave) {
            $data_quotation[$employee_leave->id] = $employee_leave;
        }

        if ($data_quotation) {
            $items = $this->get_quotation_items(array_keys($data_quotation));
        }
        $i = 1;
        $isEdit = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
        foreach ($data_quotation as $quotation_id => $d) {
            $row = array();
            $action = '';
            if ($isEdit) {
                $action .= ' <a href="' . base_url('quotation/add/' . $quotation_id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
            }
            $row[] = $d->updated_at;
            $row[] = $i;
            $row[] = $action;
            $row[] = date('d-m-Y', strtotime($d->quotation_date));
            $row[] = $d->quotation_no;
            $row[] = !empty($d->rev) ? $d->rev : '1';
            $row[] = $d->pname;

            $item_str = '';
            if (array_key_exists($quotation_id, $items)) {
                foreach ($items[$quotation_id] as $value) {
                    $item_str .= $value->item_name . ' (' . $value->item_code . ') (' . $value->quantity . ')<br>';
                }
            }
            $row[] = $item_str;
            $row[] = $d->city;
            $row[] = $d->state;
            $row[] = $d->country;
            $data[] = $row;
            $i++;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function get_quotation_items($quotation_ids) {
        $this->db->select('*');
        $this->db->where_in('quotation_id', $quotation_ids);
        $this->db->from('quotation_items');
        $data = $this->db->get()->result();
        $return = array();
        if ($data) {
            foreach ($data as $key => $value) {
                $return[$value->quotation_id][] = $value;
            }
        }
        return $return;
    }

    function get_item_status() {
        $this->db->select('*');
        $this->db->from('item_status');
        return $this->db->get()->result();
    }

    /**
     * @return mixed
     */
    function get_quotation_stage() {
        $this->db->select('id,quotation_stage');
        $this->db->from('quotation_stage');
        return $this->db->get()->result();
    }

    /**
     * @return mixed
     */
    function get_quotation_type() {
        $this->db->select('id,quotation_type');
        $this->db->from('quotation_type');
        return $this->db->get()->result();
    }

    /**
     * Stock summary report
     * @return array
     */
    function stock_summary() {
        $data = array();

        $data['from_date'] = (isset($_REQUEST['from_date']) && $_REQUEST['from_date']) ? date('Y-m-d', strtotime($_REQUEST['from_date'])) : date('Y-m-01');
        $data['to_date'] = (isset($_REQUEST['to_date']) && $_REQUEST['to_date']) ? date('Y-m-d', strtotime($_REQUEST['to_date'])) : date('Y-m-d');

        // get items
        $this->db->select("*");
        $this->db->from('items');
        $query = $this->db->get();
        $items_temp = $query->result_array();
        $data['items'] = array();
        foreach ($items_temp as $item) {
            $data['items'][$item['id']] = $item;
        }

        if ($data['items']) {
            $data['purchase_stock'] = $this->get_purchase_stock($data['items'], $data['from_date'], $data['to_date']);
            $data['sales_stock'] = $this->get_sales_stock($data['items'], $data['from_date'], $data['to_date']);
        }
        if ($this->applib->have_access_role(REPORT_STOCK_MANAGEMENT_MENU_ID, "view")) {
            set_page('report/stock_summary', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    /**
     * Purchase stock data
     * It will return between date total purchase quantities of items
     * And previous total quantities items of given start date
     * @param  array items
     * @param  [date] start date
     * @param  [date] end date
     * @return [array]
     */
    function get_purchase_stock($items, $from_date, $to_date) {
        // get purchase orders before from dates
        $sql = "SELECT count(*) as count, item_id, SUM(quantity) AS quantities FROM purchase_invoice_items WHERE item_id IN (" . implode(',', array_keys($items)) . ") AND invoice_id IN (select invoice_id from purchase_invoice where invoice_date < '" . $from_date . "') GROUP BY item_id";

        $query = $this->db->query($sql);
        $purchase_master_items_temp = $query->result_array();

        $purchase_master_items = array();
        foreach ($purchase_master_items_temp as $item) {
            $purchase_master_items[$item['item_id']] = $item;
        }

        $before_current = $this->calculate_quantities($purchase_master_items);

        // get purchase orders between from date and to date
        $sql = "SELECT count(*) as count, item_id, SUM(quantity) AS quantities FROM purchase_invoice_items WHERE item_id IN (" . implode(',', array_keys($items)) . ") AND invoice_id IN (select invoice_id from purchase_invoice where invoice_date >= '" . $from_date . "' AND invoice_date <= '" . $to_date . "') GROUP BY item_id";

        $query = $this->db->query($sql);
        $purchase_master_items_temp = $query->result_array();

        $purchase_master_items = array();
        foreach ($purchase_master_items_temp as $item) {
            $purchase_master_items[$item['item_id']] = $item;
        }
        $current = $this->calculate_quantities($purchase_master_items);

        return array('before_current' => $before_current, 'current' => $current);
    }

    /**
     * Sales stock data
     * It will return between date total sale quantities of items
     * And previous total quantities items of given start date
     * @param  array items
     * @param  [date] start date
     * @param  [date] end date
     * @return [array]
     */
    function get_sales_stock($items, $from_date, $to_date) {
        // get purchase orders before from dates
        $sql = "SELECT count(*) as count, item_id, SUM(quantity) AS quantities FROM challan_items WHERE item_id IN (" . implode(',', array_keys($items)) . ") AND challan_id IN (select challan_id from challans where challan_date < '" . $from_date . "') GROUP BY item_id";

        $query = $this->db->query($sql);
        $purchase_master_items_temp = $query->result_array();
        $purchase_master_items = array();
        foreach ($purchase_master_items_temp as $item) {
            $purchase_master_items[$item['item_id']] = $item;
        }


        $before_current = $this->calculate_quantities($purchase_master_items);

        // get purchase orders between from date and to date
        $sql = "SELECT count(*) as count, item_id, SUM(quantity) AS quantities FROM challan_items WHERE item_id IN (" . implode(',', array_keys($items)) . ") AND challan_id IN (select challan_id from challans where challan_date >= '" . $from_date . "' AND challan_date <= '" . $to_date . "') GROUP BY item_id";

        $query = $this->db->query($sql);
        $purchase_master_items_temp = $query->result_array();
        $purchase_master_items = array();
        foreach ($purchase_master_items_temp as $item) {
            $purchase_master_items[$item['item_id']] = $item;
        }

        $current = $this->calculate_quantities($purchase_master_items);

        return array('before_current' => $before_current, 'current' => $current);
    }

    /**
     * Calculate BOM item and apply formula = (bom_item_qty * master_item_quatities)
     * @param  [array] master items
     * @return [array] quantities
     */
    function calculate_quantities($master_items) {
        $quantities = array();

        if ($master_items) {

            $item_bom = $this->get_item_bom($master_items);
            foreach ($master_items as $item_id => $item) {
                if (array_key_exists($item_id, $item_bom)) {
                    foreach ($item_bom[$item_id] as $bom) {
                        $quantity_cal = $bom['qty'] * $item['quantities'];
                        if (!array_key_exists($bom['item_id'], $quantities)) {
                            $quantities[$bom['item_id']] = 0;
                        }
                        $quantities[$bom['item_id']] += $quantity_cal;
                    }
                }

                if (!array_key_exists($item_id, $quantities)) {
                    $quantities[$item_id] = 0;
                }
                $quantities[$item_id] += $item['quantities'];
            }
        }
        return $quantities;
    }

    /**
     * Get BOM items of given items
     * @param  [array] items
     * @return [array]
     */
    function get_item_bom($items) {
        $item_bom = array();
        if ($items) {
            $sql = "SELECT * from item_bom where item_master_id IN (" . implode(',', array_keys($items)) . ")";
            $query = $this->db->query($sql);
            $item_bom_temp = $query->result_array();

            foreach ($item_bom_temp as $item) {
                $item_bom[$item['item_master_id']][] = $item;
            }
        }

        return $item_bom;
    }

    function sales_lead_summary() {

        $sql_list_query = "SELECT e.inquiry_id, e.inquiry_no, s.inquiry_status,p.party_name,p.party_code,e.inquiry_date
							from inquiry e
							LEFT JOIN inquiry_status s ON s.id = e.inquiry_status_id
							LEFT JOIN party p ON p.party_id = e.party_id";

        $condition = ' WHERE 1';

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            $condition .= ' AND ( p.`created_by`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.`created_by` IS NULL )';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
            } else if ($cu_accessDomestic == 1) {
                $condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
            } else {
                
            }
        }
        $status = $this->input->get_post('status');
        $user_id = $this->input->get_post('user_id');
        if ($status != '' && $status != 'all') {
            $condition .= " AND  s.inquiry_status = '" . $status . "' ";
        }

        if ($user_id != '' && $user_id != 'all') {
            $condition .= " AND  e.created_by = '" . $user_id . "' ";
        }

        $query = $this->db->query($sql_list_query . $condition);
        $enquiries = $query->result_array();

        $this->db->order_by('id');
        $enquiry_status = $this->db->get('inquiry_status')->result();

        $this->db->order_by('name');
        $users = $this->db->where('active !=', 0)->get('staff')->result();
        if ($this->applib->have_access_role(REPORT_SALES_LEAD_SUMMARY_MENU_ID, "view")) {
            set_page('report/sales_lead_summary', array('enquiries' => $enquiries, 'enquiry_status' => $enquiry_status, 'users' => $users));
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function sales_lead_summary_datatable() {

        $requestData = $_REQUEST;
        $config['table'] = 'inquiry e';
        $config['select'] = 'e.inquiry_id, e.inquiry_no, s.inquiry_status,p.party_name,p.party_code,e.inquiry_date,e.lead_or_inquiry';
        $config['column_order'] = array(null, 'e.inquiry_id', 'p.party_code', 'p.party_name', 's.inquiry_status', 'e.inquiry_date',);
        $config['column_search'] = array('e.inquiry_no', 's.inquiry_status', 'p.party_name', 'p.party_code', 'e.inquiry_date');
        if (!empty($requestData['columns'][1]['search']['value'])) {
            $config['likes'][] = array('e.inquiry_no' => $requestData['columns'][1]['search']['value']);
        }
        if (!empty($requestData['columns'][2]['search']['value'])) {
            $config['likes'][] = array('p.party_code' => $requestData['columns'][2]['search']['value']);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $config['likes'][] = array('p.party_name' => $requestData['columns'][3]['search']['value']);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $config['likes'][] = array('s.inquiry_status' => $requestData['columns'][4]['search']['value']);
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $config['likes'][] = array('e.inquiry_date' => date("Y-m-d", strtotime($requestData['columns'][5]['search']['value'])));
        }
        $config['joins'][0] = array('join_table' => 'inquiry_status s', 'join_by' => 's.id = e.inquiry_status_id', 'join_type' => 'left');
        $config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = e.party_id', 'join_type' => 'left');
        $config['order'] = array('e.inquiry_id' => 'desc');

        $config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
        if (!empty($_POST['user_id']) && $_POST['user_id'] != 'all') {
            $config['wheres'][] = array('column_name' => 'e.assigned_to_id', 'column_value' => $_POST['user_id']);
        }

        if (!empty($_POST['inquiry_status']) && $_POST['inquiry_status'] != 'all') {
            $inquiry_status_id = $this->crud->get_id_by_val('inquiry_status', 'id', 'inquiry_status', $_POST['inquiry_status']);
            $config['wheres'][] = array('column_name' => 'e.inquiry_status_id', 'column_value' => $inquiry_status_id);
        }

        if (!empty($_POST['select_lead_inquiry']) && $_POST['select_lead_inquiry'] != 'all') {
            //echo '<pre>';print_r($_POST['select_lead_inquiry']);
            $config['wheres'][] = array('column_name' => 'e.lead_or_inquiry', 'column_value' => $_POST['select_lead_inquiry']);
        }

        //$config['where_string'] = ' 1 = 1 ';

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            $config['wheres'][] = array('column_name' => 'p.created_by', 'column_value' => $this->session->userdata('is_logged_in')['staff_id']);
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            } else {
                
            }
        }

        $inquiry_status = $this->input->get_post('inquiry_status');
        $user_id = $this->input->get_post('user_id');
        if ($inquiry_status != '' && $inquiry_status != 'all') {
            $inquiry_status_id = $this->crud->get_id_by_val('inquiry_status', 'id', 'inquiry_status', $inquiry_status);

            $config['wheres'][] = array('column_name' => 'e.inquiry_status_id', 'column_value' => $inquiry_status_id);
        }
        if ($user_id != '' && $user_id != 'all') {
            $config['wheres'][] = array('column_name' => 'e.assigned_to_id', 'column_value' => $user_id);
        }
        if ($_POST['select_lead_inquiry'] != '' && $_POST['select_lead_inquiry'] != 'all') {
            $config['wheres'][] = array('column_name' => 'e.lead_or_inquiry', 'column_value' => $_POST['select_lead_inquiry']);
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");

        foreach ($list as $enquiry_row) {
            $row = array();
            $action = '';
            if ($role_edit && $role_delete) {
                $action .= '<a href="' . BASE_URL . "enquiry/add/" . $enquiry_row->inquiry_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                $action .= ' | <a onclick="return confirm(\'Are you sure ?\');" href="' . BASE_URL . "enquiry/delete_enquiry/" . $enquiry_row->inquiry_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
            } elseif ($role_edit) {
                $action .= '<a href="' . BASE_URL . "enquiry/add/" . $enquiry_row->inquiry_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
            } elseif ($role_delete) {
                $action .= '<a onclick="return confirm(\'Are you sure ?\');" href="' . BASE_URL . "enquiry/delete_enquiry/" . $enquiry_row->inquiry_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
            }
            $row[] = $action;
            $row[] = $enquiry_row->inquiry_no;
            $row[] = $enquiry_row->party_code;
            $row[] = $enquiry_row->party_name;
            $row[] = $enquiry_row->inquiry_status;
            $row[] = date('d-m-Y', strtotime($enquiry_row->inquiry_date));
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
            "sql" => $this->db->last_query(),
        );

        //output to json format
        echo json_encode($output);
    }

    function delete_inquiry($id) {
        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
        if ($role_delete) {
            $where_array = array("inquiry_id" => $id);
            $this->crud->delete("inquiry", $where_array);

            $session_data = array(
                'success_message' => "Inquiry has been deleted !"
            );
            $this->session->set_userdata($session_data);
            redirect(BASE_URL . "report/sales_lead_summary");
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function dispatch_summary() {
        if ($this->applib->have_access_role(REPORT_DISPATCH_SUMMARY_MENU_ID, "view")) {
            set_page('dispatch/challan_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function due_sales_order() {
        if ($this->applib->have_access_role(REPORT_LIST_OF_DUE_SALES_ORDER_MENU_ID, "view")) {
            set_page('report/due_sales_order');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function delay_quotation_report() {
        if ($this->applib->have_access_role(REPORT_DELAY_QUOTATION_REPORT_MENU_ID, "view")) {
            set_page('report/delay_quotation_report');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function delay_quotation_datatables() {
        $date = $_REQUEST['review_date'];
        $date = str_replace('/', '-', $date);
        $review_date = date('Y-m-d', strtotime($date));
        $config['table'] = 'quotations q';
        $config['select'] = 'q.id as quotation_id,q.quotation_no,q.enquiry_no,p.party_name,q.quotation_date,q.due_date';
        $config['column_order'] = array(null, 'q.id', 'q.enquiry_no', 'p.party_name', 'q.quotation_date', 'q.due_date');
        $config['column_search'] = array('q.quotation_no', 'q.enquiry_no', 'p.party_name', 'DATE_FORMAT(q.quotation_date,"%d-%m-%Y")', 'DATE_FORMAT(q.due_date,"%d-%m-%Y")');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = q.party_id', 'join_type' => 'left');
        $config['wheres'][] = array('column_name' => 'q.due_date <', 'column_value' => $review_date);
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $role_edit = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
        foreach ($list as $quotation_data) {
            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . base_url('quotation/add/' . $quotation_data->quotation_id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
            }
            $row[] = $action;
            $row[] = $quotation_data->quotation_no;
            $row[] = $quotation_data->enquiry_no;
            $row[] = $quotation_data->party_name;
            $row[] = date('d-m-Y', strtotime($quotation_data->quotation_date));
            $row[] = date('d-m-Y', strtotime($quotation_data->due_date));
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function pending_quotation_for_followup_report() {
        //echo "huroazsyediuasghediuj";exit;
        if ($this->applib->have_access_role(REPORT_BOM_INFO_REPORT_MENU_ID, "view")) {
            set_page('report/pending_quotation_for_followup');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function pending_quotation_for_followup_datatables() {
        $date = $_REQUEST['review_date'];
        $date = str_replace('/', '-', $date);
        $review_date = date('Y-m-d', strtotime($date));
        $config['table'] = 'quotations q';
        $config['select'] = 'q.id as quotation_id,q.quotation_no,q.enquiry_no,p.party_name,q.quotation_date,q.due_date';
        $config['column_order'] = array(null, 'q.quotation_no', 'q.enquiry_no', 'p.party_name', 'q.quotation_date', 'q.due_date');
        $config['column_search'] = array('q.quotation_no', 'q.enquiry_no', 'p.party_name', 'DATE_FORMAT(q.quotation_date,"%d-%m-%Y")', 'DATE_FORMAT(q.due_date,"%d-%m-%Y")');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = q.party_id', 'join_type' => 'left');
        $config['wheres'][] = array('column_name' => 'q.review_date <', 'column_value' => $review_date);
        $config['wheres'][] = array('column_name' => 'q.quotation_status_id', 'column_value' => $this->crud->get_id_by_val('quotation_status', 'id', 'LOWER(quotation_status)', 'hot'));
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $role_edit = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
        foreach ($list as $quotation_data) {
            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . base_url('quotation/add/' . $quotation_data->quotation_id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
            }
            $row[] = $action;
            $row[] = $quotation_data->quotation_no;
            $row[] = $quotation_data->enquiry_no;
            $row[] = $quotation_data->party_name;
            $row[] = date('d-m-Y', strtotime($quotation_data->quotation_date));
            $row[] = date('d-m-Y', strtotime($quotation_data->due_date));
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function due_sales_order_datatables() {
        $requestData = $_REQUEST;
        $date = $_REQUEST['review_date'];
        $date = str_replace('/', '-', $date);
        $review_date = date('Y-m-d', strtotime($date));
        $config['table'] = 'sales_order s';
        $config['select'] = 's.*,q.quotation_no,p.party_name, (select count(*) from sales_order_items where s.id = sales_order_id AND item_category_id = "' . ITEM_CATEGORY_FINISHED_GOODS_ID . '" group by sales_order_id) as count_items';
        $config['column_order'] = array(null, 'q.quotation_no', 's.sales_order_no', 'p.party_name', 's.sales_order_date', 's.committed_date');
        $config['column_search'] = array('q.quotation_no', 's.sales_order_no', 'p.party_name', 's.sales_order_date', 's.committed_date');

        $config['joins'][0] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id');
        $config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id', 'join_type' => 'left');
        $config['wheres'][] = array('column_name' => 'q.review_date <', 'column_value' => $review_date);
        //$config['where_string'] = ' 1 = 1 ';

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            /* $config['where_string'] .= ' AND ( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
              if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
              } else if ($cu_accessExport == 1) {
              $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_EXPORT_ID;
              } else if ($cu_accessDomestic == 1) {
              $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_DOMESTIC_ID;
              } else {
              } */
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");

        foreach ($list as $order_row) {
            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . base_url('sales_order/add/' . $order_row->id) . '"
				   target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
            }
            $row[] = $action;
            $row[] = $order_row->quotation_id;
            $row[] = $order_row->sales_order_no;
            $row[] = $order_row->party_name;
            $row[] = date('d-m-Y', strtotime($order_row->sales_order_date));
            $row[] = date('d-m-Y', strtotime($order_row->review_date));
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function commited_sales_order() {
        if ($this->applib->have_access_role(REPORT_LIST_OF_DUE_SALES_ORDER_MENU_ID, "view")) {
            set_page('report/commited_sales_order');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function commited_sales_order_datatables() {
        $requestData = $_REQUEST;
        $date = $_REQUEST['committed_date'];
        $date = str_replace('/', '-', $date);
        $review_date = date('Y-m-d', strtotime($date));
        $config['table'] = 'sales_order s';
        $config['select'] = 's.*,q.quotation_no,p.party_name, (select count(*) from sales_order_items where s.id = sales_order_id AND item_category_id = "' . ITEM_CATEGORY_FINISHED_GOODS_ID . '" group by sales_order_id) as count_items';
        $config['column_order'] = array(null, 'q.id', 's.id', 'p.party_name', 's.sales_order_date', 's.committed_date');
        $config['column_search'] = array('q.quotation_no', 's.sales_order_no', 'p.party_name', 's.sales_order_date', 's.committed_date');

        $config['joins'][0] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id');
        $config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id');
        //$config['wheres'][0] = array('column_name'=>'s.committed_date <','column_value'=>$review_date);
        //$config['wheres'][1] = array('column_name'=>'s.id not in (select sales_id from dispatch_challan)','column_value'=>" ");
        $config['where_string'] = " `s`.`committed_date` < '" . $review_date . "' AND `s`.`id` not in (select sales_order_id from challans) ";

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            /* $config['where_string'] .= ' AND ( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
              if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
              } else if ($cu_accessExport == 1) {
              $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_EXPORT_ID;
              } else if ($cu_accessDomestic == 1) {
              $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_DOMESTIC_ID;
              } else {
              } */
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");

        foreach ($list as $order_row) {
            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . base_url('sales_order/add/' . $order_row->id) . '"
				   target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
            }
            $row[] = $action;
            $row[] = $order_row->quotation_id;
            $row[] = $order_row->sales_order_no;
            $row[] = $order_row->party_name;
            $row[] = date('d-m-Y', strtotime($order_row->sales_order_date));
            $row[] = date('d-m-Y', strtotime($order_row->committed_date));
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function upcoming_dispatches_report() {
        if ($this->applib->have_access_role(REPORT_BOM_INFO_REPORT_MENU_ID, "view")) {
            set_page('report/upcoming_dispatches_report');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function upcoming_dispatches_report_datatables() {
        $requestData = $_REQUEST;
        $date = $_REQUEST['committed_date'];
        $date = str_replace('/', '-', $date);
        $review_date = date('Y-m-d', strtotime($date));
        $config['table'] = 'sales_order s';
        $config['select'] = 's.*,q.quotation_no,p.party_name, (select count(*) from sales_order_items where s.id = sales_order_id AND item_category_id = "' . ITEM_CATEGORY_FINISHED_GOODS_ID . '" group by sales_order_id) as count_items';
        $config['column_order'] = array(null, 'q.quotation_no', 's.sales_order_no', 'p.party_name', 's.sales_order_date', 's.committed_date');
        $config['column_search'] = array('q.quotation_no', 's.sales_order_no', 'p.party_name', 's.sales_order_date', 's.committed_date');

        $config['joins'][0] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id');
        $config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id');
        //$config['wheres'][0] = array('column_name'=>'s.committed_date <','column_value'=>$review_date);
        //$config['wheres'][1] = array('column_name'=>'s.id not in (select sales_id from dispatch_challan)','column_value'=>" ");
        $config['where_string'] = " `s`.`committed_date` >= '" . $review_date . "' AND `s`.`id` not in (select sales_order_id from challans) ";

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            /* $config['where_string'] .= ' AND ( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
              if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
              } else if ($cu_accessExport == 1) {
              $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_EXPORT_ID;
              } else if ($cu_accessDomestic == 1) {
              $config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_DOMESTIC_ID;
              } else {
              } */
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");

        foreach ($list as $order_row) {
            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . base_url('sales_order/add/' . $order_row->id) . '"
				   target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
            }
            $row[] = $action;
            $row[] = $order_row->quotation_id;
            $row[] = $order_row->sales_order_no;
            $row[] = $order_row->party_name;
            $row[] = date('d-m-Y', strtotime($order_row->sales_order_date));
            $row[] = date('d-m-Y', strtotime($order_row->committed_date));
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function payment_receipt_details() {
        if ($this->applib->have_access_role(REPORT_PAYMENT_RECEIPT_DETAILS_MENU_ID, "view")) {
            $data['party_list'] = $this->crud->get_all_with_where('party', 'party_name', 'ASC', array('active !=' => 0));
            set_page('finance/payment_list', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function pending_report() {
        if ($this->applib->have_access_role(REPORT_PENDING_REPORT_MENU_ID, "view")) {
            $data = $this->pending_report_data();
            set_page('report/pending_report', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function not_invoice_created_sales_order_report() {
        if ($this->applib->have_access_role(REPORT_PENDING_REPORT_MENU_ID, "view")) {
            $data = array();
            set_page('report/not_invoice_created_sales_order_report', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function not_invoice_created_sales_order_report_datatable() {
        $challan_sales_order_ids_db = $this->db->select('sales_order_id')->from('challans')->group_by('sales_order_id')->get()->result_array();
        $challan_sales_order_ids = array();
        foreach ($challan_sales_order_ids_db as $row) {
            $challan_sales_order_ids[] = $row['sales_order_id'];
        }
        
        $invoice_created_sales_order_ids = $this->crud->getFromSQL('SELECT `sales_order_id` FROM `challans` WHERE `id` IN (SELECT `challan_id` FROM `invoices`)');
        $invoice_created_sales_order_id_arr = array();
        foreach ($invoice_created_sales_order_ids as $invoice_created_sales_order_id){
            if(!empty($invoice_created_sales_order_id->sales_order_id)){
                if(in_array($invoice_created_sales_order_id->sales_order_id, $invoice_created_sales_order_id_arr)){ } else {
                    $invoice_created_sales_order_id_arr[] = $invoice_created_sales_order_id->sales_order_id;
                }
            }
        }
        
        $config['table'] = 'sales_order s';
        $config['select'] = 's.*,q.quotation_no,p.party_name, (select count(*) from sales_order_items where s.id = sales_order_id AND item_category_id = "' . ITEM_CATEGORY_FINISHED_GOODS_ID . '" group by sales_order_id) as count_items';
        $config['column_order'] = array(null, 'q.quotation_no', 's.sales_order_no', 'p.party_name', 's.sales_order_date', 's.committed_date');
        $config['column_search'] = array('q.quotation_no', 's.sales_order_no', 'p.party_name', 'DATE_FORMAT(s.sales_order_date,"%d-%m-%Y")', 'DATE_FORMAT(s.committed_date,"%d-%m-%Y")');
        $config['joins'][0] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id');
        $config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id');
        $config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
        if (isset($_POST['is_approved']) && $_POST['is_approved'] != 'all') {
            $config['wheres'][] = array('column_name' => 's.isApproved', 'column_value' => $_POST['is_approved']);
        }
        if(!empty($invoice_created_sales_order_id_arr)){
            $config['in_not_in_where'] = 's.id NOT IN ('.implode(',', $invoice_created_sales_order_id_arr).')';
        }

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            $config['custom_where'] = '(s.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR s.created_by IS NULL)';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            }
        }
        $config['group_by'] = 's.id';
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();

        $data = array();
        $role_delete = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "edit");

        foreach ($list as $order_row) {

            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . base_url('sales_order/add/' . $order_row->id) . '"
				   class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
            }
            if ($role_delete) {
                $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs"
				   data-href="' . base_url('sales_order/delete_sales_order/' . $order_row->id) . '"><i class="fa fa-trash"></i></a>';
            }

            if ($order_row->count_items > 1) {
                $action .= ' | <a href="#" class="print_button btn-primary btn-xs show_itemprint_modal" id="' . $order_row->id . '"><i class="fa fa-print"></i></a>';
            } else {
                $action .= ' | <a href="' . base_url('sales_order/sales_order_print/' . $order_row->id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
            }

            if ($order_row->count_items > 1) {
                $action .= ' | <a href="#" class="print_button show_work_order_itemprint_modal" id="' . $order_row->id . '">Work Order</a>';
            } else {
                $action .= ' | <a href="' . base_url('sales_order/work_order/' . $order_row->id) . '" target="_blank" class="">Work Order</a>';
            }

            $action .= ' | <a href="' . base_url('sales_order/sales_order_email/' . $order_row->id) . '" class="">Email</a>';
            $row[] = $action;
            $row[] = $order_row->quotation_id;
            $row[] = $order_row->sales_order_no;
            if (in_array($order_row->id, $challan_sales_order_ids)) {
                $row[] = "Dispatched";
            } else {
                $row[] = "Pending Dispatch";
            }

            if ($order_row->isApproved == 1) {
                $row[] = "Approved";
            } else {
                $row[] = "Not Approved";
            }
            $row[] = $order_row->party_name;
            $row[] = date('d-m-Y', strtotime($order_row->sales_order_date));
            $row[] = date('d-m-Y', strtotime($order_row->committed_date));

            if ($order_row->count_items > 0) {
                if (!empty($_POST['order_status']) && $_POST['order_status'] == 'all') {
                    $data[] = $row;
                } else if ($_POST['order_status'] == 'dispatched') {
                    if (in_array($order_row->id, $challan_sales_order_ids)) {
                        $data[] = $row;
                    }
                } else if ($_POST['order_status'] == 'not_dispatched') {
                    if (!in_array($order_row->id, $challan_sales_order_ids)) {
                        $data[] = $row;
                    }
                }
            }
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function pending_report_data() {
        $response = array();
        $response['total_pending_enquiry'] = $this->get_pending_enquiry();
        $response['total_pending_quotation_for_followup'] = $this->pending_quotation_for_followup();
        $response['total_due_dispatch'] = $this->get_due_dispatch();
        $response['total_upcoming_dispatch'] = $this->get_upcoming_dispatch();
        $response['total_pending_sales_order_for_approval'] = $this->get_pending_sales_order_for_approval();
        $response['total_not_invoice_created_sales_order'] = $this->get_not_invoice_created_sales_order();
        return $response;
    }

    /**
     * @param bool $return_total
     * @return array|int
     */
    function get_pending_enquiry($return_total = true) {
        $query = $this->db->get_where('inquiry', array('inquiry_status_id' => $this->crud->get_id_by_val('inquiry_status', 'id', 'LOWER(inquiry_status)', 'pending'), 'lead_or_inquiry' => 'enquiry'));
        if ($return_total == true) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    /**
     * @param bool $return_total
     * @return array|int
     */
    function pending_quotation_for_followup($return_total = true) {
        $query = $this->db->get_where('quotations', array('review_date <=' => date('Y-m-d'), 'quotation_status_id' => $this->crud->get_id_by_val('quotation_status', 'id', 'LOWER(quotation_status)', 'hot')));
        if ($return_total == true) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    /**
     * @param bool $return_total
     * @return array|int
     */
    function get_due_dispatch($return_total = true) {
        $this->db->select('s.id');
        $this->db->from('sales_order s');
        $this->db->join('quotations q', 's.quotation_id = q.id');
        $this->db->join('party p', 'p.party_id = s.sales_to_party_id');
        $this->db->where('committed_date < ', date('Y-m-d'));
        $this->db->where('s.id NOT IN (SELECT sales_order_id FROM challans)', NULL, FALSE);
        $query = $this->db->get();
        if ($return_total == true) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    /**
     * @param bool $return_total
     * @return array|int
     */
    function get_upcoming_dispatch($return_total = true) {
        $this->db->select('id');
        $this->db->from('sales_order');
        $this->db->where('committed_date >= ', date('Y-m-d'));
        $this->db->where('id NOT IN (SELECT sales_order_id FROM challans)', NULL, FALSE);
        $query = $this->db->get();
        if ($return_total == true) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    /**
     * @param bool $return_total
     * @return array|int
     */
    function get_pending_sales_order_for_approval($return_total = true) {
        $this->db->select('s.id');
        $this->db->from('sales_order s');
        $this->db->join('party p', 'p.party_id = s.sales_to_party_id');
        $this->db->where('s.isApproved', 0);
        $this->db->where('p.active !=', 0);
        $query = $this->db->get();
        if ($return_total == true) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    /**
     * @param bool $return_total
     * @return array|int
     */
    function get_not_invoice_created_sales_order($return_total = true) {
        
        $invoice_created_sales_order_ids = $this->crud->getFromSQL('SELECT `sales_order_id` FROM `challans` WHERE `id` IN (SELECT `challan_id` FROM `invoices`)');
        $invoice_created_sales_order_id_arr = array();
        foreach ($invoice_created_sales_order_ids as $invoice_created_sales_order_id){
            if(!empty($invoice_created_sales_order_id->sales_order_id)){
                if(in_array($invoice_created_sales_order_id->sales_order_id, $invoice_created_sales_order_id_arr)){ } else {
                    $invoice_created_sales_order_id_arr[] = $invoice_created_sales_order_id->sales_order_id;
                }
            }
        }
        
        $this->db->select('s.id');
        $this->db->from('sales_order s');
        $this->db->join('party p', 'p.party_id = s.sales_to_party_id');
        $this->db->join('quotations q', 's.quotation_id = q.id');
        $this->db->where('p.active != ', 0);
        if(!empty($invoice_created_sales_order_id_arr)){
            $this->db->where_not_in('s.id', $invoice_created_sales_order_id_arr);
        }
        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            $this->db->where('(s.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR s.created_by IS NULL)');
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $this->db->where('p.party_type_1', PARTY_TYPE_DOMESTIC_ID);
            }
        }
        $query = $this->db->get();
        if ($return_total == true) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function list_of_inquiry() {
        $sql_list_query = "SELECT e.inquiry_id, e.inquiry_no, s.inquiry_status,p.party_name,p.party_code,e.inquiry_date
							from inquiry e
							LEFT JOIN inquiry_status s ON s.id = e.inquiry_status_id
							LEFT JOIN party p ON p.party_id = e.party_id";

        $condition = ' WHERE 1';

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            
        } else {
            $condition .= ' AND ( p.`created_by`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.`created_by` IS NULL )';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
            } else if ($cu_accessDomestic == 1) {
                $condition .= ' AND p.`party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
            } else {
                
            }
        }
        $status = $this->input->get_post('status');
        $user_id = $this->input->get_post('user_id');
        if ($status != '' && $status != 'all') {
            $condition .= " AND  s.inquiry_status = '" . $status . "' ";
        }

        if ($user_id != '' && $user_id != 'all') {
            $condition .= " AND  e.created_by = '" . $user_id . "' ";
        }

        $query = $this->db->query($sql_list_query . $condition);
        $enquiries = $query->result_array();

        $this->db->order_by('id');
        $enquiry_status = $this->db->get('inquiry_status')->result();

        $this->db->order_by('name');
        $users = $this->db->where('active !=', 0)->get('staff')->result();
        if ($this->applib->have_access_role(REPORT_LIST_OF_INQUIRY_MENU_ID, "view")) {
            set_page('sales/enquiry/enquiry_list', array('enquiries' => $enquiries, 'enquiry_status' => $enquiry_status, 'users' => $users));
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function list_of_quotation() {
        $role_view = $this->app_model->have_access_role(REPORT_LIST_OF_QUOTATION_MENU_ID, "view");
        if ($role_view) {
            $data = array();
            set_page('sales/quotation/quotation_list', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function list_of_sales_order() {
        if ($this->applib->have_access_role(REPORT_LIST_OF_SALES_ORDER_MENU_ID, "view")) {
            set_page('sales/order/order_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function sales_order_status() {
        if ($this->applib->have_access_role(REPORT_SALES_ORDER_STATUS_MENU_ID, "view")) {
            set_page('sales/order/order_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function quotation_followup_history() {
        $data = array();
        $data['quotation_status_data'] = $this->db->get('quotation_status')->result();
        $data['staff_data'] = $this->db->select('staff_id,name')->get('staff')->result();
        if ($this->applib->have_access_role(REPORT_QUOTATION_FOLLOWUP_HISTORY_MENU_ID, "view")) {
            set_page('report/quotation_followup_history', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function quotation_followup_history_datatable() {
        $config['table'] = 'followup_history fh';
        $config['select'] = 'fh.*,quotation_status.quotation_status,q.enquiry_no,q.id as quotation_id,q.quotation_no,p.party_name,p.email_id as party_email_id,p.phone_no as party_phone_no,city.city,state.state,country.country,staff.name as staff_name,staff.email as staff_email,staff.contact_no as staff_contact_no';
        $config['column_order'] = array(null, null, 'fh.followup_date', 'fh.history', 'quotation_status.quotation_status', 'q.id', 'q.enquiry_no', 'p.party_name', 'p.email_id', 'p.phone_no', 'city.city', 'state.state', 'country.country', 'staff.name', 'staff.email', 'staff.contact_no');
        $config['column_search'] = array('DATE_FORMAT(fh.followup_date,"%d-%m-%Y")', 'fh.history', 'quotation_status.quotation_status', 'q.quotation_no', 'q.enquiry_no', 'p.party_name', 'p.email_id', 'p.phone_no', 'city.city', 'state.state', 'country.country', 'staff.name', 'staff.email', 'staff.contact_no');
        $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'q.id = fh.quotation_id');
        $config['joins'][] = array('join_table' => 'quotation_status', 'join_by' => 'quotation_status.id = q.quotation_status_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = q.party_id');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff', 'join_by' => 'staff.staff_id = fh.followup_by');
        $config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');

        if (!empty($_POST['filter_from_date'])) {
            $config['wheres'][] = array('column_name' => 'DATE(fh.followup_date) >=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_from_date'])));
        }

        if (!empty($_POST['filter_to_date']) && $_POST['filter_to_date'] != 'all') {
            $config['wheres'][] = array('column_name' => 'DATE(fh.followup_date) <=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_to_date'])));
        }

        if (!empty($_POST['filter_status']) && $_POST['filter_status'] != 'all') {
            $config['wheres'][] = array('column_name' => 'q.quotation_status_id', 'column_value' => $_POST['filter_status']);
        }

        if (!empty($_POST['filter_followup_by']) && $_POST['filter_followup_by'] != 'all') {
            $config['wheres'][] = array('column_name' => 'staff.staff_id', 'column_value' => $_POST['filter_followup_by']);
        }

        if (!$this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
            $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
            $config['custom_where'] = '( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            }
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $isEdit = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
        $data = array();
        $cnt = 1;
        foreach ($list as $row_data) {
            $row = array();
            $action = '';
            if ($isEdit) {
                $action .= ' <a href="' . base_url('quotation/add/' . $row_data->quotation_id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
            }

            $row[] = $action;
            $row[] = $cnt;

            $row[] = date('d-m-Y', strtotime($row_data->followup_date));
            $row[] = $row_data->history;
            $row[] = $row_data->quotation_status;
            $row[] = $row_data->quotation_no;
            $row[] = $row_data->enquiry_no;
            $row[] = $row_data->party_name;
            $row[] = str_replace(',', '<br />', $row_data->party_email_id);
            $row[] = str_replace(',', '<br />', $row_data->party_phone_no);
            $row[] = $row_data->city;
            $row[] = $row_data->state;
            /* $row[] = $row_data->country;
              $row[] = $row_data->staff_name;
              $row[] = $row_data->staff_email;
              $row[] = $row_data->staff_contact_no; */
            $data[] = $row;
            $cnt++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function enquiry_followup_history_datatable() {
        $config['table'] = 'inquiry_followup_history fh';
        $config['select'] = 'fh.*,inquiry_status.inquiry_status,i.inquiry_id,i.inquiry_no,p.party_name,p.email_id as party_email_id,p.phone_no as party_phone_no,city.city,state.state,country.country,staff.name as staff_name,staff.email as staff_email,staff.contact_no as staff_contact_no';
        $config['column_order'] = array(null, 'fh.followup_date', 'fh.history', 'inquiry_status.inquiry_status', 'i.inquiry_id', 'p.party_name', 'p.email_id', 'p.phone_no', 'city.city', 'state.state', 'country.country', 'staff.name', 'staff.email', 'staff.contact_no');
        $config['column_search'] = array('DATE_FORMAT(fh.followup_date,"%d-%m-%Y")', 'fh.history', 'inquiry_status.inquiry_status', 'i.inquiry_no', 'p.party_name', 'p.email_id', 'p.phone_no', 'city.city', 'state.state', 'country.country', 'staff.name', 'staff.email', 'staff.contact_no');
        $config['joins'][] = array('join_table' => 'inquiry i', 'join_by' => 'i.inquiry_id = fh.inquiry_id');
        $config['joins'][] = array('join_table' => 'inquiry_status', 'join_by' => 'inquiry_status.id = i.inquiry_status_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = i.party_id');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff', 'join_by' => 'staff.staff_id = fh.followup_by');
        $config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');

        if (!empty($_POST['filter_from_date'])) {
            $config['wheres'][] = array('column_name' => 'DATE(fh.followup_date) >=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_from_date'])));
        }

        if (!empty($_POST['filter_to_date']) && $_POST['filter_to_date'] != 'all') {
            $config['wheres'][] = array('column_name' => 'DATE(fh.followup_date) <=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_to_date'])));
        }

        if (!empty($_POST['filter_status']) && $_POST['filter_status'] != 'all') {
            $config['wheres'][] = array('column_name' => 'i.inquiry_status_id', 'column_value' => $_POST['filter_status']);
        }

        if (!empty($_POST['filter_followup_by']) && $_POST['filter_followup_by'] != 'all') {
            $config['wheres'][] = array('column_name' => 'staff.staff_id', 'column_value' => $_POST['filter_followup_by']);
        }

        if (!$this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
            $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
            $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
            $config['custom_where'] = '( p.created_by = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.created_by IS NULL )';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            }
        }

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");
        $data = array();
        $cnt = 1;
        foreach ($list as $row_data) {
            $row = array();
            $action = '';
            if ($role_edit) {
                $action .= '<a href="' . BASE_URL . "enquiry/add/" . $row_data->inquiry_id . '" target="_blank" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
            }
            $row[] = $action;
            //$row[] = $cnt;
            $row[] = date('d-m-Y', strtotime($row_data->followup_date));
            $row[] = $row_data->history;
            $row[] = $row_data->inquiry_status;
            $row[] = $row_data->inquiry_no;
            $row[] = $row_data->party_name;
            $row[] = str_replace(',', '<br />', $row_data->party_email_id);
            $row[] = str_replace(',', '<br />', $row_data->party_phone_no);
            $row[] = $row_data->city;
            $row[] = $row_data->state;
            $row[] = $row_data->country;
            $row[] = $row_data->staff_name;
            $row[] = $row_data->staff_email;
            $row[] = $row_data->staff_contact_no;
            $data[] = $row;
            $cnt++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function bom_info() {
        $data = array();
        $data['module_data'] = array("quotation" => "Quotation", "sales_order" => "Sales Order", "dispatch_challan" => "Dispatch Challan", "invoice" => "Invoice");
        if ($this->applib->have_access_role(REPORT_BOM_INFO_REPORT_MENU_ID, "view")) {
            set_page('report/bom_info', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            //redirect("/");
        }
    }

    function bom_info_datatable() {
        $config['table'] = 'items i';
        $select = 'i.item_name,';
        if ($_POST['filter_module'] == 'quotation') {
            $select .= 'SUM(qi.quantity) as quantity,SUM(qi.rate) as sell_amount,(SELECT SUM(ib.total_amount) FROM item_bom ib WHERE ib.item_id = i.id) as one_qty_amt';
            $config['joins'][] = array('join_table' => 'quotation_items qi', 'join_by' => 'qi.item_id = i.id');
            $config['joins'][] = array('join_table' => 'quotations q', 'join_by' => 'q.id = qi.quotation_id');
            if (!empty($_POST['filter_from_date'])) {
                $config['wheres'][] = array('column_name' => 'DATE(q.quotation_date) >=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_from_date'])));
            }
            if (!empty($_POST['filter_to_date']) && $_POST['filter_to_date'] != 'all') {
                $config['wheres'][] = array('column_name' => 'DATE(q.quotation_date) <=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_to_date'])));
            }
        } elseif ($_POST['filter_module'] == 'sales_order') {
            $select .= 'SUM(soi.quantity) as quantity,SUM(soi.rate) as sell_amount,(SELECT SUM(ib.total_amount) FROM item_bom ib WHERE ib.item_id = i.id) as one_qty_amt';
            $config['joins'][] = array('join_table' => 'sales_order_items soi', 'join_by' => 'soi.item_id = i.id');
            $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = soi.sales_order_id');
            if (!empty($_POST['filter_from_date'])) {
                $config['wheres'][] = array('column_name' => 'DATE(so.sales_order_date) >=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_from_date'])));
            }
            if (!empty($_POST['filter_to_date']) && $_POST['filter_to_date'] != 'all') {
                $config['wheres'][] = array('column_name' => 'DATE(so.sales_order_date) <=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_to_date'])));
            }
        } elseif ($_POST['filter_module'] == 'dispatch_challan') {
            $select .= 'SUM(ci.quantity) as quantity,SUM(ci.rate) as sell_amount,(SELECT SUM(ib.total_amount) FROM item_bom ib WHERE ib.item_id = i.id) as one_qty_amt';
            $config['joins'][] = array('join_table' => 'challan_items ci', 'join_by' => 'ci.item_id = i.id');
            $config['joins'][] = array('join_table' => 'challans ch', 'join_by' => 'ch.id = ci.challan_id');
            if (!empty($_POST['filter_from_date'])) {
                $config['wheres'][] = array('column_name' => 'DATE(ch.challan_date) >=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_from_date'])));
            }
            if (!empty($_POST['filter_to_date']) && $_POST['filter_to_date'] != 'all') {
                $config['wheres'][] = array('column_name' => 'DATE(ch.challan_date) <=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_to_date'])));
            }
        } elseif ($_POST['filter_module'] == 'invoice') {
            $select .= 'SUM(ii.quantity) as quantity,SUM(ii.rate) as sell_amount,(SELECT SUM(ib.total_amount) FROM item_bom ib WHERE ib.item_id = i.id) as one_qty_amt';
            $config['joins'][] = array('join_table' => 'invoice_items ii', 'join_by' => 'ii.item_id = i.id');
            $config['joins'][] = array('join_table' => 'invoices inv', 'join_by' => 'inv.id = ii.invoice_id');
            if (!empty($_POST['filter_from_date'])) {
                $config['wheres'][] = array('column_name' => 'DATE(inv.sales_order_date) >=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_from_date'])));
            }
            if (!empty($_POST['filter_to_date']) && $_POST['filter_to_date'] != 'all') {
                $config['wheres'][] = array('column_name' => 'DATE(inv.sales_order_date) <=', 'column_value' => date('Y-m-d', strtotime($_POST['filter_to_date'])));
            }
        }
        $config['select'] = $select;
        $config['column_order'] = array('i.item_name', 'quantity');
        $config['column_search'] = array('i.item_name', 'quantity');

        $config['group_by'] = 'i.id';
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $cnt = 1;
        foreach ($list as $row_data) {
            $row = array();
            $row[] = $row_data->item_name;
            $row[] = $row_data->quantity;
            $row[] = $row_data->one_qty_amt;
            $amount = $row_data->quantity * $row_data->one_qty_amt;
            $row[] = $amount;
            $row[] = $row_data->sell_amount;
            $profit = $row_data->sell_amount - $amount;
            $row[] = $profit;
            $data[] = $row;
            $cnt++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function summary_report() {
        $logged_in = $this->session->userdata("is_logged_in");
        $id = $logged_in['staff_id'];

        $today_pending_inquiry = $this->crud->today_pending_inquiry($id, true);
        $overdue_pending_inquiry = $this->crud->today_pending_inquiry($id);
        $data['pending_inquiry'] = count($overdue_pending_inquiry) + count($today_pending_inquiry);

        $today_pending_lead = $this->crud->today_pending_lead($id, true);
        $overdue_pending_lead = $this->crud->today_pending_lead($id);
        $data['pending_lead'] = count($today_pending_lead) + count($overdue_pending_lead);

        $data['pending_quotation_folloup'] = $this->crud->pending_quotation_folloup($id);

        $today_dispatch = $this->crud->today_sales_order($id, true);
        $overdue_dispatch = $this->crud->today_sales_order($id);
        $data['due_dispatch'] = count($overdue_dispatch) + count($today_dispatch);

        $upcoming_dispatch = $this->crud->today_sales_order($id, true, true);
        $data['upcoming_dispatch'] = count($upcoming_dispatch);

        $today_approved = $this->crud->pending_sales_order_approved($id, true);
        $overdue_approved = $this->crud->pending_sales_order_approved($id);
        $data['sales_approval'] = count($overdue_approved) + count($today_approved);

        $inquiry_followup_t = $this->crud->pending_inquiry_followup($id, true);
        $inquiry_followup_o = $this->crud->pending_inquiry_followup($id);
        $data['inquiry_followup'] = count($inquiry_followup_t) + count($inquiry_followup_o);

        $pending_invoice_t = $this->crud->pending_sales_order_invoice($id, true);
        $pending_invoice_o = $this->crud->pending_sales_order_invoice($id);
        $data['pending_invoice'] = count($pending_invoice_t) + count($pending_invoice_o);

        //echo $this->db->last_query();exit;
        //echo '<pre>';print_r($data);exit;
        if ($this->applib->have_access_role(SUMMARY_REPORT_MENU_ID, "view")) {
            set_page('report/summary_report', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function employee_performance_chart() {
        $data = array();
        if ($this->applib->have_access_role(EMPLOYEE_PERFORMANCE_CHART_MODULE_ID, "view")) {
            set_page('report/employee_performance_chart', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
    
    function get_employee_performance_chart_value(){
        $return = array();
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
        $staff_id = $post_data['staff_id'];
        $from_date = date('Y-m-d', strtotime($post_data['from_date']));
        $to_date = date('Y-m-d', strtotime($post_data['to_date']));
        $return['enquiry'] = $this->crud->get_all_enquiry($staff_id, $from_date, $to_date);
        $return['quotation'] = $this->crud->get_all_quotation($staff_id, $from_date, $to_date);
        $return['sales_order'] = $this->crud->get_all_sales_order_without_cancel($staff_id, $from_date, $to_date);
        $return['pending_sales_order'] = $this->crud->get_all_sales_order($staff_id, $from_date, $to_date,PENDING_DISPATCH_ORDER_ID);
        $return['dispach_sales_order'] = $this->crud->get_all_sales_order($staff_id, $from_date, $to_date,DISPATCHED_ORDER_ID);
        $return['canceled_sales_order'] = $this->crud->get_all_sales_order($staff_id, $from_date, $to_date,CANCELED_ORDER_ID);
        $return['proforma_invoice'] = $this->crud->get_all_proforma_invoice($staff_id, $from_date, $to_date);
        $return['challan'] = $this->crud->get_all_challan($staff_id, $from_date, $to_date);
        $return['invoice'] = $this->crud->get_all_invoice($staff_id, $from_date, $to_date);
        print json_encode($return);
        exit;
    }

    public function sales_chart() {
        $data = array();
        if ($this->applib->have_access_role(SALES_CHART_MODULE_ID, "view")) {
            set_page('report/sales_chart', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function get_chart_data_from_date() {
        $return = array();
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
        $from_date = date('Y-m-d', strtotime($post_data['from_date']));
        $to_date = date('Y-m-d', strtotime($post_data['to_date']));
        $order_status = $post_data['order_status'];
        $quotation_status_id = $post_data['quotation_status_id'];
        $return['chart_data'] = $this->crud->get_chart_data_as_per_date($from_date, $to_date, $order_status, $quotation_status_id);
//        echo '<pre>'; print_r($return); exit;
        print json_encode($return);
        exit;
    }

    function item_costing_report() {
        if ($this->applib->have_access_role(ITEM_COSTING_REPORT_MODULE_ID, "view")) {
            set_page('report/item_costing_report');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function item_costing_datatable() {

        /* Update pending_qty in purchase_invoice_items table */
        $this->db->set('pending_qty', 'quantity', false)->update('purchase_invoice_items');

//        $items = $this->crud->get_all_records('items','id','item_name');
        $config['table'] = 'items';
        $config['select'] = '*';
        $config['column_order'] = array('item_name', 'item_code1', null);
        $config['column_search'] = array('item_name', 'item_code1');
        $config['order'] = array('id' => 'asc');
        $this->load->library('datatables', $config, 'datatable');
        $items = $this->datatable->get_datatables();
        $data = array();
        foreach ($items as $item) {
            $purchase_amount = 0;
            $result = $this->applib->get_purchase_amount_by_item_id($item->id);
            $purchase_amount = $result['purchase_amount'];
            $row = array();
            $row[] = '<a href="' . base_url() . 'report/item_costing_details/' . $item->id . '" target="_blank">' . $item->item_name . '</a>';
            $row[] = $item->item_code1;
            $row[] = (isset($result['latest_bom']->created_at) && !empty($result['latest_bom']->created_at)) ? date('d-m-Y', strtotime($result['latest_bom']->created_at)) : '';
            $row[] = (isset($result['latest_bom']->effective_date) && !empty($result['latest_bom']->effective_date)) ? date('d-m-Y', strtotime($result['latest_bom']->effective_date)) : '';
            $row[] = number_format((float)$purchase_amount, 2, '.', '');
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function item_costing_details($item_id) {
        $data = array();
        $data['item_name'] = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $item_id));
        $data['item_code'] = $this->crud->get_column_value_by_id('items', 'item_code1', array('id' => $item_id));

        $purchase_items_data = $this->applib->get_purchase_items_by_item_id($item_id);
        $data['latest_bom'] = $purchase_items_data['latest_bom'];
        $data['purchase_items'] = $purchase_items_data['purchase_items'];
        $data['expense_amount'] = $purchase_items_data['expense_amount'];

//        echo '<pre>'; print_r($data); exit;
        if ($this->applib->have_access_role(ITEM_DIFF_REPORT_MODULE_ID, "view")) {
            set_page('report/item_costing_details', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function item_diff_report() {
        if ($this->applib->have_access_role(ITEM_DIFF_REPORT_MODULE_ID, "view")) {
            set_page('report/item_diff_report');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    
    function production_item_diff_datatable() {
        /* Update pending_qty in purchase_invoice_items table */
//        $this->db->set('pending_qty', 'quantity', false)->update('purchase_invoice_items');

        $config['table'] = 'production p';
        $config['select'] = 'p.production_id AS id,p.finished_at,p.sales_order_id,p.proforma_invoice_id,p.item_id,i.item_name,i.item_code1 as item_code,so.sales_order_no,pi.proforma_invoice_no';

        $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = p.sales_order_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = p.proforma_invoice_id', 'join_type' => 'left');

        $config['joins'][] = array('join_table' => 'items i', 'join_by' => 'i.id = p.item_id', 'join_type' => 'left');
//        $config['column_order'] = array('i.item_name', 'i.item_code', null, null, null);
//        $config['column_search'] = array('i.item_name', 'i.item_code');
        $config['order'] = array('p.production_id' => 'desc');
        $config['where_string'] = ' 1 = 1 ';
        if (!empty($_POST['from_date']) && !empty($_POST['to_date'])) {
            $from_date = date('Y-m-d 00:00:00', strtotime($_POST['from_date']));
            $to_date = date('Y-m-d 23:59:00', strtotime($_POST['to_date']));
            $config['where_string'] .= ' AND p.finished_at between "' . $from_date . '" AND "' . $to_date . '" ';
        }
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
//        echo $this->db->last_query(); exit;
        $purchase_amount_total = 0;
        $sell_amount_total = 0;
        $difference_total = 0;

        /* Start Item Wise Difference Report */
        $item_diff_report_arr = array();
        $item_arr = array();
        $item_id = '';
        foreach ($list as $challan) {
            
            $sell_amount = 0;
            if (!empty($challan->sales_order_no)) {
                $sales_order_items_sql = $this->db->query("SELECT rate,igst,cgst,sgst FROM `sales_order_items` WHERE `sales_order_id` = '" . $challan->sales_order_id . "' AND item_id = '" . $challan->item_id . "'");
                $sales_order_items = $sales_order_items_sql->row_array();
                if (!empty($sales_order_items)) {
                    $igst_amount = $sales_order_items['rate'] * $sales_order_items['igst'] / 100;
                    $cgst_amount = $sales_order_items['rate'] * $sales_order_items['cgst'] / 100;
                    $sgst_amount = $sales_order_items['rate'] * $sales_order_items['sgst'] / 100;
                    $sell_amount = $sales_order_items['rate'] + $igst_amount + $cgst_amount + $sgst_amount;
                }
            } else {
                $proforma_invoice_items_sql = $this->db->query("SELECT rate,igst,cgst,sgst FROM `proforma_invoice_items` WHERE `proforma_invoice_id` = '" . $challan->proforma_invoice_id . "' AND item_id = '" . $challan->item_id . "'");
                $proforma_invoice_items = $proforma_invoice_items_sql->row_array();
                if (!empty($proforma_invoice_items)) {
                    $igst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['igst'] / 100;
                    $cgst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['cgst'] / 100;
                    $sgst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['sgst'] / 100;
                    $sell_amount = $proforma_invoice_items['rate'] + $igst_amount + $cgst_amount + $sgst_amount;
                }
            }
            
            $purchase_amount = 0;
            if (in_array($challan->item_id, $item_arr)) { } else { 
                $result = $this->applib->get_purchase_amount_by_item_id_for_production($challan->item_id, $from_date, $to_date);
                $purchase_amount = $result['purchase_amount'];
                $item_id = $challan->item_id;
            }
            $difference_amount = $sell_amount - $purchase_amount;

            if (in_array($challan->item_id, $item_arr)) {
                
                $item_diff_report_arr[$challan->item_id]['item_count'] = $item_diff_report_arr[$challan->item_id]['item_count'] + 1;
                $item_diff_report_arr[$challan->item_id]['purchase_amount'] = $item_diff_report_arr[$challan->item_id]['purchase_amount'] + $purchase_amount;
                $item_diff_report_arr[$challan->item_id]['sell_amount'] = $item_diff_report_arr[$challan->item_id]['sell_amount'] + $sell_amount;
                $item_diff_report_arr[$challan->item_id]['difference_amount'] = $item_diff_report_arr[$challan->item_id]['difference_amount'] + $difference_amount;
            } else {
                $item_diff_report_arr[$challan->item_id]['item_count'] = 1;
                $item_diff_report_arr[$challan->item_id]['item_id'] = $challan->item_id;
                $item_diff_report_arr[$challan->item_id]['item_name'] = $challan->item_name;
                $item_diff_report_arr[$challan->item_id]['item_code'] = $challan->item_code;
                $item_diff_report_arr[$challan->item_id]['purchase_amount'] = $purchase_amount;
                $item_diff_report_arr[$challan->item_id]['sell_amount'] = $sell_amount;
                $item_diff_report_arr[$challan->item_id]['difference_amount'] = $difference_amount;
            }
            $item_arr[] = $challan->item_id;

            $purchase_amount_total = $purchase_amount_total + $purchase_amount;
            $sell_amount_total = $sell_amount_total + $sell_amount;
        }

        foreach ($item_diff_report_arr as $item_diff_report) {
            $row = array();
            $row[] = '<a href="' . base_url() . 'report/item_diff_details_party_wise/' . $item_diff_report['item_id'] . '/' . $item_diff_report['item_count'] . '/' . $_POST['from_date'] . '/' . $_POST['to_date'] . '" target="_blank">' . $item_diff_report['item_name'] . '</a>';
            $row[] = $item_diff_report['item_code'];
            $row[] = number_format((float)$item_diff_report['purchase_amount'], 2, '.', '');
            $row[] = number_format((float)$item_diff_report['sell_amount'], 2, '.', '');
            $row[] = number_format((float)$item_diff_report['difference_amount'], 2, '.', '');
            $data[] = $row;
        }
        /* End Item Wise Difference Report */

        $row = array();
        $row[] = '';
        $row[] = '<b>Total</b>';
        $row[] = '<b>' . number_format((float)$purchase_amount_total, 2, '.', '') . '</b>';
        $row[] = '<b>' . number_format((float)$sell_amount_total, 2, '.', '') . '</b>';
        $difference_total = $sell_amount_total - $purchase_amount_total;
        $row[] = '<b>' . number_format((float)$difference_total, 2, '.', '') . '</b>';
        $data[] = $row;

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function item_diff_details_party_wise($item_id, $item_count, $from_date, $to_date) {
        if ($this->applib->have_access_role(ITEM_DIFF_REPORT_MODULE_ID, "view")) {
            $data['item_name'] = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $item_id));
            $data['item_code'] = $this->crud->get_column_value_by_id('items', 'item_code1', array('id' => $item_id));
            $data['item_count'] = $item_count;
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            $data['item_id'] = $item_id;
            set_page('report/item_diff_details_party_wise', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    
    function production_item_diff_party_wise_datatable() {
        /* Update pending_qty in purchase_invoice_items table */
//        $this->db->set('pending_qty', 'quantity', false)->update('purchase_invoice_items');

        $config['table'] = 'production p';
        $config['select'] = 'p.production_id AS id,p.finished_at,p.sales_order_id,p.proforma_invoice_id,p.item_id,i.item_name,i.item_code1 as item_code,so.sales_order_no,pi.proforma_invoice_no,pt.party_name AS sales_party_name,ptt.party_name AS proforma_party_name';

        $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = p.sales_order_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = p.proforma_invoice_id', 'join_type' => 'left');

        $config['joins'][] = array('join_table' => 'items i', 'join_by' => 'i.id = p.item_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party pt', 'join_by' => 'pt.party_id=so.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'party ptt', 'join_by' => 'ptt.party_id=pi.sales_to_party_id', 'join_type' => 'left');
//        $config['column_order'] = array('i.item_name', 'i.item_code', null, null, null);
//        $config['column_search'] = array('i.item_name', 'i.item_code');
        $config['order'] = array('p.production_id' => 'desc');
        $config['where_string'] = ' 1 = 1 AND p.item_id = "'.$_POST['item_id'].'"';
        if (!empty($_POST['from_date']) && !empty($_POST['to_date'])) {
            $from_date = date('Y-m-d 00:00:00', strtotime($_POST['from_date']));
            $to_date = date('Y-m-d 23:59:00', strtotime($_POST['to_date']));
//            $config['where_string'] .= ' AND p.finished_at between "' . $from_date . '" AND "' . $to_date . '"';
        }
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
//        echo $this->db->last_query(); exit;
//        echo '<pre>'; print_r($list); exit;
        $purchase_amount_total = 0;
        $sell_amount_total = 0;
        $difference_total = 0;

        /* Start Item Wise Difference Report */
        $item_arr = array();
        foreach ($list as $production_party_row) {

            $purchase_amount = 0;
            $result = $this->applib->get_purchase_amount_by_item_id_for_production_wise($production_party_row->item_id, $production_party_row->id, $from_date, $to_date);
            $purchase_amount = $result['purchase_amount'];
            
            $sell_amount = 0;
            if (!empty($production_party_row->sales_order_no)) {
                $so_pi_no = $production_party_row->sales_order_no;
                $so_pi_party_name = $production_party_row->sales_party_name;
                $sales_order_items_sql = $this->db->query("SELECT rate,igst,cgst,sgst FROM `sales_order_items` WHERE `sales_order_id` = '" . $production_party_row->sales_order_id . "' AND item_id = '" . $production_party_row->item_id . "'");
                $sales_order_items = $sales_order_items_sql->row_array();
                if (!empty($sales_order_items)) {
                    $igst_amount = $sales_order_items['rate'] * $sales_order_items['igst'] / 100;
                    $cgst_amount = $sales_order_items['rate'] * $sales_order_items['cgst'] / 100;
                    $sgst_amount = $sales_order_items['rate'] * $sales_order_items['sgst'] / 100;
                    $sell_amount = $sales_order_items['rate'] + $igst_amount + $cgst_amount + $sgst_amount;
                }
            } else {
                $so_pi_no = $production_party_row->proforma_invoice_no;
                $so_pi_party_name = $production_party_row->proforma_party_name;
                $proforma_invoice_items_sql = $this->db->query("SELECT rate,igst,cgst,sgst FROM `proforma_invoice_items` WHERE `proforma_invoice_id` = '" . $production_party_row->proforma_invoice_id . "' AND item_id = '" . $production_party_row->item_id . "'");
                $proforma_invoice_items = $proforma_invoice_items_sql->row_array();
//                echo $this->db->last_query();
                if (!empty($proforma_invoice_items)) {
                    $igst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['igst'] / 100;
                    $cgst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['cgst'] / 100;
                    $sgst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['sgst'] / 100;
                    $sell_amount = $proforma_invoice_items['rate'] + $igst_amount + $cgst_amount + $sgst_amount;
                }
            }
            
            $difference_amount = $sell_amount - $purchase_amount;

            $row = array();
            $row[] = '<a href="' . base_url() . 'report/item_diff_details/' . $production_party_row->item_id . '/' . $production_party_row->id . '/' . $_POST['from_date'] . '/' . $_POST['to_date'] . '" target="_blank">' . $so_pi_party_name . '</a>';
            $row[] = $so_pi_no;
            $row[] = date('d-m-Y', strtotime($production_party_row->finished_at));
            $row[] = number_format((float)$purchase_amount, 2, '.', '');
            $row[] = number_format((float)$sell_amount, 2, '.', '');
            $row[] = number_format((float)$difference_amount, 2, '.', '');
            $data[] = $row;

            $purchase_amount_total = $purchase_amount_total + $purchase_amount;
            $sell_amount_total = $sell_amount_total + $sell_amount;
        }

        /* End Item Wise Difference Report */

        $row = array();
        $row[] = '';
        $row[] = '';
        $row[] = '<b>Total</b>';
        $row[] = '<b>' . number_format((float)$purchase_amount_total, 2, '.', '') . '</b>';
        $row[] = '<b>' . number_format((float)$sell_amount_total, 2, '.', '') . '</b>';
        $difference_total = $sell_amount_total - $purchase_amount_total;
        $row[] = '<b>' . number_format((float)$difference_total, 2, '.', '') . '</b>';
        $data[] = $row;

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function bom_item_diff_datatable() {
        /* Update pending_qty in purchase_invoice_items table */
        $this->db->set('pending_qty', 'quantity', false)->update('purchase_invoice_items');

        $config['table'] = 'challans c';
        $config['select'] = 'c.id,c.challan_date,c.sales_order_id,c.proforma_invoice_id,ci.item_id,i.item_name,i.item_code1 as item_code,so.sales_order_no,pi.proforma_invoice_no';
        $config['joins'][] = array('join_table' => 'challan_items ci', 'join_by' => 'ci.challan_id = c.id', 'join_type' => 'left');

        $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = c.sales_order_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = c.proforma_invoice_id', 'join_type' => 'left');

        $config['joins'][] = array('join_table' => 'items i', 'join_by' => 'i.id = ci.item_id', 'join_type' => 'left');
        $config['column_order'] = array('i.item_name', 'ci.item_code', null, null, null);
        $config['column_search'] = array('i.item_name', 'ci.item_code');
        $config['order'] = array('c.id' => 'desc');
        $config['where_string'] = ' 1 = 1 ';
        if (!empty($_POST['from_date']) && !empty($_POST['to_date'])) {
            $from_date = date('Y-m-d 00:00:00', strtotime($_POST['from_date']));
            $to_date = date('Y-m-d 23:59:00', strtotime($_POST['to_date']));
            $config['where_string'] .= ' AND c.`challan_date` between "' . $from_date . '" AND "' . $to_date . '" ';
        }
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $purchase_amount_total = 0;
        $sell_amount_total = 0;
        $difference_total = 0;

        /* Start Item Wise Difference Report */
        $item_diff_report_arr = array();
        $item_arr = array();
        foreach ($list as $challan) {

            $purchase_amount = 0;
            $result = $this->applib->get_purchase_amount_by_item_id($challan->item_id);
            $purchase_amount = $result['purchase_amount'];

            $sell_amount = 0;
            if (!empty($challan->sales_order_no)) {
                $sales_order_items_sql = $this->db->query("SELECT SUM(`rate`) as sell_amount FROM `sales_order_items` WHERE `sales_order_id` = '" . $challan->sales_order_id . "' ");
                $sales_order_items = $sales_order_items_sql->row_array();
                if (!empty($sales_order_items)) {
                    $sell_amount = $sales_order_items['sell_amount'];
                }
            } else {
                $proforma_invoice_items_sql = $this->db->query("SELECT SUM(`rate`) as sell_amount FROM `proforma_invoice_items` WHERE `proforma_invoice_id` = '" . $challan->proforma_invoice_id . "' ");
                $proforma_invoice_items = $proforma_invoice_items_sql->row_array();
                if (!empty($proforma_invoice_items)) {
                    $sell_amount = $proforma_invoice_items['sell_amount'];
                }
            }
            $difference_amount = $sell_amount - $purchase_amount;

            if (in_array($challan->item_id, $item_arr)) {
                $item_diff_report_arr[$challan->item_id]['item_count'] = $item_diff_report_arr[$challan->item_id]['item_count'] + 1;
                $item_diff_report_arr[$challan->item_id]['purchase_amount'] = $item_diff_report_arr[$challan->item_id]['purchase_amount'] + $purchase_amount;
                $item_diff_report_arr[$challan->item_id]['sell_amount'] = $item_diff_report_arr[$challan->item_id]['sell_amount'] + $sell_amount;
                $item_diff_report_arr[$challan->item_id]['difference_amount'] = $item_diff_report_arr[$challan->item_id]['difference_amount'] + $difference_amount;
            } else {
                $item_diff_report_arr[$challan->item_id]['item_count'] = 1;
                $item_diff_report_arr[$challan->item_id]['item_id'] = $challan->item_id;
                $item_diff_report_arr[$challan->item_id]['item_name'] = $challan->item_name;
                $item_diff_report_arr[$challan->item_id]['item_code'] = $challan->item_code;
                $item_diff_report_arr[$challan->item_id]['purchase_amount'] = $purchase_amount;
                $item_diff_report_arr[$challan->item_id]['sell_amount'] = $sell_amount;
                $item_diff_report_arr[$challan->item_id]['difference_amount'] = $difference_amount;
            }
            $item_arr[] = $challan->item_id;

            $purchase_amount_total = $purchase_amount_total + $purchase_amount;
            $sell_amount_total = $sell_amount_total + $sell_amount;
        }

        foreach ($item_diff_report_arr as $item_diff_report) {
            $row = array();
            $row[] = '<a href="' . base_url() . 'report/item_diff_details/' . $item_diff_report['item_id'] . '/' . $item_diff_report['item_count'] . '/' . $_POST['from_date'] . '/' . $_POST['to_date'] . '" target="_blank">' . $item_diff_report['item_name'] . '</a>';
            $row[] = $item_diff_report['item_code'];
            $row[] = number_format((float)$item_diff_report['purchase_amount'], 2, '.', '');
            $row[] = number_format((float)$item_diff_report['sell_amount'], 2, '.', '');
            $row[] = number_format((float)$item_diff_report['difference_amount'], 2, '.', '');
            $data[] = $row;
        }
        /* End Item Wise Difference Report */

        $row = array();
        $row[] = '';
        $row[] = '<b>Total</b>';
        $row[] = '<b>' . number_format((float)$purchase_amount_total, 2, '.', '') . '</b>';
        $row[] = '<b>' . number_format((float)$sell_amount_total, 2, '.', '') . '</b>';
        $difference_total = $sell_amount_total - $purchase_amount_total;
        $row[] = '<b>' . number_format((float)$difference_total, 2, '.', '') . '</b>';
        $data[] = $row;

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function item_diff_details($item_id, $production_id, $from_date, $to_date) {
        $data = array();
        $data['item_name'] = $this->crud->get_column_value_by_id('items', 'item_name', array('id' => $item_id));
        $data['item_code'] = $this->crud->get_column_value_by_id('items', 'item_code1', array('id' => $item_id));
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;

        $purchase_items_data = $this->applib->get_purchase_items_by_item_id_for_production($item_id, $production_id, date('Y-m-d 00:00:00', strtotime($from_date)), date('Y-m-d 23:59:00', strtotime($to_date)));
//        echo '<pre>'; print_r($purchase_items_data); exit;
        $data['purchase_items'] = $purchase_items_data['purchase_items'];
        $data['expense_amount'] = $purchase_items_data['expense_amount'];
//        echo '<pre>'; print_r($data['expense_amount']);
        $config['table'] = 'production p';
        $config['select'] = 'p.production_id AS id,p.finished_at,p.sales_order_id,p.proforma_invoice_id,p.item_id,i.item_name,i.item_code1 as item_code,so.sales_order_no,pi.proforma_invoice_no';

        $config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = p.sales_order_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'proforma_invoices pi', 'join_by' => 'pi.id = p.proforma_invoice_id', 'join_type' => 'left');

        $config['joins'][] = array('join_table' => 'items i', 'join_by' => 'i.id = p.item_id', 'join_type' => 'left');
//        $config['column_order'] = array('i.item_name', 'i.item_code', null, null, null);
//        $config['column_search'] = array('i.item_name', 'i.item_code');
        $config['order'] = array('p.production_id' => 'desc');
        $config['where_string'] = ' p.item_id = ' . $item_id . ' AND p.production_id = ' .$production_id;
        if (!empty($from_date) && !empty($to_date)) {
            $from_date = date('Y-m-d 00:00:00', strtotime($from_date));
            $to_date = date('Y-m-d 23:59:00', strtotime($to_date));
            $config['where_string'] .= ' AND p.`finished_at` between "' . $from_date . '" AND "' . $to_date . '" ';
        }
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
//        echo $this->db->last_query(); exit;
        $sell_amount_total = 0;
        foreach ($list as $challan) {
            
            $sell_amount = 0;
            if (!empty($challan->sales_order_no)) {
                $sales_order_items_sql = $this->db->query("SELECT rate,igst,cgst,sgst FROM `sales_order_items` WHERE `sales_order_id` = '" . $challan->sales_order_id . "' AND item_id = '" . $challan->item_id . "'");
                $sales_order_items = $sales_order_items_sql->row_array();
                if (!empty($sales_order_items)) {
                    $igst_amount = $sales_order_items['rate'] * $sales_order_items['igst'] / 100;
                    $cgst_amount = $sales_order_items['rate'] * $sales_order_items['cgst'] / 100;
                    $sgst_amount = $sales_order_items['rate'] * $sales_order_items['sgst'] / 100;
                    $sell_amount = $sales_order_items['rate'] + $igst_amount + $cgst_amount + $sgst_amount;
                }
            } else {
                $proforma_invoice_items_sql = $this->db->query("SELECT rate,igst,cgst,sgst FROM `proforma_invoice_items` WHERE `proforma_invoice_id` = '" . $challan->proforma_invoice_id . "' AND item_id = '" . $challan->item_id . "'");
                $proforma_invoice_items = $proforma_invoice_items_sql->row_array();
                if (!empty($proforma_invoice_items)) {
                    $igst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['igst'] / 100;
                    $cgst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['cgst'] / 100;
                    $sgst_amount = $proforma_invoice_items['rate'] * $proforma_invoice_items['sgst'] / 100;
                    $sell_amount = $proforma_invoice_items['rate'] + $igst_amount + $cgst_amount + $sgst_amount;
                }
            }
            $sell_amount_total = $sell_amount_total + $sell_amount;
        }
        $data['sell_amount'] = $sell_amount_total;

//        echo '<pre>'; print_r($data); exit;
        if ($this->applib->have_access_role(ITEM_DIFF_REPORT_MODULE_ID, "view")) {
            set_page('report/item_diff_details', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    //~ Purchase Item Stock Report
    function purchase_item_stock_report() {
        if ($this->applib->have_access_role(PURCHASE_ITEM_STOCK_REPORT_MODULE_ID, "view")) {
            set_page('report/purchase_item_stock_report');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function purchase_item_stock_datatable() {
        
        $purchase_item_ids = '';
        if (isset($_POST['purchase_project_id']) && !empty($_POST['purchase_project_id'])) {
            $purchase_item_arr = array();
            $purchase_project_items = $this->crud->get_row_by_id('purchase_project_items', array('project_id' => $_POST['purchase_project_id']));
            if(!empty($purchase_project_items)){
                foreach ($purchase_project_items as $purchase_project_item ){
                    $purchase_item_arr[] = $purchase_project_item->item_id;
                }
                $purchase_item_ids = implode(',', $purchase_item_arr);
            }
        }
        $item_group = '';
        if (isset($_POST['item_group']) && !empty($_POST['item_group'])) {
            $item_group = $_POST['item_group'];
        }
        
        $config['select'] = 'pi.id, pi.item_name, pi.item_code, pi.current_item_stock, pi.in_production, pi.effective_stock, cim.make_name';
        $config['table'] = 'purchase_items pi';
        $config['column_order'] = array('pi.item_name', 'pi.item_code', 'cim.make_name', null);
        $config['column_search'] = array('pi.item_name', 'pi.item_code', 'cim.make_name');
        $config['joins'][1] = array('join_table' => 'challan_item_make cim', 'join_by' => 'cim.id = pi.item_make_id', 'join_type' => 'left');
        
        if (!empty($purchase_item_ids) && !empty($item_group)) {
            $config['custom_where'] = ' pi.id IN (' . $purchase_item_ids . ')';
            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => $_POST['item_group']);
        } else if (!empty($purchase_item_ids) && empty($item_group)) {
            $config['custom_where'] = ' pi.id IN (' . $purchase_item_ids . ')';
        } else if (empty($purchase_item_ids) && !empty($item_group)) {
            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => $_POST['item_group']);
        } else {
            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => '-1');
        }
        //$config['order'] = array('item_name' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        foreach ($list as $item_list) {
            $row = array();
            $row[] = $item_list->item_name;
            $row[] = $item_list->item_code;
            $row[] = $item_list->make_name;
            $row[] = $item_list->current_item_stock;
            $row[] = $item_list->in_production;
            $row[] = $item_list->effective_stock;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    //~ Purchase Item Stock Sheet Report
    function purchase_item_stock_sheet_report() {
        if ($this->applib->have_access_role(PURCHASE_ITEM_STOCK_REPORT_MODULE_ID, "view")) {
            set_page('report/purchase_item_stock_sheet_report');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function purchase_item_stock_sheet_datatable() {
        
        $from_date = (isset($_REQUEST['from_date']) && !empty($_REQUEST['from_date'])) ? date('Y-m-d', strtotime($_REQUEST['from_date'])) : '';
        $to_date = (isset($_REQUEST['to_date']) && !empty($_REQUEST['to_date'])) ? date('Y-m-d', strtotime($_REQUEST['to_date'])) : '';
        
        $purchase_item_ids = '';
        if (isset($_POST['purchase_project_id']) && !empty($_POST['purchase_project_id'])) {
            $purchase_item_arr = array();
            $purchase_project_items = $this->crud->get_row_by_id('purchase_project_items', array('project_id' => $_POST['purchase_project_id']));
            if(!empty($purchase_project_items)){
                foreach ($purchase_project_items as $purchase_project_item ){
                    $purchase_item_arr[] = $purchase_project_item->item_id;
                }
                $purchase_item_ids = implode(',', $purchase_item_arr);
            }
        }
        $item_group = '';
        if (isset($_POST['item_group']) && !empty($_POST['item_group'])) {
            $item_group = $_POST['item_group'];
        }
        
        $config['select'] = 'pi.id, pi.item_name, pi.item_code, pi.rate, pi.current_item_stock, pi.in_production, pi.effective_stock, cim.make_name';
        $config['table'] = 'purchase_items pi';
        $config['column_order'] = array('pi.item_name', 'pi.item_code', 'cim.make_name', null);
        $config['column_search'] = array('pi.item_name', 'pi.item_code', 'cim.make_name');
        $config['joins'][1] = array('join_table' => 'challan_item_make cim', 'join_by' => 'cim.id = pi.item_make_id', 'join_type' => 'left');
        
        if (!empty($purchase_item_ids) && !empty($item_group)) {
            $config['custom_where'] = ' pi.id IN (' . $purchase_item_ids . ')';
            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => $_POST['item_group']);
        } else if (!empty($purchase_item_ids) && empty($item_group)) {
            $config['custom_where'] = ' pi.id IN (' . $purchase_item_ids . ')';
        } else if (empty($purchase_item_ids) && !empty($item_group)) {
            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => $_POST['item_group']);
        } else {
            $config['wheres'][] = array('column_name' => 'pi.item_group', 'column_value' => '-1');
        }
        //$config['order'] = array('item_name' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
//        echo '<pre>'; print_r($list); exit;
        $data = array();
        foreach ($list as $item_list) {
            $opening_purchase_invoice = $this->crud->get_purchase_invoice_total_qty($item_list->id, $from_date, '');
            $current_purchase_invoice = $this->crud->get_purchase_invoice_total_qty($item_list->id, $from_date, $to_date);
            $opening_sales_order = $this->crud->get_challan_to_sales_order_or_profoma_invoice_total_qty($item_list->id, $from_date, '');
            $current_sales_order = $this->crud->get_challan_to_sales_order_or_profoma_invoice_total_qty($item_list->id, $from_date, $to_date);
            $opening_qty = $opening_purchase_invoice - $opening_sales_order;
            $closing_qty = $opening_qty + $current_purchase_invoice - $current_sales_order;
//            echo '<pre>'; print_r($opening_sales_order); exit;
//            echo '<pre>'. $this->db->last_query(); exit;
            $row = array();
            $row[] = $item_list->item_name;
            $row[] = $item_list->item_code;
            $row[] = $item_list->make_name;
            $row[] = $opening_qty;
            $row[] = $current_purchase_invoice;
            $row[] = $current_sales_order;
            $row[] = $closing_qty;
            $row[] = $item_list->rate;
            $closing_rs = $closing_qty * $item_list->rate;
            $row[] = $closing_rs;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function counts() {
        $logged_in = $this->session->userdata("is_logged_in");
        $id = $logged_in['staff_id'];

        $today_pending_inquiry = $this->crud->today_pending_inquiry($id, true);
        $overdue_pending_inquiry = $this->crud->today_pending_inquiry($id);
        $data['pending_inquiry'] = count($overdue_pending_inquiry) + count($today_pending_inquiry);

        $today_pending_lead = $this->crud->today_pending_lead($id, true);
        $overdue_pending_lead = $this->crud->today_pending_lead($id);
        $data['pending_lead'] = count($today_pending_lead) + count($overdue_pending_lead);

        $data['pending_quotation_folloup'] = $this->crud->pending_quotation_folloup($id);

        $today_dispatch = $this->crud->today_sales_order($id, true);
        $overdue_dispatch = $this->crud->today_sales_order($id);
        $data['due_dispatch'] = count($overdue_dispatch) + count($today_dispatch);

        $upcoming_dispatch = $this->crud->today_sales_order($id, true, true);
        $data['upcoming_dispatch'] = count($upcoming_dispatch);

        $today_approved = $this->crud->pending_sales_order_approved($id, true);
        $overdue_approved = $this->crud->pending_sales_order_approved($id);
        $data['sales_approval'] = count($overdue_approved) + count($today_approved);

        $inquiry_followup_t = $this->crud->pending_inquiry_followup($id, true);
        $inquiry_followup_o = $this->crud->pending_inquiry_followup($id);
        $data['inquiry_followup'] = count($inquiry_followup_t) + count($inquiry_followup_o);

        $pending_invoice_t = $this->crud->pending_sales_order_invoice($id, true);
        $pending_invoice_o = $this->crud->pending_sales_order_invoice($id);
        $data['pending_invoice'] = count($pending_invoice_t) + count($pending_invoice_o);

        //echo $this->db->last_query();exit;
        //echo '<pre>';print_r($data);exit;
        if ($this->applib->have_access_role(SUMMARY_REPORT_MENU_ID, "view")) {
            set_page('report/counts', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
}
