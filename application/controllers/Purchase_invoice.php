<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 * @property M_pdf $m_pdf
 */
class Purchase_invoice extends CI_Controller
{

	protected $staff_id = 1;
	function __construct(){
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}
    
    /**
	 * Get Purchase Order DataTable
	 */
	function get_purchase_order_datatable(){
		$config['table'] = 'purchase_order e';
		$config['select'] = 'DISTINCT(e.order_id), e.order_no, p.supplier_name,e.order_date,e.created_at';
		$config['column_order'] = array(null, 'e.order_no', 'p.supplier_name', 'e.order_date','p.created_by');
		$config['column_search'] = array('e.order_no', 'p.supplier_name', 'e.order_date');
		$config['joins'][1] = array('join_table' => 'supplier p', 'join_by' => 'p.supplier_id = e.supplier_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'purchase_order_items poi', 'join_by' => 'poi.order_id = e.order_id', 'join_type' => 'left');
		$config['order'] = array('e.order_id' => 'desc');
        $config['where_string'] = '1 = 1';
        $user_id = $this->input->get_post('user_id');
		if ($user_id != '' && $user_id != 'all') {
			$config['where_string'] .= " AND e.created_by = '" . $user_id . "' ";
		}
		if(isset($_POST['request_from']) && !empty($_POST['request_from'])){
			if($_POST['request_from'] == 'add_invoice'){
//				$config['where_string'] .= " AND e.order_id NOT IN(SELECT order_id FROM purchase_invoice)";
				$config['where_string'] .= " AND poi.quantity > poi.invoice_qty";
			}
		}
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
//		echo $this->db->last_query();exit;
		$data = array();
		$role_delete = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "edit");

		foreach ($list as $order) {
			$row = array();
			$row[] = "<a href='javascript:void(0);' class='order_row' data-order_id='".$order->order_id."'>".$order->order_no."</a>";
			$row[] = "<a href='javascript:void(0);' class='order_row' data-order_id='".$order->order_id."'>".$order->supplier_name."</a>";
			$row[] = "<a href='javascript:void(0);' class='order_row' data-order_id='".$order->order_id."'>".date('d-m-Y', strtotime($order->order_date))."</a>";
			$row[] = "<a href='javascript:void(0);' class='order_row' data-order_id='".$order->order_id."'>".date('d-m-Y', strtotime($order->created_at))."</a>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
    
    function get_order_details(){
		$order_id = $this->input->get_post("order_id");
		$purchase_order_data = $this->crud->get_row_by_id('purchase_order', array('order_id' => $order_id));
        $purchase_order_data = $purchase_order_data[0];
        $purchase_order_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_order_data->created_by));
        $purchase_order_data->created_at = substr($purchase_order_data->created_at, 8, 2) .'-'. substr($purchase_order_data->created_at, 5, 2) .'-'. substr($purchase_order_data->created_at, 0, 4);
        $purchase_order_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_order_data->updated_by));
        $purchase_order_data->updated_at = substr($purchase_order_data->updated_at, 8, 2) .'-'. substr($purchase_order_data->updated_at, 5, 2) .'-'. substr($purchase_order_data->updated_at, 0, 4);
        $purchase_order_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_order_data->approved_by));
        $purchase_order_data->approved_at = substr($purchase_order_data->approved_at, 8, 2) .'-'. substr($purchase_order_data->approved_at, 5, 2) .'-'. substr($purchase_order_data->approved_at, 0, 4);
        $data['purchase_order_data'] = $purchase_order_data;
        $lineitems = array();
        $where = array('order_id' => $order_id);
        $purchase_order_lineitems = $this->crud->get_row_by_id('purchase_order_items', $where);
        foreach($purchase_order_lineitems as $purchase_order_lineitem){
            $lineitems[] = json_decode($purchase_order_lineitem->item_data);
        }
        $data['purchase_order_lineitems'] = $lineitems;
        echo json_encode($data);
		exit;
	}
    
    /**
	 *    Purchase Invoice
	 */
    function add($invoice_id = ''){
        $data = array();
        if(!empty($invoice_id)){
            if ($this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "edit")) {
                $purchase_invoice_data = $this->crud->get_row_by_id('purchase_invoice', array('invoice_id' => $invoice_id));
                $purchase_invoice_data = $purchase_invoice_data[0];
                $purchase_invoice_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_invoice_data->created_by));
                $purchase_invoice_data->created_at = substr($purchase_invoice_data->created_at, 8, 2) .'-'. substr($purchase_invoice_data->created_at, 5, 2) .'-'. substr($purchase_invoice_data->created_at, 0, 4);
                $purchase_invoice_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $purchase_invoice_data->updated_by));
                $purchase_invoice_data->updated_at = substr($purchase_invoice_data->updated_at, 8, 2) .'-'. substr($purchase_invoice_data->updated_at, 5, 2) .'-'. substr($purchase_invoice_data->updated_at, 0, 4);
                $data['purchase_invoice_data'] = $purchase_invoice_data;
                $lineitems = array();
                $where = array('invoice_id' => $invoice_id);
                $purchase_invoice_lineitems = $this->crud->get_row_by_id('purchase_invoice_items', $where);
                foreach($purchase_invoice_lineitems as $purchase_invoice_lineitem){
                    $item_data = json_decode($purchase_invoice_lineitem->item_data);
                    $item_data->lineitem_id = $purchase_invoice_lineitem->id;
                    $lineitems[] = json_encode($item_data);
                }
                $data['purchase_invoice_lineitems'] = implode(',', $lineitems);
//                echo '<pre>';print_r($data); exit;
                set_page('purchases/invoice/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
		} else {
            if($this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"add")) {
                $data['invoice_no'] = $this->crud->get_next_autoincrement('purchase_invoice');
                set_page('purchases/invoice/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
	}
    
    function save_purchase_invoice(){
        $return = array();
		$post_data = $this->input->post();
//		echo '<pre>';print_r($post_data); exit;
    
        $line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
//		print_r($line_items_data); exit;
		
        /*--------- Convert Date as Mysql Format -------------*/
		$_POST['purchase_invoice_data']['invoice_date'] = date("Y-m-d", strtotime($_POST['purchase_invoice_data']['invoice_date']));
		$_POST['purchase_invoice_data']['note'] = !empty($_POST['purchase_invoice_data']['note']) ? $_POST['purchase_invoice_data']['note'] : NULL;
		$_POST['purchase_invoice_data']['pf_amount'] = !empty($_POST['purchase_invoice_data']['pf_amount']) ? $_POST['purchase_invoice_data']['pf_amount'] : NULL;
		$_POST['purchase_invoice_data']['pf_cgst'] = !empty($_POST['purchase_invoice_data']['pf_cgst']) ? $_POST['purchase_invoice_data']['pf_cgst'] : NULL;
		$_POST['purchase_invoice_data']['pf_sgst'] = !empty($_POST['purchase_invoice_data']['pf_sgst']) ? $_POST['purchase_invoice_data']['pf_sgst'] : NULL;
		$_POST['purchase_invoice_data']['pf_igst'] = !empty($_POST['purchase_invoice_data']['pf_igst']) ? $_POST['purchase_invoice_data']['pf_igst'] : NULL;
		$_POST['purchase_invoice_data']['pf_amount_with_gst'] = !empty($_POST['purchase_invoice_data']['pf_amount_with_gst']) ? $_POST['purchase_invoice_data']['pf_amount_with_gst'] : NULL;
		$_POST['purchase_invoice_data']['t_amount'] = !empty($_POST['purchase_invoice_data']['t_amount']) ? $_POST['purchase_invoice_data']['t_amount'] : NULL;
		$_POST['purchase_invoice_data']['t_cgst'] = !empty($_POST['purchase_invoice_data']['t_cgst']) ? $_POST['purchase_invoice_data']['t_cgst'] : NULL;
		$_POST['purchase_invoice_data']['t_sgst'] = !empty($_POST['purchase_invoice_data']['t_sgst']) ? $_POST['purchase_invoice_data']['t_sgst'] : NULL;
		$_POST['purchase_invoice_data']['t_igst'] = !empty($_POST['purchase_invoice_data']['t_igst']) ? $_POST['purchase_invoice_data']['t_igst'] : NULL;
		$_POST['purchase_invoice_data']['t_amount_with_gst'] = !empty($_POST['purchase_invoice_data']['t_amount_with_gst']) ? $_POST['purchase_invoice_data']['t_amount_with_gst'] : NULL;
		$_POST['purchase_invoice_data']['grand_total'] = !empty($_POST['purchase_invoice_data']['grand_total']) ? $_POST['purchase_invoice_data']['grand_total'] : NULL;
        
		if(isset($_POST['purchase_invoice_data']['invoice_id']) && !empty($_POST['purchase_invoice_data']['invoice_id'])){
            $this->applib->update_purchase_order_qty_stock_by_purchase_invoice_id($_POST['purchase_invoice_data']['invoice_id'], '-');
            $invoice_no = $_POST['purchase_invoice_data']['invoice_no'];
            $purchase_invoice_result = $this->crud->get_id_by_val('purchase_invoice', 'invoice_id', 'invoice_no', $invoice_no);
            if(!empty($purchase_invoice_result) && $purchase_invoice_result != $_POST['purchase_invoice_data']['invoice_id']){
                echo json_encode(array("success" => 'false', 'msg' => 'Purchase Invoice No. Already Exist!'));
                exit;
            }
            
//            foreach($line_items_data[0] as $lineitem){
//                if(isset($lineitem->lineitem_id) && !empty($lineitem->lineitem_id)){
//                    $purchase_invoice_item = $this->crud->get_row_by_id('purchase_invoice_items', array('id' => $lineitem->lineitem_id));
//                    $purchase_invoice_item = $purchase_invoice_item[0];
//                    $item_data = json_decode($purchase_invoice_item->item_data);
//                    if($lineitem->quantity < $purchase_invoice_item->pending_qty){
//                        echo json_encode(array("success" => 'false', 'msg' => 'Purchase Invoice Item : '. $item_data->item_name . ' <br /> Need to set Qty lass then or equel to Pending Qty : '. $purchase_invoice_item->pending_qty));
//                        exit;
//                    }
//                }
//            }
            
			$_POST['purchase_invoice_data']['updated_at'] = $this->now_time;
			$_POST['purchase_invoice_data']['updated_by'] = $this->staff_id;
			$this->db->where('invoice_id', $_POST['purchase_invoice_data']['invoice_id']);
			$result = $this->db->update('purchase_invoice', $_POST['purchase_invoice_data']);
			$invoice_id = $_POST['purchase_invoice_data']['invoice_id'];
            if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Purchase Invoice Updated Successfully');
                
                if(isset($post_data['deleted_lineitem_id'])){
                    
                    /* Checking for Purchase Qty is Used or not. AND Update stock in Purchase Item Master */
                    foreach($post_data['deleted_lineitem_id'] as $deleted_lineitem_id){
                        $purchase_invoice_item = $this->crud->get_row_by_id('purchase_invoice_items', array('id' => $deleted_lineitem_id));
                        $purchase_invoice_item = $purchase_invoice_item[0];
                        $item_data = json_decode($purchase_invoice_item->item_data);
                        $used_qty = $purchase_invoice_item->quantity - $purchase_invoice_item->pending_qty;
                        if(!empty($used_qty)){
                            echo json_encode(array("success" => 'false', 'msg' => 'You are not able to delete this Invoice because, Purchase Invoice Item : '. $item_data->item_name .''.$used_qty.' qty is Used'));
                            exit;
                        }
                        
                        $purchase_item_pre = $this->crud->get_row_by_id('purchase_items', array('id' => $purchase_invoice_item->item_id));
                        $purchase_item_pre = $purchase_item_pre[0];
                        $pi_uom = $purchase_item_pre->uom;
                        $pii_uom_id = $item_data->uom_id;
                        if($pi_uom != $pii_uom_id){
                            $pi_relation = $purchase_item_pre->reference_qty;
                            $quantity = $purchase_invoice_item->quantity * $pi_relation;
                        } else {
                            $quantity = $purchase_invoice_item->quantity;
                        }
                        $current_item_stock_pre = $purchase_item_pre->current_item_stock - $quantity;
                        $effective_stock_pre = $purchase_item_pre->effective_stock - $quantity;
                        $this->crud->update('purchase_items', array('current_item_stock' => $current_item_stock_pre, 'effective_stock' => $effective_stock_pre), array('id' => $purchase_invoice_item->item_id));
                    }
                    
                    $this->db->where_in('id', $post_data['deleted_lineitem_id']);
                    $this->db->delete('purchase_invoice_items');
                }
                
                foreach($line_items_data[0] as $lineitem){
                    $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $lineitem->item_id));
                    
					$add_lineitem = array();
                    $add_lineitem['invoice_id'] = $invoice_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['item_data'] = json_encode($lineitem);
                    $add_lineitem['updated_at'] = $this->now_time;
                    if(isset($lineitem->lineitem_id) && !empty($lineitem->lineitem_id)){
                        
                        if(!empty($purchase_item)){
                            $purchase_item = $purchase_item[0];
                            $pi_uom = $purchase_item->uom;
                            $pii_uom_id = $lineitem->uom_id;
                            if($pi_uom != $pii_uom_id){
                                $pi_relation = $purchase_item->reference_qty;
                                $stock_quantity = $lineitem->quantity * $pi_relation;
                            } else {
                                $stock_quantity = $lineitem->quantity;
                            }
                            
                            /* Update stock in Purchase Item Master */
                            $purchase_invoice_item = $this->crud->get_row_by_id('purchase_invoice_items', array('id' => $lineitem->lineitem_id));
                            $purchase_invoice_item = $purchase_invoice_item[0];
                            $item_data = json_decode($purchase_invoice_item->item_data);
                            $pii_uom_id = $item_data->uom_id;
                            if($pi_uom != $pii_uom_id){
                                $pi_relation = $purchase_item->reference_qty;
                                $quantity = $purchase_invoice_item->quantity * $pi_relation;
                            } else {
                                $quantity = $purchase_invoice_item->quantity;
                            }
                            $current_item_stock = $purchase_item->current_item_stock - $quantity + $stock_quantity;
                            $effective_stock = $purchase_item->effective_stock - $quantity + $stock_quantity;
                            $this->crud->update('purchase_items', array('current_item_stock' => $current_item_stock, 'effective_stock' => $effective_stock), array('id' => $lineitem->item_id));
                            
                            /* Update Pending Qty for use in Sales (FIFO) */
                            $pending_qty = $this->crud->get_id_by_val('purchase_invoice_items', 'pending_qty', 'id', $lineitem->lineitem_id);
                            $new_pending_qty = $pending_qty - $purchase_invoice_item->quantity + $lineitem->quantity;
                            $add_lineitem['pending_qty'] = $new_pending_qty;
                        }
                        
                        $this->db->where('id', $lineitem->lineitem_id);
                        $this->db->update('purchase_invoice_items', $add_lineitem);
                    } else {
                        
                        if(!empty($purchase_item)){
                            $purchase_item = $purchase_item[0];
                            $pi_uom = $purchase_item->uom;
                            $pii_uom_id = $lineitem->uom_id;
                            if($pi_uom != $pii_uom_id){
                                $pi_relation = $purchase_item->reference_qty;
                                $stock_quantity = $lineitem->quantity * $pi_relation;
                            } else {
                                $stock_quantity = $lineitem->quantity;
                            }
                            
                            /* Update stock in Purchase Item Master */
                            $quantity = $this->crud->get_id_by_val('purchase_invoice_items', 'quantity', 'id', $lineitem->lineitem_id);
                            $current_item_stock = $purchase_item->current_item_stock + $stock_quantity;
                            $effective_stock = $purchase_item->effective_stock - $quantity + $stock_quantity;
                            $this->crud->update('purchase_items', array('current_item_stock' => $current_item_stock, 'effective_stock' => $effective_stock), array('id' => $lineitem->item_id));
                        }
                        $add_lineitem['pending_qty'] = $lineitem->quantity;
                        $add_lineitem['created_at'] = $this->now_time;
                        $this->crud->insert('purchase_invoice_items',$add_lineitem);
                    }
                    $this->applib->update_purchase_item_rate($lineitem->item_id, $lineitem->item_rate);
				}
                $this->applib->update_purchase_order_qty_stock_by_purchase_invoice_id($invoice_id, '+');
			}
		} else {
            
            $invoice_no = $_POST['purchase_invoice_data']['invoice_no'];
            $purchase_invoice_result = $this->crud->get_id_by_val('purchase_invoice', 'invoice_id', 'invoice_no', $invoice_no);
            if(!empty($purchase_invoice_result)){
                echo json_encode(array("success" => 'false', 'msg' => 'Purchase Invoice No. Already Exist!'));
                exit;
            }
            
            $dataToInsert = $_POST['purchase_invoice_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;
//            echo '<pre>'; print_r($dataToInsert); exit;
			$result = $this->db->insert('purchase_invoice', $dataToInsert);
            $invoice_id = $this->db->insert_id();
			if($result){
				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Purchase Invoice Added Successfully');
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
                    $add_lineitem['invoice_id'] = $invoice_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['pending_qty'] = $lineitem->quantity;
                    $add_lineitem['item_data'] = json_encode($lineitem);
                    $add_lineitem['updated_at'] = $this->now_time;
                    $add_lineitem['created_at'] = $this->now_time;
                    $this->crud->insert('purchase_invoice_items',$add_lineitem);
                    $this->applib->update_purchase_item_rate($lineitem->item_id, $lineitem->item_rate);
				}
			}
            $this->applib->update_purchase_order_qty_stock_by_purchase_invoice_id($invoice_id, '+');
            /* Update Purchase Item Stock by Purchase Invoice ID */
            $this->applib->update_purchase_stock_by_purchase_invoice_id($invoice_id, '+');
            
		}
		print json_encode($return);
		exit;
	}
    
    function invoice_list(){
		$where_array = array('module_id' => PURCHASE_INVOICE_MODULE_ID,'title' => 'add');
		$module_role_id = $this->crud->get_column_value_by_id('module_roles','id',$where_array);
		$where_array = array('module_id' => PURCHASE_INVOICE_MODULE_ID,'role_id' => $module_role_id);
		$staff_ids_arr = $this->crud->get_columns_val_by_where('staff_roles','staff_id',$where_array);
		$staff_ids = array();
		foreach ($staff_ids_arr as $rows) {
			foreach ($rows as $data) {
				$staff_ids[] = $data;
			}
		}
		$users = $this->crud->getFromSQL('SELECT * FROM `staff` WHERE `staff_id` in ('.implode(",",$staff_ids).') AND `active` != 0 ');
		if($this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"view")) {
			set_page('purchases/invoice/invoice_list', array('users' => $users));
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
    
    function purchase_invoice_datatable(){
		$requestData = $_REQUEST;
		
		$config['table'] = 'purchase_invoice e';
		$config['select'] = 'e.invoice_id, e.invoice_no, p.supplier_name,e.invoice_date,e.created_at,po.order_no, e.pf_amount_with_gst, e.t_amount_with_gst, e.grand_total';
        $config['joins'][] = array('join_table' => 'purchase_order po', 'join_by' => 'po.order_id = e.order_id', 'join_type' => 'left');
		$config['column_order'] = array(null, 'e.invoice_no','po.order_no', 'p.supplier_name', 'e.invoice_date','e.pf_amount_with_gst', 'e.t_amount_with_gst', 'e.grand_total');
		$config['column_search'] = array('e.invoice_no','po.order_no', 'p.supplier_name', 'e.invoice_date','e.pf_amount_with_gst', 'e.t_amount_with_gst', 'e.grand_total');
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$config['likes'][] = array('e.invoice_no' => $requestData['columns'][1]['search']['value']);
		}
        if (!empty($requestData['columns'][2]['search']['value'])) {
			$config['likes'][] = array('po.order_no' => $requestData['columns'][2]['search']['value']);
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			$config['likes'][] = array('p.supplier_name' => $requestData['columns'][3]['search']['value']);
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {
			$config['likes'][] = array('e.invoice_date' => $requestData['columns'][4]['search']['value']);
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {
			$config['likes'][] = array('e.created_at' => $requestData['columns'][5]['search']['value']);
		}
		
		$config['joins'][1] = array('join_table' => 'supplier p', 'join_by' => 'p.supplier_id = e.supplier_id', 'join_type' => 'left');
		$config['order'] = array('e.invoice_id' => 'desc');
//		$config['where_string'] = ' 1 = 1 ';

		$user_id = $this->input->get_post('user_id');
		if ($user_id != '' && $user_id != 'all') {
			$config['where_string'] .= " AND e.created_by = '" . $user_id . "' ";
		}
        if(isset($_POST['from_date']) && !empty($_POST['from_date'])){
            $config['wheres'][] = array('column_name' => 'e.invoice_date >=', 'column_value' => date('Y-m-d', strtotime($_POST['from_date'])));
        }
        if(isset($_POST['to_date']) && !empty($_POST['to_date'])){
            $config['wheres'][] = array('column_name' => 'e.invoice_date <=', 'column_value' => date('Y-m-d', strtotime($_POST['to_date'])));
        }
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
//        echo '<pre>'. $this->db->last_query(); exit;
		$data = array();
		$role_delete = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "edit");
		foreach ($list as $invoice) {
			$row = array();
			$action = '';
			if ($role_edit) {
				$action .= '<a href="' . BASE_URL . "purchase_invoice/add/" . $invoice->invoice_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			}
            if ($role_delete) {
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('purchase_invoice/delete_purchase_invoice/' . $invoice->invoice_id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.$invoice->invoice_id.'</a>';
			$row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.$invoice->order_no.'</a>';
			$row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.$invoice->supplier_name.'</a>';
			$row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.date('d-m-Y', strtotime($invoice->invoice_date)).'</a>';
            $row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.$invoice->pf_amount_with_gst.'</a>';
            $row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.$invoice->t_amount_with_gst.'</a>';
            $row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.$invoice->grand_total.'</a>';
			$row[] = '<a href="' . base_url('purchase_invoice/add/' . $invoice->invoice_id . '?view') . '" >'.date('d-m-Y', strtotime($invoice->created_at)).'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
    
    function delete_purchase_invoice($id){
		$role_delete = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "delete");
		if ($role_delete) {
            
            /* Checking for Purchase Qty is Used or not. */
            $purchase_invoice_items = $this->crud->get_row_by_id('purchase_invoice_items', array('invoice_id' => $id));
            foreach($purchase_invoice_items as $purchase_invoice_item){
                $item_data = json_decode($purchase_invoice_item->item_data);
                $used_qty = $purchase_invoice_item->quantity - $purchase_invoice_item->pending_qty;
                if(!empty($used_qty)){
                    echo json_encode(array("success" => 'false', 'msg' => 'You are not able to delete this Invoice because, Purchase Invoice Item : '. $item_data->item_name .''.$used_qty.' qty is Used'));
                    exit;
                }
            }
            
            /* Update Purchase Item Stock by Purchase Invoice ID */
            $this->applib->update_purchase_order_qty_stock_by_purchase_invoice_id($id, '-');
            $this->applib->update_purchase_stock_by_purchase_invoice_id($id, '-');
            
			$where_array = array("invoice_id" => $id);
			$this->crud->delete('purchase_invoice_items', array('invoice_id' => $id));
            $this->crud->delete("purchase_invoice", $where_array);
            echo json_encode(array("success" => 'true', 'msg' => 'Invoice Removed Successfully'));
            exit;
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this.');
			redirect("/");
		}
	}
    
    function delete_purchase_order($id){
		$role_delete = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "delete");
		if ($role_delete) {
			$where_array = array("invoice_id" => $id);
			$this->crud->delete("purchase_order", $where_array);
			$this->crud->delete("purchase_order_items", $where_array);
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Purchase Order has been deleted');
			redirect(BASE_URL . "purchase_order/order_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function get_purchases_order_detail($id){
		$select = "po.*,sp.supplier_name as supplier_name, sp.address, sp.tel_no, sp.contact_no, sp.cont_person, sp.gstin, sp.pincode ,sp.email_id, st.name as assigned_by, city.city,state.state,country.country,";
		$select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno";
		$this->db->select($select);
		$this->db->from('purchase_order po');
		$this->db->join('staff st', 'st.staff_id = po.created_by', 'left');
		$this->db->join('supplier sp', 'sp.supplier_id = po.supplier_id', 'left');
		$this->db->join('city', 'city.city_id = sp.city_id', 'left');
		$this->db->join('state', 'state.state_id = sp.state_id', 'left');
		$this->db->join('country', 'country.country_id = sp.country_id', 'left');
		$this->db->join('staff stf', 'stf.staff_id = po.created_by', 'left');
		$this->db->where('po.invoice_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row(0);
		} else {
			return array();
		}
	}
    
    function get_purchases_order_items($id){
		$this->db->select('poi.*');
		$this->db->from('purchase_order_items poi');
		//~ $this->db->join('uom u', 'u.id = poi.uom_id');
		//~ $this->db->where('ic.item_category', 'Finished Goods');
		$this->db->where('poi.invoice_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}
    
    function get_purchases_order_sub_items($id){
		$this->db->select('poi.*');
		$this->db->from('purchase_order_items poi');
		//~ $this->db->join('item_category ic', 'ic.id = poi.item_category_id');
		//~ $this->db->where('ic.item_category != ', 'Finished Goods');
		$this->db->where('poi.invoice_id', $id);
		$query = $this->db->get();
		$other_items = array();
		if ($query->num_rows() > 0) {
			$other_items = $query->result_array();
		}
		return $other_items;
	}
    
    public function ajax_load_supplier($supplier_id){
		$this->load->helper('url');
		$row = $this->crud->load_supplier_detail($supplier_id);
		$return = array(
			'supplier_id' => $row->supplier_id,
			'supplier_name' => $row->supplier_name,
			'address' => $row->address,
			'city' => $row->city,
			'city_id' => $row->city_id,
			'state' => $row->state,
			'state_id' => $row->state_id,
			'country' => $row->country,
			'country_id' => $row->country_id,
			'email_id' => $row->email_id,
			'tel_no' => $row->tel_no,
			'contact_no' => $row->contact_no,
			'pincode' => $row->pincode,
			'cont_person' => $row->cont_person
		);
		//echo "<pre>";print_r($return);exit;
		print json_encode($return);
		exit;
	}
    
    public function ajax_load_purchase_item_details($item_id = 0){
		if ($item_id != 0) {
			$this->load->helper('url');
			$row = $this->crud->load_purchase_item_details_where($item_id);

			$return = array(
				'id' => $row->id,
				'item_name' => $row->item_name,
				'item_group' => $row->item_group,
				'item_code' => $row->item_code,
				'item_make_id' => $row->item_make_id,
				'uom' => $row->uom,
				'reference_qty' => $row->reference_qty,
				'reference_qty_uom' => $row->reference_qty_uom,
				'rate' => $row->rate,
				'igst' => $row->igst,
				'sgst' => $row->sgst,
				'cgst' => $row->cgst,

			);
			print json_encode($return);
			exit;
		}
	}
    
    function get_purchase_project_items($purchase_project_id, $purchase_project_qty = '1', $state_id = ''){
        if ($purchase_project_id != 0) {
			$purchase_project_items = $this->crud->get_row_by_id('purchase_project_items', array('project_id' => $purchase_project_id));
            $project_name = $this->crud->get_id_by_val('purchase_project', 'project_name', 'project_id', $purchase_project_id);
            $purchase_project_item_arr = array();
            foreach ($purchase_project_items as $purchase_project_item ){
                $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $purchase_project_item->item_id));
                $purchase_item = $purchase_item[0];
                $item_arr = array();
                $item_arr['project_name'] = $project_name;
                $item_arr['purchase_project_qty'] = $purchase_project_qty;
                $item_arr['item_id'] = $purchase_project_item->item_id;
                $item_arr['uom_id'] = $purchase_project_item->uom_id;
                $item_arr['id'] = '';
                $item_arr['item_code'] = $purchase_item->item_code;
                $item_arr['item_name'] = $purchase_item->item_name;
                $item_arr['item_make_id'] = (!empty($purchase_item->item_make_id)) ? $purchase_item->item_make_id : '';
                $item_arr['item_make_name'] = (!empty($purchase_item->item_make_id)) ? $this->crud->get_id_by_val('challan_item_make','make_name','id', $purchase_item->item_make_id) : '';
                $item_arr['item_description'] = '';
                
                $item_arr['set_qty'] = $purchase_project_item->qty;
                $item_arr['quantity'] = $purchase_project_item->qty * $purchase_project_qty;
                $item_arr['uom_name'] = $purchase_project_item->uom_name;
                $item_arr['reference_qty'] = $purchase_item->reference_qty;
                $item_arr['po_reference_qty'] = $purchase_project_item->project_reference_qty * $purchase_project_qty;
                $item_arr['po_reference_qty_uom'] = $purchase_project_item->project_reference_qty_uom;
                $item_arr['po_reference_qty_uom_name'] = $purchase_project_item->project_reference_qty_uom_name;
                $item_arr['item_rate'] = $purchase_item->rate;
                $item_arr['amount'] = $item_arr['quantity'] * $item_arr['item_rate'];
                
                $item_arr['disc_per'] = $purchase_item->discount;
                $item_arr['disc_value'] = $item_arr['amount'] * $item_arr['disc_per'] / 100;
                
                $taxable_value = $item_arr['amount'] - $item_arr['disc_value'];
                $item_arr['igst'] = '0';
                $item_arr['igst_amount'] = '0';
                $item_arr['cgst'] = '0';
                $item_arr['cgst_amount'] = '0';
                $item_arr['sgst'] = '0';
                $item_arr['sgst_amount'] = '0';
                if($state_id == '' || $state_id == GUJARAT_STATE_ID){
                    $item_arr['cgst'] = $purchase_item->cgst;
                    $item_arr['cgst_amount'] = $taxable_value * $item_arr['cgst'] / 100;
                    $item_arr['sgst'] = $purchase_item->sgst;
                    $item_arr['sgst_amount'] = $taxable_value * $item_arr['sgst'] / 100;
                } else {
                    $item_arr['igst'] = $purchase_item->igst;
                    $item_arr['igst_amount'] = $taxable_value * $item_arr['igst'] / 100;
                }
                
                $item_arr['net_amount'] = $taxable_value + $item_arr['cgst_amount'] + $item_arr['sgst_amount'] + $item_arr['igst_amount'];
                
                //{"item_id":"23","uom_id":"5","id":"","item_code":"","
                //item_name":"1NC Auxiliary contact block for 3TF30 to 3TF35 \t\t\t",
                //"item_description":"","quantity":"1","item_rate":"68","disc_per":"0","disc_value":"0",
                //"igst":"0","igst_amount":"0","cgst":"9","cgst_amount":"6","sgst":"9","sgst_amount":"6",
                //"amount":"68","net_amount":"80"}
                $purchase_project_item_arr[] = json_encode($item_arr);
            }
            echo $project_item_arr = '['.implode(',', $purchase_project_item_arr).']';
            exit;
		}
	}

    
}
