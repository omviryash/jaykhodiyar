<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Enquiry extends CI_Controller
{

	protected $staff_id = 1;

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

	}

	/**
	 *    Enquiry
	 */
	function add($inquiry_id = ''){
		$data = array();
		if(!empty($inquiry_id)){
			if ($this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit")) {
				$inquiry_data = $this->get_inquiry_by_id($inquiry_id);
				$inquiry_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $inquiry_data->created_by));
				$inquiry_data->created_at = substr($inquiry_data->created_at, 8, 2) .'-'. substr($inquiry_data->created_at, 5, 2) .'-'. substr($inquiry_data->created_at, 0, 4);
				$inquiry_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $inquiry_data->updated_by));
				$inquiry_data->updated_at = substr($inquiry_data->updated_at, 8, 2) .'-'. substr($inquiry_data->updated_at, 5, 2) .'-'. substr($inquiry_data->updated_at, 0, 4);
				$data = array(
					'inquiry_data' => $inquiry_data,
					'party_contact_person' => $this->crud->get_contact_person_by_party($inquiry_data->party_id),
				);
				if(!empty($data['inquiry_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['inquiry_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if(!empty($designation)){
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if(!empty($department)){
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
				$lineitems = array();
				$where = array('inquiry_id' => $inquiry_id);
				$inquiry_lineitems = $this->crud->get_row_by_id('inquiry_items', $where);
				foreach($inquiry_lineitems as $inquiry_lineitem){
					$lineitems[] = $inquiry_lineitem->item_data;
				}
				$data['inquiry_lineitems'] = implode(',', $lineitems);
				$followup_histories = array();
				$followup_history_data = $this->crud->get_row_by_id('inquiry_followup_history', $where);
				foreach($followup_history_data as $followup_history){
					$followup_history->followup_history_date = date('d-m-Y H:i:s', strtotime($followup_history->followup_date));
					$followup_history->followup_history_onlydate = date('d-m-Y', strtotime($followup_history->followup_date));
					$followup_history->followup_history_onlytime = date('H:i:s', strtotime($followup_history->followup_date));
					$followup_history->followup_history_history = stripslashes($followup_history->history);
					//                    $followup_history->followup_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $followup_history->followup_by);
					$followup_histories[] = json_encode($followup_history);
				}
				$data['followup_history_data'] = implode(',', $followup_histories);
                $data['completed'] = $this->get_inquiry_status_id_by_status('completed');
				//                echo '<pre>';print_r($data); exit;
				set_page('sales/enquiry/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(ENQUIRY_MODULE_ID,"add")) {
				$data['inquiry_no'] = $this->crud->get_next_autoincrement('inquiry');
				set_page('sales/enquiry/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function save_enquiry(){
		$post_data = $this->input->post();

		// check party validations
		$party_id = $_POST['inquiry']['party_id'];
		if ($party_id == "" || $party_id == "0") {
			echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
			exit;
		}
		//		$result = $this->applib->check_party_validations($party_id);
		//		if ($result['status'] == 0) {
		//			echo json_encode(array("success" => 'false', 'msg' => $result['msg']));
		//			exit;
		//		}
		//        
		//        if (isset($_POST['inquiry']['kind_attn_id']) && trim($_POST['inquiry']['kind_attn_id']) != '' && !empty(trim($_POST['inquiry']['kind_attn_id']))) {
		//            $contact_person = $post_data['contact_person'];
		//            $contact_person_update['mobile_no'] = $contact_person['contact_person_mobile_no'];
		//            $contact_person_update['email'] = $contact_person['contact_person_email_id'];
		//            $this->crud->update('contact_person', $contact_person_update, array('contact_person_id' => $_POST['inquiry']['kind_attn_id']));
		//        }

		//		echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
		//		print_r($line_items_data); exit;

		$followup_history_data = json_decode('['.$post_data['followup_history_data'].']'); 
		//		print_r($followup_history_data); exit;

        $send_sms = 0;
        if(isset($post_data['inquiry']['send_sms'])){
            $send_sms = 1;
            unset($post_data['inquiry']['send_sms']);
            unset($_POST['inquiry']['send_sms']);
        }
        
		/*--------- Convert Date as Mysql Format -------------*/
		$_POST['inquiry']['inquiry_date'] = date("Y-m-d", strtotime($_POST['inquiry']['inquiry_date']));
		$_POST['inquiry']['due_date'] = date("Y-m-d", strtotime($_POST['inquiry']['due_date']));
		$_POST['inquiry']['kind_attn_id'] = !empty($_POST['inquiry']['kind_attn_id']) ? $_POST['inquiry']['kind_attn_id'] : NULL;
		$_POST['inquiry']['industry_type_id'] = !empty($_POST['inquiry']['industry_type_id']) ? $_POST['inquiry']['industry_type_id'] : NULL;
		$_POST['inquiry']['inquiry_stage_id'] = !empty($_POST['inquiry']['inquiry_stage_id']) ? $_POST['inquiry']['inquiry_stage_id'] : NULL;
		$_POST['inquiry']['assigned_to_id'] = !empty($_POST['inquiry']['assigned_to_id']) ? $_POST['inquiry']['assigned_to_id'] : NULL;
		$_POST['inquiry']['sales_id'] = !empty($_POST['inquiry']['sales_id']) ? $_POST['inquiry']['sales_id'] : NULL;
		$_POST['inquiry']['sales_type_id'] = !empty($_POST['inquiry']['sales_type_id']) ? $_POST['inquiry']['sales_type_id'] : NULL;
		$_POST['inquiry']['lead_or_inquiry'] = !empty($_POST['inquiry']['lead_or_inquiry']) ? $_POST['inquiry']['lead_or_inquiry'] : NULL;

		if(isset($_POST['inquiry']['inquiry_id']) && !empty($_POST['inquiry']['inquiry_id'])){

			$inquiry_no = $_POST['inquiry']['inquiry_no'];
			$inquiry_result = $this->crud->get_id_by_val('inquiry', 'inquiry_id', 'inquiry_no', $inquiry_no);
			if(!empty($inquiry_result) && $inquiry_result != $_POST['inquiry']['inquiry_id']){
				echo json_encode(array("success" => 'false', 'msg' => 'Enquiry No. Already Exist!'));
				exit;
			}

			if (!isset($_POST['inquiry']['is_received_post'])) {
				$_POST['inquiry']['is_received_post'] = 0;
			}
			if (!isset($_POST['inquiry']['is_send_post'])) {
				$_POST['inquiry']['is_send_post'] = 0;
			}
			if (!isset($_POST['inquiry']['send_from_post'])) {
				$_POST['inquiry']['send_from_post'] = 0;
			}
			$_POST['inquiry']['updated_at'] = $this->now_time;
			$_POST['inquiry']['updated_by'] = $this->staff_id;
			$this->db->where('inquiry_id', $_POST['inquiry']['inquiry_id']);
			$result = $this->db->update('inquiry', $_POST['inquiry']);
			$inquiry_id = $_POST['inquiry']['inquiry_id'];
			if($result){
				//                $this->crud->update('party', array('party_type_2' => $_POST['inquiry']['sales_type_id']), array('party_id' => $party_id));
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Enquiry Updated Successfully');
				$where_array = array("inquiry_id" => $inquiry_id);
				$this->crud->delete("inquiry_items", $where_array);
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['inquiry_id'] = $inquiry_id;
					$add_lineitem['item_data'] = json_encode($lineitem);
					$add_lineitem['updated_at'] = $this->now_time;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('inquiry_items',$add_lineitem);
				}
				$this->crud->delete("inquiry_followup_history", $where_array);
				foreach($followup_history_data[0] as $line){
					$add_followup_history = array();
					$add_followup_history['inquiry_id'] = $inquiry_id;
					$add_followup_history['followup_date'] = date("Y-m-d H:i:s", strtotime($line->followup_history_date));
					$add_followup_history['history'] = addslashes($line->followup_history_history);
					$add_followup_history['followup_by'] = $this->staff_id;
					$this->crud->insert('inquiry_followup_history',$add_followup_history);
				}
			}
		} else {

			$set_inquiry_no = $this->crud->get_row_by_id('inquiry', array('inquiry_no' => '24000'));
			if(empty($set_inquiry_no)){
				$_POST['inquiry']['inquiry_no'] = '24000';
				$_POST['inquiry']['inquiry_id'] = '24000';
			} else {
				$_POST['inquiry']['inquiry_no'] = $this->crud->get_next_autoincrement('inquiry');
				// Unset inquiry_id from Inquiry Data
				if (isset($_POST['inquiry']['inquiry_id'])){
					unset($_POST['inquiry']['inquiry_id']);
				}
			}

			$dataToInsert = $_POST['inquiry'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['updated_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;

			$result = $this->db->insert('inquiry', $dataToInsert);
			$inquiry_id = $this->db->insert_id();
			if($result){
				//                $this->crud->update('party', array('party_type_2' => $_POST['inquiry']['sales_type_id']), array('party_id' => $party_id));
				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Enquiry Added Successfully');
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['inquiry_id'] = $inquiry_id;
					$add_lineitem['item_data'] = json_encode($lineitem);
					$add_lineitem['created_at'] = $this->now_time;
					$add_lineitem['updated_at'] = $this->now_time;
					$this->crud->insert('inquiry_items',$add_lineitem);
				}
				foreach($followup_history_data[0] as $line){
					$add_followup_history = array();
					$add_followup_history['inquiry_id'] = $inquiry_id;
					$add_followup_history['followup_date'] = date("Y-m-d H:i:s", strtotime($line->followup_history_date));
					$add_followup_history['history'] = addslashes($line->followup_history_history);
					$add_followup_history['followup_by'] = $this->staff_id;
					$this->crud->insert('inquiry_followup_history',$add_followup_history);
				}
			}
		}
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['inquiry']['party_id']);
            $this->applib->send_sms('inquiry', $inquiry_id, $party_phone_no, SEND_ENQUIRY_SMS_ID);
        }
		print json_encode($return);
		exit;
	}

	function enquiry_list($staff_id='',$from_date='',$to_date=''){
            $this->db->order_by('id');
            $enquiry_status = $this->db->get('inquiry_status')->result();

            $this->db->order_by('name');
            $users = $this->db->where('active !=',0)->get('staff')->result();
                          
            if($this->applib->have_access_role(ENQUIRY_MODULE_ID,"view")) {
                    set_page('sales/enquiry/enquiry_list', array('enquiry_status' => $enquiry_status, 'users' => $users,'staff_id' => $staff_id, 'from_date'=>$from_date, 'to_date'=>$to_date));
            } else {
                    $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                    redirect("/");
            }
	}

	function enquiry_datatable(){
		$requestData = $_REQUEST;
//                echo "<pre>";
//                print_r($requestData);exit;
		$config['table'] = 'inquiry e';
		$config['select'] = 'e.inquiry_id, e.inquiry_no, e.inquiry_status_id, s.inquiry_status,p.party_name,p.party_code,e.inquiry_date,e.lead_or_inquiry,sf.name,cur_s.name AS party_current_person';
		$config['column_order'] = array(null, 'e.inquiry_id', 'p.party_id', 'p.party_name', 'p.party_code', 'e.inquiry_date',);
		$config['column_search'] = array('e.inquiry_no', 's.inquiry_status', 'p.party_name', 'p.party_code', 'DATE_FORMAT(e.inquiry_date,"%d-%m-%Y")');
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$config['likes'][] = array('e.inquiry_no' => $requestData['columns'][1]['search']['value']);
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$config['likes'][] = array('p.party_code' => $requestData['columns'][2]['search']['value']);
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			$config['likes'][] = array('p.party_name' => $requestData['columns'][3]['search']['value']);
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {
			$config['likes'][] = array('s.inquiry_status' => $requestData['columns'][4]['search']['value']);
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {
			$config['likes'][] = array('e.inquiry_date' => date("Y-m-d",strtotime($requestData['columns'][5]['search']['value'])));
		}

		$config['joins'][0] = array('join_table' => 'inquiry_status s', 'join_by' => 's.id = e.inquiry_status_id', 'join_type' => 'left');
		$config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = e.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'staff sf', 'join_by' => 'sf.staff_id = e.created_by', 'join_type' => 'left');
        $config['joins'][] = array('join_table'=> 'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');
		$config['order'] = array('e.inquiry_id' => 'desc');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		
        if (!empty($_POST['user_id']) && $_POST['user_id'] != 'all') {
            if (isset($_POST['staff_flag']) && $_POST['staff_flag'] == '1') {
                $config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' => $_POST['user_id']);
            } else {
                $config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' => $_POST['user_id']);
            }
        }
        if (!empty($_POST['inquiry_status']) && $_POST['inquiry_status'] != 'all') {
			$inquiry_status_id = $this->crud->get_id_by_val('inquiry_status', 'id', 'inquiry_status', $_POST['inquiry_status']);
			$config['wheres'][] = array('column_name' => 'e.inquiry_status_id', 'column_value' => $inquiry_status_id);
		}
		if (!empty($_POST['select_lead_inquiry']) && $_POST['select_lead_inquiry'] != 'all') {
			//echo '<pre>';print_r($_POST['select_lead_inquiry']);
			$config['wheres'][] = array('column_name' => 'e.lead_or_inquiry', 'column_value' => $_POST['select_lead_inquiry']);
		}
                if (isset($_POST['from_date']) && !empty($_POST['from_date'])) {
                    $from_date = date('Y-m-d', strtotime($_POST['from_date']));
                    $config['wheres'][] = array('column_name' => 'e.inquiry_date >=', 'column_value' => $from_date);
                }
                if (isset($_POST['to_date']) && !empty($_POST['to_date'])) {
                    $to_date = date('Y-m-d', strtotime($_POST['to_date']));
                    $config['wheres'][] = array('column_name' => 'e.inquiry_date <=', 'column_value' => $to_date);
                }
		//$config['where_string'] = ' 1 = 1 ';

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		$isManagement = $this->applib->have_access_current_user_rights(MANAGEMENT_USER,"allow");
		if($isManagement == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){					
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' =>  $this->session->userdata('is_logged_in')['staff_id']);
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if($cu_accessDomestic == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			} else {}

		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
//                echo $this->db->last_query(); exit;
		$data = array();
		$role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
		$role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");

		foreach ($list as $enquiry_row) {
			$row = array();
			$action = '';
			$completed = $this->get_inquiry_status_id_by_status('completed');
			if ($role_edit) {
                if($completed != $enquiry_row->inquiry_status_id){
                    $action .= '<a href="' . BASE_URL . "enquiry/add/" . $enquiry_row->inquiry_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                } else {
                    if($this->applib->have_access_role(ENQUIRY_ALLOW_EDIT_AFTER_QUOTATION,"allow")) {
                        $action .= '<a href="' . BASE_URL . "enquiry/add/" . $enquiry_row->inquiry_id . '" class="edit_button btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                    }
                }
			}
            if ($role_delete) {
				if($completed != $enquiry_row->inquiry_status_id){
					$action .= ' <a onclick="return confirm(\'Are you sure ?\');" href="' . BASE_URL . "enquiry/delete_enquiry/" . $enquiry_row->inquiry_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
				}
			}
			$row[] = $action;
			$row[] = '<a href="' . base_url('enquiry/add/' . $enquiry_row->inquiry_id . '?view') . '" >'.$enquiry_row->inquiry_no.'</a>';
			$row[] = '<a href="' . base_url('enquiry/add/' . $enquiry_row->inquiry_id . '?view') . '" >'.$enquiry_row->party_code.'</a>';
			$row[] = '<a href="' . base_url('enquiry/add/' . $enquiry_row->inquiry_id . '?view') . '" >'.$enquiry_row->party_name.'</a>';
			$row[] = '<a href="' . base_url('enquiry/add/' . $enquiry_row->inquiry_id . '?view') . '" >'.$enquiry_row->inquiry_status.'</a>';
			$row[] = '<a href="' . base_url('enquiry/add/' . $enquiry_row->inquiry_id . '?view') . '" >'.$enquiry_row->name.'</a>';
			$row[] = '<a href="' . base_url('enquiry/add/' . $enquiry_row->inquiry_id . '?view') . '" >'.date('d-m-Y', strtotime($enquiry_row->inquiry_date)).'</a>';
            $row[] = '<a href="' . base_url('enquiry/add/' . $enquiry_row->inquiry_id . '?view') . '" >'.$enquiry_row->party_current_person.'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
			"sql" => $this->db->last_query(),

		);

		//output to json format
		echo json_encode($output);
	}

	function delete_enquiry($id){
		$role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
		if ($role_delete) {
			$where_array = array("inquiry_id" => $id);
			$this->crud->delete("inquiry", $where_array);
			$this->crud->delete("inquiry_items", $where_array);
			$this->crud->delete("inquiry_followup_history", $where_array);

			$session_data = array(
				'success_message' => "Inquiry has been deleted !"
			);
			$this->session->set_userdata($session_data);
			redirect(BASE_URL . "enquiry/enquiry_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function get_inquiry_by_id($inquiry_id){
		$this->db->select("i.*,st.name as assigned_by");
		$this->db->from('inquiry i');
		$this->db->join('staff st', 'st.staff_id = i.created_by', 'left');
		$this->db->where('i.inquiry_id', $inquiry_id);
		return $this->db->get()->row(0);
	}

	function get_inquiry_status_id_by_status($status = 'completed'){
		$this->db->select('id');
		$this->db->from('inquiry_status');
		$this->db->where('LOWER(inquiry_status)', strtolower($status));
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		} else {
			return null;
		}
	}
}
