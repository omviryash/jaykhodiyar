<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class supplier
 * @property AppModel $app_model
 * @property CI_DB_active_record $db
 */
class Supplier extends CI_Controller
{

	function __construct(){
		parent::__construct();

		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
	}
    
    /**
	 *    Supplier
	 */
	function add($supplier_id = ''){
        $data = array();
        if(!empty($supplier_id)){
            if ($this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "edit")) {
                $supplier_data = $this->crud->get_row_by_id('supplier', array('supplier_id' => $supplier_id));
                $data['supplier_data'] = $supplier_data[0];
//                echo '<pre>';print_r($data); exit;
                set_page('supplier/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
		} else {
            if($this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID,"add")) {
                set_page('supplier/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
	}
    
    function save_supplier(){
		$post_data = $this->input->post();
//		echo '<pre>';print_r($post_data); exit;
        
        /*--------- Convert Date as Mysql Format -------------*/
		$post_data['pincode'] = !empty($post_data['pincode']) ? $post_data['pincode'] : NULL;
		$post_data['contact_no'] = !empty($post_data['contact_no']) ? $post_data['contact_no'] : NULL;
		$post_data['email_id'] = !empty($post_data['email_id']) ? $post_data['email_id'] : NULL;
		$post_data['gstin'] = !empty($post_data['gstin']) ? $post_data['gstin'] : NULL;
		$post_data['cin_no'] = !empty($post_data['cin_no']) ? $post_data['cin_no'] : NULL;
		$post_data['tin_no'] = !empty($post_data['tin_no']) ? $post_data['tin_no'] : NULL;
		$post_data['cst_no'] = !empty($post_data['cst_no']) ? $post_data['cst_no'] : NULL;
		$post_data['pan_no'] = !empty($post_data['pan_no']) ? $post_data['pan_no'] : NULL;
		$post_data['aadhar_no'] = !empty($post_data['aadhar_no']) ? $post_data['aadhar_no'] : NULL;
		$post_data['cont_person'] = !empty($post_data['cont_person']) ? $post_data['cont_person'] : NULL;
		$post_data['note'] = !empty($post_data['note']) ? $post_data['note'] : NULL;
        
		if(isset($post_data['supplier_id']) && !empty($post_data['supplier_id'])){
            $post_data['updated_at'] = $this->now_time;
			$post_data['updated_by'] = $this->staff_id;
			$this->db->where('supplier_id', $post_data['supplier_id']);
            $post_data = (array) $post_data;
            if (isset($post_data['supplier_id'])){
                unset($post_data['supplier_id']);
            }
			$result = $this->db->update('supplier', $post_data);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Supplier Updated Successfully');
			}
		} else {
            $post_data['created_at'] = $this->now_time;
			$post_data['created_by'] = $this->staff_id;
			$post_data['updated_at'] = $this->now_time;
			$post_data['updated_by'] = $this->staff_id;
            $post_data = (array) $post_data;
			$result = $this->db->insert('supplier', $post_data);
            if($result){
                $return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Supplier Added Successfully');
			}
		}
		print json_encode($return);
		exit;
	}

	function supplier_list() {
		if($this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID,"view")) {
			set_page('supplier/supplier_list');
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	/**
	 * Supplier List DataTable
	 */
	function supplier_list_datatable() {
		//print_r($this->session->userdata('user_roles'));
		$config['table'] = 'supplier';
        $config['select'] = 'supplier_id,address,supplier_name,contact_no,tel_no,email_id';
        $config['column_search'] = array('supplier_id','supplier_name','tel_no','email_id');
        $config['column_order'] = array('supplier_id','supplier_name','tel_no','email_id');
        $cu_accessExport = $this->applib->have_access_current_user_rights(SUPPLIER_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(SUPPLIER_TYPE_MODULE_ID,"domestic");
        $config['order'] = array('supplier_name' => 'ASC');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        foreach ($list as $supplier) {
            $row = array();
			$action = '';
			$isEdit = $this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "edit");
			$isDelete = $this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "delete");
			if ($isEdit) {
				$action .= '<a href="' . base_url("supplier/add/" . $supplier->supplier_id) . '" class="btn btn-xs btn-primary btn-edit-supplier" data-supplier_id="' . $supplier->supplier_id . '"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_button" data-href="' . base_url('master/delete/' . $supplier->supplier_id) . '"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;            
            $row[] = '<a href="' . base_url("supplier/add/" . $supplier->supplier_id."?view") . '" >'.$supplier->supplier_name.'</a>';
            $row[] = '<a href="' . base_url("supplier/add/" . $supplier->supplier_id."?view") . '" >'.$supplier->address.'</a>';
            $row[] = '<a href="' . base_url("supplier/add/" . $supplier->supplier_id."?view") . '" >'.$supplier->contact_no.'</a>';
            $row[] = '<a href="' . base_url("supplier/add/" . $supplier->supplier_id."?view") . '" >'.$supplier->tel_no.'</a>';
            $row[] = '<a href="' . base_url("supplier/add/" . $supplier->supplier_id."?view") . '" >'.$supplier->email_id.'</a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
	}

	
//	function index()
//	{
//		$access = 1;
//
//		if ($access) {
//			//$id = $this->uri->segment(3);
//			$id = $this->input->get('id', TRUE);
//			
//			if ($id != "" && $id > 0) {
//
//				$this->db->select('*');
//				$this->db->where('supplier_id', $id);
//				$supplier = $this->db->get('supplier')->result_array();
//				
//				if (!count($supplier) > 0)
//					return;
//				 
//				$supplier[0]['city_id'] = $this->fetch_field_by_table_name('city', 'city', 'city_id', $supplier[0]['city_id']);
//				 
//				$supplier[0]['state_id'] = $this->fetch_field_by_table_name('state', 'state', 'state_id', $supplier[0]['state_id']);
//				 
//				$supplier[0]['country_id'] = $this->fetch_field_by_table_name('country', 'country', 'country_id', $supplier[0]['country_id']); 
//				$this->db->order_by('supplier_id');
//				$parties = $this->db->get('supplier')->result();
//				 
//				$data = array(
//					/*'reference' => $this->crud->get_select_data('reference'),
//					'city' => $this->crud->get_select_data('city'),
//					'state' => $this->crud->get_select_data('state'),
//					'country' => $this->crud->get_select_data('country'),
//					'sales' => $this->crud->get_select_data('sales'),
//					'sales_type' => $this->crud->get_select_data('sales_type'),
//					'designation' => $this->crud->get_select_data('designation'),
//					'department' => $this->crud->get_select_data('department'),
//					'branches' => $this->crud->get_select_data('branch'),*/
//					'supplier' => $supplier,
//					'edit' => 1
//				);
//				 //echo "<pre>";  print_r($data); echo '</pre>'; exit;
//				if($this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID,"add")) {
//					set_page('supplier/index', $data);
//				}else{
//					$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
//					redirect($_SERVER['HTTP_REFERER']);
//				}
//			} else {
//				/*$accessExport = $this->app_model->have_access_role(SUPPLIER_TYPE_MODULE_ID, "export");
//				$accessDomestic = $this->app_model->have_access_role(SUPPLIER_TYPE_MODULE_ID, "domestic");
//				if ($accessExport == 1 && $accessDomestic == 1) {
//					$this->db->order_by('supplier_id');
//					$parties = $this->db->get('supplier')
//						->result();
//				} else if ($accessExport == 1) {
//					$this->db->order_by('supplier_id');
//					$parties = $this->db->where("supplier_type_1", 5)->get('supplier')->result();
//				} else if ($accessDomestic == 1) {
//					$this->db->order_by('supplier_id');
//					$parties = $this->db->where("supplier_type_1", 6)->get('supplier')->result();
//				} else {
//					$parties = array();
//				}*/
//				/*echo "<pre>";
//                print_r($parties);
//                die;*/
//
//				$data = array(
//					/*'reference' => $this->crud->get_select_data('reference'),
//					'city' => $this->crud->get_select_data('city'),
//					'state' => $this->crud->get_select_data('state'),
//					'country' => $this->crud->get_select_data('country'),
//					'sales' => $this->crud->get_select_data('sales'),
//					'sales_type' => $this->crud->get_select_data('sales_type'),
//					'designation' => $this->crud->get_select_data('designation'),
//					'department' => $this->crud->get_select_data('department'),
//					'branches' => $this->crud->get_select_data('branch'),
//					'parties' => $parties*/
//				);
//				if($this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID,"add")) {
//					set_page('supplier/index', $data);
//				}else{
//					$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
//					redirect($_SERVER['HTTP_REFERER']);
//				}
//			}
//		} else {
//			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
//			redirect("/");
//		}
//	}
//
	}
