<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
/**
 * Class App
 * &@property AppModel $app_model
 * &@property AppLib $applib
 * &@property Crud $crud
 */
class App extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('AppModel', 'app_model');
    }

    /**
     * @param $party_code
     */
    function load_party($party_code)
    {
        $this->db->select('p.*,pt1.type,pt2.type,br.branch,ct.city,st.state,cnt.country');
        $this->db->from('party p');
        $this->db->join('party_type_1 pt1', 'pt1.id = p.party_type_1', 'left');
        $this->db->join('party_type_2 pt2', 'pt2.id = p.party_type_2', 'left');
        $this->db->join('branch br', 'br.branch_id = p.branch_id', 'left');
        $this->db->join('city ct', 'ct.city_id = p.city_id', 'left');
        $this->db->join('state st', 'st.state_id = p.state_id', 'left');
        $this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
        $this->db->like('p.party_code', $party_code);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'party_id' => $row->party_id,
                    'party_code' => $row->party_code,
                    'party_name' => $row->party_name,
                    'address' => $row->address,
                    'city' => $row->city,
                    'city_id' => $row->city_id,
                    'state' => $row->state,
                    'state_id' => $row->state_id,
                    'country' => $row->country,
                    'country_id' => $row->country_id,
                    'fax_no' => $row->fax_no,
                    'email_id' => $row->email_id,
                    'website' => $row->website,
                    'pincode' => $row->pincode,
                    'phone_no' => $row->phone_no,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function load_party_new()
    {
        $party_code = $this->input->get_post("term");
        $this->db->select('p.*,pt1.type,pt2.type,br.branch,ct.city,st.state,cnt.country');
        $this->db->from('party p');
        $this->db->join('party_type_1 pt1', 'pt1.id = p.party_type_1', 'left');
        $this->db->join('party_type_2 pt2', 'pt2.id = p.party_type_2', 'left');
        $this->db->join('branch br', 'br.branch_id = p.branch_id', 'left');
        $this->db->join('city ct', 'ct.city_id = p.city_id', 'left');
        $this->db->join('state st', 'st.state_id = p.state_id', 'left');
        $this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
        $this->db->where('p.active', 1);
        $this->db->like('p.party_code', $party_code);
        $accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
        $accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
        if ($accessExport == 1 && $accessDomestic == 1) {
            $query = $this->db->get();
        } else if ($accessExport == 1) {
            $this->db->where("party_type_1", 5);
            $query = $this->db->get();
        } else if ($accessDomestic == 1) {
            $this->db->where("party_type_1", 6);
            $query = $this->db->get();
        } else {
            echo json_encode(array());
            exit();
        }
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {

                $phone = trim($row->phone_no);
                $phone = str_replace(', ', ",", $phone);
                $phone = str_replace(', , ', ",", $phone);
                $temp = explode(",", $phone);
                $phoneNew = "";
                foreach ($temp as $t) {
                    if (trim($t) != "")
                        $phoneNew .= trim($t) . "\n";
                }
                $phone = $phoneNew;
                $phone = str_replace(',', "\n", $phone);
                $emails = str_replace(',', "\n", $row->email_id);

                $response[] = array(
                    'label' => $row->party_code . "-" . $row->party_name,
                    'value' => $row->party_code . "-" . $row->party_name,
                    'party_id' => $row->party_id,
                    'party_code' => $row->party_code,
                    'party_name' => $row->party_name,
                    'address' => $row->address,
                    'city' => $row->city,
                    'city_id' => $row->city_id,
                    'state' => $row->state,
                    'state_id' => $row->state_id,
                    'country' => $row->country,
                    'country_id' => $row->country_id,
                    'fax_no' => $row->fax_no,
                    'email_id' => $emails,
                    'website' => $row->website,
                    'pincode' => $row->pincode,
                    'phone_no' => $phone,
                    'project' => $row->project,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $item_name
     */
    function load_item($item_name)
    {
        $this->db->select('t.*,is.item_status');
        $this->db->from('items t');
        $this->db->join('item_status is', 'is.id = t.item_status', 'left');
        $this->db->like('t.item_name', $item_name);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'item_code' => $row->item_code1,
                    'item_name' => $row->item_name,
                    'item_status' => $row->item_status,
                    'uom' => $row->uom,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $item_code
     */
    function load_item_code($item_code)
    {
        $this->db->select('t.*,is.item_status');
        $this->db->from('items t');
        $this->db->join('item_status is', 'is.id = t.item_status', 'left');
        $this->db->like('t.item_code1', $item_code);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'item_code' => $row->item_code1,
                    'item_name' => $row->item_name,
                    'item_status' => $row->item_status,
                    'uom' => $row->uom,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function load_item_new()
    {
        $item_code = $this->input->get_post("term");
        $this->db->select('t.*,is.item_status');
        $this->db->from('items t');
        $this->db->join('item_status is', 'is.id = t.item_status', 'left');
        $this->db->like('t.item_code1', $item_code);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'item_code' => $row->item_code1,
                    'item_name' => $row->item_name,
                    'item_status' => $row->item_status,
                    'uom' => $row->uom,
                    'label' => $row->item_code1,
                    'value' => $row->item_code1,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function load_item_new2()
    {
        $item_name = $this->input->get_post("term");
        $this->db->select('t.*,is.item_status');
        $this->db->from('items t');
        $this->db->join('item_status is', 'is.id = t.item_status', 'left');
        $this->db->like('t.item_name', $item_name);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'item_code' => $row->item_code1,
                    'item_name' => $row->item_name,
                    'item_status' => $row->item_status,
                    'uom' => $row->uom,
                    'label' => $row->item_name,
                    'value' => $row->item_name,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function load_staff_new()
    {
        $staff_name = isset($_GET['term']) ? $_GET['term'] : '';

        $this->db->select('st.staff_id,st.name');
        $this->db->from('staff st');
        $this->db->where('st.active !=', 0);
        $this->db->where('st.name LIKE "%' . $staff_name . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $staff_name != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->staff_id,
                    'value' => $row->name,
                    'label' => $row->name,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $staff_name
     */
    function load_staff($staff_name)
    {
        $this->db->select('st.staff_id,st.name');
        $this->db->from('staff st');
        $this->db->where('st.active !=', 0);
        $this->db->like('st.name', $staff_name);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'staff_id' => $row->staff_id,
                    'name' => $row->name,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $reference
     */
    function load_reference($reference)
    {
        $this->db->select('r.id,r.reference');
        $this->db->from('reference r');
        $this->db->like('r.reference', $reference);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'reference' => $row->reference,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $industry_type
     */
    function load_industry_type($industry_type)
    {
        $this->db->select('it.id,it.industry_type');
        $this->db->from('industry_type it');
        $this->db->like('it.industry_type', $industry_type);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'industry_type' => $row->industry_type,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $inquiry_stage
     */
    function load_inquiry_stage($inquiry_stage)
    {
        $this->db->select('is.id,is.inquiry_stage');
        $this->db->from('inquiry_stage is');
        $this->db->like('is.inquiry_stage', $inquiry_stage);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'inquiry_stage' => $row->inquiry_stage,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $branch
     */
    function load_branch($branch)
    {
        $this->db->select('b.branch_id,b.branch_code,b.branch');
        $this->db->from('branch b');
        $this->db->like('b.branch', $branch);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'branch_id' => $row->branch_id,
                    'branch_code' => $row->branch_code,
                    'branch' => $row->branch,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $inquiry_status
     */
    function load_inquiry_status($inquiry_status)
    {
        $this->db->select('is.id,is.inquiry_status');
        $this->db->from('inquiry_status is');
        $this->db->like('is.inquiry_status', $inquiry_status);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'inquiry_status' => $row->inquiry_status,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $sales
     */
    function load_sales($sales)
    {
        $this->db->select('is.id,is.sales');
        $this->db->from('sales is');
        $this->db->like('is.sales', $sales);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'sales' => $row->sales,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $sales_type
     */
    function load_sales_type($sales_type)
    {
        $this->db->select('is.id,is.sales_type');
        $this->db->from('sales_type is');
        $this->db->like('is.sales_type', $sales_type);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'sales_type' => $row->sales_type,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $city
     */
    function load_city($city)
    {
        $this->db->select('ct.*,st.state,cnt.country');
        $this->db->from('city ct');
        $this->db->join('state st', 'st.state_id = ct.state_id', 'left');
        $this->db->join('country cnt', 'cnt.country_id = ct.country_id', 'left');
        $this->db->like('ct.city', $city);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'city_id' => $row->city_id,
                    'state_id' => $row->state_id,
                    'country_id' => $row->country_id,
                    'city' => $row->city,
                    'state' => $row->state,
                    'country' => $row->country,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $state
     */
    function load_state($state)
    {
        $this->db->select('st.*,cnt.country');
        $this->db->from('state st');
        $this->db->join('country cnt', 'cnt.country_id = st.country_id', 'left');
        $this->db->like('st.state', $state);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'state_id' => $row->state_id,
                    'country_id' => $row->country_id,
                    'state' => $row->state,
                    'country' => $row->country,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * @param $country
     */
    function load_country($country)
    {
        $this->db->select('cnt.*');
        $this->db->from('country cnt');
        $this->db->like('cnt.country', $country);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'country_id' => $row->country_id,
                    'country' => $row->country,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    /**
     * Load Creator
     */
    function load_creator_by()
    {
        $search = $_GET['term'];
        $city = $this->app_model->getAutoCompleteData('staff', 'name', $search, array('active !=' => 0));
        $response = array();
        $i = 0;
        foreach ($city as $cityRow) {
            $response[$i]['label'] = $cityRow->name;
            $response[$i]['value'] = $cityRow->name;
            $response[$i]['id'] = $cityRow->staff_id;
            $i++;
        }
        $response[] = array('label' => 'all', 'value' => 'all', 'id' => 'all');
        echo json_encode($response);
        exit();
    }

    function assigned_to_auto_complete()
    {
        $staff_name = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('st.staff_id,st.name');
        $this->db->from('staff st');
        $this->db->where('st.active !=', 0);
        $this->db->where('st.name LIKE "%' . $staff_name . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $staff_name != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->staff_id,
                    'value' => $row->name,
                    'label' => $row->name,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function industry_type_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('id,industry_type');
        $this->db->from('industry_type');
        $this->db->where('industry_type LIKE "%' . $search . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'value' => $row->industry_type,
                    'label' => $row->industry_type,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function priority_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('id,priority');
        $this->db->from('priority');
        $this->db->where('priority LIKE "%' . $search . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'value' => $row->priority,
                    'label' => $row->priority,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function lead_source_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('id,lead_source');
        $this->db->from('lead_source');
        $this->db->where('lead_source LIKE "%' . $search . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'value' => $row->lead_source,
                    'label' => $row->lead_source,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function lead_stage_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('id,lead_stage');
        $this->db->from('lead_stage');
        $this->db->where('lead_stage LIKE "%' . $search . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'value' => $row->lead_stage,
                    'label' => $row->lead_stage,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function lead_provider_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('st.id,st.lead_provider');
        $this->db->from('lead_provider st');
        $this->db->where('st.lead_provider LIKE "%' . $search . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'value' => $row->lead_provider,
                    'label' => $row->lead_provider,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function lead_status_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('id,lead_status');
        $this->db->from('lead_status');
        $this->db->where('lead_status LIKE "%' . $search . '%"');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'value' => $row->lead_status,
                    'label' => $row->lead_status,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function city_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('ct.*,st.state,cnt.country');
        $this->db->from('city ct');
        $this->db->join('state st', 'st.state_id = ct.state_id', 'left');
        $this->db->join('country cnt', 'cnt.country_id = ct.country_id', 'left');
        $this->db->like('ct.city', $search);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->city_id,
                    'value' => $row->city,
                    'label' => $row->city . ' - ' . $row->state,
                    'city_id' => $row->city_id,
                    'state_id' => $row->state_id,
                    'country_id' => $row->country_id,
                    'city' => $row->city,
                    'state' => $row->state,
                    'country' => $row->country,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function state_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('st.*,cnt.country');
        $this->db->from('state st');
        $this->db->join('country cnt', 'cnt.country_id = st.country_id', 'left');
        $this->db->like('st.state', $search);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->state_id,
                    'value' => $row->state,
                    'label' => $row->state . ' - ' . $row->country,
                    'country_id' => $row->country_id,
                    'country' => $row->country,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    function country_auto_complete()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('cnt.*');
        $this->db->from('country cnt');
        $this->db->like('cnt.country', $search);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0 && $search != "") {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->country_id,
                    'value' => $row->country,
                    'label' => $row->country,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }

    public function load_party_with_cnt_person()
    {
        $search = isset($_GET['term']) ? $_GET['term'] : '';
        $this->db->select('p.*,pt1.type,pt2.type,br.branch,ct.city,st.state,cnt.country');
        $this->db->from('party p');
        $this->db->join('party_type_1 pt1', 'pt1.id = p.party_type_1', 'left');
        $this->db->join('party_type_2 pt2', 'pt2.id = p.party_type_2', 'left');
        $this->db->join('branch br', 'br.branch_id = p.branch_id', 'left');
        $this->db->join('city ct', 'ct.city_id = p.city_id', 'left');
        $this->db->join('state st', 'st.state_id = p.state_id', 'left');
        $this->db->join('country cnt', 'cnt.country_id = p.country_id', 'left');
        $this->db->where('p.active', 1);
        $this->db->like('p.party_code', $search);
        $accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
        $accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
        if ($accessExport == 1 && $accessDomestic == 1) {
            $query = $this->db->get();
        } else if ($accessExport == 1) {
            $this->db->where("party_type_1", 5);
            $query = $this->db->get();
        } else if ($accessDomestic == 1) {
            $this->db->where("party_type_1", 6);
            $query = $this->db->get();
        } else {
            echo json_encode(array());
            exit();
        }
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->party_id,
                    'value' => $row->party_code,
                    'label' => $row->party_code . ' - ' . $row->party_name,
                    'party_id' => $row->party_id,
                    'party_code' => $row->party_code,
                    'party_name' => $row->party_name,
                    'address' => $row->address,
                    'city' => $row->city,
                    'city_id' => $row->city_id,
                    'state' => $row->state,
                    'state_id' => $row->state_id,
                    'country' => $row->country,
                    'country_id' => $row->country_id,
                    'fax_no' => $row->fax_no,
                    'email_id' => $row->email_id,
                    'website' => $row->website,
                    'pincode' => $row->pincode,
                    'phone_no' => $row->phone_no,
                    'contact_persons' => $this->getContactPersonByParty($row->party_id, true),
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }


    function getContactPersonByParty($party_id, $get_table_html = false)
    {
        $this->db->select('cp.name,cp.phone_no,dept.department,desi.designation');
        $this->db->from('contact_person cp');
        $this->db->join('department dept', 'dept.department_id = cp.department_id', 'left');
        $this->db->join('designation desi', 'desi.designation_id = cp.designation_id', 'left');
        $this->db->where('cp.party_id', $party_id);
        $this->db->where('cp.active !=', 0);
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            if ($get_table_html) {
                $html_response = '';
                foreach ($query->result() as $row) {
                    $html_response .= '<tr>';
                    $html_response .= '<td>' . $row->name . '</td>';
                    $html_response .= '<td>' . $row->department . '</td>';
                    $html_response .= '<td>' . $row->designation . '</td>';
                    $html_response .= '<td>' . $row->phone_no . '</td>';
                    $html_response .= '</tr>';
                }
                return $html_response;
            } else {
                foreach ($query->result() as $row) {
                    $response[] = array(
                        'name' => $row->name,
                        'phone_no' => $row->phone_no,
                        'department' => $row->department,
                        'designation' => $row->designation,
                    );
                }
                return $response;
            }
        } else {
            return $response;
        }
    }

    function get_inbox_unread_mails_v1()
    {
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            if ($this->applib->check_internet_conn()) {
                $no_unread_mails = $this->applib->count_inbox_unread_mail();
                $unread_mails = '';
                if ($no_unread_mails > 0) {
                    $unread_mails_data = $this->applib->fetch_unread_mails();
                    $unread_mails = $this->load->view('shared/unread_mails_view', array('unread_mails' => $no_unread_mails, 'unread_mails_data' => $unread_mails_data), true);
                }
                echo json_encode(array('no_unread_mails' => $no_unread_mails, 'unread_mails' => $unread_mails));
                exit();
            } else {
                echo json_encode(array('no_unread_mails' => 0, 'unread_mails' => ''));
                exit();
            }
        } else {
            $no_unread_mails = $this->applib->count_inbox_unread_mail();
            $unread_mails = '';
            if ($no_unread_mails > 0) {
                $unread_mails = $this->load->view('shared/unread_mails_view', array('unread_mails' => $no_unread_mails, 'unread_mails_data' => $this->applib->fetch_unread_mails()), true);
            }
            echo json_encode(array('no_unread_mails' => $no_unread_mails, 'unread_mails' => $unread_mails));
            exit();
        }
    }

    function get_inbox_unread_mails()
    {
        $no_unread_mails = $this->applib->count_inbox_unread_mail();
        $unread_mails_data = $this->applib->fetch_unread_mails();
        $unread_mails = $this->load->view('shared/unread_mails_view', array('unread_mails' => $no_unread_mails, 'unread_mails_data' => $unread_mails_data), true);
        echo json_encode(array('no_unread_mails' => $no_unread_mails, 'unread_mails' => $unread_mails));
        exit();
    }

    /**
     * @param $table_name
     * @param $id_column
     * @param $text_column
     * @param $search
     * @param int $page
     * @param array $where
     * @return array
     */
    function get_select2_data($table_name, $id_column, $text_column, $search, $page = 1, $where = array())
    {
        $party_select2_data = array();
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;
        $this->db->select("$id_column,$text_column");
        $this->db->from("$table_name");
        if (!empty($where)) {
            $this->db->where($where);
        }
        if ($table_name == 'party') {
            $this->db->where("active !=", 0);
            $this->db->like("$text_column", $search);
        } else {
            /*$this->db->like("$text_column", $search, 'after');*/
            $this->db->like("$text_column", $search);
        }
        $this->db->limit($resultCount, $offset);
        if ($table_name == 'party') {
			$this->db->order_by('party_id', 'DESC');
		} elseif($table_name == 'customer_feedback') {
			$this->db->order_by("id");
		}else {
			$this->db->order_by("$text_column");
		}
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $party_row) {
                $party_select2_data[] = array(
                    'id' => $party_row->$id_column,
                    'text' => $party_row->$text_column,
                );
            }
        }
        return $party_select2_data;
    }

    /**
     * @param $table_name
     * @param $id_column
     * @param $text_column
     * @param $id_column_val
     */
    function get_select2_text_by_id($table_name, $id_column, $text_column, $id_column_val)
    {
        $this->db->select("$id_column,$text_column");
        $this->db->from("$table_name");
        $this->db->where($id_column, $id_column_val);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            echo json_encode(array('success' => true, 'id' => $id_column_val, 'text' => $query->row()->$text_column));
            exit();
        }
        echo json_encode(array('success' => true, 'id' => '', 'text' => '--select--'));
        exit();
    }
    
    function get_select2_data_with_multi_column($table_name, $id_column, $text_column, $select_columns, $search, $page = 1, $where = array())
    {
        if(!empty($select_columns)){
            $select_columns = explode('||', $select_columns);
            $select_columns = implode(',', $select_columns);
        } else {
            $select_columns = '';
        }
        $party_select2_data = array();
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;
        $this->db->select("$id_column,$select_columns");
        $this->db->from("$table_name");
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->like("$text_column", $search);
        $this->db->limit($resultCount, $offset);
        $this->db->order_by("$text_column");
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            if(!empty($select_columns)){
                $select_columns = explode(',', $select_columns);
            } else {
                $select_columns = array();
            }
            foreach ($query->result() as $row) {
                $text = array();
                foreach ($select_columns as $select_column){
                    $text[] = $row->$select_column;
                }
                if(!empty($text)){
                    $text = implode(' - ', $text);
                } else {
                    $text = '';
                }
                $party_select2_data[] = array(
                    'id' => $row->$id_column,
                    'text' => $text,
                );
            }
        }
        return $party_select2_data;
    }
    
    function get_select2_text_by_id_with_multi_column($table_name, $id_column, $text_column, $select_columns, $id_column_val)
    {
        if(!empty($select_columns)){
            $select_columns = explode('||', $select_columns);
            $select_columns = implode(',', $select_columns);
        } else {
            $select_columns = '';
        }
        $this->db->select("$id_column,$select_columns");
        $this->db->from("$table_name");
        $this->db->where($id_column, $id_column_val);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            if(!empty($select_columns)){
                $select_columns = explode(',', $select_columns);
            } else {
                $select_columns = array();
            }
            $text = array();
            foreach ($select_columns as $select_column){
                $text[] = $query->row()->$select_column;
            }
            if(!empty($text)){
                $text = implode(' - ', $text);
            } else {
                $text = '';
            }
            echo json_encode(array('success' => true, 'id' => $id_column_val, 'text' => $text));
            exit();
        }
        echo json_encode(array('success' => true, 'id' => '', 'text' => '--select--'));
        exit();
    }

    /**
     * @param $table_name
     * @param $id_column
     * @param $text_column
     * @param $search
     * @param array $where
     * @return mixed
     */
    function count_select2_data($table_name, $id_column, $text_column, $search, $where = array())
    {
        $this->db->select("$id_column");
        $this->db->from("$table_name");
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->like("$text_column", $search, 'after');
        $query = $this->db->get();
        return $query->num_rows();
    }

    function party_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		$isManagement = $this->app_model->have_access_role(PARTY_MODULE_ID, "is_management");
		$where = '';
		$where .= " active != 0 ";
			if($isManagement == 1){
				if($cu_accessExport == 1 && $cu_accessDomestic == 1){
				}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
					$where .= ' AND `party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
				}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
					$where .= ' AND `party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
				}
			}else{
				$where .= ' AND ( `current_party_staff_id`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR `current_party_staff_id` IS NULL )';
				if($cu_accessExport == 1 && $cu_accessDomestic == 1){
				} else if($cu_accessExport == 1){
					$where .= ' AND `party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
				} else if($cu_accessDomestic == 1){
					$where .= ' AND `party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
				} else {}
			}
		
        $results = array(
            "results" => $this->get_select2_data('party', 'party_id', 'party_name', $search, $page, $where),
            "total_count" => $this->count_select2_data('party', 'party_id', 'party_name', $search, $where),
        );
        echo json_encode($results);
        exit();
    }

    function so_or_proforma_party_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        
        $party_id_arr = array();
        $party_ids = array();
        $sales_order_party = $this->crud->get_columns_val_by_where('sales_order', 'sales_to_party_id', array());
        foreach ($sales_order_party as $party){
            if(in_array($party, $party_id_arr)){} else {
                $party_id_arr[] = $party['sales_to_party_id'];
            }
        }
        $proforma_invoices_party = $this->crud->get_columns_val_by_where('proforma_invoices', 'sales_to_party_id', array());
        foreach ($proforma_invoices_party as $party){
            if(in_array($party, $party_id_arr)){} else {
                $party_id_arr[] = $party['sales_to_party_id'];
            }
        }
        if(!empty($party_id_arr)){
            $party_ids = implode(',', $party_id_arr);
        }
        
        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        $where = '';
        $where .= " party_id IN (".$party_ids.")";
//        $where .= ' AND ( `created_by`=' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR `created_by` IS NULL )';
        if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
        } else if ($cu_accessExport == 1) {
            $where .= ' AND `party_type_1` = ' . PARTY_TYPE_EXPORT_ID;
        } else if ($cu_accessDomestic == 1) {
            $where .= ' AND `party_type_1` = ' . PARTY_TYPE_DOMESTIC_ID;
        } else {
        }

        $results = array(
            "results" => $this->get_select2_data('party', 'party_id', 'party_name', $search, $page, $where),
//            "total_count" => $this->count_select2_data('party', 'party_id', 'party_name', $search, $where),
        );
//        echo $this->db->last_query(); 
        echo json_encode($results);
        exit();
    }
    function instalation_party_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $party_ids = array();
        $sales_order_party = $this->crud->get_columns_val_by_where('installation', 'party_id', array());
        foreach ($sales_order_party as $party){
            if(in_array($party, $party_id_arr)){} else {
                if(!empty($party['party_id'])){
                    $party_ids[] = $party['party_id'];
                }
            }
        }
        if(!empty($party_ids)){
            $party_ids = implode(',', $party_ids);
        }
        $where = " party_id IN (".$party_ids.")";
        $results = array(
            "results" => $this->get_select2_data('party', 'party_id', 'party_name', $search, $page, $where),
            "total_count" => $this->count_select2_data('party', 'party_id', 'party_name', $search, $where),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * Get all suppliers
     */
    function supplier_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $results = array(
            "results" => $this->get_select2_data('supplier', 'supplier_id', 'supplier_name', $search, $page),
            "total_count" => $this->count_select2_data('supplier', 'supplier_id', 'supplier_name', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_party_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('party', 'party_id', 'party_name', $id);
    }

    /**
     * Get suppliers
     * @param $ids
     */
    function set_supplier_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('supplier', 'supplier_id', 'supplier_name', $id);
    }

    function city_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where =  array('city !=' => '');
        $results = array(
            "results" => $this->get_select2_data('city', 'city_id', 'city', $search, $page, $where),
            "total_count" => $this->count_select2_data('city', 'city_id', 'city', $search, $where),
        );
        echo json_encode($results);
        exit();
    }

    function state_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where =  array('state !=' => '');
        $results = array(
            "results" => $this->get_select2_data('state', 'state_id', 'state', $search, $page, $where),
            "total_count" => $this->count_select2_data('state', 'state_id', 'state', $search, $where),
        );
        echo json_encode($results);
        exit();
    }

    function country_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where =  array('country !=' => '');
        $results = array(
            "results" => $this->get_select2_data('country', 'country_id', 'country', $search, $page, $where),
            "total_count" => $this->count_select2_data('country', 'country_id', 'country', $search, $where),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_city_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('city', 'city_id', 'city', $id);
    }
    function set_state_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('state', 'state_id', 'state', $id);
    }
	function set_country_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('country', 'country_id', 'country', $id);
    }

    function branch_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('branch', 'branch_id', 'branch', $search, $page),
            "total_count" => $this->count_select2_data('branch', 'branch_id', 'branch', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_branch_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('branch', 'branch_id', 'branch', $id);
    }

    function agent_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('agent', 'id', 'agent_name', $search, $page),
            "total_count" => $this->count_select2_data('agent', 'id', 'agent_name', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_agent_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('agent', 'id', 'agent_name', $id);
    }

    function stage_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('order_stage', 'id', 'order_stage', $search, $page),
            "total_count" => $this->count_select2_data('order_stage', 'id', 'order_stage', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_stage_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('order_stage', 'id', 'order_stage', $id);
    }

    function inquiry_status_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('inquiry_status', 'id', 'inquiry_status', $search, $page),
            "total_count" => $this->count_select2_data('inquiry_status', 'id', 'inquiry_status', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_inquiry_status_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('inquiry_status', 'id', 'inquiry_status', $id);
    }

    function inquiry_stage_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('inquiry_stage', 'id', 'inquiry_stage', $search, $page),
            "total_count" => $this->count_select2_data('inquiry_stage', 'id', 'inquiry_stage', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_inquiry_stage_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('inquiry_stage', 'id', 'inquiry_stage', $id);
    }

    function industry_type_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('industry_type', 'id', 'industry_type', $search, $page),
            "total_count" => $this->count_select2_data('industry_type', 'id', 'industry_type', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_industry_type_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('industry_type', 'id', 'industry_type', $id);
    }

    function technical_staff_select2_source(){
		$where = array();
		$where['department_id'] = TECHNICAL_DEPARTMENT_ID;
		$where['active'] = '1';
		$search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('staff', 'staff_id', 'name', $search, $page, $where),
            "total_count" => $this->count_select2_data('staff', 'staff_id', 'name', $search, $where),
        );
        echo json_encode($results);
        exit();
	}
    function staff_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('staff', 'staff_id', 'name', $search, $page, array('active !=' => 0)),
            "total_count" => $this->count_select2_data('staff', 'staff_id', 'name', $search, array('active !=' => 0)),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_staff_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('staff', 'staff_id', 'name', $id);
    }

    function sales_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('sales', 'id', 'sales', $search, $page),
            "total_count" => $this->count_select2_data('sales', 'id', 'sales', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_sales_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('sales', 'id', 'sales', $id);
    }

    function sales_type_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('sales_type', 'id', 'sales_type', $search, $page),
            "total_count" => $this->count_select2_data('sales_type', 'id', 'sales_type', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_sales_type_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('sales_type', 'id', 'sales_type', $id);
    }

    function set_agent_type_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('agent', 'id', 'agent_name', $id);
    }

    function reference_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('reference', 'id', 'reference', $search, $page),
            "total_count" => $this->count_select2_data('reference', 'id', 'reference', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_reference_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('reference', 'id', 'reference', $id);
    }

    function designation_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('designation', 'designation_id', 'designation', $search, $page),
            "total_count" => $this->count_select2_data('designation', 'designation_id', 'designation', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_designation_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('designation', 'designation_id', 'designation', $id);
    }

    function department_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('department', 'department_id', 'department', $search, $page),
            "total_count" => $this->count_select2_data('department', 'department_id', 'department', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_department_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('department', 'department_id', 'department', $id);
    }
    
    function grade_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('grade', 'grade_id', 'grade', $search, $page),
            "total_count" => $this->count_select2_data('grade', 'grade_id', 'grade', $search),
        );
        echo json_encode($results);
        exit();
    }
    
    function set_grade_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('grade', 'grade_id', 'grade', $id);
    }

    function item_select2_source($party_type = '') {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        $isManagement = $this->app_model->have_access_role(PARTY_MODULE_ID, "is_management");
        $where = '';
        
        if($this->applib->have_access_user_dropdown_roles(USER_DROPDOWN_HEADER_MODULE_ID,"view")){
            $where_item_active = '';
        } else {
            $where_item_active = ' AND `item_active` =1';
        }
        if ($isManagement == 1) {
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                if ($party_type == PARTY_TYPE_DOMESTIC_ID) {
                    $where .= ' `item_for_domestic` =1 '. $where_item_active;
                } else if ($party_type == PARTY_TYPE_EXPORT_ID) {
                    $where .= ' `item_for_export` =1 '. $where_item_active;
                }
            } elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
                $where .= ' `item_for_export` =1 '. $where_item_active;
            } else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
                $where .= '`item_for_domestic` =1 '. $where_item_active;
            }
        } else {
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                if ($party_type == PARTY_TYPE_DOMESTIC_ID) {
                    $where .= '`item_for_domestic` =1 '. $where_item_active;
                } else if ($party_type == PARTY_TYPE_EXPORT_ID) {
                    $where .= ' `item_for_export` =1 '. $where_item_active;
                }
            } else if ($cu_accessExport == 1) {
                $where .= '`item_for_export` =1 '. $where_item_active;
            } else if ($cu_accessDomestic == 1) {
                $where .= '`item_for_domestic` =1 '. $where_item_active;
            } else {
                
            }
        }
        $results = array(
            "results" => $this->get_select2_data('items', 'id', 'item_name', $search, $page, $where),
            "total_count" => $this->count_select2_data('items', 'id', 'item_name', $search, $where),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_item_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('items', 'id', 'item_name', $id);
    }
    function set_li_item_select2_val_by_id($id){
		$this->get_select2_text_by_id('items', 'id', 'item_name', $id);
	}
    
    function purchase_item_select2_source($item_category_id)
    {
		$where = array();
		if(!empty($item_category_id) && $item_category_id != 'null'){
			$where['item_category'] = $item_category_id;
		}
		$search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('purchase_items', 'id', 'item_name', $search, $page, $where),
            "total_count" => $this->count_select2_data('purchase_items', 'id', 'item_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    
    function purchase_item_group_wise_select2_source($item_group_id)
    {
		$where = array();
		if(!empty($item_group_id) && $item_group_id != 'null'){
			$where['item_group'] = $item_group_id;
		}
		$search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('purchase_items', 'id', 'item_name', $search, $page, $where),
            "total_count" => $this->count_select2_data('purchase_items', 'id', 'item_name', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_purchase_item_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('purchase_items', 'id', 'item_name', $id);
    }

    function quotation_type_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('quotation_type', 'id', 'quotation_type', $search, $page),
            "total_count" => $this->count_select2_data('quotation_type', 'id', 'quotation_type', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_quotation_type_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('quotation_type', 'id', 'quotation_type', $id);
    }

    function uom_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('uom', 'id', 'uom', $search, $page),
            "total_count" => $this->count_select2_data('uom', 'id', 'uom', $search),
        );
        echo json_encode($results);
        exit();
    }
    
    function get_purchase_item_uom_select2_source($purchase_item_id = ''){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $uom_arr = array();
        $uom = $this->crud->get_column_value_by_id('purchase_items', 'uom', array('id' => $purchase_item_id));
        $uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $uom));
        $uom_arr[] = array(
            'id' => $uom,
            'text' => $uom_name,
        );
        
        $required_uom = $this->crud->get_column_value_by_id('purchase_items', 'required_uom', array('id' => $purchase_item_id));
        $required_uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $required_uom));
        $uom_arr[] = array(
            'id' => $required_uom,
            'text' => $required_uom_name,
        );
        
        $results = array(
            "results" => $uom_arr,
            "total_count" => '2',
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_uom_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('uom', 'id', 'uom', $id);
    }

    function currency_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('currency', 'id', 'currency', $search, $page),
            "total_count" => $this->count_select2_data('currency', 'id', 'currency', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_currency_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('currency', 'id', 'currency', $id);
    }

    function quotation_stage_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('quotation_stage', 'id', 'quotation_stage', $search, $page),
            "total_count" => $this->count_select2_data('quotation_stage', 'id', 'quotation_stage', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_quotation_stage_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('quotation_stage', 'id', 'quotation_stage', $id);
    }

    function quotation_reason_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('quotation_reason', 'id', 'quotation_reason', $search, $page),
            "total_count" => $this->count_select2_data('quotation_reason', 'id', 'quotation_reason', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_quotation_reason_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('quotation_reason', 'id', 'quotation_reason', $id);
    }

    function quotation_status_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('quotation_status', 'id', 'quotation_status', $search, $page),
            "total_count" => $this->count_select2_data('quotation_status', 'id', 'quotation_status', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_quotation_status_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('quotation_status', 'id', 'quotation_status', $id);
    }

    function item_category_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('item_category', 'id', 'item_category', $search, $page),
            "total_count" => $this->count_select2_data('item_category', 'id', 'item_category', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_item_category_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('item_category', 'id', 'item_category', $id);
    }

    function invoice_type_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('invoice_type', 'id', 'invoice_type', $search, $page),
            "total_count" => $this->count_select2_data('invoice_type', 'id', 'invoice_type', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_invoice_type_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('invoice_type', 'id', 'invoice_type', $id);
    }

    function billing_terms_cal_codes_select2_source($billing_terms_id)
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('billing_terms_detail', 'id', 'cal_code', $search, $page, array('billing_terms_id' => $billing_terms_id)),
            "total_count" => $this->count_select2_data('billing_terms_detail', 'id', 'cal_code', $search, array('billing_terms_id' => $billing_terms_id)),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_billing_terms_cal_codes_select2_val_by_id($id)
    {
        $this->get_select2_text_by_id('billing_terms_detail', 'id', 'cal_code', $id);
    }

    function get_email_report()
    {
        $date = $this->input->get_post('date');
        $date = strtotime($date) > 0 ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
        $emails_data = $this->crud->get_all_with_where('mail_system', 'received_at', 'desc', array('DATE(created_at)' => $date, 'staff_id' => $this->session->userdata('is_logged_in')['staff_id']));
        $email_report_html = $this->load->view('shared/email_report_html', array('emails_data' => $emails_data), true);
        echo json_encode(array("success" => true, "email_report_html" => $email_report_html));
        exit();
    }

    /**
     * @param null $dir
     */
    function delete_old_files($dir = null){
        if(!$this->app_model->have_access_role(CRON_REMOVE_OLD_FILES_MENU_ID, "view")){
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
        if ($dir == null) {
            $dir = './uploads/';
        }

        $structure = glob(rtrim($dir, "/") . '/*');
        if (is_array($structure)) {
            foreach ($structure as $file) {
                if (is_dir($file)) {
                    $this->delete_old_files($file);
                } elseif (is_file($file)) {
                    $lastmod = filemtime($file);
                    //24 hours in a day * 3600 seconds per hour
                    if ((time() - $lastmod) > 24 * 3600) {
                        unlink($file);
                    }
                }
            }
        }
        $dirlastmod = filemtime($dir);
        if ((time() - $dirlastmod) > 24 * 3600) {
            rmdir($dir);
        }
        echo "Success !";
    }
    
    function terms_and_conditions_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('terms_and_conditions', 'id', 'module', $search, $page),
            "total_count" => $this->count_select2_data('terms_and_conditions', 'id', 'module', $search),
        );
        echo json_encode($results);
        exit();
    }

    /**
     * @param $id
     */
    function set_terms_and_conditions_select2_val_by_id($id){
        $this->get_select2_text_by_id('terms_and_conditions', 'id', 'module', $id);
    }
    
    function challan_item_gear_type_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_gear_type', 'gear_type_name', 'gear_type_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_gear_type', 'gear_type_name', 'gear_type_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_gear_type_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_gear_type', 'gear_type_name', 'gear_type_name', $id);
    }
    
    function challan_item_hp_kw_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_hp_kw', 'hp_kw_name', 'hp_kw_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_hp_kw', 'hp_kw_name', 'hp_kw_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_hp_kw_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_hp_kw', 'hp_kw_name', 'hp_kw_name', $id);
    }
    
    function challan_item_make_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_make', 'make_name', 'make_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_make', 'make_name', 'make_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_make_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_make', 'make_name', 'make_name', $id);
    }
    
    function challan_item_model_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_model', 'model_name', 'model_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_model', 'model_name', 'model_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_model_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_model', 'model_name', 'model_name', $id);
    }
    
    function challan_item_ratio_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_ratio', 'ratio_name', 'ratio_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_ratio', 'ratio_name', 'ratio_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_ratio_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_ratio', 'ratio_name', 'ratio_name', $id);
    }
    
    function challan_item_rpm_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_rpm', 'rpm_name', 'rpm_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_rpm', 'rpm_name', 'rpm_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_rpm_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_rpm', 'rpm_name', 'rpm_name', $id);
    }
    
    function challan_item_user_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_user', 'user_name', 'user_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_user', 'user_name', 'user_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_user_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_user', 'user_name', 'user_name', $id);
    }
    
    function challan_item_volts_cycles_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_volts_cycles', 'volts_cycles_name', 'volts_cycles_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_volts_cycles', 'volts_cycles_name', 'volts_cycles_name', $search),
        );
        echo json_encode($results);
        exit();
    }
	function set_challan_item_volts_cycles_select2_val_by_id($id){
		$id = str_replace('__', ' ', $id);
		$id = str_replace('--', '/', $id);
        $this->get_select2_text_by_id('challan_item_volts_cycles', 'volts_cycles_name', 'volts_cycles_name', $id);
    }
    
    function delivery_through_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('delivery_through', 'delivery_through_id', 'delivery_through', $search, $page),
            "total_count" => $this->count_select2_data('delivery_through', 'delivery_through_id', 'delivery_through', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_delivery_through_select2_val_by_id($id){
        $this->get_select2_text_by_id('delivery_through', 'delivery_through_id', 'delivery_through', $id);
    }
    
    function item_extra_accessories_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('item_extra_accessories', 'item_extra_accessories_id', 'item_extra_accessories_value', $search, $page),
            "total_count" => $this->count_select2_data('item_extra_accessories', 'item_extra_accessories_id', 'item_extra_accessories_value', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_item_extra_accessories_select2_val_by_id($id){
        $this->get_select2_text_by_id('item_extra_accessories', 'item_extra_accessories_id', 'item_extra_accessories_value', $id);
    }
    
    function customer_reminder_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('customer_reminder', 'id', 'topic', $search, $page),
            "total_count" => $this->count_select2_data('customer_reminder', 'id', 'topic', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_customer_reminder_select2_val_by_id($id){
        $this->get_select2_text_by_id('customer_reminder', 'id', 'topic', $id);
    }
    
    function db_backup(){
//        $this->load->helper('file');
////        ini_set("display_errors", "1");
////        error_reporting(E_ALL);
//        $this->load->dbutil();
//        $prefs = array(     
//            'format'      => 'zip',             
//            'filename'    => 'jaykhodiyar_'. date("Y-m-d-H-i-s").'.sql'
//        );
//        $backup = $this->dbutil->backup($prefs);
//        $folder = $_SERVER['DOCUMENT_ROOT'] . '/db_backup/';
//        if (!is_dir($folder)){
//			mkdir($folder, 0777, true);
//        }
//		chmod($folder, 0777);
//		$zip_name = $folder.'jaykhodiyar_'. date("Y-m-d-H-i-s") .'.zip';
//		write_file($zip_name, $backup); 
//		//~ $handle = fopen($zip_name,'w+');
//		//~ fwrite($handle,$backup);
//		//~ fclose($handle);
//		echo "Backup of Database Taken";
        
        if($this->applib->have_access_role(CRON_MENU_ID,"view")) {
            $this->load->dbutil();
            $prefs = array(
                'format'      => 'zip',
                'filename'    => 'jaykhodiyar_data_backup_'. date("Y-m-d-H-i-s").'.sql'
            );
            $backup =& $this->dbutil->backup($prefs);
            $db_name = 'jaykhodiyar_data_backup_'. date("Y-m-d-H-i-s").'.zip';
            $this->load->helper('file');
            $this->load->helper('download');
            force_download($db_name, $backup);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
    
    function software_backup(){
        if($this->applib->have_access_role(CRON_MENU_ID,"view")) {
            
//            $dir = FCPATH;
//            $zip_file = 'jaykhodiyar_'. date("Y-m-d-H-i-s");
//
//            // Get real path for our folder
//            $rootPath = realpath($dir);
//
//            // Initialize archive object
//            $zip = new ZipArchive();
//            $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
//
//            // Create recursive directory iterator
//            /** @var SplFileInfo[] $files */
//            $files = new RecursiveIteratorIterator(
//                new RecursiveDirectoryIterator($rootPath),
//                RecursiveIteratorIterator::LEAVES_ONLY
//            );
//
//            foreach ($files as $name => $file)
//            {
//                // Skip directories (they would be added automatically)
//                if (!$file->isDir())
//                {
//                    // Get real and relative path for current file
//                    $filePath = $file->getRealPath();
//                    $relativePath = substr($filePath, strlen($rootPath) + 1);
//
//                    // Add current file to archive
//                    $zip->addFile($filePath, $relativePath);
//                }
//            }
//
//            // Zip archive will be created only after closing object
//            $zip->close();
//
//
//            header('Content-Description: File Transfer');
//            header('Content-Type: application/octet-stream');
//            header('Content-Disposition: attachment; filename='.basename($zip_file));
//            header('Content-Transfer-Encoding: binary');
//            header('Expires: 0');
//            header('Cache-Control: must-revalidate');
//            header('Pragma: public');
//            header('Content-Length: ' . filesize($zip_file));
//            readfile($zip_file);
            
            $this->load->library('zip');
            $dir = FCPATH;
            $this->zip->read_dir($dir);
            $fileName = 'jaykhodiyar_software_backup_'. date("Y-m-d-H-i-s").'.zip';
            $this->zip->download($fileName);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
    
    function logs_backup(){
        if($this->applib->have_access_role(CRON_MENU_ID,"view")) {
            $this->load->dbutil();
            $this->other_db = $this->load->database('log', TRUE);
            $this->myutil = $this->load->dbutil($this->other_db, TRUE);
            $prefs = array(
                'format'      => 'zip',
                'filename'    => 'jaykhodiyar_logs_backup_'. date("Y-m-d-H-i-s").'.sql'
            );
            $backup =& $this->myutil->backup($prefs);
            $db_name = 'jaykhodiyar_logs_backup_'. date("Y-m-d-H-i-s").'.zip';
            $this->load->helper('file');
            $this->load->helper('download');
            force_download($db_name, $backup);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }
        
    function get_terms_and_conditions(){
        //pre_die($_POST);
        if($_POST['party_type'] == PARTY_TYPE_DOMESTIC_ID){
            $data = $this->applib->getTermsandConditions($_POST['module'].'_for_domestic');
        }elseif($_POST['party_type'] == PARTY_TYPE_EXPORT_ID){
            $data = $this->applib->getTermsandConditions($_POST['module'].'_for_export');
        }else{
            $data = $this->applib->getTermsandConditions($_POST['module']);
        }
        echo json_encode(array("terms_and_conditions" => $data));
    }
    
    function followup_category_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('followup_category', 'fc_id', 'fc_name', $search, $page),
            "total_count" => $this->count_select2_data('followup_category', 'fc_id', 'fc_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_followup_category_select2_val_by_id($id){
        $this->get_select2_text_by_id('followup_category', 'fc_id', 'fc_name', $id);
    }
    
    function get_quotation_reminder_notify(){
        
        $query = $this->db->query("SELECT followup_history.* from followup_history,staff where TIMESTAMPDIFF(MINUTE, followup_history.reminder_date, NOW()) > 0 AND (staff.staff_id = followup_history.followup_by OR FIND_IN_SET( staff.staff_id, followup_history.assigned_to)) AND followup_history.is_notified = 0 AND staff.is_login = 1 GROUP BY followup_history.id");
//        echo $this->db->last_query();
//        echo '<pre>'; print_r($query->result());
//        exit;
		if($query->num_rows()){
            $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
			$client->initialize();
			foreach ($query->result() as $row){
//				$result = $this->crud->update('followup_history', array('is_notified' => 1), array('id' => $row->id));
//				if($result){
                    $assigned_to_ids = array();
                    if(!empty($row->assigned_to)){
                        $assigned_to_ids = explode(',', $row->assigned_to);
                    }
                    $assigned_to_ids[] = $row->followup_by;
                    if (in_array($this->session->userdata['is_logged_in']['staff_id'], $assigned_to_ids)){
                        $message = $row->reminder_message;
                        $party_name = '';
                        $quotation_no = '';
                        $quotation_data = $this->crud->get_row_by_id('quotations', array('id' => $row->quotation_id));
                        if(!empty($quotation_data)){
                            $quotation_data = $quotation_data[0];
                            $quotation_no = $quotation_data->quotation_no;
                            $party_name = $this->crud->get_id_by_val('party', 'party_name', 'party_id', $quotation_data->party_id);
                        }
                        $followup_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $row->followup_by);
                        $client->emit('notify_quotation_reminder', ['user_id'=>$row->followup_by, 'message' => $message, 'followup_by' => $followup_by, 'quotation_no' => $quotation_no, 'party_name' => $party_name, 'fh_id' => $row->id]);
                    }
//				}
			}
			$client->close();
		}
    }
    
    function item_group_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('item_group_code', 'id', 'ig_code', $search, $page),
            "total_count" => $this->count_select2_data('item_group_code', 'id', 'ig_code', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_item_group_select2_val_by_id($id){
        $this->get_select2_text_by_id('item_group_code', 'id', 'ig_code', $id);
    }
    
    function raw_material_group_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('raw_material_group', 'id', 'group_name', $search, $page),
            "total_count" => $this->count_select2_data('raw_material_group', 'id', 'group_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_raw_material_group_select2_val_by_id($id){
        $this->get_select2_text_by_id('raw_material_group', 'id', 'group_name', $id);
    }
    
    function supplier_make_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('supplier_make', 'id', 'supplier_make_name', $search, $page),
            "total_count" => $this->count_select2_data('supplier_make', 'id', 'supplier_make_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_supplier_make_select2_val_by_id($id){
        $this->get_select2_text_by_id('supplier_make', 'id', 'supplier_make_name', $id);
    }
    
    function purchase_project_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('purchase_project', 'project_id', 'project_name', $search, $page),
            "total_count" => $this->count_select2_data('purchase_project', 'project_id', 'project_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_purchase_project_select2_val_by_id($id){
        $this->get_select2_text_by_id('purchase_project', 'project_id', 'project_name', $id);
    }
    
    function material_process_type_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('material_process_type', 'id', 'material_process_type', $search, $page),
            "total_count" => $this->count_select2_data('material_process_type', 'id', 'material_process_type', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_material_process_type_select2_val_by_id($id){
        $this->get_select2_text_by_id('material_process_type', 'id', 'material_process_type', $id);
    }
    
    function item_status_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('item_status', 'id', 'item_status', $search, $page),
            "total_count" => $this->count_select2_data('item_status', 'id', 'item_status', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_item_status_select2_val_by_id($id){
        $this->get_select2_text_by_id('item_status', 'id', 'item_status', $id);
    }
    
    function challan_item_make_id_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('challan_item_make', 'id', 'make_name', $search, $page),
            "total_count" => $this->count_select2_data('challan_item_make', 'id', 'make_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_challan_item_make_id_select2_val_by_id($id){
        $this->get_select2_text_by_id('challan_item_make', 'id', 'make_name', $id);
    }
    
	
	function sea_freight_type_select2_source()
	{
		$search = isset($_GET['q']) ? $_GET['q'] : '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$results = array(
			"results" => $this->get_select2_data('sea_freight_type', 'id', 'name', $search, $page),
			"total_count" => $this->count_select2_data('sea_freight_type', 'id', 'name', $search),
		);
		echo json_encode($results);
		exit();
	}
	function set_sea_freight_type_select2_val_by_id($id)
	{
		$this->get_select2_text_by_id('sea_freight_type', 'id', 'name', $id);
	}
	
	function quotation_no_select2_source()
	{
		$search = isset($_GET['q']) ? $_GET['q'] : '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
						
		$where = '';
		$where .= '`quotation_status_id` = '.QUOTATION_DEFAULT_STATUS_ID;
		$where .= ' AND `party_id` = '.$_GET['party_id'];
		
		$results = array(
			"results" => $this->get_select2_data('quotations', 'id', 'quotation_no', $search, $page, $where),
			"total_count" => $this->count_select2_data('quotations', 'id', 'quotation_no', $search, $where),
		);
		echo json_encode($results);
		exit();
	}
	function set_quotation_no_select2_val_by_id($id)
	{
		$this->get_select2_text_by_id('quotations', 'id', 'quotation_no', $id );
	}
	
	function sales_order_no_select2_source()
	{
		$search = isset($_GET['q']) ? $_GET['q'] : '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$where = '';
		$where .= '`sales_to_party_id` = '.$_GET['party_id'];
		$results = array(
			"results" => $this->get_select2_data('sales_order', 'id', 'sales_order_no', $search, $page, $where),
			"total_count" => $this->count_select2_data('sales_order', 'id', 'sales_order_no', $search, $where),
		);
		echo json_encode($results);
		exit();
	}
	function set_sales_order_no_select2_val_by_id($id)
	{
		$this->get_select2_text_by_id('sales_order', 'id', 'sales_order_no', $id);
	}

    function payment_by_select2_source()
	{
		$search = isset($_GET['q']) ? $_GET['q'] : '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$results = array(
			"results" => $this->get_select2_data('payment_by', 'id', 'name', $search, $page),
			"total_count" => $this->count_select2_data('payment_by', 'id', 'name', $search),
		);
		echo json_encode($results);
		exit();
	}
	function set_payment_by_select2_val_by_id($id)
	{
		$this->get_select2_text_by_id('payment_by', 'id', 'name', $id);
	}
	
	function challan_with_item_select2_source($party_id){
		$search = isset($_GET['q']) ? $_GET['q'] : '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		
        $select2_data = array();
		$resultCount = 10;
		$offset = ($page - 1) * $resultCount;
		$this->db->select("c.id, c.challan_no, ci.item_name");
		$this->db->from("challans c");
		$where = array();
        $where['is_invoice_created'] = 0;
		if (!empty($party_id)) {
			$where['sales_to_party_id'] = $party_id;
		}
        $this->db->join('challan_items ci','ci.challan_id = c.id','left');
		$this->db->where("(c.challan_no LIKE '%$search%' OR ci.item_name LIKE '%$search%')");
		$this->db->where($where);
		$this->db->limit($resultCount, $offset);
		$this->db->order_by("c.challan_no");
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$select2_data[] = array(
					'id' => $row->id,
					'text' => 'Challan No.: '. $row->challan_no .' - '.$row->item_name,
				);
			}
		}
        
		$results = array(
			"results" => $select2_data,
			"total_count" => $this->count_select2_data('challans', 'id', 'challan_no', $search, $where),
		);
        echo json_encode($results);
		exit();
	}
	function set_challan_with_item_select2_val_by_id($id){
        $this->get_select2_text_by_id('challans', 'id', 'challan_no', $id);
//        $this->db->select("c.id, c.challan_no, ci.item_name");
//		$this->db->from("challans c");
//		$this->db->join('challan_items ci','ci.challan_id = c.id','left');
//		$this->db->where('c.id', $id);
//        $this->db->limit(1);
//        $query = $this->db->get();
//        if ($query->num_rows() > 0) {
//            echo json_encode(array('success' => true, 'id' => $id_column_val, 'text' => $query->row()->$text_column));
//            exit();
//        }
//        echo json_encode(array('success' => true, 'id' => '', 'text' => '--select--'));
//        exit();
	}
    
    function electric_power_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('electric_power', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('electric_power', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_electric_power_select2_val_by_id($id){
        $this->get_select2_text_by_id('electric_power', 'id', 'label', $id);
    }
    
    function available_or_not_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('available_or_not', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('available_or_not', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_available_or_not_select2_val_by_id($id){
        $this->get_select2_text_by_id('available_or_not', 'id', 'label', $id);
    }
    
    function water_tank_or_chiller_system_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('water_tank_or_chiller_system', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('water_tank_or_chiller_system', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_water_tank_or_chiller_system_select2_val_by_id($id){
        $this->get_select2_text_by_id('water_tank_or_chiller_system', 'id', 'label', $id);
    }
    
    function water_plumbing_or_chiller_conn_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('water_plumbing_or_chiller_conn', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('water_plumbing_or_chiller_conn', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_water_plumbing_or_chiller_conn_select2_val_by_id($id){
        $this->get_select2_text_by_id('water_plumbing_or_chiller_conn', 'id', 'label', $id);
    }
    
    function lubrication_gear_oil_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('lubrication_gear_oil', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('lubrication_gear_oil', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_lubrication_gear_oil_select2_val_by_id($id){
        $this->get_select2_text_by_id('lubrication_gear_oil', 'id', 'label', $id);
    }
    
    function customer_feedback_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('customer_feedback', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('customer_feedback', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_customer_feedback_select2_val_by_id($id){
        $this->get_select2_text_by_id('customer_feedback', 'id', 'label', $id);
    }
    
    function sms_topic_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('sms_topic', 'id', 'topic_name', $search, $page),
            "total_count" => $this->count_select2_data('sms_topic', 'id', 'topic_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_sms_topic_select2_val_by_id($id){
        $this->get_select2_text_by_id('sms_topic', 'id', 'topic_name', $id);
    }
    
    function service_type_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('service_type', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('service_type', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_service_type_select2_val_by_id($id){
        $this->get_select2_text_by_id('service_type', 'id', 'label', $id);
    }
	
	function letter_for_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('letter_for', 'id', 'label', $search, $page),
            "total_count" => $this->count_select2_data('letter_for', 'id', 'label', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_letter_for_select2_val_by_id($id){
        $this->get_select2_text_by_id('letter_for', 'id', 'label', $id);
    }
	
	function letter_select2_source(){
		$user_roles = $this->app_model->getUserPrintLetterIDS($this->session->userdata('is_logged_in')['staff_id']);
		//$user_roles = implode(',',$user_roles);
		//echo '<pre>';print_r($user_roles);exit;
		
		$search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where = '';
        $where .= '`letter_for` = '.$_GET['letter_for'];
        $results = array(
            "results" => $this->get_letter_select2_data('letters', 'letter_id', 'letter_topic', $search, $page, $where ,$user_roles),
            "total_count" => $this->count_letter_select2_data('letters', 'letter_id', 'letter_topic', $search, $where, $user_roles),
        );
        echo json_encode($results);
        exit();
    }
    
    function party_select2_source_from_sales_order(){
        $to_ports = $this->crud->get_column_data_array('sales_order', 'sales_to_party_id');    
        $to_ports_arr = array();
        foreach ($to_ports as $key => $to_port) {
            array_push($to_ports_arr,json_decode($to_port));
        }
        $to_ports_arr = array_unique($this->array_flatten($to_ports_arr));
        
		$search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where = '';
        $results = array(
            "results" => $this->get_letter_select2_data('party', 'party_id', 'party_name', $search, $page, $where ,$to_ports_arr),
            "total_count" => $this->count_letter_select2_data('party', 'party_id', 'party_name', $search, $where, $to_ports_arr),
        );
        echo json_encode($results);
        exit();
    }
    
    function party_select2_source_from_quotation(){
        $to_ports = $this->crud->get_column_data_array('quotations', 'party_id');
        $to_ports_arr = array();
        foreach ($to_ports as $key => $to_port) {
            array_push($to_ports_arr,json_decode($to_port));
        }
        $to_ports_arr = array_unique($this->array_flatten($to_ports_arr));
        
		$search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where = '';
        $results = array(
            "results" => $this->get_letter_select2_data('party', 'party_id', 'party_name', $search, $page, $where ,$to_ports_arr),
            "total_count" => $this->count_letter_select2_data('party', 'party_id', 'party_name', $search, $where, $to_ports_arr),
        );
        echo json_encode($results);
        exit();
    }
    function party_select2_source_from_complain(){
        $to_ports = $this->crud->get_column_data_array('complain', 'party_id');
        $to_ports_arr = array();
        foreach ($to_ports as $key => $to_port) {
            array_push($to_ports_arr,json_decode($to_port));
        }
        $to_ports_arr = array_unique($this->array_flatten($to_ports_arr));
        
		$search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where = '';
        $results = array(
            "results" => $this->get_letter_select2_data('party', 'party_id', 'party_name', $search, $page, $where ,$to_ports_arr),
            "total_count" => $this->count_letter_select2_data('party', 'party_id', 'party_name', $search, $where, $to_ports_arr),
        );
        echo json_encode($results);
        exit();
    }
    
    public function array_flatten($array) { 
        if (!is_array($array)) { 
            return false; 
        } 
        $result = array(); 
        foreach ($array as $key => $value) { 
            if (is_array($value)) { 
                $result = array_merge($result, $this->array_flatten($value)); 
            } else { 
                $result[$key] = $value; 
            } 
        } 
        return $result; 
    }
	
	function get_letter_select2_data($table_name, $id_column, $text_column, $search, $page = 1, $where = array(), $where_in){
		$party_select2_data = array();
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;
        $this->db->select("$id_column,$text_column");
        $this->db->from("$table_name");
        if (!empty($where)) {
            $this->db->where($where);
        }
		if (!empty($where_in)) {
            $this->db->group_start();
            $where_ins_chunk = array_chunk($where_in,500);
            foreach($where_ins_chunk as $where_ins)
            {
                $this->db->or_where_in($id_column, $where_ins);
            }
            $this->db->group_end();
            //$this->db->where_in($id_column, $where_in);
        }
        $this->db->like("$text_column", $search);
        $this->db->limit($resultCount, $offset);
        $this->db->order_by("$text_column");
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $party_row) {
                $party_select2_data[] = array(
                    'id' => $party_row->$id_column,
                    'text' => $party_row->$text_column,
                );
            }
        }
        return $party_select2_data;
    }
	
	function count_letter_select2_data($table_name, $id_column, $text_column, $search, $where = array(), $where_in)
    {
        $this->db->select("$id_column");
        $this->db->from("$table_name");
        if (!empty($where)) {
            $this->db->where($where);
        }
		if (!empty($where_in)) {
            $this->db->group_start();
            $where_ins_chunk = array_chunk($where_in,500);
            foreach($where_ins_chunk as $where_ins)
            {
                $this->db->or_where_in($id_column, $where_ins);
            }
            $this->db->group_end();
            //$this->db->where_in($id_column, $where_in);
        }
        $this->db->like("$text_column", $search, 'after');
        $query = $this->db->get();
        return $query->num_rows();
    }

    
    function interviewed_person_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('interview', 'interview_id', 'name', $search, $page),
            "total_count" => $this->count_select2_data('interview', 'interview_id', 'name', $search),
        );
        echo json_encode($results);
        exit();
    }
	
	function load_text_suggetion($text_suggestion){
        $text_suggestion = urldecode($text_suggestion);
        $logged_in = $this->session->userdata("is_logged_in");
        $this->db->select('ts.id,ts.text_suggestion');
        $this->db->from('text_suggestion ts');
        $this->db->where('staff_id', $logged_in['staff_id']);
        $this->db->like('ts.text_suggestion', $text_suggestion,'after');
        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'text_suggestion' => $row->text_suggestion,
                );
                $i++;
            }
        }
        echo json_encode($response);
        exit();
    }
    
    function parts_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
            "results" => $this->get_select2_data('parts', 'id', 'part_name', $search, $page),
            "total_count" => $this->count_select2_data('parts', 'id', 'part_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_parts_select2_val_by_id($id){
        $this->get_select2_text_by_id('parts', 'id', 'part_name', $id);
    }
    
    function complain_select2_source($party_id){
		$search = isset($_GET['q']) ? $_GET['q'] : '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		
        $select2_data = array();
		$resultCount = 10;
		$offset = ($page - 1) * $resultCount;
		$this->db->select("c.complain_id, c.complain_no, c.complain_no_year");
		$this->db->from("complain c");
		$where = array();
        if (!empty($party_id)) {
			$where['party_id'] = $party_id;
		}
        $where['service_type'] = PAID_SERVICE_TYPE_ID;
        $this->db->where($where);
		$this->db->limit($resultCount, $offset);
		$this->db->order_by("c.complain_no");
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$select2_data[] = array(
					'id' => $row->complain_id,
					'text' => 'Complain No.: '. $row->complain_no_year,
				);
			}
		}
        
		$results = array(
			"results" => $select2_data,
			"total_count" => $this->count_select2_data('complain', 'complain_id', 'complain_no', $search, $where),
		);
        echo json_encode($results);
		exit();
	}
    function set_complain_select2_val_by_id($id){
        $this->get_select2_text_by_id('complain', 'complain_id', 'complain_no_year', $id);
    }
    
    function complain_no_select2_source()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $where = '';
        $where .= '`party_id` = '.$_GET['party_id'];
        $results = array(
                "results" => $this->get_select2_data('complain', 'complain_id', 'complain_no_year', $search, $page, $where),
                "total_count" => $this->count_select2_data('complain', 'complain_id', 'complain_no_year', $search, $where),
        );
        echo json_encode($results);
        exit();
    }
    
    function sales_item_select2_source(){
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $results = array(
                "results" => $this->get_select2_data('items', 'id', 'item_name', $search, $page),
//                "results" => $this->get_select2_data_with_multi_column('items', 'id', 'item_name', 'item_code1||item_name', $search, $page),
                "total_count" => $this->count_select2_data('items', 'id', 'item_name', $search),
        );
        echo json_encode($results);
        exit();
    }
    function set_sales_item_select2_val_by_id($id){
        $this->get_select2_text_by_id('items', 'id', 'item_name', $id);
//        $this->get_select2_text_by_id_with_multi_column('items', 'id', 'item_name', 'item_code1||item_name', $id);
    }
    
    function get_item_code_by_id($id){
        $this->get_select2_text_by_id('items', 'id', 'item_code1', $id);
    }
    
    function update_staff_socket_id($socket_id){
        $logged_in = $this->session->userdata("is_logged_in");
        $this->app_model->update_staff_socket_id($logged_in['staff_id'], $socket_id);
        
        $name = $this->app_model->get_id_by_val('staff','name','staff_id',$logged_in['staff_id']);
        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
        $client->initialize();
        $client->emit('staff_is_back', ['staff_id'=>$logged_in['staff_id'], 'name' => $name]);
        $client->close();
    }
    
    function update_employee_code() {
        $staff = $this->crud->get_all_records('staff','staff_id','ASC');
        $employee_code = 0;
        if(!empty($staff)){
            foreach ($staff as $staf){
                $employee_codes = $this->crud->get_max_number('staff', 'employee_code');
                $employee_code = 101;
                if ($employee_codes->employee_code > 0) {
                    $employee_code = $employee_codes->employee_code + 1;
                }
                $this->crud->update('staff',array('employee_code' => $employee_code), array('staff_id' => $staf->staff_id));
            }
        }
        echo 'success'; 
    }
    
    function get_select2_text_by_id_multiple($table_name, $id_column, $text_column, $id_column_val){
		$this->db->select("$id_column,$text_column");
		$this->db->from("$table_name");
		$this->db->where_in($id_column, $id_column_val);
		$query = $this->db->get();
		$row_data = array();
		if ($query->num_rows() > 0) {
			$inc = 0;
			foreach ($query->result() as $row) {
				$row_data[$inc]['id'] = $row->$id_column;
				$row_data[$inc]['text'] = $row->$text_column;
				$inc++;
			}
		} else {
			//$row_data[] = array('id' => '', 'text' => '--select--');
		}
		echo json_encode(array('success' => true, $row_data));
		exit();
	}
    
    function set_challan_item_make_id_select2_multi_val_by_id($ids){
        $myArray = explode(',', $ids);
        $this->get_select2_text_by_id_multiple('challan_item_make', 'id', 'make_name', $myArray);
    }
    
    function set_raw_material_group_select2_multi_val_by_id($ids){
        $myArray = explode(',', $ids);
        $this->get_select2_text_by_id_multiple('raw_material_group', 'id', 'group_name', $myArray);
    }
    
    function set_supplier_make_select2_multi_val_by_id($ids){
        $myArray = explode(',', $ids);
        $this->get_select2_text_by_id_multiple('supplier_make', 'id', 'supplier_make_name', $myArray);
    }
}
