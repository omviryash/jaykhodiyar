<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
/**
 * Class Mailbox_system1
 * &@property Datatables $datatable
 */
class Mail_system2 extends CI_Controller
{
    private $email_address = SMTP_EMAIL_ADDRESS;
    private $email_password = SMTP_EMAIL_PASSWORD;
    private $email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';
    private $staff_id = 1;

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->email_address = trim($this->session->userdata('is_logged_in')['mailbox_email']);
        $this->email_password = trim($this->session->userdata('is_logged_in')['mailbox_password']);
        $this->staff_id = trim($this->session->userdata('is_logged_in')['staff_id']);
        $this->load->library('user_agent');
    }

    function check_internet_conn()
    {
        return checkdnsrr('php.net') ? true : false;
    }

    function inbox()
    {
        $count_unseen_mails = $this->get_count_unread_mails('inbox');
        set_page('mail_system2/mailbox',array('folder'=>'inbox','count_unseen_mails'=>$count_unseen_mails));
    }

    function drafts()
    {
        set_page('mail_system2/mailbox',array('folder'=>'drafts'));
    }

    function outbox()
    {
        set_page('mail_system2/mailbox',array('folder'=>'outbox'));
    }

    function sent()
    {
        set_page('mail_system2/mailbox',array('folder'=>'sent'));
    }

    function trash()
    {
        set_page('mail_system2/mailbox',array('folder'=>'trash'));
    }

    function folder_mails($folder)
    {
        $folder = dash_to_space($folder);
        set_page('mail_system2/mailbox',array('folder'=>$folder));
    }

    function get_count_unread_mails($folder = 'inbox'){
        $this->db->select('mail_id');
        $this->db->from('mail_system');
        $this->db->where('folder',$folder);
        $this->db->where('is_unread',1);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function read()
    {
        $mail_id = $_POST['mail_id'];
        if ($this->input->is_ajax_request()) {
            $mail_detail = $this->fetch_mail_detail($mail_id);
            $mail_view = $this->load->view('mail_system2/read_email', array('mail_detail'=>$mail_detail), true);
            $no_unread_mails = $this->get_count_unread_mails($mail_detail['folder']);
            $unread_mails_data = $this->applib->fetch_unread_mails();
            $unread_mails = $this->load->view('shared/unread_mails_view',array('unread_mails'=>$no_unread_mails,'unread_mails_data'=>$unread_mails_data),true);
            echo json_encode(array('email_detail' => $mail_view, 'no_unread_mails' => $no_unread_mails, 'unread_mails' => $unread_mails));
            exit();
        } else {
            $mail_detail = $this->fetch_mail_detail($mail_id);
            set_page('mail_system2/mailbox', array('mail_detail' =>$mail_detail,'folder' =>$mail_detail['folder'],'mail_id' => $mail_id));
        }
    }

    function compose_email($action = "compose")
    {
        if (!empty($_POST['to'])) {

            /*------- If Save To Drafts --------*/
            if(isset($_POST['btn_draft'])) {
                if(isset($_POST['mail_id'])) {
                    $this->db->select('*');
                    $this->db->from('mail_system');
                    $this->db->where('mail_id', $_POST['mail_id']);
                    $this->db->limit(1);
                    $query = $this->db->get();
                    $mail_data = $query->row_array(0);
                    $mail_data['from_address'] = $this->email_address;
                    $mail_data['from_name'] = $this->email_address;
                    $mail_data['to_address'] = $_POST['to'];
                    $mail_data['to_name'] = $_POST['to'];
                    $mail_data['subject'] = $_POST['subject'];
                    $mail_data['body'] = $_POST['body'];
                    $mail_data['folder'] = 'drafts';
                    $mail_data['created_at'] = date('Y-m-d H:i:s');
                    $mail_data['received_at'] = date('Y-m-d H:i:s');
                    $mail_data['attachments'] = $mail_data['attachments'] != null?unserialize($mail_data['attachments']):null;
                    if(isset($_POST['attachments']) && count($_POST['attachments']) > 0){
                        foreach($_POST['attachments'] as $attachment_row){
                            if($attachment_row != ''){
                                $mail_data['attachments'][] = array(
                                    'filename' => basename($attachment_row, PATHINFO_FILENAME),
                                    'url' => $attachment_row,
                                    'subtype' => pathinfo($attachment_row, PATHINFO_EXTENSION),
                                );
                            }
                        }
                    }
                    $this->db->insert('mail_system', $mail_data);
                }else{
                    $attachments = array();
                    if(isset($_POST['attachments']) && count($_POST['attachments']) > 0){
                        foreach($_POST['attachments'] as $attachment_row){
                            if($attachment_row != ''){
                                $attachments[] = array(
                                    'filename' => basename($attachment_row, PATHINFO_FILENAME),
                                    'url' => $attachment_row,
                                    'subtype' => pathinfo($attachment_row, PATHINFO_EXTENSION),
                                );
                            }
                        }
                    }
                    $email_data = array(
                        'from_address' => $this->email_address,
                        'from_name' => $this->email_address,
                        'to_address' => $_POST['to'],
                        'to_name' => $_POST['to'],
                        'subject' => $_POST['subject'],
                        'body' => $_POST['body'],
                        'attachments' => count($attachments) > 0 ? serialize($attachments) : null,
                        'is_attachment' => count($attachments) > 0 ? 1 : 0,
                        'received_at' => date('Y-m-d H:i:s'),
                        'folder' => 'drafts',
                        'staff_id' => $this->staff_id,
                    );
                    $this->db->insert('mail_system', $email_data);
                }
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Mail saved successfully!');
                redirect(base_url() ."mail_system2/drafts/");
                exit();
            }else{
                $attach = $_POST['attachments'];
                if (isset($_POST['mail_id'])) {
                    $this->db->select('attachments');
                    $this->db->from('mail_system');
                    $this->db->where('mail_id', $_POST['mail_id']);
                    $this->db->limit(1);
                    $query = $this->db->get();
                    if ($query->num_rows() > 0) {
                        $attachments = $query->row()->attachments;
                        if ($attachments != null) {
                            $attachments = unserialize($attachments);
                            foreach ($attachments as $attachment_row) {
                                $attach[] = $attachment_row['url'];
                            }
                        }
                    }
                }
                if ($this->send_mail($_POST['to'], $_POST['subject'], $_POST['body'], $attach)) {
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', 'Mail sent successfully!');
                    redirect(base_url() . "mail_system2/sent/");
                    exit();
                } else {
                    $this->session->set_flashdata('success', false);
                    $this->session->set_flashdata('message', 'Wrong please try again.');
                    redirect(base_url() . "mail_system2/outbox/");
                    exit();
                }
            }
        } else {
            $settings = $this->load_settings();
            if ($action != "compose") {
                $mail_id = $_POST['mail_id'];
                $mail_detail = $this->fetch_mail_detail($mail_id);
                set_page('mail_system2/mailbox', array('settings' => $settings,'folder'=>$mail_detail['folder'],'action' => $action,'mail_detail' => $mail_detail));
            } else {
                set_page('mail_system2/mailbox', array('settings' => $settings,'folder'=>'inbox','action' => $action));
            }
        }
    }

    function create_folder()
    {
        if (isset($_POST['folder']) && $_POST['folder'] != '') {
            $folder_data = array('folder'=>$_POST['folder'],'staff_id'=>$this->staff_id);
            $this->db->insert('mail_system_folder',$folder_data);
            $sidebar_mail_label = $this->load->view('mail_system2/sidebar_mail_label', array('current_folder' => $_POST['current_folder']), true);
            $move_to_folder = $this->load->view('mail_system2/move_to_folder_view', array('current_folder' => $_POST['current_folder']), true);
            echo json_encode(array('success' => true, 'message' => 'Folder created successfully.', 'sidebar_mail_label' => $sidebar_mail_label, 'move_to_folder' => $move_to_folder));
        } else {
            echo json_encode(array('success' => false, 'message' => 'Something wrong, Please try again.'));
        }
    }

    function mail_move_to()
    {
        if (isset($_POST['mail_ids']) && count($_POST['mail_ids']) > 0) {
            $folder = trim($_POST['folder']);
            $from_folder = trim($_POST['from_folder']);
            if($from_folder == 'trash' && $folder == 'trash'){
                foreach ($_POST['mail_ids'] as $mail_id) {
                    $this->db->where('mail_id', $mail_id);
                    $this->db->delete('mail_system');
                }
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Mail successfully deleted!");
                redirect($this->agent->referrer());
            }else{
                foreach ($_POST['mail_ids'] as $mail_id) {
                    $this->db->where('mail_id', $mail_id);
                    $this->db->update('mail_system', array('folder' => $folder));
                }
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Mail successfully moved to $folder !");
                redirect($this->agent->referrer());
            }
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Please select at least one mail.');
            redirect($this->agent->referrer());
        }
    }

    function get_staff_mail_folder($staff_id){
        $this->db->select('folder');
        $this->db->from('mail_system_folder');
        $this->db->where('staff_id',$staff_id);
        $query  = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return array();
        }
    }

    function fetch_mail_detail($mail_id)
    {
        $mail_data = array();
        $this->db->select('*');
        $this->db->from('mail_system');
        $this->db->where('mail_id', $mail_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $mail_data['mail_id'] = $query->row()->mail_id;
            if ($query->row()->from_name != $this->email_address) {
                $mail_data['username'] = $query->row()->from_name;
                $mail_data['email'] = $query->row()->from_address;
            } else {
                $mail_data['username'] = $query->row()->to_name;
                $mail_data['email'] = $query->row()->to_address;
            }
            $mail_data['subject'] = $query->row()->subject;
            $mail_data['body'] = $query->row()->body;
            if ($query->row()->attachments != null) {
                $attachments = unserialize($query->row()->attachments);
                $mail_data['attachments'] = $attachments;
            } else {
                $mail_data['attachments'] = null;
            }
            $mail_data['received_at'] = $this->timeAgo($query->row()->received_at);
            $mail_data['cc'] = $query->row()->cc;
            $mail_data['bcc'] = $query->row()->bcc;
            $mail_data['folder'] = $query->row()->folder;
            if($query->row()->is_unread == 1){
                $this->db->where('mail_id', $mail_id);
                $this->db->update('mail_system',array('is_unread'=>0));
            }
        }
        return $mail_data;
    }

    function send_mail($to, $subject, $message, $attach = null)
    {
        $folder = 'sent';
        if ($this->check_internet_conn()) {
            /*$this->load->library('email');*/
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => "$this->email_address",
                'smtp_pass' => "$this->email_password",
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('JayKhodiyar', "$this->email_address");
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            if ($attach != null && $attach != '') {
                if (is_array($attach) && count($attach) > 0) {
                    foreach ($attach as $attach_row) {
                        if ($attach_row != '') {
                            $this->email->attach($attach_row);
                        }
                    }
                }
            }
            if ($this->email->send()) {
                $folder = 'sent';
            } else {
                /*echo "<pre>";print_r($attachments);die;*/
                $folder = 'outbox';
            }
        } else {
            $folder = 'outbox';
        }
        $attachments = array();
        if (is_array($attach) && count($attach) > 0) {
            foreach ($attach as $attach_row) {
                if ($attach_row != '') {
                    $attachments[] = array(
                        'filename' => basename($attach_row, PATHINFO_FILENAME),
                        'url' => $attach_row,
                        'subtype' => pathinfo($attach_row, PATHINFO_EXTENSION),
                    );
                }
            }
        }
        $email_data = array(
            'from_address' => $this->email_address,
            'from_name' => $this->email_address,
            'to_address' => $to,
            'to_name' => $to,
            'subject' => $subject,
            'body' => $message,
            'attachments' => count($attachments) > 0 ? serialize($attachments) : null,
            'is_attachment' => count($attachments) > 0 ? 1 : 0,
            'received_at' => date('Y-m-d H:i:s'),
            'folder' => $folder,
            'staff_id' => $this->staff_id,
        );
        $this->db->insert('mail_system', $email_data);
        return $this->db->insert_id() > 0 ? true : false;
    }

    function limit_words($string, $word_limit = 30)
    {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit));
    }

    function limit_character($string, $character_limit = 30)
    {
        if (strlen($string) > $character_limit) {
            return substr($string, 0, $character_limit) . '...';
        } else {
            return $string;
        }
    }

    function mails_datatable()
    {
        $config['table'] = 'mail_system i';
        $config['column_order'] = array('i.mail_id', 'i.from_name', 'i.to_name', 'i.subject', 'i.is_starred', 'i.received_at', 'i.is_attachment', 'i.is_unread');
        $config['column_search'] = array('i.from_name', 'i.to_name', 'i.subject', 'i.body');
        $config['order'] = array('i.mail_id' => 'desc');
        $config['wheres'] = array();
        $folder_name = isset($_POST['folder'])?$_POST['folder']:'inbox';
        $config['wheres'][] = array('column_name' => 'folder', 'column_value' => $folder_name);
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        foreach ($list as $mail_row) {
            $mail['is_unread'] = $mail_row->is_unread;
            $mail['attachment_status'] = $mail_row->is_attachment;
            $mail['received_at'] = $this->timeAgo($mail_row->received_at);
            $mail['is_starred'] = $mail_row->is_starred;
            $mail['subject'] = $mail_row->subject;
            if ($mail_row->from_name != $this->email_address) {
                $mail['username'] = $mail_row->from_name;
            } else {
                $mail['username'] = $mail_row->to_name;
            }
            $mail['mail_id'] = $mail_row->mail_id;
            $mail['folder'] = $mail_row->folder;
            $row = array();
            $row[] = '<input type="checkbox" class="chk-select-mail" name="mail_ids[]" value="'.$mail_row->mail_id.'">';
            $row[] = $this->load->view('shared/row_mail_datatable', array('mail' => $mail), true);
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        } //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        } //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        } //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        } //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        } //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        } //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    function upload_attachment()
    {
        $this->load->library('upload');
        $config['upload_path'] = 'resource/uploads/attachments';
        $config['allowed_types'] = 'gif|jpg|png|txt|pdf|zip';
        $config['overwrite'] = TRUE;
        $this->upload->initialize($config);
        if ($this->upload->do_upload('upload_attachment')) {
            $file_data = $this->upload->data();
            $file_url = $file_data['file_name'];
            echo json_encode(array('success' => true, 'message' => 'Attachment successfully uploaded.', 'attachment_url' => 'resource/uploads/attachments/' . $file_url));
            exit();
        } else {
            echo json_encode(array('success' => false, 'message' => 'Wrong with upload operation!', 'error' => $this->upload->display_errors()));
            exit();
        }
    }

    /**
     * @param $mb
     * @param $message_no
     * @return mixed|string
     */
    function get_mail_body($mb, $message_no)
    {
        $body = imap_fetchbody($mb, $message_no, 1.2);
        if (!strlen($body) > 0) {
            $body = imap_fetchbody($mb, $message_no, 1);
        }
        $body = preg_replace('/^\>/m', '', $body);
        return $body;
    }

    /**
     * @param $SearchResults
     * @param $SortResults
     * @return array
     */
    function order_search($SearchResults, $SortResults)
    {
        return array_values(array_intersect($SearchResults, $SortResults));
    }

    /**
     * @param $mb
     * @param $message_no
     * @return array
     */
    function get_mail_attachments($mb, $message_no)
    {
        $attachments = array();
        $structure = imap_fetchstructure($mb, $message_no);
        if (isset($structure->parts) && count($structure->parts)) {
            for ($i = 0; $i < count($structure->parts); $i++) {
                $attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
                $attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                        }
                    }
                }
                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }
                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($mb, $message_no, $i + 1);
                    if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                    } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }
        }
        $response_data = array();
        if (count($attachments) != 0) {
            foreach ($attachments as $at) {
                if ($at['is_attachment'] == 1) {
                    $response_data[] = array(
                        'filename' => $at['name'],
                        'file' => $at['attachment'],
                        'subtype' => $at['subtype'],
                    );
                }
            }
        }
        return $response_data;
    }

    function save_unread_mails()
    {
        if ($this->check_internet_conn()) {
            $mailbox = $this->email_server . "INBOX";
            $mb = imap_open($mailbox, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!3");
            $mail_status = imap_status($mb, $this->email_server . "INBOX", SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!4");
            if ($mail_status->messages > 0) {
                $sort_results = imap_sort($mb, SORTDATE, 1);
                $search_results = imap_search($mb, 'UNSEEN');
                if (is_array($search_results)) {
                    $message_nos = $this->order_search($search_results, $sort_results);
                    foreach ($message_nos as $key => $message_no) {
                        $email_headers = imap_headerinfo($mb, $message_no);
                        $mail_data = array();
                        if (isset($email_headers->from[0]->mailbox) && $email_headers->from[0]->mailbox != '') {
                            $mail_data['from_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
                            if (isset($email_headers->from[0]->personal)) {
                                $mail_data['from_name'] = $email_headers->from[0]->personal;
                            } else {
                                $mail_data['from_name'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
                            }
                        }

                        if (isset($email_headers->to[0]->mailbox) && $email_headers->to[0]->mailbox != '') {
                            $mail_data['to_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
                            if (isset($email_headers->to[0]->personal)) {
                                $mail_data['to_name'] = $email_headers->to[0]->personal;
                            } else {
                                $mail_data['to_name'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
                            }
                        }

                        if (isset($email_headers->reply_to[0]->mailbox) && $email_headers->reply_to[0]->mailbox != '') {
                            $mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
                            if (isset($email_headers->reply_to[0]->personal)) {
                                $mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
                            } else {
                                $mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
                            }
                        }

                        if (isset($email_headers->sender[0]->mailbox) && $email_headers->sender[0]->mailbox != '') {
                            $mail_data['sender_address'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
                            if (isset($email_headers->sender[0]->personal)) {
                                $mail_data['sender_name'] = $email_headers->sender[0]->personal;
                            } else {
                                $mail_data['sender_name'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
                            }
                        }

                        $attachments = $this->get_mail_attachments($mb, $message_no);
                        if (count($attachments) > 0) {
                            $mail_data['is_attachment'] = 1;
                            $attachment_url = array();
                            $time = time();
                            mkdir('resource/uploads/attachments/' . $time);
                            foreach ($attachments as $attachment) {
                                $file = "resource/uploads/attachments/$time/" . $attachment['filename'];
                                file_put_contents($file, base64_decode($attachment['file']));
                                $attachment_url[] = array(
                                    'filename' => $attachment['filename'],
                                    'url' => $file,
                                    'subtype' => $attachment['subtype'],
                                );
                            }
                            $mail_data['attachments'] = serialize($attachment_url);
                        }

                        $mail_data['cc'] = array();
                        if(isset($email_headers->cc) && count($email_headers->cc) > 0){
                            foreach($email_headers->cc as $cc_row){
                                $mail_data['cc'][] = $cc_row->mailbox.'@'.$cc_row->host;
                            }
                        }
                        if(isset($email_headers->to) && count($email_headers->to) > 1){
                            foreach($email_headers->to as $cc_row){
                                if($this->email_address != $cc_row->mailbox.'@'.$cc_row->host){
                                    $mail_data['cc'][] = $cc_row->mailbox.'@'.$cc_row->host;
                                }
                            }
                        }

                        $mail_data['cc'] = serialize($mail_data['cc']);
                        $mail_data['bcc'] = '';
                        $mail_data['subject'] = $email_headers->subject;
                        $mail_data['body'] = $this->get_mail_body($mb, $message_no);
                        $mail_data['received_at'] = $email_headers->date;
                        $mail_data['is_unread'] = $email_headers->Unseen == 'U' ? 1 : 0;
                        $mail_data['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
                        $mail_data['folder'] = 'inbox';
                        $mail_data['staff_id'] = trim($this->session->userdata('is_logged_in')['staff_id']);
                        $mail_data['created_at'] = date('Y-m-d H:i:s');
                        $this->db->insert('mail_system',$mail_data);
                    }
                }
            }
            imap_close($mb);
            echo json_encode(array('success' => true, 'message' => 'Mailbox updated successfully!'));
        } else {
            echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
        }
        exit();
    }


    public function save_unread_mails_for_all()
    {
		//$this->db->select('*');
		//$this->db->from('staff');
		//$this->db->where('mail_id', $_POST['mail_id']);
		//$this->db->limit(1);
		$count =0;
		$usercount_mail=0;
		//$prev_staffid=0;
		//$current_staff_id=0;
		$staffs = $this->db->query("SELECT * FROM staff WHERE active != '0' ");
		//print_r($staffs);
		if ($staffs->num_rows() > 0) {
			foreach ($staffs->result() as $staff) {

				//echo "<pre>";
				//print_r($staff);
				//echo "</pre>";
				$massages[$count]['staff_id'] = $staff->staff_id;
				$massages[$count]['mail_count']=0;
				$usercount_mail=0;
				if ($this->check_internet_conn()) {
					$mailbox = $this->email_server . "INBOX";
					$mb = imap_open($mailbox, $staff->mailbox_email, $staff->mailbox_password) or die(imap_last_error() . "<br>Connection Faliure!3");
					$mail_status = imap_status($mb, $this->email_server . "INBOX", SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!4");
					if ($mail_status->messages > 0) {
						$sort_results = imap_sort($mb, SORTDATE, 1);
						$search_results = imap_search($mb, 'UNSEEN');
						if (is_array($search_results)) {
							$message_nos = $this->order_search($search_results, $sort_results);
							foreach ($message_nos as $key => $message_no) {
								$usercount_mail++;
								$email_headers = imap_headerinfo($mb, $message_no);
								$mail_data = array();
								$mail_data['mail_no'] = $email_headers->Msgno;
								$mail_data['uid'] = imap_uid($mb, $message_no);
								if (isset($email_headers->from[0]->mailbox) && $email_headers->from[0]->mailbox != '') {
									$mail_data['from_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
									if (isset($email_headers->from[0]->personal)) {
										$mail_data['from_name'] = $email_headers->from[0]->personal;
									} else {
										$mail_data['from_name'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
									}
								}

								if (isset($email_headers->to[0]->mailbox) && $email_headers->to[0]->mailbox != '') {
									$mail_data['to_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
									if (isset($email_headers->to[0]->personal)) {
										$mail_data['to_name'] = $email_headers->to[0]->personal;
									} else {
										$mail_data['to_name'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
									}
								}

								if (isset($email_headers->reply_to[0]->mailbox) && $email_headers->reply_to[0]->mailbox != '') {
									$mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
									if (isset($email_headers->reply_to[0]->personal)) {
										$mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
									} else {
										$mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
									}
								}

								if (isset($email_headers->sender[0]->mailbox) && $email_headers->sender[0]->mailbox != '') {
									$mail_data['sender_address'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
									if (isset($email_headers->sender[0]->personal)) {
										$mail_data['sender_name'] = $email_headers->sender[0]->personal;
									} else {
										$mail_data['sender_name'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
									}
								}

								$mail_data['cc'] = '';
								$mail_data['bcc'] = '';
								$mail_data['subject'] = $email_headers->subject;
								$mail_data['body'] = $this->get_mail_body($mb, $message_no);
								$attachments = $this->get_mail_attachments($mb, $message_no);
								if (count($attachments) > 0) {
									$mail_data['is_attachment'] = 1;
									$attachment_url = array();
									$time = time();
									mkdir('resource/uploads/attachments/' . $time);
									foreach ($attachments as $attachment) {
										$file = "resource/uploads/attachments/$time/" . $attachment['filename'];
										file_put_contents($file, base64_decode($attachment['file']));
										$attachment_url[] = array(
											'filename' => $attachment['filename'],
											'url' => $file,
											'subtype' => $attachment['subtype'],
										);
									}
									$mail_data['attachments'] = serialize($attachment_url);
								}
								$mail_data['received_at'] = $email_headers->date;
								$mail_data['folder_label'] = 'Inbox';
								$mail_data['staff_id'] = $staff->staff_id;
								$mail_data['folder_name'] = $mailbox;
								$mail_data['is_unread'] = $email_headers->Unseen == 'U' ? 1 : 0;
								$mail_data['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
								$mail_data['created_at'] = date('Y-m-d H:i:s');
								$this->db->insert('mail_system', $mail_data);
							}
							$massages[$count]['mail_count']=$usercount_mail;
							$client = new Client(new Version1X(SERVER_REQUEST_SCHEME.'://'.HTTP_HOST.':'.PORT_NUMBER));
							$client->initialize();
							$client->emit('cron_mail_notification', ['staff_id'=> $staff->staff_id,'count_mail' => $usercount_mail]);
							$client->close();

						}
					}
					imap_close($mb);
					//echo json_encode(array('success' => true, 'message' => 'Mailbox updated successfully!'));
					$massages[$count]['success']=true;
					$massages[$count]['message']="Mailbox updated successfully!";
				} else {
					$massages[$count]['success']=false;
					$massages[$count]['message']="No internet connection available";
					//echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
				}
				$count++;
			}
		}
		echo json_encode($massages);
		exit();
    }

    /**
     * @return bool
     */
    function sent_drafts_mails()
    {
        $this->db->select('*');
        $this->db->from('mail_system');
        $this->db->where('folder_name', $this->email_server . '[Gmail]/Drafts');
        $this->db->where('is_sent', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $mail_row) {
                $attach = array();
                if ($mail_row->attachments != null) {
                    $attachments = unserialize($mail_row->attachments);
                    foreach ($attachments as $attachment_row) {
                        $attach[] = $attachment_row['url'];
                    }
                }
                if ($this->send_mail($mail_row->to_address, $mail_row->subject, $mail_row->body, $attach)) {
                    $update_data = array('is_sent' => 1, 'mail_no' => 1 + $this->get_max_mail_no_in_folder('Sent mail'), 'folder_label' => 'Sent mail', 'folder_name' => $this->email_server . $this->getServerFolderName('Sent Mail'));
                    $this->db->where('mail_id', $mail_row->mail_id);
                    $this->db->update('mail_system', $update_data);
                }
            }
        }
        return true;
    }

    function update_mailbox()
    {
        if ($this->check_internet_conn()) {
            $this->save_unread_mails();
            $this->sent_drafts_mails();
        } else {
            echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
        }
        exit();
    }

    function settings()
    {
        $this->db->select('setting_key,setting_value');
        $this->db->from('mailbox_settings');
        $query = $this->db->get();
        $setting_data = array();
        foreach ($query->result() as $setting) {
            $setting_data[$setting->setting_key] = $setting->setting_value;
        }
        $settings_html = $this->load->view('mail_system2/settings', $setting_data, true);
        echo json_encode(array('success' => true, 'message' => "Successfully load settings!", 'settings_html' => $settings_html));
        exit();
    }

    function save_settings()
    {
        $setting_data = $_POST;
        foreach ($setting_data as $setting_key => $setting_value) {
            $this->db->where('setting_key', $setting_key);
            $this->db->update('mailbox_settings', array('setting_value' => $setting_value));
        }
        echo json_encode(array('success' => true, 'message' => 'Settings save successfully!'));
        exit();
    }

    /**
     * @return array
     */
    function load_settings()
    {
        $this->db->select('setting_key,setting_value');
        $this->db->from('mailbox_settings');
        $query = $this->db->get();
        $setting_data = array();
        foreach ($query->result() as $setting) {
            $setting_data[$setting->setting_key] = $setting->setting_value;
        }
        return $setting_data;
    }
}
