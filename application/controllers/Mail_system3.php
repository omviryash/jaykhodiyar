<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
/**
 * Class Mailbox_system3
 * &@property Datatables $datatable
 * &@property Crud crud
 */
class Mail_system3 extends CI_Controller
{
	private $email_address = SMTP_EMAIL_ADDRESS;
	private $email_password = SMTP_EMAIL_PASSWORD;
	private $email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';
	private $staff_id = 1;

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->email_address = trim($this->session->userdata('is_logged_in')['mailbox_email']);
		$this->email_password = trim($this->session->userdata('is_logged_in')['mailbox_password']);
		$this->staff_id = trim($this->session->userdata('is_logged_in')['staff_id']);
		$this->load->library('user_agent');
		$this->load->model("Appmodel", "app_model");
	}

	function check_internet_conn()
	{
		return checkdnsrr('php.net') ? true : false;
	}

	function inbox()
	{
		if($this->app_model->have_access_role(MAIL_SYSTEM_INBOX_MENU_ID, "view")){
			set_page('mail_system3/mailbox', array('current_folder' => 'inbox'));
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function drafts()
	{
		set_page('mail_system3/mailbox', array('current_folder' => 'drafts'));
	}

	function outbox()
	{
		if($this->app_model->have_access_role(MAIL_SYSTEM_SEND_OUTBOX_MAILS_MENU_ID, "view")){
			set_page('mail_system3/mailbox', array('current_folder' => 'outbox'));
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function sent()
	{
		set_page('mail_system3/mailbox', array('current_folder' => 'sent'));
	}

	function trash()
	{
		set_page('mail_system3/mailbox', array('current_folder' => 'trash'));
	}

	function junk()
	{
		set_page('mail_system3/mailbox', array('current_folder' => 'junk'));
	}

	function starred()
	{
		set_page('mail_system3/mailbox', array('current_folder' => 'starred'));
	}

	function folder_mails($folder)
	{
		set_page('mail_system3/mailbox', array('current_folder' => trim(dash_to_space($folder))));
	}

	function read_mail($mail_id)
	{
		if ($this->input->is_ajax_request()) {
			$mail_detail = $this->fetch_mail_detail($mail_id);
			$mail_view = $this->load->view('mail_system3/read_mail', array('mail_detail' => $mail_detail, 'current_folder' => $mail_detail['folder']), true);
			echo json_encode(array('mail_system_detail_area' => $mail_view));
			exit();
		} else {
			$mail_detail = $this->fetch_mail_detail($mail_id);
			set_page('mail_system3/mailbox', array('mail_detail' => $mail_detail, 'current_folder' => $mail_detail['folder'], 'mail_id' => $mail_id));
		}
	}

	function mail_body_iframe($mail_id)
	{
		$this->db->select('body');
		$this->db->from('mail_system');
		$this->db->where('mail_id', $mail_id);
		$this->db->limit(1);
		$query = $this->db->get();
		echo "<style>html{height: 100%;overflow-y: hidden;}</style>";
		echo $query->row()->body;
		echo '<script type="text/javascript">';
		echo "window.onload = function(){ var iframe = window.parent.document.getElementById('mail_body_iframe');";
		echo 'var container = document.getElementsByTagName("body")[0];';
		echo 'var iframe_height = container.offsetHeight + 50;';
		echo "iframe.style.height = iframe_height + 'px'; }";
		echo '</script>';
	}

	function compose_mail($action = "compose")
	{
		//echo '<pre>';print_r($_REQUEST);exit;
		if(!$this->app_model->have_access_role(MAIL_SYSTEM_COMPOSE_MENU_ID, "view")){
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
		if (isset($_POST['btn_draft']) || isset($_POST['btn_send']) || isset($_POST['btn_save_draft'])) {
			//echo '<pre>';print_r($_POST);exit;
			/*------- If Save To Drafts --------*/
			if (isset($_POST['btn_draft']) || isset($_POST['btn_save_draft'])) {
				if (isset($_POST['mail_id'])) {
					$this->db->select('*');
					$this->db->from('mail_system');
					$this->db->where('mail_id', $_POST['mail_id']);
					$this->db->limit(1);
					$query = $this->db->get();
					$mail_data = $query->row_array(0);
					$mail_data['from_address'] = $this->email_address;
					$mail_data['from_name'] = $this->email_address;
					$mail_data['to_address'] = $_POST['to'];
					$mail_data['to_name'] = $_POST['to'];
					$mail_data['subject'] = $_POST['subject'];
					$mail_data['body'] = $_POST['body'];
					$mail_data['folder'] = 'drafts';
					$mail_data['created_at'] = date('Y-m-d H:i:s');
					$mail_data['received_at'] = date('Y-m-d H:i:s');
					$mail_data['attachments'] = $mail_data['attachments'] != null ? unserialize($mail_data['attachments']) : array();
					$mail_data['cc'] = array();
					$mail_data['bcc'] = array();

					if (isset($_POST['attachments']) && count($_POST['attachments']) > 0) {
						$mail_data['attachments'] = array();
						foreach ($_POST['attachments'] as $attachment_row) {
							if ($attachment_row != '') {
								$mail_data['attachments'][] = array(
									'filename' => basename($attachment_row, PATHINFO_FILENAME),
									'url' => $attachment_row,
									'subtype' => pathinfo($attachment_row, PATHINFO_EXTENSION),
								);
							}
						}
					}

					$mail_data['is_attachment'] = count($mail_data['attachments']) > 0 ? 1 : 0;
					$mail_data['attachments'] = count($mail_data['attachments']) > 0 ? serialize($mail_data['attachments']) : null;
					if ($_POST['cc'] != '') {
						$_POST['cc'] = explode(',', $_POST['cc']);
						if (count($_POST['cc']) > 0) {
							foreach ($_POST['cc'] as $cc_row) {
								if ($cc_row != '') {
									if (trim($cc_row) != '') {
										$mail_data['cc'][] = trim($cc_row);
									}
								}
							}
						}
					}
					$mail_data['cc'] = count($mail_data['cc']) > 0 ? serialize($mail_data['cc']) : null;

					if ($_POST['bcc'] != '') {
						$_POST['bcc'] = explode(',', $_POST['bcc']);
						if (count($_POST['bcc']) > 0) {
							foreach ($_POST['bcc'] as $bcc_row) {
								if ($bcc_row != '') {
									if (trim($bcc_row) != '') {
										$mail_data['bcc'][] = trim($bcc_row);
									}
								}
							}
						}
					}
					$mail_data['bcc'] = count($mail_data['bcc']) > 0 ? serialize($mail_data['bcc']) : null;

					if (isset($_POST['request_read_receipt'])) {
						$mail_data['is_read_receipt_req'] = 1;
					}
					if (isset($_POST['btn_save_draft'])) {
						$this->db->where('mail_id', $mail_data['mail_id']);
						$this->db->update('mail_system', $mail_data);
					} else {
						unset($mail_data['mail_id']);
						$this->db->insert('mail_system', $mail_data);
					}
				} else {
					$attachments = array();
					if (isset($_POST['attachments']) && count($_POST['attachments']) > 0) {
						foreach ($_POST['attachments'] as $attachment_row) {
							if ($attachment_row != '') {
								$attachments[] = array(
									'filename' => basename($attachment_row, PATHINFO_FILENAME),
									'url' => $attachment_row,
									'subtype' => pathinfo($attachment_row, PATHINFO_EXTENSION),
								);
							}
						}
					}


					if ($_POST['cc'] != '') {
						$_POST['cc'] = explode(',', $_POST['cc']);
						$cc_data = array();
						foreach ($_POST['cc'] as $cc_row) {
							if ($cc_row != '') {
								$cc_data[] = trim($cc_row);
							}
						}
						$_POST['cc'] = $cc_data;
					}
					if ($_POST['bcc'] != '') {
						$_POST['bcc'] = explode(',', $_POST['bcc']);
						$bcc_data = array();
						foreach ($_POST['bcc'] as $bcc_row) {
							if ($bcc_row != '') {
								$bcc_data[] = trim($bcc_row);
							}
						}
						$_POST['bcc'] = $bcc_data;
					}
					$mail_data = array(
						'from_address' => $this->email_address,
						'from_name' => $this->email_address,
						'to_address' => $_POST['to'],
						'to_name' => $_POST['to'],
						'subject' => $_POST['subject'],
						'body' => $_POST['body'],
						'attachments' => count($attachments) > 0 ? serialize($attachments) : null,
						'cc' => count($_POST['cc']) > 0 ? serialize($_POST['cc']) : null,
						'bcc' => count($_POST['bcc']) > 0 ? serialize($_POST['bcc']) : null,
						'is_attachment' => count($attachments) > 0 ? 1 : 0,
						'received_at' => date('Y-m-d H:i:s'),
						'folder' => 'drafts',
						'staff_id' => $this->staff_id,
						'created_at' => date('Y-m-d H:i:s'),
					);
					if (isset($_POST['request_read_receipt'])) {
						$mail_data['is_read_receipt_req'] = 1;
					}
					$this->db->insert('mail_system', $mail_data);
				}
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Mail saved successfully!');
				redirect(base_url() . "mail-system3/drafts/");
				exit();
			} else {
				$attach = $_POST['attachments'];
				/*if (isset($_POST['mail_id'])) {
                    $this->db->select('attachments');
                    $this->db->from('mail_system');
                    $this->db->where('mail_id', $_POST['mail_id']);
                    $this->db->limit(1);
                    $query = $this->db->get();
                    if ($query->num_rows() > 0) {
                        $attachments = $query->row()->attachments;
                        if ($attachments != null) {
                            $attachments = unserialize($attachments);
                            foreach ($attachments as $attachment_row) {
                                $attach[] = $attachment_row['url'];
                            }
                        }
                    }
                }*/
				if(empty($_POST['to'])){
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Please Enter Email Adderss !');
					redirect(base_url() . "mail-system3/compose_mail/");
					exit();
				}
				if ($this->send_mail($_POST['to'], $_POST['subject'], $_POST['body'], $attach, $_POST['cc'], $_POST['bcc'])) {
					$this->session->set_flashdata('success', true);
					$this->session->set_flashdata('message', 'Mail sent successfully!');
					redirect(base_url() . "mail-system3/sent/");
					exit();
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Mail save in outbox!');
					redirect(base_url() . "mail-system3/outbox/");
					exit();
				}
			}
		} else {
			//echo '<pre>';print_r($_POST);exit;
			$settings = $this->load_settings();
			if ($action != "compose") {
				$mail_id = $_POST['mail_id'];
				$mail_detail = $this->fetch_mail_detail($mail_id);
				set_page('mail_system3/mailbox', array('settings' => $settings, 'current_folder' => $mail_detail['folder'], 'action' => $action, 'mail_detail' => $mail_detail));
			} else {
				set_page('mail_system3/mailbox', array('settings' => $settings, 'current_folder' => 'inbox', 'action' => 'compose'));
			}
		}
	}

	function send_drafts($mail_id)
	{
		$this->db->select('to_address,subject,body,attachments,cc,bcc,is_read_receipt_req');
		$this->db->from('mail_system');
		$this->db->where('mail_id', $mail_id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$to_address = $query->row()->to_address;
			$subject = $query->row()->subject;
			$body = $query->row()->body;
			$cc = $query->row()->cc != null ? unserialize($query->row()->cc) : null;
			$bcc = $query->row()->bcc != null ? unserialize($query->row()->bcc) : null;
			$is_read_receipt_req = $query->row()->is_read_receipt_req;
			$attach = array();
			$attachments = $query->row()->attachments;
			if ($attachments != null) {
				$attachments = unserialize($attachments);
				foreach ($attachments as $attachment_row) {
					$attach[] = $attachment_row['url'];
				}
			}

			/*----- Delete mail from drafts -------*/
			$this->db->where('mail_id', $mail_id);
			$this->db->delete('mail_system');

			if ($this->send_mail($to_address, $subject, $body, $attach, $cc, $bcc, $is_read_receipt_req)) {
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Mail sent successfully!');
				redirect(base_url() . "mail-system3/sent/");
				exit();
			} else {
				$this->session->set_flashdata('success', false);
				$this->session->set_flashdata('message', 'Mail save in outbox!');
				redirect(base_url() . "mail-system3/outbox/");
				exit();
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Please try again something wrong!');
			redirect(base_url() . "mail-system3/read-mail/" . $mail_id);
			exit();
		}

	}

	function send_mail($to, $subject, $message, $attach = null, $cc = null, $bcc = null, $is_read_receipt_req = 0)
	{
		$folder = 'sent';
		if ($this->check_internet_conn()) {
			$this->load->library('email');
			$this->email->set_smtp_user($this->email_address);
			$this->email->set_smtp_pass($this->email_password);

			if (isset($_POST['request_read_receipt']) || $is_read_receipt_req == 1) {
				$this->email->set_header('Return-Receipt-To', "<$this->email_address>");
				$this->email->set_header('Read-Receipt-To', "<$this->email_address>");
				$this->email->set_header('X-Confirm-Reading-To', "<$this->email_address>");
				$this->email->set_header('Generate-Delivery-Report', "<$this->email_address>");
				$this->email->set_header('Disposition-Notification-To', "<$this->email_address>");
				$is_read_receipt_req = 1;
			}
			$this->email->set_newline("\r\n");
			/*$from_address = $this->config->item('from_address');*/
			$from_address = $this->email_address;
			/*echo "<pre>".$from_address;
            print_r($this->config);
            die();*/
			$this->email->reply_to($from_address, $from_address);
			$this->email->from($from_address, $from_address);

			if ($to != '') {
				$to = explode(',', $to);
				$to_data = array();
				foreach ($to as $to_row) {
					if ($to_row != '') {
						$to_data[] = trim($to_row);
					}
				}
				$to = $to_data;
			}

			$this->email->to($to);

			if ($cc != '') {
				$cc = explode(',', $cc);
				$cc_data = array();
				foreach ($cc as $cc_row) {
					if ($cc_row != '') {
						$cc_data[] = trim($cc_row);
					}
				}
				$cc = $cc_data;
			}
			if ($bcc != '') {
				$bcc = explode(',', $_POST['bcc']);
				$bcc_data = array();
				foreach ($bcc as $bcc_row) {
					if ($bcc_row != '') {
						$bcc_data[] = trim($bcc_row);
					}
				}
				$bcc = $bcc_data;
			}

			if ($cc != null && is_array($cc) && count($cc) > 0) {
				$this->email->cc($cc);
			}
			if ($bcc != null && is_array($bcc) && count($bcc) > 0) {
				$bcc[] = BCC_EMAIL_ADDRESS;
				$this->email->bcc($bcc);
				//print_r($bcc);die;
			} else {
				$this->email->bcc(BCC_EMAIL_ADDRESS);
			}
			$this->email->subject($subject);
			$this->email->message($message);
			if ($attach != null && $attach != '') {
				if (is_array($attach) && count($attach) > 0) {
					foreach ($attach as $attach_row) {
						if ($attach_row != '') {
							$this->email->attach($attach_row);
						}
					}
				}
			}
			if ($this->email->send()) {
				$folder = 'sent';
			} else {
				$folder = 'outbox';
			}
		} else {
			$folder = 'outbox';
		}
		$attachments = array();
		if (is_array($attach) && count($attach) > 0) {
			foreach ($attach as $attach_row) {
				if ($attach_row != '') {
					$attachments[] = array(
						'filename' => basename($attach_row, PATHINFO_FILENAME),
						'url' => $attach_row,
						'subtype' => pathinfo($attach_row, PATHINFO_EXTENSION),
					);
				}
			}
		}
		$email_data = array(
			'from_address' => $this->email_address,
			'from_name' => $this->email_address,
			'to_address' => implode(',', $to),
			'to_name' => implode(',', $to),
			'subject' => $subject,
			'body' => $message,
			'attachments' => count($attachments) > 0 ? serialize($attachments) : null,
			'is_attachment' => count($attachments) > 0 ? 1 : 0,
			'received_at' => date('Y-m-d H:i:s'),
			'folder' => $folder,
			'staff_id' => $this->staff_id,
			'is_read_receipt_req' => $is_read_receipt_req,
			'cc' => $cc,
			'bcc' => $bcc,
			'created_at' => date('Y-m-d H:i:s'),
		);
		if ($cc != null && is_array($cc) && count($cc) > 0) {
			$email_data['cc'] = serialize($cc);
		} else {
			$email_data['cc'] = null;
		}
		if ($bcc != null && is_array($bcc) && count($bcc) > 0) {
			array_pop($bcc);
			$email_data['bcc'] = serialize($bcc);
			//echo "<pre>";print_r($email_data['bcc']);die();
		} else {
			$email_data['bcc'] = null;
		}

		/*  echo "<pre>";
          print_r($email_data);
          die();*/

		if ($folder == 'sent' && isset($_POST['mail_id'])) {
			if (strtolower($this->get_mail_folder($_POST['mail_id'])) == 'drafts') {
				$this->db->where('mail_id', $_POST['mail_id']);
				$this->db->delete('mail_system');
			}
		}

		$this->db->insert('mail_system', $email_data);
		return $folder == 'sent' ? true : false;
	}

	function set_starred_flag($mail_id)
	{
		$this->db->where('mail_id', $mail_id);
		$this->db->update('mail_system', array('is_starred' => 1));
		echo true;
		exit();
	}

	function remove_starred_flag($mail_id)
	{
		$this->db->where('mail_id', $mail_id);
		$this->db->update('mail_system', array('is_starred' => 0));
		echo true;
		exit();
	}

	function print_mail($mail_id)
	{
		$mail_detail = $this->fetch_mail_detail($mail_id);
		/* echo "<pre>";
         print_r($mail_detail);
         exit();*/
		$mail_html = $this->load->view('mail_system3/print_mail', $mail_detail, true);
		$this->load->library('m_pdf');
		$pdf = new mPDF();
		$pdf->WriteHTML($mail_html);
		$pdf->Output('email.pdf', 'I');
	}

	function fetch_mail_detail($mail_id)
	{
		$mail_data = array();
		$this->db->select('*');
		$this->db->from('mail_system');
		$this->db->where('mail_id', $mail_id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$mail_data['mail_id'] = $query->row()->mail_id;
			if ($query->row()->from_name != $this->email_address) {
				$mail_data['username'] = $query->row()->from_name;
				$mail_data['email'] = $query->row()->from_address;
			} else {
				$mail_data['username'] = $query->row()->to_name;
				$mail_data['email'] = $query->row()->to_address;
			}

			$mail_data['reply_to_username'] = $query->row()->from_name;
			$mail_data['reply_to'] = $query->row()->from_address;

			$mail_data['subject'] = $query->row()->subject;
			$mail_data['body'] = $query->row()->body;
			if ($query->row()->attachments != null) {
				$attachments = unserialize($query->row()->attachments);
				$mail_data['attachments'] = $attachments;
			} else {
				$mail_data['attachments'] = null;
			}
			$mail_data['received_at'] = $this->timeAgo($query->row()->received_at);
			$mail_data['cc'] = $query->row()->cc != null ? unserialize($query->row()->cc) : array();
			$mail_data['bcc'] = $query->row()->bcc != null ? unserialize($query->row()->bcc) : array();
			$mail_data['folder'] = $query->row()->folder;
			if ($query->row()->is_unread == 1) {
				$this->db->where('mail_id', $mail_id);
				$this->db->update('mail_system', array('is_unread' => 0));
			}
			$mail_data['next_mail_id'] = $this->get_next_mail_id($mail_id, $query->row()->folder);
			$mail_data['previous_mail_id'] = $this->get_previous_mail_id($mail_id, $query->row()->folder);
		}
		/*echo "<pre>";
        print_r($mail_data);
        die();*/
		return $mail_data;
	}

	/**
     * @param $mail_id
     * @param $folder
     * @return int
     */
	function get_next_mail_id($mail_id, $folder)
	{
		$this->db->select('mail_id');
		$this->db->from('mail_system');
		$this->db->where('mail_id >', $mail_id);
		$this->db->where('folder', $folder);
		$this->db->order_by('mail_id', 'asc');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows()) {
			return $query->row()->mail_id;
		} else {
			return 0;
		}

	}

	/**
     * @param $mail_id
     * @param $folder
     * @return int
     */
	function get_previous_mail_id($mail_id, $folder)
	{
		$this->db->select('mail_id');
		$this->db->from('mail_system');
		$this->db->where('mail_id <', $mail_id);
		$this->db->where('folder', $folder);
		$this->db->order_by('mail_id', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows()) {
			return $query->row()->mail_id;
		} else {
			return 0;
		}
	}

	function get_count_unread_mails($folder = 'inbox')
	{
		$this->db->select('mail_id');
		$this->db->from('mail_system');
		$this->db->where('folder', $folder);
		$this->db->where('is_unread', 1);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function mails_datatable()
	{
		$config['table'] = 'mail_system i';
		$config['column_order'] = array('i.mail_id', 'i.from_name', 'i.to_name', 'i.subject', 'i.is_starred', 'i.received_at', 'i.is_attachment', 'i.is_unread');
		$config['column_search'] = array('i.from_name', 'i.to_name', 'i.subject', 'i.body');
		$config['order'] = array('i.mail_id' => 'desc');
		$config['wheres'] = array();
		$folder_name = isset($_POST['folder']) ? $_POST['folder'] : 'inbox';

		$config['wheres'][] = array('column_name' => 'staff_id', 'column_value' => $this->staff_id);

		if ($folder_name == 'starred') {
			$config['wheres'][] = array('column_name' => 'is_starred', 'column_value' => 1);
		} elseif ($folder_name != 'all') {
			$config['wheres'][] = array('column_name' => 'folder', 'column_value' => $folder_name);
		}

		$mail_filter = isset($_POST['mail_filter']) ? $_POST['mail_filter'] : 'all';
		if ($mail_filter == 'newest') {
			$config['order'] = array('i.mail_id' => 'desc');
		} elseif ($mail_filter == 'oldest') {
			$config['order'] = array('i.mail_id' => 'asc');
		} elseif ($mail_filter == 'unread_only') {
			$config['wheres'][] = array('column_name' => 'is_unread', 'column_value' => 1);
		} elseif ($mail_filter == 'read_only') {
			$config['wheres'][] = array('column_name' => 'is_unread !=', 'column_value' => 1);
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $mail_row) {
			if ($mail_row->from_name != $this->email_address) {
				$username = $mail_row->from_name;
			} else {
				$username = $mail_row->to_name;
			}
			$row = array();
			$row[] = '<input type="checkbox" class="chk-select-mail" name="mail_ids[]" value="' . $mail_row->mail_id . '">';
			$star_class = $mail_row->is_starred == 1 ? 'fa-star' : 'fa-star-o';
			$row[] = '<a href="javascript:void(0);" class="btn-star mailbox-star" data-mail_id="' . $mail_row->mail_id . '"><i class="fa ' . $star_class . ' text-yellow"></i></a>';
			$attachment_status = $mail_row->is_attachment == 1 ? '<i class="fa fa-paperclip"></i>' : '&nbsp;';
			$detail_row = '';
			if ($mail_row->is_unread == 1) {
				if ($folder_name == 'drafts') {
					$detail_row .= '<div class="is_unread"><a href="javascript:void(0);" data-mail_id="' . $mail_row->mail_id . '" class="btn-read-mail drafts">' . str_replace(',', '<br/>', $username) . '</a>';
					$detail_row .= '<div class="pull-right">' . $attachment_status . '</div></br>';
					$detail_row .= '<a href="javascript:void(0);" data-mail_id="' . $mail_row->mail_id . '" class="btn-read-mail drafts">' . $this->limit_character($mail_row->subject, 25) . '</a> </br>' . $this->timeAgo($mail_row->received_at) . '</div>';
				} else {
					$detail_row .= '<div class="is_unread"><a href="javascript:void(0);" data-mail_id="' . $mail_row->mail_id . '" class="btn-read-mail">' . str_replace(',', '<br/>', $username) . '</a>';
					$detail_row .= '<div class="pull-right">' . $attachment_status . '</div></br>';
					$detail_row .= $this->limit_character($mail_row->subject, 25) . '</br>' . $this->timeAgo($mail_row->received_at) . '</div>';
				}
			} else {
				if ($folder_name == 'drafts') {
					$detail_row .= '<a href="javascript:void(0);" data-mail_id="' . $mail_row->mail_id . '" class="btn-read-mail drafts">' . str_replace(',', '<br/>', $username) . '</a>';
					$detail_row .= '<div class="pull-right">' . $attachment_status . '</div></br>';
					$detail_row .= '<a href="javascript:void(0);" data-mail_id="' . $mail_row->mail_id . '" class="btn-read-mail drafts">' . $this->limit_character($mail_row->subject, 25) . '</a> </br>' . $this->timeAgo($mail_row->received_at);
				} else {
					$detail_row .= '<a href="javascript:void(0);" data-mail_id="' . $mail_row->mail_id . '" class="btn-read-mail">' . str_replace(',', '<br/>', $username) . '</a>';
					$detail_row .= '<div class="pull-right">' . $attachment_status . '</div></br>';
					$detail_row .= $this->limit_character($mail_row->subject, 25) . '</br>' . $this->timeAgo($mail_row->received_at);
				}
			}
			$row[] = $detail_row;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function mails_datatable_v1()
	{
		$config['table'] = 'mail_system i';
		$config['column_order'] = array('i.mail_id', 'i.from_name', 'i.to_name', 'i.subject', 'i.is_starred', 'i.received_at', 'i.is_attachment', 'i.is_unread');
		$config['column_search'] = array('i.from_name', 'i.to_name', 'i.subject', 'i.body');
		$config['order'] = array('i.mail_id' => 'desc');
		$config['wheres'] = array();
		$folder_name = isset($_POST['folder']) ? $_POST['folder'] : 'inbox';
		if ($folder_name == 'starred') {
			$config['wheres'][] = array('column_name' => 'is_starred', 'column_value' => 1);
		} else {
			$config['wheres'][] = array('column_name' => 'folder', 'column_value' => $folder_name);
		}
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $mail_row) {
			if ($mail_row->from_name != $this->email_address) {
				$username = $mail_row->from_name;
			} else {
				$username = $mail_row->to_name;
			}
			$row = array();
			$row[] = '<input type="checkbox" class="chk-select-mail" name="mail_ids[]" value="' . $mail_row->mail_id . '">';
			$star_class = $mail_row->is_starred == 1 ? 'fa-star' : 'fa-star-o';
			$row[] = '<a href="javascript:void(0);" class="btn-star mailbox-star" data-mail_id="' . $mail_row->mail_id . '"><i class="fa ' . $star_class . ' text-yellow"></i></a>';
			$row[] = '<a href="' . base_url() . 'mail-system3/read-mail/' . $mail_row->mail_id . '">' . $username . '</a>';
			$row[] = '<b>' . $this->limit_character($mail_row->subject, 30) . '</b> - ' . $this->limit_character(strip_tags($mail_row->body), 60);
			$row[] = $mail_row->is_attachment == 1 ? '<i class="fa fa-paperclip"></i>' : '&nbsp;';
			$row[] = $this->timeAgo($mail_row->received_at);
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function create_folder()
	{
		if (isset($_POST['folder']) && $_POST['folder'] != '') {
			$folder_data = array('folder' => $_POST['folder'], 'staff_id' => $this->staff_id);
			$this->db->insert('mail_system_folder', $folder_data);
			$sidebar_mail_label = $this->load->view('mail_system3/sidebar_mail_label', array('current_folder' => $_POST['current_folder']), true);
			$move_to_folder = $this->load->view('mail_system3/move_to_folder_view', array('current_folder' => $_POST['current_folder']), true);
			echo json_encode(array('success' => true, 'message' => 'Folder created successfully.', 'sidebar_mail_label' => $sidebar_mail_label, 'move_to_folder' => $move_to_folder));
		} else {
			echo json_encode(array('success' => false, 'message' => 'Something wrong, Please try again.'));
		}
	}

	function delete_mail()
	{
		$mail_id = $_POST['mail_id'];
		$folder = $this->get_mail_folder($mail_id);
		if ($folder == 'trash') {
			$this->db->where('mail_id', $mail_id);
			$this->db->delete('mail_system');
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Mail successfully removed!');
			redirect(base_url() . "mail-system3/trash/");
			exit();
		} else {
			$this->db->where('mail_id', $mail_id);
			$this->db->update('mail_system', array('folder' => 'trash'));
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Mail successfully moved to trash!');
			redirect(base_url() . "mail-system3/folder-mails/" . space_to_dash($folder));
			exit();
		}
	}

	function discard_drafts()
	{
		$mail_id = $_POST['mail_id'];
		$this->db->where('mail_id', $mail_id);
		$this->db->delete('mail_system');
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Drafts successfully removed!');
		redirect(base_url() . "mail-system3/drafts/");
		exit();
	}

	/**
     * @param $mail_id
     * @return string
     */
	function get_mail_folder($mail_id)
	{
		$this->db->select('folder');
		$this->db->from('mail_system');
		$this->db->where('mail_id', $mail_id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->folder;
		} else {
			return 'inbox';
		}
	}

	function mail_move_to()
	{
		if (isset($_POST['mail_ids']) && count($_POST['mail_ids']) > 0) {
			$to_folder = trim($_POST['to_folder']);
			$is_deleted = false;
			$folder = '';
			foreach ($_POST['mail_ids'] as $key => $mail_id) {
				if ($key == 0) {
					$this->db->select('folder');
					$this->db->from('mail_system');
					$this->db->where('mail_id', $mail_id);
					$this->db->limit(1);
					$query = $this->db->get();
					$folder = $query->row()->folder;
				}
				if ($folder == 'trash' && $to_folder == 'trash') {
					$this->db->where('mail_id', $mail_id);
					$this->db->delete('mail_system');
					$is_deleted = true;
				} else {
					$this->db->where('mail_id', $mail_id);
					$this->db->update('mail_system', array('folder' => $to_folder));
				}
			}
			if ($is_deleted == true) {
				echo json_encode(array('success' => true, 'message' => "Mail successfully deleted!"));
			} else {
				echo json_encode(array('success' => true, 'message' => "Mail successfully moved to $to_folder !"));
			}
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => "Please select at least one mail."));
			exit();
		}
	}

	function limit_character($string, $character_limit = 30)
	{
		if (strlen($string) > $character_limit) {
			return substr($string, 0, $character_limit) . '...';
		} else {
			return $string;
		}
	}

	function timeAgo($time_ago)
	{
		$time_ago = strtotime($time_ago);
		$cur_time = time();
		$time_elapsed = $cur_time - $time_ago;
		$seconds = $time_elapsed;
		$minutes = round($time_elapsed / 60);
		$hours = round($time_elapsed / 3600);
		$days = round($time_elapsed / 86400);
		$weeks = round($time_elapsed / 604800);
		$months = round($time_elapsed / 2600640);
		$years = round($time_elapsed / 31207680);
		// Seconds
		if ($seconds <= 60) {
			return "just now";
		} //Minutes
		else if ($minutes <= 60) {
			if ($minutes == 1) {
				return "one minute ago";
			} else {
				return "$minutes minutes ago";
			}
		} //Hours
		else if ($hours <= 24) {
			if ($hours == 1) {
				return "an hour ago";
			} else {
				return "$hours hrs ago";
			}
		} //Days
		else if ($days <= 7) {
			if ($days == 1) {
				return "yesterday";
			} else {
				return "$days days ago";
			}
		} //Weeks
		else if ($weeks <= 4.3) {
			if ($weeks == 1) {
				return "a week ago";
			} else {
				return "$weeks weeks ago";
			}
		} //Months
		else if ($months <= 12) {
			if ($months == 1) {
				return "a month ago";
			} else {
				return "$months months ago";
			}
		} //Years
		else {
			if ($years == 1) {
				return "one year ago";
			} else {
				return "$years years ago";
			}
		}
	}

	function settings()
	{
		if(!$this->app_model->have_access_role(MAIL_SYSTEM_MENU_ID, "view")){
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
		$this->db->select('setting_key,setting_value');
		$this->db->where('staff_id', $this->staff_id);
		$this->db->from('mailbox_settings');
		$query = $this->db->get();
		$settings = array();
		foreach ($query->result() as $setting) {
			$settings[$setting->setting_key] = $setting->setting_value;
		}
		if ($this->input->is_ajax_request()) {
			$settings_html = $this->load->view('mail_system3/settings', $settings, true);
			echo json_encode(array('success' => true, 'message' => "Successfully load settings!", 'settings_html' => $settings_html));
			exit();
		} else {
			set_page('mail_system3/mailbox', array('settings' => $settings, 'current_folder' => 'inbox'));
		}
	}

	function save_settings()
	{
		$setting_data = $_POST;
		$this->db->select('id');
		$this->db->where('staff_id', $this->staff_id);
		$this->db->from('mailbox_settings');
		if ($this->db->get()->num_rows() > 0) {
			foreach ($setting_data as $setting_key => $setting_value) {
				$this->db->where('setting_key', $setting_key);
				$this->db->where('staff_id', $this->staff_id);
				$this->db->update('mailbox_settings', array('setting_value' => $setting_value));
			}
		} else {
			foreach ($setting_data as $setting_key => $setting_value) {
				$s_data = array('setting_key' => $setting_key, 'setting_value' => $setting_value, 'staff_id' => $this->staff_id);
				$this->db->insert('mailbox_settings', $s_data);
			}
		}
		echo json_encode(array('success' => true, 'message' => 'Settings save successfully!'));
		exit();
	}

	/**
     * @return array
     */
	function load_settings()
	{
		$this->db->select('setting_key,setting_value');
		$this->db->where('staff_id', $this->staff_id);
		$this->db->from('mailbox_settings');
		$query = $this->db->get();
		$setting_data = array();
		foreach ($query->result() as $setting) {
			$setting_data[$setting->setting_key] = $setting->setting_value;
		}
		return $setting_data;
	}

	function save_unread_mails()
	{
		if ($this->check_internet_conn()) {
			$mailbox = $this->email_server . "INBOX";
			$mb = imap_open($mailbox, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!3");
			$mail_status = imap_status($mb, $this->email_server . "INBOX", SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!4");
			if ($mail_status->messages > 0) {
				$sort_results = imap_sort($mb, SORTDATE, 1);
				$search_results = imap_search($mb, 'UNSEEN');
				if (is_array($search_results)) {
					$message_nos = $this->order_search($search_results, $sort_results);
					foreach ($message_nos as $key => $message_no) {
						$email_headers = imap_headerinfo($mb, $message_no);

						$this->db->select('mail_id');
						$this->db->from('mail_system');
						$this->db->where('mail_uid', $email_headers->message_id);
						$this->db->where('staff_id', $this->staff_id);
						if ($this->db->get()->num_rows() > 0) {

						} else {
							/*echo "<pre>";print_r($email_headers);die;*/
							$mail_data = array();
							if (isset($email_headers->from[0]->mailbox) && $email_headers->from[0]->mailbox != '') {
								$mail_data['from_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
								if (isset($email_headers->from[0]->personal)) {
									$mail_data['from_name'] = $email_headers->from[0]->personal;
								} else {
									$mail_data['from_name'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
								}
							}

							if (isset($email_headers->to[0]->mailbox) && $email_headers->to[0]->mailbox != '') {
								$mail_data['to_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
								if (isset($email_headers->to[0]->personal)) {
									$mail_data['to_name'] = $email_headers->to[0]->personal;
								} else {
									$mail_data['to_name'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
								}
							}

							if (isset($email_headers->reply_to[0]->mailbox) && $email_headers->reply_to[0]->mailbox != '') {
								$mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
								if (isset($email_headers->reply_to[0]->personal)) {
									$mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
								} else {
									$mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
								}
							}

							if (isset($email_headers->sender[0]->mailbox) && $email_headers->sender[0]->mailbox != '') {
								$mail_data['sender_address'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
								if (isset($email_headers->sender[0]->personal)) {
									$mail_data['sender_name'] = $email_headers->sender[0]->personal;
								} else {
									$mail_data['sender_name'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
								}
							}

							$attachments = $this->get_mail_attachments($mb, $message_no);
							if (count($attachments) > 0) {
								$mail_data['is_attachment'] = 1;
								$attachment_url = array();
								$time = time();
								mkdir('resource/uploads/attachments/' . $time);
								chmod('resource/uploads/attachments/' . $time, 0777);
								foreach ($attachments as $attachment) {
									$file = "resource/uploads/attachments/$time/" . $attachment['filename'];
									file_put_contents($file, base64_decode($attachment['file']));
									$attachment_url[] = array(
										'filename' => $attachment['filename'],
										'url' => $file,
										'subtype' => $attachment['subtype'],
									);
								}
								$mail_data['attachments'] = serialize($attachment_url);
							}

							$mail_data['cc'] = array();
							if (isset($email_headers->cc) && count($email_headers->cc) > 0) {
								foreach ($email_headers->cc as $cc_row) {
									$mail_data['cc'][] = $cc_row->mailbox . '@' . $cc_row->host;
								}
							}
							if (isset($email_headers->to) && count($email_headers->to) > 1) {
								foreach ($email_headers->to as $cc_row) {
									if ($this->email_address != $cc_row->mailbox . '@' . $cc_row->host) {
										$mail_data['cc'][] = $cc_row->mailbox . '@' . $cc_row->host;
									}
								}
							}
							$mail_data['bcc'] = array();
							if (isset($email_headers->bcc) && count($email_headers->bcc) > 0) {
								foreach ($email_headers->bcc as $bcc_row) {
									$mail_data['bcc'][] = $bcc_row->mailbox . '@' . $bcc_row->host;
								}
							}
							$mail_data['cc'] = count($mail_data['cc']) > 0 ? serialize($mail_data['cc']) : null;
							$mail_data['bcc'] = count($mail_data['bcc']) > 0 ? serialize($mail_data['bcc']) : null;
							$mail_data['subject'] = $email_headers->subject;

							/*----------- Fetch Mail Body -----------------*/
							/*$this->load->library('email_message',array('connection'=>$mb,'message_no'=>$message_no));
                            $mail_data['body'] = $this->email_message->get_mail_body();*/

							$mail_data['body'] = $this->get_mail_body($mb, $message_no);
							$mail_data['body'] = str_replace('<a ', '<a target="_blank"', $mail_data['body']);
							$mail_data['mail_uid'] = $email_headers->message_id;
							$mail_data['received_at'] = $email_headers->date;
							$mail_data['is_unread'] = $email_headers->Unseen == 'U' ? 1 : 0;
							$mail_data['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
							$mail_data['folder'] = 'inbox';
							$mail_data['staff_id'] = trim($this->session->userdata('is_logged_in')['staff_id']);
							$mail_data['created_at'] = date('Y-m-d H:i:s');
							$this->db->insert('mail_system', $mail_data);
						}
					}
				}
			}
			imap_close($mb);
			echo json_encode(array('success' => true, 'message' => 'Mailbox updated successfully!'));
		} else {
			echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
		}
		exit();
	}


	function flattenParts($messageParts, $flattenedParts = array(), $prefix = '', $index = 1, $fullPrefix = true)
	{
		foreach ($messageParts as $part) {
			$flattenedParts[$prefix . $index] = $part;
			if (isset($part->parts)) {
				if ($part->type == 2) {
					$flattenedParts = flattenParts($part->parts, $flattenedParts, $prefix . $index . '.', 0, false);
				} elseif ($fullPrefix) {
					$flattenedParts = flattenParts($part->parts, $flattenedParts, $prefix . $index . '.');
				} else {
					$flattenedParts = flattenParts($part->parts, $flattenedParts, $prefix);
				}
				unset($flattenedParts[$prefix . $index]->parts);
			}
			$index++;
		}
		return $flattenedParts;
	}

	function getPart($connection, $messageNumber, $partNumber, $encoding)
	{
		$data = imap_fetchbody($connection, $messageNumber, $partNumber);
		switch ($encoding) {
			case 0:
				return $data; // 7BIT
			case 1:
				return $data; // 8BIT
			case 2:
				return $data; // BINARY
			case 3:
				return base64_decode($data); // BASE64
			case 4:
				return quoted_printable_decode($data); // QUOTED_PRINTABLE
			case 5:
				return $data; // OTHER
		}


	}

	function getFilenameFromPart($part)
	{
		$filename = '';
		if ($part->ifdparameters) {
			foreach ($part->dparameters as $object) {
				if (strtolower($object->attribute) == 'filename') {
					$filename = $object->value;
				}
			}
		}
		if (!$filename && $part->ifparameters) {
			foreach ($part->parameters as $object) {
				if (strtolower($object->attribute) == 'name') {
					$filename = $object->value;
				}
			}
		}
		return $filename;
	}

	function get_mail_body($mb, $message_no)
	{
		if (!isset($this->email_message)) {
			$this->load->library('email_message', array('connection' => $mb, 'message_no' => $message_no));
		} else {
			$this->email_message->connection = $mb;
			$this->email_message->messageNumber = $message_no;
		}
		$this->email_message->bodyHTML = '';
		$this->email_message->bodyPlain = '';
		$this->email_message->attachments = array();
		$this->email_message->getAttachments = true;
		$mailbody = $this->email_message->get_mail_body();
		if ($mailbody == '') {
			$mailbody = $this->get_mail_body_v1($mb, $message_no);
		}
		return $mailbody;
	}

	function get_mail_body_v1($mb, $message_no)
	{
		$body = $this->get_part($mb, $message_no, "TEXT/HTML");
		if ($body == "") {
			$body = $this->get_part($mb, $message_no, "TEXT/PLAIN");
		}
		return $body;
	}

	function get_part($imap, $uid, $mimetype, $structure = false, $partNumber = false)
	{
		if (!$structure) {
			$structure = imap_fetchstructure($imap, $uid);
		}
		if ($structure) {
			if ($mimetype == $this->get_mime_type($structure)) {
				if (!$partNumber) {
					$partNumber = 1;
				}
				$text = imap_fetchbody($imap, $uid, $partNumber);
				/*$text = imap_fetchbody($inbox,$email_number,1.1);*/
				switch ($structure->encoding) {
						/*case 0: return imap_7bit($text);
                    case 1: return imap_8bit($text);
                    case 2: return imap_binary($text);*/
					case 3:
						return base64_decode($text);
					case 4:
						return quoted_printable_decode($text);
					default:
						return $text;
				}
			}
			// multipart 
			if ($structure->type == 1) {
				foreach ($structure->parts as $index => $subStruct) {
					$prefix = "";
					if ($partNumber) {
						$prefix = $partNumber . ".";
					}
					$data = $this->get_part($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
					if ($data) {
						return $data;
					}
				}
			}
		}
		return false;
	}

	function get_mime_type($structure)
	{
		$primaryMimetype = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

		if ($structure->subtype) {
			return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
		}
		return "TEXT/PLAIN";
	}

	/**
     * @param $mb
     * @param $message_no
     * @return array
     */
	function get_mail_attachments($mb, $message_no)
	{
		$attachments = array();
		$structure = imap_fetchstructure($mb, $message_no);
		if (isset($structure->parts) && count($structure->parts)) {
			for ($i = 0; $i < count($structure->parts); $i++) {
				if (!isset($structure->parts[$i]->id)) {
					$attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
					$attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
					if ($structure->parts[$i]->ifdparameters) {
						foreach ($structure->parts[$i]->dparameters as $object) {
							if (strtolower($object->attribute) == 'filename') {
								$attachments[$i]['is_attachment'] = true;
								$attachments[$i]['filename'] = $object->value;
							}
						}
					}
					if ($structure->parts[$i]->ifparameters) {
						foreach ($structure->parts[$i]->parameters as $object) {
							if (strtolower($object->attribute) == 'name') {
								$attachments[$i]['is_attachment'] = true;
								$attachments[$i]['name'] = $object->value;
							}
						}
					}
					if ($attachments[$i]['is_attachment']) {
						$attachments[$i]['attachment'] = imap_fetchbody($mb, $message_no, $i + 1);
						if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
						} elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
							$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
						}
					}
				}
			}
		}
		$response_data = array();
		if (count($attachments) != 0) {
			foreach ($attachments as $at) {
				if ($at['is_attachment'] == 1) {
					$response_data[] = array(
						'filename' => $at['name'],
						'file' => $at['attachment'],
						'subtype' => $at['subtype'],
					);
				}
			}
		}
		return $response_data;
	}

	function send_outbox_mails()
	{
		$this->db->select('mail_id,to_address,subject,body,attachments,cc,bcc,is_read_receipt_req');
		$this->db->from('mail_system');
		$this->db->where('staff_id', $this->staff_id);
		$this->db->where('folder', 'outbox');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $mail_row) {
				$to_address = $mail_row->to_address;
				$subject = $mail_row->subject;
				$body = $mail_row->body;
				$cc = $mail_row->cc != null ? unserialize($mail_row->cc) : null;
				$bcc = $mail_row->bcc != null ? unserialize($mail_row->bcc) : null;
				$is_read_receipt_req = $mail_row->is_read_receipt_req;
				$attach = array();
				$attachments = $mail_row->attachments;
				if ($attachments != null) {
					$attachments = unserialize($attachments);
					foreach ($attachments as $attachment_row) {
						$attach[] = $attachment_row['url'];
					}
				}
				/*----- Delete mail from outbox -------*/
				$this->db->where('mail_id', $mail_row->mail_id);
				$this->db->delete('mail_system');

				$this->send_mail($to_address, $subject, $body, $attach, $cc, $bcc, $is_read_receipt_req);
			}
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Outbox successfully update!');
			redirect(base_url() . "mail-system3/outbox/");
			exit();
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Currently no mails in outbox!');
			redirect(base_url() . "mail-system3/outbox/");
			exit();
		}
	}

	/**
     * @param $SearchResults
     * @param $SortResults
     * @return array
     */
	function order_search($SearchResults, $SortResults)
	{
		return array_values(array_intersect($SearchResults, $SortResults));
	}

	function upload_attachment()
	{
		$number_of_files_uploaded = count($_FILES['upload_attachment']['name']);
		$this->load->library('upload');
		$attachment_url = array();
		$file_name = array();
		for ($i = 0; $i < $number_of_files_uploaded; $i++){
			$_FILES['upload_attachment_file']['name']     = $_FILES['upload_attachment']['name'][$i];
			$_FILES['upload_attachment_file']['type']     = $_FILES['upload_attachment']['type'][$i];
			$_FILES['upload_attachment_file']['tmp_name'] = $_FILES['upload_attachment']['tmp_name'][$i];
			$_FILES['upload_attachment_file']['error']    = $_FILES['upload_attachment']['error'][$i];
			$_FILES['upload_attachment_file']['size']     = $_FILES['upload_attachment']['size'][$i];
			$config['upload_path'] = 'resource/uploads/attachments';
			$config['allowed_types'] = '*';
			$config['overwrite'] = TRUE;
			$this->upload->initialize($config);
			if ($this->upload->do_upload('upload_attachment_file')) {
				$file_data = $this->upload->data();
				$attachment_url[] = 'resource/uploads/attachments/' . $file_data['file_name'];
				$file_name[] = $file_data['file_name'];
			} else {
				echo json_encode(array('success' => false, 'message' => 'Please attach file in jaykhodiyar email.', 'error' => $this->upload->display_errors()));
				exit;
			}
		}
		echo json_encode(array('success' => true, 'message' => 'Attachment successfully uploaded.', 'attachment_url' => $attachment_url, 'file_name' => $file_name));
		exit;
	}

	function save_unread_mails_for_all()
	{
		if(!$this->app_model->have_access_role(CRON_LOAD_ALL_STAFFS_UNREAD_MAILS_MENU_ID, "view")){
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
		if ($this->check_internet_conn()) {
			$this->db->select('staff_id,mailbox_email,mailbox_password');
			$this->db->from('staff');
			$this->db->where('active !=', 0);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$connection_error = '';
				$loaded_emails = array();
				foreach ($query->result() as $staff_row) {
					if ($staff_row->mailbox_email != '' && $staff_row->mailbox_password != '' && !in_array($staff_row->mailbox_email, $loaded_emails)) {
						$loaded_emails[] = $staff_row->mailbox_email;
						$staff_row->mailbox_email = trim($staff_row->mailbox_email);
						$staff_row->mailbox_password = trim($staff_row->mailbox_password);

						$mailbox = $this->email_server . "INBOX";
						$mb = imap_open($mailbox, $staff_row->mailbox_email, $staff_row->mailbox_password);
						if ($mb != FALSE) {
							$mail_status = imap_status($mb, $this->email_server . "INBOX", SA_ALL);
							if ($mail_status->messages > 0) {
								$sort_results = imap_sort($mb, SORTDATE, 1);
								$search_results = imap_search($mb, 'UNSEEN');
								if (is_array($search_results)) {
									$message_nos = $this->order_search($search_results, $sort_results);
									foreach ($message_nos as $key => $message_no) {
										$email_headers = imap_headerinfo($mb, $message_no);
										$mail_data = array();
										if (isset($email_headers->from[0]->mailbox) && $email_headers->from[0]->mailbox != '') {
											$mail_data['from_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
											if (isset($email_headers->from[0]->personal)) {
												$mail_data['from_name'] = $email_headers->from[0]->personal;
											} else {
												$mail_data['from_name'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
											}
										}

										if (isset($email_headers->to[0]->mailbox) && $email_headers->to[0]->mailbox != '') {
											$mail_data['to_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
											if (isset($email_headers->to[0]->personal)) {
												$mail_data['to_name'] = $email_headers->to[0]->personal;
											} else {
												$mail_data['to_name'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
											}
										}

										if (isset($email_headers->reply_to[0]->mailbox) && $email_headers->reply_to[0]->mailbox != '') {
											$mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
											if (isset($email_headers->reply_to[0]->personal)) {
												$mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
											} else {
												$mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
											}
										}

										if (isset($email_headers->sender[0]->mailbox) && $email_headers->sender[0]->mailbox != '') {
											$mail_data['sender_address'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
											if (isset($email_headers->sender[0]->personal)) {
												$mail_data['sender_name'] = $email_headers->sender[0]->personal;
											} else {
												$mail_data['sender_name'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
											}
										}

										$attachments = $this->get_mail_attachments($mb, $message_no);
										if (count($attachments) > 0) {
											$mail_data['is_attachment'] = 1;
											$attachment_url = array();
											$time = time();
											mkdir('resource/uploads/attachments/' . $time);
											foreach ($attachments as $attachment) {
												$file = "resource/uploads/attachments/$time/" . $attachment['filename'];
												file_put_contents($file, base64_decode($attachment['file']));
												$attachment_url[] = array(
													'filename' => $attachment['filename'],
													'url' => $file,
													'subtype' => $attachment['subtype'],
												);
											}
											$mail_data['attachments'] = serialize($attachment_url);
										}

										$mail_data['cc'] = array();
										if (isset($email_headers->cc) && count($email_headers->cc) > 0) {
											foreach ($email_headers->cc as $cc_row) {
												$mail_data['cc'][] = $cc_row->mailbox . '@' . $cc_row->host;
											}
										}
										if (isset($email_headers->to) && count($email_headers->to) > 1) {
											foreach ($email_headers->to as $cc_row) {
												if ($staff_row->mailbox_email != $cc_row->mailbox . '@' . $cc_row->host) {
													$mail_data['cc'][] = $cc_row->mailbox . '@' . $cc_row->host;
												}
											}
										}
										$mail_data['bcc'] = array();
										if (isset($email_headers->bcc) && count($email_headers->bcc) > 0) {
											foreach ($email_headers->bcc as $bcc_row) {
												$mail_data['bcc'][] = $bcc_row->mailbox . '@' . $bcc_row->host;
											}
										}
										$mail_data['cc'] = count($mail_data['cc']) > 0 ? serialize($mail_data['cc']) : null;
										$mail_data['bcc'] = count($mail_data['bcc']) > 0 ? serialize($mail_data['bcc']) : null;
										$mail_data['subject'] = $email_headers->subject;

										/*----------- Fetch Mail Body -----------------*/
										/*$this->load->library('email_message',array('connection'=>$mb,'message_no'=>$message_no));
                                        $mail_data['body'] = $this->email_message->get_mail_body();*/
										$mail_data['body'] = $this->get_mail_body($mb, $message_no);
										$mail_data['body'] = str_replace('<a ', '<a target="_blank"', $mail_data['body']);
										$mail_data['received_at'] = $email_headers->date;
										$mail_data['is_unread'] = $email_headers->Unseen == 'U' ? 1 : 0;
										$mail_data['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
										$mail_data['folder'] = 'inbox';
										$mail_data['staff_id'] = $staff_row->staff_id;
										$mail_data['created_at'] = date('Y-m-d H:i:s');
										$this->db->insert('mail_system', $mail_data);
									}
								}
							}
						}
						imap_close($mb);
					}
				}
				echo json_encode(array('success' => true, 'message' => 'Mailbox updated successfully!', 'connection_error' => $connection_error));
			} else {
				echo json_encode(array('success' => false, 'message' => 'Please try again, No staff member found!'));
			}
		} else {
			echo json_encode(array('success' => false, 'message' => 'No internet connection available'));
		}
		exit();
	}

	/**
     * @param $document_type
     * @param $document_id
     */
	function document_mail($document_type, $document_id)
	{
		/*echo '<pre><br>';print_r($document_type);
        echo '<pre><br>';print_r($document_id);exit;*/
		if ($document_type != '' && $document_id != '') {
			if ($document_type == 'quotation') {
				$this->db->select('pdf_url, email_id, party_id');
				$this->db->from('quotations');
				$this->db->where('id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {

					$this->crud->update('quotations', array('action' => 'sent'), array('id' => $document_id));

					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					$email_ids = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $query->row()->party_id);

					/*************/
					$this->db->select('f.file_url');
					$this->db->from('item_files f');
					$this->db->join('quotation_items qi', 'qi.item_id = f.item_id');
					$where = array();
					$where['qi.quotation_id'] = $document_id;
					$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
					$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
					if ($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID, "view")) {
					} else {
						if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
						} else if ($cu_accessExport == 1) {
							$where['f.item_type'] = 'Export';
						} else if ($cu_accessDomestic == 1) {
							$where['f.item_type'] = 'Domestic';
						} else {
						}
					}
					$this->db->where($where);
					$this->db->group_by('f.id');
					$query = $this->db->get();
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $file_row) {
							$document_url[] = $file_row->file_url;
						}
					}
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					$data['subject'] = ucfirst($document_type);
					//$data['to_address'] = trim($email_ids[0]);
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Document not found!');
					redirect(base_url() . "mail-system3/compose-mail/compose");
				}
			} elseif ($document_type == 'salary-report') {
				$this->db->select('pdf_url,email');
				$this->db->from('staff');
				$this->db->where('staff_id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$settings = $this->load_settings();
					$document_url = $query->row()->pdf_url;
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'][] = $document_url;
					$data['subject'] = 'Salary Report';
					$data['to_address'] = trim($query->row()->email);
					set_page('mail_system3/mailbox', $data);
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Document not found!');
					redirect(base_url() . "mail-system3/compose-mail/compose");
				}
			} elseif ($document_type == 'sales-order') {
				$this->db->select('pdf_url,email_id,sales_to_party_id');
				$this->db->from('sales_order');
				$this->db->where('id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {

					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					//$email_ids = explode(',',$query->row()->email_id);
					$party_id = $query->row()->sales_to_party_id;
					$email_ids = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $party_id);
					$this->db->select('f.file_url');
					$this->db->from('item_files f');
					$this->db->join('sales_order_items soi', 'soi.item_id = f.item_id');
					$this->db->where('soi.sales_order_id', $document_id);
					$this->db->group_by('f.id');
					$query = $this->db->get();
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $file_row) {
							$document_url[] = $file_row->file_url;
						}
					}
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					$data['subject'] = ucfirst('Sales Order');
					//$data['to_address'] = trim($email_ids[0]);
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Document not found!');
					redirect(base_url() . "mail-system3/compose-mail/compose");
				}
			} elseif ($document_type == 'invoice') {
                $this->db->select('pdf_url,sales_to_party_id');
				$this->db->from('invoices');
				$this->db->where('id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					
					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					$party_id = $query->row()->sales_to_party_id;
					$email_ids = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $party_id);
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					$data['subject'] = ucfirst('Invoice');
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				}
			} elseif ($document_type == 'proforma_invoice') {
				$this->db->select('pdf_url,email_id');
				$this->db->from('proforma_invoices');
				$this->db->where('id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$settings = $this->load_settings();
					$document_url = $query->row()->pdf_url;
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'][] = $document_url;
					$data['subject'] = 'Proforma Invoice';
					$data['to_address'] = trim($query->row()->email_id);
					set_page('mail_system3/mailbox', $data);
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Document not found!');
					redirect(base_url() . "mail-system3/compose-mail/compose");
				}
			} elseif ($document_type == 'purchases-order') {
				$this->db->select('pdf_url');
				$this->db->from('purchase_order');
				$this->db->where('order_id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$settings = $this->load_settings();
					$document_url = $query->row()->pdf_url;
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'][] = $document_url;
					$data['subject'] = 'Purchase Order';
					$data['to_address'] = trim($query->row()->email_id);
					set_page('mail_system3/mailbox', $data);
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Document not found!');
					redirect(base_url() . "mail-system3/compose-mail/compose");
				}
			} elseif ($document_type == 'customer_reminder_sent') {
				$this->db->select('subject, party_id, pdf_url');
				$this->db->from('customer_reminder_sent');
				$this->db->where('id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$settings = $this->load_settings();
					$document_url = $query->row()->pdf_url;
					$party_id = $query->row()->party_id;
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'][] = $document_url;
					$data['subject'] = $query->row()->subject;
					$data['to_address'] = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $party_id);
					set_page('mail_system3/mailbox', $data);
				} else {
					$this->session->set_flashdata('success', false);
					$this->session->set_flashdata('message', 'Document not found!');
					redirect(base_url() . "mail-system3/compose-mail/compose");
				}
			} elseif ($document_type == 'installation') {
				$this->db->select('pdf_url,party_id');
				$this->db->from('installation');
				$this->db->where('installation_id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					
					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					$party_id = $query->row()->party_id;
					$email_ids = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $party_id);
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					$data['subject'] = ucfirst('Installation');
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				}
			} elseif ($document_type == 'complain') {
				$this->db->select('pdf_url,party_id');
				$this->db->from('complain');
				$this->db->where('complain_id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					
					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					$party_id = $query->row()->party_id;
					$email_ids = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $party_id);
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					$data['subject'] = ucfirst('Complain');
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				}
			} elseif ($document_type == 'resolve_complain') {
				$this->db->select('pdf_url,party_id');
				$this->db->from('resolve_complain');
				$this->db->where('resolve_complain_id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					
					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					$party_id = $query->row()->party_id;
					$email_ids = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $party_id);
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					$data['subject'] = ucfirst('Resolve Complain');
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				}
			} elseif ($document_type == 'general_invoice') {
				$this->db->select('pdf_url,party_id');
				$this->db->from('general_invoice');
				$this->db->where('general_invoice_id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					
					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					$party_id = $query->row()->party_id;
					$email_ids = $this->crud->get_id_by_val('party', 'email_id', 'party_id', $party_id);
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					$data['subject'] = ucfirst('General Invoice');
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				}
			} elseif ($document_type == 'print_letter') {
				$this->db->select('pdf_url,letter_for_id,person_id');
				$this->db->from('print_letter');
				$this->db->where('id', $document_id);
				$this->db->limit(1);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$settings = $this->load_settings();
					$document_url = unserialize($query->row()->pdf_url);
					$letter_for_id = $query->row()->letter_for_id;
					$person_id = $query->row()->person_id;
					
					if($letter_for_id == LETTER_FOR_STAFF_ID){
						$email_ids = $this->crud->get_id_by_val('staff', 'email', 'staff_id', $person_id);	
					}elseif($letter_for_id == LETTER_FOR_INTERVIEWED_PERSON_ID){
						$email_ids = $this->crud->get_id_by_val('interview', 'email', 'interview_id', $person_id);
					}
					$data['settings'] = $settings;
					$data['current_folder'] = 'inbox';
					$data['action'] = 'compose';
					$data['document_url'] = $document_url;
					
					$letter_topic_id = $this->crud->get_id_by_val('print_letter', 'letter_topic_id', 'id', $document_id);
					$letter_topic = $this->crud->get_id_by_val('letters', 'letter_topic', 'letter_id', $letter_topic_id);
					
					$data['subject'] = ucfirst($letter_topic);
					$data['to_address'] = trim($email_ids);
					set_page('mail_system3/mailbox', $data);
				}
			}
		} else {
			$this->session->set_flashdata('success', false);
			$this->session->set_flashdata('message', 'Document not found!');
			redirect(base_url() . "mail-system3/compose-mail/compose");
		}

	}

	/**
     * @param $email
     */
	function mail_auto_complete($email)
	{
		$query = "SELECT email_list.email 
                FROM 
                ( 
                    SELECT `from_address` as email FROM mail_system WHERE staff_id = '$this->staff_id' UNION 
                    SELECT `to_address` as email FROM mail_system WHERE staff_id = '$this->staff_id' UNION
                    SELECT `reply_to` as email FROM mail_system WHERE staff_id = '$this->staff_id' UNION
                    SELECT `sender_address` as email FROM mail_system WHERE staff_id = '$this->staff_id' 
                ) email_list 
                WHERE email_list.email IS NOT NULL AND email_list.email LIKE '%$email%' ";
		$query_result = $this->db->query($query);
		$response = array();
		if ($query_result->num_rows() > 0) {
			foreach ($query_result->result() as $row) {
				$response[] = $row->email;
			}
		}

		$this->db->select('email_id');
		$this->db->from('party');
		$this->db->like('email_id', $email);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $key => $value) {
				$email_ids = explode(',', $value->email_id);
				if (count($email_ids) > 0) {
					foreach ($email_ids as $key => $email_id) {
						$response[] = $email_id;
					}
				}
			}
		}

		echo json_encode(array_unique($response));
		exit();
	}

	function email_testing()
	{
		$this->load->library('email');
		$subject = 'This is a test';
		$message = '<p>This message has been sent for testing purposes.</p>';

		// Get full html:
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
            <title>' . html_escape($subject) . '</title>
            <style type="text/css">
                body {
                    font-family: Arial, Verdana, Helvetica, sans-serif;
                    font-size: 16px;
                }
            </style>
        </head>
        <body>
        ' . $message . '
        </body>
        </html>';


		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);
		// pre_die($this->email);
		$this->email
			->from('test.presntr@gmail.com')
			->reply_to('test.presntr@gmail.com')// Optional, an account where a human being reads.
			->to('cshailesh157@gmail.com')
			->subject($subject)
			->message($body)
			->send();
		echo '<br />';

		echo $this->email->print_debugger();
	}

	function testing()
	{
		set_page('mail_system3/testing');
	}

	/**
     * @param bool $call_by_ajax
     * @return bool
     */
	function get_folder_by_staff($call_by_ajax = false)
	{
		$folder_data = $this->crud->get_all_with_where('mail_system_folder', 'folder', 'asc', array('staff_id' => $this->staff_id));
		if ($call_by_ajax == true) {
			if (!empty($folder_data)) {
				echo json_encode(array('success' => true, 'folder_data' => $folder_data));
				exit();
			} else {
				echo json_encode(array('success' => false));
				exit();
			}

		} else {
			return $folder_data;
		}
	}

	/**
     * @param $folder_id
     */
	function delete_folder($folder_id)
	{
		$folder_data = $this->crud->get_data_row_by_id('mail_system_folder', 'folder_id', $folder_id);
		if (!empty($folder_data)) {
			$this->crud->update('mail_system', array('folder' => 'inbox'), array('folder' => $folder_data->folder, 'staff_id' => $folder_data->staff_id));
			$this->crud->delete('mail_system_folder', array('folder_id' => $folder_id));
			$sidebar_mail_label = $this->load->view('mail_system3/sidebar_mail_label', array('current_folder' => 'inbox'), true);
			$move_to_folder = $this->load->view('mail_system3/move_to_folder_view', array('current_folder' => 'inbox'), true);
			echo json_encode(array('success' => true, 'message' => 'Folder deleted successfully.', 'sidebar_mail_label' => $sidebar_mail_label, 'move_to_folder' => $move_to_folder));
		} else {
			echo json_encode(array('success' => false, 'message' => "Something wrong!"));
		}
	}

	function attachment_browse_from_server()
	{
		$this->load->view('mail_system3/browse');
	}


	function upload_attachment_to_server()
	{
		$upload_url = 'resource/uploads/attachments/';
		$this->load->library('upload');
		$config['upload_path'] = $upload_url;
		$config['allowed_types'] = '*';
		$config['overwrite'] = TRUE;
		$this->upload->initialize($config);
		if ($this->upload->do_upload('upload')) {
			$file_data = $this->upload->data();
			$url = base_url($upload_url . $file_data['file_name']);
			$funcNum = $_GET['CKEditorFuncNum'];
			$CKEditor = $_GET['CKEditor'];
			$langCode = $_GET['langCode'];
			$message = '';
			echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
		}
	}

	function custom_mail_test()
	{
		$this->load->library('email');

		$subject = 'This is a test';
		$message = '<p>This message has been sent for testing purposes.</p>';

		// Get full html:
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
				body {
					font-family: Arial, Verdana, Helvetica, sans-serif;
					font-size: 16px;
				}
			</style>
		</head>
		<body>
		' . $message . '
		</body>
		</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);
		pre_die($this->email);
		$result = $this->email
			->from('test.presntr@gmail.com')
			->reply_to('test.presntr@gmail.com')// Optional, an account where a human being reads.
			->to('cshailesh157@gmail.com')
			//->to('nearbybestdeals@gmail.com')
			->subject($subject)
			->message($body)
			->send();

		pre_die($result);
		echo '<br />';
		pre_die($this->email->print_debugger());
		exit;
	}
}
