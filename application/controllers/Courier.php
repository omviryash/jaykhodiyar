<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Payment
 * &@property Crud $crud
 */
class Courier extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
    }

    function add($courier_id = '') {
        $data = array();
        if (isset($courier_id) && !empty($courier_id)) {
            if ($this->app_model->have_access_role(COURIER_MENU_ID, "edit")) {
                $where_array['courier_id'] = $courier_id;
                $couriers = $this->crud->get_row_by_id('couriers', $where_array);
                $data['created_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $couriers[0]->created_by));
                $data['created_at'] = substr($couriers[0]->created_at, 8, 2) . '-' . substr($couriers[0]->created_at, 5, 2) . '-' . substr($couriers[0]->created_at, 0, 4);
                $data['updated_by_name'] = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $couriers[0]->updated_by));
                $data['updated_at'] = substr($couriers[0]->updated_at, 8, 2) . '-' . substr($couriers[0]->updated_at, 5, 2) . '-' . substr($couriers[0]->updated_at, 0, 4);
                $data['courier_id'] = $couriers[0]->courier_id;
                $data['party_id'] = $couriers[0]->party_id;
                $data['address_type'] = $couriers[0]->address_type;
                $data['courier_name'] = $couriers[0]->courier_name;
                $data['courier_date'] = $couriers[0]->courier_date;
                $data['courier_time'] = $couriers[0]->courier_time;
                $data['docket_no'] = $couriers[0]->docket_no;
                $data['party_name'] = $couriers[0]->party_name;
                $data['party_contact_no'] = $couriers[0]->party_contact_no;
                $data['party_address'] = $couriers[0]->party_address;
                $data['contact_person_name'] = $couriers[0]->contact_person_name;
                $data['contact_person_no'] = $couriers[0]->contact_person_no;
                $data['parcel_details'] = $couriers[0]->parcel_details;
                set_page('courier/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            if ($this->app_model->have_access_role(COURIER_MENU_ID, "add")) {
                set_page('courier/add');
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }

    function save_courier() {
        $return = array();
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data); exit;
        $post_data['courier_date'] = !empty(strtotime($post_data['courier_date'])) ? date("Y-m-d", strtotime($post_data['courier_date'])) : NULL;
        $post_data['courier_time'] = !empty(strtotime($post_data['courier_time'])) ? date("H:i:s", strtotime($post_data['courier_time'])) : NULL;
        if (isset($post_data['courier_id']) && !empty($post_data['courier_id'])) {
            $post_data['updated_by'] = $this->staff_id;
            $post_data['updated_at'] = $this->now_time;
            //print_r($post_data); exit;
            if($post_data['address_type'] == null){
                $post_data['address_type'] = NULL;
            }
            if($post_data['party_id'] == null){
                $post_data['party_id'] = NULL;
            }
            $result = $this->crud->update('couriers', $post_data, array('courier_id' => $post_data['courier_id']));
            if ($result) {
                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Courier Updated Successfully');
            }
        } else {
            $post_data['created_by'] = $this->staff_id;
            $post_data['created_at'] = $this->now_time;
            if($post_data['address_type'] == null){
                $post_data['address_type'] = NULL;
            }
            $result = $this->crud->insert('couriers', $post_data);
            if ($result) {
                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Courier Added Successfully');
            }
        }
        echo json_encode($return);
        exit;
    }

    function courier_list() {
        $data = array();
        if ($this->applib->have_access_role(COURIER_MENU_ID, "view")) {
            set_page('courier/courier_list', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function courier_list_datatable() {
        $config['table'] = 'couriers c';
        $config['select'] = 'c.*, p.party_name as party_name_by_id, ';
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = c.party_id', 'join_type' => 'left');
//        $config['column_order'] = array('ss.module_name', 'pt.party_name,sup.supplier_name,stff.name as staff_name', 'ss.send_to', 'st.topic_name', 'ss.message', 'stf.name', 'ss.created_at');
//        $config['column_search'] = array('ss.module_name', 'ss.send_to', 'st.topic_name', 'ss.message', 'stf.name', 'DATE_FORMAT(ss.created_at,"%d-%m-%Y")');
        $config['order'] = array('c.courier_id' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $isEdit = $this->app_model->have_access_role(COURIER_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(COURIER_MENU_ID, "delete");
        foreach ($list as $courier) {
            $row = array();
            $action = '';
            if ($isEdit) {
                $action .= '<a href="' . base_url('courier/add/' . $courier->courier_id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
            }
            if ($isDelete) {
                $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('courier/delete_courier/' . $courier->courier_id) . '"><i class="fa fa-trash"></i></a>';
            }
            $action .= ' | <a href="' . base_url('courier/courier_pdf/'. $courier->courier_id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
            $row[] = $action;
            $party_name = '';
            if(isset($courier->party_name_by_id) && !empty($courier->party_name_by_id)){
                $party_name = $courier->party_name_by_id;
            } else {
                $party_name = $courier->party_name;
            }
            $row[] = $party_name;
            if ($courier->address_type == '1') {
                $row[] = 'Party Details Address';
            } else if ($courier->address_type == '2') {
                $row[] = 'Delivery Address';
            } else if ($courier->address_type == '3') {
                $row[] = 'Communication Address';
            } else {
                $row[] = 'Manual';
            }
            $row[] = $courier->courier_name;
            $row[] = $courier->courier_date;
            $row[] = $courier->courier_time;
            $row[] = $courier->docket_no;
            $row[] = date('d-m-Y h:i', strtotime($courier->created_at));
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function delete_courier($courier_id){
		$role_delete = $this->app_model->have_access_role(COURIER_MENU_ID, "delete");
		if ($role_delete) {
            $where_array = array("courier_id" => $courier_id);
			$this->crud->delete("couriers", $where_array);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function courier_pdf($courier_id){
		$courier_row = $this->crud->get_row_by_id('couriers',array('courier_id' => $courier_id));
        if(!empty($courier_row)) {
            $courier_row = $courier_row[0];
            if (isset($courier_row->party_id) && !empty($courier_row->party_id)) {
                $party_row = $this->crud->get_row_by_id('party', array('party_id' => $courier_row->party_id));
                $party_row = $party_row[0];
                $party_name = $party_row->party_name;
                $address = '';
                if ($courier_row->address_type == '1') {
                    $address = $party_row->address;
                } else if ($courier_row->address_type == '2') {
                    if ($courier_row->address_work == '1') {
                        $address = $party_row->address;
                    } else {
                        $address = $party_row->w_address;
                    }
                } else if ($courier_row->address_type == '3') {
                    if ($courier_row->com_address == '1') {
                        $address = $party_row->address;
                    } else {
                        $address = $party_row->com_address_address;
                    }
                }
                $contact_no = $party_row->phone_no;
                $party_contact_row = $this->crud->get_all_with_where('contact_person', 'priority', 'ASC', array('party_id' => $courier_row->party_id));
                $contact_person_name = '';
                $contact_person_no = '';
                if (!empty($party_contact_row)) {
                    $party_contact_row = $party_contact_row[0];
                    $contact_person_name = $party_contact_row->name;
                    $contact_person_no = $party_contact_row->mobile_no;
                }
            } else {
                $party_name = $courier_row->party_name;
                $address = $courier_row->party_address;
                $contact_no = $courier_row->party_contact_no;
                $contact_person_name = $courier_row->contact_person_name;
                $contact_person_no = $courier_row->contact_person_no;
            }
//            echo '<pre>'; print_r($party_row); exit;
            $html = '<div align="left" style="width: 500px; margin: 0 auto;">'
                . '<h3 align="left" style="margin-bottom: 0px">'.$party_name.'</h3>'
                . '<table style="margin-bottom: -20px;"><tr><td valign="top">Address : </td><td>'.nl2br($address).'</td></tr></table><br/>'
                . 'Contact Number : '.$contact_no.'<br/>'
                . 'Contact Person : '.$contact_person_name.' - '.$contact_person_no.'<br/>'
                . '</div>';
            $this->load->library('m_pdf');
            $param = "'utf-8','A4'";
            $pdf = $this->m_pdf->load($param);
            $pdf->SetTitle('Courier');
            $margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
            $margin_left = $margin_company[0]->margin_left;
            $margin_right = $margin_company[0]->margin_right;
            $margin_top = $margin_company[0]->margin_top;
            $margin_bottom = $margin_company[0]->margin_bottom;
            $pdf->AddPage(
                'P', //orientation
                '', //type
                '', //resetpagenum
                '', //pagenumstyle
                '', //suppress
                '30px', //margin-left
                $margin_right, //margin-right
                $margin_top, //margin-top
                $margin_bottom, //margin-bottom
                0, //margin-header
                0 //margin-footer
            );
            $pdfFilePath = 'courier_'.$courier_id.".pdf";
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
        } else {
            redirect('courier/courier_list');
        }
	}

}
