<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Sales_order extends CI_Controller {

    protected $staff_id = 1;

    function __construct() {
        parent::__construct();
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
        $this->load->model('AppModel', 'app_model');
        $this->load->model('Crud', 'crud');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    function get_quotation() {
        $quotation_id = $this->input->get_post("id");
        $quotation_data = $this->crud->get_row_by_id('quotations', array('id' => $quotation_id));
        $quotation_data = $quotation_data[0];
        $quotation_data->party_type_1 = $this->crud->get_id_by_val('party', 'party_type_1', 'party_id', $quotation_data->party_id);
        $quotation_data->enquiry_date = $this->crud->get_id_by_val('inquiry', 'inquiry_date', 'inquiry_id', $quotation_data->enquiry_id);
        $quotation_data->enquiry_date = substr($quotation_data->enquiry_date, 8, 2) . '-' . substr($quotation_data->enquiry_date, 5, 2) . '-' . substr($quotation_data->enquiry_date, 0, 4);
        $data['quotation_data'] = $quotation_data;
        $state_id = $this->crud->get_column_value_by_id('party', 'state_id', array('party_id' => $quotation_data->party_id));
        $lineitems = array();
        $where = array('quotation_id' => $quotation_id);
        $quotation_lineitems = $this->crud->get_row_by_id('quotation_items', $where);
        foreach ($quotation_lineitems as $quotation_lineitem) {
            $quotation_lineitem->disc_per = !empty($quotation_lineitem->disc_per) ? $quotation_lineitem->disc_per : '0';
            $quotation_lineitem->disc_value = !empty($quotation_lineitem->disc_value) ? $quotation_lineitem->disc_value : '0';
            $quotation_lineitem->quantity = !empty($quotation_lineitem->quantity) ? $quotation_lineitem->quantity : '0';
            $quotation_lineitem->item_rate = !empty($quotation_lineitem->rate) ? $quotation_lineitem->rate : '0';

            if ($quotation_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
                if ($state_id == GUJARAT_STATE_ID) {
                    $quotation_lineitem->igst = 0;
                    $cgst = $this->crud->get_column_value_by_id('items', 'cgst', array('id' => $quotation_lineitem->item_id));
                    $sgst = $this->crud->get_column_value_by_id('items', 'sgst', array('id' => $quotation_lineitem->item_id));
                    $quotation_lineitem->cgst = !empty($cgst) ? $cgst : '0';
                    $quotation_lineitem->sgst = !empty($sgst) ? $sgst : '0';
                } else {
                    $igst = $this->crud->get_column_value_by_id('items', 'igst', array('id' => $quotation_lineitem->item_id));
                    $quotation_lineitem->igst = !empty($igst) ? $igst : '0';
                    $quotation_lineitem->cgst = 0;
                    $quotation_lineitem->sgst = 0;
                }
            } else {
                $quotation_lineitem->igst = 0;
                $quotation_lineitem->cgst = 0;
                $quotation_lineitem->sgst = 0;
            }

            $pure_amount = $quotation_lineitem->quantity * $quotation_lineitem->item_rate;

            //$discount_amount = $pure_amount * $quotation_lineitem->disc_per / 100;
            $discount_amount = $quotation_lineitem->disc_value;
            $taxable_amount = $pure_amount - $discount_amount;
            $igst_amount = $taxable_amount * $quotation_lineitem->igst / 100;
            $cgst_amount = $taxable_amount * $quotation_lineitem->cgst / 100;
            $sgst_amount = $taxable_amount * $quotation_lineitem->sgst / 100;
            $net_amount = $taxable_amount + $igst_amount + $cgst_amount + $sgst_amount;

            $quotation_lineitem->igst_amount = $igst_amount;
            $quotation_lineitem->cgst_amount = $cgst_amount;
            $quotation_lineitem->sgst_amount = $sgst_amount;
            $quotation_lineitem->net_amount = $net_amount;
            $lineitems[] = $quotation_lineitem;
        }
        $data['quotation_lineitems'] = $lineitems;
        echo json_encode($data);
        exit;
    }

    function quotation_datatable_for_sales_order_create() {
        $requestData = $_REQUEST;
        $config['select'] = 'q.id as quotation_id,q.quotation_no,q.updated_at,q.quotation_date,q.quotation_status_id,q.rev,p.party_name,qs.quotation_status,city.city,state.state,country.country';
        $config['table'] = 'quotations q';
        $config['column_order'] = array('q.updated_at', 'q.id', null, 'q.quotation_date', 'q.party_name', 'qs.quotation_status');
        $config['column_search'] = array('q.quotation_no', 'DATE_FORMAT(q.quotation_date,"%d-%m-%Y")', 'p.party_name', 'qs.quotation_status');
        $config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = q.party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'quotation_status qs', 'join_by' => 'qs.id = q.quotation_status_id', 'join_type' => 'left');
        $config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => 0);
        $config['wheres'][] = array('column_name' => 'LOWER(qs.quotation_status)', 'column_value' => 'hot');

        if (!empty($_POST['order_for']) && $_POST['order_for'] == 'export') {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        } else if (!empty($_POST['order_for']) && $_POST['order_for'] == 'domestic') {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
        }

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(QUOTATION_MANAGEMENT_USER, "allow") == 1) {
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            }
        } else {
            $config['custom_where'] = '( p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL )';
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            } else {
                
            }
        }

        $config['order'] = array('q.updated_at' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        foreach ($list as $quotation_data) {
            $row = array();
            $row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->updated_at . '</a>';
            $row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->quotation_no . '</a>';
            $row[] = !empty($quotation_data->rev) ? $quotation_data->rev . '  &nbsp; &nbsp;' : '1 &nbsp; &nbsp;';
            $row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . date('d-m-Y', strtotime($quotation_data->quotation_date)) . '</a>';
            $row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->party_name . '</a>';
            $row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->city . '</a>';
            $row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->state . '</a>';
            $row[] = '<a href="javascript:void(0);" class="quotation_row" data-quotation_id="' . $quotation_data->quotation_id . '">' . $quotation_data->country . '</a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    /**
     *    Sales Order
     */
    function add($sales_order_id = '') {
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $data = array();
        $user_ids = $this->app_model->getUserQuoteReminderIDS($this->staff_id);
		$users = array();
		if(!empty($user_ids)){
			$users = $this->crud->getFromSQL("SELECT `staff_id`, `name` FROM `staff` WHERE `staff_id` IN (".implode(',', $user_ids).")");
		}
        if (!empty($sales_order_id)) {
            if ($this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "edit")) {
                $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $sales_order_id));
                if (empty($sales_order_data)) {
                    redirect("sales_order/order_list");
                    exit;
                }
                $sales_order_data = $sales_order_data[0];
                $login_data = $this->crud->get_row_by_id('sales_order_logins', array('sales_order_id' => $sales_order_id));
                $login_data = $login_data[0];
                $login_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->created_by_id));
                $login_data->created_at = substr($login_data->created_at, 8, 2) . '-' . substr($login_data->created_at, 5, 2) . '-' . substr($login_data->created_at, 0, 4);
                $login_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->last_modified_by_id));
                $login_data->updated_at = substr($login_data->updated_at, 8, 2) . '-' . substr($login_data->updated_at, 5, 2) . '-' . substr($login_data->updated_at, 0, 4);
                $login_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->approved_by_id));
                $login_data->approved_at = substr($login_data->approved_date_time, 8, 2) . '-' . substr($login_data->approved_date_time, 5, 2) . '-' . substr($login_data->approved_date_time, 0, 4);
                $data = array(
                    'users' => $users,
                    'sales_order_data' => $sales_order_data,
                    'login_data' => $login_data,
                    'party_contact_person' => $this->crud->get_contact_person_by_party($sales_order_data->sales_to_party_id),
                    'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
                    'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
                    'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
                    'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
                    'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
                    'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
                    'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
                    'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
                    'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
                    'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
                );
                if (!empty($data['sales_order_data']->kind_attn_id)) {
                    $data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['sales_order_data']->kind_attn_id));
                    if (!empty($data['contact_person'])) {
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if (!empty($designation)) {
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if (!empty($department)) {
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
                }
                $data['sales_order_data']->not_any_dispatched = 0;
                $lineitems = array();
                $where = array('sales_order_id' => $sales_order_id);
                $sales_order_lineitems = $this->crud->get_row_by_id('sales_order_items', $where);
                foreach ($sales_order_lineitems as $sales_order_lineitem) {
                    $sales_order_lineitem->disc_per = !empty($sales_order_lineitem->disc_per) ? $sales_order_lineitem->disc_per : '0';
                    $sales_order_lineitem->disc_value = !empty($sales_order_lineitem->disc_value) ? $sales_order_lineitem->disc_value : '0';
                    $sales_order_lineitem->quantity = !empty($sales_order_lineitem->quantity) ? $sales_order_lineitem->quantity : '0';
                    $sales_order_lineitem->item_rate = !empty($sales_order_lineitem->rate) ? $sales_order_lineitem->rate : '0';

                    $sales_order_lineitem->igst = !empty($sales_order_lineitem->igst) ? $sales_order_lineitem->igst : '0';
                    $sales_order_lineitem->cgst = !empty($sales_order_lineitem->cgst) ? $sales_order_lineitem->cgst : '0';
                    $sales_order_lineitem->sgst = !empty($sales_order_lineitem->sgst) ? $sales_order_lineitem->sgst : '0';

                    $pure_amount = $sales_order_lineitem->quantity * $sales_order_lineitem->item_rate;

                    //$discount_amount = $pure_amount * $sales_order_lineitem->disc_per / 100;
                    $discount_amount = $sales_order_lineitem->disc_value;
                    $taxable_amount = $pure_amount - $discount_amount;
                    $igst_amount = $taxable_amount * $sales_order_lineitem->igst / 100;
                    $cgst_amount = $taxable_amount * $sales_order_lineitem->cgst / 100;
                    $sgst_amount = $taxable_amount * $sales_order_lineitem->sgst / 100;
                    $net_amount = $taxable_amount + $igst_amount + $cgst_amount + $sgst_amount;

                    $sales_order_lineitem->igst_amount = $igst_amount;
                    $sales_order_lineitem->cgst_amount = $cgst_amount;
                    $sales_order_lineitem->sgst_amount = $sgst_amount;
                    $sales_order_lineitem->net_amount = $net_amount;
                    $lineitems[] = json_encode($sales_order_lineitem);

                    if ($sales_order_lineitem->dispatched_qty != 0) {
                        $data['sales_order_data']->not_any_dispatched = 1;
                    }
                }
                $data['sales_order_lineitems'] = implode(',', $lineitems);
                $data['challan_id'] = $this->crud->get_id_by_val('challans', 'id', 'sales_order_id', $sales_order_id);
                $sales_order_reminders = array();
                $where = array('order_id' => $sales_order_id);
                $sales_order_reminder_data = $this->crud->get_row_by_id('followup_order_history', $where);
                foreach ($sales_order_reminder_data as $sales_order_reminder) {
                    $sales_order_reminder->reminder_onlydate = '';
					$sales_order_reminder->reminder_onlytime = '';
					if(strtotime($sales_order_reminder->reminder_date) > 0){
						$sales_order_reminder->reminder_date = date('d-m-Y H:i:s', strtotime($sales_order_reminder->reminder_date));
						$sales_order_reminder->reminder_onlydate = date('d-m-Y', strtotime($sales_order_reminder->reminder_date));
						$sales_order_reminder->reminder_onlytime = date('H:i:s', strtotime($sales_order_reminder->reminder_date));
					}
					$sales_order_reminder->followup_onlydate = '';
					$sales_order_reminder->followup_onlytime = '';
					if(strtotime($sales_order_reminder->followup_date) > 0){
						$sales_order_reminder->followup_date = date('d-m-Y H:i:s', strtotime($sales_order_reminder->followup_date));
						$sales_order_reminder->followup_onlydate = date('d-m-Y', strtotime($sales_order_reminder->followup_date));
						$sales_order_reminder->followup_onlytime = date('H:i:s', strtotime($sales_order_reminder->followup_date));
					}
					$sales_order_reminder->history = stripslashes($sales_order_reminder->history);
					$sales_order_reminder->reminder_message = stripslashes($sales_order_reminder->reminder_message);
					$sales_order_reminders[] = json_encode($sales_order_reminder);
                    
                }
                $data['sales_order_reminder_data'] = implode(',', $sales_order_reminders);
                $data['quotation_reminder_data'] = $this->get_quotation_followup_history($sales_order_data->quotation_id);
                $inquiry_id = $this->crud->get_id_by_val('quotations', 'enquiry_id', 'id', $sales_order_data->quotation_id);
                $data['enquiry_followup_history_data'] = $this->get_enquiry_followup_history($inquiry_id);
//                echo '<pre>';print_r($data); exit;
                set_page('sales/order/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            if ($this->applib->have_access_role(SALES_ORDER_MODULE_ID, "add")) {
                $data = array(
                    'users' => $users,
                    'sales_order_no' => 'SO' . $this->get_new_sales_order_no(),
                    'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
                    'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
                    'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
                    'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
                    'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
                    'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
                    'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
                    'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
                    'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
                    'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
                );
                set_page('sales/order/add', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }

    function save_sales_order() {
        $post_data = $this->input->post();
//        echo '<pre>'; print_r($post_data);exit;
        // check party validations
        $party_id = $post_data['party']['party_id'];
        if ($party_id == "" || $party_id == "0") {
            echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
            exit;
        }
        //		$result = $this->applib->check_party_validations($party_id);
        //		if ($result['status'] == 0) {
        //			echo json_encode(array("success" => 'false', 'msg' => $result['msg']));
        //			exit;
        //		}
        //        
        //        if (isset($post_data['sales_order_data']['kind_attn_id']) && trim($post_data['sales_order_data']['kind_attn_id']) != '' && !empty(trim($post_data['sales_order_data']['kind_attn_id']))) {
        //            $contact_person = $post_data['contact_person'];
        //            $contact_person_update['mobile_no'] = $contact_person['contact_person_mobile_no'];
        //            $contact_person_update['email'] = $contact_person['contact_person_email_id'];
        //            $this->crud->update('contact_person', $contact_person_update, array('contact_person_id' => $post_data['sales_order_data']['kind_attn_id']));
        //        }
//        echo '<pre>';print_r($post_data); exit;
        $line_items_data = json_decode('[' . $post_data['line_items_data'] . ']');
        //		print_r($line_items_data); exit;

        $sales_order_reminder_data = json_decode('[' . $post_data['sales_order_reminder_data'] . ']');
        //		print_r($sales_order_reminder_data); exit;

        $send_sms = 0;
        if (isset($post_data['sales_order_data']['send_sms'])) {
            $send_sms = 1;
            unset($post_data['sales_order_data']['send_sms']);
        }

        /* --------- Convert Date as Mysql Format ------------- */
        $post_data['sales_order_data']['sales_order_date'] = date("Y-m-d", strtotime($post_data['sales_order_data']['sales_order_date']));
        $post_data['sales_order_data']['committed_date'] = date("Y-m-d", strtotime($post_data['sales_order_data']['committed_date']));
        $post_data['sales_order_data']['po_date'] = date("Y-m-d", strtotime($post_data['sales_order_data']['po_date']));

        if (!empty($post_data['sales_order_data']['received_payment_date']) && isset($post_data['sales_order_data']['received_payment_date'])) {
            $post_data['sales_order_data']['received_payment_date'] = date('Y-m-d', strtotime($post_data['sales_order_data']['received_payment_date']));
        } else {
            $post_data['sales_order_data']['received_payment_date'] = null;
        }

        if (!empty($post_data['sales_order_data']['mach_deli_min_weeks_date']) && isset($post_data['sales_order_data']['mach_deli_min_weeks_date'])) {
            $post_data['sales_order_data']['mach_deli_min_weeks_date'] = date('Y-m-d', strtotime($post_data['sales_order_data']['mach_deli_min_weeks_date']));
        } else {
            $post_data['sales_order_data']['mach_deli_min_weeks_date'] = null;
        }

        if (!empty($post_data['sales_order_data']['mach_deli_max_weeks_date']) && isset($post_data['sales_order_data']['mach_deli_max_weeks_date'])) {
            $post_data['sales_order_data']['mach_deli_max_weeks_date'] = date('Y-m-d', strtotime($post_data['sales_order_data']['mach_deli_max_weeks_date']));
        } else {
            $post_data['sales_order_data']['mach_deli_max_weeks_date'] = null;
        }

        $post_data['sales_order_data']['kind_attn_id'] = !empty($post_data['sales_order_data']['kind_attn_id']) ? $post_data['sales_order_data']['kind_attn_id'] : NULL;
        $post_data['sales_order_data']['branch_id'] = !empty($post_data['sales_order_data']['branch_id']) ? $post_data['sales_order_data']['branch_id'] : NULL;
        $post_data['sales_order_data']['agent_id'] = !empty($post_data['sales_order_data']['agent_id']) ? $post_data['sales_order_data']['agent_id'] : NULL;
        $post_data['sales_order_data']['sales_id'] = !empty($post_data['sales_order_data']['sales_id']) ? $post_data['sales_order_data']['sales_id'] : NULL;
        $post_data['sales_order_data']['currency_id'] = !empty($post_data['sales_order_data']['currency_id']) ? $post_data['sales_order_data']['currency_id'] : NULL;
        $post_data['sales_order_data']['sales_order_pref_id'] = !empty($post_data['sales_order_data']['sales_order_pref_id']) ? $post_data['sales_order_data']['sales_order_pref_id'] : NULL;
        $post_data['sales_order_data']['sales_to_party_id'] = $party_id;

        if (isset($post_data['sales_order_data']['sales_order_id']) && !empty($post_data['sales_order_data']['sales_order_id'])) {

            $sales_order_no = $post_data['sales_order_data']['sales_order_no'];
            $sales_order_result = $this->crud->get_id_by_val('sales_order', 'id', 'sales_order_no', $sales_order_no);
            if (!empty($sales_order_result) && $sales_order_result != $post_data['sales_order_data']['sales_order_id']) {
                echo json_encode(array("success" => 'false', 'msg' => 'Sales Order No. Already Exist!'));
                exit;
            }

            $sales_order_id = $post_data['sales_order_data']['sales_order_id'];
            if (isset($post_data['sales_order_data']['sales_order_id']))
                unset($post_data['sales_order_data']['sales_order_id']);

            $this->db->where('id', $sales_order_id);
            $result = $this->db->update('sales_order', $post_data['sales_order_data']);
            if ($result) {

                $login_data['last_modified_by_id'] = $this->staff_id;
                $login_data['modified_date_time'] = $this->now_time;
                $login_data['updated_at'] = $this->now_time;
                $this->crud->update('sales_order_logins', $login_data, array('sales_order_id' => $sales_order_id));

                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Sales Order Updated Successfully');
                $where_array = array("sales_order_id" => $sales_order_id);
                $this->crud->delete("sales_order_items", $where_array);
                $is_dispatched_arr = array();
                foreach ($line_items_data[0] as $lineitem) {
                    $add_lineitem = array();
                    $add_lineitem['sales_order_id'] = $sales_order_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['item_code'] = $lineitem->item_code;
                    $add_lineitem['item_name'] = $lineitem->item_name;
                    $add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
                    $add_lineitem['item_description'] = $lineitem->item_description;
                    if (isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)) {
                        $add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
                    }
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['currency_id'] = $lineitem->currency_id;
                    $add_lineitem['rate'] = $lineitem->item_rate;
                    $add_lineitem['amount'] = $lineitem->amount;
                    $add_lineitem['disc_per'] = !empty($lineitem->disc_per) ? $lineitem->disc_per : '0';
                    $add_lineitem['disc_value'] = $lineitem->disc_value;
                    $add_lineitem['igst'] = $lineitem->igst;
                    $add_lineitem['cgst'] = $lineitem->cgst;
                    $add_lineitem['sgst'] = $lineitem->sgst;
                    $add_lineitem['igst_amount'] = $lineitem->igst_amount;
                    $add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
                    $add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    $add_lineitem['dispatched_qty'] = $lineitem->dispatched_qty;
                    $add_lineitem['created_at'] = $this->now_time;
                    $this->crud->insert('sales_order_items', $add_lineitem);
                    if ($add_lineitem['quantity'] == $add_lineitem['dispatched_qty']) {
                        $is_dispatched_arr[] = 1;
                    }
                }
                $is_dispatched_count = count($is_dispatched_arr);
                $line_items_count = count($line_items_data[0]);
                if ($is_dispatched_count == $line_items_count) {
                    $this->crud->update('sales_order', array("is_dispatched" => DISPATCHED_ORDER_ID), array('id' => $sales_order_id));
                } else if (isset($post_data['sales_order_data']['is_dispatched']) && $post_data['sales_order_data']['is_dispatched'] == CANCELED_ORDER_ID) {
                    $this->crud->update('sales_order', array("is_dispatched" => CANCELED_ORDER_ID), array('id' => $sales_order_id));
                } else if (isset($post_data['sales_order_data']['is_dispatched']) && $post_data['sales_order_data']['is_dispatched'] == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) {
                    $this->crud->update('sales_order', array("is_dispatched" => CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID), array('id' => $sales_order_id));
                } else if (isset($post_data['sales_order_data']['is_dispatched']) && $post_data['sales_order_data']['is_dispatched'] == PENDING_DISPATCH_ORDER_ID) {
                    $this->crud->update('sales_order', array("is_dispatched" => PENDING_DISPATCH_ORDER_ID), array('id' => $sales_order_id));
                }

                if(isset($post_data['deleted_sales_order_reminder_id'])){
					$this->db->where_in('id', $post_data['deleted_sales_order_reminder_id']);
					$this->db->delete('followup_order_history');
				}
				$add_followup_order_history_arr = array();
				$edit_followup_order_history_arr = array();
				foreach($sales_order_reminder_data[0] as $key => $line){
					if(isset($line->reminder_action) && $line->reminder_action == 'add'){
						$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
						$followup_onlytime = '00:00:00';
						if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
							$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
						}
						$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
						$reminder_onlytime = '00:00:00';
						if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
							$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
						}
						$add_followup_order_history = array(
							'order_id' => $sales_order_id,
							'followup_category_id' => $line->followup_category_id,
							'followup_date' => $followup_onlydate.' '.$followup_onlytime,
							'reminder_category_id' => $line->reminder_category_id,
							'reminder_date' => $reminder_onlydate.' '.$reminder_onlytime,
							'history' => addslashes($line->history),
							'reminder_message' => addslashes($line->reminder_message),
							'followup_by' => $line->followup_by,
							'assigned_to' => $line->assigned_to,
						);
						if(isset($line->followup_at) && !empty($line->followup_at)){
							$add_followup_order_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
						}
						$add_followup_order_history['updated_by'] = $line->updated_by;
						if(isset($line->updated_at) && !empty($line->updated_at)){
							$add_followup_order_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
						}
						if(isset($line->is_notified) && $line->is_notified == 1){
							$add_followup_order_history['is_notified'] = 1;
						}
						array_push($add_followup_order_history_arr, $add_followup_order_history);
					}
					$edit_followup_order_history = array();
					if(isset($line->reminder_action) && $line->reminder_action == 'edit'){
						$edit_followup_order_history['id'] = $line->id;
                        $edit_followup_order_history['order_id'] = $sales_order_id;
						$edit_followup_order_history['followup_category_id'] = $line->followup_category_id;
						$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
						$followup_onlytime = '00:00:00';
						if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
							$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
						}
						$edit_followup_order_history['followup_date'] = $followup_onlydate.' '.$followup_onlytime;
						$edit_followup_order_history['reminder_category_id'] = $line->reminder_category_id;
						$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
						$reminder_onlytime = '00:00:00';
						if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
							$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
						}
						$edit_followup_order_history['reminder_date'] = $reminder_onlydate.' '.$reminder_onlytime;
						$edit_followup_order_history['history'] = addslashes($line->history);
						$edit_followup_order_history['reminder_message'] = addslashes($line->reminder_message);
						$edit_followup_order_history['followup_by'] = $line->followup_by;
						$edit_followup_order_history['assigned_to'] = $line->assigned_to;

						if(isset($line->followup_at) && !empty($line->followup_at)){
							$edit_followup_order_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
						}
						$edit_followup_order_history['updated_by'] = $line->updated_by;
						if(isset($line->updated_at) && !empty($line->updated_at)){
							$edit_followup_order_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
						}
						if(isset($line->is_notified) && $line->is_notified == 1){
							$edit_followup_order_history['is_notified'] = 1;
						}
						if(empty($line->id)){
							unset($line->id);
							unset($edit_followup_order_history['id']);
							array_push($add_followup_order_history_arr,$edit_followup_order_history);
						}else{
							array_push($edit_followup_order_history_arr, $edit_followup_order_history);
						}
					}
				}
				if(!empty($add_followup_order_history_arr)){
					$this->db->insert_batch('followup_order_history', $add_followup_order_history_arr);
				}
				if(!empty($edit_followup_order_history_arr)){
					$this->db->update_batch('followup_order_history', array_filter($edit_followup_order_history_arr), 'id');
				}
            }
        } else {
            $post_data['sales_order_data']['sales_order_no'] = 'SO' . $this->get_new_sales_order_no();
            $dataToInsert = $post_data['sales_order_data'];
            $dataToInsert['created_at'] = $this->now_time;
            $dataToInsert['created_by'] = $this->staff_id;
            // Unset quotation_id from Inquiry Data
            if (isset($dataToInsert['sales_order_id']))
                unset($dataToInsert['sales_order_id']);

            $result = $this->db->insert('sales_order', $dataToInsert);
            $sales_order_id = $this->db->insert_id();
            if ($result) {

                $login_data['sales_order_id'] = $sales_order_id;
                $login_data['created_by_id'] = $this->staff_id;
                $login_data['created_date_time'] = $this->now_time;
                $login_data['created_at'] = $this->now_time;
                $login_data['last_modified_by_id'] = $this->staff_id;
                $login_data['modified_date_time'] = $this->now_time;
                $login_data['updated_at'] = $this->now_time;
                $this->crud->insert('sales_order_logins', $login_data);

                /* -- Update Qoutation Status -- */
                $quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $post_data['sales_order_data']['quotation_id']);
                //print_r($quotation_no);exit;
                if (!empty($quotation_no)) {
                    $this->crud->update('quotations', array('quotation_status_id' => QUOTATION_STATUS_ID_ON_ORDER_CREATE), array('quotation_no' => $quotation_no));
                }

                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Sales Order Added Successfully');
                foreach ($line_items_data[0] as $lineitem) {
                    $add_lineitem = array();
                    $add_lineitem['sales_order_id'] = $sales_order_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['item_code'] = $lineitem->item_code;
                    $add_lineitem['item_name'] = $lineitem->item_name;
                    $add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
                    $add_lineitem['item_description'] = $lineitem->item_description;
                    if (isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)) {
                        $add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
                    }
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['currency_id'] = $lineitem->currency_id;
                    $add_lineitem['rate'] = $lineitem->item_rate;
                    $add_lineitem['amount'] = $lineitem->amount;
                    $add_lineitem['disc_per'] = !empty($lineitem->disc_per) ? $lineitem->disc_per : '0';
                    $add_lineitem['disc_value'] = $lineitem->disc_value;
                    $add_lineitem['igst'] = $lineitem->igst;
                    $add_lineitem['cgst'] = $lineitem->cgst;
                    $add_lineitem['sgst'] = $lineitem->sgst;
                    $add_lineitem['igst_amount'] = $lineitem->igst_amount;
                    $add_lineitem['cgst_amount'] = $lineitem->cgst_amount;
                    $add_lineitem['sgst_amount'] = $lineitem->sgst_amount;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    if (isset($lineitem->dispatched_qty) && !empty($lineitem->dispatched_qty)) {
                        $add_lineitem['dispatched_qty'] = $lineitem->dispatched_qty;
                    }
                    $add_lineitem['created_at'] = $this->now_time;
                    $this->crud->insert('sales_order_items', $add_lineitem);
                }
                $add_followup_order_history_arr = array();
				foreach($sales_order_reminder_data[0] as $line){
					$add_followup_order_history = array();
                    $add_followup_order_history['order_id'] = $sales_order_id;
					$add_followup_order_history['followup_category_id'] = $line->followup_category_id;
					$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
					$followup_onlytime = '00:00:00';
					if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
						$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
					}
					$add_followup_order_history['followup_date'] = $followup_onlydate.' '.$followup_onlytime;
					$add_followup_order_history['reminder_category_id'] = $line->reminder_category_id;
					$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
					$reminder_onlytime = '00:00:00';
					if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
						$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
					}
					$add_followup_order_history['reminder_date'] = $reminder_onlydate.' '.$reminder_onlytime;
					$add_followup_order_history['history'] = addslashes($line->history);
					$add_followup_order_history['reminder_message'] = addslashes($line->reminder_message);
					$add_followup_order_history['followup_by'] = $line->followup_by;
					$add_followup_order_history['assigned_to'] = $line->assigned_to;

					if(isset($line->followup_at) && !empty($line->followup_at)){
						$add_followup_order_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
					}
					$add_followup_order_history['updated_by'] = $line->updated_by;
					if(isset($line->updated_at) && !empty($line->updated_at)){
						$add_followup_order_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
					}
					array_push($add_followup_order_history_arr, $add_followup_order_history);
				}
                if(!empty($add_followup_order_history_arr)){
                    $this->db->insert_batch('followup_order_history', array_filter($add_followup_order_history_arr));
                }
                $this->sales_order_notification();
            }
        }
        if ($send_sms == 1) {
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['sales_order_data']['sales_to_party_id']);
            $this->applib->send_sms('order', $sales_order_id, $party_phone_no, SEND_SALES_ORDER_SMS_ID);
        }
        print json_encode($return);
        exit;
    }

    function add_export($sales_order_id = '') {
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $data = array();
        $user_ids = $this->app_model->getUserQuoteReminderIDS($this->staff_id);
		$users = array();
		if(!empty($user_ids)){
			$users = $this->crud->getFromSQL("SELECT `staff_id`, `name` FROM `staff` WHERE `staff_id` IN (".implode(',', $user_ids).")");
		}
        if (!empty($sales_order_id)) {
            if ($this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "edit")) {
                $sales_order_data = $this->crud->get_row_by_id('sales_order', array('id' => $sales_order_id));
                if (empty($sales_order_data)) {
                    redirect("sales_order/order_list");
                    exit;
                }
                $sales_order_data = $sales_order_data[0];

                /* $sales_order_data->port_of_loading_country = $this->crud->get_id_by_val('country', 'country_id', 'country', $sales_order_data->port_of_loading_country);
                  $sales_order_data->port_of_discharge_country = $this->crud->get_id_by_val('country', 'country_id', 'country', $sales_order_data->port_of_discharge_country);
                  $sales_order_data->place_of_delivery_country = $this->crud->get_id_by_val('country', 'country_id', 'country', $sales_order_data->place_of_delivery_country); */

                $login_data = $this->crud->get_row_by_id('sales_order_logins', array('sales_order_id' => $sales_order_id));
                $login_data = $login_data[0];
                $login_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->created_by_id));
                $login_data->created_at = substr($login_data->created_at, 8, 2) . '-' . substr($login_data->created_at, 5, 2) . '-' . substr($login_data->created_at, 0, 4);
                $login_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->last_modified_by_id));
                $login_data->updated_at = substr($login_data->updated_at, 8, 2) . '-' . substr($login_data->updated_at, 5, 2) . '-' . substr($login_data->updated_at, 0, 4);
                $login_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $login_data->approved_by_id));
                $login_data->approved_at = substr($login_data->approved_date_time, 8, 2) . '-' . substr($login_data->approved_date_time, 5, 2) . '-' . substr($login_data->approved_date_time, 0, 4);
                $data = array(
                    'users' => $users,
                    'sales_order_data' => $sales_order_data,
                    'login_data' => $login_data,
                    'party_contact_person' => $this->crud->get_contact_person_by_party($sales_order_data->sales_to_party_id),
                    'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
                    'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
                    'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
                    'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
                    'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
                    'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
                    'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
                    'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
                    'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
                    'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
                    'company_banks' => $this->crud->get_all_with_where('company_banks', 'id', 'ASC', array('for_export' => '1')),
                );
                if (!empty($data['sales_order_data']->kind_attn_id)) {
                    $data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['sales_order_data']->kind_attn_id));
                    if (!empty($data['contact_person'])) {
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if (!empty($designation)) {
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if (!empty($department)) {
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
                }
                $data['sales_order_data']->not_any_dispatched = 0;
                $lineitems = array();
                $where = array('sales_order_id' => $sales_order_id);
                $sales_order_lineitems = $this->crud->get_row_by_id('sales_order_items', $where);
                foreach ($sales_order_lineitems as $sales_order_lineitem) {
                    $sales_order_lineitem->disc_per = !empty($sales_order_lineitem->disc_per) ? $sales_order_lineitem->disc_per : '0';
                    $sales_order_lineitem->disc_value = !empty($sales_order_lineitem->disc_value) ? $sales_order_lineitem->disc_value : '0';
                    $sales_order_lineitem->quantity = !empty($sales_order_lineitem->quantity) ? $sales_order_lineitem->quantity : '0';
                    $sales_order_lineitem->item_rate = !empty($sales_order_lineitem->rate) ? $sales_order_lineitem->rate : '0';

                    $pure_amount = $sales_order_lineitem->quantity * $sales_order_lineitem->item_rate;

                    //$discount_amount = $pure_amount * $sales_order_lineitem->disc_per / 100;
                    $discount_amount = $sales_order_lineitem->disc_value;
                    $taxable_amount = $pure_amount - $discount_amount;
                    $net_amount = $taxable_amount;

                    $sales_order_lineitem->net_amount = $net_amount;
                    $lineitems[] = json_encode($sales_order_lineitem);

                    if ($sales_order_lineitem->dispatched_qty != 0) {
                        $data['sales_order_data']->not_any_dispatched = 1;
                    }
                }
                $data['sales_order_lineitems'] = implode(',', $lineitems);
                $data['challan_id'] = $this->crud->get_id_by_val('challans', 'id', 'sales_order_id', $sales_order_id);
                $sales_order_reminders = array();
                $where = array('order_id' => $sales_order_id);
                $sales_order_reminder_data = $this->crud->get_row_by_id('followup_order_history', $where);
                foreach ($sales_order_reminder_data as $sales_order_reminder) {
                    $sales_order_reminder->reminder_onlydate = '';
					$sales_order_reminder->reminder_onlytime = '';
					if(strtotime($sales_order_reminder->reminder_date) > 0){
						$sales_order_reminder->reminder_date = date('d-m-Y H:i:s', strtotime($sales_order_reminder->reminder_date));
						$sales_order_reminder->reminder_onlydate = date('d-m-Y', strtotime($sales_order_reminder->reminder_date));
						$sales_order_reminder->reminder_onlytime = date('H:i:s', strtotime($sales_order_reminder->reminder_date));
					}
					$sales_order_reminder->followup_onlydate = '';
					$sales_order_reminder->followup_onlytime = '';
					if(strtotime($sales_order_reminder->followup_date) > 0){
						$sales_order_reminder->followup_date = date('d-m-Y H:i:s', strtotime($sales_order_reminder->followup_date));
						$sales_order_reminder->followup_onlydate = date('d-m-Y', strtotime($sales_order_reminder->followup_date));
						$sales_order_reminder->followup_onlytime = date('H:i:s', strtotime($sales_order_reminder->followup_date));
					}
					$sales_order_reminder->history = stripslashes($sales_order_reminder->history);
					$sales_order_reminder->reminder_message = stripslashes($sales_order_reminder->reminder_message);
					$sales_order_reminders[] = json_encode($sales_order_reminder);
                    
                }
                $data['sales_order_reminder_data'] = implode(',', $sales_order_reminders);
                $data['quotation_reminder_data'] = $this->get_quotation_followup_history($sales_order_data->quotation_id);
                $inquiry_id = $this->crud->get_id_by_val('quotations', 'enquiry_id', 'id', $sales_order_data->quotation_id);
                $data['enquiry_followup_history_data'] = $this->get_enquiry_followup_history($inquiry_id);
                //                echo '<pre>';print_r($data); exit;
                set_page('sales/order/add_export', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        } else {
            if ($this->applib->have_access_role(SALES_ORDER_MODULE_ID, "add")) {
                $data = array(
                    'users' => $users,
                    'sales_order_no' => 'SO' . $this->get_new_sales_order_no(),
                    'item_category' => $this->crud->get_all_records('item_category', 'id', 'asc'),
                    'sales_order_pref' => $this->crud->get_all_records('sales_order_pref', 'id', 'asc'),
                    'transportation_by' => $this->crud->get_all_records('transportation_by', 'id', 'asc'),
                    'foundation_drawing_required' => $this->crud->get_all_records('foundation_drawing_required', 'id', 'asc'),
                    'loading_by' => $this->crud->get_all_records('loading_by', 'id', 'asc'),
                    'inspection_required' => $this->crud->get_all_records('inspection_required', 'id', 'asc'),
                    'unloading_by' => $this->crud->get_all_records('unloading_by', 'id', 'asc'),
                    'erection_commissioning' => $this->crud->get_all_records('erection_commissioning', 'id', 'asc'),
                    'road_insurance_by' => $this->crud->get_all_records('road_insurance_by', 'id', 'asc'),
                    'for_required' => $this->crud->get_all_records('for_required', 'id', 'asc'),
                    'company_banks' => $this->crud->get_all_with_where('company_banks', 'id', 'ASC', array('for_export' => '1')),
                );
                set_page('sales/order/add_export', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }
    }

    function save_sales_order_export() {
        $post_data = $this->input->post();
        //echo '<pre>';print_r($post_data['sales_order_data']);exit;
        // check party validations
        $party_id = $post_data['party']['party_id'];
        if ($party_id == "" || $party_id == "0") {
            echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
            exit;
        }
        $line_items_data = json_decode('[' . $post_data['line_items_data'] . ']');
        //		print_r($line_items_data); exit;

        $sales_order_reminder_data = json_decode('[' . $post_data['sales_order_reminder_data'] . ']');
        //		print_r($sales_order_reminder_data); exit;

        $send_sms = 0;
        if (isset($post_data['sales_order_data']['send_sms'])) {
            $send_sms = 1;
            unset($post_data['sales_order_data']['send_sms']);
        }

        /* --------- Convert Date as Mysql Format ------------- */
        $post_data['sales_order_data']['sales_order_date'] = date("Y-m-d", strtotime($post_data['sales_order_data']['sales_order_date']));
        $post_data['sales_order_data']['committed_date'] = date("Y-m-d", strtotime($post_data['sales_order_data']['committed_date']));
        $post_data['sales_order_data']['po_date'] = date("Y-m-d", strtotime($post_data['sales_order_data']['po_date']));

        if (!empty($post_data['sales_order_data']['received_payment_date']) && isset($post_data['sales_order_data']['received_payment_date'])) {
            $post_data['sales_order_data']['received_payment_date'] = date('Y-m-d', strtotime($post_data['sales_order_data']['received_payment_date']));
        } else {
            $post_data['sales_order_data']['received_payment_date'] = null;
        }

        if (!empty($post_data['sales_order_data']['mach_deli_min_weeks_date']) && isset($post_data['sales_order_data']['mach_deli_min_weeks_date'])) {
            $post_data['sales_order_data']['mach_deli_min_weeks_date'] = date('Y-m-d', strtotime($post_data['sales_order_data']['mach_deli_min_weeks_date']));
        } else {
            $post_data['sales_order_data']['mach_deli_min_weeks_date'] = null;
        }

        if (!empty($post_data['sales_order_data']['mach_deli_max_weeks_date']) && isset($post_data['sales_order_data']['mach_deli_max_weeks_date'])) {
            $post_data['sales_order_data']['mach_deli_max_weeks_date'] = date('Y-m-d', strtotime($post_data['sales_order_data']['mach_deli_max_weeks_date']));
        } else {
            $post_data['sales_order_data']['mach_deli_max_weeks_date'] = null;
        }

        $post_data['sales_order_data']['kind_attn_id'] = !empty($post_data['sales_order_data']['kind_attn_id']) ? $post_data['sales_order_data']['kind_attn_id'] : NULL;
        $post_data['sales_order_data']['branch_id'] = !empty($post_data['sales_order_data']['branch_id']) ? $post_data['sales_order_data']['branch_id'] : NULL;
        $post_data['sales_order_data']['agent_id'] = !empty($post_data['sales_order_data']['agent_id']) ? $post_data['sales_order_data']['agent_id'] : NULL;
        $post_data['sales_order_data']['sales_id'] = !empty($post_data['sales_order_data']['sales_id']) ? $post_data['sales_order_data']['sales_id'] : NULL;
        $post_data['sales_order_data']['currency_id'] = !empty($post_data['sales_order_data']['currency_id']) ? $post_data['sales_order_data']['currency_id'] : NULL;
        $post_data['sales_order_data']['bank_detail'] = !empty($post_data['sales_order_data']['bank_detail']) ? $post_data['sales_order_data']['bank_detail'] : NULL;
        $post_data['sales_order_data']['sales_order_pref_id'] = !empty($post_data['sales_order_data']['sales_order_pref_id']) ? $post_data['sales_order_data']['sales_order_pref_id'] : NULL;
        $post_data['sales_order_data']['delivery_through_id'] = !empty($post_data['sales_order_data']['delivery_through_id']) ? $post_data['sales_order_data']['delivery_through_id'] : NULL;

        $post_data['sales_order_data']['sales_to_party_id'] = $party_id;
        if (isset($post_data['sales_order_data']['port_of_loading_country_id']) && !empty($post_data['sales_order_data']['port_of_loading_country_id'])) {
            $post_data['sales_order_data']['port_of_loading_country'] = $this->crud->get_id_by_val('country', 'country', 'country_id', $post_data['sales_order_data']['port_of_loading_country_id']);
        } else {
            $post_data['sales_order_data']['port_of_loading_country_id'] = NULL;
            $post_data['sales_order_data']['port_of_loading_country'] = NULL;
        }
        if (isset($post_data['sales_order_data']['port_of_discharge_country_id']) && !empty($post_data['sales_order_data']['port_of_discharge_country_id'])) {
            $post_data['sales_order_data']['port_of_discharge_country'] = $this->crud->get_id_by_val('country', 'country', 'country_id', $post_data['sales_order_data']['port_of_discharge_country_id']);
        } else {
            $post_data['sales_order_data']['port_of_discharge_country_id'] = NULL;
            $post_data['sales_order_data']['port_of_discharge_country'] = NULL;
        }
        if (isset($post_data['sales_order_data']['place_of_delivery_country_id']) && !empty($post_data['sales_order_data']['place_of_delivery_country_id'])) {
            $post_data['sales_order_data']['place_of_delivery_country'] = $this->crud->get_id_by_val('country', 'country', 'country_id', $post_data['sales_order_data']['place_of_delivery_country_id']);
        } else {
            $post_data['sales_order_data']['place_of_delivery_country_id'] = NULL;
            $post_data['sales_order_data']['place_of_delivery_country'] = NULL;
        }

        if (isset($post_data['sales_order_data']['sales_order_id']) && !empty($post_data['sales_order_data']['sales_order_id'])) {

            $sales_order_no = $post_data['sales_order_data']['sales_order_no'];
            $sales_order_result = $this->crud->get_id_by_val('sales_order', 'id', 'sales_order_no', $sales_order_no);
            if (!empty($sales_order_result) && $sales_order_result != $post_data['sales_order_data']['sales_order_id']) {
                echo json_encode(array("success" => 'false', 'msg' => 'Sales Order No. Already Exist!'));
                exit;
            }

            $sales_order_id = $post_data['sales_order_data']['sales_order_id'];
            if (isset($post_data['sales_order_data']['sales_order_id']))
                unset($post_data['sales_order_data']['sales_order_id']);

            $this->db->where('id', $sales_order_id);
            $result = $this->db->update('sales_order', $post_data['sales_order_data']);
            if ($result) {

                $login_data['last_modified_by_id'] = $this->staff_id;
                $login_data['modified_date_time'] = $this->now_time;
                $login_data['updated_at'] = $this->now_time;
                $this->crud->update('sales_order_logins', $login_data, array('sales_order_id' => $sales_order_id));

                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Sales Order Updated Successfully');
                $where_array = array("sales_order_id" => $sales_order_id);
                $this->crud->delete("sales_order_items", $where_array);
                $is_dispatched_arr = array();
                foreach ($line_items_data[0] as $lineitem) {
                    $add_lineitem = array();
                    $add_lineitem['sales_order_id'] = $sales_order_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['item_code'] = $lineitem->item_code;
                    $add_lineitem['item_name'] = $lineitem->item_name;
                    $add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
                    $add_lineitem['item_description'] = $lineitem->item_description;
                    if (isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)) {
                        $add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
                    }
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['currency_id'] = $lineitem->currency_id;
                    $add_lineitem['rate'] = $lineitem->item_rate;
                    $add_lineitem['amount'] = $lineitem->amount;
                    $add_lineitem['disc_per'] = !empty($lineitem->disc_per) ? $lineitem->disc_per : '0';
                    $add_lineitem['disc_value'] = $lineitem->disc_value;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    $add_lineitem['dispatched_qty'] = $lineitem->dispatched_qty;
                    $add_lineitem['created_at'] = $this->now_time;
                    $this->crud->insert('sales_order_items', $add_lineitem);
                    if ($add_lineitem['quantity'] == $add_lineitem['dispatched_qty']) {
                        $is_dispatched_arr[] = 1;
                    }
                }
                $is_dispatched_count = count($is_dispatched_arr);
                $line_items_count = count($line_items_data[0]);
                if ($is_dispatched_count == $line_items_count) {
                    $this->crud->update('sales_order', array("is_dispatched" => DISPATCHED_ORDER_ID), array('id' => $sales_order_id));
                } else if (isset($post_data['sales_order_data']['is_dispatched']) && $post_data['sales_order_data']['is_dispatched'] == CANCELED_ORDER_ID) {
                    $this->crud->update('sales_order', array("is_dispatched" => CANCELED_ORDER_ID), array('id' => $sales_order_id));
                } else if (isset($post_data['sales_order_data']['is_dispatched']) && $post_data['sales_order_data']['is_dispatched'] == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) {
                    $this->crud->update('sales_order', array("is_dispatched" => CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID), array('id' => $sales_order_id));
                } else if (isset($post_data['sales_order_data']['is_dispatched']) && $post_data['sales_order_data']['is_dispatched'] == PENDING_DISPATCH_ORDER_ID) {
                    $this->crud->update('sales_order', array("is_dispatched" => PENDING_DISPATCH_ORDER_ID), array('id' => $sales_order_id));
                }

                if(isset($post_data['deleted_sales_order_reminder_id'])){
					$this->db->where_in('id', $post_data['deleted_sales_order_reminder_id']);
					$this->db->delete('followup_order_history');
				}
				$add_followup_order_history_arr = array();
				$edit_followup_order_history_arr = array();
				foreach($sales_order_reminder_data[0] as $key => $line){
					if(isset($line->reminder_action) && $line->reminder_action == 'add'){
						$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
						$followup_onlytime = '00:00:00';
						if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
							$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
						}
						$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
						$reminder_onlytime = '00:00:00';
						if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
							$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
						}
						$add_followup_order_history = array(
							'order_id' => $sales_order_id,
							'followup_category_id' => $line->followup_category_id,
							'followup_date' => $followup_onlydate.' '.$followup_onlytime,
							'reminder_category_id' => $line->reminder_category_id,
							'reminder_date' => $reminder_onlydate.' '.$reminder_onlytime,
							'history' => addslashes($line->history),
							'reminder_message' => addslashes($line->reminder_message),
							'followup_by' => $line->followup_by,
							'assigned_to' => $line->assigned_to,
						);
						if(isset($line->followup_at) && !empty($line->followup_at)){
							$add_followup_order_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
						}
						$add_followup_order_history['updated_by'] = $line->updated_by;
						if(isset($line->updated_at) && !empty($line->updated_at)){
							$add_followup_order_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
						}
						if(isset($line->is_notified) && $line->is_notified == 1){
							$add_followup_order_history['is_notified'] = 1;
						}
						array_push($add_followup_order_history_arr, $add_followup_order_history);
					}
					$edit_followup_order_history = array();
					if(isset($line->reminder_action) && $line->reminder_action == 'edit'){
						$edit_followup_order_history['id'] = $line->id;
                        $edit_followup_order_history['order_id'] = $sales_order_id;
						$edit_followup_order_history['followup_category_id'] = $line->followup_category_id;
						$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
						$followup_onlytime = '00:00:00';
						if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
							$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
						}
						$edit_followup_order_history['followup_date'] = $followup_onlydate.' '.$followup_onlytime;
						$edit_followup_order_history['reminder_category_id'] = $line->reminder_category_id;
						$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
						$reminder_onlytime = '00:00:00';
						if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
							$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
						}
						$edit_followup_order_history['reminder_date'] = $reminder_onlydate.' '.$reminder_onlytime;
						$edit_followup_order_history['history'] = addslashes($line->history);
						$edit_followup_order_history['reminder_message'] = addslashes($line->reminder_message);
						$edit_followup_order_history['followup_by'] = $line->followup_by;
						$edit_followup_order_history['assigned_to'] = $line->assigned_to;

						if(isset($line->followup_at) && !empty($line->followup_at)){
							$edit_followup_order_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
						}
						$edit_followup_order_history['updated_by'] = $line->updated_by;
						if(isset($line->updated_at) && !empty($line->updated_at)){
							$edit_followup_order_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
						}
						if(isset($line->is_notified) && $line->is_notified == 1){
							$edit_followup_order_history['is_notified'] = 1;
						}
						if(empty($line->id)){
							unset($line->id);
							unset($edit_followup_order_history['id']);
							array_push($add_followup_order_history_arr,$edit_followup_order_history);
						}else{
							array_push($edit_followup_order_history_arr, $edit_followup_order_history);
						}
					}
				}
				if(!empty($add_followup_order_history_arr)){
					$this->db->insert_batch('followup_order_history', $add_followup_order_history_arr);
				}
				if(!empty($edit_followup_order_history_arr)){
					$this->db->update_batch('followup_order_history', array_filter($edit_followup_order_history_arr), 'id');
				}
            }
        } else {
            $post_data['sales_order_data']['sales_order_no'] = 'SO' . $this->get_new_sales_order_no();
            $dataToInsert = $post_data['sales_order_data'];
            $dataToInsert['created_at'] = $this->now_time;
            $dataToInsert['created_by'] = $this->staff_id;
            // Unset quotation_id from Inquiry Data
            if (isset($dataToInsert['sales_order_id']))
                unset($dataToInsert['sales_order_id']);

            $result = $this->db->insert('sales_order', $dataToInsert);
            $sales_order_id = $this->db->insert_id();
            if ($result) {

                $login_data['sales_order_id'] = $sales_order_id;
                $login_data['created_by_id'] = $this->staff_id;
                $login_data['created_date_time'] = $this->now_time;
                $login_data['created_at'] = $this->now_time;
                $login_data['last_modified_by_id'] = $this->staff_id;
                $login_data['modified_date_time'] = $this->now_time;
                $login_data['updated_at'] = $this->now_time;
                $this->crud->insert('sales_order_logins', $login_data);

                /* -- Update Qoutation Status -- */
                $quotation_no = $this->crud->get_id_by_val('quotations', 'quotation_no', 'id', $post_data['sales_order_data']['quotation_id']);
                //print_r($quotation_no);exit;
                if (!empty($quotation_no)) {
                    $this->crud->update('quotations', array('quotation_status_id' => QUOTATION_STATUS_ID_ON_ORDER_CREATE), array('quotation_no' => $quotation_no));
                }

                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Sales Order Added Successfully');
                foreach ($line_items_data[0] as $lineitem) {
                    $add_lineitem = array();
                    $add_lineitem['sales_order_id'] = $sales_order_id;
                    $add_lineitem['item_id'] = $lineitem->item_id;
                    $add_lineitem['item_code'] = $lineitem->item_code;
                    $add_lineitem['item_name'] = $lineitem->item_name;
                    $add_lineitem['item_category_id'] = ITEM_CATEGORY_FINISHED_GOODS_ID;
                    $add_lineitem['item_description'] = $lineitem->item_description;
                    if (isset($lineitem->item_extra_accessories_id) && !empty($lineitem->item_extra_accessories_id)) {
                        $add_lineitem['item_extra_accessories_id'] = $lineitem->item_extra_accessories_id;
                    }
                    $add_lineitem['quantity'] = $lineitem->quantity;
                    $add_lineitem['currency_id'] = $lineitem->currency_id;
                    $add_lineitem['rate'] = $lineitem->item_rate;
                    $add_lineitem['amount'] = $lineitem->amount;
                    $add_lineitem['disc_per'] = !empty($lineitem->disc_per) ? $lineitem->disc_per : '0';
                    $add_lineitem['disc_value'] = $lineitem->disc_value;
                    $add_lineitem['net_amount'] = $lineitem->net_amount;
                    if (isset($lineitem->dispatched_qty) && !empty($lineitem->dispatched_qty)) {
                        $add_lineitem['dispatched_qty'] = $lineitem->dispatched_qty;
                    }
                    $add_lineitem['created_at'] = $this->now_time;
                    $this->crud->insert('sales_order_items', $add_lineitem);
                }
                $add_followup_order_history_arr = array();
				foreach($sales_order_reminder_data[0] as $line){
					$add_followup_order_history = array();
                    $add_followup_order_history['order_id'] = $sales_order_id;
					$add_followup_order_history['followup_category_id'] = $line->followup_category_id;
					$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
					$followup_onlytime = '00:00:00';
					if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
						$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
					}
					$add_followup_order_history['followup_date'] = $followup_onlydate.' '.$followup_onlytime;
					$add_followup_order_history['reminder_category_id'] = $line->reminder_category_id;
					$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
					$reminder_onlytime = '00:00:00';
					if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
						$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
					}
					$add_followup_order_history['reminder_date'] = $reminder_onlydate.' '.$reminder_onlytime;
					$add_followup_order_history['history'] = addslashes($line->history);
					$add_followup_order_history['reminder_message'] = addslashes($line->reminder_message);
					$add_followup_order_history['followup_by'] = $line->followup_by;
					$add_followup_order_history['assigned_to'] = $line->assigned_to;

					if(isset($line->followup_at) && !empty($line->followup_at)){
						$add_followup_order_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
					}
					$add_followup_order_history['updated_by'] = $line->updated_by;
					if(isset($line->updated_at) && !empty($line->updated_at)){
						$add_followup_order_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
					}
					array_push($add_followup_order_history_arr, $add_followup_order_history);
				}
				$this->db->insert_batch('followup_order_history', array_filter($add_followup_order_history_arr));
                $this->sales_order_notification();
            }
        }
        if ($send_sms == 1) {
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['sales_order_data']['sales_to_party_id']);
            $this->applib->send_sms('order', $sales_order_id, $party_phone_no, SEND_SALES_ORDER_SMS_ID);
        }
        print json_encode($return);
        exit;
    }

    function sales_order_notification() {
        $client = new Client(new Version1X(SERVER_REQUEST_SCHEME . '://' . HTTP_HOST . ':' . PORT_NUMBER));
        $client->initialize();
        $client->emit('notify_sales_order_for_approve', []);
        $client->close();
    }

    function order_list($status='',$staff_id='',$from_date='',$to_date='') {
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $role_view = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "view");
        if ($role_view) {
            $data = array();
//                        $users = $this->db->where('active !=',0)->get('staff')->result();
            $users = $lead_owner = $this->crud->get_staff_dropdown();
            $data['users'] = $users;
            $data['status']=$status;
            $data['staff_id']=$staff_id;
            $data['from_date']=$from_date;
            $data['to_date']=$to_date;
            set_page('sales/order/order_list', $data);
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function order_datatable() {
//            echo '<pre>'; print_r($_POST['user_id']); exit;
        
        $invoice_created_sales_order_ids = $this->crud->getFromSQL('SELECT `sales_order_id` FROM `challans` WHERE `id` IN (SELECT `challan_id` FROM `invoices`)');
        $invoice_created_sales_order_id_arr = array();
        foreach ($invoice_created_sales_order_ids as $invoice_created_sales_order_id){
            if(!empty($invoice_created_sales_order_id->sales_order_id)){
                if(in_array($invoice_created_sales_order_id->sales_order_id, $invoice_created_sales_order_id_arr)){ } else {
                    $invoice_created_sales_order_id_arr[] = $invoice_created_sales_order_id->sales_order_id;
                }
            }
        }
        
        $config['table'] = 'sales_order s';
        $config['select'] = 's.*,q.quotation_no,p.party_name,p.party_type_1,city.city,state.state,country.country,sf.name AS sales_person';
        $config['select'] .= ',GROUP_CONCAT(soi.item_code) AS so_item_code,cur_s.name AS party_current_person';
        $config['column_order'] = array(null, 'q.id', 's.id', null, null, 'soi.item_code', 'p.party_name', 's.sales_order_date', 's.committed_date');
        $config['column_search'] = array('q.quotation_no', 's.sales_order_no', 'p.party_name', 'DATE_FORMAT(s.sales_order_date,"%d-%m-%Y")', 'DATE_FORMAT(s.committed_date,"%d-%m-%Y")', 'city.city', 'state.state', 'country.country');
        $config['joins'][0] = array('join_table' => 'quotations q', 'join_by' => 's.quotation_id = q.id', 'join_type' => 'left');
        $config['joins'][1] = array('join_table' => 'party p', 'join_by' => 'p.party_id = s.sales_to_party_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'sales_order_items soi', 'join_by' => 'soi.sales_order_id = s.id', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff sf', 'join_by' => 'sf.staff_id = s.created_by', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');
        $config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
        if (isset($_POST['is_approved']) && $_POST['is_approved'] != 'all') {
            $config['wheres'][] = array('column_name' => 's.isApproved', 'column_value' => $_POST['is_approved']);
        }
        if (isset($_POST['from_date']) && !empty($_POST['from_date'])) {
            $from_date = date('Y-m-d', strtotime($_POST['from_date']));
            $config['wheres'][] = array('column_name' => 's.sales_order_date >=', 'column_value' => $from_date);
        }
        if (isset($_POST['to_date']) && !empty($_POST['to_date'])) {
            $to_date = date('Y-m-d', strtotime($_POST['to_date']));
            $config['wheres'][] = array('column_name' => 's.sales_order_date <=', 'column_value' => $to_date);
        }
        if (isset($_POST['user_id']) && $_POST['user_id'] != 'all') {
            $config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' => $_POST['user_id']);
        }
        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
        if ($this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER, "allow") == 1) {
            if (isset($_POST['invoice_not_created']) && $_POST['invoice_not_created'] == 'true') {
                if(!empty($invoice_created_sales_order_id_arr)){
                    $config['custom_where'] = " `s.id` NOT IN(".implode(',', $invoice_created_sales_order_id_arr).")";
                }
            }
            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            }
        } else {
            if (isset($_POST['invoice_not_created']) && $_POST['invoice_not_created'] == 'true') {
                if(!empty($invoice_created_sales_order_id_arr)){
                    $config['custom_where'] = ' `s.id` NOT IN('.implode(',', $invoice_created_sales_order_id_arr).') AND (p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL)';
                } else {
                    $config['custom_where'] = ' (p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL)';
                }
            } else {
                $config['custom_where'] = '(p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL)';
            }

            if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                
            } else if ($cu_accessExport == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
            } else if ($cu_accessDomestic == 1) {
                $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
            } else {
                
            }
            $config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' => $this->session->userdata('is_logged_in')['staff_id']);
        }

        if (!empty($_POST['order_status']) && $_POST['order_status'] == 'all') {
            
        } else {
            $config['wheres'][] = array('column_name' => 's.is_dispatched', 'column_value' => $_POST['order_status']);
        }

        if (!empty($_POST['party_type']) && $_POST['party_type'] == 'all') {
            
        } else if (isset($_POST['party_type']) && $_POST['party_type'] == 'export') {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
        } else if (isset($_POST['party_type']) && $_POST['party_type'] == 'domestic') {
            $config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
        }

        $config['group_by'] = 's.id';
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
//		echo $this->db->last_query();exit;
        $role_delete = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "delete");
        $role_edit = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "edit");
        foreach ($list as $order_row) {

            $sales_order_items = $this->crud->get_row_by_id('sales_order_items', array('sales_order_id' => $order_row->id));
            if (!empty($sales_order_items[0])) {
                $order_row->count_items = count($sales_order_items[0]);
            } else {
                $order_row->count_items = 1;
            }

            $row = array();
            $action = '';
            if($order_row->is_dispatched != CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID){
                if ($role_edit) {
                    if ($order_row->party_type_1 == PARTY_TYPE_EXPORT_ID) {
                        $action .= '<a href="' . base_url('sales_order/add_export/' . $order_row->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
                    } elseif ($order_row->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
                        $action .= '<a href="' . base_url('sales_order/add/' . $order_row->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a> ';
                    }
                }
                if ($role_delete) {
                    if ($order_row->delete_not_allow != 1) {
                        $action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('sales_order/delete_sales_order/' . $order_row->id) . '"><i class="fa fa-trash"></i></a>';
                    }
                }
            }
            if ($order_row->isApproved == 1) {
                $action .= '<form id="print_' . $order_row->id . '" method="post" action="' . base_url('sales_order/sales_order_print/') . '" style="width: 25px; display: initial;" target="_blank" title="Print">
                            <input type="hidden" name="sales_order_id" id="sales_order_id" value="' . $order_row->id . '">
                            <a class="btn-primary btn-xs" href="javascript:{}" onclick="document.getElementById(\'print_' . $order_row->id . '\').submit();"><i class="fa fa-print"></i></a>
                        </form> ';
//                $action .= ' | <a href="' . base_url('sales_order/sales_order_print/' . $order_row->id) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-print"></i></a>';
                $action .= ' | <a href="' . base_url('sales_order/work_order/' . $order_row->id) . '" target="_blank" title="Work Order" class=""><i class="fa fa-print"></i></a>';
                $action .= ' | <a href="' . base_url('sales_order/sales_order_email/' . $order_row->id) . '" class="" title="Email"><i class="fa fa-envelope"></i></a>';
            }
            $row[] = $action;
            $so_item_code = explode(",", $order_row->so_item_code);
            $so_item_code = (count($so_item_code) > 1 ? 'Multiple' : $so_item_code[0]);
            $order_row->so_item_code = str_replace(',', ', ', $order_row->so_item_code);
            if ($order_row->party_type_1 == PARTY_TYPE_EXPORT_ID) {
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->quotation_no . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->sales_order_no . '</a>';
                if ($order_row->is_dispatched == DISPATCHED_ORDER_ID) {
                    $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >Dispatched</a>';
                } else if ($order_row->is_dispatched == CANCELED_ORDER_ID) {
                    $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >Canceled Order</a>';
                } else if ($order_row->is_dispatched == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) {
                    $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >Challan Created from Proforma Invoice</a>';
                } else {
                    $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >Pending Dispatch</a>';
                }

                if ($order_row->isApproved == 1) {
                    $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >Approved</a>';
                } else {
                    $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >Not Approved</a>';
                }

                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="' . $order_row->so_item_code . '" >' . $so_item_code . '</a>';

                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->party_name . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->sales_person . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . date('d-m-Y', strtotime($order_row->sales_order_date)) . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->city . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->state . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->country . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add_export/' . $order_row->id . '?view') . '" >' . $order_row->party_current_person . '</a>';
            } elseif ($order_row->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->quotation_no . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->sales_order_no . '</a>';
                if ($order_row->is_dispatched == DISPATCHED_ORDER_ID) {
                    $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >Dispatched</a>';
                } else if ($order_row->is_dispatched == CANCELED_ORDER_ID) {
                    $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >Canceled Order</a>';
                } else if ($order_row->is_dispatched == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) {
                    $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >Challan Created from Proforma Invoice</a>';
                } else {
                    $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >Pending Dispatch</a>';
                }

                if ($order_row->isApproved == 1) {
                    $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >Approved</a>';
                } else {
                    $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >Not Approved</a>';
                }
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="' . $order_row->so_item_code . '" >' . $so_item_code . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->party_name . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->sales_person . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . date('d-m-Y', strtotime($order_row->sales_order_date)) . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->city . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->state . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->country . '</a>';
                $row[] = '<a href="' . base_url('sales_order/add/' . $order_row->id . '?view') . '" >' . $order_row->party_current_person . '</a>';
            }
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function delete_sales_order($id) {
        $role_delete = $this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "delete");
        if ($role_delete) {

            $quotation_id = $this->crud->get_id_by_val('sales_order', 'quotation_id', 'id', $id);
            $this->crud->update('quotations', array('quotation_status_id' => $this->crud->get_id_by_val('quotation_status', 'id', 'LOWER(quotation_status)', 'hot')), array('id' => $quotation_id));

            $where_array = array("id" => $id);
            $this->crud->delete("sales_order", $where_array);
            $where_array = array("sales_order_id" => $id);
            $this->crud->delete("sales_order_items", $where_array);
            $where_array = array("order_id" => $id);
            $this->crud->delete("followup_order_history", $where_array);

            $session_data = array(
                'success_message' => "Sales Order has been deleted !"
            );
            $this->session->set_userdata($session_data);
            redirect(BASE_URL . "sales_order/order_list");
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
    }

    function sales_order_print() {
        if (isset($_POST['sales_order_id']) && $_POST['sales_order_id']) {
            $this->print_pdf($_POST['sales_order_id'], 'print');
        }
    }

    function sales_order_email($id) {
        $this->print_pdf($id, 'email');
    }

    function print_pdf($id, $type) {
        $select_item = (isset($_GET['item']) && $_GET['item']) ? $_GET['item'] : '';
        $sales_order_data = $this->get_sales_order_detail($id);
        $sales_order_data->delivery_through = $this->crud->get_column_value_by_id('delivery_through', 'delivery_through', array('delivery_through_id' => $sales_order_data->delivery_through_id));
        //echo '<pre>';print_r($sales_order_data);exit;
        if (isset($sales_order_data->delivery_city)) {
            $sales_order_data->delivery_city = $this->crud->get_id_by_val('city', 'city', 'city_id', $sales_order_data->delivery_city);
        }
        if (isset($sales_order_data->delivery_state)) {
            $sales_order_data->delivery_state = $this->crud->get_id_by_val('state', 'state', 'state_id', $sales_order_data->delivery_state);
        }
        if (isset($sales_order_data->delivery_country)) {
            $sales_order_data->delivery_country = $this->crud->get_id_by_val('country', 'country', 'country_id', $sales_order_data->delivery_country);
        }

        $company_details = $this->applib->get_company_detail();

        $company_ceo_details['name'] = $company_details['authorized_signatory_name'];
        $company_ceo_details['signature'] = $company_details['authorized_signatory_image'];
//                echo '<pre>';print_r($company_details);exit;
        //$sales_orders_items = $this->feed_saleorders_items($id);
        $sales_order_items = $this->get_sales_order_items($id);
        //echo '<pre>';print_r($sales_order_items);exit;
        $item_array = array();
        if (!empty($sales_order_items)) {
            foreach ($sales_order_items as $key => $item_row) {
                $sales_order_items[$key]['hsn'] = $this->crud->get_id_by_val('items', 'hsn_code', 'id', $item_row['item_id']);
                $sales_order_items[$key]['item_extra_accessories'] = $this->crud->get_id_by_val('item_extra_accessories', 'item_extra_accessories_label', 'item_extra_accessories_id', $item_row['item_extra_accessories_id']);
            }
        }
        //echo '<pre>';print_r($sales_order_items);exit;
        $this->load->library('m_pdf');
        $margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
        $margin_left = $margin_company[0]->margin_left;
        $margin_right = $margin_company[0]->margin_right;
        $margin_top = $margin_company[0]->margin_top;
        $margin_bottom = $margin_company[0]->margin_bottom;
        if (!empty($sales_order_items)) {
            foreach ($sales_order_items as $key => $item_row) {
                if ($select_item && $item_row['id'] != $select_item) {
                    continue;
                }
                $param = "'utf-8','A4'";
                $pdf = $this->m_pdf->load($param);
                $pdf->AddPage(
                    'P', //orientation
                    '', //type
                    '', //resetpagenum
                    '', //pagenumstyle
                    '', //suppress
                    $margin_left, //margin-left
                    $margin_right, //margin-right
                    $margin_top, //margin-top
                    $margin_bottom, //margin-bottom
                    0, //margin-header
                    0 //margin-footer
                );
                if ($sales_order_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
                    $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
                } else {
                    $letterpad_details = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'export_letter_pad_content');
                }
                if ($key == 0) {
                    $other_items = $this->get_sales_order_sub_items($id);
                    $html = $this->load->view('sales/order/pdf', array('sales_order_data' => $sales_order_data, 'sales_order_item' => $item_row, 'sales_order_items' => $sales_order_items, 'company_details' => $company_details, 'company_ceo_details' => $company_ceo_details, 'is_first_item' => true, 'other_items' => $other_items, 'letterpad_details' => $letterpad_details), true);
                } else {
                    $html = $this->load->view('sales/order/pdf', array('sales_order_data' => $sales_order_data, 'sales_order_item' => $item_row, 'sales_order_items' => $sales_order_items, 'company_details' => $company_details, 'company_ceo_details' => $company_ceo_details, 'is_first_item' => false, 'letterpad_details' => $letterpad_details), true);
                }
                $pdf->WriteHTML($html);
                $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: '. $letterpad_details->header_logo_alignment .';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>');
                $pdf->SetHTMLFooter($letterpad_details->footer_detail);
//                $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/SalesOrder_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');


                // Terms and conditions Start

                if ($sales_order_data->party_type_1 == PARTY_TYPE_EXPORT_ID) {
                    if ($sales_order_data->export_order_type == 'Sales Contract') {
                        $t_and_c_id = $this->crud->get_id_by_val('terms_and_conditions', 'id', 'module', 'sales_order_terms_and_conditions_for_contract');
                    } elseif ($sales_order_data->export_order_type == 'Sales Order') {
                        $t_and_c_id = $this->crud->get_id_by_val('terms_and_conditions', 'id', 'module', 'sales_order_terms_and_conditions_for_export_order');
                    } else {
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Please Select Export Order Type');
                        redirect('sales_order/add_export/' . $id, 'refresh');
                    }
                } elseif ($sales_order_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) {
                    $t_and_c_id = $this->crud->get_id_by_val('terms_and_conditions', 'id', 'module', 'sales_order_terms_and_conditions_for_domestic');
                }
                $t_and_c_result_array = $this->crud->get_columns_val_by_where('terms_and_conditions_sub', '*', array('module_id' => $t_and_c_id));

                $replacements = array(0 => array('detail' => $sales_order_data->terms_condition_purchase));
                $t_and_c_result_array = array_replace($t_and_c_result_array, $replacements);
                $terms_conditionss = '';
                $tc_flag_inc = 1;
                if (!empty($t_and_c_result_array)) {
                    $tc_count = count($t_and_c_result_array);
                    $tc_count = $tc_count + 1;
                    //                    $first = reset($query->result_array());
                    //                    $last = end($query->result_array());
                    foreach ($t_and_c_result_array as $key => $page_detail_row) {
                        $pdf->AddPage(
                            'P', //orientation
                            '', //type
                            '', //resetpagenum
                            '', //pagenumstyle
                            '', //suppress
                            $margin_left, //margin-left
                            $margin_right, //margin-right
                            $margin_top, //margin-top
                            $margin_bottom, //margin-bottom
                            0, //margin-header
                            0 //margin-footer
                        );
                        if ($tc_flag_inc == 1) {
                            $terms_conditionss[0] .= '<div align="center" style="font-size: 18px;"><b>Terms And Conditions</b></div>';
                        } $tc_flag_inc++;
                        //$terms_conditionss[$key] .= nl2br($page_detail_row['detail']);
                        $terms_conditionss[$key] .= $page_detail_row['detail'];
                        //                        if(end($query->result_array())){
                        if($tc_flag_inc == $tc_count){
                        $terms_conditionss[$key] .= '<div><table>
                                <tr>
                                    <td colspan="10">
                                        <table class="no-border">
                                            <tr class="no-border">
                                                <td align="center" class="no-border-top footer-detail-area" style="border-left: 0!important;">
                                                    <strong>' . $sales_order_data->party_name . '</strong><br />
                                                </td>
                                                <td align="center" class="no-border-top footer-detail-area">
                                                    <strong>For, ' . $company_details['name'] . '</strong>
                                                </td>
                                                <td align="center" class="no-border-top footer-detail-area">
                                                    <strong>For, ' . $company_details['name'] . '</strong>
                                                </td>
                                            </tr>
                                            <tr class="no-border">
                                                <td class="no-border footer-sign-area text-bold no-border-left no-border-top no-border-bottom">
                                                    ' . $sales_order_data->contact_person_name . '<br /> Purchaser Sign
                                                </td>
                                                <td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                                    <img height="80px" width="80px" src="' . image_url('staff/signature/' . $company_ceo_details['signature']) . '"><br />
                                                    ' . $company_ceo_details['name'] . '<br/> Authorized Signatory
                                                </td>
                                                <td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                                    <img src="' . BASE_URL . '/resource/image/jk_symbol.jpg" style="margin-bottom:5px"><br />
                                                    ' . $company_details['m_d_name'] . '<br/> Managing Director
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table></div>';
                        }
                        //                        }
                        $pdf->WriteHTML($terms_conditionss[$key]);
                        $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: ' . $letterpad_details->header_logo_alignment . ';"><img src="' . base_url() . image_dir('letterpad_header_logo/' . $letterpad_details->header_logo) . '" width="100px"></div>');
                        $pdf->SetHTMLFooter($letterpad_details->footer_detail);
//                        $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/SalesOrder_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
                    }
                }//exit;
                // Terms and conditions End
                /*print_r($terms_conditionss);
                exit();*/
                if ($type == 'print') {
                    $pdfFilePath = "sales_order.pdf";
                    $pdf->Output($pdfFilePath, "I");
                } elseif ($type == 'email') {
                    $pdfFilePath = "./uploads/sales_order_" . $id . "_" . $item_row['id'] . ".pdf";
                    $files_urls[] = $pdfFilePath;
                    $pdf->Output($pdfFilePath, "F");
                    break;
                }
            }
        } else {
            $html = $this->load->view('sales/order/pdf', array('sales_order_data' => $sales_order_data, 'sales_order_item' => array(), 'company_details' => $company_details, 'company_ceo_details' => $company_ceo_details, 'is_first_item' => false), true);
            $param = "'utf-8','A4'";
            $pdf = $this->m_pdf->load($param);
            $pdf->AddPage(
                'P', //orientation
                '', //type
                '', //resetpagenum
                '', //pagenumstyle
                '', //suppress
                $margin_left, //margin-left
                $margin_right, //margin-right
                $margin_top, //margin-top
                $margin_bottom, //margin-bottom
                0, //margin-header
                0 //margin-footer
            );
            if ($type == 'print') {
                $pdfFilePath = "sales_order.pdf";
                $pdf->Output($pdfFilePath, "I");
            } elseif ($type == 'email') {
                $pdfFilePath = "./uploads/sales_order_" . $id . ".pdf";
                $files_urls[] = $pdfFilePath;
                $pdf->Output($pdfFilePath, "F");
            }
        }
        if ($type == 'email') {
            $this->db->where('id', $id);
            $this->db->update('sales_order', array('pdf_url' => serialize($files_urls)));
            redirect(base_url() . 'mail-system3/document-mail/sales-order/' . $id);
        }
    }

    function work_order($id) {
        $select_item = (isset($_GET['item']) && $_GET['item']) ? $_GET['item'] : '';
        $sales_order_data = $this->get_sales_order_detail($id);
        //~ echo '<pre>';print_r($sales_order_data);exit;
        $sales_order_data->delivery_city = $this->crud->get_id_by_val('city', 'city', 'city_id', $sales_order_data->delivery_city);
        $sales_order_data->delivery_state = $this->crud->get_id_by_val('state', 'state', 'state_id', $sales_order_data->delivery_state);
        $sales_order_data->delivery_country = $this->crud->get_id_by_val('country', 'country', 'country_id', $sales_order_data->delivery_country);
        $sales_order_data->sales_executive_name = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $this->session->userdata('is_logged_in')['staff_id']);
        $company_details = $this->applib->get_company_detail();
        //$sales_orders_items = $this->feed_saleorders_items($id);
        $sales_order_items = $this->get_sales_order_items($id);
        $item_array = array();
        if (!empty($sales_order_items)) {
            foreach ($sales_order_items as $key => $item_row) {
                $sales_order_items[$key]['hsn'] = $this->crud->get_id_by_val('items', 'hsn_code', 'id', $item_row['item_id']);
            }
        }
        //echo '<pre>';print_r($sales_order_items);exit;
        $this->load->library('m_pdf');
        $margin_company = $this->crud->get_all_with_where('company', '', '', array('id' => COMPANY_ID));
        $margin_left = $margin_company[0]->margin_left;
        $margin_right = $margin_company[0]->margin_right;
        $margin_top = $margin_company[0]->margin_top;
        $margin_bottom = $margin_company[0]->margin_bottom;
        if (!empty($sales_order_items)) {
            foreach ($sales_order_items as $key => $item_row) {
                if ($select_item && $item_row['id'] != $select_item) {
                    continue;
                }
                $param = "'utf-8','A4'";
                $pdf = $this->m_pdf->load($param);
                $pdf = $this->m_pdf->load($param);
                $pdf->pagenumPrefix = 'Page ';
                $pdf->pagenumSuffix = ' - ';
                $pdf->nbpgPrefix = ' out of ';
                $pdf->nbpgSuffix = ' pages';
                $pdf->setFooter('{PAGENO}{nbpg}');
                $pdf->AddPage(
                    'P', //orientation
                    '', //type
                    '', //resetpagenum
                    '', //pagenumstyle
                    '', //suppress
                    $margin_left, //margin-left
                    $margin_right, //margin-right
                    $margin_top, //margin-top
                    $margin_bottom, //margin-bottom
                    0, //margin-header
                    0 //margin-footer
                );
                if ($key == 0) {
                    $other_items = $this->get_sales_order_sub_items($id);
                    $html = $this->load->view('sales/order/work_order_pdf', array('sales_order_data' => $sales_order_data, 'sales_order_item' => $item_row, 'sales_order_items' => $sales_order_items, 'company_details' => $company_details, 'is_first_item' => true, 'other_items' => $other_items), true);
                } else {
                    $html = $this->load->view('sales/order/work_order_pdf', array('sales_order_data' => $sales_order_data, 'sales_order_item' => $item_row, 'sales_order_items' => $sales_order_items, 'company_details' => $company_details, 'is_first_item' => false), true);
                }
                $pdf->WriteHTML($html);
                $pdfFilePath = "sales_order.pdf";
                $pdf->Output($pdfFilePath, "I");
            }
        } else {
            $html = $this->load->view('sales/order/work_order_pdf', array('sales_order_data' => $sales_order_data, 'sales_order_item' => array(), 'company_details' => $company_details, 'is_first_item' => false), true);
            $param = "'utf-8','A4'";
            $pdf = $this->m_pdf->load($param);
            $pdf->AddPage(
                'P', //orientation
                '', //type
                '', //resetpagenum
                '', //pagenumstyle
                '', //suppress
                $margin_left, //margin-left
                $margin_right, //margin-right
                $margin_top, //margin-top
                $margin_bottom, //margin-bottom
                0, //margin-header
                0 //margin-footer
            );
            $pdf->WriteHTML($html);
            $pdfFilePath = "work_order.pdf";
            $pdf->Output($pdfFilePath, "I");
        }
    }

    function get_quotation_followup_history($quotation_id) {
        $where = array('quotation_id' => $quotation_id);
        $followup_histories = array();
        $followup_history_data = $this->crud->get_row_by_id('followup_history', $where);
        foreach($followup_history_data as $followup_history){
            $followup_history->followup_history_date = date('d-m-Y H:i:s', strtotime($followup_history->followup_date));
            $followup_history->followup_history_onlydate = date('d-m-Y', strtotime($followup_history->followup_date));
            $followup_history->followup_history_onlytime = date('H:i:s', strtotime($followup_history->followup_date));
            $followup_history->followup_history_history = stripslashes($followup_history->history);
            $followup_histories[] = json_encode($followup_history);
        }
        $quotation_reminders = array();
        $quotation_reminder_data = $this->crud->get_row_by_id('followup_history', $where);
        foreach($quotation_reminder_data as $quotation_reminder){
            $quotation_reminder->reminder_onlydate = '';
            $quotation_reminder->reminder_onlytime = '';
            if(strtotime($quotation_reminder->reminder_date) > 0){
                $quotation_reminder->reminder_date = date('d-m-Y H:i:s', strtotime($quotation_reminder->reminder_date));
                $quotation_reminder->reminder_onlydate = date('d-m-Y', strtotime($quotation_reminder->reminder_date));
                $quotation_reminder->reminder_onlytime = date('H:i:s', strtotime($quotation_reminder->reminder_date));
            }
            $quotation_reminder->followup_onlydate = '';
            $quotation_reminder->followup_onlytime = '';
            if(strtotime($quotation_reminder->followup_date) > 0){
                $quotation_reminder->followup_date = date('d-m-Y H:i:s', strtotime($quotation_reminder->followup_date));
                $quotation_reminder->followup_onlydate = date('d-m-Y', strtotime($quotation_reminder->followup_date));
                $quotation_reminder->followup_onlytime = date('H:i:s', strtotime($quotation_reminder->followup_date));
            }
            $quotation_reminder->history = stripslashes($quotation_reminder->history);
            $quotation_reminder->reminder_message = stripslashes($quotation_reminder->reminder_message);
            $quotation_reminders[] = json_encode($quotation_reminder);
        }
        $return_quotation_reminder_data = implode(',', $quotation_reminders);
        return $return_quotation_reminder_data;
    }
    
    function get_enquiry_followup_history($inquiry_id) {
        $followup_histories = array();
        $where = array('inquiry_id' => $inquiry_id);
        $followup_history_data = $this->crud->get_row_by_id('inquiry_followup_history', $where);
        foreach($followup_history_data as $followup_history){
            $followup_history->followup_history_date = date('d-m-Y H:i:s', strtotime($followup_history->followup_date));
            $followup_history->followup_history_onlydate = date('d-m-Y', strtotime($followup_history->followup_date));
            $followup_history->followup_history_onlytime = date('H:i:s', strtotime($followup_history->followup_date));
            $followup_history->followup_history_history = stripslashes($followup_history->history);
            //                    $followup_history->followup_by = $this->crud->get_id_by_val('staff', 'name', 'staff_id', $followup_history->followup_by);
            $followup_histories[] = json_encode($followup_history);
        }
        $return_enquiry_followup_history_data = implode(',', $followup_histories);
        return $return_enquiry_followup_history_data;
    }
    
    function get_sales_order_items($id) {
        $this->db->select('soi.*');
        $this->db->from('sales_order_items soi');
        $this->db->join('item_category ic', 'ic.id = soi.item_category_id');
        $this->db->where('ic.item_category', 'Finished Goods');
        $this->db->where('soi.sales_order_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function get_sales_order_sub_items($id) {
        $this->db->select('soi.*');
        $this->db->from('sales_order_items soi');
        $this->db->join('item_category ic', 'ic.id = soi.item_category_id');
        $this->db->where('ic.item_category != ', 'Finished Goods');
        $this->db->where('soi.sales_order_id', $id);
        $query = $this->db->get();
        $other_items = array();
        if ($query->num_rows() > 0) {
            $other_items = $query->result_array();
        }
        return $other_items;
    }

    function get_sales_order_detail($id) {
        $select = "sales.sales,currency.currency,quo.quotation_no,so.id as sales_order_id,so.*,party.gst_no AS p_gst_no,party.email_id AS party_email_id,party.pincode,party.party_code,party.party_name,party.phone_no as p_phone_no,party.fax_no,party.w_delivery_party_name AS delivery_party_name,party.w_address AS delivery_address,party.w_city AS delivery_city,party.w_state AS delivery_state,party.w_country AS delivery_country,party.w_phone1 AS delivery_contact_no,party.w_email AS delivery_email_id,party.oldw_pincode AS delivery_oldw_pincode,party.address_work,party.address,party.phone_no,party.party_type_1,city.city,state.state,country.country,party.utr_no AS party_cin_no,";
        $select .= "cp.name as contact_person_name,cp.email as contact_person_email,cp.mobile_no as contact_person_phone,";
        $select .= "stf.name as created_name,stf.email as created_email,stf.contact_no as created_contactno,";
        $select .= "sales_order_pref.sales_order_pref,transportation_by.transportation_by,foundation_drawing_required.foundation_drawing_required,loading_by.loading_by,";
        $select .= "inspection_required.inspection_required,unloading_by.unloading_by,erection_commissioning.erection_commissioning,road_insurance_by.road_insurance_by,for_required.for_required,";
        $select .= "so.bank_detail,company_banks.id,company_banks.detail AS banks_detail,sea_freight_type.name AS sea_freight_type,";
        $this->db->select($select);
        $this->db->from('sales_order so');
        $this->db->join('quotations quo', 'quo.id = so.quotation_id', 'left');
        $this->db->join('sales', 'sales.id = so.sales_id', 'left');
        $this->db->join('party party', 'party.party_id = so.sales_to_party_id', 'left');
        $this->db->join('contact_person cp', 'cp.contact_person_id = so.kind_attn_id', 'left');
        $this->db->join('city', 'city.city_id = party.city_id', 'left');
        $this->db->join('state', 'state.state_id = party.state_id', 'left');
        $this->db->join('country', 'country.country_id = party.country_id', 'left');
        $this->db->join('currency', 'currency.id = so.currency_id', 'left');
        $this->db->join('staff stf', 'stf.staff_id = so.created_by', 'left');
        $this->db->join('sales_order_pref', 'sales_order_pref.id = so.sales_order_pref_id', 'left');
        $this->db->join('transportation_by', 'transportation_by.id = so.transportation_by_id', 'left');
        $this->db->join('foundation_drawing_required', 'foundation_drawing_required.id = so.foundation_drawing_required_id', 'left');
        $this->db->join('loading_by', 'loading_by.id = so.loading_by_id', 'left');
        $this->db->join('inspection_required', 'inspection_required.id = so.inspection_required_id', 'left');
        $this->db->join('unloading_by', 'unloading_by.id = so.unloading_by_id', 'left');
        $this->db->join('erection_commissioning', 'erection_commissioning.id = so.erection_commissioning_id', 'left');
        $this->db->join('road_insurance_by', 'road_insurance_by.id = so.road_insurance_by_id', 'left');
        $this->db->join('for_required', 'for_required.id = so.for_required_id', 'left');
        $this->db->join('company_banks', 'company_banks.id = so.bank_detail', 'left');
        $this->db->join('sea_freight_type', 'sea_freight_type.id = so.sea_freight_type', 'left');
        $this->db->where('so.id', $id);
        $this->db->where('party.active', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            //echo '<pre>';print_r($query->row(0));exit;
            return $query->row(0);
        } else {
            return array();
        }
    }

    function get_new_sales_order_no() {
        $this->db->select_max('id');
        $this->db->from('sales_order');
        $this->db->limit(1);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->id + 1;
        } else {
            return 1;
        }
    }

    function update_sales_order_status_to_approved() {
        if ($_POST['sales_order_id'] != '') {
            $sales_order_id = $_POST['sales_order_id'];
            $this->db->where('id', $sales_order_id);
            $this->db->update('sales_order', array('isApproved' => 1));
            $login_data['approved_by_id'] = $this->staff_id;
            $login_data['approved_date_time'] = $this->now_time;
            $login_data['updated_at'] = $this->now_time;
            $this->crud->update('sales_order_logins', $login_data, array('sales_order_id' => $sales_order_id));
            echo json_encode(array('success' => true, 'message' => 'Sales Order approved successfully!'));
            exit();
        } else {
            redirect("sales_order/order_list");
            exit;
            exit();
        }
    }

    function update_sales_order_status_to_disapproved() {
        if ($_POST['sales_order_id'] != '') {
            $sales_order_id = $_POST['sales_order_id'];
            $this->db->where('id', $sales_order_id);
            $this->db->update('sales_order', array('isApproved' => 0));
            $login_data['approved_by_id'] = null;
            $login_data['approved_date_time'] = null;
            $login_data['updated_at'] = $this->now_time;
            $this->crud->update('sales_order_logins', $login_data, array('sales_order_id' => $sales_order_id));
            echo json_encode(array('success' => true, 'message' => 'Sales Order disapproved successfully!'));
            exit();
        } else {
            redirect("sales_order/order_list");
            exit;
            exit();
        }
    }

    function delivery_reminder($id = '') {
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $customer_reminder = array();
        if (isset($id) && !empty($id)) {
            $where_array['id'] = $id;
            $customer_reminder = $this->crud->get_all_with_where('customer_reminder_sent', 'id', 'ASC', $where_array);
        }
        $data = array();
        $data['id'] = ($customer_reminder) ? $customer_reminder[0]->id : '';
        $data['customer_reminder_id'] = ($customer_reminder) ? $customer_reminder[0]->customer_reminder_id : '';
        $data['sales_order_id'] = ($customer_reminder) ? $customer_reminder[0]->sales_order_id : '';
        $data['party_id'] = ($customer_reminder) ? $customer_reminder[0]->party_id : '';
        $data['subject'] = ($customer_reminder) ? $customer_reminder[0]->subject : '';
        $data['content_data'] = ($customer_reminder) ? $customer_reminder[0]->content_data : '';
        $data['letter_no'] = ($customer_reminder) ? $this->applib->get_reminder_letter_no($customer_reminder[0]->letter_no) : '';
        $data['letter_validity'] = ($customer_reminder) ? $customer_reminder[0]->letter_validity : '';
        $data['received_payment_detail'] = ($customer_reminder) ? $customer_reminder[0]->received_payment_detail : '';
        $data['sales_order_data'] = '';
        if ($customer_reminder) {
            $data['sales_order_data'] = $this->get_sales_order_detail($data['sales_order_id']);
        }
        set_page('reminder/customer_reminder', $data);
    }

    function order_confirmation_reminder($id = '') {
        $this->delivery_reminder($id);
    }

    function order_cancel_reminder($id = '') {
        $this->delivery_reminder($id);
    }

    function dispatch_reminder($id = '') {
        $this->delivery_reminder($id);
    }

    function price_upgrade_reminder($id = '') {
        $this->delivery_reminder($id);
    }

    function order_payment_reminder($id = '') {
        $this->delivery_reminder($id);
    }

    function balance_payment_reminder($id = '') {
        $this->delivery_reminder($id);
    }

    function delivery_reminder_list() {
        set_page('reminder/customer_reminder_list');
    }

}
