<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_purchase extends CI_Controller
{
	public $logged_in_id = null;
	public $now_time = null;
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
		$this->load->library('user_agent');
		$this->logged_in_id = $this->session->userdata('is_logged_in')['admin_id'];
		$this->now_time = date('Y-m-d H:i:s');
	}

	function order_terms(){
		if (!empty($_POST)) {
			$this->db->where('module', 'purchse_order');
			$data['detail'] = $_POST['details'];
			$data['updated_at'] = $this->now_time;
			$data['updated_by'] = $this->logged_in_id;
			$this->db->update('terms_and_conditions', $data);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Puchse Order Terms and Conditions successfully saved!');
			redirect('master_purchase/order_terms');
		} else {
			$terms_detail = $this->applib->getTermsandConditions('purchse_order');
			if($this->applib->have_access_role(PURCHASE_ORDER_TERMS_MENU_ID,"view")) {
				set_page('master_purchase/terms_condition_order', array('terms_detail' => $terms_detail));
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}
	
	function invoice_terms(){
		if (!empty($_POST)) {
			$this->db->where('module', 'purchse_invoice');
			$data['detail'] = $_POST['details'];
			$data['updated_at'] = $this->now_time;
			$data['updated_by'] = $this->logged_in_id;
			$this->db->update('terms_and_conditions', $data);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Puchse Invoice Terms and Conditions successfully saved!');
			redirect('master_purchase/order_terms');
		} else {
			$terms_detail = $this->applib->getTermsandConditions('purchse_invoice');
			if($this->applib->have_access_role(PURCHASE_INVOICE_TERMS_MENU_ID,"view")) {
				set_page('master_purchase/terms_condition_invoice', array('terms_detail' => $terms_detail));
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}

		}
	}
	
}