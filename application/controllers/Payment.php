<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Payment
 * &@property Crud $crud
 */
class Payment extends CI_Controller
{	

	function __construct()
	{
		parent::__construct();
//		if (!$this->session->userdata('is_logged_in')) {
//			redirect('');
//		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel','app_model'); 
		$this->load->model('Crud','crud');       
	}

	public function add($payment_id=''){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		if(isset($payment_id) && !empty($payment_id)){
			if($this->app_model->have_access_role(PAYMENT_MODULE_ID, "edit")){
				$payment_data = $this->crud->get_row_by_id('payment', array('payment_id' => $payment_id));
				if(empty($payment_data)){
					redirect("payment/payment_list"); exit;
				}
				$payment_data = $payment_data[0];
				$payment_data->created_by_name = $this->crud->get_id_by_val('staff', 'name','staff_id',$payment_data->created_by);
				$payment_data->created_at =  substr($payment_data->created_at, 8, 2) .'-'. substr($payment_data->created_at, 5, 2) .'-'. substr($payment_data->created_at, 0, 4);
				$payment_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $payment_data->updated_by));
				$payment_data->updated_at =  substr($payment_data->updated_at, 8, 2) .'-'. substr($payment_data->updated_at, 5, 2) .'-'. substr($payment_data->updated_at, 0, 4);
				$payment_data->approved_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $payment_data->approved_by));
				$payment_data->approved_at = substr($payment_data->approved_at, 8, 2) .'-'. substr($payment_data->approved_at, 5, 2) .'-'. substr($payment_data->approved_at, 0, 4);
				//echo '<pre>';print_r($payment_data);exit;
				set_page('account/payment/add', $payment_data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}else{
			if($this->app_model->have_access_role(PAYMENT_MODULE_ID, "add")){
				$maxid = 0;
				$row = $this->crud->getFromSQL('SELECT MAX(receipt_no) AS `receipt_no` FROM `payment`');
				$maxid = $row[0]->receipt_no + 1;
				$data['max_receipt_no'] = $maxid;
				set_page('account/payment/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}	 
		}
	}

	function save_payment_data(){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$post_data = $this->input->post();
		//echo '<pre>';print_r($post_data['party_id']);exit;
		$party_id = $post_data['party_id'];
		if ($party_id == "" || $party_id == "0") {
			echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
			exit;
		}
        
        $send_sms = 0;
        if(isset($post_data['send_sms'])){
            $send_sms = 1;
            unset($post_data['send_sms']);
            unset($_POST['send_sms']);
        }
        
		$post_data['party_id'] = isset($_POST['party_id']) ? $_POST['party_id'] : '';
		$post_data['referance_from'] = isset($_POST['referance_from']) ? $_POST['referance_from'] : '';
		$post_data['referance_no'] = isset($_POST['referance_no']) ? $_POST['referance_no'] : '';
		$post_data['payment_bank'] = isset($_POST['payment_bank']) ? $_POST['payment_bank'] : '';
		$post_data['payment_by'] = isset($_POST['payment_by']) ? $_POST['payment_by'] : '';
		$post_data['payment_by_no'] = isset($_POST['payment_by_no']) ? $_POST['payment_by_no'] : '';
		$post_data['receipt_date_2'] = isset($_POST['receipt_date_2']) ? date('Y-m-d',strtotime($_POST['receipt_date_2'])) : '';
		$post_data['payment_date'] = isset($_POST['payment_date']) ? date('Y-m-d',strtotime($_POST['payment_date'])) : '';
		$post_data['amount'] = isset($_POST['amount']) ? $_POST['amount'] : '';
		$post_data['payment_type'] = isset($_POST['payment_type']) ? $_POST['payment_type'] : '';
		$post_data['payment_note'] = isset($_POST['payment_note']) ? $_POST['payment_note'] : '';

		if(isset($post_data['payment_id']) && !empty($post_data['payment_id'])){
			$payment_id = $post_data['payment_id'];
			if (isset($post_data['payment_id']))
				unset($post_data['payment_id']);
			if (isset($post_data['payment_edit']))
				unset($post_data['payment_edit']);
			$post_data['updated_at'] = date('Y-m-d H:i:s');
			$post_data['updated_by'] = $this->staff_id;
			$this->db->where('payment_id', $payment_id);
			$result = $this->db->update('payment', $post_data);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Payment Updated Successfully');				
			}
		} else {
			$row = $this->crud->getFromSQL('SELECT MAX(receipt_no) AS `receipt_no` FROM `payment`');
			$maxid = $row[0]->receipt_no + 1;
			$post_data['receipt_no'] = $maxid;
			$dataToInsert = $post_data;
			$dataToInsert['created_at'] = date('Y-m-d H:i:s');
			$dataToInsert['created_by'] = $this->staff_id;
			// Unset quotation_id from Inquiry Data
			if (isset($dataToInsert['payment_id']))
				unset($dataToInsert['payment_id']);
			if (isset($dataToInsert['payment_edit']))
				unset($dataToInsert['payment_edit']);

			$result = $this->db->insert('payment', $dataToInsert);
			$payment_id = $this->db->insert_id();
			if($result){
				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Payment Added Successfully');				
			}
		}
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['party_id']);
            $this->applib->send_sms('payment', $payment_id, $party_phone_no, SEND_PAYMENT_RECEIPT_SMS_ID);
        }
		print json_encode($return);
		exit;
	}

	function payment_list(){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		if($this->app_model->have_access_role(PAYMENT_MODULE_ID, "view")){
			set_page('account/payment/payment_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function payment_list_datatable(){
		$post_data = $this->input->post();
		$search_key = ''; 
		if($post_data != ''){
			$search_key['party_id'] = $post_data['party_id'];
			$search_key['from_date'] = $post_data['from_date'];
			$search_key['to_date'] = $post_data['to_date'];
		}

		$config['table'] = 'payment';
		$config['select'] = 'payment.*,party.party_name,so.sales_order_no,quo.quotation_no,com.complain_no_year';
		$config['column_search'] = array('payment.receipt_no','party.party_name','so.sales_order_no','quo.quotation_no','com.complain_no_year','payment.payment_bank','amount','DATE_FORMAT(payment_date,"%d-%m-%Y")');
		$config['column_order'] = array(null, 'payment.receipt_no', 'party.party_name','so.sales_order_no,quo.quotation_no,com.complain_no_year','payment.payment_bank','amount','payment_date', );
		$config['joins'][] = array('join_table' => 'party', 'join_by' => 'party.party_id = payment.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales_order so', 'join_by' => 'so.id = payment.referance_no', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotations quo', 'join_by' => 'quo.id = payment.referance_no', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'complain com', 'join_by' => 'com.complain_id = payment.referance_no', 'join_type' => 'left');

		//$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => 0);
		$config['where_string'] = ' party.active != 0 ';

		if($search_key['party_id'] != ''){
			$config['where_string'] .= ' AND party.party_id = '. $search_key['party_id'];
		}

		if($search_key['from_date'] != ''){
			$config['where_string'] .= ' AND payment_date >= "'. date('Y-m-d', strtotime($search_key['from_date'])) .'" ';
		}

		if($search_key['to_date'] != ''){
			$config['where_string'] .= ' AND payment_date <= "'.  date('Y-m-d', strtotime($search_key['to_date'])) .'" ';
		}

		$config['order'] = array('party.party_name' => 'ASC','payment_date' => 'DESC');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();

		foreach ($list as $payment) {
			$row = array();
			$action = '';
			$isEdit = $this->app_model->have_access_role(PAYMENT_MODULE_ID, "edit");
			$isDelete = $this->app_model->have_access_role(PAYMENT_MODULE_ID, "delete");

			if ($isEdit) {
				$action .= '<a href="' . base_url("payment/add/" . $payment->payment_id ) . '" class="btn btn-xs btn-primary btn-edit-payment" data-payment_id="' . $payment->payment_id . '"><i class="fa fa-edit"></i></a>';
			}
			if ($isDelete) {
				$action .= ' <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_button" data-href="' . base_url('payment/delete/'.$payment->payment_id) . '"><i class="fa fa-trash"></i></a>';
			}
			if ($payment->payment_type != 'debit' && $payment->is_approved == 1) {
//				$action .= ' <a href="'.base_url('payment/payment_receipt/' . $payment->payment_id).'" target="_blank" class="btn btn-primary btn-xs" ><i class="fa fa-print"></i></a> ';
				$action .= '<form id="print_' . $payment->payment_id . '" method="post" action="' .base_url('payment/payment_receipt').'" style="width: 25px; display: initial;" target="_blank">
                            <input type="hidden" name="payment_id" id="payment_id" value="' . $payment->payment_id . '">
                            <a class="btn btn-primary btn-xs" href="javascript:{}" onclick="document.getElementById(\'print_' . $payment->payment_id . '\').submit();"><i class="fa fa-print"></i></a>
                        </form> ';
			}
			$row[] = $action;
			$row[] = $payment->receipt_no;
			$row[] = $payment->party_name;
			if($payment->referance_from == 'sales_order'){
				$row[] = $payment->sales_order_no;	
			}elseif($payment->referance_from == 'quotation'){
				$row[] = $payment->quotation_no;
			} elseif($payment->referance_from == 'complain'){
				$row[] = $payment->complain_no_year;
			}
			$row[] = $payment->payment_bank;
			$row[] = money_format('%i',$payment->amount);
			$row[] = date('d-m-Y',strtotime($payment->payment_date));
			$row[] = $payment->payment_type;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function payment_receipt()	{
        $id = $_POST['payment_id'];
        if(!empty($id)){
            $payment_data = $this->get_payment_detail($id);
            $company_details = $this->applib->get_company_detail();
            $payment_declaration = $this->crud->get_id_by_val('terms_and_conditions','detail','module','payment_declaration');
            //pre_die($payment_data);
            $this->load->library('m_pdf');
            $param = "'utf-8','A4'";
            $pdf = new mPDF('utf-8', array(210,180));
    //		$pdf = $this->m_pdf->load($param);
            $margin_left = $company_details['margin_left'];
            $margin_right = $company_details['margin_right'];
            $margin_top = $company_details['margin_top'];
            $margin_bottom = $company_details['margin_bottom'];
            $page_detail_html = $this->load->view('account/payment/payment_receipt', array('payment_data' => $payment_data , 'payment_declaration' => $payment_declaration), true);

            $pdf->AddPage(
                'P', //orientation
                '', //type
                '', //resetpagenum
                '', //pagenumstyle
                '', //suppress
                $margin_left, //margin-left
                $margin_right, //margin-right
                $margin_top, //margin-top
                $margin_bottom, //margin-bottom
                0, //margin-header
                0 //margin-footer
            );
            $pdf->WriteHTML($page_detail_html);
            $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
            $pdfFilePath = "payment_receipt.pdf";
            //pre_die($pdf);

            $pdf->Output($pdfFilePath, "I");
        }
	}

	function get_payment_detail($id){
		$select = "payment.*,party.party_name,so.sales_order_no,quo.quotation_no,com.complain_no_year,pb.name AS payment_by,";
		$select .= "c.city,s.state,reciver.name AS reciver_name,approver.name AS approver_name,";
		$select .= "reciver.signature AS reciver_signature,approver.signature AS approver_signature";
		$this->db->select($select);
		$this->db->from('payment');
		$this->db->join('party party', 'payment.party_id=party.party_id', 'left');
		$this->db->join('payment_by pb', 'pb.id=payment.payment_by', 'left');
		$this->db->join('sales_order so', 'so.id = payment.referance_no', 'left');
		$this->db->join('quotations quo', 'quo.id = payment.referance_no', 'left');
		$this->db->join('complain com', 'com.complain_id = payment.referance_no', 'left');
		$this->db->join('city c', 'c.city_id = party.city_id', 'left');
		$this->db->join('state s', 's.state_id = party.state_id', 'left');
		$this->db->join('staff reciver', 'reciver.staff_id = payment.reciver_id', 'left');
		$this->db->join('staff approver', 'approver.staff_id = payment.approved_by', 'left');
		
		$this->db->where('payment.payment_id', $id);
		$this->db->where('party.active !=', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->row();
                        //echo "<pre>"; print_r($result); exit;
			if($result->referance_from == 'sales_order'){
				$quotation_id = $this->crud->get_id_by_val('sales_order','quotation_id','id',$result->referance_no);
				$sales_order_date = $this->crud->get_id_by_val('sales_order','sales_order_date','id',$result->referance_no);
				$quotation_no = $this->crud->get_id_by_val('quotations','quotation_no','id',$quotation_id);
				$quotation_date = $this->crud->get_id_by_val('quotations','quotation_date','id',$quotation_id);
				$complain_date = $this->crud->get_id_by_val('complain','complain_date','complain_id',$result->referance_no);
                                

				$result->sales_order_date = $sales_order_date;
				$result->referance_no = $result->sales_order_no;
				$result->sales_order_quotation_no = $quotation_no;
				$result->sales_order_quotation_date = $quotation_date;
			}elseif($result->referance_from == 'quotation'){
				$result->referance_no = $result->quotation_no;
				$result->sales_order_no = '';
			}elseif($result->referance_from == 'complain'){
				$complain_date = $this->crud->get_id_by_val('complain','complain_date','complain_id',$result->referance_no);
				$result->referance_no = $result->complain_no_year;
                $result->complain_date = $complain_date;
                $result->sales_order_no = '';
				
			}
			//pre_die($result);
			return $result;
		} else {
			return array();
		}
	}

	function update_payment_status_to_approve(){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		if ($_POST['payment_id'] != '') {
			$payment_id = $_POST['payment_id'];
			$approve_val = $this->crud->get_id_by_val('payment','is_approved','payment_id',$payment_id);
			if($approve_val == 0){
				$data['approved_by'] = $this->session->userdata('is_logged_in')['staff_id'];
				$data['approved_at'] = date('Y-m-d H:i:s');
				$data['is_approved'] = 1;
				$msg = 'Payment Receipt approved successfully!';
			}elseif ($approve_val == 1) {
				$data['approved_by'] = null;
				$data['approved_at'] = null;
				$data['is_approved'] = 0;
				$msg = 'Payment Receipt disapproved successfully!';
			}else{
				$msg = 'Error Occu !';
			}
			$result = $this->crud->update('payment', $data, array('payment_id' => $payment_id));
			if($result){
				echo json_encode(array('success' => true, 'message' => $msg, 'status' => $data['is_approved']));
			}else{
				echo json_encode(array('success' => true, 'message' => 'some error occurred !', 'status' => $data['is_approved']));	
			}
			exit();
		} else {
			redirect("sales_order/order_list"); exit;
			exit();
		}
	}
	
	function get_party_data_by_refrance(){
		$referance_from = $_POST['referance_from'];
		if($referance_from = 'sales_order'){
			//$party_ids = $this->crud->get_column_value_implode('sales_order','sales_to_party_id');	
			$party_ids = $this->crud->get_columns_val_by_where('sales_order','sales_to_party_id',array('sales_to_party_id !=' => null));
		}elseif($referance_from = 'po_order'){
			
		}
		$res = array();
		$res = $this->app_model->array_flatten($party_ids);
		//echo '<pre>';print_r($this->db->last_query());
		echo '<pre>';print_r($res);
		//$res = $this->array_flatten($party_ids);
		//echo '<pre>';print_r($res);
		//echo json_encode(array('success' => true, 'party_ids' => $party_ids));
		exit();
	}
	
	function delete($id='')	{
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Deleted Successfully');
	}
}