<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Sheet
 * &@property Crud $crud
 */
class Sheet extends CI_Controller
{
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
	}
	
	function google_sheet($id)
    {
        $staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        if (!count($this->crud->get_row_by_id('google_sheet_staff', array('google_sheet_id' => $id, 'staff_id' => $staff_id))) > 0) {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect('');
        }
        $sheet_data = $this->crud->get_row_by_id('google_sheets', array('id' => $id));
        $sheet_data = $sheet_data['0'];
        set_page('sheet/google_sheet',array('sheet_data'=>$sheet_data));
    }
    
    function add_google_sheet(){
        $id = $this->input->get('id');
        //print_r($id) ; exit;
        if ($id != "" && $id > 0) {
            $this->db->select('*');
            $this->db->where('id', $id);
            $sheet = $this->db->get('google_sheets')->result_array();

            if (!count($sheet) > 0)
                return;
            //$this->db->order_by('id');
            $this->db->select('staff_id');
            $this->db->where('google_sheet_id', $id);
            $rows = $this->db->get('google_sheet_staff')->result();
            $i = 0;        
            foreach($rows as $row)
            {
                $sheet_user[$i] = $row->staff_id;
                $i++;        
            }    

            //print_r($sheet_user) ; exit;
            $users = $this->crud->getFromSQL("SELECT * FROM staff WHERE active != '0' ORDER BY name ASC");
            $data = array(
                'sheet' => $sheet,
                'sheet_user' => $sheet_user,
                'users' => $users,
                'edit' => 1
            );
            
            if($this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "edit")){
                set_page('sheet/add_google_sheet', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
        }else{
            $data = array();
            $data['users'] = $this->crud->getFromSQL("SELECT * FROM staff WHERE active != '0' ORDER BY name ASC");
            if($this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "add")){
                set_page('sheet/add_google_sheet', $data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect("/");
            }
            
        }
        
        
	}

    function save_sheet()
    {
        $status = 1;
        $msg = "Saved Successfully!";
        $post_data = $this->input->post();
        $sheetName = $post_data['name'];
        $sheetUrl = $post_data['url'];
        $sheetSequence = $post_data['sequence'];
        
        
        
        //pre_die($post_data);
        $google_sheet_id = $post_data['sheet_id'];
        $staff_ids = $post_data['staff_ids'];
        if(!empty($google_sheet_id)) {
            
            
            $sheet_data = array(
                'name' => $post_data['name'],
                'url' => $post_data['url'],
                'sequence' => $post_data['sequence']
            );
            $this->crud->update('google_sheets',$sheet_data, array('id' => $google_sheet_id));
            $this->crud->delete('google_sheet_staff', array('google_sheet_id' => $google_sheet_id));
            //echo $this->db->last_query();exit;

            if ($status == 1) {
                if (!empty($staff_ids) && is_array($staff_ids)) {
                    foreach ($staff_ids as $staff_id) {
                        $this->crud->insert('google_sheet_staff', array('google_sheet_id' => $google_sheet_id,'staff_id' => $staff_id, 'created_at' => date('Y-m-d H:i:s')));
                    }
                }    
            }
            
        } else {
            
            if ($sheetName != "") {
                $sheetName = strtolower($sheetName);
            }
            // Sheet name unique validatoin
            $sql_name_query = "SELECT * FROM google_sheets WHERE TRIM(LOWER(name)) = '" . addslashes($sheetName) . "'";
            $query_name = $this->db->query($sql_name_query);
            $rows_name = $query_name->result_array();
            if (count($rows_name) > 0) {
                $status = 0;
                $msg = "Sheet name already exists.";
            }
            
            
            if ($sheetUrl != "") {
                $sheetUrl = strtolower($sheetUrl);
            }
            // Sheet Url unique validatoin
            $sql_url_query = "SELECT * FROM google_sheets WHERE TRIM(LOWER(url)) = '" . addslashes($sheetUrl) . "'";
            $query_url = $this->db->query($sql_url_query);
            $rows_url = $query_url->result_array();
            if (count($rows_url) > 0) {
                $status = 0;
                $msg = "Google Sheet already exists.";
            }
            
            
            if ($sheetSequence != "") {
                $sheetSequence = strtolower($sheetSequence);
            }
            // Sheet Sequance unique validatoin
            $sql_sequence_query = "SELECT * FROM google_sheets WHERE TRIM(LOWER(sequence)) = '" . addslashes($sheetSequence) . "'";
            $query_sequence = $this->db->query($sql_sequence_query);
            $rows_sequence = $query_sequence->result_array();
            if (count($rows_sequence) > 0) {
                $status = 0;
                $msg = "Sequence already exists.";
                $sequence_status == 0; 
            }
            
            
            
            $sheet_data = array(
                'name' => $post_data['name'],
                'url' => $post_data['url'],
                'sequence' => $post_data['sequence'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            
            if ($status == 1) {
                $google_sheet_id = $this->crud->insert('google_sheets', $sheet_data);
                if (!empty($staff_ids) && is_array($staff_ids)) {
                    foreach ($staff_ids as $staff_id) {
                        $this->crud->insert('google_sheet_staff', array('google_sheet_id' => $google_sheet_id,'staff_id' => $staff_id, 'created_at' => date('Y-m-d H:i:s')));
                    }
                }
            }
            
        }
        echo json_encode(array("status" => $status, "msg" => $msg, "sheet_id"=>$google_sheet_id));
        exit;
    }
    
    function update(){
        
        $status = 1;
        $msg = "Roles has been updated successfully.";
        $user_id = $this->input->post("user_id");
        
        if(intval($user_id) > 0){
            $roles = $this->input->post("roles");
            
            // delete old roles
            $sql = "DELETE FROM staff_roles WHERE staff_id='$user_id'";
            $this->crud->execuetSQL($sql);
            
            // echo "<pre>";print_r($roles);exit;
            
            // add new roles
            if(is_array($roles) && count($roles) > 0){                
                foreach($roles as $module_id => $role_id){
                    $tmp = explode("_", $module_id);
                    $module_id = $tmp[1];
                    
                    $dataToInsert = array(
                      'staff_id' => $user_id,
                      'module_id' => $module_id,  
                      'role_id' => $role_id,
                    );
                    $this->crud->insert("staff_roles", $dataToInsert);
                }    
            }    
        }else{
            $status = 0;
            $msg = "Please select staff user.";
        }
        
        echo json_encode(array("status" => $status,"msg" => $msg));
        exit;
    }
    
    function google_sheet_list(){
        $data = array();
        $data['users'] = $this->crud->getFromSQL("SELECT * FROM staff WHERE active != '0'");
        if($this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "view")){
            set_page('sheet/google_sheet_list');
        } else {
            $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
            redirect("/");
        }
        
    }
    
    function sheet_list_datatable() {
        $staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $config['table'] = 'google_sheets gs';
        $config['select'] = 'gs.id, gs.name, gs.url, gs.sequence, gss.staff_id';
        $config['joins'][] =  array('join_table' => 'google_sheet_staff gss', 'join_by' => 'gss.google_sheet_id = gs.id','join_type'=>'left');
        $config['column_search'] = array('gs.id','gs.name','gs.sequence','gs.url');
        $config['column_order'] = array('gs.id','gs.name','gs.sequence','gs.url');
        $config['order'] = array('gs.name' => 'ASC');
        $config['wheres'][] = array('column_name' => 'gss.staff_id','column_value'=>$staff_id);
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $login_user_sheet_view = $this->applib->get_login_user_google_sheet();
        $data = array();
        foreach ($list as $sheet) {
            $row = array();
            $action = '';
            $isEdit = $this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "edit");
            $isDelete = $this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "delete");
            if ($isEdit) {
                $action .= '<a href="' . base_url("sheet/add_google_sheet?id=" . $sheet->id."&edit") . '" class="btn btn-xs btn-primary btn-edit-sheet" data-sheet_id="' . $sheet->id . '"><i class="fa fa-edit"></i></a>';
            }
            if ($isDelete) {
                $action .= ' <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_button" data-href="' . base_url('sheet/delete_sheet/' . $sheet->id) . '"><i class="fa fa-trash"></i></a>';
            }
            $row[] = $action;
            $row[] = $sheet->name;
            $row[] = $sheet->sequence;
            $row[] = $sheet->url;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function delete_sheet($sheet_id)
    {
        $this->crud->delete('google_sheets', array('id' => $sheet_id));
        $this->session->set_flashdata('success', true);
        $this->session->set_flashdata('message', 'Deleted Successfully');
        echo json_encode(array('success'=>true,'message'=>"sheet deleted successfully!"));
        exit();
    }
	
}
