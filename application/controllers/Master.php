<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Master
 * &@property Crud $crud
 * &@property AppLib $applib
 */
class Master extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
        $this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
	}

	/* function index()
	{
		
	} */

	function company_detail()
	{
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id', COMPANY_ID);
		$this->db->limit(1);
		$query = $this->db->get();
        
		if ($query->num_rows() > 0) {
			$results = $query->row_array(0);
            set_page('company_detail/add', $results);
		} else {
			redirect(base_url());
		}
	}

	function google_sheet()
	{
		set_page('google_sheet');
	}

	function daily_work_entry()
	{
		if(isset($_POST['select_user']) && !empty($_POST['select_user'])){
			$userid = $_POST['select_user'];
		} else {
			$logged_in = $this->session->userdata("is_logged_in");
			$userid = $logged_in['staff_id'];
		}
		$username = $logged_in['name'];
		$activity_details = '';
		$recordid = 0;
		$this->db->select('*');
		$this->db->from('daily_work_entry');
		$this->db->where('userid', $userid);
		$this->db->where('entry_date = CURDATE()');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$results = $query->row_array(0);
			$activity_details = $results['activity_details'];
			$recordid = $results['id'];
		}
		$data['users'] = $this->crud->getFromSQL("SELECT * FROM staff WHERE active != '0' ORDER BY name ASC");
		$data['userid'] = $userid;
		$data['username'] = $username;
		$data['activity_details'] = $activity_details;
		$data['recordid'] = $recordid;
		set_page('daily_work_entry', $data);
	}
	
	function add_daily_work_entry()
	{
		if ($this->input->post('userid')) {
			$post_data = $this->input->post();
			$userid = $post_data['userid'];
			$data = array(
				'userid' => $userid,
				'username' => $post_data['username'],
				'entry_date' => date('Y-m-d', strtotime($post_data['entry_date'])),
				'activity_details' => $post_data['activity_details'],
				'created_date' => date('Y-m-d H:i:s'),
			);
			$this->db->select('*');
			$this->db->from('daily_work_entry');
			$this->db->where('userid', $userid);
			$this->db->where('entry_date', date('Y-m-d', strtotime($post_data['entry_date'])));
			$this->db->limit(1);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$this->crud->update("daily_work_entry", array('activity_details' => $data['activity_details']), array('id' => $query->row()->id));
			} else {
				$this->crud->insert("daily_work_entry", $data);
			}
		}
		echo json_encode(array('success' => true, 'message' => 'Work detail successfully saved.'));
		exit();
	}
	
	function get_daily_work_entry()
	{
		if(isset($_POST['select_user']) && !empty($_POST['select_user'])){
			$userid = $_POST['select_user'];
		} else {
			$logged_in = $this->session->userdata("is_logged_in");
			$userid = $logged_in['staff_id'];
		}
		$this->db->select('*');
		$this->db->from('daily_work_entry');
		$this->db->where('userid', $userid);
		$this->db->where('entry_date = "'.date('Y-m-d', strtotime($_POST['entry_date'])).'" ');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$results = $query->row_array(0);
			$activity_details = $results['activity_details'];
			$recordid = $results['id'];
		}
		echo json_encode(array('success' => true, 'activity_details' => $activity_details));
		exit();
	}

	function add_company($id)
	{
		$company = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$company = $this->crud->get_all_with_where('company', 'name', 'ASC', $where_array);
            $result_data['id'] = $company[0]->id;
			$result_data['name'] = $company[0]->name;
			$result_data['address'] = $company[0]->address;
			$result_data['contact_no'] = $company[0]->contact_no;
			$result_data['pan_no'] = $company[0]->pan_no;
			$result_data['tan_no'] = $company[0]->tan_no;
			$result_data['pf_no'] = $company[0]->pf_no;
			$result_data['esic_no'] = $company[0]->esic_no;
			$result_data['pt_no'] = $company[0]->pt_no;
			$result_data['service_tax_no'] = $company[0]->service_tax_no;
			$result_data['vat_no'] = $company[0]->vat_no;
			$result_data['cst_no'] = $company[0]->cst_no;
			$result_data['end_use_code'] = $company[0]->end_use_code;
			$result_data['lut'] = $company[0]->lut;
		}
		set_page('company_detail/add', $result_data);
	}

	function update_company()
	{
		$post_data = $this->input->post();
        $post_data['tds_per'] = isset($post_data['tds_per']) && !empty($post_data['tds_per']) ? $post_data['tds_per'] : NULL;
                if (!empty($_FILES['authorized_signatory_image']['name'])) {
			$post_data['authorized_signatory_image'] = $this->applib->upload_image('authorized_signatory_image', image_dir('staff/signature/'), false);				
			if(isset($post_data['authorized_signatory_image']['error'])){
				unset($post_data['authorized_signatory_image']);
			}
		}else if (!empty($post_data['authorized_signatory_image_old'])) {
			$post_data['authorized_signatory_image'] = $post_data['authorized_signatory_image_old'];
		} else {
			$post_data['authorized_signatory_image'] = null;
		}
		unset($post_data['authorized_signatory_image_old']);
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('company', $post_data, array('id' => $post_data['id']));
		if ($result) {
            $this->crud->update('staff', array('pf_per' => $post_data['c_pf_per'], 'esi_per' => $post_data['c_esi_per']), array());
            $this->crud->execuetSQL('UPDATE `staff` SET `pf_amount`= `basic_pay` * `pf_per` / 100');
            $this->crud->execuetSQL('UPDATE `staff` SET `esi_amount`= `salary` * `esi_per` / 100');
            $this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Company Updated Successfully');
			$post_data['id'] = $result;
			echo json_encode($post_data);
		}
	}

	function user()
	{
		$result_data = $this->crud->get_all_records('users', 'id', 'ASC');
		set_page('user/index', array('results' => $result_data));
	}

	function add_user()
	{
		set_page('user/add');
	}

	function save_company()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('company', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Company Added Successfully');
			echo json_encode($post_data);
		}
	}

	function save_user()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$post_data['password'] = md5($post_data['password']);
		unset($post_data['c_password']);
		$result = $this->crud->insert('users', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			echo json_encode($post_data);
		}
	}

	function agent($id = '')
	{
		$agent = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$agent = $this->crud->get_all_with_where('agent', 'agent_name', 'ASC', $where_array);
		}
		$result_data = $this->crud->get_agent_list();

		$return = array();
		$return['results'] = $result_data;
		$return['id'] = ($agent) ? $agent[0]->id : '';
		$return['agent_name'] = ($agent) ? $agent[0]->agent_name : '';
		$return['company_name'] = ($agent) ? $agent[0]->company_name : '';
		$return['commission'] = ($agent) ? $agent[0]->commission : '';
		$return['party_type_1'] = ($agent) ? $agent[0]->party_type_1 : '';
		$return['address'] = ($agent) ? $agent[0]->address : '';
		$return['city'] = ($agent) ? $agent[0]->city: '';
		$return['state'] = ($agent) ? $agent[0]->state: '';
		$return['country'] = ($agent) ? $agent[0]->country: '';
		$return['phone_no'] = ($agent) ? $agent[0]->phone_no : '';
		$return['email'] = ($agent) ? $agent[0]->email : '';
		$return['note'] = ($agent) ? $agent[0]->note : '';
		$return['fix_commission'] = ($agent) ? $agent[0]->fix_commission : '';
		set_page('agent/index', $return);
	}
	
	
	function agent_datatable(){
		$config['table'] = 'agent a';
		$config['select'] = 'a.*, p.type,s.name,c.city,st.state,ct.country';
		$config['column_order'] = array(null, 'a.agent_name','a.company_name','a.address','a.city','a.state','a.country','a.phone_no','a.email' ,'a.note','p.type' ,'a.fix_commission','a.commission','s.name');
		$config['column_search'] = array('a.agent_name','a.company_name','a.address','a.phone_no','a.email' ,'a.note','p.type' ,'a.fix_commission','a.commission','s.name');
		$config['joins'][] = array('join_table' => 'party_type_1 p', 'join_by' => 'p.id = a.party_type_1', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city c', 'join_by' => 'c.city_id = a.city', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state st', 'join_by' => 'st.state_id = a.state', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country ct', 'join_by' => 'ct.country_id = a.country', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'staff s', 'join_by' => 's.staff_id = a.created_by', 'join_type' => 'left');
		$config['order'] = array('a.agent_name' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$isEdit = $this->app_model->have_access_role(MASTER_AGENT_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_AGENT_MENU_ID, "delete");
		foreach ($list as $agents) {
			$row = array();
			$action = '';
			if($isEdit){
				$action .= '<a href="' . base_url('master/agent/' . $agents->id) . '" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
			} 
			if($isDelete){
				$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'.base_url('master/delete/'.$agents->id).'"><i class="fa fa-trash"></i></a>';
			}
			$row[] = $action;
			$row[] = $agents->agent_name; 
			$row[] = $agents->company_name; 
			$row[] = $agents->address; 
			$row[] = $agents->city; 
			$row[] = $agents->state; 
			$row[] = $agents->country; 
			$row[] = $agents->phone_no; 
			$row[] = $agents->email; 
			$row[] = $agents->note; 
			$row[] = $agents->type; 
			$row[] = $agents->fix_commission; 
			$row[] = $agents->commission; 
			$row[] = $agents->name; 
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}	

	function add_agent()
	{
		if(!$this->applib->have_access_role(MASTER_AGENT_MENU_ID,"add")) {
			$this->session->set_flashdata('error_message', 'You have not permission to add Agent.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$post_data['created_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);

		$result = $this->crud->insert('agent', $post_data);
		if ($result) {
			$post_data['id'] = $result;
			$post_data['created_by'] = trim($this->session->userdata('is_logged_in')['staff_id']);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Agent Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_agent()
	{
		if(!$this->applib->have_access_role(MASTER_AGENT_MENU_ID,"edit")) {
			$this->session->set_flashdata('error_message', 'You have not permission to edit Agent.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$where1['id'] = $post_data['party_type_1'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('agent', $post_data, array('id' => $post_data['id']));
		if ($result) {
			$post_data['id'] = $result;
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Agent Updated Successfully');
			echo json_encode($post_data);
		}
	}

	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table, array($id_name => $id));
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Deleted Successfully');
	}

	function edit_quotation_item($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$quotation_items = $this->crud->get_items_detail_by_quotation($id_name);
		//echo '<pre>'; print_r($quotation_items); echo '</pre>'; exit;
	}

	function follow_history_datatable()
	{
		$entry_date = date('Y-m-d', strtotime($_POST['entry_date']));
		if(isset($_POST['select_user']) && !empty($_POST['select_user'])){
			$staff_id = $_POST['select_user'];
		} else {
			$staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		}
		$query = "SELECT 'Quotation' as t_type,fh.history,fh.followup_date,p.party_name,p.phone_no,p.email_id FROM followup_history fh INNER JOIN quotations q ON q.id = fh.quotation_id INNER JOIN party p ON p.party_id = q.party_id WHERE fh.followup_by = '$staff_id' AND fh.followup_date = '$entry_date' ";
		$query .= " UNION ALL SELECT 'Inquiry',ifh.history,ifh.followup_date,p.party_name,p.phone_no,p.email_id FROM inquiry_followup_history ifh INNER JOIN inquiry i ON i.inquiry_id = ifh.inquiry_id INNER JOIN party p ON p.party_id = i.party_id WHERE ifh.followup_by = '$staff_id' AND ifh.followup_date = '$entry_date'";
		$column_search = array('fh.history', 'p.party_name', 'p.phone_no', 'p.email_id');
		$i = 0;
		$query = $this->db->query($query);


		$result_data = $query->result();
		$data = array();
		$inc = 1;
		foreach ($result_data as $party) {
			$row = array();
			$row[] = $inc;
			$row[] = $party->t_type;
			$row[] = $party->party_name;
			$row[] = $party->phone_no;
			$row[] = $party->email_id;
			$row[] = $party->history;
			$data[] = $row;
			$inc++;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $query->num_rows(),
			"recordsFiltered" => $query->num_rows(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function follow_history_datatable_v1()
	{
		$config['table'] = 'staff st';
		$config['select'] = 'fh.history,p.party_name,p.phone_no,p.email_id';
		$config['joins'][] = array('join_table' => 'followup_history fh', 'join_by' => 'fh.followup_by = st.staff_id');
		$config['joins'][] = array('join_table' => 'quotations', 'join_by' => 'quotations.id = fh.quotation_id');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = quotations.party_id');
		$config['column_search'] = array('fh.history', 'p.party_name', 'p.phone_no', 'p.email_id');
		$config['column_order'] = array('fh.history', 'p.party_name', 'p.phone_no', 'p.email_id');
		$config['wheres'][] = array('column_name' => 'fh.followup_date', 'column_value' => date('Y-m-d', strtotime($_POST['entry_date'])));
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $party) {
			$row = array();
			$row[] = $party->party_name;
			$row[] = $party->phone_no;
			$row[] = $party->email_id;
			$row[] = $party->history;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function get_usersent_mailcount()
	{
		$date = (isset($_POST['date'])) ? date('Y-m-d', strtotime($_POST['date'])) : date('Y-m-d');

		$staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$query = "SELECT count(*) as count FROM `mail_system` WHERE staff_id = $staff_id AND folder = 'outbox' AND DATE(received_at) = '$date'";
		$query = $this->db->query($query);

		$result_data = $query->row_array();
		echo json_encode($result_data);
	}

	function billing_terms()
	{
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS,"add")) {
			set_page('billing_terms/billing_terms');
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	/**
	 * @param $billing_terms_id
	 */
	function edit_billing_terms($billing_terms_id)
	{
		if(!$this->applib->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS,"edit")) {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$data['billing_terms_data'] = $this->db->get_where('billing_terms',array('id'=>$billing_terms_id),1)->row_array();
		$data['billing_terms_details'] = $this->crud->get_all_with_where('billing_terms_detail','id','desc',array('billing_terms_id'=>$billing_terms_id));
		set_page('billing_terms/billing_terms_edit',$data);
	}

	function save_billing_terms()
	{
		$billing_terms_data = array(
			'billing_terms_name'=>isset($_POST['billing_terms_name'])?$_POST['billing_terms_name']:'',
			'billing_terms_date'=>isset($_POST['billing_terms_date']) && strtotime($_POST['billing_terms_date']) > 0?date('Y-m-d',strtotime($_POST['billing_terms_date'])):date('Y-m-d'),
		);
		if(!empty($_POST['billing_terms_id'])){
			$billing_terms_id = $_POST['billing_terms_id'];
			$this->crud->update('billing_terms',$billing_terms_data,array('id'=>$_POST['billing_terms_id']));
		}else{
			$billing_terms_data['created_at'] = date('Y-m-d');
			$billing_terms_id = $this->crud->insert('billing_terms',$billing_terms_data);
		}
		echo json_encode(array('success'=>true,'message'=>'Billing terms saved success!','billing_terms_id'=>$billing_terms_id));
		exit();
	}

	/**
	 * @param $billing_terms_id
	 */
	function add_billing_terms_detail($billing_terms_id)
	{
		$billing_terms_detail_data = array(
			'billing_terms_id'=>$billing_terms_id,
			'cal_code'=>isset($_POST['cal_code'])?$_POST['cal_code']:'',
			'cal_definition'=>isset($_POST['cal_definition'])?$_POST['cal_definition']:'',
			'narration'=>isset($_POST['narration'])?$_POST['narration']:'',
			'percentage'=>(float) isset($_POST['percentage'])?$_POST['percentage']:0,
			'value'=>(float) isset($_POST['value'])?$_POST['value']:0,
			'gl_acc'=>isset($_POST['gl_acc'])?$_POST['gl_acc']:'',
			'created_at'=>date('Y-m-d'),
		);
		$terms_detail_id = $this->crud->insert('billing_terms_detail',$billing_terms_detail_data);
		if(!empty($_POST['cal_code_definition'])){
			$cal_code_definition = $_POST['cal_code_definition'];
			foreach($cal_code_definition as $key=>$cal_code_definition_row){
				$cal_code_definition_data = array(
					'billing_terms_detail_id'=>$terms_detail_id,
					'is_first_element'=>$key == 0?1:0,
					'cal_operation'=>isset($cal_code_definition_row['cal_operation'])?$cal_code_definition_row['cal_operation']:'',
					'cal_code_id'=>$cal_code_definition_row['cal_code_id'] == 'null' || empty($cal_code_definition_row['cal_code_id'])?$terms_detail_id:$cal_code_definition_row['cal_code_id'],
					'created_at'=>date('Y-m-d H:i:s'),
				);
				$this->crud->insert('cal_code_definition',$cal_code_definition_data);
			}
		}
		echo json_encode(array('success'=>true,'message'=>'Billing terms detail added success!','terms_detail_id'=>$terms_detail_id));
		exit();
	}

	/**
	 * @param $terms_detail_id
	 */
	function save_billing_terms_detail($terms_detail_id)
	{
		$billing_terms_detail_data = array(
			'cal_code'=>isset($_POST['cal_code'])?$_POST['cal_code']:'',
			'cal_definition'=>isset($_POST['cal_definition'])?$_POST['cal_definition']:'',
			'narration'=>isset($_POST['narration'])?$_POST['narration']:'',
			'percentage'=>(float) isset($_POST['percentage'])?$_POST['percentage']:0,
			'value'=>(float) isset($_POST['value'])?$_POST['value']:0,
			'gl_acc'=>isset($_POST['gl_acc'])?$_POST['gl_acc']:'',
		);
		if(!empty($_POST['cal_code_definition'])){
			$this->crud->delete('cal_code_definition',array('billing_terms_detail_id'=>$terms_detail_id));
			$cal_code_definition = $_POST['cal_code_definition'];
			foreach($cal_code_definition as $key=>$cal_code_definition_row){
				$cal_code_definition_data = array(
					'billing_terms_detail_id'=>$terms_detail_id,
					'is_first_element'=>$key == 0?1:0,
					'cal_operation'=>isset($cal_code_definition_row['cal_operation'])?$cal_code_definition_row['cal_operation']:'',
					'cal_code_id'=>$cal_code_definition_row['cal_code_id'] == 'null' || empty($cal_code_definition_row['cal_code_id'])?$terms_detail_id:$cal_code_definition_row['cal_code_id'],
					'created_at'=>date('Y-m-d H:i:s'),
				);
				$this->crud->insert('cal_code_definition',$cal_code_definition_data);
			}
		}
		$this->crud->update('billing_terms_detail',$billing_terms_detail_data,array('id'=>$terms_detail_id));
		echo json_encode(array('success'=>true,'message'=>'Billing terms detail saved success!','terms_detail_id'=>$terms_detail_id));
		exit();
	}

	/**
	 * @param $terms_detail_id
	 */
	function get_billing_terms_detail($terms_detail_id)
	{
		$terms_detail_data = $this->db->get_where('billing_terms_detail', array('id' => $terms_detail_id), 1)->row_array(0);
		$terms_detail_data['cal_definition_detail'] = $this->get_cal_code_definition($terms_detail_id);
		$terms_detail_data['terms_detail_id'] = $terms_detail_data['id'];
		//pre_die($terms_detail_data);
		echo json_encode(array("success" => true, 'message' => 'Data loaded success', 'terms_detail_data' => $terms_detail_data));
		exit();
	}

	/**
	 * @param $terms_detail_id
	 */
	function get_billing_terms_detail_v1($terms_detail_id)
	{
		$terms_detail_data = $this->db->get_where('billing_terms_detail', array('id' => $terms_detail_id), 1)->row_array(0);
		$cal_definition = trim($terms_detail_data['cal_definition']);
		$cal_definition = explode('|',$cal_definition);
		$cal_definition_detail = array();
		$cnt = 0;
		foreach($cal_definition as $key=>$cal_definition_row) {
			$cal_definition_row = explode(' ',$cal_definition_row);
			if ($key == 0) {
				$cal_definition_detail[] = array(
					'is_first_element' => true,
					'cal_operation' => '',
					'cal_code_id' => $cal_definition_row[0],
				);
			} else {
				$cal_definition_detail[] = array(
					'is_first_element' => false,
					'cal_operation' => $cal_definition_row[0],
					'cal_code_id' => $cal_definition_row[1],
				);
			}
		}
		$terms_detail_data['cal_definition_detail'] = $this->get_cal_code_definition($terms_detail_id);
		$terms_detail_data['terms_detail_id'] = $terms_detail_data['id'];
		//pre_die($terms_detail_data);
		echo json_encode(array("success" => true, 'message' => 'Data loaded success', 'terms_detail_data' => $terms_detail_data));
		exit();
	}

	function get_cal_code_definition($billing_terms_detail_id)
	{
		$this->db->select('ccd.is_first_element,ccd.cal_operation,ccd.cal_code_id');
		$this->db->from('cal_code_definition ccd');
		$this->db->join('billing_terms_detail btd', 'btd.id = ccd.cal_code_id');
		$this->db->where('ccd.billing_terms_detail_id', $billing_terms_detail_id);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$response[] = array(
					'is_first_element' => $row->is_first_element == 1?true:false,
					'cal_operation' => $row->cal_operation,
					'cal_code_id' => $row->cal_code_id,
				);
			}
		}
		return $response;
	}

	/**
	 * @param $terms_detail_id
	 */
	function delete_billing_terms_detail($terms_detail_id)
	{
		$this->crud->delete('billing_terms_detail',array('id'=>$terms_detail_id));
		echo json_encode(array('success'=>true,'message'=>'Billing terms detail removed success!'));
		exit();
	}

	function billing_terms_list()
	{
		if($this->applib->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS,"view")) {
			set_page('billing_terms/billing_terms_list');
		}else{
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
	}

	function billing_terms_template_code_datatable($billing_terms_template_id = null)
	{
		$config['table'] = 'billing_terms_detail btd';
		$config['select'] = 'btd.*';
		$config['column_search'] = array('btd.cal_code','btd.narration','btd.percentage');
		$config['column_order'] = array('btd.cal_code','btd.narration','btd.percentage');
		$config['order'] = array('id' => 'DESC');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$sr_no = 1;
		foreach ($list as $billing_terms) {
			$row = array();
			$row[] = $sr_no;
			$row[] = $billing_terms->cal_code;
			$row[] = $billing_terms->narration;
			$row[] = "Defi";
			$row[] = $billing_terms->percentage;
			$data[] = $row;
			$sr_no++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function billing_terms_datatable()
	{
		$config['table'] = 'billing_terms';

		$config['select'] = 'id,billing_terms_name,billing_terms_date';
		$config['column_search'] = array('billing_terms_name','billing_terms_date');
		$config['column_order'] = array('billing_terms_name','billing_terms_date');
		$config['order'] = array('id' => 'DESC');
		$this->load->library('datatables', $config, 'datatable');
		$isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "delete");
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $billing_terms) {
			$row = array();
			$action = '';
			if($isEdit){
				$action .= '<a href="'.base_url().'master/edit_billing_terms/'.$billing_terms->id.'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';	
			}
			if($isDelete){
				$action .= ' <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_button" data-href="' . base_url('master/delete_billing_terms/' . $billing_terms->id) . '"><i class="fa fa-trash"></i></a>';	
			}
			$row[] = $action;
			$row[] = $billing_terms->billing_terms_name;
			$row[] = date('d-m-Y',strtotime($billing_terms->billing_terms_date));
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	/**
	 * @param $billing_terms_id
	 */
	function delete_billing_terms($billing_terms_id)
	{
		$this->crud->delete('billing_terms', array('id' => $billing_terms_id));
		echo json_encode(array('success' => true, 'message' => "Billings terms delete successfully!"));
		exit();
	}
    
    function bom_add($bom_id = ''){
        $data['projects'] = $this->crud->get_all_records('purchase_project','project_id','project_name');
        
        if(!empty($bom_id)){
			if ($this->app_model->have_access_role(MASTER_BOM_MENU_ID, "edit")) {
				$bom_data = $this->crud->get_row_by_id('bom_master', array('bom_id' => $bom_id));
				if(empty($bom_data)){
					redirect("master/bom_list"); exit;
				}
				$bom_data = $bom_data[0];
                $bom_data->project_item_group = $this->crud->get_column_value_by_id('items', 'item_group', array('id' => $bom_data->sales_item_id));
                $bom_data->item_code = $this->crud->get_column_value_by_id('items', 'item_code1', array('id' => $bom_data->sales_item_id));
                $bom_data->effective_date = substr($bom_data->effective_date, 8, 2) .'-'. substr($bom_data->effective_date, 5, 2) .'-'. substr($bom_data->effective_date, 0, 4);
                $bom_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $bom_data->created_by));
				$bom_data->created_at = substr($bom_data->created_at, 8, 2) .'-'. substr($bom_data->created_at, 5, 2) .'-'. substr($bom_data->created_at, 0, 4);
				$bom_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $bom_data->updated_by));
				$bom_data->updated_at = substr($bom_data->updated_at, 8, 2) .'-'. substr($bom_data->updated_at, 5, 2) .'-'. substr($bom_data->updated_at, 0, 4);
				$data['bom_data'] = $bom_data;
                
                $where = array('bom_id' => $bom_id);
				$data['bom_item_expenses'] = $this->crud->get_row_by_id('bom_item_expenses', $where);
				$data['bom_projects'] = $this->crud->get_row_by_id('bom_projects', $where);
                
                $project_lineitems = array();
                $bom_projects = $this->crud->get_row_by_id('bom_projects', $where);
                foreach($bom_projects as $bom_project){
                    $bom_project->bom_project_id = $bom_project->bom_project_id;
                    $bom_project->project_id = $bom_project->project_id;
                    $bom_project->project_name = $this->crud->get_column_value_by_id('purchase_project', 'project_name', array('project_id' => $bom_project->project_id));
                    $bom_project->project_qty = $bom_project->project_qty;
					$project_lineitems[] = json_encode($bom_project);
				}
				$data['bom_projects'] = implode(',', $project_lineitems);
                
                $lineitems = array();
                $bom_item_details = $this->crud->get_row_by_id('bom_item_details', $where);
                foreach($bom_item_details as $bom_item_detail){
                    $bom_item_detail->lineitem_id = $bom_item_detail->id;
                    $bom_item_detail->item_code = $this->crud->get_column_value_by_id('purchase_items', 'item_code', array('id' => $bom_item_detail->item_id));
                    $bom_item_detail->item_name = $this->crud->get_column_value_by_id('purchase_items', 'item_name', array('id' => $bom_item_detail->item_id));
                    $bom_item_detail->project_name = (!empty($bom_item_detail->project_id)) ? $this->crud->get_column_value_by_id('purchase_project', 'project_name', array('project_id' => $bom_item_detail->project_id)) : '';
                    if(empty($bom_item_detail->project_name)){
                        $bom_item_detail->item_group = $this->crud->get_column_value_by_id('items', 'item_group', array('id' => $bom_data->sales_item_id));
                    }
                    $bom_item_detail->set_qty = $bom_item_detail->basic_qty;
                    $bom_item_detail->uom_id = $bom_item_detail->uom_id;
                    $bom_item_detail->uom_name = $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $bom_item_detail->uom_id));
                    $bom_item_detail->purchase_project_qty = $bom_item_detail->project_qty;
                    $bom_item_detail->loss = isset($bom_item_detail->loss) && !empty($bom_item_detail->loss) ? $bom_item_detail->loss : '';
                    $bom_item_detail->job_expense = isset($bom_item_detail->job_expense) && !empty($bom_item_detail->job_expense) ? $bom_item_detail->job_expense : '';
                    if(empty($bom_item_detail->uom_id)){
                        if(!empty($bom_item_detail->project_id)){
                            $bom_item_detail->uom_id = $this->crud->get_column_value_by_id('purchase_project_items', 'uom_id', array('project_id' => $bom_item_detail->project_id, 'item_id' => $bom_item_detail->item_id));
                        } else {
                            $bom_item_detail->uom_id = $this->crud->get_column_value_by_id('purchase_items', 'uom', array('id' => $bom_item_detail->item_id));
                        }
                    }    
					$lineitems[] = json_encode($bom_item_detail);
				}
				$data['bom_item_details'] = implode(',', $lineitems);
				
//				echo '<pre>';print_r($data); exit;
				set_page('bom/bom_add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
            if($this->applib->have_access_role(MASTER_BOM_MENU_ID,"view")) {
                set_page('bom/bom_add',$data);
            } else {
                $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }
    
    function get_qty_and_uom_by_item($purchase_item_id){
        $data = array();
        $purchase_items = $this->crud->get_row_by_id('purchase_items',array('id' => $purchase_item_id));
        if(!empty($purchase_items)){
            $data['purchase_item_qty'] = (!empty($purchase_items[0]->required_qty)) ? $purchase_items[0]->required_qty : '';
            $data['uom_name'] = (!empty($purchase_items[0]->required_uom)) ? $purchase_items[0]->required_uom : '';
            $data['item_code'] = (!empty($purchase_items[0]->item_code)) ? $purchase_items[0]->item_code : '';
        } 
        print json_encode($data);
        exit;
    }
        
    function get_purchase_project_items($purchase_project_id, $purchase_project_qty = '1'){
        if ($purchase_project_id != 0) {
			$purchase_project_items = $this->crud->get_row_by_id('purchase_project_items', array('project_id' => $purchase_project_id));
            $project_name = $this->crud->get_id_by_val('purchase_project', 'project_name', 'project_id', $purchase_project_id);
            $purchase_project_item_arr = array();
            foreach ($purchase_project_items as $purchase_project_item ){
                $purchase_item = $this->crud->get_row_by_id('purchase_items', array('id' => $purchase_project_item->item_id));
                $purchase_item = $purchase_item[0];
                $item_arr = array();
                $item_arr['project_id'] = $purchase_project_id;
                $item_arr['project_name'] = $project_name;
                $item_arr['purchase_project_qty'] = $purchase_project_qty;
                $item_arr['item_id'] = $purchase_project_item->item_id;
                $item_arr['uom_id'] = $purchase_project_item->uom_id;
                $item_arr['uom_name'] = (!empty($purchase_project_item->uom_id)) ? $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $purchase_project_item->uom_id)) : '';
                $item_arr['item_code'] = $purchase_item->item_code;
                $item_arr['item_name'] = $purchase_item->item_name;
                $item_arr['set_qty'] = $purchase_project_item->qty;
                $item_arr['loss'] = '';
                $item_arr['job_expense'] = '';
                $item_arr['quantity'] = $purchase_project_item->qty * $purchase_project_qty;
                $purchase_project_item_arr[] = json_encode($item_arr);
            }
            echo $project_item_arr = '['.implode(',', $purchase_project_item_arr).']';
            exit;
		}
	}
    
    function save_bom(){
		$post_data = $this->input->post();
//		echo '<pre>';print_r($post_data); exit;
		$project_line_items_data = json_decode('['.$post_data['project_line_items_data'].']'); 
//        echo '<pre>'; print_r($line_items_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
//        echo '<pre>'; print_r($line_items_data); exit;
//        print_r($line_items_data); exit;

		/*--------- Convert Date as Mysql Format -------------*/
        $post_data['bom_data']['sales_item_id'] = !empty($post_data['bom_data']['sales_item_id']) ? $post_data['bom_data']['sales_item_id'] : NULL;
        $post_data['bom_data']['effective_date'] = !empty(strtotime($post_data['bom_data']['effective_date'])) ? date("Y-m-d", strtotime($post_data['bom_data']['effective_date'])) : NULL;
        $post_data['bom_data']['updated_by'] = $this->staff_id;
        $post_data['bom_data']['updated_at'] = $this->now_time;
        
		if(isset($post_data['bom_data']['bom_id']) && !empty($post_data['bom_data']['bom_id'])){

			$bom_id = $post_data['bom_data']['bom_id'];
			if (isset($post_data['bom_data']['bom_id']))
				unset($post_data['bom_data']['bom_id']);

			$this->db->where('bom_id', $bom_id);
			$result = $this->db->update('bom_master', $post_data['bom_data']);
			if($result){
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','BOM Updated Successfully');
				
                if(isset($post_data['deleted_bom_item_expense_id'])){
                    $this->db->where_in('bom_item_expense_id', $post_data['deleted_bom_item_expense_id']);
                    $this->db->delete('bom_item_expenses');
                }
                if(isset($post_data['bom_item_expenses']['item_expense'][0]) && !empty($post_data['bom_item_expenses']['item_expense'][0])){
                    foreach($post_data['bom_item_expenses']['item_expense'] as $kay => $item_expense){
                        $add_project = array();
                        $add_project['bom_id'] = $bom_id;
                        $add_project['item_expense'] = $item_expense;
                        $add_project['expense'] = $post_data['bom_item_expenses']['expense'][$kay];
                        $add_project['updated_by'] = $this->staff_id;
                        $add_project['updated_at'] = $this->now_time;
                        if(isset($post_data['bom_item_expenses']['bom_item_expense_id'][$kay]) && !empty($post_data['bom_item_expenses']['bom_item_expense_id'][$kay])){
                            $this->db->where('bom_item_expense_id', $post_data['bom_item_expenses']['bom_item_expense_id'][$kay]);
                            $this->db->update('bom_item_expenses', $add_project);
                        } else {
                            $add_project['created_by'] = $this->staff_id;
                            $add_project['created_at'] = $this->now_time;
                            $this->crud->insert('bom_item_expenses',$add_project);
                        }
                    }
                }
                
                if(isset($post_data['deleted_bom_project_id'])){
                    $this->db->where_in('bom_project_id', $post_data['deleted_bom_project_id']);
                    $this->db->delete('bom_projects');
                }
                if(isset($project_line_items_data[0]) && !empty($project_line_items_data[0])){
                    foreach($project_line_items_data[0] as $project_line_item){
                        $add_project = array();
                        $add_project['bom_id'] = $bom_id;
                        $add_project['project_id'] = $project_line_item->project_id;
                        $add_project['project_qty'] = $project_line_item->project_qty;
                        $add_project['updated_by'] = $this->staff_id;
                        $add_project['updated_at'] = $this->now_time;
                        if(isset($project_line_item->bom_project_id) && !empty($project_line_item->bom_project_id)){
                            $this->db->where('bom_project_id', $project_line_item->bom_project_id);
                            $this->db->update('bom_projects', $add_project);
                        } else {
                            $add_project['created_by'] = $this->staff_id;
                            $add_project['created_at'] = $this->now_time;
                            $this->crud->insert('bom_projects',$add_project);
                        }
                    }
                }
                
                if(isset($post_data['deleted_lineitem_id'])){
                    $this->db->where_in('id', $post_data['deleted_lineitem_id']);
                    $this->db->delete('bom_item_details');
                }
                if(isset($line_items_data[0]) && !empty($line_items_data[0])){
                    foreach($line_items_data[0] as $lineitem){
                        $add_lineitem = array();
                        $add_lineitem['bom_id'] = $bom_id;
                        $add_lineitem['item_id'] = $lineitem->item_id;
                        if(isset($lineitem->project_id) && !empty($lineitem->project_id)){
                            $add_lineitem['project_id'] = $lineitem->project_id;
                        } else {
                            $add_lineitem['project_id'] = NULL;
                        }
                        $add_lineitem['basic_qty'] = isset($lineitem->set_qty) && !empty($lineitem->set_qty) ? $lineitem->set_qty : 0;
                        if(isset($lineitem->purchase_project_qty) && !empty($lineitem->purchase_project_qty)){
                            $add_lineitem['project_qty'] = $lineitem->purchase_project_qty;
                        } else {
                            $add_lineitem['project_qty'] = NULL;
                        }
                        $add_lineitem['uom_id'] = $lineitem->uom_id;
                        $add_lineitem['loss'] = isset($lineitem->loss) && !empty($lineitem->loss) ? $lineitem->loss : NULL;
                        $add_lineitem['job_expense'] = isset($lineitem->job_expense) && !empty($lineitem->job_expense) ? $lineitem->job_expense : NULL;
                        $add_lineitem['quantity'] = isset($lineitem->quantity) && !empty($lineitem->quantity) ? $lineitem->quantity : 0;
                        $add_lineitem['updated_by'] = $this->staff_id;
                        $add_lineitem['updated_at'] = $this->now_time;
                        if(isset($lineitem->lineitem_id) && !empty($lineitem->lineitem_id)){
                            $this->db->where('id', $lineitem->lineitem_id);
                            $this->db->update('bom_item_details', $add_lineitem);
                        } else {
                            $add_lineitem['created_by'] = $this->staff_id;
                            $add_lineitem['created_at'] = $this->now_time;
                            $this->crud->insert('bom_item_details',$add_lineitem);
                        }
                    }
                }
			}
		} else {
			$dataToInsert = $post_data['bom_data'];
			$post_data['bom_data']['created_by'] = $this->staff_id;
			$post_data['bom_data']['created_at'] = $this->now_time;
			$result = $this->db->insert('bom_master', $post_data['bom_data']);
			$bom_id = $this->db->insert_id();
			if($result){
                $return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','BOM Added Successfully');
                
                if(isset($post_data['bom_item_expenses']['item_expense'][0]) && !empty($post_data['bom_item_expenses']['item_expense'][0])){
                    foreach($post_data['bom_item_expenses']['item_expense'] as $kay => $item_expense){
                        $add_project = array();
                        $add_project['bom_id'] = $bom_id;
                        $add_project['item_expense'] = $item_expense;
                        $add_project['expense'] = $post_data['bom_item_expenses']['expense'][$kay];
                        $add_project['created_by'] = $this->staff_id;
                        $add_project['created_at'] = $this->now_time;
                        $add_project['updated_by'] = $this->staff_id;
                        $add_project['updated_at'] = $this->now_time;
                        $this->crud->insert('bom_item_expenses',$add_project);
                    }
                }
                
                if(isset($project_line_items_data[0]) && !empty($project_line_items_data[0])){
                    foreach($project_line_items_data[0] as $project_line_item){
                        $add_project = array();
                        $add_project['bom_id'] = $bom_id;
                        $add_project['project_id'] = $project_line_item->project_id;
                        $add_project['project_qty'] = $project_line_item->project_qty;
                        $add_project['created_by'] = $this->staff_id;
                        $add_project['created_at'] = $this->now_time;
                        $add_project['updated_by'] = $this->staff_id;
                        $add_project['updated_at'] = $this->now_time;
                        $this->crud->insert('bom_projects',$add_project);
                    }
                }
                
                if(isset($line_items_data[0]) && !empty($line_items_data[0])){
                    foreach($line_items_data[0] as $lineitem){
                        $add_lineitem = array();
                        $add_lineitem['bom_id'] = $bom_id;
                        $add_lineitem['item_id'] = $lineitem->item_id;
                        if(isset($lineitem->project_id) && !empty($lineitem->project_id)){
                            $add_lineitem['project_id'] = $lineitem->project_id;
                        } else {
                            $add_lineitem['project_id'] = NULL;
                        }
                        $add_lineitem['basic_qty'] = isset($lineitem->set_qty) && !empty($lineitem->set_qty) ? $lineitem->set_qty : 0;
                        if(isset($lineitem->purchase_project_qty) && !empty($lineitem->purchase_project_qty)){
                            $add_lineitem['project_qty'] = $lineitem->purchase_project_qty;
                        } else {
                            $add_lineitem['project_qty'] = NULL;
                        }
                        $add_lineitem['uom_id'] = $lineitem->uom_id;
                        $add_lineitem['loss'] = isset($lineitem->loss) && !empty($lineitem->loss) ? $lineitem->loss : NULL;
                        $add_lineitem['job_expense'] = isset($lineitem->job_expense) && !empty($lineitem->job_expense) ? $lineitem->job_expense : NULL;
                        $add_lineitem['quantity'] = isset($lineitem->quantity) && !empty($lineitem->quantity) ? $lineitem->quantity : 0;
                        $add_lineitem['created_by'] = $this->staff_id;
                        $add_lineitem['created_at'] = $this->now_time;
                        $add_lineitem['updated_by'] = $this->staff_id;
                        $add_lineitem['updated_at'] = $this->now_time;
                        $this->crud->insert('bom_item_details',$add_lineitem);
                    }
                }
			}
		}
		print json_encode($return);
		exit;
	}
    
    function bom_list(){
        if ($this->app_model->have_access_role(MASTER_BOM_MENU_ID, "view")) {
			set_page('bom/bom_list');
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
    }
    
    function bom_master_datatable() {
        $config['select'] = 'b.*, i.item_name, i.item_code1';
        $config['table'] = 'bom_master b';
        $config['column_order'] = array(null, 'i.item_name', 'i.item_code1', 'effective_date');
        $config['column_search'] = array('i.item_name', 'i.item_code1', 'DATE_FORMAT(b.effective_date,"%d-%m-%Y")');
        $config['joins'][] = array('join_table' => 'items i', 'join_by' => 'i.id = b.sales_item_id', 'join_type' => 'left');
        $config['order'] = array('id' => 'DESC');

        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        $isEdit = $this->app_model->have_access_role(MASTER_BOM_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_BOM_MENU_ID, "delete");
        foreach ($list as $bom_list) {
            $row = array();
            $action = '';
            if ($isEdit) {
                $action .= '<a href="' . base_url('master/bom_add/' . $bom_list->bom_id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
            }
            if ($isDelete) {
                $action .= ' | <a onclick="return confirm(\'Are you sure ?\');" href="' . BASE_URL . "master/delete_bom/" . $bom_list->bom_id . '" class="delete_button btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
            }
            $row[] = $action;
            $row[] = $bom_list->item_name;
            $row[] = $bom_list->item_code1;
            $row[] = date('d-m-Y', strtotime($bom_list->effective_date));
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function delete_bom($bom_id){
		$role_delete = $this->app_model->have_access_role(MASTER_BOM_MENU_ID, "delete");
		if ($role_delete) {
            $where_array = array("bom_id" => $bom_id);
            $result = $this->crud->delete("bom_master", $where_array);
            if($result['error'] == 'Error'){
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message','You cannot delete this Item. This Item has been used.');
            } elseif($result['success'] == 'Deleted') {
                $this->crud->delete("bom_projects", $where_array);
                $this->crud->delete("bom_item_details", $where_array);
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','BOM has been deleted');
            }
			redirect(BASE_URL . "master/bom_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function get_purchase_items($sales_items_id){
        if ($sales_items_id != 0) {
			$purchase_project_items = $this->crud->get_column_value_by_id('items', 'item_group', array('id' => $sales_items_id));
//            echo '<pre>'; print_r($purchase_project_items); exit;
            echo $purchase_project_items;
            exit;
		}
    }
    
    function get_purchase_items_lineitems($item_group){
        if ($item_group != 0) {
			$purchase_items = $this->crud->get_row_by_id('purchase_items', array('item_group' => $item_group));
//            echo '<pre>'; print_r($purchase_items); exit;
            $purchase_project_item_arr = array();
            foreach ($purchase_items as $purchase_item){
                $item_arr = array();
                $item_arr['project_id'] = '';
                $item_arr['project_name'] = '';
                $item_arr['purchase_project_qty'] = '';
                $item_arr['item_id'] = $purchase_item->id;
                $item_arr['uom_id'] = $purchase_item->required_uom;
                $item_arr['uom_name'] = (!empty($purchase_item->required_uom)) ? $this->crud->get_column_value_by_id('uom', 'uom', array('id' => $purchase_item->required_uom)) : '';
                $item_arr['item_code'] = $purchase_item->item_code;
                $item_arr['item_name'] = $purchase_item->item_name;
                $item_arr['set_qty'] = isset($purchase_item->required_qty) && !empty($purchase_item->required_qty) ? $purchase_item->required_qty : '';
                $item_arr['loss'] = '';
                $item_arr['job_expense'] = '';
                $item_arr['quantity'] = isset($purchase_item->required_qty) && !empty($purchase_item->required_qty) ? $purchase_item->required_qty : '';
                $item_arr['item_group'] = $item_group;
                $purchase_project_item_arr[] = json_encode($item_arr);
            }
            echo $project_item_arr = '['.implode(',', $purchase_project_item_arr).']';
            exit;
		}
	}
    
    function settings(){
		$settings_role = $this->app_model->have_access_role(COMPANY_DETAIL_MENU_ID, "view");
		if ($settings_role) {
            $data = array();
            $setting_data = $this->crud->get_all_records('settings', 'settings_id', 'asc');
            $data['setting_data'] = $setting_data;
            set_page('company_detail/settings', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function update_setting() {
        $post_data = $this->input->post();
        if (!empty($_FILES['sidebar_logo']['name'])) {
			$post_data['sidebar_logo'] = $this->applib->upload_image('sidebar_logo', image_dir(''), false);
			if(isset($post_data['sidebar_logo']['error'])){
				unset($post_data['sidebar_logo']);
			}
		}else if (!empty($post_data['sidebar_logo_old'])) {
			$post_data['sidebar_logo'] = $post_data['sidebar_logo_old'];
		} else {
			$post_data['sidebar_logo'] = null;
		}
		unset($post_data['sidebar_logo_old']);
        $result = $this->crud->update('settings', array('settings_value' => $post_data['sidebar_logo']), array('settings_key' => 'sidebar_logo'));
        $result = $this->crud->update('settings', array('settings_value' => $post_data['theme_color']), array('settings_key' => 'theme_color'));
		if ($result) {
            $this->session->set_userdata('sidebar_logo', $post_data['sidebar_logo']);
            $this->session->set_userdata('theme_color', $post_data['theme_color']);
            $this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Setting Data Updated Successfully');
			redirect("/master/settings");
		}
    }
    
}
