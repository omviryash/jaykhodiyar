<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Users
 * @property Crud $crud
 * @property AppModel $app_model
 */
class Quotation extends CI_Controller
{

	protected $staff_id = 1;

	function __construct()
	{
		parent::__construct();
//		if (!$this->session->userdata('is_logged_in')) {
//			redirect('');
//		}
		$this->staff_id = $this->session->userdata('is_logged_in')['staff_id'];
		$this->now_time = date('Y-m-d H:i:s');
		$this->load->model('AppModel', 'app_model');
		$this->load->model('Crud', 'crud');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

	}

	function get_inquiry(){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$inquiry_id = $this->input->get_post("id");
		$inquiry_data = $this->get_inquiry_by_id($inquiry_id);
		$inquiry_data->inquiry_date = substr($inquiry_data->inquiry_date, 8, 2) .'-'. substr($inquiry_data->inquiry_date, 5, 2) .'-'. substr($inquiry_data->inquiry_date, 0, 4);
		$inquiry_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $inquiry_data->created_by));
		$inquiry_data->created_at = substr($inquiry_data->created_at, 8, 2) .'-'. substr($inquiry_data->created_at, 5, 2) .'-'. substr($inquiry_data->created_at, 0, 4);
		$inquiry_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $inquiry_data->updated_by));
		$inquiry_data->updated_at = substr($inquiry_data->updated_at, 8, 2) .'-'. substr($inquiry_data->updated_at, 5, 2) .'-'. substr($inquiry_data->updated_at, 0, 4);
		$data['inquiry_data'] = $inquiry_data;
		$lineitems = array();
		$where = array('inquiry_id' => $inquiry_id);
		$inquiry_lineitems = $this->crud->get_row_by_id('inquiry_items', $where);
		foreach($inquiry_lineitems as $inquiry_lineitem){
			$c_lineitems = json_decode($inquiry_lineitem->item_data);
			$c_lineitems->disc_per = 0;
			$c_lineitems->disc_value = 0;
			$c_lineitems->net_amount = $c_lineitems->amount;
			$lineitems[] = $c_lineitems;
		}
		$data['inquiry_lineitems'] = $lineitems;
		echo json_encode($data);
		exit;
	}

	function enquiry_datatable_for_quptatoin_create(){
		$requestData = $_REQUEST; 
		$config['select'] = 'i.*,p.party_name as party_name, p.party_code as party_code, is.inquiry_status';
		$config['table'] = 'inquiry i';
		$config['column_order'] = array('i.inquiry_id','i.inquiry_date','p.party_code','p.party_name','is.inquiry_status');
		$config['column_search'] = array('i.inquiry_id','DATE_FORMAT(i.inquiry_date,"%d-%m-%Y")','p.party_code','p.party_name','is.inquiry_status');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'i.party_id = p.party_id');
		$config['joins'][] = array('join_table' => 'inquiry_status is', 'join_by' => 'i.inquiry_status_id = is.id');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => '0');
		$config['wheres'][] = array('column_name' => 'i.inquiry_status_id', 'column_value' => PENDING_ENQUIRY_ID);

        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($this->applib->have_access_current_user_rights(MANAGEMENT_USER,"allow") == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			}
		}else{
			$config['wheres'][] = array('column_name' => 'p.current_party_staff_id', 'column_value' =>  $this->session->userdata('is_logged_in')['staff_id']);
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_EXPORT_ID);
			} else if($cu_accessDomestic == 1){
				$config['wheres'][] = array('column_name' => 'p.party_type_1', 'column_value' => PARTY_TYPE_DOMESTIC_ID);
			} else {}
		}
        
		$config['order'] = array('i.inquiry_id' => 'desc');
		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		if(!empty($list)){
			foreach ($list as $enquiry_data) {
				$row = array();
				$row[] = '<a href="javascript:void(0);" class="enquiry_row" data-enquiry_id="' . $enquiry_data->inquiry_id . '">' . $enquiry_data->inquiry_no . '</a>';
				$row[] = '<a href="javascript:void(0);" class="enquiry_row" data-enquiry_id="' . $enquiry_data->inquiry_id . '">' . date('d-m-Y', strtotime($enquiry_data->inquiry_date)) . '</a>';
				$row[] = '<a href="javascript:void(0);" class="enquiry_row" data-enquiry_id="' . $enquiry_data->inquiry_id . '">' . $enquiry_data->party_code . '</a>';
				$row[] = '<a href="javascript:void(0);" class="enquiry_row" data-enquiry_id="' . $enquiry_data->inquiry_id . '">' . $enquiry_data->party_name . '</a>';
				$row[] = '<a href="javascript:void(0);" class="enquiry_row" data-enquiry_id="' . $enquiry_data->inquiry_id . '">' . $enquiry_data->inquiry_status . '</a>';
				$data[] = $row;
			}
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	/**
	 *    Quotation
	 */
	function add($quotation_id = '', $from_reminder = ''){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		//print_r($from_reminder);exit;
		$data = array();
		$user_ids = $this->app_model->getUserQuoteReminderIDS($this->staff_id);
		$data['users'] = array();
		if(!empty($user_ids)){
			$data['users'] = $this->crud->getFromSQL("SELECT `staff_id`, `name` FROM `staff` WHERE `staff_id` IN (".implode(',', $user_ids).") ORDER BY name");
		}
		if(!empty($quotation_id)){
			if ($this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit")) {
				$quotation_data = $this->crud->get_row_by_id('quotations', array('id' => $quotation_id));
				if(empty($quotation_data)){
					redirect("quotation/quotation_list"); exit;
				}
				$quotation_data = $quotation_data[0];
				if(isset($from_reminder) && $from_reminder == 'revision'){
					$quotation_data_rev = $this->crud->getFromSQL("SELECT MAX(`rev`) AS rev FROM `quotations` WHERE `quotation_no` =  '$quotation_data->quotation_no' ");
					$quotation_data->rev = $quotation_data_rev[0]->rev;
				}

				$quotation_data->enquiry_date = $this->crud->get_id_by_val('inquiry', 'inquiry_date', 'inquiry_id', $quotation_data->enquiry_id);
				$quotation_data->enquiry_date = substr($quotation_data->enquiry_date, 8, 2) .'-'. substr($quotation_data->enquiry_date, 5, 2) .'-'. substr($quotation_data->enquiry_date, 0, 4);
				$quotation_data->created_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $quotation_data->created_by));
				$quotation_data->created_at = substr($quotation_data->created_at, 8, 2) .'-'. substr($quotation_data->created_at, 5, 2) .'-'. substr($quotation_data->created_at, 0, 4);
				$quotation_data->updated_by_name = $this->crud->get_column_value_by_id('staff', 'name', array('staff_id' => $quotation_data->updated_by));
				$quotation_data->updated_at = substr($quotation_data->updated_at, 8, 2) .'-'. substr($quotation_data->updated_at, 5, 2) .'-'. substr($quotation_data->updated_at, 0, 4);
				$data['quotation_data'] = $quotation_data;
				$data['party_contact_person'] = $this->crud->get_contact_person_by_party($quotation_data->party_id);
				if(!empty($data['quotation_data']->kind_attn_id)){
					$data['contact_person'] = $this->crud->get_row_by_id('contact_person', array('contact_person_id' => $data['quotation_data']->kind_attn_id));
                    if(!empty($data['contact_person'])){
                        $data['contact_person'] = $data['contact_person'][0];
                        $designation = $this->crud->get_row_by_id('designation', array('designation_id' => $data['contact_person']->designation_id));
                        $department = $this->crud->get_row_by_id('department', array('department_id' => $data['contact_person']->department_id));
                        $data['contact_person']->designation = '';
                        if(!empty($designation)){
                            $data['contact_person']->designation = $designation[0]->designation;
                        }
                        $data['contact_person']->department = '';
                        if(!empty($department)){
                            $data['contact_person']->department = $department[0]->department;
                        }
                    }
				}
				$lineitems = array();
				$where = array('quotation_id' => $quotation_id);
				$quotation_lineitems = $this->crud->get_row_by_id('quotation_items', $where);
                $sales_id = $this->crud->get_id_by_val('party', 'party_type_1', 'party_id', $quotation_data->party_id);
                
				foreach($quotation_lineitems as $quotation_lineitem){
					$quotation_lineitem->item_rate = $quotation_lineitem->rate;
					$quotation_item_documents = json_decode($quotation_lineitem->item_documents, TRUE);
                    
                    $new_quotation_item_documents = array();
                    $item_documents = array();
                    if($sales_id == PARTY_TYPE_EXPORT_ID){
                        $item_documents = $this->crud->get_row_by_id('item_documents', array('item_id' => $quotation_lineitem->item_id, 'allow_to_edit' => 1, 'document_type' => 'export_quotation'));
                    } else if($sales_id == PARTY_TYPE_DOMESTIC_ID){
                        $item_documents = $this->crud->get_row_by_id('item_documents', array('item_id' => $quotation_lineitem->item_id, 'allow_to_edit' => 1, 'document_type' => 'domestic_quotation'));
                    }
                    foreach ($item_documents as $document){
                        if(!empty($quotation_item_documents)){
                            $matched = $this->applib->search_in_multidimensional_array($quotation_item_documents, 'sequence', $document->sequence);
                            if(!empty($matched)){
                                $new_quotation_item_documents[] = $matched[0];
                            } else {
                                $new_quotation_item_documents[] = $document;
                            }
                        } else {
                            $new_quotation_item_documents[] = $document;
                        }
                    }
                    
//                    print_r($new_quotation_item_documents);
                    $quotation_lineitem->item_documents = $new_quotation_item_documents;
					$lineitems[] = json_encode($quotation_lineitem);
				}

                $data['quotation_lineitems'] = implode(',', $lineitems);
				$followup_histories = array();
				$followup_history_data = $this->crud->get_row_by_id('followup_history', $where);
				foreach($followup_history_data as $followup_history){
					$followup_history->followup_history_date = date('d-m-Y H:i:s', strtotime($followup_history->followup_date));
					$followup_history->followup_history_onlydate = date('d-m-Y', strtotime($followup_history->followup_date));
					$followup_history->followup_history_onlytime = date('H:i:s', strtotime($followup_history->followup_date));
					$followup_history->followup_history_history = stripslashes($followup_history->history);
					$followup_histories[] = json_encode($followup_history);
				}
				$data['followup_history_data'] = implode(',', $followup_histories);
				$quotation_reminders = array();
				$quotation_reminder_data = $this->crud->get_row_by_id('followup_history', $where);
				foreach($quotation_reminder_data as $quotation_reminder){
					$quotation_reminder->reminder_onlydate = '';
					$quotation_reminder->reminder_onlytime = '';
					if(strtotime($quotation_reminder->reminder_date) > 0){
						$quotation_reminder->reminder_date = date('d-m-Y H:i:s', strtotime($quotation_reminder->reminder_date));
						$quotation_reminder->reminder_onlydate = date('d-m-Y', strtotime($quotation_reminder->reminder_date));
						$quotation_reminder->reminder_onlytime = date('H:i:s', strtotime($quotation_reminder->reminder_date));
					}
					$quotation_reminder->followup_onlydate = '';
					$quotation_reminder->followup_onlytime = '';
					if(strtotime($quotation_reminder->followup_date) > 0){
						$quotation_reminder->followup_date = date('d-m-Y H:i:s', strtotime($quotation_reminder->followup_date));
						$quotation_reminder->followup_onlydate = date('d-m-Y', strtotime($quotation_reminder->followup_date));
						$quotation_reminder->followup_onlytime = date('H:i:s', strtotime($quotation_reminder->followup_date));
					}
					$quotation_reminder->history = stripslashes($quotation_reminder->history);
					$quotation_reminder->reminder_message = stripslashes($quotation_reminder->reminder_message);
					$quotation_reminders[] = json_encode($quotation_reminder);
				}
				$data['quotation_reminder_data'] = implode(',', $quotation_reminders);
				$data['from_reminder'] = $from_reminder;
				//                echo '<pre>';print_r($data); exit;
				set_page('sales/quotation/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		} else {
			if($this->applib->have_access_role(QUOTATION_MODULE_ID,"add")) {
				$all_quotation_no = $this->db->select('RIGHT(`quotation_no`,length(`quotation_no`)-1) AS quotation_no')->from('quotations')->get()->result_array();
				$quotation_no_arr = array();
				foreach ($all_quotation_no as $quotation_no){
					$quotation_no_arr[] = (int) $quotation_no['quotation_no'];
				}
				rsort($quotation_no_arr);
				if(!empty($quotation_no_arr)){
					$next_quotation_no = $quotation_no_arr[0] + 1;
				} else {
					$next_quotation_no = 1;
				}
				$data['quotation_no'] = 'Q'.$next_quotation_no;
				$data['from_reminder'] = $from_reminder;
				set_page('sales/quotation/add', $data);
			} else {
				$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
				redirect("/");
			}
		}
	}

	function save_quotation(){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$post_data = $this->input->post();
		// check party validations
//		echo '<pre>';print_r($post_data);exit;
		$party_id = $post_data['party']['party_id'];
		if ($party_id == "" || $party_id == "0") {
			echo json_encode(array("success" => 'false', 'msg' => 'Please select Party!'));
			exit;
		}
		//		$result = $this->applib->check_party_validations($party_id);
		//		if ($result['status'] == 0) {
		//			echo json_encode(array("success" => 'false', 'msg' => $result['msg']));
		//			exit;
		//		}
		//        
		//        if (isset($post_data['quotation_data']['kind_attn_id']) && trim($post_data['quotation_data']['kind_attn_id']) != '' && !empty(trim($post_data['quotation_data']['kind_attn_id']))) {
		//            $contact_person = $post_data['contact_person'];
		//            $contact_person_update['mobile_no'] = $contact_person['contact_person_mobile_no'];
		//            $contact_person_update['email'] = $contact_person['contact_person_email_id'];
		//            $this->crud->update('contact_person', $contact_person_update, array('contact_person_id' => $post_data['quotation_data']['kind_attn_id']));
		//        }

		//echo '<pre>';print_r($post_data); exit;
		$line_items_data = json_decode('['.$post_data['line_items_data'].']'); 
		//		print_r($line_items_data); exit;
		$quotation_reminder_data = json_decode('['.$post_data['quotation_reminder_data'].']'); 
		//print_r($quotation_reminder_data); exit;

        $send_sms = 0;
        if(isset($post_data['quotation_data']['send_sms'])){
            $send_sms = 1;
            unset($post_data['quotation_data']['send_sms']);
        }
        $send_whatsapp_sms = 0;
        if(isset($post_data['quotation_data']['send_whatsapp_sms'])){
            $send_whatsapp_sms = 1;
            $whatsapp_no = $post_data['quotation_data']['whatsapp_no'];
            $this->crud->update('party', array('whatsapp_no' => $whatsapp_no), array('party_id' => $party_id));
            unset($post_data['quotation_data']['send_whatsapp_sms']);
        }
        if(isset($post_data['quotation_data']['whatsapp_no'])){
            unset($post_data['quotation_data']['whatsapp_no']);
        }
		/*--------- Convert Date as Mysql Format -------------*/
		$post_data['quotation_data']['enquiry_date'] = date("Y-m-d", strtotime($post_data['quotation_data']['enquiry_date']));
		$post_data['quotation_data']['quotation_date'] = date("Y-m-d", strtotime($post_data['quotation_data']['quotation_date']));
		$post_data['quotation_data']['rev_date'] = date("Y-m-d", strtotime($post_data['quotation_data']['rev_date']));
		$post_data['quotation_data']['kind_attn_id'] = !empty($post_data['quotation_data']['kind_attn_id']) ? $post_data['quotation_data']['kind_attn_id'] : NULL;
		$post_data['quotation_data']['quotation_type_id'] = !empty($post_data['quotation_data']['quotation_type_id']) ? $post_data['quotation_data']['quotation_type_id'] : NULL;
		$post_data['quotation_data']['quotation_status_id'] = !empty($post_data['quotation_data']['quotation_status_id']) ? $post_data['quotation_data']['quotation_status_id'] : NULL;
		$post_data['quotation_data']['quotation_stage_id'] = !empty($post_data['quotation_data']['quotation_stage_id']) ? $post_data['quotation_data']['quotation_stage_id'] : NULL;
		$post_data['quotation_data']['sales_type_id'] = !empty($post_data['quotation_data']['sales_type_id']) ? $post_data['quotation_data']['sales_type_id'] : NULL;
		$post_data['quotation_data']['sales_id'] = !empty($post_data['quotation_data']['sales_id']) ? $post_data['quotation_data']['sales_id'] : NULL;
		$post_data['quotation_data']['currency_id'] = !empty($post_data['quotation_data']['currency_id']) ? $post_data['quotation_data']['currency_id'] : NULL;
		$post_data['quotation_data']['party_id'] = $party_id;

		if(isset($post_data['quotation_data']['quotation_id']) && !empty($post_data['quotation_data']['quotation_id']) && empty($post_data['quotation_data']['revised_from_id']) ){
			$quotation_no = $post_data['quotation_data']['quotation_no'];
			$quotation_result = $this->crud->get_id_by_val('quotations', 'id', 'quotation_no', $quotation_no);

			if($post_data['quotation_data']['rev'] == 1){
				if(!empty($quotation_result) && $quotation_result != $post_data['quotation_data']['quotation_id']){
					echo json_encode(array("success" => 'false', 'msg' => 'Quotation No. Already Exist!'));
					exit;
				}	
			}

			$quotation_id = $post_data['quotation_data']['quotation_id'];
			if (isset($post_data['quotation_data']['quotation_id']))
				unset($post_data['quotation_data']['quotation_id']);

			$post_data['quotation_data']['updated_by'] = $this->staff_id;
			$this->db->where('id', $quotation_id);
			$result = $this->db->update('quotations', $post_data['quotation_data']);
			if($result){
				//$this->crud->update('party', array('party_type_2' => $post_data['quotation_data']['sales_type_id']), array('party_id' => $party_id));
				$return['success'] = "Updated";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Quotation Updated Successfully');
				$where_array = array("quotation_id" => $quotation_id);
				$this->crud->delete("quotation_items", $where_array);
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['quotation_id'] = $quotation_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					if(isset($lineitem->item_description)){
						$add_lineitem['item_description'] = $lineitem->item_description;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					if(isset($lineitem->currency_id)){
						$add_lineitem['currency_id'] = $lineitem->currency_id;
					}
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = !empty($lineitem->disc_per) ? $lineitem->disc_per : '0';
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
                    if(isset($lineitem->item_documents)){
						$add_lineitem['item_documents'] = json_encode($lineitem->item_documents);
					}
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('quotation_items',$add_lineitem);
				}

				if(isset($post_data['deleted_quotation_reminder_id'])){
					$this->db->where_in('id', $post_data['deleted_quotation_reminder_id']);
					$this->db->delete('followup_history');
				}
				$add_followup_history_arr = array();
				$edit_followup_history_arr = array();
				foreach($quotation_reminder_data[0] as $key => $line){
					if(isset($line->reminder_action) && $line->reminder_action == 'add'){
						$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
						$followup_onlytime = '00:00:00';
						if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
							$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
						}
						$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
						$reminder_onlytime = '00:00:00';
						if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
							$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
						}
						$add_followup_history = array(
							'quotation_id' => $quotation_id,
							'followup_category_id' => $line->followup_category_id,
							'followup_date' => $followup_onlydate.' '.$followup_onlytime,
							'reminder_category_id' => $line->reminder_category_id,
							'reminder_date' => $reminder_onlydate.' '.$reminder_onlytime,
							'history' => addslashes($line->history),
							'reminder_message' => addslashes($line->reminder_message),
							'followup_by' => $line->followup_by,
							'assigned_to' => $line->assigned_to,
						);
						if(isset($line->followup_at) && !empty($line->followup_at)){
							$add_followup_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
						}
						$add_followup_history['updated_by'] = $line->updated_by;
						if(isset($line->updated_at) && !empty($line->updated_at)){
							$add_followup_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
						}
						if(isset($line->is_notified) && $line->is_notified == 1){
							$add_followup_history['is_notified'] = 1;
						}
						array_push($add_followup_history_arr, $add_followup_history);    
					}
					$edit_followup_history = array();
					if(isset($line->reminder_action) && $line->reminder_action == 'edit'){
						$edit_followup_history['id'] = $line->id;
						$edit_followup_history['quotation_id'] = $quotation_id;
						$edit_followup_history['followup_category_id'] = $line->followup_category_id;
						$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
						$followup_onlytime = '00:00:00';
						if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
							$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
						}
						$edit_followup_history['followup_date'] = $followup_onlydate.' '.$followup_onlytime;
						$edit_followup_history['reminder_category_id'] = $line->reminder_category_id;
						$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
						$reminder_onlytime = '00:00:00';
						if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
							$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
						}
						$edit_followup_history['reminder_date'] = $reminder_onlydate.' '.$reminder_onlytime;
						$edit_followup_history['history'] = addslashes($line->history);
						$edit_followup_history['reminder_message'] = addslashes($line->reminder_message);
						$edit_followup_history['followup_by'] = $line->followup_by;
						$edit_followup_history['assigned_to'] = $line->assigned_to;

						if(isset($line->followup_at) && !empty($line->followup_at)){
							$edit_followup_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
						}
						$edit_followup_history['updated_by'] = $line->updated_by;
						if(isset($line->updated_at) && !empty($line->updated_at)){
							$edit_followup_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
						}
						if(isset($line->is_notified) && $line->is_notified == 1){
							$edit_followup_history['is_notified'] = 1;
						}
						if(empty($line->id)){
							unset($line->id);
							unset($edit_followup_history['id']);
							array_push($add_followup_history_arr,$edit_followup_history);    
						}else{
							array_push($edit_followup_history_arr, $edit_followup_history);    	
						}
					}
				}
				if(!empty($add_followup_history_arr)){
					$this->db->insert_batch('followup_history', $add_followup_history_arr);	
				}
				if(!empty($edit_followup_history_arr)){
					$this->db->update_batch('followup_history', array_filter($edit_followup_history_arr), 'id');	
				}

			}
		}elseif(isset($post_data['quotation_data']['quotation_id']) && isset($post_data['quotation_data']['revised_from_id'])){
			$quotation_no = $post_data['quotation_data']['quotation_no'];
            
            // Set Revision Max 0
            $this->crud->update('quotations', array("latest_revision" => 0), array('quotation_no' => $quotation_no));
            
			if (isset($post_data['quotation_data']['quotation_id']))
				unset($post_data['quotation_data']['quotation_id']);
			$dataToInsert = $post_data['quotation_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;

			$result = $this->db->insert('quotations', $dataToInsert);
			$quotation_id = $this->db->insert_id();
			if($result){
				//                $this->crud->update('party', array('party_type_2' => $post_data['quotation_data']['sales_type_id']), array('party_id' => $party_id));

				/*--- Update inquiry to completed -----------*/
				$this->crud->update('inquiry', array("inquiry_status_id" => $this->get_inquiry_status_id_by_status('completed')), array('inquiry_id' => $post_data['quotation_data']['enquiry_id']));

				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Quotation Revision Added Successfully');
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['quotation_id'] = $quotation_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					if(isset($lineitem->item_description)){
						$add_lineitem['item_description'] = $lineitem->item_description;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					if(isset($lineitem->currency_id)){
						$add_lineitem['currency_id'] = $lineitem->currency_id;
					}
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = !empty($lineitem->disc_per) ? $lineitem->disc_per : '0';
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
                    if(isset($lineitem->item_documents)){
						$add_lineitem['item_documents'] = json_encode($lineitem->item_documents);
					}
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('quotation_items',$add_lineitem);
				}
				$add_followup_history_arr = array();
				foreach($quotation_reminder_data[0] as $line){
					$add_followup_history = array();
					$add_followup_history['quotation_id'] = $quotation_id;
					$add_followup_history['followup_category_id'] = $line->followup_category_id;
					$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
					$followup_onlytime = '00:00:00';
					if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
						$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
					}
					$add_followup_history['followup_date'] = $followup_onlydate.' '.$followup_onlytime;
					$add_followup_history['reminder_category_id'] = $line->reminder_category_id;
					$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
					$reminder_onlytime = '00:00:00';
					if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
						$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
					}
					$add_followup_history['reminder_date'] = $reminder_onlydate.' '.$reminder_onlytime;
					$add_followup_history['history'] = addslashes($line->history);
					$add_followup_history['reminder_message'] = addslashes($line->reminder_message);
					$add_followup_history['followup_by'] = $line->followup_by;
					$add_followup_history['assigned_to'] = $line->assigned_to;

					if(isset($line->followup_at) && !empty($line->followup_at)){
						$add_followup_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
					}
					$add_followup_history['updated_by'] = $line->updated_by;
					if(isset($line->updated_at) && !empty($line->updated_at)){
						$add_followup_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
					}
					array_push($add_followup_history_arr, $add_followup_history);					
				}
				$this->db->insert_batch('followup_history', array_filter($add_followup_history_arr)); 
			}

		}else {

			$all_quotation_no = $this->db->select('RIGHT(`quotation_no`,length(`quotation_no`)-1) AS quotation_no')->from('quotations')->get()->result_array();
			$quotation_no_arr = array();
			foreach ($all_quotation_no as $quotation_no){
				$quotation_no_arr[] = (int) $quotation_no['quotation_no'];
			}
			rsort($quotation_no_arr);
			if(!empty($quotation_no_arr)){
				if (in_array('24000', $quotation_no_arr)) {
					$post_data['quotation_data']['quotation_no'] = $quotation_no_arr[0] + 1;
					$post_data['quotation_data']['quotation_no'] = 'Q'.$post_data['quotation_data']['quotation_no'];
				} else {
					$post_data['quotation_data']['quotation_no'] = 'Q24000';
				}
			} else {
				$post_data['quotation_data']['quotation_no'] = 'Q24000';
			}

			$dataToInsert = $post_data['quotation_data'];
			$dataToInsert['created_at'] = $this->now_time;
			$dataToInsert['created_by'] = $this->staff_id;
			$dataToInsert['updated_by'] = $this->staff_id;

			$result = $this->db->insert('quotations', $dataToInsert);
			$quotation_id = $this->db->insert_id();
			if($result){
				//                $this->crud->update('party', array('party_type_2' => $post_data['quotation_data']['sales_type_id']), array('party_id' => $party_id));

				/*--- Update inquiry to completed -----------*/
				$this->crud->update('inquiry', array("inquiry_status_id" => $this->get_inquiry_status_id_by_status('completed')), array('inquiry_id' => $post_data['quotation_data']['enquiry_id']));

				$return['success'] = "Added";
				$this->session->set_flashdata('success',true);
				$this->session->set_flashdata('message','Quotation Added Successfully');
				foreach($line_items_data[0] as $lineitem){
					$add_lineitem = array();
					$add_lineitem['quotation_id'] = $quotation_id;
					$add_lineitem['item_id'] = $lineitem->item_id;
					$add_lineitem['item_code'] = $lineitem->item_code;
					$add_lineitem['item_name'] = $lineitem->item_name;
					if(isset($lineitem->item_description)){
						$add_lineitem['item_description'] = $lineitem->item_description;
					}
					$add_lineitem['quantity'] = $lineitem->quantity;
					if(isset($lineitem->currency_id)){
						$add_lineitem['currency_id'] = $lineitem->currency_id;
					}
					$add_lineitem['rate'] = $lineitem->item_rate;
					$add_lineitem['amount'] = $lineitem->amount;
					$add_lineitem['disc_per'] = !empty($lineitem->disc_per) ? $lineitem->disc_per : '0';
					$add_lineitem['disc_value'] = $lineitem->disc_value;
					$add_lineitem['net_amount'] = $lineitem->net_amount;
					$add_lineitem['created_at'] = $this->now_time;
					$this->crud->insert('quotation_items',$add_lineitem);
				}
				$add_followup_history_arr = array();
				foreach($quotation_reminder_data[0] as $line){
					$add_followup_history = array();
					$add_followup_history['quotation_id'] = $quotation_id;
					$add_followup_history['followup_category_id'] = $line->followup_category_id;
					$followup_onlydate = date("Y-m-d", strtotime($line->followup_onlydate));
					$followup_onlytime = '00:00:00';
					if(isset($line->followup_onlytime) && !empty($line->followup_onlytime)){
						$followup_onlytime = date("H:i:s", strtotime($line->followup_onlytime));
					}
					$add_followup_history['followup_date'] = $followup_onlydate.' '.$followup_onlytime;
					$add_followup_history['reminder_category_id'] = $line->reminder_category_id;
					$reminder_onlydate = date("Y-m-d", strtotime($line->reminder_onlydate));
					$reminder_onlytime = '00:00:00';
					if(isset($line->reminder_onlytime) && !empty($line->reminder_onlytime)){
						$reminder_onlytime = date("H:i:s", strtotime($line->reminder_onlytime));
					}
					$add_followup_history['reminder_date'] = $reminder_onlydate.' '.$reminder_onlytime;
					$add_followup_history['history'] = addslashes($line->history);
					$add_followup_history['reminder_message'] = addslashes($line->reminder_message);
					$add_followup_history['followup_by'] = $line->followup_by;
					$add_followup_history['assigned_to'] = $line->assigned_to;
					if(isset($line->followup_at) && !empty($line->followup_at)){
						$add_followup_history['followup_at'] = date("Y-m-d H:i:s", strtotime($line->followup_at));
					}
					$add_followup_history['updated_by'] = $line->updated_by;
					if(isset($line->updated_at) && !empty($line->updated_at)){
						$add_followup_history['updated_at'] = date("Y-m-d H:i:s", strtotime($line->updated_at));
					}
					array_push($add_followup_history_arr, $add_followup_history);                    
				}
				$this->db->insert_batch('followup_history', array_filter($add_followup_history_arr)); 
			}
		}
        if($send_sms == 1){
            $party_phone_no = $this->crud->get_id_by_val('party', 'phone_no', 'party_id', $post_data['quotation_data']['party_id']);
            $this->applib->send_sms('quotation', $quotation_id, $party_phone_no, SEND_QUOTATION_SMS_ID);
        }
        if($send_whatsapp_sms == 1){
            $sms = $this->applib->send_whatsapp_sms('quotation', $quotation_id, SEND_QUOTATION_SMS_ID);
            $this->applib->send_whatsapp_sms_through_twilio($sms);
//            $sms = $this->applib->send_whatsapp_sms('quotation', $quotation_id, SEND_QUOTATION_SMS_ID);
//            $return['send_whatsapp_sms_url'] = 'https://api.whatsapp.com/send?phone='.$whatsapp_no.'&text='.$sms;
        }
		print json_encode($return);
		exit;
	}

	function get_inquiry_status_id_by_status($status = 'completed'){
		$this->db->select('id');
		$this->db->from('inquiry_status');
		$this->db->where('LOWER(inquiry_status)', strtolower($status));
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		} else {
			return null;
		}
	}

	function quotation_list($status='',$staff_id='',$from_date='',$to_date=''){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$role_view = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "view");
		if ($role_view) {
			$data['status']=$status;
			$data['staff_id']=$staff_id;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			set_page('sales/quotation/quotation_list', $data);
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}

	function quotation_datatable($status=''){
		$requestData = $_REQUEST;
//                echo '<pre>'; print_r($_POST['from_date']); exit;
		//pre_die($_POST);
		//$config['select'] = 'p.email_id,p.phone_no,q.updated_at,q.id,q.rev,q.enquiry_no,q.quotation_no,q.quotation_status_id,q.party_name,p.party_name as pname, city.city, country.country, agent.agent_name, q.id, quotation_status.quotation_status, (select count(*) from quotation_items where q.id = quotation_id group by quotation_id) as count_items ';
		$config['select'] = 'q.id,q.quotation_no,q.latest_revision,q.rev,q.enquiry_no,p.party_name as pname,p.phone_no,p.email_id,q.quotation_status_id,city.city, country.country,staff.name as staff_name,q.updated_at, qs.quotation_status,';
		$config['select'] .= ',GROUP_CONCAT(qi.item_code) AS qi_item_code,state.state,cur_s.name AS party_current_person';
		$config['table'] = 'quotations q';
		$config['column_order'] = array(null,null,'q.quotation_no',null,'q.enquiry_no','qi.item_code','p.party_name','p.phone_no','p.email_id','qs.quotation_status','city.city','state.state','country.country','staff.name');
		$config['column_search'] = array('q.enquiry_no', 'q.quotation_no', 'p.party_name', 'p.phone_no', 'p.email_id','city.city','state.state','country.country','staff.name');
		$config['joins'][] = array('join_table' => 'party p', 'join_by' => 'p.party_id = q.party_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'country', 'join_by' => 'country.country_id = p.country_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'state', 'join_by' => 'state.state_id = p.state_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'city', 'join_by' => 'city.city_id = p.city_id', 'join_type' => 'left');
		
		$config['joins'][] = array('join_table' => 'quotation_status qs', 'join_by' => 'qs.id = q.quotation_status_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotation_stage', 'join_by' => 'quotation_stage.id = q.quotation_stage_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'quotation_type', 'join_by' => 'quotation_type.id = q.quotation_type_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'sales_type', 'join_by' => 'sales_type.id = q.sales_type_id', 'join_type' => 'left');
		$config['joins'][] = array('join_table' => 'staff', 'join_by' => 'staff.staff_id = q.created_by', 'join_type' => 'left');
        $config['joins'][] = array('join_table' => 'staff cur_s','join_by'=>'cur_s.staff_id = p.current_party_staff_id','join_type'=>'left');
		$config['joins'][] = array('join_table' => 'quotation_items qi', 'join_by' => 'qi.quotation_id = q.id', 'join_type' => 'right');
		$config['wheres'][] = array('column_name' => 'p.active !=', 'column_value' => 0);
        if(isset($_POST['latest_revision_only']) && $_POST['latest_revision_only'] == '1'){
            $config['where_string'] = ' q.latest_revision = 1 ';
		} else {
            $config['where_string'] = ' 1 = 1 ';
        }
        
		
		if(isset($_POST['quotation_status']) && !empty($_POST['quotation_status'])){
			$config['where_string'] .= ' AND q.quotation_status_id = '.$_POST['quotation_status'];
		}

		$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
		$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
		if($this->applib->have_access_current_user_rights(QUOTATION_MANAGEMENT_USER,"allow") == 1){
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){					
			}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
				$config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_EXPORT_ID;
			}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
				$config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_DOMESTIC_ID;
			}
		}else{
			$config['where_string'] .= ' AND ( p.current_party_staff_id = ' . $this->session->userdata('is_logged_in')['staff_id'] . ' OR p.current_party_staff_id IS NULL )';
			if($cu_accessExport == 1 && $cu_accessDomestic == 1){
			} else if($cu_accessExport == 1){
				$config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_EXPORT_ID;
			} else if($cu_accessDomestic == 1){
				$config['where_string'] .= ' AND p.party_type_1 = ' . PARTY_TYPE_DOMESTIC_ID;
			} else {}
		}

		if($status=='pending'){
			$config['where_string'] .= ' AND q.id NOT IN(SELECT sales_order.quotation_id FROM sales_order) AND q.due_date <= CURDATE()';
		}

		if (!empty($requestData['columns'][1]['search']['value'])) {
			$config['likes'][] = array('q.quotation_no' => trim($requestData['columns'][1]['search']['value']));
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$config['likes'][] = array('q.enquiry_no' => trim($requestData['columns'][2]['search']['value']));
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			$config['likes'][] = array('p.party_name' => trim($requestData['columns'][3]['search']['value']));
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {
			$config['likes'][] = array('agent.agent_name' => trim($requestData['columns'][4]['search']['value']));
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {
			$config['likes'][] = array('country.country' => trim($requestData['columns'][5]['search']['value']));
		}
		if (!empty($requestData['columns'][6]['search']['value'])) {
			$config['likes'][] = array('city.city' => trim($requestData['columns'][6]['search']['value']));
		}
		if (!empty($requestData['columns'][13]['search']['value'])) {
			$config['likes'][] = array('state.state' => trim($requestData['columns'][13]['search']['value']));
		}
		if (!empty($requestData['columns'][7]['search']['value'])) {
			$config['likes'][] = array('qs.quotation_status' => trim($requestData['columns'][7]['search']['value']));
		}
		if (!empty($requestData['columns'][8]['search']['value'])) {
			$config['likes'][] = array('quotation_stage.quotation_stage' => trim($requestData['columns'][8]['search']['value']));
		}
		if (!empty($requestData['columns'][9]['search']['value'])) {
			$config['likes'][] = array('quotation_type.quotation_type' => trim($requestData['columns'][9]['search']['value']));
		}
		if (!empty($requestData['columns'][10]['search']['value'])) {
			$config['likes'][] = array('sales_type.sales_type' => trim($requestData['columns'][10]['search']['value']));
		}
		if (!empty($requestData['columns'][11]['search']['value'])) {
			$quotation_daterange = $requestData['columns'][11]['search']['value'];
			$quotation_startdate = substr($quotation_daterange, 6, 4).'-'.substr($quotation_daterange, 3, 2).'-'.substr($quotation_daterange, 0, 2);
			$quotation_enddate = substr($quotation_daterange, 19, 4).'-'.substr($quotation_daterange, 16, 2).'-'.substr($quotation_daterange, 13, 2);
			$config['where_string'] .= ' AND `quotation_date` between "'.$quotation_startdate.'" AND "'.$quotation_enddate.'" ';
		}
        if (!empty($requestData['columns'][12]['search']['value'])) {
			$config['likes'][] = array('staff.name' => trim($requestData['columns'][12]['search']['value']));
		}
                if (isset($_POST['from_date']) && !empty($_POST['from_date'])) {
                    $from_date = date('Y-m-d', strtotime($_POST['from_date']));
                    $config['wheres'][] = array('column_name' => 'e.inquiry_date >=', 'column_value' => $from_date);
                }
                if (isset($_POST['to_date']) && !empty($_POST['to_date'])) {
                    $to_date = date('Y-m-d', strtotime($_POST['to_date']));
                    $config['wheres'][] = array('column_name' => 'e.inquiry_date <=', 'column_value' => $to_date);
                }
		$config['order'] = array('q.updated_at' => 'desc');
		$config['group_by'] = 'q.id';
		$this->load->library('datatables', $config, 'datatable');
		/*echo "<pre>";print_r($this->datatable);die();*/
		$list = $this->datatable->get_datatables();
//		echo $this->db->last_query(); exit;
		$data = array();
		$isEdit = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
		$isDelete = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "delete");
		if(!empty($list)){
			foreach ($list as $quotation_data) {				
				$row = array();
				$action = '';
				if ($isEdit) {
                    if($quotation_data->quotation_status_id != QUOTATION_STATUS_ID_ON_ORDER_CREATE){
                        $action .= '<a href="' . base_url('quotation/add/' . $quotation_data->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
                    } else {
                        if($this->applib->have_access_role(QUOTATION_ALLOW_EDIT_AFTER_SALES_ORDER,"allow")) {
                            $action .= '<a href="' . base_url('quotation/add/' . $quotation_data->id) . '" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
                        }
                    }
				}
				if ($isDelete) {
					if($quotation_data->quotation_status_id != QUOTATION_STATUS_ID_ON_ORDER_CREATE){
						$action .= ' | <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="' . base_url('quotation/delete_quotation/' . $quotation_data->id) . '"><i class="fa fa-trash"></i></a>';
					}
				}
				$action .= ' | <a href="#" class="print_button btn-primary btn-xs show_itemprint_modal" id="' . $quotation_data->id . '"><i class="fa fa-print"></i></a>';
				$action .= ' | <a href="' . base_url('quotation/quotation_email/' . $quotation_data->id) . '" class="email_button">Email</a>';
				$row[] = $quotation_data->id;
				$row[] = $action;
				$row[] = $quotation_data->quotation_no;
				$action = '<a href="' . base_url('quotation/add/' . $quotation_data->id.'/revision') . '" class="revise_button" data-href="#" title="Revise"><i class="fa fa-repeat"></i></a>';
				$row[] = !empty($quotation_data->rev) ? $quotation_data->rev . '  &nbsp; &nbsp;' . $action : '1 &nbsp; &nbsp;' . $action;
				
				$row[] = $quotation_data->enquiry_no;
				$qi_item_code = explode(",", $quotation_data->qi_item_code);
				$qi_item_code = (count($qi_item_code) > 1 ? 'Multiple' : $qi_item_code[0]);
				$quotation_data->qi_item_code = str_replace(',', ', ', $quotation_data->qi_item_code);
				$row[] = '<a href="' . base_url('quotation/add/' . $quotation_data->id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$quotation_data->qi_item_code.'" >'. $qi_item_code .'</a>';
				$row[] = $quotation_data->pname;
				$row[] = $quotation_data->phone_no;
				$email_id = substr($quotation_data->email_id,0,14);
				$quotation_data->email_id = str_replace(',', ', ', $quotation_data->email_id);
				$row[] = '<a href="' . base_url('quotation/add/' . $quotation_data->id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$quotation_data->email_id.'" >'.$email_id.'</a>';
				$row[] = $quotation_data->quotation_status;
				$row[] = $quotation_data->city;
				$row[] = $quotation_data->state;
				$row[] = $quotation_data->country;
				//$row[] = '<a href="' . base_url('quotation/add/' . $quotation_data->id . '?view') . '" data-toggle="tooltip" data-placement="bottom" title="'.$quotation_data->email_id.'" >'.$email_id.'</a>';
				$row[] = $quotation_data->staff_name;
				$row[] = $quotation_data->party_current_person;
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function quotation_print(){
//		if (!$this->session->userdata('is_logged_in')) {
//			redirect('');
//		}
//        echo "<pre>"; print_r($_POST); exit;
        $id = (isset($_POST['quotation_id']) && $_POST['quotation_id']) ? $_POST['quotation_id'] : '';
        if(!empty($id)){
            $company_details = $this->applib->get_company_detail();
            $select_item = (isset($_POST['quotation_item_id']) && $_POST['quotation_item_id']) ? $_POST['quotation_item_id'] : '';
            $letter_pad = (isset($_POST['l_pad']) && $_POST['l_pad']) ? $_POST['l_pad'] : '';
            $quotation_data = $this->crud->get_column_value_by_quotation($id);
            $kind_attn = $this->getKindAttnParty($quotation_data->party_id);
            $quotation_items = $this->crud->get_items_detail_by_quotation($id);
            $margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
            $margin_left = $margin_company[0]->margin_left;
            $margin_right = $margin_company[0]->margin_right;
            $margin_top = $margin_company[0]->margin_top;
            $margin_bottom = $margin_company[0]->margin_bottom;
            foreach ($quotation_items as $item_row) {
                if ($select_item && $item_row->id != $select_item) {
                    continue;
                }
                $this->load->library('m_pdf');
                $param = "'utf-8','A4'";
                $pdf = $this->m_pdf->load($param);
                $this->db->select('*');
                $this->db->from('item_documents');
                $this->db->where('item_id', $item_row->item_id);
                $this->db->where('document_type', strtolower($quotation_data->sales) . '_quotation');
                $query = $this->db->get();

                //print_r($query->result());exit;
                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $key => $page_detail_row) {
                        if($page_detail_row->allow_to_edit == 1){
                            $item_documents = json_decode($item_row->item_documents, true);
                            if(!empty($item_documents)){
                                $matched = $this->applib->search_in_multidimensional_array($item_documents, 'sequence', $page_detail_row->sequence);
                                if(!empty($matched)){
                                    $page_detail_row->detail = $matched[0]['detail'];
                                }
                            }
                        }
                        $pdf->AddPage(
                            'P', //orientation
                            '', //type
                            '', //resetpagenum
                            '', //pagenumstyle
                            '', //suppress
                            $margin_left, //margin-left
                            $margin_right, //margin-right
                            $margin_top, //margin-top
                            $margin_bottom, //margin-bottom
                            0, //margin-header
                            0 //margin-footer
                        );
                        $signature = $this->crud->get_id_by_val('staff','signature','staff_id',$quotation_data->created_by);
                        $vars = array(
                            '{{Price_Table}}' => $print_export_items,
                            '{{Price_Table_With_FreeErectionLine}}' => $Price_Table_With_FreeErectionLine,
                            '{{To_Party_Name}}' => $quotation_data->pname,
                            '{{To_Address}}' => nl2br($quotation_data->address),
                            '{{To_City}}' => $quotation_data->city,
                            '{{To_Pin}}' => !empty($quotation_data->pincode)?' - '.$quotation_data->pincode:'',
                            '{{To_State}}' => $quotation_data->state,
                            '{{To_Country}}' => $quotation_data->country,
                            '{{To_Tel_No}}' => $quotation_data->fax_no,
                            '{{To_Contact_No}}' => $quotation_data->phone_no,
                            '{{To_Email}}' => $quotation_data->email_id,
                            '{{To_Contact_Person}}' => $this->crud->get_id_by_val('contact_person','name','contact_person_id',$quotation_data->kind_attn_id),
                            '{{From_Company_Name}}' => $company_details['name'],
                            '{{From_Contact_Person}}' => $quotation_data->created_name,
                            '{{From_City}}' => $company_details['city'],
                            '{{From_State}}' => $company_details['state'],
                            '{{From_Country}}' => $company_details['country'],
                            '{{From_Tel_No}}' => $company_details['contact_no'],
                            '{{From_Email}}' => $company_details['email_id'],
                            '{{Quotation_Date}}' => date("d/m/Y", strtotime($quotation_data->rev_date)),
                            '{{From_Date}}' => date("d/m/Y", strtotime($quotation_data->rev_date)),
                            '{{From_Ref_No}}' => $this->applib->get_quotation_ref_no($quotation_data->quotation_no,$item_row->item_code),
                            '{{Item_Name}}' => $item_row->item_name,
                            '{{Reference}}' => $this->crud->get_id_by_val('reference','reference','id',$quotation_data->p_reference_id),
                            '{{Sales_Executeve}}' => $this->crud->get_id_by_val('staff','name','staff_id',$quotation_data->created_by),
                            '{{Sales_Person_No}}' => $this->crud->get_id_by_val('staff','contact_no','staff_id',$quotation_data->created_by),
                            '{{Sales_Person_Email}}' => $this->crud->get_id_by_val('staff','email','staff_id',$quotation_data->created_by),
                            '{{Signature_URL}}' => image_url('staff/signature/'.$signature),
                            '{{Company_Name}}' =>  $company_details['name'],
                            '{{GST_No}}' =>  $quotation_data->gst_no,
                            '{{CIN_No}}' =>  $quotation_data->utr_no,
                        );
                        if ($key == 0) {
                            $print_export_items = $this->load->view('sales/quotation/print_price_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
                            $Price_Table_With_FreeErectionLine = $this->load->view('sales/quotation/print_price_freeerection_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
                            $page_detail_row->detail = strtr($page_detail_row->detail, $vars);
                            $page_detail_html = $page_detail_row->detail;

                            $letterpad_header_logo = '';
                            if(empty($letter_pad)){
                        		$letterpad_content = '';
	                        } else {
	                        	if(isset($_POST['lp_master']) && !empty($_POST['lp_master'])){
                                    if (strtolower($quotation_data->sales) == 'domestic') {
                                        $letterpad_content = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
                                    } else {
                                        $letterpad_content = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'export_letter_pad_content');
                                    }
                                    $letterpad_details = $letterpad_content;
	                                $letterpad_content = $letterpad_content->footer_detail;
	                            } else {
	                            	$letterpad_content = '<div style="background:url(' . base_url() . 'resource/image/Quotation_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>';
	                            }
	                        }
	                        if (strtolower($quotation_data->sales) == 'domestic') {
                                $quotation_html = $this->load->view('sales/quotation/print_domestic', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row, 'page_detail_html' => $page_detail_html, 'letterpad_content' => $letterpad_content, 'letterpad_details' => $letterpad_details), true);
                            } else {
                                $quotation_html = $this->load->view('sales/quotation/print_export', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row, 'page_detail_html' => $page_detail_html, 'letterpad_content' => $letterpad_content, 'letterpad_details' => $letterpad_details), true);
                            }
	                        $pdf->WriteHTML($quotation_html);
                        } else {
                            $print_export_items = $this->load->view('sales/quotation/print_price_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
                            $Price_Table_With_FreeErectionLine = $this->load->view('sales/quotation/print_price_freeerection_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
                            $page_detail_row->detail = strtr($page_detail_row->detail, $vars);
                            $pdf->WriteHTML('<div style="padding:10px;">'.$page_detail_row->detail.'</div>');
                        }

                        if(empty($letter_pad)){
                        }else{
                            if(isset($_POST['lp_master']) && !empty($_POST['lp_master'])){
                                if (strtolower($quotation_data->sales) == 'domestic') {
                                    $letterpad_content = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
                                } else {
                                    $letterpad_content = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'export_letter_pad_content');
                                }
                                $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: '. $letterpad_content->header_logo_alignment .';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_content->header_logo) .'" width="100px"></div>');
                                $pdf->SetHTMLFooter($letterpad_content->footer_detail);
                            } else {
                                $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Quotation_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
                            }
                        }
                    }
                } else {
                    $pdf->AddPage(
                        'P', //orientation
                        '', //type
                        '', //resetpagenum
                        '', //pagenumstyle
                        '', //suppress
                        $margin_left, //margin-left
                        $margin_right, //margin-right
                        $margin_top, //margin-top
                        $margin_bottom, //margin-bottom
                        0, //margin-header
                        0 //margin-footer
                    );
                    if (strtolower($quotation_data->sales) == 'domestic') {
                        $quotation_html = $this->load->view('sales/quotation/print_domestic', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row), true);
                    } else {
                        $quotation_html = $this->load->view('sales/quotation/print_export', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row), true);
                    }
                    $pdf->WriteHTML($quotation_html);
                    if(empty($letter_pad)){
                    }else{
                        if(isset($_POST['lp_master']) && !empty($_POST['lp_master'])){
                            if (strtolower($quotation_data->sales) == 'domestic') {
                                $letterpad_content = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'domestic_letter_pad_content');
                            } else {
                                $letterpad_content = $this->crud->get_data_row_by_id('letterpad_content', 'module_name', 'export_letter_pad_content');
                            }
                            $pdf->SetHTMLHeader('<div style="padding-top: 18px; text-align: '. $letterpad_content->header_logo_alignment .';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_content->header_logo) .'" width="100px"></div>');
                            $pdf->SetHTMLFooter($letterpad_content->footer_detail);
                        } else {
                            $pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Quotation_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px; "></div>');
                        }
                    }
                }
                /*echo "<pre>";
                print_r($pdf);
                exit();*/
                $party_name  = str_replace(' ', '_', $this->crud->get_id_by_val('party','party_name','party_id',$quotation_data->party_id));
                $pdfFilePath = $quotation_data->quotation_no . "_" . $item_row->item_code . "_" . $party_name . ".pdf";
                $pdf->Output($pdfFilePath, "I");
            }
        }
	}

	function quotation_download($id){
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$company_details = $this->applib->get_company_detail();
		$quotation_data = $this->crud->get_column_value_by_quotation($id);
		$kind_attn = $this->getKindAttnParty($quotation_data->party_id);
		$quotation_items = $this->crud->get_items_detail_by_quotation($id);
		$dir_name = time() . "_quotation";
		$upload_path = "./uploads/" . $dir_name . "/";
		mkdir($upload_path);
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		//echo '<pre>';print_r($margin_company);exit;
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		foreach ($quotation_items as $item_row) {
			$this->load->library('m_pdf');
			$param = "'utf-8','A4'";
			$pdf = $this->m_pdf->load($param);
			$this->db->select('detail');
			$this->db->from('item_documents');
			if (strtolower($quotation_data->sales) == 'domestic') {
				$this->db->where('document_type', 'domestic_quotation');
			} else {
				$this->db->where('document_type', 'export_quotation');
			}
			$this->db->where('item_id', $item_row->item_id);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $key => $page_detail_row) {
                    if($page_detail_row->allow_to_edit == 1){
                        $item_documents = json_decode($item_row->item_documents, true);
                        if(!empty($item_documents)){
                            $matched = $this->applib->search_in_multidimensional_array($item_documents, 'sequence', $page_detail_row->sequence);
                            if(!empty($matched)){
                                $page_detail_row->detail = $matched[0]['detail'];
                            }
                        }
                    }
					$pdf->AddPage(
						'P', //orientation
						'', //type
						'', //resetpagenum
						'', //pagenumstyle
						'', //suppress
						$margin_left, //margin-left
						$margin_right, //margin-right
						$margin_top, //margin-top
						$margin_bottom, //margin-bottom
						0, //margin-header
						0 //margin-footer
					);
                    $signature = $this->crud->get_id_by_val('staff','signature','staff_id',$quotation_data->created_by);
					$vars = array(
						'{{Price_Table}}' => $print_export_items,
						'{{Price_Table_With_FreeErectionLine}}' => $Price_Table_With_FreeErectionLine,
						'{{To_Party_Name}}' => $quotation_data->pname,
						'{{To_Address}}' => nl2br($quotation_data->address),
						'{{To_City}}' => $quotation_data->city,
						'{{To_Pin}}' => !empty($quotation_data->pincode)?' - '.$quotation_data->pincode:'',
						'{{To_State}}' => $quotation_data->state,
						'{{To_Country}}' => $quotation_data->country,
						'{{To_Tel_No}}' => $quotation_data->fax_no,
						'{{To_Contact_No}}' => $quotation_data->phone_no,
						'{{To_Email}}' => $quotation_data->email_id,
						'{{To_Contact_Person}}' => $this->crud->get_id_by_val('contact_person','name','contact_person_id',$quotation_data->kind_attn_id),
						'{{From_Company_Name}}' => $company_details['name'],
						'{{From_Contact_Person}}' => $quotation_data->created_name,
						'{{From_City}}' => $company_details['city'],
						'{{From_State}}' => $company_details['state'],
						'{{From_Country}}' => $company_details['country'],
						'{{From_Tel_No}}' => $company_details['contact_no'],
						'{{From_Email}}' => $company_details['email_id'],
						'{{Quotation_Date}}' => date("d/m/Y", strtotime($quotation_data->rev_date)),
                        '{{From_Date}}' => date("d/m/Y", strtotime($quotation_data->rev_date)),
						'{{From_Ref_No}}' => $this->applib->get_quotation_ref_no($quotation_data->quotation_no,$item_row->item_code),
						'{{Item_Name}}' => $item_row->item_name,
						'{{Reference}}' => $this->crud->get_id_by_val('reference','reference','id',$quotation_data->p_reference_id),
						'{{Sales_Executeve}}' => $this->crud->get_id_by_val('staff','name','staff_id',$quotation_data->created_by),
						'{{Sales_Person_No}}' => $this->crud->get_id_by_val('staff','contact_no','staff_id',$quotation_data->created_by),
						'{{Sales_Person_Email}}' => $this->crud->get_id_by_val('staff','email','staff_id',$quotation_data->created_by),
                        '{{Signature_URL}}' => image_url('staff/signature/'.$signature),
                        '{{Company_Name}}' =>  $company_details['name'],
                        '{{GST_No}}' =>  $quotation_data->gst_no,
                        '{{CIN_No}}' =>  $quotation_data->utr_no,
					);
					if ($key == 0) {
						$print_export_items = $this->load->view('sales/quotation/print_price_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
						$Price_Table_With_FreeErectionLine = $this->load->view('sales/quotation/print_price_freeerection_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
						$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
						$page_detail_html = $page_detail_row->detail;
						if (strtolower($quotation_data->sales) == 'domestic') {
							$quotation_html = $this->load->view('sales/quotation/print_domestic', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row, 'page_detail_html' => $page_detail_html), true);
						} else {
							$quotation_html = $this->load->view('sales/quotation/print_export', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row, 'page_detail_html' => $page_detail_html), true);
						}
						$pdf->WriteHTML($quotation_html);
					} else {
						$print_export_items = $this->load->view('sales/quotation/print_price_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
						$Price_Table_With_FreeErectionLine = $this->load->view('sales/quotation/print_price_freeerection_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
						$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
						$pdf->WriteHTML($page_detail_row->detail);
					}
					$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Quotation_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
				}
			} else {
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				if (strtolower($quotation_data->sales) == 'domestic') {
					$quotation_html = $this->load->view('sales/quotation/print_domestic', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row), true);
				} else {
					$quotation_html = $this->load->view('sales/quotation/print_export', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row), true);
				}
				$pdf->WriteHTML($quotation_html);
				$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Quotation_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
			}

			$party_name  = str_replace(' ', '_', $this->crud->get_id_by_val('party','party_name','party_id',$quotation_data->party_id));
			$filename = $quotation_data->quotation_no . "_" . $item_row->item_code . "_" . $party_name . ".pdf";
			$pdf->Output($upload_path . $filename, "F");
		}
		/*--------------- Create Zip File  --------------------*/
		$rootPath = realpath($upload_path);
		$zip = new ZipArchive();
		$ZipFileName = "$dir_name.zip";
		$ZipFileUrl = "./uploads/$ZipFileName";
		$zip->open($ZipFileUrl, 1);
		$filesToDelete = array();
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);
		foreach ($files as $name => $file) {
			if (!$file->isDir()) {
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen($rootPath) + 1);
				$zip->addFile($filePath, $relativePath);
				$filesToDelete[] = $filePath;
			}
		}
		$zip->close();
		/*---------- Remove all files from dir & dir ---------- */
		foreach ($filesToDelete as $file) {
			unlink($file);
		}
		rmdir($upload_path);
		/*---------------- Download Zip --------------------*/
		header("Content-type: application/zip");
		header("Content-Disposition: attachment; filename=quotation-data.zip");
		header("Content-length: " . filesize($ZipFileUrl));
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile("$ZipFileUrl");
		/*---------------- Remove Zip --------------------*/
		unlink($ZipFileUrl);
	}

	function quotation_email($id){
		if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$company_details = $this->applib->get_company_detail();
		$quotation_data = $this->crud->get_column_value_by_quotation($id);
		$kind_attn = $this->getKindAttnParty($quotation_data->party_id);
		$quotation_items = $this->crud->get_items_detail_by_quotation($id);
		$this->load->library('m_pdf');
		$param = "'utf-8','A4'";
		$files_urls = array();
		$margin_company = $this->crud->get_all_with_where('company','','',array('id' => COMPANY_ID));
		$margin_left = $margin_company[0]->margin_left;
		$margin_right = $margin_company[0]->margin_right;
		$margin_top = $margin_company[0]->margin_top;
		$margin_bottom = $margin_company[0]->margin_bottom;
		foreach ($quotation_items as $item_row) {
			$pdf = $this->m_pdf->load($param);
			$this->db->select('detail');
			$this->db->from('item_documents');
			$this->db->where('item_id', $item_row->item_id);
			if (strtolower($quotation_data->sales) == 'domestic') {
				$this->db->where('document_type', 'domestic_quotation');
			} else {
				$this->db->where('document_type', 'export_quotation');
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $key => $page_detail_row) {
                    if($page_detail_row->allow_to_edit == 1){
                        $item_documents = json_decode($item_row->item_documents, true);
                        if(!empty($item_documents)){
                            $matched = $this->applib->search_in_multidimensional_array($item_documents, 'sequence', $page_detail_row->sequence);
                            if(!empty($matched)){
                                $page_detail_row->detail = $matched[0]['detail'];
                            }
                        }
                    }
					$pdf->AddPage(
						'P', //orientation
						'', //type
						'', //resetpagenum
						'', //pagenumstyle
						'', //suppress
						$margin_left, //margin-left
						$margin_right, //margin-right
						$margin_top, //margin-top
						$margin_bottom, //margin-bottom
						0, //margin-header
						0 //margin-footer
					);
                    $signature = $this->crud->get_id_by_val('staff','signature','staff_id',$quotation_data->created_by);
					$vars = array(
						'{{Price_Table}}' => $print_export_items,
						'{{Price_Table_With_FreeErectionLine}}' => $Price_Table_With_FreeErectionLine,
						'{{To_Party_Name}}' => $quotation_data->pname,
						'{{To_Address}}' => nl2br($quotation_data->address),
						'{{To_City}}' => $quotation_data->city,
						'{{To_Pin}}' => !empty($quotation_data->pincode)?' - '.$quotation_data->pincode:'',
						'{{To_State}}' => $quotation_data->state,
						'{{To_Country}}' => $quotation_data->country,
						'{{To_Tel_No}}' => $quotation_data->fax_no,
						'{{To_Contact_No}}' => $quotation_data->phone_no,
						'{{To_Email}}' => $quotation_data->email_id,
						'{{To_Contact_Person}}' => $this->crud->get_id_by_val('contact_person','name','contact_person_id',$quotation_data->kind_attn_id),
						'{{From_Company_Name}}' => $company_details['name'],
						'{{From_Contact_Person}}' => $quotation_data->created_name,
						'{{From_City}}' => $company_details['city'],
						'{{From_State}}' => $company_details['state'],
						'{{From_Country}}' => $company_details['country'],
						'{{From_Tel_No}}' => $company_details['contact_no'],
						'{{From_Email}}' => $company_details['email_id'],
						'{{Quotation_Date}}' => date("d/m/Y", strtotime($quotation_data->rev_date)),
                        '{{From_Date}}' => date("d/m/Y", strtotime($quotation_data->rev_date)),
						'{{From_Ref_No}}' => $this->applib->get_quotation_ref_no($quotation_data->quotation_no,$item_row->item_code),
						'{{Item_Name}}' => $item_row->item_name,
						'{{Reference}}' => $this->crud->get_id_by_val('reference','reference','id',$quotation_data->p_reference_id),
						'{{Sales_Executeve}}' => $this->crud->get_id_by_val('staff','name','staff_id',$quotation_data->created_by),
						'{{Sales_Person_No}}' => $this->crud->get_id_by_val('staff','contact_no','staff_id',$quotation_data->created_by),
						'{{Sales_Person_Email}}' => $this->crud->get_id_by_val('staff','email','staff_id',$quotation_data->created_by),
                        '{{Signature_URL}}' => image_url('staff/signature/'.$signature),
                        '{{Company_Name}}' =>  $company_details['name'],
                        '{{GST_No}}' =>  $quotation_data->gst_no,
                        '{{CIN_No}}' =>  $quotation_data->utr_no,
					);
					if ($key == 0) {
						$print_export_items = $this->load->view('sales/quotation/print_price_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
						$Price_Table_With_FreeErectionLine = $this->load->view('sales/quotation/print_price_freeerection_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);						
						$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
						$page_detail_html = $page_detail_row->detail;
						if (strtolower($quotation_data->sales) == 'domestic') {
							$quotation_html = $this->load->view('sales/quotation/print_domestic', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row, 'page_detail_html' => $page_detail_html), true);
						} else {
							$quotation_html = $this->load->view('sales/quotation/print_export', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row, 'page_detail_html' => $page_detail_html), true);
						}
						$pdf->WriteHTML($quotation_html);
					} else {
						$print_export_items = $this->load->view('sales/quotation/print_price_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
						$Price_Table_With_FreeErectionLine = $this->load->view('sales/quotation/print_price_freeerection_table', array('quotation_data' => $quotation_data, 'item_row' => $item_row), true);
						$page_detail_row->detail = strtr($page_detail_row->detail, $vars);
						$pdf->WriteHTML($page_detail_row->detail);
					}
					$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Quotation_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
				}
			} else {
				$pdf->AddPage(
					'P', //orientation
					'', //type
					'', //resetpagenum
					'', //pagenumstyle
					'', //suppress
					$margin_left, //margin-left
					$margin_right, //margin-right
					$margin_top, //margin-top
					$margin_bottom, //margin-bottom
					0, //margin-header
					0 //margin-footer
				);
				if (strtolower($quotation_data->sales) == 'domestic') {
					$quotation_html = $this->load->view('sales/quotation/print_domestic', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row), true);
				} else {
					$quotation_html = $this->load->view('sales/quotation/print_export', array('quotation_data' => $quotation_data, 'kind_attn' => $kind_attn, 'item_row' => $item_row), true);
				}
				$pdf->WriteHTML($quotation_html);
				$pdf->SetHTMLFooter('<div style="background:url(' . base_url() . 'resource/image/Quotation_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:400px;"></div>');
			}
			$party_name  = str_replace(' ', '_', $this->crud->get_id_by_val('party','party_name','party_id',$quotation_data->party_id));
			$pdfFilePath = "./uploads/" . $quotation_data->quotation_no . "_" . $item_row->item_code . "_" . $party_name . ".pdf";
			$files_urls[] = $pdfFilePath;
			$pdf->Output($pdfFilePath, "F");
		}
		$this->db->where('id', $id);
		$this->db->update('quotations', array('pdf_url' => serialize($files_urls)));
		redirect(base_url() . 'mail-system3/document-mail/quotation/' . $id);
	}

	function getKindAttnParty($party_id){
		$this->db->select('cp.contact_person_id,cp.priority,cp.name,cp.mobile_no,cp.phone_no,dept.department,desi.designation,cp.email');
		$this->db->from('contact_person cp');
		$this->db->join('department dept', 'dept.department_id = cp.department_id', 'left');
		$this->db->join('designation desi', 'desi.designation_id = cp.designation_id', 'left');
		$this->db->where('cp.party_id', $party_id);
		$this->db->where('cp.active !=',0);
		$this->db->order_by('cp.priority,cp.contact_person_id');
		$this->db->limit(1);
		$query = $this->db->get();
		$response = array();
		if ($query->num_rows() > 0) {
			$response = $query->row_array(0);
		}
		return $response;
	}

	function feed_quotation_items($quotation_id){
		$this->db->select('*');
		$this->db->from('quotation_items');
		$this->db->where('quotation_id', $quotation_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$item_data = $query->result();
			echo json_encode(array('success' => true, 'message' => 'Item loaded successfully!', 'item_data' => $item_data));
			exit();
		} else {
			echo json_encode(array('success' => false, 'message' => 'Please select item!'));
			exit();
		}
	}

	function get_inquiry_by_id($inquiry_id){
		$this->db->select("i.*,st.name as assigned_by");
		$this->db->from('inquiry i');
		$this->db->join('staff st', 'st.staff_id = i.created_by', 'left');
		$this->db->where('i.inquiry_id', $inquiry_id);
		return $this->db->get()->row(0);
	}

	function delete_quotation($id){
        if (!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$role_delete = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "delete");
		if ($role_delete) {
			$enquiry_id = $this->crud->get_id_by_val('quotations', 'enquiry_id', 'id', $id);
			$this->crud->update('inquiry', array("inquiry_status_id" => PENDING_ENQUIRY_ID), array('inquiry_id' => $enquiry_id));

			$where_array = array("id" => $id);
			$this->crud->delete("quotations", $where_array);
			$where_array = array("quotation_id" => $id);
			$this->crud->delete("quotation_items", $where_array);
			$this->crud->delete("followup_history", $where_array);

			$session_data = array(
				'success_message' => "Quotation has been deleted !"
			);
			$this->session->set_userdata($session_data);
			redirect(BASE_URL . "quotation/quotation_list");
		} else {
			$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
			redirect("/");
		}
	}
    
    function ajax_load_item_print_details($item_id, $sales_id){
        
		if ($item_id != 0) {
            if($sales_id == PARTY_TYPE_EXPORT_ID){
                $where = array('item_id' => $item_id, 'allow_to_edit' => 1, 'document_type' => 'export_quotation');
            } else if($sales_id == PARTY_TYPE_DOMESTIC_ID){
                $where = array('item_id' => $item_id, 'allow_to_edit' => 1, 'document_type' => 'domestic_quotation');
            }
			$item_documents = $this->crud->get_row_by_id('item_documents',$where);
            $return = $item_documents;
//            echo "<pre>"; print_r($item_documents); exit;
			print json_encode($return);
			exit;
		}
	}
}
