<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Item
 * &@property AppLib $applib
 */
class Item extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logged_in')) {
			redirect('');
		}
		$this->load->model('AppModel','app_model');
		$this->load->model('Crud','crud');
	}
	
	/* function index()
	{
		
	} */
	
	function item_master(){
		$result_data = $this->crud->get_all_records('items','id','ASC');
		set_page('item/item_master',array('results'=>$result_data));
	}

	function file_upload_demo(){
		set_page('item/file_upload_demo');
	}
	
	function add_item_master($id = 0)
	{
		$data = array();
		if (isset($id) && !empty($id)) {
			$where_array['id'] = $id;
			$item = $this->crud->get_all_with_where('items', 'item_category', 'ASC', $where_array);
			$data['item_categories'] = $this->get_item_categories();
			$data['id'] = $item[0]->id;
			$data['item_category'] = $item[0]->item_category;
			$data['item_code1'] = $item[0]->item_code1;
			$data['item_code2'] = $item[0]->item_code2;
			$data['uom'] = $item[0]->uom;
			$data['qty'] = $item[0]->qty;
			$data['item_type'] = $item[0]->item_type;
			$data['stock'] = $item[0]->stock;
			$data['document'] = $item[0]->document;
			$data['item_group'] = $item[0]->item_group;
			$data['item_active'] = $item[0]->item_active;
			$data['item_name'] = $item[0]->item_name;
			$data['cfactor'] = $item[0]->cfactor;
			$data['conv_uom'] = $item[0]->conv_uom;
			$data['item_mpt'] = $item[0]->item_mpt;
			$data['item_status'] = $item[0]->item_status;
			$data['image'] = $item[0]->image;
			$data['quotation_detail_pages'] = array();
			$this->db->select('page_detail');
			$this->db->from('item_quotation_detail');
			$this->db->where('item_id',$id);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $page_row) {
					$data['quotation_detail_pages'][] = $page_row->page_detail;
				}
			}
			$data['item_files'] = array();
			$this->db->select('id,file_url');
			$this->db->from('item_files');
			$this->db->where('item_id',$id);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result_array() as $file_row) {
					$data['item_files'][] = $file_row;
				}
			}
		}
		set_page('item/add_item_master', $data);
	}

	function upload_item_file($item_id = 0)
	{
		if ($item_id != 0) {
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('item_file')) {
				return false;
			}
			$data = $this->upload->data();
			$file_data = array(
				'name' => $data['file_name'],
				'size' => $data['file_size'] * 1024,
				'type' => $data['file_type'],
				'url' => './uploads/' . $data['file_name'],
			);
			echo json_encode($file_data);
			exit();
		}

	}

	function update_item_master()
	{
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$post_data['item_category'] = $this->input->post('item_category');
		$post_data['item_code1'] = $this->input->post('item_code1');
		$post_data['uom'] = $this->input->post('uom');
		$post_data['qty'] = $this->input->post('qty');
		$post_data['item_type'] = $this->input->post('item_type');
		$post_data['stock'] = $this->input->post('stock');
		$post_data['item_group'] = $this->input->post('item_group');
		$post_data['item_active'] = isset($_POST['item_active']) ? $_POST['item_active'] : '0';
		$post_data['item_name'] = $this->input->post('item_name');
		$post_data['cfactor'] = $this->input->post('cfactor');
		$post_data['conv_uom'] = $this->input->post('conv_uom');
		$post_data['item_mpt'] = $this->input->post('item_mpt');
		$post_data['item_status'] = $this->input->post('item_status');
		$where_array['id'] = $this->input->post('id');
		$post_data['image'] = $this->crud->get_column_value_by_id('items', 'image', $where_array);
		if (isset($_FILES['document'])) {
			if (!empty($_FILES['document']['name'])) {
				unlink(base_url('resource/uploads/document/' . $_FILES['document']['name']));
				$this->crud->upload_file('document', 'resource/uploads/document');
				$post_data['document'] = $_FILES['document']['name'];
			} else {
				$post_data['document'] = $this->crud->get_column_value_by_id('items', 'document', $where_array);
			}
		}
		if (isset($_FILES['image'])) {
			if (!empty($_FILES['image']['name'])) {
				unlink('resource/uploads/images/items/' . $_FILES['image']['name']);
				$this->crud->upload_file('image', 'resource/uploads/images/items');
				$post_data['image'] = $_FILES['image']['name'];
			} else {
				$post_data['image'] = $this->crud->get_column_value_by_id('items', 'image', $where_array);
			}
		}
		$result = $this->crud->update('items', $post_data, $where_array);

		$item_id = $this->input->post('id');
		$item_quotation_detail = $_POST['item_quotation_detail'];
		if (!empty($item_quotation_detail)) {
			/*--- Delete Old Detail Pages ---*/
			$this->db->where('item_id', $item_id);
			$this->db->delete('item_quotation_detail');

			/*--- Insert New Detail Pages ---*/
			foreach ($item_quotation_detail as $item_quotation_detail_row) {
				if ($item_quotation_detail_row != '') {
					$this->crud->insert('item_quotation_detail', array('item_id' => $item_id, 'page_detail' => $item_quotation_detail_row));
				}
			}
		}

		if (count($_FILES['item_files']['name']) > 0) {
			foreach ($_FILES['item_files']['name'] as $key => $file_name) {
				if (!empty($file_name) && $file_name != '') {
					$_FILES['temp_file'] = array(
						'name' => $file_name,
						'type' => $_FILES['item_files']['type'][$key],
						'tmp_name' => $_FILES['item_files']['tmp_name'][$key],
						'error' => $_FILES['item_files']['error'][$key],
						'size' => $_FILES['item_files']['size'][$key],
					);
					$file_url = $this->applib->upload_image('temp_file', './resource/uploads/images/items/', true);
					if ($file_url != false) {
						$item_file_data = array(
							'item_id' => $item_id,
							'file_url' => $file_url
						);
						$this->crud->insert('item_files', $item_file_data);
					}
				}
			}
		}

		if ($result) {
			$data['success'] = $result;
			$data['message'] = 'Item Updated Successfully';
			$item_files = array();
			$this->db->select('id,file_url');
			$this->db->from('item_files');
			$this->db->where('item_id',$item_id);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result_array() as $file_row) {
					$item_files[] = $file_row;
				}
			}
			$data['item_file_listing'] = $this->load->view('shared/item_files',array('item_files'=>$item_files),true);
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Item Updated Successfully');
			echo json_encode($data);
		}
	}

	/**
	 * @param $file_id
	 */
	function remove_item_file($file_id)
	{
		$file_url = $_POST['file_url'];
		$this->db->where('id',$file_id);
		$this->db->delete('item_files');
		if(file_exists($file_url)){
			unlink($file_url);
		}
		echo json_encode(array('success'=>true,'message'=>"File removed successfully!"));
	}

	/**
	 * @return mixed
	 */
	function get_item_categories()
	{
		$this->db->select('*');
		$this->db->from('item_category');
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @param $category_id
	 * @return mixed
	 */
	function get_items_by_category($category_id)
	{
		$this->db->select('id,item_name');
		$this->db->from('items');
		$this->db->where('item_category', $category_id);
		$query = $this->db->get();
		echo json_encode($query->result());
	}
	
	function save_item_master()
	{
		$post_data = $this->input->post();
		/*echo "<pre>";
		print_r($_FILES);
		print_r($post_data);
		die;*/

		$post_data['created_at'] = date('Y-m-d H:i:s');
		$post_data['document'] = '';
		$post_data['image'] = '';
		$post_data['item_active'] = isset($_POST['item_active']) ? $_POST['item_active'] : '0';
		if (isset($_FILES['document'])) {
			$post_data['document'] = $this->crud->upload_file('document', 'resource/uploads/document');
		}
		if (isset($_FILES['image'])) {
			$post_data['image'] = $this->crud->upload_file('image', 'resource/uploads/images/items');
		}
		$item_quotation_detail = $post_data['item_quotation_detail'];
		$item_id = $post_data['id'];
		unset($post_data['item_quotation_detail']);
		unset($post_data['id']);
		if ($item_id == '' || $item_id == null) {
			$item_id = $this->crud->insert('items', $post_data);
			if (!empty($item_quotation_detail)) {
				foreach ($item_quotation_detail as $item_quotation_detail_row) {
					if ($item_quotation_detail_row != '') {
						$this->crud->insert('item_quotation_detail', array('item_id' => $item_id, 'page_detail' => $item_quotation_detail_row));
					}
				}
			}
		} else {
			$this->db->where('id', $item_id);
			$this->db->update('items', $post_data);
			if (!empty($item_quotation_detail)) {
				/*--- Delete Old Detail Pages ---*/
				$this->db->where('item_id', $item_id);
				$this->db->delete('item_quotation_detail');

				/*--- Insert New Detail Pages ---*/
				foreach ($item_quotation_detail as $item_quotation_detail_row) {
					if ($item_quotation_detail_row != '') {
						$this->crud->insert('item_quotation_detail', array('item_id' => $item_id, 'page_detail' => $item_quotation_detail_row));
					}
				}
			}
		}

		$item_file_ids = array();
		if (count($_FILES['item_files']['name']) > 0) {
			foreach ($_FILES['item_files']['name'] as $key => $file_name) {
				if (!empty($file_name) && $file_name != '') {
					$_FILES['temp_file'] = array(
						'name' => $file_name,
						'type' => $_FILES['item_files']['type'][$key],
						'tmp_name' => $_FILES['item_files']['tmp_name'][$key],
						'error' => $_FILES['item_files']['error'][$key],
						'size' => $_FILES['item_files']['size'][$key],
					);
					$file_url = $this->applib->upload_image('temp_file', './resource/uploads/images/items/', true);
					if ($file_url != false) {
						$item_file_data = array(
							'item_id' => $item_id,
							'file_url' => $file_url
						);
						$item_file_ids[] = $this->crud->insert('item_files', $item_file_data);
					}
				}
			}
		}

		if ($item_id > 0) {
			$data['success'] = true;
			$data['item_id'] = $item_id;
			$data['message'] = "Item successfully saved!";
			$item_files = array();
			$this->db->select('id,file_url');
			$this->db->from('item_files');
			$this->db->where('item_id',$item_id);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result_array() as $file_row) {
					$item_files[] = $file_row;
				}
			}
			$data['item_file_listing'] = $this->load->view('shared/item_files',array('item_files'=>$item_files),true);
			echo json_encode($data);
		} else {
			echo json_encode(array('success' => false, 'message' => "Something wrong!"));
		}
		exit();
	}

	/**
	 * @param $item_id
	 */
	function save_item_bom($item_id)
	{
		$item_bom_data = $this->input->post();
		$item_bom_data['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('item_bom',$item_bom_data);
		$item_bom_id = $this->db->insert_id();
		echo json_encode(array('success'=>true,'message'=>"Item BOM successfully added!.",'item_bom_id'=>$item_bom_id));
		exit();
	}

	/**
	 * @param $id
	 */
	function item_category($id){
		$category = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$category = $this->crud->get_all_with_where('item_category','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_category','id','ASC');
		set_page('item/item_category',array('results'=>$result_data, 'id' => $category[0]->id, 'category_name' => $category[0]->item_category));
	}

	function update_item_category()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_category', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Category Updated Successfully');
			echo json_encode($post_data);
		}
	}


	function add_item_category()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_category',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Category Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function item_group_code($id){
		$item_group = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$item_group = $this->crud->get_all_with_where('item_group_code','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_group_code','id','ASC');
		//print_r($result_data); exit;
		set_page('item/item_group_code',array('results'=>$result_data, 'id' => $item_group[0]->id, 'ig_code' => $item_group[0]->ig_code ,'ig_description' => $item_group[0]->ig_description));
	}
	
	function update_item_group_codes()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_group_code', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Group Codes Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_item_group_code()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_group_code',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Group Codes Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function item_type($id){
		$item_type = array();
		if(isset($id) && !empty($id)){
		$where_array['id'] = $id;
		$item_type = $this->crud->get_all_with_where('item_type','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_type','id','ASC');
		set_page('item/item_type',array('results'=>$result_data, 'id' => $item_type[0]->id, 'item_type' => $item_type[0]->item_type));
	}
	
	function update_item_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_type', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Type Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_item_type()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Type Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function item_status($id){
		$items_status = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$items_status = $this->crud->get_all_with_where('item_status','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_status','id','ASC');
		set_page('item/item_status',array('results'=>$result_data, 'id' => $items_status[0]->id, 'item_status' => $items_status[0]->item_status));
	}
	
	function update_item_status()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_status', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Status Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_item_status()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_status',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Status Added Successfully');
			echo json_encode($post_data);
		}
	}

	function material_process_type($id){
		$material_process_types = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$material_process_types = $this->crud->get_all_with_where('material_process_type','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('material_process_type','id','ASC');
		set_page('item/material_process_type',array('results'=>$result_data, 'id' => $material_process_types[0]->id, 'material_process_type' => $material_process_types[0]->material_process_type));
	}

	function update_material_process_type()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('material_process_type', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Material Process Type Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_material_process_type()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('material_process_type',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Material Process Type Added Successfully');
			echo json_encode($post_data);
		}
	}

	
	
	function main_group($id){
		$main_groups = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$main_groups = $this->crud->get_all_with_where('main_group','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('main_group','id','ASC');
		set_page('item/main_group',array('results'=>$result_data, 'id' => $main_groups[0]->id, 'code' => $main_groups[0]->code ,  'item_group' => $main_groups[0]->item_group , 'description' => $main_groups[0]->description));
	}

	function update_main_group()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('main_group', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Group Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_main_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('main_group',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Item Group Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function sub_group($id){
		$sub_groups = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$sub_groups = $this->crud->get_all_with_where('sub_group','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('sub_group','id','ASC');
		set_page('item/sub_group',array('results'=>$result_data, 'id' => $sub_groups[0]->id, 'sub_item_group' => $sub_groups[0]->sub_item_group));
	}

	function update_sub_group()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('sub_group', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Sub Item Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_sub_group()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('sub_group',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Sub Item Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function make($id){
		$make = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$make = $this->crud->get_all_with_where('item_make','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_make','id','ASC');
		set_page('item/make',array('results'=>$result_data,'id' => $make[0]->id, 'item_make' => $make[0]->item_make));
	}
	
	function add_make()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_make',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Make Item Added Successfully');
			echo json_encode($post_data);
		}
	}

	function update_make()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_make', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Make Item Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function drawing_number($id){
		$drawing_numbers = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$drawing_numbers = $this->crud->get_all_with_where('drawing_number','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('drawing_number','id','ASC');
		set_page('item/drawing_number',array('results'=>$result_data, 'id' => $drawing_numbers[0]->id, 'drawing_number' => $drawing_numbers[0]->drawing_number));
	}

	function update_drawing_number()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('drawing_number', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Drawing Number Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_drawing_number()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('drawing_number',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Drawing Number Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function material_specification($id){
		$material_specifications = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$material_specifications = $this->crud->get_all_with_where('material_specification','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('material_specification','id','ASC');
		set_page('item/material_specification',array('results'=>$result_data, 'id' => $material_specifications[0]->id,'material' => $material_specifications[0]->material, 'material_specification' => $material_specifications[0]->material_specification));
	}
	
	function update_material_specification()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('material_specification', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Material Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_material_specification()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('material_specification',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Material Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function item_class($id){
		$items_class = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$items_class = $this->crud->get_all_with_where('item_class','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('item_class','id','ASC');
		set_page('item/item_class',array('results'=>$result_data, 'id' => $items_class[0]->id,'item_class' => $items_class[0]->item_class));
	}
	
	function update_item_class()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('item_class', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Class Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_item_class()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('item_class',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Class Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function excise($id){
		$excises = array();
		if(isset($id) && !empty($id)){
			$where_array['id'] = $id;
			$excises = $this->crud->get_all_with_where('excise','id','ASC',$where_array);
		}
		$result_data = $this->crud->get_all_records('excise','id','ASC');
		set_page('item/excise',array('results'=>$result_data, 'id' => $excises[0]->id,'description' => $excises[0]->description, 'sub_heading' => $excises[0]->sub_heading));
	}
	
	function update_excise()
	{
		$post_data = $this->input->post();
		$where_array['id'] = $post_data['id'];
		$post_data['updated_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->update('excise', $post_data, array('id' => $post_data['id']));
		
		if ($result) {
			
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Excise Updated Successfully');
			echo json_encode($post_data);
		}
	}
	
	function add_excise()
	{
		$post_data = $this->input->post();
		$post_data['created_at'] = date('Y-m-d H:i:s');
		$result = $this->crud->insert('excise',$post_data);
		if($result)
		{
			$post_data['id'] = $result;
			$this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Excise Added Successfully');
			echo json_encode($post_data);
		}
	}
	
	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		$this->crud->delete($table,array($id_name=>$id));
		$this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
	}
}
