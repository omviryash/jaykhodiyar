<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php
			$add_role= $this->applib->have_access_role(COMMON_PLACE_SMS_MENU_ID,"add");
			?>
			<small class='text-primary text-bold'>Sent SMS List</small>
			<?php if ($add_role) { ?>
				<a href="<?=base_url()?>sms/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add SMS</a>
			<?php } ?>
		</h1>
	</section>
	<div class="clearfix">
		<div class="row">
			<div style="margin: 15px;">
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-body">
                            <label class="col-md-2">Module Name :</label>
                            <div class="col-md-4">
                                <select name="module_name" id="module_name" class="form-control input-sm">
                                    <option value="all" >All</option>
                                    <?php foreach($modules as $module){ ?>
                                        <option value="<?php echo $module; ?>"><?php echo $module; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
							<div class="row">
								<div class="clearfix"></div>
								<div class="col-md-12">
									<table id="sent_sms_list" class="table custom-table agent-table" width="100%">
										<thead>
											<tr>      		 
												<th>Module Name</th>
												<th>Receiver Name</th>
												<th>Number</th>
												<th>SMS Topic</th>
                                                <th>Message</th>
												<th>Send By</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>                                                
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var table;
	$(document).ready(function() {
		table = $('#sent_sms_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
			"ajax": {
				"url": "<?php echo site_url('sms/sent_sms_list_datatable')?>",
				"type": "POST",
                "data":function(d){
                    d.module_name = $("#module_name").val();
                }
			},
			"scroller": {
				"loadingIndicator": true,
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%",
			"deferRender": true,
			"scrollY": 500,            
		}); 
        
        $("#module_name").select2();
        $(document).on('change',"#module_name",function() {
            table.draw();
        });
	});

</script>
