<div class="content-wrapper">
    <style>
        .select2-container--open .select2-dropdown--below { border: 1px solid #2b2d3f; top: 0px; left: -1px; }
    </style>
    <form class="form-horizontal" action="<?= base_url('sms/save_send_sms') ?>" method="post" id="save_send_sms" novalidate>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">SMS</small>
                <?php
                $sms_view_role = $this->applib->have_access_role(COMMON_PLACE_SMS_MENU_ID, "view");
                $sms_add_role = $this->applib->have_access_role(COMMON_PLACE_SMS_MENU_ID, "add");
                ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" id="save_send_sms" style="margin: 5px;">Save & Send </button>
                <?php if ($sms_view_role): ?>
                    <a href="<?= base_url() ?>sms/sent_sms_list" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Sent SMS List</a>
                <?php endif; ?>
                <?php if ($sms_add_role): ?>
                    <a href="<?= BASE_URL ?>sms/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add SMS</a>
                <?php endif; ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">SMS Details</a></li>   
                        <?php if (isset($sms_id) && !empty($sms_id)) { ?>
                            <li class=""><a href="#tab_2" data-toggle="tab">Login Details</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <!-- Horizontal Form -->
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> SMS Details</legend>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="sms_topic" class="col-md-3 input-sm">Select Topic<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-md-6">
                                                            <select name="sms_topic" id="sms_topic" class="form-control input-sm"></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-md-3 input-sm">&nbsp;</label>
                                                        <div class="col-md-9">
                                                            <div><label>Variables : </label> {{Party_Name}}, {{Supplier_Name}}, {{Staff_Name}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="message" class="col-md-3 input-sm">Message<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-md-9">
                                                            <textarea name="message" id="message" class="form-control" value="" rows="5"></textarea> 
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label>Send To : </label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <?php if($this->applib->have_access_role(COMMON_PLACE_SMS_SEND_TO_LIST_ID, "party")) { ?>
                                                        <div class="form-group">
                                                            <label for="party" class="col-md-3 input-sm">Party</label>
                                                            <div class="col-md-6">  
                                                                <select name="party_id[]" id="party_id" class="form-control input-sm" multiple=""></select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="all_party" class="input-sm">Check For All Party</label>
                                                                <input type="checkbox" name="all_party" id="all_party"  />
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                    <?php if($this->applib->have_access_role(COMMON_PLACE_SMS_SEND_TO_LIST_ID, "supplier")) { ?>
                                                        <div class="form-group">
                                                            <label for="supplier" class="col-md-3 input-sm">Supplier</label>
                                                            <div class="col-md-6">  
                                                                <select name="supplier[]" id="supplier" class="form-control input-sm" multiple=""></select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="all_supplier" class="input-sm">Check For All Supplier</label> <input type="checkbox" name="all_supplier" id="all_supplier" value="all_supplier" />
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                    <?php if($this->applib->have_access_role(COMMON_PLACE_SMS_SEND_TO_LIST_ID, "staff")) { ?>
                                                        <div class="form-group">
                                                            <label for="staff" class="col-md-3 input-sm">Staff</label>
                                                            <div class="col-md-6">  
                                                                <select name="staff[]" id="staff" class="form-control input-sm" multiple=""></select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="all_staff" class="input-sm">Check For All Staff</label> <input type="checkbox" name="all_staff" id="all_staff" value="all_staff" />
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div> 
                                        </div>										
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            <?php if (isset($sms_id) && !empty($sms_id)) { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Created By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?= (isset($created_by_name)) ? $created_by_name : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?= (isset($created_at)) ? $created_at : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?= (isset($updated_by_name)) ? $updated_by_name : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?= (isset($updated_at)) ? $updated_at : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Approved By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm approved_by" id="approved_by" value="<?= (isset($approved_by_name)) ? $approved_by_name : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Approved Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm approved_date_time" id="approved_date_time" value="<?= (isset($approved_at)) ? $approved_at : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
        $(document).ready(function () {
            initAjaxSelect2($("#sms_topic"), "<?= base_url('app/sms_topic_select2_source') ?>");
            <?php if (isset($sms_topic)) { ?>
                setSelect2Value($("#sms_topic"), "<?= base_url('app/set_sms_topic_select2_val_by_id/' . $sms_topic) ?>");
            <?php } ?>
            initAjaxSelect2($("#party_id"), "<?= base_url('app/party_select2_source') ?>");
            <?php if (isset($party_id)) { ?>
                setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id/' . $party_id) ?>");
            <?php } ?>
            initAjaxSelect2($("#supplier"), "<?= base_url('app/supplier_select2_source') ?>");
            <?php if (isset($supplier)) { ?>
                setSelect2Value($("#supplier"), "<?= base_url('app/set_supplier_select2_val_by_id/' . $supplier) ?>");
            <?php } ?>
            initAjaxSelect2($("#staff"), "<?= base_url('app/staff_select2_source') ?>");
            <?php if (isset($staff)) { ?>
                setSelect2Value($("#staff"), "<?= base_url('app/set_staff_select2_val_by_id/' . $staff) ?>");
            <?php } ?>
                
            $(document).on('change', '#sms_topic', function () {
                var sms_topic = $(this).val();
                $.ajax({
                    url: '<?php echo BASE_URL; ?>sms/get_message',
                    type: "POST",
                    data: {sms_topic: sms_topic},
                    dataType: 'json',
                    success: function (data) {
                        if (data != null || data != '') {
                            $('#message').val(data);
                        } else {
                            $('#message').val('');
                        }
                    }
                });
            });
            
            $(document).on('submit', '#save_send_sms', function () {
        
            if($.trim($("#sms_topic").val()) == ''){
                show_notify('Please Select Sms Topic.',false);
                return false;
            }
            if($.trim($("#message").val()) == ''){
                show_notify('Please Type Message.',false);
                return false;
            }
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
			$.ajax({
				url: "<?=base_url('sms/save_send_sms') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('sms/add') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });

            
        });
</script>