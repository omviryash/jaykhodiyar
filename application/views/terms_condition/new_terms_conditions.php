
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Terms And Conditions : <?php echo (isset($module_details)) ? $module_details->module_name : ''; ?>
            <small class="text-primary text-bold"></small>
            <button type="button" class="btn btn-primary btn-sm pull-right btn-save-tandc" style="margin: 5px;">
                Save
            </button>
            <a href="<?=base_url('terms_condition/terms_conditions_table') ?>" style="margin: 5px;" class="btn btn-primary btn-sm pull-right">Terms And Conditions Module Names</a>
            <?php /*if($this->applib->have_access_role(MASTER_ITEM_MENU_ID,"add")){ ?>
            <a href="<?=base_url('tandc/add_tandc_master') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Terms And Conditions</a>
            <?php }*/ ?>
        </h1>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <?php foreach ($related_modules as $key => $related_module_row) { ?>
                            <li <?php if($related_module_row->id == $module_id) { echo 'class="active"'; } ?> >
                                <a href="#tab_<?php echo $related_module_row->id;?>" data-toggle="tab" id="tabs_<?php echo $related_module_row->id;?>" data-modules="<?php echo $related_module_row->id;?>" class="btn_modules" align="center">
                                    <?php 
                                        $related_module_row->label = str_replace('>>', '<br>', $related_module_row->label);
                                        echo $related_module_row->label;
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form method="post" action="<?=base_url();?>terms_condition/terms_conditions" <?php echo (isset($module_id) && !empty($module_id)) ? 'style="display: none;"' : ''; ?> >
                                <select name="modules" id="modules" class="form-control select2" onchange="this.form.submit()">
                                    <option>--Select--</option>
                                    <?php foreach($modules as $module){ ?>
                                    <option value="<?=$module->id;?>" <?=(isset($module_id) ? ($module->id == $module_id ? 'selected' : '') :  '');?>><?=$module->module_name;?> >> <?=$module->label;?></option>
                                    <?php } ?>
                                </select>
                            </form>
                            <form method="POST" action="<?=base_url('terms_condition/save_tandc_master') ?>" id="form_terms_and_conditions" enctype="multipart/form-data">
                                <input type="hidden" class="form-control input-sm" id="id" name="module_id" value="<?=isset($module_id)?$module_id:''?>">
                                <div class="row section_terms_and_condition">
                                    <?php
                                    if(isset($terms_and_condition_detail_pages) && count($terms_and_condition_detail_pages) > 0 ) {
                                        foreach($terms_and_condition_detail_pages as $page_key=>$page_detail) {
                                            $page_key = $page_key + 1;
                                    ?>
                                    <div class="col-md-12 section_tandc_document" data-section_no="<?=$page_key?>" data-document_type="terms_and_condition">
                                        <div class="col-md-6">
                                            <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <?php
                                        if ($page_key != 1) {
                                            echo '<button type="button" class="btn btn-danger remove_tandc_document" data-document_type="terms_and_condition">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                        }
                                            ?>
                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="tandc_document"
                                                  id="terms_and_condition_tandc_document_<?= $page_key ?>"
                                                  style="visibility: hidden; display: none;" data-document_type="terms_and_condition"><?= $page_detail ?></textarea>
                                        <br/>
                                    </div>
                                    <?php
                                        }
                                    } else {
                                    ?>
                                    <div class="col-md-12 section_tandc_document" data-section_no="1" data-document_type="terms_and_condition">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="tandc_document" id="terms_and_condition_tandc_document_1"  data-document_type="terms_and_condition" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="col-md-12 text-right qtn-section-add-btn">
                                        <button type="button" class="btn btn-primary add_tandc_document" data-document_type="terms_and_condition">Add more</button>
                                    </div>
                                </div>
                                <div class="col-md-12 section_tandc_document hidden" data-section_no="" data-document_type="">
                                    <div class="col-md-6">
                                        <h4>Detail page : <span class="cnt-detail-page">2</span></h4>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="btn btn-danger remove_tandc_document" data-document_type="">Remove Page <span class="cnt-detail-page">2</span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <textarea class="" id="" style="visibility: hidden; display: none;"  data-document_type=""></textarea>
                                    <br/>
                                </div>
                                <input type="submit" class="hidden btn-submit-lead-form" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    var cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var id_cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var add_tandc_bom = 0;
    var ITEM_ID = '<?=isset($id)?$id:0?>';
    $(document).ready(function(){
        $('.select2').select2();

        $(document).on("click",".btn_modules",function(){
            var modules = $(this).attr('data-modules');
            $("#modules").val(modules).change();
        });

        $('.tandc_document').each(function(){
            id_cnt_qtn_section++;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g );
            CKEDITOR.replace($(this).attr('id'),{
                filebrowserBrowseUrl: '<?=base_url();?>mail-system3/attachment_browse_from_server',
                filebrowserUploadUrl: '<?=base_url();?>mail-system3/upload_attachment_to_server',
                extraPlugins: 'lineheight',
                removePlugins:''
                //removePlugins:'forms,iframe,preview,a11yhelp,about,link,pagebreak,flash,save,print,newpage'
            });
        });

        $(document).on('click','.remove_tandc_document',function(){
            var document_type = $(this).data('document_type');
            $(this).closest('.section_tandc_document').fadeOut("slow",function(){
                $(this).remove();
                cnt_qtn_section = 0;
                $('.section_tandc_document[data-document_type="'+document_type+'"]:not(.hidden)').each(function(){
                    cnt_qtn_section++;
                    $(this).find('.cnt-detail-page').each(function(){
                        $(this).text(cnt_qtn_section);
                    });
                });
            });
        });

        $(document).on('click','.add_tandc_document',function(){
            id_cnt_qtn_section++;
            var document_type = $(this).data('document_type');
            var section_tandc_document = $('.section_tandc_document.hidden').clone();

            cnt_qtn_section = $('.section_tandc_document[data-document_type="'+document_type+'"]').length;
            cnt_qtn_section = cnt_qtn_section + 1;
            section_tandc_document.removeClass('hidden');
            section_tandc_document.attr('data-section_no',cnt_qtn_section);
            section_tandc_document.attr('data-document_type',document_type);
            section_tandc_document.find('.remove_tandc_document').attr('data-document_type',document_type);
            section_tandc_document.find('.cnt-detail-page').each(function(){
                $(this).text(cnt_qtn_section);
            });
            section_tandc_document.find('textarea').attr('id',document_type+'_tandc_document_'+id_cnt_qtn_section);
            section_tandc_document.find('textarea').attr('data-document_type',document_type);
            section_tandc_document.find('textarea').addClass('tandc_document');
            section_tandc_document.insertBefore('.section_'+document_type+' > .qtn-section-add-btn');
            CKEDITOR.replace(section_tandc_document.find('textarea').attr('id'),{
                filebrowserBrowseUrl: '<?=base_url();?>mail-system3/attachment_browse_from_server',
                filebrowserUploadUrl: '<?=base_url();?>mail-system3/upload_attachment_to_server',
                extraPlugins: 'lineheight',
                removePlugins:''
                /*removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'*/
            });
        });
        
        $(document).on('click','.btn-save-tandc',function(){
            $("#form_terms_and_conditions").submit();
        });
        
        $("#form_terms_and_conditions").on("submit",function(e){
            e.preventDefault();
            if($('#modules').val() == "" || $('#modules').val() == null){
                show_notify('Choose Module!', false);
                return false;
            }

            var form_data = new FormData(this);
            $('textarea.tandc_document').each(function(){
                var document_type = $(this).data('document_type');
                form_data.append('tandc_documents['+document_type+'][]',CKEDITOR.instances[$(this).attr('id')].getData());
            });
            $("#ajax-loader").show();
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                dataType:'json',
                success: function(data){
                    if(data.success == true){
                        console.log(data);
                        MODULE_ID = data.module_id;
                        $("#id").val(MODULE_ID);
                        show_notify(data.message,true);
                        $("#ajax-loader").hide();
                    }
                }
            });
        });
        
    });
</script>
