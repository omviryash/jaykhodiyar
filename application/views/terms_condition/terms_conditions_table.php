
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Terms And Conditions Module Names
            <small class="text-primary text-bold"></small>
        </h1>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tc_datatable" class="table custom-table" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Module Names</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($terms_and_conditions_modules as $module){ ?>
                                        <tr>
                                            <td><a href="javascript:void(0)" data-modules="<?=$module->id;?>" class="btn_modules"><?=$module->module_name;?></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
</div>
<form id="form_modules" method="post" action="<?=base_url();?>terms_condition/terms_conditions">
    <input type="hidden" name="modules" id="modules">
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on("click",".btn_modules",function(){
            var modules = $(this).attr('data-modules');
            $("#modules").val(modules);
            $("#form_modules").submit();
        });

        $('#tc_datatable').DataTable({
            "ordering": false,
            "searching": true,
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
    });
</script>
