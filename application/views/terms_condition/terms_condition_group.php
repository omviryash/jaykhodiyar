<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_TERMS_CONDITION_GROUP_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_TERMS_CONDITION_GROUP_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_TERMS_CONDITION_GROUP_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Terms & Condition Group</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Terms & Condition Group
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table terms-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Code</th>
						                                            <th>Desctiption</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                        <?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('terms_condition/terms_condition_group/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('terms_condition/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
						                                            </td>
						                                            <td><?=$row->code ?></td>
						                                            <td><?=$row->description ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
                                                        <?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?> Terms & Condition Group
													</div>
													<div style="margin:20px">	
														<form method="POST" 
															<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('terms_condition/update_terms_condition_group') ?>" 
															<?php } else { ?>
															action="<?=base_url('terms_condition/add_terms_condition_group') ?>" 
															<?php } ?> id="form_terms">
															<?php if(isset($id) && !empty($id)){ ?>
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
															<?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-2 input-sm">Code<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="code" name="code" value="<?php echo $code; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-2 input-sm">Description<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="description" name="description" value="<?php echo $description; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															
                                                            <?php if(isset($id) && !empty($id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Terms & Condition Group</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Terms & Condition Group</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

        $("#form_terms").on("submit",function(e){
			e.preventDefault();
			if($("#code").val() == ""){
				show_notify('Fill value Code.', false);
				return false;
			}
			if($("#description").val() == ""){
				show_notify('Fill value Description.', false);
				return false;
			}
			var url = '<?php echo base_url('terms_condition/delete/') ?>';
			var value = $("#code").val();
			var description = $("#description").val();
			if(value != '' && description != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.code+'</td>';
		                TableRow += '<td>'+data.description+'</td>';
		                TableRow += '</tr>';
		                $('.terms-table > tbody > tr:last ').after(TableRow);
		                $("#form_terms")[0].reset();
		                show_notify('Saved Successfully!',true);*/
		                window.location.href = "<?php echo base_url('terms_condition/terms_condition_group') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=terms_condition_group',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('terms_condition/terms_condition_group') ?>";
					}
				});
			}
		});
		
    });
</script>
