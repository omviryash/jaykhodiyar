<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_DEFAULT_TERMS_CONDITION_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_DEFAULT_TERMS_CONDITION_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_DEFAULT_TERMS_CONDITION_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Default Terms & Condition</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Default Terms & Condition
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table terms-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Template Name</th>
						                                            <th>Group Name</th>
						                                            <th>Terms & Condition</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                        <?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('terms_condition/default_terms_condition/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('terms_condition/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
						                                            </td>
						                                            <td><?=$row->template_name ?></td>
						                                            <td><?=$row->group_name ?></td>
						                                            <td><?=$row->terms_condition ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
                                                        <?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?> Default Terms & Condition
													</div>
													<div style="margin:20px">	
														<form method="POST"
															<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('terms_condition/update_default_terms_condition') ?>" 
															<?php } else { ?>
															action="<?=base_url('terms_condition/add_default_terms_condition') ?>" 
															<?php } ?> id="form_terms">
															<?php if(isset($id) && !empty($id)){ ?>
			                                                        <input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
			                                                <?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-6 input-sm">Default Terms & Condition For<span class="required-sign">*</span></label>
																<div class="col-sm-6">
			                                                        <?php
			                                                        	$inquiry = $quotation = $sales = "";
			                                                        	if(isset($id) && !empty($id)){
			                                                        		if ($template_name == 'Enquiry') {
			                                                        			$inquiry = 'selected';
			                                                        		}
			                                                        		if ($template_name == 'Quotation') {
			                                                        			$quotation = 'selected';
			                                                        		}
			                                                        		if ($template_name == 'Sales') {
			                                                        			$sales = 'selected';
			                                                        		}
			                                                        	}
			                                                        ?>
                                                                    <select class="form-control input-sm select2" name="template_name" id="template_name" <?php echo $btn_disable;?>>
																		<option value="">Select One</option>
																		<option <?php echo $inquiry; ?>>Enquiry</option>
																		<option <?php echo $quotation; ?>>Quotation</option>
																		<option <?php echo $sales; ?>>Sales</option>				                                                        
			                                                        </select>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-6 input-sm">Group<span class="required-sign">*</span></label>
			                                                    <div class="col-sm-4">
                                                                    <input type="text" class="form-control input-sm" id="group_name" name="group_name" readonly value="<?php echo $group_name; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
			                                                    <div class="col-sm-1" style="padding: 0;">
																	<a class="openpopup" style="background: #dfdfdf; padding: 0px 5px;"><i class="fa fa-binoculars"></i></a>			                                                    
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-6 input-sm">Terms & Condition<span class="required-sign">*</span></label>
																<div class="col-sm-6">
                                                                    <textarea class="form-control" rows="1" name="terms_condition" id="terms_condition" <?php echo $btn_disable;?>><?php echo $terms_condition; ?></textarea>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group" style="margin:7px !important;"></div>
															<?php if(isset($id) && !empty($id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Default Terms & Condition</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Default Terms & Condition</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
		<div class="modal fade" id="myModal">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Terms & Condition Group</h4>
		        </div>
		        <div class="modal-body">
		          	<div class="row">
		          		<div class="col-sm-12">
							<table id="example2" class="table custom-table">
								<thead>
									<tr>
										<th>Code</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody>
									<?php 
                                    if(!empty($modal_results)) {
                                    	foreach ($modal_results as $row) {
                                    ?>
                                    <tr class="modalDataSelect">
                                        <td><?=$row->code ?></td>
                                        <td class="select"><?=$row->description ?></td>
                                    </tr>
                                        <?php 
                                    		} }
                                        ?>
								</tbody>
							</table>
		          		</div>
		          	</div>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		</div>
</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});
        $("#example2").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

        $("#form_terms").on("submit",function(e){
			e.preventDefault();
			if($("#template_name").val() == ""){
				show_notify('Fill value Default Terms & Condition For.', false);
				return false;
			}
			if($("#group_name").val() == ""){
				show_notify('Fill value Group.', false);
				return false;
			}
			if($("#terms_condition").val() == ""){
				show_notify('Fill value Terms & Conditions.', false);
				return false;
			}
			var url = '<?php echo base_url('terms_condition/delete/') ?>';
			var value = $("#template_name").val();
			var group_name = $("#group_name").val();
			var terms_condition = $("#terms_condition").val();
			if(value != '' && group_name != '' && terms_condition)
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.template_name+'</td>';
		                TableRow += '<td>'+data.group_name+'</td>';
		                TableRow += '<td>'+data.terms_condition+'</td>';
		                TableRow += '</tr>';
		                $('.terms-table > tbody > tr:last ').after(TableRow);
		                $("#form_terms")[0].reset();*/
		                window.location.href = "<?php echo base_url('terms_condition/default_terms_condition') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=default_terms_condition',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('terms_condition/default_terms_condition') ?>";
					}
				});
			}
		});

        $(".openpopup").on("click",function(){
			$("#myModal").modal("show");			
		});

		$(".modalDataSelect").on("click",function(){
			var td_data = $(this).closest("tr").find(".select").text();
			$("#group_name").val(td_data);
			$("#myModal").modal("hide");
		});
		
    });
</script>
