<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_TERMS_CONDITION_DETAIL_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_TERMS_CONDITION_DETAIL_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_TERMS_CONDITION_DETAIL_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Terms & Condition Detail</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Terms & Condition Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table terms-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Code</th>
						                                            <th>Group</th>
						                                            <th>Detail</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                        <?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('terms_condition/terms_condition_detail/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> 
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('terms_condition/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
                                                                    </td>
						                                            <td><?=$row->code ?></td>
						                                            <td><?=$row->group_name2 ?></td>
						                                            <td><?=$row->description ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
                                                        <?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?> Terms & Condition Detail
													</div>
													<div style="margin:10px">	
														<form method="POST" 
															<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('terms_condition/update_terms_condition_detail') ?>" 
															<?php } else { ?>
															action="<?=base_url('terms_condition/add_terms_condition_detail') ?>" 
															<?php } ?> id="form_terms">
															<?php if(isset($id) && !empty($id)){ ?>
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
															<?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">Code<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="code" name="code" value="<?php echo $code; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">Group Code/Name<span class="required-sign">*</span></label>
																<div class="col-sm-3">
                                                                    <input type="text" class="form-control input-sm" id="group_name1" name="group_name1" readonly value="<?php echo $group_name1; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
			                                                    <div class="col-sm-4">
                                                                    <input type="text" class="form-control input-sm" id="group_name2" name="group_name2" readonly value="<?php echo $group_name2; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
			                                                    <div class="col-sm-1" style="padding: 0;">
																	<a class="openpopup" style="background: #dfdfdf; padding: 0px 5px;"><i class="fa fa-binoculars"></i></a>			                                                    
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">Description<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <textarea class="form-control" rows="1" name="description" id="description" <?php echo $btn_disable;?>><?php echo $description; ?></textarea>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group" style="margin:7px !important;"></div>
															
                                                            <?php if(isset($id) && !empty($id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Terms & Condition Detail</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Terms & Condition Detail</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
		
		<div class="modal fade" id="myModal">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Terms & Condition Group</h4>
		        </div>
		        <div class="modal-body">
		          	<div class="row">
		          		<div class="col-sm-12">
							<table id="example2" class="table custom-table">
								<thead>
									<tr>
										<th>Code</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody>
								<?php 
									if(!empty($modal_results)) {
										foreach ($modal_results as $modal_result) {
								?>
										<tr class="modalDataSelect">
											<td class="select_1"><?php echo $modal_result->code; ?></td>
											<td class="select_2"><?php echo $modal_result->description; ?></td>
										</tr>
								<?php
										}
									}
								?>
								</tbody>
							</table>
		          		</div>
		          	</div>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		</div>
</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});
        $("#example2").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

        $("#form_terms").on("submit",function(e){
			e.preventDefault();
			if($("#code").val() == ""){
				show_notify('Fill value Code.', false);
				return false;
			}
			if($("#group_name1").val() == ""){
				show_notify('Fill value Group.', false);
				return false;
			}
			if($("#description").val() == ""){
				show_notify('Fill value Description.', false);
				return false;
			}
			var url = '<?php echo base_url('terms_condition/delete/') ?>';
			var value = $("#code").val();
			var group_name1 = $("#group_name1").val();
			var group_name2 = $("#group_name2").val();
			var description = $("#description").val();
			if(value != '' && description != '' && group_name1 != '' && group_name2 != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.code+'</td>';
		                TableRow += '<td>'+data.group_name2+'</td>';
		                TableRow += '<td>'+data.description+'</td>';
		                TableRow += '</tr>';
		                $('.terms-table > tbody > tr:last ').after(TableRow);
		                $("#form_terms")[0].reset();*/
		                window.location.href = "<?php echo base_url('terms_condition/terms_condition_detail') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=terms_condition_detail',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('terms_condition/terms_condition_detail') ?>";
					}
				});
			}
		});

        $(".openpopup").on("click",function(){
			$("#myModal").modal("show");			
		});

		$(".modalDataSelect").on("click",function(){
			var td_data1 = $(this).closest("tr").find(".select_1").text();
			var td_data2 = $(this).closest("tr").find(".select_2").text();
			$("#group_name1").val(td_data1);
			$("#group_name2").val(td_data2);
			$("#myModal").modal("hide");
		});
		
    });
</script>
