<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="form-group">
            <?php if($this->session->flashdata('success') == true){?>
                <div class="col-sm-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
                    </div>
                </div>
            <?php }?>
        </div>
        <h1>
            <small class="text-primary text-bold">Company's Information</small>

            <?php if($this->applib->have_access_role(COMPANY_DETAIL_MENU_ID,"edit")) { ?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn-submit">
                        <?php if(isset($id) && !empty($id)){ ?>Save
                        <?php } else { ?>Submit
                        <?php } ?></button>
             <?php } ?>

        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST"
                                    <?php if(isset($id) && !empty($id)){ ?>
                                    action="<?=base_url('master/update_company') ?>" 
                                    <?php } else { ?>
                                    action="<?=base_url('master/save_company') ?>" 
                                    <?php } ?> id="add_company_form">
                                    <?php if(isset($id) && !empty($id)){ ?>
										<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
									<?php } ?>
                                    <div class="row">                                           
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="code" class="col-sm-3 input-sm">Company Code</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="code" name="code" value="<?php echo $code; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-3 input-sm">Company Name</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo $name; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="ceo_name" class="col-sm-3 input-sm">Company CEO</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="ceo_name" name="ceo_name" value="<?php echo $ceo_name; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="m_d_name" class="col-sm-3 input-sm">Company M.D</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="m_d_name" name="m_d_name" value="<?php echo $m_d_name; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="address" class="col-sm-3 input-sm">Company Address</label>
                                                    <div class="col-sm-7">
                                                        <textarea class="form-control" style="margin-bottom: 3px;" rows="3" name="address" id="address"><?php echo $address; ?></textarea>
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="city" class="col-sm-3 input-sm">City.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="city" name="city" value="<?php echo $city; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                                
                                                <div class="form-group">
                                                    <label for="pincode" class="col-sm-3 input-sm">Pincode</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="pincode" name="pincode" value="<?php echo $pincode; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                
                                                <div class="form-group">
                                                    <label for="state" class="col-sm-3 input-sm">State</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="state" name="state" value="<?php echo $state; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="country" class="col-sm-3 input-sm">Country.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="country" name="country" value="<?php echo $country; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="contact_no" class="col-sm-3 input-sm">Tel No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="contact_no" name="contact_no" value="<?php echo $contact_no; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="cell_no" class="col-sm-3 input-sm">Contact No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="cell_no" name="cell_no" value="<?php echo $cell_no; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="pan_no" class="col-sm-3 input-sm">Pan. No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="pan_no" name="pan_no" value="<?php echo $pan_no; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="email_id" class="col-sm-3 input-sm">Email-Id</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="email_id" name="email_id" value="<?php echo $email_id; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="authorized_signatory_name" class="col-sm-3 input-sm">Authorized Signatory Name</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="authorized_signatory_name" name="authorized_signatory_name" value="<?php echo $authorized_signatory_name; ?>">
                                                        <p class="input-sm">(e.g. Sales Order Print)</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="tds_per" class="col-sm-3 input-sm"> TDS % </label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm num_only" id="tds_per" name="tds_per" value="<?php echo $tds_per; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="c_pf_per" class="col-sm-3 input-sm"> PF % </label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm num_only" id="c_pf_per" name="c_pf_per" value="<?php echo $c_pf_per; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="c_esi_per" class="col-sm-3 input-sm"> ESI % </label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm num_only" id="c_esi_per" name="c_esi_per" value="<?php echo $c_esi_per; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                
                                                <div class="form-group">
                                                    <label for="margin_left" class="col-sm-5 input-sm">Print Margin Left</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="margin_left" name="margin_left" value="<?php echo $margin_left; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="margin_right" class="col-sm-5 input-sm">Print Margin Right</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="margin_right" name="margin_right" value="<?php echo $margin_right; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="margin_top" class="col-sm-5 input-sm">Print Margin Top</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="margin_top" name="margin_top" value="<?php echo $margin_top; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="margin_bottom" class="col-sm-5 input-sm">Print Margin Bottom</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="margin_bottom" name="margin_bottom" value="<?php echo $margin_bottom; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="invoice_terms" class="col-sm-5 input-sm">Invoice Terms</label>
                                                    <div class="col-sm-7">
                                                        <textarea class="form-control" id="invoice_terms" name="invoice_terms" style="margin-bottom: 3px;" rows="3"><?php echo $invoice_terms; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                
                                                <div class="form-group">
                                                    <label for="pf_no" class="col-sm-5 input-sm">PF No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="pf_no" name="pf_no" value="<?php echo $pf_no; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                
                                                <div class="form-group">
                                                    <label for="esic_no" class="col-sm-5 input-sm">ECC No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="esic_no" name="esic_no" value="<?php echo $esic_no; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                                
                                                <div class="form-group">
                                                    <label for="iec_no" class="col-sm-5 input-sm">IEC No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="iec_no" name="iec_no" value="<?php echo $iec_no; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="tin_no" class="col-sm-5 input-sm">Tin No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="tin_no" name="tin_no" value="<?php echo $tin_no; ?>">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="cst_no" class="col-sm-5 input-sm">CST No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="cst_no" name="cst_no" value="<?php echo $cst_no; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="gst_no" class="col-sm-5 input-sm">GST No.</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="gst_no" name="gst_no" value="<?php echo $gst_no; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="end_use_code" class="col-sm-5 input-sm">End Use Code</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="end_use_code" name="end_use_code" value="<?php echo $end_use_code; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="lut" class="col-sm-5 input-sm">LUT</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" id="lut" name="lut" value="<?php echo $lut; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                        <label for="authorized_signatory_image" class="col-sm-5 input-sm">Authorized Signatory Image</label>
                                                        <div class="col-sm-7">
                                                                <input type="file" name="authorized_signatory_image" class="form-control input-sm" src="<?= isset($authorized_signatory_image) ? image_url('staff/signature/'.$authorized_signatory_image) : '' ;?>">
                                                                <p class="input-sm">(e.g. Sales Order Print)</p>
                                                                <?php if(isset($authorized_signatory_image)){ ?>
                                                                <input type="hidden" name="authorized_signatory_image_old" class="form-control input-sm" value="<?=$authorized_signatory_image;?>">
                                                                <?php } ?>
                                                                <?php if(isset($authorized_signatory_image) && !empty($authorized_signatory_image)){ ?>
                                                                    <img height="100px" width="100px" src="<?= isset($authorized_signatory_image) ? image_url('staff/signature/'.$authorized_signatory_image) : '';?>">
                                                                <?php } ?>
                                                        </div>
                                                </div>
<!--                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 input-sm">Bank Info For Domestic</label>
                                                    <div class="col-sm-7">
                                                       <textarea class="form-control" style="margin-bottom: 3px;" rows="4" name="bank_domestic" id="bifdomestic"><?php //echo $bank_domestic; ?></textarea>
                                                    </div>
                                                </div>
                                                 <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 input-sm">Bank Info For Export</label>
                                                    <div class="col-sm-7">
                                                       <textarea class="form-control" style="margin-bottom: 3px;" rows="4" name="bank_export" id="bifexport"><?php //echo $bank_export; ?></textarea>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $(document).on("click",'.btn-submit',function(){
            $("#add_company_form").submit();
        });
    	$("#add_company_form").on("submit",function(e){
			e.preventDefault();
			var name = $("#name").val();
			var address = $("#address").val();
			var contact_no = $("#contact_no").val();
			var pan_no = $("#pan_no").val();
			var tan_no = $("#tan_no").val();
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                dataType:'json',
                success: function(data){
                    window.location.href = "<?php echo base_url('master/company_detail') ?>";
                }
            });
			
		});
		
    });
</script>
