<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url('/resource/plugins/colorpicker/bootstrap-colorpicker.min.css'); ?>">
<div class="content-wrapper">
    <form class="form-horizontal" action="<?php echo base_url('master/update_setting') ?>" method="post" id="update_setting" enctype="multipart/form-data">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="form-group">
                <?php if ($this->session->flashdata('success') == true) { ?>
                    <div class="col-sm-12">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <h1>
                <small class="text-primary text-bold">Settings</small>
                <?php if ($this->applib->have_access_role(COMPANY_DETAIL_MENU_ID, "edit")) { ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right btn-submit">Update</button>
                <?php } ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="row">
                <div style="margin: 15px;">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="sidebar_logo" class="col-sm-4 input-sm"><?php echo $setting_data[0]->settings_label; ?></label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="sidebar_logo" class="form-control input-sm" src="<?php echo!empty($setting_data[0]->settings_value) ? base_url() . image_dir($setting_data[0]->settings_value) : ''; ?>">
                                                    <?php if (isset($setting_data[0]->settings_value) && !empty($setting_data[0]->settings_value)) { ?>
                                                        <input type="hidden" name="sidebar_logo_old" class="form-control input-sm" value="<?php echo $setting_data[0]->settings_value; ?>">
                                                        <img width="80px" src="<?= isset($setting_data[0]->settings_value) ? base_url() . image_dir($setting_data[0]->settings_value) : ''; ?>">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="theme_color" class="col-sm-4 input-sm"><?php echo $setting_data[1]->settings_label; ?></label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="theme_color" class="form-control my-colorpicker1 colorpicker-element" autocomplete="off" value="<?php echo $setting_data[1]->settings_value; ?>">
                                                    <small>Default code is : #1f7eba</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Bootstrap Color Picker -->
<script src="<?php echo base_url('/resource/plugins/colorpicker/bootstrap-colorpicker.min.js'); ?>"></script>
<script>
    $(document).ready(function () {
        //Colorpicker
        $('.my-colorpicker1').colorpicker();
    });
</script>
