<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Company's Information</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Company Detail
														<a href="<?=base_url('master/add_company') ?>" class="btn btn-info btn-xs pull-right">Add Company</a>
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Company Name</th>
						                                            <th>Address</th>
						                                            <th>Contact Number</th>
						                                            <th>PAN No</th>
						                                            <th>TAN No</th>
						                                            <th>PF No</th>
						                                            <th>ESIC No</th>
						                                            <th>Service Tax No</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                    	<?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('master/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a> | <a href="<?php echo base_url('master/add_company/'.$row->id); ?>" class=" btn-primary btn-xs"><i class="fa fa-edit"></i></a></td>
						                                            <td><?=$row->name ?></td>
						                                            <td><?=$row->address ?></td>
						                                            <td><?=$row->contact_no ?></td>
						                                            <td><?=$row->pan_no ?></td>
						                                            <td><?=$row->tan_no ?></td>
						                                            <td><?=$row->pf_no ?></td>
						                                            <td><?=$row->esic_no ?></td>
						                                            <td><?=$row->service_tax_no ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable();

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=company',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('master/company_detail') ?>";
					}
				});
			}
		});
		
    });
</script>
