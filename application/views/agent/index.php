<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php
            $this->load->view('shared/success_false_notify');
            ?>
            <small class="text-primary text-bold" style="margin-left: 5px;">Agent</small>
            <a href="<?= BASE_URL ?>master/agent/" class="btn btn-info btn-xs pull-right" style="margin-right: 10px;">Add Agent</a>
        </h1>
    </section>
    <div class="clearfix">
        <div class="row" style="margin: 8px;">
            <?php
            $role_add = $this->applib->have_access_role(MASTER_AGENT_MENU_ID, "add");
            $role_edit = $this->applib->have_access_role(MASTER_AGENT_MENU_ID, "edit");
            $role_view = $this->applib->have_access_role(MASTER_AGENT_MENU_ID, "view");
            $role_delete = $this->applib->have_access_role(MASTER_AGENT_MENU_ID, "delete");
            ?>
            <?php if ($role_add || $role_edit) { ?>
                <div class="col-md-12">
                    <div class="box box-primary">

                        <?php if (!$role_add && $role_edit) { ?>
                            <div class="box-header with-border" color="red">
                                <p><font size="3" color="red">You have no Permission to Add New Agent.</font></p>
                            </div>

                        <?php } ?>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form method="POST"
                                    <?php if (isset($id) && !empty($id)) { ?>
                                              action="<?= base_url('master/update_agent') ?>" 
                                          <?php } else { ?>
                                              action="<?= base_url('master/add_agent') ?>" 
                                          <?php } ?> id="agent-form">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Add Agent </legend>
                                            <div class="row" style="margin-top: 10px;">

                                                <?php if (isset($id) && !empty($id)) { ?>
                                                    <input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
                                                <?php } ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Agent Name<span class="required-sign">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control input-sm" id="agent_name" name="agent_name" value="<?php echo $agent_name; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Address</label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" id="address" name="address"><?php echo $address; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">State</label>
                                                        <div class="col-sm-8">
                                                            <select name="state" id="state" class="form-control input-sm"><?php echo $state; ?></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Phone No.</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control input-sm num_only" id="phone_no" name="phone_no" value="<?php echo $phone_no; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Note</label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" id="note" name="note"><?php echo $note; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">    
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Company Name</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control input-sm" id="company_name" name="company_name" value="<?php echo $company_name; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">City</label>
                                                        <div class="col-sm-8">
                                                            <select name="city" id="city" class="form-control input-sm"><?php echo $city; ?></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Country</label>
                                                        <div class="col-sm-8">
                                                            <select name="country" id="country" class="form-control input-sm"><?php echo $country; ?></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Email</label>
                                                        <div class="col-sm-8">
                                                            <input type="email" class="form-control input-sm" id="email" name="email" value="<?php echo $email; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Agent Department</label>
                                                        <div class="col-sm-8">
                                                            <?php
                                                            $accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
                                                            $accessDomestic = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
                                                            ?>
                                                            <?php if ($accessExport == 1 && $accessDomestic == 1): ?>
                                                                <select name="party_type_1" id="party_type_1" class="form-control input-sm">
                                                                </select>
                                                            <?php elseif ($accessExport == 1): ?>

                                                                <input type="text" class="form-control input-sm" id="party_type_1_display" placeholder="" value="<?php
                                                                if (isset($party_type_1)) {
                                                                    echo $party_type_1;
                                                                } else {
                                                                    echo "Customer-Export";
                                                                }
                                                                ?>" readonly="readonly">
                                                                <input type="hidden" name="party_type_1" id="party_type_1" value="5">
                                                            <?php elseif ($accessDomestic == 1): ?>
                                                                <input type="text" class="form-control input-sm" id="party_type_1_display" placeholder="" value="<?php
                                                                if (isset($party_type_1)) {
                                                                    echo $party_type_1;
                                                                } else {
                                                                    echo "Customer-Domestic";
                                                                }
                                                                ?>" readonly="readonly">
                                                                <input type="hidden" name="party_type_1" id="party_type_1" value="6">
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Fix Commission</label>
                                                        <div class="col-sm-8">
                                                            <input type="number" class="form-control input-sm" id="fix_commission" name="fix_commission" value="<?php echo $fix_commission; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-4 input-sm">Commission In %<span class="required-sign">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="number" class="form-control input-sm" id="commission" name="commission" value="<?php echo $commission; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <?php
                                                    if (isset($id) && !empty($id)) {
                                                        if ($role_edit) {
                                                            ?>
                                                            <button type="submit" class="btn btn-info  btn-xs pull-right" style="margin-right: 10px; margin-top: 10px;">Update Agent</button>
                                                            <?php
                                                        }
                                                    } else {
                                                        if ($role_add) {
                                                            ?>
                                                            <button type="submit" class="btn btn-info btn-xs pull-right" style="margin-right: 10px; margin-top: 10px;">Save Agent</button>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <div class="box-footer">

                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($role_view) { ?>
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Agents</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="example1" class="table custom-table agent-table">
                                            <thead>
                                                <tr>
                                                    <th width="50">Action</th>
                                                    <th>Agent Name</th>
                                                    <th>Company Name</th>
                                                    <th>Address</th>
                                                    <th>City</th>
                                                    <th>State</th>
                                                    <th>Country</th>
                                                    <th>Phone No.</th>
                                                    <th>Email</th>
                                                    <th>Note</th>
                                                    <th>Agent Department</th>
                                                    <th>Fix Commission</th>
                                                    <th>Commission In %</th>
                                                    <th>Created By</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">

                            </div>
                        </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var table;
    $(document).ready(function () {
        initAjaxSelect2($("#city"), "<?= base_url('app/city_select2_source') ?>");
<?php if (isset($city) && !empty($city)) { ?>
            setSelect2Value($("#city"), "<?= base_url('app/set_city_select2_val_by_id/' . $city) ?>");
<?php } ?>
        initAjaxSelect2($("#state"), "<?= base_url('app/state_select2_source') ?>");
<?php if (isset($state) && !empty($state)) { ?>
            setSelect2Value($("#state"), "<?= base_url('app/set_state_select2_val_by_id/' . $state) ?>");
<?php } ?>
        initAjaxSelect2($("#country"), "<?= base_url('app/country_select2_source') ?>");
<?php if (isset($country) && !empty($country)) { ?>
            setSelect2Value($("#country"), "<?= base_url('app/set_country_select2_val_by_id/' . $country) ?>");
<?php } ?>

<?php if ($accessExport == 1 && $accessDomestic == 1): ?>
            initAjaxSelect2($("#party_type_1"), "<?= base_url('app/sales_select2_source') ?>");
            setSelect2Value($("#party_type_1"), "<?= base_url('app/set_sales_select2_val_by_id/' . $party_type_1) ?>");
<?php endif; ?>

        table = $('#example1').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo base_url('master/agent_datatable') ?>",
                "type": "POST"
            },
            "scrollY": '350px',
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columnDefs": [
                {"targets": 0, "orderable": false}
            ]
        });

        $("#agent-form").on("submit", function (e) {
            e.preventDefault();
<?php if (isset($id) && !empty($id)) { ?>
                var success_status = check_is_unique('agent', 'agent_name', $("#agent_name").val(), 'id', '<?= $id ?>');
<?php } else { ?>
                var success_status = check_is_unique('agent', 'agent_name', $("#agent_name").val());
<?php } ?>
            if (success_status == 0) {
                if ($('p.agent_name-unique-error').length > 0) {
                    $("p.agent_name-unique-error").text('Agent already exist!');
                } else {
                    $("#agent_name").after("<p class='text-danger agent_name-unique-error'>Agent already exist!</p>");
                }
                return false;
            } else {
                $("p.agent_name-unique-error").text(' ');
            }
            var agent_name = $("#agent_name").val();
            var commission = $("#commission").val();
            if (agent_name == '') {
                show_notify('Please Enter Agent Name', false);
                $("#agent_name").focus()
                return false;
            } else if (commission == '') {
                show_notify('Please Enter commission In %', false);
                $("#commission").focus()
                return false;
            }
            var url = '<?php echo base_url('party/delete/') ?>';
            var value = $("#commission").val();
            var name = $("#agent_name").val();
            if (value != '' && name != '')
            {
                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        /*url += '/' + data.id;
                         var TableRow = '<tr>';
                         TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
                         TableRow += '<td>'+data.agent_name+'</td>';
                         TableRow += '<td>'+data.commission+'</td>';
                         TableRow += '<td>'+data.created_by+'</td>';
                         TableRow += '</tr>';
                         $('.agent-table > tbody > tr:last ').after(TableRow);
                         $("#agent-form")[0].reset();
                         show_notify('Saved Successfully!',true);*/
                        window.location.href = "<?php echo base_url('master/agent') ?>";
                    }
                });
            }
        });

        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=agent',
                    success: function (data) {
                        tr.remove();
                        window.location.href = "<?php echo base_url('master/agent') ?>";
                    }
                });
            }
        });

    });
</script>
