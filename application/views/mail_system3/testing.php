<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		
		<div class="row">
			<div class="col-md-3">
				<h3>
					Mailbox
		
				</h3>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div class="has-feedback">
						<input type="text" class="form-control txt-search-all-mail" placeholder="Search Mail">
						<span class="glyphicon glyphicon-search form-control-feedback"></span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="content">
		<div class="row">
			<textarea id="compose-textarea" class="form-control" style="height: 300px" name="body"></textarea>
		</div>
	</section>
</div>
<script>
	$(function () {
        console.log(CKEDITOR);
		CKEDITOR.config.allowedContent = true;
		CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g ); 
		CKEDITOR.replace('compose-textarea',{
			removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
		});
	});
</script>