<html>
	<head>
		<title></title>
		<style>
			.text-right{
				text-align: right;
			}
			td.mail-subject {
				padding: 10px 0px;
				background-color: #d9d9d9;
				text-align: center;
				font-size: 25px;
			}
			table{
				width: 100%;
			}
		</style>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<td class="mail-subject"><?=$subject;?></td>
				</tr>
				<?php
					if($email == $username) {
						?>
						<tr>
							<td class="mail-username" colspan="2"><?= $email; ?></td>
						</tr>
						<?php
					}else{
						?>
						<tr>
							<td class="mail-username" colspan="2"><?=$username;?> <<?=$email;?>> </td>
						</tr>
						<?php
					}
				?>
				<?php
					if(count($cc) > 0) {
						echo "<tr><td>cc : ";
						foreach ($cc as $key=>$cc_row) {
							if($key != 0){
								echo ", ";
							}
							echo $cc_row;
						}
						echo "</td></tr>";
					}
				?>
				<?php
					if(count($bcc) > 0) {
						echo "<tr><td>bcc : ";
						foreach ($bcc as $key=>$bcc_row) {
							if($key != 0){
								echo ", ";
							}
							echo $bcc_row;
						}
						echo "</td></tr>";
					}
				?>
			<tr>
				<td><?=$body?></td>
			</tr>
			<tr>
				<td class="text-right"><?=$received_at?></td>
			</tr>
			</thead>
		</table>
	</body>
</html>