<style>
	td.mailbox-subject{
		white-space: nowrap;
		max-width: 620px !important;
		overflow: hidden;
	}
	td.mailbox-attachment{
		max-width: 30px !important;
	}
	.is_unread {
		font-weight: 700;
	}
	td.mailbox-subject:hover {
    	cursor: pointer;
	}
	iframe{
	    overflow:hidden;
	}
	div.DTS div.dataTables_scrollBody {
		background: none;
	}
</style>
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
		<div class="row">
			<div class="col-md-3">
				<h3>
					Mailbox
					<small> <span class="top-no-unread-mails"><?=isset($count_unseen_mails)?$count_unseen_mails:0?></span> new messages</small>
				</h3>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div class="has-feedback">
						<input type="text" class="form-control txt-search-all-mail" placeholder="Search Mail">
						<span class="glyphicon glyphicon-search form-control-feedback" style="padding-top: 4px;" ></span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="content">
		<div class="row">
			<?php

			echo "<div class='col-md-2 folder_list_area'>";
			if($this->uri->segment(2) == 'settings' || $this->uri->segment(2) == 'read-mail' || $this->uri->segment(2) == 'read_mail' || isset($action)) {
				/*$this->load->view('mail_system3/folder_list',array('current_folder'=>$current_folder));*/
				$this->load->view('mail_system3/folder_list_ajax', array('current_folder' => $current_folder));
			} else {
				$this->load->view('mail_system3/folder_list_ajax', array('current_folder' => $current_folder));
			}
			echo "</div>";
			echo "<div class='col-md-3 mail_list_area'>";
			$this->load->view('mail_system3/mail_list',array('current_folder'=>$current_folder));
			echo "</div>";

			echo "<div class='col-md-7 mail_system_detail_area'>";
			if($this->uri->segment(2) == 'read-mail' || $this->uri->segment(2) == 'read_mail') {
				$this->load->view('mail_system3/read_mail', array('mail_detail' => $mail_detail,'mail_id'=>$mail_id,'current_folder'=>$current_folder));
			}elseif(isset($action) && $action == 'compose'){
				if(isset($document_url)){
					$data['settings'] = $settings;
					$data['action'] = $action;
					$data['document_url'] = $document_url;
					$data['subject'] = $subject;
					$data['to_address'] = $to_address;
					$this->load->view('mail_system3/compose_mail',$data);
				}else{
					$this->load->view('mail_system3/compose_mail',array('action'=>'compose','settings' => $settings));
				}
			}elseif(isset($action) && $action == 'drafts'){
				$this->load->view('mail_system3/compose_mail',array('action'=>'drafts','settings' => $settings,'mail_detail' => $mail_detail));
			}elseif(isset($action) && $action == 'forward'){
				$this->load->view('mail_system3/compose_mail',array('action'=>'forward','settings' => $settings,'mail_detail' => $mail_detail));
			}elseif(isset($action) && $action == 'reply'){
				$this->load->view('mail_system3/compose_mail',array('action'=>'reply','settings' => $settings,'mail_detail' => $mail_detail));
			} elseif($this->uri->segment(2) == 'settings'){
				$this->load->view('mail_system3/settings',array('settings' => $settings));
			}
			echo "</div>";
			?>
		</div>
	</section>
</div>
<div id="modal-create-folder" class="modal fade" role="dialog">
	<div class="modal-dialog" style="max-width: 480px">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-create-folder-header">Create Folder</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="<?=base_url()?>mail-system3/create-folder/" id="frm-create-folder" class="form-horizontal frm-create-folder">
						<input type="hidden" name="current_folder" value="<?=$current_folder?>" >
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label input-sm">Folder Name</label>
										<div class="col-sm-8">
											<input type="text" class="form-control input-sm folder" id="folder" name="folder" required>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm btn-close-add-folder-modal" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-default btn-sm sbt-create-folder">Create</button>
			</div>
		</div>

	</div>
</div>
<div id="modal-manage-folder" class="modal fade" role="dialog">
	<div class="modal-dialog" style="max-width:480px">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-create-folder-header">Manage Folder</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-1">
						&nbsp;
					</div>
					<div class="col-md-10">
						<table class="table custom-table">
							<thead>
							<tr>
								<th>Folder</th>
								<th style="width: 100px">Action</th>
							</tr>
							</thead>
							<tbody class="manage-folder-tbody">
							<tr>
								<td></td>
								<td></td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<form id="frm_mailbox" action="" method="post">
	<input type="hidden" name="action" class="action" value="">
	<input type="hidden" name="mail_id" class="mail_id">
	<input type="hidden" name="current_folder" class="current_folder" id="current_folder" value="<?=$current_folder?>">
	<input type="hidden" name="mail_filter" class="mail_filter" id="mail_filter" value="all">
	<input type="hidden" name="folder" class="folder" id="folder">
</form>
<script>
	var current_folder = '<?=$current_folder?>';
	var loader = "";
	loader += '<div class="box">';
	loader += '<div class="overlay">';
	loader += '<i class="fa fa-refresh fa-spin"></i>';
	loader += '</div>';
	loader += '<div class="box-header with-border">';
	loader += '<h3 class="box-title">Loading...</h3>';
	loader += '</div>';
	loader += '</div>';

	$(document).ready(function(){
		/*$('ul.nav-stacked').droppable({
      		drop: function( event, ui ) {
	        $( this ).addClass( "ui-state-highlight" ).find( "p" ).html( "Dropped!" );
	      	}
	    });*/
		set_unread_mails();
		var MailListTable = $('#table-mail-list').DataTable({
			serverSide: true,
			ordering: false,
			processing: true,
			"ajax": {
				"url": "<?=base_url("mail-system3/mails-datatable/")?>",
				"type": "POST",
				"data": function ( d ) {
					d.folder = $("#current_folder").val();
					d.mail_filter = $("#mail_filter").val();
				}
			},
			searching: true,
			scrollY: 500,
			scroller: {
				loadingIndicator: true
			},
			"columnDefs": [
				{
					"targets": [0,1,2],
					"orderable": false
				}
			],
			"columns": [
				{ },
				{ "className":'mailbox-star'},
				{ "className":'mailbox-subject'}
			],
			"drawCallback":function(){
				$('.mailbox-messages input[type="checkbox"]').iCheck({
					checkboxClass: 'icheckbox_flat-blue',
					radioClass: 'iradio_flat-blue'
				});
				var clicks = $('.checkbox-toggle').data('clicks');
				if (clicks) {
					//Uncheck all checkboxes
					$(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
					$(".fa",'.checkbox-toggle').removeClass("fa-check-square-o").addClass('fa-square-o');
					$('.checkbox-toggle').data("clicks",!clicks);
				}
			}
		});

		$('#table-mail-list_filter').hide();

		$(document).on('click','.btn-maximize-minimize',function(){
			$('.mail_system_detail_area').toggleClass('window-maximize');
			$('.folder_list_area,.mail_list_area').toggleClass('hidden');
			if($('.mail_system_detail_area').hasClass('window-maximize')) {
				$(this).html('Minimize');
			} else {
				$(this).html('Maximize');
			}
		});

		$(document).on('click','.btn-remove-attachment',function(){
			var attachment_url = $(this).data('attachment_url');
			$(this).closest('li').fadeOut('medium',function(){
				$(this).remove();
			});
			$("input[value='"+attachment_url+"']").remove();
		});
		$(document).on('click','.btn-delete-folder',function(){
			var BtnObj = $(this);
			var folder_id = $(this).data('folder_id');
			$.ajax({
				url:"<?=base_url()?>mail-system3/delete-folder/"+folder_id,
				type: 'POST',
				data: null,
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success:function(data){
					if(data.success == true){
						$('#select-folder-header').html(data.move_to_folder);
						$('#select-folder-footer').html(data.move_to_folder);
						$('#sidebar-mail-label').html(data.sidebar_mail_label);
						$('.btn-folder[data-folder="inbox"]').trigger('click');
						BtnObj.closest('tr').fadeOut('medium',function(){
							$(this).remove();
						});
						show_notify(data.message,data.success);
					}else{
						show_notify(data.message,data.success);
					}
				}
			});
		});
		$(document).on('click','.btn-manage-folder',function(){
			$.ajax({
				url:"<?=base_url()?>mail-system3/get_folder_by_staff/true",
				type: 'POST',
				data: null,
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success:function(data){
					if(data.success == true){
						var folder_data = data.folder_data;
						var table_html = "";
						$.each(folder_data,function(index,value){
							table_html += "<tr>";
								table_html += "<td>"+value.folder+"</td>";
								table_html += '<td><a href="javascript:void(0);" class="btn-delete-folder btn-danger btn-xs" data-folder_id="'+value.folder_id+'"><i class="fa fa-trash"></i></a></td>';
							table_html += "/<tr>";
						})
						$("tbody.manage-folder-tbody").html(table_html);
						$("#modal-manage-folder").modal('show');
					}
				}
			});
		});

		$(document).on('click','.btn-mail-filter',function(){
			/*---- Clear search text --------*/
			$('.txt-search-all-mail').val('');
			$('.txt-search-mail').val('');

			/*---- Set Active To INBOX --------*/
			$("#current_folder").val('inbox');
			$('.btn-folder:not(.btn-view-folder)').addClass('btn-view-folder').closest('li').removeClass('active');
			$('.btn-folder[data-folder="inbox"]').removeClass('btn-view-folder').closest('li').addClass('active');

			$("#mail_filter").val($(this).data('mail_filter'));
			MailListTable.draw();
			$(this).closest('ul').find('li').each(function(){
				$(this).removeClass('active');
				$(this).find('a').addClass('btn-mail-filter');
			});
			$(this).removeClass('btn-mail-filter').closest('li').addClass('active');
		});

		$(document).on('keyup','.txt-search-mail',function(){
			MailListTable.search($(this).val()).draw() ;
		});

		$(document).on('keyup','.txt-search-all-mail',function(){
			$("#current_folder").val('all');
			$(".folder-name").text("Search");
			$('.btn-folder:not(.btn-view-folder)').addClass('btn-view-folder').closest('li').removeClass('active');
			MailListTable.search($(this).val()).draw() ;
		});
		$(document).on('click','.btn-mails-move-to',function(){
			console.log($(this).data('folder'));
			$("#frm-mail-list").find('input[name="to_folder"]').val($(this).data('folder'));
			$("#frm-mail-list").attr('action','<?=base_url()?>mail-system3/mail_move_to/').submit();
		});

		$(document).on('submit','#frm-mail-list',function(e){
			e.preventDefault();
			$.ajax({
				url:$(this).attr('action'),
				type: 'POST',
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success:function(data){
					MailListTable.draw();
					show_notify(data.message,data.success);
				}
			});
		});

		$(document).on('click',".checkbox-toggle",function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$(".mailbox-messages input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks",!clicks);
		});

		$(document).on('click',".mailbox-star",function (e) {
			e.preventDefault();
			var $this = $(this).find("a > i");
			var glyph = $this.hasClass("glyphicon");
			var fa = $this.hasClass("fa");
			//Switch states
			if (glyph) {
				$this.toggleClass("glyphicon-star");
				$this.toggleClass("glyphicon-star-empty");
			}
			if (fa) {
				$this.toggleClass("fa-star");
				$this.toggleClass("fa-star-o");
			}
		});

		$(document).on('click','.btn-star',function(){
			var star_button = $(this);
			var mail_id = $(this).data('mail_id');
			if($(this).find('.fa-star').length == 0){
				var url = "<?=base_url()?>mail-system3/set-starred-flag/"+mail_id;
			}else{
				var url = "<?=base_url()?>mail-system3/remove-starred-flag/"+mail_id;
			}
			if(current_folder == 'starred'){
				star_button.closest('tr').fadeOut('slow');
			}
			$.ajax({
				url: url,
				type: "GET",
				data: null,
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {

				}
			});
		});

		$(document).on('click','.btn-view-folder',function(){
			/*---------- Clear Search Text ------*/
			$('.txt-search-all-mail').val('');
			$('.txt-search-mail').val('');

			$('.btn-folder').each(function(){
				$(this).addClass('btn-view-folder');
				$(this).closest('li').removeClass('active');
			});
			$(this).removeClass('btn-view-folder').closest('li').addClass('active');
			current_folder = $(this).data('folder');
			$('.folder-name').text(current_folder);
			$("#current_folder").val(current_folder);
			MailListTable.search('').draw();
		});

		$(document).on('click','.sbt-create-folder',function(){
			$('#frm-create-folder').submit();
		});


		$(document).on('click','td.mailbox-subject',function(){
			var mail_id = $(this).find('.btn-read-mail').data('mail_id');
			$("#ajax-loader").show();
			$(this).find('.is_unread').removeClass('is_unread');
			$.ajax({
				url: "<?=base_url()?>mail-system3/read-mail/"+mail_id,
				type: "POST",
				data: null,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					set_unread_mails();
					$(".mail_system_detail_area").html(data.mail_system_detail_area);
					$("#ajax-loader").hide();
				},
				error: function (msg) {
					$("#ajax-loader").hide();
				}
			});
		});

		$(document).on('click','.btn-read-mail,.btn-mail-pagination',function(e){
			var mail_id = $(this).data('mail_id');
			if($(this).hasClass('drafts')){
				$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
				$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/drafts');
				$("#frm_mailbox").submit();
			}else{
				$("#ajax-loader").show();
				$(this).closest('.mailbox-subject').find('.is_unread').removeClass('is_unread');
				$.ajax({
					url: "<?=base_url()?>mail-system3/read-mail/"+mail_id,
					type: "POST",
					data: null,
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function (data) {
						set_unread_mails();
						$(".mail_system_detail_area").html(data.mail_system_detail_area);
						$("#ajax-loader").hide();
					},
					error: function (msg) {
						$("#ajax-loader").hide();
					}
				});
			}
		});

		$(document).on('submit','#frm-create-folder',function(e){
			e.preventDefault();
			$("#modal-create-folder-header").text('Processing...');
			var folder_name = $("form#frm-create-folder").find('input#folder').val();
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					console.log(data);
					if(data.success == true){
						$('#select-folder-header').html(data.move_to_folder);
						$('#select-folder-footer').html(data.move_to_folder);
						$('#sidebar-mail-label').html(data.sidebar_mail_label);
						show_notify(data.message,true);
						$('#modal-create-folder').modal('hide');
						$('.btn-mails-move-to[data-folder="'+folder_name+'"]').trigger('click');
						$("form#frm-create-folder").find('input#folder').val('');
					}else{
						show_notify(data.message,false);
					}
					$("#modal-create-folder-header").text('Create Folder');
				}
			});
		});

		$(document).on('click','.btn-trash-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/delete-mail');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-forward-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/forward');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-reply-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/reply');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-discard-drafts',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/discard-drafts/');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-refresh-mail',function(){
			var ButtonObj = $(this);
			ButtonObj.html('<i class="fa fa-refresh fa-spin"></i> Refreshing...');
			$.ajax({
				url: '<?=base_url()?>mail-system3/save-unread-mails',
				type: "GET",
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					if (data.success == false) {
						show_notify(data.message, false);
						ButtonObj.html('<i class="fa fa-refresh"></i>');
						return false;
					}
					set_unread_mails();
					MailListTable.draw();
					ButtonObj.html('<i class="fa fa-refresh"></i>');
					if (data.success == true) {
						show_notify(data.message, true);
					}
				}
			});
		});

		$(document).on('click','.btn-reply-mail-list',function(){
			var i = 1;
			$('.chk-select-mail:checked').each(function() {
				if(i == 1){
					$("form#frm_mailbox").find('.mail_id').val(this.value);
					$("form#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/reply');
					$("form#frm_mailbox").submit();
					return false;
				}
				i++;
			});
		});
		$(document).on('click','.btn-forward-mail-list',function(){
			var i = 1;
			$('.chk-select-mail:checked').each(function() {
				if(i == 1){
					$("form#frm_mailbox").find('.mail_id').val(this.value);
					$("form#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/forward');
					$("form#frm_mailbox").submit();
					return false;
				}
				i++;
			});
		});

		$(document).on('submit','#frm-settings',function(e){
			e.preventDefault();
			$('.btn-save-settings').html('Saving...');
			$.ajax({
				url: '<?=base_url()?>mail-system3/save-settings/',
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					$('.btn-save-settings').html('Save');
					if(data.success == true){
						show_notify(data.message,true);
					}else{
						show_notify('Something wrong!',false);
					}
				}
			});
		});

		$(document).on('click','.fileinput-remove',function(){
			$("input.attachment_url").each(function(index){
				if(index == 0){
					$(this).val('');
				}else{
					$(this).remove();
				}
			});
		});
		$(document).on('click','.btn-remove-cc',function(){
			$(this).closest('.cc-section').fadeOut('medium',function(){$(this).remove();});
		});
		$(document).on('click','.btn-remove-bcc',function(){
			$(this).closest('.bcc-section').fadeOut('medium',function(){$(this).remove();});
		});
		$(document).on('click','.btn-add-cc',function(){
			var cc_section = $('.cc-section.hidden').clone();
			cc_section.removeClass('hidden').find('input.cc-input').attr('name','cc[]');
			//cc_section.insertAfter('.cc-section:not(.hidden):last');
			cc_section.insertBefore($(this).closest('div.row'));
		});
		$(document).on('click','.btn-add-bcc',function(){
			var bcc_section = $('.bcc-section.hidden').clone();
			bcc_section.removeClass('hidden').find('input.bcc-input').attr('name','bcc[]');
			//bcc_section.insertAfter('.bcc-section:not(.hidden):last');
			bcc_section.insertBefore($(this).closest('div.row'));
		});
	});
	function set_unread_mails() {
		$.ajax({
			url: "<?=base_url('app/get_inbox_unread_mails/');?>",
			type: "GET",
			data: null,
			contentType: false,
			cache: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.no_unread_mails > 0){
					$('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">' + data.no_unread_mails + '</span>');
					$('.sidebar-unread-mails').append('<span class="pull-right-container"><span class="label label-primary pull-right">' + data.no_unread_mails + '</span></span>');
					$('.top-no-unread-mails').text(data.no_unread_mails);
					if ($('.mailbox-list-inbox-label').find('.no-unread-mails').length == 0) {
						$('.mailbox-list-inbox-label').append('<span class="label label-primary pull-right no-unread-mails">' + data.no_unread_mails + '</span>');
					} else {
						$('.mailbox-list-inbox-label').find('.no-unread-mails').text(data.no_unread_mails);
					}
				}else{
					$('.mailbox-list-inbox-label').find('.no-unread-mails').remove();
					$('.unread-mail-icon').find('.inbox-unread-mails').remove();
					$('.sidebar-unread-mails').find('.pull-right-container').remove();
					$('.top-no-unread-mails').text(0);
				}
				$('.messages-menu').find('ul.dropdown-menu').remove();
				$('.messages-menu').append(data.unread_mails);

			}
		});
	}
</script>
<?php
if($this->uri->segment(2) == 'settings') {
	?>
	<script>
		$(function () {
			//$("#signature-textarea").wysihtml5();
			CKEDITOR.config.allowedContent = true;
			CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g );
			CKEDITOR.replace('signature-textarea', {
				height: 350,
				filebrowserBrowseUrl: '<?=base_url();?>mail-system3/attachment_browse_from_server',
				filebrowserUploadUrl: '<?=base_url();?>mail-system3/upload_attachment_to_server',
				removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
			});
		});
	</script>
	<?php
}
if($this->uri->segment(2) == 'compose-mail' || $this->uri->segment(2) == 'compose_mail'
	|| $this->uri->segment(2) == 'document-mail' || $this->uri->segment(2) == 'document_mail') {
	?>
	<script>
		$(function () {
			var emailBloodhound = new Bloodhound({
				datumTokenizer: function (d) {
					return Bloodhound.tokenizers.whitespace(d.email);
				},
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				remote: {
					url: $.core.url('mail-system3/mail-auto-complete/%QUERY'),
					wildcard: '%QUERY'
				}
			});
			emailBloodhound.initialize();

			$(".to-input").tagsinput({
				tagClass: 'label-default email-tags',
				freeInput: true,
				typeaheadjs: {
					source: emailBloodhound.ttAdapter()
				}
			});

			$(".cc-input").tagsinput({
				tagClass: 'label-default email-tags',
				typeaheadjs: {
					source: emailBloodhound.ttAdapter()
				},
				freeInput: true
			});

			$(".bcc-input").tagsinput({
				tagClass: 'label-default email-tags',
				typeaheadjs: {
					source: emailBloodhound.ttAdapter(),
				},
				freeInput: true
			});
			CKEDITOR.config.allowedContent = true;
			CKEDITOR.config.protectedSource.push(/<\?[\s\S]*?\?>/g);
			CKEDITOR.replace('compose-textarea', {
				height: 350,
				filebrowserBrowseUrl: '<?=base_url();?>mail-system3/attachment_browse_from_server',
				filebrowserUploadUrl: '<?=base_url();?>mail-system3/upload_attachment_to_server',
				removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
			});
			CKEDITOR.on('instanceReady',function(){
			   $('.cke_toolbar_break').removeClass();
			   $('a.cke_button').css('padding-left','4px');
			   $('a.cke_button').css('padding-right','4px');
			});
			$('#discardbtnicon').click(function () {
				window.location.href = '<?=base_url()?>mail-system3/inbox/';
			});
			$(document).on('change', '#upload_attachment', function () {
				$("form#frmComposeMail").find('.overlay').removeClass('hidden');
				var formdata = new FormData();
				$.each($("#upload_attachment"), function(i, obj) {
					$.each(obj.files,function(j, file){
						formdata.append('upload_attachment['+j+']', file);
					})
				});
				$.ajax({
					url: "<?=base_url('mail-system3/upload_attachment/');?>",
					type: "POST",
					data: formdata,
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function (data) {
						$("form#frmComposeMail").find('.overlay').addClass('hidden');
						if (data.success == true) {
							show_notify(data.message, true);
							var f_inc = 0;
							$.each(data.attachment_url, function(attachment_url_k, attachment_url_v) {
								if ($(".attachment_url:last").val() == '') {
									$(".attachment_url:last").val(attachment_url_v);
								} else {
									var attach_html = $(".attachment_url:last").clone();
									attach_html.insertAfter($(".attachment_url:last"));
									$(".attachment_url:last").val(attachment_url_v);
								}
								var new_attachments = '	<li><span class="mailbox-attachment-icon"><i class="fa fa-file-o"></i></span>';
								new_attachments		+='		<div class="mailbox-attachment-info">';
								new_attachments		+='			<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>' + data.file_name[f_inc] + '</a>';
								new_attachments		+='			<span class="mailbox-attachment-size text-right">';
								new_attachments		+='				<a href="' + attachment_url_v + '" class="btn btn-default btn-xs" download="' + data.file_name[f_inc] + '"><i class="fa fa-cloud-download"></i></a>';
								new_attachments		+='		    	<a href="javascript:void(0);" class="btn btn-default btn-xs btn-remove-attachment" data-attachment_url="' + attachment_url_v + '" title="Remove Attachment"><i class="fa fa-times"></i></a>';
								new_attachments		+='			</span>';
								new_attachments		+='		</div>';
								new_attachments		+='	</li>';
								$('ul.mailbox-attachments').append(new_attachments);
								f_inc++;
							});
						} else {
							show_notify(data.message, false);
						}
					}
				});
			});
		});
	</script>
	<?php
}
?>
