<div class="">
	<form id="frmComposeMail" action="<?=base_url("mail-system3/compose-mail/$action/")?>" method="post" enctype="multipart/form-data">
		<?php
		if(isset($mail_detail['mail_id'])) {
            ?>
            <input type="hidden" name="mail_id" value="<?= $mail_detail['mail_id']; ?>">
            <?php
        }
		?>
		<div class="box box-primary">
			<div class="overlay hidden">
				<i class="fa fa-refresh fa-spin"></i>
			</div>
			<div class="box-header with-border">
				<div class="col-md-8 no-padding">
					<?php
					if($action == 'compose'){
						echo '<h3 class="box-title">Compose New Message</h3>';
					}elseif($action == 'forward'){
						echo '<h3 class="box-title">Forward To : </h3>';
					}elseif($action == 'drafts'){
						echo '<h3 class="box-title">Drafts : </h3>';
					}elseif($action == 'reply'){
						echo '<h3 class="box-title">Reply To : '.$mail_detail['reply_to_username'].'</h3>';
					}
					?>
				</div>
				<div class="col-md-4 text-right">
					<a href="javascript:void(0);" class="btn btn-primary btn-xs btn-maximize-minimize">Maximize</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="form-group">
					<?php
					if($action == 'compose') {
						if(isset($to_address)){
							echo '<input class="form-control to-input" id="to-input" placeholder="TO:" name="to" value="'.$to_address.'">';
						} else {
							echo '<input class="form-control to-input" id="to-input" placeholder="TO:" name="to" value="">';
						}
					} else if($action == 'reply'){
						echo '<input class="form-control to-input" id="to-input" placeholder="TO:" name="to" value="' .$mail_detail['reply_to'].'" required="required">';
					} else if ($action == 'forward'){
						echo '<input class="form-control to-input" id="to-input" placeholder="TO:" name="to" value="">';
					} else if ($action == 'drafts'){ 
						echo '<input class="form-control to-input" id="to-input" placeholder="TO:" name="to" value="' .$mail_detail['email'].'" >';
					}
					?>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group cc-section">
							<?php
							if(isset($mail_detail['cc']) && is_array($mail_detail['cc']) && count($mail_detail['cc']) > 0 && $action != 'forward') {
								$cc_input_items = implode(',',$mail_detail['cc']);
								echo '<input type="text" class="form-control cc-input" placeholder="CC:" name="cc" value="'.$cc_input_items.'">';
							}else{
								echo '<input type="text" class="form-control cc-input" placeholder="CC:" name="cc" value="">';
							}
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group bcc-section">
							<?php
							if(isset($mail_detail['bcc']) && is_array($mail_detail['bcc'])  && count($mail_detail['bcc']) > 0 && $action != 'forward') {
								$bcc_input_items = implode(',',$mail_detail['bcc']);
								echo '<input type="text" class="form-control bcc-input" placeholder="Bcc:" name="bcc" value="'.$bcc_input_items.'">';
							}else{
								echo '<input type="text" class="form-control bcc-input" placeholder="Bcc:" name="bcc" value="">';
							}
							?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<?php
					if($action == 'compose'){
						if(isset($subject)){
							echo '<input class="form-control" placeholder="Subject:" name="subject" required="required" value="'.$subject.'">';
						}else{
							echo '<input class="form-control" placeholder="Subject:" name="subject" required="required">';
						}
					}elseif($action == 'forward'){
						echo '<input class="form-control" placeholder="Subject:" name="subject" value="Fwd: '.$mail_detail['subject'].'" required="required">';
					}elseif($action == 'reply'){
						echo '<input class="form-control" placeholder="Subject:" name="subject" value="Re: '.$mail_detail['subject'].'" required="required">';
					}elseif($action == 'drafts'){
						echo '<input class="form-control" placeholder="Subject:" name="subject" value="'.$mail_detail['subject'].'" required="required">';
					}
					?>
				</div>

				<div class="form-group input-group cc-section hidden">
					<input type="text" class="form-control cc-input" placeholder="Cc:" value="">
					<span class="input-group-btn">
					  <button type="button" class="btn btn-danger btn-flat btn-remove-cc"><i class="fa fa-trash-o"></i></button>
					</span>
				</div>
				<div class="form-group input-group bcc-section hidden">
					<input type="text" class="form-control bcc-input" placeholder="Bcc:" value="">
					<span class="input-group-btn">
					  <button type="button" class="btn btn-danger btn-flat btn-remove-bcc"><i class="fa fa-trash-o"></i></button>
					</span>
				</div>
				<div class="form-group">
					<?php
					if($action == 'compose'){
						if(isset($settings['signature'])){
							echo '<textarea id="compose-textarea" class="form-control" style="height: 400px;" name="body">'.$settings['signature'].'</textarea>';
						}else{
							echo '<textarea id="compose-textarea" class="form-control" style="height: 400px;" name="body"></textarea>';
						}
					}elseif($action == 'forward' || $action == 'reply'){
						if (isset($settings['signature'])) {
							if($action == 'forward'){
								echo '<textarea id="compose-textarea" class="form-control" style="height: 400px;" name="body"> <br />'.$mail_detail['body'].$settings['signature'].'</textarea>';
							}else{
								echo '<textarea id="compose-textarea" class="form-control" style="height: 400px;" name="body"> <br />'.$settings['signature'].$mail_detail['body'].'</textarea>';
							}
						} else {
							echo '<textarea id="compose-textarea" class="form-control" style="height: 400px;" name="body"> <br />' . $mail_detail['body'] . '</textarea>';
						}
					}elseif($action == 'drafts'){
						echo '<textarea id="compose-textarea" class="form-control" style="height: 400px;" name="body">'.$mail_detail['body'].'</textarea>';
					}
					?>
				</div>

				<?php
				if($action != 'reply' && ($action == 'forward' || $action == 'drafts' || isset($document_url))) {
					?>
					<!--Attachments Files Lists-->
					<ul class="mailbox-attachments clearfix">
						<?php
						if (isset($mail_detail['attachments']) && count($mail_detail['attachments']) > 0) { //print_r($mail_detail['attachments']);
							foreach ($mail_detail['attachments'] as $attachment) {
								if (in_array($attachment['subtype'], array('png', 'jpg', 'jpeg', 'gif'))) {
									$mime_type = 'image/' . $attachment['subtype'];
									?>
									<li>
										<?php
										if (isset($attachment['url'])) {
											?>
											<span class="mailbox-attachment-icon has-img"><img
													src="<?= base_url() . $attachment['url']; ?>"
													alt="Attachment"></span>
											<?php
										} else {
											?>
											<span class="mailbox-attachment-icon has-img"><img
													src="data:<?= $mime_type ?>;base64,<?= $attachment['file']; ?>"
													alt="Attachment"></span>
											<?php
										}
										?>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i
													class="fa fa-camera"></i> <?= $attachment['filename']; ?></a>
											<span class="mailbox-attachment-size text-right">
												&nbsp;
												<?php
												if (isset($attachment['url'])) {
													?>
													<a href="<?= base_url() . $attachment['url']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												} else {
													?>
													<a href="data:<?= $mime_type ?>;base64,<?= $attachment['file']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												}
												?>
												<a href="javascript:void(0);"
												   class="btn btn-default btn-xs btn-remove-attachment"
												   data-attachment_url="<?= isset($attachment['url']) ? $attachment['url'] : ''; ?>"
												   title="Remove Attachment"><i class="fa fa-times"></i></a>
											</span>
										</div>
									</li>
									<?php
								} elseif (in_array($attachment['subtype'], array('pdf'))) {
									$mime_type = 'application/pdf';
									?>
									<li>
										<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i
													class="fa fa-paperclip"></i> <?= $attachment['filename']; ?></a>
											<span class="mailbox-attachment-size text-right">
												&nbsp;
												<?php
												if (isset($attachment['url'])) {
													?>
													<a href="<?= base_url() . $attachment['url']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												} else {
													?>
													<a href="data:<?= $mime_type ?>;base64,<?= $attachment['file']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												}
												?>
												<a href="javascript:void(0);"
												   class="btn btn-default btn-xs btn-remove-attachment"
												   data-attachment_url="<?= isset($attachment['url']) ? $attachment['url'] : ''; ?>"
												   title="Remove Attachment"><i class="fa fa-times"></i></a>
											</span>
										</div>
									</li>
									<?php
								} elseif (in_array($attachment['subtype'], array('plain', 'txt'))) {
									$mime_type = 'text/plain';
									?>
									<li>
										<span class="mailbox-attachment-icon"><i class="fa fa-file-text-o"></i></span>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i
													class="fa fa-paperclip"></i> <?= $attachment['filename']; ?></a>
											<span class="mailbox-attachment-size text-right">
												&nbsp;
												<?php
												if (isset($attachment['url'])) {
													?>
													<a href="<?= base_url() . $attachment['url']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												} else {
													?>
													<a href="data:<?= $mime_type ?>;base64,<?= $attachment['file']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												}
												?>
												<a href="javascript:void(0);"
												   class="btn btn-default btn-xs btn-remove-attachment"
												   data-attachment_url="<?= isset($attachment['url']) ? $attachment['url'] : ''; ?>"
												   title="Remove Attachment"><i class="fa fa-times"></i></a>
											</span>
										</div>
									</li>
									<?php
								} elseif (in_array($attachment['subtype'], array('zip'))) {
									$mime_type = 'application/' . $attachment['subtype'];
									?>
									<li>
										<span class="mailbox-attachment-icon"><i class="fa fa-file-zip-o"></i></span>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i
													class="fa fa-paperclip"></i> <?= $attachment['filename']; ?></a>
											<span class="mailbox-attachment-size text-right">
												&nbsp;
												<?php
												if (isset($attachment['url'])) {
													?>
													<a href="<?= base_url() . $attachment['url']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												} else {
													?>
													<a href="data:<?= $mime_type ?>;base64,<?= $attachment['file']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												}
												?>
												<a href="javascript:void(0);"
												   class="btn btn-default btn-xs btn-remove-attachment"
												   data-attachment_url="<?= isset($attachment['url']) ? $attachment['url'] : ''; ?>"
												   title="Remove Attachment"><i class="fa fa-times"></i></a>
											</span>
										</div>
									</li>
									<?php
								} else {
									$mime_type = 'application/' . $attachment['subtype'];
									?>
									<li>
										<span class="mailbox-attachment-icon"><i class="fa fa-file-o"></i></span>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i
													class="fa fa-paperclip"></i> <?= $attachment['filename']; ?></a>
											<span class="mailbox-attachment-size text-right">
											&nbsp;
												<?php
												if (isset($attachment['url'])) {
													?>
													<a href="<?= base_url() . $attachment['url']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												} else {
													?>
													<a href="data:<?= $mime_type ?>;base64,<?= $attachment['file']; ?>"
													   class="btn btn-default btn-xs"
													   download="<?= $attachment['filename']; ?>"><i
															class="fa fa-cloud-download"></i></a>
													<?php
												}
												?>
												<a href="javascript:void(0);"
												   class="btn btn-default btn-xs btn-remove-attachment"
												   data-attachment_url="<?= isset($attachment['url']) ? $attachment['url'] : ''; ?>"
												   title="Remove Attachment"><i class="fa fa-times"></i></a>
											</span>
										</div>
									</li>
									<?php
								} ?>
								<input type="hidden" name="attachments[]" class="attachment_url"
									   value="<?= isset($attachment['url']) ? $attachment['url'] : '' ?>">
								<?php
							}
						}
						?>

						<?php
						if (isset($document_url) && count($document_url) > 0) {
							foreach ($document_url as $document_url_row) {
								?>
								<li>
									<span class="mailbox-attachment-icon"><i class="fa fa-file-o"></i></span>
									<div class="mailbox-attachment-info">
										<a href="javascript:void(0);" class="mailbox-attachment-name"><i
												class="fa fa-paperclip"></i> <?= basename($document_url_row, PATHINFO_FILENAME); ?>
										</a>
										<span class="mailbox-attachment-size text-right">
											&nbsp;
											<a href="<?= base_url() . $document_url_row; ?>"
											   class="btn btn-default btn-xs"
											   download="<?= $document_url_row; ?>"><i class="fa fa-cloud-download"></i></a>
											<a href="javascript:void(0);"
											   class="btn btn-default btn-xs btn-remove-attachment"
											   data-attachment_url="<?= $document_url_row; ?>"
											   title="Remove Attachment"><i class="fa fa-times"></i></a>
										</span>
									</div>
								</li>
								<input type="hidden" name="attachments[]" class="attachment_url"
									   value="<?= $document_url_row; ?>">
								<?php
							}
						}
						?>
					</ul>
					<!--End Attachments Files Lists-->
					<?php
				} else { echo '<ul class="mailbox-attachments clearfix"></ul>'; }
				?>
				<div class="form-group">
					<input type="file" class="file-input-ajax" name="upload_attachment[]" id="upload_attachment" multiple = "multiple" >
					<input type="hidden" name="attachments[]" class="attachment_url">
					<p class="help-block">Max. 32MB</p>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="request_read_receipt" checked> Request read receipt
										</label>
									</div>
								</div>
								<!-- <div class="col-md-6 text-right">
									<button type="button" class="btn btn-primary btn-flat btn-add-bcc">Add more bcc</button>
								</div> -->
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="pull-right">
							<?php
							if($action == 'drafts') {
								echo '<button type="submit" class="btn btn-default" name="btn_save_draft"><i class="fa fa-pencil"></i> Save</button>';
							} else {
								echo '<button type="submit" class="btn btn-default" name="btn_draft"><i class="fa fa-pencil"></i> Draft</button>';
							}
							?>
							<button type="submit" class="btn btn-primary" name="btn_send" id="btn_send"><i class="fa fa-envelope-o"></i> Send</button>
						</div>
					</div>
				</div>
				<?php
				if($action == 'drafts') {
					echo '<button type="button" class="btn btn-default btn-discard-drafts" data-mail_id="' . $mail_detail['mail_id'] . '"><i class="fa fa-times"></i> Discard</button>';
				}
				?>
			</div>
			<!-- /.box-footer -->
		</div>
	</form>
</div>
<style> .file-caption.kv-fileinput-caption, .fileinput-remove.fileinput-remove-button{ display: none; } </style>
<script>
	$(document).ready(function(){
		$("#btn_send").click(function(){
			if ($(".bootstrap-tagsinput")){
				if($(".bootstrap-tagsinput span").hasClass("email-tags")){
					var spamm = $('.email-tags').html();
					if(spamm  == ""){
						console.log(spamm);
						show_notify("Please add To email address !", false);
						return false;
					}
				}else{
					show_notify("Please add To email address !", false);
					return false;
				}
			}
		});
		$(".file-input-ajax").fileinput({
			uploadUrl: "<??>", // server upload action
			uploadAsync: true,
			maxFileCount: 5,
			showPreview:false,
			showUpload:false,
			browseIcon:"<i class='fa fa-paperclip'></i>&nbsp;",
			browseLabel:"Attachment",
			initialPreview: [],
			fileActionSettings: {
				removeIcon: '<i class="icon-bin"></i>',
				removeClass: 'btn btn-link btn-xs btn-icon',
				uploadIcon: '<i class="icon-upload"></i>',
				uploadClass: 'btn btn-link btn-xs btn-icon',
				indicatorNew: '<i class="icon-file-plus text-slate"></i>',
				indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
				indicatorError: '<i class="icon-cross2 text-danger"></i>',
				indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
			}
		});
	});
</script>
