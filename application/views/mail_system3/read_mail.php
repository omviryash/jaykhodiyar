<div class="">
	<div class="box box-primary">
		<div class="overlay hidden"><i class="fa fa-refresh fa-spin"></i></div>
		<div class="box-header with-border">
			<h3 class="box-title">Read Mail</h3>
			<div class="box-tools pull-right">
				<a href="javascript:void(0);" class="btn btn-box-tool <?=$mail_detail['previous_mail_id'] != 0?'btn-mail-pagination':'';?>" data-mail_id="<?=$mail_detail['previous_mail_id']?>" data-toggle="tooltip" title="Previous" <?=$mail_detail['previous_mail_id'] == 0?'disabled="disabled"':'';?>><i class="fa fa-chevron-left"></i></a>
				<a href="javascript:void(0);" class="btn btn-box-tool <?=$mail_detail['next_mail_id'] != 0?'btn-mail-pagination':'';?>" data-mail_id="<?=$mail_detail['next_mail_id']?>" data-toggle="tooltip" title="Next" <?=$mail_detail['next_mail_id'] == 0?'disabled="disabled"':'';?>><i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
			<div class="mailbox-read-info">
				<h3><?=$mail_detail['subject'];?></h3>
				<?php
				echo "<h5>".$mail_detail['username']."</h5>";
				if(isset($mail_detail['cc']) && is_array($mail_detail['cc']) && count($mail_detail['cc']) > 0) {
					echo "<h5> <strong>CC : </strong>";
					foreach($mail_detail['cc'] as $key=>$cc){
						if($key != 0){
							echo ", ";
						}
						echo $cc;
					}
					echo "</h5>";
				}
				if(isset($mail_detail['bcc']) && is_array($mail_detail['bcc']) && count($mail_detail['bcc']) > 0) {
					echo "<h5> <strong>BCC : </strong>";
					foreach ($mail_detail['bcc'] as $key => $bcc) {
						if ($key != 0) {
							echo ", ";
						}
						echo $bcc;
					}
					echo "</h5>";
				}
				?>
				<span class="mailbox-read-time pull-right"><?=$mail_detail['received_at'];?></span></h5>
			</div>
			<!-- /.mailbox-read-info -->
			<div class="mailbox-controls with-border text-center">
				<div class="btn-group">
					<a  href="javascript:void(0);" class="btn-trash-mail btn btn-default btn-sm" data-mail_id="<?=$mail_detail['mail_id'];?>" data-toggle="tooltip" data-container="body" title="Delete">
						<i class="fa fa-trash-o"></i></a>
					<?php
					if($current_folder != 'drafts') {
						?>
						<a  href="javascript:void(0);" class="btn-reply-mail btn btn-default btn-sm" data-mail_id="<?=$mail_detail['mail_id'];?>" data-toggle="tooltip" data-container="body" title="Reply">
							<i class="fa fa-reply"></i></a>
						<a href="javascript:void(0);" class="btn-forward-mail btn btn-default btn-sm" data-mail_id="<?=$mail_detail['mail_id'];?>" data-toggle="tooltip" data-container="body" title="Forward">
							<i class="fa fa-share"></i></a>
						<?php
					}
					?>
				</div>
				<a href="<?=base_url()?>mail-system3/print-mail/<?=$mail_detail['mail_id'];?>" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
					<i class="fa fa-print"></i></a>
			</div>
			<!-- /.mailbox-controls -->
			<div class="mailbox-read-message">
				<iframe src="<?=base_url();?>mail-system3/mail_body_iframe/<?=$mail_detail['mail_id']?>"  frameborder="0" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%" id="mail_body_iframe"></iframe>
			</div>
			<!-- /.mailbox-read-message -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			<ul class="mailbox-attachments clearfix">
				<?php
				if(isset($mail_detail['attachments']) && count($mail_detail['attachments']) > 0 ) { //print_r($mail_detail['attachments']);
					foreach ($mail_detail['attachments'] as $attachment) {
						if(in_array($attachment['subtype'],array('png','jpg','jpeg','gif'))){
							$mime_type = 'image/'.$attachment['subtype'];
							?>
							<li>
								<?php
								if(isset($attachment['url'])) {
									?>
									<span class="mailbox-attachment-icon has-img"><img src="<?=base_url().$attachment['url'];?>" alt="Attachment"></span>
									<?php
								} else {
									?>
									<span class="mailbox-attachment-icon has-img"><img src="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" alt="Attachment"></span>
									<?php
								}
								?>
								<div class="mailbox-attachment-info">
									<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-camera"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
								</div>
							</li>
							<?php
						}elseif(in_array($attachment['subtype'],array('pdf'))){
							$mime_type = 'application/pdf';
							?>
							<li>
								<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
								<div class="mailbox-attachment-info">
									<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
								</div>
							</li>
							<?php
						}elseif(in_array($attachment['subtype'],array('plain','txt'))){
							$mime_type = 'text/plain';
							?>
							<li>
								<span class="mailbox-attachment-icon"><i class="fa fa-file-text-o"></i></span>
								<div class="mailbox-attachment-info">
									<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
								</div>
							</li>
							<?php
						}elseif(in_array($attachment['subtype'],array('zip'))){
							$mime_type = 'application/'.$attachment['subtype'];
							?>
							<li>
								<span class="mailbox-attachment-icon"><i class="fa fa-file-zip-o"></i></span>
								<div class="mailbox-attachment-info">
									<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
								</div>
							</li>
							<?php
						}else{ 
							$mime_type = 'application/'.$attachment['subtype'];
							?>
							<li>
								<span class="mailbox-attachment-icon"><i class="fa fa-file-o"></i></span>
								<div class="mailbox-attachment-info">
									<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
								</div>
							</li>
							<?php
						} ?>
						<?php
					}
				}
				?>
			</ul>
		</div>
		<!-- /.box-footer -->
		<div class="box-footer">
			<div class="pull-right">
				<?php
					if($current_folder == 'drafts'){
				?>
						<a  href="<?=base_url()?>mail-system3/send-drafts/<?=$mail_detail['mail_id'];?>" class="btn-reply-mail btn btn-default"><i class="fa fa-envelope-o"></i> Send</a>
						<?php
					}else {
						?>
						<a  href="javascript:void(0);" data-mail_id="<?=$mail_detail['mail_id'];?>" class="btn-reply-mail btn btn-default"><i class="fa fa-reply"></i> Reply</a>
						<a  href="javascript:void(0);" data-mail_id="<?=$mail_detail['mail_id'];?>" class="btn-forward-mail btn btn-default"><i class="fa fa-share"></i> Forward</a>
						<?php
					}
				?>
			</div>
			<button type="button" class="btn btn-default btn-trash-mail" data-mail_id="<?=$mail_detail['mail_id'];?>"><i class="fa fa-trash-o"></i> Delete</button>
			<a href="<?=base_url()?>mail-system3/print-mail/<?=$mail_detail['mail_id'];?>" class="btn btn-default"	><i class="fa fa-print"></i> Print</a>
		</div>
		<!-- /.box-footer -->
	</div>
</div>