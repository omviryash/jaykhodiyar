<div class="">
	<?php if($this->applib->have_access_role(MAIL_SYSTEM_COMPOSE_MENU_ID,"view")) { ?>
	<a href="<?=base_url('mail-system3/compose-mail/compose/')?>" class="btn btn-primary btn-block">Compose</a>
	<?php } ?>
	<a href="<?=base_url('mail-system3/settings/')?>" class="btn btn-primary btn-block margin-bottom">Settings</a>

	<?php if($this->applib->have_access_role(MAIL_SYSTEM_INBOX_MENU_ID,"view")) { ?>
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Folders</h3>

			<div class="box-tools">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body no-padding">
			<ul class="nav nav-pills nav-stacked">
				<li class="<?=$current_folder == 'inbox'?'active':''?>">
					<a href="javascript:void(0);" class="<?=$current_folder == 'inbox'?'':'btn-view-folder'?> btn-folder mailbox-list-inbox-label" data-folder="inbox" style="width: 75%;float: left;">
						<i class="fa fa-inbox"></i> Inbox
					</a>
					<button type="button" class="btn btn-danger btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu ul-mail-filter" role="menu">
						<!--Newest 1st, Oldest 1st, Unread Only, Read Only, All-->
						<li><a href="javascript:void(0);" data-mail_filter="newest" class="btn-mail-filter">Newest</a></li>
						<li><a href="javascript:void(0);" data-mail_filter="oldest" class="btn-mail-filter">Oldest</a></li>
						<li><a href="javascript:void(0);" data-mail_filter="unread_only" class="btn-mail-filter">Unread Only</a></li>
						<li><a href="javascript:void(0);" data-mail_filter="read_only" class="btn-mail-filter">Read Only</a></li>
						<li class="divider"></li>
						<li class="active"><a href="javascript:void(0);" data-mail_filter="all">All</a></li>
					</ul>
				</li>
				<li class="<?=$current_folder == 'starred'?'active':''?>">
					<a href="javascript:void(0);" class="<?=$current_folder == 'starred'?'':'btn-view-folder'?> btn-folder" data-folder="starred">
						<i class="fa fa-star-o"></i> Starred
					</a>
				</li>
				<li class="<?=$current_folder == 'sent'?'active':''?>">
					<a href="javascript:void(0);" class="<?=$current_folder == 'sent'?'':'btn-view-folder'?> btn-folder" data-folder="sent"><i class="fa fa-envelope-o"></i> Sent</a>
				</li>
				<li class="<?=$current_folder == 'drafts'?'active':''?>">
					<a href="javascript:void(0);" class="<?=$current_folder == 'drafts'?'':'btn-view-folder'?> btn-folder" data-folder="drafts"><i class="fa fa-file-text-o"></i> Drafts</a>
				</li>
				<li class="<?=$current_folder == 'outbox'?'active':''?>">
					<a href="javascript:void(0);" class="<?=$current_folder == 'outbox'?'':'btn-view-folder'?> btn-folder" data-folder="outbox"><i class="fa fa-archive"></i> Outbox</a>
				</li>
				<li class="<?=$current_folder == 'junk'?'active':''?>">
					<a href="javascript:void(0);" class="<?=$current_folder == 'junk'?'':'btn-view-folder'?> btn-folder" data-folder="junk"><i class="fa fa-filter"></i> Junk</a>
				</li>
				<li class="<?=$current_folder == 'trash'?'active':''?>">
					<a href="javascript:void(0);" class="<?=$current_folder == 'trash'?'':'btn-view-folder'?> btn-folder" data-folder="trash"><i class="fa fa-trash-o"></i> Trash</a>
				</li>
			</ul>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /. box -->
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Labels</h3>

			<div class="box-tools">
				<button type="button" class="btn btn-box-tool btn-manage-folder" title="Manage Labels"><i class="fa fa-cogs"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body no-padding">
			<ul class="nav nav-pills nav-stacked" id="sidebar-mail-label">
				<?php
				$folders = $this->applib->get_staff_mail_folders();
				?>
				<?php
				foreach ($folders as $folder_row) {
					?>
					<li class="<?= $current_folder == $folder_row->folder ? 'active' :''?>">
						<a href="javascript:void(0);"  class="<?=$current_folder == $folder_row->folder?'':'btn-view-folder'?> btn-folder" data-folder="<?=space_to_dash($folder_row->folder);?>">
							<i class="fa fa-circle-o text-red"></i> <?=$folder_row->folder;?>
						</a>
					</li>
					<?php
				}
				?>
			</ul>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
	<?php } ?>
</div>
