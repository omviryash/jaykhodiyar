<div class="">
	<div class="box box-primary">
		<div class="overlay hidden">
			<i class="fa fa-refresh fa-spin"></i>
		</div>
		<form id="frm-settings" method="post">
			<div class="overlay hidden"><i class="fa fa-refresh fa-spin"></i></div>
			<div class="box-header with-border">
				<h3 class="box-title">Settings</h3>
			</div>

			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<h4>Email Signature</h4>
						<div class="form-group">
							<textarea id="signature-textarea" class="form-control" style="height: 200px" name="signature"><?=isset($settings['signature'])?$settings['signature']:'';?></textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="box-footer">
				<div class="form-group">
					<div class="col-md-2 pull-right no-padding">
						<button type="submit" class="btn btn-block btn-flat btn-primary btn-save-settings">Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>