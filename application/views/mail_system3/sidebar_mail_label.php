<?php
$folders = $this->applib->get_staff_mail_folders();
?>
<?php
foreach ($folders as $folder_row) {
	?>
	<li class="<?= $current_folder == $folder_row->folder ? 'active' : '' ?>">
		<a href="javascript:void(0);" class="btn-view-folder btn-folder" data-folder="<?=space_to_dash($folder_row->folder);?>">
			<i class="fa fa-circle-o text-red"></i> <?=$folder_row->folder;?>
		</a>
	</li>
	<?php
}
?>