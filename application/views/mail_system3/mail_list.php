<?php if($this->applib->have_access_role(MAIL_SYSTEM_INBOX_MENU_ID,"view")) { ?>
<div class="">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title folder-name"><?=$current_folder;?></h3>

            <div class="box-tools pull-right">
                <div class="has-feedback">
                    <input type="text" class="form-control txt-search-mail" placeholder="Search Mail">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm btn-mails-move-to" data-folder="trash"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-reply-mail-list"><i class="fa fa-reply"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-forward-mail-list"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm btn-refresh-mail"><i class="fa fa-refresh"></i></button>
                <span class="dropdown">
                    <button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown">
                        <i class="fa fa-folder-o"></i>
                    </button>
                    <ul id="select-folder-header" class="dropdown-menu select-folder">
                        <?php
                            $this->load->view('mail_system3/move_to_folder_view', array('current_folder' => $current_folder));
                        ?>
                    </ul>
                </span>
                <div class="pull-right">
                    <div class="btn-group">
                        <!--<button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>-->
                    </div>
                    <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
            </div>
            <div class="mailbox-messages">
                <form method="post" action="" id="frm-mail-list">
                    <input type="hidden" name="from_folder" value="<?=$current_folder;?>">
                    <input type="hidden" name="to_folder" value="<?=$current_folder;?>">
                    <div class="table-responsive">
                        <table class="table table-hover table-striped" id="table-mail-list">
                            <thead>
                            </thead>
                        </table>
                    </div>
                </form>
            </div>
            <!-- /.mail-box-messages -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer no-padding">
            <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm btn-mails-move-to" data-folder="trash"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-reply-mail-list"><i class="fa fa-reply"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-forward-mail-list"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm  btn-refresh-mail"><i class="fa fa-refresh"></i></button>
                <span class="dropdown">
                    <button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown">
                        <i class="fa fa-folder-o"></i>
                    </button>
                    <ul id="select-folder-header" class="dropdown-menu select-folder">
                        <?php
                        $this->load->view('mail_system3/move_to_folder_view', array('current_folder' => $current_folder));
                        ?>
                    </ul>
                </span>

                <div class="pull-right">
                    <div class="btn-group">
                        <!--<button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>-->
                    </div>
                    <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
            </div>
        </div>
    </div>
    <!-- /. box -->
</div>
<?php } ?>
