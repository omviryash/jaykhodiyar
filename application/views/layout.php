<?php
	$currUrl = $this->uri->segment(1);
	$currUrl = str_replace('_', ' ', $currUrl);
    if($currUrl == ''){
        $currUrl = 'Dashboard';
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="<?=base_url('resource/image/jk-sidebar-logo.png')?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?=ucwords($currUrl)?> | Jay Khodiyar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <?php
        if($this->uri->segment(1) == 'hr' && $this->uri->segment(2) == "yearly-leaves") {
            ?>
            <link rel="stylesheet" type="text/css" href="<?= plugins_url("multi-dates-picker/css/mdp.css"); ?>">
            <link rel="stylesheet" type="text/css" href="<?= plugins_url("multi-dates-picker/css/prettify.css"); ?>">
            <?php
        }
    ?>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=bootstrap_url('css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=bootstrap_url('font-awesome/font-awesome.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=bootstrap_url('fonts/ionicons.min.css');?>">

    <!--Page Specific Css-->

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?=plugins_url('iCheck/all.css');?>">

    <!--Page Specific Css-->
    
    

    <!----------------Notify---------------->
    <link rel="stylesheet" href="<?=plugins_url('notify/jquery.growl.css');?>">
    <!---------------- Notify ---------------->

    <!-- iCheck -->
    <link rel="stylesheet" href="<?=plugins_url('iCheck/flat/blue.css');?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?=plugins_url('morris/morris.css');?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=plugins_url('jvectormap/jquery-jvectormap-1.2.2.css');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=plugins_url('datepicker/datepicker3.css');?>">
    <!-- Time Picker -->
    <link rel="stylesheet" href="<?=plugins_url('timepicker/bootstrap-timepicker.min.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=plugins_url('daterangepicker/daterangepicker.css');?>">
    <?php
    if(in_array($this->uri->segment(1),array('mailbox','mail-system','mail-system2','mail-system3','mail_system','mail_system2','mail_system3'))) {
        ?>
        <script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
        <?php
    }
    ?>
    <?php //if($this->uri->segment(2) == 'quotation'){?>
        <!-- DataTables -->
       <?php /* <link rel="stylesheet" href="<?=plugins_url('datatables/dataTables.bootstrap.css');?>"> */ ?>
	<?php
	if($this->uri->segment(1) != 'installation'){
	?>
	    <link rel="stylesheet" href="<?=plugins_url('datatables/media/css/jquery.dataTables.min.css');?>">
        <link rel="stylesheet" href="<?=plugins_url('datatables/extensions/Scroller/css/scroller.dataTables.min.css');?>">	
	<?php
	}else{
	?>
	
	<link rel="stylesheet" href="<?=plugins_url('datatables/media/css/jquery.dataTables.min.css');?>">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
	<link rel="stylesheet" href="<?=plugins_url('datatables/extensions/Scroller/css/scroller.dataTables.min.css');?>">	
   
	<?php
	}
	?>
    
    <?php //} ?>

    <!--      Multi Dates Pickers   -->


    <!-- jQuery 2.2.3 -->
    <script src="<?=plugins_url('jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?=bootstrap_url('js/jquery-ui.min.js');?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=bootstrap_url('js/bootstrap.min.js');?>"></script>

    <!-------- App Core Js --------->
    <script src="<?=dist_url('js/core.js');?>"></script>
    <!-------- /App Core Js --------->

    <!------ TypeAhead ------->
    <link rel="stylesheet" href="<?=plugins_url('typeahead/typeahead.css');?>">
    <script src="<?=plugins_url('typeahead/typeahead.bundle.min.js');?>"></script>
    <!------ /TypeAhead ------->
    
    
    <?php if($this->uri->segment(1) == 'mailbox' || $this->uri->segment(1) == 'mail-system' || $this->uri->segment(1) == 'mail-system2' || $this->uri->segment(1) == 'mail-system3'){?>
        <script src="<?=plugins_url('uploaders/fileinput.min.js');?>" type="text/javascript"></script>
        <link rel="stylesheet" href="<?=plugins_url('uploaders/dropzone.css');?>">
        <link rel="stylesheet" href="<?=plugins_url('bootstrap-tagsinput/bootstrap-tagsinput.css');?>">
        <link rel="stylesheet" href="<?=plugins_url('bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css');?>">
        <script src="<?=plugins_url('bootstrap-tagsinput/bootstrap-tagsinput.js');?>"></script>
    <?php } ?>
    
    <script type="text/javascript">
        var urls = '{"base":"<?=rtrim(base_url(),'/')?>","css":"<?=base_url()?>/dist/css","js":"<?=base_url()?>/dist/js"}';
    </script>
    
    <!-- select 2 -->
    <!-- <link rel="stylesheet" href="<?=plugins_url('s2/s2.css');?>">
    <script src="<?=plugins_url('s2/s2.js');?>"></script> -->
    <link rel="stylesheet" href="<?=plugins_url('s2/select2.css');?>">
    <script src="<?=plugins_url('s2/select2.full.js');?>"></script>
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=dist_url('css/AdminLTE.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=dist_url('css/skins/_all-skins.min.css');?>">
    <link rel="stylesheet" href="<?=bootstrap_url('css/custom.css');?>">
    <!-------- Chat --------->
    <link rel="stylesheet" href="<?=dist_url('css/custom.css');?>">
    <!-------- /Chat --------->
    
    <!-------- /Parsleyjs --------->
    <link rel="stylesheet" href="<?=base_url('resource/plugins/parsleyjs/src/parsley.css');?>">
    <script type="text/javascript" src="<?=base_url('resource/plugins/parsleyjs/dist/parsley.min.js');?>"></script>
    
    <?php
    if($this->uri->segment(2) == 'add_item_master' || $this->uri->segment(2) == 'add-item-master' || $this->uri->segment(2) == 'file_upload_demo' || $this->uri->segment(2) == 'terms_conditions') {
        ?>
        <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="<?= plugins_url('fileupload/css/jquery.fileupload.css'); ?>">
        <link rel="stylesheet" href="<?= plugins_url('fileupload/css/jquery.fileupload-ui.css'); ?>">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <noscript><link rel="stylesheet" href="<?= plugins_url('fileupload/css/jquery.fileupload-noscript.css'); ?>"></noscript>
        <noscript><link rel="stylesheet" href="<?= plugins_url('fileupload/css/jquery.fileupload.css'); ?>"></noscript>
        <script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
        <?php
    }
    ?>
    <link rel="stylesheet" href="<?=base_url('resource/plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css');?>">
    <script>
        var oldExportAction = function (self, e, dt, button, config) {
            if (button[0].className.indexOf('buttons-copy') >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action(e, dt, button, config);
            } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
            } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
            } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                if ($.fn.dataTable.ext.buttons.excelHtml5.available(dt, config)) {
                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config);
                }
                else {
                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                }
            } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
        };

        var newExportAction = function (e, dt, button, config) {
            var self = this;
            var oldStart = dt.settings()[0]._iDisplayStart;
//            $(".box-body").addClass('overlay clearfix');
//            $(".box-body").append('<i class="fa fa-refresh fa-spin"></i>');
            $('#ajax-loader').show();
            dt.one('preXhr', function (e, s, data) {
                // Just this once, load all data from the server...
                data.start = 0;
                data.length = 2147483647;

                dt.one('preDraw', function (e, settings) {
                    // Call the original action function
                    oldExportAction(self, e, dt, button, config);

                    dt.one('preXhr', function (e, s, data) {
                        // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                        // Set the property to what it was before exporting.
                        settings._iDisplayStart = oldStart;
                        data.start = oldStart;
                    });

                    // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                    setTimeout(dt.ajax.reload, 0);
//
//                    $(".box-body").removeClass('overlay');
//                    $(".box-body i.fa.fa-refresh.fa-spin").remove();
                    $('#ajax-loader').hide();
                    // Prevent rendering of the full data to the DOM
                    return false;
                });
            });

            // Requery the server with the new one-time export settings
            dt.ajax.reload();
        };
    </script>
    
    <link rel="stylesheet" href="<?=plugins_url('sweetalert/dist/sweetalert.css');?>">
    <script src="<?= plugins_url('sweetalert/dist/sweetalert.min.js'); ?>"></script>
    
</head>
<?php
$segment1 = $this->uri->segment(1);
?>
<body class="hold-transition skin-jk-light sidebar-mini <?=in_array($segment1,array('mail-system3','mail_system3'))?'sidebar-collapse':''?>">

<div class="wrapper">

    <?php
        $this->load->view('header');
       /* $this->load->view('sidebar');*/
    ?>
