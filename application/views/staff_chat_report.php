<style>
    table.dataTable thead th, table.dataTable thead td, table.dataTable.row-border tbody td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
        border-bottom: 1px solid #a3a3a3;
    }
    .dataTables_filter{
        float: left !important;
    }
    .dataTables_wrapper .dataTables_filter input {
        border: none;
        border-bottom: 1px solid #a3a3a3;
        padding: 8px;
    }
    div.dataTables_scrollBody {
        /*background: repeating-linear-gradient(45deg, #edeeff, #edeeff 10px, #fff 10px, #fff 20px);*/
        background: repeating-linear-gradient(45deg, #fff, #fff 10px, #fff 10px, #fff 20px) !important;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Staff Chat Report</small>
            <a href="<?=base_url();?>hr/add" class="btn pull-right btn-info"><i class="fa fa-plus pull-left" style="line-height: 21px;"></i>Add Employee</a>
        </h1>
    </section>
    <section class="content">
        <div class="box box-info" >
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="staff_chat_report" class="row-border" width="100%">
                            <thead>
                                <tr>
                                    <th width="7%">&nbsp;</th>
                                    <th width="28%">Name</th>
                                    <th width="35%">Title</th>
                                    <th width="15%" class="text-center">Accepted</th>
                                    <th width="15%" class="text-center">Response Time</th>
                                </tr>
                            <thead>
                            <tbody>
                            <?php
                                if (isset($agents) && is_array($agents)) {
                                    foreach ($agents as $agent) {
                                        if (in_array($agent['staff_id'], $chat_roles)) {
                            ?>
                                            <tr>
                                                <td style="padding: 12px;">
                                                    <i class="pull-right <?php echo ($agent['is_login'] == 1) ? 'fa fa-circle text-green' : 'fa fa-circle-o'; ?>" style="font-size: 10px"></i>
                                                    <img height="50px" width="50px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                                                </td>
                                                <td><?php echo $agent['name']; ?><br/><?php echo ($agent['is_login'] == 1) ? 'Online' : 'Offline'; ?></td>
                                                <td><?php echo $agent['chat_title']; ?></td>
                                                <td class="text-center"><?php echo $agent['accepted_visitors']; ?></td>
                                                <td class="text-center"><?php echo $agent['response_time']; ?></td>
                                            </tr>
                            <?php
                                        }
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function(){ 
//        $('#staff_chat_report').Datatable();
        table = $('#staff_chat_report').DataTable({
            "scrollY": 700,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
        });
    });
</script>