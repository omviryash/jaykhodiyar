<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Master : Purchase Item</small>
            <button type="button" class="btn btn-info btn-xs pull-right btn-save-item" style="margin: 5px;">
                <?php if(isset($id) && !empty($id)){ ?>Update 
                <?php } else { ?>Save 
                <?php } ?>
            </button>
            <?php if($this->applib->have_access_role(MASTER_ITEM_MENU_ID,"view")){ ?>
			<a href="<?=base_url('item/purchase_item') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Purchase Item List</a>
            <?php } ?>
            <?php if($this->applib->have_access_role(MASTER_ITEM_MASTER_MENU_ID,"add")){ ?>
			<a href="<?=base_url('item/add_purchase_item') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Add</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <form method="POST" 
                <?php if(isset($id) && !empty($id)){ ?>
                action="<?=base_url('item/update_purchase_item') ?>" 
                <?php } else { ?>
                action="<?=base_url('item/save_purchase_item') ?>" 
                <?php } ?> id="add_purchase_item_form" enctype="multipart/form-data">
                <input type="hidden" class="form-control input-sm" id="id" name="id" value="<?=isset($id)?$id:''?>">
                <input type="hidden" value="1" name="item_active" id= "item_active" />
                <div class="nav-tabs-custom">
					<?php
						$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
						$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
					?>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab"><?php if(isset($id) && !empty($id)){ echo 'Edit'; } else { echo 'Add'; } ?> Item</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $category_data = $this->crud->get_all_records('item_category','id','ASC'); ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="item_category" class="col-sm-3 input-sm">Item Category<span class="required-sign">*</span></label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm select2" name="item_category" id="item_category">
                                                    <option value=""> -- Select -- </option>
                                                    <?php
                                                    if(!empty($category_data)) {
                                                        foreach ($category_data as $category_row) { ?>
                                                            <option value="<?=$category_row->id ?>" 
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($item_category) && $category_row->id == $item_category) { echo 'selected'; }
                                                                } ?>><?=$category_row->item_category ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="item_name" class="col-sm-3 input-sm">Item Name<span class="required-sign">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="item_name" name="item_name" value="<?=isset($item_name) ? htmlspecialchars($item_name) : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="rate" class="col-sm-3 input-sm" style="padding-right: 0px;">Rate<span class="required-sign">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm pull-left num_only" id="rate" name="rate" value="<?=isset($rate) ? $rate : '' ?>" style="width: 80px;" />
                                                <label for="uom" class="input-sm pull-left" style="width: 100px;"> For 1 UOM <span class="required-sign">*</span></label>
                                                <div class="col-sm-1" style="width: 120px;">
                                                    <select class="input-sm pull-left" name="uom" id="uom"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="qty_2" class="col-sm-3 input-sm" style="padding-right: 0px;">Required Qty<span class="required-sign">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm pull-left num_only" id="qty_2" name="qty_2" value="<?=isset($qty_2) ? $qty_2 : '' ?>" style="width: 80px;" />
                                                <label for="uom_2" class="input-sm pull-left" style="width: 100px;"> Required UOM<span class="required-sign">*</span></label>
                                                <div class="col-sm-1" style="width: 120px;">
                                                    <select class="input-sm pull-left" name="uom_2" id="uom_2"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="reference_qty" class="col-sm-3 input-sm" style="padding-right: 0px;">Relation<span class="required-sign">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm pull-left num_only" id="reference_qty" name="reference_qty" value="<?=isset($reference_qty) ? $reference_qty : '1' ?>" style="width: 80px;" />
                                                <label class="input-sm pull-left" style="width: 100px;"><span id="selected_rate_uom"></span> For 1 <span id="selected_required_uom"></span></label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><div class="form-group">
                                            <label for="sales_rate" class="col-sm-3 input-sm">Sales Rate</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm num_only" id="sales_rate" name="sales_rate" value="<?=isset($sales_rate) ? $sales_rate : '0' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="discount" class="col-sm-3 input-sm">Discount</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm num_only" id="discount" name="discount" value="<?=isset($discount) ? $discount : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="igst" class="col-sm-3 input-sm">IGST</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm num_only" id="igst" name="igst" value="<?=isset($igst) ? $igst : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="cgst" class="col-sm-3 input-sm">CGST</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control input-sm num_only" id="cgst" name="cgst" value="<?=isset($cgst) ? $cgst : '' ?>" />
                                            </div>
                                            <label for="sgst" class="col-sm-1 input-sm">SGST</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control input-sm num_only" id="sgst" name="sgst" value="<?=isset($sgst) ? $sgst : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="item_group" class="col-sm-3 input-sm">Item Group<span class="required-sign">*</span></label>
                                            <div class="col-sm-6" style="margin-right: 1px; padding-right: 0px;">
                                                <select class="form-control input-sm " name="item_group" id="item_group"></select>
                                            </div>
                                            <div class="col-sm-2" style="padding-left: 0px;">
                                                <input type="text" class="form-control input-sm" id="item_group_code" style="padding: 0px !important; margin: 0px !important;" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="item_code" name="item_code" value="<?=isset($item_code)?$item_code:'';?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="supplier_make_id" class="col-sm-3 input-sm">Supplier Make</label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm " name="supplier_make_id" id="supplier_make_id"></select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="item_make_id" class="col-sm-3 input-sm">Item Make</label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm " name="item_make_id" id="item_make_id"></select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="hsn_code" class="col-sm-3 input-sm">HSN Code<span class="required-sign">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="hsn_code" name="hsn_code" value="<?=isset($hsn_code)?$hsn_code:''?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <input type="hidden" class="form-control input-sm" id="cfactor" name="cfactor" value="<?=isset($cfactor)?$cfactor:''; ?>">
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="item_mpt" class="col-sm-3 input-sm">Material Process Type</label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm " name="item_mpt" id="item_mpt"></select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="item_status" class="col-sm-3 input-sm">Status</label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm" name="item_status" id="item_status"></select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="stock" class="col-sm-3 input-sm">Batch Wise Stock ?</label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm select2" name="stock" id="stock">
                                                    <option value="0"
                                                        <?php if(isset($id) && !empty($id)){ 
                                                            if(isset($stock) && $stock == 0) { echo 'selected'; }
                                                        } ?>>Yes</option>
                                                    <option value="1"
                                                        <?php if(isset($id) && !empty($id)){ 
                                                            if(isset($stock) && $stock == 1) { echo 'selected'; }
                                                        } ?>>No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="raw_material_group_id" class="col-sm-3 input-sm">Raw Material Group</label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm " name="raw_material_group_id" id="raw_material_group_id"></select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
                <input type="submit" class="hidden btn-submit-lead-form" />
            </form>
        </div>
        <!-- /.col -->
    </div>
</div>
<script>
    var cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var id_cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var cnt_file_section  = 1;
    var id_cnt_file_section  = 1;
    var ITEM_ID = '<?=isset($id)?$id:0?>';
    $(document).ready(function(){
        $('.select2').select2();
        initAjaxSelect2($("#uom"),"<?=base_url('app/uom_select2_source')?>");
        <?php if(isset($uom)){ ?>
            setSelect2Value($("#uom"),"<?=base_url('app/set_uom_select2_val_by_id/'.$uom)?>");
            display_rate_uom();
        <?php } ?>
            
        initAjaxSelect2($("#reference_qty_uom"),"<?=base_url('app/uom_select2_source')?>");
        <?php if(isset($reference_qty_uom)){ ?>
            setSelect2Value($("#reference_qty_uom"),"<?=base_url('app/set_uom_select2_val_by_id/'.$reference_qty_uom)?>");
        <?php } ?>
            
            initAjaxSelect2($("#uom_2"),"<?=base_url('app/uom_select2_source')?>");
        <?php if(isset($uom_2)){ ?>
            setSelect2Value($("#uom_2"),"<?=base_url('app/set_uom_select2_val_by_id/'.$uom_2)?>");
            display_required_uom();
        <?php } ?>
        
        initAjaxSelect2($("#item_group"),"<?=base_url('app/item_group_select2_source')?>");
        <?php if(isset($item_group)){ ?>
            setSelect2Value($("#item_group"),"<?=base_url('app/set_item_group_select2_val_by_id/'.$item_group)?>");
        <?php } else if(isset($item_group_id)){ ?>
            setSelect2Value($("#item_group"),"<?=base_url('app/set_item_group_select2_val_by_id/'.$item_group_id)?>");
        <?php } ?>
        <?php if(isset($item_group_code)){ ?>
            var item_group_code = '<?php echo $item_group_code; ?>';
            $('#item_group_code').val(item_group_code);
        <?php } ?>
            
        initAjaxSelect2($("#supplier_make_id"),"<?=base_url('app/supplier_make_select2_source')?>");
        <?php if(isset($supplier_make_id)){ ?>
            setSelect2Value($("#supplier_make_id"),"<?=base_url('app/set_supplier_make_select2_val_by_id/'.$supplier_make_id)?>");
        <?php } ?>
            
        initAjaxSelect2($("#item_make_id"),"<?=base_url('app/challan_item_make_id_select2_source')?>");
        <?php if(isset($item_make_id)){ ?>
            setSelect2Value($("#item_make_id"),"<?=base_url('app/set_challan_item_make_id_select2_val_by_id/'.$item_make_id)?>");
        <?php } ?>
        
        initAjaxSelect2($("#item_mpt"),"<?=base_url('app/material_process_type_select2_source')?>");
        <?php if(isset($item_mpt)){ ?>
            setSelect2Value($("#item_mpt"),"<?=base_url('app/set_material_process_type_select2_val_by_id/'.$item_mpt)?>");
        <?php } else { ?>
            setSelect2Value($("#item_mpt"),"<?=base_url('app/set_material_process_type_select2_val_by_id/'.DEFAULT_PURCHASE_ITEM_MATERIAL_PROCESS_TYPE_ID)?>");
        <?php } ?>
            
        initAjaxSelect2($("#item_status"),"<?=base_url('app/item_status_select2_source')?>");
        <?php if(isset($item_status)){ ?>
            setSelect2Value($("#item_status"),"<?=base_url('app/set_item_status_select2_val_by_id/'.$item_status)?>");
        <?php } else { ?>
            setSelect2Value($("#item_status"),"<?=base_url('app/set_item_status_select2_val_by_id/'.DEFAULT_PURCHASE_ITEM_STATUS_ID)?>");
        <?php } ?>
            
        initAjaxSelect2($("#raw_material_group_id"),"<?=base_url('app/raw_material_group_select2_source')?>");
        <?php if(isset($raw_material_group_id)){ ?>
            setSelect2Value($("#raw_material_group_id"),"<?=base_url('app/set_raw_material_group_select2_val_by_id/'.$raw_material_group_id)?>");
        <?php } ?>
            
        $(document).on('change', '#item_group', function(){
            var item_group_id = $("#item_group").val();
            $.ajax({
                url: "<?=base_url('item/get_item_group_code') ?>/"+ item_group_id,
                type: "POST",
                success: function (response) {
                    var json = $.parseJSON(response);
                    $('#item_group_code').val('');
                    $('#item_group_code').val(json['item_group_code']);
                    return false;
                },
            });
        });
        
        $(document).on('change', '#uom', function(){
            display_rate_uom();
        });
        
        $(document).on('change', '#uom_2', function(){
            display_required_uom();
        });
        
        $(document).on('click','.btn-save-item',function(){
            $("#add_purchase_item_form").submit();
        });
        
        $("#add_purchase_item_form").on("submit",function(e){
            e.preventDefault();
            if($('#item_category').val() == "" || $('#item_category').val() == null){
                show_notify('Item Category is required!', false);
                $('#item_category').select2('open');
                return false;
            }
            if($('#item_group').val() == "" || $('#item_group').val() == null){
                show_notify('Item Group is required!', false);
                $('#item_group').select2('open');
                return false;
            }
            if($('#item_name').val() == "" || $('#item_name').val() == null){
                show_notify('Item Name is required!', false);
                $('#item_name').focus();
                return false;
            }
            if($('#rate').val() == "" || $('#rate').val() == null){
                show_notify('Item Rate is required!', false);
                $('#rate').focus();
                return false;
            }
            if($('#uom').val() == "" || $('#uom').val() == null){
                show_notify('Item UOM is required!', false);
                $('#uom').select2('open');
                return false;
            }
            if($('#reference_qty').val() == "" || $('#reference_qty').val() == null){
                show_notify('Relation is required!', false);
                $('#reference_qty').focus();
                return false;
            }
            if($('#hsn_code').val() == "" || $('#hsn_code').val() == null){
                show_notify('HSN Code is required!', false);
                $('#hsn_code').focus();
                return false;
            }
            if($('#qty_2').val() == "" || $('#qty_2').val() == null){
                show_notify('Required Qty is required!', false);
                $('#qty_2').focus();
                return false;
            }
            if($('#uom_2').val() == "" || $('#uom_2').val() == null){
                show_notify('Required UOM is required!', false);
                $('#uom_2').select2('open');
                return false;
            }

            <?php if(isset($id) && !empty($id)){ ?>
                var success_status = check_is_unique('purchase_items','item_name',$("#item_name").val(),'id','<?=$id?>','item_group',$("#item_group").val());
            <?php } else { ?>
                var success_status = check_is_unique('purchase_items','item_name',$("#item_name").val(),'id',$('#id').val(),'item_group',$("#item_group").val());
            <?php } ?>
            if(success_status == 0){
                if($('p.item_name-unique-error').length > 0){
                    $("p.item_name-unique-error").text('Item name already exist!');
                }else{
                    $("#item_name").after("<p class='text-danger item_name-unique-error'>Item name already exist!</p>");
                }
                show_notify('Item Name already exist!',false);
                return false;   
            }else{
                $("p.item_name-unique-error").text(' ');
            }

            <?php if(isset($id) && !empty($id)){ ?>
                if($("#item_code").val() == ''){
                    success_status = 1;
                } else {
                    var success_status = check_is_unique('purchase_items','item_code',$("#item_code").val(),'id','<?=$id?>','item_group',$("#item_group").val());
                }
            <?php } else { ?>
                if($("#item_code").val() == ''){
                    success_status = 1;
                } else {
                    var success_status = check_is_unique('purchase_items','item_code',$("#item_code").val(),'id',$('#id').val(),'item_group',$("#item_group").val());
                }
            <?php } ?>
            if(success_status == 0){
                if($('p.item_code-unique-error').length > 0){
                    $("p.item_code-unique-error").text('Item code already exist!');
                    show_notify('Item code already exist!',false);
                }else{
                    $("#item_code").after("<p class='text-danger item_code-unique-error'>Item code already exist!</p>");
                    show_notify('Item code already exist!',false);
                }
                return false;
            }else{
                $("p.item_code-unique-error").text(' ');
            }

            var item_code = $("#item_code").val();
            var item_name = $("#item_name").val();
//            if(item_code == '') {
//                show_notify('Fill value of item code',false);
//            }else
            if(item_name == ''){
                show_notify('Fill value of item name',false);
            }else{
                var form_data = new FormData(this);
                $("#ajax-loader").show();
                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json',
                    success: function(data){
                        if(data.success == true){
                            window.location.href = "<?php echo base_url('item/purchase_item') ?>";
                            return false;
                            cnt_file_section = 1;
                            id_cnt_file_section = 1;
                            ITEM_ID = data.item_id;
                            $("#id").val(ITEM_ID);
                            show_notify(data.message,true);
                            $('.section_item_file_listing').html(data.item_file_listing);
                            $('#add_purchase_item_form').attr('action','<?=base_url('item/update_purchase_item') ?>');
                            $("#ajax-loader").hide();
                        }
                    }
                });
            }
        });
    });
    
    function display_rate_uom(){
        var rate_uom = $('#uom').select2('data');
        $('#selected_rate_uom').html('');
        if(rate_uom != '' && rate_uom != null){
            rate_uom = rate_uom[0].text;
            $('#selected_rate_uom').html(rate_uom);
        } else {
            $('#selected_rate_uom').html('');
        }
    }
    
    function display_required_uom(){
        var required_uom = $('#uom_2').select2('data');
        $('#selected_required_uom').html('');
        if(required_uom != '' && required_uom != null){
            required_uom = required_uom[0].text;
            $('#selected_required_uom').html(required_uom);
        } else {
            $('#selected_required_uom').html('');
        }
    }
</script>
