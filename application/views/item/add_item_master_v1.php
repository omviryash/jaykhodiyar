
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Master : Item Master</small>
            <button type="button" class="btn btn-info btn-xs pull-right btn-save-item" style="margin: 5px;">
                <?php if(isset($id) && !empty($id)){ ?>Update
                <?php } else { ?>Save
                <?php } ?>
            </button>
            <a href="<?=base_url('item/add_item_master') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Add</a>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">

        </div>
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <form method="POST"
                <?php if(isset($id) && !empty($id)){ ?>
                    action="<?=base_url('item/update_item_master') ?>"
                <?php } else { ?>
                    action="<?=base_url('item/save_item_master') ?>"
                <?php } ?> id="add_company_form" enctype="multipart/form-data">
                <input type="hidden" class="form-control input-sm" id="id" name="id" value="<?=isset($id)?$id:''?>">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">
                                <?php if(isset($id) && !empty($id)){ ?>Edit
                                <?php } else { ?>Add
                                <?php } ?> Item</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Item Quotation Detail</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Item Files</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $category_data = $this->crud->get_all_records('item_category','id','ASC');
                                    $uom_data = $this->crud->get_all_records('uom','id','ASC');
                                    $item_type_data = $this->crud->get_all_records('item_type','id','ASC');
                                    $item_group_data = $this->crud->get_all_records('item_group_code','id','ASC');
                                    $material_data = $this->crud->get_all_records('material_process_type','id','ASC');
                                    $item_status_data = $this->crud->get_all_records('item_status','id','ASC');
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Item Category</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="item_category" id="item_category">
                                                    <?php
                                                    if(!empty($category_data)) {
                                                        foreach ($category_data as $category_row) { ?>
                                                            <option value="<?=$category_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){
                                                                    if(isset($item_category) && $category_row->id == $item_category)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$category_row->item_category ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Item Code</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="item_code1" name="item_code1" value="<?=isset($item_code1)?$item_code1:'';?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">UOM</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="uom" id="uom">
                                                    <option value="">Select UOM</option>
                                                    <?php
                                                    if(!empty($uom_data)) {
                                                        foreach ($uom_data as $uom_row) { ?>
                                                            <option value="<?=$uom_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){
                                                                    if(isset($uom) && $uom_row->id == $uom)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$uom_row->uom ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Conv Qty</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="qty" name="qty" value="<?=isset($qty)?$qty:0?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Item Type</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="item_type" id="item_type">
                                                    <option value="">Select Item Type</option>
                                                    <?php
                                                    if(!empty($item_type_data)) {
                                                        foreach ($item_type_data as $item_type_row) { ?>
                                                            <option value="<?=$item_type_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){
                                                                    if(isset($item_type) && $item_type_row->id == $item_type)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$item_type_row->item_type ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Batch Wise Stock ?</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="stock" id="stock">
                                                    <option value="0"
                                                        <?php if(isset($id) && !empty($id)){
                                                            if(isset($stock) && $stock == 0)
                                                            {
                                                                echo 'selected';
                                                            }
                                                        } ?>>No</option>
                                                    <option value="1"
                                                        <?php if(isset($id) && !empty($id)){
                                                            if(isset($stock) && $stock == 1)
                                                            {
                                                                echo 'selected';
                                                            }
                                                        } ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Upload Document</label>
                                            <div class="col-sm-7">
                                                <input type="file" class="form-control input-sm" id="document" name="document" value="<?=isset($document)?$document:''?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">Item Group</label>
                                            <div class="col-sm-5">
                                                <select class="form-control input-sm select2" name="item_group" id="item_group">
                                                    <?php
                                                    if(!empty($item_group_data)) {
                                                        foreach ($item_group_data as $item_group_row) { ?>
                                                            <option value="<?=$item_group_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){
                                                                    if(isset($item_group) && $item_group_row->id == $item_group)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$item_group_row->ig_code ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3" style="padding-left: 0">
                                                <label>Active? </label> <input type="checkbox" value="1"
                                                    <?php if(isset($item_active) && $item_active != 0)
                                                    {
                                                        echo 'checked="checked"';
                                                    }
                                                    ?> name="item_active"/>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">Item Name</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control input-sm" id="item_name" name="item_name" value="<?=isset($item_name)?$item_name:''; ?>">
                                            </div>
                                            <div class="col-sm-4">
                                                <a class="btn btn-default btn-xs" href="#">Repeat Item (How this work?)</a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">CFactor</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="cfactor" name="cfactor" value="<?=isset($cfactor)?$cfactor:''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">Conv. UOM</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="conv_uom" id="conv_uom">
                                                    <option value="">Select Conv. UOM</option>
                                                    <?php
                                                    if(!empty($uom_data)) {
                                                        foreach ($uom_data as $uom_row) { ?>
                                                            <option value="<?=$uom_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){
                                                                    if(isset($conv_uom) && $uom_row->id == $conv_uom)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$uom_row->uom ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Material Process Type</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control input-sm select2" name="item_mpt" id="item_mpt">
                                                        <option value="">Select Material Process Type</option>
                                                        <?php
                                                        if(!empty($material_data)) {
                                                            foreach ($material_data as $material_row) { ?>
                                                                <option value="<?=$material_row->id ?>"
                                                                    <?php if(isset($id) && !empty($id)){
                                                                        if(isset($item_mpt) && $material_row->id == $item_mpt)
                                                                        {
                                                                            echo 'selected';
                                                                        }
                                                                    } ?>><?=$material_row->material_process_type ?></option>
                                                            <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Status</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control input-sm select2" name="item_status">
                                                        <option value="">Select Status</option>
                                                        <?php
                                                        if(!empty($item_status_data)) {
                                                            foreach ($item_status_data as $item_status_row) { ?>
                                                                <option value="<?=$item_status_row->id ?>"
                                                                    <?php if(isset($id) && !empty($id)){
                                                                        if(isset($item_status) && $item_status_row->id == $item_status)
                                                                        {
                                                                            echo 'selected';
                                                                        }
                                                                    } ?>><?=$item_status_row->item_status ?></option>
                                                            <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Item image</label>
                                                <div class="col-sm-7">
                                                    <input type="file" class="form-control input-sm" id="image" name="image" accept="image/*">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <a data-toggle="modal" data-target="#modal-item-bom" class="btn btn-info btn-xs" href="javascript:void(0);">BOM Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="col-md-12">
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <?php
                                if(isset($quotation_detail_pages) && count($quotation_detail_pages) > 0 ) {
                                    foreach($quotation_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_quotation_detail">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_quotation_detail">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_quotation_detail"
                                                          id="item_quotation_detail_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_quotation_detail">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_quotation_detail" id="item_quotation_detail_1" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_quotation_detail">Add more</button>
                                </div>
                                <div class="col-md-12 section_item_quotation_detail hidden">
                                    <div class="col-md-6">
                                        <h4>Detail page : <span class="cnt-detail-page">2</span></h4>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button type="button" class="btn btn-danger remove_item_quotation_detail">Remove Page <span class="cnt-detail-page">2</span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <textarea class="" id="" style="visibility: hidden; display: none;"></textarea>
                                    <br/>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="container">
                                                <br/>
                                                <div id="fileupload">
                                                    <div class="row fileupload-buttonbar">
                                                        <div class="col-lg-7">
                                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                                            <span class="btn btn-success fileinput-button">
                                                                <i class="glyphicon glyphicon-plus"></i>
                                                                <span>Add files...</span>
                                                                <input type="file" name="item_file"  id="" multiple>
                                                            </span>
                                                            <button type="submit" class="btn btn-primary start">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                                <span>Start upload</span>
                                                            </button>
                                                            <button type="reset" class="btn btn-warning cancel">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                                <span>Cancel upload</span>
                                                            </button>
                                                            <button type="button" class="btn btn-danger delete">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                                <span>Delete</span>
                                                            </button>
                                                            <input type="checkbox" class="toggle">
                                                        </div>
                                                        <div class="col-lg-5 fileupload-progress fade">
                                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                            </div>
                                                            <div class="progress-extended">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <!-- The table listing the files available for upload/download -->
                                                    <table role="presentation" class="table table-striped">
                                                        <tbody class="files">
                                                        <?php
                                                        if(isset($item_files) && count($item_files) > 0) {
                                                            foreach ($item_files as $key => $item_file_row) {
                                                                $key = $key + 1;
                                                                $file_name = basename($item_file_row['file_url'],PATHINFO_FILENAME);
                                                                ?>
                                                                <tr class="template-download fade in">
                                                                    <td></td>
                                                                    <td>
                                                                        <p class="name">
                                                                            <a href="<?=base_url().$item_file_row['file_url']; ?>" title="<?=$file_name?>" download="<?=$file_name?>">
                                                                                <?=$file_name?>
                                                                            </a>
                                                                        </p>
                                                                    </td>
                                                                    <td></td>
                                                                    <td>
                                                                        <button class="btn btn-danger delete" data-type="DELETE" data-url="<?= base_url().'item/remove-item-file/'.$item_file_row['id'];?>">
                                                                            <i class="glyphicon glyphicon-trash"></i>
                                                                            <span>Delete</span>
                                                                        </button>
                                                                        <input type="checkbox" name="delete" value="<?=$item_file_row['id'];?>" class="toggle">
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
                <input type="submit" class="hidden btn-submit-lead-form" />
            </form>
        </div>
        <!-- /.col -->
    </div>
</div>
<div class="modal fade" id="modal-item-bom">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">BOM Details</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <form method="post" id="frm-add-item-bom" class="form-horizontal">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">Select Category</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm select2 select-bom-category-id" name="item_category_id" required="required">
                                            <option value="">select category</option>
                                            <?php
                                            if(count($item_categories) > 0) {
                                                foreach ($item_categories as $category_row) {
                                                    ?>
                                                    <option value="<?=$category_row->id;?>"><?=$category_row->item_category;?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">Select Item</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm select2 select-bom-item-id" name="item_id" required="required">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">Quantity</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="qty" class="form-control input-sm" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <input type="submit" class="btn btn-block btn-success btn-xs" value="Add"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <hr />
                            <div class="col-md-12">
                                <table class="table table-bordered" id="table-item-bom">
                                    <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(isset($item_bom_data) && count($item_bom_data) > 0){
                                        foreach($item_bom_data as $item_bom_row){
                                            ?>
                                            <tr>
                                                <td><?=$item_bom_row->item_category?></td>
                                                <td><?=$item_bom_row->item_name?></td>
                                                <td><?=$item_bom_row->qty?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $(function () {

    });
</script>
<script>
    var cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var id_cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var cnt_file_section  = 1;
    var id_cnt_file_section  = 1;
    var add_item_bom = 0;
    var ITEM_ID = '<?=isset($id)?$id:0?>';
    $(document).ready(function(){

        var UploadObj = $('#fileupload').fileupload({
            url: '<?=base_url()?>item/upload-item-file/',
            formData:{"item_id":ITEM_ID}
        }).bind('fileuploadadd', function (e, data) {
            if(ITEM_ID == 0){
                show_notify("Please save item first!");
                return false;
            }else{
                var that = $(this).data('blueimp-fileupload');
                that.options.url = "<?=base_url()?>item/upload-item-file/"+ITEM_ID;
            }
        });
        //console.log(UploadObj);

        $('#fileupload').addClass('fileupload-processing');

        $('.item_quotation_detail').each(function(){
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g );
            CKEDITOR.replace($(this).attr('id'),{
                extraPlugins: 'lineheight',
                removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
            });
        });

        $(document).on('change','.select-bom-category-id',function(){
            var category_id = $(this).val();
            $.ajax({
                url:"<?=base_url()?>item/get-items-by-category/"+category_id,
                type:"POST",
                dataType:'json',
                success:function(data){
                    var option_html = '';
                    if(data.length > 0){
                        $.each(data,function(index,value){
                            console.log("Index : "+index+" Value : "+value);
                            option_html += "<option value='"+value.id+"'>"+value.item_name+"</option>";
                        })
                    }
                    $('.select-bom-item-id').select2("destroy");
                    $('.select-bom-item-id').html(option_html).select2();
                }
            });
        });
        $(document).on('click','.remove_item_quotation_detail',function(){
            $(this).closest('.section_item_quotation_detail').fadeOut("slow",function(){
                $(this).remove();
                cnt_qtn_section = 0;
                $('.section_item_quotation_detail:not(.hidden)').each(function(){
                    cnt_qtn_section++;
                    $(this).find('.cnt-detail-page').each(function(){
                        $(this).text(cnt_qtn_section);
                    });
                });
            });
        });

        $(document).on('click','.add_item_quotation_detail',function(){
            id_cnt_qtn_section++;
            cnt_qtn_section++;
            var section_item_quotation_detail = $('.section_item_quotation_detail.hidden').clone();
            section_item_quotation_detail.removeClass('hidden');
            section_item_quotation_detail.find('.cnt-detail-page').each(function(){
                $(this).text(cnt_qtn_section);
            });
            section_item_quotation_detail.find('textarea').attr('id','item_quotation_detail_'+id_cnt_qtn_section);
            section_item_quotation_detail.find('textarea').addClass('item_quotation_detail');
            section_item_quotation_detail.insertBefore('.qtn-section-add-btn');
            CKEDITOR.replace(section_item_quotation_detail.find('textarea').attr('id'),{
                extraPlugins: 'lineheight',
                removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
            });
        });

        $(document).on('click','.btn-save-item',function(){
            $("#add_company_form").submit();
        });
        $("#add_company_form").on("submit",function(e){
            e.preventDefault();

            <?php if(isset($id) && !empty($id)){ ?>
            var success_status = check_is_unique('items','item_name',$("#item_name").val(),'id','<?=$id?>');
            <?php } else { ?>
            var success_status = check_is_unique('items','item_name',$("#item_name").val());
            <?php } ?>
            if(success_status == 0){
                if($('p.item_name-unique-error').length > 0){
                    $("p.item_name-unique-error").text('Item name already exist!');
                }else{
                    $("#item_name").after("<p class='text-danger item_name-unique-error'>Item name already exist!</p>");
                }
                return false;
            }else{
                $("p.item_name-unique-error").text(' ');
            }

            <?php if(isset($id) && !empty($id)){ ?>
            var success_status = check_is_unique('items','item_code1',$("#item_code1").val(),'id','<?=$id?>');
            <?php } else { ?>
            var success_status = check_is_unique('items','item_code1',$("#item_code1").val());
            <?php } ?>
            if(success_status == 0){
                if($('p.item_code1-unique-error').length > 0){
                    $("p.item_code1-unique-error").text('Item code already exist!');
                }else{
                    $("#item_code1").after("<p class='text-danger item_code1-unique-error'>Item code already exist!</p>");
                }
                return false;
            }else{
                $("p.item_code1-unique-error").text(' ');
            }

            var item_code1 = $("#item_code1").val();
            var item_name = $("#item_name").val();
            if(item_code1 == '') {
                show_notify('Fill value of item code',false);
            }else if(item_name == ''){
                show_notify('Fill value of item name',false);
            }else{
                var form_data = new FormData(this);
                $('textarea.item_quotation_detail').each(function(){
                    form_data.append('item_quotation_detail[]',CKEDITOR.instances[$(this).attr('id')].getData());
                });
                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json',
                    success: function(data){
                        if(data.success == true){
                            cnt_file_section = 1;
                            id_cnt_file_section = 1;
                            ITEM_ID = data.item_id;
                            $("#id").val(ITEM_ID);
                            show_notify(data.message,true);
                            if(add_item_bom == 1){
                                $("#frm-add-item-bom").submit();
                            }
                            $('.section_item_file_listing').html(data.item_file_listing);
                        }
                    }
                });
            }
        });
        $(document).on('submit','#frm-add-item-bom',function(e){
            e.preventDefault();
            if(ITEM_ID == 0){
                add_item_bom = 1;
                $("#add_company_form").submit();
            }else{
                var bom_item_category = $(".select-bom-category-id > option:selected").text();
                var bom_item = $(".select-bom-item-id > option:selected").text();
                var qty = $(this).find('input[name="qty"]').val();
                $(this).find('input[name="qty"]').val('');
                $.ajax({
                    url: "<?=base_url()?>item/save-item-bom/"+ITEM_ID,
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json',
                    success: function(data){
                        if(data.success == true){
                            $(".select-bom-category-id").val('').trigger('change');
                            add_item_bom = 0;
                            var table_row = "<tr>";
                            table_row += "<td>"+bom_item_category+"</td>";
                            table_row += "<td>"+bom_item+"</td>";
                            table_row += "<td>"+qty+"</td>";
                            table_row += "</tr>";
                            $("table#table-item-bom > tbody").append(table_row);
                            show_notify(data.message,true);
                        }else {
                            show_notify(data.message,false);
                        }
                    }
                });
            }
        });
    });
</script>
