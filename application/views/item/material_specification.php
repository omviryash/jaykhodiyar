<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
           $this->load->view('shared/success_false_notify');
		 ?>
        <h1>
            <small class="text-primary text-bold">Item Material Specification</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Item Material Specification Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table item-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Material</th>
						                                            <th>Material Specification</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php
                                            if($this->applib->have_access_role(MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID,"add") || $this->applib->have_access_role(MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID,"edit")) {
                                            ?>
                                            
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
														<?php } else {
                                                        if($this->applib->have_access_role(MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID,"add")) {
                                                            $btn_disable = null;
                                                        }else{
                                                            $btn_disable = 'disabled';
                                                            
                                                        }
                                                        ?>Add 
														<?php } ?> Material
													</div>
													<div style="margin:10px">	
														<form method="POST"
														<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('item/update_material_specification') ?>" 
															<?php } else { ?>
															action="<?=base_url('item/add_material_specification') ?>" 
															<?php } ?>
															id="form_item">
															<div class="form-group">
																<?php if(isset($id) && !empty($id)){ ?>
																	<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
																<?php } ?>
																<label for="inputEmail3" class="col-sm-4 input-sm">Material<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="material" name="material" value="<?php echo $material;?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">Material Specification<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <textarea class="form-control" rows="1" name="material_specification" id="material_specification" <?php echo $btn_disable;?>><?php echo $material_specification;?></textarea>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group" style="margin:7px !important;"></div>
															<?php 
                                                            if(isset($id) && !empty($id)){ 
                                                            ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Material</button>
                                                            <?php 
                                                            } else {
                                                            ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Material</button>
                                                            <?php
                                                            }
                                                            ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    var table;
	$(document).ready(function(){
		table = $('#example1').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('item/material_specification_datatable')?>",
				"type": "POST"
			},
			"scrollY": 300,
			"scroller": {
				"loadingIndicator": true
			}
		});

        $("#form_item").on("submit",function(e){
			e.preventDefault();

			if($("#material").val() == ""){
				show_notify('Fill value Material.', false);
				return false;
			}
			if($("#material_specification").val() == ""){
				show_notify('Fill value Material Specification.', false);
				return false;
			}
			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('material_specification','material',$("#material").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('material_specification','material',$("#material").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.material-unique-error').length > 0){
					$("p.material-unique-error").text('Material already exist!');
				}else{
					$("#material").after("<p class='text-danger material-unique-error'>Material already exist!</p>");
				}
				return false;
			}else{
				$("p.material-unique-error").text('');
			}

			var url = '<?php echo base_url('item/delete/') ?>';
			var value = $("#material").val();
			var material_specification = $("#material_specification").val();
			if(value != '' && material_specification != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="#" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.material+'</td>';
		                TableRow += '<td>'+data.material_specification+'</td>';
		                TableRow += '</tr>';
		                $('.item-table > tbody > tr:last ').after(TableRow);
		                $("#form_item")[0].reset();*/
		               // show_notify('Saved Successfully!',true);
		                window.location.href = "<?php echo base_url('item/material_specification') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=material_specification',
					success: function(data){
						tr.remove();
						//show_notify('Deleted Successfully!',true);
						window.location.href = "<?php echo base_url('item/material_specification') ?>";
					}
				});
			}
		});
    });
</script>
