<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
		<h1>
			<small class="text-primary text-bold">Item Group category</small>
		</h1>
		<!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-7">
										<div class="panel panel-default">
											<div class="panel-heading">
												Item Group
											</div>
											<div style="margin: 10px;">
												<table id="example1" class="table custom-table item-table">
													<thead>
														<tr>
															<th>Action</th>
															<th>Item Group</th>
															<th>Item Group Code</th>
															<th>Item Group Description</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php
									if($this->applib->have_access_role(MASTER_ITEM_MAIN_GROUP_MENU_ID,"add") || $this->applib->have_access_role(MASTER_ITEM_MAIN_GROUP_MENU_ID,"edit")) {
									?>
									<div class="col-md-5">
										<div class="panel panel-default">
											<div class="panel-heading clearfix">
												<?php if(isset($id) && !empty($id)){ ?>Edit 
												<?php } else { 
											if($this->applib->have_access_role(MASTER_ITEM_MAIN_GROUP_MENU_ID,"add")) {
												$btn_disable = null;
											}else{
												$btn_disable = 'disabled';
											}
												?>Add 
												<?php } ?> Item Group
											</div>
											<div style="margin:10px">	
												<form method="POST"
													  <?php if(isset($id) && !empty($id)){ ?>
													  action="<?=base_url('item/update_main_group') ?>" 
													  <?php } else { ?>
													  action="<?=base_url('item/add_main_group') ?>" 
													  <?php } ?>	
													  id="form_item">
													<div class="form-group">
														<?php if(isset($id) && !empty($id)){ ?>
														<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
														<?php } ?>
														<label for="inputEmail3" class="col-sm-5 input-sm">Item Group Code<span class="required-sign">*</span></label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="code" name="code" value="<?php echo $code?>" <?php echo $btn_disable;?>>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-5 input-sm">Item Group<span class="required-sign">*</span></label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="item_group" name="item_group" value="<?php echo $item_group?>" <?php echo $btn_disable;?>>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-5 input-sm">Item Group Description<span class="required-sign">*</span></label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="description" name="description" value="<?php echo $description?>" <?php echo $btn_disable;?>>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="form-group" style="margin:7px !important;"></div>
													<?php 
													if(isset($id) && !empty($id)){ 
													?>
													<button type="submit" class="btn btn-info btn-block btn-xs">Edit Item Group</button>
													<?php 
													} else {
													?>
													<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Item Group</button>
													<?php
													}
													?>
												</form>
											</div>
										</div>                                            
									</div>
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>

<script>
	var table;
	$(document).ready(function(){
		table = $('#example1').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('item/main_group_datatables')?>",
				"type": "POST"
			},
			"scrollY": 300,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});

		$("#form_item").on("submit",function(e){
			e.preventDefault();

			if($("#code").val() == ""){
				show_notify('Fill value Item Group Code.', false);
				return false;
			}
			if($("#item_group").val() == ""){
				show_notify('Fill value Item Group.', false);
				return false;
			}
			if($("#description").val() == ""){
				show_notify('Fill value Item Group Description.', false);
				return false;
			}
			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('main_group','item_group',$("#item_group").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('main_group','item_group',$("#item_group").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.item_group-unique-error').length > 0){
					$("p.item_group-unique-error").text('Group code already exist!');
				}else{
					$("#item_group").after("<p class='text-danger item_group-unique-error'>Group code already exist!</p>");
				}
				return false;
			}else{
				$("p.item_group-unique-error").text('');
			}

			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('main_group','code',$("#code").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('main_group','code',$("#code").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.code-unique-error').length > 0){
					$("p.code-unique-error").text('Code already exist!');
				}else{
					$("#code").after("<p class='text-danger code-unique-error'>Code already exist!</p>");
				}
				return false;
			}else{
				$("p.code-unique-error").text('');
			}

			var url = '<?php echo base_url('item/delete/') ?>';
			var value = $("#code").val();
			var item_group = $("#item_group").val();
			var description = $("#description").val();
			if(value != '' && item_group != '' && description != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="#" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.item_group+'</td>';
		                TableRow += '<td>'+data.code+'</td>';
		                TableRow += '<td>'+data.description+'</td>';
		                TableRow += '</tr>';
		                $('.item-table > tbody > tr:last ').after(TableRow);
		                $("#form_item")[0].reset();*/
						// show_notify('Saved Successfully!',true);
						window.location.href = "<?php echo base_url('item/main_group') ?>";
					}
				});
			}
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=main_group',
					success: function(data){
						tr.remove();
						//show_notify('Deleted Successfully!',true);
						window.location.href = "<?php echo base_url('item/main_group') ?>";
					}
				});
			}
		});

	});
</script>
