<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
		<h1>
			<small class="text-primary text-bold">Item Group Code</small>
		</h1>
		<!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-7">
										<div class="panel panel-default">
											<div class="panel-heading">
												Item Group Code
											</div>
											<div style="margin: 10px;">
												<table id="example1" class="table custom-table item-table">
													<thead>
														<tr>
															<th>Action</th>
															<th>Item Group Code</th>
															<th>Item Group Description</th>
														</tr>
													</thead>
													<tbody>

													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php
									if($this->applib->have_access_role(MASTER_ITEM_GROUP_CODE_MENU_ID,"add") || $this->applib->have_access_role(MASTER_ITEM_GROUP_CODE_MENU_ID,"edit")) {
									?>
									<div class="col-md-5">
											<div class="panel panel-default">
												<div class="panel-heading clearfix">
													<?php if(isset($id) && !empty($id)){ ?>Edit
													<?php } else {
														if($this->applib->have_access_role(MASTER_ITEM_GROUP_CODE_MENU_ID,"add")) {
															$btn_disable = null;
														}else{
															$btn_disable = 'disabled';
														}
													?>Add
													<?php } ?>  Item Group Code
												</div>
												<div style="margin:10px">
													<form method="POST"
														<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('item/update_item_group_codes') ?>"
														<?php } else { ?>
															action="<?=base_url('item/add_item_group_code') ?>"
														<?php } ?>
														  id="form_item">
														<div class="form-group">
															<?php if(isset($id) && !empty($id)){ ?>
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
															<?php } ?>
															<label for="inputEmail3" class="col-sm-5 input-sm">Item Group Code<span class="required-sign">*</span></label>
															<div class="col-sm-7">
																<input type="text" class="form-control input-sm" id="ig_code" name="ig_code" <?php echo $btn_disable;?> value="<?php echo $ig_code; ?>">
															</div>
														</div>
														<div class="clearfix"></div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-5 input-sm">Item Group Description<span class="required-sign">*</span></label>
															<div class="col-sm-7">
																<textarea class="form-control" rows="1" name="ig_description" <?php echo $btn_disable;?> id="ig_description"><?php echo $ig_description; ?></textarea>
															</div>
														</div>
														<div class="clearfix"></div>
														<div class="form-group" style="margin:7px !important;"></div>
														
															<?php 
															if(isset($id) && !empty($id)){ 
															?>
															<button type="submit" class="btn btn-info btn-block btn-xs">Edit Item Group</button>
															<?php 
															} else {
																
															?>
															<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Item Group</button>
															<?php
															} 
															?> 
														
													</form>
												</div>
											</div>
										</div>
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>

<script>
	var table;
	$(document).ready(function(){
		table = $('.item-table').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('item/item_group_datatable')?>",
				"type": "POST"
			},
			"scrollY": 300,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});

		$("#form_item").on("submit",function(e){
			e.preventDefault();

			if($("#ig_code").val() == ""){
				show_notify('Fill value Item Group Code.', false);
				return false;
			}
			if($("#ig_description").val() == ""){
				show_notify('Fill value Item Group Description.', false);
				return false;
			}
			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('item_group_code','ig_code',$("#ig_code").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('item_group_code','ig_code',$("#ig_code").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.ig_code-unique-error').length > 0){
					$("p.ig_code-unique-error").text('Item group code already exist!');
				}else{
					$("#ig_code").after("<p class='text-danger ig_code-unique-error'>Item group code already exist!</p>");
				}
				return false;
			}else{
				$("p.ig_code-unique-error").text(' ');
			}

			var url = '<?php echo base_url('item/delete/') ?>';
			var value = $("#ig_code").val();
			var ig_description = $("#ig_description").val();
			//if(value != '' && ig_description != '')
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="#" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.ig_code+'</td>';
		                TableRow += '<td>'+data.ig_description+'</td>';
		                TableRow += '</tr>';
		                $('.item-table > tbody > tr:last ').after(TableRow);
		                $("#form_item")[0].reset();*/
						//show_notify('Saved Successfully!',true);
						window.location.href = "<?php echo base_url('item/item_group_code') ?>";
					}
				});
			}
			/*else
			{
				if($('p.ig_description-unique-error').length > 0){
					$("p.ig_description-unique-error").text('Please enter description!');
				}else{
					$("#ig_description").after("<p class='text-danger ig_code-unique-error'>Please enter description!</p>");
				}
				return false;
			}*/
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=item_group_code',
					success: function(data){
						tr.remove();
						//show_notify('Deleted Successfully!',true);
						window.location.href = "<?php echo base_url('item/item_group_code') ?>";

					}
				});
			}
		});

	});
</script>
