<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php $this->load->view('shared/success_false_notify'); ?>
		<h1><small class="text-primary text-bold">Supplier Make</small></h1>
	</section>
	<div class="clearfix">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-7">
										<div class="panel panel-default">
											<div class="panel-heading">
												Supplier Make
											</div>
											<div style="margin: 10px;">
												<table id="example1" class="table custom-table item-table">
													<thead>
														<tr>
															<th>Action</th>
															<th>Supplier Make Name</th>
														</tr>
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
									<?php if($this->applib->have_access_role(MASTER_SUPPLIER_MAKE_MENU_ID,"add") || $this->applib->have_access_role(MASTER_SUPPLIER_MAKE_MENU_ID,"edit")) { ?>
									<div class="col-md-5">
											<div class="panel panel-default">
												<div class="panel-heading clearfix">
													<?php if(isset($id) && !empty($id)){ ?>Edit
													<?php } else {
														if($this->applib->have_access_role(MASTER_SUPPLIER_MAKE_MENU_ID,"add")) {
															$btn_disable = null;
														}else{
															$btn_disable = 'disabled';
														}
													?>Add
													<?php } ?>  Supplier Make
												</div>
												<div style="margin:10px">
													<form method="POST"
														<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('item/update_supplier_make') ?>"
														<?php } else { ?>
															action="<?=base_url('item/add_supplier_make') ?>"
														<?php } ?>
														  id="form_item">
														<div class="form-group">
															<?php if(isset($id) && !empty($id)){ ?>
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
															<?php } ?>
															<label for="supplier_make_name" class="col-sm-5 input-sm">Supplier Make Name<span class="required-sign">*</span></label>
															<div class="col-sm-7">
																<input type="text" class="form-control input-sm" id="supplier_make_name" name="supplier_make_name" <?php echo $btn_disable;?> value="<?php echo $supplier_make_name; ?>">
															</div>
														</div>
														<div class="clearfix"></div>
														<div class="form-group" style="margin:7px !important;"></div>
															<?php if(isset($id) && !empty($id)){ ?>
                                                                <button type="submit" class="btn btn-info btn-block btn-xs">Edit Supplier Make</button>
															<?php  } else { ?>
															<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Supplier Make</button>
															<?php }  ?> 
													</form>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>
<script>
	var table;
	$(document).ready(function(){
		table = $('.item-table').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('item/supplier_make_datatable')?>",
				"type": "POST"
			},
			"scrollY": 300,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});

		$("#form_item").on("submit",function(e){
			e.preventDefault();

			if($("#supplier_make_name").val() == ""){
				show_notify('Fill value Supplier Make Name.', false);
				return false;
			}
			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('supplier_make','supplier_make_name',$("#supplier_make_name").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('supplier_make','supplier_make_name',$("#supplier_make_name").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.supplier_make_name-unique-error').length > 0){
					$("p.supplier_make_name-unique-error").text('Supplier Make already exist!');
				}else{
					$("#supplier_make_name").after("<p class='text-danger ig_code-unique-error'>Supplier Make already exist!</p>");
				}
				return false;
			}else{
				$("p.supplier_make_name-unique-error").text(' ');
			}

			var url = '<?php echo base_url('item/delete/') ?>';
			var value = $("#supplier_make_name").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="#" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.ig_code+'</td>';
		                TableRow += '<td>'+data.ig_description+'</td>';
		                TableRow += '</tr>';
		                $('.item-table > tbody > tr:last ').after(TableRow);
		                $("#form_item")[0].reset();*/
						//show_notify('Saved Successfully!',true);
						window.location.href = "<?php echo base_url('item/supplier_make') ?>";
					}
				});
			}
			/*else
			{
				if($('p.ig_description-unique-error').length > 0){
					$("p.ig_description-unique-error").text('Please enter description!');
				}else{
					$("#ig_description").after("<p class='text-danger ig_code-unique-error'>Please enter description!</p>");
				}
				return false;
			}*/
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=supplier_make',
					success: function(data){
						tr.remove();
						//show_notify('Deleted Successfully!',true);
						window.location.href = "<?php echo base_url('item/supplier_make') ?>";

					}
				});
			}
		});

	});
</script>
