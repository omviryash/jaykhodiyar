<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <div id="ajax-loader-content" style="display: none;">
        <div style="width:100%; height:100%; left:230px; top:50px; position:fixed; opacity:0; filter:alpha(opacity=40); background:#000000;z-index:110;">
        </div>
        <div style="float:left;width:100%; left:230px; top:45%; text-align:center; position:fixed; padding:0px; z-index:110;">
            <img src="<?php echo BASE_URL; ?>/resource/image/loader.gif" width="150" height="150">
        </div>
    </div>
    <form method="POST" action="<?= base_url('item/save_purchase_project') ?>" id="add_purchase_project_form" enctype="multipart/form-data">
         <?php if (isset($purchase_project->project_id) && !empty($purchase_project->project_id)) { ?>
            <input type="hidden" name="purchase_project[project_id]" class="project_id" value="<?= $purchase_project->project_id ?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Master : Purchase Project</small>
                <button type="submit" class="btn btn-info btn-xs pull-right btn-save-item module_save_btn" style="margin: 5px;">
                    <?php if (isset($project_id) && !empty($project_id)) { ?>Update 
                    <?php } else { ?>Save 
                    <?php } ?>
                </button>
                <?php if ($this->applib->have_access_role(MASTER_ITEM_MENU_ID, "view")) { ?>
                    <a href="<?= base_url('item/purchase_project') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Purchase Project List</a>
                <?php } ?>
                <?php if ($this->applib->have_access_role(MASTER_ITEM_MASTER_MENU_ID, "add")) { ?>
                    <a href="<?= base_url('item/add_purchase_project') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Add</a>
                <?php } ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">
                            <?php
                                if (isset($project_id) && !empty($project_id)) {
                                    echo 'Edit';
                                } else {
                                    echo 'Add';
                                }
                            ?>
                            Project</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="project_name" class="col-sm-3 ">Project Name<span class="required-sign">&nbsp;*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control input-sm" id="project_name" name="purchase_project[project_name]" value="<?= isset($purchase_project->project_name) ? $purchase_project->project_name : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <legend class="scheduler-border text-primary text-bold">Item Details</legend>
                                            <label for="item_group_id" class="col-sm-2 input-sm">Select Item Group<span class="required-sign">&nbsp;*</span></label>
                                            <div class="col-sm-3">
                                                <select name="line_items_data[item_group_id]" id="item_group_id" class="form-control input-sm item_group_id select2"></select>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" id="ig_code" class="form-control input-sm" value="<?php if(isset($purchase_project->ig_description)){ echo $purchase_project->ig_description; } ?>" readonly="readonly" />
                                            </div>
                                            <div class="clearfix"></div>
                                            <label for="item_make_id" class="col-sm-2 input-sm">Item Make</label>
                                            <div class="col-sm-4">
                                                <select name="line_items_data[item_make_id][]" id="item_make_id" multiple="multiple" class="form-control input-sm item_make_id"></select>
                                            </div>
                                            <label for="raw_material_group_id" class="col-sm-2 input-sm">Raw Material Group</label>
                                            <div class="col-sm-4">
                                                <select name="line_items_data[raw_material_group_id][]" id="raw_material_group_id" multiple="multiple" class="form-control input-sm raw_material_group_id"></select>
                                            </div>
                                            <div class="clearfix"></div>
                                            <label for="supplier_make_id" class="col-sm-2 input-sm">Supplier Make</label>
                                            <div class="col-sm-4">
                                                <select name="line_items_data[supplier_make_id][]" id="supplier_make_id" multiple="multiple" class="form-control input-sm supplier_make_id"></select>
                                            </div>
                                        <div class="clearfix"></div><br />
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="padding: 0px 2px;">
                                                <div class="form-group text-center">
                                                    <label for="motor_user" class="input-sm"> Select Item</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group text-center">
                                                    <label for="motor_user" class="input-sm"> Item Code</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group text-center">
                                                    <label for="motor_make" class="input-sm"> Required Qty </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="padding: 0px 2px;">
                                                <div class="form-group text-center">
                                                    <label for="motor_hp" class="input-sm"> Required UOM </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="item_detail_fields">
                                            <div id="item_fields">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="line_items_data[ppi_id][]" id="ppi_id" class="ppi_id" >
                                                    <div class="col-md-3" style="padding: 0px 2px;">
                                                        <div class="form-group">
                                                            <select name="line_items_data[item_id][]" class="motor_user form-control input-sm item_data" id="item_id" >
                                                                <option value=""> - Select - </option>
                                                                <?php foreach ($purchase_items as $purchase_items) { ?>
                                                                    <option value="<?= $purchase_items->id; ?>" data-item_code="<?php echo $purchase_items->item_code; ?>"> <?= $purchase_items->item_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="padding: 0px 2px;">
                                                        <div class="form-group">
                                                            <input name="item_code" class="motor_user form-control input-sm item_code" id="item_code" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="padding: 0px 2px;">
                                                        <div class="form-group">
                                                            <input type="text" name="line_items_data[qty][]" id="qty" class="form-control input-sm num_only qty"  data-name="qty">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding: 0px 2px;">
                                                        <div class="form-group">
                                                            <select name="line_items_data[uom][]" class="motor_make form-control input-sm select2 uom" id="uom" >
                                                                <option value=""> - Select - </option>
                                                                <?php foreach ($uom as $uoms) { ?>
                                                                    <option value="<?= $uoms->id; ?>" ><?= $uoms->uom; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button type="button" class="btn btn-danger btn-xs pull-center remove_item_detail" style="display: none;"> <i class="fa fa-close"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <a href="javascript:void(0);" class="btn btn-info btn-xs pull-right add_motor_detail" id="add_motor_detail"> <i class="fa fa-plus"></i></a>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
//        $('#item_id').select2();
            
            initAjaxSelect2($("#item_group_id"),"<?=base_url('app/item_group_select2_source')?>");
        <?php if(isset($purchase_project) && !empty($purchase_project->item_group_id)) { ?>
           setSelect2Value($("#item_group_id"),"<?=base_url('app/set_item_group_select2_val_by_id/'.$purchase_project->item_group_id)?>");
        <?php } ?>
            
            initAjaxSelect2($("#item_make_id"),"<?=base_url('app/challan_item_make_id_select2_source')?>");
        <?php if(isset($purchase_project->item_make_id)){ ?>
            setSelect2MultiValue($("#item_make_id"),"<?=base_url('app/set_challan_item_make_id_select2_multi_val_by_id/'.$purchase_project->item_make_id)?>");
//        <?php } ?>
//            
            initAjaxSelect2($("#raw_material_group_id"),"<?=base_url('app/raw_material_group_select2_source')?>");
        <?php if(isset($purchase_project->raw_material_group_id)){ ?>
            setSelect2MultiValue($("#raw_material_group_id"),"<?=base_url('app/set_raw_material_group_select2_multi_val_by_id/'.$purchase_project->raw_material_group_id)?>");
//        <?php } ?>

            initAjaxSelect2($("#supplier_make_id"),"<?=base_url('app/supplier_make_select2_source')?>");
        <?php if(isset($purchase_project->supplier_make_id)){ ?>
            setSelect2MultiValue($("#supplier_make_id"),"<?=base_url('app/set_supplier_make_select2_multi_val_by_id/'.$purchase_project->supplier_make_id)?>");
//      <?php } ?>
//        
       <?php if (isset($purchase_project->project_id) && !empty($purchase_project->project_id)) { ?>
            var purchase_project_lineitems = <?php echo $group_wise_items; ?>;
            var row_increment = 1;
            var option = '';
            option = '<option value=""> - Select - </option>'
            $.each(purchase_project_lineitems, function (key, value) {
                    option += '<option value="' + value.item_id + '" data-item_code="'+ value.item_code +'">' + value.item_name + '</option>';
            });
            $('.item_data').html(option);
            row_increment++;
            $('#item_group_id').prop('disabled', true);
            $('#item_make_id').prop('disabled', true);
            $('#raw_material_group_id').prop('disabled', true);
            $('#supplier_make_id').prop('disabled', true);
            var purchase_project_lineitems = <?php echo $purchase_project_lineitems; ?>;
            //console.log(purchase_project_lineitems);
            $('#item_detail_fields #item_fields').not('#item_fields:first').remove();
            var item_data_rows = Object.keys(purchase_project_lineitems).length;
            var row_inc = 1;
            $.each(purchase_project_lineitems, function (cust_data_i, cust_data_v) {
                
                $.each(cust_data_v, function (index, value){
                
                    if (value == null) {
                        value = '';
                    }
                    if(index == 'uom_id')
                    {
                       $(document).find('.uom:last').val(value);
                    }                    
                    $('#item_detail_fields #item_fields:nth-last-child(1) #' + index).val(value);
                });
                if (row_inc < item_data_rows) {
                    add_motor_detail();
                }
                row_inc++;
            });
            $(document).find('.item_data').select2();
        <?php } ?>
            
        $(document).on('change', '.item_data', function (e) {
            var item_code = $(this).children("option:selected").data('item_code');
            $(this).closest('#item_fields').find('.item_code').val(item_code);
        });
        
            
        $(document).on('change', '#item_group_id', function () {
            $("#ajax-loader-content").show();
            var item_group_id = $('#item_group_id').val();
            $.ajax({
                url: "<?= base_url('item/get_items_group_code') ?>/" + item_group_id,
                type: "GET",
//                dataType: 'json',
                async: false,
                cache: false,
                success: function (response) {
                    var json = $.parseJSON(response);
                    $('#ig_code').val(json.ig_description);
                }
            });
            Display_lineitems();
        });
        
        $(document).on('change', '#item_make_id, #raw_material_group_id, #supplier_make_id', function () {
            <?php if (isset($purchase_project->project_id) && !empty($purchase_project->project_id)) { } else { ?>
                $("#ajax-loader-content").show();
                Display_lineitems();
            <?php } ?>
        });

        $(document).on('input', '#qty', function () {
            get_project_reference_qty();
        });

        $(document).on('submit', '#add_purchase_project_form', function () {

            if ($.trim($("#project_name").val()) == '') {
                show_notify('Please Enter Project Name!', false);
                $('#project_name').focus();
                return false;
            }
            if ($.trim($("#item_group_id").val()) == '') {
                show_notify('Please Select Item Group!', false);
                $('#item_group_id').select2('open');
                return false;
            }

           var reqlength = $('.qty').length;
           var item_uom_check = 0;
           var item_check_i = 0;
           var uom_check_i = 0;
           var value = $('.qty').filter(function () {
               var item_id = $(this).closest("#item_fields").find('#item_id').val();
               var uom = $(this).closest("#item_fields").find('#uom').val();
               if(this.value != "" && (item_id == "" && uom == "")){
                  item_uom_check++;
               }else if(this.value != "" && item_id == ""){
                  item_check_i++;
               }else if(this.value != "" &&  uom == ""){
                  uom_check_i++;
               }
               return this.value != '';
            });
            
            if(item_uom_check > 0){
                show_notify('Please select Item and UOM!', false);
                return false;
            }else if(item_check_i > 0){
                show_notify('Please select Item!', false);
                return false;
            }else if(uom_check_i > 0) {
                show_notify('Please select UOM!', false);
                return false;
            }
            
            if (value.length == 0 && (value.length !== reqlength)) {
                show_notify('Please Enter Qty for atleast One item!', false);
                return false;
            }

            $('.module_save_btn').attr('disabled', 'disabled');
            $('.overlay').show();
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('item/save_purchase_project') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                   
                    var json = $.parseJSON(response);
                    if (json.status == 0) {
                        show_notify(json.msg, false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                        return false;
                    }
                    if (json['success'] == 'false') {
                        show_notify(json['msg'], true);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                    }
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('item/purchase_project') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('item/purchase_project') ?>";
                    }
                    $('.overlay').hide();
                    return false;
                },
            });
            return false;
        });

    });

    function get_project_reference_qty() {
        var qty = $('#qty').val();
        var reference_qty = $('#reference_qty').val();
        if (qty != '' && qty != null && reference_qty != '' && reference_qty != null) {
            var project_reference_qty = parseInt(qty) * parseInt(reference_qty);
            $('#project_reference_qty').val(project_reference_qty);
        }
    }

    function get_item_group(id) {
        
        $.ajax({
            url: "<?= base_url('app/set_item_group_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if (response.text == '--select--') {
                    response.text = '';
                }
                $('#item_group').val(response.id);
            },
        });
    }

    function get_uom_name(id) {
        $.ajax({
            url: "<?= base_url('app/set_uom_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                
                alert(response);
                if (response.text == '--select--') {
                    response.text = '';
                }
                $('#uom_name').val(response.text);
            },
        });
    }

    function get_ref_uom_name(id) {
        $.ajax({
            url: "<?= base_url('app/set_uom_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if (response.text == '--select--') {
                    response.text = '';
                }
                $('#project_reference_qty_uom_name').val(response.text);
            },
        });
    }
    
    function Display_lineitems(){
        var item_group_id = $('#item_group_id').val();
        var item_make_id = $('#item_make_id').val();
        var raw_material_group_id = $('#raw_material_group_id').val();
        var supplier_make_id = $('#supplier_make_id').val();
        $.ajax({
            url: "<?= base_url('item/get_items_uom_select_from_item_group') ?>/",
            type: "POST",
            data: {item_group_id : item_group_id, item_make_id : item_make_id, raw_material_group_id : raw_material_group_id, supplier_make_id : supplier_make_id},
            success: function (response) {
                $("#ajax-loader-content").hide();
                var json = $.parseJSON(response);
                if(json != ''){
                }
                $('#item_detail_fields #item_fields').not('#item_fields:first').remove();
                $('#item_detail_fields #item_fields').find('.item_code:first').val('');
                $('#item_detail_fields #item_fields').find('.qty:first').val('');
                $('#item_detail_fields #item_fields').find('.uom:first').val('');
                var motor_data_rows = Object.keys(json).length;
                var row_inc = 1;
                var row_increment = 1;
                var option = '';
                option = '<option value=""> - Select - </option>'
                $.each(json, function (key, value) {
                        option += '<option value="' + value.item_id + '" data-item_code="'+ value.item_code +'">' + value.item_name + '</option>';
                });
                $('#item_id').html(option);
                row_increment++;
                $.each(json, function (item_data_i, item_data_v) {
                    $(document).find('.item_data:last').val(item_data_v.item_id);
                    $(document).find('.item_code:last').val(item_data_v.item_code);
                    $(document).find('.qty:last').val(item_data_v.qty_id);
                    $(document).find('.uom:last').val(item_data_v.uom_id);
                    //$('#item_detail_fields #item_fields:nth-last-child(1) #').val(value);
                    if (row_inc < motor_data_rows) {

                        $('.add_motor_detail').click();
                    }
                    row_inc++;
                });
                $(document).find('.item_data').select2();
            },
        });
    }

        $(document).on('click', '.add_motor_detail', function () {
            add_motor_detail()
        });
        
        $(document).on('click', '.remove_item_detail', function () {
            var ppi_id =  $(this).closest("#item_fields").find('#ppi_id').val();
           
            $('#add_purchase_project_form').append('<input type="hidden" name="deleted_ppi_id[]" id="deleted_ppi_id" value="' + ppi_id + '" />');
            $(this).closest("#item_fields").remove();
        });
        $(document).find('.item_data').select2();
        function add_motor_detail() {
            $(document).find(".item_data").select2("destroy");
            $("#item_fields").clone().appendTo("#item_detail_fields");
            $(document).find('.item_data').select2();
            $(".remove_item_detail:last").css('display', 'block');
            $(".ppi_id:last").val('');
            $(".qty:last ").val('');
            $(".item_code:last ").val('');
        }
</script>
