<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><small class="text-primary text-bold">Purchase Project</small></h1>
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Purchase Project Detail
                                        <?php if($this->applib->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID,"add")) { ?>
                                            <a href="<?=base_url('item/add_purchase_project') ?>" class="btn btn-info btn-xs pull-right">Add Project</a>
                                        <?php } ?>
                                    </div>
                                    <div style="margin: 10px;">
                                        <table id="example1" class="table display custom-table" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th width="100px">Action</th>
                                                    <th>Project Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		table = $('#example1').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('item/purchase_project_datatable')?>",
                "type": "POST"
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
        });
	});
</script>
