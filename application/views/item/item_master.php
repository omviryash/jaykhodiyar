<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
		<h1>
			<small class="text-primary text-bold">Item Master</small>
		</h1>
		<!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												Item Master Detail
												<?php
												if($this->applib->have_access_role(MASTER_ITEM_MASTER_MENU_ID,"add")) {
													?>
														<a href="<?=base_url('item/add_item_master') ?>" class="btn btn-info btn-xs pull-right">Add Item</a>
													<?php
												}
												?>
											</div>
											<div style="margin: 10px;">
												<table id="example1" class="table display custom-table" cellspacing="0" width="100%">
													<thead>
														<tr>
															<th>Action</th>
															<th>Item Name</th>
															<th>Item Code</th>
															<th>Item For</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>

<script>
	var table;
	$(document).ready(function(){
		
		table = $('#example1').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('item/item_master_datatable')?>",
                "type": "POST"
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
        });
		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=items',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('item/item_master') ?>";
					}
				});
			}
		});

	});
</script>
