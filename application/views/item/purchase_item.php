<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small class="text-primary text-bold">Purchase Item</small>
            <?php if($this->applib->have_access_role(MASTER_PURCHASE_ITEM_MENU_ID,"add")) {?>
                <a href="<?=base_url('item/add_purchase_item') ?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;" >Add Purchase Item</a>
            <?php } ?>
		</h1>
	</section>
	<div class="clearfix">
		<div class="col-md-12">
            <form id="import_purchase_item" class="" action="<?=base_url('item/import_purchase_item') ?>" method="post" enctype="multipart/form-data" data-parsley-validate="">
                <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="import_file" class="control-label">Import Purchase Items .CSV File</label>
                                    <input type="file" name="import_file" id="import_file" required />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>&nbsp;</label><br />
                                <button type="submit" name="action" value="export"  class="btn btn-primary btn-sm">Import Purchase Items</button>
                            </div>
                            <div class="col-md-6">
                                <label>&nbsp;</label><br />
                                <a href="<?=base_url('item/download_demo_for_import_purchase_item') ?>" class="btn btn-primary btn-sm pull-right" style="margin: 5px;" target="_blank" >Download Demo For Import Purchase Item</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					
                    <div class="form-group">
                        <label for="item_group" class="col-md-1">Item Group</label>
                        <div class="col-md-3" style="margin-right: 1px; padding-right: 0px;">
                            <select class="form-control input-sm " name="item_group" id="item_group"></select>
                        </div>
                    </div>
                    <table id="purchase_item_list" class="table display custom-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th width="80px">
                                    Action <br />
                                    <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                                    <button type="button" class="btn btn-default btn-sm delete_checked_purchase_items"><i class="fa fa-trash"></i></button>
                                </th>
                                <th>Item Category</th>
                                <th>Item Group</th>
                                <th>Item Name</th>
                                <th>Item Code</th>
                                <th>Supplier Make</th>
                                <th>Item Make</th>
                                <th>Raw Material Group</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
	</div>

</div>

<script>
	var table;
	$(document).ready(function(){
		initAjaxSelect2($("#item_group"),"<?=base_url('app/item_group_select2_source')?>");
		table = $('#purchase_item_list').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('item/purchase_item_datatable')?>",
                "type": "POST",
                "data":function(d){
                    d.item_group = $("#item_group").val();
                }
            },
            "scrollY": 550,
            "scroller": {
                "loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%",
            "columnDefs": [ { "targets": 0, "orderable": false } ]
        });
        
        $(document).on('change',"#item_group",function() {
            table.draw();
            $(".checkbox-toggle i").removeClass("fa-check-square-o").addClass('fa-square-o');
        });
        
        $(document).on('click',".checkbox-toggle",function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$("#purchase_item_list input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$("#purchase_item_list input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks",!clicks);
		});
        
		$(document).on("click",".delete_checked_purchase_items",function(){
            var checked_purchase_item_ids = [];
            $.each($("#purchase_item_list input[type='checkbox']:checked"), function(){
                checked_purchase_item_ids.push($(this).val());
            });
            if(checked_purchase_item_ids != ''){
                var value = confirm('Are you sure delete selected records?');
                if(value){
    				$.ajax({
    					url: "<?php echo site_url('item/delete_checked_purchase_items')?>",
    					type: "POST",
    					data: {checked_purchase_item_ids: checked_purchase_item_ids},
    					success: function(data){
                            table.draw();
                            $(".checkbox-toggle i").removeClass("fa-check-square-o").addClass('fa-square-o');
//    						window.location.href = "<?php echo base_url('item/purchase_item') ?>";
    					}
    				});
                    return false;
                }
            } else {
                show_notify('Please select Item!', false);
                return false;
            }
        });
		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=purchase_items',
					success: function(response){
                        var json = $.parseJSON(response);
                        if (json['error'] == 'Error') {
                            show_notify('You cannot delete this Item. This Item has been used.', false);
                        } else if (json['success'] == 'Deleted') {
                            table.draw();
                            show_notify('Purchase Item Deleted Successfully!', true);
                        }
                        return false;
					}
				});
			}
		});

	});
</script>
