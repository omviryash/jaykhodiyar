<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
           $this->load->view('shared/success_false_notify');
		 ?>
        <h1>
            <small class="text-primary text-bold">Excise</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Excise Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table item-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Excise Code</th>
						                                            <th>Excise Description</th>
						                                            <th>Sub Heading</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php
                                            if($this->applib->have_access_role(MASTER_ITEM_EXCISE_MENU_ID,"add") || $this->applib->have_access_role(MASTER_ITEM_EXCISE_MENU_ID,"edit")) {
                                            ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
														<?php } else {
                                                        if($this->applib->have_access_role(MASTER_ITEM_EXCISE_MENU_ID,"add")) {
                                                            $btn_disable = null;
                                                        }else{
                                                            $btn_disable = 'disabled';
                                                        }
                                                        ?>Add 
														<?php } ?> Excise Temp
													</div>
													<div style="margin:10px">	
														<form method="POST"
														<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('item/update_excise') ?>" 
															<?php } else { ?>
															action="<?=base_url('item/add_excise') ?>" 
															<?php } ?>
														   id="form_item">
															<div class="form-group">
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
																<label for="inputEmail3" class="col-sm-4 input-sm">Excise Description<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="description" name="description" value="<?php echo $description; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">																
																<label for="inputEmail3" class="col-sm-4 input-sm">Sub Heading<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="sub_heading" name="sub_heading" value="<?php echo $sub_heading; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group" style="margin:7px !important;"></div>
															
                                                            <?php 
                                                            if(isset($id) && !empty($id)){ 
                                                            ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Excise</button>
                                                            <?php 
                                                            } else {
                                                            ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Excise</button>
                                                            <?php
                                                            }
                                                            ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    var table;
	$(document).ready(function(){
		table = $('.item-table').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('item/excise_datatable')?>",
				"type": "POST"
			},
			"scrollY": 300,
			"scroller": {
				"loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
		});

        $("#form_item").on("submit",function(e){
			e.preventDefault();
			if($("#description").val() == ""){
				show_notify('Fill value Item Description.', false);
				return false;
			}
			if($("#sub_heading").val() == ""){
				show_notify('Fill value Sub Heading.', false);
				return false;
			}
			var url = '<?php echo base_url('item/delete/') ?>';
			var value = $("#description").val();
			var sub_heading = $("#sub_heading").val();
			if(value != '' && sub_heading != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="#" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.id+'</td>';
		                TableRow += '<td>'+data.description+'</td>';
		                TableRow += '<td>'+data.sub_heading+'</td>';
		                TableRow += '</tr>';
		                $('.item-table > tbody > tr:last ').after(TableRow);
		                $("#form_item")[0].reset();*/
		               // show_notify('Saved Successfully!',true);
		                window.location.href = "<?php echo base_url('item/excise') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=excise',
					success: function(data){
						tr.remove();
						//show_notify('Deleted Successfully!',true);
						    window.location.href = "<?php echo base_url('item/excise') ?>";
					}
				});
			}
		});
		
    });
</script>
