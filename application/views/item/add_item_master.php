
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Master : Item Master</small>
            <button type="button" class="btn btn-info btn-xs pull-right btn-save-item" style="margin: 5px;">
                <?php if(isset($id) && !empty($id)){ ?>Update 
                <?php } else { ?>Save 
                <?php } ?>
            </button>
            <?php if($this->applib->have_access_role(MASTER_ITEM_MENU_ID,"add")){ ?>
			<a href="<?=base_url('item/add_item_master') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Add</a>
            <?php } ?>
            <?php if($this->applib->have_access_role(MASTER_ITEM_MENU_ID,"view")){ ?>
			<a href="<?=base_url('item/item_master') ?>" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Item Master List</a>
            <?php } ?>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">

        </div>
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <form method="POST" 
                <?php if(isset($id) && !empty($id)){ ?>
                action="<?=base_url('item/update_item_master') ?>" 
                <?php } else { ?>
                action="<?=base_url('item/save_item_master') ?>" 
                <?php } ?> id="add_company_form" enctype="multipart/form-data">
                <input type="hidden" class="form-control input-sm" id="id" name="id" value="<?=isset($id)?$id:''?>">
                <div class="nav-tabs-custom">
					<?php
						$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
						$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
					?>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">
                            <?php if(isset($id) && !empty($id)){ ?>Edit 
                            <?php } else { ?>Add 
                            <?php } ?> Item</a></li>
                        <?php if($cu_accessExport == 1 && $cu_accessDomestic == 1){ ?>
							<li><a href="#tab_domestic_quotation" data-toggle="tab">Domestic Quotation</a></li>
							<li><a href="#tab_export_quotation" data-toggle="tab">Export Quotation</a></li>
							<!--<li><a href="#tab_domestic_proforma_invoice" data-toggle="tab">Domestic Proforma Invoice</a></li>-->
							<!--<li><a href="#tab_export_proforma_invoice" data-toggle="tab">Export Proforma Invoice</a></li>-->
							<!--<li><a href="#tab_domestic_order" data-toggle="tab">Domestic Order</a></li>-->
							<!--<li><a href="#tab_export_order" data-toggle="tab">Export Order</a></li>-->
							<!--<li><a href="#tab_domestic_dispatch" data-toggle="tab">Domestic Dispatch</a></li>-->
							<!--<li><a href="#tab_export_dispatch" data-toggle="tab">Export Dispatch</a></li>-->
							<!--<li><a href="#tab_domestic_invoice" data-toggle="tab">Domestic Invoice</a></li>-->
							<!--<li><a href="#tab_export_invoice" data-toggle="tab">Export Invoice</a></li>-->
						<?php } else if($cu_accessExport == 1){ ?>
							<li><a href="#tab_export_quotation" data-toggle="tab">Export Quotation</a></li>
							<!--<li><a href="#tab_export_proforma_invoice" data-toggle="tab">Export Proforma Invoice</a></li>-->
							<!--<li><a href="#tab_export_order" data-toggle="tab">Export Order</a></li>-->
							<!--<li><a href="#tab_export_dispatch" data-toggle="tab">Export Dispatch</a></li>-->
							<!--<li><a href="#tab_export_invoice" data-toggle="tab">Export Invoice</a></li>-->
						<?php } else if($cu_accessDomestic == 1){ ?>
							<li><a href="#tab_domestic_quotation" data-toggle="tab">Domestic Quotation</a></li>
							<!--<li><a href="#tab_domestic_proforma_invoice" data-toggle="tab">Domestic Proforma Invoice</a></li>-->
							<!--<li><a href="#tab_domestic_order" data-toggle="tab">Domestic Order</a></li>-->
							<!--<li><a href="#tab_domestic_dispatch" data-toggle="tab">Domestic Dispatch</a></li>-->
							<!--<li><a href="#tab_domestic_invoice" data-toggle="tab">Domestic Invoice</a></li>-->
						<?php } else {}	?>
                        <li><a href="#tab_domestic_item_files" data-toggle="tab">Domestic Quot. Items</a></li>
                        <li><a href="#tab_export_item_files" data-toggle="tab">Export Quot. Items</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                                <h4 style="text-align: center;">
                                    <b>Item Details</b>
                                </h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                        $category_data = $this->crud->get_all_records('item_category','id','ASC');
                                        $uom_data = $this->crud->get_all_records('uom','id','ASC');
                                        $item_type_data = $this->crud->get_all_records('item_type','id','ASC');
                                        $item_group_data = $this->crud->get_all_records('item_group_code','id','ASC');
                                        $material_data = $this->crud->get_all_records('material_process_type','id','ASC');
                                        $item_status_data = $this->crud->get_all_records('item_status','id','ASC');
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Item Category</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="item_category" id="item_category">
                                                    <?php
                                                    if(!empty($category_data)) {
                                                        foreach ($category_data as $category_row) { ?>
                                                            <option value="<?=$category_row->id ?>" 
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($item_category) && $category_row->id == $item_category)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$category_row->item_category ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Item Code<span class="required-sign">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="item_code1" name="item_code1" value="<?=isset($item_code1)?$item_code1:'';?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">UOM</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="uom" id="uom">
                                                    <option value="">Select UOM</option>
                                                    <?php
                                                    if(!empty($uom_data)) {
                                                        foreach ($uom_data as $uom_row) { ?>
                                                            <option value="<?=$uom_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($uom) && $uom_row->id == $uom)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$uom_row->uom ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Conv Qty</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="qty" name="qty" value="<?=isset($qty)?$qty:0?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Item Type</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="item_type" id="item_type">
                                                    <option value="">Select Item Type</option>
                                                    <?php
                                                    if(!empty($item_type_data)) {
                                                        foreach ($item_type_data as $item_type_row) { ?>
                                                            <option value="<?=$item_type_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($item_type) && $item_type_row->id == $item_type)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$item_type_row->item_type ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Batch Wise Stock ?</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="stock" id="stock">
                                                    <option value="0"
                                                        <?php if(isset($id) && !empty($id)){ 
                                                            if(isset($stock) && $stock == 0)
                                                            {
                                                                echo 'selected';
                                                            }
                                                        } ?>>No</option>
                                                    <option value="1"
                                                        <?php if(isset($id) && !empty($id)){ 
                                                            if(isset($stock) && $stock == 1)
                                                            {
                                                                echo 'selected';
                                                            }
                                                        } ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Upload Document</label>
                                            <div class="col-sm-7">
                                                <input type="file" class="form-control input-sm" id="document" name="document" value="<?=isset($document)?$document:''?>">
                                            </div>
                                        </div>
                                            <?php if(isset($id) && $document != ""){?>
                                            <div class="clearfix"></div>
                                                <div class="col-sm-6">
                                                    <a href="<? echo base_url('resource/uploads/document/'.$document); ?>" download class="btn btn-default input-sm btn-xs" style=" margin-left: 55%"><i class="fa fa-download"></i> Download Current Document</a>
                                                </div>
                                            <?php } ?>
										<div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">HSN Code</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="hsn_code" name="hsn_code" value="<?=isset($hsn_code)?$hsn_code:''?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">IGST %</label>
                                            <div class="col-sm-7">
                                                <input type="number" class="form-control input-sm" id="igst" name="igst" value="<?=isset($igst) ? $igst : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">CGST %</label>
                                            <div class="col-sm-7">
                                                <input type="number" class="form-control input-sm" id="cgst" name="cgst" value="<?=isset($cgst) ? $cgst : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">SGST %</label>
                                            <div class="col-sm-7">
                                                <input type="number" class="form-control input-sm" id="sgst" name="sgst" value="<?=isset($sgst) ? $sgst : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Item For</label>
                                            <div class="col-md-6">
                                                <label for="item_for_domestic" class="col-sm-4 input-sm" style="margin-left: 0px; padding-left: 0px;">Domestic</label>
                                                    <input type="checkbox" name="item_for_domestic" id="item_for_domestic" class="col-sm-1" style="margin-left: 0px; padding-left: 0px;" value="1"  <?php if(isset($item_for_domestic) && $item_for_domestic == 1){ echo 'checked="checked"'; } else {} ?>>
                                                <label for="item_for_export" class="col-sm-3 input-sm">Export</label>
                                                    <input type="checkbox" name="item_for_export" id="item_for_export" class="col-sm-2" value="1" <?php if(isset($item_for_export) && $item_for_export == 1){ echo 'checked="checked"'; } else {} ?>>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">Item Group</label>
                                            <div class="col-sm-5">
                                                <select class="form-control input-sm select2" name="item_group" id="item_group">
                                                    <?php
                                                    if(!empty($item_group_data)) {
                                                        foreach ($item_group_data as $item_group_row) { ?>
                                                            <option value="<?=$item_group_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($item_group) && $item_group_row->id == $item_group)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$item_group_row->ig_code ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3" style="padding-left: 0">
                                                <label>Active? </label> <input type="checkbox" value="1" 
                                                        <?php if(isset($item_active) && $item_active != 0)
                                                            {
                                                                echo 'checked="checked"';
                                                            }

                                                            if(isset($item_active)){

															} else {
																echo 'checked';
															}
                                                        ?> name="item_active"/>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">Item Name<span class="required-sign">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="item_name" name="item_name" value="<?=isset($item_name)?$item_name:''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">CFactor</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="cfactor" name="cfactor" value="<?=isset($cfactor)?$cfactor:''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 input-sm">Conv. UOM</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="conv_uom" id="conv_uom">
                                                    <option value="">Select Conv. UOM</option>
                                                    <?php
                                                    if(!empty($uom_data)) {
                                                        foreach ($uom_data as $uom_row) { ?>
                                                            <option value="<?=$uom_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($conv_uom) && $uom_row->id == $conv_uom)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$uom_row->uom ?></option>
                                                        <?php } } ?>
                                                </select>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Material Process Type</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control input-sm select2" name="item_mpt" id="item_mpt">
                                                        <option value="">Select Material Process Type</option>
                                                        <?php
                                                        if(!empty($material_data)) {
                                                            foreach ($material_data as $material_row) { ?>
                                                                <option value="<?=$material_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($item_mpt) && $material_row->id == $item_mpt)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$material_row->material_process_type ?></option>
                                                            <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Status</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control input-sm select2" name="item_status">
                                                        <option value="">Select Status</option>
                                                        <?php
                                                        if(!empty($item_status_data)) {
                                                            foreach ($item_status_data as $item_status_row) { ?>
                                                                <option value="<?=$item_status_row->id ?>"
                                                                <?php if(isset($id) && !empty($id)){ 
                                                                    if(isset($item_status) && $item_status_row->id == $item_status)
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                } ?>><?=$item_status_row->item_status ?></option>
                                                            <?php } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-4 input-sm">Rate</label>
                                                <div class="col-sm-7">
                                                    <input type="number" class="form-control input-sm" id="rate" name="rate" value="<?=isset($rate) ? $rate : '' ?>" />
                                                </div>
                                            </div>
                                            <?php
                                            if(!empty($rates_in_val)){
                                                foreach ($rates_in_val as $rate_in_val) {
                                                    if(!empty($rate_in_val['currency'])){
                                            ?>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-4 input-sm">Rate in <?=$rate_in_val['currency']?></label>
                                                <div class="col-sm-7">
                                                    <input type="number" class="form-control input-sm" id="rate_<?=$rate_in_val['id'];?>" name="rate_in[<?=$rate_in_val['id'];?>]" value="<?=isset($rate_in_val['value']) ? $rate_in_val['value'] : '' ?>" />
                                                </div>
                                            </div>
                                            <?php        
                                                    } }
                                            }elseif(!empty($rates_in)){
                                                foreach ($rates_in as $rate_in) {                                                    
                                            ?>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-4 input-sm">Rate in <?=$rate_in->currency?></label>
                                                <div class="col-sm-7">
                                                    <input type="number" class="form-control input-sm" id="rate_<?=$rate_in->id;?>" name="rate_in[<?=strtolower($rate_in->id);?>]" value="" />
                                                </div>
                                            </div>
                                            <?php
                                                }
                                            }
                                            ?>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Item image</label>
                                                <div class="col-sm-7">
                                                    <input type="file" class="form-control input-sm" id="image" name="image" accept="image/*">
                                                </div>
                                            </div>
                                            <?php if(isset($id) && $image != ""){?>
                                            <div class="clearfix"></div>
                                                <div class="col-sm-3">
                                                    <img src="<? echo base_url('resource/uploads/images/items/'.$image); ?>" class="" id="image" name="image" accept="image/*" style="width: 100; margin-left: 170%" height="100" width="100">
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
<!--                                        <div class="col-md-4">
                                            <a data-toggle="modal" data-target="#modal-item-bom" class="btn btn-info btn-xs" href="javascript:void(0);">BOM Details</a>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_domestic_quotation">
							<div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Domestic Quotation</b>
                                </h4>
                                <br/>
								<table border="1" width="100%">
									<tbody>
										<tr>
											<td width="150px">&nbsp; For : Price Table</td>
											<td>&nbsp; Add this : <b>{{Price_Table}}</b> or <b>{{Price_Table_With_FreeErectionLine}}</b> </td>
										</tr>
                                        <tr>
                                            <td>&nbsp; For : To Variables</td>
                                            <td>&nbsp; Add this : {{To_Party_Name}}, {{To_Address}}, {{To_City}}, {{To_Pin}}, {{To_State}}, {{To_Country}}, {{To_Tel_No}}, {{To_Contact_No}}, {{To_Email}}, {{To_Contact_Person}}, {{GST_No}}, {{CIN_No}}</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp; For : From Variables</td>
                                            <td>&nbsp; Add this : {{From_Company_Name}}, {{From_Contact_Person}}, {{From_City}}, {{From_State}}, {{From_Country}}, {{From_Tel_No}}, {{From_Email}}, {{Quotation_Date}}, {{From_Ref_No}}, {{Reference}}</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp; For : Sales Executive</td>
                                            <td>&nbsp; Add this : {{Sales_Executeve}}, {{Sales_Person_No}}, {{Sales_Person_Email}}</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp; For : From Item Name</td>
                                            <td>&nbsp; Add this : {{Item_Name}}</td>
                                        </tr>
									</tbody>
								</table>
							</div>
                            <div class="row section_domestic_quotation">
                                <?php
                                    if(isset($domestic_quotation_detail_pages) && count($domestic_quotation_detail_pages) > 0 ) {
                                        $page_inc = 0;
                                        foreach($domestic_quotation_detail_pages as $page_key=>$page_detail) {
                                            $page_key = $page_key + 1;
                                            ?>
                                            <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="domestic_quotation">
                                                <div class="col-md-6">
                                                    <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span>
                                                        <label class="input-sm domestic_allow_to_edit"  style="font-size: 16px; margin-left: 15px;"> 
                                                            <input type="checkbox" id="domestic_allow_to_edit" value="<?= $page_key ?>"
                                                                <?php if(isset($domestic_allow_to_edit) && $domestic_allow_to_edit[$page_inc] == 1){ echo 'checked="checked"'; } ?>
                                                                name="domestic_allow_to_edit[]"/> &nbsp;Allow To Edit
                                                        </label>
                                                    </h4>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <?php
                                                    if ($page_key != 1) {
                                                        echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="domestic_quotation">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                    }
                                                    ?>
                                                </div>
                                                <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="domestic_quotation_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="domestic_quotation"><?= $page_detail ?></textarea>
                                                <br/>
                                            </div>
                                            <?php
                                            $page_inc++;
                                        }
                                    } else {
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="domestic_quotation">
                                            <div class="col-md-6">
                                                <h4>Detail page : 1
                                                <label class="input-sm domestic_allow_to_edit"  style="font-size: 16px; margin-left: 15px;"> 
                                                    <input type="checkbox" id="domestic_allow_to_edit" value="1"
                                                        name="domestic_allow_to_edit[]"/> &nbsp;Allow To Edit
                                                </label>
                                                </h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                
                                            </div>
                                            <div class="clearfix"></div>
                                            <textarea class="item_document" id="domestic_quotation_item_document_1"  data-document_type="domestic_quotation" style="visibility: hidden; display: none;"></textarea>
                                            <br/>
                                        </div>
                                        <?php

                                    }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="domestic_quotation">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_export_quotation">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Export Quotation</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td width="150px">&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> or <b>{{Price_Table_With_FreeErectionLine}}</b> </td>
                                    </tr>
                                    <tr>
										<td>&nbsp; For : To Variables</td>
										<td>&nbsp; Add this : {{To_Party_Name}}, {{To_Address}}, {{To_City}}, {{To_Pin}}, {{To_State}}, {{To_Country}}, {{To_Tel_No}}, {{To_Contact_No}}, {{To_Email}}, {{To_Contact_Person}}, {{GST_No}}, {{CIN_No}}</td>
									</tr>
									<tr>
										<td>&nbsp; For : From Variables</td>
										<td>&nbsp; Add this : {{From_Company_Name}}, {{From_Contact_Person}}, {{From_City}}, {{From_State}}, {{From_Country}}, {{From_Tel_No}}, {{From_Email}}, {{Quotation_Date}}, {{From_Ref_No}}, {{Reference}}</td>
									</tr>
									<tr>
										<td>&nbsp; For : Sales Executive</td>
										<td>&nbsp; Add this : {{Sales_Executeve}}, {{Sales_Person_No}}, {{Sales_Person_Email}}</td>
									</tr>
                                    <tr>
                                        <td>&nbsp; For : From Item Name</td>
                                        <td>&nbsp; Add this : {{Item_Name}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_export_quotation">
                                <?php
                                if(isset($export_quotation_detail_pages) && count($export_quotation_detail_pages) > 0 ) {
                                    $page_inc = 0;
                                    foreach($export_quotation_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="export_quotation">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span>
                                                <label class="input-sm export_allow_to_edit"  style="font-size: 16px; margin-left: 15px;"> 
                                                    <input type="checkbox" id="export_allow_to_edit" value="<?= $page_key ?>"
                                                        <?php if(isset($export_allow_to_edit) && $export_allow_to_edit[$page_inc] == 1){ echo 'checked="checked"'; } ?>
                                                        name="export_allow_to_edit[]"/> &nbsp;Allow To Edit
                                                </label>
                                                </h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="export_quotation">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="export_quotation_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="export_quotation"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                        $page_inc++;
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="export_quotation">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1
                                            <label class="input-sm export_allow_to_edit"  style="font-size: 16px; margin-left: 15px;"> 
                                                    <input type="checkbox" id="domestic_allow_to_edit" value="1"
                                                        name="export_allow_to_edit[]"/> &nbsp;Allow To Edit
                                                </label>
                                            </h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="export_quotation_item_document_1"  data-document_type="export_quotation" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="export_quotation">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_domestic_proforma_invoice">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Domestic Proforma Invoice</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_domestic_proforma_invoice">
                                <?php
                                if(isset($domestic_proforma_invoice_detail_pages) && count($domestic_proforma_invoice_detail_pages) > 0 ) {
                                    foreach($domestic_proforma_invoice_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="domestic_proforma_invoice">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="domestic_proforma_invoice">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="domestic_proforma_invoice_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="domestic_proforma_invoice"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="domestic_proforma_invoice">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="domestic_proforma_invoice_item_document_1"  data-document_type="domestic_proforma_invoice" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="domestic_proforma_invoice">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_export_proforma_invoice">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Export Proforma Invoice</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_export_proforma_invoice">
                                <?php
                                if(isset($export_proforma_invoice_detail_pages) && count($export_proforma_invoice_detail_pages) > 0 ) {
                                    foreach($export_proforma_invoice_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="export_proforma_invoice">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="export_proforma_invoice">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="export_proforma_invoice_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="export_proforma_invoice"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="export_proforma_invoice">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="export_proforma_invoice_item_document_1"  data-document_type="export_proforma_invoice" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="export_proforma_invoice">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_domestic_order">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Domestic Order</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_domestic_order">
                                <?php
                                if(isset($domestic_order_detail_pages) && count($domestic_order_detail_pages) > 0 ) {
                                    foreach($domestic_order_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="domestic_order">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="domestic_order">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="domestic_order_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="domestic_order"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="domestic_order">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="domestic_order_item_document_1"  data-document_type="domestic_order" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="domestic_order">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_export_order">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Export Order</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_export_order">
                                <?php
                                if(isset($export_order_detail_pages) && count($export_order_detail_pages) > 0 ) {
                                    foreach($export_order_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="export_order">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="export_order">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="export_order_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="export_order"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="export_order">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="export_order_item_document_1"  data-document_type="export_order" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="export_order">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_domestic_dispatch">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Domestic Dispatch</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_domestic_dispatch">
                                <?php
                                if(isset($domestic_dispatch_detail_pages) && count($domestic_dispatch_detail_pages) > 0 ) {
                                    foreach($domestic_dispatch_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="domestic_dispatch">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="domestic_dispatch">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="domestic_dispatch_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="domestic_dispatch"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="domestic_dispatch">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="domestic_dispatch_item_document_1"  data-document_type="domestic_dispatch" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="domestic_dispatch">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_export_dispatch">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Export Dispatch</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_export_dispatch">
                                <?php
                                if(isset($export_dispatch_detail_pages) && count($export_dispatch_detail_pages) > 0 ) {
                                    foreach($export_dispatch_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="export_dispatch">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="export_dispatch">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="export_dispatch_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="export_dispatch"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="export_dispatch">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="export_dispatch_item_document_1"  data-document_type="export_dispatch" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="export_dispatch">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_domestic_invoice">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Domestic Invoice</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_domestic_invoice">
                                <?php
                                if(isset($domestic_invoice_detail_pages) && count($domestic_invoice_detail_pages) > 0 ) {
                                    foreach($domestic_invoice_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="domestic_invoice">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="domestic_invoice">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="domestic_invoice_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="domestic_invoice"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="domestic_invoice">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="domestic_invoice_item_document_1"  data-document_type="domestic_invoice" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="domestic_invoice">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_export_invoice">
                            <div class="col-md-12">
                                <h4 style="text-align: center;">
                                    <b>Export Invoice</b>
                                </h4>
                                <br/>
                                <table border="1" width="100%">
                                    <tbody>
                                    <tr>
                                        <td>&nbsp; For : Price Table</td>
                                        <td>&nbsp; Add this : <b>{{Price_Table}}</b> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row section_export_invoice">
                                <?php
                                if(isset($export_invoice_detail_pages) && count($export_invoice_detail_pages) > 0 ) {
                                    foreach($export_invoice_detail_pages as $page_key=>$page_detail) {
                                        $page_key = $page_key + 1;
                                        ?>
                                        <div class="col-md-12 section_item_document" data-section_no="<?=$page_key?>" data-document_type="export_invoice">
                                            <div class="col-md-6">
                                                <h4>Detail page : <span class="cnt-detail-page"><?= $page_key ?></span></h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?php
                                                if ($page_key != 1) {
                                                    echo '<button type="button" class="btn btn-danger remove_item_document" data-document_type="export_invoice">Remove Page <span class="cnt-detail-page">' . $page_key . '</span></button>';
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                                <textarea class="item_document"
                                                          id="export_invoice_item_document_<?= $page_key ?>"
                                                          style="visibility: hidden; display: none;" data-document_type="export_invoice"><?= $page_detail ?></textarea>
                                            <br/>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-12 section_item_document" data-section_no="1" data-document_type="export_invoice">
                                        <div class="col-md-6">
                                            <h4>Detail page : 1</h4>
                                        </div>
                                        <div class="col-md-6 text-right">

                                        </div>
                                        <div class="clearfix"></div>
                                        <textarea class="item_document" id="export_invoice_item_document_1"  data-document_type="export_invoice" style="visibility: hidden; display: none;"></textarea>
                                        <br/>
                                    </div>
                                    <?php

                                }
                                ?>
                                <div class="col-md-12 text-right qtn-section-add-btn">
                                    <button type="button" class="btn btn-primary add_item_document" data-document_type="export_invoice">Add more</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_domestic_item_files">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="container">
                                                <br/>
                                                <div id="domestic_fileupload">
                                                    <div class="row fileupload-buttonbar">
                                                        <div class="col-lg-7">
                                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                                            <span class="btn btn-success fileinput-button">
                                                                <i class="glyphicon glyphicon-plus"></i>
                                                                <span>Add files...</span>
                                                                <input type="file" name="item_file"  id="" multiple>
                                                            </span>
                                                            <button type="submit" class="btn btn-primary start">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                                <span>Start upload</span>
                                                            </button>
                                                            <button type="reset" class="btn btn-warning cancel">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                                <span>Cancel upload</span>
                                                            </button>
                                                            <button type="button" class="btn btn-danger delete">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                                <span>Delete</span>
                                                            </button>
                                                            <input type="checkbox" class="toggle">
                                                        </div>
                                                        <div class="col-lg-5 fileupload-progress fade">
                                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                            </div>
                                                            <div class="progress-extended">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <!-- The table listing the files available for upload/download -->
                                                    <table role="presentation" class="table table-striped">
                                                        <tbody class="files">
                                                        <?php
                                                        if(isset($domestic_item_files) && count($domestic_item_files) > 0) {
                                                            foreach ($domestic_item_files as $key => $item_file_row) {
                                                                $key = $key + 1;
                                                                $file_name = basename($item_file_row['file_url'],PATHINFO_FILENAME);
                                                                ?>
                                                                <tr class="template-download fade in">
                                                                    <td></td>
                                                                    <td>
                                                                        <p class="name">
                                                                            <a href="<?=base_url().$item_file_row['file_url']; ?>" title="<?=$file_name?>" download="<?=$file_name?>">
                                                                                <?=$file_name?>
                                                                            </a>
                                                                        </p>
                                                                    </td>
                                                                    <td></td>
                                                                    <td>
                                                                        <button class="btn btn-danger delete" data-type="DELETE" data-url="<?= base_url().'item/remove-item-file/'.$item_file_row['id'];?>">
                                                                            <i class="glyphicon glyphicon-trash"></i>
                                                                            <span>Delete</span>
                                                                        </button>
                                                                        <input type="checkbox" name="delete" value="<?=$item_file_row['id'];?>" class="toggle">
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

						<div class="tab-pane" id="tab_export_item_files">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="container">
                                                <br/>
                                                <div id="export_fileupload">
                                                    <div class="row fileupload-buttonbar">
                                                        <div class="col-lg-7">
                                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                                            <span class="btn btn-success fileinput-button">
                                                                <i class="glyphicon glyphicon-plus"></i>
                                                                <span>Add files...</span>
                                                                <input type="file" name="item_file"  id="" multiple>
                                                            </span>
                                                            <button type="submit" class="btn btn-primary start">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                                <span>Start upload</span>
                                                            </button>
                                                            <button type="reset" class="btn btn-warning cancel">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                                <span>Cancel upload</span>
                                                            </button>
                                                            <button type="button" class="btn btn-danger delete">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                                <span>Delete</span>
                                                            </button>
                                                            <input type="checkbox" class="toggle">
                                                        </div>
                                                        <div class="col-lg-5 fileupload-progress fade">
                                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                            </div>
                                                            <div class="progress-extended">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <!-- The table listing the files available for upload/download -->
                                                    <table role="presentation" class="table table-striped">
                                                        <tbody class="files">
                                                        <?php
                                                        if(isset($export_item_files) && count($export_item_files) > 0) {
                                                            foreach ($export_item_files as $key => $item_file_row) {
                                                                $key = $key + 1;
                                                                $file_name = basename($item_file_row['file_url'],PATHINFO_FILENAME);
                                                                ?>
                                                                <tr class="template-download fade in">
                                                                    <td></td>
                                                                    <td>
                                                                        <p class="name">
                                                                            <a href="<?=base_url().$item_file_row['file_url']; ?>" title="<?=$file_name?>" download="<?=$file_name?>">
                                                                                <?=$file_name?>
                                                                            </a>
                                                                        </p>
                                                                    </td>
                                                                    <td></td>
                                                                    <td>
                                                                        <button class="btn btn-danger delete" data-type="DELETE" data-url="<?= base_url().'item/remove-item-file/'.$item_file_row['id'];?>">
                                                                            <i class="glyphicon glyphicon-trash"></i>
                                                                            <span>Delete</span>
                                                                        </button>
                                                                        <input type="checkbox" name="delete" value="<?=$item_file_row['id'];?>" class="toggle">
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 section_item_document hidden" data-section_no="" data-document_type="">
                            <div class="col-md-6">
                                <h4>Detail page : <span class="cnt-detail-page">2</span>
                                    <label class="input-sm allow_to_edit"  style="font-size: 16px; margin-left: 15px;"> 
                                        <input type="checkbox"  /> &nbsp;Allow To Edit
                                    </label>
                                </h4>
                            </div>
                            
                            <div class="col-md-6 text-right">
                                <button type="button" class="btn btn-danger remove_item_document" data-document_type="">Remove Page <span class="cnt-detail-page">2</span></button>
                            </div>
                            <div class="clearfix"></div>
                            <textarea class="" id="" style="visibility: hidden; display: none;"  data-document_type=""></textarea>
                            <br/>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
                <input type="submit" class="hidden btn-submit-lead-form" />
            </form>
        </div>
        <!-- /.col -->
    </div>
</div>
<div class="modal fade" id="modal-item-bom">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">BOM Details</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <form method="post" id="frm-add-item-bom" class="form-horizontal">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">Select Category</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm select2 select-bom-category-id" name="item_category_id" required="required">
                                            <option value="">select category</option>
                                            <?php
                                                if(count($item_categories) > 0) {
                                                    foreach ($item_categories as $category_row) {
                                                        ?>
                                                        <option value="<?=$category_row->id;?>"><?=$category_row->item_category;?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">Select Item</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm select2 select-bom-item-id" name="item_id" required="required">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">UOM</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm select2 select-uom" name="uom_id" id="uom_id" required="required">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">Quantity</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="qty" class="form-control input-sm" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4">Total Amount</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="total_amount" class="form-control input-sm" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <input type="submit" class="btn btn-block btn-success btn-xs" value="Add"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <hr />
                            <div class="col-md-12">
                                <table class="table table-bordered" id="table-item-bom">
                                    <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Item</th>
                                        <th>UOM</th>
                                        <th>Qty</th>
                                        <th>Total Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $total_qty = 0;
                                    $total_amount = 0;
                                        if(isset($item_bom_data) && count($item_bom_data) > 0){
                                            foreach($item_bom_data as $item_bom_row){
                                                ?>
                                                <tr>
                                                    <td><?=$item_bom_row['item_category']?></td>
                                                    <td><?=$item_bom_row['item_name']?></td>
                                                    <td><?=$item_bom_row['uom']?></td>
                                                    <td><?=$item_bom_row['qty']?></td>
                                                    <td class="text-right"><?=$item_bom_row['total_amount'];?></td>
                                                </tr>
                                    <?php
                                                $total_amount = $total_amount + $item_bom_row['total_amount'];
                                                $total_qty = $total_qty + $item_bom_row['qty'];
                                            }
                                        }
                                    ?>
                                    <tr class="grand_total">
                                        <td colspan="3">
                                            Grand Total
                                        </td>
                                        <td class="qty_grand_total"><?=$total_qty?></td>
                                        <td class="total_amount_grand_total text-right"><?=round($total_amount)?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $(function () {

    });
</script>
<script>
    var cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var id_cnt_qtn_section  = <?=isset($page_key)?$page_key:1?>;
    var cnt_file_section  = 1;
    var id_cnt_file_section  = 1;
    var add_item_bom = 0;
    var ITEM_ID = '<?=isset($id)?$id:0?>';
    $(document).ready(function(){
        initAjaxSelect2($("#uom_id"),"<?=base_url('app/uom_select2_source')?>");

        var UploadObj = $('#domestic_fileupload').fileupload({
            url: '<?=base_url()?>item/upload-domestic-item-file/',
            formData:{"item_id":ITEM_ID}
        }).bind('fileuploadadd', function (e, data) {
            if(ITEM_ID == 0){
                show_notify("Please save item first!");
                return false;
            }else{
                var that = $(this).data('blueimp-fileupload');
                that.options.url = "<?=base_url()?>item/upload-domestic-item-file/"+ITEM_ID;
            }
        });
        //console.log(UploadObj);
        $('#domestic_fileupload').addClass('fileupload-processing');

        var UploadObj = $('#export_fileupload').fileupload({
            url: '<?=base_url()?>item/upload-export-item-file/',
            formData:{"item_id":ITEM_ID}
        }).bind('fileuploadadd', function (e, data) {
            if(ITEM_ID == 0){
                show_notify("Please save item first!");
                return false;
            }else{
                var that = $(this).data('blueimp-fileupload');
                that.options.url = "<?=base_url()?>item/upload-export-item-file/"+ITEM_ID;
            }
        });
        $('#export_fileupload').addClass('fileupload-processing');

        $('.item_document').each(function(){
            id_cnt_qtn_section++;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g );
            CKEDITOR.replace($(this).attr('id'),{
                filebrowserBrowseUrl: '<?=base_url();?>mail-system3/attachment_browse_from_server',
                filebrowserUploadUrl: '<?=base_url();?>mail-system3/upload_attachment_to_server',
                extraPlugins: 'lineheight',
                removePlugins:''
                //removePlugins:'forms,iframe,preview,a11yhelp,about,link,pagebreak,flash,save,print,newpage'
            });
        });

        $(document).on('change','.select-bom-category-id',function(){
            var category_id = $(this).val();
            $.ajax({
                url:"<?=base_url()?>item/get-items-by-category/"+category_id,
                type:"POST",
                dataType:'json',
                success:function(data){
                    var option_html = '';
                    if(data.length > 0){
                        $.each(data,function(index,value){
                            console.log("Index : "+index+" Value : "+value);
                            option_html += "<option value='"+value.id+"'>"+value.item_name+"</option>";
                        })
                    }
                    $('.select-bom-item-id').html(option_html).select2();
                }
            });
        });
        $(document).on('click','.remove_item_document',function(){
            var document_type = $(this).data('document_type');
            $(this).closest('.section_item_document').fadeOut("slow",function(){
                $(this).remove();
                cnt_qtn_section = 0;
                $('.section_item_document[data-document_type="'+document_type+'"]:not(.hidden)').each(function(){
                    cnt_qtn_section++;
                    $(this).find('.cnt-detail-page').each(function(){
                        $(this).text(cnt_qtn_section);
                    });
                });
            });
        });

        $(document).on('click','.add_item_document',function(){
            id_cnt_qtn_section++;
            var document_type = $(this).data('document_type');
            var section_item_document = $('.section_item_document.hidden').clone();
            
            cnt_qtn_section = $('.section_item_document[data-document_type="'+document_type+'"]').length;
            cnt_qtn_section = cnt_qtn_section + 1;
            section_item_document.removeClass('hidden');
            section_item_document.attr('data-section_no',cnt_qtn_section);
            section_item_document.attr('data-document_type',document_type);
            section_item_document.find('.remove_item_document').attr('data-document_type',document_type);
            section_item_document.find('.cnt-detail-page').each(function(){
                $(this).text(cnt_qtn_section);
            });
            section_item_document.find('textarea').attr('id',document_type+'_item_document_'+id_cnt_qtn_section);
            section_item_document.find('textarea').attr('data-document_type',document_type);
            if (document_type == "domestic_quotation"){
                section_item_document.find("input[type='checkbox']").attr('name','domestic_allow_to_edit[]');
                section_item_document.find("input[type='checkbox']").attr('value',cnt_qtn_section);
            }
            if (document_type == "export_quotation"){
                section_item_document.find("input[type='checkbox']").attr('name','export_allow_to_edit[]');
                section_item_document.find("input[type='checkbox']").attr('value',cnt_qtn_section);
            }
            section_item_document.find('textarea').addClass('item_document');
            section_item_document.insertBefore('.section_'+document_type+' > .qtn-section-add-btn');
            CKEDITOR.replace(section_item_document.find('textarea').attr('id'),{
                filebrowserBrowseUrl: '<?=base_url();?>mail-system3/attachment_browse_from_server',
                filebrowserUploadUrl: '<?=base_url();?>mail-system3/upload_attachment_to_server',
                extraPlugins: 'lineheight',
                removePlugins:''
                /*removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'*/
            });
        });

        $(document).on('click','.btn-save-item',function(){
            $("#add_company_form").submit();
        });
        $("#add_company_form").on("submit",function(e){
            e.preventDefault();
            $("p.item_code1-unique-error").text('');
            $("p.item_name-unique-error").text('');
            if($('#item_code1').val() == "" || $('#item_code1').val() == null){
                if($('p.item_code1-unique-error').length > 0){
                    $("p.item_code1-unique-error").text('Item code required!');
                } else {
                    $("#item_code1").after("<p class='text-danger item_code1-unique-error'>Item code required!</p>");
                }
                    show_notify('Item Code is required!', false);
                    return false;
            }

            if($('#item_name').val() == "" || $('#item_name').val() == null){
                if($('p.item_name-unique-error').length > 0){
                    $("p.item_name-unique-error").text('Item Name required!');
                } else {
                    $("#item_name").after("<p class='text-danger item_name-unique-error'>Item Name required!</p>");
                }
                show_notify('Item Name is required!', false);
                return false;
            }

            <?php if(isset($id) && !empty($id)){ ?>
                var success_status = check_is_unique('items','item_name',$("#item_name").val(),'id','<?=$id?>');
            <?php } else { ?>
                var success_status = check_is_unique('items','item_name',$("#item_name").val(),'id',$('#id').val());
            <?php } ?>
            if(success_status == 0){
                if($('p.item_name-unique-error').length > 0){
                    $("p.item_name-unique-error").text('Item name already exist!');
                }else{
                    $("#item_name").after("<p class='text-danger item_name-unique-error'>Item name already exist!</p>");
                }
                show_notify('Item Name already exist!',false);
                return false;   
            }else{
                $("p.item_name-unique-error").text(' ');
            }

            <?php if(isset($id) && !empty($id)){ ?>
                var success_status = check_is_unique('items','item_code1',$("#item_code1").val(),'id','<?=$id?>');
            <?php } else { ?>
                var success_status = check_is_unique('items','item_code1',$("#item_code1").val(),'id',$('#id').val());
            <?php } ?>
            if(success_status == 0){
                if($('p.item_code1-unique-error').length > 0){
                    $("p.item_code1-unique-error").text('Item code already exist!');
                    show_notify('Item code already exist!',false);
                }else{
                    $("#item_code1").after("<p class='text-danger item_code1-unique-error'>Item code already exist!</p>");
                    show_notify('Item code already exist!',false);
                }
                return false;
            }else{
                $("p.item_code1-unique-error").text(' ');
            }

            var item_code1 = $("#item_code1").val();
            var item_name = $("#item_name").val();
            if($("#item_for_domestic").prop('checked') == false && $("#item_for_export").prop('checked') == false){
                show_notify('Please Check atleast one item For Domestic or Export',false);
            }else if(item_code1 == '') {
                show_notify('Fill value of item code',false);
            }else if(item_name == ''){
                show_notify('Fill value of item name',false);
            }else{
                var form_data = new FormData(this);
                $('textarea.item_document').each(function(){
                    var document_type = $(this).data('document_type');
                    form_data.append('item_documents['+document_type+'][]',CKEDITOR.instances[$(this).attr('id')].getData());
                });
                $("#ajax-loader").show();
                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json',
                    success: function(data){
                        if(data.success == true){
                            cnt_file_section = 1;
                            id_cnt_file_section = 1;
                            ITEM_ID = data.item_id;
                            $("#id").val(ITEM_ID);
                            show_notify(data.message,true);
                            if(add_item_bom == 1){
                                $("#frm-add-item-bom").submit();
                            }
                            $('.section_item_file_listing').html(data.item_file_listing);
                            $('#add_company_form').attr('action','<?=base_url('item/update_item_master') ?>');
                            $("#ajax-loader").hide();
                        }
                    }
                });
            }
        });
        $(document).on('submit','#frm-add-item-bom',function(e){
            e.preventDefault();
            if(ITEM_ID == 0){
                add_item_bom = 1;
                $("#add_company_form").submit();
            }else{
                var BomFormData = new FormData(this);
                var bom_item_category = $(".select-bom-category-id > option:selected").text();
                var bom_item = $(".select-bom-item-id > option:selected").text();
                var qty = $(this).find('input[name="qty"]').val();
                var uom = $("#uom_id").select2('data')[0].text;
                var total_amount = $(this).find('input[name="total_amount"]').val();
                $(this).find('input[name="qty"]').val('');
                $(this).find('input[name="total_amount"]').val('');
                BomFormData.append('item_master_id',ITEM_ID);
                $.ajax({
                    url: "<?=base_url()?>item/save-item-bom/"+ITEM_ID,
                    type: "POST",
                    data:BomFormData,
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json',
                    success: function(data){
                        if(data.success == true){
                            $(".select-bom-category-id").val('').trigger('change');
                            add_item_bom = 0;
                            var table_row = "<tr>";
                            table_row += "<td>"+bom_item_category+"</td>";
                            table_row += "<td>"+bom_item+"</td>";
                            table_row += "<td>"+uom+"</td>";
                            table_row += "<td>"+qty+"</td>";
                            table_row += "<td class='text-right'>"+total_amount+"</td>";
                            table_row += "</tr>";
                            var total_amount_grand_total = $("td.total_amount_grand_total").text();
                            var qty_grand_total = $("td.qty_grand_total").text();
                            total_amount_grand_total = +total_amount + +total_amount_grand_total;
                            qty_grand_total = +qty + +qty_grand_total;
                            $("td.total_amount_grand_total").html(parseFloat(total_amount_grand_total).toFixed(2));
                            $("td.qty_grand_total").html(qty_grand_total);
                            $("tr.grand_total").before(table_row);
                            /*$("table#table-item-bom > tbody").append(table_row);*/
                            show_notify(data.message,true);
                        }else {
                            show_notify(data.message,false);
                        }
                    }
                });
            }
        });
    });
</script>
