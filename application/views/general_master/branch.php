<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
           $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BRANCH_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BRANCH_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BRANCH_MENU_ID, "add");
		 ?>
        <h1>
            <small class="text-primary text-bold">Branch</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Branch Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table item-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Branch</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                        <?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('general_master/branch/'.$row->branch_id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('general_master/delete/'.$row->branch_id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
						                                                
						                                            </td>
						                                            <td><?=$row->branch ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($branch_id) && !empty($branch_id)){ ?>Edit 
														<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?>  Branch
													</div>
													<div style="margin:20px">	
														<form method="POST"
														<?php if(isset($branch_id) && !empty($branch_id)){ ?>
															action="<?=base_url('general_master/update_branch') ?>" 
															<?php } else { ?>
															action="<?=base_url('general_master/add_branch') ?>" 
															<?php } ?>
														 id="form_item">
															<div class="form-group">
																<?php if(isset($branch_id) && !empty($branch_id)){ ?>
																	<input type="hidden" class="form-control input-sm" name="branch_id" id="branch_id" value="<?php echo $branch_id; ?>" >
																<?php } ?>
																<label for="inputEmail3" class="col-sm-2 input-sm">Branch<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="branch" name="branch" value="<?php echo $branch; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															
                                                            <?php if(isset($branch_id) && !empty($branch_id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Branch</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Branch</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

        $("#form_item").on("submit",function(e){
			e.preventDefault();
			if($("#branch").val() == ""){
				show_notify('Fill value Branch.', false);
				return false;
			}
			var value = $("#branch").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
		                 window.location.href = "<?php echo base_url('general_master/branch') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=branch_id&table_name=branch',
					success: function(data){
						tr.remove();
						//show_notify('Deleted Successfully!',true);
						 window.location.href = "<?php echo base_url('general_master/branch') ?>";
					}
				});
			}
		});
		
    });
</script>
