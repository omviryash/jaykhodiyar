<?php
if(isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['edit'])){             
    echo '<script> $(window).load(function(){ fill_supplier_data('.$_GET['id'].');  }); </script>';
}

?>
<input type="hidden" id="refresh" value="no">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1 style="margin-right:0.8%;">
            <small class="text-primary text-bold">Master : Add Supplier</small>

            <?php //$accessAdd  = $this->app_model->have_access_role(SUPPLIER_MODULE_ID, "add"); ?>
            <?php //if($accessAdd):?>
            <button type="button" class="btn btn-info btn-xs pull-right btn_save_supplier" style="margin-top:0.5%;">Save Supplier</button> 
            <?php
            $purchase_supplier_view_role = $this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "view");
            $purchase_supplier_add_role = $this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "add");
            if ($purchase_supplier_view_role){ ?>
            <a href="<?php echo base_url('supplier/supplier_list');?>" style="margin-right: 5px;margin-top:0.5%;" class="btn btn-info btn-xs pull-right">Load Supplier</a> 
            <?php } if ($purchase_supplier_add_role){ ?>
            <a href="<?=BASE_URL?>supplier" class="btn btn-info btn-xs pull-right" style="margin-right: 5px; margin-top:0.5%;">Add supplier</a>
            <?php } ?>
        </h1>
    </section>
    <?php //if($accessAdd):?>
    <div class="clearfix">
    <div class="row">
        <div  style="margin-left:1.5%; margin-right:1.5%;">
        <div class="col-md-12"> 
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Supplier Details</a></li>                     
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row"> 
                            <div class="col-md-12">
                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border text-primary text-bold"> Supplier Details</legend>
                                <form class="form-horizontal" id="supplierdetailform">
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <input type="hidden" name="supplier_id" value="<?php if(isset($supplier)){ echo $supplier[0]['supplier_id'];} ?>">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">Supplier Name<span class="required-sign">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm supplier_name" name="supplier_name" id="supplier_name" placeholder="" value="<?php if(isset($supplier)){ echo $supplier[0]['supplier_name'];} ?>">
                                                    <span style="display:none;" class="supplier_namespan membererror">Supplier Name must be Required.</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">Address<span class="required-sign">*</span></label>
                                                <div class="col-sm-9">   
                                                    <textarea class="form-control address" id="address" name="address" placeholder=""><?php if(isset($supplier)){ echo str_replace(',', "\n", $supplier[0]['address']);} ?></textarea>                                                         
                                                    <span style="display:none;" class="span membererror">Address must be Required.</span>
                                                </div>
                                            </div>    
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3  input-sm">City</label>
                                                <div class="col-sm-9 dispaly-flex">
													<div class="col-md-6" style="padding:0px !important;">
														<select name="city" id="city" class="form-control input-sm" onChange="getstatedetails(this.value)"></select>
                                                    </div>
													<div class="col-md-2" style="padding:0px !important;text-align:center;">
                                                        <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>    
														<small><a style="color:#1f7eba" href="<?= base_url() ?>supplier/city/" target="_blank">(Add City)</a></small>
                                                        <?php } ?>
													</div>
													<div class="col-md-4" style="padding-right:0px !important;">
														<input type="text" class="form-control input-sm pincode" name="pincode" id="pincode" placeholder="Pin Code" value="<?php if(isset($supplier)){ echo $supplier[0]['pincode'];} ?>">
													</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3  input-sm">State</label>
                                                <div class="col-sm-9 dispaly-flex">
													<div class="col-md-9" style="padding:0px !important;">
														<select name="state" id="state" class="form-control input-sm"></select>
                                                    </div>
													<div class="col-md-3" style="padding:0px !important;text-align:center;">
                                                        <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>    
														<small><a style="color:#1f7eba" href="<?= base_url() ?>supplier/state/" target="_blank">(Add State)</a></small>
                                                        <?php } ?>
													</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3  input-sm">Country</label>
                                                <div class="col-sm-9">
													<div class="col-md-9" style="padding:0px !important;">
														<select name="country" id="country" class="form-control input-sm"></select>
                                                    </div>
													<div class="col-md-3" style="padding:0px !important;text-align:center;">
                                                        <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>    
														<small><a style="color:#1f7eba" href="<?= base_url() ?>supplier/country/" target="_blank">(Add Country)</a></small>
                                                        <?php } ?>
													</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">Contact No.</label>
                                                <div class="col-sm-9">
                                                    <textarea type="text" class="form-control  " name="pan" id="pan" placeholder="" value=""></textarea>
                                                    <small class="">Add multiple contact number by Comma separated.</small>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3  input-sm">Tel No.<span class="required-sign">*</span></label>
                                                <div class="col-sm-9">                                                        
                                                    <textarea class="form-control tel_no" id="tel_no" name="tel_no" placeholder=""><?php if(isset($supplier)){ echo $supplier[0]['tel_no']; } ?></textarea>
                                                    <small class="">Add multiple telephone number by Comma separated.</small>
                                                    <span style="display:none;" class="tel_nospan membererror">Telephone No must be Required.</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3  input-sm">E-mail ID</label>

                                                <div class="col-sm-9">
                                                    <textarea class="form-control email_id" id="email_id" name="email_id"><?php if(isset($supplier)){ echo $supplier[0]['email_id']; } ?></textarea>
                                                    <small class="">Add multiple email by Comma separated.</small>
                                                    <span style="display:none;" class="email_idspan membererror">E-mail ID must be Required.</span>
                                                    <span style="display:none;" class="notvalidemail membererror">Invalid Email Address...!</span>
                                                </div>
                                            </div>    
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3  input-sm">GST No.<!--<span class="required-sign">*</span>--></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm gstin" name="gstin" id="gstin" placeholder="" value="<?php if(isset($supplier)){ echo $supplier[0]['gstin'];} ?>">
                                                </div>
                                            </div>    
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">TIN No.</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-sm" name="tin_no" id="tin_no" placeholder="" value="<?php if(isset($supplier)){ echo $supplier[0]['tin_no'];} ?>">
                                            </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">CST No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm " name="" id="" placeholder="" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">PAN No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm pan" name="pan" id="pan" placeholder="" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">Aadhar No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm aadhar" name="aadhar" id="aadhar" placeholder="" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">Contact Person</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm" name="" id="" placeholder="" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 input-sm">Note</label>
                                                <div class="col-sm-9">
                                                    <textarea type="text" class="form-control" name="" id="" placeholder="" value=""></textarea>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </form>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        </div>
        <!-- /.col -->
    </div>
    </div>
    <section class="custom content-header">
        <h1 style="margin-right:0.8%;">
            <!--<small class="text-primary text-bold">Master : Add supplier</small>-->

            <?php if ($purchase_supplier_add_role){ ?>
            <button type="button" class="btn btn-info btn-xs pull-right btn_save_supplier" style="margin-top:-0.8%;">Save supplier</button> 
            <?php }  if ($purchase_supplier_view_role){ ?>
            <a href="<?=BASE_URL?>supplier/supplier_list" class="btn btn-info btn-xs pull-right" style="margin-right: 5px; margin-top:-0.8%;">Load supplier</a>
            <?php } if ($purchase_supplier_add_role){ ?>
            <a href="<?=BASE_URL?>supplier" class="btn btn-info btn-xs pull-right" style="margin-right: 5px; margin-top:-0.8%;">Add supplier</a> 
            <?php } ?>

        </h1>
    </section>
    <?php //endif;?>
</div>

<script>
    var supplier_ID = 0;
    function setStateAndCountryByCity(city_id, id)
    {   

        $.ajax({
            url: "<?=BASE_URL?>supplier/fetch_state_country_by_city",
            dataType: "json",
            data: {city_id : city_id},
            success: function(data) 
            {
                // data = JSON.parse(data);
                if(id == "city")
                {
                    $("#supplierdetailform #state").val(data.stateName);
                    $("#supplierdetailform #country").val(data.countryName);                    
                }
                else
                {
                    $("#w_state").val(data.stateName);
                    $("#w_country").val(data.countryName);                    
                }    
            },
            error: function(error){
            }    
        });
    }


    function ValidateEmail(email_id) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email_id);
    };
    jQuery( document ).ready(function() {
        jQuery(".btn_save_supplier").click(function(){
            var supplier_name = document.forms["suppliercodeform"]["supplier_name"].value;
            if (supplier_name == "" || supplier_name == null) {
                jQuery(".supplier_namespan").show();
                show_notify(jQuery(".supplier_namespan").text(),false);
                jQuery("#supplier_name").focus();
                jQuery("a[href='#tab_1']").trigger("click");
                return false;
            }
            else{jQuery(".supplier_namespan").hide();}
        });
    });


    function fill_supplier_data(supplier_ID){ 

        $.ajax({
            url: "<?=base_url();?>supplier/get_supplier_by_id/"+supplier_ID,
            type: "POST",
            data: null,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (supplier_data) {                   
                $("#ajax-loader").hide();
                $("#supplier_list_modal").modal("hide");
                $.each(supplier_data,function(index,value){
                    if($("[name='"+index+"']").length > 0) {
                        if ($("[name='" + index + "']").is(':radio')) {
                            if ($("[name='" + index + "'][value='" + value + "']").length > 0) {
                                $("[name='" + index + "'][value='" + value + "']").iCheck('check');
                                $("input[type='radio'][value='" + value + "'].address_work").trigger('ifChanged');
                                $('a[href="#tab_1"]').trigger('click');
                            }
                        } else if ($("[name='" + index + "']").is(':input')) {
							$("[name='" + index + "']").val(value);
                        }
                    }
                })


                $('.supplier_id').each(function(){
                    $(this).val(supplier_data.supplier_id);
                }); 

                setSelect2Value($("select[name='city']"),'<?=base_url()?>app/set_city_select2_val_by_id/'+supplier_data.city_id);
                getstatedetails(supplier_data.city_id);
                setSelect2Value($("select[name='state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+supplier_data.state_id);
                setSelect2Value($("select[name='country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+supplier_data.country_id);

                $('tbody#tbl-contact-person-body').html(supplier_data.contact_persons_html);
            }
        });
    }

    $(document).ready(function(){ 

        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
        
        var $input = $('#refresh');
        $input.val() == 'yes' ? location.reload(true) : $input.val('yes'); 
    });

    $(document).on('click','.btn_save_supplier',function(){

        if ($('#supplier_name').val() == "" || $('#supplier_name').val() == null){
            show_notify('Supplier name is required!', false);
            return false;
        }

        if($('#address').val() == "" || $('#address').val() == null){
            show_notify('Address is required!', false);
            return false;
        }    

        if($('#tel_no').val() == "" || $('#tel_no').val() == null){
            show_notify('Telephone No is required!', false);
            return false;
        }  

        var DataStr = $('#tab_1 :input').serialize();             
        console.log(DataStr);
        var supplier_id = savesupplierDetail(DataStr);
        
    });

    function savesupplierDetail(DataStr){
        var ReturnData = 0;
        $.ajax({
            method:"post",
            url: "<?=BASE_URL?>supplier/save_supplier_detail/",
            data: DataStr,
            success: function(data) {

                data = JSON.parse(data);

                if(data.status == 1)
                {
                    $('.supplier_id[name="supplier_id"]').each(function(){
                        $(this).val(data.id);
                    });
                    //show_notify('Saved Successfully!',true);
                    document.location="<?php echo base_url().'supplier/supplier_list'; ?>"
                }
                else
                {
                    show_notify(data.msg,false);
                    if(data.email_error == 1)
                    {
                        jQuery("a[href='#tab_1']").trigger("click");
                        jQuery("#email_id").focus();
                    } 
                    if(data.phone_error == 1)
                    {
                        jQuery("a[href='#tab_1']").trigger("click");
                        jQuery("#tel_no").focus();
                    }    

                }

            }
        });
    }

    function check_priority_is_unique(contact_person_id = ''){
        var DataStr = "supplier_id="+$("#supplier_id").val()+"&priority="+$("#contact_person_priority").val()+"&contact_person_id="+contact_person_id;
        var response = '1';
        $.ajax({
            url: "<?=base_url()?>supplier/check_priority_is_unique/",
            type: "POST",
            data: DataStr,
            async:false
        }).done(function(data) {
            response = data;
        });
        return response;
    }



    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }


    function split(val) {
        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    function getstatedetails(id)
    {
        if(id != '' && id != null){
            $.ajax({
                type: "POST",
                url: '<?=base_url();?>supplier/ajax_get_state/'+id,
                data: id='cat_id',
                success: function(data){
                    var json = $.parseJSON(data);
                    //alert(data);
                    if (json['state']) {
                        $("#state").html('');
                        $("#state").html(json['state']);
                        $("#s2id_state span:first").html($("#state option:selected").text());    
                    }
                    if (json['country']) {
                        $("#country").html('');
                        $("#country").html(json['country']);
                        $("#s2id_country span:first").html($("#country option:selected").text());    
                    }


                },
            });
        }
    }



    /*Select2 Focus*/
    jQuery(document).ready(function($) {
        var docBody = $(document.body);
        var shiftPressed = false;
        var clickedOutside = false;
        //var keyPressed = 0;

        docBody.on('keydown', function(e) {
            var keyCaptured = (e.keyCode ? e.keyCode : e.which);
            //shiftPressed = keyCaptured == 16 ? true : false;
            if (keyCaptured == 16) { shiftPressed = true; }
        });
        docBody.on('keyup', function(e) {
            var keyCaptured = (e.keyCode ? e.keyCode : e.which);
            //shiftPressed = keyCaptured == 16 ? true : false;
            if (keyCaptured == 16) { shiftPressed = false; }
        });

        docBody.on('mousedown', function(e){
            // remove other focused references
            clickedOutside = false;
            // record focus
            if ($(e.target).is('[class*="select2"]')!=true) {
                clickedOutside = true;
            }
        });

        docBody.on('select2:opening', function(e) {
            // this element has focus, remove other flags
            clickedOutside = false;
            // flag this Select2 as open
            $(e.target).attr('data-s2open', 1);
        });
        docBody.on('select2:closing', function(e) {
            // remove flag as Select2 is now closed
            $(e.target).removeAttr('data-s2open');
        });

        docBody.on('select2:close', function(e) {
            var elSelect = $(e.target);
            elSelect.removeAttr('data-s2open');
            var currentForm = elSelect.closest('form');
            var othersOpen = currentForm.has('[data-s2open]').length;
            if (othersOpen == 0 && clickedOutside==false) {
                /* Find all inputs on the current form that would normally not be focus`able:
             *  - includes hidden <select> elements whose parents are visible (Select2)
             *  - EXCLUDES hidden <input>, hidden <button>, and hidden <textarea> elements
             *  - EXCLUDES disabled inputs
             *  - EXCLUDES read-only inputs
             */
                var inputs = currentForm.find(':input:enabled:not([readonly], input:hidden, button:hidden, textarea:hidden)')
                .not(function () {   // do not include inputs with hidden parents
                    return $(this).parent().is(':hidden');
                });
                var elFocus = null;
                $.each(inputs, function (index) {
                    var elInput = $(this);
                    if (elInput.attr('id') == elSelect.attr('id')) {
                        if ( shiftPressed) { // Shift+Tab
                            elFocus = inputs.eq(index - 1);
                        } else {
                            elFocus = inputs.eq(index + 1);
                        }
                        return false;
                    }
                });
                if (elFocus !== null) {
                    // automatically move focus to the next field on the form
                    var isSelect2 = elFocus.siblings('.select2').length > 0;
                    if (isSelect2) {
                        elFocus.select2('open');
                    } else {
                        elFocus.focus();
                    }
                }
            }
        });

        /**
     * Capture event where the user entered a Select2 control using the keyboard.
     * http://stackoverflow.com/questions/20989458
     * http://stackoverflow.com/questions/1318076
     */
        docBody.on('focus', '.select2', function(e) {
            var elSelect = $(this).siblings('select');
            var test1 = elSelect.is('[disabled]');
            var test2 = elSelect.is('[data-s2open]');
            var test3 = $(this).has('.select2-selection--single').length;
            if (elSelect.is('[disabled]')==false && elSelect.is('[data-s2open]')==false
                && $(this).has('.select2-selection--single').length>0) {
                elSelect.attr('data-s2open', 1);
                elSelect.select2('open');
            }
        });
    }); 
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
	<?php } ?>
</script>
