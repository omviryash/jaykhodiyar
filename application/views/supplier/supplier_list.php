<?php $this->load->view('success_false_notify');?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <small class="text-primary text-bold">Supplier List</small>
			<?php
                $purchase_supplier_view_role = $this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "view");
                $purchase_supplier_add_role = $this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "add");
            ?>
            <?php if ($purchase_supplier_add_role){ ?>
                <a href="<?=base_url('supplier/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
			<?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
							<div style="margin: 10px;">
								<div class="col-md-12">
									<table id="supplier_list_datatable" class="table custom-table agent-table" width="100%">
										<thead>
											<tr>
												<th>Action</th>
												<th>Supplier Name</th>
												<th>Supplier Address</th>
												<th>Contact no. </th>
												<th>Tel no. </th>
												<th>Email Id</th>	
											</tr>
										</thead>
										<tbody>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		
		table = $('#supplier_list_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('supplier/supplier_list_datatable')?>",
                "type": "POST"
            },
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
        });
		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=supplier_id&table_name=supplier',
					success: function(data){
						tr.remove();
						table.draw();
						show_notify('Supplier Deleted Successfully!', true);
						//window.location.href = "<?php echo base_url('supplier/supplier_list') ?>";
					}
				});
			}
		});
	});
</script>
