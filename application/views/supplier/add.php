<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('supplier/save_supplier') ?>" method="post" id="save_supplier" novalidate>
        <?php if(isset($supplier_data->supplier_id) && !empty($supplier_data->supplier_id)){ ?>
            <input type="hidden" name="supplier_id" class="supplier_id" value="<?=$supplier_data->supplier_id?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="custom content-header">
            <h1>
                <small class="text-primary text-bold">Purchase : Supplier</small>
                <small class="text-center"><label class="label label-warning">Ctrl+S = Save</label></small>
                <?php $purchase_supplier_add_role = $this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "add");
                      $purchase_supplier_edit_role = $this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "edit");
                ?>
                <?php if($purchase_supplier_add_role || $purchase_supplier_edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
                <?php endif;?>
                <?php $purchase_supplier_view_role = $this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "view"); ?>
                <?php if($purchase_supplier_view_role):?>
                    <a href="<?=BASE_URL?>supplier/supplier_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Supplier List</a>
                <?php endif;?>
                <?php if($purchase_supplier_add_role): ?>
                    <a href="<?=BASE_URL?>supplier/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add Supplier</a>
                <?php endif;?>
            </h1>
        </section>
        <div class="clearfix">
        <?php if($purchase_supplier_add_role || $purchase_supplier_edit_role): ?>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Supplier Details</a></li>                     
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row"> 
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Supplier Details</legend>
                                        <div class="row">
                                            <div class="col-md-6"> 
                                                <div class="form-group">
                                                    <label for="supplier_name" class="col-sm-3 input-sm">Supplier Name<span class="required-sign">*</span></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm supplier_name" name="supplier_name" id="supplier_name" placeholder="" value="<?=(isset($supplier_data->supplier_name))? $supplier_data->supplier_name : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="address" class="col-sm-3 input-sm">Address<span class="required-sign">*</span></label>
                                                    <div class="col-sm-9">   
                                                        <textarea class="form-control address" id="address" name="address" placeholder=""><?=(isset($supplier_data->address))? $supplier_data->address : ''; ?></textarea>
                                                    </div>
                                                </div>    
                                                <div class="form-group">
                                                    <label for="city_id" class="col-sm-3  input-sm">City</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <div class="col-md-6" style="padding:0px !important;">
                                                            <select name="city_id" id="city_id" class="form-control input-sm" onChange="getstatedetails(this.value)"></select>
                                                        </div>
                                                        <div class="col-md-2" style="padding:0px !important;text-align:center;">
                                                            <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>    
                                                            <small><a style="color:#1f7eba" href="<?= base_url() ?>party/city/" target="_blank">(Add City)</a></small>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col-md-4" style="padding-right:0px !important;">
                                                            <input type="text" class="form-control input-sm pincode" name="pincode" id="pincode" placeholder="Pin Code" value="<?=(isset($supplier_data->pincode))? $supplier_data->pincode : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="state_id" class="col-sm-3  input-sm">State</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <div class="col-md-9" style="padding:0px !important;">
                                                            <select name="state_id" id="state_id" class="form-control input-sm" onChange="getcountrydetails(this.value)"></select>
                                                        </div>
                                                        <div class="col-md-3" style="padding:0px !important;text-align:center;">
                                                            <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>    
                                                            <small><a style="color:#1f7eba" href="<?= base_url() ?>party/state/" target="_blank">(Add State)</a></small>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="country_id" class="col-sm-3  input-sm">Country</label>
                                                    <div class="col-sm-9">
                                                        <div class="col-md-9" style="padding:0px !important;">
                                                            <select name="country_id" id="country_id" class="form-control input-sm"></select>
                                                        </div>
                                                        <div class="col-md-3" style="padding:0px !important;text-align:center;">
                                                            <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>    
                                                            <small><a style="color:#1f7eba" href="<?= base_url() ?>party/country/" target="_blank">(Add Country)</a></small>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="contact_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">*</span></label>
                                                    <div class="col-sm-9">
                                                        <textarea type="text" class="form-control " name="contact_no" id="contact_no" placeholder="" ><?=(isset($supplier_data->contact_no))? $supplier_data->contact_no : ''; ?></textarea>
                                                        <small class="">Add multiple Contact number by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tel_no" class="col-sm-3  input-sm">Tel No.</label>
                                                    <div class="col-sm-9">                                                        
                                                        <textarea class="form-control tel_no" id="tel_no" name="tel_no" placeholder=""><?=(isset($supplier_data->tel_no))? $supplier_data->tel_no : ''; ?></textarea>
                                                        <small class="">Add multiple telephone number by Comma separated.</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email_id" class="col-sm-3  input-sm">E-mail ID</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control email_id" id="email_id" name="email_id"><?=(isset($supplier_data->email_id))? $supplier_data->email_id : ''; ?></textarea>
                                                        <small class="">Add multiple Email by Comma separated.</small>
                                                    </div>
                                                </div>    
                                                <div class="form-group">
                                                    <label for="gstin" class="col-sm-3  input-sm">GST No.<!--<span class="required-sign">*</span>--></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm gstin" name="gstin" id="gstin" placeholder="" value="<?=(isset($supplier_data->gstin))? $supplier_data->gstin : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="cin_no" class="col-sm-3 input-sm">CIN No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="cin_no" id="cin_no" class="form-control input-sm" placeholder="" value="<?php if(isset($supplier_data->cin_no)){ echo $supplier_data->cin_no; } ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                <label for="tin_no" class="col-sm-3 input-sm">TIN No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm" name="tin_no" id="tin_no" placeholder="" value="<?=(isset($supplier_data->tin_no))? $supplier_data->tin_no : ''; ?>">
                                                </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="cst_no" class="col-sm-3 input-sm">CST No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm " name="cst_no" id="cst_no" placeholder="" value="<?=(isset($supplier_data->cst_no))? $supplier_data->cst_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pan_no" class="col-sm-3 input-sm">PAN No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm pan_no" name="pan_no" id="pan_no" placeholder="" value="<?=(isset($supplier_data->pan_no))? $supplier_data->pan_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="aadhar_no" class="col-sm-3 input-sm">Aadhar No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm aadhar" name="aadhar_no" id="aadhar_no" placeholder="" value="<?=(isset($supplier_data->aadhar_no))? $supplier_data->aadhar_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="cont_person" class="col-sm-3 input-sm">Contact Person</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" name="cont_person" id="cont_person" placeholder="" value="<?=(isset($supplier_data->cont_person))? $supplier_data->cont_person : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="note" class="col-sm-3 input-sm">Note</label>
                                                    <div class="col-sm-9">
                                                        <textarea type="text" class="form-control" name="note" id="note" placeholder="" ><?=(isset($supplier_data->note))? $supplier_data->note : ''; ?></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        <?php endif; ?>
        </div>
        <section class="custom content-header">
            <?php $purchase_supplier_add_role = $this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "add"); ?>
            <?php if($purchase_supplier_add_role || $purchase_supplier_edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
            <?php endif; ?>
            <?php $purchase_supplier_view_role = $this->app_model->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "view"); ?>
            <?php if($purchase_supplier_view_role):?>
                <a href="<?=BASE_URL?>supplier/supplier_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Supplier List</a>
            <?php endif; ?>
            <?php if($purchase_supplier_add_role): ?>
                <a href="<?=BASE_URL?>supplier/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add Supplier</a>
            <?php endif; ?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.select2').select2();
        initAjaxSelect2($("#city_id"),"<?=base_url('app/city_select2_source')?>");
        <?php if(isset($supplier_data->city_id)){ ?>
            setSelect2Value($("#city_id"),"<?=base_url('app/set_city_select2_val_by_id/'.$supplier_data->city_id)?>");
        <?php } ?>
        initAjaxSelect2($("#state_id"),"<?=base_url('app/state_select2_source')?>");
        <?php if(isset($supplier_data->state_id)){ ?>
            setSelect2Value($("#state_id"),"<?=base_url('app/set_state_select2_val_by_id/'.$supplier_data->state_id)?>");
        <?php } ?>
        initAjaxSelect2($("#country_id"),"<?=base_url('app/country_select2_source')?>");
        <?php if(isset($supplier_data->country_id)){ ?>
            setSelect2Value($("#country_id"),"<?=base_url('app/set_country_select2_val_by_id/'.$supplier_data->country_id)?>");
        <?php } ?>      
            
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_supplier").submit();
                return false;
            }
        });        
        $(document).on('submit', '#save_supplier', function () {
            if($.trim($("#supplier_name").val()) == ''){
                show_notify('Please Enter Supplier Name.',false);
                $("#supplier_name").focus();
                return false;
            }
            if($.trim($("#address").val()) == ''){
                show_notify('Please Enter Address.',false);
                $("#address").focus();
                return false;
            }
            if($.trim($("#contact_no").val()) == ''){
                show_notify('Please Enter Contact No.',false);
                $("#contact_no").focus();
                return false;
            }
            $('.module_save_btn').attr('disabled','disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
			$.ajax({
				url: "<?=base_url('supplier/save_supplier') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('supplier/add') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('supplier/supplier_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });
        
    });
    function getstatedetails(id){
        if(id != '' && id != null){
            $.ajax({
                type: "POST",
                url: '<?=base_url();?>party/ajax_get_state/'+id,
                data: id='cat_id',
                success: function(data){
                    var json = $.parseJSON(data);
                    //alert(data);
                    if (json['state']) {
                        $("#state_id").html('');
                        $("#state_id").html(json['state']);
                        $("#s2id_state span:first").html($("#state option:selected").text());    
                    }
                    if (json['country']) {
                        $("#country_id").html('');
                        $("#country_id").html(json['country']);
                        $("#s2id_country span:first").html($("#country option:selected").text());    
                    }


                },
            });
        }
    }
	
	function getcountrydetails(id){
        //alert('this id value :'+id);
        if(id != '' && id != null){
            $.ajax({
                type: "POST",
                url: '<?=base_url();?>party/ajax_get_country/'+id,
                data: id='cat_id',
                success: function(data){
                    var json = $.parseJSON(data);
					console.log(json);
                    //$("#s2id_state span:first").html($("#state option:selected").text());
                    if (json['country']) {
                        $("#country_id").html('');
                        $("#country_id").html(json['country']);
                        $("#s2id_country span:first").html($("#country option:selected").text());
                    }
				},
            });
        }
    }
    
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
	<?php } ?>
</script>
