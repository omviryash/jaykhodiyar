<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Followup Category</small>
        </h1>
        <!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-7">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Followup Category
                                            </div>
                                            <div style="margin: 10px;">
                                                <table id="example1" class="table custom-table item-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Action</th>
                                                            <th>Followup Category</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if($this->applib->have_access_role(MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID,"add") || $this->applib->have_access_role(MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID,"edit")) {
                                    ?>
                                    <div class="col-md-5">
                                        <div class="panel panel-default">
                                            <div class="panel-heading clearfix">
                                                <?php if(isset($fc_id) && !empty($fc_id)){ ?>Edit
                                                <?php } else {
                                        if($this->applib->have_access_role(MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID,"add")) {
                                            $btn_disable = null;
                                        }else{
                                            $btn_disable = 'disabled';
                                        }
                                                ?>Add
                                                <?php } ?>  Followup Category
                                            </div>
                                            <div style="margin:10px">
                                                <form method="POST"
                                                      <?php if(isset($fc_id) && !empty($fc_id)){ ?>
                                                      action="<?=base_url('master_sales/update_FOLLOWUP_CATEGORY') ?>"
                                                      <?php } else { ?>
                                                      action="<?=base_url('master_sales/add_FOLLOWUP_CATEGORY') ?>"
                                                      <?php } ?>
                                                      id="form_item">
                                                    <div class="form-group">
                                                        <?php if(isset($fc_id) && !empty($fc_id)){ ?>
                                                        <input type="hidden" class="form-control input-sm" name="fc_id" id="fc_id" value="<?php echo $fc_id; ?>" >
                                                        <?php } ?>
                                                        <label for="inputEmail3" class="col-sm-5 input-sm">Followup Category<span class="required-sign">*</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control input-sm" id="fc_name" name="fc_name" <?php echo $btn_disable;?> value="<?php echo $fc_name; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group" style="margin:7px !important;"></div>

                                                    <?php 
                                        if(isset($fc_id) && !empty($fc_id)){ 
                                                    ?>
                                                    <button type="submit" class="btn btn-info btn-block btn-xs">Edit Followup Category</button>
                                                    <?php 
                                        } else {

                                                    ?>
                                                    <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Followup Category</button>
                                                    <?php
                                        } 
                                                    ?> 

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    var table;
    $(document).ready(function(){
        table = $('.item-table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('master_sales/followup_category_datatable')?>",
                "type": "POST"
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

        $("#form_item").on("submit",function(e){
            e.preventDefault();

            if($("#fc_name").val() == ""){
                show_notify('Fill value Followup Category.', false);
                return false;
            }
            if($("#ig_description").val() == ""){
                show_notify('Fill value Followup Category Description.', false);
                return false;
            }
            <?php if(isset($fc_id) && !empty($fc_id)){ ?>
            var success_status = check_is_unique('followup_category','fc_name',$("#fc_name").val(),'fc_id','<?=$fc_id?>');
            <?php } else { ?>
            var success_status = check_is_unique('followup_category','fc_name',$("#fc_name").val());
            <?php } ?>
            if(success_status == 0){
                if($('p.name-unique-error').length > 0){
                    $("p.name-unique-error").text('Followup Category already exist!');
                }else{
                    $("#fc_name").after("<p class='text-danger name-unique-error'>Followup Category already exist!</p>");
                }
                return false;
            }else{
                $("p.name-unique-error").text(' ');
            }

            var url = '<?php echo base_url('master_sales/delete/') ?>';
            var value = $("#fc_name").val();
            var ig_description = $("#ig_description").val();
            //if(value != '' && ig_description != '')
            if(value != '')
            {
                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType:'json',
                    success: function(data){
                        /*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="#" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.name+'</td>';
		                TableRow += '<td>'+data.ig_description+'</td>';
		                TableRow += '</tr>';
		                $('.item-table > tbody > tr:last ').after(TableRow);
		                $("#form_item")[0].reset();*/
                        //show_notify('Saved Successfully!',true);
                        window.location.href = "<?php echo base_url('master_sales/followup_category') ?>";
                    }
                });
            }
            /*else
			{
				if($('p.ig_description-unique-error').length > 0){
					$("p.ig_description-unique-error").text('Please enter description!');
				}else{
					$("#ig_description").after("<p class='text-danger name-unique-error'>Please enter description!</p>");
				}
				return false;
			}*/
        });

        $(document).on("click",".delete_button",function(){
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if(value){
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=fc_id&table_name=followup_category',
                    success: function(data){
                        tr.remove();
                        //show_notify('Deleted Successfully!',true);
                        window.location.href = "<?php echo base_url('master_sales/followup_category') ?>";

                    }
                });
            }
        });

    });
</script>
