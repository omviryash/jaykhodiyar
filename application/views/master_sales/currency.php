<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_SALES_CURRENCY_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_SALES_CURRENCY_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_SALES_CURRENCY_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Currency</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Currency Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table sales-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Currency</th>
						                                            <th>INR Amount</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                        <?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('master_sales/currency/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('master_sales/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
                                                                    </td>
						                                            <td><?=$row->currency ?></td>
						                                            <td><?=$row->inr_amt ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
														<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?> Currency
													</div>
													<div style="margin:10px">	
														<form method="POST" 
															<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('master_sales/update_currency') ?>" 
															<?php } else { ?>
															action="<?=base_url('master_sales/add_currency') ?>" 
															<?php } ?> id="form_sales">
															<?php if(isset($id) && !empty($id)){ ?>
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
															<?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">Currency<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="currency" name="currency" value="<?php echo $currency; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">INR Amount<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="inr_amt" name="inr_amt" value="<?php echo $inr_amt; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group" style="margin:7px !important;"></div>
															
                                                            <?php if(isset($id) && !empty($id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Currency</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Currency</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

        $("#form_sales").on("submit",function(e){
			e.preventDefault();

			if($("#currency").val() == ""){
				show_notify('Fill value Currency.', false);
				return false;
			}
			if($("#inr_amt").val() == ""){
				show_notify('Enter INR amount.', false);
				return false;
			}
			
			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('currency','currency',$("#currency").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('currency','currency',$("#currency").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.currency-unique-error').length > 0){
					$("p.currency-unique-error").text('Currency already exist!');
				}else{
					$("#currency").after("<p class='text-danger currency-unique-error'>Currency already exist!</p>");
				}
				return false;
			}else{
				$("p.currency-unique-error").text(' ');
			}

			var url = '<?php echo base_url('master_sales/delete/') ?>';
			var value = $("#currency").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.currency+'</td>';
		                TableRow += '</tr>';
		                $('.sales-table > tbody > tr:last ').after(TableRow);
		                $("#form_sales")[0].reset();
		                show_notify('Saved Successfully!',true);*/
		                window.location.href = "<?php echo base_url('master_sales/currency') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=currency',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('master_sales/currency') ?>";
					}
				});
			}
		});
		
    });
</script>
