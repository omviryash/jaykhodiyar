<div class="content-wrapper">
    <?php $this->load->view('success_false_notify');
    $this->logged_in_id = $this->session->userdata('is_logged_in')['staff_id'];
    $this->staff_name = $this->session->userdata('is_logged_in')['name'];
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
//            $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "add");
        $isView = $this->app_model->have_access_role(FEEDBACK_MENU_ID, "view");
        ?>
        <h1>
            <small class="text-primary text-bold">Feedback</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Feedback
													</div>
                                                    <?php if($isView){ ?>
													<div style="margin: 10px;">
						                                <table id="example1" class="table custom-table feedback-table" style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Action</th>
                                                                    <th>Responsible</th>
                                                                    <th>Assign To</th>
                                                                    <th>Date</th>
                                                                    <th>Note</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
					                                </div>
                                                    <?php } ?>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5" hidden="">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($feedback_data->feedback_id) && !empty($feedback_data->feedback_id)){ ?>Edit 
														<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?> Feedback
													</div>
													<div style="margin:10px">	
														<form class="form-horizontal" action="<?= base_url('feedback_reply/save_feedback') ?>"  method="post" id="save_feedback" novalidate enctype="multipart/form-data">                                    
                                                            <?php if (isset($feedback_data->feedback_id) && !empty($feedback_data->feedback_id)) { ?>
                                                                <input type="hidden" name="feedback_id" class="feedback_id" value="<?= $feedback_data->feedback_id ?>">
                                                            <?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">Responsible<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <select name="feedback_created" id="feedback_created" class="form-control select2 input-sm">
                                                                        <option value=""> - Select - </option>
                                                                        <option value="<?php echo $this->logged_in_id; ?>" <?php echo isset($feedback_data->feedback_created) && $feedback_data->feedback_created == $this->logged_in_id ? 'Selected' : ''; ?>><?php echo $this->staff_name; ?></option>
                                                                        <option value="0" <?php echo isset($feedback_data->feedback_created) && $feedback_data->feedback_created == 0 ? 'Selected' : ''; ?>>Jk Admin</option>
                                                                    </select>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="feedback_date" class="col-sm-4 input-sm">Date<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="datepicker1" name="feedback_date" value="<?= (isset($feedback_data->feedback_date)) ? date('d-m-Y', strtotime($feedback_data->feedback_date)) : date('d-m-Y'); ?>">
			                                                    </div>
															</div>
															<div class="clearfix"></div>
                                                            <div class="form-group">
																<label for="note" class="col-sm-4 input-sm">Note<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <textarea class="form-control input-sm" id="note" name="note" ><?= (isset($feedback_data->note)) ? $feedback_data->note : ''; ?></textarea>
			                                                    </div>
															</div>
															<div class="form-group" style="margin:7px !important;"></div>
															<?php if(isset($feedback_data->feedback_id) && !empty($feedback_data->feedback_id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Feedback</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Feedback</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <input type="hidden" id="clicked_item_id" value="<?= isset($feedback_data->feedback_id) ? $feedback_data->feedback_id : '-1'; ?>" >
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                Reply
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Reply
                            </div>
                            <?php if($isView){ ?>
                            <div style="margin: 10px;">
                                <table id="reply_table" class="table custom-table feedback-table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Assign To</th>
                                            <th>Date</th>
                                            <th>Reply</th> 
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if($isAdd || $isEdit) { ?>
                    <div class="col-md-5">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <?php if(isset($reply_data->reply_id) && !empty($reply_data->reply_id)){ ?>Edit 
                                <?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
                                <?php } ?> Reply
                            </div>
                            <div style="margin:10px">	
                                <form class="form-horizontal" action="<?= base_url('feedback_reply/save_reply') ?>" method="post" id="reply_form" novalidate enctype="multipart/form-data">                                    
                                    <?php if (isset($reply_data->reply_id) && !empty($reply_data->reply_id)) { ?>
                                        <input type="hidden" name="reply_id" class="reply_id" value="<?= $reply_data->reply_id ?>">
                                    <?php } ?>
                                        <input type="hidden" name="feedback_id" class="feedback_id" id="feedback_id" value="<?= isset($feedback_data->feedback_id) ? $feedback_data->feedback_id : ''; ?>">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 input-sm">Assign To<span class="required-sign">*</span></label>
                                        <div class="col-sm-7">
                                            <select name="assign_to_id" id="assign_to_id" class="form-control select2 input-sm">
                                                <option value=""> - Select - </option>
                                                <option value="0" <?php echo isset($reply_data->assign_to_id) && $reply_data->assign_to_id == 0 ? 'Selected' : ''; ?>>Jk Admin</option>
                                                <?php if(isset($party_data) && !empty($party_data)){ ?>
                                                    <option value="<?php echo $party_data->party_id; ?>" <?php echo isset($reply_data->assign_to_id) && $reply_data->assign_to_id == $party_data->party_id ? 'Selected' : ''; ?>><?php echo $party_data->party_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label for="reply_date" class="col-sm-4 input-sm">Date<span class="required-sign">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control input-sm" id="datepicker2" name="reply_date" value="<?= (isset($reply_data->reply_date)) ? date('d-m-Y', strtotime($reply_data->reply_date)) : date('d-m-Y'); ?>">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label for="note" class="col-sm-4 input-sm">Reply<span class="required-sign">*</span></label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control input-sm" id="reply" name="note" ><?= (isset($reply_data->note)) ? $reply_data->note : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin:7px !important;"></div>
                                    <?php if(isset($reply_data->reply_id) && !empty($reply_data->reply_id)){ ?>
                                    <button type="submit" class="btn btn-info btn-block btn-xs">Edit Reply</button>
                                    <?php } else { ?>
                                    <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Reply</button>
                                    <?php } ?>
                                </form>
                            </div>
                        </div>                                            
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</div>

<script>
    $(document).ready(function(){
        feedback_table = $("#example1").DataTable({
			"scrollY":        "300px",
            "serverSide": true,
			"scrollCollapse": true,
			"aaSorting": [[0, 'desc']],
			"paging":         false,
            "ajax": {
                "url": "<?php echo site_url('feedback_reply/feedback_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                },
            },
		});
        <?php if(isset($reply_data) && !empty($reply_data)){ ?>
            $('#myModal').modal('show');
        <?php } ?>
    
        reply_table = $("#reply_table").DataTable({
//			"scrollY":        "300px",
//			"scrollCollapse": true,
			"aaSorting": [[1, 'desc']],
            "serverSide": true,
			"paging":         false,
            "ajax": {
                "url": "<?php echo site_url('feedback_reply/reply_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                    d.feedback_id = $('#clicked_item_id').val();
                },
            },
		});
    
        <?php if(isset($view) && !empty($view)){ ?>
            $('#clicked_item_id').val(<?= $feedback_data->feedback_id; ?>);
            $('#myModal').modal('show');
        <?php } ?>

        $(document).on('submit', '#save_feedback', function () {
            if ($.trim($("#feedback_created").val()) == '') {
                show_notify('Please Select Responsible Person.', false);
                $("#feedback_created").focus();
                return false;
            }
            if ($.trim($("#datepicker1").val()) == '') {
                show_notify('Please Select Date.', false);
                $("#datepicker1").focus();
                return false;
            }
            if ($.trim($("#note").val()) == '') {
                show_notify('Please Enter Note.', false);
                $("#note").focus();
                return false;
            }
            $('.module_save_btn').attr('disabled', 'disabled');
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('feedback_reply/save_feedback') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    $('.module_save_btn').removeAttr('disabled', 'disabled');
                    var json = $.parseJSON(response);
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('feedback_reply/add') ?>";
                    } else if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('feedback_reply/add') ?>";
                    }
                    return false;
                },
            });
            return false;
        });
        
        $(document).on('submit', '#reply_form', function () {
            if ($.trim($("#assign_to_id").val()) == '') {
                show_notify('Please Select Assign To.', false);
                $("#assign_to_id").focus();
                return false;
            }
            if ($.trim($("#datepicker2").val()) == '') {
                show_notify('Please Select Date.', false);
                $("#datepicker2").focus();
                return false;
            }
            if ($.trim($("#reply").val()) == '') {
                show_notify('Please Enter Reply.', false);
                $("#reply").focus();
                return false;
            }
            $('.module_submit_btn').attr('disabled', 'disabled');
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('feedback_reply/save_reply') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);

                    if (json['error'] == 'Exist') {
                        show_notify(json['error_exist'], false);
                    } else if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('feedback_reply/add') ?>";
                    } else if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('feedback_reply/add') ?>";
                    }
                    $('.module_submit_btn').removeAttr('disabled', 'disabled');
                    return false;
                },

            });
            return false;
        });
        
        $(document).on('click', '.feedback_row', function () {
            $('#clicked_item_id').val($(this).attr('data-feedback_id'));
            $('#feedback_id').val($(this).attr('data-feedback_id'));
            $('#assign_to_id option:gt(1)').remove();
            $('#assign_to_id').append($("<option></option>").attr("value",$(this).attr('data-party_id')).text($(this).attr('data-party_name')));
            reply_table.draw();
            $('#myModal').modal('show');
        });
        
        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=feedback_id&table_name=user_feedback',
					success: function(data){
						tr.remove();
					}
				});
			}
		});
        
        $(document).on("click", ".delete_feedback", function () {
            if (confirm('Are you sure delete this records?')) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: '',
                    success: function (data) {
                        feedback_table.draw();
                        show_notify('Feedback Deleted Successfully!', true);
                    }
                });
            }
        });
        
        $(document).on("click", ".delete_reply", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=reply_id&table_name=reply',
                    success: function (data) {
                        reply_table.draw();
                        show_notify('Reply Deleted Successfully!', true);
                    }
                });
            }
        });

    });
</script>
