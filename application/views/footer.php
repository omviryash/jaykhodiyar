<div class="control-sidebar-bg"></div>
</div>
<div class="modal fade" id="transfer_chat_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Transfer Agent</h4>
            </div>
            <div class="modal-body">
                <ul class="users-list clearfix">
                    <?php
                        if (isset($agents) && is_array($agents)) {
                            foreach ($agents as $agent) {
                                if (in_array($agent['staff_id'], $chat_roles)) {
                    ?>
                                    <li style="width: 24%;">
                                        <a class="users-list-name transfer_to_user_name" href="javascript:void(0);" data-from_staff_id="<?php echo $this->session->userdata('is_logged_in')['staff_id']; ?>" data-to_staff_id="<?php echo $agent['staff_id']; ?>" >
                                            <?php if (isset($agent['image']) && !empty($agent['image'])) { ?>
                                                <img src="<?php echo IMAGE_URL; ?>staff/<?php echo $agent['image']; ?>"
                                                     alt="User Image" width="50" height="50">
                                                 <?php } else { ?>
                                                     <?php if (trim($agent['gender']) == 'f') { ?>
                                                    <img src="<?php echo IMAGE_URL; ?>staff/default_f.png" alt="User Image" width="50" height="50">
                                                <?php } else if (trim($agent['gender']) == 'm') { ?>
                                                    <img src="<?php echo IMAGE_URL; ?>staff/default_m.png" alt="User Image" width="50" height="50">
                                                <?php } else { ?>
                                                    <img src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image" width="50" height="50">
                                                <?php } ?>
                                            <?php } ?>
                                        </a>
                                        <a class="users-list-name transfer_to_user_name" href="javascript:void(0);" data-from_staff_id="<?php echo $this->session->userdata('is_logged_in')['staff_id']; ?>" data-to_staff_id="<?php echo $agent['staff_id']; ?>" ><?php echo $agent['name']; ?></a>
                                    </li>
                    <?php
                                }
                            }
                        }
                    ?>
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<button onclick="notify_sales_order_for_approve()" style="display: none;">Notify me!</button>
<?php
    if ($this->uri->segment(1) != 'mail-system2') {
        ?>
        <form id="frm_mailbox" action="" method="post">
            <input type="hidden" name="action" class="action" value="">
            <input type="hidden" name="mail_id" class="mail_id" value="">
        </form>
        <?php
    }
?>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        if (!Notification) {
            alert('Desktop notifications not available in your browser.');
            return;
        }
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }
    });

    function notify_sales_order_for_approve() {
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        } else {
            $.ajax({
                type: "POST",
                async: false,
                data: {},
                url: "<?= base_url('welcome/notify_sales_order_for_approve'); ?>",
                success: function (response) {
                    if (response != 0) {
                        var notification = new Notification('Sales Order for Approve', {
                            icon: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR5BV-b53B0-PUrfVIaGauiUZlPPMMcAUJcQb8Uv3aQZzfKryBDmQ",
                            body: response + " Sales Orders are pending for Approve!",
                        });
                        notification.onclick = function () {
                            window.open("<?php echo base_url('sales_order/order_list/'); ?>");
                        };
                        return false;
                    }
                }
            });
            <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){ ?>
                $.ajax({
                type: "POST",
                async: false,
                data: {},
                url: "<?= base_url('welcome/get_notification_data'); ?>",
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (response != '') {
                        var new_lineitem_html = "";
                        row_html = "";
                        var base_url = '<?= base_url()?>';
                        var length_count = json.length;
                        row_html += "<li class='header'>You have <span class='notification_count'>"+length_count+"</span> notifications</li>";
//                        $('.notifications-menu').addClass('open');
                        highlight_notification();
                        new_lineitem_html += row_html;
                        $.each(json, function (index, value) {
                            row_html = "";
                            if(value.feedback_id){
                                row_html += "<li>";
                                row_html += "<a href='"+base_url+"feedback_reply/add/"+value.feedback_id+"' target='_blank'>  ";
                                row_html += "<i class='fa fa-commenting text-aqua'></i> <b>"+value.created_by+"</b> : "+value.note;
                                row_html += "</a></li>";
                            } else if(value.reminder_id){
                                row_html += "<li>";
                                row_html += "<a href='"+base_url+"reminder/edit/"+value.reminder_id+"' target='_blank'> ";
                                row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>"+value.assign_by+"</b> : "+value.remind;
                                row_html += "</a></li>";
                            } else if(value.id){
                                if(value.sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"sales_order/add/"+value.id+"' target='_blank'> ";
                                    row_html += "<i class='fa fa-cart-plus text-aqua'></i> <b>Sales Order "+value.sales_order_no+" : </b> Pending Approve </a>";
                                    row_html += "</li>";
                                }
                                if(value.sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"sales_order/add_export/"+value.id+"' target='_blank'> ";
                                    row_html += "<i class='fa fa-cart-plus text-aqua'></i> <b>Sales Order "+value.sales_order_no+" : </b> Pending Approve </a>";
                                    row_html += "</li>";
                                }
                                
                            } else if(value.re_quotation_id){
                                <?php if($this->applib->have_access_role(QUOTATION_MODULE_ID,"edit")){ ?>
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"quotation/add/"+value.re_quotation_id+"' target='_blank'>  ";
                                    row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>Quot. "+value.quotation_no+" Reminder</b> : "+value.fc_name;
                                    row_html += "</a></li>";
                                <?php } ?>
                            } else if(value.re_sales_order_id){
                                if(value.sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"sales_order/add/"+value.re_sales_order_id+"' target='_blank'> ";
                                    row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>Sales Order "+value.sales_order_no+" Reminder</b> : "+value.fc_name;
                                    row_html += "</a></li>";
                                }
                                if(value.sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"sales_order/add_export/"+value.re_sales_order_id+"' target='_blank'> ";
                                    row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>Sales Order "+value.sales_order_no+" Reminder</b> : "+value.fc_name;
                                    row_html += "</a></li>";
                                }
                            }
                            new_lineitem_html += row_html;
                            row_html = '';
                        });
                        row_html = '';
                        $('.notification_count').html('');
                        $('.notification_count').html(length_count);
                        $('.notification_list').html('');
                        $('.notification_list').html(new_lineitem_html);
                        console.log(new_lineitem_html);
                        return false;
                    }
                }
            });
        return false;
            <?php } ?>
        }
        return false;
    }
    
    function highlight_notification(){
        $('.click_notification').click();
        $('span').closest(".notification_count").addClass("shadow_notification");
        setTimeout(function(){ $('span').closest(".notification_count").removeClass("shadow_notification"); }, 30000);
//        swal({
//            title: "Notification Reminder !",
//            text: "Click on Notification icon and See your Reminder",
//            imageUrl: "<?php // echo base_url('resource/image/notify_icon.png'); ?>"
//        });
    }
    
    function notify_reminder_notification() {
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }
            $.ajax({
                type: "POST",
                async: false,
                data: {},
                url: "<?= base_url('welcome/get_notification_data'); ?>",
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (response != '') {
                        var notification = new Notification('New Reminder', {
                            icon: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR5BV-b53B0-PUrfVIaGauiUZlPPMMcAUJcQb8Uv3aQZzfKryBDmQ",
                        });
                        var new_lineitem_html = "";
                        row_html = "";
                        var base_url = '<?= base_url()?>';
                        var length_count = json.length;
                        row_html += "<li class='header'>You have <span class='notification_count'>"+length_count+"</span> notifications</li>";
                        highlight_notification();
//                        $('.notifications-menu').addClass('open');
                        new_lineitem_html += row_html;
                        $.each(json, function (index, value) {
                            row_html = "";
                            if(value.feedback_id){
                                row_html += "<li>";
                                row_html += "<a href='"+base_url+"feedback_reply/add/"+value.feedback_id+"' target='_blank'>  ";
                                row_html += "<i class='fa fa-commenting text-aqua'></i> <b>"+value.created_by+"</b> : "+value.note+" ";
                                row_html += "</a></li>";
                            } else if(value.reminder_id){
                                row_html += "<li>";
                                row_html += "<a href='"+base_url+"reminder/edit/"+value.reminder_id+"' target='_blank'>  ";
                                row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>"+value.assign_by+"</b> : "+value.remind+" ";
                                row_html += "</a></li>";
                            } else if(value.id){
                                <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){ ?>
                                    row_html += "<li>";
                                    if(value.sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                                        row_html += "<a href='"+base_url+"sales_order/add/"+value.id+"' target='_blank'>  ";
                                    }
                                    if(value.sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                                        row_html += "<a href='"+base_url+"sales_order/add_export/"+value.id+"' target='_blank'>  ";
                                    }
                                    row_html += "<i class='fa fa-cart-plus text-aqua'></i> <b>Sales Order "+value.sales_order_no+" : </b> Pending Approve </a>";
                                    row_html += "</a></li>";
                                <?php } ?>
                            } else if(value.re_quotation_id){
                                <?php if($this->applib->have_access_role(QUOTATION_MODULE_ID,"edit")){ ?>
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"quotation/add/"+value.re_quotation_id+"' target='_blank'>  ";
                                    row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>Quot. "+value.quotation_no+" Reminder</b> : "+value.fc_name;
                                    row_html += "</a></li>";
                                <?php } ?>
                            } else if(value.re_sales_order_id){
                                if(value.sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"sales_order/add/"+value.re_sales_order_id+"' target='_blank'> ";
                                    row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>Sales Order "+value.sales_order_no+" Reminder</b> : "+value.fc_name;
                                    row_html += "</a></li>";
                                }
                                if(value.sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                                    row_html += "<li>";
                                    row_html += "<a href='"+base_url+"sales_order/add_export/"+value.re_sales_order_id+"' target='_blank'> ";
                                    row_html += "<i class='fa fa-bell-o text-aqua'></i> <b>Sales Order "+value.sales_order_no+" Reminder</b> : "+value.fc_name;
                                    row_html += "</a></li>";
                                }
                            }
                            new_lineitem_html += row_html;
                            row_html = '';
                        });
                        row_html = '';
                        $('.notification_count').html('');
                        $('.notification_count').html(length_count);
                        $('.notification_list').html('');
                        $('.notification_list').html(new_lineitem_html);
                        console.log(new_lineitem_html);
                        return false;
                    }
                }
            });
        return false;
    }

    $.widget.bridge('uibutton', $.ui.button);
    
</script>

<script src="<?= dist_url('js/auto_complete.js'); ?>"></script>
<!-- Morris.js charts -->
<script src="<?= bootstrap_url('js/raphael-min.js'); ?>"></script>

<?php if ($this->uri->segment(2) == 'dashboarddemo') { ?>
    <script src="<?= plugins_url('morris/morris.min.js'); ?>"></script>
    <script src="<?= dist_url('js/pages/dashboard.js'); ?>"></script>
<?php } ?>
<!-- Sparkline -->
<script src="<?= plugins_url('sparkline/jquery.sparkline.min.js'); ?>"></script>
<!-- jvectormap -->
<script src="<?= plugins_url('jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
<script src="<?= plugins_url('jvectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?= plugins_url('knob/jquery.knob.js'); ?>"></script>
<!-- daterangepicker -->
<script src="<?= bootstrap_url('js/moment.min.js'); ?>"></script>
<script src="<?= plugins_url('daterangepicker/daterangepicker.js'); ?>"></script>
<!-- datepicker -->
<script src="<?= plugins_url('datepicker/bootstrap-datepicker.js'); ?>"></script>
<!-- timepicker -->
<script src="<?= plugins_url('timepicker/bootstrap-timepicker.min.js'); ?>"></script>
<?php if (in_array($this->uri->segment(1), array('mailbox', 'mail-system', 'mail-system2', 'mail-system3', 'mail_system', 'mail_system2', 'mail_system3'))) { ?>
    <!-- Bootstrap WYSIHTML5 -->
<?php } ?>
<!-- Slimscroll -->
<script src="<?= plugins_url('slimScroll/jquery.slimscroll.min.js'); ?>"></script>

<!--Page Specific Js-->

<script src="<?= plugins_url('datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= plugins_url('datatables/extensions/Scroller/js/dataTables.scroller.min.js'); ?>"></script>
<script src="<?=base_url('resource/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js');?>"></script>
<script src="<?=base_url('resource/plugins/datatables/extensions/Buttons/js/buttons.flash.min.js');?>"></script>
<script src="<?=base_url('resource/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js');?>"></script>
<script src="<?=base_url('resource/plugins/datatables/extensions/Buttons/js/buttons.print.min.js');?>"></script>
<script src="<?=base_url('resource/plugins/datatables/extensions/Buttons/js/jszip.min.js');?>"></script>
<script src="<?=base_url('resource/plugins/datatables/extensions/Buttons/js/pdfmake.min.js');?>"></script>
<script src="<?=base_url('resource/plugins/datatables/extensions/Buttons/js/vfs_fonts.js');?>"></script>


<script src="<?= plugins_url('iCheck/icheck.min.js'); ?>"></script>
<?php if ($this->uri->segment(2) == 'add_item_master' || $this->uri->segment(2) == 'add-item-master' || $this->uri->segment(2) == 'file_upload_demo') { ?>
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
        <td>
        <span class="preview"></span>
        </td>
        <td>
        <p class="name">{%=file.name%}</p>
        <strong class="error text-danger"></strong>
        </td>
        <td>
        <p class="size">Processing...</p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
        {% if (!i && !o.options.autoUpload) { %}
        <button class="btn btn-primary start" disabled>
        <i class="glyphicon glyphicon-upload"></i>
        <span>Start</span>
        </button>
        {% } %}
        {% if (!i) { %}
        <button class="btn btn-warning cancel">
        <i class="glyphicon glyphicon-ban-circle"></i>
        <span>Cancel</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
        <td>
        <span class="preview">
        {% if (file.thumbnailUrl) { %}
        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
        {% } %}
        </span>
        </td>
        <td>
        <p class="name">
        {% if (file.url) { %}
        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
        {% } else { %}
        <span>{%=file.name%}</span>
        {% } %}
        </p>
        {% if (file.error) { %}
        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
        {% } %}
        </td>
        <td>
        <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
        {% if (file.deleteUrl) { %}
        <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
        <i class="glyphicon glyphicon-trash"></i>
        <span>Delete</span>
        </button>
        <input type="checkbox" name="delete" value="1" class="toggle">
        {% } else { %}
        <button class="btn btn-warning cancel">
        <i class="glyphicon glyphicon-ban-circle"></i>
        <span>Cancel</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}
    </script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
    <!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
    <!-- blueimp Gallery script -->
    <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?= plugins_url('fileupload/js/jquery.iframe-transport.js'); ?>"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?= plugins_url('fileupload/js/jquery.fileupload.js'); ?>"></script>
    <!-- The File Upload processing plugin -->
    <script src="<?= plugins_url('fileupload/js/jquery.fileupload-process.js'); ?>"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="<?= plugins_url('fileupload/js/jquery.fileupload-image.js'); ?>"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="<?= plugins_url('fileupload/js/jquery.fileupload-audio.js'); ?>"></script>
    <!-- The File Upload video preview plugin -->
    <script src="<?= plugins_url('fileupload/js/jquery.fileupload-video.js'); ?>"></script>
    <!-- The File Upload validation plugin -->
    <script src="<?= plugins_url('fileupload/js/jquery.fileupload-validate.js'); ?>"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?= plugins_url('fileupload/js/jquery.fileupload-ui.js'); ?>"></script>
<?php } ?>

<!-- FastClick -->
<script src="<?= plugins_url('fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?= dist_url('js/app.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= dist_url('js/demo.js'); ?>"></script>
<!-- keyboard shortcode key -->
<script type="text/javascript" src="<?=dist_url();?>js/shortcut.js">"></script>
<script src="<?php echo base_url('node_modules/socket.io-client/dist/socket.io.js'); ?>"></script>
<script>
    var port_number = '<?php echo PORT_NUMBER; ?>';
    /*------------- Check For Unique --------------------*/
    function check_is_unique(table_name, column_name, column_value, id_column_name1 = '', id_column_value1 = '', id_column_name2 = '', id_column_value2 = '') {
        var DataStr = "table_name=" + table_name + "&column_name=" + column_name + "&column_value=" + column_value + "&id_column_name1=" + id_column_name1 + "&id_column_value1=" + id_column_value1 + "&id_column_name2=" + id_column_name2 + "&id_column_value2=" + id_column_value2;
        var response = '1';
        $.ajax({
            url: "<?= base_url() ?>party/check_is_unique/",
            type: "POST",
            data: DataStr,
            async: false
        }).done(function (data) {
            response = data;
        });
        return response;
    }
    /*------------- Check For Unique --------------------*/

    var isActive;
    //window.onfocus = function () { 
    isActive = true;
    //}; 

    window.onblur = function () {
        isActive = false;
    };
    var userdata_limitoffset = [];

    var socket = io.connect('http://' + window.location.hostname + ':'+port_number);
    
    socket.on('connect', function(data) {
        $.ajax({
            type: "POST",
            data: {},
            url: "<?= base_url('app/update_staff_socket_id'); ?>/"+socket.id,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                if (data.success = true) {
                }
            }
        });
        return false;

    });
    
    var beat = 0;
    var reciver_id = 0;
    var conn_usr_array = {};
    function receivesHeartbeat() {
        setTimeout(receivesHeartbeat, 90000);
        //console.log(conn_usr_array);
        $.each(conn_usr_array, function (index, value)
        {
            if (value == 1)
            {
                conn_usr_array[index] = 0;
                //console.log("Connected"+index);
            } else if (value == 0)
            {
                chkpopupval = chckuserpopup(index);
                if (chkpopupval)
                {
                    close_popup(index);
                    send_email_chat_visitor(index);
                }
            }
        });
    }

    socket.on('notify_sales_order_for_approve', function (data) {
        <?php if ($this->applib->have_access_user_dropdown_roles(USER_DROPDOWN_HEADER_MODULE_ID, "view")) { ?>
            notify_sales_order_for_approve();
        <?php } ?>
        return false;
    });
    
    socket.on('reminder_notify', function (data) {
        <?php if ($this->applib->have_access_user_dropdown_roles(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"edit")) { ?>
            notify_reminder_notification();
        <?php } ?>
        return false;
    });

    socket.on('ping1', function (data) {
        socket.emit('pong1', {beat: <?= $this->session->userdata['is_logged_in']['staff_id']; ?>});
        beat = data.beat;
        sender_id = data.sender_id;
        conn_usr_array[sender_id] = beat;
        beat = 1;
    });
    setTimeout(receivesHeartbeat, 90000);
    socket.on('cron_mail_notification', function (data) {
        if (data.staff_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>" && data.count_mail > 0)
        {
            $('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">' + data.count_mail + '</span>');
        }
    });
    socket.on('new_message_client', function (data) {
        console.log('new_message_client');
        console.log(data);
        if (data.to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
        {
            if (isActive == false)
            {
                notifyBrowser(data.from_name, data.msg, '<?php echo base_url(); ?>messages/agent/' + data.from_id);
            }
            chkpopupval = chckuserpopup(data.from_id);
            if (chkpopupval)
            {
                $("#" + data.from_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + data.from_name + '</span> <span class="direct-chat-timestamp pull-right">' + data.date_time + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + data.from_image + '" alt="message user image"> <div class="direct-chat-text 13"> ' + data.msg + ' </div></div>');
                $('.direct-chat-messages').scroll();
                $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                $('#txt_' + data.from_id).trigger('focus');
            } else
            {
                register_popup(data.from_id, data.from_name);
                $("#" + data.from_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + data.from_name + '</span> <span class="direct-chat-timestamp pull-right">' + data.date_time + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + data.from_image + '" alt="message user image"> <div class="direct-chat-text 14"> ' + data.msg + ' </div></div>');
                $('.direct-chat-messages').scroll();
                $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                $('#txt_' + data.from_id).trigger('focus');

            }
        }
        if (data.from_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
        {
            $("#" + data.to_id + " .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">' + data.from_name + '</span><span class="direct-chat-timestamp pull-left">' + data.date_time + '</span></div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + data.from_image + '" alt="message user image"><div class="direct-chat-text 15">' + data.msg + '</div></div>');
            $('#txt_' + data.from_id).trigger('focus');

        }
    });

    socket.on('reminder_to_user', function (data) {
        if (data.user_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>") {
            notifyBrowser("Reminder", data.reminder, '<?php echo base_url(); ?>', data.reminder_id);
        }
    });

    socket.on('notify_quotation_reminder', function (data) {
        notify_quotation_reminder(data.message, data.followup_by, data.quotation_no, data.party_name, data.fh_id);
        return false;
    });

    function notify_quotation_reminder(message, followup_by, quotation_no, party_name, fh_id) {
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        } else {
            var notification = new Notification('Quotation Reminder for ' + quotation_no, {
                icon: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR5BV-b53B0-PUrfVIaGauiUZlPPMMcAUJcQb8Uv3aQZzfKryBDmQ",
                body: ' Party : ' + party_name + ' \n By : ' + followup_by + ' \n Message : ' + message,
            });
            // Change status call from here.
            notification.onshow = function () {
                if(fh_id != ''){
                    $.ajax({
                        type: "POST",
                        data: {'fh_id': fh_id},
                        url: "<?= base_url('reminder/update_quotation_reminder_onshow_notify'); ?>",
                        success: function (data)
                        {
                            return true;
                        }
                    });
                }
            };
            notification.onclick = function () {
                window.open("<?php echo base_url('/'); ?>");
            };
            return false;
        }
        return false;
    }

    <?php if ($this->applib->have_access_role(VISITOR_CHAT_NOTIFICATION_ID, "allow")) { ?>
    socket.on('new_message_visitor_to_client', function (data) {
        console.log('visitor_to_client');
        console.log(data);
        if (isActive == false) {
            //notifyBrowser(data.from_name,data.msg,'<?php echo base_url(); ?>messages/index/'+data.from_session_id);
        }
        formattedDate = getDateString(new Date(data.date_time), "d M y H:m:s");
        chkpopupval = chckuserpopup(data.from_session_id);
        if (chkpopupval) {
            notifyBrowser(data.from_name, data.msg, '<?php echo base_url(); ?>messages/index/' + data.from_session_id);
            if (isActive == false) {

            }
            $("#" + data.from_session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + data.from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + data.from_image + '" alt="message user image"> <div class="direct-chat-text 16"> ' + data.msg + ' </div></div>');
            $('.direct-chat-messages').scroll();
            $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
            $('#txt_visitor_' + data.from_session_id).trigger('focus');
        } else {
            $.ajax({
                type: "POST",
                data: {
                    'from_id': data.from_id
                },
                url: "<?= base_url('frontend/check_agent_isOnline'); ?>",
                success: function (response) {
                    var json = $.parseJSON(response);
                    console.log(json)
                    if (json.success) {
                        if (json.online_status == false) {
                            notifyBrowser(data.from_name, data.msg, '<?php echo base_url(); ?>messages/index/' + data.from_session_id);
                            if (isActive == false) {

                            }
                            <?php $this->session->set_userdata('visitor_first_message', '1'); ?>
                            visitor_register_popup(data.from_session_id, data.from_name, 0, 0);
                            $("#" + data.from_session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + data.from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + data.from_image + '" alt="message user image"> <div class="direct-chat-text 17"> ' + data.msg + ' </div></div>');
                            $('.direct-chat-messages').scroll();
                            $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                            $('#txt_visitor_' + data.from_session_id).trigger('focus');
                        } else {
                            get_visitor_messages(data.from_session_id, 1, data.from_name);
                        }
                    } else {
                        alert('Error : frontend/check_agent_isOnline');
                    }
                }
            });
        }
    });
    <?php } ?>

    socket.on('new_visitor_come', function (data) {
        chkpopupval = chckuserpopup(data.session_id);
        if (chkpopupval)
        {
        } else
        {
            visitor_register_popup(data.session_id, data.visitor_name, 0, 0);
        }

    });

    socket.on('add_visitor_data', function (data) {
        //console.log(data);
        $("#" + data.session_id + " .box-title").html(data.name + ' (Visitor)');
        $("#" + data.session_id + " .user-details").html('<div>User<span class="pull-right text-red">' + data.name + '</span></div><div>Email<span class="pull-right text-red">' + data.email + '</span></div><div>Phone<span class="pull-right text-yellow">' + data.phone + '</span></div><div>IP<span class="pull-right text-yellow">' + data.ip + '</span></div><div>From<span class="pull-right text-yellow">' + '<?php echo base_url(); ?>' + '</span></div><div>City<span class="pull-right text-yellow">' + data.city + '</span></div><div> Country<span class="pull-right text-yellow">' + data.country + '</span></div><div>Currency<span class="pull-right text-yellow">' + data.currency + '</span></div><div>Date <span class="pull-right text-green">' + data.date + '</span></div>');

        //for message details page infor update
        $("." + data.session_id + " .contacts-list-name").html(data.name + ' (Visitor)');
        $("." + data.session_id + " .contacts-list-msg").html(data.email);

        vis_id_msg = $('.user_details').attr('visitor-id');
        if (vis_id_msg == data.session_id)
        {
            get_visitor_info(data.session_id);
        }
    });

    socket.on('new_message_client_to_visitor', function (data) {
        console.log('client_to_visitor');
        console.log(data);
        formattedDate = getDateString(new Date(data.date_time), "d M y H:m:s");
        /*if(isActive==false)
         {
         notifyBrowser(data.from_name,data.msg,'<?php echo base_url(); ?>');
         }*/
        chkpopupval = chckuserpopup(data.to_session_id);
        if (chkpopupval) {
            if (data.from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>) {
                $("#" + data.to_session_id).find('.direct-chat-messages').append(data.chat_html);
                $("#" + data.to_session_id).find('.direct-chat-messages').animate({scrollTop: 2000000000}, 1000);
            } else {
                $("#" + data.to_session_id).find('.direct-chat-messages').append(data.other_agent_chat_html);
                $("#" + data.to_session_id).find('.direct-chat-messages').animate({scrollTop: 2000000000}, 1000);
            }
        } else {
            $.ajax({
                type: "POST",
                data: {'from_id': data.from_id},
                url: "<?= base_url('frontend/check_agent_isOnline'); ?>",
                success: function (response) {
                    var json = $.parseJSON(response);
                    console.log(json);
                    if (json.success) {
                        if (json.online_status == true) {
                            visitor_register_popup(data.to_session_id, data.to_name, 0, 0);
                            $("#" + data.to_session_id + " .direct-chat-messages").append('<div class="direct-chat-msg right"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + data.to_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + data.to_image + '" alt="message user image"> <div class="direct-chat-text 18"> ' + data.msg + ' </div></div>');
                            $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                        } else {
                            get_visitor_messages(data.to_session_id, 1, data.to_name);
                        }
                    } else {
                        alert('Error : frontend/check_agent_isOnline');
                    }
                }
            });

        }
    });

    socket.on('new_file_client_to_client', function (data) {
        console.log('client_to_client');
        console.log(data);
        if (data.to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
        {
            chkpopupval = chckuserpopup(data.from_id);
            if (chkpopupval)
            {
                get_messages(data.from_id, 1);
            } else
            {
                register_popup(data.from_id, data.from_name, 0, 0);
            }
        }
        if (data.from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
        {
            get_messages(data.to_id, 1);
        }
    });

    socket.on('new_file_visitor_to_client', function (data) {
        if (data.to_table == "staff,admin")
        {
            formattedDate = getDateString(new Date(data.date_time), "d M y H:m:s");
            chkpopupval = chckuserpopup(data.from_session_id);
            if (chkpopupval)
            {
                get_visitor_messages(data.from_session_id, 1, data.from_name);
            } else
            {
                visitor_register_popup(data.from_session_id, data.from_name, 0, 0);
            }
        }
    });

    socket.on('new_file_client_to_visitor', function (data) {
        formattedDate = getDateString(new Date(data.date_time), "d M y H:m:s");
        chkpopupval = chckuserpopup(data.to_session_id);
        if (chkpopupval) {
            if (data.from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>) {
                $("#" + data.to_session_id).find('.direct-chat-messages').append(data.chat_html);
                $("#" + data.to_session_id).find('.direct-chat-messages').animate({scrollTop: 2000000000}, 1000);
                $('#txt_visitor_' + data.to_session_id).trigger('focus');
            } else {
                $("#" + data.to_session_id).find('.direct-chat-messages').append(data.other_agent_chat_html);
                $("#" + data.to_session_id).find('.direct-chat-messages').animate({scrollTop: 2000000000}, 1000);
                $('#txt_visitor_' + data.to_session_id).trigger('focus');
            }
        } else {
            visitor_register_popup(data.to_session_id, data.to_name, 0, 0);
            $("#" + data.to_session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + data.to_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + data.to_image + '" alt="message user image"> <div class="direct-chat-text 19"> ' + data.msg + ' </div></div>');
            $('.direct-chat-messages').scroll();
            $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
            $('#txt_visitor_' + data.to_session_id).trigger('focus');
        }
    });

    function inrFormat(val) {
        var x = val;
        x = x.toString();
        var afterPoint = '';
        if (x.indexOf('.') > 0)
            afterPoint = x.substring(x.indexOf('.'), x.length);
        x = Math.floor(x);
        x = x.toString();
        var lastThree = x.substring(x.length - 3);
        var otherNumbers = x.substring(0, x.length - 3);
        if (otherNumbers != '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
        return res;
    }

    $(document).ready(function () {

        /*$(".select2").select2({
         placeholder: " --Select-- ",
         allowClear: true
         });*/
        //$('form').parsley();
        $(document).on('input', ".num_only", function () {
            this.value = this.value.replace(/[^\d\.\-]/g, '');
        });

        $(document).on('input', ".a_to_z_not_allow", function () {
            this.value = this.value.replace(/[^\d\+\(\)\\\\\/\,]/g, '');
        });

        $(document).on('click', '.btn_go_back', function () {
            goBack();
        });

        $(document).keyup(function (e) {
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                if ($('.modal.in').length > 0) {
                    goBack();
                }
            }
        });

        $(document).on('click', '.close', function () {
            console.log('Closed!');
            $(this).closest('div.alert-div').hide();
        });
        $(".input-datepicker").datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: "linked",
            todayHighlight: true,
            autoclose: true
        });

        $(".timepicker").timepicker({
            format: 'hh:mm:ss',
            showInputs: false,
            showSeconds: false,
            showMeridian: false,
            minuteStep: 15
        });

        $(".input-datetimepicker").datepicker({
            format: 'dd-mm-yyyy hh:mm:ss',
            todayBtn: "linked",
            todayHighlight: true,
            autoclose: true
        });
        $('input[type="radio"].address_work').iCheck({
            radioClass: 'iradio_minimal-green'
        });
        $('input[type="radio"].com_address').iCheck({
            radioClass: 'iradio_minimal-green'
        });
        $('input[type="checkbox"].enquiry_received_by').iCheck({
            checkboxClass: 'icheckbox_flat-green',
        });
        $('#datepicker1,#datepicker2,#datepicker3').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: "linked",
            todayHighlight: true,
            autoclose: true
        });

        $(document).on('click', '.btn_close_window', function () {
            var e = $.Event('keypress');
            e.which = 87;
            e.ctrlKey = true;
            console.log(e);
            $(document).trigger(e);
        });

        $(document).on('click', '.send_agent_btn', function () {
            var TextBoxObj = $(this).closest('.input-group').find('input[name="message"]');
            sendmsg(TextBoxObj);
        });
        $(document).on('click', '.send_visitor_btn', function () {
            var TextBoxObj = $(this).closest('.input-group').find('input[name="message"]');
            sendmsgtovisitor(TextBoxObj);
        });

        $(document).on('click', '.btn-header-read-mail', function () {
            $("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
            $("#frm_mailbox").attr('action', '<?= base_url() ?>mail-system2/read');
            $("#frm_mailbox").submit();
        });
//        getInboxUnreadMails();

        String.prototype.replaceAll = function (search, replacement) {
            var target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };
        $('input.numberwithcomma').keyup(function () {
            var input = $(this).val().replaceAll(',', '');
            if (input.length < 1)
                $(this).val('');
            else {
                var val = parseFloat(input);
                var formatted = inrFormat(input);
                if (formatted.indexOf('.') > 0) {
                    var split = formatted.split('.');
                    formatted = split[0] + '.' + split[1].substring(0, 2);
                }
                $(this).val(formatted);
            }
        });

        $('input.numberwithoutzero').keyup(function () {
            var input = $(this).val().replaceAll(',', '');
            if (input.length < 1) {
                $(this).val('');
            } else {
                input = input.replace(/^0+/, '');
                $(this).val(input);
            }
        });

        <?php if ($this->uri->segment(1) == 'hr' && $this->uri->segment(2) == "expected-interview") { ?>
            $('#datepicker1,#datepicker2,#datepicker3').datepicker({
                format: 'dd-mm-yyyy',
                todayBtn: "linked",
                todayHighlight: true,
                autoclose: true
            });
        <?php } ?>
            
        $('form input:visible, .focus:visible, textarea:visible').eq(0).focus();    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('form input:visible, .focus:visible, textarea:visible').eq(0).focus();
        });
//        $('.modal').on('hidden.bs.modal', function (e) {
//            $('form input:visible, .focus:visible, textarea:visible').eq(0).focus();
//        });
//        
        shortcut.add("f1",function(event) {
            event.preventDefault();
            $('#tabs_1').click();
        });
        shortcut.add("f2",function(event) {
            event.preventDefault();
            $('#tabs_2').click();
        });
        shortcut.add("f3",function(event) {
            event.preventDefault();
            $('#tabs_3').click();
        });
        shortcut.add("f4",function(event) {
            event.preventDefault();
            $('#tabs_4').click();
        });
        shortcut.add("f5",function(event) {
            event.preventDefault();
            $('#tabs_5').click();
        });
            
        $(document).on('click', '.open_transfer_chat_modal', function(){
            $('#transfer_chat_modal').modal({backdrop: 'static', keyboard: false});
            $('#transfer_chat_modal').modal('show');
			var visitor_chatbox_id = $(this).data('visitor_chatbox_id');
            $('#transfer_chat_modal').attr('data-open_visitor_chatbox_id', visitor_chatbox_id);
        });
        
        $(document).on('click', '.modal-body .transfer_to_user_name', function () {
            var from_staff_id = $(this).attr('data-from_staff_id');
            var to_staff_id = $(this).attr('data-to_staff_id');
            var open_visitor_chatbox_id = $('#transfer_chat_modal').attr('data-open_visitor_chatbox_id');
            $.ajax({
                type: "POST",
                async: false,
                data: {'from_staff_id': from_staff_id, 'to_staff_id': to_staff_id, 'visitor_session_id': open_visitor_chatbox_id},
                url: "<?= base_url('chat/transfer_chat_to_other'); ?>",
                success: function (data){
                }
            });
            return false;
        });
        
        socket.on('transfer_chat_to_other', function (data) {
            if(data.to_staff_id == <?php echo $this->session->userdata('is_logged_in')['staff_id']; ?>){
                if(confirm('Please Accept Visitor chat : ' + data.visitor_name + ' Transferred to you from : ' + data.from_staff_name)){
//                    alert(data.from_staff_id + ',' + data.to_staff_id + ',' + data.visitor_session_id);
                    $.ajax({
                        type: "POST",
                        async: false,
                        data: {'from_staff_id': data.from_staff_id, 'to_staff_id': data.to_staff_id, 'visitor_session_id': data.visitor_session_id, 'response': 'true'},
                        url: "<?= base_url('chat/transfer_chat_to_other_response'); ?>",
                        success: function (data){
                        }
                    });
                    visitor_register_popup(data.visitor_session_id, data.visitor_name, 0, 0);
                    get_visitor_messages(data.visitor_session_id, 1, data.visitor_name);
                } else {
                    $.ajax({
                        type: "POST",
                        async: false,
                        data: {'from_staff_id': data.from_staff_id, 'to_staff_id': data.to_staff_id, 'visitor_session_id': data.visitor_session_id, 'response': 'false'},
                        url: "<?= base_url('chat/transfer_chat_to_other_response'); ?>",
                        success: function (data){
                        }
                    });
                }
            }
            return false;
        });
        socket.on('transfer_chat_to_other_response', function (data) {
            if(data.from_staff_id == <?php echo $this->session->userdata('is_logged_in')['staff_id']; ?>){
                if(data.response == 'true'){
                    $('#transfer_chat_modal').removeAttr('data-open_visitor_chatbox_id');
                    $('#transfer_chat_modal').modal('hide');
                    $('#close_visitor_chatbox_'+data.visitor_session_id).attr('onClick', 'javascript:close_popup(' + data.visitor_session_id + ', 0)');
                    $('#close_visitor_chatbox_'+data.visitor_session_id).click();
                    show_notify(data.to_staff_name + ' has Accepted your Transferred chat Request.', true);
                } else {
                    show_notify(data.to_staff_name + ' has Rejected your Transferred chat Request.', false);
                }
            }
        });

        $(document).on('focusout', '.twitter-typeahead input[type=text]', function(){
            setTimeout(function () {
                $('.twitter-typeahead input[type=text]').val('');
            }, 20);
        });

        $('.dt-button.buttons-copy').css('border', 'none');
        $('.dt-button.buttons-csv').css('border', 'none');
        $('.dt-button.buttons-excel').css('border', 'none');
        $('.dt-button.buttons-pdf').css('border', 'none');
        $('.dt-button.buttons-print').css('border', 'none');
        $('.dt-button.buttons-copy').html('<img src="<?php echo base_url(); ?>resource/dist/img/copy_icon.png" style="width:25px;" alt="Copy" title="Copy" >');
        $('.dt-button.buttons-csv').html('<img src="<?php echo base_url(); ?>resource/dist/img/csv_icon.png" style="width:25px;" alt="CSV" title="CSV" >');
        $('.dt-button.buttons-excel').html('<img src="<?php echo base_url(); ?>resource/dist/img/excel_icon.png" style="width:25px;" alt="Excel" title="Excel" >');
        $('.dt-button.buttons-pdf').html('<img src="<?php echo base_url(); ?>resource/dist/img/pdf_icon.png" style="width:25px;" alt="PDF" title="PDF" >');
        $('.dt-button.buttons-print').html('<img src="<?php echo base_url(); ?>resource/dist/img/print_icon.png" style="width:25px;" alt="Print" title="Print" >');

    });
</script>
<?php if ($this->uri->segment(1) == 'hr' && $this->uri->segment(2) == "yearly-leaves") { ?>
    <script src="<?= plugins_url("multi-dates-picker/js/jquery-ui-1.11.1.js"); ?>"></script>
    <script src="<?= plugins_url("multi-dates-picker/jquery-ui.multidatespicker.js"); ?>"></script>
    <script src="<?= plugins_url("multi-dates-picker/js/prettify.js"); ?>"></script>
    <script src="<?= plugins_url("multi-dates-picker/js/lang-css.js"); ?>"></script>
    <script>
        var today = new Date();
        var y = today.getFullYear();
        var Holidays = <?= json_encode($YearlyHoliday) ?>;
        console.log(Holidays);
        $('#full-year').multiDatesPicker({
            addDates: Holidays,
            numberOfMonths: [3, 4],
            defaultDate: '1/1/' + y
        });
    </script>
    <!--      Multi Dates Pickers   -->
<?php } ?>
<!----------------- NOTIFICATION ----------------->
<script src="<?= plugins_url('notify/jquery.growl.js'); ?>"></script>
<script>
    /**
     *
     * @param notify_msg
     * @param notify_type
     */
    function show_notify(notify_msg, notify_type)
    {
        if (notify_type == true) {
            $.growl.notice({title: "Success!", message: notify_msg});
        } else {
            $.growl.error({title: "False!", message: notify_msg});
        }
    }
</script>
<!----------------- NOTIFICATION ----------------->

<!----------------- Chat ----------------->
<script>
    //this function can remove a array element.
    Array.remove = function (array, from, to) {
        var rest = array.slice((to || from) + 1 || array.length);
        array.length = from < 0 ? array.length + from : from;
        return array.push.apply(array, rest);
    };
    //this variable represents the total number of popups can be displayed according to the viewport width
    var total_popups = 0;
    //arrays of popups ids
    var popups = [];
    //this is used to close a popup
    $.removeFromArray = function (value, arr) {
        return $.grep(arr, function (elem, index) {
            return elem !== value;
        });
    };

    function close_popup(id, visitorpopup = 0, notifychat = 0, no_send_mail = 0) {
        if (visitorpopup == 1)
        {
            var chathtml = $("#" + id + " .direct-chat-messages").html();
            if (no_send_mail == 1) {
            } else if (visitorpopup == 1) {
                send_chat_mail(id, chathtml, notifychat);
            } else {
                send_chat_mail(id, chathtml);
            }
        }
        userdata_limitoffset.splice($.inArray(id, userdata_limitoffset), 1);
        //userdata_limitoffset = $.removeFromArray(8, userdata_limitoffset);
        for (var iii = 0; iii < popups.length; iii++) {
            if (id == popups[iii]) {
                Array.remove(popups, iii);
                document.getElementById(id).remove();
                calculate_popups();
                return;
            }
    }
    }
    function send_email_chat_visitor(userid)
    {
        $.ajax({
            type: "POST",
            data: {'userid': userid},
            url: "<?= base_url('frontend/sendEmailToadminForOldChatOfVisitor'); ?>",
            success: function (data)
            {
            }
        });
    }

    function send_chat_mail(id, html, notifychat = 0)
    {
        $.ajax({
            type: "POST",
            data: {'id': id},
            url: "<?= base_url('chat/send_chat_mail'); ?>",
            success: function (data)
            {
                if (notifychat == 1)
                {
                    show_notify('Email Sent Successfully!', true);
                }
                return true;
            }
        });
    }

    function getInboxUnreadMails() {
        $.ajax({
            url: "<?= base_url('app/get_inbox_unread_mails/'); ?>",
            type: "GET",
            data: null,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                if (data.no_unread_mails > 0) {
                    $('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">' + data.no_unread_mails + '</span>');
                    $('.sidebar-unread-mails').append('<span class="pull-right-container"><span class="label label-primary pull-right">' + data.no_unread_mails + '</span></span>');
                }
                $('.messages-menu').append(data.unread_mails);
            }
        });
    }

    function notifyBrowser(title, desc, url, reminder_id = '') {
        if (!Notification) {
            console.log('Desktop notifications not available in your browser..');
            return;
        }
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        } else {
            var notification = new Notification(title, {
                icon: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR5BV-b53B0-PUrfVIaGauiUZlPPMMcAUJcQb8Uv3aQZzfKryBDmQ',
                tag: 'chat-notify',
                body: desc,
            });
            isActive == false;
            
            notification.onshow = function () {
                if(reminder_id != ''){
                    $.ajax({
                        type: "POST",
                        data: {'reminder_id': reminder_id},
                        url: "<?= base_url('reminder/update_reminder_onshow_notify'); ?>",
                        success: function (data)
                        {
                            return true;
                        }
                    });
                }
                // Change status call from here.
            };
            
            // Remove the notification from Notification Center when clicked.
            notification.onclick = function () {
                window.open(url);
            };

            // Callback function when the notification is closed.
            notification.onclose = function () {
                console.log('Notification closed');
            };
        }
    }

    function display_popups() {
        var right = 10;
        var iii = 0;
        for (iii; iii < total_popups; iii++) {
            if (popups[iii] != undefined) {
                var element = document.getElementById(popups[iii]);
                element.style.right = right + "px";
                right = right + 320;
                element.style.display = "block";
            }
        }
        for (var jjj = iii; jjj < popups.length; jjj++) {
            var element = document.getElementById(popups[jjj]);
            element.style.display = "none";
        }
    }

    // for fileupload in chat box
    var file_upload_to_id = "";
    var file_upload_to_visitor_id = "";

    function fileUpload(userid)
    {
        var w = 400;
        var h = 200;
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        file_upload_to_id = userid;
        window.open("<?= base_url(); ?>upload", "Upload File", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    }

    function fileUploadtovisitor(userid)
    {
        var w = 400;
        var h = 200;
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        file_upload_to_visitor_id = userid;
        window.open("<?= base_url(); ?>upload/fileuploadtovisitor", "Upload File", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    }

    function uploadfile(temp)
    {
        sendchatimage(file_upload_to_id, temp, "admin", "admin");
    }

    function uploadfilefromadmintovisitor(temp)
    {
        sendchatimagefromadmintovisitor(file_upload_to_visitor_id, temp, "admin", "visitors");
    }

    function sendchatimage(to_id, msg, from_table, to_table)
    {
        $.ajax({
            type: "POST",
            data: {'to_id': to_id, 'msg': msg, 'from_table': from_table, 'to_table': to_table},
            url: "<?= base_url('chat/addAdminImage'); ?>",
            success: function (data)
            {
                return true;
            }
        });
    }

    function sendchatimagefromadmintovisitor(to_id, msg, from_table, to_table)
    {
        $.ajax({
            type: "POST",
            data: {'to_id': to_id, 'msg': msg, 'from_table': from_table, 'to_table': to_table},
            url: "<?= base_url('chat/addAdminToVisitorImage'); ?>",
            success: function (data)
            {
                return true;
            }
        });
    }

    $('.visitor_pop').click(function () {
        alert('df')
    });

    //creates markup for a new popup. Adds the id to popups array.
    function register_popup(id, name, lastid, getusermsgs = 0) {
        if (typeof (lastid) === 'undefined')
            lastid = 0;
        for (var iii = 0; iii < popups.length; iii++) {
            //already registered. Bring it to front.
            if (id == popups[iii]) {
                Array.remove(popups, iii);
                popups.unshift(id);
                calculate_popups();
                return true;
            }
        }
        var element = '<div class="popup-box chat-popup" id="' + id + '">';
        element += '<div class="box box-warning direct-chat direct-chat-warning">';
        element += '<div class="box-header with-border"> <h3 class="box-title">' + name + '</h3>';
        element += '<div class="box-tools pull-right">';
        element += '<button type="button" class="btn btn-box-tool" data-widget="collapse">';
        element += '<i class="fa fa-minus"></i> </button>';
        element += '<button onClick="javascript:close_popup(' + id + ');" type="button" class="btn btn-box-tool" data-widget="remove">';
        element += '<a href="javascript:close_popup(' + id + ');"><i class="fa fa-times"></a></i>';
        element += '</button></div></div>';
        element += '<div class="box-body"> <div class="direct-chat-messages"></div></div>';
        element += '<div class="box-footer"> <div class="input-group">';

        element += '<input type="text" id="txt_' + id + '" userid="' + id + '" fromtable="admin" totable="admin"  onfocus="set_read_msgs(' + id + ')" onkeyup="checkenter(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send 5 text_suggestion_' + id + '">';

        element += '<span class="input-group-btn">';
        element += '<button type="button" class="btn btn-warning btn-flat sendmsg_btn send_agent_btn" id="btn_' + id + '">Send</button></span>';
        element += '</div></div></div>&nbsp;&nbsp;</div>';
        get_staff_info(id);
        //With Upload Button var element = '<div class="popup-box chat-popup" id="'+id+'"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">'+name+'</h3> <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button><button onClick="javascript:close_popup('+id+');" type="button" class="btn btn-box-tool" data-widget="remove"><a href="javascript:close_popup('+id+');"><i class="fa fa-times"></a></i> </button> </div></div><div class="box-body"> <div class="direct-chat-messages"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_'+id+'" userid="'+id+'" fromtable="admin" totable="admin"  onfocus="set_read_msgs('+id+')" onkeyup="checkenter(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send 1"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn" id="btn_'+id+'" onclick="fileUpload('+id+');" ><i class="fa fa-upload"></i></button> </span> </div></div></div></div>';
        //document.getElementsByTagName("body")[0].innerHTML = document.getElementsByTagName("body")[0].innerHTML + element;
        $('body').append(element);

        /*---------- Chat Template Auto Complete -------------------*/
        var chatTemplateBloodhound = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.text_suggestion);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: $.core.url('app/load_text_suggetion/%QUERY'),
                wildcard: '%QUERY'
            }
        });
        chatTemplateBloodhound.initialize();

        var $el = $('input.text_suggestion_' + id)
        $el.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'text_suggestion',
            displayKey: 'text_suggestion',
            source: chatTemplateBloodhound.ttAdapter(),
            templates: {
                suggestion: function (data) {
                    return '<p><strong>' + data.text_suggestion + '</strong></p>';
                }
            }
        });
        $('#txt_' + id).val(null).trigger('change');
        /*---------- Chat Template Auto Complete -------------------*/

        userdata_limitoffset[id] = [];
        userdata_limitoffset[id]['limit'] = 5;
        userdata_limitoffset[id]['offset'] = 5;
        console.log(userdata_limitoffset);
        var limit = 5;
        var offset = 5;
        $('.direct-chat-messages').on('scroll', function () {
            var scroll = $('.direct-chat-messages').scrollTop();
            if (scroll == 0)
            {
                console.log("on top");
                //busy = true;
                // start to load the first set of data
                //busy = true;
                offset = limit + offset;
                //displayRecords(limit, offset);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('messages/getScrollUserMsg'); ?>",
                    data: {'userid': id, 'limit': limit, 'offset': offset},
                    cache: false,
                    beforeSend: function () {

                    },
                    success: function (html) {
                        if (html != 'false')
                        {
                            var oldmsg = $("#" + id + " .direct-chat-messages").html();
                            $.each(JSON.parse(html), function (index, value) {

                                from_id = value.from_id;
                                to_id = value.to_id;
                                from_name = value.from_name;
                                to_name = value.to_name;
                                from_table = value.from_table;
                                message = value.message;
                                file = value.file;
                                date = value.date_time;
                                image = value.image;
                                lastTimeID = value.id;

                                formattedDate = getDateString(new Date(value.date_time), "d M y H:m:s");
                                if (message == "" || message == null)
                                {
                                    message = "<a class='chatfilecreceive' target='_blank' href='<?php echo base_url(); ?>file/download/" + file + "'>" + file + "</a>";
                                }
                                if (to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
                                {
                                    $("#" + from_id + " .direct-chat-messages").prepend('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 20"> ' + message + ' </div></div>');
                                    $('#txt_' + from_id).trigger('focus');
                                } else
                                {
                                    $("#" + to_id + " .direct-chat-messages").prepend('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">' + from_name + '</span><span class="direct-chat-timestamp pull-left">' + formattedDate + '</span></div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"><div class="direct-chat-text 21">' + message + '</div></div>');
                                    $('#txt_' + to_id).trigger('focus');

                                }
                            });
                        }

                    }
                });
            }
        });
        popups.unshift(id);
        calculate_popups();
        if (getusermsgs == 0)
        {
            get_messages(id, 1);
        }
        $('#txt_' + id).trigger('focus');
        return true;
    }

    function visitor_register_popup(id, name, lastid, getusermsgs = 0) {

        if (typeof (lastid) === 'undefined')
            lastid = 0;
        for (var iii = 0; iii < popups.length; iii++) {
            //already registered. Bring it to front.
            if (id == popups[iii]) {
                Array.remove(popups, iii);
                popups.unshift(id);
                calculate_popups();
                return true;
            }
        }
        var element = '<div class="popup-box chat-popup" id="' + id + '"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">' + name + '</h3> <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button><button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Details"><i class="fa fa-user"></i></button><button type="button" class="btn btn-box-tool open_transfer_chat_modal" title="Transfer To" data-visitor_chatbox_id="' + id + '"><i class="fa fa-users"></i></button><button onClick="javascript:close_popup(' + id + ',1);" type="button" id="close_visitor_chatbox_' + id + '" class="btn btn-box-tool visitor_pop" data-widget="remove"><a href="javascript:close_popup(' + id + ',1);"><i class="fa fa-times"></a></i> </button> </div></div><div class="box-body"> <div class="direct-chat-messages"></div><div style="padding:10px;" class="direct-chat-contacts user-details"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_visitor_' + id + '" fromid="<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>" toid="' + id + '" fromtable="admin" totable="visitors"  onfocus="set_visitor_read_msgs(' + id + ')" onkeyup="checkenterfrmvisitor(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send vc_text_suggestion_' + id + ' 2"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn send_visitor_btn" id="btn_' + id + '">Send</button><button type="button" class="btn btn-success btn-flat btn-send-file" id="btn_' + id + '" onclick="fileUploadtovisitor(' + id + ');" ><i class="fa fa-upload"></i></button> </span> </div></div></div>&nbsp;&nbsp;</div>';
        get_visitor_info_popup(id);
        //document.getElementsByTagName("body")[0].innerHTML = document.getElementsByTagName("body")[0].innerHTML + element;
        $('body').append(element);
        popups.unshift(id);
        calculate_popups();
        if (getusermsgs == 0)
        {
            get_visitor_messages(id, 1, name);
        }
        var hgt = $("#" + id + " .direct-chat-messages").height();
        $("#" + id + " .direct-chat-messages").animate({scrollTop: hgt * 10}, 500);
        $('#txt_visitor_' + id).trigger('focus');
        
        /*---------- Visitor Chat Template Auto Complete -------------------*/
        var visitorChatTemplateBloodhound = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.text_suggestion);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: $.core.url('app/load_text_suggetion/%QUERY'),
                wildcard: '%QUERY'
            }
        });
        visitorChatTemplateBloodhound.initialize();

        var $el = $('input.vc_text_suggestion_' + id)
        $el.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'text_suggestion',
            displayKey: 'text_suggestion',
            source: visitorChatTemplateBloodhound.ttAdapter(),
            templates: {
                suggestion: function (data) {
                    return '<p><strong>' + data.text_suggestion + '</strong></p>';
                }
            }
        });
        $('#txt_visitor_' + id).val(null).trigger('change');
        /*---------- Chat Template Auto Complete -------------------*/
        
        return true;
    }

    function set_read_msgs(userid)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        /*$.ajax({
         type:"POST",
         data: {'userid' : userid},
         url:"<?= base_url(); ?>messages/updateReadMsg",
         success:function(data) {}
         });*/
    }

    function set_visitor_read_msgs(userid)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        /*$.ajax({
         type:"POST",
         data: {'userid' : userid},
         url:"<?= base_url(); ?>messages/updateVisitorReadMsg",
         success:function(data)	{	}
         });*/
    }

    function get_staff_info(userid)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        $.ajax({
            type: "POST",
            data: {'userid': userid},
            url: "<?= base_url(); ?>messages/getStaffById",
            success: function (data)
            {
                if (data != 'false')
                {
                    //console.log(data);
                    $.each(JSON.parse(data), function (index, value) {
                        user = value.name;
                        email = value.email;
                        birthdate = value.birth_date;
                        contactno = value.contact_no;
                        ip = '';
                        message_from = '';
                        city = '';
                        country = '';
                        currency = '';
                        date = '';
                        is_login = value.is_login;

                        var visitor_title = user;
                        if(is_login == '0'){
                            visitor_title += ' <i class="pull-right fa fa-circle-o" style="font-size: 10px"></i> - Left ';
                        } else {
                            visitor_title += ' <i class="pull-right fa fa-circle text-green" style="font-size: 10px"></i>';
                        }
                        $("#" + userid + " h3").html(visitor_title);
                        $(".user-details").html('<li><a href="#">User<span class="pull-right text-red">' + user + '</span></a></li><li><a href="#">Email<span class="pull-right text-red">' + email + '</span></a></li><li><a href="#">Phone<span class="pull-right text-yellow">' + contactno + '</span></a></li><li><a href="#">IP<span class="pull-right text-yellow">' + ip + '</span></a></li><li><a href="#">From<span class="pull-right text-yellow">' + message_from + '</span></a></li><li><a href="#">City<span class="pull-right text-yellow">' + city + '</span></a></li><li><a href="#">Country<span class="pull-right text-yellow">' + country + '</span></a></li><li><a href="#">Currency<span class="pull-right text-yellow">' + currency + '</span></a></li><li><a href="#">Date <span class="pull-right text-green">' + date + '</span></a></li>');
                        $(".user_details").html('Agent Details');

                    });
                }
            }
        });
    }
    function get_visitor_info(userid)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        $.ajax({
            type: "POST",
            data: {'userid': userid},
            url: "<?= base_url('messages/getVisitorById'); ?>",
            success: function (data)
            {
                if (data != 'false')
                {
                    //console.log(data);
                    $.each(JSON.parse(data), function (index, value) {
                        user = value.name;
                        email = value.email;
                        phone = value.phone;
                        ip = value.ip;
                        /*message_from = '<?php /* echo base_url(); */ ?>';*/
                        message_from = value.from_address;
                        city = value.city;
                        country = value.country;
                        currency = value.currency;
                        others = value.others;
                        date = value.created_at;
                        is_login = value.is_login;

                        var visitor_title = '';
                        if(user != '' && user != null){
                            visitor_title = user + ' (Visitor)';
                        } else {
                            visitor_title = 'Visitor';
                        }
                        if(is_login == '0'){
                            visitor_title += ' <i class="pull-right fa fa-circle-o" style="font-size: 10px"></i> - Left ';
                        } else {
                            visitor_title += ' <i class="pull-right fa fa-circle text-green" style="font-size: 10px"></i>';
                        }
                            
                        $("#" + userid + " h3").html(visitor_title);
                        $(".user-details").html('<li><a href="#">User<span class="pull-right text-red">' + user + '</span></a></li><li><a href="#">Email<span class="pull-right text-red">' + email + '</span></a></li><li><a href="#">Phone<span class="pull-right text-yellow">' + phone + '</span></a></li><li><a href="#">IP<span class="pull-right text-yellow">' + ip + '</span></a></li><li><a href="#">From<span class="pull-right text-yellow">' + message_from + '</span></a></li><li><a href="#">City<span class="pull-right text-yellow">' + city + '</span></a></li><li><a href="#">Country<span class="pull-right text-yellow">' + country + '</span></a></li><li><a href="#">Currency<span class="pull-right text-yellow">' + currency + '</span></a></li><li><a href="#">Others<span class="pull-right text-yellow">' + others + '</span></a></li><li><a href="#">Date <span class="pull-right text-green">' + date + '</span></a></li>');
                        $(".user_details").html('Visitor Details')

                    });
                }
            }
        });
    }

    //function for display visitor details in popup
    function get_visitor_info_popup(userid)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        $.ajax({
            type: "POST",
            data: {'userid': userid},
            url: "<?= base_url('messages/getVisitorById'); ?>",
            success: function (data)
            {
                if (data != 'false')
                {
                    //console.log(data);
                    $.each(JSON.parse(data), function (index, value) {
                        user = value.name;
                        email = value.email;
                        phone = value.phone;
                        ip = value.ip;
                        /*message_from = '<?php /* echo base_url(); */ ?>';*/
                        message_from = value.from_address;
                        city = value.city;
                        country = value.country;
                        currency = value.currency;
                        date = value.created_at;
                        is_login = value.is_login;

                        var visitor_title = '';
                        if(user != '' && user != null){
                            visitor_title = user + ' (Visitor)';
                        } else {
                            visitor_title = 'Visitor';
                        }
                        if(is_login == '0'){
                            visitor_title += ' <i class="pull-right fa fa-circle-o" style="font-size: 10px"></i> - Left ';
                        } else {
                            visitor_title += ' <i class="pull-right fa fa-circle text-green" style="font-size: 10px"></i>';
                        }
                            
                        $("#" + userid + " h3").html(visitor_title);
                        $("#" + userid + " .user-details").html('<div>User<span class="pull-right text-red">' + user + '</span></div><div>Email<span class="pull-right text-red">' + email + '</span></div><div>Phone<span class="pull-right text-yellow">' + phone + '</span></div><div>IP<span class="pull-right text-yellow">' + ip + '</span></div><div>From<span class="pull-right text-yellow">' + message_from + '</span></div><div>City<span class="pull-right text-yellow">' + city + '</span></div><div> Country<span class="pull-right text-yellow">' + country + '</span></div><div>Currency<span class="pull-right text-yellow">' + currency + '</span></div><div>Date <span class="pull-right text-green">' + date + '</span></div>');

                    });
                }
            }
        });
    }

    function get_unread_msg(userid)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        return $.ajax({
            type: "POST",
            data: {'userid': userid},
            url: "<?= base_url('messages/getUnreadMsg'); ?>"
        });
    }

    function get_messages(userid, newmsg = 0)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        $.ajax({
            type: "POST",
            data: {'lastid': lastTimeID, 'userid': userid},
            url: "<?= base_url('messages/getUserMsg'); ?>",
            success: function (data)
            {
                if (data != 'false')
                {
                    var oldmsg = $("#" + userid + " .direct-chat-messages").html();
                    if (newmsg == 1 && oldmsg != "")
                    {
                        $("#" + userid + " .direct-chat-messages").html("");
                    }
                    //console.log(data);
                    $.each(JSON.parse(data), function (index, value) {

                        from_id = value.from_id;
                        to_id = value.to_id;
                        from_name = value.from_name;
                        to_name = value.to_name;
                        from_table = value.from_table;
                        message = value.message;
                        file = value.file;
                        date = value.date_time;
                        image = value.image;
                        lastTimeID = value.id;

                        formattedDate = getDateString(new Date(value.date_time), "d M y H:m:s");
                        if (message == "" || message == null)
                        {
                            message = "<a class='chatfilecreceive' target='_blank' href='<?php echo base_url(); ?>file/download/" + file + "'>" + file + "</a>";
                        }
                        if (to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
                        {
                            $("#" + from_id + " .direct-chat-messages").prepend('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 1"> ' + message + ' </div></div>');
                            $('#txt_' + from_id).trigger('focus');
                        } else
                        {
                            $("#" + to_id + " .direct-chat-messages").prepend('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">' + from_name + '</span><span class="direct-chat-timestamp pull-left">' + formattedDate + '</span></div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"><div class="direct-chat-text 2"><a title="Double-click To Save This." class="msg_save" style="text-decoration:none;" data-toggle="popover" data-trigger="hover" data-content="Some content">' + message + '</a></div></div>');
                            $('#txt_' + to_id).trigger('focus');

                        }
                    });
                    $("#" + userid + " .direct-chat-messages").scroll();
                    $("#" + userid + " .direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                }
            }
        });
    }

    function get_visitor_messages(session_id, newmsg = 0, name)
    {
        $.ajax({
            type: "POST",
            data: {'session_id': session_id},
            url: "<?= base_url('messages/getVisitorMsg'); ?>",
            success: function (data)
            {
                if (data != 'false')
                {
                    $('h3').attr('visitor-id', session_id);
                    var oldmsg = $("#" + session_id + " .direct-chat-messages").html();
                    if (newmsg == 1 && oldmsg != "")
                    {
                        $("#" + session_id + " .direct-chat-messages").html("");
                    }
                    //console.log(data);
                    $.each(JSON.parse(data), function (index, value) {

                        from_id = value.from_id;
                        to_id = value.to_id;
                        from_name = value.from_name;
                        to_name = value.to_name;
                        from_table = value.from_table;
                        message = value.message;
                        file = value.file;
                        date = value.date_time;
                        image = value.image;
                        lastTimeID = value.id;

                        formattedDate = getDateString(new Date(value.date_time), "d M y H:m:s");
                        if (message == "" || message == null)
                        {
                            message = "<a class='chatfilecreceive' target='_blank' href='<?php echo base_url(); ?>file/download/" + file + "'>" + file + "</a>";
                        }

                        if (from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?> && from_table == 'admin')
                        {
                            $("#" + session_id + " .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">' + value.name + '</span><span class="direct-chat-timestamp pull-left">' + formattedDate + '</span></div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"><div class="direct-chat-text 3">' + message + '</div></div>');
                            /*$("#"+session_id+" .direct-chat-messages").scroll();
                             $("#"+session_id+" .direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
                             $('#txt_visitor_'+session_id).trigger('focus');*/
                        } else
                        {
                            $("#" + session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + value.name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 4"> ' + message + ' </div></div>');
                            /*$("#"+session_id+" .direct-chat-messages").scroll();
                             $("#"+session_id+" .direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
                             $('#txt_visitor_'+session_id).trigger('focus');*/
                        }
                    });

                    $("#" + session_id + " .direct-chat-messages").scroll();
                    $("#" + session_id + " .direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                    $('#txt_' + session_id).trigger('focus');
                    $('#txt_visitor_' + session_id).trigger('focus');
                }
            }
        });
    }

    function msg_popup(id, name, lastid) {
        if (typeof (lastid) === 'undefined')
            lastid = 0;
        close_popup(id);
        var element = '<div class="chat-popup" id="' + id + '"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">' + name + '</h3> <div class="box-tools pull-right"></div></div><div class="box-body"> <div class="direct-chat-messages"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_' + id + '" userid="' + id + '" fromtable="admin" totable="admin" onfocus="set_read_msgs(' + id + ')" onkeyup="checkenter(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send msg_text_suggestion_' + id + ' 3"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn send_agent_btn" id="btn_' + id + '" >Send</button> </span> </div></div></div></div>';
        document.getElementById("chatwin").innerHTML = element;
        get_messages(id);
        get_staff_info(id);
        var hgt = $("#" + id + " .direct-chat-messages").height();
        $("#" + id + " .direct-chat-messages").animate({scrollTop: hgt * 10}, 500);
        $('#txt_' + id).trigger('focus');
        
        /*---------- Chat Template Auto Complete -------------------*/
        var chatTemplateBloodhound = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.text_suggestion);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: $.core.url('app/load_text_suggetion/%QUERY'),
                wildcard: '%QUERY'
            }
        });
        chatTemplateBloodhound.initialize();

        var $el = $('input.msg_text_suggestion_' + id)
        $el.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'text_suggestion',
            displayKey: 'text_suggestion',
            source: chatTemplateBloodhound.ttAdapter(),
            templates: {
                suggestion: function (data) {
                    return '<p><strong>' + data.text_suggestion + '</strong></p>';
                }
            }
        });
        $('#txt_' + id).val(null).trigger('change');
        /*---------- Chat Template Auto Complete -------------------*/
        
        return true;
    }

    function visitor_msg_popup(id, name, lastid) {
        if (typeof (lastid) === 'undefined')
            lastid = 0;
        close_popup(id);
        var element = '<div class="chat-popup" id="' + id + '"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">' + name + '</h3><div class="box-tools pull-right"><a href="javascript:close_popup(' + id + ',1,1);" class="btn btn-sm btn-warning btn-flat">Close Chat</a></div></div><div class="box-body"> <div class="direct-chat-messages"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_visitor_' + id + '" fromid="<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>" toid="' + id + '" fromtable="admin" totable="visitors"  onfocus="set_visitor_read_msgs(' + id + ')" onkeyup="checkenterfrmvisitor(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send msg_vc_text_suggestion_' + id + ' 4"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn send_visitor_btn" id="btn_' + id + '">Send</button> </span> </div></div></div></div>';
        document.getElementById("chatwin").innerHTML = element;
        get_visitor_messages(id);
        get_visitor_info(id);
        var hgt = $("#" + id + " .direct-chat-messages").height();
        $("#" + id + " .direct-chat-messages").animate({scrollTop: hgt * 10}, 500);
        $('#txt_visitor_' + id).trigger('focus');
        
        /*---------- Visitor Chat Template Auto Complete -------------------*/
        var visitorChatTemplateBloodhound = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.text_suggestion);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: $.core.url('app/load_text_suggetion/%QUERY'),
                wildcard: '%QUERY'
            }
        });
        visitorChatTemplateBloodhound.initialize();

        var $el = $('input.msg_vc_text_suggestion_' + id)
        $el.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'text_suggestion',
            displayKey: 'text_suggestion',
            source: visitorChatTemplateBloodhound.ttAdapter(),
            templates: {
                suggestion: function (data) {
                    return '<p><strong>' + data.text_suggestion + '</strong></p>';
                }
            }
        });
        $('#txt_visitor_' + id).val(null).trigger('change');
        /*---------- Chat Template Auto Complete -------------------*/
        
        return true;
    }

    //calculate the total number of popups suitable and then populate the toatal_popups variable.
    function calculate_popups() {
        var width = window.innerWidth;
        if (width < 540) {
            total_popups = 0;
        } else {
            width = width - 0;
            //320 is width of a single popup box
            total_popups = parseInt(width / 320);
        }
        display_popups();
    }
    function checkenter(obj, e)
    {
        if (e.keyCode == 13) {
            sendmsg(obj)
        }
    }
    function checkenterfrmvisitor(obj, e)
    {
        if (e.keyCode == 13) {
            sendmsgtovisitor(obj)
        }
    }
    function sendmsg(obj) {
        var to_id = $(obj).attr("userid");
        var from_table = $(obj).attr("fromtable");
        var to_table = $(obj).attr("totable");
        if (to_table == "visitors") {
            var msg = $('#txt_visitor_' + to_id).val();
        } else {
            var msg = $('#txt_' + to_id).val();
        }
        if (msg != '')
        {
            // we have commented the code for adding message after hittinh enter,it will automatically display as functino call for new message
            if (to_table == "visitors") {
                $('#txt_visitor_' + to_id).val('');
                send_visitor_msg(to_id, msg, from_table, to_table);
            } else {
                $('#txt_' + to_id).val('');
                sendchatmsg(to_id, msg, from_table, to_table);
            }
        }
    }
    function sendmsgtovisitor(obj) {
        var to_id = $(obj).attr("toid");
        var from_table = $(obj).attr("fromtable");
        var to_table = $(obj).attr("totable");
        if (to_table == "visitors") {
            var msg = $('#txt_visitor_' + to_id).val();
        } else {
            var msg = $('#txt_' + to_id).val();
        }
        if (msg != '')
        {
            // we have commented the code for adding message after hittinh enter,it will automatically display as functino call for new message
            if (to_table == "visitors") {
                $('#txt_visitor_' + to_id).val('');
                send_visitor_msg(to_id, msg, from_table, to_table);
            } else {
                $('#txt_' + to_id).val('');
                sendchatmsg(to_id, msg, from_table, to_table);
            }
        }
    }

    var getDateString = function (date, format) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                getPaddedComp = function (comp) {
                    return ((parseInt(comp) < 10) ? ('0' + comp) : comp)
                },
                formattedDate = format,
                o = {
                    "y+": date.getFullYear(), // year
                    "M+": months[date.getMonth()], //month
                    "d+": getPaddedComp(date.getDate()), //day
                    "h+": getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
                    "H+": getPaddedComp(date.getHours()), //hour
                    "m+": getPaddedComp(date.getMinutes()), //minute
                    "s+": getPaddedComp(date.getSeconds()), //second
                    "S+": getPaddedComp(date.getMilliseconds()), //millisecond,
                    "b+": (date.getHours() >= 12) ? 'PM' : 'AM'
                };

        for (var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                formattedDate = formattedDate.replace(RegExp.$1, o[k]);
            }
        }
        return formattedDate;
    };
    function sendchatmsg(to_id, msg, from_table, to_table)
    {
        $.ajax({
            type: "POST",
            data: {'to_id': to_id, 'msg': msg, 'from_table': from_table, 'to_table': to_table},
            url: "<?= base_url('chat/addAdminChat'); ?>",
            success: function (data)
            {
                $("#" + to_id + " .direct-chat-messages").scroll();
                $("#" + to_id + " .direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                get_messages(to_id, 1);
                return true;
            }
        });
    }


    socket.on('close_visitor_popup', function (data) {
        if (data.from_id != '<?= $this->session->userdata("is_logged_in")["staff_id"] ?>') {
            close_popup(data.to_session_id, 1, 0, 1);
        }
        return false;
    });

    function send_visitor_msg(to_id, msg, from_table, to_table) {
        var formdata = new FormData();
        formdata.append("to_id", to_id);
        formdata.append("msg", msg);
        formdata.append("from_table", from_table);
        formdata.append("to_table", to_table);
        $.ajax({
            type: "POST",
            data: formdata,
            url: "<?= base_url('chat/send_visitor_msg'); ?>",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                if (data.success = true) {
                }
            }
        });
    }

    function chckuserpopup(id)
    {
        var f = 0;
        for (var iii = 0; iii < popups.length; iii++) {
            //already registered. Bring it to front.
            if (id == popups[iii]) {
                Array.remove(popups, iii);
                popups.unshift(id);
                calculate_popups();
                f = 1;
            }
        }
        return f;
    }

    //recalculate when window is loaded and also when window is resized.
    window.addEventListener("resize", calculate_popups);
    window.addEventListener("load", calculate_popups);
    var lastTimeID = 0;

    function get_chat(userid)
    {
        if (typeof (userid) === 'undefined')
            userid = 0;
        $.ajax({
            type: "POST",
            data: {'lastid': lastTimeID, 'userid': userid},
            url: "<?= base_url('chat/getAdminChat'); ?>",
            success: function (data) {
                if (data != 'false')
                {
                    console.log(data);
                    $.each(JSON.parse(data), function (index, value) {
                        from_id = value.from_id;
                        to_id = value.to_id;
                        from_name = value.from_name;
                        to_name = value.to_name;
                        from_table = value.from_table;
                        message = value.message;
                        image = value.image;
                        file = value.file;
                        date = value.date_time;
                        lastTimeID = value.id;
                        isread = value.is_read;
                        formattedDate = getDateString(new Date(value.date_time), "d M y H:m:s");
                        if (message == "" || message == null) {
                            message = "<a target='_blank' href='uploads/" + file + "'>" + file + "</a>";
                        }
                        //check if message is send by current user, then check to_id and if that id popup is open then add message else open popup
                        //alert(isread + "-" + message);
                        if (to_id == "") {
                            chkpopupval = chckuserpopup(from_id);
                            if (chkpopupval) {
                                $("#" + from_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 5"> ' + message + ' </div></div>');
                                $('.direct-chat-messages').scroll();
                                $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                            } else {
                                if (isread == 0) {
                                    register_popup(from_id, from_name, lastTimeID, 1);
                                    get_messages(from_id, 1);
                                } else {
                                    $("#" + from_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 6"> ' + message + ' </div></div>');
                                    $('.direct-chat-messages').scroll();
                                    $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                                }
                            }
                        } else {
                            if (from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
                            {
                                chkpopupval = chckuserpopup(to_id);
                                if (chkpopupval) {
                                    $("#" + to_id + " .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">' + from_name + '</span><span class="direct-chat-timestamp pull-left">' + formattedDate + '</span></div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"><div class="direct-chat-text 7">' + message + '</div></div>');
                                    $('.direct-chat-messages').scroll();
                                    $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                                } else {
                                    if (isread == 0) {
                                        register_popup(to_id, to_name, lastTimeID, 1);
                                        get_messages(to_id, 1);
                                    } else {
                                        $("#" + to_id + " .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">' + from_name + '</span><span class="direct-chat-timestamp pull-left">' + formattedDate + '</span></div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"><div class="direct-chat-text 8">' + message + '</div></div>');
                                        $('.direct-chat-messages').scroll();
                                        $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    function get_visitor_chat() {
        $.ajax({
            type: "POST",
            data: {'lastid': lastTimeID},
            url: "<?= base_url('chat/getBackendSideVisitorChat'); ?>",
            success: function (data)
            {
                if (data != 'false')
                {
                    //console.log(data);
                    $.each(JSON.parse(data), function (index, value) {
                        from_id = value.from_id;
                        to_id = value.to_id;
                        from_name = value.name;
                        to_name = value.to_name;
                        from_table = value.from_table;
                        message = value.message;
                        image = value.image;
                        file = value.file;
                        date = value.date_time;
                        lastTimeID = value.id;
                        isread = value.is_read;
                        name = value.name;
                        session_id = value.session_id
                        formattedDate = getDateString(new Date(value.date_time), "d M y H:m:s");
                        if (message == "" || message == null)
                        {
                            message = "<a target='_blank' href='uploads/" + file + "'>" + file + "</a>";
                        }
                        //check if message is send by current user, then check to_id and if that id popup is open then add message else open popup
                        //if(to_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
                        if (from_table == 'visitors')
                        {
                            chkpopupval = chckuserpopup(session_id);
                            if (chkpopupval)
                            {
                                $("#" + session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 9"> ' + message + ' </div></div>');
                                $('.direct-chat-messages').scroll();
                                $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                            } else
                            {
                                if (isread == 0)
                                {
                                    visitor_register_popup(session_id, name, lastTimeID, 1);
                                    get_visitor_messages(session_id, 1, name);
                                } else
                                {
                                    $("#" + session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + from_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 10"> ' + message + ' </div></div>');
                                    $('.direct-chat-messages').scroll();
                                    $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                                }

                            }
                        } else
                        {
                            chkpopupval = chckuserpopup(session_id);
                            if (chkpopupval)
                            {
                                $("#" + session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + to_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 11"> ' + message + ' </div></div>');
                                $('.direct-chat-messages').scroll();
                                $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                            } else
                            {
                                if (isread == 0)
                                {
                                    visitor_register_popup(session_id, name, lastTimeID, 1);
                                    //get_visitor_messages(from_id,1,name);
                                    get_visitor_messages(session_id, 1, name);
                                } else
                                {
                                    $("#" + session_id + " .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">' + to_name + '</span> <span class="direct-chat-timestamp pull-right">' + formattedDate + '</span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/' + image + '" alt="message user image"> <div class="direct-chat-text 12"> ' + message + ' </div></div>');
                                    $('.direct-chat-messages').scroll();
                                    $(".direct-chat-messages").animate({scrollTop: 2000000000}, 1000);
                                }

                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * @param $selector
     * @constructor
     */
    function initAjaxSelect2($selector, $source_url)
    {
        $selector.select2({
            placeholder: " --Select-- ",
            allowClear: true,
            width: "100%",
            ajax: {
                url: $source_url,
                dataType: 'json',
                delay: 250,
                async: false,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * 5) < data.total_count
                        }
                    };
                },
                cache: true
            }
        });
    }

    function setSelect2Value($selector, $source_url = '')
    {
        if ($source_url != '') {
            $.ajax({
                url: $source_url,
                type: "GET",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data.success == true) {
                        $selector.empty().append($('<option/>').val(data.id).text(data.text)).val(data.id).trigger("change");
                    }
                }
            });
        } else {
            $selector.empty().append($('<option/>').val('').text('--select--')).val('').trigger("change");
    }
    }
    
    function setSelect2MultiValue($selector,$source_url = '')
    {
        if($source_url != '') {
            $.ajax({
                url: $source_url,
                type: "GET",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
						var selectValues = data[0];
						$.each(selectValues, function(key, value) {   
							$selector.select2("trigger", "select", {
								data: value
							});
						});
                    }
                }
            });
        } else {
            $selector.empty().append($('<option/>').val('').text('--select--')).val('').trigger("change");
        }
    }
    
    function goBack() {
        window.history.back();
    }
    var date = new Date();
    date.setDate(date.getDate() - 1);
    $(function () {
        $('.committed-datepicker').datepicker({
            startDate: date,
            format: 'dd-mm-yyyy',
            todayBtn: "linked",
            todayHighlight: true,
            autoclose: true
        });
    });

    function strWordWrap(str, maxWidth) {
        var newLineStr = "\n";
        done = false;
        res = '';
        do {
            found = false;
            // Inserts new line at first whitespace of the line
            for (i = maxWidth - 1; i >= 0; i--) {
                if (testWhite(str.charAt(i))) {
                    res = res + [str.slice(0, i), newLineStr].join('');
                    str = str.slice(i + 1);
                    found = true;
                    break;
                }
            }
            // Inserts new line at maxWidth position, the word is too long to wrap
            if (!found) {
                res += [str.slice(0, maxWidth), newLineStr].join('');
                str = str.slice(maxWidth);
            }

            if (str.length < maxWidth)
                done = true;
        } while (!done);

        return res + str;
    }
    function testWhite(x) {
        var white = new RegExp(/^\s$/);
        return white.test(x.charAt(0));
    }
    ;

    function display_as_a_viewpage() {
        $("form :input").prop("disabled", true);
        $("form :input").css("background-color", '#fff');
        $("form :input").css("border", 'none');
        $("form :input").css("box-shadow", 'none');
        $("form .select2-selection").css("background-color", '#fff !important');
        $("form .select2-selection").css("border", 'none');
        $("form .select2-selection").css("box-shadow", 'none');
        $("form .select2-selection__arrow").css("display", 'none');
        $(".btn.btn-primary").remove();
        $(".btn").remove();
    }

    socket.on('staff_is_left', function (data) {
        var user = data.name;
        var staff_title = user;
        staff_title += ' <i class="pull-right fa fa-circle-o" style="font-size: 10px"></i> - Left ';
        $("#" + data.staff_id + " h3").html('');
        $("#" + data.staff_id + " h3").html(staff_title);
    });
    
    socket.on('staff_is_back', function (data) {
        var user = data.name;
        var staff_title = user;
        staff_title += ' <i class="pull-right fa fa-circle text-green" style="font-size: 10px"></i>';
        $("#" + data.staff_id + " h3").html('');
        $("#" + data.staff_id + " h3").html(staff_title);
    });

    socket.on('visitor_is_left', function (data) {
        var user = data.name;
        var visitor_title = '';
        if(user != '' && user != null){
            visitor_title = user + ' (Visitor)';
        } else {
            visitor_title = 'Visitor';
        }
        visitor_title += ' <i class="pull-right fa fa-circle-o" style="font-size: 10px"></i> - Left ';
        $("#" + data.session_id + " h3").html('');
        $("#" + data.session_id + " h3").html(visitor_title);
    });
    
    socket.on('visitor_is_back', function (data) {
        var user = data.name;
        var visitor_title = '';
        if(user != '' && user != null){
            visitor_title = user + ' (Visitor)';
        } else {
            visitor_title = 'Visitor';
        }
        visitor_title += ' <i class="pull-right fa fa-circle text-green" style="font-size: 10px"></i>';
        $("#" + data.session_id + " h3").html('');
        $("#" + data.session_id + " h3").html(visitor_title);
    });
    
    $('body').on('keydown', 'input,select,.select2-search__field, textarea', function(e) {
        var self = $(this)
          , form = self.parents('form:eq(0)')
          , focusable
          , next
          , prev
          ;

        var id = $(this).attr('id');
        if(id == 'add_lineitem'){
            $('#add_lineitem').click();
        } else if (e.shiftKey) {
            if (e.keyCode == 13 && $(this).is("textarea") == false) {
                focusable =   form.find('input,a,select,.select2-search__field,button,textarea').filter(':visible');
                prev = focusable.eq(focusable.index(this)-1); 

                if (prev.length) {
                   prev.focus();
                } else {
                    form.submit();
                }
            }
        } else if (e.keyCode == 13 && $(this).is("textarea") == false) {
            focusable = form.find('input,a,select,.select2-search__field,button,textarea').filter(':visible');
            next = focusable.eq(focusable.index(this)+1);
            if (next.length) {
                next.focus();
            } else {
                form.submit();
            }
            return false;
        }
    });
    
    /**
        * WARNING: untested using Select2's option ['selectOnClose'=>true]
        *
        * This code was written because the Select2 widget does not handle
        * tabbing from one form field to another.  The desired behavior is that
        * the user can use [Enter] to select a value from Select2 and [Tab] to move
        * to the next field on the form.
        *
        * The following code moves focus to the next form field when a Select2 'close'
        * event is triggered.  If the next form field is a Select2 widget, the widget
        * is opened automatically.
        *
        * Users that click elsewhere on the document will cause the active Select2
        * widget to close.  To prevent the code from overriding the user's focus choice
        * a flag is added to each element that the users clicks on.  If the flag is
        * active, then the automatic focus script does not happen.
        *
        * To prevent conflicts with multiple Select2 widgets opening at once, a second
        * flag is used to indicate the open status of a Select2 widget.  It was
        * necessary to use a flag instead of reading the class '--open' because using the
        * class '--open' as an indicator flag caused timing/bubbling issues.
        *
        * To simulate a Shift+Tab event, a flag is recorded every time the shift key
        * is pressed.
        */
        var docBody = $(document.body);
        var shiftPressed = false;
        var clickedOutside = false;
        //var keyPressed = 0;

        docBody.on('keydown', function(e) {
            var keyCaptured = (e.keyCode ? e.keyCode : e.which);
            //shiftPressed = keyCaptured == 16 ? true : false;
            if (keyCaptured == 16) { shiftPressed = true; }
        });
        docBody.on('keyup', function(e) {
            var keyCaptured = (e.keyCode ? e.keyCode : e.which);
            //shiftPressed = keyCaptured == 16 ? true : false;
            if (keyCaptured == 16) { shiftPressed = false; }
        });

        docBody.on('mousedown', function(e){
            // remove other focused references
            clickedOutside = false;
            // record focus
            if ($(e.target).is('[class*="select2"]')!=true) {
                clickedOutside = true;
            }
        });

        docBody.on('select2:opening', function(e) {
            // this element has focus, remove other flags
            clickedOutside = false;
            // flag this Select2 as open
            $(e.target).attr('data-s2open', 1);
        });
        docBody.on('select2:closing', function(e) {
            // remove flag as Select2 is now closed
            $(e.target).removeAttr('data-s2open');
        });

        docBody.on('select2:close', function(e) {
            var elSelect = $(e.target);
            elSelect.removeAttr('data-s2open');
            var currentForm = elSelect.closest('form');
            var othersOpen = currentForm.has('[data-s2open]').length;
            if (othersOpen == 0 && clickedOutside==false) {
                /* Find all inputs on the current form that would normally not be focus`able:
                 *  - includes hidden <select> elements whose parents are visible (Select2)
                 *  - EXCLUDES hidden <input>, hidden <button>, and hidden <textarea> elements
                 *  - EXCLUDES disabled inputs
                 *  - EXCLUDES read-only inputs
                 */
                var inputs = currentForm.find(':input:enabled:not([readonly], input:hidden, button:hidden, textarea:hidden)')
                    .not(function () {   // do not include inputs with hidden parents
                        return $(this).parent().is(':hidden');
                    });
                var elFocus = null;
                $.each(inputs, function (index) {
                    var elInput = $(this);
                    if (elInput.attr('id') == elSelect.attr('id')) {
                        if ( shiftPressed) { // Shift+Tab
                            elFocus = inputs.eq(index - 1);
                        } else {
                            elFocus = inputs.eq(index + 1);
                        }
                        return false;
                    }
                });
                if (elFocus !== null) {
                    // automatically move focus to the next field on the form
                    var isSelect2 = elFocus.siblings('.select2').length > 0;
                    if (isSelect2) {
                        elFocus.select2('open');
                    } else {
                        elFocus.focus();
                    }
                }
            }
        });

        /**
         * Capture event where the user entered a Select2 control using the keyboard.
         * http://stackoverflow.com/questions/20989458
         * http://stackoverflow.com/questions/1318076
         */
        docBody.on('focus', '.select2', function(e) {
            var elSelect = $(this).siblings('select');
            var test1 = elSelect.is('[disabled]');
            var test2 = elSelect.is('[data-s2open]');
            var test3 = $(this).has('.select2-selection--single').length;
            if (elSelect.is('[disabled]')==false && elSelect.is('[data-s2open]')==false
                && $(this).has('.select2-selection--single').length>0) {
                elSelect.attr('data-s2open', 1);
                elSelect.select2('open');
            }
        });

        function send_custom_twilio_whatsapp_message(from_number, to_number, msg_text){
            socket.emit('send_custom_twilio_whatsapp_message', {from_number: from_number, to_number: to_number, msg_text: msg_text});
        }
        socket.on('send_custom_twilio_whatsapp_message', function (data) {
    //        console.log(data);
            get_twilio_messages_in_conversation(data.conversation_radio);
        });
    //    function get_twilio_whatsapp_conversation_list(){
    //        socket.emit('get_twilio_whatsapp_conversation_list', {});
    //    }
    //    socket.on('get_twilio_whatsapp_conversation_list', function (data) {
    //        console.log(data);
    //        set_twilio_conversation_list(data);
    //    });
        function get_twilio_messages_in_conversation(from_number = ''){
            socket.emit('get_twilio_messages_in_conversation', {from_number: from_number});
        }
        socket.on('get_twilio_messages_in_conversation', function (data) {
            console.log(data);
            if(data.from_number != ''){
                set_twilio_messages_in_conversation(data);
            } else {
                set_twilio_conversation(data);
            }
        });
        function get_twilio_messages_media(accountSid, sid){
            socket.emit('get_twilio_messages_media', {accountSid: accountSid, sid:sid});
        }
        socket.on('get_twilio_messages_media', function (data) {
    //        console.log(data);
            $.each(data.response, function (index, value) {
                var media_uri = value.uri;
                media_uri = 'https://api.twilio.com' + media_uri.replace(".json", "");
                $('#'+value.accountSid+'_'+value.parentSid+'').attr('src', media_uri);
            });
        });

        function read_twilio_messages(from_number, user_id){
            if(from_number != ''){
                socket.emit('read_twilio_messages', {from_number: from_number, user_id: user_id});
            }
        }
        socket.on('read_twilio_messages', function (data) {
            if(data.user_id == '<?php echo $this->session->userdata('is_logged_in')['staff_id']; ?>'){
                console.log(data);
                if(data.from_number != ''){
                    set_twilio_messages_in_conversation(data);
                }
            }
        });
        socket.on('new_twilio_sendbox_message_comes', function (data) {
            var conversation_radio = $("input[name='conversation_radio']:checked").attr('data-conversation_id');
            if(conversation_radio != undefined){
                read_twilio_messages(conversation_radio, '<?php echo $this->session->userdata('is_logged_in')['staff_id']; ?>');
            }
        });
        socket.on('message_comes_in_webhook', function (data) {
            var conversation_radio = $("input[name='conversation_radio']:checked").attr('data-conversation_id');
            if(conversation_radio != undefined){
                get_twilio_messages_in_conversation(conversation_radio);
            }
        });

</script>
<!----------------- / Chat ----------------->

</body>
</html>
