<div class="content-wrapper">
	<section class="custom content-header"  >
		<?php
		$this->load->view('shared/success_false_notify');
		?>
		<h1>
			<small class="text-primary text-bold">Terms and Conditions : Puchase Order</small>
		</h1>
	</section>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Puchase Order T & C
				</div>
				<div class="panel-body">
					<div class="col-md-4">
						
					</div>

					<div class="col-md-12">
						
					</div>
					<form action="<?=base_url('master_purchase/order_terms')?>" method="post">
						<div class="col-md-12">
							<br/>
							<textarea id="editor1" name="details" <?= $this->app_model->have_access_role(PURCHASE_ORDER_TERMS_MENU_ID, "edit") ? '' : 'disabled' ?> rows="10" cols="80" style="visibility: hidden; display: none;"><?=$terms_detail;?></textarea>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-2">
							<br/>
							<?php if($this->app_model->have_access_role(PURCHASE_ORDER_TERMS_MENU_ID, "edit")) { ?>
							<button type="submit" class="btn btn-block btn-info btn-xs">Submit</button>
							<?php } ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script>
	$(document).ready(function(){
		CKEDITOR.replace('editor1');
	});
</script>
