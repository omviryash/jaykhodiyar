<style>
    tr.bordered {
        border-bottom: 1px solid #a3a3a3;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Chat</small>
        </h1>
    </section>
    <section class="content">
        <div class="box box-info" >
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3" style="background-color: #1d3559; height: 500px; padding: 0px;">
                            <table width="100%" style="margin-top: 10px;">
                                <tr style="background-color: #3f5677; color: white;">
                                    <td><i class="fa fa-diamond" style="color: white; margin-left: 10px;"></i></td>
                                    <td>Gangadharan<span class="pull-right" style="margin-right: 5px;">12:04</span><br/>The Machines.</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>