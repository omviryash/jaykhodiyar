<?php
if(count($result)!=0){
	$isDelete = $this->app_model->have_access_role(OPPORTUNITIES_LEAD_MODULE_ID,"delete");
?>
	<!--<div class="table-responsive">-->
		<table class="table table-search-leads custom-table">
			<thead>
			<tr>
				<th>Lead No.</th>
				<th>Party Name.</th>
				<th>Lead Title</th>
				<th>Start Date</th>
				<!--<th>End Date</th>-->
				<th>Lead Owner</th>
				<th>Assigned To</th>
				<th>Stage</th>
				<?php
				if($isDelete){
					?>
					<th>Edit</th>
					<?php
				}
				?>
			</tr>
			</thead>
			<tbody>
			<?php for($i=0;$i<count($result);$i++) { ?>
				<tr id="lead_search_row_<?php echo $result[$i]->id ?>">
					<td><?php echo $result[$i]->lead_no ?></td>
					<td><?php echo $result[$i]->party_name ?></td>
					<td><?php echo $result[$i]->lead_title ?></td>
					<td><?php echo date('d-m-Y',strtotime($result[$i]->start_date)); ?></td>
					<!--<td><?php /*if(strtotime($result[$i]->end_date) > 0){echo date('d-m-Y',strtotime($result[$i]->end_date));} */?></td>-->
					<td><?php echo $this->crud->get_column_value_by_id('staff','name',array('staff_id'=>$result[$i]->lead_owner_id)); ?></td>
					<td><?php echo $this->crud->get_column_value_by_id('staff','name',array('staff_id'=>$result[$i]->assigned_to_id)); ?></td>
					<td><?php echo $this->crud->get_column_value_by_id('lead_stage','lead_stage',array('id'=>$result[$i]->lead_stage_id)); ?></td>
					<?php
					if($isDelete){
						?>
						<td>
							<a href="javascript:void(0)" class="btn btn-xs btn-primary" onclick="edit_leads('<?php echo $result[$i]->id ?>')"><span class="glyphicon glyphicon-edit"></span></a>
							<a href="javascript:void(0)" class="btn btn-xs btn-danger" onclick="delete_leads('<?php echo $result[$i]->id ?>')"><span class="glyphicon glyphicon-remove"></span></a>
						</td>
						<?php
					}
					?>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<?php } else { ?>
			No Data Found
		<?php } ?>
	<!--</div>-->
<script>
	$(".table-search-leads").dataTable();
</script>
