<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Sales : Opportunities (Leads)
                <!-- <a onclick="submitform()" href="javascript:void(0)"> <span class="glyphicon glyphicon-plus"></span>  </a> --> </small>
            <button type="button" class="btn btn-info btn-xs pull-right btn-save-leads" style="margin: 5px;">
                Save Lead
            </button>
            <button type="button" class="btn btn-info btn-xs pull-right btn_load_party" data-toggle="modal"
                    data-target="#lead_search" style="margin: 5px;">Search Leads
            </button>
            <a href="<?=BASE_URL?>sales" class="btn btn-success btn-xs pull-right" style="margin: 5px;">Add Lead</a> 
        </h1>
    </section>

    <!-- Modal -->
    <div id="lead_search" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search Leads</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal Form -->
                            <div class="box box-info search-leads-form ">
                                <form class="form-horizontal">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 control-label input-sm">Lead Source</label>
                                                    <div class="col-sm-7">
                                                        
                                                        <select name="lead_source_s" id="lead_source_s" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                <?php foreach($lead_source as $ls): ?>
                                                                    <option value="<?php echo $ls->id; ?>"><?php echo $ls->lead_source; ?></option>
                                                                <?php endforeach; ?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 control-label input-sm">Lead Status</label>
                                                    <div class="col-sm-7">
                                                        <select name="lead_status_s" id="lead_status_s" class="form-control input-sm select2">
                                                            <?php foreach($lead_status as $ls): ?>
                                                                <option value="<?php echo $ls->id; ?>" <?= strtolower($ls->lead_status) == 'open'?'selected="selected"':''?>><?php echo $ls->lead_status; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 control-label input-sm">Industry Type</label>
                                                    <div class="col-sm-7">
                                                        
                                                        <select name="industry_type_s" id="industry_type_s" class="form-control input-sm select2">
                                                            <option value="">--Select--</option>
                                                            <?php foreach($industry_type as $it): ?>
                                                                <option value="<?php echo $it->id; ?>"><?php echo $it->industry_type; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 control-label input-sm">Lead Owner</label>
                                                    <div class="col-sm-7">
                                                        <select name="lead_owner_s" id="lead_owner_s" class="form-control input-sm select2">
                                                            <option value="">--Select--</option>
                                                            <?php foreach($lead_owner as $lo): ?>
                                                                <option value="<?php echo $lo->staff_id; ?>"><?php echo $lo->name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 control-label input-sm">Lead Assigned To</label>
                                                    <div class="col-sm-7">
                                                        
                                                        <select name="assigned_to_s" id="assigned_to_s" class="form-control input-sm select2">
                                                            <option value="">--Select--</option>
                                                            <?php foreach($lead_owner as $lo): ?>
                                                                <option value="<?php echo $lo->staff_id; ?>"><?php echo $lo->name; ?></option>
                                                            <?php endforeach; ?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-5 control-label input-sm">Created
                                                        By</label>
                                                    <div class="col-sm-7">
                                                        <select name="filter_creator_by" id="filter_creator_by" class="form-control input-sm select2">
                                                            <?php foreach($lead_owner as $lo): ?>
                                                                <option value="<?php echo $lo->staff_id; ?>"<?php if($this->session->userdata('is_logged_in')['staff_id'] == $lo->staff_id){	echo 'selected';}?>><?php echo $lo->name; ?></option>
                                                            <?php endforeach; ?>
                                                            <option value="all">all</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button id="search_result" type="button"
                                                        class="btn btn-xs btn-default pull-right">
                                                    <span class="glyphicon glyphicon-search"></span> Search
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br/>
                                            <div class="col-md-12" id="result_search_leads">

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <!-- <div class="box-footer">
                                      <button type="submit" class="btn btn-info pull-right">Sign in</button>
                                    </div> -->
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <form action="<?= base_url('sales/save_leads') ?>" class="" id="lead_form" name="lead_form" method="post">
                <input type="hidden" name="lead_id" id="lead_id" value="0">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Lead Details</a></li>
<!--                        <li><a href="#tab_2" data-toggle="tab">Party Details</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Contact Person</a></li>-->
                        <li><a href="#tab_4" data-toggle="tab">Sales Activity</a></li>
                        <li><a href="#tab_5" data-toggle="tab">Item Details</a></li>
                        <li><a href="#tab_6" data-toggle="tab">Company Profile</a></li>
                        <li><a href="#tab_7" data-toggle="tab">Letter</a></li>
                        <li><a href="#tab_8" data-toggle="tab">Reports</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Lead Details
                                                </legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead No.</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="lead_no" name="lead_data[lead_no]" disabled="disabled" value="<?=$lead_no?>">
                                                        </div>
                                                        <div class="col-md-4 text-right">
                                                            <button type="button" class="btn btn-info btn-xs">Repeat Sales Lead </button>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead
                                                            Title</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm"  id="lead_title" name="lead_data[lead_title]">
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead Start Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm pull-right input-datepicker" name="lead_data[start_date]" value="<?= date('d-m-Y'); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead End Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm pull-right disabled" name="lead_data[end_date]" disabled="disabled">
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead
                                                            Details</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control" rows="5" name="lead_data[lead_detail]" id="lead_detail"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Industry
                                                            Type</label>
                                                        <div class="col-sm-9">
                                                            <select name="lead_data[industry_type_id]" id="industry_type_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                    <?php foreach($industry_type as $it): ?>
                                                                        <option value="<?php echo $it->id; ?>"><?php echo $it->industry_type; ?></option>
                                                                    <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead Owner</label>
                                                        <div class="col-sm-9">
                                                            <select name="lead_data[lead_owner_id]" id="lead_owner_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                    <?php foreach($lead_owner as $lo): ?>
                                                                        <option value="<?= $lo->staff_id; ?>"<?php if($this->session->userdata('is_logged_in')['staff_id'] == $lo->staff_id){echo 'selected';}?>><?= $lo->name; ?></option>
                                                                    <?php endforeach; ?> 
                                                            </select>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Assigned
                                                            To</label>
                                                        <div class="col-sm-9">
                                                            <select name="lead_data[assigned_to_id]" id="assigned_to_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                    <?php foreach($lead_owner as $lo): ?>
                                                                        <option value="<?= $lo->staff_id; ?>"<?php if($this->session->userdata('is_logged_in')['staff_id'] == $lo->staff_id){echo 'selected';}?>><?= $lo->name; ?></option>
                                                                    <?php endforeach; ?>
                                                            </select>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3"
                                                               class="col-sm-3 input-sm">Priority</label>
                                                        <div class="col-sm-9">
                                                            <select name="lead_data[priority_id]" id="priority_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                <?php foreach($priority as $pri): ?>
                                                                    <option value="<?php echo $pri->id; ?>"><?php echo $pri->priority; ?></option>
                                                                <?php endforeach; ?> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead
                                                            Source</label>
                                                        <div class="col-sm-9">
                                                            <select name="lead_data[lead_source_id]" id="lead_source_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                <?php foreach($lead_source as $ls): ?>
                                                                    <option value="<?php echo $ls->id; ?>"><?php echo $ls->lead_source; ?></option>
                                                                <?php endforeach; ?> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                   <!-- <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead
                                                            Stage</label>
                                                        <div class="col-sm-9">
                                                            <select name="lead_data[lead_stage_id]" id="lead_stage_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                <?php foreach($lead_stage as $ls): ?>
                                                                    <option value="<?php echo $ls->id; ?>"><?php echo $ls->lead_stage; ?></option>
                                                                <?php endforeach; ?> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div> -->

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Lead
                                                            Provider</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="lead_data[lead_provider_id]" id="lead_provider_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                <?php foreach($lead_provider as $lp): ?>
                                                                    <option value="<?php echo $lp->id; ?>"><?php echo $lp->lead_provider; ?></option>
                                                                <?php endforeach; ?> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3"
                                                               class="col-sm-3 input-sm">Status</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="lead_data[lead_status_id]" id="lead_status_id" class="form-control input-sm select2">
                                                                <?php foreach($lead_status as $ls): ?>
                                                                    <option value="<?php echo $ls->id; ?>" <?= strtolower($ls->lead_status) == 'open'?'selected="selected"':''?>><?php echo $ls->lead_status; ?></option>
                                                                <?php endforeach; ?> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">Review Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm pull-right input-datepicker" id="" name="lead_data[review_date]" value="<?= date('d-m-Y'); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset class="scheduler-border">
                                                        <legend class="scheduler-border text-primary text-bold">
                                                            Customer Details
                                                        </legend>
                                                        <input type="hidden" class="form-control input-sm party_id" name="lead_data[party_id]" value="0">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">Party Name</label>
                                                                <div class="col-sm-9">
                                                                    <select name="party_data[party_code]" id="party_code" class="form-control input-sm select2 party_code" onchange="party_details(this.value)">
                                                                        <option value="">--Select--</option>
                                                                        <?php foreach($party_code as $pc): ?>
                                                                            <option value="<?= $pc->party_id; ?>"><?= $pc->party_name; ?></option>
                                                                        <?php endforeach; ?> 
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    Party Type
                                                                </label>
                                                                <div class="col-sm-9 no-padding">
                                                                    <input type="hidden" class="form-control input-sm party_name" id="party_name" name="party_data[party_name]" placeholder="">
                                                                    <div class="col-sm-6">
																		<select name="party_data[party_type_1]" id="party_type_1" class="form-control input-sm select2 party_type_1">
																			<option value="">--Select--</option>
																			<?php foreach($party_type_1 as $pt_1): ?>
																				<option value="<?php echo $pt_1->id; ?>"><?php echo $pt_1->type; ?></option>
																			<?php endforeach; ?> 
																		</select>
																	</div>
																	<div class="col-sm-6">
																		<select name="party_data[party_type_2]" id="party_type_2" class="form-control input-sm select2 party_type_2">
																			<option value="">--Select--</option>
																			<?php foreach($party_type_2 as $pt_2): ?>
																				<option value="<?php echo $pt_2->id; ?>"><?php echo $pt_2->type; ?></option>
																			<?php endforeach; ?> 
																		</select>
																		
																		
																	</div>
                                                                    
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    Address
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <textarea class="form-control address" rows="2" id="address" name="party_data[address]"></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
																<label for="inputEmail3" class="col-sm-3 control-label input-sm">City</label>
																<div class="col-sm-9 dispaly-flex">
																	<select name="party_data[city_id]" id="city_id" class="form-control input-sm select2" onChange="getstatedetails(this.value)">
																		<option value="">--Select--</option>
																		<?php foreach($city as $ct): ?>
																			<option value="<?= $ct->city_id; ?>"><?= $ct->city; ?></option>
																		<?php endforeach; ?> 
																	</select>
																</div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-3 control-label input-sm">State</label>
																<div class="col-sm-9 dispaly-flex">
																	<select name="party_data[state_id]" id="state_id" class="form-control input-sm select2">
																	</select>
																</div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-3 control-label input-sm">Country</label>

																<div class="col-sm-9">
																	<select name="party_data[country_id]" id="country_id" class="form-control input-sm select2">
																	</select>
																</div>
															</div>

                                                            <!-- <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">City</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control input-sm city" id="city" placeholder="">
                                                                    <input type="hidden" class="city_id" id="city_id" name="party_data[city_id]" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    State
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control input-sm state" id="" placeholder="">
                                                                    <input type="hidden" class="state_id" id="" name="party_data[state_id]" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    Country
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control input-sm country" id="" placeholder="">
                                                                    <input type="hidden" class="country_id" id="" name="party_data[country_id]" placeholder="">
                                                                </div>
                                                            </div> -->

                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    Fax No.
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control input-sm fax_no" id="" name="party_data[fax_no]" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    Pin-Code
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control input-sm pincode" id="" name="party_data[pincode]" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    Phone no.
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <textarea class="form-control phone_no" rows="2" id="" name="party_data[phone_no]" placeholder=""></textarea>
                                                                </div>
                                                            </div>


                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <label for="inputEmail3"
                                                                       class="col-sm-3 input-sm">
                                                                    E-mail ID
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <textarea class="form-control email_id" rows="2" id="" name="party_data[email_id]" placeholder=""></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 input-sm">
                                                                    Website
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control input-sm website" id="" name="party_data[website]" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset class="scheduler-border">
                                                        <legend class="scheduler-border text-primary text-bold">
                                                            Contact Person
                                                        </legend>
                                                    
                                                        <div class="row" style="margin-top: 10px;">
                                                        <div class="col-sm-1"></div>
                                                        <div class="col-sm-10">
                                                            <table style="" class="table custom-table contact-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Priority</th>
                                                                        <th>Person Name</th>
                                                                        <th>Email</th>
                                                                        <th>Department</th>
                                                                        <th>Designation</th>
                                                                        <th>Phone No.</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody class="contact-table-body">
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                    </fieldset>     
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
<!--                         /.tab-pane 
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                            </div>
                        </div>
                         /.tab-pane 
                        <div class="tab-pane" id="tab_3">
                        </div>-->
                        
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_5">
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_6">
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_7">
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_8">
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
                <input type="submit" class="hidden btn-submit-lead-form" />
            </form>
        </div>
        <!-- /.col -->
    </div>
</div>

<script>
    var LeadId = 0;
    
    function edit_leads(id)
    {   
        $("#ajax-loader").show();
        var FormVar = $("#lead_form");
        $("#lead_search .close").trigger("click");
        FormVar.find('input,textarea').each(function(){
              $(this).val('');
        });
        $("#lead_id").val(id);
        $.ajax({
            url: $.core.url('sales/get_lead'),
            type: "POST",
            data: {id: id},
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) 
                {
                    var datum = data.party;
                    $partyCode = datum.party_code;
                    
                    //$(".party_code").val($partyCode);
                    if ($partyCode) {
						$("select[name='party_data[party_code]']").select2().select2('val',$partyCode).change();
						//$('.party_code').val(json['party_code']);
					}else{
						$("select[name='party_data[party_code]']").select2().select2("val", '');
					}

					if (datum.city_id) {
						$("select[name='party_data[city_id]']").select2().select2('val',datum.city_id).change();
						//$('.party_code').val(json['party_code']);
					}else{
						$("select[name='party_data[city_id]']").select2().select2("val", '');
					}

					$('.address').val(datum.address);
					$('.party_name').val(datum.party_name)
                    /*$('.party_name').val(datum.party_name);
                    $('.party_id').val(datum.party_id);
                    $('.address').val(datum.address);
                    $('.city').val(datum.city);
                    $('.city_id').val(datum.city_id);
                    $('.state').val(datum.state);
                    $('.state_id').val(datum.state_id);
                    $('.country').val(datum.country);
                    $('.country_id').val(datum.country_id);
                    $('.fax_no').val(datum.fax_no);
                    $('.email_id').val(datum.email_id);
                    $('.website').val(datum.website);
                    $('.pincode').val(datum.pincode);
                    $('.phone_no').val(datum.phone_no);
                    $(".project").val(datum.project);*/
                    $("tbody.contact-table-body").html(datum.contact_persons);
                    
                    // prefill lead data
                    var lead = data.lead_data;
                    $("#lead_no").val(lead.lead_no);
                    $("#lead_title").val(lead.lead_title);
                    
                    $("input[name='lead_data[party_id]']").val(lead.party_id);
                    $("input[name='lead_data[start_date]']").val(lead.start_date);
                    $("input[name='lead_data[end_date]']").val(lead.end_date);
                    $("input[name='lead_data[review_date]']").val(lead.review_date);
                    $("textarea[name='lead_data[lead_detail]']").val(lead.lead_detail);                    
                    
                    $("#industry_type").val(lead.industry_type);
                    $("#lead_owner").val(lead.lead_owner);
                    $("#assigned_to").val(lead.assigned_to);
                    $("#priority").val(lead.priority);
                    $("#lead_source").val(lead.lead_source);
                    $("#lead_stage").val(lead.lead_stage);
                    $("#lead_provider").val(lead.lead_provider);
                    $("#lead_status").val(lead.lead_status);
                    
                    /*$("input[name='lead_data[industry_type_id]']").val(lead.industry_type_id);
                    $("input[name='lead_data[lead_owner_id]']").val(lead.lead_owner_id);
                    $("input[name='lead_data[assigned_to_id]']").val(lead.assigned_to_id);
                    $("input[name='lead_data[priority_id]']").val(lead.priority_id);
                    $("input[name='lead_data[lead_source_id]']").val(lead.lead_source_id);
                    $("input[name='lead_data[lead_stage_id]']").val(lead.lead_stage_id);
                    $("input[name='lead_data[lead_provider_id]']").val(lead.lead_provider_id);
                    $("input[name='lead_data[lead_status_id]']").val(lead.lead_status_id);*/

                    $("select[name='lead_data[industry_type_id]']").select2().select2('val',lead.industry_type_id).change();
                    $("select[name='lead_data[lead_owner_id]']").select2().select2('val',lead.lead_owner_id).change();
                    $("select[name='lead_data[assigned_to_id]']").select2().select2('val',lead.assigned_to_id).change();
                    $("select[name='lead_data[priority_id]']").select2().select2('val',lead.priority_id).change();
                    $("select[name='lead_data[lead_source_id]']").select2().select2('val',lead.lead_source_id).change();
                    $("select[name='lead_data[lead_stage_id]']").select2().select2('val',lead.lead_stage_id).change();
                    $("select[name='lead_data[lead_provider_id]']").select2().select2('val',lead.lead_provider_id).change();
                    $("select[name='lead_data[lead_status_id]']").select2().select2('val',lead.lead_status_id).change();
                    
                } 
                else 
                {
                    show_notify(data.msg, false);
                }
                $("#ajax-loader").hide();
            }
        });
        
    }
    
    function delete_leads(id) {
        var c = confirm("Are You sure ?");
        if (c) {
            $.ajax({
                url: "<?=base_url()?>sales/delete",
                type: "POST",
                data: {"id": id},
                success: function (data) {
                    $("#lead_search_row_" + id).hide();
                    show_notify('Deleted Successfully!', true);
                }
            });
        }
    }

    $(document).ready(function () {

        /*-------------------- For Search Leads ----------------------------*/

        $(document).on("click", ".btn_load_party", function () {
            $('#search_result').trigger('click');
        });

        $(document).on("click", "#search_result", function () {
            var lead_source = $("#lead_source_s").val();
            var lead_status = $("#lead_status_s").val();
            var industry_type = $("#industry_type_s").val();
            var lead_owner = $("#lead_owner_s").val();
            var assigned_to = $("#assigned_to_s").val();
            var created_by = $("#filter_creator_by").val();
            var data = {
                "lead_source": lead_source,
                "lead_status": lead_status,
                "industry_type": industry_type,
                "lead_owner": lead_owner,
                "assigned_to": assigned_to,
                "created_by": created_by
            };
            $.ajax({
                url: "<?=base_url()?>sales/search-lead",
                type: "POST",
                data: data,
                success: function (data) {
                    $("#result_search_leads").html(data);
                }
            });
        });

        $(document).on('click','.btn-save-leads',function(){
            $(".btn-submit-lead-form").trigger('click');
                //$("#lead_form").submit();
        });

        $("#lead_form").on("submit",function (e) {
            e.preventDefault();
            /*if($('#lead_no').val() == ''){
                show_notify('Lead no is required!', false);
                return false;
            }*/

            /*if($('#lead_title').val() == ''){
                show_notify('Lead title is required!', false);
                return false;
            }*/

            if($.trim($('#party_code').val()) == "")
            {
                show_notify("Party detail is required!",false);
                return false;
            }    
            
            /*if($('.party_code').val() == ''){
                show_notify('Party detail is required!', false);
                return false;
            }*/
            var FormVar = $(this);
            $.ajax({
                url: $.core.url('sales/save-lead'),
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.status == true) {
                        FormVar.find('input,textarea').each(function () {
                            // $(this).val('');
                        });
                        show_notify(data.msg, true);
                        $("#lead_id").val(data.lead_id);
                        $("#lead_no").val(data.lead_id);
                        edit_leads(data.lead_id);
                    } else if (data.status == false) {
                        show_notify(data.msg, false);
                    }
                }
            });
        });
    });
    
    function getstatedetails(id)
    {
       //alert('this id value :'+id);
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>party/ajax_get_state/'+id,
            data: id='cat_id',
            success: function(data){
                var json = $.parseJSON(data);
                //alert(data);
                if (json['state']) {
                    $("#state_id").html('');
                    $("#state_id").html(json['state']);
                    $("#s2id_state_id span:first").html($("#state_id option:selected").text());    
                }
                if (json['country']) {
                    $("#state_id").html('');
                    $("#state_id").html(json['country']);
                    $("#s2id_country_id span:first").html($("#state_id option:selected").text());    
                }
                
                
            },
         });
    }
    function party_details(id){
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/'+id,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);
                
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
				
				if (json['party_type_1_id']) {
                    $(".party_type_1").select2("val", json['party_type_1_id']);
                }else{
					$(".party_type_1").select2("val", '');
				}

                if (json['party_type_2_id']) {
                    $(".party_type_2").select2("val", json['party_type_2_id']);
                }else{
					$(".party_type_2").select2("val", '');
				}

                 if (json['address']) {
                    $(".address").val(json['address']);
                }
                
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                }
                
                if (json['city']) {
                    $("#city").val(json['city']);
                }
                if (json['city_id']) {
                    //$("#city_id").val(json['city_id']);
                    $("#city_id").select2("val", json['city_id']).change();
                }
                if (json['state']) {
                    $(".state").val(json['state']);
                }
                if (json['state_id']) {
                    $(".state_id").val(json['state_id']);
                }
                if (json['country']) {
                    $(".country").val(json['country']);
                }
                if (json['country_id']) {
                    $(".country_id").val(json['country_id']);
                }
                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                }
                if (json['email_id']) {
                    $(".email_id").val(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }
                if (json['website']) {
                    $(".website").val(json['website']);
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                }
                if (json['project']) {
                    $(".project").val(json['project']);
                }
                if (json['contact_persons']) {
                    $("tbody.contact-table-body").html(json['contact_persons']);
                }
            },
         });
    }
</script>
