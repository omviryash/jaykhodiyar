<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Sales : Tracking</small>
        </h1>
    </section>

    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <?php
                    if ($this->session->userdata('success_message')) {
                        echo '<div class="alert alert-success"><a class="close" data-close="alert"></a><strong>Success!</strong> ' . $this->session->userdata('success_message') . '</div>';
                        $success_message = array('success_message' => "");
                        $this->session->unset_userdata('success_message');
                    }
                    ?>
				<!-- Start Advance Search -->
				<div class="col-md-6">
					<a href="javascript:void(0)" class="btn btn-info btn-sm filtration_div"> Advanced Search </a>
					<div class="filtration_fields" id="filtration_fields">
						<br>
						<div class="col-md-12">
							<div class="row">
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='1'> Customer Name</a> <!-- -
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='1'> Invoice Number</a> -
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='4'> Created At</a>  -->
							</div>	
						</div>
						<br><br>


						<table cellpadding="3" cellspacing="0" border="0" style="width:100%;">
							<thead>
								<tr>
									<th>Target</th>
									<th style="text-align:center">Search text</th>
								</tr>
							</thead>
							<tbody>
								<!-- <tr id="filter_global">
									<td>Global search</td>
									<td align="center"><input type="text" class="form-control input-sm global_filter" id="global_filter"></td>
								</tr> -->
								<tr id="filter_col4" data-column="1">
									<td>Column - Party Name</td>
									<!--<td align="center"><input type="text" class="column_filter" id="col3_filter"></td>-->
									<td align="center">
										<select name="party_name" id="party_name1" class="form-control input-sm" onchange='filterPartyName()'>
										</select>
									</td>
								</tr>
								<!-- <tr id="filter_col2" data-column="2">
									<td>Column - Invoice Number</td>
									<td align="center"><input type="text" class="form-control input-sm column_filter" id="col2_filter"></td>
								</tr>
								<tr id="filter_col6" data-column="4">
									<td>Column - Created At</td>
									<td align="center"><input type="text" class="form-control input-sm column_filter" id="col5_filter"></td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
				<div class="clearfix"></div>
				<br />
				<!--End Advance Search-->
                    <form action="" method="POST">

                        <div class="row">
                            <?php 
                            	$current_status = 'all';
								/*if($this->input->get_post("status")){ 
									$current_status = $this->input->get_post("status"); 
								} else {
									$current_status = 'pending';
								}*/
                                if($this->input->get_post("user_id")){ 
									$current_staff = $this->input->get_post("user_id"); 
								} else {
									$current_staff = $this->session->userdata('is_logged_in')['staff_id'];
								}
							?>
                            <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
								<div class="col-md-4">
									Search by user:
									<select class="form-control select2" name="user_id" onchange="this.form.submit()">
										<option value="all" <?php if($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
										<?php

										if (!empty($users)) {
											foreach ($users as $client) {
												if (trim($client->name) != "") {
													$selected = $current_staff == $client->staff_id ? 'selected' : '';
													echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
												}
											}
										}
										?>
									</select>
								</div>
                            <?php } ?>
                            <div class="col-md-4"><br/>
                                <!-- <button class="btn btn-info" type="submit">Search</button> -->
                                &nbsp;
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>

                    </form>
                    <div class="table-responsive" style="overflow-x:hidden">
                        <?php
                        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
                        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");
                        ?>
                        <table id="tracksale_list_table" class="table custom-table table-striped">
                            <thead>
                            <tr>
                            	<th>Party</th>
                            	<th>Enquiry Date</th>
                            	<th>Quotation Date</th>
                            	<th>Proforma Invoice Date</th>
                            	<th>Order Date</th>
                            	<th>Challan Date</th>
                            	<th>Invoice Date</th>
                            	<th>Payment Pending</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function() {
        $(".select2").select2();
	    initAjaxSelect2($("#party_name1"),"<?=base_url('app/party_select2_source')?>");
	    $('#filtration_fields').hide();
		$(document).on('click', '.filtration_div', function(){
			$(".filtration_fields").toggle(500);
		});
        table = $('#tracksale_list_table').DataTable({
            "serverSide": true,
            "processing": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('sales/track_sale_datetable')?>",
                "type": "POST",
                "data":function(d){
                    d.user_id = '<?=isset($_REQUEST['user_id'])?$_REQUEST['user_id']:$staff_id;?>';
                }
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
			}
		});
	});
	
	$('input.global_filter').on( 'keyup click', function () {
			filterGlobal();
	} );
	$('input.column_filter').on( 'keyup click', function () {
		filterColumn( $(this).parents('tr').attr('data-column') );
	} );
	$('.showhideColumn').on('click', function(){
		var tbl_column = table.column($(this).attr('data-columnindex'));
		tbl_column.visible(!tbl_column.visible());
	});
	function filterGlobal () {
		$('#tracksale_list_table').DataTable().search(
			$('#global_filter').val()
		).draw();
	}

	function filterColumn ( i ) {
		$('#tracksale_list_table').DataTable().column( i ).search(
			$('#col'+i+'_filter').val()
		).draw();
	}
	function filterPartyName () {
		$('#tracksale_list_table').DataTable().column(1).search(
			$("#party_name1 option:selected").text()
		).draw();
	}
	
</script>
