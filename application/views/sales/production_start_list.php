<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Production Start List</small>
            <a href="<?= base_url('production_start/production_item_detail') ?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Start Another </a>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="table-responsive" style="overflow-x:hidden">
                            <table id="order_list_table" class="table custom-table table-striped">
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Start Date</th>
                                        <th>SO No. / PI No.</th>
                                        <th>Item Name</th>
                                        <th>Item Code</th>
                                        <th>Status</th>
                                        <th>Finished Date</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </div>

    <script>
        var table;
        $(document).ready(function () {
            $('.select2').select2();
            
            var buttonCommon = {
                exportOptions: {
                    format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                    columns: [1,2,3,4,5,6],
                }
            };
            
            table = $('#order_list_table').DataTable({
                "serverSide": true,
                "ordering": true,
                "searching": true,
                "aaSorting": [[1, 'desc']],
                "ajax": {
                    "url": "<?php echo site_url('production_start/production_list_datatable') ?>",
                    "type": "POST",
                    "data": function (d) {
                    }
                },
                <?php if($this->applib->have_access_current_user_rights(START_PRODUCTION_MODULE_ID,"export_data")){ ?>
                    dom: 'Bfrtip',
                    buttons: [
                        $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Productions', action: newExportAction } ),
                        $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Productions', action: newExportAction } ),
                        $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Productions', action: newExportAction } ),
                        $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Productions', action: newExportAction } ),
                        $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Productions', orientation: 'landscape', action: newExportAction } ),
                    ],
                <?php } ?>
                "scrollY": 500,
                "scroller": {
                    "loadingIndicator": true
                },
                "sScrollX": "100%",
                "sScrollXInner": "110%"
            });

            $(document).on("click", ".delete_button", function () {
                var value = confirm('Are you sure delete this records?');
                var tr = $(this).closest("tr");
                $("#ajax-loader").show();
                if (value) {
                    $.ajax({
                        url: $(this).data('href'),
                        type: "POST",
                        data: 'id_name=production_id&table_name=production',
                        success: function (response) {
                            var json = $.parseJSON(response);
                            if (json['success'] == 'Deleted') {
                                window.location.href = "<?php echo base_url('production_start/production_start_list') ?>";
                            }
                            return false;
                        }
                    });
                }
            });
            
            $(document).on("click", ".finish_button", function () {
                var value = confirm('Are you sure Finish this Item Production?');
                var tr = $(this).closest("tr");
                if (value) {
                    $.ajax({
                        url: $(this).data('href'),
                        type: "POST",
                        data: 'id_name=production_id&table_name=production',
                        success: function (response) {
                            var json = $.parseJSON(response);
                            if (json['success'] == 'Finished') {
                                window.location.href = "<?php echo base_url('production_start/production_start_list') ?>";
                            }
                            return false;
                        }
                    });
                }
            });
        });
    </script>
