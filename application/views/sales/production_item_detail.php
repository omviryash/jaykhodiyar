<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form class="form-horizontal" action="" method="post" id="save_production" novalidate enctype="multipart/form-data">
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Production Item Detail</small>
            <small class="text-center"><label class="label label-warning">Ctrl+S = Start</label></small>
            <?php if (isset($production_data->production_id) && !empty($production_data->production_id)) { ?>
                <a href="<?= base_url('production_start/production_item_detail') ?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Start Another </a>
            <?php } else { ?>
                <button type="submit" class="btn btn-info btn-xs pull-right start_production" style="margin: 5px;" id="start_production">Start</button>
                <a href="<?= base_url('production_start/production_item_detail') ?>" class="btn btn-info btn-xs pull-right start_another" style="margin: 5px; display: none">Start Another </a>
            <?php } ?>
            <?php $production_start_view_role = $this->applib->have_access_role(START_PRODUCTION_MODULE_ID, "view"); ?>
            <?php if ($production_start_view_role): ?>
                <a href="<?= base_url('production_start/production_start_list') ?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Production List</a>
            <?php endif; ?>
        </h1>
    </section>
    <?php if (isset($production_data->production_id) && !empty($production_data->production_id)) { ?>
        <input type="hidden" name="production_id" id="production_id" value="<?= $production_data->production_id ?>">
    <?php } ?>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <input type="hidden" name="ps_sales_order_id" id="ps_sales_order_id" value="" >
                    <input type="hidden" name="ps_proforma_invoice_id" id="ps_proforma_invoice_id" value="">
                    <input type="hidden" name="ps_item_id" id="ps_item_id" value="" >
                    <input type="hidden" name="bom_id" id="bom_id" value="" >

                    <div class="row">
                        <div class="col-md-4">
                            <label>Item Name</label>
                            <input type="text" id="item_name" class="form-control" value="<?= (isset($item_name)) ? $item_name : ''; ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label>Item Code</label>
                            <input type="text" id="item_code" class="form-control" value="<?= (isset($item_code)) ? $item_code : ''; ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label>Status</label><br>
                            <?php echo (isset($production_data) && ($production_data->pro_status == '1')) ? 'In Production' : ''; ?>
                            <?php echo (isset($production_data) && ($production_data->pro_status == '2')) ? 'Finished' : ''; ?>
                        </div>
                        <div class="col-md-2">
                            <label>Production For</label><br>
                            <span id="production_for"><?php echo (isset($production_data)) ? $production_for : ''; ?></span>
                        </div>
                        <div class="col-md-2 bom_link"></div>
                        <div class="clearfix"></div><br />
                        <div class="col-md-6">
                            <table style="" class="table custom-table">
                                <thead>
                                    <tr>
                                        <th style="width: 70%;">Project Name</th>
                                        <th class="text-right" style="width: 30%;" align="right">Project Qty</th>
                                    </tr>
                                </thead>
                                <tbody id="project_list">
                                    <?php
                                    if (isset($production_data->production_id) && !empty($production_data->production_id)) {
                                        foreach ($production_projects as $pr) {
                                            ?>
                                            <tr>
                                                <td style="width: 70%;"><?= (isset($pr->project_name)) ? $pr->project_name : ''; ?></td>
                                                <td style="width: 30%;" align="right"><?= (isset($pr->project_qty)) ? $pr->project_qty : ''; ?></td>
                                            </tr>
                                        <?php }
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8" style="padding: 0px 2px;">
                                    <div class="form-group text-center">
                                        <label for="" class="input-sm">Item Expense</label>
                                    </div>
                                </div>
                                <div class="col-md-3" style="padding: 0px 2px;">
                                    <div class="form-group text-center">
                                        <label for="" class="input-sm">Expense</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div id="bom_item_expense_fields">
                                    <?php
                                        if(isset($production_item_expenses) && !empty($production_item_expenses)){
                                            foreach ($production_item_expenses as $key => $production_item_expense){
                                    ?>
                                        <div id="item_expense_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <input type="text" name="bom_item_expenses[item_expense][]" id="item_expense" class="form-control input-sm item_expense" value="<?php echo $production_item_expense->item_expense; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-3" style="padding-left: 20px;">
                                                    <div class="form-group">
                                                        <input type="text" name="bom_item_expenses[expense][]" id="expense" class="form-control input-sm expense num_only" value="<?php echo $production_item_expense->expense; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-danger btn-xs pull-center remove_bom_item_expense" <?php echo ($key == 0) ? 'style="display: none;"' : ''; ?> > <i class="fa fa-close"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } } else { ?>
                                        <div id="item_expense_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <input type="text" name="bom_item_expenses[item_expense][]" id="item_expense" class="form-control input-sm item_expense">
                                                    </div>
                                                </div>
                                                <div class="col-md-3" style="padding-left: 20px;">
                                                    <div class="form-group">
                                                        <input type="text" name="bom_item_expenses[expense][]" id="expense" class="form-control input-sm expense num_only">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-danger btn-xs pull-center remove_bom_item_expense" style="display: none;"> <i class="fa fa-close"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-11">
                                    <button type="button" class="btn btn-info btn-xs pull-right add_bom_item_expenses"> <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="clearfix">
                                <input type="hidden" name="purchase_item_index" id="purchase_item_index" />
                                <input type="hidden" name="bom_item_details[lineitem_id]"   id="lineitem_id" />
                                <input type="hidden" name="bom_item_details[purchase_project_id]" id="purchase_project_id" />
                                <input type="hidden" name="bom_item_details[purchase_project_name]" id="purchase_project_name" />
                                <input type="hidden" name="bom_item_details[purchase_project_qty]" id="purchase_project_qty" />
                                <input type="hidden" name="bom_item_details[item_code]" id="purchase_item_code" />
                                <input type="hidden" name="bom_item_details[effective_stock]" id="effective_stock" value=""/>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="purchase_item_id" class="input-sm">Purchase Items <span class="required-sign">&nbsp;*</span></label>
                                        <select name="bom_item_details[purchase_item_id]" id="purchase_item_id" class="form-control input-sm purchase_item_id" >
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <label for="purchase_item_qty" class="input-sm">Qty <span class="required-sign">&nbsp;*</span></label>
                                    <div class="form-group">
                                        <input type="text" name="bom_item_details[purchase_item_qty]" id="purchase_item_qty" class="form-control input-sm purchase_item_qty num_only" >
                                    </div>
                                </div>
                                <div class="col-md-2" style="padding-left: 20px;">
                                    <label for="uom_name" class="input-sm">UOM<span class="required-sign">&nbsp;*</span></label>
                                    <div class="form-group">
                                        <select class="input-sm pull-left" name="bom_item_details[uom_id]" id="uom_name"></select>
                                    </div>
                                </div>
                                <div class="col-md-1" style="padding-left: 20px;">
                                    <label for="loss" class="input-sm">Loss Qty</label>
                                    <div class="form-group">
                                        <input type="text" name="bom_item_details[loss][]" id="loss" class="form-control input-sm loss num_only" >
                                    </div>
                                </div>
                                <div class="col-md-2" style="padding-left: 20px;">
                                    <label for="job_expense" class="input-sm">Job Expense</label>
                                    <div class="form-group">
                                        <input type="text" name="bom_item_details[job_expense][]" id="job_expense" class="form-control input-sm job_expense num_only" >
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <label for="" class="input-sm">&nbsp;</label>
                                    <button type="button" id="add_purchase_item" class="btn btn-info btn-xs add_purchase_item pull-right"> <i class="fa fa-plus"></i> Add Item</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <table style="" class="table custom-table item-table">
                                <thead>
                                    <tr>
                                        <th width="55px">Action</th>
                                        <th>Sr. No.</th>
                                        <th>Name</th>
                                        <th>Item Code</th>
                                        <th class="text-right">Basic Qty</th>
                                        <th class="text-center">UOM</th>
                                        <th class="text-right">Loss Qty</th>
                                        <th class="text-right">Job Expense</th>
                                        <th>Project</th>
                                        <th class="text-right">Project Qty</th>
                                        <th class="text-center">Stock</th>
                                        <th class="text-right">Total Qty</th>
                                    </tr>
                                </thead>
                                <tbody id="lineitem_list"></tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-right"></th>
                                        <th class="text-center"></th>
                                        <th class="text-right"></th>
                                        <th class="text-right"></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-right">Total</th>
                                        <th class="text-right"><span class="set_qty_total"></span><input type="hidden" name="set_qty_total" id="set_qty_total" /></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<div class="clearfix"></div>
<?php if (isset($production_data->production_id) && !empty($production_data->production_id)) {
    
} else {
    ?>
    <div class="modal fade" id="select_sales_order_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Sales Order Or Export Proforma Invoice To Production Start
                        <span class="pull-right">
                            <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                            <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                        </span>
                    </h4>
                </div>
                <div class="modal-body">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#sales_order_tab" data-toggle="tab">Select Sales Order </a></li>
                            <li><a href="#export_proforma_invoice_tab" data-toggle="tab">Select Export Proforma Invoice</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="sales_order_tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive" style="overflow-x:hidden">
                                            <table id="sales_order_datatable" class="table custom-table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Enquiry No</th>
                                                        <th>Quotation No</th>
                                                        <th>Order No</th>
                                                        <th>Party</th>
                                                        <th>Item Name</th>
                                                        <th>Contact No</th>
                                                        <th>Email Id</th>
                                                        <th>Party Type</th>
                                                        <th>City</th>
                                                        <th>State</th>
                                                        <th>Country</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane active" id="export_proforma_invoice_tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive" style="overflow-x:hidden">
                                            <table id="export_proforma_invoice_datatable" class="table custom-table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Enquiry No</th>
                                                        <th>Quotation No</th>
                                                        <th>Proforma No</th>
                                                        <th>Party</th>
                                                        <th>Item Name</th>
                                                        <th>Contact No</th>
                                                        <th>Email Id</th>
                                                        <th>Party Type</th>
                                                        <th>City</th>
                                                        <th>State</th>
                                                        <th>Country</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php } ?>

<script type="text/javascript" src="<?= dist_url('js/sales_order.js'); ?>"></script>
<script type="text/javascript">
    var lineitem_objectdata = [];
    var edit_lineitem_inc = 0;
    var project_item_group = '';
    var remove_all_project_iemts = '';
    <?php if (isset($production_item_details)) { ?>
        var li_lineitem_objectdata = [<?php echo $production_item_details; ?>];
        var lineitem_objectdata = [];
        if (li_lineitem_objectdata != '') {
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
        display_lineitem_html(lineitem_objectdata);
    <?php } ?>
    
    var item_is_missing = '0';
    $(document).ready(function () {
        $('[href="#export_proforma_invoice_tab"]').click();
        $('[href="#sales_order_tab"]').click();

//        initAjaxSelect2($("#purchase_item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source')?>");

        var table;
        table = $('#sales_order_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('production_start/sales_order_itemqtywise_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                }
            },
            "scrollY": 350,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "bInfo": false
        });

        var epi_table;
        epi_table = $('#export_proforma_invoice_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('production_start/export_proforma_invoice_itemqtywise_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                }
            },
            "scrollY": 350,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "bInfo": false
        });

        $('#select_sales_order_modal').modal({backdrop: 'static', keyboard: false});
        $('#select_sales_order_modal').modal('show');
        $('#select_sales_order_modal').on('shown.bs.modal', function () {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

        $(document).on('click', '.sales_order_row', function () {
            $('.sales_order_row').prop('disabled', true);
            var tr = $(this).closest('tr');
            var sales_order_item_id = $(this).data('sales_order_item_id');
            var proforma_invoice_item_id = 0;

            $('#ps_sales_order_id').val($(this).data('sales_order_id'));
            $('#production_for').html($(this).data('sales_order_no'));
            $('#ps_proforma_invoice_id').val(0);
            $('#ps_item_id').val(sales_order_item_id);

            get_item_bom_detail(sales_order_item_id, proforma_invoice_item_id);
            $('#select_sales_order_modal').modal('hide');
        });

        $(document).on('click', '.proforma_invoice_row', function () {
            $('.proforma_invoice_row').prop('disabled', true);
            var tr = $(this).closest('tr');
            var proforma_invoice_item_id = $(this).data('proforma_invoice_item_id');
            var sales_order_item_id = 0;
            

            $('#ps_proforma_invoice_id').val($(this).data('proforma_invoice_id'));
            $('#production_for').html($(this).data('proforma_invoice_no'));
            $('#ps_sales_order_id').val(0);
            $('#ps_item_id').val(proforma_invoice_item_id);
            
            get_item_bom_detail(sales_order_item_id, proforma_invoice_item_id);
            $('#select_sales_order_modal').modal('hide');
        });
        
        $(document).on('click', '.add_bom_item_expenses', function () {
            $("#item_expense_fields").clone().appendTo("#bom_item_expense_fields");
            $(".remove_bom_item_expense:last").css('display', 'block');
            $(".bom_item_expense_id:last, .item_expense:last,.expense:last").val('');
            $(".item_expense:last").focus();
        });

        $(document).on('click', '.remove_bom_item_expense', function () {
            $(this).closest("#item_expense_fields").remove();
            var bom_item_expense_id = $(this).closest("#item_expense_fields").find("#bom_item_expense_id").val();
            $('#save_production').append('<input type="hidden" name="deleted_bom_item_expense_id[]" id="deleted_bom_item_expense_id" value="' + bom_item_expense_id + '" />');
        });

        initAjaxSelect2($("#uom_name"),"<?=base_url('app/get_purchase_item_uom_select2_source')?>/");
        $(document).on('change', '#purchase_item_id', function () {
            var purchase_item_id = $('#purchase_item_id').val();
            if(purchase_item_id != '' && purchase_item_id != null){
                initAjaxSelect2($("#uom_name"),"<?=base_url('app/get_purchase_item_uom_select2_source')?>/" + purchase_item_id);
            } else {
                $("#uom_name").val(null).trigger("change");
                initAjaxSelect2($("#uom_name"),"<?=base_url('app/get_purchase_item_uom_select2_source')?>/");
            }
        });
        
        $(document).on('change', '#uom_name', function () {
            var purchase_item_id = $('#purchase_item_id').val();
            var uom_name = $('#uom_name').val();
            if(uom_name != '' && uom_name != null){
                $.ajax({
                    url: '<?php echo base_url('production_start/ajax_load_purchase_item_details') ?>',
                    method : 'POST',
                    async: false,
                    data : {purchase_item_id: purchase_item_id},
                    success : function(response){
                        var json = $.parseJSON(response);
                        $('#effective_stock').val(json.effective_stock);
                    }
                });
            }
        });
        
        $(document).on('click', '#add_purchase_item', function () {
            if ($.trim($("#purchase_item_id").val()) == '') {
                show_notify('Please Select Purchase Item.', false);
                $("#purchase_item_id").focus();
                return false;
            }
            if ($.trim($("#purchase_item_qty").val()) == '') {
                show_notify('Please Enter Purchase Item Qty.', false);
                $("#purchase_item_qty").focus();
                return false;
            }
            if($.trim($("#uom_name").val()) == '') {
                show_notify('Please select UOM', false);
                $("#uom_name").focus();
                return false;
            }
            var lineitem = {};
            get_effective_amt();
//            alert($("#effective_stock").val());
            lineitem['lineitem_id'] = $("#lineitem_id").val();
            lineitem['item_id'] = $("#purchase_item_id").val();
            lineitem['item_name'] = $("#purchase_item_id option:selected").text();
            lineitem['item_code'] = $("#purchase_item_code").val();
            var uom_name = $('#uom_name').select2('data');
            lineitem['uom_id'] = $('#uom_name').val();
            lineitem['uom_name'] = uom_name[0].text;
            lineitem['project_id'] = $("#purchase_project_id").val();
            lineitem['project_name'] = $("#purchase_project_name").val();
            lineitem['loss'] = $("#loss").val();
            lineitem['job_expense'] = $("#job_expense").val();
            
            var purchase_project_qty = $("#purchase_project_qty").val();
            lineitem['purchase_project_qty'] = purchase_project_qty;
            lineitem['set_qty'] = $.trim($("#purchase_item_qty").val());
            lineitem['effective_stock'] = $("#effective_stock").val();
//            alert($("#effective_stock").val());
            var purchase_item_qty = $.trim($("#purchase_item_qty").val());
            if(purchase_project_qty == '' || purchase_project_qty == 0 || purchase_project_qty == null){ purchase_project_qty = 1; }
            purchase_item_qty = purchase_item_qty * purchase_project_qty;
            lineitem['quantity'] = purchase_item_qty;
            if(parseFloat(purchase_item_qty) > parseFloat(lineitem['effective_stock'])){
                item_is_missing = '1';
                lineitem['inout_stock'] = '2';
            } else {
                lineitem['inout_stock'] = '1';
            }
            var new_lineitem = JSON.parse(JSON.stringify(lineitem));
            var purchase_item_index = $("#purchase_item_index").val();
            
            if (purchase_item_index != '') {
                lineitem_objectdata.splice(purchase_item_index, 1, new_lineitem);
            } else {
                lineitem_objectdata.push(new_lineitem);
            }
            display_lineitem_html(lineitem_objectdata);
            $('#lineitem_id').val('');
            $("#purchase_item_id").val(null).trigger("change");
            $("#uom_name").val(null).trigger("change");
            $("#purchase_project_id").val('');
            $("#purchase_project_name").val('');
            $("#purchase_project_qty").val('');
            $("#purchase_item_qty").val('');
            $("#purchase_item_index").val('');
            $("#loss").val('');
            $("#job_expense").val('');
            edit_lineitem_inc = 0;
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_production").submit();
                return false;
            }
        });

        $(document).on('submit', '#save_production', function () {
            if (lineitem_objectdata == '') {
                show_notify("Please Add Atleast One Item.", false);
                return false;
            }
            if(item_is_missing == '1'){
                if(confirm('Purchase missing item, Are you sure to Start Production?')){ 
                } else { 
                    return false;
                }
            }
            var postData = new FormData(this);
            
            var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
            postData.append('line_items_data', lineitem_objectdata_stringify);
            $("#ajax-loader").show();
            $.ajax({
				url: "<?=base_url('production_start/start_production') ?>",
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
//				data: {
//                    sales_order_id: $('#ps_sales_order_id').val(), 
//                    ps_item_id: $('#ps_item_id').val(), 
//                    proforma_invoice_id: $('#ps_proforma_invoice_id').val()
//                    
//                },
				success: function (response) {
					var json = $.parseJSON(response);
                    if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('production_start/production_start_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
            return false;
        });
        
        $(document).on('change', '.pts_set_qty', function () {
            var current_qty = $(this).val();
            var pts_selected_index = $(this).data('pts_selected_index');
            lineitem_objectdata[pts_selected_index].set_qty = current_qty;
            var project_qty = lineitem_objectdata[pts_selected_index].project_qty;
            var quantity = parseFloat(project_qty || 1) * parseFloat(current_qty);
            lineitem_objectdata[pts_selected_index].quantity = quantity;
            if(parseFloat(current_qty) > parseFloat(lineitem_objectdata[pts_selected_index].effective_stock)){
                item_is_missing = '1';
                lineitem_objectdata[pts_selected_index].inout_stock = '2';
            } else {
                lineitem_objectdata[pts_selected_index].inout_stock = '1';
            }
            display_lineitem_html(lineitem_objectdata);
        });
        
        $(document).on('change', '.pts_loss', function () {
            var current_los = $(this).val();
            var pts_selected_index = $(this).data('pts_selected_index');
            lineitem_objectdata[pts_selected_index].loss = current_los;
        });
    
        $(document).on('change', '.pts_job_expense', function () {
            var current_job_expense = $(this).val();
            var pts_selected_index = $(this).data('pts_selected_index');
            lineitem_objectdata[pts_selected_index].job_expense = current_job_expense;
        });
    });

    function get_item_bom_detail(sales_order_item_id, proforma_invoice_item_id) {
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>production_start/get_item_bom_detail/' + sales_order_item_id + '/' + proforma_invoice_item_id,
            type: "POST",
            success: function (response) {
//                if(response != ''){
                var json = $.parseJSON(response);
                
                $('#item_name').val(json.item_name);
                $('#item_code').val(json.item_code);
                if(typeof(json.error_exist) != "undefined" && json.error_exist !== null && json.error_exist == 'bom_not_exist') {
                    show_notify('Please Create BOM for Item : ' + json.item_name + '<br /> to see currect results', false);
                    $("#ajax-loader").hide();
                    $('.start_another').show();
                    $('.start_production').hide();
                    return false;
                } else {
                    $('#bom_id').val(json.bom_id);
                    $('.bom_link').html('<a href="<?= base_url('master/bom_add') ?>/'+ json.bom_id + '" target="_blank" class="btn btn-info btn-xs pull-right" style="margin: 5px;">View BOM</a>');
                    var lineitem_project = json.projects;
                    var new_lineitem_project_html = "";
                    $.each(lineitem_project, function (index, value) {
                        var row_html = '<tr><td style="width: 70%;"><input type="hidden" name="bom_projets[project_id][]" id="item_expense" value="'+ value.project_id +'" >' + value.project_name + '</td>' +
                                '<td style="width: 30%;" class="text-right"><input type="hidden" name="bom_projets[project_qty][]" id="item_expense" value="'+ value.project_qty +'" >' + value.project_qty + '</td>';
                        new_lineitem_project_html += row_html;
                        
                    });
                    $('tbody#project_list').html(new_lineitem_project_html);
                    var new_lineitem_expense_html = "";
                    if(json.expense != '' && json.expense != null){
                        var lineitem_expense = json.expense;
                        $.each(lineitem_expense, function (index, value) {
                            new_lineitem_expense_html +='<div id="item_expense_fields">' +
                                    '<div class="col-md-12">' +
                                        '<div class="col-md-8">' +
                                            '<div class="form-group">' +
                                                '<input type="text" name="bom_item_expenses[item_expense][]" id="item_expense" value="'+ value.item_expense +'" class="form-control input-sm item_expense">' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="col-md-3" style="padding-left: 20px;">' +
                                            '<div class="form-group">' +
                                                '<input type="text" name="bom_item_expenses[expense][]" id="expense" value="'+ value.expense +'" class="form-control input-sm expense num_only">' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="col-md-1">' +
                                            '<button type="button" class="btn btn-danger btn-xs pull-center remove_bom_item_expense" style="display: none;"> <i class="fa fa-close"></i></button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>';
                        });
                    } else {
                        new_lineitem_expense_html +='<div id="item_expense_fields">' +
                                    '<div class="col-md-12">' +
                                        '<div class="col-md-8">' +
                                            '<div class="form-group">' +
                                                '<input type="text" name="bom_item_expenses[item_expense][]" id="item_expense" value="" class="form-control input-sm item_expense">' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="col-md-3" style="padding-left: 20px;">' +
                                            '<div class="form-group">' +
                                                '<input type="text" name="bom_item_expenses[expense][]" id="expense" value="" class="form-control input-sm expense num_only">' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="col-md-1">' +
                                            '<button type="button" class="btn btn-danger btn-xs pull-center remove_bom_item_expense" style="display: none;"> <i class="fa fa-close"></i></button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>';
                    }
                    $('#bom_item_expense_fields').html(new_lineitem_expense_html);

                    var li_lineitem_objectdata = json.purchase_items;
                    if(li_lineitem_objectdata.length == '0'){
                        $('#start_production').attr('disabled','disabled');
                    }
                    if (li_lineitem_objectdata != '') {
                        $.each(li_lineitem_objectdata, function (index, value) {
                            if(parseFloat(value.quantity) > parseFloat(value.effective_stock)){
                                item_is_missing = '1';
                                value.inout_stock = '2';
                            } else {
                                value.inout_stock = '1';
                            }
                            lineitem_objectdata.push(value);
                        });
                    }
                    display_lineitem_html(lineitem_objectdata);
                    get_item_from_group(json.item_id);
                    $("#ajax-loader").hide();
                }
            },
        });
    }
    
    function display_lineitem_html(lineitem_objectdata) {
        $('#ajax-loader').show();
        var new_lineitem_html = '';
        var set_qty_total = 0;
        var set_basic_qty_total = 0;
        var tr_inc = 1;
//        console.log(lineitem_objectdata);
        $.each(lineitem_objectdata, function (index, value) {
            var value_uom_name = '';
            if(typeof(value.uom_name) != "undefined" && value.uom_name !== null) {
                value_uom_name = value.uom_name;
            } else {
                $.ajax({
                    url: "<?=base_url('app/set_uom_select2_val_by_id/') ?>/" + value.uom_id,
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    cache: false,
                    success: function (response) {
                        if(response.text == '--select--'){ response.text = ''; }
                        value_uom_name = response.text;
                    },
                });
            }
            
            if(value.inout_stock == 1){
                var inout_stock = '<span class="text-success text-bold">' + value.effective_stock + '</span>';
            } else {
                var inout_stock = '<span class="text-danger text-bold">' + value.effective_stock + '</span>';
            }
            if(value.project_name != '' && value.project_name != null){
                value.project_name = value.project_name;
            } else {
                value.project_name = '';
            }
            var lineitem_edit_btn = '';
            var lineitem_delete_btn = '';
            lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
            lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
            var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
                    lineitem_edit_btn +
                    lineitem_delete_btn +
                    '</td>' +
                    '<td>' + tr_inc + '</td>' +
                    '<td>' + value.item_name + '</td>';
                if(typeof(value.item_code) != "undefined" && value.item_code !== null) {
                    row_html += '<td>' + value.item_code + '</td>';
                } else {
                    row_html += '<td></td>';
                }
                    row_html += '<td class="text-right"><input type="text" id="set_qty_' + index + '" data-pts_selected_index="' + index + '" class="pts_set_qty text-right num_only" value="'+ value.set_qty + '" style="width:100px;"></td>' +
                    '<td class="text-center">' + value_uom_name + '</td>' +
                    '<td class="text-right"><input type="text" id="loss_' + index + '" data-pts_selected_index="' + index + '" class="pts_loss text-right num_only" value="'+ value.loss + '" style="width:100px;"></td>' +
                    '<td class="text-right"><input type="text" id="pts_job_expense_' + index + '" data-pts_selected_index="' + index + '" class="pts_job_expense text-right num_only" value="'+ value.job_expense + '" style="width:100px;"></td>' + 
                    '<td>' + value.project_name + '</td>';
                if(typeof(value.purchase_project_qty) != "undefined" && value.purchase_project_qty !== null) {
                    row_html += '<td class="text-right">' + value.purchase_project_qty + '</td>';
                } else {
                    row_html += '<td class="text-right"></td>';
                }
                row_html += '<td class="text-center">' + inout_stock + '</td>';
                row_html += '<td class="text-right">' + value.quantity + '</td>';
            new_lineitem_html += row_html;
            set_basic_qty_total += +value.set_qty;
            set_qty_total += +value.quantity;
            tr_inc++;
        });
        $('tbody#lineitem_list').html(new_lineitem_html);
        $('tfoot span.set_basic_qty_total').html(set_basic_qty_total.toFixed(2));
        $('#set_basic_qty_total').val(set_basic_qty_total.toFixed(2));
        $('tfoot span.set_qty_total').html(set_qty_total.toFixed(2));
        $('#set_qty_total').val(set_qty_total.toFixed(2));
        $('#ajax-loader').hide();
    }
    
    function edit_lineitem(index) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $('#ajax-loader').show();
        if (edit_lineitem_inc == 0) {
            edit_lineitem_inc = 1;
            $(".add_lineitem").removeAttr("disabled");
        }
        value = lineitem_objectdata[index];
//        console.log(value);
        if(typeof(value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
			$("#lineitem_id").val(value.lineitem_id);
		}
        $("#purchase_item_index").val(index);
        setSelect2Value($("#purchase_item_id"),"<?=base_url('app/set_purchase_item_select2_val_by_id')?>/" + value.item_id);
        $("#purchase_item_id").val(value.item_id).trigger("change");
        $("#purchase_item_code").val(value.item_code);
        $("#purchase_project_id").val(value.project_id);
        $("#purchase_project_name").val(value.project_name);
        $("#purchase_project_qty").val(value.purchase_project_qty);
        $("#purchase_item_qty").val(value.set_qty);
        $("#effective_stock").val(value.effective_stock);
        $("#loss").val(value.loss);
        $("#job_expense").val(value.job_expense);
        $("#uom_name").val(value.uom_id).trigger("change");
        setSelect2Value($("#uom_name"),"<?=base_url('app/set_uom_select2_val_by_id')?>/" + value.uom_id);
        $('#ajax-loader').hide();
    }
    
    function remove_lineitem(index) {
        if (confirm('Are you sure ?')) {
            value = lineitem_objectdata[index];
            if (typeof (value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
                $('#save_production').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.lineitem_id + '" />');
            }
            lineitem_objectdata.splice(index, 1);
            display_lineitem_html(lineitem_objectdata);
        }
    }
    
    function get_effective_amt(){
        var purchase_item_id = $('#purchase_item_id').val();
        if(purchase_item_id != '' && purchase_item_id != null){
            $.ajax({
                url: '<?php echo base_url('production_start/get_effective_amt') ?>',
                method : 'POST',
                async: false, 
                data : {purchase_item_id: purchase_item_id},
                success : function(response){
                    var json = $.parseJSON(response);
                    $('#effective_stock').val(json.effective_stock);
                }
            });
        }
    }
    
    function get_item_from_group(group_id){
        $("#purchase_item_id").val('').trigger("change");
        if (group_id != '' && group_id != null) {
            $.ajax({
                type: "GET",
                async: false,
                url: '<?= base_url(); ?>master/get_purchase_items/' + group_id,
                success: function (data) {
                    var data = $.parseJSON(data);
                    initAjaxSelect2($("#purchase_item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source')?>/" + data);
                }
            });
        } else {
            $("#purchase_item_id").val('').trigger("change");
            initAjaxSelect2($("#purchase_item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source')?>");
        }
    }

<?php if (isset($_GET['view'])) { ?>
        $(window).load(function () {
            display_as_a_viewpage();
        });
<?php } ?>
</script>


