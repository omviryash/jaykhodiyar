<?php $this->load->view('success_false_notify'); ?>
<?php
    $enquiry_allow_edit_after_quotation = 1;
    if(isset($completed) && $completed == $inquiry_data->inquiry_status_id) {
        if($this->applib->have_access_role(ENQUIRY_ALLOW_EDIT_AFTER_QUOTATION,"allow")) {
            $enquiry_allow_edit_after_quotation = 1;
        } else {
            $enquiry_allow_edit_after_quotation = 0;
        }
    } else {
        $enquiry_allow_edit_after_quotation = 1;
    }
?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('enquiry/save_enquiry') ?>" method="post" id="save_enquiry" novalidate>
        <?php if(isset($inquiry_data->inquiry_id) && !empty($inquiry_data->inquiry_id)){ ?>
            <input type="hidden" name="inquiry[inquiry_id]" class="inquiry_id" value="<?=$inquiry_data->inquiry_id?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="custom content-header">
            <h1>
                <small class="text-primary text-bold">Sales : Enquiry</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, Ctrl+S = Save</label></small>
                <?php $enquiry_add_role = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "add");
                      $enquiry_edit_role= $this->applib->have_access_role(ENQUIRY_MODULE_ID,"edit");  
                ?>
                <?php if($enquiry_add_role || $enquiry_edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
                <?php endif;?>
                <?php $enquiry_role_view = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "view"); ?>
                <?php if($enquiry_role_view):?>
                    <a href="<?=BASE_URL?>enquiry/enquiry_list/?status=pending&user_id=<?=$staff_id?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Enquiry List</a>
                <?php endif;?>
                <?php if($enquiry_add_role): ?>
                    <a href="<?=BASE_URL?>enquiry/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add Enquiry</a>
                <?php endif;?>
                <?php $quotation_add_role = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "add"); ?>
                <?php if($quotation_add_role):?>
                    <a href="<?=BASE_URL?>quotation/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Go To Quotation</a>
                <?php endif;?>
                <?php if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view")) { ?>
                    <span class="pull-right" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="inquiry[send_sms]" id="send_sms" class="send_sms" >  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } ?>
            </h1>
        </section>
        <div class="clearfix">
        <?php if($enquiry_add_role || $enquiry_edit_role): ?>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Enquiry Details</a></li>
                        <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                        <!--<li><a href="#tab_3" data-toggle="tab">Confirmation</a></li>-->
                        <?php if(isset($inquiry_data->inquiry_id) && !empty($inquiry_data->inquiry_id)){ ?>
                        <li><a href="#tab_4" data-toggle="tab"  id="tabs_3">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Enquiry Details</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inq_no" style="" class="col-sm-3 input-sm">Enquiry No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="inq_no" name="inquiry[inquiry_no]" value="<?=(isset($inquiry_data->inquiry_no))? $inquiry_data->inquiry_no : ''; ?>" readonly=""/>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inquiry_stage_id" class="col-sm-3 input-sm">Enquiry Stage</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="inquiry[inquiry_stage_id]" id="inquiry_stage_id" class="form-control input-sm select2"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inquiry_date" class="col-sm-3 input-sm">Enquiry Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm pull-right" id="inquiry_date" name="inquiry[inquiry_date]" value="<?=(isset($inquiry_data->inquiry_date))? date('d-m-Y', strtotime($inquiry_data->inquiry_date)) : date('d-m-Y'); ?>" readonly="" >
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_id" class="col-sm-3 input-sm">Sales</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="inquiry[sales_id]" id="sales_id" class="form-control input-sm select2 sales_id " disabled="disabled"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_type_id" class="col-sm-3 input-sm">Sales Type</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="inquiry[sales_type_id]" id="sales_type_id" class="form-control input-sm select2 sales_type_id"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="industry_type_id" class="col-sm-3 input-sm">Industry Type</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="inquiry[industry_type_id]" id="industry_type_id" class="form-control input-sm select2">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inquiry_status_id" class="col-sm-3 input-sm">Status</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <?php if($enquiry_allow_edit_after_quotation == 1){ ?>
                                                            <select name="inquiry[inquiry_status_id]" class="form-control input-sm" id="inquiry_status_id" ></select>
                                                        <?php } else { ?>
                                                            <select name="inquiry[inquiry_status_id]" class="form-control input-sm" id="inquiry_status_id" disabled ></select>
                                                            <input type="hidden" name="inquiry[inquiry_status_id]" value="<?php echo $inquiry_data->inquiry_status_id; ?>" >
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="assigned_to_id" class="col-sm-3 input-sm">Assigned To</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="inquiry[assigned_to_id]" id="assigned_to_id" class="form-control input-sm select2 assigned_to_id" <?=empty($this->applib->have_access_role(CAN_ASSIGNE_ENQUIRY_ID,"allow")) ? 'disabled' : '';?> ></select>
														<?php if(empty($this->applib->have_access_role(CAN_ASSIGNE_ENQUIRY_ID,"allow"))){ ?>
														<input type="hidden" name="inquiry[assigned_to_id]" value="<?=(isset($inquiry_data->assigned_to_id)) ? $inquiry_data->assigned_to_id : $staff_id; ?>">
														<?php } ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="lead_or_inquiry" class="col-sm-3 input-sm">Lead or Enquiry<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="inquiry[lead_or_inquiry]" id="lead_or_inquiry" class="form-control input-sm select2 ">
                                                            <option value="">--Select--</option>
                                                            <option value="lead" <?=(isset($inquiry_data->lead_or_inquiry) && $inquiry_data->lead_or_inquiry == 'lead')? 'Selected' : ''; ?>>Lead</option>
                                                            <option value="enquiry" <?=(isset($inquiry_data->lead_or_inquiry) && $inquiry_data->lead_or_inquiry == 'enquiry')? 'Selected' : ''; ?> <?=(isset($inquiry_data->lead_or_inquiry))? '' : 'Selected'; ?> >Enquiry</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="datepicker2" class="col-sm-3 input-sm">Next Reminder Date</label>
                                                    <div class="col-sm-5">
                                                        <input type="text" name="inquiry[due_date]" class="form-control input-sm pull-right due_date" id="datepicker2" value="<?=(isset($inquiry_data->due_date))? date('d-m-Y', strtotime($inquiry_data->due_date)) : date('d-m-Y'); ?>" >
                                                    </div>
                                                    <button type="button" class="btn btn-info btn-xs btn_follow_up_history">Follow Up History</button>
                                                </div>
                                            </div>
                                        </div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Party Details</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select name="inquiry[party_id]" id="party_id" class="form-control input-sm" onchange="party_details(this.value)"></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" placeholder="" readonly>
													</div>
												</div>
												<div class="clearfix"></div>
												<input type="hidden" class="form-control input-sm party_name" id="party_name" name="party[party_name]" placeholder="">
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="form-group">
													<label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
													<div class="col-sm-9">
														<textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
														<textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="city" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="state" class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="country" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
														<small>Add multiple email id by Comma separated.</small>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9">
														<textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
														<small>Add multiple phone number by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_website" class="col-sm-3 input-sm">Website</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm website" id="party_website" name="party[website]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="agent_id" class="col-sm-3 input-sm">Agent</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[agent_id]" id="agent_id" class="form-control input-sm" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="inquiry[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
															<option value="">--Select--</option>
                                                            <?php if(isset($inquiry_data->inquiry_id) && !empty($inquiry_data->inquiry_id)){ ?>
                                                                <?php foreach($party_contact_person as $pcp_key => $pcp_value): ?>
                                                                <option value="<?= $pcp_value->contact_person_id; ?>"  <?=$inquiry_data->kind_attn_id == $pcp_value->contact_person_id ?'selected="selected"':'';?>><?= $pcp_value->name; ?></option>
                                                                <?php endforeach; ?>
                                                            <?php } ?>
														</select>
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Designation</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_designation"><?php echo isset($contact_person->designation) ? $contact_person->designation : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Department</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_department"><?php echo isset($contact_person->department) ? $contact_person->department : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person->mobile_no) ? $contact_person->mobile_no : '' ;?>" disabled="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person->email) ? $contact_person->email : '' ;?>" disabled="">
                                                    </div>
                                                </div>
											</div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($inquiry_lineitems)){ ?>
                                            <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="item_id" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id" onchange="item_details(this.value)"></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_code]" id="item_code" class="form-control input-sm item_code disabled" data-name="item_code" readonly="readonly">
                                                    <input type="hidden" class="item_name" id="item_name" name="line_items_data[item_name]" data-name="item_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_description]" id="item_description" class="form-control input-sm item_description" data-name="item_description" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity" class="col-sm-3 input-sm">Qty<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity num_only" value="1" data-name="quantity" placeholder="">
                                                </div>
                                                <div class="clearfix"></div>
                                                <label for="rate" class="col-sm-3 input-sm">Rate</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <select name="line_items_data[currency_id]" id="currency_id" class="form-control input-sm currency_id" ></select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate"  value="0" placeholder="">
                                                </div>
                                                <div class="clearfix"></div>
                                                <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-sm-12"><div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" />
                                            </div>
                                        </div></div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <table style="" class="table custom-table item-table">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lineitem_list"></tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th class="text-right"><span class="qty_total"></span><input type="hidden" name="qty_total" id="qty_total" /></th>
                                                <th class="text-right"></th>
                                                <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Enquiry Received By</legend>
                                                <div class="form-group">
                                                    <label for="is_received_post" class="col-sm-12 input-sm">
                                                        <input type="checkbox" class="enquiry_received_by is_received_post" name="inquiry[is_received_post]" id="is_received_post" value="1" <?=(isset($inquiry_data->is_received_post) && $inquiry_data->is_received_post == 1) ?'checked="checked"':''; ?>> &nbsp; Get data from party
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="is_received_post_section <?=(isset($inquiry_data->is_received_post) && $inquiry_data->is_received_post == 1) ?'':' hidden '; ?>">
                                                    <div class="form-group">
                                                        <label for="received_email" class="col-sm-3 input-sm">E-Mail</label>
                                                        <div class="col-sm-9">
                                                            <input type="email" class="form-control input-sm received_email" id="received_email" name="inquiry[received_email]" value="<?=(isset($inquiry_data->received_email)) ? $inquiry_data->received_email : ''; ?>" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="received_fax" class="col-sm-3 input-sm">Fax</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm received_fax" id="received_fax" name="inquiry[received_fax]" value="<?=(isset($inquiry_data->received_fax)) ? $inquiry_data->received_fax : ''; ?>" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="received_verbal" class="col-sm-3 input-sm">Verbal</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm received_verbal" id="received_verbal" name="inquiry[received_verbal]" value="<?=(isset($inquiry_data->received_verbal)) ? $inquiry_data->received_verbal : ''; ?>" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Send Confirmation By</legend>
                                                <div class="form-group">
                                                    <label for="is_send_post" class="col-sm-12 input-sm">
                                                        <input type="checkbox" class="enquiry_received_by is_send_post" name="inquiry[is_send_post]" id="is_send_post" value="1" <?=(isset($inquiry_data->is_send_post) && $inquiry_data->is_send_post == 1) ?'checked="checked"':''; ?>> &nbsp; Get data from party
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="is_send_post_section <?=(isset($inquiry_data->is_send_post) && $inquiry_data->is_send_post == 1) ?'':' hidden '; ?>"">
                                                    <div class="form-group">
                                                        <label for="send_email" class="col-sm-3 input-sm">E-Mail</label>
                                                        <div class="col-sm-9">
                                                            <input type="email" class="form-control input-sm send_email" id="send_email" name="inquiry[send_email]" value="<?=(isset($inquiry_data->send_email)) ? $inquiry_data->send_email : ''; ?>" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="send_fax" class="col-sm-3 input-sm">Fax</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm send_fax" id="send_fax" name="inquiry[send_fax]" value="<?=(isset($inquiry_data->send_fax)) ? $inquiry_data->send_fax : ''; ?>" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="send_from_post" class="col-sm-12 input-sm">
                                                        <input type="checkbox" class="enquiry_received_by send_from_post" name="inquiry[send_from_post]" id="send_from_post" value="1" <?=(isset($inquiry_data->send_from_post) && $inquiry_data->send_from_post == 1) ?'checked="checked"':''; ?>> &nbsp; Post
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="send_from_post_section <?=(isset($inquiry_data->send_from_post) && $inquiry_data->send_from_post == 1) ?'':' hidden '; ?>"">
                                                    <div class="form-group send_from_post_name_div">
                                                        <label for="send_from_post_name" class="col-sm-3 input-sm">Send From</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm send_from_post_name" id="send_from_post_name" name="inquiry[send_from_post_name]" value="<?=(isset($inquiry_data->send_from_post_name)) ? $inquiry_data->send_from_post_name : ''; ?>" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <?php if(isset($inquiry_data->inquiry_id) && !empty($inquiry_data->inquiry_id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=$inquiry_data->created_by_name;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=$inquiry_data->created_at;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=$inquiry_data->updated_by_name;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=$inquiry_data->updated_at;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        <?php endif;?>
        </div>
        <section class="custom content-header">
            <?php $enquiry_add_role = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "add"); ?>
            <?php if($enquiry_add_role || $enquiry_edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
            <?php endif;?>
            <?php $enquiry_role_view = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "view"); ?>
            <?php if($enquiry_role_view):?>
                <a href="<?=BASE_URL?>enquiry/enquiry_list/?status=pending&user_id=<?=$staff_id?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Enquiry List</a>
            <?php endif;?>
            <?php if($enquiry_add_role): ?>
                <a href="<?=BASE_URL?>enquiry/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add Enquiry</a>
            <?php endif;?>
            <?php $quotation_add_role = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "add"); ?>
            <?php if($quotation_add_role):?>
                <a href="<?=BASE_URL?>quotation/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Go To Quotation</a>
            <?php endif;?>
        </section>
        <div class="modal" id="modal_follow_up_history">
            <div class="modal-dialog" style="width:80%; height: auto; max-height: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title col-sm-9">Enquiry Followup History</h4>
                        <button type="button" class="close btn-close-follow-up-history"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="scheduler-border item-detail item_fields_div">
                                    <input type="hidden" name="followup_history_index" id="followup_history_index" />
                                    <?php if(isset($followup_history_data)){ ?>
                                        <input type="hidden" name="followup_history_data[id]" id="followup_history_id" />
                                    <?php } ?>
                                    <legend class="scheduler-border text-primary text-bold">Enquiry Followup History</legend>
                                    <input type="hidden" name="followup_history_data[followup_by]" id="followup_by" class="followup_by" value="<?=$this->session->userdata('is_logged_in')['staff_id'];?>" />
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="followup_history_date" class="col-sm-3 input-sm">Date</label>
                                            <div class="col-sm-5">
                                                <input type="hidden" class="form-control input-sm pull-right" name="followup_history_data[followup_history_date]" id="followup_history_date" value="<?= date('d-m-Y H:i:s'); ?>" readonly="">
                                                <input type="text" class="form-control input-sm pull-right" name="followup_history_data[followup_history_onlydate]" id="followup_history_onlydate" value="<?= date('d-m-Y'); ?>" readonly="">
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control input-sm pull-right" name="followup_history_data[followup_history_onlytime]" id="followup_history_onlytime" value="<?= date('H:i:s'); ?>" readonly="">
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="followup_history_history" class="col-sm-3 input-sm">History</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="4" name="followup_history_data[followup_history_history]" id="followup_history_history" required ></textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="button" id="add_followup_history" class="btn btn-info btn-sm add_followup_history" value="Add" />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div> <br />
                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-10">
                                <table style="" class="table custom-table item-table">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>History</th>
                                            <th>Followed Up By</th>
                                        </tr>
                                    </thead>
                                    <tbody id="followup_history_list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </form>
    <div class="clearfix"></div>
</div>  
<script type="text/javascript">
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
	var lineitem_objectdata = [];
    <?php if(isset($inquiry_lineitems)){ ?>
		var li_lineitem_objectdata = [<?php echo $inquiry_lineitems; ?>];
		var lineitem_objectdata = [];
        if(li_lineitem_objectdata != ''){
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
	<?php } ?>
    display_lineitem_html(lineitem_objectdata);
    var edit_followup_history_inc = 0;
	var followup_history_objectdata = [];
    <?php if(isset($followup_history_data)){ ?>
		var li_followup_history_objectdata = [<?php echo $followup_history_data; ?>];
		var followup_history_objectdata = [];
        if(li_followup_history_objectdata != ''){
            $.each(li_followup_history_objectdata, function (index, value) {
                followup_history_objectdata.push(value);
            });
        }
	<?php } ?>
    display_followup_history_html(followup_history_objectdata);
    <?php if(isset($inquiry_data->inquiry_id) && !empty($inquiry_data->inquiry_id)){ ?>
        first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function(){
        $('.select2').select2();
        $('input[type="checkbox"].send_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        initAjaxSelect2($("#party_id"),"<?=base_url('app/party_select2_source')?>");
        <?php if(isset($inquiry_data->party_id)){ ?>
			setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id/'.$inquiry_data->party_id)?>");
		<?php } ?>
        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
        initAjaxSelect2($("#agent_id"),"<?=base_url('app/agent_select2_source')?>");
        initAjaxSelect2($("#inquiry_status_id"),"<?=base_url('app/inquiry_status_select2_source')?>");
        setSelect2Value($("#inquiry_status_id"),"<?=base_url('app/set_inquiry_status_select2_val_by_id/1')?>");
        <?php if(isset($inquiry_data->inquiry_status_id)){ ?>
            setSelect2Value($("#inquiry_status_id"),"<?=base_url('app/set_inquiry_status_select2_val_by_id/'.$inquiry_data->inquiry_status_id)?>");
        <?php } ?>
        initAjaxSelect2($("#inquiry_stage_id"),"<?=base_url('app/inquiry_stage_select2_source')?>");
        setSelect2Value($("#inquiry_stage_id"),"<?=base_url('app/set_inquiry_stage_select2_val_by_id/'.DEFAULT_INQUIRY_STAGE_ID)?>");
        <?php if(isset($inquiry_data->inquiry_stage_id)){ ?>
            setSelect2Value($("#inquiry_stage_id"),"<?=base_url('app/set_inquiry_stage_select2_val_by_id/'.$inquiry_data->inquiry_stage_id)?>");
        <?php } ?>
        initAjaxSelect2($("#industry_type_id"),"<?=base_url('app/industry_type_select2_source')?>");
        setSelect2Value($("#industry_type_id"),"<?=base_url('app/set_industry_type_select2_val_by_id/'.DEFAULT_INDUSTRY_TYPE_ID)?>");
        <?php if(isset($inquiry_data->industry_type_id)){ ?>
            setSelect2Value($("#industry_type_id"),"<?=base_url('app/set_industry_type_select2_val_by_id/'.$inquiry_data->industry_type_id)?>");
        <?php } ?>
        initAjaxSelect2($("#assigned_to_id"),"<?=base_url('app/staff_select2_source')?>");
        setSelect2Value($("#assigned_to_id"),"<?=base_url('app/set_staff_select2_val_by_id/'.$staff_id)?>");
        <?php if(isset($inquiry_data->assigned_to_id)){ ?>
            setSelect2Value($("#assigned_to_id"),"<?=base_url('app/set_staff_select2_val_by_id/'.$inquiry_data->assigned_to_id)?>");
        <?php } ?>
        initAjaxSelect2($("#sales_type_id"),"<?=base_url('app/sales_type_select2_source')?>");
        <?php if(isset($inquiry_data->sales_type_id)){ ?>
            setSelect2Value($("#sales_type_id"),"<?=base_url('app/set_sales_type_select2_val_by_id/'.$inquiry_data->sales_type_id)?>");
        <?php } ?>
        initAjaxSelect2($("#reference_id"),"<?=base_url('app/reference_select2_source')?>");
//        $(document).on('click', '#item_id', function () {
//            if($.trim($('#party_id').val()) == '' || $.trim($('#party_id').val()) == 'null'){
//                initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/');
//                $('#item_id').data('select2').destroy();
//                show_notify('Please Select Party.',false); //Punit@Hari@321
//                return false;
//            }
//        });
        $(document).on('click', '#item_id', function () {
            $('#item_id').select2();
            $('#item_id').select2('destroy');
            $('#item_id').find('option').remove().end();
            $('#item_id').select2();
            if($.trim($('#party_id').val()) == '' || $.trim($('#party_id').val()) == 'null'){
                show_notify('Please Select Party.',false);
                return false;
            } else {
                var party_type_id = $("#sales_id").val();
                if(party_type_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_DOMESTIC_ID; ?>);
                } else if (party_type_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_EXPORT_ID; ?>);
                }
            }
        });
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source')?>");
    
        $(".due_date").on("change",function (){ 
            $('#inquiry_due_date').val(this.value);
        });
        
        $(document).on('input','#item_rate',function () {
            if(this.value > 9900000){
                this.value = 9900000;
            }
        });
        
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if(target == '#tab_1'){
                $('#party_id').select2('open');
                $('#item_id').select2('close');
            } else if(target == '#tab_2'){
                $('#item_id').select2('open');
                $('#party_id').select2('close');
            } else {
                $('#item_id').select2('close');
                $('#party_id').select2('close');
            }
        });
        
        $(document).on('click', '.btn_follow_up_history', function () {
            $("#modal_follow_up_history").modal('show');
        });
        
        $(document).on('click', '.btn-close-follow-up-history', function () {
            if ($('textarea[name="followup_history_history"]').val() != '') {
                var confirm_value = confirm('Are you sure ?');
                if (confirm_value) {
                    $("#modal_follow_up_history").modal('hide');
                }
            } else {
                $("#modal_follow_up_history").modal('hide');
            }
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_enquiry").submit();
                return false;
            }
        });
        
        $(document).on('submit', '#save_enquiry', function () {
        
            if($.trim($("#party_id").val()) == ''){
                show_notify('Please Select Party.',false);
                return false;
            }
            if($.trim($("#reference_id").val()) == ''){
                show_notify('Please Select Reference.',false);
                return false;
            }
            if($.trim($("#lead_or_inquiry").val()) == ''){
                show_notify('Please Select Lead or Inquiry.',false);
                return false;
            }
            
            if($.trim($("#item_id").val()) != ''){
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }
//            if(lineitem_objectdata == ''){
//				show_notify("Please select any one Item.", false);
//				return false;
//			}
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
			var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
			postData.append('line_items_data', lineitem_objectdata_stringify);
			var followup_history_objectdata_stringify = JSON.stringify(followup_history_objectdata);
			postData.append('followup_history_data', followup_history_objectdata_stringify);
			$.ajax({
				url: "<?=base_url('enquiry/save_enquiry') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('enquiry/add') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('enquiry/enquiry_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });

        $(document).on("ifChanged",".is_received_post",function(){
            if($(this).is(":checked")){
                $(".is_received_post_section").removeClass('hidden');
                var party_id = $('[name="party[party_id]"]').val();
                if(party_id != '') {
                    $.ajax({
                        url: $.core.url('sales/get_enquiry_received_by_detail/'+party_id),
                        type: "POST",
                        data:null,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success == true) {
                                var  received_by_detail = data.received_by_detail
                                $('[name="inquiry[received_email]"]').val(received_by_detail.email_id);
                                $('[name="inquiry[received_fax]"]').val(received_by_detail.fax_no);
                                $('[name="inquiry[received_verbal]"]').val(received_by_detail.verbal);
                            }
                        }
                    });
                }
            }else{
                $(".is_received_post_section").addClass('hidden');
            }
        });

        $(document).on("ifChanged",".is_send_post",function(){
            if($(this).is(":checked")){
                $(".is_send_post_section").removeClass('hidden');
                var party_id = $('[name="party[party_id]"]').val();
                if(party_id != '') {
                    $.ajax({
                        url: $.core.url('sales/get_enquiry_send_by_detail/'+party_id),
                        type: "POST",
                        data:null,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success == true) {
                                var  send_by_detail = data.send_by_detail;
                                $('[name="inquiry[send_email]"]').val(send_by_detail.email_id);
                                $('[name="inquiry[send_fax]"]').val(send_by_detail.fax_no);
                            }
                        }
                    });
                }
            }else{
                $(".is_send_post_section").addClass('hidden');
            }

        });

        $(document).on("ifChanged",".send_from_post",function(){
            if($(this).is(":checked")){
                $(".send_from_post_section").removeClass('hidden');
            }else{
                $(".send_from_post_section").addClass('hidden');
            }
        });
        
        $(document).on('input', '#quantity,#item_rate', function () {
            setAmount();
        });
        
        $(document).on("change",'#contact_person_id',function() {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?=base_url();?>party/get-contact-person-by-id/"+contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });
        
        $(document).on("change",'#currency_id',function(){
            if(edit_lineitem_inc == 0){
                var currency_id = $("#currency_id").val();
                var item_id = $("#item_id").val();
                if(item_id != '' && item_id != null) {
                    $.ajax({
                        type: "POST",
                        url: '<?=base_url();?>sales/ajax_load_item_details/' + item_id,
                        data: id = 'item_id',
                        success: function (data) {
                            var json = $.parseJSON(data);
                            if (json['item_rate_in']) {
                                var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                                $(".item_rate").val(item_rate_in_json[currency_id]);
                            } else {
                                $(".item_rate").val('');
                            }
                            setAmount();
                        }
                    });
                }
            } else { edit_lineitem_inc = 0; }
        });
        
        $('#add_lineitem').on('click', function() {
			var item_id = $("#item_id").val();
			if(item_id == '' || item_id == null){
				show_notify("Please select Item!", false);
				return false;
			}
			var quantity = $("#quantity").val();
			if(quantity == '' || quantity == null){
				show_notify("Item quantity is required!", false);
				return false;
			}
			
			var key = '';
			var value = '';
			var lineitem = {};
			$('select[name^="line_items_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			$('input[name^="line_items_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			var new_lineitem = JSON.parse(JSON.stringify(lineitem));
			var line_items_index = $("#line_items_index").val();
			if(line_items_index != ''){
				lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
			} else {
				lineitem_objectdata.push(new_lineitem);
			}
			display_lineitem_html(lineitem_objectdata);
			$('#lineitem_id').val('');
            $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
			$("#item_id").val(null).trigger("change");
			$("#quantity").val('1');
			$("#line_items_index").val('');
            if(on_save_add_edit_item == 1){
                on_save_add_edit_item == 0;
                $('#save_enquiry').submit();
            }
            var sales_id = $("#sales_id").val();
            if(sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
            }
            if(sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
            }
		});
        
        $('#add_followup_history').on('click', function() {
			var followup_history_date = $("#followup_history_date").val();
			if(followup_history_date == '' || followup_history_date == null){
				show_notify("Please Enter History Date!", false);
				return false;
			}
			var followup_history_history = $("#followup_history_history").val();
			if(followup_history_history == '' || followup_history_history == null){
				show_notify("Please Enter History!", false);
				return false;
			}
			
			var key = '';
			var value = '';
			var followup_history = {};
            followup_history['followup_by'] = $('#followup_by').val();
            followup_history['followup_history_date'] = $('#followup_history_date').val();
            followup_history['followup_history_onlydate'] = $('#followup_history_onlydate').val();
            followup_history['followup_history_onlytime'] = $('#followup_history_onlytime').val();
            followup_history['followup_history_history'] = $('#followup_history_history').val();
			var new_followup_history = JSON.parse(JSON.stringify(followup_history));
			var followup_history_index = $("#followup_history_index").val();
			if(followup_history_index != ''){
				followup_history_objectdata.splice(followup_history_index, 1, new_followup_history);
			} else {
				followup_history_objectdata.push(new_followup_history);
			}
			display_followup_history_html(followup_history_objectdata);
			$('#followup_history_id').val('');
            $("#followup_history_date").val('<?php echo date('d-m-Y H:i:s'); ?>');
            $("#followup_history_onlydate").val('<?php echo date('d-m-Y'); ?>');
            $("#followup_history_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#followup_history_history").val('');
            $("#followup_history_index").val('');
        });
    });
    
    function display_lineitem_html(lineitem_objectdata){
		var new_lineitem_html = '';
		var qty_total = 0;
		var amount_total = 0;
//		console.log(lineitem_objectdata);
		$.each(lineitem_objectdata, function (index, value) {
            var value_currency;
			$.ajax({
				url: "<?=base_url('app/set_currency_select2_val_by_id/') ?>/" + value.currency_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_currency = response.text;
				},
			});
            
            var lineitem_edit_btn = ''; var lineitem_delete_btn = '';
            <?php if($enquiry_allow_edit_after_quotation == 1){ ?>
                lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
                lineitem_delete_btn = ' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a> ';
            <?php } ?>
			var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
			lineitem_edit_btn + lineitem_delete_btn +
			'</td>' +
			'<td>' + value.item_code + '</td>' +
			'<td>' + value.item_name + '</td>' +
            '<td class="text-right">' + value.quantity + '</td>';
            if(value_currency != ''){
                row_html += '<td class="text-right">' + value_currency + ' - ' + value.item_rate + '</td>';
            } else {
                row_html += '<td class="text-right">' + value.item_rate + '</td>';
            }
            row_html += '<td class="text-right">' + value.amount + '</td></tr>';
			new_lineitem_html += row_html;
			qty_total += parseInt(value.quantity);
			amount_total += parseInt(value.amount);
		});
		$('tfoot span.qty_total').html(qty_total); $('#qty_total').val(qty_total);
		$('tfoot span.amount_total').html(amount_total);$('#amount_total').val(amount_total);
		$('tbody#lineitem_list').html(new_lineitem_html);
	}
    
    function edit_lineitem(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_lineitem_inc == 0){  edit_lineitem_inc = 1; /*$('.edit_lineitem_'+ index).click();*/ }
		value = lineitem_objectdata[index];
		$("#line_items_index").val(index);
		
                var party_type_id = $("#sales_id").val();
                if(party_type_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_DOMESTIC_ID; ?>);
                } else if (party_type_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_EXPORT_ID; ?>);
                }
		setSelect2Value($("#item_id"),"<?=base_url('app/set_li_item_select2_val_by_id/')?>/" + value.item_id);
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source/')?>");
		setSelect2Value($("#currency_id"),"<?=base_url('app/set_currency_select2_val_by_id/')?>/" + value.currency_id);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#lineitem_id").val(value.id);
		}
        $("#item_code").val(value.item_code);
        $("#item_name").val(value.item_name);
        $("#item_description").val(value.item_description);
        $("#item_rate").val(value.item_rate);
        $("#quantity").val(value.quantity);
        $("#amount").val(value.amount);
		$('.overlay').hide();
	}
	
	function remove_lineitem(index){
        if(confirm('Are you sure ?')){
            value = lineitem_objectdata[index];
            if(typeof(value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index,1);
            display_lineitem_html(lineitem_objectdata);
        }
	}
    
    function display_followup_history_html(followup_history_objectdata){
		var new_followup_history_html = '';
//		console.log(followup_history_objectdata); 
		$.each(followup_history_objectdata, function (index, value) {
            var value_item_name;
			$.ajax({
				url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + value.followup_by,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_item_name = response.text;
				},
			});
            var followup_history_edit_btn = '';
			followup_history_edit_btn = '<a class="btn btn-xs btn-primary edit_followup_history_' + index + '" href="javascript:void(0);" onclick="edit_followup_history(' + index + ')"><i class="fa fa-edit"></i></a> ';
            var followup_history_row_html = '<tr class="followup_history_index_' + index + '"><td class="">';
			followup_history_row_html += followup_history_edit_btn;
			followup_history_row_html += ' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_followup_history(' + index + ')"><i class="fa fa-remove"></i></a> ';
			followup_history_row_html += '</td>';
			followup_history_row_html += '<td>' + value.followup_history_onlydate + '</td>';
			followup_history_row_html += '<td>' + value.followup_history_onlytime + '</td>';
			followup_history_row_html += '<td>' + value.followup_history_history + '</td>';
			followup_history_row_html += '<td>' + value_item_name + '</td>';
            new_followup_history_html += followup_history_row_html;
		});
		$('tbody#followup_history_list').html(new_followup_history_html);
	}
    
    function edit_followup_history(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_followup_history_inc == 0){  edit_followup_history_inc = 1; $('.edit_followup_history_'+ index).click(); }
		value = followup_history_objectdata[index];
		$("#followup_history_index").val(index);
		
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#followup_history_id").val(value.id);
		}
        $("#followup_history_date").val(value.followup_history_date);
        $("#followup_history_onlydate").val(value.followup_history_onlydate);
        $("#followup_history_onlytime").val(value.followup_history_onlytime);
        $("#followup_history_history").val(value.followup_history_history);
		$('.overlay').hide();
	}
	
	function remove_followup_history(index){
        if(confirm('Are you sure ?')){
            value = followup_history_objectdata[index];
            if(typeof(value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_followup_history_id[]" id="deleted_followup_history_id" value="' + value.id + '" />');
            }
            followup_history_objectdata.splice(index,1);
            display_followup_history_html(followup_history_objectdata);
        }
	}
    
    function convertDate(inputFormat) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date(inputFormat);
        return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
    }
    
    function setAmount(){
        var quantity = $("#quantity").val();
        var rate = $("#item_rate").val();
        var amount = quantity * rate;
        $("#amount").val(parseF(amount));
    }
    function parseF(value, decimal) {
        decimal = 0;
        return value ? parseFloat(value).toFixed(decimal) : 0;
    }
    
    
    function party_details(id){
        $("#ajax-loader").show();
        $("#sales_id").val('');
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/'+id,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                }else{
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"),'<?=base_url()?>app/set_city_select2_val_by_id/'+json['city_id']);
                }else{
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                }else{
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                }else{
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                }else{
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }else{
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                }else{
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                }else{
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                }else{
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"),'<?=base_url()?>app/set_reference_select2_val_by_id/'+json['reference_id']);
                }else{
                    setSelect2Value($("#reference_id"));
                }
                if(json['reference_description']){
                    $("#reference_description").html(json['reference_description']);
                }else{
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"),'<?=base_url()?>app/set_sales_select2_val_by_id/'+json['party_type_1_id']);
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
                    }
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
                    }
                }else{
                    setSelect2Value($("#sales_id"));
                }
                
                if(id != ''){
                    var party_type_id = $("#sales_id").val();
                        $('#item_id').select2();
                        $('#item_id').select2('destroy');
                        $('#item_id').find('option').remove().end();
                        $('#item_id').select2();
                        if(party_type_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                            initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_DOMESTIC_ID; ?>);
                        } else if (party_type_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                            initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_EXPORT_ID; ?>);
                        }
                }

                if (json['party_type_2_id'] != '') { 
                    setSelect2Value($("#sales_type_id"),'<?=base_url()?>app/set_sales_type_select2_val_by_id/'+json['party_type_2_id']);
                }else{
                    setSelect2Value($("#sales_type_id"));
                }

                if (json['agent_id']) {
                    setSelect2Value($("#agent_id"),'<?=base_url()?>app/set_agent_select2_val_by_id/'+json['agent_id']);
                }else{
                    setSelect2Value($("#agent_id"));
                }

                if(first_time_edit_mode == 1){
                    if (json['contact_persons_array']) {
                        var option_html = '';
                        if(json['contact_persons_array'].length > 0){
//                            console.log(json['contact_persons_array']);
                            $.each(json['contact_persons_array'],function(index,value){
                                option_html += "<option value='"+value.contact_person_id+"'>"+value.name+"</option>";
                            })
                            $('select[name="inquiry[kind_attn_id]"]').html(option_html).select2();
                        } else {
                            $('select[name="inquiry[kind_attn_id]"]').html('');
                        }
                        $('select[name="inquiry[kind_attn_id]"]').change();
                    }
                } else { first_time_edit_mode = 1; }
                $("#ajax-loader").hide();
            }
        });
    }
    
    function item_details(id){
        if(edit_lineitem_inc == 0){
            if(id != '') {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>sales/ajax_load_item_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
    //                    console.log(data);
                        var json = $.parseJSON(data);
                        if (json['item_name']) {
                            $(".item_name").val(json['item_name']);
                        } else {
                            $(".item_name").val("");
                        }

                        if (json['item_code']) {
                            $(".item_code").val(json['item_code']);
                        } else {
                            $(".item_code").val('');
                        }
                        
                        if (json['item_rate_in']) {
                            var currency_id = $("#currency_id").val();
                            var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                            $(".item_rate").val(item_rate_in_json[currency_id]);
                        } else {
                            $(".item_rate").val('');
                        }
                        setAmount();
                    }
                });
            }
        }
    }
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
    <?php } ?>
</script>