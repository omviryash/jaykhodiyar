<?php $column_cnt = 10; ?>
<html>
	<head>
		<title>
			<?=(isset($sales_order_data->export_order_type) && !empty($sales_order_data->export_order_type)  ? ($sales_order_data->export_order_type ==  'Sales Order' ? 'Overseas Order' : 'Sales Contract') : 'Sales Order');?>
		</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:10px;
			}
			tr > td:last-child{
				border-right: 1px solid !important;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:12px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				/*width: 33.33%;*/
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #000000;
				font-size: 12px;
			}
            /*ol{
                padding-top: 5px !important;
                padding-bottom: 5px !important;
                margin-top: 5px !important;
                margin-bottom: -50px !important;
            }
            ul{
                padding-top: 5px !important;
                padding-bottom: 5px !important;
                margin-top: 5px !important;
                margin-bottom: -50px !important;
            }*/
            li {
    	    	margin-bottom: 10px;
            }
		</style>
	</head>
	<body>
	<?php
        $CURRENCY = '';
		if($sales_order_data->currency != ''){
			$CURRENCY = strtoupper($sales_order_data->currency);
		}
	?>
		<?php if ($sales_order_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) { ?>
        <?php
            if(isset($letterpad_details->header_logo)){
				echo '<div style="text-align: '.$letterpad_details->header_logo_alignment.';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>';
			}
        ?>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="<?=$column_cnt;?>">SALES ORDER</td>
			</tr>
			<tr>
                <td class="text-center text-bold" colspan="5"><b>Manufacturer & Supplier</b></td>
				<td class=" text-bold" colspan="2">Date : </td>
				<td class=" text-bold" colspan="3"><?=strtotime($sales_order_data->sales_order_date) != 0?date('d/m/Y',strtotime($sales_order_data->sales_order_date)):date('d/m/Y')?></td>
			</tr>
			<tr>
                <td class="text-bold" colspan="5" style="font-size:15px;"><b><?=$company_details['name'];?></b></td>
				<td class="text-bold" colspan="2">Salse Order No. :</td>
				<td class="text-bold" colspan="3"><?=$this->applib->get_sales_order_no($sales_order_data->sales_order_no);?></td>
			</tr>
			<tr>
				<td class="no-border-top text-bold" rowspan="4" colspan="5">
					<?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
                    <b>GST No : <?=$company_details['gst_no'];?></b>
                </td>
				<td class="text-bold" colspan="2">Quotation No. : </td>
				<td class="text-bold" colspan="3"><?=$this->applib->get_quotation_ref_no($sales_order_data->quotation_no,$sales_order_item['item_code']);?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Purchase Order No.:</td>
				<td class=" text-bold" colspan="2">
					<?php
						if(!empty($sales_order_data->cust_po_no)){
							echo $sales_order_data->cust_po_no;
						} else {
							echo $sales_order_data->sales_order_pref;
						}
					?>
				</td>
                <td class="text-bold"><?=strtotime($sales_order_data->po_date) != 0?date('d/m/Y',strtotime($sales_order_data->po_date)):date('d/m/Y')?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Sales Executive :</td>
				<td class=" text-bold" colspan="3"><?=$sales_order_data->created_name?></td>
			</tr>
			<tr>
                <td class=" text-bold" colspan="2">Validity of Order : </td>
				<td class=" text-bold" colspan="3"><?=$sales_order_data->sales_order_validaty?> Days</td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10"><b>Supplier Terms</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Transportation By : </td>
				<td class="text-center" colspan="2"><?=$sales_order_data->transportation_by;?></td>
				<td class="text-center"><?=$sales_order_data->transport_amount;?></td>
				<td class=" text-bold" colspan="3">Foundation Drawing Provide</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->foundation_drawing_required;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Road Insurance By</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->road_insurance_by;?></td>
				<td class="text-center"><?=$sales_order_data->insurance_amount;?></td>
				<td class=" text-bold" colspan="3">Installation & Commissioning</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->erection_commissioning;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Oil Barrel (Drum 210 Liter)</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->oil_barrel;?></td>
				<td class="text-center"><?=$sales_order_data->oil_barrel_amount;?></td>
				<td class=" text-bold" colspan="3">Loading By : </td>
				<td class="text-center" colspan="2"><?=$sales_order_data->loading_by;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Third party inspection cost</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->third_party_inspection_cost;?></td>
				<td class="text-center"><?=$sales_order_data->third_party_inspection_cost_amount;?></td>
				<td class=" text-bold" colspan="3">Unloading By</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->unloading_by;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Machine To All Motor Cable</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->machine_to_all_motor_cable;?></td>
				<td class="text-center"><?=$sales_order_data->machine_to_all_motor_cable_amount;?></td>
				<td class=" text-bold" colspan="3">Inspection Required</td>
				<td class="text-center" colspan="2"><?=$sales_order_data->inspection_required;?></td>
			</tr>
			<tr>
				<td class="text-center text-bold width-50-pr" colspan="5"><b>Purchaser</b></td>
                <td class="text-center text-bold width-50-pr" colspan="5"><b>Mode Of Shipment</b></td>
			</tr>
			<tr>
                <td class="text-bold" colspan="5" style="font-size:15px;"><b><?=$sales_order_data->party_name?></b></td>
				<td class=" text-bold" colspan="5">Name : <?=$sales_order_data->mode_of_shipment_name?>, Truck No: <?=$sales_order_data->mode_of_shipment_truck_number?></td>
			</tr>
			<tr>
				<td class="no-border-top text-bold" rowspan="5" colspan="5">
					<?=nl2br($sales_order_data->address);?><br />
					City : <?=$sales_order_data->city?> -  <?=$sales_order_data->pincode?> (<?=$sales_order_data->state?>) <?=$sales_order_data->country?>.<br />
					Email : <?=explode(",", $sales_order_data->party_email_id)[0];?><br />
					Tel No.:  <?=$sales_order_data->fax_no;?>,<br />
					Contact No.:  <?=$sales_order_data->p_phone_no;?><?= ($sales_order_data->p_phone_no != $sales_order_data->contact_person_phone) ? ', '.$sales_order_data->contact_person_phone : '' ?><br />
					Contact Person: <?=$sales_order_data->contact_person_name?><br />
                    <b>GST No. : <?=$sales_order_data->p_gst_no;?></b><?= isset($sales_order_data->party_cin_no) && !empty($sales_order_data->party_cin_no) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$sales_order_data->party_cin_no.'</b>' : ''; ?>
				</td>
				<td class="text-center text-bold" colspan="5"><b>Delivery Schedule</b></td>
			</tr>
			<tr>
				<td class=" text-bold"><b>Minimum</b></td>
				<td class="text-center text-bold"><?=$sales_order_data->mach_deli_min_weeks;?> Days</td>
				<td class="text-center text-bold" colspan="3">
					<?php
						if($sales_order_data->mach_deli_min_weeks != 0){
							echo strtotime($sales_order_data->mach_deli_min_weeks_date) != 0?date('d/m/Y',strtotime($sales_order_data->mach_deli_min_weeks_date)):date('d/m/Y');
						}
					?>
				</td>
			</tr>
			<tr>
                <td class=" text-bold"><b>Maximum</b></td>
				<td class="text-center text-bold"><?=$sales_order_data->mach_deli_max_weeks;?> Days</td>
				<td class="text-center text-bold" colspan="3">
					<?php 
						if($sales_order_data->mach_deli_max_weeks != 0){
							echo strtotime($sales_order_data->mach_deli_max_weeks_date) != 0?date('d/m/Y',strtotime($sales_order_data->mach_deli_max_weeks_date)):date('d/m/Y');
						}
					?>
				</td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="5"><b>Payment Terms</b></td>
			</tr>
			<tr>
				<td class="" colspan="5"> <?=nl2br($sales_order_data->supplier_payment_terms);?></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="5"><b>Purchaser Delivery Address</b></td>
                <td class="text-center text-bold" colspan="5"><b>Received Payment Details</b></td>
			</tr>
			<tr>
				<td class="" colspan="5">
					<?php if($sales_order_data->address_work == 0){ ?>
						<b style="font-size: 15px"><?=$sales_order_data->delivery_party_name?></b><br />
						<?=nl2br($sales_order_data->delivery_address)?><br />
						City : <?=$sales_order_data->delivery_city?> -  <?=$sales_order_data->delivery_oldw_pincode?> (<?=$sales_order_data->delivery_state?>) <?=$sales_order_data->delivery_country?>.<br />
						Email : <?=$sales_order_data->delivery_email_id;?><br />
						Contact No.:  <?=$sales_order_data->delivery_contact_no;?>
					<?php } else { ?>
						<b style="font-size: 15px"><?=$sales_order_data->party_name?></b><br />
						<?=nl2br($sales_order_data->address);?><br />
						City : <?=$sales_order_data->city?> -  <?=$sales_order_data->pincode?> (<?=$sales_order_data->state?>) <?=$sales_order_data->country?>.<br />
						Email : <?=explode(",", $sales_order_data->party_email_id)[0];?><br />
						Contact No.:  <?=$sales_order_data->p_phone_no;?><?= ($sales_order_data->p_phone_no != $sales_order_data->contact_person_phone) ? ', '.$sales_order_data->contact_person_phone : '' ?><br />
					<?php } ?>
                </td>
				<td class="" valign="top" colspan="5">
                    <?php if(isset($sales_order_data->received_payment) && !empty($sales_order_data->received_payment)){ ?>
                    <?=$CURRENCY?> <?=$sales_order_data->received_payment?>/- as an advance on Date <?=strtotime($sales_order_data->received_payment_date) != 0?date('d/m/Y',strtotime($sales_order_data->received_payment_date)):''?> by <?=$sales_order_data->received_payment_type;?> <?=$sales_order_data->received_payment_cheque_no?>
                    <?php } ?>
                </td>
			</tr>
			<tr>
				<td class="text-center text-bold" width="6%"><b>Sr.No.</b></td>
				<td class="text-center text-bold" width=""><b>Item Name</b></td>
				<td class="text-center text-bold" width="7%"><b>HSN</b></td>
				<td class="text-center text-bold" width="5%"><b>Qty</b></td>
				<td class="text-center text-bold" width="9%"><b>Rate</b></td>
				<td class="text-center text-bold" width="9%"><b>Discount</b></td>
				<td class="text-center text-bold" width="8%"><b>CGST</b></td>
				<td class="text-center text-bold" width="8%"><b>SGST</b></td>
				<td class="text-center text-bold" width="8%"><b>IGST</b></td>
				<td class="text-center text-bold" width="12%"><b>Amount</b></td>
			</tr>
            
            <?php
                $sub_total = 0;
				$discount_total = 0;
				$second_sub_total = 0;
				$gst_total = 0;
				$round_off = 0;
				$packing_forwarding_amount = 0;
				$transport_amount = 0;
				$insurance_amount = 0;
				$other_amount = 0;
				$other_gst = 0;
				$grand_total = 0;
				if (!empty($sales_order_items)) {
					$i_inc = 1;
					foreach ($sales_order_items as $key => $item_row) {
						$before_discount_price = $item_row['quantity'] * $item_row['rate'];
						//$discount_amt = ($before_discount_price * $item_row['disc_per'])/100;
                        $discount_amt = $item_row['disc_value'];
						$discounted_price = $before_discount_price - $discount_amt;
						$cgst_amount = ($discounted_price * $item_row['cgst'])/100;
						$sgst_amount = ($discounted_price * $item_row['sgst'])/100;
						$igst_amount = ($discounted_price * $item_row['igst'])/100;
						$net_amount = $discounted_price + $cgst_amount + $sgst_amount + $igst_amount;
						
			?>
						<tr>
							<td class="text-center" ><?=$i_inc?></td>
							<td class="text-left" ><?=$item_row['item_name'];?> <br /> <?=$item_row['item_extra_accessories'];?></td>
							<td class="text-center" ><?=$item_row['hsn'];?></td>
							<td class="text-center" ><?=$item_row['quantity'].' '.$item_row['uom'];?></td>
							<td class="text-right" ><?=moneyformat($item_row['rate'])?></td>
							<td class="text-center" ><?=$item_row['disc_per'];?> %</td>
							<td class="text-center" ><?=$item_row['cgst'];?> %</td>
							<td class="text-center" ><?=$item_row['sgst'];?> %</td>
							<td class="text-center" ><?=$item_row['igst'];?> %</td>
                            <!--<td class="text-right" ><?=moneyformat($item_row['net_amount'])?></td>-->
							<td class="text-right" ><?=moneyformat($net_amount)?></td>
						</tr>
			<?php
						$i_inc++;
						$discount_total += $item_row['disc_value'];
						$sub_total += ($item_row['quantity'] * $item_row['rate']);
						$gst_total += ($item_row['cgst_amount'] + $item_row['sgst_amount'] + $item_row['igst_amount']);
							
					}
				}
				$second_sub_total = $sub_total - $discount_total;
				
				if(!empty($sales_order_data->packing_forwarding_amount)){
					$packing_forwarding_amount = !empty($sales_order_data->packing_forwarding_amount) ? $sales_order_data->packing_forwarding_amount : '0.00';
                    $other_gst += get_gst($sales_order_data->packing_forwarding_amount,$sales_order_data->packing_forwarding_cgst);
					$other_gst += get_gst($sales_order_data->packing_forwarding_amount,$sales_order_data->packing_forwarding_sgst);
					$other_gst += get_gst($sales_order_data->packing_forwarding_amount,$sales_order_data->packing_forwarding_igst);
				}
				if(!empty($sales_order_data->transport_amount)){
					$second_sub_total += $sales_order_data->transport_amount;
					$other_gst += get_gst($sales_order_data->transport_amount,$sales_order_data->transportation_by_cgst);
					$other_gst += get_gst($sales_order_data->transport_amount,$sales_order_data->transportation_by_sgst);
					$other_gst += get_gst($sales_order_data->transport_amount,$sales_order_data->transportation_by_igst);
				}
				if(!empty($sales_order_data->insurance_amount)){
					$second_sub_total += $sales_order_data->insurance_amount;
					$other_gst += get_gst($sales_order_data->insurance_amount,$sales_order_data->road_insurance_by_cgst);
					$other_gst += get_gst($sales_order_data->insurance_amount,$sales_order_data->road_insurance_by_sgst);
					$other_gst += get_gst($sales_order_data->insurance_amount,$sales_order_data->road_insurance_by_igst);
				}
				if(!empty($sales_order_data->oil_barrel_amount)){
					$second_sub_total += $sales_order_data->oil_barrel_amount;
					$other_gst += get_gst($sales_order_data->oil_barrel_amount,$sales_order_data->third_party_inspection_cost_cgst);
					$other_gst += get_gst($sales_order_data->oil_barrel_amount,$sales_order_data->third_party_inspection_cost_sgst);
					$other_gst += get_gst($sales_order_data->oil_barrel_amount,$sales_order_data->third_party_inspection_cost_igst);
				}
				if(!empty($sales_order_data->third_party_inspection_cost_amount)){
					$second_sub_total += $sales_order_data->third_party_inspection_cost_amount;
					$other_gst += get_gst($sales_order_data->third_party_inspection_cost_amount,$sales_order_data->oil_barrel_cgst);
					$other_gst += get_gst($sales_order_data->third_party_inspection_cost_amount,$sales_order_data->oil_barrel_sgst);
					$other_gst += get_gst($sales_order_data->third_party_inspection_cost_amount,$sales_order_data->oil_barrel_igst);					
				}
				if(!empty($sales_order_data->machine_to_all_motor_cable_amount)){
					$second_sub_total += $sales_order_data->machine_to_all_motor_cable_amount;
					$other_gst += get_gst($sales_order_data->machine_to_all_motor_cable_amount,$sales_order_data->machine_to_all_motor_cable_cgst);
					$other_gst += get_gst($sales_order_data->machine_to_all_motor_cable_amount,$sales_order_data->machine_to_all_motor_cable_sgst);
					$other_gst += get_gst($sales_order_data->machine_to_all_motor_cable_amount,$sales_order_data->machine_to_all_motor_cable_igst);					
				}
				$grand_total = $second_sub_total + $gst_total + $round_off + $packing_forwarding_amount + $other_gst;
			?>
            <tr>
				<td valign='top' class="text-left text-bold" rowspan="5" colspan="5">
					<b>Note :</b> <?=nl2br($sales_order_data->notes);?>
				</td>
				<td class="text-right text-bold" colspan="3">Sub Total</td>
				<td class="text-right text-bold" colspan="2"> <?=moneyformat($sub_total)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="3">Discount</td>
				<td class="text-right text-bold" colspan="2"><?=moneyformat($discount_total)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="3">Sub Total</td>
				<td class="text-right text-bold" colspan="2"><?=moneyformat($second_sub_total)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="3">GST</td>
				<td class="text-right text-bold" colspan="2"><?=moneyformat($gst_total + $other_gst)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="3">Packing & Forwarding</td>
				<td class="text-right text-bold" colspan="2"><?=moneyformat($packing_forwarding_amount)?></td>				
			</tr>
			<tr>
				<td class=" text-bold" colspan="5">In Words <?=$CURRENCY?> : <?=money_to_word($grand_total)?> only.</td>
				<td class="text-right text-bold" colspan="3">Grand Total</td>
				<td class="text-right text-bold" colspan="2"><?=$CURRENCY?> <?=moneyformat($grand_total)?></td>
			</tr>
			<?php if($is_first_item == true):?>
				<?php if(!empty($other_items)):?>
					<tr>
						<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">No</td>
						<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">Item</td>
						<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">Qty</td>
						<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">I/E</td>
						<td class="text-center text-bold" colspan="<?=$column_cnt - 6 ;?>">Price  <?=$CURRENCY?>.</td>
						<td class="text-center text-bold" colspan="<?=$column_cnt - 6 ;?>">T/Price  <?=$CURRENCY?></td>
					</tr>
					<?php foreach($other_items as $other_item_row):?>
						<tr>
							<td class="text-center" colspan="<?=$column_cnt - 7 ;?>">001</td>
							<td class="text-center" colspan="<?=$column_cnt - 7 ;?>"><?=$other_item_row['item_name'];?></td>
							<td class="text-center" colspan="<?=$column_cnt - 7 ;?>"><?=$other_item_row['quantity'];?></td>
							<td class="text-center" colspan="<?=$column_cnt - 7 ;?>">Buyer</td>
							<td class="text-center" colspan="<?=$column_cnt - 6 ;?>">&nbsp;</td>
							<td class="text-right" colspan="<?=$column_cnt - 6 ;?>"> <?=$CURRENCY?> <?=$other_item_row['amount'];?></td>
						</tr>
					<?php endforeach;?>
				<?php endif;?>
			<?php endif;?>
			<tr>
				<td colspan="<?=$column_cnt;?>">
					<table class="no-border">
						<tr class="no-border">
							<td align="center" class="no-border-top footer-detail-area" style="border-left: 0!important;">
								<strong><?=$sales_order_data->party_name?></strong><br />
							</td>
							<td align="center" class="no-border-top footer-detail-area">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
							<td align="center" class="no-border-top footer-detail-area">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
						</tr>
						<tr class="no-border">
							<td class="no-border footer-sign-area text-bold no-border-left no-border-top no-border-bottom">
                                <?=$sales_order_data->contact_person_name?><br />
								Purchaser Sign
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                <img height="80px" width="80px" src="<?=image_url('staff/signature/'.$company_ceo_details['signature']);?>"><br />
								<?=$company_ceo_details['name'];?><br/>
								Authorized Signatory
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                <img src="<?php echo BASE_URL;?>/resource/image/jk_symbol.jpg" style="margin-bottom:5px"><br />
								<?=$company_details['m_d_name'];?><br/>
								Managing Director
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<?php } else if($sales_order_data->party_type_1 == PARTY_TYPE_EXPORT_ID){ ?>
        <?php
            if(isset($letterpad_details->header_logo)){
				echo '<div style="text-align: '.$letterpad_details->header_logo_alignment.';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>';
			}
        ?>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="<?=$column_cnt;?>">
					<?=(isset($sales_order_data->export_order_type) ? ($sales_order_data->export_order_type ==  'Sales Order' ? 'Overseas Order' : ($sales_order_data->export_order_type ==  'Sales Contract' ? 'Sales Contract' : '')) : 'Overseas Order');?>
				</td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="5"><b>Manufacturer & Supplier</b></td>
				<td class="text-bold" colspan="2">Sales Order No. :</td>
                                <td class="text-bold" colspan="2"><b><?=$this->applib->get_sales_order_no($sales_order_data->sales_order_no);?></b></td>

                                <td class=" text-bold" colspan="1"><b><?=strtotime($sales_order_data->sales_order_date) != 0?date('d/m/Y',strtotime($sales_order_data->sales_order_date)):date('d/m/Y')?></b></td>
			</tr>
			<tr>
				<td valign="top" class="text-bold" colspan="5" style="font-size:15px;"><b>Jay Khodiyar Machine Tools</b></td>
				<td class="text-bold" colspan="2">Quotation No. : </td>
                                <td class="text-bold" colspan="2"><b><?=$this->applib->get_quotation_ref_no($sales_order_data->quotation_no,$sales_order_item['item_code']);?></b></td>
                                <td class="text-bold" colspan="1"><b><?=strtotime($quotation_data->quotation_date) != 0?date('d/m/Y',strtotime($quotation_data->quotation_date)):date('d/m/Y')?></b></td>
			</tr>
			<tr>
				<td valign="top" class="no-border-top text-bold" rowspan="5" colspan="5">
					<?=nl2br($company_details['address']);?><br />
					City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
					Email: <?=$company_details['email_id'];?><br />
					Tel No. : <?=$company_details['contact_no'];?>,<br />
					Contact No. : <?=$company_details['cell_no'];?><br />
				</td>
				<td class="text-bold" colspan="2">Purchase Order No.:</td>
				<td class="text-bold" colspan="2">
                    <b>
                    <?php
                        if(!empty($sales_order_data->cust_po_no)){
                            echo $sales_order_data->cust_po_no;
                        } else {
                            echo $sales_order_data->sales_order_pref;
                        }
                    ?>
                    </b>
                </td>
				<td class="text-bold" colspan="1"><b><?=strtotime($sales_order_data->po_date) != 0?date('d/m/Y',strtotime($sales_order_data->po_date)):date('d/m/Y')?></b></td>
			</tr>
            <tr>
				<td class=" text-bold" colspan="2">Sales Executive :</td>
                <td class=" text-bold" colspan="3"><b><?=$sales_order_data->created_name?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">IEC No. : </td>
                <td class=" text-bold" colspan="3"><b><?=$company_details['iec_no'];?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">GST No. : </td>
                <td class=" text-bold" colspan="3"><b><?=$company_details['gst_no'];?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">PAN No. : </td>
                <td class=" text-bold" colspan="3"><b><?=$company_details['pan_no'];?></b></td>
			</tr>
			
			<tr>
				<td class="text-center text-bold width-50-pr" colspan="5"><b>Purchaser</b></td>
				<td class="text-center text-bold width-50-pr" colspan="5"><b>Terms of Payment & Delivery</b></td>
			</tr>
			<tr>
				<td valign="top" class="text-bold" colspan="5" style="font-size:15px;"><b><?=$sales_order_data->party_name?></b></td>
				<td class="text-bold no-border-bottom" colspan="5"></td>
			</tr>
			<tr>
				<td valign="top" class="no-border-top text-bold" rowspan="3" colspan="5">
					<?=nl2br($sales_order_data->address);?><br />
					City : <?=$sales_order_data->city?> -  <?=$sales_order_data->pincode?> (<?=$sales_order_data->state?>) <?=$sales_order_data->country?>.<br />
					Email : <?=explode(",", $sales_order_data->party_email_id)[0];?><br />
					Tel No.:  <?=$sales_order_data->fax_no;?>,<br />
					Contact No.:  <?=$sales_order_data->p_phone_no;?><?= ($sales_order_data->p_phone_no != $sales_order_data->contact_person_phone) ? ', '.$sales_order_data->contact_person_phone : '' ?><br />
					Contact Person: <?=$sales_order_data->contact_person_name?><br />
				</td>
                                <td class="text-bold no-border-top" colspan="5" style="padding-top: -16px;" ><?=$sales_order_data->supplier_payment_terms;?></td>
				
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="5"><b>Our Bank Accounts Details </b></td>
			</tr>
			
			<tr>
				<td class=" text-bold" colspan="5" rowspan=""><?=$sales_order_data->banks_detail?></td>
			</tr>
			
			
			<tr>
				<td class="text-center text-bold" colspan="10"><b>Dispatch Detials</b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="2">Loading At :</td>
                                <td class="text-bold" colspan="3"><b><?=$sales_order_data->loading_at;?></b></td>
				<td class="text-bold" colspan="2">Port of Discharge:</td>
                                <td class="text-bold" colspan="2"><b><?=$sales_order_data->port_of_discharge_city;?></b></td>
                                <td class="text-bold" colspan="1"><b><?=$sales_order_data->port_of_discharge_country;?></b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="2">Port of Loading :</td>
                                <td class="text-bold" colspan="2"><b><?=$sales_order_data->port_of_loading_city;?></b></td>
				<td class="text-bold" colspan="1"><b><?=$sales_order_data->port_of_loading_country;?></b></td>
                                <td class="text-bold" colspan="2">Place of Delivery :</td>
                                <td class="text-bold" colspan="2"><b><?=$sales_order_data->place_of_delivery_city;?></b></td>
                                <td class="text-bold" colspan="1"><b><?=$sales_order_data->place_of_delivery_country;?></b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="2">Delivery Through : </td>
                                <td class="text-bold" colspan="3"><b><?=$sales_order_data->delivery_through;?></b></td>
				<td class="text-bold" colspan="2">Shipping Line Name : </td>
                                <td class="text-bold" colspan="3"><b><?=$sales_order_data->shipping_line_name;?></b></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="1"><b>Sr.No.</b></td>
				<td class="text-center text-bold"colspan="3"><b>Item Name</b></td>
				<td class="text-center text-bold" colspan="1"><b>HSN</b></td>
				<td class="text-center text-bold" colspan="1"><b>Qty</b></td>
				<td class="text-center text-bold" colspan="2"><b>Rate Per Unit <?=$CURRENCY?></b></td>
				<td class="text-center text-bold" colspan="2"><b>Amount <?=$CURRENCY?></b></td>
			</tr>

			<?php
				  $sub_total = 0;
				  $discount_total = 0;
				  $second_sub_total = 0;
				  $round_off = 0;
				  $packing_forwarding_amount = 0;
				  $transport_amount = 0;
				  $insurance_amount = 0;
				  $other_amount = 0;
				  $other_gst = 0;
				  $grand_total = 0;
				  if (!empty($sales_order_items)) {
					  $i_inc = 1;
					  foreach ($sales_order_items as $key => $item_row) {
						  $before_discount_price = $item_row['quantity'] * $item_row['rate'];
						  //$discount_amt = ($before_discount_price * $item_row['disc_per'])/100;
                          $discount_amt = $item_row['disc_value'];
						  $discounted_price = $before_discount_price - $discount_amt;
						  $net_amount = $discounted_price ;

			?>
			<tr>
				<td class="text-center text-bold" colspan="1" ><?=$i_inc?></td>
				<td class="text-left text-bold" colspan="3"><b><?=$item_row['item_name'];?> <br /> <?=$item_row['item_extra_accessories'];?></b></td>
				<td class="text-center text-bold" colspan="1" ><?=$item_row['hsn'];?></td>
				<td class="text-center text-bold" colspan="1" ><?=$item_row['quantity'].' '.$item_row['uom'];?></td>
				<td class="text-right text-bold" colspan="2"><?=number_format((float)$item_row['rate'], 2, '.', ''); ?></td>
				<!--<td class="text-right" ><?=moneyformat($item_row['net_amount'])?></td>-->
				<td class="text-right text-bold" colspan="2"><?=number_format((float)$before_discount_price, 2, '.', ''); ?></td>
			</tr>
			<?php
				$i_inc++;
						  $sub_total += ($item_row['quantity'] * $item_row['rate']);
					  }
				  }
				  $second_sub_total = $sub_total - $discount_total;
				  $packing_forwarding_amount = !empty($sales_order_data->packing_forwarding_amount) ? $sales_order_data->packing_forwarding_amount : '0.00';
				  $sea_freight = $sales_order_data->sea_freight;
				  
				  /*if(!empty($sales_order_data->transport_amount)){
					  $second_sub_total += $sales_order_data->transport_amount;
					  $other_gst += get_gst($sales_order_data->transport_amount,$sales_order_data->transportation_by_cgst);
					  $other_gst += get_gst($sales_order_data->transport_amount,$sales_order_data->transportation_by_sgst);
					  $other_gst += get_gst($sales_order_data->transport_amount,$sales_order_data->transportation_by_igst);
				  }*/
				  $grand_total = $second_sub_total + $round_off + $other_gst + $packing_forwarding_amount + $sea_freight;
			?>
			<tr>
				<td valign='top' class="text-left text-bold" rowspan="3" colspan="6">
					<b>Note :</b> <?=nl2br($sales_order_data->notes);?>
				</td>
				<td class="text-right text-bold" colspan="2">Ex-Factory</td>
                                <td class="text-right text-bold" colspan="2"><b><?=number_format((float)$sub_total, 2, '.', ''); ?></b></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="2">Packing & Forwarding</td>
                                <td class="text-right text-bold" colspan="2"><b><?=number_format((float)$packing_forwarding_amount, 2, '.', ''); ?></b></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="2">Sea Freight (<?=$sales_order_data->sea_freight_type?>) <?=$sales_order_data->port_of_discharge_city;?></td>
                                <td class="text-right text-bold" colspan="2"><b><?=number_format((float)$sea_freight, 2, '.', ''); ?></b></td>
			</tr>
			<tr>
                            <td class=" text-bold" colspan="6"><b>In Words <?=$CURRENCY?> :</b> <?=money_to_word($grand_total)?> only.</td>
                                <td class="text-right text-bold" colspan="2"><b>Total <?=$CURRENCY?></b></td>
                                <td class="text-right text-bold" colspan="2"><b><?=number_format((float)$grand_total, 2, '.', ''); ?></b></td>
			</tr>
			<?php if($is_first_item == true):?>
			<?php if(!empty($other_items)):?>
			<tr>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">No</td>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">Item</td>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">Qty</td>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">I/E</td>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 6 ;?>">Price  <?=$CURRENCY?>.</td>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 6 ;?>">T/Price  <?=$CURRENCY?></td>
			</tr>
			<?php foreach($other_items as $other_item_row):?>
			<tr>
				<td class="text-center" colspan="<?=$column_cnt - 7 ;?>">001</td>
				<td class="text-center" colspan="<?=$column_cnt - 7 ;?>"><?=$other_item_row['item_name'];?></td>
				<td class="text-center" colspan="<?=$column_cnt - 7 ;?>"><?=$other_item_row['quantity'];?></td>
				<td class="text-center" colspan="<?=$column_cnt - 7 ;?>">Buyer</td>
				<td class="text-center" colspan="<?=$column_cnt - 6 ;?>">&nbsp;</td>
				<td class="text-right" colspan="<?=$column_cnt - 6 ;?>"> <?=$CURRENCY?> <?=$other_item_row['amount'];?></td>
			</tr>
			<?php endforeach;?>
			<?php endif;?>
			<?php endif;?>
			<tr>
				<td colspan="<?=$column_cnt;?>">
					<table class="no-border">
						<tr class="no-border">
							<td align="center" class="no-border-top footer-detail-area" style="border-left: 0!important;">
								<strong><?=$sales_order_data->party_name?></strong><br />
							</td>
							<td align="center" class="no-border-top footer-detail-area">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
							<td align="center" class="no-border-top footer-detail-area">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
						</tr>
						<tr class="no-border">
							<td class="no-border footer-sign-area text-bold no-border-left no-border-top no-border-bottom">
								<?=$sales_order_data->contact_person_name?><br />
								Purchaser Sign
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                <img height="80px" width="80px" src="<?=image_url('staff/signature/'.$company_ceo_details['signature']);?>"><br />
								<?=$company_ceo_details['name'];?><br/>
								Authorized Signatory
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                <img src="<?php echo BASE_URL;?>/resource/image/jk_symbol.jpg" style="margin-bottom:5px"><br />
								<?=$company_details['m_d_name'];?><br/>
								Managing Director
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		
	<?php } ?>
	</body>
</html>
<i class="fa fa-angle-down"></i>
<?php
function get_gst($price, $gst_per){
	return ($price * $gst_per)/100;
}
?>
