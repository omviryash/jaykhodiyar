<?php
    if (!$this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "view")) {
        $this->session->set_flashdata('error_message', 'You have not permission to access this page.');
        redirect("/");
    }
?>
<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Sales Order List</small>
            <?php
                $sales_order_view_role = $this->applib->have_access_role(SALES_ORDER_MODULE_ID,"view");
                $sales_order_add_role = $this->applib->have_access_role(SALES_ORDER_MODULE_ID,"add");
            ?>
            <?php if($sales_order_add_role): 
			if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export")) { ?>
			<a href="<?=base_url('sales_order/add_export')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Export Order</a>
			<?php } if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic")) { ?>
			<a href="<?=base_url('sales_order/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Order</a>
			<?php } 
			endif;?>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <label class="col-md-1">Status:</label>
                    <div class="col-md-2">
                        <select name="order_status" id="order_status" class="form-control input-sm">
                            <option value="all" <?php if(isset($status) && $status == 'all') { echo 'selected="selected"'; } elseif(isset($_GET['status']) && $_GET['status'] == 'all'){ echo 'selected="selected"'; } ?>>All</option>
                            <option value="<?php echo PENDING_DISPATCH_ORDER_ID; ?>" <?php if(isset($status)&& $status != 'all' && $status == PENDING_DISPATCH_ORDER_ID) { echo 'selected="selected"'; } elseif(isset($_GET['status']) && $_GET['status'] == PENDING_DISPATCH_ORDER_ID) { echo 'selected="selected"'; } ?>>Pending Dispatch</option>
                            <option value="<?php echo DISPATCHED_ORDER_ID; ?>" <?php if(isset($status) && $status == DISPATCHED_ORDER_ID) { echo 'selected="selected"'; } elseif(isset($_GET['status']) && $_GET['status'] == DISPATCHED_ORDER_ID ){ echo 'selected="selected"';} ?>>Dispatched</option>
                            <option value="<?php echo CANCELED_ORDER_ID; ?>" <?php if(isset($status) && $status == CANCELED_ORDER_ID) { echo 'selected="selected"'; } elseif(isset($_GET['status']) && $_GET['status'] == CANCELED_ORDER_ID) { echo 'selected="selected"'; } ?>>Canceled Order</option>
                            <option value="<?php echo CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID; ?>" <?php if(isset($status) && $status == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) { echo 'selected="selected"'; } elseif(isset($_GET['status']) && $_GET['status'] == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) { echo 'selected="selected"'; } ?>>Challan Created from Proforma Invoice</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="pull-left">Is Approved: &nbsp;&nbsp;</label>
                        <select name="is_approved" id="is_approved" class="form-control input-sm select2" style="width: 60%;">
                            <option value="all">All</option>
                            <option value="1" <?=isset($_GET['is_approved']) && $_GET['is_approved'] == 1?'selected="selected"':''?>>Approved</option>
                            <option value="0" <?=isset($_GET['is_approved']) && $_GET['is_approved'] == 0?'selected="selected"':''?>>Not Approved</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="pull-left">Party Type: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <select name="party_type" id="party_type" class="form-control input-sm select2" style="width: 60%;">
                            <?php
                            $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
                            $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
                            $isManagement = $this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow");
                            if($isManagement == 1){
                                if($cu_accessExport == 1 && $cu_accessDomestic == 1){
                                ?>
                                <option value="all">All</option>
                                <option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
                                <option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
                                <?php
                                }elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
                                ?>
                                    <option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
                                <?php
                                }else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
                                ?>
                                    <option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
                                <?php
                                }
                            }else{
                                if($cu_accessExport == 1 && $cu_accessDomestic == 1){
                                ?>
                                <option value="all">All</option>
                                <option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
                                <option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
                                <?php
                                } else if($cu_accessExport == 1){
                                ?>
                                    <option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
                                <?php
                                } else if($cu_accessDomestic == 1){
                                ?>
                                    <option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
                                <?php
                                } else {}

                            }
                            ?>								
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="invoice_not_created">Only Invoice Not Created</label> &nbsp; 
                        <input type="checkbox" name="invoice_not_created" id="invoice_not_created" />
                    </div>
                    <?php 
                        if($this->input->get_post("status")){ 
                                $current_status = $this->input->get_post("status"); 
                        } else {
                                $current_status = 'pending';
                        }
                        if($this->input->get_post("user_id")){ 
                                $current_staff = $this->input->get_post("user_id"); 
                        } else {
                                $current_staff = $this->session->userdata('is_logged_in')['staff_id'];
                        }
                    ?>
                    <div class="clearfix"></div><br />
                    <div class="col-md-3">
                        <label class="pull-left">From Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" name="from_date" id="datepicker1" class="form-control input-sm" style="width: 60%;" value="<?php echo isset($from_date) && !empty($from_date) ? date('d-m-Y', strtotime($from_date)) : ''; ?>">
                    </div>
                    <div class="col-md-3">
                        <label class="pull-left">To Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" name="to_date" id="datepicker2" class="form-control input-sm" style="width: 60%;" value="<?php echo isset($to_date) && !empty($to_date) ? date('d-m-Y', strtotime($to_date)) : ''; ?>">
                    </div>
                    <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
                        <div class="col-md-4">
                            <label class="pull-left">Sales Person: &nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <select class="form-control select_user_id input-sm" name="user_id" id="user_id" style="width: 60%;">
                                <option value="all" <?php if(isset($staff_id) && $staff_id == '0'){ echo ' Selected '; } elseif ($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
                                    <?php
                                        if (!empty($users)) {
                                            foreach ($users as $client) {
                                                if (trim($client->name) != "") {
                                                    $selected = '';
                                                    if(isset($staff_id)){
                                                        if(!empty($staff_id) && $staff_id == $client->staff_id){
                                                            $selected = $staff_id == $client->staff_id ? 'selected' : '';
                                                        } 
                                                    } else {
                                                        $selected = $current_staff == $client->staff_id ? 'selected' : '';
                                                    }
                                                        echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
                                                }
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div><br />
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="order_list_table" class="table custom-table table-striped">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Quotation No.</th>
                                <th>Sales Order No.</th>
                                <th>Status</th>
                                <th>Is Approved</th>
                                <th>Item Code</th>
                                <th>Customer Name </th>
                                <th>Sales Person</th>
                                <th>Sales Order Date</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                                <th>Party Current Person</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    
</div>
<div class="modal fade" id="open_show_itemprint_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    Item Print
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="example1" class="table custom-table table-striped dataTable">
                        <thead>
                            <tr>
                                <th width="50%">
                                    Item
                                </th>
                                <th width="50%">
                                    Print
                                </th>
                                <!-- <th width="20%" class="text-center">
                                    Action
                                </th> -->
                            </tr>                        
                        </thead>
                        <tbody id="items_td">
                        </tbody>
                    </table>
                </div>
            </div>
       </div>
     </div>
</div>
<script>
	var table;
    $(document).ready(function() {
        $(".select2").select2();
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [1,2,3,4,5,6,7,8,9,10,11,12],
			}
		};
        
		table = $('#order_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[2, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('sales_order/order_datatable')?>",
                "type": "POST",
                "data":function(d){
                    d.order_status = $("#order_status").val();
                    d.is_approved = $("#is_approved").val();
                    d.invoice_not_created = $('input[name="invoice_not_created"]').prop('checked');
                    d.party_type = $("#party_type").val();
                    d.from_date = $('#datepicker1').val();
                    d.to_date = $('#datepicker2').val();
                    d.user_id = $('#user_id').val();
                }
            },
            <?php if($this->applib->have_access_current_user_rights(SALES_ORDER_MODULE_ID,"export_data")){ ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Sales Orders', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Sales Orders', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Sales Orders', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Sales Orders', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Sales Orders', orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
            "scrollY": 725,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
		});

        $("#order_status").select2();

        $(document).on('change',"#order_status,#is_approved",function() {
            table.draw();
        });
        
        $(document).on('change',"#invoice_not_created",function() {
            table.draw();
        });
		$(document).on('change',"#party_type",function() {
            table.draw();
        });
        
        $(document).on('change',"#datepicker1",function() {
            table.draw();
        });
        
        $(document).on('change',"#datepicker2",function() {
            table.draw();
        });
        
        $(document).on('change',"#user_id",function() {
            table.draw();
        });
		
        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=sales_order',
                    success: function(data) {
                    }
                });
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=sales_order_id&table_name=sales_order_item',
                    success: function(data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

        $(document).on('click', '.show_itemprint_modal', function(e){
            e.preventDefault();
            var saleorder_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();

            $( "#items_td" ).html('');
            $.ajax({
                url: '<?php echo base_url('sales/feed_saleorders_items/') ?>/'+saleorder_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    if (data.item_data) {
                        jQuery.each( data.item_data, function( i, val ) {
                            $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('sales/sales_order_print/'); ?>/'+saleorder_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        });
                    }
                }
            });
        });

        $(document).on('click', '.show_work_order_itemprint_modal', function(e){
            e.preventDefault();
            var saleorder_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();

            $( "#items_td" ).html('');
            $.ajax({
                url: '<?php echo base_url('sales/feed_saleorders_items/') ?>/'+saleorder_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    if (data.item_data) {
                        jQuery.each( data.item_data, function( i, val ) {
                            $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('sales/work_order/'); ?>/'+saleorder_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        });
                    }
                }
            });
        });

    });
</script>
