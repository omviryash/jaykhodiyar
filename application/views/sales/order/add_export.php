<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('sales_order/save_sales_order_export') ?>" method="post" id="save_sales_order_export" novalidate>
        <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
        <input type="hidden" name="sales_order_data[sales_order_id]" id="sales_order_id" value="<?=$sales_order_data->id?>">
        <?php } ?>
        <input type="hidden" name="sales_order_data[quotation_id]" id="quotation_id" value="<?=(isset($sales_order_data->quotation_id))? $sales_order_data->quotation_id : ''; ?>" />
        <input type="hidden" name="sales_order_data[currency_id]" id="so_currency_id" value="<?=(isset($sales_order_data->currency_id))? $sales_order_data->currency_id : ''; ?>" />
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> 
                <small class="text-primary text-bold">Sales Order <span id="partyname"></span></small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, F5 = Tab 5, Ctrl+S = Save</label></small>
                <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
                <?php if(isset($sales_order_data->isApproved) && $sales_order_data->isApproved == 1){ ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){ ?>
                <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;" <?=($sales_order_data->delete_not_allow == 1) ? ' Disabled ' : ''; ?>>Disapprove</button>
                <?php } else { ?>
                <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                <?php } ?>
                <?php } else { ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){ ?>
                <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                <?php } else { ?>
                <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
                $sales_order_view_role= $this->applib->have_access_role(SALES_ORDER_MODULE_ID,"view");
                $sales_order_add_role= $this->applib->have_access_role(SALES_ORDER_MODULE_ID,"add");
                $sales_order_edit_role= $this->applib->have_access_role(SALES_ORDER_MODULE_ID,"edit");
                ?>
                <?php if($sales_order_add_role || $sales_order_edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($sales_order_view_role): ?>
                <a href="<?= base_url()?>sales_order/order_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Order List</a>
                <?php endif;?>
                <?php if($sales_order_add_role): ?>
                <a href="<?= base_url()?>sales_order/add_export/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add Export Order</a>
                <?php endif;?>
                <?php if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view") && isset($sales_order_data->isApproved) && $sales_order_data->isApproved == 1) { ?>
                    <span class="pull-right send_sms_div" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="sales_order_data[send_sms]" id="send_sms" class="send_sms" >  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } else if ($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view")) { ?>
                    <span class="pull-right send_sms_div hidden" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="sales_order_data[send_sms]" id="send_sms" class="send_sms" >  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" id="tabs_1" data-toggle="tab">Sales Order Details</a></li>
                        <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                        <li><a href="#tab_3" data-toggle="tab" id="tabs_3" class="sales_order_reminder_tab">Sales Order Reminder</a></li>
                        <li><a href="#tab_4" data-toggle="tab" id="tabs_4">S.O. General</a></li>
                        <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
                        <li><a href="#tab_5" data-toggle="tab" id="tabs_5">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Sales Order Details</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sales_order_no" <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ } else { echo 'style="display:none;"'; } ?> class="col-sm-3 input-sm">Sales Order No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="sales_order_data[sales_order_no]" id="sales_order_no" class="form-control input-sm" <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ } else { echo 'style="display:none;"'; } ?> readonly="" value="<?=(isset($sales_order_data->sales_order_no))? $sales_order_data->sales_order_no : $sales_order_no; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="branch_id" class="col-sm-3 input-sm">Branch</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="sales_order_data[branch_id]" id="branch_id" class="branch_id form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="agent_id" class="col-sm-3 input-sm">Agent</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="sales_order_data[agent_id]" id="agent_id" class="agent_id form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_order_date" class="col-sm-3 input-sm">S.O. Date</label>
                                                    <div class="col-sm-9">
                                                        <?php if($this->app_model->have_access_role(SALES_ORDER_MODULE_ID, "change_date_allow")){ ?>
                                                            <input type="text" name="sales_order_data[sales_order_date]" id="sales_order_date" class="form-control input-sm pull-right input-datepicker" value="<?=(isset($sales_order_data->sales_order_date))? date('d-m-Y', strtotime($sales_order_data->sales_order_date)) : date('d-m-Y'); ?>">
                                                        <?php } else { ?>
                                                            <input type="text" name="sales_order_data[sales_order_date]" id="sales_order_date" class="form-control input-sm pull-right" readonly="readonly" value="<?=(isset($sales_order_data->sales_order_date))? date('d-m-Y', strtotime($sales_order_data->sales_order_date)) : date('d-m-Y'); ?>">
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="committed_date" class="col-sm-3 input-sm">Committed Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="sales_order_data[committed_date]" id="committed_date" class="form-control input-sm pull-right input-datepicker" value="<?=(isset($sales_order_data->committed_date))? date('d-m-Y', strtotime($sales_order_data->committed_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_id" class="col-sm-3 input-sm">Sales</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="sales_order_data[sales_id]" id="sales_id" class="form-control input-sm select2 sales_id" disabled="disabled"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="is_dispatched" class="col-sm-3 input-sm">Order Status</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <?php 
                                                            if(isset($sales_order_data->is_dispatched) && $sales_order_data->is_dispatched == DISPATCHED_ORDER_ID){
                                                                echo '<label>Dispatched</label>';
                                                            } else if(isset($sales_order_data->not_any_dispatched) && $sales_order_data->not_any_dispatched == 1){
                                                                echo '<label>Pending Dispatch</label>';
                                                            } else if(isset($sales_order_data->is_dispatched) && $sales_order_data->is_dispatched != DISPATCHED_ORDER_ID){
                                                                if($this->app_model->have_access_role(ALLOW_TO_CHANGE_STATUS_CANCEL_TO_PENDING, "allow") && isset($sales_order_data->is_dispatched)) { 
                                                        ?>
                                                                <select name="sales_order_data[is_dispatched]" class="is_dispatched form-control input-sm select2" id="is_dispatched" >
                                                                    <option value="<?php echo PENDING_DISPATCH_ORDER_ID; ?>" <?php echo ($sales_order_data->is_dispatched == PENDING_DISPATCH_ORDER_ID) ? ' Selected ' : ''; ?> > Pending Dispatch </option>
                                                                    <option value="<?php echo CANCELED_ORDER_ID; ?>" <?php echo ($sales_order_data->is_dispatched == CANCELED_ORDER_ID) ? ' Selected ' : ''; ?> > Canceled Order </option>
                                                                    <option value="<?php echo CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID; ?>" <?php echo ($sales_order_data->is_dispatched == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) ? ' Selected ' : ''; ?> > Challan Created from Proforma Invoice </option>
                                                                </select>
                                                        <?php 
                                                                } elseif($sales_order_data->is_dispatched == CANCELED_ORDER_ID) {
                                                                    echo '<label>Canceled Order</label>';
                                                                } else { ?>
                                                                    <select name="sales_order_data[is_dispatched]" class="is_dispatched form-control input-sm select2" id="is_dispatched" >
                                                                        <option value="<?php echo PENDING_DISPATCH_ORDER_ID; ?>" <?php echo ($sales_order_data->is_dispatched == PENDING_DISPATCH_ORDER_ID) ? ' Selected ' : ''; ?> > Pending Dispatch </option>
                                                                        <option value="<?php echo CANCELED_ORDER_ID; ?>" <?php echo ($sales_order_data->is_dispatched == CANCELED_ORDER_ID) ? ' Selected ' : ''; ?> > Canceled Order </option>
                                                                        <option value="<?php echo CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID; ?>" <?php echo ($sales_order_data->is_dispatched == CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID) ? ' Selected ' : ''; ?> > Challan Created from Proforma Invoice </option>
                                                                    </select>
                                                                <?php }
                                                        
                                                            } else { 
                                                                echo '<label>Pending Dispatch</label>';
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="<?php echo (isset($sales_order_data->is_dispatched) && $sales_order_data->is_dispatched == CANCELED_ORDER_ID) ? '' : ' hidden '; ?> cancel_reason_div">
                                                    <div class="form-group">
                                                        <label for="sales_order_cancel_reason" class="col-sm-3 input-sm">Cancel Reason</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <textarea rows="2" class="form-control" name="sales_order_data[cancel_reason]" id="cancel_reason_text"><?=(isset($sales_order_data->cancel_reason))? $sales_order_data->cancel_reason : ''; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="proof_of_cancellation" class="col-sm-3 input-sm">Proof of Cancellation</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <input type="text" name="sales_order_data[proof_of_cancellation]" id="proof_of_cancellation" class="form-control input-sm" value="<?=(isset($sales_order_data->proof_of_cancellation))? $sales_order_data->proof_of_cancellation : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cust_po_no" class="col-sm-3 input-sm">Purchase Order No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="sales_order_data[cust_po_no]" id="cust_po_no" class="form-control input-sm" value="<?=(isset($sales_order_data->cust_po_no))? $sales_order_data->cust_po_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="po_date" class="col-sm-3 input-sm">Purchase Order Date</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <input type="text" name="sales_order_data[po_date]" id="po_date" class="form-control input-sm input-datepicker" value="<?=(isset($sales_order_data->po_date))? date('d-m-Y', strtotime($sales_order_data->po_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_order_pref_id" class="col-sm-3 input-sm">Purchase Order Ref</label>
                                                    <div class="col-sm-9">
                                                        <select name="sales_order_data[sales_order_pref_id]" id="sales_order_pref_id" class="form-control input-sm select2" >
                                                            <option value="">--Select--</option>
                                                            <?php foreach($sales_order_pref as $sales_order_pref_row): ?>
                                                            <option value="<?=$sales_order_pref_row->id?>" <?=(isset($sales_order_data->sales_order_pref_id) && $sales_order_data->sales_order_pref_id == $sales_order_pref_row->id)? ' Selected ' : ''; ?> >
                                                                <?=$sales_order_pref_row->sales_order_pref;?>
                                                            </option>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="notes" class="col-sm-3 input-sm">Sales Order Notes</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="sales_order_data[notes]" id="notes" rows="1" ><?=(isset($sales_order_data->notes))? $sales_order_data->notes : ''; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group" style="padding-top: 3px;">
                                                    <label for="work_order_notes" class="col-sm-3 input-sm">Work Order Notes</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="sales_order_data[work_order_notes]" id="work_order_notes" rows="1" ><?=(isset($sales_order_data->work_order_notes))? $sales_order_data->work_order_notes : ''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 <?php echo (isset($sales_order_data->is_dispatched) && $sales_order_data->is_dispatched == CANCELED_ORDER_ID) ? '' : ' hidden '; ?> cancel_reason_div">
                                                <fieldset class="scheduler-border">
                                                    <legend class="scheduler-border text-primary text-bold"> Cancel Letter </legend>
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <textarea name="sales_order_data[cancel_letter]" id="cancel_letter" class="form-control" style="height: 90px;">
                                                                <?=(isset($sales_order_data->cancel_letter))? $sales_order_data->cancel_letter : ''; ?>
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Party Detail</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="sales_order_data[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" placeholder="" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <input type="hidden" name="party[party_name]" id="party_name" class="form-control input-sm party_name" placeholder="">
                                                <input type="hidden" name="party[party_id]" id="party_party_id" class="form-control input-sm party_party_id" placeholder="">
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="address" class="col-sm-3 input-sm">Address</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="city" class="col-sm-3 input-sm">City</label>
                                                    <div class="col-sm-9">
                                                        <div class="col-md-6" style="padding:0px !important;">
                                                            <select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
                                                        </div>
                                                        <div class="col-md-6" style="padding-right:0px !important;">
                                                            <input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="state" class="col-sm-3 input-sm">State</label>
                                                    <div class="col-sm-9">
                                                        <select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="country" class="col-sm-3 input-sm">Country</label>
                                                    <div class="col-sm-9">
                                                        <select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
                                                        <small>Add multiple email id by Comma separated.</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" placeholder="" disabled="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
                                                        <small>Add multiple phone number by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="party_website" class="col-sm-3 input-sm">Website</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm website" id="party_website" name="party[website]" placeholder="" disabled="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <?php /*<div class="form-group">
													<label for="agent_id" class="col-sm-3 input-sm">Agent</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[agent_id]" id="agent_id" class="form-control input-sm"></select>
													</div>
												</div>
												<div class="clearfix"></div>*/ ?>
                                                <div class="form-group">
                                                    <label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="sales_order_data[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
                                                            <option value="">--Select--</option>
                                                            <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
                                                            <?php foreach($party_contact_person as $pcp_key => $pcp_value): ?>
                                                            <option value="<?= $pcp_value->contact_person_id; ?>"  <?=$sales_order_data->kind_attn_id == $pcp_value->contact_person_id ?'selected="selected"':'';?>><?= $pcp_value->name; ?></option>
                                                            <?php endforeach; ?>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Designation</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_designation"><?php echo isset($contact_person->designation) ? $contact_person->designation : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Department</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_department"><?php echo isset($contact_person->department) ? $contact_person->department : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person->mobile_no) ? $contact_person->mobile_no : '' ;?>" disabled="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person->email) ? $contact_person->email : '' ;?>" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($sales_order_lineitems)){ ?>
                                        <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="item_id" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id" onchange="item_details(this.value)"></select>
                                                </div>                                                       
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_code]" id="item_code" class="form-control input-sm item_code disabled" data-name="item_code" readonly="readonly">
                                                    <input type="hidden" class="item_name" id="item_name" name="line_items_data[item_name]" data-name="item_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_description]" id="item_description" class="form-control input-sm item_description" data-name="item_description" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_extra_accessories_id" class="col-sm-3 input-sm">Item Extra Accessories<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_extra_accessories_id]" id="item_extra_accessories_id" class="item_extra_accessories_id form-control input-sm select2 item_data" data-input_name="item_extra_accessories_id" ></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity num_only" value="1" data-name="quantity" placeholder="">
                                                    <input type="hidden" name="line_items_data[dispatched_qty]" id="dispatched_qty" value="0" />
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-3 input-sm">Rate<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <select name="line_items_data[currency_id]" id="currency_id" class="form-control input-sm currency_id" ></select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate"  value="0" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="disc_per" class="col-sm-3 input-sm">Disc(%)</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[disc_per]" id="disc_per" class="form-control input-sm disc_per num_only" data-input_name="disc_per" placeholder="">
                                                </div>
                                                <label for="disc_value" class="col-sm-2 input-sm">Disc(Value)</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[disc_value]" id="disc_value" class="form-control input-sm disc_value num_only" data-input_name="disc_value" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                </div>
                                                <label for="net_amount" class="col-sm-2 input-sm">Net Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[net_amount]" id="net_amount" class="form-control input-sm net_amount disabled" data-input_name="net_amount" placeholder="" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-sm-12"><div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" />
                                            </div>
                                            </div></div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <table style="" class="table custom-table item-table">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Amount</th>
                                                <th class="text-right">Disc(value)</th>
                                                <th class="text-right">Net Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lineitem_list"></tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th class="text-right"><span class="qty_total"></span><input type="hidden" name="qty_total" id="qty_total" /></th>
                                                <th class="text-right"></th>
                                                <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                                <th class="text-right" class="text-right"><span class="disc_value_total"></span><input type="hidden" name="disc_value_total" id="disc_value_total" /></th>
                                                <th class="text-right" class="text-right"><span class="net_amount_total"></span><input type="hidden" name="net_amount_total" id="net_amount_total" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Sales Order Reminder </legend>
                                        <input type="hidden" name="sales_order_reminder_index" id="sales_order_reminder_index" />
                                        <input type="hidden" name="sales_order_reminder_data[followup_by]" id="followup_by" class="followup_by" value="<?=$this->session->userdata('is_logged_in')['staff_id'];?>" />
                                        <input type="hidden" name="sales_order_reminder_data[id]" id="id" class="id" value="" />
                                        <input type="hidden" name="sales_order_reminder_data[reminder_action]" id="reminder_action" class="reminder_action" value="add" />
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="" class="col-md-2 input-sm">&nbsp;</label>
                                                <input type="button" id="add_sales_order_reminder" class="btn btn-info btn-xs add_sales_order_reminder pull-right" value="New Salse Order Reminder" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="followup_category_id" class="col-md-2 input-sm">Followup Categories<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-md-3">
                                                    <select name="sales_order_reminder_data[followup_category_id]" id="followup_category_id" class="form-control input-sm" style="width: 100%;"></select>
                                                </div>
                                                <div class="col-md-7">
                                                    <label for="datepicker1" class="input-sm pull-left">Date<span class="required-sign">&nbsp;*</span></label>
                                                    <input type="hidden" name="sales_order_reminder_data[followup_date]" id="followup_date" class="form-control input-sm pull-left" value="<?= date('d-m-Y H:i:s'); ?>" readonly="">
                                                    <input type="text" name="sales_order_reminder_data[followup_onlydate]" id="datepicker1" class="form-control input-sm pull-left" value="<?= date('d-m-Y'); ?>" style="width:100px;">
                                                    <div class="pull-left col-md-7" style="width:100px;">
                                                        <div class="bootstrap-timepicker" style="position:initial;">
                                                            <input type="text" name="sales_order_reminder_data[followup_onlytime]" id="followup_onlytime" class="form-control input-sm pull-left timepicker" value="<?= date('H:i:s'); ?>" style="width:100px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="history" class="col-md-2 input-sm">Followup History<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-md-10">
                                                    <textarea name="sales_order_reminder_data[history]" id="history" class="form-control" rows="2" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                        <div class="form-group" style="padding-top: 5px;">
                                            <div class="col-md-12">
                                                <label for="reminder_category_id" class="col-md-2 input-sm">Reminder Categories<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-md-3">
                                                    <select name="sales_order_reminder_data[reminder_category_id]" id="reminder_category_id" class="form-control input-sm" style="width: 100%;"></select>
                                                </div>
                                                <div class="col-md-7">
                                                    <label for="datepicker2" class="input-sm pull-left">Next Reminder Date</label>
                                                    <input type="text" name="sales_order_reminder_data[reminder_onlydate]" id="datepicker2" class="form-control input-sm pull-left" value="<?=date('d-m-Y'); ?>" style="width:100px;" >
                                                    <div class="pull-left col-md-7" style="width:100px;">
                                                        <div class="bootstrap-timepicker" style="position:initial;">
                                                            <input type="text" name="sales_order_reminder_data[reminder_onlytime]" id="reminder_onlytime" class="form-control input-sm pull-left timepicker" value="<?= date('H:i:s'); ?>" style="width:100px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="reminder_message" class="col-md-2 input-sm">Reminder Message</label>
                                                <div class="col-md-10">
                                                    <textarea name="sales_order_reminder_data[reminder_message]" id="reminder_message" class="form-control" rows="2" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-md-2 input-sm">Assigned To</label>
                                                <div class="col-md-10">
                                                    <?php if(isset($users) && count($users) > 0):?>
                                                    <?php foreach($users as $row):?>
                                                    <label style="width: 200px;"><input type="checkbox" name="sales_order_reminder_data[assigned_to][]" class="assigned_to" value="<?php echo $row->staff_id;?>">&nbsp;&nbsp;&nbsp;&nbsp;<?=$row->name;?></label>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="" class="col-md-2 input-sm">&nbsp;</label>
                                                <input type="button" id="save_sales_order_reminder" class="btn btn-info btn-xs save_sales_order_reminder pull-right" value="Save Sales Order Reminder" <?=$this->app_model->have_access_role(QUOTATION_FOLLOWUP, "add") ? '' : 'disabled'?>/>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Sales Order Reminder List</legend>
                                        <table style="" class="table custom-table item-table">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Created By</th>
                                                    <th>Followup Cat.</th>
                                                    <th>History</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Reminder Cat.</th>
                                                    <th>Next Remi. Date</th>
                                                    <th>Time</th>
                                                    <th>Reminder Message</th>
                                                    <th width="130px">Assigned To</th>
                                                </tr>
                                            </thead>
                                            <tbody id="sales_order_reminder_list"></tbody>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                            <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="modal-title">Quotation Followup History list</h4>
                                        <table style="" class="table custom-table item-table">
                                            <thead>
                                                <tr>
                                                    <th>Created By</th>
                                                    <th>Followup Cat.</th>
                                                    <th>History</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Reminder Cat.</th>
                                                    <th>Next Remi. Date</th>
                                                    <th>Time</th>
                                                    <th>Reminder Message</th>
                                                    <th>Assigned To</th>
                                                </tr>
                                            </thead>
                                            <tbody id="quotation_followup_history_list"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="modal-title">Enquiry Followup History list</h4>
                                        <table style="" class="table custom-table item-table">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>History</th>
                                                    <th>Followed Up By</th>
                                                </tr>
                                            </thead>
                                            <tbody id="enquiry_followup_history_list"></tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Delivery Schedule </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-5 input-sm"> Minimum (Days)<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control input-sm" id="minimum" name="sales_order_data[mach_deli_min_weeks]" placeholder="" value="<?=(isset($sales_order_data->mach_deli_min_weeks))? $sales_order_data->mach_deli_min_weeks : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-5 input-sm"> Maximum (Days)<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control input-sm" id="maximum" name="sales_order_data[mach_deli_max_weeks]" placeholder="" value="<?=(isset($sales_order_data->mach_deli_max_weeks))? $sales_order_data->mach_deli_max_weeks : ''; ?>" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm" id="minimum_weeks" name="sales_order_data[mach_deli_min_weeks_date]" placeholder="" value="<?=(isset($sales_order_data->mach_deli_min_weeks_date))? date('d-m-Y', strtotime($sales_order_data->mach_deli_min_weeks_date)) : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm approved_date_time" id="maximum_weeks" name="sales_order_data[mach_deli_max_weeks_date]" placeholder="" value="<?=(isset($sales_order_data->mach_deli_max_weeks_date))? date('d-m-Y', strtotime($sales_order_data->mach_deli_max_weeks_date)) : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Dispatch Detials </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Export Order Type<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-md-8 dispaly-flex">
                                                    <select name="sales_order_data[export_order_type]" id="export_order_type" class="form-control input-sm select2" style="width:100%" >
                                                        <option value="" <?=(isset($sales_order_data->export_order_type) ? '' : 'selected');?>>--select--</option>
                                                        <option value="Sales Order" <?=(isset($sales_order_data->export_order_type) ? ($sales_order_data->export_order_type ==  'Sales Order' ? 'selected' : '') : '');?>>Overseas Order</option>
                                                        <option value="Sales Contract" <?=(isset($sales_order_data->export_order_type) ? ($sales_order_data->export_order_type ==  'Sales Contract' ? 'selected' : '') : '');?>>Sales Contract</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Packing & Forwarding </label>
                                                <div class="col-sm-4">
                                                    <select name="sales_order_data[packing_forwarding_by]" id="packing_forwarding_by" class=" input-sm select2" style="width:100%" >
                                                        <option value="1" <?=(isset($sales_order_data->packing_forwarding_by) && $sales_order_data->packing_forwarding_by == 1 ) ? 'selected' : '';?> >Excluding</option>
                                                        <option value="2" <?=(isset($sales_order_data->packing_forwarding_by) && $sales_order_data->packing_forwarding_by == 2 ) ? 'selected' : '';?> >Including</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm" id="packing_forwarding_amount" name="sales_order_data[packing_forwarding_amount]" placeholder="Packing & Forwarding Amount" value="<?=(isset($sales_order_data->packing_forwarding_amount))? $sales_order_data->packing_forwarding_amount : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Loading At </label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" id="loading_at" name="sales_order_data[loading_at]" placeholder="" value="<?=(isset($sales_order_data->loading_at))? $sales_order_data->loading_at : 'Factory-Rajkot'; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Port of Loading</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm" placeholder='Port' id="port_of_loading_city" name="sales_order_data[port_of_loading_city]" placeholder="" value="<?=(isset($sales_order_data->port_of_loading_city))? $sales_order_data->port_of_loading_city : ''; ?>" required="">
                                                </div>
                                                <div class="col-sm-4">
                                                    <!--<input type="text" class="form-control input-sm" placeholder='Country' id="port_of_loading_country_id" name="sales_order_data[port_of_loading_country_id]" placeholder="" value="<?=(isset($sales_order_data->port_of_loading_country_id))? $sales_order_data->port_of_loading_country_id : ''; ?>" required="">-->
                                                    <select name="sales_order_data[port_of_loading_country_id]" id="port_of_loading_country_id" class="form-control input-sm select2"></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Delivery Through<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-8">
                                                    <select name="sales_order_data[delivery_through_id]" class="delivery_through_id form-control input-sm select2" id="delivery_through_id" ></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Port of Discharge</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm" placeholder='Port' id="port_of_discharge_city" name="sales_order_data[port_of_discharge_city]" placeholder="" value="<?=(isset($sales_order_data->port_of_discharge_city))? $sales_order_data->port_of_discharge_city : ''; ?>" required="">
                                                </div>
                                                <div class="col-sm-4">
                                                    <!--<input type="text" class="form-control input-sm" placeholder='Country' id="port_of_discharge_country_id" name="sales_order_data[port_of_discharge_country_id]" placeholder="" value="<?=(isset($sales_order_data->port_of_discharge_country_id))? $sales_order_data->port_of_discharge_country_id : ''; ?>" required="">-->
                                                    <select name="sales_order_data[port_of_discharge_country_id]" id="port_of_discharge_country_id" class="form-control input-sm select2"></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Place of Delivery</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm" placeholder='Place' id="place_of_delivery_city" name="sales_order_data[place_of_delivery_city]" placeholder="" value="<?=(isset($sales_order_data->place_of_delivery_city))? $sales_order_data->place_of_delivery_city : ''; ?>" required="">
                                                </div>
                                                <div class="col-sm-4">
                                                    <!--<input type="text" class="form-control input-sm" placeholder='Country' id="place_of_delivery_country_id" name="sales_order_data[place_of_delivery_country_id]" placeholder="" value="<?=(isset($sales_order_data->place_of_delivery_country_id))? $sales_order_data->place_of_delivery_country_id : ''; ?>" required="">-->
                                                    <select name="sales_order_data[place_of_delivery_country_id]" id="place_of_delivery_country_id" class="form-control input-sm select2"></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Shipping Line Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" id="shipping_line_name" name="sales_order_data[shipping_line_name]" placeholder="" value="<?=(isset($sales_order_data->shipping_line_name))? $sales_order_data->shipping_line_name : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="sea_freight_by" class="col-sm-4 input-sm"> Sea Freight</label>
                                                <div class="col-sm-4">
                                                    <select name="sales_order_data[sea_freight_by]" id="sea_freight_by" class=" input-sm select2" style="width:100%" >
                                                        <option value="1" <?=(isset($sales_order_data->sea_freight_by) && $sales_order_data->sea_freight_by == 1 ) ? 'selected' : '';?> >Excluding</option>
                                                        <option value="2" <?=(isset($sales_order_data->sea_freight_by) && $sales_order_data->sea_freight_by == 2 ) ? 'selected' : '';?> >Including</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm num_only" id="sea_freight" name="sales_order_data[sea_freight]" placeholder="Sea Freight Amount" value="<?=(isset($sales_order_data->sea_freight))? $sales_order_data->sea_freight : ''; ?>" required="" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="sea_freight_type" class="col-sm-4 input-sm"> Sea Freight Type</label>
                                                <div class="col-sm-8">
                                                    <select name="sales_order_data[sea_freight_type]" id="sea_freight_type" class="form-control input-sm select2"></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Bank Details </legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?php foreach($company_banks as $key => $value): ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="sales_order_data[bank_detail]" class="bank_detail" id="bank_detail_<?=$value->id; ?>" value="<?=$value->id; ?>" <?=(isset($sales_order_data->bank_detail) ? ($sales_order_data->bank_detail ==  $value->id ? 'checked' : '') : '');?>>
                                                        <?=nl2br($value->detail);?>
                                                    </label>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Terms of Payment & Delivery </legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea id="supplier_payment_terms" class="form-control" style="height: 100px" name="sales_order_data[supplier_payment_terms]">
                                                    <?=(isset($sales_order_data->supplier_payment_terms))? $sales_order_data->supplier_payment_terms : ''; ?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Terms And Condition</legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea id="terms_condition_purchase" class="form-control" style="height: 100px" name="sales_order_data[terms_condition_purchase]">
                                                    <?=(isset($sales_order_data->terms_condition_purchase))? $sales_order_data->terms_condition_purchase : ''; ?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_5">
                            <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=(isset($login_data->created_by_name))? $login_data->created_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=(isset($login_data->created_at))? $login_data->created_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=(isset($login_data->updated_by_name))? $login_data->updated_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=(isset($login_data->updated_at))? $login_data->updated_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Approved By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_by" id="approved_by" value="<?=(isset($login_data->approved_by_name))? $login_data->approved_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_date_time" id="approved_date_time" value="<?=(isset($login_data->approved_at))? $login_data->approved_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            
        </div>
        <section class="content-header">
            <h1> 
                <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
                <?php if(isset($sales_order_data->isApproved) && $sales_order_data->isApproved == 1){ ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){ ?>
                <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;" <?=($sales_order_data->delete_not_allow == 1) ? ' Disabled ' : ''; ?>>Disapprove</button>
                <?php } else { ?>
                <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                <?php } ?>
                <?php } else { ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){ ?>
                <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                <?php } else { ?>
                <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
                $sales_order_view_role= $this->applib->have_access_role(SALES_ORDER_MODULE_ID,"view");
                $sales_order_add_role= $this->applib->have_access_role(SALES_ORDER_MODULE_ID,"add");
                ?>
                <?php if($sales_order_add_role || $sales_order_edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($sales_order_view_role): ?>
                <a href="<?= base_url()?>sales_order/order_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Order List</a>
                <?php endif;?>
                <?php if($sales_order_add_role): ?>
                <a href="<?= base_url()?>sales_order/add_export/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add Export Order</a>
                <?php endif;?>
            </h1>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ } else { ?>
<div class="modal fade" id="select_quotation_modal" tabindex="-1" role="dialog" aria-labelledby="select_quotation_label" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h4 class="modal-title">Select Quotation
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <table id="quotation_datatable" class="table custom-table table-striped">
                    <thead>
                        <tr>
                            <th class="hidden">Quotation No.</th>
                            <th>Quotation No.</th>
                            <th>Revision</th>
                            <th>Quotation Date</th>
                            <th>Customer Name</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="hidden">Quotation No.</th>
                            <th>Quotation No.</th>
                            <th>Revision</th>
                            <th>Quotation Date</th>
                            <th>Customer Name</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?=dist_url('js/sales_order.js');?>"></script>
<script type="text/javascript">
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
    var lineitem_objectdata = [];
    <?php if(isset($sales_order_lineitems)){ ?>
    var li_lineitem_objectdata = [<?php echo $sales_order_lineitems; ?>];
    var lineitem_objectdata = [];
    if (li_lineitem_objectdata != '') {
        $.each(li_lineitem_objectdata, function(index, value) {
            lineitem_objectdata.push(value);
        });
    }
    <?php } ?>
    display_lineitem_html(lineitem_objectdata);
    var edit_sales_order_reminder_inc = 0;
	var sales_order_reminder_objectdata = [];
    <?php if(isset($sales_order_reminder_data)){ ?>
		var li_sales_order_reminder_objectdata = [<?php echo $sales_order_reminder_data; ?>];
		var sales_order_reminder_objectdata = [];
        if(li_sales_order_reminder_objectdata != ''){
            $.each(li_sales_order_reminder_objectdata, function (index, value) {
                sales_order_reminder_objectdata.push(value);
            });
        }
	<?php } ?>
    display_sales_order_reminder_html(sales_order_reminder_objectdata);
    var quotation_reminder_objectdata = [];
    <?php if(isset($quotation_reminder_data)){ ?>
		var li_quotation_reminder_objectdata = [<?php echo $quotation_reminder_data; ?>];
		var quotation_reminder_objectdata = [];
        if(li_quotation_reminder_objectdata != ''){
            $.each(li_quotation_reminder_objectdata, function (index, value) {
                quotation_reminder_objectdata.push(value);
            });
        }
	<?php } ?>
    display_quotation_reminder_html(quotation_reminder_objectdata);
    var enquiry_followup_history_objectdata = [];
    <?php if(isset($enquiry_followup_history_data)){ ?>
		var li_enquiry_followup_history_objectdata = [<?php echo $enquiry_followup_history_data; ?>];
		var enquiry_followup_history_objectdata = [];
        if(li_enquiry_followup_history_objectdata != ''){
            $.each(li_enquiry_followup_history_objectdata, function (index, value) {
                enquiry_followup_history_objectdata.push(value);
            });
        }
	<?php } ?>
    display_enquiry_followup_history_html(enquiry_followup_history_objectdata);
    <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ ?>
    first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function() {
        $('.select2').select2();
        $('input[type="checkbox"].send_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        initAjaxSelect2($("#sales_id"), "<?=base_url('app/sales_select2_source')?>");
        initAjaxSelect2($("#currency_id"), "<?=base_url('app/currency_select2_source')?>");
        initAjaxSelect2($("#branch_id"), "<?=base_url('app/branch_select2_source')?>");
        <?php if(isset($sales_order_data->branch_id)){ ?>
        setSelect2Value($("#branch_id"), "<?=base_url('app/set_branch_select2_val_by_id/'.$sales_order_data->branch_id)?>");
        <?php } else { ?>
        setSelect2Value($("#branch_id"), "<?=base_url('app/set_branch_select2_val_by_id/'.DEFAULT_BRANCH_ID)?>");
        <?php } ?>
        initAjaxSelect2($("#agent_id"), "<?=base_url('app/agent_select2_source')?>");
        <?php if(isset($sales_order_data->agent_id)){ ?>
        setSelect2Value($("#agent_id"), "<?=base_url('app/set_agent_select2_val_by_id/'.$sales_order_data->agent_id)?>");
        <?php } ?>
        
        initAjaxSelect2($("#city"), "<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"), "<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"), "<?=base_url('app/country_select2_source')?>");
        initAjaxSelect2($("#item_id"), "<?=base_url('app/item_select2_source')?>");
        initAjaxSelect2($("#reference_id"), "<?=base_url('app/reference_select2_source')?>");
        initAjaxSelect2($("#item_extra_accessories_id"), "<?=base_url('app/item_extra_accessories_select2_source')?>");
        setSelect2Value($("#item_extra_accessories_id"), "<?=base_url('app/set_item_extra_accessories_select2_val_by_id/'.DEFAULT_ITEM_EXTRA_ACCESSORIES_ID)?>");

        initAjaxSelect2($("#reminder_category_id"),"<?=base_url('app/followup_category_select2_source')?>");
        initAjaxSelect2($("#followup_category_id"),"<?=base_url('app/followup_category_select2_source')?>");

        initAjaxSelect2($("#delivery_through_id"),"<?=base_url('app/delivery_through_select2_source')?>");
        <?php if(isset($sales_order_data->delivery_through_id)){ ?>
			setSelect2Value($("#delivery_through_id"),"<?=base_url('app/set_delivery_through_select2_val_by_id/'.$sales_order_data->delivery_through_id)?>");
        <?php } ?>

        <?php if(isset($sales_order_data->sales_to_party_id)){ ?>
        setSelect2Value($("#party_id"), "<?=base_url('app/set_party_select2_val_by_id/'.$sales_order_data->sales_to_party_id)?>");
        party_details(<?=$sales_order_data->sales_to_party_id; ?>);
        <?php } ?>

        initAjaxSelect2($("#port_of_loading_country_id"), "<?=base_url('app/country_select2_source')?>");
        <?php if(isset($sales_order_data->port_of_loading_country_id)){ ?>
        setSelect2Value($("#port_of_loading_country_id"), "<?=base_url('app/set_country_select2_val_by_id/'.$sales_order_data->port_of_loading_country_id)?>");
        <?php } ?>

        initAjaxSelect2($("#port_of_discharge_country_id"), "<?=base_url('app/country_select2_source')?>");
        <?php if(isset($sales_order_data->port_of_discharge_country_id)){ ?>
        setSelect2Value($("#port_of_discharge_country_id"), "<?=base_url('app/set_country_select2_val_by_id/'.$sales_order_data->port_of_discharge_country_id)?>");
        <?php } ?>

        initAjaxSelect2($("#place_of_delivery_country_id"), "<?=base_url('app/country_select2_source')?>");
        <?php if(isset($sales_order_data->place_of_delivery_country_id)){ ?>
        setSelect2Value($("#place_of_delivery_country_id"), "<?=base_url('app/set_country_select2_val_by_id/'.$sales_order_data->place_of_delivery_country_id)?>");
        <?php } ?>

        initAjaxSelect2($("#sea_freight_type"), "<?=base_url('app/sea_freight_type_select2_source')?>");
        <?php if(isset($sales_order_data->sea_freight_type)){ ?>
        setSelect2Value($("#sea_freight_type"), "<?=base_url('app/set_sea_freight_type_select2_val_by_id/'.$sales_order_data->sea_freight_type)?>");
        <?php } else{ ?>
        setSelect2Value($("#sea_freight_type"), "<?=base_url('app/set_sea_freight_type_select2_val_by_id/1')?>");
        <?php } ?>

        var table
        table = $('#quotation_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [
                [1, 'desc']
            ],
            "ajax": {
                "url": "<?php echo site_url('sales_order/quotation_datatable_for_sales_order_create')?>",
                "type": "POST",
                "data": function(d) {
                    d.order_for = 'export';
                }
            },
            "scrollY": 400,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columns": [{
                "class": "hidden"
            },
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                       ]
        });

        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.protectedSource.push(/<\?[\s\S]*?\?>/g);
        CKEDITOR.replace('supplier_payment_terms', {
            removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        CKEDITOR.replace('terms_condition_purchase', {
            removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        CKEDITOR.replace('cancel_letter', {
            height: 120,
            removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        
        $('#is_dispatched').on("change", function (e) {
            if($(this).val() == "<?=CANCELED_ORDER_ID;?>"){
                $(".cancel_reason_div").removeClass("hidden");
                $.ajax({
            url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
            type: "POST",
            data: {
                module: 'export_sales_order_cancel_order_letter'
            },
            dataType: 'json',
            success: function(data) {
                CKEDITOR.instances['cancel_letter'].setData(data.terms_and_conditions);
            }
        });
            }else{
                $(".cancel_reason_div").addClass("hidden");
            }
        });

        $('#select_quotation_modal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#select_quotation_modal').modal('show');
        $('#select_quotation_modal').on('shown.bs.modal', function() {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

        $(document).on('click', '.quotation_row', function() {
            var tr = $(this).closest('tr');
            var quotation_id = $(this).data('quotation_id');
            $("#quotation_id").val(quotation_id);
            feedQuotationData(quotation_id);
            $('#select_quotation_modal').modal('hide');
            $('#sales_order_date').focus();
        });

        $(document).on('input', '#item_rate', function() {
            if (this.value > 9900000) {
                this.value = 9900000;
            }
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href"); // activated tab
            if (target == '#tab_1') {
                $('#item_id').select2('close');
                $('#followup_category_id').select2('close');
                $('#sales_order_date').off("focus").datepicker("show");
            } else if (target == '#tab_2') {
                $('#item_id').select2('open');
                $('#sales_order_date').off("focus").datepicker("hide");
                $('#followup_category_id').select2('close');
            } else if (target == '#tab_3') { 
                $('#followup_category_id').select2('open');
                $('#item_id').select2('close');
                $( "#frdate" ).datepicker( "destroy" );
                $('#sales_order_date').off("focus").datepicker("hide");
            } else {
                $('#item_id').select2('close');
                $('#sales_order_date').off("focus").datepicker("hide");
                $('#followup_category_id').select2('close');
            }
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_sales_order_export").submit();
                return false;
            }
        });

        $(document).on('submit', '#save_sales_order_export', function() {
            
            if($($('#is_dispatched')).val() == "<?=CANCELED_ORDER_ID;?>"){
                if ($.trim($("#cancel_reason_text").val()) == '') {
                    show_notify("Please give reason for cancel order.", false);
                    $("#cancel_reason_text").focus();
                    return false;
                }
                if ($.trim($("#proof_of_cancellation").val()) == '') {
                    show_notify('Please Give Proof Of Cancellation.', false);
                    $("#proof_of_cancellation").focus();
                    return false;
                }
                if ($.trim($("#cancel_letter").val()) == '') {
                    show_notify('Please Type Cancel Letter.', false);
                    $("#cancel_letter").focus();
                    return false;
                }
            }
            
            if ($.trim($("#party_id").val()) == '') {
                show_notify('Please Select Party.', false);
                return false;
            }
            if ($.trim($("#reference_id").val()) == '') {
                show_notify('Please Select Reference.', false);
                return false;
            }

            if ($.trim($("#item_id").val()) != '') {
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }

            if ($.trim($("#minimum").val()) == '' || $.trim($("#minimum").val()) == 0) {
                show_notify('Please Enter Delivery Schedule Minimum (Days).', false);
                $('[href="#tab_4"]').click();
                return false;
            }
            if ($.trim($("#maximum").val()) == '' || $.trim($("#maximum").val()) == 0) {
                show_notify('Please Enter Delivery Schedule Maximum (Days).', false);
                $('[href="#tab_4"]').click();
                return false;
            }

            if ($("#export_order_type").val() == '') {
                show_notify('Please Select Export Order Type.', false);
                $('[href="#tab_4"]').click();
                return false;
            }
            
            if ($("#packing_forwarding_by").val() == '1' && $("#packing_forwarding_amount").val() == '') {
                show_notify('Please Enter Packing Forwarding Amount.', false);
                $('[href="#tab_4"]').click();
                return false;
            }
            if ($("#sea_freight_by").val() == '1' && $("#sea_freight").val() == '') {
                show_notify('Please Enter Sea Freight Amount.', false);
                $('[href="#tab_4"]').click();
                return false;
            }
            if ($("#delivery_through_id").val() == '' || $("#delivery_through_id").val() == null) {
                show_notify('Please Select Delivery Through.', false);
                $('[href="#tab_4"]').click();
                return false;
            }
            
            var isChecked = $('.bank_detail').is(':checked');
            if (isChecked == false) {
                show_notify('Please Select Bank Account Details.', false);
                $('[href="#tab_4"]').click();
                return false;
            }
            
            //if(followup_history_objectdata == ''){
            //	show_notify("Please add at least one follow up history.", false);
            //	return false;
            //}
            if (lineitem_objectdata == '') {
                show_notify("Please select any one Item.", false);
                return false;
            }
			
			var extra_accessories = null;
			var extra_accessories_item_name;
			$.each(lineitem_objectdata, function (index, value) {
                if(value.item_extra_accessories_id == '' || value.item_extra_accessories_id == null){
					extra_accessories_item_name = value.item_name
					extra_accessories = false;
				}
            });
			if(extra_accessories == false){
			   	show_notify(extra_accessories_item_name + " : Extra Accessories is required!", false);
				return false;
			}
			
            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
			var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
            postData.append('line_items_data', lineitem_objectdata_stringify);
            var sales_order_reminder_objectdata_stringify = JSON.stringify(sales_order_reminder_objectdata);
			postData.append('sales_order_reminder_data', sales_order_reminder_objectdata_stringify);
            $.ajax({
                url: "<?=base_url('sales_order/save_sales_order_export') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function(response) {
                    var json = $.parseJSON(response);
                    if (json.status == 0) {
                        show_notify(json.msg, false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                        return false;
                    }
                    if (json['success'] == 'false') {
                        show_notify(json['msg'], false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                    }
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('sales_order/order_list') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('sales_order/order_list') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });

        $(document).on("change", '#contact_person_id', function() {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?=base_url();?>party/get-contact-person-by-id/" + contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });

        $(document).on("change", '#currency_id', function() {
            if (edit_lineitem_inc == 0) {
                var currency_id = $("#currency_id").val();
                $("#so_currency_id").val(currency_id);
                var item_id = $("#item_id").val();
                if (item_id != '' && item_id != null) {
                    $.ajax({
                        type: "POST",
                        url: '<?=base_url();?>sales/ajax_load_item_details/' + item_id,
                        data: id = 'item_id',
                        success: function(data) {
                            var json = $.parseJSON(data);
                            if (json['item_rate_in']) {
                                var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                                $("#item_rate").val(item_rate_in_json[currency_id]);
                            } else {
                                $("#item_rate").val('');
                            }
                            input_value_get_amount();
                        }
                    });
                }
            } else {
                edit_lineitem_inc = 0;
            }
        });

        $(document).on("change", '#export_order_type', function() {
            var export_order_type = $(this).val();
            var module;
            if(export_order_type == 'Sales Order'){
                module = 'sales_order_terms_and_conditions_for_export_order';
            }else if(export_order_type == 'Sales Contract'){
                module = 'sales_order_terms_and_conditions_for_contract';
            }
            $.ajax({
                url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                type: "POST",
                data: {
                    //party_type: quotation_data.party_type_1,
                    module: module
                },
                dataType: 'json',
                success: function(data) {
                    CKEDITOR.instances['terms_condition_purchase'].setData( data.terms_and_conditions );
                }
            });
        });

        $(document).on("click", ".btn_approve_ord", function() {
            $(".send_sms_div").removeClass('hidden');
            $("#ajax-loader").show();
            var sales_order_id = $('#sales_order_id').val();
            $.ajax({
                url: '<?=BASE_URL?>sales_order/update_sales_order_status_to_approved',
                type: "POST",
                data: {
                    sales_order_id: sales_order_id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){?>
                        $(".btn_approve_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_disapprove_ord' style='margin: 5px;'>Disapprove</button>");
                        <?php }else{?>
                        $(".btn_approve_ord").after("<small class='text-primary text-bold pull-right app_lab' style='margin: 5px;'>Approved</small>");
                        <?php }?>
                        $('.btn_approve_ord').remove();
                        show_notify(data.message, true);
                    } else {
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on("click", ".btn_disapprove_ord", function() {
            $(".send_sms_div").addClass('hidden');
            $("#ajax-loader").show();
            var sales_order_id = $('#sales_order_id').val();
            $.ajax({
                url: '<?=BASE_URL?>sales_order/update_sales_order_status_to_disapproved',
                type: "POST",
                data: {
                    sales_order_id: sales_order_id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $(".btn_disapprove_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_approve_ord' style='margin: 5px;'>Approve</button>");
                        //$('.btn_approve_ord').show();
                        $('.btn_disapprove_ord').remove();
                        show_notify(data.message, true);
                    } else {
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $('#add_lineitem').on('click', function() {
            var item_id = $("#item_id").val();
            if (item_id == '' || item_id == null) {
                show_notify("Please select Item!", false);
                return false;
            }
            var quantity = $("#quantity").val();
            var dispatched_qty = $("#dispatched_qty").val();
            if (quantity == '' || quantity == null) {
                show_notify("Item quantity is required!", false);
                return false;
            }
            if(quantity < dispatched_qty){
                show_notify( dispatched_qty + " Qty challan(s) created, so you can Not make Qty less than " + dispatched_qty, false);
				return false;
            }
            var item_rate = $("#item_rate").val();
            if (item_rate == '' || item_rate == null) {
                show_notify("Item Rate is required!", false);
                return false;
            }
			var item_extra_accessories_id = $("#item_extra_accessories_id").val();
			if(item_extra_accessories_id == '' || item_extra_accessories_id == null){
				show_notify("Item Extra Accessories is required!", false);
				return false;
			}
			
            var key = '';
            var value = '';
            var lineitem = {};
            $('select[name^="line_items_data"]').each(function(e) {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            $('input[name^="line_items_data"]').each(function() {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            var new_lineitem = JSON.parse(JSON.stringify(lineitem));
            var line_items_index = $("#line_items_index").val();
            if (line_items_index != '') {
                lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
            } else {
                lineitem_objectdata.push(new_lineitem);
            }
            display_lineitem_html(lineitem_objectdata);
            $('#lineitem_id').val('');
            $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
            $("#item_id").val(null).trigger("change");
            $("#item_id").prop( "disabled", false );
            $("#item_extra_accessories_id").val(null).trigger("change");
            $("#quantity").val('1');
            $("#dispatched_qty").val('0');
            $("#disc_per").val('0');
            $("#line_items_index").val('');
            if (on_save_add_edit_item == 1) {
                on_save_add_edit_item == 0;
                $('#save_sales_order_export').submit();
            }
            var sales_id = $("#sales_id").val();
            if (sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>') {
                setSelect2Value($("#currency_id"), '<?=base_url()?>app/set_currency_select2_val_by_id/' + <?=INR_CURRENCY_ID; ?>);
            }                
            if (sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>') {
                setSelect2Value($("#currency_id"), '<?=base_url()?>app/set_currency_select2_val_by_id/' + <?=USD_CURRENCY_ID; ?>);
            }
        });

        $('#save_sales_order_reminder').on('click', function() {
			var followup_category_id = $("#followup_category_id").val();
			if(followup_category_id == '' || followup_category_id == null){
				show_notify("Please Select Followup Category!", false);
                $("#followup_category_id").select2('open');
				return false;
			}
            var followup_date = $("#datepicker1").val();
			if(followup_date == '' || followup_date == null){
				show_notify("Please Enter Followup Date!", false);
                $("#datepicker1").focus();
				return false;
			}
			var history = $("#history").val();
			if(history == '' || history == null){
				show_notify("Please Enter Followup History!", false);
                $("#history").focus();
				return false;
			}
            var reminder_category_id = $("#reminder_category_id").val();
			if(reminder_category_id == '' || reminder_category_id == null){
				show_notify("Please Select Reminder Category!", false);
                $("#reminder_category_id").select2('open');
				return false;
			}
			var key = '';
			var value = '';
			var sales_order_reminder = {};
			$('select[name^="sales_order_reminder_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("sales_order_reminder_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				sales_order_reminder[key] = value;
			});
			$('input[name^="sales_order_reminder_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("sales_order_reminder_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				sales_order_reminder[key] = value;
			});
            sales_order_reminder['history'] = $('#history').val();
            sales_order_reminder['reminder_message'] = $('#reminder_message').val();
            sales_order_reminder['followup_by'] = $('#followup_by').val();
            sales_order_reminder['followup_at'] = $('#followup_at').val();
            sales_order_reminder['updated_by'] = '<?=$staff_id;?>';
            sales_order_reminder['updated_at'] = '<?=date('d-m-Y H:i:s');?>';
            sales_order_reminder['reminder_action'] = $('#reminder_action').val();
            sales_order_reminder['id'] = $('#id').val();
			
            var i = 0;
            sales_order_reminder['assigned_to'] = '';
            $('.assigned_to:checked').each(function () {
                if(i == 0){
                    sales_order_reminder['assigned_to'] += $(this).val();
                } else {
                    sales_order_reminder['assigned_to'] += ',' + $(this).val();
                }
                i++;
            });
            var new_sales_order_reminder = JSON.parse(JSON.stringify(sales_order_reminder));
			var sales_order_reminder_index = $("#sales_order_reminder_index").val();
			if(sales_order_reminder_index != ''){
				sales_order_reminder_objectdata.splice(sales_order_reminder_index, 1, new_sales_order_reminder);
                sales_order_reminder_objectdata.reverse();
			} else {
				sales_order_reminder_objectdata.reverse();
				sales_order_reminder_objectdata.push(new_sales_order_reminder);
			}
			display_sales_order_reminder_html(sales_order_reminder_objectdata);
			$("#followup_category_id").val(null).trigger("change");
            $("#followup_date").val('<?php echo date('d-m-Y H:i:s'); ?>');
            $("#datepicker1").val('');
            $("#datepicker1").val('<?php echo date('d-m-Y'); ?>');
            $("#followup_onlytime").val('');
            $("#followup_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#history").val('');
            $("#reminder_category_id").val(null).trigger("change");
            $("#datepicker2").val('');
            $("#datepicker2").val('<?php echo date('d-m-Y'); ?>');
            $("#reminder_onlytime").val('');
            $("#reminder_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#reminder_message").val('');
            $("#reminder_action").val('add');
            $("#id").val('');
            $("#followup_by").val('<?=$staff_id?>');
            $("#followup_at").val('<?=date('d-m-Y H:i:s');?>');
            $('.assigned_to').prop("checked", false);
            $("#sales_order_reminder_index").val('');
			if(edit_sales_order_reminder_inc == 1 && <?=$this->app_model->have_access_role(QUOTATION_FOLLOWUP, "add")?> != 1){
				$('#save_sales_order_reminder').attr("disabled", "disabled");
			}
        });
					
        $('#add_sales_order_reminder').on('click', function() {
			$("#followup_category_id").val(null).trigger("change");
            $("#followup_date").val('<?php echo date('d-m-Y H:i:s'); ?>');
            $("#datepicker1").val('');
            $("#datepicker1").val('<?php echo date('d-m-Y'); ?>');
            $("#followup_onlytime").val('');
            $("#followup_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#history").val('');
            $("#reminder_category_id").val(null).trigger("change");
            $("#datepicker2").val('');
            $("#datepicker2").val('<?php echo date('d-m-Y'); ?>');
            $("#reminder_onlytime").val('');
            $("#reminder_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#reminder_message").val('');
            $("#reminder_action").val('add');
            $("#id").val('');
            $("#followup_by").val('<?=$staff_id?>');
            $("#followup_at").val('<?= date('d-m-Y H:i:s'); ?>');
            $('.assigned_to').prop("checked", false);
            $("#sales_order_reminder_index").val('');
        });
    });

    function feedQuotationData(quotation_id) {
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>sales_order/get_quotation',
            type: "POST",
            async: false,
            data: {
                id: quotation_id
            },
            success: function(data) {
                $("#ajax-loader").hide();
                json = JSON.parse(data);
                var quotation_data = json.quotation_data;
                setSelect2Value($("#party_id"), "<?=base_url('app/set_party_select2_val_by_id')?>/" + quotation_data.party_id);
                $('#party_name').val($('#party_id').val());
                $('#party_party_id').val(quotation_data.party_id);
                party_details(quotation_data.party_id);
                $('#quotation_no').val(quotation_data.quotation_no);
                $('#quotation_rev').val(quotation_data.rev);
                $('select[name="sales_order_data[kind_attn_id]"]').val(quotation_data.kind_attn_id).change();
                if (json.inquiry_lineitems != '') {
                    lineitem_objectdata = json.quotation_lineitems;
                    display_lineitem_html(lineitem_objectdata);
                }
                if (quotation_data.party_type_1 != null) {
                    $.ajax({
                        url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                        type: "POST",
                        data: {
                            party_type: quotation_data.party_type_1,
                            module: 'sales_order_payment_terms'
                        },
                        dataType: 'json',
                        success: function(data) {
                            CKEDITOR.instances['supplier_payment_terms'].setData(data.terms_and_conditions);
                        }
                    });
                }
            },
        });
    }

    function display_lineitem_html(lineitem_objectdata) {
        var new_lineitem_html = '';
        var qty_total = 0;
        var amount_total = 0;
        var disc_value_total = 0;
        var net_amount_total = 0;
        //		console.log(lineitem_objectdata);
        $.each(lineitem_objectdata, function(index, value) {
            var value_currency;
            $.ajax({
                url: "<?=base_url('app/set_currency_select2_val_by_id/') ?>/" + value.currency_id,
                type: "POST",
                dataType: 'json',
                async: false,
                cache: false,
                success: function(response) {
                    if (response.text == '--select--') {
                        response.text = '';
                    }
                    value_currency = response.text;
                },
            });

            var lineitem_edit_btn = '';
            lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
            var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
                lineitem_edit_btn;
                <?php if(empty($sales_order_data->id)){ ?>
                    row_html += ' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a> ';
                <?php } else { ?>
                    if(value.dispatched_qty == 0){
                        row_html += ' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a> ';
                    }
                <?php } ?>
                row_html += '</td>' +
                '<td>' + value.item_code + '</td>' +
                '<td>' + value.item_name + '</td>' +
                '<td class="text-right">' + value.quantity + '</td>';
            if (value_currency != '') {
                row_html += '<td class="text-right">' + value_currency + ' - ' + value.item_rate + '</td>';
            } else {
                row_html += '<td class="text-right">' + value.item_rate + '</td>';
            }
            row_html += '<td class="text-right">' + value.amount + '</td>' +
                '<td class="text-right">' + value.disc_value + '</td>' +
                '<td class="text-right">' + value.net_amount + '</td></tr>';
            new_lineitem_html += row_html;
            qty_total += +value.quantity;
            amount_total += +value.amount;
            disc_value_total += +value.disc_value;
            net_amount_total += +value.net_amount;
        });
        $('tfoot span.qty_total').html(qty_total);
        $('#qty_total').val(qty_total);
        $('tfoot span.amount_total').html(amount_total);
        $('#amount_total').val(amount_total);
        $('tfoot span.disc_value_total').html(disc_value_total);
        $('#disc_value_total').val(disc_value_total);
        $('tfoot span.net_amount_total').html(net_amount_total.toFixed(2));
        $('#net_amount_total').val(net_amount_total.toFixed(2));
        $('tbody#lineitem_list').html(new_lineitem_html);
    }

    function edit_lineitem(index) {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        $('.box-body .overlay').show();
        if (edit_lineitem_inc == 0) {
            edit_lineitem_inc = 1; /*$('.edit_lineitem_'+ index).click();*/
        }
        value = lineitem_objectdata[index];
        $("#line_items_index").val(index);

        initAjaxSelect2($("#item_id"), "<?=base_url('app/item_select2_source/')?>");
        setSelect2Value($("#item_id"), "<?=base_url('app/set_li_item_select2_val_by_id/')?>/" + value.item_id);
        if(value.dispatched_qty != 0){
            $("#item_id").prop( "disabled", true );
        } else {
            $("#item_id").prop( "disabled", false );
        }
        initAjaxSelect2($("#item_extra_accessories_id"), "<?=base_url('app/item_extra_accessories_select2_source/')?>");
        setSelect2Value($("#item_extra_accessories_id"), "<?=base_url('app/set_item_extra_accessories_select2_val_by_id/')?>/" + value.item_extra_accessories_id);
        initAjaxSelect2($("#currency_id"), "<?=base_url('app/currency_select2_source/')?>");
        setSelect2Value($("#currency_id"), "<?=base_url('app/set_currency_select2_val_by_id/')?>/" + value.currency_id);
        if (typeof(value.id) != "undefined" && value.id !== null) {
            $("#lineitem_id").val(value.id);
        }
        $("#item_code").val(value.item_code);
        $("#item_name").val(value.item_name);
        $("#item_description").val(value.item_description);
        $("#item_rate").val(value.item_rate);
        $("#quantity").val(value.quantity);
        $("#dispatched_qty").val(value.dispatched_qty);
        $("#amount").val(value.amount);
        $("#disc_per").val(value.disc_per);
        $("#disc_value").val(value.disc_value);
        $("#net_amount").val(value.net_amount);
        $('.overlay').hide();
    }

    function remove_lineitem(index) {
        if (confirm('Are you sure ?')) {
            value = lineitem_objectdata[index];
            if (typeof(value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index, 1);
            display_lineitem_html(lineitem_objectdata);
        }
    }

    function display_sales_order_reminder_html(sales_order_reminder_objectdata){
		var new_sales_order_reminder_html = '';
		sales_order_reminder_objectdata.reverse();
		console.log(sales_order_reminder_objectdata);
		$.each(sales_order_reminder_objectdata, function (index, value) {
            var value_followup_category;
			$.ajax({
				url: "<?=base_url('app/set_followup_category_select2_val_by_id/') ?>/" + value.followup_category_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_followup_category = response.text;
				},
			});
            var value_reminder_category;
			$.ajax({
				url: "<?=base_url('app/set_followup_category_select2_val_by_id/') ?>/" + value.reminder_category_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_reminder_category = response.text;
				},
			});
            var value_staff_name;
			//alert(value.followup_by);
			$.ajax({
				url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + value.followup_by,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_staff_name = response.text;
				},
			});
            var value_assigned_to_names = '';
            var assigned_to = value.assigned_to;
            if(assigned_to != '' && assigned_to != null){
                var assigned_to_arr = assigned_to.split(',');
                var i = 0;
                $.each(assigned_to_arr, function(i, val){
                    if(i == 0){ 
                    } else {
                        value_assigned_to_names += ', ';
                    }
                    i++;
                    $.ajax({
                        url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + val,
                        type: "POST",
                        dataType: 'json',
                        async: false,
                        cache: false,
                        success: function (response) {
                            if(response.text == '--select--'){ response.text = ''; }
                            value_assigned_to_names += response.text;
                        },
                    });
                });
            }
            var sales_order_reminder_edit_btn = '';
            var sales_order_reminder_delete_btn = '';
			<?php if($this->app_model->have_access_role(QUOTATION_FOLLOWUP, "edit")): ?>
			sales_order_reminder_edit_btn = '<a class="btn btn-xs btn-primary edit_sales_order_reminder_' + index + '" href="javascript:void(0);" onclick="edit_sales_order_reminder(' + index + ')"><i class="fa fa-edit"></i></a> ';
			<?php endif;
			if($this->app_model->have_access_role(QUOTATION_FOLLOWUP, "delete")): ?>
			sales_order_reminder_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_sales_order_reminder(' + index + ')"><i class="fa fa-remove"></i></a>';
			<?php endif;?>
			
			var sales_order_reminder_row_html = '<tr class="sales_order_reminder_index_' + index + '"><td class="">';
			sales_order_reminder_row_html += sales_order_reminder_edit_btn;
			sales_order_reminder_row_html += sales_order_reminder_delete_btn;
			sales_order_reminder_row_html += '</td>';
            sales_order_reminder_row_html += '<td>' + value_staff_name + '</td>';
			sales_order_reminder_row_html += '<td>' + value_followup_category + '</td>';
            sales_order_reminder_row_html += '<td>' + value.history + '</td>';
			sales_order_reminder_row_html += '<td nowrap>' + value.followup_onlydate + '</td>';
			sales_order_reminder_row_html += '<td nowrap>' + value.followup_onlytime + '</td>';
			sales_order_reminder_row_html += '<td>' + value_reminder_category + '</td>';
			sales_order_reminder_row_html += '<td nowrap>' + value.reminder_onlydate + '</td>';
			sales_order_reminder_row_html += '<td nowrap>' + value.reminder_onlytime + '</td>';
			sales_order_reminder_row_html += '<td>' + value.reminder_message + '</td>';
			sales_order_reminder_row_html += '<td>' + value_assigned_to_names + '</td>';
			new_sales_order_reminder_html += sales_order_reminder_row_html;
		});
		$('tbody#sales_order_reminder_list').html(new_sales_order_reminder_html);
	}
    
    function edit_sales_order_reminder(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_sales_order_reminder_inc == 0){
			edit_sales_order_reminder_inc = 1; 
			$('.edit_sales_order_reminder_'+ index).click();
			$("#save_sales_order_reminder").removeAttr("disabled");
		}
		value = sales_order_reminder_objectdata[index];
		$("#sales_order_reminder_index").val(index);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#sales_order_reminder_id").val(value.id);
		}
        initAjaxSelect2($("#followup_category_id"),"<?=base_url('app/followup_category_select2_source/')?>");
		setSelect2Value($("#followup_category_id"),"<?=base_url('app/set_followup_category_select2_val_by_id/')?>/" + value.followup_category_id);
        initAjaxSelect2($("#reminder_category_id"),"<?=base_url('app/followup_category_select2_source/')?>");
		setSelect2Value($("#reminder_category_id"),"<?=base_url('app/set_followup_category_select2_val_by_id/')?>/" + value.reminder_category_id);
        $("#followup_date").val(value.followup_date);
        $("#datepicker1").val(value.followup_onlydate);
        $("#followup_onlytime").val(value.followup_onlytime);
        $("#history").val(value.history);
        $("#datepicker2").val(value.reminder_onlydate);
        $("#reminder_onlytime").val(value.reminder_onlytime);
        $("#reminder_message").val(value.reminder_message);
        $("#reminder_action").val("edit");
        $("#id").val(value.id);
        $("#followup_by").val(value.followup_by);
        $("#followup_at").val(value.followup_at);
        $('.assigned_to').prop("checked", false);
        var assigned_to = value.assigned_to;
        if(assigned_to != ''){
            var assigned_to_arr = assigned_to.split(',');
            $.each(assigned_to_arr, function(i, val){
                $("input[value='" + val + "']").prop('checked', true);
            });
        }
		$('.overlay').hide();
	}
	
	function remove_sales_order_reminder(index){
		if(confirm('Are you sure ?')){
            value = sales_order_reminder_objectdata[index];
			if(typeof(value.id) != "undefined" && value.id !== null) {
				$('#save_sales_order').append('<input type="hidden" name="deleted_sales_order_reminder_id[]" id="deleted_sales_order_reminder_id" value="' + value.id + '" />');
            }
			sales_order_reminder_objectdata.splice(index,1);
            sales_order_reminder_objectdata.reverse();
            display_sales_order_reminder_html(sales_order_reminder_objectdata);
        }
	}
    
    function display_quotation_reminder_html(quotation_reminder_objectdata){
		var new_quotation_reminder_html = '';
		quotation_reminder_objectdata.reverse();
		$.each(quotation_reminder_objectdata, function (index, value) {
            var value_followup_category;
			$.ajax({
				url: "<?=base_url('app/set_followup_category_select2_val_by_id/') ?>/" + value.followup_category_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_followup_category = response.text;
				},
			});
            var value_reminder_category;
			$.ajax({
				url: "<?=base_url('app/set_followup_category_select2_val_by_id/') ?>/" + value.reminder_category_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_reminder_category = response.text;
				},
			});
            var value_staff_name;
			//alert(value.followup_by);
			$.ajax({
				url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + value.followup_by,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_staff_name = response.text;
				},
			});
            var value_assigned_to_names = '';
            var assigned_to = value.assigned_to;
            if(assigned_to != '' && assigned_to != null){
                var assigned_to_arr = assigned_to.split(',');
                var i = 0;
                $.each(assigned_to_arr, function(i, val){
                    if(i == 0){
                    } else {
                        value_assigned_to_names += ', ';
                    }
                    i++;
                    $.ajax({
                        url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + val,
                        type: "POST",
                        dataType: 'json',
                        async: false,
                        cache: false,
                        success: function (response) {
                            if(response.text == '--select--'){ response.text = ''; }
                            value_assigned_to_names += response.text;
                        },
                    });
                });
            }
			var quotation_reminder_row_html = '<tr class="quotation_reminder_index_' + index + '">';
			quotation_reminder_row_html += '<td>' + value_staff_name + '</td>';
			quotation_reminder_row_html += '<td>' + value_followup_category + '</td>';
            quotation_reminder_row_html += '<td>' + value.history + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.followup_onlydate + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.followup_onlytime + '</td>';
			quotation_reminder_row_html += '<td>' + value_reminder_category + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.reminder_onlydate + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.reminder_onlytime + '</td>';
			quotation_reminder_row_html += '<td>' + value.reminder_message + '</td>';
			quotation_reminder_row_html += '<td>' + value_assigned_to_names + '</td>';
			new_quotation_reminder_html += quotation_reminder_row_html;
		});
		$('tbody#quotation_followup_history_list').html(new_quotation_reminder_html);
	}
    
    function display_enquiry_followup_history_html(enquiry_followup_history_objectdata){
		var new_followup_history_html = '';
//		console.log(enquiry_followup_history_objectdata);
		$.each(enquiry_followup_history_objectdata, function (index, value) {
            var value_item_name;
			$.ajax({
				url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + value.followup_by,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_item_name = response.text;
				},
			});
            var followup_history_row_html = '<tr class="followup_history_index_' + index + '">';
			followup_history_row_html += '<td>' + value.followup_history_onlydate + '</td>';
			followup_history_row_html += '<td>' + value.followup_history_onlytime + '</td>';
			followup_history_row_html += '<td>' + value.followup_history_history + '</td>';
			followup_history_row_html += '<td>' + value_item_name + '</td>';
            new_followup_history_html += followup_history_row_html;
		});
		$('tbody#enquiry_followup_history_list').html(new_followup_history_html);
	}

    function party_details(id) {
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/' + id,
            async: false,
            data: 'party_id='+id,
            success: function(data) {
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                } else {
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $(".party_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"), '<?=base_url()?>app/set_city_select2_val_by_id/' + json['city_id']);
                } else {
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"), '<?=base_url()?>app/set_state_select2_val_by_id/' + json['state_id']);
                } else {
                    setSelect2Value($("#state"));
                }
                setSelect2Value($("#port_of_loading_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/<?=DEFAULT_PORT_OF_LOADING_COUNTRY_ID;?>');
                if (json['country_id']) {
                    setSelect2Value($("#country"), '<?=base_url()?>app/set_country_select2_val_by_id/' + json['country_id']);
                    setSelect2Value($("#port_of_discharge_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                    setSelect2Value($("#place_of_delivery_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                } else {
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                } else {
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                } else {
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                } else {
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                } else {
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                } else {
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"), '<?=base_url()?>app/set_reference_select2_val_by_id/' + json['reference_id']);
                } else {
                    setSelect2Value($("#reference_id"));
                }
                if (json['reference_description']) {
                    $("#reference_description").html(json['reference_description']);
                } else {
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"), '<?=base_url()?>app/set_sales_select2_val_by_id/' + json['party_type_1_id']);
                    if (json['party_type_1_id'] == '<?=PARTY_TYPE_DOMESTIC_ID;?>') {
                        setSelect2Value($("#currency_id"), '<?=base_url()?>app/set_currency_select2_val_by_id/' + <?=INR_CURRENCY_ID; ?>);
                                        }
                                        if (json['party_type_1_id'] == '<?=PARTY_TYPE_EXPORT_ID;?>') {
                            setSelect2Value($("#currency_id"), '<?=base_url()?>app/set_currency_select2_val_by_id/' + <?=USD_CURRENCY_ID; ?>);
                                            }
                                            var currency_title = $('#select2-currency_id-container').attr('title');
                            $('#received_payment_currency').html(currency_title);
                        } else {
                            setSelect2Value($("#sales_id"));
                        }

                        <?php if(isset($sales_order_data->id) && !empty($sales_order_data->id)){ } else { ?>
                        if (json['branch_id']) {
                            setSelect2Value($("#branch_id"), "<?=base_url('app/set_branch_select2_val_by_id')?>/" + json['branch_id']);
                        } else {
                            setSelect2Value($("#branch_id"));
                        }

                        if (json['agent_id']) {
                            setSelect2Value($("#agent_id"), '<?=base_url()?>app/set_agent_select2_val_by_id/' + json['agent_id']);
                        } else {
                            setSelect2Value($("#agent_id"));
                        }
                        <?php } ?>

                        if (first_time_edit_mode == 1) {
                            if (json['contact_persons_array']) {
                                var option_html = '';
                                if (json['contact_persons_array'].length > 0) {
                                    //                            console.log(json['contact_persons_array']);
                                    $.each(json['contact_persons_array'], function(index, value) {
                                        option_html += "<option value='" + value.contact_person_id + "'>" + value.name + "</option>";
                                    })
                                    $('select[name="sales_order_data[kind_attn_id]"]').html(option_html).select2();
                                } else {
                                    $('select[name="sales_order_data[kind_attn_id]"]').html('');
                                }
                                $('select[name="sales_order_data[kind_attn_id]"]').change();
                            }
                        } else {
                            first_time_edit_mode = 1;
                        }
                        $("#ajax-loader").hide();
                    }
                });
            }

    function item_details(id) {
        if (edit_lineitem_inc == 0) {
            if (id != '') {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>sales/ajax_load_item_details/' + id,
                    data: id = 'item_id',
                    success: function(data) {
//                        console.log(data);
                        var json = $.parseJSON(data);
                        if (json['item_name']) {
                            $(".item_name").val(json['item_name']);
                        } else {
                               $(".item_name").val("");
                        }

                        if (json['item_code']) {
                            $(".item_code").val(json['item_code']);
                        } else {
                            $(".itm_code").val('');
                        }

                        if (json['item_rate_in']) {
                            var currency_id = $("#currency_id").val();
                            var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                            $(".item_rate").val(item_rate_in_json[currency_id]);
                        } else {
                            $(".item_rate").val('');
                        }

                        var state_id = $('#state').val();
                        input_value_get_amount();
                    }
                });
            }
        }
    }

    <?php if (isset($_GET['view'])){ ?>
    $(window).load(function() {
        display_as_a_viewpage();
    });
    <?php } ?>
</script>