<?php
$segment1 = $this->uri->segment(1);
$segment2 = $this->uri->segment(2);
$segment3 = $this->uri->segment(3);
$segment4 = $this->uri->segment(4);
?>
<?php $this->load->view('success_false_notify'); ?>
<?php
    $quotation_allow_edit_after_sales_order = 1;
    if(isset($quotation_data->quotation_status_id) && $quotation_data->quotation_status_id == QUOTATION_STATUS_ID_ON_ORDER_CREATE) {
        if($this->applib->have_access_role(QUOTATION_ALLOW_EDIT_AFTER_SALES_ORDER,"allow")) {
            $quotation_allow_edit_after_sales_order = 1;
        } else {
            $quotation_allow_edit_after_sales_order = 0;
        }
    } else {
        $quotation_allow_edit_after_sales_order = 1;
    }
?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('quotation/save_quotation') ?>" method="post" id="save_quotation" novalidate>
        <?php if(isset($quotation_data->id) && !empty($quotation_data->id)){ ?>
            <input type="hidden" name="quotation_data[quotation_id]" id="quotation_id" value="<?=$quotation_data->id?>">
			<?php
			if($segment4 == 'revision'){
			?>
			<input type="hidden" name="quotation_data[revised_from_id]" id="revised_from_id" value="<?=$quotation_data->id?>">
			<?php
			}
             } ?>
        <input type="hidden" name="quotation_data[enquiry_id]" id="enquiry_id" value="<?=(isset($quotation_data->enquiry_id))? $quotation_data->enquiry_id : ''; ?>" />
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Sales : Quotation</small>
                <?php
                    $quotation_view_role= $this->applib->have_access_role(QUOTATION_MODULE_ID,"view");
                    $quotation_add_role= $this->applib->have_access_role(QUOTATION_MODULE_ID,"add");
                    $quotation_edit_role= $this->applib->have_access_role(QUOTATION_MODULE_ID,"edit");
                ?>
                <?php if($quotation_add_role || $quotation_edit_role): ?>
                	<button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">
					<?php
					if($segment4 == 'revision'):echo "Save Revision";
					else:echo "Save";
					endif;
					?>	
					</button>
                <?php endif;?>
                <?php if($quotation_view_role): ?>
                    <a href="<?= base_url()?>quotation/quotation_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Quotation List</a>
                <?php endif;?>
                <?php if($quotation_add_role): ?>
                    <a href="<?= base_url()?>quotation/add" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Quotation</a>
                <?php endif;?>
                <?php if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view")) { ?>
                    <span class="pull-right" style="margin-right: 20px;">
                        <div class="form-group">
                            <span for="send_whatsapp_sms" class="col-sm-12 input-sm text-green" style="font-size: 18px; line-height: 25px;">
                                <input type="checkbox" name="quotation_data[send_whatsapp_sms]" id="send_whatsapp_sms" class="send_whatsapp_sms" <?=(isset($quotation_data->id) && !empty($quotation_data->id)) ? '' : 'checked=""'; ?>>  &nbsp; Send<img src="<?php echo base_url(); ?>resource/dist/img/whatsapp_icon.png" style="width:25px;" >
                                <input type="text" name="quotation_data[whatsapp_no]" id="whatsapp_no" class="input-sm whatsapp_no" >
                            </span>
                        </div>
                    </span>
                    <span class="pull-right" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="quotation_data[send_sms]" id="send_sms" class="send_sms" <?=(isset($quotation_data->id) && !empty($quotation_data->id)) ? '' : 'checked=""'; ?> >  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                    <span class="text-danger" style="font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; WhatsApp number must be with country code (Without `+` sign) (e.g. 919876543210)</span>
                <?php } ?>
                    <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, Ctrl+S = Save</label></small>
                <div class="clearfix"></div>
            </h1>
        </section>
        <div class="clearfix">
        <?php if($quotation_add_role || $quotation_edit_role): ?>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li <?php if($from_reminder == 'reminder'){ } else { echo 'class="active"'; } ?> ><a href="#tab_1" id="tabs_1" data-toggle="tab">Quotation Details</a></li>
                        <li><a href="#tab_2" id="tabs_2" data-toggle="tab">Item Details</a></li>
                        <li <?php if($from_reminder == 'reminder'){ echo 'class="active"'; } ?> ><a href="#tab_3" data-toggle="tab"  id="tabs_3" class="quotation_reminder_tab">Quotation Reminder</a></li>
                        <?php if(isset($quotation_data->id) && !empty($quotation_data->id)){ ?>
                        <li><a href="#tab_4" data-toggle="tab" id="tabs_4">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane <?php if($from_reminder == 'reminder'){ } else { echo 'active'; } ?>" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Quotation Detail</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="enquiry_no" class="col-sm-4 input-sm">Enquiry No.</label>
                                                    <div class="col-sm-8 dispaly-flex">
                                                        <input type="text" id="enquiry_no" name="quotation_data[enquiry_no]" class="form-control input-sm" value="<?=(isset($quotation_data->enquiry_no))? $quotation_data->enquiry_no : ''; ?>" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="enquiry_date" class="col-sm-4 input-sm">Enquiry Date</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" id="enquiry_date" name="quotation_data[enquiry_date]" class="form-control input-sm disabled" value="<?=(isset($quotation_data->enquiry_date))? $quotation_data->enquiry_date : ''; ?>" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="rev_date" class="col-sm-4 input-sm">Revision</label>
                                                    <div class="col-sm-8 dispaly-flex">
                                                        
														<?php
														if($segment4 == 'revision'){
														?>
														<input type="text" name="quotation_data[rev]" id="rev" class="form-control input-sm " value="<?=(isset($quotation_data->rev))? $quotation_data->rev + 1 : '1'; ?>" readonly="readonly">
														&nbsp; <input type="text" name="quotation_data[rev_date]" class="form-control input-sm " value="<?=date('d-m-Y'); ?>" readonly="readonly">
														<?php
														}else{
														?>
														<input type="text" name="quotation_data[rev]" id="rev" class="form-control input-sm " value="<?=(isset($quotation_data->rev))? $quotation_data->rev : '1'; ?>" readonly="readonly">
														&nbsp; <input type="text" name="quotation_data[rev_date]" class="form-control input-sm " value="<?=(isset($quotation_data->rev_date))? date('d-m-Y', strtotime($quotation_data->rev_date)) : date('d-m-Y'); ?>" readonly="readonly">
														<?php	
														}
														?>
                                                        &nbsp; <?php /*<button type="button" class="btn btn-info btn-xs btn-generate-rev">Gen. Rev</button>*/ ?>
                                                        <input type="hidden" id="generate_revision" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="quotation_no" style="" class="col-sm-4 input-sm">Quotation No.</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" id="quotation_no" name="quotation_data[quotation_no]" class="form-control input-sm" value="<?=(isset($quotation_data->quotation_no))? $quotation_data->quotation_no : ''; ?>" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="quotation_date" class="col-sm-4 input-sm">Quotation Date</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="quotation_data[quotation_date]" id="quotation_date" class="form-control input-sm pull-right disabled" value="<?=(isset($quotation_data->quotation_date))? date('d-m-Y', strtotime($quotation_data->quotation_date)) : date('d-m-Y'); ?>" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="quotation_type_id" class="col-sm-4 input-sm">Qtn Type</label>
                                                    <div class="col-sm-8">
                                                        <select name="quotation_data[quotation_type_id]" id="quotation_type_id" class="form-control input-sm quotation_type"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="quotation_status_id" class="col-sm-4 input-sm">Status</label>
                                                    <div class="col-sm-8 dispaly-flex">
                                                        <?php if($quotation_allow_edit_after_sales_order == 1){ ?>
                                                            <select name="quotation_data[quotation_status_id]" class="form-control input-sm" id="quotation_status_id"></select>
                                                        <?php } else { ?>
                                                            <select name="quotation_data[quotation_status_id]" class="form-control input-sm" id="quotation_status_id" disabled ></select>
                                                            <input type="hidden" name="quotation_data[quotation_status_id]" value="<?php echo $quotation_data->quotation_status_id; ?>" >
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="quotation_stage_id" class="col-sm-4 input-sm">Quotation Stage</label>
                                                    <div class="col-sm-8">
                                                        <select name="quotation_data[quotation_stage_id]" id="quotation_stage_id"  class="form-control quotation_stage input-sm"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="action" class="col-sm-4 input-sm">Action</label>
                                                    <div class="col-sm-8">
                                                        <select name="quotation_data[action]" id="action" class="form-control input-sm select2" >
                                                            <option value="no_action" <?=(isset($quotation_data->action) && $quotation_data->action == 'no_action')? ' Selected ' : ''; ?> >No Action</option>
                                                            <option value="sent" <?=(isset($quotation_data->action) && $quotation_data->action == 'sent')? ' Selected ' : ''; ?> >Sent</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="sales_id" class="col-sm-4 input-sm">Sales</label>
                                                    <div class="col-sm-8">
                                                        <select name="quotation_data[sales_id]" id="sales_id" class="form-control input-sm select2 sales_id" disabled="disabled"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-4 input-sm">Sales Type</label>
                                                    <div class="col-sm-8">
                                                        <select name="quotation_data[sales_type_id]" id="sales_type_id" class="form-control input-sm sales_type"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="reason" class="col-sm-4 input-sm">Reason</label>
                                                    <div class="col-sm-8 dispaly-flex">
                                                        <select name="quotation_data[reason]" id="reason" class="form-control input-sm quotation_type"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-4 input-sm">Reason Remark</label>
                                                    <div class="col-sm-8 dispaly-flex">
                                                        <textarea name="quotation_data[reason_remark]" id="reason_remark" class="form-control" rows="1" ><?=(isset($quotation_data->reason_remark))? $quotation_data->reason_remark : ''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Enquiry Received By</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
                                                        <select name="quotation_data[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" placeholder="" readonly>
													</div>
												</div>
												<div class="clearfix"></div>
												<input type="hidden" name="party[party_name]" id="party_name" class="form-control input-sm party_name" placeholder="">
												<input type="hidden" name="party[party_id]" id="party_party_id" class="form-control input-sm party_party_id" placeholder="">
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="form-group">
													<label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
													<div class="col-sm-9">
														<textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
														<textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="city" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="state" class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="country" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
														<small>Add multiple email id by Comma separated.</small>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9">
														<textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
														<small>Add multiple phone number by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_website" class="col-sm-3 input-sm">Website</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm website" id="party_website" name="party[website]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="agent_id" class="col-sm-3 input-sm">Agent</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[agent_id]" id="agent_id" class="form-control input-sm" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="quotation_data[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
															<option value="">--Select--</option>
                                                            <?php if(isset($quotation_data->id) && !empty($quotation_data->id)){ ?>
                                                                <?php foreach($party_contact_person as $pcp_key => $pcp_value): ?>
                                                                <option value="<?= $pcp_value->contact_person_id; ?>"  <?=$quotation_data->kind_attn_id == $pcp_value->contact_person_id ?'selected="selected"':'';?>><?= $pcp_value->name; ?></option>
                                                                <?php endforeach; ?>
                                                            <?php } ?>
                                                        </select>
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Designation</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_designation"><?php echo isset($contact_person->designation) ? $contact_person->designation : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Department</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_department"><?php echo isset($contact_person->department) ? $contact_person->department : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person->mobile_no) ? $contact_person->mobile_no : '' ;?>" disabled="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person->email) ? $contact_person->email : '' ;?>" disabled="">
                                                    </div>
                                                </div>
											</div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($quotation_lineitems)){ ?>
                                            <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="item_id" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id" onchange="item_details(this.value)"></select>
                                                </div>                                                       
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_code]" id="item_code" class="form-control input-sm item_code disabled" data-name="item_code" readonly="readonly">
                                                    <input type="hidden" class="item_name" id="item_name" name="line_items_data[item_name]" data-name="item_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_description]" id="item_description" class="form-control input-sm item_description" data-name="item_description" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity num_only" value="1" data-name="quantity" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-3 input-sm">Rate<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <select name="line_items_data[currency_id]" id="currency_id" class="form-control input-sm currency_id" ></select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate"  value="0" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="disc_per" class="col-sm-3 input-sm">Disc(%)</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[disc_per]" id="disc_per" class="form-control input-sm disc_per num_only" data-input_name="disc_per" placeholder="">
                                                </div>
                                                <label for="disc_value" class="col-sm-2 input-sm">Disc(Value)</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[disc_value]" id="disc_value" class="form-control input-sm disc_value num_only" data-input_name="disc_value" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                </div>
                                                <label for="net_amount" class="col-sm-2 input-sm">Net Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[net_amount]" id="net_amount" class="form-control input-sm net_amount disabled" data-input_name="net_amount" placeholder="" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-sm-12"><div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" />
                                            </div>
                                        </div></div>
                                        <div class="row print_section"></div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <table style="" class="table custom-table item-table">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Amount</th>
                                                <th class="text-right">Disc(value)</th>
                                                <th class="text-right">Net Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lineitem_list"></tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th class="text-right"><span class="qty_total"></span><input type="hidden" name="qty_total" id="qty_total" /></th>
                                                <th class="text-right"></th>
                                                <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                                <th class="text-right" class="text-right"><span class="disc_value_total"></span><input type="hidden" name="disc_value_total" id="disc_value_total" /></th>
                                                <th class="text-right" class="text-right"><span class="net_amount_total"></span><input type="hidden" name="net_amount_total" id="net_amount_total" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane <?php if($from_reminder == 'reminder'){ echo 'active'; } ?>" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Quotation Reminder </legend>
                                        <input type="hidden" name="quotation_reminder_index" id="quotation_reminder_index" />
                                        <input type="hidden" name="quotation_reminder_data[followup_by]" id="followup_by" class="followup_by" value="<?=$this->session->userdata('is_logged_in')['staff_id'];?>" />
                                        <input type="hidden" name="quotation_reminder_data[id]" id="id" class="id" value="" />
                                        <input type="hidden" name="quotation_reminder_data[reminder_action]" id="reminder_action" class="reminder_action" value="add" />
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="" class="col-md-2 input-sm">&nbsp;</label>
                                                <input type="button" id="add_quotation_reminder" class="btn btn-info btn-xs add_quotation_reminder pull-right" value="New Quota. Reminder" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="followup_category_id" class="col-md-2 input-sm">Followup Categories<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-md-3">
                                                    <select name="quotation_reminder_data[followup_category_id]" id="followup_category_id" class="form-control input-sm" style="width: 100%;"></select>
                                                </div>
                                                <div class="col-md-7">
                                                    <label for="datepicker1" class="input-sm pull-left">Date<span class="required-sign">&nbsp;*</span></label>
                                                    <input type="hidden" name="quotation_reminder_data[followup_date]" id="followup_date" class="form-control input-sm pull-left" value="<?= date('d-m-Y H:i:s'); ?>" readonly="">
                                                    <input type="text" name="quotation_reminder_data[followup_onlydate]" id="datepicker1" class="form-control input-sm pull-left" value="<?= date('d-m-Y'); ?>" style="width:100px;">
                                                    <div class="pull-left col-md-7" style="width:100px;">
                                                        <div class="bootstrap-timepicker" style="position:initial;">
                                                            <input type="text" name="quotation_reminder_data[followup_onlytime]" id="followup_onlytime" class="form-control input-sm pull-left timepicker" value="<?= date('H:i:s'); ?>" style="width:100px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="history" class="col-md-2 input-sm">Followup History<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-md-10">
                                                    <textarea name="quotation_reminder_data[history]" id="history" class="form-control" rows="2" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                        <div class="form-group" style="padding-top: 5px;">
                                            <div class="col-md-12">
                                                <label for="reminder_category_id" class="col-md-2 input-sm">Reminder Categories<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-md-3">
                                                    <select name="quotation_reminder_data[reminder_category_id]" id="reminder_category_id" class="form-control input-sm" style="width: 100%;"></select>
                                                </div>
                                                <div class="col-md-7">
                                                    <label for="datepicker2" class="input-sm pull-left">Next Reminder Date</label>
                                                    <input type="text" name="quotation_reminder_data[reminder_onlydate]" id="datepicker2" class="form-control input-sm pull-left" value="<?=date('d-m-Y'); ?>" style="width:100px;" >
                                                    <div class="pull-left col-md-7" style="width:100px;">
                                                        <div class="bootstrap-timepicker" style="position:initial;">
                                                            <input type="text" name="quotation_reminder_data[reminder_onlytime]" id="reminder_onlytime" class="form-control input-sm pull-left timepicker" value="<?= date('H:i:s'); ?>" style="width:100px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="reminder_message" class="col-md-2 input-sm">Reminder Message</label>
                                                <div class="col-md-10">
                                                    <textarea name="quotation_reminder_data[reminder_message]" id="reminder_message" class="form-control" rows="2" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-md-2 input-sm">Assigned To</label>
                                                <div class="col-md-10">
                                                    <?php if(isset($users) && count($users) > 0):?>
                                                    <?php foreach($users as $row):?>
                                                    <label style="width: 200px;"><input type="checkbox" name="quotation_reminder_data[assigned_to][]" class="assigned_to" value="<?php echo $row->staff_id;?>">&nbsp;&nbsp;&nbsp;&nbsp;<?=$row->name;?></label>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="" class="col-md-2 input-sm">&nbsp;</label>
                                                <input type="button" id="save_quotation_reminder" class="btn btn-info btn-xs save_quotation_reminder pull-right" value="Save Quotation Reminder" <?=$this->app_model->have_access_role(QUOTATION_FOLLOWUP, "add") ? '' : 'disabled'?>/>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Quotation Reminder List</legend>
                                        <table style="" class="table custom-table item-table">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Created By</th>
                                                    <th>Followup Cat.</th>
                                                    <th>History</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Reminder Cat.</th>
                                                    <th>Next Remi. Date</th>
                                                    <th>Time</th>
                                                    <th>Reminder Message</th>
                                                    <th width="130px">Assigned To</th>
                                                </tr>
                                            </thead>
                                            <tbody id="quotation_reminder_list"></tbody>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <?php if(isset($quotation_data->id) && !empty($quotation_data->id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=$quotation_data->created_by_name;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=$quotation_data->created_at;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=$quotation_data->updated_by_name;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=$quotation_data->updated_at;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        <?php endif;?>
        </div>
        <section class="content-header">
            <h1>
                <?php
                    $quotation_view_role= $this->applib->have_access_role(QUOTATION_MODULE_ID,"view");
                    $quotation_add_role= $this->applib->have_access_role(QUOTATION_MODULE_ID,"add");
                ?>
                 <?php if($quotation_add_role || $quotation_edit_role): ?>
				<button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">
					<?php
					if($segment4 == 'revision'):echo "Save Revision";
					else:echo "Save";
					endif;
					?>	
				</button>
                <?php endif;?>
                <?php if($quotation_view_role): ?>
                    <a href="<?= base_url()?>quotation/quotation_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Quotation List</a>
                <?php endif;?>
                <?php if($quotation_add_role): ?>
                    <a href="<?= base_url()?>quotation/add" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Quotation</a>
                <?php endif;?>
            </h1>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if(isset($quotation_data->id) && !empty($quotation_data->id)){ } else { ?>
<div class="modal fade" id="select_enquiry_modal" tabindex="-1" role="dialog" aria-labelledby="select_enquiry_label" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h4 class="modal-title">Select Enquiry
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="enquiry_datatable" class="table custom-table table-striped">
                        <thead>
                            <tr>
                                <th>Enquiry No.</th>
                                <th>Enquiry Date</th>
                                <th>Party Code</th>
                                <th>Party Name</th>
                                <th>Enquiry Status</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Enquiry No.</th>
                                <th>Enquiry Date</th>
                                <th>Party Code</th>
                                <th>Party Name</th>
                                <th>Enquiry Status</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript">
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
	var lineitem_objectdata = [];
    <?php if(isset($quotation_lineitems)){ ?>
		var li_lineitem_objectdata = [<?php echo $quotation_lineitems; ?>];
		var lineitem_objectdata = [];
        if(li_lineitem_objectdata != ''){
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
	<?php } ?>
    display_lineitem_html(lineitem_objectdata);
    
    var edit_quotation_reminder_inc = 0;
	var quotation_reminder_objectdata = [];
    <?php if(isset($quotation_reminder_data)){ ?>
		var li_quotation_reminder_objectdata = [<?php echo $quotation_reminder_data; ?>];
		var quotation_reminder_objectdata = [];
        if(li_quotation_reminder_objectdata != ''){
            $.each(li_quotation_reminder_objectdata, function (index, value) {
                quotation_reminder_objectdata.push(value);
            });
        }
	<?php } ?>
    display_quotation_reminder_html(quotation_reminder_objectdata);
    <?php if(isset($quotation_data->id) && !empty($quotation_data->id)){ ?>
        first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function(){
        $('.select2').select2();
        $('input[type="checkbox"].send_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        $('input[type="checkbox"].send_whatsapp_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        initAjaxSelect2($("#quotation_type_id"),"<?=base_url('app/quotation_type_select2_source')?>");
        <?php if(isset($quotation_data->quotation_type_id)){ ?>
			setSelect2Value($("#quotation_type_id"),"<?=base_url('app/set_quotation_type_select2_val_by_id/'.$quotation_data->quotation_type_id)?>");
        <?php } else { ?>
            setSelect2Value($("#quotation_type_id"),"<?=base_url('app/set_quotation_type_select2_val_by_id/'.DEFAULT_QUOTATION_TYPE_ID)?>");
        <?php } ?>
        initAjaxSelect2($("#sales_type_id"),"<?=base_url('app/sales_type_select2_source')?>");
        initAjaxSelect2($("#sales_id"),"<?=base_url('app/sales_select2_source')?>");
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source')?>");
        initAjaxSelect2($("#agent_id"),"<?=base_url('app/agent_select2_source')?>");
        initAjaxSelect2($("#quotation_stage_id"),"<?=base_url('app/quotation_stage_select2_source')?>");
        initAjaxSelect2($("#reason"),"<?=base_url('app/quotation_reason_select2_source')?>");
        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
//        initAjaxSelect2($("#item_id"),"<?=base_url('app/item_select2_source')?>");
        initAjaxSelect2($("#reference_id"),"<?=base_url('app/reference_select2_source')?>");
        initAjaxSelect2($("#quotation_status_id"),"<?=base_url('app/quotation_status_select2_source')?>");
        initAjaxSelect2($("#reminder_category_id"),"<?=base_url('app/followup_category_select2_source')?>");
        initAjaxSelect2($("#followup_category_id"),"<?=base_url('app/followup_category_select2_source')?>");
        <?php if(isset($quotation_data->quotation_status_id)){ ?>
			setSelect2Value($("#quotation_status_id"),"<?=base_url('app/set_quotation_status_select2_val_by_id/'.$quotation_data->quotation_status_id)?>");
        <?php } else { ?>
            setSelect2Value($("#quotation_status_id"),"<?=base_url('app/set_quotation_status_select2_val_by_id/'.QUOTATION_DEFAULT_STATUS_ID)?>");
        <?php } ?>
        <?php if(isset($quotation_data->quotation_stage_id)){ ?>
			setSelect2Value($("#quotation_stage_id"),"<?=base_url('app/set_quotation_stage_select2_val_by_id/'.$quotation_data->quotation_stage_id)?>");
        <?php } else { ?>
            setSelect2Value($("#quotation_stage_id"),"<?=base_url('app/set_quotation_stage_select2_val_by_id/'.QUOTATION_DEFAULT_STAGE_ID)?>");
        <?php } ?>
        
        <?php if(isset($quotation_data->party_id)){ ?>
			setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id/'.$quotation_data->party_id)?>");
            party_details(<?=$quotation_data->party_id; ?>);
		<?php } ?>
        
        var table
        table = $('#enquiry_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[0,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('quotation/enquiry_datatable_for_quptatoin_create')?>",
                "type": "POST"
            },
            "scrollY": 400,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columns": [
                null,
                null,
                null,
                null,
                null,
            ]
        });
        
        $('#select_enquiry_modal').modal({backdrop: 'static', keyboard: false});
        $('#select_enquiry_modal').modal('show');
        $('#select_enquiry_modal').on('shown.bs.modal',function(){
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
		        
        $(document).on('click', '.enquiry_row', function () {
            var enquiry_id = $(this).data('enquiry_id');
            $("#enquiry_id").val(enquiry_id);
            feedInquiryData(enquiry_id);
            $('#select_enquiry_modal').modal('hide');
            $('#quotation_type_id').select2('open');
        });
    
        $(document).on('input','#item_rate',function () {
            if(this.value > 9900000){
                this.value = 9900000;
            }
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if(target == '#tab_1'){
                $('#quotation_type_id').select2('open');
                $('#item_id').select2('close');
                $('#followup_category_id').select2('close');
            } else if(target == '#tab_2'){
                $('#item_id').select2('open');
                $('#quotation_type_id').select2('close');
                $('#followup_category_id').select2('close');
            } else if(target == '#tab_3') {
                $('#followup_category_id').select2('open');
                $('#quotation_type_id').select2('close');
                $('#item_id').select2('close');
            } else {
                $('#quotation_type_id').select2('close');
                $('#item_id').select2('close');
                $('#followup_category_id').select2('close');
            }
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_quotation").submit();
                return false;
            }
        });
        
        $(document).on('submit', '#save_quotation', function () {
        
            if($.trim($("#party_id").val()) == ''){
                show_notify('Please Select Party.',false);
                return false;
            }
            if($.trim($("#reference_id").val()) == ''){
                show_notify('Please Select Reference.',false);
                return false;
            }
            
            if($.trim($("#item_id").val()) != ''){
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }
            if(quotation_reminder_objectdata == ''){
				show_notify("Please add at least one follow up history.", false);
				return false;
			}
            if(lineitem_objectdata == ''){
				show_notify("Please select any one Item.", false);
				return false;
			}
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
            
            var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
			postData.append('line_items_data', lineitem_objectdata_stringify);
            quotation_reminder_objectdata.reverse();
			var quotation_reminder_objectdata_stringify = JSON.stringify(quotation_reminder_objectdata);
			postData.append('quotation_reminder_data', quotation_reminder_objectdata_stringify);
			$.ajax({
				url: "<?=base_url('quotation/save_quotation') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
                    if (json['send_whatsapp_sms_url']) {
                        window.open(json['send_whatsapp_sms_url'],"_blank");
                    }
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('quotation/quotation_list') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('quotation/quotation_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });

        $('#add_lineitem').on('click', function() {
			var item_id = $("#item_id").val();
			if(item_id == '' || item_id == null){
				show_notify("Please select Item!", false);
				return false;
			}
			var quantity = $("#quantity").val();
			if(quantity == '' || quantity == null){
				show_notify("Item quantity is required!", false);
				return false;
			}
			var item_rate = $("#item_rate").val();
			if(item_rate == '' || item_rate == null){
				show_notify("Item Rate is required!", false);
				return false;
			}
			
			var key = '';
			var value = '';
			var lineitem = {};
			var item_documents_item_id = [];
			var item_documents_sequence = [];
			var item_documents_detail = [];
            $('select[name^="line_items_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			$('input[name^="line_items_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
            $('input[name^="item_documents_item_id"]').each(function(key, value) {
                key = $(this).attr('name');
                key = key.replace("item_documents_item_id[", "");
				key = key.replace("]", "");
				value = $(this).val();
				item_documents_item_id[key] = value;
			});
            $('input[name^="item_documents_sequence"]').each(function(key, value) {
                key = $(this).attr('name');
                key = key.replace("item_documents_sequence[", "");
				key = key.replace("]", "");
				value = $(this).val();
				item_documents_sequence[key] = value;
			});
            var detail_inc = 0;
            $('textarea[name^="item_documents_detail"]').each(function(key, value) {
//                key = $(this).attr('name');
                key = $(this).attr('name');
                key = key.replace("item_documents_detail[", "");
				key = key.replace("]", "");
                var detail_id = 'item_document_'+key;
                value = CKEDITOR.instances[detail_id].getData();
                item_documents_detail[key] = value;
                detail_inc++;
			});
            var item_documents = [];
            $(item_documents_item_id).each(function(key, value) {
                if(item_documents_sequence[key] != ''){
                    item_documents[key] = {'item_id': item_documents_item_id[key], 'sequence': item_documents_sequence[key], 'detail': item_documents_detail[key]};
                }
            });
            lineitem['item_documents'] = item_documents;
            
            var new_lineitem = JSON.parse(JSON.stringify(lineitem));
			var line_items_index = $("#line_items_index").val();
			if(line_items_index != ''){
				lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
			} else {
				lineitem_objectdata.push(new_lineitem);
			}
            console.log(lineitem_objectdata);
			display_lineitem_html(lineitem_objectdata);
			$('#lineitem_id').val('');
            $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
			$("#item_id").val(null).trigger("change");
			$("#quantity").val('1');
			$("#disc_per").val('0');
            $('.print_section').html('');
			$("#line_items_index").val('');
            if(on_save_add_edit_item == 1){
                on_save_add_edit_item == 0;
                $('#save_quotation').submit();
            }
            var sales_id = $("#sales_id").val();
            if(sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
            }
            if(sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
            }
		});
        
        $('#save_quotation_reminder').on('click', function() {
			var followup_category_id = $("#followup_category_id").val();
			if(followup_category_id == '' || followup_category_id == null){
				show_notify("Please Select Followup Category!", false);
				return false;
			}
            var followup_date = $("#datepicker1").val();
			if(followup_date == '' || followup_date == null){
				show_notify("Please Enter Followup Date!", false);
				return false;
			}
			var history = $("#history").val();
			if(history == '' || history == null){
				show_notify("Please Enter Followup History!", false);
				return false;
			}
            var reminder_category_id = $("#reminder_category_id").val();
			if(reminder_category_id == '' || reminder_category_id == null){
				show_notify("Please Select Followup Category!", false);
				return false;
			}
			var key = '';
			var value = '';
			var quotation_reminder = {};
			$('select[name^="quotation_reminder_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("quotation_reminder_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				quotation_reminder[key] = value;
			});
			$('input[name^="quotation_reminder_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("quotation_reminder_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				quotation_reminder[key] = value;
			});
            quotation_reminder['history'] = $('#history').val();
            quotation_reminder['reminder_message'] = $('#reminder_message').val();
            quotation_reminder['followup_by'] = $('#followup_by').val();
            quotation_reminder['followup_at'] = $('#followup_at').val();
            quotation_reminder['updated_by'] = '<?=$staff_id;?>';
            quotation_reminder['updated_at'] = '<?=date('d-m-Y H:i:s');?>';
            quotation_reminder['reminder_action'] = $('#reminder_action').val();
            quotation_reminder['id'] = $('#id').val();
			
            var i = 0;
            quotation_reminder['assigned_to'] = '';
            $('.assigned_to:checked').each(function () {
                if(i == 0){
                    quotation_reminder['assigned_to'] += $(this).val();
                } else {
                    quotation_reminder['assigned_to'] += ',' + $(this).val();
                }
                i++;
            });
            var new_quotation_reminder = JSON.parse(JSON.stringify(quotation_reminder));
			var quotation_reminder_index = $("#quotation_reminder_index").val();
			if(quotation_reminder_index != ''){
				quotation_reminder_objectdata.splice(quotation_reminder_index, 1, new_quotation_reminder);
                quotation_reminder_objectdata.reverse();								
			} else {
				quotation_reminder_objectdata.reverse();
				quotation_reminder_objectdata.push(new_quotation_reminder);
			}
			display_quotation_reminder_html(quotation_reminder_objectdata);
			$("#followup_category_id").val(null).trigger("change");
            $("#followup_date").val('<?php echo date('d-m-Y H:i:s'); ?>');
            $("#datepicker1").val('');
            $("#datepicker1").val('<?php echo date('d-m-Y'); ?>');
            $("#followup_onlytime").val('');
            $("#followup_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#history").val('');
            $("#reminder_category_id").val(null).trigger("change");
            $("#datepicker2").val('');
            $("#datepicker2").val('<?php echo date('d-m-Y'); ?>');
            $("#reminder_onlytime").val('');
            $("#reminder_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#reminder_message").val('');
            $("#reminder_action").val('add');
            $("#id").val('');
            $("#followup_by").val('<?=$staff_id?>');
            $("#followup_at").val('<?=date('d-m-Y H:i:s');?>');
            $('.assigned_to').prop("checked", false);
            $("#quotation_reminder_index").val('');
			if(edit_quotation_reminder_inc == 1 && <?=$this->app_model->have_access_role(QUOTATION_FOLLOWUP, "add")?> != 1){
				$('#save_quotation_reminder').attr("disabled", "disabled");
			}
        });
					
        $('#add_quotation_reminder').on('click', function() {
			$("#followup_category_id").val(null).trigger("change");
            $("#followup_date").val('<?php echo date('d-m-Y H:i:s'); ?>');
            $("#datepicker1").val('');
            $("#datepicker1").val('<?php echo date('d-m-Y'); ?>');
            $("#followup_onlytime").val('');
            $("#followup_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#history").val('');
            $("#reminder_category_id").val(null).trigger("change");
            $("#datepicker2").val('');
            $("#datepicker2").val('<?php echo date('d-m-Y'); ?>');
            $("#reminder_onlytime").val('');
            $("#reminder_onlytime").val('<?php echo date('H:i:s'); ?>');
            $("#reminder_message").val('');
            $("#reminder_action").val('add');
            $("#id").val('');
            $("#followup_by").val('<?=$staff_id?>');
            $("#followup_at").val('<?= date('d-m-Y H:i:s'); ?>');
            $('.assigned_to').prop("checked", false);
            $("#quotation_reminder_index").val('');
        });
		
		$(document).on('input', '#quantity,#item_rate', function () {
            setAmount();
        });
        
        $(document).on('input', '#disc_per', function () {
            setDiscAmount();
        });

        $(document).on('input', '#disc_value', function () {
            setDiscPer();
        });
        
        $(document).on("change",'#contact_person_id',function() {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?=base_url();?>party/get-contact-person-by-id/"+contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });
        
        $(document).on("change",'#currency_id',function(){
            if(edit_lineitem_inc == 0){
                var currency_id = $("#currency_id").val();
                var item_id = $("#item_id").val();
                if(item_id != '' && item_id != null) {
                    $.ajax({
                        type: "POST",
                        url: '<?=base_url();?>sales/ajax_load_item_details/' + item_id,
                        data: id = 'item_id',
                        success: function (data) {
                            var json = $.parseJSON(data);
                            if (json['item_rate_in']) {
                                var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                                $("#item_rate").val(item_rate_in_json[currency_id]);
                            } else {
                                $("#item_rate").val('');
                            }
                            setAmount();
                        }
                    });
                }
            } else { edit_lineitem_inc = 0; }
        });
    });
                
    function feedInquiryData(enquiry_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>quotation/get_inquiry',
            type: "POST",
            async: false,
            data: {id: enquiry_id},
            success: function(data){
                json = JSON.parse(data);
                var inquiry_data = json.inquiry_data;
                $("#enquiry_no").val(inquiry_data.inquiry_no);
                $("#enquiry_date").val(inquiry_data.inquiry_date);
                setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id')?>/"+inquiry_data.party_id);
                $('#party_name').val($('#party_id').val());
                $('#party_party_id').val(inquiry_data.party_id);
                setSelect2Value($("#sales_type_id"),"<?=base_url('app/set_sales_type_select2_val_by_id')?>/"+inquiry_data.sales_type_id);
                party_details($('#party_id').val());
                $('select[name="quotation_data[kind_attn_id]"]').val(inquiry_data.kind_attn_id).change();
                if(json.inquiry_lineitems != ''){
                    lineitem_objectdata = json.inquiry_lineitems;
                    display_lineitem_html(lineitem_objectdata);
                }
                var party_type_id = $("#sales_id").val();
                $('#item_id').select2();
                $('#item_id').select2('destroy');
                $('#item_id').find('option').remove().end();
                $('#item_id').select2();
                if(party_type_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_DOMESTIC_ID; ?>);
                } else if (party_type_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_EXPORT_ID; ?>);
                }
                $("#ajax-loader").hide();
            },
        });
    }
    
    function display_lineitem_html(lineitem_objectdata){
		var new_lineitem_html = '';
		var qty_total = 0;
		var amount_total = 0;
		var disc_value_total = 0;
		var net_amount_total = 0;
//		console.log(lineitem_objectdata);
		$.each(lineitem_objectdata, function (index, value) {
            var value_currency;
			$.ajax({
				url: "<?=base_url('app/set_currency_select2_val_by_id/') ?>/" + value.currency_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_currency = response.text;
				},
			});
            
			var lineitem_edit_btn = ''; var lineitem_delete_btn = '';
            <?php if($quotation_allow_edit_after_sales_order == 1){ ?>
                lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
                lineitem_delete_btn = ' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a> ';
            <?php } ?>
			var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
			lineitem_edit_btn + lineitem_delete_btn +
            '</td>' +
			'<td>' + value.item_code + '</td>' +
			'<td>' + value.item_name + '</td>' +
            '<td class="text-right">' + value.quantity + '</td>';
            if(value_currency != ''){
                row_html += '<td class="text-right">' + value_currency + ' - ' + value.item_rate + '</td>';
            } else {
                row_html += '<td class="text-right">' + value.item_rate + '</td>';
            }
            row_html += '<td class="text-right">' + value.amount + '</td>' +
            '<td class="text-right">' + value.disc_value + '</td>' +
            '<td class="text-right">' + value.net_amount + '</td></tr>';
			new_lineitem_html += row_html;
			qty_total += parseInt(value.quantity);
			amount_total += parseInt(value.amount);
			disc_value_total += parseInt(value.disc_value);
			net_amount_total += parseInt(value.net_amount);
		});
		$('tfoot span.qty_total').html(qty_total); $('#qty_total').val(qty_total);
		$('tfoot span.amount_total').html(amount_total); $('#amount_total').val(amount_total);
		$('tfoot span.disc_value_total').html(disc_value_total); $('#disc_value_total').val(disc_value_total);
		$('tfoot span.net_amount_total').html(net_amount_total); $('#net_amount_total').val(net_amount_total);
		$('tbody#lineitem_list').html(new_lineitem_html);
	}
    
    function edit_lineitem(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_lineitem_inc == 0){  edit_lineitem_inc = 1; /*$('.edit_lineitem_'+ index).click();*/ }
		value = lineitem_objectdata[index];
		$("#line_items_index").val(index);
		
//		initAjaxSelect2($("#item_id"),"<?=base_url('app/item_select2_source/')?>");
		setSelect2Value($("#item_id"),"<?=base_url('app/set_li_item_select2_val_by_id/')?>/" + value.item_id);
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source/')?>");
		setSelect2Value($("#currency_id"),"<?=base_url('app/set_currency_select2_val_by_id/')?>/" + value.currency_id);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#lineitem_id").val(value.id);
		}
        $("#item_code").val(value.item_code);
        $("#item_name").val(value.item_name);
        $("#item_description").val(value.item_description);
        $("#item_rate").val(value.item_rate);
        $("#quantity").val(value.quantity);
        $("#amount").val(value.amount);
        $("#disc_per").val(value.disc_per);
        $("#disc_value").val(value.disc_value);
        $("#net_amount").val(value.net_amount);
        
        var detail_page_html = '';
        $(value.item_documents).each(function(i_key, i_value) {
            console.log(i_value);
            detail_page_html += '<div class="col-md-12">';
            detail_page_html += '    <h4>Detail page : <span class="cnt-detail-page">'+i_value.sequence+'</span></h4>';
            detail_page_html += '    <input type="hidden" name="item_documents_item_id['+i_key+']" value="'+i_value.item_id+'">';
            detail_page_html += '    <input type="hidden" name="item_documents_sequence['+i_key+']" value="'+i_value.sequence+'">';
            detail_page_html += '    <textarea name="item_documents_detail['+i_key+']" id="item_document_'+i_key+'" class="form-control item_document">'+i_value.detail+'</textarea>';
            detail_page_html += '</div>';
        });
        $('.print_section').html(detail_page_html);
        $(value.item_documents).each(function(i_key, i_value) {
            CKEDITOR.replace('item_document_'+i_key,{
                extraPlugins: 'lineheight',
                removePlugins:''
            });
        });
        
		$('.overlay').hide();
	}
	
	function remove_lineitem(index){
        if(confirm('Are you sure ?')){
            value = lineitem_objectdata[index];
            if(typeof(value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index,1);
            display_lineitem_html(lineitem_objectdata);
        }
	}
    
    function display_quotation_reminder_html(quotation_reminder_objectdata){
		var new_quotation_reminder_html = '';
		quotation_reminder_objectdata.reverse();
		console.log(quotation_reminder_objectdata);
		$.each(quotation_reminder_objectdata, function (index, value) {
            var value_followup_category;
			$.ajax({
				url: "<?=base_url('app/set_followup_category_select2_val_by_id/') ?>/" + value.followup_category_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_followup_category = response.text;
				},
			});
            var value_reminder_category;
			$.ajax({
				url: "<?=base_url('app/set_followup_category_select2_val_by_id/') ?>/" + value.reminder_category_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_reminder_category = response.text;
				},
			});
            var value_staff_name;
			//alert(value.followup_by);
			$.ajax({
				url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + value.followup_by,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_staff_name = response.text;
				},
			});
            var value_assigned_to_names = '';
            var assigned_to = value.assigned_to;
            if(assigned_to != '' && assigned_to != null){
                var assigned_to_arr = assigned_to.split(',');
                var i = 0;
                $.each(assigned_to_arr, function(i, val){
                    if(i == 0){ 
                    } else {
                        value_assigned_to_names += ', ';
                    }
                    i++;
                    $.ajax({
                        url: "<?=base_url('app/set_staff_select2_val_by_id/') ?>/" + val,
                        type: "POST",
                        dataType: 'json',
                        async: false,
                        cache: false,
                        success: function (response) {
                            if(response.text == '--select--'){ response.text = ''; }
                            value_assigned_to_names += response.text;
                        },
                    });
                });
            }
            var quotation_reminder_edit_btn = '';
            var quotation_reminder_delete_btn = '';
			<?php if($this->app_model->have_access_role(QUOTATION_FOLLOWUP, "edit")): ?>
			quotation_reminder_edit_btn = '<a class="btn btn-xs btn-primary edit_quotation_reminder_' + index + '" href="javascript:void(0);" onclick="edit_quotation_reminder(' + index + ')"><i class="fa fa-edit"></i></a> ';
			<?php endif;
			if($this->app_model->have_access_role(QUOTATION_FOLLOWUP, "delete")): ?>
			quotation_reminder_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_quotation_reminder(' + index + ')"><i class="fa fa-remove"></i></a>';
			<?php endif;?>
			
			var quotation_reminder_row_html = '<tr class="quotation_reminder_index_' + index + '"><td class="">';
			quotation_reminder_row_html += quotation_reminder_edit_btn;
			quotation_reminder_row_html += quotation_reminder_delete_btn;
			quotation_reminder_row_html += '</td>';
            quotation_reminder_row_html += '<td>' + value_staff_name + '</td>';
			quotation_reminder_row_html += '<td>' + value_followup_category + '</td>';
            quotation_reminder_row_html += '<td>' + value.history + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.followup_onlydate + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.followup_onlytime + '</td>';
			quotation_reminder_row_html += '<td>' + value_reminder_category + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.reminder_onlydate + '</td>';
			quotation_reminder_row_html += '<td nowrap>' + value.reminder_onlytime + '</td>';
			quotation_reminder_row_html += '<td>' + value.reminder_message + '</td>';
			quotation_reminder_row_html += '<td>' + value_assigned_to_names + '</td>';
			new_quotation_reminder_html += quotation_reminder_row_html;
		});
		$('tbody#quotation_reminder_list').html(new_quotation_reminder_html);
	}
    
    function edit_quotation_reminder(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_quotation_reminder_inc == 0){
			edit_quotation_reminder_inc = 1; 
			$('.edit_quotation_reminder_'+ index).click();
			$("#save_quotation_reminder").removeAttr("disabled");
		}
		value = quotation_reminder_objectdata[index];
		$("#quotation_reminder_index").val(index);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#quotation_reminder_id").val(value.id);
		}
        initAjaxSelect2($("#followup_category_id"),"<?=base_url('app/followup_category_select2_source/')?>");
		setSelect2Value($("#followup_category_id"),"<?=base_url('app/set_followup_category_select2_val_by_id/')?>/" + value.followup_category_id);
        initAjaxSelect2($("#reminder_category_id"),"<?=base_url('app/followup_category_select2_source/')?>");
		setSelect2Value($("#reminder_category_id"),"<?=base_url('app/set_followup_category_select2_val_by_id/')?>/" + value.reminder_category_id);
        $("#followup_date").val(value.followup_date);
        $("#datepicker1").val(value.followup_onlydate);
        $("#followup_onlytime").val(value.followup_onlytime);
        $("#history").val(value.history);
        $("#datepicker2").val(value.reminder_onlydate);
        $("#reminder_onlytime").val(value.reminder_onlytime);
        $("#reminder_message").val(value.reminder_message);
        $("#reminder_action").val("edit");
        $("#id").val(value.id);
        $("#followup_by").val(value.followup_by);
        $("#followup_at").val(value.followup_at);
        $('.assigned_to').prop("checked", false);
        var assigned_to = value.assigned_to;
        if(assigned_to != ''){
            var assigned_to_arr = assigned_to.split(',');
            $.each(assigned_to_arr, function(i, val){
                $("input[value='" + val + "']").prop('checked', true);
            });
        }
		$('.overlay').hide();
	}
	
	function remove_quotation_reminder(index){
		if(confirm('Are you sure ?')){
            value = quotation_reminder_objectdata[index];
			if(typeof(value.id) != "undefined" && value.id !== null) {
				$('#save_quotation').append('<input type="hidden" name="deleted_quotation_reminder_id[]" id="deleted_quotation_reminder_id" value="' + value.id + '" />');
            }
			quotation_reminder_objectdata.splice(index,1);
            quotation_reminder_objectdata.reverse();
            display_quotation_reminder_html(quotation_reminder_objectdata);
        }
	}
    
    function convertDate(inputFormat) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date(inputFormat);
        return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
    }
    
    function setAmount(){
        var quantity = $("#quantity").val();
        var rate = $("#item_rate").val();
        var amount = quantity * rate;
        $("#amount").val(parseF(amount));
        setDiscAmount();
    }
    
    function setDiscAmount(){
        var discount = $("#disc_per").val();
        var amount = $("#amount").val();
        var discount_amount = (amount * discount) / 100;
        $("#disc_value").val(parseF(discount_amount));
        setTotalAmount();
    }

    function setDiscPer(){
        var discount_amount = $("#disc_value").val();
        var amount = $("#amount").val();
        var discount = (discount_amount * 100) / amount;
        $("#disc_per").val(parseFloat(discount).toFixed(2));
        setTotalAmount();
    }

    function setTotalAmount(){
        var amount = $("#amount").val();
        var disc_value = $("#disc_value").val();
        var total_amount = +amount - +disc_value;
        $("#net_amount").val(parseF(total_amount));
    }

    function parseF(value, decimal) {
        decimal = 0;
        return value ? parseFloat(value).toFixed(decimal) : 0;
    }
                
    function party_details(id){
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/'+id,
            async: false,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                }else{
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $(".party_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"),'<?=base_url()?>app/set_city_select2_val_by_id/'+json['city_id']);
                }else{
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                }else{
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                }else{
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                }else{
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }else{
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                }else{
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                }else{
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                }else{
                    $(".phone_no").val("");
                }
                if (json['whatsapp_no']) {
                    $(".whatsapp_no").val(json['whatsapp_no']);
                }else{
                    $(".whatsapp_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"),'<?=base_url()?>app/set_reference_select2_val_by_id/'+json['reference_id']);
                }else{
                    setSelect2Value($("#reference_id"));
                }
                if(json['reference_description']){
                    $("#reference_description").html(json['reference_description']);
                }else{
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"),'<?=base_url()?>app/set_sales_select2_val_by_id/'+json['party_type_1_id']);
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
                    }
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
                    }
                }else{
                    setSelect2Value($("#sales_id"));
                }

                if (json['party_type_2_id'] != '') { 
                    setSelect2Value($("#sales_type_id"),'<?=base_url()?>app/set_sales_type_select2_val_by_id/'+json['party_type_2_id']);
                }else{
                    setSelect2Value($("#sales_type_id"));
                }

                if (json['agent_id']) {
                    setSelect2Value($("#agent_id"),'<?=base_url()?>app/set_agent_select2_val_by_id/'+json['agent_id']);
                }else{
                    setSelect2Value($("#agent_id"));
                }
                var party_type_id = $("#sales_id").val();
                $('#item_id').select2();
                $('#item_id').select2('destroy');
                $('#item_id').find('option').remove().end();
                $('#item_id').select2();
                if(party_type_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_DOMESTIC_ID; ?>);
                } else if (party_type_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                    initAjaxSelect2($("#item_id"),'<?=base_url()?>app/item_select2_source/'+<?=PARTY_TYPE_EXPORT_ID; ?>);
                }
                if(first_time_edit_mode == 1){
                    if (json['contact_persons_array']) {
                        var option_html = '';
                        if(json['contact_persons_array'].length > 0){
//                            console.log(json['contact_persons_array']);
                            $.each(json['contact_persons_array'],function(index,value){
                                option_html += "<option value='"+value.contact_person_id+"'>"+value.name+"</option>";
                            })
                            $('select[name="quotation_data[kind_attn_id]"]').html(option_html).select2();
                        } else {
                            $('select[name="quotation_data[kind_attn_id]"]').html('');
                        }
                        $('select[name="quotation_data[kind_attn_id]"]').change();
                    }
                } else { first_time_edit_mode = 1; }
                $("#ajax-loader").hide();
            }
        });
    }
    
    function item_details(id){
        if(edit_lineitem_inc == 0){
            if(id != '') {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>sales/ajax_load_item_details/' + id,
                    data: { id: id},
                    success: function (data) {
                        //console.log(data);
                        var json = $.parseJSON(data);
                        if (json['item_name']) {
                            $(".item_name").val(json['item_name']);
                        } else {
                            $(".item_name").val("");
                        }

                        if (json['item_code']) {
                            $(".item_code").val(json['item_code']);
                        } else {
                            $(".itm_code").val('');
                        }
                        
                        if (json['item_rate_in']) {
                            var currency_id = $("#currency_id").val();
                            var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                            $(".item_rate").val(item_rate_in_json[currency_id]);
                        } else {
                            $(".item_rate").val('');
                        }
                        setAmount();
                    }
                });
                var sales_id = $('#sales_id').val();
                get_quotation_document(id, sales_id);
            } else {
                $('.print_section').html('');
            }
        }
    }
    
    function get_quotation_document(id, sales_id){
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>quotation/ajax_load_item_print_details/' + id + '/' + sales_id,
            data: {id: 'item_id'},
            success: function (data) {
                var json = $.parseJSON(data);
                var detail_page_html = '';
                var party_type_id = $('#sales_id').val();
                var d_page_inc = 1;
                $.each(json,function(index,value){
//                    console.log(value);
                    detail_page_html += '<div class="col-md-12">';
                    detail_page_html += '    <h4>Detail page : <span class="cnt-detail-page">'+value.sequence+'</span></h4>';
                    detail_page_html += '    <input type="hidden" name="item_documents_item_id['+index+']" value="'+id+'">';
                    detail_page_html += '    <input type="hidden" name="item_documents_sequence['+index+']" value="'+value.sequence+'">';
                    detail_page_html += '    <textarea name="item_documents_detail['+index+']" id="item_document_'+index+'" class="form-control item_document">'+value.detail+'</textarea>';
                    detail_page_html += '</div>';
                    d_page_inc++;
                });
                
                $('.print_section').html(detail_page_html);
                var d_page_inc = 1;
                $.each(json,function(index,value){
                    loadEditor('item_document_'+index);
                    d_page_inc++;
                });
            }
        });
    }
    function loadEditor(id){
//        var instance = CKEDITOR.instances[id];
//        if(instance){
//            CKEDITOR.destroy(instance);
//        }
        CKEDITOR.replace(id);
    }
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
    <?php } ?>
</script>