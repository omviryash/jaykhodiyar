<style>
	@media print {
		table{
			border-spacing: 0;
		}
		.text-center {
			text-align: center !important;
		}
		.text-right {
			text-align: right !important;
		}
		.text-left {
			text-align: left !important;
		}
		.text-underline {
			text-decoration: underline;
		}
		.section-title{
			font-size: 20px;
			font-weight: 900;
			margin: 15px 10px;
		}
		.no-margin{
			margin: 0;
		}
		table.table-item-detail > thead > tr > th:last-child{
			border-right: solid 1px #000000;
		}
		table.table-item-detail > thead > tr > th:first-child{
			border-left: solid 1px #000000;
		}
		table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
			border-right: solid 1px #000000;
		}
		table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
			border-left: solid 1px #000000;
		}
		table.table-item-detail > thead > tr > th{
			border-top: solid 1px #000000;
			border-bottom: solid 1px #000000;
		}
		table.table-item-detail > tbody > tr > th{
			border-top: solid 1px #000000;
			border-bottom: solid 1px #000000;
		}
	}
	table.table-item-detail > thead > tr > th:last-child{
		border-right: solid 1px #000000;
	}
	table.table-item-detail > thead > tr > th:first-child{
		border-left: solid 1px #000000;
	}
	table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
		border-right: solid 1px #000000;
	}
	table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
		border-left: solid 1px #000000;
	}
	table.table-item-detail > thead > tr > th{
		border-top: solid 1px #000000;
		border-bottom: solid 1px #000000;
	}
	table.table-item-detail > tbody > tr > th{
		border-top: solid 1px #000000;
		border-bottom: solid 1px #000000;
	}
	table{
		border-spacing: 0;
	}
	.text-center {
		text-align: center !important;
	}
	.text-right {
		text-align: right !important;
	}
	.text-left {
		text-align: left !important;
	}
	.text-underline {
		text-decoration: underline;
	}
	.section-title{
		font-size: 20px;
		font-weight: 900;
		margin: 15px 10px;
	}
	.no-margin{
		margin: 0;
	}
	table tbody tr th,table tbody tr td {
		padding-left: 5px;
		padding-right: 5px;
	}
</style>
<div>
	<div class="text-center text-underline" style="font-weight: 900!important;color: #0097d6;">
		<!-- <h4>Quotation for <?=$item_row->item_name;?> (Exports)</h4> -->
	</div>
	<?php
	$CURRENCY = '';
	if($item_row->currency != ''){
		$CURRENCY = strtoupper($item_row->currency);
	}
	?>
	<table border="1" style="font-size: 14px; width: 100%;" class="table-item-detail">
		<thead>
		<tr>
			<th style="width:10px;">Sr.No.</th>
			<th>Description</th>
			<th style="width:150px;">Rate</th>
            <th style="width:100px;">Qty.</th>
			<th style="width:150px;">Amount</th>
			<!--<th style="width:150px;">Amount <?/*=$CURRENCY*/?></th>-->
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class="text-center" style="height: 50px;">1</td>
			<td><?=$item_row->item_name?$item_row->item_name:$item_row->item_description;?></td>
			<td class="text-right"><?=moneyformat($item_row->rate);?></td>
            <td class="text-center"><?=$item_row->quantity;?></td>
			<td class="text-right"><?=moneyformat($item_row->amount);?></td>
		</tr>
		<tr>
			<td class="text-center" style="height: 50px;">2</td>
			<td>Erection & Commissioning Charges</td>
			<td class="text-right">Included</td>
            <td class="text-center">1</td>
            <td class="text-right">Included</td>
		</tr>
		<tr>
			<th colspan="3" class="text-left"><?php if($item_row->amount > 0){?>(In Words: <?=money_to_word($item_row->amount)?> <?=$CURRENCY?> Only)<?php }?></th>
			<th colspan="2" class="text-right">TOTAL <?=$CURRENCY?> <?=moneyformat($item_row->amount);?></th>
		</tr>
		</tbody>
	</table>
</div>
