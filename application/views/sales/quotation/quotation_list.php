<?php
if (!$this->app_model->have_access_role(QUOTATION_MODULE_ID, "view")) {
	$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
	redirect("/");
}
?>
<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<small class="text-primary text-bold">Quotation List</small>
			<?php $quotation_add_role= $this->applib->have_access_role(QUOTATION_MODULE_ID,"add"); ?>
			<?php if($quotation_add_role): ?>
			<a class="pull-right btn btn-info btn-xs" style="margin-right: 5px;margin-top: 5px;margin-bottom: 5px;" href="<?php echo BASE_URL ?>quotation/add">Add Quotation</a>
			<?php endif;?>
		</h1>
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<!-- Start Advance Search -->
					<div class="col-md-12">
						<a href="javascript:void(0)" class="btn btn-info btn-xs filtration_div"> + </a>
						<div class="filtration_fields" id="filtration_fields">
							<div class="row">

								<div class="col-md-6">
									<!-- Horizontal Form -->
									<!-- /.box-header -->
									<!-- form start -->
									<form class="form-horizontal">
										<div class="box-body">
											<div class="form-group" id="filter_global">
												<label for="inputEmail3" class="col-sm-4 control-label">Global search</label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm global_filter" id="global_filter">
												</div>
											</div>
											<div class="form-group" id="filter_col2" data-column="1">
												<label for="inputEmail3" class="col-sm-4 control-label">Quotation No</label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm column_filter" id="col1_filter">
												</div>
											</div>
											<div class="form-group" id="filter_col3" data-column="2">
												<label for="inputEmail3" class="col-sm-4 control-label">Enquiry No</label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm column_filter" id="col2_filter">
												</div>
											</div>
											<div class="form-group" id="filter_col4" data-column="3">
												<label for="inputEmail3" class="col-sm-4 control-label">Customer Name</label>
												<div class="col-sm-8">
													<select name="customer_name" id="customer_name1" class="form-control input-sm" onchange="filterCustomername(3)"></select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-4 control-label">Quotation Type</label>
												<div class="col-sm-8">
													<select name="quotation_type_id" class="form-control input-sm" id="quotation_type_id" onchange="filter_quotation_type(9)"></select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-4 control-label">Sales Type</label>
												<div class="col-sm-8">
													<select name="sales_type_id" class="form-control input-sm" id="sales_type_id" onchange="filter_sales_type(10)"></select>
												</div>
											</div>
											<div class="form-group" id="filter_col4" data-column="12">
												<label for="created_by" class="col-sm-4 control-label">Created By</label>
												<div class="col-sm-8">
													<select name="created_by" id="created_by" class="form-control input-sm" onchange="filterCreatedBy(12)"></select>
												</div>
											</div>
										</div>
										<!-- /.box-body -->
									</form>
									<!-- /.box -->									
								</div>
								<div class="col-md-6">
									<form class="form-horizontal">
										<div class="box-body">
											<div class="form-group" id="filter_col7" data-column="6">
												<label for="inputEmail3" class="col-sm-4 control-label">City</label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm column_filter" id="col6_filter">
												</div>
											</div>
											<div class="form-group" id="filter_col7" data-column="13">
												<label for="inputEmail3" class="col-sm-4 control-label">State</label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm column_filter" id="col13_filter">
												</div>
											</div>
											<div class="form-group" id="filter_col6" data-column="5">
												<label for="inputEmail3" class="col-sm-4 control-label">Country</label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm column_filter" id="col5_filter">
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-4 control-label">Quotation Status</label>
												<div class="col-sm-8">
													<select name="quotation_status_id" class="form-control input-sm" id="quotation_status_id" onchange="filter_quotation_status(7)" ></select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-4 control-label">Quotation Stage</label>
												<div class="col-sm-8">
													<select name="quotation_stage_id" class="form-control input-sm" id="quotation_stage_id" onchange="filter_quotation_stage(8)"></select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-4 control-label">Date Range</label>
												<div class="col-sm-8">
													<div class="checkbox">
														<label>
															<input type="checkbox" name="quotation_daterange_wise" id="quotation_daterange_wise"/>
															<input type="text" name="quotation_daterange" class="form-control input-sm" id="quotation_daterange" placeholder="dd/mm/yyyy - dd/mm/yyyy" value=""/>
														</label>
													</div>
												</div>
											</div>
                                            <div class="form-group">
												<label for="inputEmail3" class="col-sm-4 control-label">Latest Revision Only</label>
												<div class="col-sm-8">
													<div class="checkbox">
														<label>
                                                            <input type="checkbox" name="latest_revision" id="latest_revision" checked=""/>
                                                            <input type="hidden" name="latest_revision_only" id="latest_revision_only" value="1"/>
														</label>
													</div>
												</div>
											</div>
										</div>
										<!-- /.box-body -->
									</form>
									<!-- /.box -->									
								</div>
								<!--/.col (right) -->
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<br/>
					<!--End Advance Search-->
					<div class="col-md-12">
						<table id="quotation_datatable" class="table custom-table agent-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Action</th>
									<th>Q No</th>
									<th>Revision</th>
									<th>Enq No</th>
									<th>Item Code</th>
									<th>Customer Name</th>
									<th>Customer Phone</th> 
									<th>Customer Email</th> 
									<th>Status</th>
									<th>City</th>
									<th>State</th>
									<th>Country</th>
									<th>Created By</th>
									<th>Party Current Person</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
		<div class="crearfix"></div>
	</div>
</div>
<div class="modal fade" id="open_show_itemprint_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 100%;height: auto;max-height: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">
					Item Print
				</h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive" style="overflow-x:scroll">
					<table id="example1" class="table custom-table table-striped dataTable">
						<thead>
							<tr>
								<th width="40%">
									Item
								</th>
								<th width="30%">
									With Letter Pad Print
								</th>
								<th width="30%">
									Without Letter Pad Print
								</th>
							</tr>                        
						</thead>
						<tbody id="items_td">							                  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="crearfix"></div>
<script>
	var table;
	var base_url = "<?=base_url('quotation/add/')?>";
	$(document).ready(function () {
		initAjaxSelect2($("#customer_name1"),"<?=base_url('app/party_select2_source')?>");
        <?php if(isset($status) && $status == '0'){ } else { ?>
            var newOption = new Option('Hot', 4, false, false);
            $('#quotation_status_id').append(newOption);
        <?php } ?>
		initAjaxSelect2($("#quotation_stage_id"),"<?=base_url('app/quotation_stage_select2_source')?>");
		initAjaxSelect2($("#quotation_type_id"),"<?=base_url('app/quotation_type_select2_source')?>");
		initAjaxSelect2($("#sales_type_id"),"<?=base_url('app/sales_type_select2_source')?>");
		initAjaxSelect2($("#created_by"),"<?=base_url('app/staff_select2_source')?>");
        <?php if(isset($staff_id) && !empty($staff_id)){ ?>
            setSelect2Value($("#created_by"),"<?=base_url('app/set_staff_select2_val_by_id/'.$staff_id)?>");
        <?php } ?>

        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [2,4,5,6,7,8,9,10,11,12,13,14],
			}
		};

		//$('#filtration_fields').hide();
		$(document).on('click', '.filtration_div', function(){
			$(".filtration_fields").toggle(500);
		});
		$('input[name="quotation_daterange"]').daterangepicker({
			locale: {
				format: 'DD/MM/YYYY'
			}
		});
        
        table = $('#quotation_datatable').DataTable({
            "serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[2,'desc'] ],
                       
			"ajax": {
				"url": "<?php echo site_url('quotation/quotation_datatable/'.$status)?>",
				"type": "POST",
				"data":function(d){
					d.quotation_status = $("#quotation_status_id option:selected").val();
					d.latest_revision_only = $("#latest_revision_only").val();
				},                                
                            
			},
            <?php if($this->applib->have_access_current_user_rights(QUOTATION_MODULE_ID,"export_data")){ ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Quotations', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Quotations', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Quotations', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Quotations', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Quotations', orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
                        language: {
                              processing: "Loading..."
                         },
			"scrollY": 500,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%",
			"aoColumns": [
				{ "class": "hidden" },
				null,
				null,
				null,
				null,
				{ "sType": 'numeric' },
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null
			],
            "language": {
                "infoFiltered": ""
            },
			"columnDefs": [
				{
					"targets": 0,"visible": false,"searchable": false,
				},
				{
					"targets": 1
				},
				{
					"targets": 2,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},
				{
					"type": "numeric-comma", targets: 3 
				},
				{
					"targets": 4,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},
				{
					"targets": 6,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},
				{
					"targets": 7,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : data.length > 14 ? "<a href=" + base_url + "/" + full[0] + "?view data-toggle='tooltip' data-placement='bottom' title='"+ data +"'>" + data.substr( 0, 14 ) + "</a>" : "<a href=" + base_url + "/" + full[0] + "?view data-toggle='tooltip' data-placement='bottom' title='"+ data +"'>" + data + "</a>";
					}
				},
				{
					"targets": 8,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},
				{
					"targets": 9,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},
				{
					"targets": 10,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},{
					"targets": 11,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},{
					"targets": 12,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},{
					"targets": 13,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},{
					"targets": 14,
					"render": function ( data, type, full, meta ) {
						return data == null ? '' : "<a href=" + base_url + "/" + full[0] + "?view>" + data + "</a>";
					}
				},
			],
		});

        <?php  if(isset($from_date) && !empty($from_date) && isset($to_date) && !empty($to_date)) { ?> 
            $('#quotation_daterange').val('<?php echo date('d/m/Y', strtotime($from_date)) ." - ". date('d/m/Y', strtotime($to_date)); ?>');
            $('#quotation_daterange_wise').prop('checked', true);
            $('#latest_revision').prop('checked', false);
            $("#latest_revision_only").val('0');
            table.column(11).search($("#quotation_daterange").val()).draw();
            table.column(12).search($("#created_by option:selected").text()).draw();
        <?php }  ?>

		$('input.global_filter').on( 'keyup click', function () {
			filterGlobal();
		} );

		$('input.column_filter').on( 'keyup click', function () {
			filterColumn( $(this).parents('.form-group').attr('data-column') );
			console.log($(this).parents('.form-group').attr('data-column'));
		} );

		$('.showhideColumn').on('click', function(){
			var tbl_column = table.column($(this).attr('data-columnindex'));
			tbl_column.visible(!tbl_column.visible());
		});

		$("#agent-form").on("submit", function (e) {
			e.preventDefault();
			var url = '<?php echo base_url('party/delete/') ?>';
			var value = $("#commission").val();
			var name = $("#agent_name").val();
			if (value != '' && name != '') {
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function (data) {
						url += '/' + data.id;
						var TableRow = '<tr>';
						TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-xs btn-danger" data-href="' + url + '"><i class="fa fa-remove"></i></a></td>';
						TableRow += '<td>' + data.agent_name + '</td>';
						TableRow += '<td>' + data.commission + '</td>';
						TableRow += '<td>' + data.created_by + '</td>';
						TableRow += '</tr>';
						$('.agent-table > tbody > tr:last ').after(TableRow);
						$("#agent-form")[0].reset();
						show_notify('Saved Successfully!', true);
					}
				});
			}
		});

		$(document).on("click", ".delete_button", function () {
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if (value) {
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=quotations',
					success: function (data) {
						tr.remove();
						show_notify('Deleted Successfully!', true);
					}
				});
			}
		});

		$(document).on("click", "#quotation_daterange_wise", function () {
			if(this.checked) {
				table.column(11).search(
					$("#quotation_daterange").val()
				).draw();
			} else {
				table.column(11).search('').draw();
			}
		});

		$(document).on("click", ".applyBtn", function () {
			if($('#quotation_daterange_wise').prop("checked") == true){
				$('#quotation_daterange_wise').click();$('#quotation_daterange_wise').click();
			} else {
				$('#quotation_daterange_wise').click();
			}
		});
        
        $(document).on("click", "#latest_revision", function () {
			if(this.checked) {
                $("#latest_revision_only").val('1');
			} else {
				$("#latest_revision_only").val('0');
			}
            table.draw();
		});

		$(document).ready(function () {
			initAjaxSelect2($("#quotation_status_id"),"<?=base_url('app/quotation_status_select2_source')?>");
		});

	});

	$(document).on('click', '.show_itemprint_modal', function(e){
		e.preventDefault();
		var quotation_id = $(this).attr('id');
		$("#open_show_itemprint_modal").modal();
		$( "#items_td" ).html('');
		$.ajax({
			url: '<?php echo base_url('quotation/feed_quotation_items/') ?>/'+quotation_id,
			type: "POST",
			data: '',
			dataType: 'json',
			success: function (data) {
				console.log(data);
				if (data.item_data) {
					jQuery.each( data.item_data, function( i, val ) {
//						$( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('quotation/quotation_print/'); ?>/'+quotation_id+'?item='+val.id+'&l_pad=with" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td> <td><a href="<?php echo base_url('quotation/quotation_print/'); ?>/'+quotation_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><form id="print_'+quotation_id+'_'+i+'" method="post" action="<?php echo base_url('quotation/quotation_print/'); ?>" style="width: 25px; display: initial;" target="_blank"> <input type="hidden" name="quotation_id" id="quotation_id" value="'+quotation_id+'"> <input type="hidden" name="quotation_item_id" id="quotation_item_id" value="' +val.id+ '"> <input type="hidden" name="l_pad" id="l_pad" value="with"> <a class="print_button btn-primary btn-xs" href="javascript:{}" onclick="document.getElementById(\'print_' +quotation_id+'_'+i+'\').submit();"><i class="fa fa-print"></i></a></form><form id="print_'+quotation_id+'_'+i+'_11" method="post" action="<?php echo base_url('quotation/quotation_print/'); ?>" style="width: 25px; display: initial;" target="_blank"> <input type="hidden" name="quotation_id" id="quotation_id" value="'+quotation_id+'"> <input type="hidden" name="quotation_item_id" id="quotation_item_id" value="' +val.id+ '"> <input type="hidden" name="l_pad" id="l_pad" value="with"> <input type="hidden" name="lp_master" id="lp_master" value="lp_master"> <a class="print_button btn-primary btn-xs" href="javascript:{}" onclick="document.getElementById(\'print_' +quotation_id+'_'+i+'_11\').submit();"><i class="fa fa-print"></i></a></form></td><td><form id="print_'+quotation_id+'no_'+i+'" method="post" action="<?php echo base_url('quotation/quotation_print/'); ?>" style="width: 25px; display: initial;" target="_blank"> <input type="hidden" name="quotation_id" id="quotation_id" value="'+quotation_id+'"> <input type="hidden" name="quotation_item_id" id="quotation_item_id" value="' +val.id+ '"> <a class="print_button btn-primary btn-xs" href="javascript:{}" onclick="document.getElementById(\'print_' +quotation_id+'no_'+i+'\').submit();"><i class="fa fa-print"></i></a></form></td></tr>');
					});
				}
			}
		});
	});

	function filterGlobal () {
		table.search(
			$('#global_filter').val()
		).draw();
	}
	function filterColumn ( i ) {
		table.column( i ).search(
			$('#col'+i+'_filter').val()
		).draw();
	}
	function filterCustomername (i) {
		table.column(i).search(
			$("#customer_name1 option:selected").text()
		).draw();
	}
	function filter_quotation_status (i) {
		table.column( i ).search(
			$("#quotation_status_id option:selected").text()
		).draw();
	}
	function filter_quotation_stage (i) {
		table.column(i).search(
			$("#quotation_stage_id option:selected").text()
		).draw();
	}
	function filter_quotation_type (i) {
		table.column(i).search(
			$("#quotation_type_id option:selected").text()
		).draw();
	}
	function filter_sales_type (i) {
		table.column(i).search(
			$("#sales_type_id option:selected").text()
		).draw();
	}
	function filterCreatedBy (i) {
		table.column(i).search(
			$("#created_by option:selected").text()
		).draw();
	}

</script>
