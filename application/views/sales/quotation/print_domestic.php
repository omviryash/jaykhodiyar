<html>
	<head>
		<title>JayKhodiyar</title>
		<style>
			@media print {
                <?php /*body{
                    background: url(<?php echo base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo); ?>);
                    background-repeat: no-repeat;
                    background-size: 10% !important;
                    background-position: top left;
                } */ ?>
				table{
					border-spacing: 0;
				}
				.text-center {
					text-align: center !important;
				}
				.text-right {
					text-align: right !important;
				}
				.text-left {
					text-align: left !important;
				}
				.text-underline {
					text-decoration: underline;
				}
				.section-title{
					font-size: 20px;
					font-weight: 900;
					margin: 15px 10px;
				}
				.no-margin{
					margin: 0;
				}
				table.table-item-detail > thead > tr > th:last-child{
					border-right: solid 1px #000000;
				}
				table.table-item-detail > thead > tr > th:first-child{
					border-left: solid 1px #000000;
				}
				table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
					border-right: solid 1px #000000;
				}
				table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
					border-left: solid 1px #000000;
				}
				table.table-item-detail > thead > tr > th{
					border-top: solid 1px #000000;
					border-bottom: solid 1px #000000;
				}
				table.table-item-detail > tbody > tr > th{
					border-top: solid 1px #000000;
					border-bottom: solid 1px #000000;
				}
			}
			table.table-item-detail > thead > tr > th:last-child{
				border-right: solid 1px #000000;
			}
			table.table-item-detail > thead > tr > th:first-child{
				border-left: solid 1px #000000;
			}
			table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
				border-right: solid 1px #000000;
			}
			table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
				border-left: solid 1px #000000;
			}
			table.table-item-detail > thead > tr > th{
				border-top: solid 1px #000000;
				border-bottom: solid 1px #000000;
			}
			table.table-item-detail > tbody > tr > th{
				border-top: solid 1px #000000;
				border-bottom: solid 1px #000000;
			}
			table{
				border-spacing: 0;
			}
			table tr td {
				padding-left: 5px;
				padding-right: 5px;
			}
			.text-center {
				text-align: center !important;
			}
			.text-right {
				text-align: right !important;
			}
			.text-left {
				text-align: left !important;
			}
			.text-underline {
				text-decoration: underline;
			}
			.section-title{
				font-size: 20px;
				font-weight: 900;
				margin: 15px 10px;
			}
			.no-margin{
				margin: 0;
			}
			.content {
				padding: 10px;
			}
		</style>
	</head>
	<body>
		<?php
			if(isset($letterpad_details->header_logo)){
				echo '<div style="text-align: '.$letterpad_details->header_logo_alignment.';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>';
			}
            if(isset($page_detail_html)){
				echo $page_detail_html;
			}
		?>
	</body>
</html>
