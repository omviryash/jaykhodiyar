<html>
	<head>
		<title>JayKhodiyar</title>
		<style>
			@media print {
				table{
					border-spacing: 0;
				}
				.text-center {
					text-align: center;
				}
				.text-underline {
					text-decoration: underline;
				}
				.section-title{
					font-size: 20px;
					font-weight: 900;
					margin: 15px 10px;
				}
				.no-margin{
					margin: 0;
				}
			}
		</style>
	</head>
	<body>
		<table class="table-company-detail" border="1" style="font-size: 14px; width: 100%;">
			<tr>
				<th style="width:50%;">To:</th>
				<th style="width:50%;">From:</th>
			</tr>
			<tr>
				<td><h4><?=$quotation_data->pname; ?></h4></td>
				<td><h4>Jay Khodiyar Machine Tools</h4></td>
			</tr>
			<tr>
				<td>
					<h4><?=$quotation_data->address; ?>
					<br/> City : <?=$quotation_data->city;?>
					<br/> Pin : <?=$quotation_data->pincode; ?>
					<br/> State : <?=$quotation_data->state;?>
					<br/> Country : <?=$quotation_data->country;?>
						</h4>
				</td>
				<td><h4>Contact Person: Mr. Rudra joshi
					<br/>Cell: +91 8000000076 </h4></td>
			</tr>
			<tr>
				<td><h4>Contact Person : <?=$kind_attn->name; ?></h4></td>
				<td><h4>Email: <a href="mailto:info@jaykhodiyargroup.com">info@jaykhodiyargroup.com</a></h4></td>
			</tr>
			<tr>
				<td><h4>Contact No. : <?=$kind_attn->phone_no; ?></h4></td>
				<td><h4>Date: <?=date("d-m-Y", strtotime($quotation_data->enquiry_date)); ?></h4></td>
			</tr>
			<tr>
				<td><h4>Email : <a href="mailto:<?=$kind_attn->contact_person_email_id; ?>"><?=$kind_attn->email; ?></a></h4></td>
				<td><h4>Ref.No. JK/BRQ/JUMBO/9075/Q19259/15/16</h4></td>
			</tr>
		</table>
		<div>
			<div class="text-center text-underline" style="font-weight: 900!important;">
				<h4>Quotation for Briquetting Machine (Model JUMBO-BRQ 9075 - Domestic)</h4>
			</div>
			<p>
				Dear Sir,
			</p>
			<p>
				In reference to your Enquiry, and subsequent, as desired kindly find below attached the Techno Commercial offer for your reference and kind consideration.
			</p>
			<h5 class="text-underline no-margin">Our offer consists of the following</h5>
			<table border="1" style="font-size: 14px; width: 100%;">
				<tr>
					<td>Annexure-1</td>
					<td>Raw material specifications & Finished Product application</td>
				</tr>
				<tr>
					<td>Annexure-2</td>
					<td>Technical Specification & Other Data</td>
				</tr>
				<tr>
					<td>Annexure-3</td>
					<td>Scope of Jay Khodiyar Machine Tools</td>
				</tr>
				<tr>
					<td></td>
					<td>PART-A	      Main Unit</td>
				</tr>
				<tr>
					<td></td>
					<td>PART-B	      Material Handling Unit</td>
				</tr>
				<tr>
					<td></td>
					<td>PART-C       Layout Plan</td>
				</tr>
				<tr>
					<td>Annexure-4</td>
					<td>Raw material specifications & Finished Product application</td>
				</tr>
				<tr>
					<td>Annexure-5</td>
					<td>Raw material specifications & Finished Product application</td>
				</tr>

			</table>
			<p>
				At the outset we believe that the below Commercial offer is in line with your order and suitable to your requirements of advancements in existing process. Kindly advice, if we can be of any further assistance to you. If you need any clarification/information, Please feel free to contact the undersign.
			</p>
			<p>
				Thanking you and looking forward to a positive response from your end, we remain.
			</p>
			<p>
				Sincerely yours,
			</p>
			<br/>
			<p>
				Authorized Signatory <br/>
				<strong>
					Ms. Komal Kothari <br/>
					(General Manager)
				</strong>
			</p>

			<p align="center">
				<strong align="center" style="font-size:18px; color:green"; line-height:100%; margin: 0; padding: 0;>SAVE THE FUEL, SAVE THE NATION</strong><br />
				<strong align="center" style="color:green"; line-height:100%; margin: 0; padding: 0;>Convert your Agro-Waste into Profits No binding agents required<strong>
			</p>
		</div>
	</body>
</html>


