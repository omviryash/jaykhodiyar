<?php
if(!empty($modules_roles)) {
    $modules = array();
    foreach ($modules_roles as $modules_row) {
        $module_no = explode('.', $modules_row['main_module']);
        switch (count($module_no)) {
            case 1:
                $modules[$module_no[0]] = $modules_row;
                break;
            case 2:
                $modules[$module_no[0]]['sub_modules'][$module_no[1]] = $modules_row;
                break;
            case 3:
                $modules[$module_no[0]]['sub_modules'][$module_no[1]]['sub_modules'][$module_no[2]] = $modules_row;
                break;
            case 4:
                $modules[$module_no[0]]['sub_modules'][$module_no[1]]['sub_modules'][$module_no[2]]['sub_modules'][$module_no[3]] = $modules_row;
                break;

        }
    }
}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <h1>
            <small class="text-primary text-bold">Manage Staff Roles</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab">Staff Roles</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="main-frm">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-1 control-label input-sm">Staff User<span class="required-sign">*</span></label>
                                        <div class="col-sm-4">
                                            <select class="form-control select2" name="user_id" onchange="window.location='<?php echo base_url(); ?>setting?user_id='+$(this).val();">
                                                <option value="">- Select User - </option>
                                                <?php foreach($users as $user):?>
                                                    <option <?php echo $user_id == $user->staff_id ? 'selected="selected"':''; ?> value="<?php echo $user->staff_id; ?>"><?php echo $user->name; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="clearfix">&nbsp;</div>

                                    <a class="btn btn-success btn-update-roles pull-right">Update</a>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Module AND Roles
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="20%">Module</th>
                                                    <th width="80%">
                                                        Roles
                                                        <a class="btn btn-xs btn-danger un-chk-all pull-right">Un Select ALL</a>
                                                        <a class="btn btn-xs btn-primary chk-all pull-right" style="margin-right: 5px;">Select ALL</a>

                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if(count($modules) > 0):
                                                            foreach($modules as $row):
                                                                ?>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <input type="checkbox" class="chk-master" id="c-master"/>
                                                                        <label class="col-sm2" for="c-master">Select All Masters</label>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            endforeach;
                                                        endif;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <a class="btn btn-success btn-update-roles pull-right">Update</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $(".chk-all").click(function(){
            $(".chkids").prop("checked",true);
        });

        $(".chk-master").click(function(){
            if($(".chk-master").prop("checked")){
                $(".a_master").prop("checked",true);
            } else {
                $(".a_master").prop("checked",false);
            }
        });

        $(".chk-sales").click(function(){
            if($(".chk-sales").prop("checked")){
                $(".b_sales").prop("checked",true);
            } else {
                $(".b_sales").prop("checked",false);
            }
        });

        $(".chk-admin").click(function(){
            if($(".chk-admin").prop("checked")){
                $(".y_admin").prop("checked",true);
            } else {
                $(".y_admin").prop("checked",false);
            }
        });

        $(".chk-purchase").click(function(){
            if($(".chk-purchase").prop("checked")){
                $(".purchase").prop("checked",true);
            } else {
                $(".purchase").prop("checked",false);
            }
        });

        $(".chk-store").click(function(){
            if($(".chk-store").prop("checked")){
                $(".store").prop("checked",true);
            } else {
                $(".store").prop("checked",false);
            }
        });

        $(".chk-dispatch").click(function(){
            if($(".chk-dispatch").prop("checked")){
                $(".dispatch").prop("checked",true);
            } else {
                $(".dispatch").prop("checked",false);
            }
        });

        $(".chk-finance").click(function(){
            if($(".chk-finance").prop("checked")){
                $(".finance").prop("checked",true);
            } else {
                $(".finance").prop("checked",false);
            }
        });

        $(".chk-hr").click(function(){
            if($(".chk-hr").prop("checked")){
                $(".hr").prop("checked",true);
            } else {
                $(".hr").prop("checked",false);
            }
        });

        $(".chk-other").click(function(){
            if($(".chk-other").prop("checked")){
                $(".z").prop("checked",true);
            } else {
                $(".z").prop("checked",false);
            }
        });

        $(".un-chk-all").click(function(){
            $(".chkids").prop("checked",false);
        });

        $(".btn-update-roles").click(function(){
            $("#ajax-loader").show();
            $.ajax({
                type: 'post',
                url: '<?=BASE_URL?>setting/update',
                data: $('.main-frm').serialize(),
                success: function(data) {
                    var data = JSON.parse(data);
                    $msg = data.msg;
                    $("#ajax-loader").hide();
                    if(data.status == 1)
                    {
                        show_notify($msg,true);
                    }
                    else
                    {
                        show_notify($msg,false);
                    }

                },
                error: function(e) {
                    $("#ajax-loader").hide();
                }
            });
            return false;
        });
    });
</script>

