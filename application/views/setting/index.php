<?php
//$isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_STAFF_ROLES_MENU_ID, "edit");
if($this->applib->have_access_role(MASTER_GENERAL_MASTER_STAFF_ROLES_MENU_ID,'edit')){
    $isEdit = true;
}else{
    $isEdit = false;
}
//$isEdit = true;
//echo  $isEdit;exit;
if($isEdit) { $btn_disable = null; }else{ $btn_disable = 'disabled';}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <h1>
            <small class="text-primary text-bold">Manage Staff Roles</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab">Staff Roles</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="main-frm">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-1 control-label input-sm">Staff User<span class="required-sign">*</span></label>
                                        <div class="col-sm-4">
                                            <select class="form-control select2" name="user_id" onchange="window.location='<?php echo base_url(); ?>setting?user_id='+$(this).val();">
                                                <option value="">- Select User - </option>
                                                <?php foreach($users as $user):?>
                                                <option <?php echo $user_id == $user->staff_id ? 'selected="selected"':''; ?> value="<?php echo $user->staff_id; ?>"><?php echo $user->name; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="clearfix">&nbsp;</div>
                                    <?php
                                    if($isEdit) {
                                    ?>
                                    <a class="btn btn-success btn-update-roles pull-right" title="Updates" style="position:fixed; right:10px;">Update</a>
                                    <?php
                                    }
                                    ?>
                                    <a class="btn btn-md btn-primary btn_page_up pull-right" style="position:fixed; right:90px;" title="Go to Top"><i class="fa fa-arrow-up"></i></a>
                                    <a class="btn btn-md btn-primary btn_page_down pull-right" style="position:fixed; right:140px;" title="Go to Down"><i class="fa fa-arrow-down"></i></a>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Module AND Roles
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="22%">Module</th>
                                                        <th width="78%">
                                                            Roles
                                                            <?php
                                                            if($isEdit) {
                                                            ?>
                                                            <a class="btn btn-xs btn-danger un-chk-all pull-right">Un Select ALL</a>
                                                            <a class="btn btn-xs btn-primary chk-all pull-right" style="margin-right: 5px;">Select ALL</a>
                                                            <a class="btn btn-xs btn-info close_all pull-right" style="margin-right: 5px;">Close ALL</a>
                                                            <a class="btn btn-xs btn-info open_all pull-right" style="margin-right: 5px;">Open All</a>
                                                            <?php
                                                            }
                                                            ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $master_status = 1;
                                                    $sales_status = 1;
                                                    $admin_status = 1;
                                                    $purchase_status = 1;
                                                    $store_status = 1;
                                                    $dispatch_status = 1;
                                                    $finance_status = 1;
                                                    $hr_status = 1;
                                                    $other_status = 1;
                                                    $selectall_inc = 0;
                                                    if(count($modules_roles) > 0):?>
                                                    <?php foreach($modules_roles as $key => $row):?>
                                                    <?php 
                                                    $current_key = $key;
                                                    $current_key = $current_key+1;
                                                    $main_module_pos = strpos($row['main_module'], '.');
                                                    $next_module_pos = strpos($modules_roles[$current_key]['main_module'], '.');
                                                    if($main_module_pos === false && $next_module_pos !== false ){ ?>
                                                    <tr class="toggle_menu" btn_id_attr="btn_class<?php echo $key;?>" btn2_id_attr="itag_class<?php echo $key;?>">
                                                        <td>
                                                            <?php $selectall_inc++; ?>
                                                            <button type="button" class="btn btn-xs" data-widget="collapse" id="btn_class<?php echo $key;?>"><i class="fa fa-minus" id="itag_class<?php echo $key;?>"></i></button>&nbsp;&nbsp;
                                                            <label for="selectall_box_<?php echo $selectall_inc; ?>" ><strong> Select All <?php echo $row['title']; ?></strong></label> &nbsp;&nbsp;
                                                            <input type="checkbox" name="selectall_box" id="selectall_box_<?php echo $selectall_inc; ?>" <?php echo $btn_disable;?>/>
                                                            
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <?php } else if($row['main_module'] === '5.5.4'){ ?>
                                                    <tr class="toggle_menu" btn_id_attr="btn_class<?php echo $key;?>">
                                                            <td>
                                                                <?php // $selectall_inc++; ?>
                                                                <button type="button" class="btn btn-xs" data-widget="collapse" id="btn_class<?php echo $key;?>"><i class="fa fa-minus"></i></button>&nbsp;&nbsp;
                                                                <label for="selectall_box_<?=$selectall_inc; ?>" ><strong> Select All <?=$row['title']; ?></strong></label>&nbsp;&nbsp;
                                                                <input type="checkbox" name="selectall_box" id="selectall_box_<?=$selectall_inc; ?>" <?php echo $btn_disable;?>/>
                                                                
                                                            </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr class="collapse toggle_details">
                                                        <td><?php echo $row['title'];?></td>
                                                        <td>
                                                            <?php foreach($row['roles'] as $role):?>
                                                            <label class="col-sm-2">
                                                                <input type="checkbox" <?php echo $btn_disable;?> <?php echo in_array($role['id'], $user_roles) ? 'checked="checked"':''; ?> class="chkids <?php echo $row['main_module']; ?>  selectall_box_<?=$selectall_inc; ?>" value="<?php echo $role['id'];?>" name="roles[<?php echo $role['id'];?>_<?php echo $role['module_id'];?>]" /> <?php echo ucwords(str_replace("_"," ",$role['title']));?>
                                                            </label>
                                                            <?php endforeach;?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                    if($isEdit) {
                                    ?>
                                    <a class="btn btn-success btn-update-roles pull-right">Update</a>
                                    <?php
                                    }
                                    ?>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $(".chk-all").click(function(){
            $(".chkids").prop("checked",true);
        });

        $(document).on('click', 'input[name="selectall_box"]', function(){
            var checkbox_class = '.' + $(this).attr('id');
            if($(this).prop("checked")){
                $(checkbox_class).prop("checked",true);
            } else {
                $(checkbox_class).prop("checked",false);
            }
        });	

        $(".chk-master").click(function(){
            if($(".chk-master").prop("checked")){
                $(".a_master").prop("checked",true);
            } else {
                $(".a_master").prop("checked",false);
            }
        });

        $(".chk-sales").click(function(){
            if($(".chk-sales").prop("checked")){
                $(".b_sales").prop("checked",true);
            } else {
                $(".b_sales").prop("checked",false);
            }
        });

        $(".chk-admin").click(function(){
            if($(".chk-admin").prop("checked")){
                $(".y_admin").prop("checked",true);
            } else {
                $(".y_admin").prop("checked",false);
            }
        });

        $(".chk-purchase").click(function(){
            if($(".chk-purchase").prop("checked")){
                $(".purchase").prop("checked",true);
            } else {
                $(".purchase").prop("checked",false);
            }
        });

        $(".chk-store").click(function(){
            if($(".chk-store").prop("checked")){
                $(".store").prop("checked",true);
            } else {
                $(".store").prop("checked",false);
            }
        });

        $(".chk-dispatch").click(function(){
            if($(".chk-dispatch").prop("checked")){
                $(".dispatch").prop("checked",true);
            } else {
                $(".dispatch").prop("checked",false);
            }
        });

        $(".chk-finance").click(function(){
            if($(".chk-finance").prop("checked")){
                $(".finance").prop("checked",true);
            } else {
                $(".finance").prop("checked",false);
            }
        });

        $(".chk-hr").click(function(){
            if($(".chk-hr").prop("checked")){
                $(".hr").prop("checked",true);
            } else {
                $(".hr").prop("checked",false);
            }
        });

        $(".chk-other").click(function(){
            if($(".chk-other").prop("checked")){
                $(".z").prop("checked",true);
            } else {
                $(".z").prop("checked",false);
            }
        });

        $(".un-chk-all").click(function(){
            $(".chkids").prop("checked",false);
        });

        $(".btn-update-roles").click(function(){
            $("#ajax-loader").show();
            $.ajax({
                type: 'post',
                url: '<?=BASE_URL?>setting/update',
                data: $('.main-frm').serialize(),
                success: function(data) {
                    var data = JSON.parse(data);
                    $msg = data.msg;
                    $("#ajax-loader").hide();
                    if(data.status == 1)
                    {
                        show_notify($msg,true);
                    }
                    else
                    {
                        show_notify($msg,false);
                    }

                },
                error: function(e) {
                    $("#ajax-loader").hide();
                }
            });
            return false;
        });
        
        $('.toggle_menu').click(function(){
            var btn_id = $(this).attr('btn_id_attr');
        
            if($(this).find('i').hasClass("fa fa-minus")){
                $(this).nextUntil('tr.toggle_menu').collapse('show');
                $(this).find('button').html('');
                $('#'+btn_id).html('<i class="fa fa-plus"></i>');
              
            } else {
                $(this).nextUntil('tr.toggle_menu').collapse('hide');
                $(this).find('button').html('');
                $('#'+btn_id).html('<i class="fa fa-minus"></i>');
            }
        });
        
        $(".btn_page_up").click(function(){
            $("html, body").animate({scrollTop: 0}, "slow");
        });
        
        $(".btn_page_down").click(function(){
            y = $(window).scrollTop(); 
            $("html, body").animate({ scrollTop: y + $(window).height() }, "slow");
        });
        
        $('.open_all').click(function(){
            $('.toggle_details').collapse('show');
            $('.toggle_menu').find('button').html('');
            $('.toggle_menu').find('button').html('<i class="fa fa-plus"></i>');
        });
        
        $('.close_all').click(function(){
            $('.toggle_details').collapse('hide');
            $('.toggle_menu').find('button').html('');
            $('.toggle_menu').find('button').html('<i class="fa fa-minus"></i>');
        });
    });
</script>
