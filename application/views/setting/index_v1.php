<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <h1>
            <small class="text-primary text-bold">Manage Staff Roles</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab">Staff Roles</a>
                    </li>
                </ul>                    
                <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="main-frm">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-1 control-label input-sm">Staff User<span class="required-sign">*</span></label>
                                            <div class="col-sm-4">
                                                <select class="form-control select2" name="user_id" onchange="window.location='<?php echo base_url(); ?>setting?user_id='+$(this).val();">
                                                    <option value="">- Select User - </option>
                                                    <?php foreach($users as $user):?>
                                                    <option <?php echo $user_id == $user->staff_id ? 'selected="selected"':''; ?> value="<?php echo $user->staff_id; ?>"><?php echo $user->name; ?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>                                       
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        
                                        <a class="btn btn-success btn-update-roles pull-right">Update</a>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Module AND Roles
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th width="20%">Module</th>
                                                            <th width="80%">
                                                                Roles                                                                
                                                                <a class="btn btn-xs btn-danger un-chk-all pull-right">Un Select ALL</a>
                                                                <a class="btn btn-xs btn-primary chk-all pull-right" style="margin-right: 5px;">Select ALL</a>
                                                                
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
														<?php
															$master_status = 1;
															$sales_status = 1;
															$admin_status = 1;
															$purchase_status = 1;
															$store_status = 1;
															$dispatch_status = 1;
															$finance_status = 1;
															$hr_status = 1;
															$other_status = 1;
															if(count($modules_roles) > 0):?>
                                                        <?php foreach($modules_roles as $row):?>
                                                        <?php 
															if($master_status == 1){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-master" id="c-master"/> 
																		<label class="col-sm2" for="c-master">Select All Masters</label>
																	</td>
																</tr>
														<?php } $master_status = 0;
															if($sales_status == 1 && $row['main_module'] == 'b_sales'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-sales" id="c-sales"/> 
																		<label class="col-sm2" for="c-sales">Select All Sales</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'b_sales'){ $sales_status = 0; }
															if($admin_status == 1 && $row['main_module'] == 'y_admin'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-admin" id="c-admin"/> 
																		<label class="col-sm2" for="c-admin">Select All Admins</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'y_admin'){ $admin_status = 0; }
															if($purchase_status == 1 && $row['main_module'] == 'purchase'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-purchase" id="c-purchase"/> 
																		<label class="col-sm2" for="c-purchase">Select All Purchase</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'purchase'){ $purchase_status = 0; }
															if($store_status == 1 && $row['main_module'] == 'store'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-store" id="c-store"/> 
																		<label class="col-sm2" for="c-store">Select All Store</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'store'){ $store_status = 0; }
															if($dispatch_status == 1 && $row['main_module'] == 'dispatch'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-dispatch" id="c-dispatch"/> 
																		<label class="col-sm2" for="c-dispatch">Select All Dispatch</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'dispatch'){ $dispatch_status = 0; }
															if($dispatch_status == 1 && $row['main_module'] == 'dispatch'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-dispatch" id="c-dispatch"/> 
																		<label class="col-sm2" for="c-dispatch">Select All Dispatch</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'dispatch'){ $dispatch_status = 0; }
															if($finance_status == 1 && $row['main_module'] == 'finance'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-finance" id="c-finance"/> 
																		<label class="col-sm2" for="c-finance">Select All Finance</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'finance'){ $finance_status = 0; }
															if($other_status == 1 && $row['main_module'] == 'z'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-other" id="c-other"/> 
																		<label class="col-sm2" for="c-other">Select All Other</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'z'){ $other_status = 0; }
															if($hr_status == 1 && $row['main_module'] == 'hr'){ ?>
																<tr>
																	<td colspan="2">
																		<input type="checkbox" class="chk-hr" id="c-hr"/> 
																		<label class="col-sm2" for="c-hr">Select All HR</label>
																	</td>
																</tr>
														<?php } 
														if($row['main_module'] == 'hr'){ $hr_status = 0; } ?>
                                                        <tr>
                                                            <td><?php echo $row['title'];?></td>
                                                            <td>
                                                                <?php foreach($row['roles'] as $role):?>
                                                                <label class="col-sm-2">
                                                                    <input type="checkbox" <?php echo in_array($role['id'], $user_roles) ? 'checked="checked"':''; ?> class="chkids <?php echo $row['main_module']; ?>" value="<?php echo $role['id'];?>" name="roles[<?php echo $role['id'];?>_<?php echo $role['module_id'];?>]" /> <?php echo ucwords($role['title']);?>                                                            
                                                                </label>
                                                                <?php endforeach;?>
                                                            </td>
                                                        </tr>
                                                        <?php endforeach;?>
                                                        <?php endif;?>
                                                    </tbody>
                                                </table>                                                
                                            </div>
                                        </div>                                           
                                        <a class="btn btn-success btn-update-roles pull-right">Update</a>
                                    </form>
                                </div>
                            </div>
                        </div>    
                </div>                
            </div>
        </div>
    </div>    
</div>    

<script type="text/javascript">
    $(document).ready(function(){
       
       $(".chk-all").click(function(){
           $(".chkids").prop("checked",true);
       });
       
       $(".chk-master").click(function(){
           if($(".chk-master").prop("checked")){
				$(".a_master").prop("checked",true);
			} else {
				$(".a_master").prop("checked",false);
			}
       });
       
       $(".chk-sales").click(function(){
           if($(".chk-sales").prop("checked")){
				$(".b_sales").prop("checked",true);
			} else {
				$(".b_sales").prop("checked",false);
			}
       });
       
       $(".chk-admin").click(function(){
           if($(".chk-admin").prop("checked")){
				$(".y_admin").prop("checked",true);
			} else {
				$(".y_admin").prop("checked",false);
			}
       });
       
       $(".chk-purchase").click(function(){
           if($(".chk-purchase").prop("checked")){
				$(".purchase").prop("checked",true);
			} else {
				$(".purchase").prop("checked",false);
			}
       });
       
       $(".chk-store").click(function(){
           if($(".chk-store").prop("checked")){
				$(".store").prop("checked",true);
			} else {
				$(".store").prop("checked",false);
			}
       });
       
       $(".chk-dispatch").click(function(){
           if($(".chk-dispatch").prop("checked")){
				$(".dispatch").prop("checked",true);
			} else {
				$(".dispatch").prop("checked",false);
			}
       });
       
       $(".chk-finance").click(function(){
           if($(".chk-finance").prop("checked")){
				$(".finance").prop("checked",true);
			} else {
				$(".finance").prop("checked",false);
			}
       });
       
       $(".chk-hr").click(function(){
           if($(".chk-hr").prop("checked")){
				$(".hr").prop("checked",true);
			} else {
				$(".hr").prop("checked",false);
			}
       });
       
       $(".chk-other").click(function(){
           if($(".chk-other").prop("checked")){
				$(".z").prop("checked",true);
			} else {
				$(".z").prop("checked",false);
			}
       });
       
       $(".un-chk-all").click(function(){
           $(".chkids").prop("checked",false);
       });
       
       $(".btn-update-roles").click(function(){
            $("#ajax-loader").show(); 
            $.ajax({
                type: 'post',
                url: '<?=BASE_URL?>setting/update',
                data: $('.main-frm').serialize(),
                success: function(data) {
                    var data = JSON.parse(data);
                    $msg = data.msg;
                    $("#ajax-loader").hide(); 
                    if(data.status == 1)
                    {
                        show_notify($msg,true);
                    }
                    else
                    {
                        show_notify($msg,false);
                    }    
                    
                },
                error: function(e) {
                    $("#ajax-loader").hide();
                }
            });          
            return false;
       });
    });
</script>
