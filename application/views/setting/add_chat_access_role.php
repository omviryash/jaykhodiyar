<?php
$isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_CHAT_ROLES_MENU_ID, "edit");
//$isEdit = true;
if($isEdit) { $btn_disable = null; }else{ $btn_disable = 'disabled';}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <h1>
            <small class="text-primary text-bold">Staff Acces Role for Chat</small>

            
            <!--<a href="<?= base_url()?>sheet/google_sheet_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Sheet List</a>
            <a href="<?= base_url()?>sheet/add_google_sheet/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add New Sheet</a>-->
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <!--<a href="#tab_1" data-toggle="tab">Staff Roles</a>-->
                    </li>
                </ul>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="main-frm" method="post" id="sheet_form">
                                    <!--<input type="hidden" class="" name="sheet_id" id="sheet_id" value="<?php if(isset($sheet)){ echo $sheet[0]['id'];} ?>">-->
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-1 control-label input-sm">Staff User<span class="required-sign">*</span></label>
                                        <div class="col-sm-4">
                                            <select class="form-control select2" name="user_id" onchange="window.location='<?php echo base_url(); ?>setting/chat_access_role?user_id='+$(this).val();">
                                                <option value="">- Select User - </option>
                                                <?php foreach($users as $user):?>
                                                <option <?php echo $user_id == $user->staff_id ? 'selected="selected"':''; ?> value="<?php echo $user->staff_id; ?>"><?php echo $user->name; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="clearfix">&nbsp;</div>
                                    <?php
                                    if($isEdit) {
                                    ?>
                                    <button type="button" onclick="submit_form()" class="btn btn-info btn-xs pull-right" style="margin: 5px;">
                                        Save
                                    </button>
                                    <?php
                                    }
                                    ?>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Select Staff Acces Role for Chat
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                        <?php
                                                        if($isEdit) {
                                                        ?>
                                                        <a class="btn btn-xs btn-danger un-chk-all pull-right">Un Select ALL</a>
                                                        <a class="btn btn-xs btn-primary chk-all pull-right" style="margin-right: 5px;">Select ALL</a>
                                                        <?php
                                                        }
                                                        ?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="col-md-12">

                                                    <?php if(count($agents) > 0):?>
                                                    <?php foreach($agents as $row):?>
                                                    
                                                    <div class="form-group col-md-6">
                                                        <div class="col-md-6">
                                                            <label><input type="checkbox" <?php echo $btn_disable;?> <?php if(!empty($user_roles)){ echo in_array($row['staff_id'], $user_roles) ? 'checked="checked"':''; } ?> class="chkids" name="staff_ids[]" value="<?php echo $row['staff_id'];?>">&nbsp;&nbsp;&nbsp;&nbsp;<?=$row['name'];?></label>
                                                        </div>
                                                    </div>
                                                    
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                                    
                                                </div>
                                                <div class="col-md-12"></div>
                                                <div class="col-md-12"></div>
                                                <div class="col-md-12"></div>
                                                <div class="col-md-12"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if($isEdit) {
                                    ?>
                                        <button type="button" onclick="submit_form()" class="btn btn-info btn-xs pull-right" style="margin: 5px;">
                                            Save
                                        </button>
                                    <?php
                                    }
                                    ?>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>                
            </div>
        </div>
    </div>
    
    <!--<a href="<?= base_url()?>sheet/google_sheet_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Sheet List</a>
    <a href="<?= base_url()?>sheet/add_google_sheet/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add New Sheet</a>-->
</div>    

<script type="text/javascript">

    function submit_form() {
        $("#sheet_form").submit();
    }
    $(document).ready(function() {
        $(".chk-all").click(function () {
            $(".chkids").prop("checked", true);
        });


        $(".un-chk-all").click(function () {
            $(".chkids").prop("checked", false);
        });

        $(document).on("submit","#sheet_form",function(e){
            e.preventDefault();
            /*if(!$('input[name="staff_ids[]"]:checked').length > 0){
                show_notify('Please select at least one staff!', false);
                return false;
            }*/
            
            $("#ajax-loader").show(); 
            $.ajax({
                type: 'post',
                url: '<?=BASE_URL?>setting/save_chat_access_role',
                data: $('#sheet_form').serialize(),
                //data: new FormData(this),
                success: function(data) {
                    var data = JSON.parse(data);
                    $msg = data.msg;
                    $("#ajax-loader").hide(); 
                    if(data.status == 1)
                    {
                        show_notify($msg,true);
                    }
                    else
                    {
                        show_notify($msg,false);
                    }    

                },
                error: function(e) {
                    $("#ajax-loader").hide();
                }
            });          
            return false;
        });
    });
</script>
