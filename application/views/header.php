<style>
    .navbar-custom-menu>.navbar-nav>li>.dropdown-menu{
        z-index: 120 !important;
    }
    @keyframes blink_notification_icon {
       0% { box-shadow: 0 0 0px 0px orange; }
       25% { box-shadow: 0 0 10px 5px orange; font-size : 32px; }
       25% { box-shadow: 0 0 20px 10px orange; }
       100% { box-shadow: 0 0 30px 15px orange; }
       100% { box-shadow: 0 0 40px 20px orange; }
    }
    .shadow_notification{
        animation: blink_notification_icon 1.0s linear infinite;
        position : absolute;
    }
</style>
<header class="main-header">
    <!-- Logo -->
    <?php /*<a href="<?=base_url();?>" class="logo">
		<img src="<?php echo BASE_URL;?>/resource/image/jaykhodiyar_logo.jpg">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!-- <span class="logo-mini"><b>Jay</b>Khodiyar</span> -->
        <!-- logo for regular state and mobile devices -->
        <!-- <span class="logo-lg"><b>Jay </b>Khodiyar</span> -->
    </a> */ ?>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <!-- Sidebar toggle button-->
        <a href="javascript:void(0);" class="btn_go_back sidebar_btn_go_back" title="Go Back">
            <i class="fa fa-arrow-left"></i>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown staff-menu" style="background-color: #e38a39; font-weight: 700;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <?php 
                            if(CURRENT_DATABASE == 'jaykhodiyar'){
                                echo '2019-20';
                            } else if(CURRENT_DATABASE == 'jaykhodiyarupto19'){
                                echo 'Up to 19';
                            }
                        ?>
                    </a>
					<ul class="dropdown-menu" role="menu">
                        <!-- User image -->
                        <li class="">
                            <form>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select name="current_selected_year" id="current_selected_year" class="form-control input-sm">
                                            <option value="jaykhodiyar"<?php if(CURRENT_DATABASE == 'jaykhodiyar'){ echo 'selected'; }?>>2019-20</option>
                                            <option value="jaykhodiyarupto19"<?php if(CURRENT_DATABASE == 'jaykhodiyarupto19'){ echo 'selected'; }?>>Up to 19</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
                
                <?php $feedbacks = $this->crud->get_notification_dropdown();?>
                <?php $feedbacks_count = count($feedbacks);?>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle click_notification" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning notification_count" ><?php echo $feedbacks_count ;?></span>
                    </a>
                    <ul class="dropdown-menu notification_list " style="max-height: 350px; overflow-y: scroll;">
                        <li class="header">You have <span class=""><?php echo $feedbacks_count ;?></span> notifications</li>
                        <?php foreach($feedbacks as $feedback): ?>
                            <li>
                                <?php if(isset($feedback->feedback_id)){ ?>
                                <a href="<?= base_url('feedback_reply/add/'.$feedback->feedback_id.'/view'); ?>" target="_blank">
                                        <i class="fa fa-commenting text-aqua"></i> <b><?php echo $feedback->created_by;?></b> : <?php echo $feedback->note; ?>
                                    </a>
                                <?php } else if(isset($feedback->reminder_id)) { ?>
                                    <a href="<?= base_url('reminder/edit/'.$feedback->reminder_id); ?>" target="_blank">
                                        <i class="fa fa-bell-o text-aqua"></i> <b><?php echo $feedback->assign_by;?></b> : <?php echo $feedback->remind; ?>
                                    </a>
                                <?php } else if(isset($feedback->id)) { ?>
                                    <?php if($this->applib->have_access_role(CAN_APPROVE_SALES_ORDER,"allow")){ ?>
                                        <?php if($feedback->sales_id == PARTY_TYPE_DOMESTIC_ID){ ?>
                                            <a href="<?= base_url('sales_order/add/'.$feedback->id); ?>" target="_blank">
                                                <i class="fa fa-cart-plus text-aqua"></i> <b>Sales Order <?php echo $feedback->sales_order_no; ?> : </b>Pending Approve
                                            </a>
                                        <?php } else if($feedback->sales_id == PARTY_TYPE_EXPORT_ID){  ?>
                                            <a href="<?= base_url('sales_order/add_export/'.$feedback->id); ?>" target="_blank">
                                                <i class="fa fa-cart-plus text-aqua"></i> <b>Sales Order <?php echo $feedback->sales_order_no; ?> : </b>Pending Approve
                                            </a>
                                        <?php }  ?>
                                    <?php }  ?>
                                <?php } else if(isset($feedback->re_quotation_id)) { ?>
                                    <?php if($this->applib->have_access_role(QUOTATION_MODULE_ID,"edit")){ ?>
                                        <a href="<?= base_url('quotation/add/'.$feedback->re_quotation_id); ?>" target="_blank">
                                            <i class="fa fa-bell-o text-aqua"></i> <b>Quot.<?php echo $feedback->quotation_no; ?> Reminder</b> : <?php echo $feedback->fc_name; ?>
                                        </a>
                                    <?php } ?>
                                <?php } else if(isset($feedback->re_sales_order_id)) { ?>
                                    <?php if($feedback->sales_id == PARTY_TYPE_DOMESTIC_ID){ ?>
                                        <a href="<?= base_url('sales_order/add/'.$feedback->re_sales_order_id); ?>" target="_blank">
                                            <i class="fa fa-bell-o text-aqua"></i> <b>Sales Order <?php echo $feedback->sales_order_no;?> Reminder</b> : <?php echo $feedback->fc_name; ?>
                                        </a>
                                    <?php } else if($feedback->sales_id == PARTY_TYPE_EXPORT_ID){  ?>
                                        <a href="<?= base_url('sales_order/add_export/'.$feedback->re_sales_order_id); ?>" target="_blank">
                                            <i class="fa fa-bell-o text-aqua"></i> <b>Sales Order <?php echo $feedback->sales_order_no;?> Reminder</b> : <?php echo $feedback->fc_name; ?>
                                        </a>
                                    <?php }  ?>
                                <?php }  ?>
                                
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                
				<?php //if($this->session->userdata('logged_in_as')['staff_id'] == 1) { ?>
				<?php if($this->applib->have_access_user_dropdown_roles(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
                <li class="dropdown staff-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-users"></i>
                    </a>
					<ul class="dropdown-menu" role="menu">
                        <!-- User image -->
                        <li class="">
                            <form>
                                <div class="form-group">
                                    <?php $lead_owner = $this->crud->get_staff_dropdown();?>
                                    <div class="col-sm-12">
                                        <select name="staff_session_id" id="staff_session_id" class="form-control input-sm">
                                            <option value="">--Select--</option>
                                            <?php foreach($lead_owner as $lo): ?>
                                                <option value="<?= $lo->staff_id; ?>"<?php if($this->session->userdata('is_logged_in')['staff_id'] == $lo->staff_id){echo 'selected';}?>><?= $lo->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>

                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
				<?php } ?>
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle unread-mail-icon" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=isset($image) && !empty($image) && file_exists(image_dir('staff/'.$image))?image_url('staff/'.$image):base_url() . 'resource/dist/img/default-user.png';?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?=isset($name)?ucwords($name):'Admin';?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?=isset($image) && !empty($image) && file_exists(image_dir('staff/'.$image))?image_url('staff/'.$image):  base_url() . 'resource/dist/img/default-user.png';?>" class="img-circle" alt="User Image">
                            <p>
                                <?=isset($name)?ucwords($name):'Admin';?>
                                <br/>
                                <?=isset($email)?$email:'';?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?php if($this->applib->have_access_role(CHANGE_PASSWORD_MODULE_ID,"allow")) { ?>
                                    <a href="<?=base_url()?>auth/profile" class="btn btn-default btn-flat">Profile</a>
                                <?php } ?>
                            </div>
                            <div class="pull-right">
                                <a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!--<li class="dropdown">
                    <a href="JavaScript:window.close()" title="Close Window">
                        <i class="fa fa-close"></i>
                    </a>
                </li>-->
            </ul>
        </div>
    </nav>
    
    <?php
        if(isset($this->session->userdata()['theme_color'])){
            $theme_color = $this->session->userdata()['theme_color'];
        } else {
            $theme_color = '#1f7eba';
        }
    ?>
    <style>
        .skin-jk-light .main-header .navbar {
            background-color: <?php echo $theme_color; ?>;
        }
        .skin-jk-light .main-header li.user-header {
            background-color: <?php echo $theme_color; ?>;
        }
        .skin-jk-light .wrapper,
        .skin-jk-light .main-sidebar,
        .skin-jk-light .left-side {
            background-color: <?php echo $theme_color; ?>;
        }
        .skin-jk-light .sidebar-menu>li:hover>a,
        .skin-jk-light .sidebar-menu>li.active>a {
            color: <?php echo $theme_color; ?>;
        }
    </style>
    
</header>
<?php
    function moneyFormatIndia($num)
    {
        $explrestunits = "";
        if (strlen($num) > 3) {
            $lastthree = substr($num, strlen($num) - 3, strlen($num));
            $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
            $restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for ($i = 0; $i < sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if ($i == 0) {
                    $explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i] . ",";
                }
            }
            $thecash = $explrestunits . $lastthree;
        } else {
            $thecash = $num;
        }
        return $thecash; // writes the final format where $currency is the currency symbol.
    }
?>
<div id="ajax-loader" style="display: none;">
    <div style="width:100%; height:100%; left:0px; top:0px; position:fixed; opacity:0; filter:alpha(opacity=40); background:#000000;z-index:999999999;">
    </div>
    <div style="float:left;width:100%; left:0px; top:45%; text-align:center; position:fixed; padding:0px; z-index:999999999;">
        <img src="<?php echo BASE_URL;?>/resource/image/loader.gif" width="150" height="150">        
    </div>     
</div>
<script>
var base_url = '<?=base_url();?>';
$(document).on('change','#staff_session_id',function(){
	$.ajax({
		type: "POST",
		url: '<?=base_url();?>auth/change_seesion_staff',
		data: { staff_id: $(this).val() },
		dataType: 'json',
		success: function(data){
			if(data.success) {
				location.reload();
			} else {
				alert('Something Went Wrong Please Try Again');
			}
		},
	 });
});
$(document).on('change','#current_selected_year',function(){
	var current_selected_year = $('#current_selected_year').val();
    if(current_selected_year != ''){
        window.location.href = '<?php echo $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER['HTTP_HOST']; ?>/' + current_selected_year;
    }
});
</script>
