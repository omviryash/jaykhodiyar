<?php $column_cnt = 18; ?>
<html>
    <head>
        <title>Challan</title>
        <style>
            .text-center{
            text-align: center;
            }
            table{
            border-spacing: 0;
            width: 100%;
            border-bottom: 1px solid;
            border-right: 1px solid;
            }
            td{
            padding: 1px 1px 1px 1px;
            border-left: 1px solid;
            border-top: 1px solid;
            font-size:10px;
            }
            tr > td:last-child{
            border-right: 1px solid !important;
            }
            tr:last-child > td{
            border-bottom: 1px solid !important;
            }
            .text-right{
            text-align: right;
            }
            .text-bold{
            font-weight: 900 !important;
            font-size:12px !important;
            }
            .text-header{
            font-size: 20px;
            }
            .no-border-top{
            border-top:0;
            }
            .no-border-bottom{
            border-bottom:0 !important;
            }
            .no-border-left{
            border-left:0;
            }
            .no-border-right{
            border-right:0;
            }
            .width-50-pr{
            width:50%;
            }
            td.footer-sign-area{
            height: 82x;
            vertical-align: bottom;
            /*width: 33.33%;*/
            text-align: center;
            }
            .no-border{
            border: 0!important;
            }
            .footer-detail-area{
            color: #000000;
            font-size: 12px;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td class="text-center text-bold text-header" colspan="4">Delivery Challan</td>
            </tr>
            <tr>
                <td class="text-center text-bold" width="50%"><b>Manufacturer & Supplier</b></td>
                <td class=" text-bold" align="left" width="20%">Challan No. : </td>
                <td class=" text-bold" width="15%"><b><center><?=$challan_data->challan_no;?></center></b></td>
                <td class="text-bold" width="15%"><b><center><?=date('d/m/Y', strtotime($challan_data->challan_date)); ?></center></b></td>
            </tr>
            <tr>
                <td class="text-bold" rowspan="6" >
					<span style="font-size:15px;"><b><?=$company_details['name']?></b></span><br />
					<?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
                    <b>GST No : <?=$company_details['gst_no'];?></b>
				</td>
                <td class="text-bold"  align="left">Invoice No. : </td>
                <td class="text-bold" ><b><center><?=$challan_data->invoice_no;?></center></b></td>
                <td class="text-bold" ><b><center><?=($challan_data->invoice_date != '' && strtotime($challan_data->invoice_date) != 0)?date('d/m/Y',strtotime($challan_data->invoice_date)):'';?></center></b></td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Quotation No. : </td>
                <td class="text-bold" colspan="2">
                    <b><?=$this->applib->get_quotation_ref_no($sales_order_data->quotation_no,$challan_item['item_code']);?></b>
                </td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Purchase Order No.: </td>
                <td class="text-bold" >
                    <b><?=$sales_order_data->cust_po_no; ?> </b>
                </td>
                <td class="text-bold" >
                    <b><center><?=$sales_order_data->po_date;?></center></b>
                </td>
            </tr>
            <tr>
                <?php if(!empty($challan_data->sales_order_id)){ ?>
                    <td class=" text-bold"  align="left">Salse Order No. : </td>
                    <td class=" text-bold" >
                        <b><?=$this->applib->get_sales_order_no($sales_order_data->sales_order_no);?></b>
                    </td>
                    <td class="text-bold" >
                        <b><center><?=$sales_order_data->sales_order_date;?></center></b>
                    </td>
                <?php } else { ?>
                    <td class=" text-bold"  align="left">Proforma Invoice No. : </td>
                    <td class=" text-bold" >
                        <b><?=$sales_order_data->proforma_invoice_no;?></b>
                    </td>
                    <td class="text-bold" >
                        <b><center><?=$sales_order_data->proforma_invoice_date;?></center></b>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <td class=" text-bold">Loading At : </td>
                <td class=" text-bold" colspan="2"><b><?=$challan_data->challan_loading_at?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Port of Loading : </td>
                <td class=" text-bold" colspan="2"><b><?=$challan_data->port_of_loading?> - <?=$challan_data->port_of_loading_country?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold width-50-pr"><b>Purchaser</b></td>
                <td class=" text-bold" >Port of  Discharge :</td>
                <td class=" text-bold" colspan="2"><b><?=$challan_data->port_of_discharge?> - <?=$challan_data->port_of_discharge_country?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" rowspan="7" >
					<span style="font-size:15px;"><b><?=$challan_data->party_name?></b><br /></span>
					<?=nl2br($challan_data->address);?>
                    City : <?=$challan_data->city?> -  <?=$challan_data->pincode?> (<?=$challan_data->state?>) <?=$challan_data->country?>.<br />
                    Email : <?=$challan_data->party_email_id;?> <?php //<?=explode(",", $challan_data->party_email_id)[0];?><br />
                    Tel No.:  <?=$challan_data->fax_no;?>,<br />
                    Contact No.:  <?=$challan_data->p_phone_no;?>, <?php //$challan_data->contact_person_phone?><br />
                    Contact Person: <?=$challan_data->contact_person_name?><br />
                    <b>GST No. : <?=$challan_data->party_gst_no;?></b><?= isset($challan_data->party_cin_no) && !empty($challan_data->party_cin_no) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$challan_data->party_cin_no.'</b>' : ''; ?>
				</td>
                <td class=" text-bold" >Place of Delivery : </td>
                <td class=" text-bold" colspan="2"><b><?=$challan_data->port_place_of_delivery?> - <?=$challan_data->place_of_delivery_country?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Delivery Through : </td>
                <td class=" text-bold" colspan="2"><b><?=$challan_data->delivery_through?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Name of shipment :</td>
                <td class=" text-bold" colspan="2"><b><?=$challan_data->name_of_shipment?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Truck / Container No. :</td>
                <td class=" text-bold" colspan="2"><b><?=$challan_item['truck_container_no'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1">LR / BL No. :</td>
                <td class=" text-bold" colspan="1"><b><?=$challan_item['lr_bl_no'];?></b> </td>
                <td class=" text-bold" ><b><center>
                    <?php
                        if(!empty($challan_item['lr_bl_no'])){
                            echo strtotime($challan_item['lr_date']) != 0?date('d/m/Y',strtotime($challan_item['lr_date'])):'';
                        }
                    ?>
                </center></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Transport Person :</td>
                <td class=" text-bold" colspan="2"><b><?=$challan_item['contact_person'];?> - <?=$challan_item['contact_no'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Driver Name : </td>
                <td class=" text-bold" colspan="2"><b><?=$challan_item['driver_name'];?> - <?=$challan_item['driver_contact_no'];?></b></td>
            </tr>
            <tr>
                <td class="text-bold" style="font-size:15px;"><b> <?=$challan_item['item_name'];?></b></td>
                <td class="text-bold" colspan="1" style="font-size:12px;"><b>Serial No. : </b></td>
                <td class="text-bold " colspan="2" style="font-size:12px;"><b><?=$challan_item['item_serial_no'];?> </b></td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="text-center text-bold" colspan="18"><b>Motor Serial No. Details</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2" align="center"><b>User</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Make</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>HP</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>KW</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>Frequency</b></td>
                <td class=" text-bold" colspan="1" align="center"><b>RPM</b></td>
                <td class=" text-bold" colspan="1" align="center"><b>Volts</b></td>
                <td class=" text-bold" colspan="5" align="center"><b>Serial No.</b></td>
            </tr>
            <?php if(!empty($motor_data)){ foreach($motor_data as $motor){ ?>
            <tr>
                <td class=" text-bold" colspan="2" align="center"><?php echo $motor->user_name; ?></td>
                <td class=" text-bold" colspan="3" align="center"><?php echo $motor->make_name; ?></td>
                <td class=" text-bold" colspan="2" align="center"><?php echo $motor->hp_name; ?></td>
                <td class=" text-bold" colspan="2" align="center"><?php echo $motor->kw_name; ?></td>
                <td class=" text-bold" colspan="2" align="center"><?php echo $motor->frequency_name; ?></td>
                <td class=" text-bold" colspan="1" align="center"><?php echo $motor->rpm_name; ?></td>
                <td class=" text-bold" colspan="1" align="center"><?php echo $motor->volts_cycles_name; ?></td>
                <td class=" text-bold" colspan="5" align="center"><?php echo $motor->motor_serial_no; ?></td>
            </tr>
            <?php } } else { for($i=1; $i<=10; $i++){ ?>
            <tr>
                <td class=" text-bold" colspan="2">&nbsp;</td>
                <td class=" text-bold" colspan="3">&nbsp;</td>
                <td class=" text-bold" colspan="2">&nbsp;</td>
                <td class=" text-bold" colspan="2">&nbsp;</td>
                <td class=" text-bold" colspan="2">&nbsp;</td>
                <td class=" text-bold" colspan="1">&nbsp;</td>
                <td class=" text-bold" colspan="1">&nbsp;</td>
                <td class=" text-bold" colspan="5">&nbsp;</td>
            </tr>
            <?php $i++; } } ?>
            <tr>
                <td class="text-center text-bold" colspan="18"><b>Gearbox Serial No. Details</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3" align="center"><b>User</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Make</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Gear Type</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Model</b></td>
                <td class=" text-bold" colspan="1" align="center"><b>Ratio</b></td>
                <td class=" text-bold" colspan="5" align="center"><b>Serial No.</b></td>
            </tr>
            <?php if(!empty($gearbox_data)){ foreach($gearbox_data as $gearbox){ ?>
            <tr>
                <td class=" text-bold" colspan="3" align="center"><?php echo $gearbox->user_name; ?></td>
                <td class=" text-bold" colspan="3" align="center"><?php echo $gearbox->make_name; ?></td>
                <td class=" text-bold" colspan="3" align="center"><?php echo $gearbox->gear_type_name; ?></td>
                <td class=" text-bold" colspan="3" align="center"><?php echo $gearbox->model_name; ?></td>
                <td class=" text-bold" colspan="1" align="center"><?php echo $gearbox->ratio_name; ?></td>
                <td class=" text-bold" colspan="5" align="center"><?php echo $gearbox->gearbox_serial_no; ?></td>
            </tr>
            <?php } } else { for($i=1; $i<=10; $i++){ ?>
            <tr>
                <td class=" text-bold" colspan="3">&nbsp;</td>
                <td class=" text-bold" colspan="3">&nbsp;</td>
                <td class=" text-bold" colspan="3">&nbsp;</td>
                <td class=" text-bold" colspan="3">&nbsp;</td>
                <td class=" text-bold" colspan="1">&nbsp;</td>
                <td class=" text-bold" colspan="5">&nbsp;</td>
            </tr>
            <?php $i++; } } ?>
            <tr>
                <td class="text-center text-bold width-50-pr" colspan="<?=$column_cnt;?>" style="font-size:15px;"><b>&nbsp;</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2" align="center"><b>Sr.No.</b> </td>
                <td class=" text-bold" colspan="3" align="center"><b>Item Code</b> </td>
                <td class=" text-bold" colspan="6" align="center"><b>Item Name</b> </td>
                <td class=" text-bold" colspan="1" align="center"><b>HSN</b> </td>
                <td class=" text-bold" colspan="2" align="center"><b>Packing Mark</b> </td>
                <td class=" text-bold" colspan="2" align="center"><b>No. of Packing</b> </td>
                <td class=" text-bold" colspan="2" align="center"><b>No. of Unit</b> </td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2" align="center">1</td>
                <td class=" text-bold" colspan="3" align="center"><?=$challan_item['item_code'];?></td>
                <td class=" text-bold" colspan="6"><?=$challan_item['item_name'];?> <br /> <?=$challan_item['item_extra_accessories'];?></td>
                <td class=" text-bold" colspan="1" align="center"><?=$challan_item['hsn_no'];?></td>
                <td class=" text-bold" colspan="2" align="center"><?=$challan_item['packing_mark'];?></td>
                <td class=" text-bold" colspan="2" align="center"><?=$challan_item['no_of_packing'];?></td>
                <td class=" text-bold" colspan="2" align="center">
                    1<!--<?=$challan_item['no_of_unit'];?>-->
                </td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="3">&nbsp; </td>
                <td class=" text-bold" colspan="6">&nbsp; </td>
                <td class=" text-bold" colspan="1">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="3">&nbsp; </td>
                <td class=" text-bold" colspan="6">&nbsp; </td>
                <td class=" text-bold" colspan="1">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="3">&nbsp; </td>
                <td class=" text-bold" colspan="6">&nbsp; </td>
                <td class=" text-bold" colspan="1">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="3">&nbsp; </td>
                <td class=" text-bold" colspan="6">&nbsp; </td>
                <td class=" text-bold" colspan="1">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="3">&nbsp; </td>
                <td class=" text-bold" colspan="6">&nbsp; </td>
                <td class=" text-bold" colspan="1">&nbsp; </td>
                <td class=" text-bold" colspan="2" align="right">Total : </td>
                <td class=" text-bold" colspan="2">&nbsp; </td>
                <td class=" text-bold" colspan="2" align="center">1</td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="<?=$column_cnt;?>" style="height: 50px;"><b>Declaration : </b><?=$challan_item['challan_declaration'];?></td>
            </tr>
            <?php /*<tr>
                <td valign="bottom" class="text-center text-bold no-border-bottom" colspan="5" style="height: 80px;">&nbsp;</td>
                
                <td valign="bottom" class="text-center text-bold no-border-bottom" colspan="4" style="height: 80px;">&nbsp;</td>
                
                <td valign="bottom" class="text-right text-bold no-border-bottom" colspan="4" style="height: 80px;">
                
                  <b><?=$challan_data->party_name?></b><br /><br /><br /><br />		
            </td>
            <td valign="bottom" class="text-right text-bold no-border-bottom" colspan="5" style="height: 80px;">
                <b>For, Jay Khodiyar Machine Tools</b><br /><br /><br /><br />
            </td>
            </tr>
            <tr>
                <td valign="bottom" class="text-center text-bold no-border-top" colspan="5">Prepared By</td>
                <td valign="bottom" class="text-center text-bold no-border-top" colspan="4">Checked By</td>
                <td valign="bottom" class="text-center text-bold no-border-top" colspan="4">Receiver Signature & Stamp</td>
                <td valign="bottom" class="text-center text-bold no-border-top" colspan="5">Authorized Signature</td>
            </tr>
            */ ?>
            <tr>
                <td colspan="<?=$column_cnt;?>">
                    <table class="no-border">
                        <tr class="no-border">
                            <td  class="no-border no-border-top footer-detail-area" style="width: 18%; text-align: center;">
                                <strong><?=$challan_data->prepared_by?></strong>
                            </td>
                            <td  class="no-border-top footer-detail-area" style="width: 18%; text-align: center;">
                                <strong><?=$challan_data->checked_by?></strong>
                            </td>
                            <td  class="no-border-top footer-detail-area" style="width: 34%; text-align: center;">
                                <strong><?=$challan_data->party_name?></strong>
                            </td>
                            <td  class="no-border-top footer-detail-area" style="width: 30%; text-align: center;">
                                <strong>For, <?=$company_details['name'];?></strong>
                            </td>
                        </tr>
                        <tr class="no-border">
                            <td class="no-border footer-sign-area text-bold no-border-top no-border-bottom">
                                Prepared By
                            </td>
                            <td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                Checked By
                            </td>
                            <td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                Receiver Signature & Stamp
                            </td>
                            <td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                Authorized Signature
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
