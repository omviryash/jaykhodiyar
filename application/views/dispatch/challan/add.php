<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('challan/save_challan') ?>" method="post" id="save_challan" novalidate>
        <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
            <input type="hidden" name="challan_data[challan_id]" id="challan_id" value="<?=$challan_data->id?>">
        <?php } ?>
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">
                    Dispatch : Challan 
                    <span id="create_from_name">
                        <?php
                            if(!empty($challan_data->sales_order_id)){
                                echo ' : Create From Sales Order : ';
                                echo (isset($sales_order_data->sales_order_no))? $sales_order_data->sales_order_no : '';
                            } else if(!empty($challan_data->proforma_invoice_id)){
                                echo ' : Create From Proforma Invoice : ';
                                echo (isset($sales_order_data->proforma_invoice_no))? $sales_order_data->proforma_invoice_no : '';
                            }
                        ?>
                    </span>
                </small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, Ctrl+S = Save</label></small>
                <?php
                    $challan_add_role = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "add");
                    $challan_edit_role = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "edit");
                    $challan_view_role = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "view");
                ?>
                <?php if($challan_add_role || $challan_edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
                <?php endif;?>
                <?php if($challan_view_role): ?>
                    <a href="<?=base_url()?>challan/challan_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Challan List</a>
                <?php endif;?>
                <?php if($challan_add_role): ?>
                    <a href="<?=base_url()?>challan/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Challan</a>
                <?php endif;?>
                <?php if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view")) { ?>    
                    <span class="pull-right" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="challan_data[send_sms]" id="send_sms" class="send_sms">  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } ?>
            </h1>
        </section>
        <div class="clearfix">
        <?php if($challan_add_role || $challan_edit_role): ?>
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Challan Details</a></li>
                    <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                    <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
                    <li><a href="#tab_3" data-toggle="tab" id="tabs_3">Login</a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="challan_data[sales_order_id]" class="sales_order_id" id="sales_order_id" value="<?=(isset($challan_data->sales_order_id))? $challan_data->sales_order_id : ''; ?>">
                                <input type="hidden" name="challan_data[sales_order_item_id]" class="sales_order_item_id" id="sales_order_item_id" value="<?=(isset($challan_data->sales_order_item_id))? $challan_data->sales_order_item_id : ''; ?>">
                                <input type="hidden" name="challan_data[proforma_invoice_id]" class="proforma_invoice_id" id="proforma_invoice_id" value="<?=(isset($challan_data->proforma_invoice_id))? $challan_data->proforma_invoice_id : ''; ?>">
                                <input type="hidden" name="challan_data[proforma_invoice_item_id]" class="proforma_invoice_item_id" id="proforma_invoice_item_id" value="<?=(isset($challan_data->proforma_invoice_item_id))? $challan_data->proforma_invoice_item_id : ''; ?>">
                                <fieldset class="scheduler-border">
                                <legend class="scheduler-border text-primary text-bold"> Challan Detail</legend>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="challan_no" style="" class="col-sm-3 input-sm">Challan No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="challan_no form-control input-sm" id="challan_no" name="challan_data[challan_no]" value="<?=(isset($challan_data->challan_no))? $challan_data->challan_no : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Challan Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm input-datepicker challan_date" id="challan_date" placeholder="" name="challan_data[challan_date]" value="<?=(isset($challan_data->challan_date))? date('d-m-Y', strtotime($challan_data->challan_date)) : date('d-m-Y'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="invoice_no" style="" class="col-sm-3 input-sm">Invoice No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="invoice_no form-control input-sm" id="invoice_no" value="<?=(isset($challan_data->invoice_no))? $challan_data->invoice_no : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="invoice_date" style="" class="col-sm-3 input-sm">Invoice Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="invoice_date form-control input-sm" id="invoice_date" value="<?=($challan_data->invoice_date != '' && strtotime($challan_data->invoice_date) != 0)?date('d-m-Y',strtotime($challan_data->invoice_date)):'';?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" style="" class="col-sm-3 input-sm">Quotation No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="quotation_no form-control input-sm" name="quotation_data[quotation_no]" id="quotation_no" value="<?=(isset($sales_order_data->quotation_no))? $sales_order_data->quotation_no : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="purchase_order_no" style="" class="col-sm-3 input-sm">Purchase Order No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="purchase_order_no form-control input-sm" id="cust_po_no" value="<?=(isset($sales_order_data->cust_po_no))? $sales_order_data->cust_po_no : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="purchase_order_date" style="" class="col-sm-3 input-sm">Purchase Order Date.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="purchase_order_date form-control input-sm" id="po_date" value="<?=(isset($sales_order_data->po_date))? date('d-m-Y', strtotime($sales_order_data->po_date)) : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="sales_order_no" style="" class="col-sm-3 input-sm">Sales Order No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="sales_order_no form-control input-sm" id="sales_order_no" value="<?=(isset($sales_order_data->sales_order_no))? $sales_order_data->sales_order_no : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="sales_order_date" style="" class="col-sm-3 input-sm">Sales Order Date.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="sales_order_date form-control input-sm" id="sales_order_date" value="<?=(isset($sales_order_data->sales_order_date))? date('d-m-Y', strtotime($sales_order_data->sales_order_date)) : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="proforma_invoice_no" style="" class="col-sm-3 input-sm">Proforma Invoice No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="proforma_invoice_no form-control input-sm" id="proforma_invoice_no" value="<?=(isset($sales_order_data->proforma_invoice_no))? $sales_order_data->proforma_invoice_no : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="proforma_invoice_date" style="" class="col-sm-3 input-sm">Proforma Invoice Date.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="proforma_invoice_date form-control input-sm" id="proforma_invoice_date" value="<?=(isset($sales_order_data->proforma_invoice_date))? date('d-m-Y', strtotime($sales_order_data->proforma_invoice_date)) : ''; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="loading_at" class="col-sm-3 input-sm">Loading At</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm " id="loading_at"  name="challan_data[loading_at]" value="<?=(isset($challan_data->loading_at))? $challan_data->loading_at : 'Factory-Rajkot'; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="prepared_by" class="col-sm-3 input-sm">Prepared By</label>
                                                <div class="col-sm-9">
                                                    <select name="challan_data[prepared_by]" class="prepared_by form-control input-sm select2" id="prepared_by" ></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="port_of_loading" class="col-sm-3 input-sm">Port of Loading</label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="challan_data[port_of_loading]" id="port_of_loading" class="form-control input-sm " placeholder="" value="<?=(isset($challan_data->port_of_loading))? $challan_data->port_of_loading : ''; ?>" >
                                                </div>
                                                <div class="col-sm-4">
													<select name="challan_data[port_of_loading_country_id]" id="port_of_loading_country_id" class="form-control input-sm select2"></select>
												</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="checked_by" class="col-sm-3 input-sm">Checked By</label>
                                                <div class="col-sm-9">
                                                    <select name="challan_data[checked_by]" class="checked_by form-control input-sm select2" id="checked_by" ></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="port_of_discharge" class="col-sm-3 input-sm">Port of Discharge</label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="challan_data[port_of_discharge]" id="port_of_discharge" class="form-control input-sm " placeholder="" value="<?=(isset($challan_data->port_of_discharge))? $challan_data->port_of_discharge : ''; ?>" >
                                                </div>
                                                <div class="col-sm-4">
													<select name="challan_data[port_of_discharge_country_id]" id="port_of_discharge_country_id" class="form-control input-sm select2"></select>
												</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="port_place_of_delivery" class="col-sm-3 input-sm">Place of Delivery</label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="challan_data[port_place_of_delivery]" id="place_of_delivery" class="form-control input-sm " placeholder="" value="<?=(isset($challan_data->port_place_of_delivery))? $challan_data->port_place_of_delivery : ''; ?>" >
                                                </div>
                                                <div class="col-sm-4">
													<select name="challan_data[place_of_delivery_country_id]" id="place_of_delivery_country_id" class="form-control input-sm select2"></select>
												</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="delivery_through_id" class="col-sm-3 input-sm">Delivery Through</label>
                                                <div class="col-sm-9">
                                                    <select name="challan_data[delivery_through_id]" class="delivery_through_id form-control input-sm select2" id="delivery_through_id" ></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="name_of_shipment" class="col-sm-3 input-sm">Name of Shipment</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="challan_data[name_of_shipment]" id="name_of_shipment" class="form-control input-sm " placeholder="" value="<?=(isset($challan_data->name_of_shipment))? $challan_data->name_of_shipment : ''; ?>" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Notes</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" name="challan_data[notes]" ><?=(isset($challan_data->notes))? $challan_data->notes : ''; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                </fieldset>
                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border text-primary text-bold"> Party Detail</legend>
                                    <div class="row" style="margin-top: 10px;">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <select name="challan_data[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" placeholder="" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <input type="hidden" name="party[party_name]" id="party_name" class="form-control input-sm party_name" placeholder="">
                                            <input type="hidden" name="party[party_id]" id="party_party_id" class="form-control input-sm party_party_id" placeholder="">
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="address" class="col-sm-3 input-sm">Address</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="city" class="col-sm-3 input-sm">City</label>
                                                <div class="col-sm-9">
                                                    <div class="col-md-6" style="padding:0px !important;">
                                                        <select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
                                                    </div>
                                                    <div class="col-md-6" style="padding-right:0px !important;">
                                                        <input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="state" class="col-sm-3 input-sm">State</label>
                                                <div class="col-sm-9">
                                                    <select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="country" class="col-sm-3 input-sm">Country</label>
                                                <div class="col-sm-9">
                                                    <select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
                                                    <small>Add multiple email id by Comma separated.</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" placeholder="" disabled="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
                                                    <small>Add multiple phone number by Comma separated.</small>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="party_website" class="col-sm-3 input-sm">Website</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm website" id="party_website" name="party[website]" placeholder="" disabled="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <?php /*<div class="form-group">
                                                <label for="agent_id" class="col-sm-3 input-sm">Agent</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <select name="party[agent_id]" id="agent_id" class="form-control input-sm"></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>*/ ?>
                                            <div class="form-group">
                                                <label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <select name="challan_data[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
                                                        <option value="">--Select--</option>
                                                        <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
                                                            <?php foreach($party_contact_person as $pcp_key => $pcp_value): ?>
                                                            <option value="<?= $pcp_value->contact_person_id; ?>"  <?=$challan_data->kind_attn_id == $pcp_value->contact_person_id ?'selected="selected"':'';?>><?= $pcp_value->name; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Designation</label>
                                                <div class="col-sm-9">
                                                    <div id="contact_person_designation"><?php echo isset($contact_person->designation) ? $contact_person->designation : '' ;?></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Department</label>
                                                <div class="col-sm-9">
                                                    <div id="contact_person_department"><?php echo isset($contact_person->department) ? $contact_person->department : '' ;?></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person->mobile_no) ? $contact_person->mobile_no : '' ;?>" disabled="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person->email) ? $contact_person->email : '' ;?>" disabled="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="scheduler-border challan-item-section">
                                    <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                    <div class="col-md-6">
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                            <div class="col-sm-9">
                                                <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
                                                    <input type="hidden" name="challan_item_data[challan_item_id]" id="challan_item_id" class="input-sm form-control" value="<?=(isset($challan_item_data->id))? $challan_item_data->id : ''; ?>" readonly="readonly">
                                                <?php } ?>
                                                <input type="hidden" name="challan_item_data[item_id]" id="item_id" class="input-sm form-control" value="<?=(isset($challan_item_data->item_id))? $challan_item_data->item_id : ''; ?>" readonly="readonly">
                                                <input type="text" name="challan_item_data[item_name]" id="item_name" class="input-sm form-control disabled" value="<?=(isset($challan_item_data->item_name))? $challan_item_data->item_name : ''; ?>" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Item Code</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[item_code]" class="input-sm form-control disabled" id="item_code" value="<?=(isset($challan_item_data->item_code))? $challan_item_data->item_code : ''; ?>" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Item Description</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[item_description]" class=" form-control input-sm item_data" id="item_description" placeholder="" value="<?=(isset($challan_item_data->item_description))? $challan_item_data->item_description : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="contact_person" class="col-sm-3 input-sm"> Transport Person </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[contact_person]" class=" form-control input-sm item_data" id="contact_person" placeholder="" value="<?=(isset($challan_item_data->contact_person))? $challan_item_data->contact_person : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="contact_no" class="col-sm-3 input-sm"> Contact No. </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[contact_no]" class=" form-control input-sm item_data" id="contact_no" placeholder="" value="<?=(isset($challan_item_data->contact_no))? $challan_item_data->contact_no : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="driver_name" class="col-sm-3 input-sm"> Driver Name </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[driver_name]" class=" form-control input-sm item_data" id="driver_name" placeholder="" value="<?=(isset($challan_item_data->driver_name))? $challan_item_data->driver_name : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="driver_contact_no" class="col-sm-3 input-sm"> Driver Contact No. </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[driver_contact_no]" class=" form-control input-sm item_data" id="driver_contact_no" placeholder="" value="<?=(isset($challan_item_data->driver_contact_no))? $challan_item_data->driver_contact_no : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
                                            <div class="col-sm-2">
                                                <input type="text" name="challan_item_data[quantity]" class=" form-control input-sm item_data num_only" id="quantity" placeholder="" value="1" readonly="readonly">
                                            </div>
                                            <label for="item_extra_accessories_id" class="col-sm-3 input-sm text-right">Item Extra Accessories</label>
                                            <div class="col-sm-4">
                                                <select name="challan_item_data[item_extra_accessories_id]" class=" form-control input-sm select2 item_data" id="item_extra_accessories_id" ></select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="item_serial_no" class="col-sm-3 input-sm"> Item Serial No. </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[item_serial_no]" class=" form-control input-sm item_data" id="item_serial_no" data-input_name="item_serial_no" placeholder="" value="<?=(isset($challan_item_data->item_serial_no))? $challan_item_data->item_serial_no : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="truck_container_no" class="col-sm-3 input-sm"> Truck / Container No. </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[truck_container_no]" class=" form-control input-sm item_data" id="truck_container_no" placeholder="" value="<?=(isset($challan_item_data->truck_container_no))? $challan_item_data->truck_container_no : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="lr_bl_no" class="col-sm-3 input-sm"> LR / BL No. </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[lr_bl_no]" class=" form-control input-sm item_data" id="lr_bl_no" placeholder="" value="<?=(isset($challan_item_data->lr_bl_no))? $challan_item_data->lr_bl_no : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="lr_date" class="col-sm-3 input-sm"> LR Date </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[lr_date]" class=" form-control input-sm item_data input-datepicker" id="lr_date" placeholder="" value="<?=(isset($challan_item_data->lr_date))? date('d-m-Y', strtotime($challan_item_data->lr_date)) : date('d-m-Y'); ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="packing_mark" class="col-sm-3 input-sm"> Packing Mark </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[packing_mark]" class=" form-control input-sm item_data" id="packing_mark" placeholder="" value="<?=(isset($challan_item_data->packing_mark))? $challan_item_data->packing_mark : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="no_of_packing" class="col-sm-3 input-sm"> No. of Packing </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[no_of_packing]" class=" form-control input-sm item_data" id="no_of_packing" placeholder="" value="<?=(isset($challan_item_data->no_of_packing))? $challan_item_data->no_of_packing : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="no_of_unit" class="col-sm-3 input-sm"> No. of Unit </label>
                                            <div class="col-sm-9">
                                                <input type="text" name="challan_item_data[no_of_unit]" class=" form-control input-sm item_data" id="no_of_unit" placeholder="" value="<?=(isset($challan_item_data->no_of_unit))? $challan_item_data->no_of_unit : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-11">
                                        <h5 class="text-center"><b>Motor Serial No. Details</b></h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_user" class="input-sm"> User </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_make" class="input-sm"> Make </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_hp" class="input-sm"> HP </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_kw" class="input-sm"> KW </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_frequency" class="input-sm"> Frequency </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_rpm" class="input-sm"> RPM </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_volts_cycles" class="input-sm"> Volts </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_serial_no" class="input-sm"> Serial No.  </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="motor_detail_fields">
                                        <div id="motor_fields">
                                        <div class="col-md-12">
                                            <input type="hidden" name="item_details[challan_motor_id][]" id="challan_motor_id" class="challan_motor_id" >
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[motor_user][]" class="motor_user form-control input-sm select2 item_data" id="motor_user" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($motor_user as $motor_user_row){ ?>
                                                        <option value="<?= $motor_user_row->id; ?>" <?= isset($motor_user_id) && $motor_user_row->id == $motor_user_id?'selected="selected"':'';?>><?= $motor_user_row->user_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[motor_make][]" class="motor_make form-control input-sm select2 item_data" id="motor_make" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($motor_make as $motor_make_row){ ?>
                                                        <option value="<?= $motor_make_row->id; ?>" <?= isset($motor_make_id) && $motor_make_row->id == $motor_make_id?'selected="selected"':'';?>><?= $motor_make_row->make_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[motor_hp][]" class="motor_hp form-control input-sm select2 item_data" id="motor_hp" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($motor_hp as $motor_hp_row){ ?>
                                                        <option value="<?= $motor_hp_row->id; ?>" <?= isset($motor_hp_id) && $motor_hp_row->id == $motor_hp_id?'selected="selected"':'';?>><?= $motor_hp_row->hp_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[motor_kw][]" class="motor_kw form-control input-sm select2 item_data" id="motor_kw" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($motor_kw as $motor_kw_row){ ?>
                                                        <option value="<?= $motor_kw_row->id; ?>" <?= isset($motor_kw_id) && $motor_kw_row->id == $motor_kw_id?'selected="selected"':'';?>><?= $motor_kw_row->kw_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[motor_frequency][]" class="motor_frequency form-control input-sm select2 item_data" id="motor_frequency" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($motor_frequency as $motor_frequency_row){ ?>
                                                        <option value="<?= $motor_frequency_row->id; ?>" <?= isset($motor_frequency_id) && $motor_frequency_row->id == $motor_frequency_id?'selected="selected"':'';?>><?= $motor_frequency_row->frequency_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[motor_rpm][]" class="motor_rpm form-control input-sm select2 item_data" id="motor_rpm" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($motor_rpm as $motor_rpm_row){ ?>
                                                        <option value="<?= $motor_rpm_row->id; ?>" <?= isset($motor_rpm_id) && $motor_rpm_row->id == $motor_rpm_id?'selected="selected"':'';?>><?= $motor_rpm_row->rpm_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[motor_volts_cycles][]" class="motor_volts_cycles form-control input-sm select2 item_data" id="motor_volts_cycles" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($motor_volts_cycles as $motor_volts_cycles_row){ ?>
                                                        <option value="<?= $motor_volts_cycles_row->id; ?>" <?= isset($motor_volts_cycles_id) && $motor_volts_cycles_row->id == $motor_volts_cycles_id?'selected="selected"':'';?>><?= $motor_volts_cycles_row->volts_cycles_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <input name="item_details[motor_serial_no][]" type="text" class="motor_serial_no form-control input-sm item_data" id="motor_serial_no" >
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn btn-danger btn-xs pull-center remove_motor_detail" style="display: none;"> <i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-11">
                                        <button type="button" class="btn btn-info btn-xs pull-right add_motor_detail"> <i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-10">
                                        <h5 class="text-center"><b>Gearbox Serial No. Details</b></h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_user" class="input-sm"> User </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_make" class="input-sm"> Make </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_gear_type" class="input-sm"> Gear Type </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_model" class="input-sm"> Model </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_ratio" class="input-sm"> Ratio </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_serial_no" class="input-sm"> Serial No.  </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="gearbox_detail_fields">
                                        <div id="gearbox_fields">
                                        <div class="col-md-12">
                                            <input type="hidden" name="item_details[challan_gearbox_id][]" id="challan_gearbox_id" class="challan_gearbox_id" >
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[gearbox_user][]" class="gearbox_user form-control input-sm select2 item_data" id="gearbox_user" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($gearbox_user as $gearbox_user_row){ ?>
                                                        <option value="<?= $gearbox_user_row->id; ?>" <?= isset($gearbox_user_id) && $gearbox_user_row->id == $gearbox_user_id?'selected="selected"':'';?>><?= $gearbox_user_row->user_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[gearbox_make][]" class="gearbox_make form-control input-sm select2 item_data" id="gearbox_make" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($gearbox_make as $gearbox_make_row){ ?>
                                                        <option value="<?= $gearbox_make_row->id; ?>" <?= isset($gearbox_make_id) && $gearbox_make_row->id == $gearbox_make_id?'selected="selected"':'';?>><?= $gearbox_make_row->make_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[gearbox_gear_type][]" class="gearbox_gear_type form-control input-sm select2 item_data" id="gearbox_gear_type" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($gearbox_gear_type as $gearbox_gear_type_row){ ?>
                                                        <option value="<?= $gearbox_gear_type_row->id; ?>" <?= isset($gearbox_gear_type_id) && $gearbox_gear_type_row->id == $gearbox_gear_type_id?'selected="selected"':'';?>><?= $gearbox_gear_type_row->gear_type_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[gearbox_model][]" class="gearbox_model form-control input-sm select2 item_data" id="gearbox_model" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($gearbox_model as $gearbox_model_row){ ?>
                                                        <option value="<?= $gearbox_model_row->id; ?>" <?= isset($gearbox_model_id) && $gearbox_model_row->id == $gearbox_model_id?'selected="selected"':'';?>><?= $gearbox_model_row->model_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <select name="item_details[gearbox_ratio][]" class="gearbox_ratio form-control input-sm select2 item_data" id="gearbox_ratio" >
                                                        <option value=""> - Select - </option>
                                                    <?php foreach($gearbox_ratio as $gearbox_ratio_row){ ?>
                                                        <option value="<?= $gearbox_ratio_row->id; ?>" <?= isset($gearbox_ratio_id) && $gearbox_ratio_row->id == $gearbox_ratio_id?'selected="selected"':'';?>><?= $gearbox_ratio_row->ratio_name; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                <div class="form-group">
                                                    <input name="item_details[gearbox_serial_no][]" type="text" class="gearbox_serial_no form-control input-sm item_data" id="gearbox_serial_no" >
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn btn-danger btn-xs pull-center remove_gearbox_detail"> <i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <button type="button" class="btn btn-info btn-xs pull-right add_gearbox_detail"> <i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Created By</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=(isset($challan_data->created_by_name))? $challan_data->created_by_name : ''; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Created Date</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=(isset($challan_data->created_at))? $challan_data->created_at : ''; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Updated By</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=(isset($challan_data->updated_by_name))? $challan_data->updated_by_name : ''; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=(isset($challan_data->updated_at))? $challan_data->updated_at : ''; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <?php endif;?>
        </div>
        <section class="content-header">
            <?php if($challan_add_role || $challan_edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
            <?php endif;?>
            <?php if($challan_view_role): ?>
                <a href="<?=base_url()?>challan/challan_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Challan List</a>
            <?php endif;?>
            <?php if($challan_add_role): ?>
                <a href="<?=base_url()?>challan/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Challan</a>
            <?php endif;?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if(isset($challan_data->id) && !empty($challan_data->id)){ } else { ?>
<div class="modal fade" id="select_sales_order_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select Sales Order Or Export Proforma Invoice
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#sales_order_tab" data-toggle="tab">Select Sales Order </a></li>
                        <li><a href="#export_proforma_invoice_tab" data-toggle="tab">Select Export Proforma Invoice</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="sales_order_tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive" style="overflow-x:hidden">
                                        <table id="sales_order_datatable" class="table custom-table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Enquiry No</th>
                                                <th>Quotation No</th>
                                                <th>Order No</th>
                                                <th>Party</th>
                                                <th>Item Name</th>
                                                <th>Contact No</th>
                                                <th>Email Id</th>
                                                <th>Party Type</th>
                                                <th>City</th>
                                                <th>State</th>
                                                <th>Country</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" id="export_proforma_invoice_tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive" style="overflow-x:hidden">
                                        <table id="export_proforma_invoice_datatable" class="table custom-table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Enquiry No</th>
                                                <th>Quotation No</th>
                                                <th>Proforma No</th>
                                                <th>Party</th>
                                                <th>Item Name</th>
                                                <th>Contact No</th>
                                                <th>Email Id</th>
                                                <th>Party Type</th>
                                                <th>City</th>
                                                <th>State</th>
                                                <th>Country</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?=dist_url('js/sales_order.js');?>"></script>
<script type="text/javascript">
    var first_time_edit_mode = 1;
    <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
        first_time_edit_mode = 0;
        $("#ajax-loader").show();
    <?php } ?>
    $(document).ready(function(){
        $('input[type="checkbox"].send_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        $('[href="#export_proforma_invoice_tab"]').click();
        $('[href="#sales_order_tab"]').click();
        
        initAjaxSelect2($("#delivery_through_id"),"<?=base_url('app/delivery_through_select2_source')?>");
        <?php if(isset($challan_data->delivery_through_id)){ ?>
			setSelect2Value($("#delivery_through_id"),"<?=base_url('app/set_delivery_through_select2_val_by_id/'.$challan_data->delivery_through_id)?>");
        <?php } ?>
        initAjaxSelect2($("#prepared_by"),"<?=base_url('app/staff_select2_source')?>");
        <?php if(isset($challan_data->prepared_by)){ ?>
			setSelect2Value($("#prepared_by"),"<?=base_url('app/set_staff_select2_val_by_id/'.$challan_data->prepared_by)?>");
        <?php } ?>
        initAjaxSelect2($("#checked_by"),"<?=base_url('app/staff_select2_source')?>");
        <?php if(isset($challan_data->checked_by)){ ?>
			setSelect2Value($("#checked_by"),"<?=base_url('app/set_staff_select2_val_by_id/'.$challan_data->checked_by)?>");
        <?php } ?>
        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
        initAjaxSelect2($("#reference_id"),"<?=base_url('app/reference_select2_source')?>");
        initAjaxSelect2($("#item_extra_accessories_id"),"<?=base_url('app/item_extra_accessories_select2_source')?>");
        <?php if(isset($challan_item_data->item_extra_accessories_id)){ ?>
			setSelect2Value($("#item_extra_accessories_id"),"<?=base_url('app/set_item_extra_accessories_select2_val_by_id/'.$challan_item_data->item_extra_accessories_id)?>");
        <?php } else { ?>
            setSelect2Value($("#item_extra_accessories_id"),"<?=base_url('app/set_item_extra_accessories_select2_val_by_id/'.DEFAULT_ITEM_EXTRA_ACCESSORIES_ID)?>");
        <?php } ?>
        
        <?php if(isset($challan_data->sales_to_party_id)){ ?>
			setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id/'.$challan_data->sales_to_party_id)?>");
            party_details(<?=$challan_data->sales_to_party_id; ?>);
		<?php } ?>
        
        initAjaxSelect2($("#port_of_loading_country_id"),"<?=base_url('app/country_select2_source')?>");
		<?php if(isset($challan_data->port_of_loading_country_id)){ ?>
		setSelect2Value($("#port_of_loading_country_id"),"<?=base_url('app/set_country_select2_val_by_id/'.$challan_data->port_of_loading_country_id)?>");
		<?php } ?>

		initAjaxSelect2($("#port_of_discharge_country_id"),"<?=base_url('app/country_select2_source')?>");
		<?php if(isset($challan_data->port_of_discharge_country_id)){ ?>
		setSelect2Value($("#port_of_discharge_country_id"),"<?=base_url('app/set_country_select2_val_by_id/'.$challan_data->port_of_discharge_country_id)?>");
		<?php } ?>

		initAjaxSelect2($("#place_of_delivery_country_id"),"<?=base_url('app/country_select2_source')?>");
		<?php if(isset($challan_data->place_of_delivery_country_id)){ ?>
		setSelect2Value($("#place_of_delivery_country_id"),"<?=base_url('app/set_country_select2_val_by_id/'.$challan_data->place_of_delivery_country_id)?>");
		<?php } ?>
        
        var table
        table = $('#sales_order_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('challan/sales_order_itemqtywise_datatable')?>",
                "type": "POST",
                "data": function(d){
                    d.request_from = "challan";
                }
            },
            "scrollY": 350,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "bInfo" : false
        });
        
        var epi_table
        epi_table = $('#export_proforma_invoice_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('challan/export_proforma_invoice_itemqtywise_datatable')?>",
                "type": "POST",
                "data": function(d){
                    d.request_from = "challan";
                }
            },
            "scrollY": 350,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "bInfo" : false
        });
        
        $('#select_sales_order_modal').modal({backdrop: 'static', keyboard: false});
        $('#select_sales_order_modal').modal('show');
        $('#select_sales_order_modal').on('shown.bs.modal',function(){
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();	
        });
                
        $(document).on('click', '.sales_order_row', function () {
            $('.sales_order_row').prop('disabled', true);
            var tr = $(this).closest('tr');
            var sales_order_id = $(this).data('sales_order_id');
            var sales_order_item_id = $(this).data('sales_order_item_id');
            $("#sales_order_id").val(sales_order_id);
            $("#sales_order_item_id").val(sales_order_item_id);
            feedSalesOrderData(sales_order_id, sales_order_item_id);
            $('#select_sales_order_modal').modal('hide');
            $('#challan_date').focus();
        });
        
        $(document).on('click', '.proforma_invoice_row', function () {
            $('.proforma_invoice_row').prop('disabled', true);
            var tr = $(this).closest('tr');
            var proforma_invoice_id = $(this).data('proforma_invoice_id');
            var proforma_invoice_item_id = $(this).data('proforma_invoice_item_id');
            $("#proforma_invoice_id").val(proforma_invoice_id);
            $("#proforma_invoice_item_id").val(proforma_invoice_item_id);
            feedProformaInvoiceData(proforma_invoice_id, proforma_invoice_item_id);
            $('#select_sales_order_modal').modal('hide');
            $('#challan_date').focus();
        });
        
        $(document).on('click','.add_motor_detail',function() {
			$("#motor_fields").clone().appendTo("#motor_detail_fields");
			$(".remove_motor_detail:last").css('display', 'block');
			$(".challan_motor_id:last ").val('');
			$(".motor_serial_no:last ").val('');
		});
		
		$(document).on('click','.remove_motor_detail',function() {
			$(this).closest("#motor_fields").remove();
            var challan_motor_id = $(this).closest("#motor_fields").find("#challan_motor_id").val();
            $('#save_challan').append('<input type="hidden" name="deleted_challan_motor_id[]" id="deleted_challan_motor_id" value="' + challan_motor_id + '" />');
		});

		$(document).on('click','.add_gearbox_detail',function() {
            if (($("#challan_gearbox_empty").length > 0)){
                $("#challan_gearbox_empty").remove();
                $("#gearbox_fields").show();
            } else {
                $("#gearbox_fields").clone().appendTo("#gearbox_detail_fields");
            }
			$(".remove_gearbox_detail:last").css('display', 'block');
            $(".challan_gearbox_id:last ").val('');
			$(".gearbox_serial_no:last ").val('');
		});
		
		$(document).on('click','.remove_gearbox_detail',function() {
            var reqlength = $('.gearbox_serial_no').length;
            if(reqlength == 1){
                $(this).closest("#gearbox_fields").append('<input type="hidden" name="challan_gearbox_empty[]" id="challan_gearbox_empty">');
                $(this).closest("#gearbox_fields").hide();
            } else {
                $(this).closest("#gearbox_fields").remove();
            }
            var challan_gearbox_id = $(this).closest("#gearbox_fields").find("#challan_gearbox_id").val();
            $('#save_challan').append('<input type="hidden" name="deleted_challan_gearbox_id[]" id="deleted_challan_gearbox_id" value="' + challan_gearbox_id + '" />');
		});
		
		$(document).on('change','.motor_hp',function() {
			$(this).closest("#motor_fields").find('.motor_kw').val($(this).val());
		});
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href"); // activated tab
            if (target == '#tab_1') {
                $('#item_extra_accessories_id').select2('close');
                $('#challan_date').off("focus").datepicker("show");
            } else if (target == '#tab_2') {
                $('#item_extra_accessories_id').select2('open');
                $('#challan_date').off("focus").datepicker("hide");
            } else {
                $('#item_extra_accessories_id').select2('close');
                $('#challan_date').off("focus").datepicker("hide");
            }
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_challan").submit();
                return false;
            }
        });
    
        $(document).on('submit', '#save_challan', function () {
            if($.trim($("#party_id").val()) == ''){
                show_notify('Please Select Party.',false);
                return false;
            }
            if($.trim($("#reference_id").val()) == ''){
                show_notify('Please Select Reference.',false);
                return false;
            }
            if($.trim($("#item_id").val()) == ''){
                show_notify("Please select Item.", false);
                return false;
            }
            
            var reqlength = $('.motor_serial_no').length;
            var value = $('.motor_serial_no').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Enter All Motor Serial No. !', false);
                return false;
            }

            var value = $('.motor_user').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Select All Motor User. !', false);
                return false;
            }

            var value = $('.motor_volts_cycles').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Select All Motor Volts Cycles. !', false);
                return false;
            }

            var value = $('.motor_rpm').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Select All Motor RPM. !', false);
                return false;
            }

            var value = $('.motor_frequency').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Select All Motor Frequency. !', false);
                return false;
            }

            var value = $('.motor_kw').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Select All Motor KW. !', false);
                return false;
            }

            var value = $('.motor_hp').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Select All Motor HP. !', false);
                return false;
            }
            var value = $('.motor_make').filter(function () {
                return this.value != '';
            });
            if (value.length>=0 && (value.length !== reqlength)) {
                show_notify('Please Select All Motor Make. !', false);
                return false;
            }

            var reqlength = $('.gearbox_serial_no').length;
            if($('#gearbox_fields').is(':visible')) {
                var value = $('.gearbox_serial_no').filter(function () {
                    return this.value != '';
                });
                if (value.length>=0 && (value.length !== reqlength)) {
                    show_notify('Please Enter All Gearbox Serial No. !', false);
                    return false;
                }
                
                var value = $('.gearbox_user').filter(function () {
                    return this.value != '';
                });
                if (value.length>=0 && (value.length !== reqlength)) {
                    show_notify('Please Select All Gearbox User. !', false);
                    return false;
                }

                var value = $('.gearbox_make').filter(function () {
                    return this.value != '';
                });
                if (value.length>=0 && (value.length !== reqlength)) {
                    show_notify('Please Select All Gearbox Make. !', false);
                    return false;
                }

                var value = $('.gearbox_gear_type').filter(function () {
                    return this.value != '';
                });
                if (value.length>=0 && (value.length !== reqlength)) {
                    show_notify('Please Select All Gearbox Type. !', false);
                    return false;
                }

                var value = $('.gearbox_model').filter(function () {
                    return this.value != '';
                });
                if (value.length>=0 && (value.length !== reqlength)) {
                    show_notify('Please Select All Gearbox Model. !', false);
                    return false;
                }

                var value = $('.gearbox_ratio').filter(function () {
                    return this.value != '';
                });
                if (value.length>=0 && (value.length !== reqlength)) {
                    show_notify('Please Select All Gearbox Ratio. !', false);
                    return false;
                }
            }
            
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
            
            var item_id = $('#item_id').val();
            var check_item_bom_created_or_not = '1'; 
            $.ajax({
				url: "<?=base_url('challan/check_item_bom_created_or_not') ?>/"+ item_id,
				type: "POST",
				async: false,
				success: function (response) {
					var json = $.parseJSON(response);
					if (json['status'] == '0'){
						if(confirm('BOM Not created for this item.')){
                            check_item_bom_created_or_not = '1';
                        } else {
                            check_item_bom_created_or_not = '0';
                        }
					}
				},
			});
    
            if(check_item_bom_created_or_not == '1'){
                $.ajax({
                    url: "<?=base_url('challan/save_challan') ?>",
                    type: "POST",
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    data: postData,
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.status == 0){
                            show_notify(json.msg,false);
                            $('.module_save_btn').removeAttr('disabled','disabled');
                            return false;
                        }
                        if (json['success'] == 'false'){
                            show_notify(json['msg'],false); 
                            $('.module_save_btn').removeAttr('disabled','disabled');
                        }
                        if (json['success'] == 'Added'){
                            window.location.href = "<?php echo base_url('challan/challan_list') ?>";
                        }
                        if (json['success'] == 'Updated'){
                            window.location.href = "<?php echo base_url('challan/challan_list') ?>";
                        }
                        $("#ajax-loader").hide();
                        return false;
                    },
                });
            } else {
                $("#ajax-loader").hide();
                $('.module_save_btn').removeAttr('disabled','disabled');
            }
            return false;
        });

        $(document).on("change",'#contact_person_id',function() {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?=base_url();?>party/get-contact-person-by-id/"+contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });
        
        <?php if(isset($challan_data->id) && !empty($challan_data->id)){ ?>
            var motor_data = <?=$motor_data;?>;
            //console.log(motor_data);
            $('#motor_detail_fields #motor_fields').not('#motor_fields:first').remove();
            var motor_data_rows = Object.keys(motor_data).length;
            var row_inc = 1;
            $.each(motor_data, function(motor_data_i,motor_data_v){
                $.each(motor_data_v, function(index,value){
                    if(index == 'id'){
                        motor_data_v['challan_motor_id'] = motor_data_v[index];
                        index = 'challan_motor_id';
                    }
                    if(index == 'challan_item_id'){
                        delete motor_data_v[index];
                    }
                    if(value == null){ value = ''; }
                    $('#motor_detail_fields #motor_fields:nth-last-child(1) #'+ index).val(value);
                });
                if(row_inc < motor_data_rows){
                    $('.add_motor_detail').click();
                }
                row_inc++;
            });
            var gearbox_data = <?=$gearbox_data;?>;
            $('#gearbox_detail_fields #gearbox_fields').not('#gearbox_fields:first').remove();
            var gearbox_data_rows = Object.keys(gearbox_data).length;
            var row_inc = 1;
            $.each(gearbox_data, function(gearbox_data_i,gearbox_data_v){
                $.each(gearbox_data_v, function(index,value){
                    if(index == 'id'){
                        gearbox_data_v['challan_gearbox_id'] = gearbox_data_v[index];
                        index = 'challan_gearbox_id';
                    }
                    if(index == 'challan_item_id'){
                        delete gearbox_data_v[index];
                    }
                    if(value == null){ value = ''; }
                    $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) #'+ index).val(value);
                });
                if(row_inc < gearbox_data_rows){
                    $('.add_gearbox_detail').click();
                }
                row_inc++;
            });
        <?php } ?>
        
    });
    
    function feedSalesOrderData(sales_order_id, sales_order_item_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>challan/get_sales_order',
            type: "POST",
            async: false,
            data: {sales_order_id:sales_order_id,sales_order_item_id:sales_order_item_id},
            success: function(data){
                json = JSON.parse(data);
                var sales_order_data = json.sales_order_data;
                var sales_order_item = json.sales_order_item;
                setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id')?>/"+sales_order_data.sales_to_party_id);
                $('#party_name').val($('#party_id').val());
                $('#party_party_id').val(sales_order_data.sales_to_party_id);
                party_details($('#party_id').val());
                $('#quotation_no').val(sales_order_data.quotation_no);
                $('#cust_po_no').val(sales_order_data.cust_po_no);
                $('#po_date').val(sales_order_data.po_date);
                $('#sales_order_no').val(sales_order_data.sales_order_no);
                $('#create_from_name').html(' : Create From Sales Order : ' + sales_order_data.sales_order_no);
                $('#sales_order_date').val(sales_order_data.sales_order_date);
                $('select[name="challan_data[kind_attn_id]"]').val(sales_order_data.kind_attn_id).change();
                
                $('#port_of_loading').val(sales_order_data.port_of_loading_city);
                $('#port_of_discharge').val(sales_order_data.port_of_discharge_city);
                $('#place_of_delivery').val(sales_order_data.place_of_delivery_city);
                if(sales_order_data.port_of_loading_country_id != '' && sales_order_data.port_of_loading_country_id != null){
                    setSelect2Value($("#port_of_loading_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+sales_order_data.port_of_loading_country_id);
                }
                if(sales_order_data.port_of_discharge_country_id != '' && sales_order_data.port_of_discharge_country_id != null){
                    setSelect2Value($("#port_of_discharge_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+sales_order_data.port_of_discharge_country_id);
                }
                if(sales_order_data.place_of_delivery_country_id != '' && sales_order_data.place_of_delivery_country_id != null){
                    setSelect2Value($("#place_of_delivery_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+sales_order_data.place_of_delivery_country_id);
                }
                setSelect2Value($("#delivery_through_id"),'<?=base_url()?>app/set_delivery_through_select2_val_by_id/'+sales_order_data.delivery_through_id);
                $('#name_of_shipment').val(sales_order_data.shipping_line_name);
                
                $('#item_id').val(sales_order_item.item_id);
                $('#item_name').val(sales_order_item.item_name);
                $('#item_code').val(sales_order_item.item_code);
                
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_1']) ? $default_details_arr['m_user_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_1']) ? $default_details_arr['m_make_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_1']) ? $default_details_arr['m_hp_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_1']) ? $default_details_arr['m_kw_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_1']) ? $default_details_arr['m_frequency_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_1']) ? $default_details_arr['m_rpm_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_1']) ? $default_details_arr['m_volts_1'] : '';?>);

                $('.add_motor_detail').click();
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_2']) ? $default_details_arr['m_user_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_2']) ? $default_details_arr['m_make_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_2']) ? $default_details_arr['m_hp_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_2']) ? $default_details_arr['m_kw_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_2']) ? $default_details_arr['m_frequency_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_2']) ? $default_details_arr['m_rpm_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_2']) ? $default_details_arr['m_volts_2'] : '';?>);

                $('.add_motor_detail').click();
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_3']) ? $default_details_arr['m_user_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_3']) ? $default_details_arr['m_make_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_3']) ? $default_details_arr['m_hp_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_3']) ? $default_details_arr['m_kw_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_3']) ? $default_details_arr['m_frequency_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_3']) ? $default_details_arr['m_rpm_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_3']) ? $default_details_arr['m_volts_3'] : '';?>);

                $('.add_motor_detail').click();
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_4']) ? $default_details_arr['m_user_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_4']) ? $default_details_arr['m_make_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_4']) ? $default_details_arr['m_hp_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_4']) ? $default_details_arr['m_kw_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_4']) ? $default_details_arr['m_frequency_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_4']) ? $default_details_arr['m_rpm_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_4']) ? $default_details_arr['m_volts_4'] : '';?>);
            
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_user').val(<?=isset($default_details_arr['g_user_1']) ? $default_details_arr['g_user_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_make').val(<?=isset($default_details_arr['g_make_1']) ? $default_details_arr['g_make_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_gear_type').val(<?=isset($default_details_arr['g_gear_type_1']) ? $default_details_arr['g_gear_type_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_model').val(<?=isset($default_details_arr['g_model_1']) ? $default_details_arr['g_model_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_ratio').val(<?=isset($default_details_arr['g_ratio_1']) ? $default_details_arr['g_ratio_1'] : '';?>);

                $('.add_gearbox_detail').click();
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_user').val(<?=isset($default_details_arr['g_user_2']) ? $default_details_arr['g_user_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_make').val(<?=isset($default_details_arr['g_make_2']) ? $default_details_arr['g_make_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_gear_type').val(<?=isset($default_details_arr['g_gear_type_2']) ? $default_details_arr['g_gear_type_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_model').val(<?=isset($default_details_arr['g_model_2']) ? $default_details_arr['g_model_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_ratio').val(<?=isset($default_details_arr['g_ratio_2']) ? $default_details_arr['g_ratio_2'] : '';?>);
                setSelect2Value($("#item_extra_accessories_id"),'<?=base_url()?>app/set_item_extra_accessories_select2_val_by_id/'+sales_order_item.item_extra_accessories_id);
                $("#ajax-loader").hide();
            },
        });
    }
    
    function feedProformaInvoiceData(proforma_invoice_id, proforma_invoice_item_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>challan/get_proforma_invoice',
            type: "POST",
            async: false,
            data: {proforma_invoice_id:proforma_invoice_id,proforma_invoice_item_id:proforma_invoice_item_id},
            success: function(data){
                json = JSON.parse(data);
                var proforma_invoice_data = json.proforma_invoice_data;
                var proforma_invoice_item = json.proforma_invoice_item;
                setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id')?>/"+proforma_invoice_data.sales_to_party_id);
                $('#party_name').val($('#party_id').val());
                $('#party_party_id').val(proforma_invoice_data.sales_to_party_id);
                party_details($('#party_id').val());
                $('#quotation_no').val(proforma_invoice_data.quotation_no);
                $('#cust_po_no').val(proforma_invoice_data.cust_po_no);
                $('#po_date').val(proforma_invoice_data.po_date);
                $('#proforma_invoice_no').val(proforma_invoice_data.proforma_invoice_no);
                $('#create_from_name').html(' : Create From Proforma Invoice : ' + proforma_invoice_data.proforma_invoice_no);
                $('#proforma_invoice_date').val(proforma_invoice_data.sales_order_date);
                $('select[name="challan_data[kind_attn_id]"]').val(proforma_invoice_data.kind_attn_id).change();
                
                $('#port_of_loading').val(proforma_invoice_data.port_of_loading_city);
                $('#port_of_discharge').val(proforma_invoice_data.port_of_discharge_city);
                $('#place_of_delivery').val(proforma_invoice_data.place_of_delivery_city);
                if(proforma_invoice_data.port_of_loading_country_id != '' && proforma_invoice_data.port_of_loading_country_id != null){
                    setSelect2Value($("#port_of_loading_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+proforma_invoice_data.port_of_loading_country_id);
                }
                if(proforma_invoice_data.port_of_discharge_country_id != '' && proforma_invoice_data.port_of_discharge_country_id != null){
                    setSelect2Value($("#port_of_discharge_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+proforma_invoice_data.port_of_discharge_country_id);
                }
                if(proforma_invoice_data.place_of_delivery_country_id != '' && proforma_invoice_data.place_of_delivery_country_id != null){
                    setSelect2Value($("#place_of_delivery_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+proforma_invoice_data.place_of_delivery_country_id);
                }
                setSelect2Value($("#delivery_through_id"),'<?=base_url()?>app/set_delivery_through_select2_val_by_id/'+proforma_invoice_data.delivery_through_id);
                $('#name_of_shipment').val(proforma_invoice_data.shipping_line_name);
                
                $('#item_id').val(proforma_invoice_item.item_id);
                $('#item_name').val(proforma_invoice_item.item_name);
                $('#item_code').val(proforma_invoice_item.item_code);
                
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_1']) ? $default_details_arr['m_user_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_1']) ? $default_details_arr['m_make_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_1']) ? $default_details_arr['m_hp_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_1']) ? $default_details_arr['m_kw_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_1']) ? $default_details_arr['m_frequency_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_1']) ? $default_details_arr['m_rpm_1'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_1']) ? $default_details_arr['m_volts_1'] : '';?>);

                $('.add_motor_detail').click();
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_2']) ? $default_details_arr['m_user_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_2']) ? $default_details_arr['m_make_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_2']) ? $default_details_arr['m_hp_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_2']) ? $default_details_arr['m_kw_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_2']) ? $default_details_arr['m_frequency_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_2']) ? $default_details_arr['m_rpm_2'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_2']) ? $default_details_arr['m_volts_2'] : '';?>);

                $('.add_motor_detail').click();
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_3']) ? $default_details_arr['m_user_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_3']) ? $default_details_arr['m_make_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_3']) ? $default_details_arr['m_hp_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_3']) ? $default_details_arr['m_kw_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_3']) ? $default_details_arr['m_frequency_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_3']) ? $default_details_arr['m_rpm_3'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_3']) ? $default_details_arr['m_volts_3'] : '';?>);

                $('.add_motor_detail').click();
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_user').val(<?=isset($default_details_arr['m_user_4']) ? $default_details_arr['m_user_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_make').val(<?=isset($default_details_arr['m_make_4']) ? $default_details_arr['m_make_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_hp').val(<?=isset($default_details_arr['m_hp_4']) ? $default_details_arr['m_hp_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_kw').val(<?=isset($default_details_arr['m_kw_4']) ? $default_details_arr['m_kw_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_frequency').val(<?=isset($default_details_arr['m_frequency_4']) ? $default_details_arr['m_frequency_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_rpm').val(<?=isset($default_details_arr['m_rpm_4']) ? $default_details_arr['m_rpm_4'] : '';?>);
                $('#motor_detail_fields #motor_fields:nth-last-child(1) .motor_volts_cycles').val(<?=isset($default_details_arr['m_volts_4']) ? $default_details_arr['m_volts_4'] : '';?>);

                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_user').val(<?=isset($default_details_arr['g_user_1']) ? $default_details_arr['g_user_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_make').val(<?=isset($default_details_arr['g_make_1']) ? $default_details_arr['g_make_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_gear_type').val(<?=isset($default_details_arr['g_gear_type_1']) ? $default_details_arr['g_gear_type_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_model').val(<?=isset($default_details_arr['g_model_1']) ? $default_details_arr['g_model_1'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_ratio').val(<?=isset($default_details_arr['g_ratio_1']) ? $default_details_arr['g_ratio_1'] : '';?>);

                $('.add_gearbox_detail').click();
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_user').val(<?=isset($default_details_arr['g_user_2']) ? $default_details_arr['g_user_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_make').val(<?=isset($default_details_arr['g_make_2']) ? $default_details_arr['g_make_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_gear_type').val(<?=isset($default_details_arr['g_gear_type_2']) ? $default_details_arr['g_gear_type_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_model').val(<?=isset($default_details_arr['g_model_2']) ? $default_details_arr['g_model_2'] : '';?>);
                $('#gearbox_detail_fields #gearbox_fields:nth-last-child(1) .gearbox_ratio').val(<?=isset($default_details_arr['g_ratio_2']) ? $default_details_arr['g_ratio_2'] : '';?>);
                setSelect2Value($("#item_extra_accessories_id"),'<?=base_url()?>app/set_item_extra_accessories_select2_val_by_id/'+proforma_invoice_item.item_extra_accessories_id);
                $("#ajax-loader").hide();
            },
        });
    }
    
    function party_details(id){
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/'+id,
            async: false,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                }else{
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $(".party_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"),'<?=base_url()?>app/set_city_select2_val_by_id/'+json['city_id']);
                }else{
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                }else{
                    setSelect2Value($("#state"));
                }
                setSelect2Value($("#port_of_loading_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/<?=DEFAULT_PORT_OF_LOADING_COUNTRY_ID;?>');
                if (json['country_id']) {
                    setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                    setSelect2Value($("#port_of_discharge_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                    setSelect2Value($("#place_of_delivery_country_id"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                }else{
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                }else{
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }else{
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                }else{
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                }else{
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                }else{
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"),'<?=base_url()?>app/set_reference_select2_val_by_id/'+json['reference_id']);
                }else{
                    setSelect2Value($("#reference_id"));
                }
                if(json['reference_description']){
                    $("#reference_description").html(json['reference_description']);
                }else{
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"),'<?=base_url()?>app/set_sales_select2_val_by_id/'+json['party_type_1_id']);
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
                    }
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
                    }
                    var currency_title = $('#select2-currency_id-container').attr('title');
                    $('#received_payment_currency').html(currency_title);
                }else{
                    setSelect2Value($("#sales_id"));
                }

                <?php if(isset($challan_data->id) && !empty($challan_data->id)){ } else { ?>
                    if (json['branch_id']) {
                        setSelect2Value($("#branch_id"),"<?=base_url('app/set_branch_select2_val_by_id')?>/"+json['branch_id']);
                    }else{
                        setSelect2Value($("#branch_id"));
                    }

                    if (json['agent_id']) {
                        setSelect2Value($("#agent_id"),'<?=base_url()?>app/set_agent_select2_val_by_id/'+json['agent_id']);
                    }else{
                        setSelect2Value($("#agent_id"));
                    }
                <?php } ?>

                if(first_time_edit_mode == 1){
                    if (json['contact_persons_array']) {
                        var option_html = '';
                        if(json['contact_persons_array'].length > 0){
//                            console.log(json['contact_persons_array']);
                            $.each(json['contact_persons_array'],function(index,value){
                                option_html += "<option value='"+value.contact_person_id+"'>"+value.name+"</option>";
                            })
                            $('select[name="challan_data[kind_attn_id]"]').html(option_html).select2();
                        } else {
                            $('select[name="challan_data[kind_attn_id]"]').html('');
                        }
                        $('select[name="challan_data[kind_attn_id]"]').change();
                    }
                } else { first_time_edit_mode = 1; }
                $("#ajax-loader").hide();
            }
        });
    }
    
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
    <?php } ?>
</script>
