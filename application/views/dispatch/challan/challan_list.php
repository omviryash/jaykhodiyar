<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small class='text-primary text-bold'>
				<?php 
				if($this->uri->segment(1) == 'report'){ echo "Report : Dispatch Challan"; }
				if($this->uri->segment(1) == 'challan'){ echo "Dispatch : Challan List"; }
				?>
			</small>
			<?php $challan_add_role = $this->app_model->have_access_role(CHALLAN_MODULE_ID, "add"); ?>
			<?php if($challan_add_role): ?>
			<a href="<?=base_url()?>challan/add/" class="btn btn-info btn-xs pull-right">Add Challan</a>
			<?php endif;?>
		</h1>
	</section>
	<div class="clearfix">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-2">
								<div class="form-group">
									<label for="" class="control-label input-sm">Party type</label>
									<select name="party_type" id="party_type" class="form-control input-sm select2" style="width: 100%;">
										<?php
										$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
										$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
										$isManagement = $this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER,"allow");
										if($isManagement == 1){
											if($cu_accessExport == 1 && $cu_accessDomestic == 1){
										?>
										<option value="all">All</option>
										<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
										<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
										<?php
											}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
										?>
										<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
										<?php
											}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
										?>
										<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
										<?php
											}
										}else{
											if($cu_accessExport == 1 && $cu_accessDomestic == 1){
										?>
										<option value="all">All</option>
										<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
										<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
										<?php
											} else if($cu_accessExport == 1){
										?>
										<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
										<?php
											} else if($cu_accessDomestic == 1){
										?>
										<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
										<?php
											} else {}

										}
										?>								
									</select>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label for="" class="control-label input-sm">City</label>
									<select name="city_id" id="city_id" class="form-control input-sm"></select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="" class="control-label input-sm">State</label>
									<select name="state_id" id="state_id" class="form-control input-sm"></select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="" class="control-label input-sm">Country</label>
									<select name="country_id" id="country_id" class="form-control input-sm"></select>
								</div>
							</div>
                    <?php 
                        if($this->input->get_post("status")){ 
                                $current_status = $this->input->get_post("status"); 
                        } else {
                                $current_status = 'pending';
                        }
                        if($this->input->get_post("user_id")){ 
                                $current_staff = $this->input->get_post("user_id"); 
                        } else {
                                $current_staff = $this->session->userdata('is_logged_in')['staff_id'];
                        }
                    ?>
                    <div class="clearfix"></div>
                    <div class="col-md-2">
                        <label for="" class="control-label input-sm">From Date:</label>
                        <input type="text" name="from_date" id="datepicker1" class="form-control input-sm from_date" value="<?php echo isset($from_date) && !empty($from_date) ? date('d-m-Y', strtotime($from_date)) : ''; ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="" class="control-label input-sm">To Date:</label>
                        <input type="text" name="to_date" id="datepicker2" class="form-control input-sm to_date" value="<?php echo isset($to_date) && !empty($to_date) ? date('d-m-Y', strtotime($to_date)) : ''; ?>">
                    </div>
                    <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
                        <div class="col-md-2">
                            <label for="" class="control-label input-sm">Sales Person:</label>
                            <select class="form-control select_user_id input-sm select2" name="user_id" id="user_id">
                                <option value="all" <?php if(isset($staff_id) && $staff_id == '0'){ echo ' Selected '; } elseif ($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
                                    <?php
                                        if (!empty($users)) {
                                            foreach ($users as $client) {
                                                if (trim($client->name) != "") {
                                                    $selected = '';
                                                    if(isset($staff_id)){
                                                        if(!empty($staff_id) && $staff_id == $client->staff_id){
                                                            $selected = $staff_id == $client->staff_id ? 'selected' : '';
                                                        } 
                                                    } else {
                                                        $selected = $current_staff == $client->staff_id ? 'selected' : '';
                                                    }
                                                        echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
                                                }
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                    <?php } ?>
						</div>
                        <div class="clearfix"></div><br />
						<div class="col-md-12">
							<table id="table_challan_list" class="table custom-table agent-table">
								<thead>
									<tr>
										<th>Action</th>
										<th>Invoice No</th>
										<th>Challan No</th>
										<th>Order / Proforma No.</th>
										<th>Quotation No</th>
										<th>Enquiry No</th>
										<th>Item Code</th>
                                        <th>Party</th>
										<th>Contact No</th>
										<th>Email Id</th>
										<th>Sales Person</th>
										<th>Challan Date</th>
										<th>City</th>
										<th>State</th>
										<th>Country</th>
                                        <th>Party Current Person</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('.select2').select2();
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
			}
		};
        
		table = $('#table_challan_list').DataTable({
            "processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('challan/challan_datatable')?>",
				"type": "POST",
				"data": function(d){
					d.city_id = $("#city_id").val();
					d.state_id = $("#state_id").val();
					d.country_id = $("#country_id").val();
					d.party_type = $("#party_type").val();
                    d.from_date = $('#datepicker1').val();
                    d.to_date = $('#datepicker2').val();
                    d.user_id = $('#user_id').val();
				},
			},
            <?php if($this->applib->have_access_current_user_rights(CHALLAN_MODULE_ID,"export_data")){ ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Challans', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Challans', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Challans', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Challans', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Challans', orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
			"scrollY": 500,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});

		initAjaxSelect2($("#city_id"),"<?=base_url('app/city_select2_source')?>");
		initAjaxSelect2($("#state_id"),"<?=base_url('app/state_select2_source')?>");
		initAjaxSelect2($("#country_id"),"<?=base_url('app/country_select2_source')?>");

		$(document).on("change", "#city_id,#state_id,#country_id,#party_type,.from_date,.select_user_id,.to_date", function() {
			table.draw();
		});
		
		$(document).on("click", ".delete_button", function() {
			var value = confirm('Are you sure delete this records?');
			if (value) {
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=challans',
					success: function(data) {
						table.draw();
						show_notify('Deleted Successfully!', true);
					}
				});
                return false;
			}
            return false;
		});

	});
</script>
