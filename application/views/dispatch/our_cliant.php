<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
            if ($this->uri->segment(1) == 'report') {
                echo "<small class='text-primary text-bold'>Report : Dispatch Challan</small>";
            }
            //echo $this->uri->segment(1);
            if ($this->uri->segment(1) == 'dispatch') {
                echo "<small class='text-primary text-bold'>Dispatch : Customer List</small>";
            }
            $service_add_role = $this->app_model->have_access_role(SERVICE_MODULE_ID, "add");
            $service_view_role = $this->app_model->have_access_role(SERVICE_MODULE_ID, "view");
            ?>
            <?php if ($service_add_role): ?>
                <a href="<?= base_url() ?>service/service" class="btn btn-info btn-xs pull-right" >Add Service</a>                
            <?php endif; ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="party_type" class="control-label input-sm">Party type</label>
                            <select name="party_type" id="party_type" class="form-control input-sm select2" style="width: 100%;">
                                <?php
                                $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export");
                                $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic");
                                $isManagement = $this->applib->have_access_current_user_rights(SALES_ORDER_MANAGEMENT_USER, "allow");
                                if ($isManagement == 1) {
                                    if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                                        ?>
                                        <option value="all">All</option>
                                        <option value="export" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'export' ? 'selected="selected"' : '' ?>>Export</option>
                                        <option value="domestic" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'domestic' ? 'selected="selected"' : '' ?>>Domestic</option>
                                        <?php
                                    } elseif ($cu_accessExport == 1 && $cu_accessDomestic != 1) {
                                        ?>
                                        <option value="export" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'export' ? 'selected="selected"' : '' ?>>Export</option>
                                        <?php
                                    } else if ($cu_accessDomestic == 1 && $cu_accessExport != 1) {
                                        ?>
                                        <option value="domestic" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'domestic' ? 'selected="selected"' : '' ?>>Domestic</option>
                                        <?php
                                    }
                                } else {
                                    if ($cu_accessExport == 1 && $cu_accessDomestic == 1) {
                                        ?>
                                        <option value="all">All</option>
                                        <option value="export" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'export' ? 'selected="selected"' : '' ?>>Export</option>
                                        <option value="domestic" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'domestic' ? 'selected="selected"' : '' ?>>Domestic</option>
                                        <?php
                                    } else if ($cu_accessExport == 1) {
                                        ?>
                                        <option value="export" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'export' ? 'selected="selected"' : '' ?>>Export</option>
                                        <?php
                                    } else if ($cu_accessDomestic == 1) {
                                        ?>
                                        <option value="domestic" <?= isset($_GET['party_type']) && $_GET['party_type'] == 'domestic' ? 'selected="selected"' : '' ?>>Domestic</option>
                                        <?php
                                    } else {
                                        
                                    }
                                }
                                ?>								
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="table_our_cliant_list" class="table custom-table agent-table">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Order No</th>
                                    <th>Quotation No</th>
                                    <th>Enquiry No</th>
                                    <th>Party Name</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Machine Serial No.</th>
                                    <th>Challan Date</th>
                                    <th>Item Name</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Action</th>
                                    <th>Order No</th>
                                    <th>Quotation No</th>
                                    <th>Enquiry No</th>
                                    <th>Party Name</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Machine Serial No.</th>
                                    <th>Challan Date</th>
                                    <th>Item Name</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        table = $('#table_our_cliant_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dispatch/cliant_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                    d.party_type = $("#party_type").val();
                },
            },
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            }
        });

        $(document).on("change", "#party_type", function () {
            table.draw();
        });

        $(document).on('click', '.show_itemprintplat_modal', function (e) {
            e.preventDefault();
            var challan_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();
            $("#items_td").html('');
            $.ajax({
                url: '<?php echo base_url('dispatch/challan_items/') ?>/' + challan_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.item_data) {
                        jQuery.each(data.item_data, function (i, val) {
                            console.log(val);
                            $("#items_td").append('<tr><td>' + val.item_name + '</td><td><a href="<?php echo base_url('dispatch/challan_plat_print/'); ?>/' + challan_id + '?item=' + val.id + '" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>');
                        });
                    }
                }
            });
        });

        $(document).on('click', '.show_itemprint_modal', function (e) {
            e.preventDefault();
            var challan_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();
            $("#items_td").html('');
            $.ajax({
                url: '<?php echo base_url('dispatch/challan_items/') ?>/' + challan_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.item_data) {
                        jQuery.each(data.item_data, function (i, val) {
                            console.log(val);
                            $("#items_td").append('<tr><td>' + val.item_name + '</td><td><a href="<?php echo base_url('dispatch/challan_print/'); ?>/' + challan_id + '?item=' + val.id + '" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>');
                        });
                    }
                }
            });
        });

    });
</script>
