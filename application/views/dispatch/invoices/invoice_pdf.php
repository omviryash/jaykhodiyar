<?php $column_cnt = 18; ?>
<html>
    <head>
        <title><?=$invoice_data->invoice_type?> Invoice</title>
        <style>
            .text-center{
            text-align: center;
            }
            table{
            border-spacing: 0;
            width: 100%;
            border-bottom: 1px solid;
            border-right: 1px solid;
            }
            td{
            padding: 1px 1px 1px 1px;
            border-left: 1px solid;
            border-top: 1px solid;
            font-size:10px;
            }
            tr > td:last-child{
            border-right: 1px solid !important;
            }
            tr:last-child > td{
            border-bottom: 1px solid !important;
            }
            .text-right{
            text-align: right;
            }
            .text-bold{
            font-weight: 900 !important;
            font-size:12px !important;
            }
            .text-header{
            font-size: 20px;
            }
            .no-border-top{
            border-top:0;
            }
            .no-border-bottom{
            border-bottom:0 !important;
            }
            .no-border-left{
            border-left:0;
            }
            .no-border-right{
            border-right:0;
            }
            .width-50-pr{
            width:50%;
            }
            td.footer-sign-area{
            height: 100px;
            vertical-align: bottom;
            width: 33.33%;
            text-align: center;
            }
            .no-border{
            border: 0!important;
            }
            .footer-detail-area{
            color: #698c66;
            font-size: 12px;
            }
        </style>
    </head>
    <body>
        <?php
            $CURRENCY = "INR";
            setlocale(LC_MONETARY, 'en_IN');
            if($invoice_data->currency != ''){
            	$CURRENCY = strtoupper($invoice_data->currency);
            }
		?>
        <?php
            if(isset($letterpad_details->header_logo)){
				echo '<div style="text-align: '.$letterpad_details->header_logo_alignment.';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>';
			}
        ?>
        <?php if ($invoice_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) { ?>
        <table>
            <tr>
                <td class="text-center text-bold text-header" colspan="4"><?=$invoice_data->invoice_type?> Invoice</td>
            </tr>
            <tr>
                <td class="text-center text-bold"><b>Manufacturer & Supplier</b></td>
                <td class="text-center text-bold" colspan="3"><b>Cash / Debit Memo</b></td>
            </tr>
            <tr>
                <td class="text-bold" style="font-size:15px;"><b><?=$company_details['name'];?></b></td>
                <td class="text-bold"  align="left" width="340px">Invoice : </td>
                <td class="text-bold" colspan="2">
                    <b>Original &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp; Duplicate &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp; Triplicate</b>
                </td>
            </tr>
            <tr>
                <td class="no-border-top text-bold" rowspan="5" >
                    <?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
                    <b>GST No : <?=$company_details['gst_no'];?></b>
                </td>
                <td class="text-bold"  align="left">Invoice No. : </td>
                <td class="text-bold"  align="left"><b><?=$invoice_data->invoice_no;?></b></td>
                <td class="text-bold"  align="left"><b>
                    <?=strtotime($invoice_data->invoice_date) != 0?date('d/m/Y',strtotime($invoice_data->invoice_date)):'';?></b>
                </td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Challan No. : </td>
                <td class="text-bold" ><b>
                    <?=$sales_order_data->challan_no; ?></b>
                </td>
                <td class="text-bold" ><b>
                    <?=$sales_order_data->challan_date; ?></b> 
                </td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Quotation No. : </td>
                <td class="text-bold" colspan="2"><b>
                    <?=$sales_order_data->quotation_no;?></b>
                    <?php //$this->applib->get_quotation_ref_no($invoice_data->quotation_no,$sales_order_item['item_code']);?>
                </td>
            </tr>
            <tr>
                <td class=" text-bold"  align="left">Purchase Order No.: </td>
                <td class=" text-bold" ><b>
                    <?=$sales_order_data->cust_po_no; ?></b> 
                </td>
                <td class="text-bold" ><b>
                    <?=$sales_order_data->po_date; ?></b> 
                </td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Salse Order No. : </td>
                <td class="text-bold"><b>
                    <?=$this->applib->get_sales_order_no($sales_order_data->sales_order_no);?></b>
                </td>
                <td class="text-bold"><b>
                    <?=$sales_order_data->sales_order_date;?></b>
                </td>
            </tr>
            <tr>
                <td class="text-center text-bold width-50-pr"><b>Purchaser</b></td>
                <td class="text-center text-bold width-50-pr" colspan="3"><b>Mode Of Shipment</b></td>
            </tr>
            <tr>
                <td class="text-bold" rowspan="10" valign="top">
                    <b style="font-size:15px;"><?=$invoice_data->party_name?></b><br />
                    <?=nl2br($invoice_data->address);?><br />
                    City : <?=$invoice_data->city?> -  <?=$invoice_data->pincode?> (<?=$invoice_data->state?>) <?=$invoice_data->country?>.<br />
                    Email : <?=$invoice_data->party_email_id;?><br />
                    Tel No.:  <?=$invoice_data->fax_no;?>,<br />
                    Contact No.:  <?=$invoice_data->p_phone_no;?><?= ($invoice_data->p_phone_no != $invoice_data->contact_person_phone) ? ', '.$invoice_data->contact_person_phone : '' ?><br />
                    Contact Person: <?=$invoice_data->contact_person_name?><br />
                    <b>GST No. : <?=$invoice_data->party_gst_no;?></b><?= isset($invoice_data->party_cin_no) && !empty($invoice_data->party_cin_no) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$invoice_data->party_cin_no.'</b>' : ''; ?>
                </td>
                <td class=" text-bold">Loading At : </td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->loading_at?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Port of Loading : </td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->port_of_loading?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Port of  Discharge : </td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->port_of_discharge?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Place of Delivery : </td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->port_place_of_delivery?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Delivery Through : </td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->delivery_through?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Name of shipment : </td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->name_of_shipment?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Truck / Container No. :</td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->vehicle_no;?></b></td>
            </tr>
            <tr>
                <td class=" text-bold">LR / BL No. : </td>
                <td class=" text-bold"><b><?=$invoice_data->lr_no;?></b></td>
                <td class=" text-bold" ><b><?=$invoice_data->lr_date;?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Contact Person :</td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->contact_person;?> - <?=$invoice_data->contact_person_no;?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" >Driver Name : </td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->driver_name;?> - <?=$invoice_data->driver_contact_no;?></b></td>
            </tr>
            <tr>
                <td class="text-left text-bold " colspan="4">&nbsp;</td>
            </tr>
        </table>
       
        <table>
            <tr>
                <td class="no-border-top text-bold" colspan="1" align="center" width="20px"><b>Sr.No.</b> </td>
                <td class="no-border-top text-bold" colspan="5" align="center" width="200px"><b>Item Name</b> </td>
                <td class="no-border-top text-bold" colspan="2" align="center">&nbsp;&nbsp;<b>HSN</b>&nbsp;&nbsp;</td>
                <td class="no-border-top text-bold" colspan="1" align="center" width="90px"><b>Unit / Nos</b></td>
                <td class="no-border-top text-bold" colspan="2" align="center" width="40px"><b>Rate</b></td>
                <td class="no-border-top text-bold" colspan="1" align="center" width="40px"><b>&nbsp;Discount&nbsp;</b> </td>
                <td class="no-border-top text-bold" colspan="1" align="center" width="22px"><b>&nbsp;CGST&nbsp;</b> </td>
                <td class="no-border-top text-bold" colspan="1" align="center" width="22px"><b>SGST</b> </td>
                <td class="no-border-top text-bold" colspan="1" align="center" width="22px"><b>&nbsp;IGST&nbsp;</b> </td>
                <td class="no-border-top text-bold" colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;<b>Amount</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <?php
                $pure_amount = 0;
                $discount_total = 0;
                $taxable_amount = 0;
                $invoice_cgst_amount = 0;
                $invoice_sgst_amount = 0;
                $invoice_igst_amount = 0;
                $gst_total = 0;
                $grand_total = 0;
                if (!empty($invoice_items)) {
                    $invoice_item_count = count($invoice_items);
                    $i_inc = 1;
                    foreach ($invoice_items as $key => $item_row) {
            ?>
                        <tr>
                            <td class=" text-bold" colspan="1" align="center"><?=$i_inc?></td>
                            <td class=" text-bold" colspan="5"><?=$item_row['item_name'];?><br /><?=$item_row['item_extra_accessories'];?></td>
                            <td class="text-center text-bold" colspan="2"><?=$item_row['hsn_no'];?></td>
                            <td class="text-center text-bold" colspan="1"><?=$item_row['quantity'];?></td>
                            <td class="text-right text-bold" colspan="2"><?=money_format('%!i', $item_row['rate']);?></td>
                            <td class="text-center text-bold" colspan="1"><?=$item_row['disc_per'];?>%</td>
                            <td class="text-center text-bold" colspan="1"><?=$item_row['cgst'];?>%</td>
                            <td class="text-center text-bold" colspan="1"><?=$item_row['sgst'];?>%</td>
                            <td class="text-center text-bold" colspan="1"><?=$item_row['igst'];?>%</td>
                            <td class="text-right text-bold" colspan="3"><?=money_format('%!i', $item_row['net_amount']);?></td>
                        </tr>
            <?php
                        $i_inc++;
                        $discount_total += $item_row['disc_value'];
                        $pure_amount += $item_row['quantity'] * $item_row['rate'];
                        $invoice_cgst_amount += $item_row['cgst_amount'];
                        $invoice_sgst_amount += $item_row['sgst_amount'];
                        $invoice_igst_amount += $item_row['igst_amount'];
                        $gst_total += $item_row['cgst_amount'] + $item_row['sgst_amount'] + $item_row['igst_amount'];
                    }
                }
            
                $taxable_amount = $pure_amount - $discount_total;

                $invoice_data->freight = !empty($invoice_data->freight)?$invoice_data->freight:'0';
                $invoice_data->packing_forwarding = !empty($invoice_data->packing_forwarding)?$invoice_data->packing_forwarding:'0';
                $invoice_data->transit_insurance = !empty($invoice_data->transit_insurance)?$invoice_data->transit_insurance:'0';
                $total_amount = $taxable_amount + $invoice_data->freight + $invoice_data->packing_forwarding + $invoice_data->transit_insurance;

                $invoice_data->freight_cgst = !empty($invoice_data->freight_cgst)?$invoice_data->freight_cgst:'0';
                $freight_cgst_amount = $invoice_data->freight * $invoice_data->freight_cgst / 100;
                $invoice_data->freight_sgst = !empty($invoice_data->freight_sgst)?$invoice_data->freight_sgst:'0';
                $freight_sgst_amount = $invoice_data->freight * $invoice_data->freight_sgst / 100;
                $invoice_data->freight_igst = !empty($invoice_data->freight_igst)?$invoice_data->freight_igst:'0';
                $freight_igst_amount = $invoice_data->freight * $invoice_data->freight_igst / 100;
                $freight_gst_amount = $freight_cgst_amount + $freight_sgst_amount + $freight_igst_amount;
                $freight_amount = $invoice_data->freight + $freight_gst_amount;

                $invoice_data->packing_forwarding_cgst = !empty($invoice_data->packing_forwarding_cgst)?$invoice_data->packing_forwarding_cgst:'0';
                $packing_forwarding_cgst_amount = $invoice_data->packing_forwarding * $invoice_data->packing_forwarding_cgst / 100;
                $invoice_data->packing_forwarding_sgst = !empty($invoice_data->packing_forwarding_sgst)?$invoice_data->packing_forwarding_sgst:'0';
                $packing_forwarding_sgst_amount = $invoice_data->packing_forwarding * $invoice_data->packing_forwarding_sgst / 100;
                $invoice_data->packing_forwarding_igst = !empty($invoice_data->packing_forwarding_igst)?$invoice_data->packing_forwarding_igst:'0';
                $packing_forwarding_igst_amount = $invoice_data->packing_forwarding * $invoice_data->packing_forwarding_igst / 100;
                $packing_forwarding_gst_amount = $packing_forwarding_cgst_amount + $packing_forwarding_sgst_amount + $packing_forwarding_igst_amount;
                $packing_forwarding_amount = $invoice_data->packing_forwarding + $packing_forwarding_gst_amount;

                $invoice_data->transit_insurance_cgst = !empty($invoice_data->transit_insurance_cgst)?$invoice_data->transit_insurance_cgst:'0';
                $transit_insurance_cgst_amount = $invoice_data->transit_insurance * $invoice_data->transit_insurance_cgst / 100;
                $invoice_data->transit_insurance_sgst = !empty($invoice_data->transit_insurance_sgst)?$invoice_data->transit_insurance_sgst:'0';
                $transit_insurance_sgst_amount = $invoice_data->transit_insurance * $invoice_data->transit_insurance_sgst / 100;
                $invoice_data->transit_insurance_igst = !empty($invoice_data->transit_insurance_igst)?$invoice_data->transit_insurance_igst:'0';
                $transit_insurance_igst_amount = $invoice_data->transit_insurance * $invoice_data->transit_insurance_igst / 100;
                $transit_insurance_gst_amount = $transit_insurance_cgst_amount + $transit_insurance_sgst_amount + $transit_insurance_igst_amount;
                $transit_insurance_amount = $invoice_data->transit_insurance + $transit_insurance_gst_amount;

                $cgst_amount = $invoice_cgst_amount + $freight_cgst_amount + $packing_forwarding_cgst_amount + $transit_insurance_cgst_amount;
				$sgst_amount = $invoice_sgst_amount + $freight_sgst_amount + $packing_forwarding_sgst_amount + $transit_insurance_sgst_amount;
				$igst_amount = $invoice_igst_amount + $freight_igst_amount + $packing_forwarding_igst_amount + $transit_insurance_igst_amount;
				$total_gst_amount = $invoice_cgst_amount + $invoice_sgst_amount + $invoice_igst_amount + $freight_gst_amount + $packing_forwarding_gst_amount + $transit_insurance_gst_amount;
				$grand_total = $total_amount + $total_gst_amount;
            ?>
            <?php
                for($blank_row = 10; $blank_row > $invoice_item_count; $blank_row--){
            ?>
                    <tr>
                        <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                        <td class=" text-bold" colspan="5"></td>
                        <td class=" text-bold" colspan="2"></td>
                        <td class=" text-bold" colspan="1"></td>
                        <td class=" text-bold" colspan="2"></td>
                        <td class=" text-bold" colspan="1"></td>
                        <td class=" text-bold" colspan="1"></td>
                        <td class=" text-bold" colspan="1"></td>
                        <td class=" text-bold" colspan="1"></td>
                        <td class=" text-bold" colspan="3" align="center"></td>
                    </tr>
            <?php } ?>
            <tr>
                <td class="" colspan="12" rowspan="5" valign="top">
                    <table class="no-border">
                        <tr>
                            <td align="center" class="no-border-top no-border-left" width="140px" >GST Summary</td>
                            <td align="center" class="no-border-top" >Taxable Amt</td>
                            <td align="center" class="no-border-top" >CGST</td>
                            <td align="center" class="no-border-top" >SGST</td>
                            <td align="center" class="no-border-top" >IGST</td>
                            <td align="center" class="no-border-top" width="60px" >Total</td>
                        </tr>
                        <?php
							if (!empty($invoice_items)) {
								foreach ($invoice_items as $key => $i_row) {
									$p_amount = $i_row['quantity'] * $i_row['rate'];
									$t_amount = $p_amount - $i_row['disc_value'];
						?>
									<tr>
										<td class="no-border-left" ><?=$i_row['item_name'];?></td>
										<td align="right"><?=money_format('%!i', $t_amount);?></td>
										<td align="right"><?=money_format('%!i', $i_row['cgst_amount']);?> : @<?=$i_row['cgst'];?>%</td>
										<td align="right"><?=money_format('%!i', $i_row['sgst_amount']);?> : @<?=$i_row['sgst'];?>%</td>
										<td align="right"><?=money_format('%!i', $i_row['igst_amount']);?> : @<?=$i_row['igst'];?>%</td>
										<td align="right"><?=money_format('%!i', $i_row['net_amount']);?></td>
									</tr>
                        <?php 
								}
							}
						?>
                        <tr>
                            <td class="no-border-left" >Freight</td>
                            <td align="right"><?=money_format('%!i', $invoice_data->freight);?></td>
                            <td align="right"><?=money_format('%!i', $freight_cgst_amount);?> : @<?=$invoice_data->freight_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_sgst_amount);?> : @<?=$invoice_data->freight_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_igst_amount);?> : @<?=$invoice_data->freight_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" >Packing & Forwarding</td>
                            <td align="right"><?=money_format('%!i', $invoice_data->packing_forwarding);?></td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_cgst_amount);?> : @<?=$invoice_data->packing_forwarding_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_sgst_amount);?> : @<?=$invoice_data->packing_forwarding_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_igst_amount);?> : @<?=$invoice_data->packing_forwarding_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" >Transit Insurance</td>
                            <td align="right"><?=money_format('%!i', $invoice_data->transit_insurance);?></td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_cgst_amount);?> : @<?=$invoice_data->transit_insurance_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_sgst_amount);?> : @<?=$invoice_data->transit_insurance_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_igst_amount);?> : @<?=$invoice_data->transit_insurance_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" ><b>Total</b></td>
                            <td align="right"><?=money_format('%!i', $total_amount);?></td>
                            <td align="right"><?=money_format('%!i', $cgst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $sgst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $igst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $grand_total);?></td>
                        </tr>
                    </table>
                </td>
                <td class=" text-bold" colspan="3">Sub Total : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $pure_amount);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Discount : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $discount_total);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Sub Total : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $taxable_amount);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Freight : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $invoice_data->freight);?></b></td>
            </tr>
            <tr>
		<td class=" text-bold" colspan="3">P & F : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $invoice_data->packing_forwarding);?></b></td>
            </tr>
            <tr>
		<td class=" text-bold" colspan="12" valign="top"><b>In Words <?=$CURRENCY?> : </b><?=money_to_word($grand_total)?> only.</td>
                <td class=" text-bold" colspan="3">Insurance : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $invoice_data->transit_insurance);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="12" rowspan="5" valign="top"><b>Terms & Conditions : </b><br /><?=$invoice_data->invoice_terms_and_conditions;?></td>
                <td class=" text-bold" colspan="3">Total : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $total_amount);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">GST : </td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $total_gst_amount);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3"><b>Grand Total : </b></td>
                <td class=" text-bold" colspan="3" align="right"><b><?=money_format('%!i', $grand_total);?></b></td>
            </tr>
            <tr>
				<td class=" text-bold" colspan="6" align="center">
					<b>For, <?=$company_details['name'];?></b>
                    <br /><br /><br /><br /><br /><br /><br />
                </td>
			</tr>
			<tr>
				<td class="no-border-top text-bold" colspan="6" align="center">
					<div style="text-align:center;">Authorized Signature</div>
				</td>
			</tr>
        </table>
        <?php } else if($invoice_data->party_type_1 == PARTY_TYPE_EXPORT_ID){ ?>
        <table>
            <tr>
                <td class="text-center text-bold text-header" colspan="8">COMMERCIAL INVOICE</td>
            </tr>
            <tr>
                <td class="text-center text-bold"colspan="4" width=""><b>Consigner</b></td>
                <td class="text-bold" colspan="1">Invoice No. :</td>
                <td class="text-bold" colspan="2"><b><?=$invoice_data->invoice_no;?></b></td>
                <td class="text-center text-bold" colspan="1"><b><?=strtotime($invoice_data->invoice_date) != 0?date('d/m/Y',strtotime($invoice_data->invoice_date)):'';?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4" style="font-size:15px;"><b><?=$company_details['name'];?></b></td>
                <td class="text-bold" colspan="1"  align="left" width="140px">Quotation No. :</td>
                <td class="text-bold" colspan="2"><b><?=$sales_order_data->quotation_no;?></b></td>
                <td class="text-center text-bold" colspan="1"><b><?=$sales_order_data->quotation_date; ?></b></td>
            </tr>
            <tr>
                <td class="no-border-top text-bold" colspan="4" rowspan="4">
                    <?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
                    <br/>
                </td>
                <td class="text-bold"  align="left" colspan="1" >Purchase Order No.:</td>
                <td class="text-bold"  align="left" colspan="2"><b><?=$sales_order_data->cust_po_no; ?></b></td>
                <td class="text-center text-bold" align="left" colspan="1"><center><b><?=$sales_order_data->po_date; ?></b></center></td>
            </tr>
            <tr>
                <?php if(!empty($invoice_data->sales_order_id)){ ?>
                    <td class=" text-bold"  align="left">Salse Order No. : </td>
                    <td class=" text-bold" colspan="2" >
                        <b><?=$this->applib->get_sales_order_no($sales_order_data->sales_order_no);?></b>
                    </td>
                    <td class="text-bold" colspan="1" >
                        <b><center><?=$sales_order_data->sales_order_date;?></center></b>
                    </td>
                <?php } else { ?>
                    <td class=" text-bold"  align="left">Proforma Invoice No. : </td>
                    <td class=" text-bold" colspan="2" >
                        <b><?=$sales_order_data->proforma_invoice_no;?></b>
                    </td>
                    <td class="text-bold" colspan="1" >
                        <b><center><?=$sales_order_data->sales_order_date;?></center></b>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <td class=" text-bold"  align="left" colspan="1">IEC No. :</td>
                <td class=" text-bold" colspan="3" ><b><?=$company_details['iec_no'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold"  align="left" colspan="1">GST No. :</td>
                <td class=" text-bold" colspan="3" ><b><?=$company_details['gst_no'];?></b></td>
            </tr>
            <tr>
				<td class="text-bold"  align="left" colspan="1">END USE CODE. :</td>
                <td class="text-bold"  align="left" colspan="3"><b><?=$company_details['end_use_code'];?></b></td>
                <td class="text-bold" colspan="1">PAN No. :</td>
                <td class="  text-bold" colspan="3"><b><?=$company_details['pan_no'];?></b></td>
            </tr>
            <tr>
				<td class="text-bold"  align="left" colspan="1" rowspan="2">LUT. :</td>
                <td class="text-bold"  align="left" colspan="3" rowspan="2"><b><?=$company_details['lut'];?></b></td>				
				<td class="text-bold" colspan="1">L.C.No. :</td>
                <td class=" text-bold" colspan="2"><b><?=$invoice_data->lc_no; ?></b></td>
                <td class=" text-bold" colspan="1"><center><b><?=$invoice_data->lc_date; ?></b></center></td>
            </tr>
             <tr>
                 <td class="text-bold"  align="left" colspan="1">Sea Freight :</td>
                 <td class="text-bold"  align="left" colspan="3"><b><?=$invoice_data->sea_freight_type; ?></b></td>
            </tr>
             <tr>
                 <td class="text-center text-bold" colspan="4"><b>Consignee</b></td>
                <td class="text-center text-bold" colspan="4"><b>Terms of Payment & Delivery:</b></td>
            </tr>
            <?php if(!empty(trim($invoice_data->lc_no))){ ?>
                <tr>
                    <td class="text-bold" colspan="4" valign="top">
						<?php
						$consignee_info = $invoice_data->consignee_info;
						$consignee_info_arr = explode("\n", $consignee_info);
						$first = array_slice($consignee_info_arr, 0, 1);
						$others_char = array_slice($consignee_info_arr, 1);
						$others_char = implode("\n", $others_char);
						?>
						<b style="font-size:15px;"><?=$first[0];?></b><br/>
                        <?=nl2br($others_char); ?>
                    </td>
                    <td class="text-bold" colspan="4" valign="top"><?=$invoice_data->supplier_payment_terms;?></td>
                </tr>
                <tr>
                    <td class=" text-bold" colspan="4"><b>Buyer (If Other than Consingee) or Applicant Party</b></td>
                    <td class="text-center text-bold" colspan="4"><b>Our Bank Accounts Details</b></td>
                </tr>
                <tr>
                    <td class="text-bold " colspan="4" valign="top">
                        <b style="font-size:15px;"><?=$invoice_data->party_name?></b><br />
                        <?=nl2br($invoice_data->address);?><br />
                        City : <?=$invoice_data->city?> -  <?=$invoice_data->pincode?> (<?=$invoice_data->state?>) <?=$invoice_data->country?>.<br />
                        Email : <?=explode(",", $invoice_data->party_email_id)[0];?><br />
                        Tel No.:  <?=$invoice_data->fax_no;?>,<br />
                        Contact No.:  <?=$invoice_data->p_phone_no;?>, <?=$invoice_data->contact_person_phone?><br />
                        Contact Person: <?=$invoice_data->contact_person_name?><br />
                    </td>
                    <td class="text-bold" colspan="4" valign="top">
                        <?=$invoice_data->banks_detail?>
                    </td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td class="text-bold" colspan="4" rowspan="3" valign="top">
                        <b style="font-size:15px;"><?=$invoice_data->party_name?></b><br />
                        <?=nl2br($invoice_data->address);?><br />
                        City : <?=$invoice_data->city?> -  <?=$invoice_data->pincode?> (<?=$invoice_data->state?>) <?=$invoice_data->country?>.<br />
                        Email : <?=explode(",", $invoice_data->party_email_id)[0];?><br />
                        Tel No.:  <?=$invoice_data->fax_no;?>,<br />
                        Contact No.:  <?=$invoice_data->p_phone_no;?>, <?=$invoice_data->contact_person_phone?><br />
                        Contact Person: <?=$invoice_data->contact_person_name?><br />
                    </td>
                    <td class="text-bold" colspan="4" valign="top"><?=$invoice_data->supplier_payment_terms;?></td>
                </tr>
                <tr>
                    <td class="text-center text-bold" colspan="4"><b>Our Bank Accounts Details</b></td>
                </tr>
                <tr>
                    <td class="text-bold" colspan="4"  valign="top">
                        <?=$invoice_data->banks_detail?>
                    </td>
                </tr>
                
            <?php } ?>
            <?php if(count($invoice_items) == 1) { ?>
            <tr>
                <td class="text-bold text-center" colspan="8"><b>&nbsp;</b></td>
            </tr>
            <?php } ?>    
            <?php
                if (!empty($invoice_items)) {
                    foreach ($invoice_items as $key => $item_row) {
            ?>
                        <tr>
                            <td class="text-bold" colspan="4"><b><?=$item_row['item_name'];?></b></td>
                            <td class=" text-bold " colspan="1">Serial No. :</td>
                            <td class="text-bold " colspan="3"><b><?=$item_row['item_serial_no'];?></b></td>
                        </tr>
            <?php } } ?>
        </table>
        <table>
            <?php if(count($invoice_items) < 3) { ?>   
            <tr>
                <td class="text-bold text-center no-border-top" colspan="30"><b>&nbsp;</b></td>
            </tr>
            <?php } ?>    
            <tr>
                <td class="text-bold text-center" colspan="30"><b>Dispatch Details</b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="5" width="100px">Loading At :</td>
                <td class="text-bold text-center" colspan="10"><b><?=$invoice_data->loading_at;?></b></td>
                <td class="text-bold" colspan="5" width="100px">Port Of Discharge :</td>
                <td class="text-bold text-center" colspan="5" width="100px"><b><?=$invoice_data->port_of_discharge;?></b></td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->port_of_discharge_country;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="5">Port Of Loading :</td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->port_of_loading;?></b></td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->port_of_loading_country;?></b></td>
                <td class="text-bold" colspan="5">Place Of Delivery :</td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->port_place_of_delivery;?></b></td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->place_of_delivery_country;?></b></td>
           </tr>
            <tr>
                <td class="text-bold" colspan="5">Delivery through :</td>
                <td class="text-bold text-center" colspan="10"><b><?=$invoice_data->delivery_through;?></b></td>
                <td class="text-bold" colspan="5">Shipping Line Name :</td>
                <td class="text-bold text-center" colspan="10"><b><?=$invoice_data->name_of_shipment;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="5" >Country of Origin of Goods :</td>
                <td class="text-bold text-center" colspan="10"><b><?=$invoice_data->origin_of_goods_country;?></b></td>
                <td class="text-bold" colspan="5">Country of final Destination :</td>
                <td class="text-bold text-center" colspan="10"><b><?=$invoice_data->final_destination_country;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="5">Container No. :</td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->vehicle_no;?></b></td>
                <td class="text-bold" colspan="5">Line Seal No. :</td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->line_seal_no;?></b></td>
                <td class="text-bold" colspan="5" >Exporter Seal No. :</td>
                <td class="text-bold text-center" colspan="5"><b><?=$invoice_data->exporter_seal_no;?></b></td>
            </tr>-->
        </table>
        <table>
            <?php if(!empty(trim($invoice_data->lc_no))){ } else { ?>
            <tr><td class="no-border-top" colspan="21">&nbsp;</td></tr>
            <?php } ?>
            <tr>
                <td class="text-bold"  colspan="4" align="center"><b>Marks and Nos of Packs</b></td>
                <td class="text-bold"  colspan="1" align="center"><b>Sr No.</b></td>
                <td class="text-bold"  colspan="8" align="center"><b>Item Name</b></td>
                <td class="text-bold"  colspan="2" align="center"><b>HSN Code</b></td>
                <td class="text-bold"  colspan="2" align="center"><b>Rate <?=$CURRENCY?></b></td>
                <td class="text-bold"  colspan="2" align="center"><b>Unit / Nos</b></td>
                <td class="text-bold"  colspan="2" align="center" ><b>Amount <?=$CURRENCY?></b></td>
            </tr>
            <?php
                $net_amount = 0;
                $grand_total = 0;
                if (!empty($invoice_items)) {
                    $invoice_item_count = count($invoice_items);
                    $i_inc = 1;
                    foreach ($invoice_items as $key => $item_row) {
            ?>
                        <tr>
                            <td class=" text-bold" colspan="4" align="left">
                                <?=$item_row['packing_mark'];?> <br />
                                <?=(!empty($item_row['no_of_packing'])) ? '1 To '.$item_row['no_of_packing'] : '';?> <br />
                                <?=(!empty($item_row['gross_weight'])) ? 'Gross Wt '.$item_row['gross_weight'].' '.$item_row['gross_weight_uom'] : '';?> <br />
                                <?=(!empty($item_row['net_weight'])) ? 'Net Wt '.$item_row['net_weight'].' '.$item_row['net_weight_uom'] : '';?> <br />
                            </td>
                            <td class=" text-bold" colspan="1" align="center"><?=$i_inc?></td>
                            <td class=" text-bold" colspan="8" align="left"><?=$item_row['item_name'];?><br /><?=$item_row['item_extra_accessories'];?></td>
                            <td class=" text-bold" colspan="2" align="center" width="30px"><?=$item_row['hsn_no'];?></td>
                            <td class=" text-bold" colspan="2" align="right">&nbsp;<?=number_format((float)$item_row['rate'], 2, '.', ''); ?>&nbsp;</td>
                            <td class=" text-bold" colspan="2" align="center"><?=$item_row['quantity'];?></td>
                            <td class=" text-bold" colspan="2" align="right"><?=number_format((float)$item_row['amount'], 2, '.', ''); ?></td>
                        </tr>
            <?php
                        $i_inc++;
                        $net_amount += $item_row['amount'];
                    }
                }
                $grand_total = $net_amount + $invoice_data->packing_forwarding + $invoice_data->sea_freight;
            ?>
            
            <tr>
                <td class=" text-bold" colspan="16"><b>In Words <?=$CURRENCY?> : </b> <?=money_to_word($grand_total)?> only.</td>
                <td class=" text-bold" colspan="3" align="right">Ex-Factory</td>
                <td class=" text-bold" colspan="2" align="right"><b><?=number_format((float)$net_amount, 2, '.', ''); ?></b></td>
            </tr>
            <tr>
                <td class="text-bold no-border-bottom" colspan="16" rowspan="6" valign="top"><b>Declaration:</b> <?=$invoice_data->sales_invoice_declaration_for_export;?></td>
                <td class=" text-bold" colspan="3" align="right">Packing & Forwarding</td>
                <td class=" text-bold" colspan="2" align="right"><b><?=number_format((float)$invoice_data->packing_forwarding, 2, '.', ''); ?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3" align="right">Sea Freight (<?=$invoice_data->sea_freight_type; ?>) <?=$invoice_data->port_of_discharge; ?></td>
                <td class=" text-bold" colspan="2" align="right"><b><?=number_format((float)$invoice_data->sea_freight, 2, '.', ''); ?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3" align="right"><b>Total <?=$CURRENCY?></b></td>
                <td class=" text-bold" colspan="2" align="right"><b><?=number_format((float)$grand_total, 2, '.', ''); ?></b></td>
            </tr>
            <tr>
                <td class=" text-bold no-border-bottom" colspan="5" align="center" valign="top"><b>For, <?=$company_details['name'];?> </td>
            </tr> 
            <tr>
                <td class=" text-bold no-border-top no-border-bottom text-center" colspan="5">
                    <br /><br /><br /><br /><br /><br /><br />
                </td>
            </tr>
            <tr>
                <td class=" text-bold no-border-top" colspan="5" align="center">Authorized Signature</td>
            </tr>

            
        </table>
        <?php } ?>
    </body>
</html>
