<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small class="text-primary text-bold">Invoice List</small>
			<?php $invoice_add_role = $this->app_model->have_access_role(INVOICE_MODULE_ID, "add"); ?>
			<?php if($invoice_add_role): ?>
			<?php if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export")) { ?>
			<a href="<?=base_url('invoice/add_export')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Export Invoice</a>
			<?php } if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic")) { ?>
			<a href="<?=base_url('invoice/add_domestic')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Invoice</a>
			<?php } ?>
			<?php endif;?>
		</h1>
	</section>
	<div class="clearfix">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="col-md-12">
						<div class="col-md-3">
							<label class="pull-left">Party Type: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<select name="party_type" id="party_type" class="form-control input-sm select2" style="width: 60%;">                                
								<?php
								$cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
								$cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
								$isManagement = $this->applib->have_access_current_user_rights(PROFORMA_MANAGEMENT_USER,"allow");
								if($isManagement == 1){
									if($cu_accessExport == 1 && $cu_accessDomestic == 1){
								?>
								<option value="all">All</option>
								<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
								<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
								<?php
									}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
								?>
								<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
								<?php
									}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
								?>
								<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
								<?php
									}
								}else{
									if($cu_accessExport == 1 && $cu_accessDomestic == 1){
								?>
								<option value="all">All</option>
								<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
								<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
								<?php
									} else if($cu_accessExport == 1){
								?>
								<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
								<?php
									} else if($cu_accessDomestic == 1){
								?>
								<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
								<?php
									} else {}                                        
								}
								?>
							</select>
						</div>
                                                <?php 
                                                    if($this->input->get_post("status")){ 
                                                            $current_status = $this->input->get_post("status"); 
                                                    } else {
                                                            $current_status = 'pending';
                                                    }
                                                    if($this->input->get_post("user_id")){ 
                                                            $current_staff = $this->input->get_post("user_id"); 
                                                    } else {
                                                            $current_staff = $this->session->userdata('is_logged_in')['staff_id'];
                                                    }
                                                ?>
                                                <div class="col-md-3">
                                                    <label class="pull-left">From Date:&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" name="from_date" id="datepicker1" class="form-control input-sm from_date" style="width: 60%;" value="<?php echo isset($from_date) && !empty($from_date) ? date('d-m-Y', strtotime($from_date)) : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="pull-left">To Date:&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" name="to_date" id="datepicker2" class="form-control input-sm to_date" style="width: 60%;" value="<?php echo isset($to_date) && !empty($to_date) ? date('d-m-Y', strtotime($to_date)) : ''; ?>">
                                                </div>
                                                <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
                                                    <div class="col-md-3">
                                                        <label class="pull-left">Sales Person:&nbsp;</label>
                                                        <select class="form-control select_user_id input-sm select2" name="user_id" id="user_id" style="width: 60%;">
                                                            <option value="all" <?php if(isset($staff_id) && $staff_id == '0'){ echo ' Selected '; } elseif ($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
                                                                <?php
                                                                    if (!empty($users)) {
                                                                        foreach ($users as $client) {
                                                                            if (trim($client->name) != "") {
                                                                                $selected = '';
                                                                                if(isset($staff_id)){
                                                                                    if(!empty($staff_id) && $staff_id == $client->staff_id){
                                                                                        $selected = $staff_id == $client->staff_id ? 'selected' : '';
                                                                                    } 
                                                                                } else {
                                                                                    $selected = $current_staff == $client->staff_id ? 'selected' : '';
                                                                                }
                                                                                    echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
                                                                            }
                                                                        }
                                                                    }
                                                                ?>
                                                        </select>
                                                    </div>
                                                <?php } ?>
						</div>
                    <div class="clearfix"></div><br />
					<div class="col-md-12">
						<div class="table-responsive" style="overflow-x:hidden">
							<table id="invoice_list_table" class="table custom-table table-striped">
								<thead>
									<tr>
										<th>Action</th>
										<th>Invoice No.</th>
										<th>Challan No.</th>
										<th>Order / Proforma No.</th>
										<th>Quotation No.</th>
										<th>Party </th>
										<th>Sales Person </th>
										<th>Invoice Date</th>
                                        <th>Party Current Person</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
                                    </div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<section class="content-header">
		<h1>
			<?php if($invoice_add_role): ?>
			<?php if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export")) { ?>
			<a href="<?=base_url('invoice/add_export')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Export Invoice</a>
			<?php } if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic")) { ?>
			<a href="<?=base_url('invoice/add_domestic')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Invoice</a>
			<?php } ?>
			<?php endif;?>
		</h1>
	</section>
</div>
<div class="modal fade" id="with_without_letterpad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Select With or Without Letter pad in Invoice Print
					<span class="pull-right">
						<a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
						<a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
					</span>
				</h4>
			</div>
			<div class="modal-body text-center">
                <a href="#" target="_blank" class="btn-default btn" id="with_letterpad" ><i class="fa fa-print"></i> With Letter pad</a><br /><br />
                <a href="#" target="_blank" class="btn-default btn" id="without_letterpad" ><i class="fa fa-print"></i> Without Letter pad</a>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
	var table;
	$(document).ready(function() {
		$('.select2').select2();
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [1,2,3,4,5,6,7,8],
			}
		};
        
		table = $('#invoice_list_table').DataTable({
            "serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'desc']],
			"ajax": {
				"url": "<?php echo site_url('invoice/invoice_datatable')?>",
				"type": "POST",
				"data":function(d){
					d.party_type = $("#party_type").val();
                    d.from_date = $('#datepicker1').val();
                    d.to_date = $('#datepicker2').val();
                    d.user_id = $('#user_id').val();
				}
			},
            <?php if($this->applib->have_access_current_user_rights(INVOICE_MODULE_ID,"export_data")){ ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Invoices', orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
			"scrollY": 500,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});
        
		$(document).on('click',".with_without_letterpad",function() {
            $('#with_without_letterpad').modal('show');
            var invoice_id = $(this).data('invoice_id');
            $('#with_letterpad').attr('href', '<?=base_url('invoice/invoice_print/');?>/' + invoice_id + '/wlp');
            $('#without_letterpad').attr('href', '<?=base_url('invoice/invoice_print/');?>/' + invoice_id + '/');
        });
        
        $(document).on('click',"#with_letterpad, #without_letterpad",function() {
            $('#with_without_letterpad').modal('hide');
        });
        
		$(document).on('change',"#party_type,.from_date,.select_user_id,.to_date",function() {
			table.draw();
		});

		$(document).on("click", ".delete_button", function() {
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if (value) {
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=invoices',
					success: function(data) {
						show_notify("Invoice removed successfully",true);
						tr.fadeOut(function(){
							$(this).remove();
						});
					}
				});
			}
		});

	});
</script>
