<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('invoice/save_domestic_invoice') ?>" method="post" id="save_domestic_invoice" novalidate>
        <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ ?>
            <input type="hidden" name="invoice_data[invoice_id]" id="invoice_id" value="<?=$invoice_data->id?>">
        <?php } ?>
        <input type="hidden" name="invoice_data[challan_id]" id="challan_id" value="<?=(isset($invoice_data->challan_id))? $invoice_data->challan_id : ''; ?>" />
        <input type="hidden" name="invoice_data[currency_id]" id="so_currency_id" value="<?=(isset($invoice_data->currency_id))? $invoice_data->currency_id : ''; ?>" />
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> 
                <small class="text-primary text-bold">Domestic Invoices <span id="partyname"></span></small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F = Tab 4, Ctrl+S = Save</label></small>
                <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ ?>
                <?php if(isset($invoice_data->isApproved) && $invoice_data->isApproved == 1){ ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_INVOICE,"allow")){ ?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;">Disapprove</button>
                <?php } else { ?>
                    <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                <?php } ?>
                <?php } else { ?>
                    <?php if($this->applib->have_access_role(CAN_APPROVE_INVOICE,"allow")){ ?>
                        <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                    <?php } else { ?>
                        <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                    <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
                    $invoice_view_role= $this->applib->have_access_role(INVOICE_MODULE_ID,"view");
                    $invoice_add_role= $this->applib->have_access_role(INVOICE_MODULE_ID,"add");
                    $invoice_edit_role= $this->applib->have_access_role(INVOICE_MODULE_ID,"edit");
                ?>
                <?php if($invoice_edit_role || $invoice_add_role) : ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($invoice_view_role): ?>
				<a href="<?= base_url()?>invoice/invoice_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Invoice List</a>
                <?php endif;?>
                <?php if($invoice_add_role): ?>
				<a href="<?= base_url()?>invoice/add_domestic/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Invoice</a>
                <?php endif;?>
                <?php if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view") && isset($invoice_data->isApproved) && $invoice_data->isApproved == 1) { ?>
                    <span class="pull-right send_sms_div" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="invoice_data[send_sms]" id="send_sms" class="send_sms" <?=(isset($invoice_data->id) && !empty($invoice_data->id)) ? 'checked=""' : ''; ?>>  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } else if ($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view")) { ?>
                    <span class="pull-right send_sms_div hidden" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="invoice_data[send_sms]" id="send_sms" class="send_sms" <?=(isset($invoice_data->id) && !empty($invoice_data->id)) ? 'checked=""' : ''; ?>>  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Invoices Details</a></li>
                        <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                        <li><a href="#tab_3" data-toggle="tab" id="tabs_3">General</a></li>
                        <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ ?>
                        <li><a href="#tab_4" data-toggle="tab" id="tabs_4">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Invoices Details</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="invoice_no" class="col-sm-3 input-sm">Invoice No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="invoice_data[invoice_no]" id="invoice_no" class="form-control input-sm" <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ } else { echo 'style="display:none;"'; } ?> readonly="" value="<?=(isset($invoice_data->invoice_no))? $invoice_data->invoice_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="challan_no" style="" class="col-sm-3 input-sm">Challan No.</label>
													<div class="col-sm-9">
														<input type="text" class="challan_no form-control input-sm" id="challan_no" value="<?=(isset($sales_order_data->challan_no))? $sales_order_data->challan_no : ''; ?>" readonly="readonly">
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="quotation_no" style="" class="col-sm-3 input-sm">Quotation No.</label>
													<div class="col-sm-9">
                                                        <input type="text" class="quotation_no form-control input-sm" id="quotation_no" value="<?=(isset($sales_order_data->quotation_no))? $sales_order_data->quotation_no : ''; ?>" readonly="readonly">
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="cust_po_no" style="" class="col-sm-3 input-sm">Purchase Order No.</label>
													<div class="col-sm-9">
														<input type="text" class="cust_po_no form-control input-sm" id="cust_po_no" value="<?=(isset($sales_order_data->cust_po_no))? $sales_order_data->cust_po_no : ''; ?>" readonly="readonly">
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="sales_order_no" style="" class="col-sm-3 input-sm">Sales Order No.</label>
													<div class="col-sm-9">
														<input type="text" class="sales_order_no form-control input-sm" id="sales_order_no" value="<?=(isset($sales_order_data->sales_order_no))? $sales_order_data->sales_order_no : ''; ?>" readonly="readonly">
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="invoice_type_id" class="col-sm-3 input-sm">Invoice Type</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="invoice_data[invoice_type_id]" class="invoice_type_id form-control input-sm" id="invoice_type_id">
														</select>
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_id" class="col-sm-3 input-sm">Sales</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="invoice_data[sales_id]" id="sales_id" class="form-control input-sm select2 sales_id" disabled="disabled"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="invoice_date" class="col-sm-3 input-sm">Invoice Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="invoice_data[invoice_date]" id="invoice_date" class="form-control input-sm pull-right input-datepicker" value="<?=(isset($invoice_data->invoice_date))? date('d-m-Y', strtotime($invoice_data->invoice_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="challan_date" class="col-sm-3 input-sm">Challan Date</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm challan_date" id="challan_date" value="<?=(isset($sales_order_data->challan_date))? $sales_order_data->challan_date : ''; ?>" readonly="readonly" >
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">&nbsp;</label>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="purchase_order_date" style="" class="col-sm-3 input-sm">Purchase Order Date</label>
													<div class="col-sm-9">
														<input type="text" class="purchase_order_date form-control input-sm" id="po_date" value="<?=(isset($sales_order_data->po_date))? $sales_order_data->po_date : ''; ?>" readonly="readonly">
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="so_date" style="" class="col-sm-3 input-sm">Sales Order Date</label>
													<div class="col-sm-9">
														<input type="text" class="so_date form-control input-sm" id="so_date" value="<?=(isset($sales_order_data->sales_order_date))? $sales_order_data->sales_order_date : ''; ?>" readonly="readonly">
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="notes" class="col-sm-3 input-sm">Notes</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="invoice_data[notes]" id="notes" rows="2" ><?=(isset($invoice_data->notes))? $invoice_data->notes : ''; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>                                                
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Party Detail</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
                                                        <select name="invoice_data[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" placeholder="" readonly>
													</div>
												</div>
												<div class="clearfix"></div>
												<input type="hidden" name="party[party_name]" id="party_name" class="form-control input-sm party_name" placeholder="">
												<input type="hidden" name="party[party_id]" id="party_party_id" class="form-control input-sm party_party_id" placeholder="">
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="form-group">
													<label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
													<div class="col-sm-9">
														<textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
														<textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="city" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="state" class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="country" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
														<small>Add multiple email id by Comma separated.</small>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9">
														<textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
														<small>Add multiple phone number by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_website" class="col-sm-3 input-sm">Website</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm website" id="party_website" name="party[website]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<?php /*<div class="form-group">
													<label for="agent_id" class="col-sm-3 input-sm">Agent</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[agent_id]" id="agent_id" class="form-control input-sm"></select>
													</div>
												</div>
												<div class="clearfix"></div>*/ ?>
												<div class="form-group">
													<label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="invoice_data[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
															<option value="">--Select--</option>
                                                            <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ ?>
                                                                <?php foreach($party_contact_person as $pcp_key => $pcp_value): ?>
                                                                <option value="<?= $pcp_value->contact_person_id; ?>"  <?=$invoice_data->kind_attn_id == $pcp_value->contact_person_id ?'selected="selected"':'';?>><?= $pcp_value->name; ?></option>
                                                                <?php endforeach; ?>
                                                            <?php } ?>
                                                        </select>
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Designation</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_designation"><?php echo isset($contact_person->designation) ? $contact_person->designation : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Department</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_department"><?php echo isset($contact_person->department) ? $contact_person->department : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person->mobile_no) ? $contact_person->mobile_no : '' ;?>" disabled="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person->email) ? $contact_person->email : '' ;?>" disabled="">
                                                    </div>
                                                </div>
											</div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($invoice_lineitems)){ ?>
                                            <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="challan_no_for_item" class="col-sm-3 input-sm">Select Challan<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[challan_id]" id="challan_no_for_item" class="form-control input-sm challan_id" onchange="challan_details(this.value)"></select>
                                                    <input type="hidden" id="challan_no_i" name="line_items_data[challan_no]">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_name" class="col-sm-3 input-sm">Item Name</label>
                                                <div class="col-sm-9">
                                                    <input type="hidden" class="item_id" id="item_id" name="line_items_data[item_id]" data-name="item_id">
                                                    <input type="text" class="form-control input-sm item_name disabled" id="item_name" name="line_items_data[item_name]" data-name="item_name" placeholder="" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_code]" id="item_code" class="form-control input-sm item_code disabled" data-name="item_code" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_description]" id="item_description" class="form-control input-sm item_description" data-name="item_description" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_extra_accessories_id" class="col-sm-3 input-sm">Item Extra Accessories</label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_extra_accessories_id]" id="item_extra_accessories_id" class="item_extra_accessories_id form-control input-sm select2 item_data" data-input_name="item_extra_accessories_id" ></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity num_only" value="1" data-name="quantity" placeholder="" readonly="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-3 input-sm">Rate<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <select name="line_items_data[currency_id]" id="currency_id" class="form-control input-sm currency_id" ></select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate"  value="0" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="disc_per" class="col-sm-3 input-sm">Disc(%)</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[disc_per]" id="disc_per" class="form-control input-sm disc_per num_only" data-input_name="disc_per" placeholder="">
                                                </div>
                                                <label for="disc_value" class="col-sm-2 input-sm">Disc(Value)</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[disc_value]" id="disc_value" class="form-control input-sm disc_value num_only" data-input_name="disc_value" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="igst" class="col-sm-3 input-sm">IGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[igst]" id="igst" class="form-control input-sm igst item_data" data-input_name="igst" placeholder="">
                                                </div>
                                                <label for="igst_amount" class="col-sm-2 input-sm">IGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[igst_amount]" id="igst_amount" class="form-control input-sm igst_amount item_data" readonly data-input_name="igst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="cgst" class="col-sm-3 input-sm">CGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[cgst]" id="cgst" class="form-control input-sm cgst numberwithoutzero item_data" data-input_name="cgst" placeholder="">
                                                </div>
                                                <label for="cgst_amount" class="col-sm-2 input-sm">CGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[cgst_amount]" id="cgst_amount" class="form-control input-sm cgst_amount numberwithoutzero item_data" readonly data-input_name="cgst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="sgst" class="col-sm-3 input-sm">SGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[sgst]" id="sgst" class="form-control input-sm sgst item_data" data-input_name="sgst" placeholder="">
                                                </div>
                                                <label for="sgst_amount" class="col-sm-2 input-sm">SGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[sgst_amount]" id="sgst_amount" class="form-control input-sm sgst_amount item_data" readonly data-input_name="sgst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                </div>
                                                <label for="net_amount" class="col-sm-2 input-sm">Net Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[net_amount]" id="net_amount" class="form-control input-sm net_amount disabled" data-input_name="net_amount" placeholder="" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-sm-12"><div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" />
                                            </div>
                                        </div></div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <table style="" class="table custom-table item-table">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Challan No.</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Amount</th>
                                                <th class="text-right">Disc(value)</th>
                                                <th class="text-right">IGST %</th>
                                                <th class="text-right">CGST %</th>
                                                <th class="text-right">SGST %</th>
                                                <th class="text-right">Net Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lineitem_list"></tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th class="text-right"><span class="qty_total"></span><input type="hidden" name="qty_total" id="qty_total" /></th>
                                                <th class="text-right"></th>
                                                <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                                <th class="text-right" class="text-right"><span class="disc_value_total"></span><input type="hidden" name="disc_value_total" id="disc_value_total" /></th>
                                                <th class="text-right"></th>
                                                <th class="text-right"></th>
                                                <th class="text-right"></th>
                                                <th class="text-right" class="text-right"><span class="net_amount_total"></span><input type="hidden" name="net_amount_total" id="net_amount_total" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Genaral Fields To Be Set For Print </legend>
                                        <div class="col-md-6">
											<div class="form-group">
												<label for="freight" class="col-sm-4 input-sm"> Freight </label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm num_only" id="freight" name="invoice_data[freight]" placeholder="Freight Amount" value="<?=(isset($invoice_data->freight))? $invoice_data->freight : ''; ?>">
												</div>
											</div>
											<div class="form-group">
												<label for="packing_forwarding" class="col-sm-4 input-sm"> Packing & Forwarding </label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm num_only" id="packing_forwarding" name="invoice_data[packing_forwarding]" placeholder="Packing & Forwarding Amount" value="<?=(isset($invoice_data->packing_forwarding))? $invoice_data->packing_forwarding : ''; ?>">
												</div>
											</div>
											<div class="form-group">
												<label for="transit_insurance" class="col-sm-4 input-sm"> Transit Insurance </label>
												<div class="col-sm-8">
													<input type="text" class="form-control input-sm num_only" id="transit_insurance" name="invoice_data[transit_insurance]" placeholder="Transit Insurance Amount" value="<?=(isset($invoice_data->transit_insurance))? $invoice_data->transit_insurance : ''; ?>">
												</div>
											</div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Loading At </label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" id="loading_at" name="invoice_data[loading_at]" placeholder="" value="<?=(isset($invoice_data->loading_at))? $invoice_data->loading_at : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Port of Loading</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" placeholder='Port' id="port_of_loading" name="invoice_data[port_of_loading]" placeholder="" value="<?=(isset($invoice_data->port_of_loading))? $invoice_data->port_of_loading : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="delivery_through_id" class="col-sm-4 input-sm">Delivery Through</label>
                                                <div class="col-sm-8">
                                                    <select name="invoice_data[delivery_through_id]" class="delivery_through_id form-control input-sm select2" id="delivery_through_id" ></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="contact_person" class="col-sm-4 input-sm"> Transport Person </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="invoice_data[contact_person]" class=" form-control input-sm item_data" id="contact_person" placeholder="" value="<?=(isset($invoice_data->contact_person))? $invoice_data->contact_person : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="contact_person_no" class="col-sm-4 input-sm"> Contact No. </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="invoice_data[contact_person_no]" class=" form-control input-sm item_data" id="contact_person_no" placeholder="" value="<?=(isset($invoice_data->contact_person_no))? $invoice_data->contact_person_no : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="driver_name" class="col-sm-4 input-sm"> Driver Name </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="invoice_data[driver_name]" class=" form-control input-sm item_data" id="driver_name" placeholder="" value="<?=(isset($invoice_data->driver_name))? $invoice_data->driver_name : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="driver_contact_no" class="col-sm-4 input-sm"> Driver Contact No. </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="invoice_data[driver_contact_no]" class=" form-control input-sm item_data" id="driver_contact_no" placeholder="" value="<?=(isset($invoice_data->driver_contact_no))? $invoice_data->driver_contact_no : ''; ?>">
                                                </div>
                                            </div>
                                        </div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">CGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="freight_cgst"  name="invoice_data[freight_cgst]" value="<?=(isset($invoice_data->freight_cgst))? $invoice_data->freight_cgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">SGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="freight_sgst"  name="invoice_data[freight_sgst]" value="<?=(isset($invoice_data->freight_sgst))? $invoice_data->freight_sgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">IGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="freight_igst"  name="invoice_data[freight_igst]" value="<?=(isset($invoice_data->freight_igst))? $invoice_data->freight_igst : '0'; ?>" >
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">CGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="packing_forwarding_cgst"  name="invoice_data[packing_forwarding_cgst]" value="<?=(isset($invoice_data->packing_forwarding_cgst))? $invoice_data->packing_forwarding_cgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">SGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="packing_forwarding_sgst"  name="invoice_data[packing_forwarding_sgst]" value="<?=(isset($invoice_data->packing_forwarding_sgst))? $invoice_data->packing_forwarding_sgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">IGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="packing_forwarding_igst"  name="invoice_data[packing_forwarding_igst]" value="<?=(isset($invoice_data->packing_forwarding_igst))? $invoice_data->packing_forwarding_igst : '0'; ?>" >
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">CGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="transit_insurance_cgst"  name="invoice_data[transit_insurance_cgst]" value="<?=(isset($invoice_data->transit_insurance_cgst))? $invoice_data->transit_insurance_cgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">SGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="transit_insurance_sgst"  name="invoice_data[transit_insurance_sgst]" value="<?=(isset($invoice_data->transit_insurance_sgst))? $invoice_data->transit_insurance_sgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">IGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="transit_insurance_igst"  name="invoice_data[transit_insurance_igst]" value="<?=(isset($invoice_data->transit_insurance_igst))? $invoice_data->transit_insurance_igst : '0'; ?>" >
													</div>
												</div>
											</div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Port of Discharge</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" placeholder='Port' id="port_of_discharge" name="invoice_data[port_of_discharge]" placeholder="" value="<?=(isset($invoice_data->port_of_discharge))? $invoice_data->port_of_discharge : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Place of Delivery</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" placeholder='Place' id="port_place_of_delivery" name="invoice_data[port_place_of_delivery]" placeholder="" value="<?=(isset($invoice_data->port_place_of_delivery))? $invoice_data->port_place_of_delivery : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Name of Shipment</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" placeholder='Name of Shipment' id="name_of_shipment" name="invoice_data[name_of_shipment]" placeholder="" value="<?=(isset($invoice_data->name_of_shipment))? $invoice_data->name_of_shipment : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="vehicle_no" class="col-sm-4 input-sm"> Truck / Container No. </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="invoice_data[vehicle_no]" class=" form-control input-sm item_data" id="vehicle_no" placeholder="" value="<?=(isset($invoice_data->vehicle_no))? $invoice_data->vehicle_no : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="lr_no" class="col-sm-4 input-sm"> LR / BL No. </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="invoice_data[lr_no]" class=" form-control input-sm item_data" id="lr_no" placeholder="" value="<?=(isset($invoice_data->lr_no))? $invoice_data->lr_no : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="lr_date" class="col-sm-4 input-sm"> LR Date </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="invoice_data[lr_date]" class=" form-control input-sm item_data input-datepicker" id="lr_date" placeholder="" value="<?=(isset($invoice_data->id))? (isset($invoice_data->lr_date))? date('d-m-Y', strtotime($invoice_data->lr_date)) : '' : date('d-m-Y'); ?>">
                                                </div>
                                            </div>
										</div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=(isset($login_data->created_by_name))? $login_data->created_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=(isset($login_data->created_at))? $login_data->created_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=(isset($login_data->updated_by_name))? $login_data->updated_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=(isset($login_data->updated_at))? $login_data->updated_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Approved By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_by" id="approved_by" value="<?=(isset($login_data->approved_by_name))? $login_data->approved_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_date_time" id="approved_date_time" value="<?=(isset($login_data->approved_at))? $login_data->approved_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        </div>
        <section class="content-header">
            <h1> 
                <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ ?>
                <?php if(isset($invoice_data->isApproved) && $invoice_data->isApproved == 1){ ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_INVOICE,"allow")){ ?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;">Disapprove</button>
                <?php } else { ?>
                    <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                <?php } ?>
                <?php } else { ?>
                    <?php if($this->applib->have_access_role(CAN_APPROVE_INVOICE,"allow")){ ?>
                        <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                    <?php } else { ?>
                        <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                    <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
                    $invoice_view_role= $this->applib->have_access_role(INVOICE_MODULE_ID,"view");
                    $invoice_add_role= $this->applib->have_access_role(INVOICE_MODULE_ID,"add");
                    $invoice_edit_role= $this->applib->have_access_role(INVOICE_MODULE_ID,"edit");
                ?>
                <?php if($invoice_edit_role || $invoice_add_role) : ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($invoice_view_role): ?>
				<a href="<?= base_url()?>invoice/invoice_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Invoice List</a>
                <?php endif;?>
                <?php if($invoice_add_role): ?>
				<a href="<?= base_url()?>invoice/add_domestic/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Invoice</a>
                <?php endif;?>
            </h1>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ } else { ?>
<div class="modal fade" id="select_challan_modal" tabindex="-1" role="dialog" aria-labelledby="select_challan_label" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h4 class="modal-title">Select Challan
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <table id="challan_datatable" class="table custom-table table-striped">
                    <thead>
                    <tr>
                        <th>Enquiry No</th>
                        <th>Quotation No</th>
                        <th>Order No</th>
                        <th>Challan No</th>
                        <th>Challan Date</th>
                        <th>Party</th>
                        <th>Item Code</th>
                        <th>Contact No</th>
                        <th>Email Id</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script type="text/javascript" src="<?=dist_url('js/sales_order.js');?>"></script>
<script type="text/javascript">
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
	var lineitem_objectdata = [];
    <?php if(isset($invoice_lineitems)){ ?>
		var li_lineitem_objectdata = [<?php echo $invoice_lineitems; ?>];
		var lineitem_objectdata = [];
        if(li_lineitem_objectdata != ''){
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
	<?php } ?>
    display_lineitem_html(lineitem_objectdata);
    
    <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ ?>
        first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function(){
        $('.select2').select2();
        $('input[type="checkbox"].send_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        initAjaxSelect2($("#sales_id"),"<?=base_url('app/sales_select2_source')?>");
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source')?>");
        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
        initAjaxSelect2($("#reference_id"),"<?=base_url('app/reference_select2_source')?>");
        initAjaxSelect2($("#item_extra_accessories_id"),"<?=base_url('app/item_extra_accessories_select2_source')?>");
        setSelect2Value($("#item_extra_accessories_id"),"<?=base_url('app/set_item_extra_accessories_select2_val_by_id/'.DEFAULT_ITEM_EXTRA_ACCESSORIES_ID)?>");
        initAjaxSelect2($("#delivery_through_id"),"<?=base_url('app/delivery_through_select2_source')?>");
        <?php if(isset($invoice_data->delivery_through_id)){ ?>
			setSelect2Value($("#delivery_through_id"),"<?=base_url('app/set_delivery_through_select2_val_by_id/'.$invoice_data->delivery_through_id)?>");
        <?php } ?>
        initAjaxSelect2($("#invoice_type_id"),"<?=base_url('app/invoice_type_select2_source')?>");
        <?php if(isset($invoice_data->invoice_type_id)){ ?>
			setSelect2Value($("#invoice_type_id"),"<?=base_url('app/set_invoice_type_select2_val_by_id/'.$invoice_data->invoice_type_id)?>");
        <?php } else { ?>
            setSelect2Value($("#invoice_type_id"),'<?=base_url()?>app/set_invoice_type_select2_val_by_id/'+<?=INVOICE_TYPE_ID?>);
        <?php } ?>
        
        <?php if(isset($invoice_data->sales_to_party_id)){ ?>
			setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id/'.$invoice_data->sales_to_party_id)?>");
            party_details(<?=$invoice_data->sales_to_party_id; ?>);
            initAjaxSelect2($("#challan_no_for_item"),"<?=base_url('app/challan_with_item_select2_source/'.$invoice_data->sales_to_party_id)?>");
		<?php } ?>
        
        var table = $('#challan_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[5,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('invoice/challan_datatable')?>",
                "type": "POST",
				"data":function(d){
					d.invoice_for = 'domestic';
				}
            },
            "scrollY": 400,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columns": [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ]
        });
		
        $('#select_challan_modal').modal({backdrop: 'static', keyboard: false});
        $('#select_challan_modal').modal('show');
        $('#select_challan_modal').on('shown.bs.modal',function(){
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();	
        });
                
        $(document).on('click', '.challan_row', function () {
            var tr = $(this).closest('tr');
            var challan_id = $(this).data('challan_id');
            $("#challan_id").val(challan_id);
            feedChallanData(challan_id);
            $('#select_challan_modal').modal('hide');
            $('#invoice_date').focus();
        });
    
        $(document).on('input','#item_rate',function () {
            if(this.value > 9900000){
                this.value = 9900000;
            }
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href"); // activated tab
            if (target == '#tab_1') {
                $('#challan_no_for_item').select2('close');
                $('#invoice_date').off("focus").datepicker("show");
            } else if (target == '#tab_2') {
                $('#challan_no_for_item').select2('open');
                $('#invoice_date').off("focus").datepicker("hide");
            } else {
                $('#challan_no_for_item').select2('close');
                $('#invoice_date').off("focus").datepicker("hide");
            }
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_domestic_invoice").submit();
                return false;
            }
        });
        
        $(document).on('submit', '#save_domestic_invoice', function () {
            if($.trim($("#party_id").val()) == ''){
                show_notify('Please Select Party.',false);
                return false;
            }
            if($.trim($("#reference_id").val()) == ''){
                show_notify('Please Select Reference.',false);
                return false;
            }
            if($.trim($("#challan_no_for_item").val()) != ''){
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }

			if(lineitem_objectdata == ''){
				show_notify("Please select any one Item.", false);
				return false;
			}
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
			var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
			postData.append('line_items_data', lineitem_objectdata_stringify);
			$.ajax({
				url: "<?=base_url('invoice/save_domestic_invoice') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('invoice/invoice_list') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('invoice/invoice_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });

        $(document).on("change",'#contact_person_id',function() {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?=base_url();?>party/get-contact-person-by-id/"+contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });
        
        $(document).on("change",'#currency_id',function(){
            if(edit_lineitem_inc == 0){
                var currency_id = $("#currency_id").val();
                $("#so_currency_id").val(currency_id);
                var item_id = $("#item_id").val();
                if(item_id != '' && item_id != null) {
                    $.ajax({
                        type: "POST",
                        url: '<?=base_url();?>sales/ajax_load_item_details/' + item_id,
                        data: id = 'item_id',
                        success: function (data) {
                            var json = $.parseJSON(data);
                            if (json['item_rate_in']) {
                                var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                                $("#item_rate").val(item_rate_in_json[currency_id]);
                            } else {
                                $("#item_rate").val('');
                            }
                            input_value_get_amount();
                        }
                    });
                }
            } else { edit_lineitem_inc = 0; }
        });
        
        $(document).on("click",".btn_approve_ord",function(){
            $(".send_sms_div").removeClass('hidden');
            $("#ajax-loader").show();
            var invoice_id = $('#invoice_id').val();
            $.ajax({
                url: '<?=BASE_URL?>invoice/update_invoice_status_to_approved',
                type: "POST",
                data: {invoice_id:invoice_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        <?php if($this->applib->have_access_role(CAN_APPROVE_INVOICE,"allow")){?>
                        $(".btn_approve_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_disapprove_ord' style='margin: 5px;'>Disapprove</button>");
                        <?php }else{?>
                        $(".btn_approve_ord").after("<small class='text-primary text-bold pull-right app_lab' style='margin: 5px;'>Approved</small>");
                        <?php }?>
                        $('.btn_approve_ord').remove();
                        show_notify(data.message, true);
                    }
                    else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on("click",".btn_disapprove_ord",function(){
            $(".send_sms_div").addClass('hidden');
            $("#ajax-loader").show();
            var invoice_id = $('#invoice_id').val();
            $.ajax({
                url: '<?=BASE_URL?>invoice/update_invoice_status_to_disapproved',
                type: "POST",
                data: {invoice_id:invoice_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $(".btn_disapprove_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_approve_ord' style='margin: 5px;'>Approve</button>");
                        //$('.btn_approve_ord').show();
                        $('.btn_disapprove_ord').remove();
                        show_notify(data.message, true);
                    }
                    else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });
        
        $('#add_lineitem').on('click', function() {
			var item_id = $("#challan_no_for_item").val();
			if(item_id == '' || item_id == null){
				show_notify("Please select Challan!", false);
				return false;
			}
			var quantity = $("#quantity").val();
			if(quantity == '' || quantity == null){
				show_notify("Item quantity is required!", false);
				return false;
			}
			var item_rate = $("#item_rate").val();
			if(item_rate == '' || item_rate == null){
				show_notify("Item Rate is required!", false);
				return false;
			}
			
			var key = '';
			var value = '';
			var lineitem = {};
			$('select[name^="line_items_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			$('input[name^="line_items_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			var new_lineitem = JSON.parse(JSON.stringify(lineitem));
			var line_items_index = $("#line_items_index").val();
            if(line_items_index != ''){
				lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
			} else {
				lineitem_objectdata.push(new_lineitem);
			}
			display_lineitem_html(lineitem_objectdata);
			$('#lineitem_id').val('');
            $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
			$("#challan_no_for_item").val(null).trigger("change");
			$("#item_extra_accessories_id").val(null).trigger("change");
            $("#quantity").val('1');
			$("#disc_per").val('0');
			$("#line_items_index").val('');
            if(on_save_add_edit_item == 1){
                on_save_add_edit_item == 0;
                $('#save_domestic_invoice').submit();
            }
            var sales_id = $("#sales_id").val();
            if(sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
            }
            if(sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
            }
		});
        
    });
    
    function feedChallanData(challan_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>invoice/get_challan',
            type: "POST",
            async: false,
            data: {challan_id: challan_id},
            success: function(data){
                $("#ajax-loader").hide();
                json = JSON.parse(data);
                var sales_order_data = json.sales_order_data;
                $('#cust_po_no').val(sales_order_data.cust_po_no);
                $('#po_date').val(sales_order_data.po_date);
                $('#quotation_no').val(sales_order_data.quotation_no);
                $('#sales_order_no').val(sales_order_data.sales_order_no);
                $('#so_date').val(sales_order_data.sales_order_date);
                var challan_data = json.challan_data;
                initAjaxSelect2($("#challan_no_for_item"),"<?=base_url('app/challan_with_item_select2_source')?>/"+challan_data.party_id);
                setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id')?>/"+challan_data.party_id);
                $('#party_name').val($('#party_id').val());
                $('#party_party_id').val(challan_data.party_id);
                party_details($('#party_id').val());
                $('#challan_id').val(challan_data.id);
                $('#challan_no').val(challan_data.challan_no);
                $('#challan_date').val(challan_data.challan_date);
                $('#loading_at').val(challan_data.loading_at);
                $('#port_of_loading').val(challan_data.port_of_loading);
                $('#port_of_discharge').val(challan_data.port_of_discharge);
                $('#port_place_of_delivery').val(challan_data.port_place_of_delivery);
                $('#port_of_loading').val(challan_data.port_of_loading);
                $('#packing_forwarding').val(sales_order_data.packing_forwarding_amount);
                $('#packing_forwarding_cgst').val(sales_order_data.packing_forwarding_cgst);
                $('#packing_forwarding_sgst').val(sales_order_data.packing_forwarding_sgst);
                $('#packing_forwarding_igst').val(sales_order_data.packing_forwarding_igst);
                setSelect2Value($("#delivery_through_id"),"<?=base_url('app/set_delivery_through_select2_val_by_id/')?>/"+challan_data.delivery_through_id);
                $('#name_of_shipment').val(challan_data.name_of_shipment);
                $('select[name="invoice_data[kind_attn_id]"]').val(challan_data.kind_attn_id).change();
                if(json.challan_item != ''){
                    lineitem_objectdata.push(json.challan_item);
                    display_lineitem_html(lineitem_objectdata);
                }
                
                var challan_item_data = json.challan_item;
                $('#contact_person').val(challan_item_data.contact_person);
                $('#contact_person_no').val(challan_item_data.contact_no);
                $('#driver_name').val(challan_item_data.driver_name);
                $('#driver_contact_no').val(challan_item_data.driver_contact_no);
                $('#vehicle_no').val(challan_item_data.truck_container_no);
                $('#lr_no').val(challan_item_data.lr_bl_no);
                $('#lr_date').val(challan_item_data.lr_date);
            },
        });
    }
    
    function display_lineitem_html(lineitem_objectdata){
		var new_lineitem_html = '';
		var qty_total = 0;
		var amount_total = 0;
		var disc_value_total = 0;
		var net_amount_total = 0;
		console.log(lineitem_objectdata);
		$.each(lineitem_objectdata, function (index, value) {
            var value_currency;
			$.ajax({
				url: "<?=base_url('app/set_currency_select2_val_by_id/') ?>/" + value.currency_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_currency = response.text;
				},
			});
            
			var lineitem_edit_btn = '';
			lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
			var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
			lineitem_edit_btn +
			' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a> ' +
			'</td>' +
			'<td>' + value.challan_no + '</td>' +
			'<td>' + value.item_code + '</td>' +
			'<td>' + value.item_name + '</td>' +
            '<td class="text-right">' + value.quantity + '</td>';
            if(value_currency != ''){
                row_html += '<td class="text-right">' + value_currency + ' - ' + parseFloat(value.item_rate).toFixed(2) + '</td>';
            } else {
                row_html += '<td class="text-right">' + parseFloat(value.item_rate).toFixed(2) + '</td>';
            }
            row_html += '<td class="text-right">' + parseFloat(value.amount).toFixed(2) + '</td>' +
            '<td class="text-right">' + parseFloat(value.disc_value).toFixed(2) + '</td>' +
            '<td class="text-right">' + value.igst + '</td>' +
            '<td class="text-right">' + value.cgst + '</td>' +
            '<td class="text-right">' + value.sgst + '</td>' +
            '<td class="text-right">' + parseFloat(value.net_amount).toFixed(2) + '</td></tr>';
			new_lineitem_html += row_html;
			qty_total += +value.quantity;
			amount_total += +value.amount;
			disc_value_total += +value.disc_value;
			net_amount_total += +value.net_amount;
		});
		$('tfoot span.qty_total').html(qty_total); $('#qty_total').val(qty_total);
		$('tfoot span.amount_total').html(amount_total.toFixed(2)); $('#amount_total').val(amount_total.toFixed(2));
		$('tfoot span.disc_value_total').html(disc_value_total.toFixed(2)); $('#disc_value_total').val(disc_value_total.toFixed(2));
        $('tfoot span.net_amount_total').html(net_amount_total.toFixed(2)); $('#net_amount_total').val(net_amount_total.toFixed(2));
		$('tbody#lineitem_list').html(new_lineitem_html);
	}
                
    function edit_lineitem(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_lineitem_inc == 0){  edit_lineitem_inc = 1; /*$('.edit_lineitem_'+ index).click();*/ }
		value = lineitem_objectdata[index];
		$("#line_items_index").val(index);
		var party_id = $("#party_id").val();
		initAjaxSelect2($("#challan_no_for_item"),"<?=base_url('app/challan_with_item_select2_source/')?>/"+party_id);
		setSelect2Value($("#challan_no_for_item"),"<?=base_url('app/set_challan_with_item_select2_val_by_id/')?>/" + value.challan_id);
		initAjaxSelect2($("#item_extra_accessories_id"),"<?=base_url('app/item_extra_accessories_select2_source/')?>");
        setSelect2Value($("#item_extra_accessories_id"),"<?=base_url('app/set_item_extra_accessories_select2_val_by_id/')?>/" + value.item_extra_accessories_id);
		initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source/')?>");
		setSelect2Value($("#currency_id"),"<?=base_url('app/set_currency_select2_val_by_id/')?>/" + value.currency_id);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#lineitem_id").val(value.id);
		}
        $("#challan_no_i").val(value.challan_no);
        $("#item_id").val(value.item_id);
        $("#item_code").val(value.item_code);
        $("#item_name").val(value.item_name);
        $("#item_description").val(value.item_description);
        $("#item_rate").val(value.item_rate);
        $("#quantity").val(value.quantity);
        $("#amount").val(value.amount);
        $("#disc_per").val(value.disc_per);
        $("#disc_value").val(value.disc_value);
        $("#igst").val(value.igst);
        $("#cgst").val(value.cgst);
        $("#sgst").val(value.sgst);
        $("#igst_amount").val(value.igst_amount);
        $("#cgst_amount").val(value.cgst_amount);
        $("#sgst_amount").val(value.sgst_amount);
        $("#net_amount").val(value.net_amount);
		$('.overlay').hide();
	}
	
	function remove_lineitem(index){
        if(confirm('Are you sure ?')){
            value = lineitem_objectdata[index];
            if(typeof(value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index,1);
            display_lineitem_html(lineitem_objectdata);
        }
	}
     
    function party_details(id){
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/'+id,
            async: false,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                }else{
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $(".party_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"),'<?=base_url()?>app/set_city_select2_val_by_id/'+json['city_id']);
                }else{
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                }else{
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                }else{
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                }else{
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }else{
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                }else{
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                }else{
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                }else{
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"),'<?=base_url()?>app/set_reference_select2_val_by_id/'+json['reference_id']);
                }else{
                    setSelect2Value($("#reference_id"));
                }
                if(json['reference_description']){
                    $("#reference_description").html(json['reference_description']);
                }else{
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"),'<?=base_url()?>app/set_sales_select2_val_by_id/'+json['party_type_1_id']);
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
                    }
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
                    }
                    var currency_title = $('#select2-currency_id-container').attr('title');
                    $('#received_payment_currency').html(currency_title);
                }else{
                    setSelect2Value($("#sales_id"));
                }

                <?php if(isset($invoice_data->id) && !empty($invoice_data->id)){ } else { ?>
                    if (json['branch_id']) {
                        setSelect2Value($("#branch_id"),"<?=base_url('app/set_branch_select2_val_by_id')?>/"+json['branch_id']);
                    }else{
                        setSelect2Value($("#branch_id"));
                    }

                    if (json['agent_id']) {
                        setSelect2Value($("#agent_id"),'<?=base_url()?>app/set_agent_select2_val_by_id/'+json['agent_id']);
                    }else{
                        setSelect2Value($("#agent_id"));
                    }
                <?php } ?>

                if(first_time_edit_mode == 1){
                    if (json['contact_persons_array']) {
                        var option_html = '';
                        if(json['contact_persons_array'].length > 0){
//                            console.log(json['contact_persons_array']);
                            $.each(json['contact_persons_array'],function(index,value){
                                option_html += "<option value='"+value.contact_person_id+"'>"+value.name+"</option>";
                            })
                            $('select[name="invoice_data[kind_attn_id]"]').html(option_html).select2();
                        } else {
                            $('select[name="invoice_data[kind_attn_id]"]').html('');
                        }
                        $('select[name="invoice_data[kind_attn_id]"]').change();
                    }
                } else { first_time_edit_mode = 1; }
                $("#ajax-loader").hide();
            }
        });
    }
    
    function challan_details(id){
        if(edit_lineitem_inc == 0){
            if(id != '') {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>invoice/ajax_load_challan_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
    //                    console.log(data);
                        var json = $.parseJSON(data);
                        var challan_data = json.challan_data;
                        var challan_item = json.challan_item;
                        $("#challan_no_i").val(challan_item.challan_no);
                        $("#item_id").val(challan_item.item_id);
                        $("#item_name").val(challan_item.item_name);
                        $("#item_code").val(challan_item.item_code);
                        setSelect2Value($("#item_extra_accessories_id"),"<?=base_url('app/set_item_extra_accessories_select2_val_by_id/')?>/"+challan_item.item_extra_accessories_id);
                        if(edit_lineitem_inc == 0){  edit_lineitem_inc = 1; }
                        setSelect2Value($("#currency_id"),"<?=base_url('app/set_currency_select2_val_by_id/')?>/"+challan_item.currency_id);
                        $("#item_rate").val(challan_item.item_rate);
                        $("#disc_per").val(challan_item.disc_per);
                        $("#disc_value").val(challan_item.disc_value);
                        
                        $("#igst").val(challan_item.igst);
                        $("#igst_amount").val(challan_item.igst_amount);
                        $("#cgst").val(challan_item.cgst);
                        $("#cgst_amount").val(challan_item.cgst_amount);
                        $("#sgst").val(challan_item.sgst);
                        $("#sgst_amount").val(challan_item.sgst_amount);
                        
                        $("#amount").val(challan_item.amount);
                        $("#net_amount").val(challan_item.net_amount);
                    }
                });
            }
        }
    }
    
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
    <?php } ?>
</script>