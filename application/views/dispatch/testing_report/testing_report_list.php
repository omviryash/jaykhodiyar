<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class='text-primary text-bold'>
                Testing Report List
            </small>
            <?php $testing_report_add_role = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "add"); ?>
            <?php if($testing_report_add_role): ?>
                <a href="<?=base_url()?>testing_report/add/" class="btn btn-info btn-xs pull-right">Add Testing Report</a>
            <?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_testing_report_list" class="table custom-table agent-table">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Testing Report No</th>
                                    <th>Challan No</th>
                                    <th>Order / Proforma No</th>
                                    <th>Quotation No</th>
                                    <th>Enquiry No</th>
                                    <th>Item Code</th>
                                    <th>Party</th>
                                    <th>Contact No</th>
                                    <th>Email Id</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Party Current Person</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [1,2,3,4,5,6,7,8,9,10,11,12,13],
			}
		};
        
        table = $('#table_testing_report_list').DataTable({
            <?php if($this->applib->have_access_current_user_rights(TESTING_REPORT_MODULE_ID,"export_data")){ ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Testing Reports', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Testing Reports', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Testing Reports', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Testing Reports', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Testing Reports', orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('testing_report/testing_report_datatable')?>",
                "type": "POST",
                "data": function(d){
                    d.city_id = $("#city_id").val();
                },
            },
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

        initAjaxSelect2($("#city_id"),"<?=base_url('app/city_select2_source')?>");

        $(document).on("change", "#city_id,#state_id,#country_id", function() {
            //table.draw();
        });

        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=testing_report',
                    success: function(data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

    });
</script>
