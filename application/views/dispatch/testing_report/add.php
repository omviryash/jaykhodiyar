<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?= base_url('testing_report/save_testing_report') ?>" method="post" id="save_testing_report" novalidate>
        <?php if (isset($testing_report_data->testing_report_id) && !empty($testing_report_data->testing_report_id)) { ?>
            <input type="hidden" name="testing_report_data[testing_report_id]" id="testing_report_id" value="<?= $testing_report_data->testing_report_id ?>">
        <?php } ?>
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Dispatch : Testing Report</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, Ctrl+S = Save</label></small>
                <?php
                $testing_report_add_role = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "add");
                $testing_report_edit_role = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "edit");
                $testing_report_view_role = $this->app_model->have_access_role(TESTING_REPORT_MODULE_ID, "view");
                ?>
                <?php if ($testing_report_add_role || $testing_report_edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
                <?php endif; ?>
                <?php if ($testing_report_view_role): ?>
                    <a href="<?= base_url() ?>testing_report/testing_report_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Testing Report List</a>
                <?php endif; ?>
                <?php if ($testing_report_add_role): ?>
                    <a href="<?= base_url() ?>testing_report/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Testing Report</a>
                <?php endif; ?>
            </h1>
        </section>
        <div class="clearfix">
            <?php if ($testing_report_add_role || $testing_report_edit_role): ?>
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Testing Report Details</a></li>
                            <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                            <?php if (isset($testing_report_data->testing_report_id) && !empty($testing_report_data->testing_report_id)) { ?>
                            <li><a href="#tab_3" data-toggle="tab" id="tabs_3">Login</a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="testing_report_data[challan_id]" class="challan_id" id="challan_id" value="<?= (isset($testing_report_data->challan_id)) ? $testing_report_data->challan_id : ''; ?>">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Testing Report Detail</legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="testing_report_no" style="" class="col-sm-5 input-sm">Testing Report No.</label>
                                                            <div class="col-sm-7">
                                                                <input type="hidden" class="testing_report_no form-control input-sm" id="testing_report_no" name="testing_report_data[testing_report_no]" value="<?= (isset($testing_report_data->testing_report_no)) ? $testing_report_data->testing_report_no : ''; ?>" readonly="readonly">
                                                                <input type="text" class="form-control input-sm" value="<?= (isset($testing_report_data->testing_report_no_year)) ? $testing_report_data->testing_report_no_year : ''; ?>" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-5 input-sm">Testing Report Date</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control input-sm input-datepicker testing_report_date" id="testing_report_date" name="testing_report_data[testing_report_date]" value="<?= (isset($testing_report_data->testing_report_date)) ? date('d-m-Y', strtotime($testing_report_data->testing_report_date)) : date('d-m-Y'); ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="testing_report_no" style="" class="col-sm-5 input-sm">Challan No.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="challan_no form-control input-sm" id="challan_no" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-5 input-sm">Challan Date</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control input-sm input-datepicker challan_date" id="challan_date" value="" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" style="" class="col-sm-5 input-sm">Quotation No.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="quotation_no form-control input-sm" id="quotation_no" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="purchase_order_no" style="" class="col-sm-5 input-sm">Purchase Order No.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="purchase_order_no form-control input-sm" id="cust_po_no" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="purchase_order_date" style="" class="col-sm-5 input-sm">Purchase Order Date.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="purchase_order_date form-control input-sm" id="po_date" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_no" style="" class="col-sm-5 input-sm">Sales Order No.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="sales_order_no form-control input-sm" id="sales_order_no" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_date" style="" class="col-sm-5 input-sm">Sales Order Date.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="sales_order_date form-control input-sm" id="sales_order_date" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_no" style="" class="col-sm-5 input-sm">Proforma Invoice No.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="sales_order_no form-control input-sm" id="proforma_invoice_no" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_date" style="" class="col-sm-5 input-sm">Proforma Invoice Date.</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="sales_order_date form-control input-sm" id="proforma_invoice_date" value="" readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="manufacturing_m_y" class="col-sm-5 input-sm">Manufacturing Month & Year</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control input-sm " id="manufacturing_m_y" name="testing_report_data[manufacturing_m_y]" value="<?= (isset($testing_report_data->manufacturing_m_y)) ? $testing_report_data->manufacturing_m_y : ''; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="assembling_equipment" class="col-sm-5 input-sm">Assembling Equipment</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="assembling_equipment form-control input-sm" id="datepicker2" name="testing_report_data[assembling_equipment]" value="<?= (isset($testing_report_data->assembling_equipment)) ? date('d-m-Y', strtotime($testing_report_data->assembling_equipment)) : ''; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="quality_checked_by" class="col-sm-5 input-sm">Quality Checked By</label>
                                                            <div class="col-sm-4" style="padding-right: 0px;">
                                                                <select name="testing_report_data[quality_checked_by]" class="quality_checked_by form-control input-sm select2" id="quality_checked_by" ></select>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control input-sm quality_checked_by_contact" id="quality_checked_by_contact" value="<?= (isset($testing_report_data->quality_checked_by_contact)) ? $testing_report_data->quality_checked_by_contact : ''; ?>" placeholder="Mobile No." readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="equipment_testing_date" class="col-sm-5 input-sm">Equipment Testing</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="equipment_testing_date form-control input-sm" id="datepicker3" name="testing_report_data[equipment_testing_date]" value="<?= (isset($testing_report_data->equipment_testing_date)) ? date('d-m-Y', strtotime($testing_report_data->equipment_testing_date)) : ''; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="loading_at" class="col-sm-5 input-sm">Equipment Inspection By</label>
                                                            <div class="col-sm-4" style="padding-right: 0px;">
                                                                <select name="testing_report_data[equipment_inspection_by]" class="equipment_inspection_by form-control input-sm select2" id="equipment_inspection_by" ></select>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control input-sm equipment_inspection_by_contact" id="equipment_inspection_by_contact" value="<?= (isset($testing_report_data->equipment_inspection_by_contact)) ? $testing_report_data->equipment_inspection_by_contact : ''; ?>" placeholder="Mobile No." readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="equipment_testing_by" class="col-sm-5 input-sm">Equipment Testing By</label>
                                                            <div class="col-sm-4" style="padding-right: 0px;">
                                                                <select name="testing_report_data[equipment_testing_by]" class="equipment_testing_by form-control input-sm select2" id="equipment_testing_by" ></select>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control input-sm equipment_testing_by_contact" id="equipment_testing_by_contact" value="<?= (isset($testing_report_data->equipment_testing_by_contact)) ? $testing_report_data->equipment_testing_by_contact : ''; ?>" placeholder="Mobile No." readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_site_incharge_person" class="col-sm-5 input-sm">Incharge Person</label>
                                                            <div class="col-sm-4" style="padding-right: 0px;">
                                                                <select name="testing_report_data[our_site_incharge_person]" class="our_site_incharge_person form-control input-sm select2" id="our_site_incharge_person" ></select>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control input-sm our_site_incharge_person_contact" id="our_site_incharge_person_contact" value="<?= (isset($testing_report_data->our_site_incharge_person_contact)) ? $testing_report_data->our_site_incharge_person_contact : ''; ?>" placeholder="Mobile No." readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="loading_at" class="col-sm-5 input-sm">Delivery of Equipment</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control input-sm delivery_of_equipment" id="delivery_of_equipment" name="testing_report_data[delivery_of_equipment]" value="<?= (isset($testing_report_data->delivery_of_equipment)) ? date('d-m-Y', strtotime($testing_report_data->delivery_of_equipment)) : ''; ?>" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Party Detail</legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="testing_report_data[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <input type="hidden" name="testing_report_data[party_name]" id="party_name" class="party_name" placeholder="">
                                                    <input type="hidden" name="testing_report_data[party_id]" id="party_party_id" class="party_party_id" placeholder="">
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-3 input-sm">Address</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="city" class="col-sm-3 input-sm">City</label>
                                                        <div class="col-sm-9">
                                                            <div class="col-md-6" style="padding:0px !important;">
                                                                <select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
                                                            </div>
                                                            <div class="col-md-6" style="padding-right:0px !important;">
                                                                <input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="state" class="col-sm-3 input-sm">State</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="country" class="col-sm-3 input-sm">Country</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
                                                            <small>Add multiple email id by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
                                                            <small>Add multiple phone number by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_website" class="col-sm-3 input-sm">Website</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm website" id="party_website" name="party[website]" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="testing_report_data[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
                                                                <option value="">--Select--</option>
                                                                <?php if (isset($testing_report_data->testing_report_id) && !empty($testing_report_data->testing_report_id)) { ?>
                                                                    <?php foreach ($party_contact_person as $pcp_key => $pcp_value): ?>
                                                                        <option value="<?= $pcp_value->contact_person_id; ?>"  <?= $testing_report_data->kind_attn_id == $pcp_value->contact_person_id ? 'selected="selected"' : ''; ?>><?= $pcp_value->name; ?></option>
                                                                    <?php endforeach; ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Designation</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_designation"><?php echo isset($contact_person) ? $contact_person->designation : ''; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Department</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_department"><?php echo isset($contact_person) ? $contact_person->department : ''; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person) ? $contact_person->mobile_no : ''; ?>" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person) ? $contact_person->email : ''; ?>" disabled="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border testing_report-item-section">
                                            <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                            <div class="col-md-6">
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Equipment Name</label>
                                                    <div class="col-sm-9">
                                                        <input type="hidden" name="testing_report_data[item_id]" id="item_id" class="input-sm form-control" value="<?= (isset($testing_report_data->item_id)) ? $testing_report_data->item_id : ''; ?>" readonly="readonly">
                                                        <input type="text" name="testing_report_data[item_name]" id="item_name" class="input-sm form-control disabled" value="<?= (isset($testing_report_data->item_name)) ? $testing_report_data->item_name : ''; ?>" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Item Code</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="" class="input-sm form-control disabled" id="item_code" value="<?= (isset($testing_report_data->item_code)) ? $testing_report_data->item_code : ''; ?>" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Item Description</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="" class="form-control input-sm item_data" id="item_description" value="<?= (isset($testing_report_data->item_description)) ? $testing_report_data->item_description : ''; ?>" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="" class="form-control input-sm item_data num_only" id="quantity" value="1" readonly="readonly">
                                                    </div>
                                                    <label for="item_extra_accessories_id" class="col-sm-3 input-sm text-right">Item Extra Accessories</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="item_extra_accessories" class="form-control input-sm item_data" id="item_extra_accessories_id" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-3 input-sm"> Item Serial No. </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="" class="form-control input-sm item_data" id="item_serial_no" value="<?= (isset($testing_report_data->item_serial_no)) ? $testing_report_data->item_serial_no : ''; ?>" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Electric Motor & Equipment Testing No Load Report At Jay Khodiyar Factory</legend>
                                            <div class="col-md-12">
                                                <div class="table-responsive">          
                                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th><strong>User</strong></th>
                                                                <th><strong>Make</strong></th>
                                                                <th><strong>HP</strong></th>
                                                                <th><strong>KW</strong></th>
                                                                <th><strong>Frequency</strong></th>
                                                                <th><strong>RPM</strong></th>
                                                                <th><strong>Volts</strong></th>
                                                                <th><strong>Serial No.</strong></th>
                                                                <th><strong>Normal Amps</strong></th>
                                                                <th><strong>Working Amps</strong></th>
                                                                <th><strong>Total WorkingHours</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="motor_testing_details_jk"></tbody>												
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="table-responsive">          
                                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th><strong>User</strong></th>
                                                                <th><strong>Make</strong></th>
                                                                <th><strong>Gear Type</strong></th>
                                                                <th><strong>Model</strong></th>
                                                                <th><strong>Ratio</strong></th>
                                                                <th><strong>Serial No.</strong></th>
                                                                <th><strong>Normal Sound</strong></th>
                                                                <th><strong>Working Sound</strong></th>
                                                                <th><strong>Total WorkingHours</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="gearbox_testing_details_jk"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold"> Equipment Testing Normal Load Report At Jay Khodiyar Factory</legend>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-2 input-sm"><strong>Testing Capacity</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Capacity Hrs</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Test of Raw Material</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Moisture of Raw Material</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>After Procces of Moisture</strong></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-2 input-sm">Testing Normal Load.</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_normal_capacity_hrs]" class="form-control input-sm item_data" id="jk_testing_normal_capacity_hrs" value="<?= (isset($testing_report_data->jk_testing_normal_capacity_hrs)) ? $testing_report_data->jk_testing_normal_capacity_hrs : ''; ?>" placeholder="Kgs">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_normal_test_raw_material]" class="form-control input-sm item_data" id="jk_testing_normal_test_raw_material" value="<?= (isset($testing_report_data->jk_testing_normal_test_raw_material)) ? $testing_report_data->jk_testing_normal_test_raw_material : ''; ?>" placeholder="Name of Raw Material">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_normal_moisture_raw_material]" class="form-control input-sm item_data" id="jk_testing_normal_moisture_raw_material" value="<?= (isset($testing_report_data->jk_testing_normal_moisture_raw_material)) ? $testing_report_data->jk_testing_normal_moisture_raw_material : ''; ?>" placeholder="Moisture %">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_normal_after_procces_moisture]" class="form-control input-sm item_data" id="jk_testing_normal_after_procces_moisture" value="<?= (isset($testing_report_data->jk_testing_normal_after_procces_moisture)) ? $testing_report_data->jk_testing_normal_after_procces_moisture : ''; ?>" placeholder="Moisture %">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-2 input-sm">Testing Full Load.</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_full_capacity_hrs]" class="form-control input-sm item_data" id="jk_testing_full_capacity_hrs" value="<?= (isset($testing_report_data->jk_testing_full_capacity_hrs)) ? $testing_report_data->jk_testing_full_capacity_hrs : ''; ?>" placeholder="Kgs">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_full_test_raw_material]" class="form-control input-sm item_data" id="jk_testing_full_test_raw_material" value="<?= (isset($testing_report_data->jk_testing_full_test_raw_material)) ? $testing_report_data->jk_testing_full_test_raw_material : ''; ?>" placeholder="Name of Raw Material">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_full_moisture_raw_material]" class="form-control input-sm item_data" id="jk_testing_full_moisture_raw_material" value="<?= (isset($testing_report_data->jk_testing_full_moisture_raw_material)) ? $testing_report_data->jk_testing_full_moisture_raw_material : ''; ?>" placeholder="Moisture %">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="testing_report_data[jk_testing_full_after_procces_moisture]" class="form-control input-sm item_data" id="jk_testing_full_after_procces_moisture" value="<?= (isset($testing_report_data->jk_testing_full_after_procces_moisture)) ? $testing_report_data->jk_testing_full_after_procces_moisture : ''; ?>" placeholder="Moisture %">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold"> Temperature Parameters At Jay Khodiyar Factory</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_temp_oli" class="col-sm-6 input-sm">Oil Temperature.</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_temp_oli]" class="form-control input-sm item_data" id="jk_temp_oli" value="<?= (isset($testing_report_data->jk_temp_oli)) ? $testing_report_data->jk_temp_oli : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jk_temp_eqipment" class="col-sm-6 input-sm">Equipment Temperature.</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_temp_eqipment]" class="form-control input-sm item_data" id="jk_temp_eqipment" value="<?= (isset($testing_report_data->jk_temp_eqipment)) ? $testing_report_data->jk_temp_eqipment : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_temp_head" class="col-sm-6 input-sm">Head Temperature.</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_temp_head]" class="form-control input-sm item_data" id="jk_temp_head" value="<?= (isset($testing_report_data->jk_temp_head)) ? $testing_report_data->jk_temp_head : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jk_temp_kiln" class="col-sm-6 input-sm">Kiln Temperature.</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_temp_kiln]" class="form-control input-sm item_data" id="jk_temp_kiln" value="<?= (isset($testing_report_data->jk_temp_kiln)) ? $testing_report_data->jk_temp_kiln : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_temp_hot_air" class="col-sm-6 input-sm">Hot Air Temperature.</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_temp_hot_air]" class="form-control input-sm item_data" id="jk_temp_hot_air" value="<?= (isset($testing_report_data->jk_temp_hot_air)) ? $testing_report_data->jk_temp_hot_air : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jk_temp_weather" class="col-sm-6 input-sm">Weather.</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_temp_weather]" class="form-control input-sm item_data" id="jk_temp_weather" value="<?= (isset($testing_report_data->jk_temp_weather)) ? $testing_report_data->jk_temp_weather : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Equipment Testing</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_et_equipment_rpm" class="col-sm-6 input-sm">Equipment RPM</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_et_equipment_rpm]" id="jk_et_equipment_rpm"  class="form-control input-sm" value="<?= (isset($testing_report_data->jk_et_equipment_rpm)) ? $testing_report_data->jk_et_equipment_rpm : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_et_testing" class="col-sm-6 input-sm">Testing</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_et_testing]" id="jk_et_testing" class="form-control input-sm" value="<?= (isset($testing_report_data->jk_et_testing)) ? $testing_report_data->jk_et_testing : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_et_stop_run_time" class="col-sm-6 input-sm">Stop Run Time (Minutes)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_et_stop_run_time]" id="jk_et_stop_run_time" class="form-control input-sm" value="<?= (isset($testing_report_data->jk_et_stop_run_time)) ? $testing_report_data->jk_et_stop_run_time : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Control Panel Board</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_cpb_testing" class="col-sm-6 input-sm">Testing</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_cpb_testing]" id="jk_cpb_testing" class="form-control input-sm" value="<?= (isset($testing_report_data->jk_cpb_testing)) ? $testing_report_data->jk_cpb_testing : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="jk_cpb_working_hours" class="col-sm-6 input-sm">Working Hours</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="testing_report_data[jk_cpb_working_hours]" id="jk_cpb_working_hours" class="form-control input-sm" value="<?= (isset($testing_report_data->jk_cpb_working_hours)) ? $testing_report_data->jk_cpb_working_hours : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold">Details</legend>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <label for="item_serial_no" class="col-sm-12 input-sm">Test Specification</label>
                                                    <div class="col-sm-12">
                                                        <textarea rows="5" class="form-control" name="testing_report_data[test_specification]"><?= (isset($testing_report_data->test_specification)) ? $testing_report_data->test_specification : ''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <label for="item_serial_no" class="col-sm-12 input-sm">Test Detail</label>
                                                    <div class="col-sm-12">
                                                        <textarea rows="5" class="form-control" name="testing_report_data[test_detail]"><?= (isset($testing_report_data->test_detail)) ? $testing_report_data->test_detail : ''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>										
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <?php if (isset($testing_report_data->testing_report_id) && !empty($testing_report_data->testing_report_id)) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_by" id="created_by" value="<?= (isset($testing_report_data->created_by_name)) ? $testing_report_data->created_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_at" id="created_at" value="<?= (isset($testing_report_data->created_at)) ? $testing_report_data->created_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_by" id="updated_by" value="<?= (isset($testing_report_data->updated_by_name)) ? $testing_report_data->updated_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_at" id="updated_at" value="<?= (isset($testing_report_data->updated_at)) ? $testing_report_data->updated_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
            <?php endif; ?>
        </div>
        <section class="content-header">
            <?php if ($testing_report_add_role || $testing_report_edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
            <?php endif; ?>
            <?php if ($testing_report_view_role): ?>
                <a href="<?= base_url() ?>testing_report/testing_report_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Testing Report List</a>
            <?php endif; ?>
            <?php if ($testing_report_add_role): ?>
                <a href="<?= base_url() ?>testing_report/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Testing Report</a>
            <?php endif; ?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if (isset($testing_report_data->testing_report_id) && !empty($testing_report_data->testing_report_id)) {
    
} else {
    ?>
    <div class="modal fade" id="select_challan_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Challan
                        <span class="pull-right">
                            <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                            <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                        </span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="pick_challan_datatable" class="table custom-table table-striped">
                            <thead>
                                <tr>
                                    <th>Challan No</th>
                                    <th>Order / Proforma No</th>
                                    <th>Quotation No</th>
                                    <th>Enquiry No</th>
                                    <th>Item Code</th>
                                    <th>Party</th>
                                    <th>phone No </th>
                                    <th>Email Id</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                </tr>
                            </thead>
                            <tbody></tbody>                        
                        </table>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php } ?>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?= dist_url('js/sales_order.js'); ?>"></script>
<script type="text/javascript">
    var first_time_edit_mode = 1;
<?php if (isset($testing_report_data->testing_report_id) && !empty($testing_report_data->testing_report_id)) { ?>
        first_time_edit_mode = 0;
        feed_challan_data(<?= $testing_report_data->challan_id; ?>);
        $("#ajax-loader").show();
<?php } ?>
    $(document).ready(function () {
        initAjaxSelect2($("#quality_checked_by"), "<?= base_url('app/technical_staff_select2_source') ?>");
<?php if (isset($testing_report_data->quality_checked_by)) { ?>
            setSelect2Value($("#quality_checked_by"), "<?= base_url('app/set_staff_select2_val_by_id/' . $testing_report_data->quality_checked_by) ?>");
<?php } ?>

        initAjaxSelect2($("#equipment_testing_by"), "<?= base_url('app/technical_staff_select2_source') ?>");
<?php if (isset($testing_report_data->equipment_testing_by)) { ?>
            setSelect2Value($("#equipment_testing_by"), "<?= base_url('app/set_staff_select2_val_by_id/' . $testing_report_data->equipment_testing_by) ?>");
<?php } ?>

        initAjaxSelect2($("#equipment_inspection_by"), "<?= base_url('app/technical_staff_select2_source') ?>");
<?php if (isset($testing_report_data->equipment_inspection_by)) { ?>
            setSelect2Value($("#equipment_inspection_by"), "<?= base_url('app/set_staff_select2_val_by_id/' . $testing_report_data->equipment_inspection_by) ?>");
<?php } ?>

        initAjaxSelect2($("#our_site_incharge_person"), "<?= base_url('app/technical_staff_select2_source') ?>");
<?php if (isset($testing_report_data->our_site_incharge_person)) { ?>
            setSelect2Value($("#our_site_incharge_person"), "<?= base_url('app/set_staff_select2_val_by_id/' . $testing_report_data->our_site_incharge_person) ?>");
<?php } ?>

        initAjaxSelect2($("#city"), "<?= base_url('app/city_select2_source') ?>");
        initAjaxSelect2($("#state"), "<?= base_url('app/state_select2_source') ?>");
        initAjaxSelect2($("#country"), "<?= base_url('app/country_select2_source') ?>");
        initAjaxSelect2($("#reference_id"), "<?= base_url('app/reference_select2_source') ?>");



<?php if (isset($testing_report_data->sales_to_party_id)) { ?>
            setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id/' . $testing_report_data->sales_to_party_id) ?>");
            party_details(<?= $testing_report_data->sales_to_party_id; ?>);
<?php } ?>

        var table
        table = $('#pick_challan_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('testing_report/pick_challan_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                    d.request_from = "testing_report";
                }
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

        $('#select_challan_modal').modal({backdrop: 'static', keyboard: false});
        $('#select_challan_modal').modal('show');
        $('#select_challan_modal').on('shown.bs.modal', function () {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

        $(document).on('click', '.challan_row', function () {
            var tr = $(this).closest('tr');
            var challan_id = $(this).data('challan_id');
            $("#challan_id").val(challan_id);
            feed_challan_data(challan_id);
            $('#select_challan_modal').modal('hide');
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_testing_report").submit();
                return false;
            }
        });

        $(document).on('submit', '#save_testing_report', function () {
            if ($.trim($("#party_id").val()) == '') {
                show_notify('Please Select Party.', false);
                return false;
            }
            if ($.trim($("#reference_id").val()) == '') {
                show_notify('Please Select Reference.', false);
                return false;
            }

            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('testing_report/save_testing_report') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.status == 0) {
                        show_notify(json.msg, false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                        return false;
                    }
                    if (json['success'] == 'false') {
                        show_notify(json['msg'], false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                    }
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('testing_report/testing_report_list') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('testing_report/testing_report_list') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });

        $(document).on("change", '#contact_person_id', function () {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?= base_url(); ?>party/get-contact-person-by-id/" + contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });

        $(document).on("change", '#quality_checked_by, #equipment_inspection_by, #equipment_testing_by, #our_site_incharge_person', function () {
            var id = $(this).val();
            var id_attr = $(this).attr('id');
            $.ajax({
                url: "<?= base_url(); ?>testing_report/get_staff_contact_no/" + id,
                type: "POST",
                success: function (data) {
                    $('#' + id_attr + '_contact').val('');
                    $('#' + id_attr + '_contact').val(data);
                }
            });
        });

    });

    function feed_challan_data(challan_id) {
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>testing_report/get_challan',
            type: "POST",
            async: false,
            data: {challan_id: challan_id},
            success: function (data) {
                json = JSON.parse(data);
                //console.log(json);
                var challan_data = json.challan_data;
                var challan_item = json.challan_item_data;

<?php if (isset($testing_report_data->party_id)) {
    
} else {
    ?>
                    setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id') ?>/" + challan_data.sales_to_party_id);
                    $('#party_name').val($('#party_id').val());
                    $('#party_party_id').val(challan_data.sales_to_party_id);

                    $('#manufacturing_m_y').val(challan_data.challan_date.substring(3, 11));
                    $("#delivery_of_equipment").val(challan_data.challan_date);
                    $("#datepicker2").datepicker("setDate", challan_data.challan_date);
                    $("#datepicker3").datepicker("setDate", challan_data.challan_date);
                    party_details($('#party_id').val());
<?php } ?>
                $('#challan_no').val(challan_data.challan_no);
                $('#challan_date').val(challan_data.challan_date);
                $('#quotation_no').val(challan_data.quotation_no);
                $('#sales_order_no').val(challan_data.sales_order_no);
                $('#sales_order_date').val(challan_data.sales_order_date);

                $('#proforma_invoice_no').val(challan_data.proforma_invoice_no);
                $('#proforma_invoice_date').val(challan_data.proforma_invoice_date);

                $('#cust_po_no').val(challan_data.cust_po_no);
                $('#po_date').val(challan_data.po_date);
                $('select[name="testing_report_data[kind_attn_id]"]').val(challan_data.kind_attn_id).change();
                $('#item_id').val(challan_item.item_id);
                $('#item_name').val(challan_item.item_name);
                $('#item_code').val(challan_item.item_code);
                $('#item_serial_no').val(challan_item.item_serial_no);
                $('#item_extra_accessories_id').val(challan_item.item_extra_accessories);
                $('#item_description').val(challan_item.item_description);

                var motor_data = json.motor_data;
                var li_motor_serial_objectdata = json.motor_data;

                var motor_serial_objectdata = [];
                if (li_motor_serial_objectdata != '') {
                    $.each(li_motor_serial_objectdata, function (index, value) {
                        motor_serial_objectdata.push(value);
                    });
                }

                display_motor_serial_html(motor_serial_objectdata);
                var gearbox_data = json.gearbox_data;
                var li_gearbox_serial_objectdata = json.gearbox_data;
                var gearbox_serial_objectdata = [];
                if (li_gearbox_serial_objectdata != '') {
                    $.each(li_gearbox_serial_objectdata, function (index, value) {
                        gearbox_serial_objectdata.push(value);
                    });
                }
                display_gearbox_serial_html(gearbox_serial_objectdata);
                $("#ajax-loader").hide();
            },
        });
    }
    function display_motor_serial_html(motor_serial_objectdata) {
        var motor_testing_details_jk = '';
        $.each(motor_serial_objectdata, function (index, value) {
            var motor_testing_no_jk = '<tr class="motor_serial_index_' + index + '">';
            motor_testing_no_jk += '<td>' + value.motor_user + '</td>';
            motor_testing_no_jk += '<td>' + value.motor_make + '</td>';
            motor_testing_no_jk += '<td>' + value.motor_hp + '</td>';
            motor_testing_no_jk += '<td>' + value.motor_kw + '</td>';
            motor_testing_no_jk += '<td>' + value.motor_frequency + '</td>';
            motor_testing_no_jk += '<td>' + value.motor_rpm + '</td>';
            motor_testing_no_jk += '<td>' + value.motor_volts_cycles + '</td>';
            motor_testing_no_jk += '<td>' + value.motor_serial_no + '</td>';

            var normal_amps_jk = value.normal_amps_jk == null ? '' : value.normal_amps_jk;
            var working_amps_jk = value.working_amps_jk == null ? '' : value.working_amps_jk;
            var total_workinghours_jk = value.total_workinghours_jk == null ? '' : value.total_workinghours_jk;
            motor_testing_no_jk += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][normal_amps_jk]" class="form-control col-sm-1 input-sm" id="normal_amps_jk_' + value.id + '" value="' + normal_amps_jk + '" placeholder="Sound"></td>';
            motor_testing_no_jk += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][working_amps_jk]" class="form-control col-sm-1 input-sm" id="working_amps_jk_' + value.id + '" value="' + working_amps_jk + '" placeholder="Sound"></td>';
            motor_testing_no_jk += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][total_workinghours_jk]" class="form-control col-sm-1 input-sm" id="total_workinghours_jk_' + value.id + '" value="' + total_workinghours_jk + '" placeholder="Hrs"></td>';
            motor_testing_details_jk += motor_testing_no_jk;
        });
        $('tbody#motor_testing_details_jk').html(motor_testing_details_jk);
    }
    function display_gearbox_serial_html(gearbox_serial_objectdata) {
        var gearbox_testing_details_jk = '';
        $.each(gearbox_serial_objectdata, function (index, value) {
            var gearbox_testing_no_jk = '<tr class="gearbox_serial_index_' + index + '">';
            gearbox_testing_no_jk += '<td>' + value.gearbox_user + '</td>';
            gearbox_testing_no_jk += '<td>' + value.gearbox_make + '</td>';
            gearbox_testing_no_jk += '<td>' + value.gearbox_gear_type + '</td>';
            gearbox_testing_no_jk += '<td>' + value.gearbox_model + '</td>';
            gearbox_testing_no_jk += '<td>' + value.gearbox_ratio + '</td>';
            gearbox_testing_no_jk += '<td>' + value.gearbox_serial_no + '</td>';

            var normal_amps_jk = value.normal_amps_jk == null ? '' : value.normal_amps_jk;
            var working_amps_jk = value.working_amps_jk == null ? '' : value.working_amps_jk;
            var total_workinghours_jk = value.total_workinghours_jk == null ? '' : value.total_workinghours_jk;
            gearbox_testing_no_jk += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][normal_amps_jk]" class="form-control col-sm-1 input-sm" id="normal_amps_jk_' + value.id + '" value="' + normal_amps_jk + '" placeholder="Sound"></td>';
            gearbox_testing_no_jk += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][working_amps_jk]" class="form-control col-sm-1 input-sm" id="working_amps_jk_' + value.id + '" value="' + working_amps_jk + '" placeholder="Sound"></td>';
            gearbox_testing_no_jk += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][total_workinghours_jk]" class="form-control col-sm-1 input-sm" id="total_workinghours_jk_' + value.id + '" value="' + total_workinghours_jk + '" placeholder="Hrs"></td>';
            gearbox_testing_details_jk += gearbox_testing_no_jk;

        });
        $('tbody#gearbox_testing_details_jk').html(gearbox_testing_details_jk);
    }

    function party_details(id) {
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?= base_url(); ?>sales/ajax_load_party_with_cnt_person/' + id,
            async: false,
            data: id = 'party_id',
            success: function (data) {
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                } else {
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $(".party_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"), '<?= base_url() ?>app/set_city_select2_val_by_id/' + json['city_id']);
                } else {
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"), '<?= base_url() ?>app/set_state_select2_val_by_id/' + json['state_id']);
                } else {
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"), '<?= base_url() ?>app/set_country_select2_val_by_id/' + json['country_id']);
                } else {
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                } else {
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                } else {
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                } else {
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                } else {
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                } else {
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"), '<?= base_url() ?>app/set_reference_select2_val_by_id/' + json['reference_id']);
                } else {
                    setSelect2Value($("#reference_id"));
                }
                if (json['reference_description']) {
                    $("#reference_description").html(json['reference_description']);
                } else {
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"), '<?= base_url() ?>app/set_sales_select2_val_by_id/' + json['party_type_1_id']);
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_DOMESTIC_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= INR_CURRENCY_ID; ?>);
                    }
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_EXPORT_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= USD_CURRENCY_ID; ?>);
                    }
                    var currency_title = $('#select2-currency_id-container').attr('title');
                    $('#received_payment_currency').html(currency_title);
                } else {
                    setSelect2Value($("#sales_id"));
                }

<?php if (isset($testing_report_data->testing_report_id) && !empty($testing_report_data->testing_report_id)) {
    
} else {
    ?>
                    if (json['branch_id']) {
                        setSelect2Value($("#branch_id"), "<?= base_url('app/set_branch_select2_val_by_id') ?>/" + json['branch_id']);
                    } else {
                        setSelect2Value($("#branch_id"));
                    }

                    if (json['agent_id']) {
                        setSelect2Value($("#agent_id"), '<?= base_url() ?>app/set_agent_select2_val_by_id/' + json['agent_id']);
                    } else {
                        setSelect2Value($("#agent_id"));
                    }
<?php } ?>

                if (first_time_edit_mode == 1) {
                    if (json['contact_persons_array']) {
                        var option_html = '';
                        if (json['contact_persons_array'].length > 0) {
                            //                            console.log(json['contact_persons_array']);
                            $.each(json['contact_persons_array'], function (index, value) {
                                option_html += "<option value='" + value.contact_person_id + "'>" + value.name + "</option>";
                            })
                            $('select[name="testing_report_data[kind_attn_id]"]').html(option_html).select2();
                        } else {
                            $('select[name="testing_report_data[kind_attn_id]"]').html('');
                        }
                        $('select[name="testing_report_data[kind_attn_id]"]').change();
                    }
                } else {
                    first_time_edit_mode = 1;
                }
                $("#ajax-loader").hide();
            }
        });
    }

<?php if (isset($_GET['view'])) { ?>
        $(window).load(function () {
            display_as_a_viewpage();
        });
<?php } ?>
</script>
