<?php 
$column_cnt = 18;
$CURRENCY = "INR";
if($testing_report_data->currency != ''){
	$CURRENCY = strtoupper($testing_report_data->currency);
}
?>
<html>
	<head>
		<title>Testing</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;				
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:10px;
			}
			tr > td:last-child{
				border-right: 1px solid !important;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:12px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 82x;
				vertical-align: bottom;
				/*width: 33.33%;*/
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #000000;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="4">Testing</td>
			</tr>
			<tr>
				<td class="text-bold text-center"><b>Manufacturer & Supplier</b></td>
				<td class="text-bold" >Report No. </td>
				<td class="text-bold" ><b><?=$testing_report_data['testing_report_no_year'];?></b></td>
				<td class="text-bold"><b><?=strtotime($testing_report_data['testing_report_date']) != 0 ? date('d/m/Y',strtotime($testing_report_data['testing_report_date'])) : date('d/m/Y')?></b>
				</td>
			</tr>
			<tr>
				<td class="text-bold" rowspan="6" >
					<span style="font-size:15px;"><b><?=$company_details['name']?></b></span><br />
					<?=nl2br($company_details['address']);?><br />
					City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
					Email: <?=$company_details['email_id'];?><br />
					Tel No. : <?=$company_details['contact_no'];?>,<br />
					Contact No. : <?=$company_details['cell_no'];?><br />
					<b>GST No : <?=$company_details['gst_no'];?></b>
				</td>
				<td class="text-bold" >Challan No. </td>
				<td class="text-bold" ><b><?=$testing_report_data['challan_no'];?></b></td>
				<td class="text-bold"><b><?=strtotime($testing_report_data['challan_date']) != 0 ? date('d/m/Y',strtotime($testing_report_data['challan_date'])):'';?>	</b></td>
			</tr>
			<tr>
				<td class="text-bold" align="left">Purchase Order No. </td>
				<td class="text-bold"><b>
					<?=$testing_report_data['cust_po_no'];?></b>
				</td>
				<td class="text-bold"><b>
					<?=strtotime($testing_report_data['po_date']) != 0 ? date('d/m/Y',strtotime($testing_report_data['po_date'])):'';?>	
					</b></td>
			</tr>
			<tr>
				<td class="text-bold" align="left">Quotation No. </td>
				<td class="text-bold" colspan="2"><b>
					<?=$testing_report_data['quotation_no'];?></b>
				</td>				
			</tr>
			<?php if(!empty($testing_report_data['sales_order_no'])){ ?>
			<tr>
				<td class="text-bold" align="left">Sales Order No.</td>
				<td class="text-bold"><b>
					<?=$this->applib->get_sales_order_no($testing_report_data['sales_order_no']);?></b>
				</td>
				<td class="text-bold"><b>
					<?=strtotime($testing_report_data['sales_order_date']) != 0 ? date('d/m/Y',strtotime($testing_report_data['sales_order_date'])):'';?>
					</b></td>
			</tr>
			<?php }else{ ?>
			<tr>
				<td class="text-bold" align="left">Proforma Invoice No.</td>
				<td class="text-bold"><b>
					<?=$testing_report_data['proforma_invoice_no'];?></b>
				</td>
				<td class="text-bold"><b>
					<?=strtotime($testing_report_data['proforma_invoice_date']) != 0 ? date('d/m/Y',strtotime($testing_report_data['proforma_invoice_date'])):'';?>
					</b></td>
			</tr>
			<?php }?>

			<tr>
				<td class="text-bold" align="left">Manufacturing Month & Year </td>
				<td class="text-bold" colspan="2"><b><?=$testing_report_data['manufacturing_m_y']?></b></td>
			</tr>
			<tr>
				<td class="text-bold">Assembling Equipment </td>
				<td class="text-bold" colspan="2"><b><?=strtotime($testing_report_data['assembling_equipment']) != 0 ? date('d/m/Y',strtotime($testing_report_data['assembling_equipment'])):'';?></b></td>
			</tr>
			<tr>
				<td class="text-center text-bold width-50-pr"><b>Purchaser</b></td>
				<td class="text-bold" >Quality Checked By </td>
				<td class="text-bold" colspan="2"><b><?=$testing_report_data['quality_checked_by']?></b></td>
			</tr>
			<tr>
				<td class="text-bold" rowspan="5" valign="top" >
					<span style="font-size:15px;"><b><?=$testing_report_data['party_name']?></b><br /></span>
					<?=nl2br($testing_report_data['address']);?>
					City : <?=$testing_report_data['city']?> - <?=$testing_report_data['pincode']?> (<?=$testing_report_data['state']?>) <?=$testing_report_data['country']?>.<br />
					Email : <?=$testing_report_data['party_email_id'];?> <?php //<?=explode(",", $testing_report_data->party_email_id)[0];?><br />
					Tel No.: <?=$testing_report_data['fax_no'];?>,<br />
					Contact No.: <?=$testing_report_data['p_phone_no'];?>, <?php //$testing_report_data->contact_person_phone?><br />
					Contact Person: <?=$testing_report_data['contact_person_name']?><br />
					<?php if($testing_report_data['party_type_1'] == PARTY_TYPE_DOMESTIC_ID){ ?>
					<b>GST No. : <?=$testing_report_data['party_gst_no'];?></b><?= isset($testing_report_data['party_cin_no']) && !empty($testing_report_data['party_cin_no']) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$testing_report_data['party_cin_no'].'</b>' : ''; ?>
					<?php } ?>
				</td>
				<td class="text-bold" >Equipment Testing </td>
				<td class="text-bold" colspan="2"><b><?=strtotime($testing_report_data['equipment_testing_date']) != 0 ? date('d/m/Y',strtotime($testing_report_data['equipment_testing_date'])):'';?></b></td>
			</tr>
			<tr>
				<td class="text-bold" >Equipment Testing By </td>
				<td class="text-bold" colspan="2"><b><?=$testing_report_data['equipment_testing_by']?></b></td>
			</tr>
			<tr>
				<td class="text-bold" >Equipment Inspection By </td>
				<td class="text-bold" colspan="2"><b><?=$testing_report_data['equipment_inspection_by']?></b></td>
			</tr>
			<tr>
				<td class="text-bold" >Delivery of Equipment </td>
				<td class="text-bold" colspan="2"><b>
					<?=strtotime($testing_report_data['delivery_of_equipment']) != 0 ? date('d/m/Y',strtotime($testing_report_data['delivery_of_equipment'])):'';?></b>
				</td>
			</tr>
			<tr>
				<td class="text-bold" >Incharge Person </td>
				<td class="text-bold" colspan="2"><b>
					<?=!empty($testing_report_data['our_site_incharge_person_text']) ? $testing_report_data['our_site_incharge_person_text'] : $testing_report_data['our_site_incharge_person'];?></b>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td class="text-bold" colspan="10" align="center"><b>Item Code</b></td>
				<td class="text-bold" colspan="10" align="center"><b>Equipment Name</b></td>
				<td class="text-bold" colspan="10" align="center"><b>Equipment Serial No.</b></td>

			</tr>
			<tr>
				<td class="text-bold" colspan="10" align="center"><?=$testing_report_data['item_code'];?></td>
				<td class="text-bold" colspan="10" align="center"><?=$testing_report_data['item_name'];?></td>
				<td class="text-bold" colspan="10" align="center"><b><?=$testing_report_data['item_serial_no'];?></b></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="30"><b>Electric Motor & Equipment Testing No Load Report At Jay Khodiyar Factory</b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="3" align="center"><b>User</b></td>
				<td class="text-bold" colspan="2" align="center"><b>HP</b></td>
				<td class="text-bold" colspan="3" align="center"><b>KW</b></td>
				<td class="text-bold" colspan="2" align="center"><b>Frequency</b></td>
				<td class="text-bold" colspan="2" align="center"><b>RPM</b></td>
				<td class="text-bold" colspan="4" align="center"><b>Volts</b></td>
				<td class="text-bold" colspan="2" align="center"><b>Serial No.</b></td>
				
				<td class="text-bold" colspan="3" align="center"><b>Normal Amps</b></td>
				<td class="text-bold" colspan="4" align="center"><b>Working Amps</b></td>
				<td class="text-bold" colspan="5" align="center"><b>Total Working Hours</b></td>
			</tr>
			<?php if(!empty($motor_data)){ foreach($motor_data as $motor){ ?>
			<tr>
				<td class="text-bold" colspan="3" align="center"><?=$motor->motor_user;?></td>
				<td class="text-bold" colspan="2" align="center"><?=$motor->motor_hp;?></td>
				<td class="text-bold" colspan="3" align="center"><?=$motor->motor_kw;?></td>
				<td class="text-bold" colspan="2" align="center"><?=$motor->motor_frequency;?></td>
				<td class="text-bold" colspan="2" align="center"><?=$motor->motor_rpm;?></td>
				<td class="text-bold" colspan="4" align="center"><?=$motor->motor_volts_cycles;?></td>
				<td class="text-bold" colspan="2" align="center"><?=$motor->motor_serial_no;?></td>
				<td class="text-bold" colspan="3" align="center"><?=$motor->normal_amps_jk;?></td>
				<td class="text-bold" colspan="4" align="center"><?=$motor->working_amps_jk;?></td>
				<td class="text-bold" colspan="5" align="center"><?=$motor->total_workinghours_jk;?></td>
			</tr>
			<?php } } ?>
			<tr>
				<td class="text-bold" colspan="3" align="center"><b>User</b></td>
				<td class="text-bold" colspan="3" align="center"><b>Make</b></td>
				<td class="text-bold" colspan="3" align="center"><b>Gear Type</b></td>
				<td class="text-bold" colspan="4" align="center"><b>Model</b></td>
				<td class="text-bold" colspan="3" align="center"><b>Ratio</b></td>
				<td class="text-bold" colspan="2" align="center"><b>Serial No.</b></td>
				<td class="text-bold" colspan="3" align="center"><b>Normal Sound</b></td>
				<td class="text-bold" colspan="4" align="center"><b>Working Sound</b></td>
				<td class="text-bold" colspan="5" align="center"><b>Total Working Hours</b></td>
			</tr>
			<?php if(!empty($gearbox_data)){ foreach($gearbox_data as $gearbox){ ?>
                <tr>
                    <td class="text-bold" colspan="3" align="center"><?=$gearbox->gearbox_user;?></td>
                    <td class="text-bold" style="white-space:nowrap;" colspan="3" align="center"><?=$gearbox->gearbox_make;?></td>
                    <td class="text-bold" colspan="3" align="center"><?=$gearbox->gearbox_gear_type;?></td>
                    <td class="text-bold" colspan="4" align="center"><?=$gearbox->gearbox_model;?></td>
                    <td class="text-bold" colspan="3" align="center"><?=$gearbox->gearbox_ratio;?></td>
                    <td class="text-bold" colspan="2" align="center"><?=$gearbox->gearbox_serial_no;?></td>
                    <td class="text-bold" colspan="3" align="center"><?=$gearbox->normal_amps_jk;?></td>
                    <td class="text-bold" colspan="4" align="center"><?=$gearbox->working_amps_jk;?></td>
                    <td class="text-bold" colspan="5" align="center"><?=$gearbox->total_workinghours_jk;?></td>
                </tr>
			<?php } } else { ?>
                <tr>
                    <td class="text-bold" colspan="3" align="center">&nbsp;</td>
                    <td class="text-bold" style="white-space:nowrap;" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="4" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="2" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="4" align="center"></td>
                    <td class="text-bold" colspan="5" align="center"></td>
                </tr>
			<?php } ?>

			<tr>
				<td class="text-center text-bold" colspan="30"><b>Equipment Testing Normal Load Report At Jay Khodiyar Factory</b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="7" align="center"><b>Testing Capacity</b></td>
				<td class="text-bold" colspan="3" align="center"><b>Capacity Hrs</b></td>
				<td class="text-bold" colspan="6" align="center"><b>Test of Raw Material</b></td>
				<td class="text-bold" colspan="6" align="center"><b>Moisture of raw material</b></td>
				<td class="text-bold" colspan="8" align="center"><b>After Procces of Moisture</b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="7" align="center"><b>Testing Normal Load</b></td>
				<td class="text-bold" colspan="3" align="center"><?=$testing_report_data['jk_testing_normal_capacity_hrs'];?></td>
				<td class="text-bold" colspan="6" align="center"><?=$testing_report_data['jk_testing_normal_test_raw_material'];?></td>
				<td class="text-bold" colspan="6" align="center"><?=$testing_report_data['jk_testing_normal_moisture_raw_material'];?></td>
				<td class="text-bold" colspan="8" align="center"><?=$testing_report_data['jk_testing_normal_after_procces_moisture'];?></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="7" align="center"><b>Testing Full Load</b></td>
				<td class="text-bold" colspan="3" align="center"><?=$testing_report_data['jk_testing_full_capacity_hrs'];?></td>
				<td class="text-bold" colspan="6" align="center"><?=$testing_report_data['jk_testing_full_test_raw_material'];?></td>
				<td class="text-bold" colspan="6" align="center"><?=$testing_report_data['jk_testing_full_moisture_raw_material'];?></td>
				<td class="text-bold" colspan="8" align="center"><?=$testing_report_data['jk_testing_full_after_procces_moisture'];?></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="30"><b>Temperature Parameters At Jay Khodiyar Factory</b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="3" align="center"> <b>Oil Temperature</b> </td>
				<td class="text-bold" colspan="2" align="center"><?=$testing_report_data['jk_temp_oli'];?></td>
				<td class="text-bold" colspan="3" align="center"> <b>Equipment Temperature</b> </td>
				<td class="text-bold" colspan="2" align="center"><?=$testing_report_data['jk_temp_eqipment'];?></td>
				<td class="text-bold" colspan="3" align="center"> <b>Head Temperature</b> </td>
				<td class="text-bold" colspan="3" align="center"><?=$testing_report_data['jk_temp_head'];?></td>
				<td class="text-bold" colspan="2" align="center"> <b>Kiln Temperature</b> </td>
				<td class="text-bold" colspan="2" align="center"><?=$testing_report_data['jk_temp_kiln'];?></td>
				<td class="text-bold" colspan="3" align="center"> <b>Hot Air Temperature</b> </td>
				<td class="text-bold" colspan="2" align="center"><?=$testing_report_data['jk_temp_hot_air'];?></td>
				<td class="text-bold" colspan="2" align="center"> <b>weather</b> </td>
				<td class="text-bold" colspan="3" align="center"><?=$testing_report_data['jk_temp_weather'];?></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="30" align="center"><b>Equipment Testing</b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="5" align="center"><b>Equipment RPM</b></td>
				<td class="text-bold" colspan="5" align="center"><?=$testing_report_data['jk_et_equipment_rpm'];?></td>
				<td class="text-bold" colspan="5" align="center"><b>Testing</b></td>
				<td class="text-bold" colspan="5" align="center"><?=$testing_report_data['jk_et_testing'];?></td>
				<td class="text-bold" colspan="6" align="center"><b>Stop Run Time (Minutes)</b></td>
				<td class="text-bold" colspan="4" align="center"><?=$testing_report_data['jk_et_stop_run_time'];?></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="30" align="center"><b>Control Panel Board</b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="5" align="center"><b>Testing</b></td>
				<td class="text-bold" colspan="10" align="center"><?=$testing_report_data['jk_cpb_testing'];?></td>
				<td class="text-bold" colspan="5" align="center"><b>Working Hours</b></td>
				<td class="text-bold" colspan="10" align="center"><?=$testing_report_data['jk_cpb_working_hours'];?></td>
			</tr>

			<tr>
				<td class="text-center text-bold" colspan="15">TEST SPECIFICATION</td>
				<td class="text-center text-bold" colspan="15"> TEST DETAIL</td>
			</tr>
			<tr>
				<td valign="top" class="text-bold" style="height:42px;" colspan="15"><?=nl2br($testing_report_data['test_specification']);?></td>
				<td valign="top" class="text-bold" style="height:42px;" colspan="15"><?=nl2br($testing_report_data['test_detail']);?></td>
			</tr>
			<tr>
				<td valign="top" class="text-bold" style="height:82x;" colspan="30"><b>Declaration</b> : <?=nl2br($testing_report_data['test_declaration']);?></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10">
					<?=!empty($testing_report_data['our_site_incharge_person_text']) ? $testing_report_data['our_site_incharge_person_text'] : $testing_report_data['our_site_incharge_person'];?>
				</td>
				<td class="text-center text-bold" colspan="10"> 
					<?=isset($testing_report_data['quality_checked_by']) ? $testing_report_data['quality_checked_by'] : '';?>
				</td>
				<td class="text-center text-bold" colspan="10"> For, Jay Khodiyar Machine Tools </td>
			</tr>
			<tr>
				<td class="text-center text-bold footer-sign-area" colspan="10"> &nbsp; </td>
				<td class="text-center text-bold footer-sign-area no-border-bottom" colspan="10"> &nbsp; </td>
				<td class="text-center text-bold footer-sign-area" colspan="10"> &nbsp; </td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10"><?=!empty($testing_report_data['our_site_incharge_person_text']) ? $testing_report_data['our_site_incharge_person_contact'] : $testing_report_data['our_site_incharge_person_contect_staff'];?></td>
				<td class="text-center text-bold no-border-top no-border-bottom" colspan="10"></td>
				<td class="text-center text-bold no-border-top no-border-bottom" colspan="10">&nbsp; </td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10"><b>Incharge Person</b></td>
				<td class="text-center text-bold" colspan="10"><b>Checked By</b></td>
				<td class="text-center text-bold" colspan="10"><b>Authorized Signature</b></td>
			</tr>
		</table>
	</body>
</html>
