<?php $column_cnt = 8; ?>
<html>
	<head>
		<title>Plat Print</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
			}
			td{
				padding: 5px 5px;
				font-size:14px;
				margin-bottom: 5px;
			}
			.show_border {
				border: 1px solid;
				width: 25%;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:14px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				width: 33.33%;
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #698c66;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
	
        <div style="font-size:10px; color:#333; clear:both;">
			<div style="width:100%;">
				<div style="text-align: center;">
					<img src="<?php echo base_url(); ?>resource/dist/img/credit/dandb-logo.jpg" />
					<img src="<?php echo base_url(); ?>resource/dist/img/credit/urs-logo.jpg"  />
					<img src="<?php echo base_url(); ?>resource/dist/img/credit/tuv-logo.jpg"  />
				</div>
			</div>
			<div style="float:right; width:100%; color:#333; font-size:10px; line-height:16px;">
				<h2>Manufactured By :</h2>
				<img src="<?php echo base_url(); ?>resource/dist/img/credit/footer-jaykhodiyar-logo.jpg"  width="100%" / >
				<p style="font-size: 14px; text-align: center; line-height: 18px;">Samrat Industrial Area Street No.2, Kaneriya Oil Industries, Near Atual Gas Agency, Gondal Road,	Rajkot - 360 004 (Gujrat) INDIA Ph.: +91-281-2367512, 2367594, 2388115, <br>
				Fax: +91-281-2373344  Mobile No. +91 98253 25361<br>
				Email: export@jaykhodiyargruop.com , 
				Web: www.jaykhodiyargruop.com , SKYPE ID: jkbriquetting
			   </p>
			</div>
		</div>
		<hr />

		<table style="margin-top: 10px;">
			<tr>
				<td class="text-left text-bold">Name of Trade Mark</td>
				<td class="show_border"><?=$testing_report_data->trademark_name?></td>
				<td class="text-left text-bold">Model :</td>
				<td class="show_border"><?=$item_row['item_code']?></td>
			</tr>
			<tr><td colspan="4"></td></tr>
			<tr>
				<td class="text-left text-bold">Full Load Current</td>
				<td class="show_border"><?=$testing_report_data->full_load_current?></td>
				<td class="text-left text-bold">Sr.No.</td>
				<td class="show_border"><?=$testing_report_data->serial_no?></td>
			</tr>
			<tr><td colspan="4"></td></tr>
			<tr>
				<td class="text-left text-bold">Rated Voltage</td>
				<td class="show_border"><?=$testing_report_data->rated_voltage?></td>
				<td class="text-left text-bold">Nos. of Phases</td>
				<td class="show_border"><?=$testing_report_data->phases_no?></td>
			</tr>
			<tr><td colspan="4"></td></tr>
			<tr>
				<td class="text-left text-bold">Short Circuit Rating</td>
				<td class="show_border"><?=$testing_report_data->short_circuit_rating?></td>
				<td class="text-left text-bold">Capacity</td>
				<td class="show_border"><?=$testing_report_data->capacity?></td>
			</tr>
			<tr><td colspan="4"></td></tr>
			<tr>
				<td class="text-left text-bold">Operating Manual Ref.No.</td>
				<td class="show_border"><?=$testing_report_data->operating_manual_reference_no?></td>
				<td class="text-left text-bold"></td>
			</tr>
			<tr><td colspan="4"></td></tr>
			<tr>
				<td class="text-left text-bold">Gross Weight</td>
				<td class="show_border"><?=$testing_report_data->gross_weight?> Kgs (Approx)</td>
				<td class="text-left text-bold">Manufacturing Month/Year</td>
				<td class="show_border"><?=$testing_report_data->manufacturing_monthYear?></td>
			</tr>
		</table>
	</body>
</html>
