<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Finance : Invoice - <span id="partyname"></span></small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
            <?php
            $invoice_add_role = $this->app_model->have_access_role(INVOICE_MODULE_ID, "add");
            $invoice_view_role = $this->app_model->have_access_role(INVOICE_MODULE_ID, "view");
            ?>
            <?php if($invoice_add_role): ?>
            <button type="button" onclick="submit_form()" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Save</button>
            <?php endif;?>
            <?php if($invoice_view_role): ?>
            <a href="<?= base_url()?>finance/invoice-list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Load Invoice</a>
            <?php endif;?>
            <?php if($invoice_add_role): ?>
            <a href="<?= base_url()?>finance/invoice/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add Invoice</a>
            <?php endif;?>
        </h1>
    </section>

    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <form action="<?= base_url('finance/save_invoice') ?>" class="" id="lead_form" name="lead_form" method="post">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">

                        <li class="active"><a href="#tab_1" data-toggle="tab">Sales Invoice</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Items</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Billing Terms</a></li>
                        <!--<li><a href="#tab_7" data-toggle="tab" id="so_general_tab">General</a></li>-->
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="invoice_data[sales_order_id]" class="sales_order_id" id="sales_order_id">
                                    <input type="hidden" name="invoice_data[invoice_id]" id="invoice_id" value="">
									<fieldset class="scheduler-border">
									<legend class="scheduler-border text-primary text-bold">Sales Invoice Detail</legend>
									<div class="row" style="margin-top: 10px;">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label for="invoice_no" style="" class="col-sm-3 input-sm">Invoice No.</label>
													<div class="col-sm-9">
														<input type="text" class="invoice_no form-control input-sm" id="invoice_no" name="invoice_data[invoice_no]" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="invoice_date" class="col-sm-3 input-sm">Invoice Date</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm input-datepicker invoice_date" id="invoice_date" placeholder="" name="invoice_data[invoice_date]" value="<?=date('d-m-Y');?>">
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="challan_id" style="" class="col-sm-3 input-sm">Challan No.</label>
													<div class="col-sm-9">
														<input type="text" class="challan_id form-control input-sm" id="challan_id" name="invoice_data[challan_id]" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Challan Date</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm input-datepicker challan_date" id="challan_date" readonly="readonly" >
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="" style="" class="col-sm-3 input-sm">Quotation No.</label>
													<div class="col-sm-9">
														<input type="text" class="quotation_no form-control input-sm" id="quotation_no" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="purchase_order_no" style="" class="col-sm-3 input-sm">Purchase Order No.</label>
													<div class="col-sm-9">
														<input type="text" class="purchase_order_no form-control input-sm" id="cust_po_no" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="purchase_order_date" style="" class="col-sm-3 input-sm">Purchase Order Date.</label>
													<div class="col-sm-9">
														<input type="text" class="purchase_order_date form-control input-sm" id="po_date" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="sales_order_no" style="" class="col-sm-3 input-sm">Sales Order No.</label>
													<div class="col-sm-9">
														<input type="text" class="sales_order_no form-control input-sm" id="sales_order_no" name="invoice_data[sales_order_no]" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="sales_order_date" style="" class="col-sm-3 input-sm">Sales Order Date.</label>
													<div class="col-sm-9">
														<input type="text" class="sales_order_date form-control input-sm" id="sales_order_date" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Invoice Type</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="invoice_data[invoice_type_id]" class="invoice_type_id form-control input-sm" id="invoice_type_id">
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Currency</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="invoice_data[currency_id]" class="currency_id form-control input-sm" id="currency_id">
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Sales</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="invoice_data[sales_id]" class="sales_id form-control input-sm" id="sales_id">
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="loading_at" class="col-sm-3 input-sm">Loading At</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm " id="loading_at"  name="invoice_data[loading_at]" value="Factory-Rajkot" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="port_of_loading" class="col-sm-3 input-sm">Port of Loading</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm " id="port_of_loading"  name="invoice_data[port_of_loading]" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="port_of_discharge" class="col-sm-3 input-sm">Port of Discharge</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm " id="port_of_discharge"  name="invoice_data[port_of_discharge]" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="port_place_of_delivery" class="col-sm-3 input-sm">Place of Delivery</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm " id="port_place_of_delivery"  name="invoice_data[port_place_of_delivery]" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="delivery_through_id" class="col-sm-3 input-sm">Delivery Through</label>
													<div class="col-sm-9">
														<select name="invoice_data[delivery_through_id]" class="delivery_through_id form-control input-sm select2" id="delivery_through_id" ></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="name_of_shipment" class="col-sm-3 input-sm">Name of Shipment</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm " id="name_of_shipment"  name="invoice_data[name_of_shipment]" >
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group" >
													<label for="freight" class="col-sm-3 input-sm">Freight</label>
													<div class="col-sm-3">
														<input type="text" class="form-control input-sm " id="freight"  name="invoice_data[freight]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>CGST %</small> <input type="text" class="form-control input-sm " id="freight_cgst"  name="invoice_data[freight_cgst]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>SGST %</small> <input type="text" class="form-control input-sm " id="freight_sgst"  name="invoice_data[freight_sgst]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>IGST %</small> <input type="text" class="form-control input-sm " id="freight_igst"  name="invoice_data[freight_igst]" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group" >
													<label for="packing_forwarding" class="col-sm-3 input-sm">Packing & Forwarding</label>
													<div class="col-sm-3">
														<input type="text" class="form-control input-sm " id="packing_forwarding"  name="invoice_data[packing_forwarding]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>CGST %</small> <input type="text" class="form-control input-sm " id="packing_forwarding_cgst"  name="invoice_data[packing_forwarding_cgst]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>SGST %</small> <input type="text" class="form-control input-sm " id="packing_forwarding_sgst"  name="invoice_data[packing_forwarding_sgst]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>IGST %</small> <input type="text" class="form-control input-sm " id="packing_forwarding_igst"  name="invoice_data[packing_forwarding_igst]" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group" >
													<label for="transit_insurance" class="col-sm-3 input-sm">Transit Insurance</label>
													<div class="col-sm-3">
														<input type="text" class="form-control input-sm " id="transit_insurance"  name="invoice_data[transit_insurance]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>CGST %</small> <input type="text" class="form-control input-sm " id="transit_insurance_cgst"  name="invoice_data[transit_insurance_cgst]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>SGST %</small> <input type="text" class="form-control input-sm " id="transit_insurance_sgst"  name="invoice_data[transit_insurance_sgst]" >
													</div>
													<div class="col-sm-2" style="padding-left: 0px;">
														<small>IGST %</small> <input type="text" class="form-control input-sm " id="transit_insurance_igst"  name="invoice_data[transit_insurance_igst]" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group" style="margin-top:3px;">
													<label for="" class="col-sm-3 input-sm">Notes</label>
													<div class="col-sm-9">
														<textarea class="form-control" name="invoice_data[notes]" rows="2" ></textarea>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">
										<input type="hidden" name="buyer_data[invoice_id]" class="invoice_id">
										<input type="hidden" name="buyer_data[party_id]" class="party_id" id="party_id">
										<legend class="scheduler-border text-primary text-bold">
											Party Detail
										</legend>

										<div class="row" style="margin-top: 10px;">
											
											<div class="col-md-6">
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select class="sales_to_party_id form-control input-sm" id="sales_to_party_id" disabled="disabled"></select>
														<input type="hidden" name="invoice_data[sales_to_party_id]" id="party_name" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Party Code</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm party_code" id="party_code" name="buyer_data[party_code]" readonly="readonly">
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
														<!--<input type="text" class="form-control input-sm address" id="address" name="buyer_data[address]"> -->
														<textarea class="form-control address" id="address" name="buyer_data[address]"></textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group margin-top-5px">
													<label for="" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="buyer_data[city_id]" class="city_id form-control input-sm" id="city_id" onchange="getStateDetails(this.value)"></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" class="form-control input-sm pull-right pincode" id="pincode" name="buyer_data[pincode]">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group margin-top-5px">
													<label for=""
														   class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="buyer_data[state_id]" class="state_id form-control input-sm" id="state_id">
															<option value="">--Select--</option>
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group margin-top-5px">
													<label for="" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="buyer_data[country_id]" class="country_id form-control input-sm" id="country_id">
															<option value="">--Select--</option>
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea class="email_id form-control" rows="2" id="email_id" name="buyer_data[email_id]"></textarea>
														<small>Add multiple emails by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<?php /*<div class="form-group">
													<label for="" class="col-sm-3 input-sm">GST No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm pull-right gst_no" id="gst_no" name="buyer_data[gst_no]" value="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">PAN No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm pull-right pan_no" id="pan_no" name="buyer_data[pan_no]">
													</div>
												</div>*/ ?>
											</div>
											<div class="col-md-6">
												<div class="form-group margin-top-5px">
													<label for="" class="col-sm-3 input-sm">Tel No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm pull-right fax_no" id="fax_no" name="buyer_data[fax_no]">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Contact No.</label>
													<div class="col-sm-9">
														<textarea class="form-control phone_no" rows="2" id="phone_no" name="buyer_data[phone_no]"></textarea>
														<small>Add multiple phone number by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Website</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm pull-right website" id="website" name="buyer_data[website]">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9">
														<select name="invoice_data[kind_attn_id]" class="kind_attn_id form-control input-sm" id="kind_attn_id">
															<option value="">--Select--</option>
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Designation</label>
													<div class="col-sm-9">
														<div id="contact_person_designation"><?php echo isset($contact_person) ? $contact_person[0]->designation : '' ;?></div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Department</label>
													<div class="col-sm-9">
														<div id="contact_person_department"><?php echo isset($contact_person) ? $contact_person[0]->department : '' ;?></div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">
														Mobile No.
													</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person) ? $contact_person[0]->mobile_no : '' ;?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">
														Email
													</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person) ? $contact_person[0]->email : '' ;?>">
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</fieldset>
							    </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset class="scheduler-border invoice-item-section">
                                                <legend class="scheduler-border text-primary text-bold">Item Details</legend>
                                                <input type="hidden" class="invoice_item_id" id="invoice_item_id" value="0">
                                                <input type="hidden" class="edit_delete_item_id" id="edit_delete_item_id" name="edit_delete_item_id" value="0">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-6">
                                                            <select class="item_id item_data form-control input-sm select2" id="item_id" data-input_name="item_id" onchange="item_details(this.value)">
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button type="button" class="btn btn-info btn-xs btn-add-item">
                                                                Add Item
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">
                                                            Item Category
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <select class="item_category_id item_data form-control input-sm" id="item_category_id" data-input_name="item_category_id">
                                                                <?php foreach($item_category as $item_category_row): ?>
                                                                    <option value="<?=$item_category_row->id?>"><?=$item_category_row->item_category;?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">
                                                            Item Code
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <input type="hidden" class="item_name item_data" id="item_name" data-input_name="item_name" placeholder="">
                                                            <input type="text" data-input_name="item_code" class="input-sm form-control item_data item_code disabled" id="item_code" value="" readonly="readonly">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Item Description</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="item_description form-control input-sm item_data" id="item_description" data-input_name="item_description" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person" class="col-sm-3 input-sm"> Transport Person </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="contact_person form-control input-sm item_data" id="contact_person" data-input_name="contact_person" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_no" class="col-sm-3 input-sm"> Contact No. </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="contact_no form-control input-sm item_data" id="contact_no" data-input_name="contact_no" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="driver_name" class="col-sm-3 input-sm"> Driver Name </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="driver_name form-control input-sm item_data" id="driver_name" data-input_name="driver_name" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="driver_contact_no" class="col-sm-3 input-sm"> Driver Contact No. </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="driver_contact_no form-control input-sm item_data" id="driver_contact_no" data-input_name="driver_contact_no" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="item_serial_no" class="col-sm-3 input-sm"> Item Serial No. </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="item_serial_no form-control input-sm item_data" id="item_serial_no" data-input_name="item_serial_no" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
												</div>
												<div class="col-md-6">
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="quantity form-control input-sm item_data num_only" id="quantity" data-input_name="quantity" placeholder="" readonly="readonly" >
                                                        </div>
                                                        <label for="" class="col-sm-3 input-sm">Rate<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="rate form-control input-sm item_data num_only" id="rate" data-input_name="rate" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Disc(%)</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="disc_per form-control input-sm item_data num_only" id="disc_per" data-input_name="disc_per" placeholder="">
                                                        </div>
                                                        <label for="" class="col-sm-3 input-sm">Disc(Amount)</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="disc_value form-control input-sm item_data num_only" id="disc_value" data-input_name="disc_value" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">IGST %</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control input-sm igst item_data" id="igst" data-input_name="igst" placeholder="">
                                                        </div>
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">IGST Amount</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control input-sm igst_amount item_data" readonly id="igst_amount" data-input_name="igst_amount" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">CGST %</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control input-sm cgst numberwithoutzero item_data" id="cgst" data-input_name="cgst" placeholder="">
                                                        </div>
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">CGST Amount</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control input-sm cgst_amount numberwithoutzero item_data" readonly id="cgst_amount" data-input_name="cgst_amount" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">SGST %</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control input-sm sgst item_data" id="sgst" data-input_name="sgst" placeholder="">
                                                        </div>
                                                        <label for="inputEmail3" class="col-sm-3 input-sm">SGST Amount</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control input-sm sgst_amount item_data" readonly id="sgst_amount" data-input_name="sgst_amount" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Total Amount</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="total_amount form-control input-sm item_data disabled" id="total_amount" data-input_name="total_amount" placeholder="" readonly>
                                                        </div>
                                                        <div class="col-sm-3">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="item_extra_accessories_id" class="col-sm-3 input-sm">Item Extra Accessories</label>
														<div class="col-sm-9">
															<select class="item_extra_accessories_id form-control input-sm select2 item_data" id="item_extra_accessories_id" data-input_name="item_extra_accessories_id" ></select>
														</div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="truck_container_no" class="col-sm-3 input-sm"> Truck / Container No. </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="truck_container_no form-control input-sm item_data" id="truck_container_no" data-input_name="truck_container_no" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="lr_bl_no" class="col-sm-3 input-sm"> LR / BL No. </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="lr_bl_no form-control input-sm item_data" id="lr_bl_no" data-input_name="lr_bl_no" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="lr_date" class="col-sm-3 input-sm"> LR Date </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="lr_date form-control input-sm item_data input-datepicker" id="lr_date" data-input_name="lr_date" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <table style="" class="table custom-table item-details-table" id="item_table">
                                                <thead>
                                                <tr>
                                                    <th>Action</th>
													<th>Item Code</th>
													<th>Item Name</th>
													<th>Rate</th>
													<th>Quantity</th>
													<th>Discount</th>
													<th>IGST</th>
													<th>CGST</th>
													<th>SGST</th>
													<th>Total Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody id="item-details-table-body"></tbody>
                                                <tfoot>
													<tr>
														<td id=""><b>Total</b></td>
														<td id=""></td>
														<td id=""></td>
														<td id=""></td>
														<td id="qty_total"></td>
														<td id=""></td>
														<td id=""></td>
														<td id=""></td>
														<td id=""></td>
														<td id="total_amounts"></td>
													</tr>
												</tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_3">
							<fieldset class="scheduler-border">
								<legend class="scheduler-border text-primary text-bold"> Terms And Conditions </legend>
								<div class="col-md-12">
									<div class="form-group">
										<textarea id="terms_condition_purchase" class="form-control" style="height: 100px" name="invoice_data[terms_condition_purchase]">
										</textarea>
									</div>
								</div>
							</fieldset><br />
                        </div>

                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </form>
        </div>
        <!-- /.col -->
    </div>
    <section class="content-header">
        <h1>
            <?php if($invoice_add_role): ?>
            <button type="button" onclick="submit_form()" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Save</button>
            <?php endif;?>
            <?php if($invoice_view_role): ?>
            <a href="<?= base_url()?>finance/invoice-list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Load Invoice</a>
            <?php endif;?>
            <?php if($invoice_add_role): ?>
            <a href="<?= base_url()?>finance/invoice/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add Invoice</a>
            <?php endif;?>
        </h1>
    </section>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h4 class="modal-title">Select Challan
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="sales_challan_datatable" class="table custom-table table-striped">
                        <thead>
                        <tr>
                            <th>Party</th>
                            <th>Enquiry No</th>
                            <th>Quotation No</th>
                            <th>Order No</th>
                            <th>Challan No</th>
                            <th>Challan Date</th>
                            <th>Contact No</th>
                            <th>Email Id</th>
                            <th>Sales</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr>
                            <th>Party</th>
                            <th>Enquiry No</th>
                            <th>Quotation No</th>
                            <th>Order No</th>
                            <th>Challan No</th>
                            <th>Challan Date</th>
                            <th>Contact No</th>
                            <th>Email Id</th>
                            <th>Sales</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="billing_terms_modal" role="dialog">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Billing Terms</h4>
            </div>
            <div class="modal-body">
                <table id="billing_terms_datatable" class="table custom-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Template Name</th>
                        <th>Template Date</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Template Name</th>
                        <th>Template Date</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?=dist_url('js/sales.js');?>"></script>
<script>
    var INVOICE_ID = 0;
    var ADD_ITEM = 0;
    var EDIT_ITEM = 0;
    var DELETE_ITEM = 0;
    var TOTAL_AMOUNT = 0;
    var update_value_status = 0;
    var cal_codes_amount_label = [];
    var cal_codes = [];
    var code_label = [];
    var template_data = {};
    function submit_form() {
        if ($.trim($("#sales_to_party_id").val()) == '') {
            show_notify('Sales to party is required !', false);
            $("#sales_to_party").focus();
            return false;
        }
        var item_table_tbody = $("#item_table tbody");
        if (item_table_tbody.children().length == 0) {
            show_notify('Please add at least one item', false);
            return false;
        }
        $("#lead_form").submit();
    }
    function generateDateAfterGivenWeek(week){
		var date = new Date();
		date.setDate(date.getDate() + 7*parseInt(week));
		var dateMsg = date.getDate()+'-'+ (parseInt(date.getMonth()+1)) +'-'+date.getFullYear();
		return dateMsg;
	}

    $(window).load(function() {
        $('.dataTables_scrollHeadInner, .dataTables_scrollFootInner').css('width','auto');
        $('.dataTables_scrollHeadInner table, .dataTables_scrollFootInner table').css('width','100%');
    });

    $(document).on('click', '.btn_change_template', function () {
        $('#billing_terms_modal').modal('show');
    });

    $('#billing_terms_modal').on('shown.bs.modal',function(){
        $('#billing_terms_datatable').DataTable().search('').draw();
    });

    $(document).on('click', '.addnew', function () {
        //$(this).closest('.corporat_contact').find(".assigned_to").select2("destroy");
        var section_index = $('div.corporat_contact').length;
        var newDiv = $(".corporat_contact:first").clone()
        .find(':input')
        .each(function () {
            this.name = this.name.replace(/\[(\d+)\]/, function (str) {
                return '[' + section_index + ']';
            });
        })
        .end()
        .appendTo("#addmore_contact");
        newDiv.find('.remove_contact').css('visibility', 'visible');
    });
    $(document).on('click', '.remove_contact', function () {
        $(this).closest('div.corporat_contact').remove();
    });

    $(document).ready(function () {

        var billing_terms_datatable = $('#billing_terms_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[0,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('sales/billing_terms_datatable')?>",
                "type": "POST"
            },
            "scrollY": 400,
            "scroller": {
                "loadingIndicator": true
            }
        });

        $(document).on('click', '.btn_set_billing_terms', function () {
            var billing_terms_id = $(this).data('billing_terms_id');
            $.ajax({
                url: '<?=base_url()?>sales/set_billing_terms_in_sales_order/' + billing_terms_id,
                type: "POST",
                data: {"amount":TOTAL_AMOUNT},
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $("#billing_terms_id").val(billing_terms_id);
                        template_data = data.template_data;
                        set_billing_terms();
                        $('#billing_terms_modal').modal('hide');
                    } else {
                        show_notify(data.message, false);
                    }
                }
            });

        });

        initAjaxSelect2($("#sales_to_party_id"),"<?=base_url('app/party_select2_source')?>");
        initAjaxSelect2($("#sales_id"),"<?=base_url('app/sales_select2_source')?>");
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source')?>");
        initAjaxSelect2($("#city_id"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state_id"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country_id"),"<?=base_url('app/country_select2_source')?>");
        initAjaxSelect2($("#item_id"),"<?=base_url('app/item_select2_source')?>");
        initAjaxSelect2($("#invoice_type_id"),"<?=base_url('app/invoice_type_select2_source')?>");
		initAjaxSelect2($("#delivery_through_id"),"<?=base_url('app/delivery_through_select2_source')?>");
		initAjaxSelect2($("#item_extra_accessories_id"),"<?=base_url('app/item_extra_accessories_select2_source')?>");
		setSelect2Value($("#invoice_type_id"),'<?=base_url()?>app/set_invoice_type_select2_val_by_id/'+<?=INVOICE_TYPE_ID?>);
		
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g );
        CKEDITOR.replace('terms_condition_purchase',{
            removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });

        $(document).on("click", ".btn_load_party", function () {
            $('#search_result').trigger('click');
        });
        $(document).on("blur", "#minimum", function () {
            var minumum = $(this).val();
            if(minumum != 0){
                var newdate = generateDateAfterGivenWeek(minumum);
                $('#minimum_weeks').val(newdate);
            } else {
				$('#minimum_weeks').val('');
			}
        });
        $(document).on("blur", "#maximum", function () {
            var maximum = $(this).val();
            if(maximum != 0){
                var newdate = generateDateAfterGivenWeek(maximum);
                $('#maximum_weeks').val(newdate);
            } else {
				$('#maximum_weeks').val('');
			}
        });

        $('#myModal').modal({backdrop: 'static', keyboard: false});

        $('#myModal').modal('show');

        var table = $('#sales_challan_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('finance/sales_challan_datatable')?>",
                "type": "POST",
                "data": function(d){
                    d.request_from = "invoice";
                }
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
            }
        });
        $('#myModal').on('shown.bs.modal', function() {
            $('#sales_challan_datatable').DataTable().search('').draw();
        });



        $(document).on('click', '.btn-edit-item', function () {
			$('#item_id').removeAttr('onchange');
            var id = $(this).data('id');
            if(INVOICE_ID == 0){
                EDIT_ITEM = 1;
                $("#edit_delete_item_id").val(id);
                $("#lead_form").submit();
            }else{
                if($("#invoice_item_id").val() != 0 || $("#invoice_item_id").val() != '0'){
                }
                $.ajax({
                    url: '<?=base_url()?>finance/get-invoice-item-by-id/'+id,
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        EDIT_ITEM = 0;
                        if(data.success == true) {
                            $('.invoice_item_id').val(id);
                            $('.btn-add-item').html('Save Item');
                            $.each(data.item_data,function(index,value){
                                if($('select.'+index).length > 0){
								}else if($('.item_data.'+index).length > 0) {
                                    $('.item_data.'+index).val(value);
                                }
                            });
                            $("#item_category_id").val(data.item_data.item_category_id).trigger("change");
                            setSelect2Value($("#item_id"),'<?=base_url()?>app/set_item_select2_val_by_id/'+data.item_data.item_id);
                            setSelect2Value($("#item_extra_accessories_id"),'<?=base_url()?>app/set_item_extra_accessories_select2_val_by_id/'+data.item_data.item_extra_accessories_id);
                            $('#item_id').attr('onchange', 'item_details(this.value)');
                        } else {
                            show_notify(data.message, false);
                        }
                    }
                });
            }
        });

        $(document).on('click','.btn-feed-sales-challan-data', function () {
            var sales_order_id = $(this).data('sales_order_id');
            var challan_id = $(this).data('sales_challan_id');
            $("#challan_id").val(challan_id);
            $("#ajax-loader").show();
            $.ajax({
                url: '<?php echo BASE_URL; ?>finance/get_challan',
                type: "POST",
                data: {sales_order_id: sales_order_id, challan_id: challan_id},
                dataType: 'json',
                success: function (data) {
                    console.log(data.challan_data.party_type_1);
                    $('#myModal').modal('hide');
                    $("#ajax-loader").hide();

                    var challan_data = data.challan_data;
                    
                    $.each(challan_data, function(index, value) {
						if(index != 'contact_no' && index != 'lr_bl_no' && index != 'lr_date'){
							if($("#"+index).length > 0){
								if($("#"+index).is("select")){ } else {
									$("#" + index).val(value);
								}
							}
						}
                    });

                    setSelect2Value($("#sales_to_party_id"),"<?=base_url('app/set_party_select2_val_by_id')?>/"+challan_data.sales_to_party_id);
                    $('#party_name').val($('#sales_to_party_id').val());
                    setSelect2Value($("#sales_id"),"<?=base_url('app/set_sales_select2_val_by_id')?>/"+challan_data.sales_id);
                    setSelect2Value($("#currency_id"),"<?=base_url('app/set_currency_select2_val_by_id')?>/"+challan_data.currency_id);
                    setSelect2Value($("#city_id"),"<?=base_url('app/set_city_select2_val_by_id')?>/"+challan_data.city_id);
                    setSelect2Value($("#delivery_through_id"),"<?=base_url('app/set_delivery_through_select2_val_by_id')?>/"+challan_data.delivery_through_id);
                    
                    var challan_items = data.challan_items;
                    if(challan_items.length > 0) {
						var qty_total = 0;
						var total_amounts = 0;
                        var challan_items_table_html = '';
                        $.each(challan_items, function (index, value) {
                            var row_html = '<tr><td>' +
                                '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" data-id="' + value.id + '"><i class="fa fa-remove"></i></a>' +
                                ' <a class="btn btn-xs btn-primary btn-edit-item" href="javascript:void(0);" data-id="' + value.id + '"><i class="fa fa-edit"></i></a>' +
                                '</td>' +
                                '<td>' + value.item_code + '</td>' +
                                '<td>' + value.item_name + '</td>' +
                                '<td>' + value.rate + '</td>' +
								'<td class="quantity_td">' + value.quantity + '</td>' +
								'<td>' + value.disc_per + '</td>' +
								'<td>' + value.igst + '</td>' +
								'<td>' + value.cgst + '</td>' +
								'<td>' + value.sgst + '</td>' +
								'<td class="total_amount_td">'+value.total_amount+'</td></tr>';
                            challan_items_table_html += row_html;
                            qty_total += parseInt(value.quantity);
							total_amounts += parseInt(value.total_amount);
                        });
                        $('tbody#item-details-table-body').html(challan_items_table_html);
                        $('#qty_total').html(qty_total);
						$('#total_amounts').html(total_amounts);
                    }
                    var option_html = '';
                    var party_contact_person = data.party_contact_person;
                    if(party_contact_person.length > 0){
                        $.each(party_contact_person,function(index,value){
                            option_html += "<option value='"+value.contact_person_id+"'>"+value.name+"</option>";
                            if(index == 0){
								$('input[name="contact_person[contact_person_mobile_no]"]').val(value.mobile_no);
								$('input[name="contact_person[contact_person_email_id]"]').val(value.email);
								$('#contact_person_designation').html(value.designation);
								$('#contact_person_department').html(value.department);
							}
                        });
                    	$('select[name="invoice_data[kind_attn_id]"]').html(option_html).select2();
					} else {
						$('select[name="invoice_data[kind_attn_id]"]').html("<option val=''>--select--</option>").select2();
					}
                    
                    getStateDetails($('[name="buyer_data[city_id]"]').val());
                    setSelect2Value($("#state_id"),"<?=base_url('app/set_state_select2_val_by_id')?>/"+challan_data.state_id);
                    setSelect2Value($("#country_id"),"<?=base_url('app/set_country_select2_val_by_id')?>/"+challan_data.country_id);

                    if (challan_data.taxes_data) {
                        taxes_data_html = '';
                        i = 0;
                        $.each(challan_data.taxes_data,function(index,value){
                            taxes_data_html += '<div class="corporat_contact col-md-12"> \
                                 <div class="form-group col-md-4"> \
                                    <label for="inputEmail3" class="col-sm-4 control-label input-sm">Tax Name</label> \
                                    <div class=""> \
                                        <input type="text" class="form-control input-sm pull-right" name="invoice_data[taxes_data]['+i+'][name]" value="'+value.name+'"> \
                                    </div> \
                                </div>  \
                                <div class="form-group col-md-4"> \
                                    <label for="inputEmail3" class="col-sm-1 control-label input-sm">Percentage</label> \
                                    <div class="bootstrap-timepicker" style="position:initial;"> \
                                        <input type="text" class="form-control input-sm pull-right" name="invoice_data[taxes_data]['+i+'][pecentage]" value="'+value.pecentage+'"> \
                                    </div> \
                                </div> \
                                <div class="form-group col-md-4"> \
                                    <div class="remove_contact" style="margin-top: 20px;"><span class="btn btn-primary" title="Remove Contact">x</span></div> \
                                </div>   \
                            </div>';
                            i++;
                        });
                        $('#addmore_contact').append(taxes_data_html);
                    }
                    $('#myModal').modal('hide');
                    var currency_title = $('#select2-currency_id-container').attr('title');
					$('#received_payment_currency').html(currency_title);
                    if (data.challan_data.party_type_1 != null){
                        $.ajax({
                            url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                            type: "POST",
                            data: {party_type: data.challan_data.party_type_1, module:'sales_invoice_terms_and_conditions'},
                            dataType: 'json',
                            success: function (data) {
                                console.log(data.terms_and_conditions);
                                CKEDITOR.instances['terms_condition_purchase'].setData( data.terms_and_conditions );
                            }
                        });
                        /*$.ajax({
                            url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                            type: "POST",
                            data: {party_type: quotation_data.party_type_1, module:'sales_order_terms_and_conditions'},
                            dataType: 'json',
                            success: function (data) {
                                console.log(data.terms_and_conditions);
                                CKEDITOR.instances['terms_condition_purchase'].setData( data.terms_and_conditions );
                            }
                        });*/
                    }
                },
                error: function (msg) {
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('change','select[name="invoice_data[kind_attn_id]"]',function(){
        	var contact_person_id = $(this).val();
			if(contact_person_id != ''){
				$.ajax({
					url:'<?=base_url()?>party/get-contact-person-by-id/'+contact_person_id,
					type: "POST",
					data: null,
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function (data) {
						if (data.success == true) {
							$('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
							$('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
							$('#contact_person_designation').html(data.contact_person_data.designation);
							$('#contact_person_department').html(data.contact_person_data.department);
						}
					}
				});
			}
		});

        $("#lead_form").on("submit", function (e) {
            e.preventDefault();

            if($('[data-input_name="item_id"]').val() != "" && $('[data-input_name="item_id"]').val() != null ){
                if($('[data-input_name="quantity"]').val() == "" || $('[data-input_name="quantity"]').val() == 0){
                    show_notify('Item quantity is required!', false);
                    return false;
                }

                if($('[data-input_name="rate"]').val() == "" || $('[data-input_name="rate"]').val() == 0){
                    show_notify('Item rate is required!', false);
                    return false;
                }

                if($('[data-input_name="rate"]').val() > 9900000) {
                    show_notify('Please enter valid rate!', false);
                    return false;
                }
                ADD_ITEM = 1;
                //$('.btn-add-item').click();
            }

            /*------ check sales order number filled or not -----*/
            var OrderFormData = new FormData(this);
            OrderFormData.append('invoice_data[terms_condition_purchase]',CKEDITOR.instances['terms_condition_purchase'].getData());

            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: OrderFormData,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if(data.success == true) {
                        if(ADD_ITEM == 0 && EDIT_ITEM == 0 && DELETE_ITEM == 0){
                            show_notify('Invoice saved successfully!', true);
                        }
                        INVOICE_ID = data.invoice_id;
                        $('#invoice_id').val(INVOICE_ID);
                        $('.invoice_id').val(INVOICE_ID);
                        var invoice_items = data.invoice_items;
                        if(invoice_items.length > 0){
							var qty_total = 0;
							var total_amounts = 0;
                            var quotation_items_table_html = '';
                            $.each(invoice_items, function(index, value) {
                                var row_html ='<tr><td>' +
                                    '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" data-id="'+value.id+'"><i class="fa fa-remove"></i></a>' +
                                    ' <a class="btn btn-xs btn-primary btn-edit-item" href="javascript:void(0);" data-id="'+value.id+'"><i class="fa fa-edit"></i></a>' +
                                    '</td>'+
                                    '<td>'+value.item_code+'</td>'+
                                    '<td>'+value.item_name+'</td>'+
                                    '<td>' + value.rate + '</td>' +
									'<td class="quantity_td">' + value.quantity + '</td>' +
									'<td>' + value.disc_per + '</td>' +
									'<td>' + value.igst + '</td>' +
									'<td>' + value.cgst + '</td>' +
									'<td>' + value.sgst + '</td>' +
									'<td class="total_amount_td">'+value.total_amount+'</td></tr>';
                                quotation_items_table_html += row_html;
                                qty_total += parseInt(value.quantity);
								total_amounts += parseInt(value.total_amount);
                            });
                            $('tbody#item-details-table-body').html(quotation_items_table_html);
                            $('#qty_total').html(qty_total);
							$('#total_amounts').html(total_amounts);
                        }
                        if (ADD_ITEM == 1) {
                            $(".btn-add-item").trigger('click');
                        }
                        var edit_delete_item_id = data.edit_delete_item_id;
                        if(edit_delete_item_id != 0 && edit_delete_item_id != '0'){
                            if (EDIT_ITEM == 1) {
                                $('a.btn-edit-item[data-id="'+edit_delete_item_id+'"]').trigger('click');
                            }
                            if (DELETE_ITEM == 1) {
                                $('a.btn-delete-item[data-id="'+edit_delete_item_id+'"]').trigger('click');
                            }
                        }
                        $("#edit_delete_item_id").val(0);
                    } else {
                        show_notify(data.message, false);
                    }
                }
            });
        });

        $('form').on('keyup keypress', function(e) {
            if($('.invoice_item_id').val() != 0){
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            }
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#lead_form").submit();
                return false;
            }
        });  

        $(".btn-add-item").on("click", function() {
            var ButtonObj = $(this);
            if($.trim($("#item_code").val()) == "" || $("#item_code").val() == null)
            {
                show_notify('Please select item !', false);
                return false;
            }

            if($('[data-input_name="quantity"]').val() == "" || $('[data-input_name="quantity"]').val() == 0)
            {
                show_notify('Item quantity is required!', false);
                return false;
            }

            if($('[data-input_name="rate"]').val() == "" || $('[data-input_name="rate"]').val() == 0)
            {
                show_notify('Item rate is required!', false);
                return false;
            }

            if($('[data-input_name="rate"]').val() > 9900000) {
                show_notify('Please enter valid rate!', false);
                return false;
            }

            if(INVOICE_ID == 0){
                ADD_ITEM = 1;
                $("#lead_form").submit();
            }else{
                var ItemData = new FormData();
                $('.invoice-item-section').find('.item_data').each(function(){
                    if($(this).data('input_name') != undefined) {
                        ItemData.append($(this).data('input_name'),$(this).val());
                    }
                });
                if($('.invoice_item_id').val() == 0){
                    $.ajax({
                        url: "<?=base_url()?>finance/add-invoice-item/"+INVOICE_ID,
                        type: "POST",
                        data: ItemData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'json',
                        success: function(data) {
                            if(data.success == true){
                                ADD_ITEM = 0;
                                var invoice_item_id = data.invoice_item_id;
                                var row_html ='<tr><td>' +
                                    '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" data-id="'+invoice_item_id+'"><i class="fa fa-remove"></i></a>' +
                                    ' <a class="btn btn-xs btn-primary btn-edit-item" href="javascript:void(0);" data-id="'+invoice_item_id+'"><i class="fa fa-edit"></i></a>' +
                                    '</td>'+
                                    '<td>'+$(".item_code").val()+'</td>'+
                                    '<td>'+$(".item_name").val()+'</td>'+
                                    '<td>'+$(".rate").val()+'</td>'+
									'<td class="quantity_td">'+$(".quantity").val()+'</td>'+
									'<td>'+$(".disc_per").val()+'</td>'+
									'<td>'+$('.igst').val()+'</td>'+
									'<td>'+$('.cgst').val()+'</td>'+
									'<td>'+$('.sgst').val()+'</td>'+
									'<td class="total_amount_td">'+$('.total_amount').val()+'</td></tr>';
                                $('tbody#item-details-table-body').append(row_html);
								add_item_get_total();
                                $('.invoice-item-section').find('.item_data').each(function(){
                                    $(this).val('');
                                });
                                $("#item_category_id").val($("#item_category_id option:first").val());
                                setSelect2Value($("#item_id"));
                                setSelect2Value($("#item_extra_accessories_id"));
                                show_notify('Item added successfully!',true);
                            }else{
                                show_notify('Something wrong!',false);
                            }
                        }
                    });
                }else{
                    var invoice_item_id = $('.invoice_item_id').val();
                    $.ajax({
                        url: "<?=base_url()?>finance/edit-invoice-item/"+invoice_item_id,
                        type: "POST",
                        data: ItemData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'json',
                        success: function(data) {
                            if(data.success == true){
                                $('.invoice_item_id').val(0);
                                $(".btn-add-item").html('Add Item');
                                ADD_ITEM = 0;
                                var row_html ='<td>' +
                                    '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" data-id="'+invoice_item_id+'"><i class="fa fa-remove"></i></a>' +
                                    ' <a class="btn btn-xs btn-primary btn-edit-item" href="javascript:void(0);" data-id="'+invoice_item_id+'"><i class="fa fa-edit"></i></a>' +
                                    '</td>'+
                                    '<td>'+$(".item_code").val()+'</td>'+
                                    '<td>'+$(".item_name").val()+'</td>'+
                                    '<td>'+$(".rate").val()+'</td>'+
									'<td class="quantity_td">'+$(".quantity").val()+'</td>'+
									'<td>'+$(".disc_per").val()+'</td>'+
									'<td>'+$('.igst').val()+'</td>'+
									'<td>'+$('.cgst').val()+'</td>'+
									'<td>'+$('.sgst').val()+'</td>'+
									'<td class="total_amount_td">'+$('.total_amount').val()+'</td></tr>';
									
								var old_qty = $('a[data-id="'+invoice_item_id+'"]').closest('tr').find('.quantity_td').html();
								var new_qty = $('.quantity').val();
								var qty_total = $('#qty_total').html();
								$('#qty_total').html(parseInt(qty_total) - parseInt(old_qty) + parseInt(new_qty));
								var old_total = $('a[data-id="'+invoice_item_id+'"]').closest('tr').find('.total_amount_td').html();
								var new_total = $('.total_amount').val();
								var total_amounts = $('#total_amounts').html();
								$('#total_amounts').html(parseInt(total_amounts) - parseInt(old_total) + parseInt(new_total));

                                $('a[data-id="'+invoice_item_id+'"]').closest('tr').html(row_html);

                                $('.invoice-item-section').find('.item_data').each(function(){
                                    $(this).val('');
                                });
                                $("#item_category_id").val($("#item_category_id option:first").val());
                                setSelect2Value($("#item_id"));
                                setSelect2Value($("#item_extra_accessories_id"));
                                show_notify('Item saved successfully!',true);
                            }else{
                                show_notify('Something wrong!',false);
                            }
                        }
                    });
                }
            }
        });

        $(document).on("change",'[name="invoice_data[sales_to_party_id]"]',function() {
            var party_id = $(this).val();
            $.ajax({
                url: "<?=base_url()?>sales/get-party-by-id/" + party_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        var party_data = data.party_data;
                        var party_contact_person = party_data.party_contact_person;
                        $.each(party_data, function (index, value) {
                            if ($("#" + index).length > 0) {
                                if ($("#" + index).is("select")) {

                                } else {
                                    if (index == 'email_id') {
                                        $('[name="buyer_data[email_id]"]').val(value.replace(/<br *\/?>/gi, '\n'));
                                    } else {
                                        $("#" + index).val(value);
                                    }
                                }
                            }
                        })

                        if(update_value_status != 0)
                        {
                            setSelect2Value($("#branch_id"), "<?=base_url('app/set_branch_select2_val_by_id')?>/" + party_data.branch_id);
                            setSelect2Value($("#agent_id"), "<?=base_url('app/set_agent_select2_val_by_id')?>/" + party_data.agent_id);
                            setSelect2Value($("#sales_id"), "<?=base_url('app/set_sales_select2_val_by_id')?>/" + party_data.party_type_1);
                            setSelect2Value($("#city_id"), "<?=base_url('app/set_city_select2_val_by_id')?>/" + party_data.city_id);
                        } else {
                            update_value_status = 1;
                        }

                        var option_html = '';
                        if (party_contact_person.length > 0) {
                            $.each(party_contact_person, function (index, value) {
                                option_html += "<option value='" + value.contact_person_id + "'>" + value.name + "</option>";
                                //~ if(index == 0){
									//~ $('input[name="contact_person[contact_person_mobile_no]"]').val(value.mobile_no);
									//~ $('input[name="contact_person[contact_person_email_id]"]').val(value.email_id);
									//~ $('#contact_person_designation').html(value.designation);
									//~ $('#contact_person_department').html(value.department);
								//~ }
                            });
							$('select[name="invoice_data[kind_attn_id]"]').html(option_html).select2();
						} else {
							$('select[name="invoice_data[kind_attn_id]"]').html("<option val=''>--select--</option>").select2();
						}
                        
                        getStateDetails($('[name="buyer_data[city_id]"]').val());
                        setSelect2Value($("#state_id"), "<?=base_url('app/set_state_select2_val_by_id')?>/" + party_data.state_id);
						setSelect2Value($("#country_id"), "<?=base_url('app/set_country_select2_val_by_id')?>/" + party_data.country_id);
                    }
                }
            });
        });

        $(document).on("click",".btn-delete-item",function(){
            var tr = $(this).closest('tr');
            var item_id = $(this).data("id");
            if(INVOICE_ID != 0){
                var confirm_value = confirm('Are you sure ?');
                if(confirm_value) {
					
					var old_qty = $(this).closest('tr').find('.quantity_td').html();
					var qty_total = $('#qty_total').html();
					$('#qty_total').html(parseInt(qty_total) - parseInt(old_qty));
					var old_total = $(this).closest('tr').find('.total_amount_td').html();
					var total_amounts = $('#total_amounts').html();
					$('#total_amounts').html(parseInt(total_amounts) - parseInt(old_total));
					
                    $.ajax({
                        url: '<?=base_url()?>finance/get-invoice-item-by-id/'+item_id,
                        type: "POST",
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success == true) {
                                $.ajax({
                                    url: '<?=BASE_URL?>finance/delete-invoice-item/' + item_id,
                                    type: "POST",
                                    data: null,
                                    success: function (data) {
                                        DELETE_ITEM = 0;
                                        tr.fadeOut('medium', function () {
                                            $(this).remove();
                                        });
                                        show_notify('Item removed successfully!', true)
                                    }
                                });
                                $('.invoice-item-section').find('.item_data').each(function(){
                                    $(this).val('');
                                });
                                $("#item_category_id").val($("#item_category_id option:first").val());
                                setSelect2Value($("#item_id"));
                            }
                        }
                    });
                }
            }else{
                DELETE_ITEM = 1;
                $("#edit_delete_item_id").val(item_id);
                $("#lead_form").submit();
            }
        });

        $(document).on('input', '#quantity,#rate', function () {
            setAmount();
        });

        $(document).on('input','#disc_per',function () {
            setDiscAmount();
        });

        $(document).on('input','#disc_value',function () {
            setDiscPer();
        });
        var currency_title = $('#select2-currency_id-container').attr('title');
		$('#received_payment_currency').html(currency_title);
    });

    function set_billing_terms() {
        var template_table_html = '';
        $.each(template_data,function(index,template_value){
            template_table_html += "<tr>";
            template_table_html += "<td>"+template_value.sr_no+"</td>";
            template_table_html += "<td>"+template_value.cal_code+"</td>";
            template_table_html += "<td>"+template_value.description+"</td>";
            var cal_definition_label = '';
            var cal_definition_data = template_value.cal_definition_data;
            console.log(cal_definition_data);

            if (cal_definition_data.length == 1) {
                code_label[template_value.cal_code_id] = template_value.cal_code;
            }else{
                code_label[template_value.cal_code_id] = '';
            }
            cal_codes[template_value.cal_code_id] = template_value.cal_code;

            if (template_value.cal_code_id == cal_definition_data[0].cal_code_id) {
                cal_codes_amount_label[template_value.cal_code_id] = (+$("#total_amount").val() * +template_value.percentage) / 100;
            }else{
                cal_codes_amount_label[template_value.cal_code_id] = 0;
            }

            $.each(cal_definition_data,function(index,value) {

                if (cal_definition_data.length == 1) {
                    if(template_value.cal_code_id != cal_definition_data[0].cal_code_id){
                        if (value.cal_operation == '-') {
                            cal_codes_amount_label[template_value.cal_code_id] = -((+cal_codes_amount_label[value.cal_code_id] * +template_value.percentage) / 100);
                        } else {
                            cal_codes_amount_label[template_value.cal_code_id] = +((+cal_codes_amount_label[value.cal_code_id] * +template_value.percentage) / 100);
                        }
                    }
                    code_label[template_value.cal_code_id] = cal_codes[value.cal_code_id];
                } else {
                    if (value.cal_operation == '-') {
                        cal_codes_amount_label[template_value.cal_code_id] = +cal_codes_amount_label[template_value.cal_code_id] + -cal_codes_amount_label[value.cal_code_id];
                    } else {
                        cal_codes_amount_label[template_value.cal_code_id] = +cal_codes_amount_label[template_value.cal_code_id] + +cal_codes_amount_label[value.cal_code_id];
                    }
                    code_label[template_value.cal_code_id] += " " + value.cal_operation + " " + cal_codes[value.cal_code_id];
                }
            })
            template_table_html += "<td>"+code_label[template_value.cal_code_id]+"</td>";
            template_table_html += "<td class='text-right'>"+template_value.percentage+"</td>";
            template_table_html += "<td class='text-right'>"+parseFloat(cal_codes_amount_label[template_value.cal_code_id]).toFixed(2)+"</td>";
            template_table_html += "</tr>";
        })
        $('table#table_billing_terms_detail > tbody').html(template_table_html);
    }

    function parseF(value, decimal) {
        decimal = 0;
        return value ? parseFloat(value).toFixed(decimal) : 0;
    }

    function getStateDetails(id)
    {
        if(id != '' && id != null){
			$.ajax({
				type: "POST",
				url: '<?=base_url();?>party/ajax_get_state/'+id,
				data: id='cat_id',
				success: function(data){
					var json = $.parseJSON(data);
					if (json['state']) {
						$("#state_id").html('');
						$("#state_id").html(json['state']);
						$("#s2id_state_id span:first").html($("#state_id option:selected").text());
					}
					if (json['country']) {
						$("#country_id").html('');
						$("#country_id").html(json['country']);
						$("#s2id_country_id span:first").html($("#country_id option:selected").text());
					}
				}
			});
		}
    }
</script>
