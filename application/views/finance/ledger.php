<?php
//print_r($service_list);
//print_r($party_list);
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Ledger</small>
            <?php if($this->app_model->have_access_role(INVOICE_MODULE_ID, "add")){ ?>
            <a href="<?=base_url()?>finance/invoice" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice</a>
            <?php } ?>
            <?php if($this->app_model->have_access_role(PAYMENT_MODULE_ID, "add")){ ?>
            <a href="<?=base_url()?>finance/payment" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Payment</a>
            <?php } ?>
        </h1> 
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form-horizontal" action="<?php echo base_url('service/saving_service_data');?>" method="post" id="form_service">
                                        <div class="col-md-4"> 
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Party</label>
                                                <div class="col-sm-8 dispaly-flex">
                                                    <select name="party_id" id="customer_name1" class="form-control input-sm" onchange="party_details(this.value)">
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <br />
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <table id="ledger_list" class="table custom-table agent-table" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Particular</th>
                                                        <th>Date</th>
                                                        <th>Debit</th>
                                                        <th>Credit</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="ledger_td">
                                                </tbody>
                                                <tfoot id="ledger_footer">
                                                </tfoot>
                                            </table>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


    var table;

    $(document).ready(function() {

        initAjaxSelect2($("#customer_name1"),"<?=base_url('app/party_select2_source')?>");

        /*
        table = $('#service_list').DataTable({

            "serverSide": true,
            "ordering": true,
            "searching": true,
             "aaSorting": [[2, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('service/service_list_datatable')?>",
                "type": "POST",    
                "data": function(d){
                    d.party_id = $("#customer_name1").val();                    
                    d.from_date = $("input[name=from_date]").val();
                    d.to_date = $("input[name=to_date]").val();
                },    

             "ajax": function ( data, callback, settings ) {
                 setTimeout( function () {
                     var out = [];             
                     for ( var i=data.start, ien=data.start+data.length ; i<ien ; i++ ) {
                         out.push(  [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5',  i+'-6' ] );
                     }             

                     callback( {
                       "draw": data.draw,
                       "data": out,
                       "recordsTotal": data.recordsTotal,
                       "recordsFiltered": data.recordsFiltered
                     });

                 }, 50 );
              },     

             },
            "scroller": {
                "loadingIndicator": true
            },
            "deferRender": true,

            "scrollY": "200px"            

        }); */
    });

    $(document).on("change", "#customer_name1", function(){        

        $("#ajax-loader").show();
        var id = $(this).val();
        console.log("id:--"+id);
        $.ajax({
            url: '<?php echo BASE_URL; ?>finance/getLedgers',
            type: "POST",
            data: {id: id},
            success: function(data)
            {
                $("#ajax-loader").hide();
                data = JSON.parse(data);
                console.log(data);

                $( "#ledger_td" ).text("");
                $( "#ledger_footer").text("");
                var total_debit = 0;
                var total_credit = 0;
                jQuery.each( data, function( i, val ) {
                    //console
                    console.log(val);
                    if(val.payment_type=='debit')
                    {
                        console.log(typeof(total_debit)+"--:--"+typeof(parseInt(val.debit)));
                        total_debit = total_debit+parseInt(val.amount);
                        $( "#ledger_td" ).append("<tr><td>"+val.tableName+"</td><td>"+val.created+"</td><td style='text-align: right;'>"+val.amount+"</td><td></td></tr>" );
                    }
                    else
                    {
                        console.log(typeof(total_credit)+"--:--"+typeof(parseInt(val.debit)));
                        total_credit = total_credit+parseInt(val.amount);
                        $( "#ledger_td" ).append("<tr><td>"+val.tableName+"</td><td>"+val.created+"</td><td></td><td style='text-align: right;'>"+val.amount+"</td></tr>" );
                    }
                });
                var balance = total_credit-total_debit;
                $( "#ledger_footer").append("<tr><td colspan='2'><strong>Total</strong></td><td style='text-align: right;'><strong>"+total_debit+"</strong></td><td style='text-align: right;'><strong>"+total_credit+"</strong></td></tr>");
                $( "#ledger_footer").append("<tr><td colspan='4' style='text-align: right;'>------------------</td></tr><tr><td colspan='2'><strong>Balance</strong></td><td style='text-align: right;' colspan='2'><strong>"+balance+"</strong></td></tr>");
                //table.draw();
                /*if (data.status == 1)
                {
                    setSelect2Value($("#customer_name1"),'<?=base_url()?>app/set_party_select2_val_by_id/'+data.party.party_id);                    
                    party_details(data.party.party_id);
                }*/
            },
            error: function(msg) {
                $("#ajax-loader").hide();
            } 
        });
    }); 


    $(document).on("click",".delete_button",function(){
        var value = confirm('Are you sure delete this records?');
        var tr = $(this).closest("tr");
        if(value){
            $.ajax({
                url: $(this).data('href'),
                type: "POST",
                data: 'id_name=service_id&table_name=service',
                success: function(data){
                    tr.remove();
                    window.location.href = "<?php echo base_url('service/index') ?>";
                }
            });
        }
    });

    function feedLeedData(id)
    {
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>sales/get_lead',
            type: "POST",
            data: {id: id},
            success: function(data)
            {
                $("#ajax-loader").hide();
                data = JSON.parse(data);
                //console.log(data);
                if (data.status == 1)
                {
                    setSelect2Value($("#customer_name1"),'<?=base_url()?>app/set_party_select2_val_by_id/'+data.party.party_id);                    
                    party_details(data.party.party_id); 
                }
            },
            error: function(msg) {
                $("#ajax-loader").hide();
            } 
        });
    }

    function party_details(id){

        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/'+id,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);                
                //agent_id  branch_id     
            }
        });
    }

</script>
