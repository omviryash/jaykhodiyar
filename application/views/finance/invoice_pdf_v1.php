<?php
	$column_cnt = 8;
?>
<html>
	<head>
		<title>Sales Order</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:16px;
			}
			tr > td:last-child{
				border-right: 1px solid !important;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:14px !important;
			}
			.text-header{
				font-size: 20px;
				font-weight: 700;
			}
			.no-border-top{
				border-top:0;
			}
			.no-padding{
				padding:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			.width-40-pr{
				width:40%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				width: 33.33%;
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #698c66;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
	<?php
		$CURRENCY = "INR";
		if($invoice_data->currency != ''){
			$CURRENCY = strtoupper($invoice_data->currency);
		}
	?>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="<?=$column_cnt;?>">INVOICE</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 5;?>" rowspan="3">
					MFG.<br/>
					<strong><?=$company_details['name'];?></strong><br/>
					<?=$company_details['address'];?><br/>
					<?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>) <?=$company_details['country'];?>.<br/>
					Tel: +91-281- 2367594, 2367512,<br/>
					2388115<br/>
					Fax: <?=$company_details['fax_no'];?><br/>
					Email : <a href="mail_to:<?=$company_details['email_id'];?>"><?=$company_details['email_id'];?></a><br/>
					TIN &nbsp;&nbsp; NO. <?=$company_details['tin_no'];?> <br/>
					C.S.T. NO. <?=$company_details['cst_no'];?> <br/>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 3;?>">
					<strong>P. Invoice No.: <?=$this->applib->get_invoice_no($invoice_data->invoice_no);?></strong>
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 5;?>">
					Order No. <?=$this->applib->get_sales_order_no($invoice_data->sales_order_no);?>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					<strong>Date:<?=date('d/m/Y',strtotime($invoice_data->sales_order_date))?></strong>
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 3;?>">
					<strong>1. <u>Payment milestone :</u></strong><br/>
					30% of the ordered value in advance along with your confirm order<br/>
					20% of the ordered value within 30 days from date of your confirm order<br/>
					Balance at the time of delivery, before dispatch Other terms as Per Quotation. <strong>JK/JUMBO/Q19667/17/18</strong><br/>
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 5;?>">
					<strong><?=$invoice_data->party_name;?></strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 3;?>">
					<strong><u>Our Banks Account Details</u></strong>
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					<?=$invoice_data->address;?>
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					Dist: <?=$invoice_data->city;?>
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					State: <?=$invoice_data->state;?>
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					Contact person : <?=$invoice_data->contact_person_name;?>
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					Phone : <?=$invoice_data->contact_person_phone;?>
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					Email : <?=$invoice_data->contact_person_email;?>
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					<strong>Loading: Rajkot,GUJ.</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					<strong>Origin of Goods : India</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					<strong>Vessel / Flight No. : -</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					<strong>Place of Delivery: Perambalore T N</strong>
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 7;?>" width="150">
					<strong>Marks and Nos of Packs</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 7;?>" width="10">
					<strong>Sl.No.</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 5;?>" width="200">
					<strong>Description of Goods</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 7;?>">
					<strong>Rate Per Unit</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 7;?>">
					<strong>Quantity</strong>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 7;?>">
					<strong>Amount Rs.</strong>
				</td>
			</tr>
			<?php
				$sub_total = 0;
			?>
			<?php if(!empty($invoice_items)):?>
				<?php foreach($invoice_items as $item_row):?>
					<tr>
						<td class="text-bold" colspan="<?=$column_cnt - 7;?>">
							<?=$item_row['item_code'];?>
						</td>
						<td class="text-bold" colspan="<?=$column_cnt - 7;?>">
							1
						</td>
						<td class="text-bold" colspan="<?=$column_cnt - 5;?>">
							<?=$item_row['item_name'];?>
						</td>
						<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
							<?=$item_row['rate'];?>
						</td>
						<td class="text-bold text-center" colspan="<?=$column_cnt - 7;?>">
							<?=$item_row['quantity'];?>
						</td>
						<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
							<?=$item_row['amount'];?>
						</td>
					</tr>

				<?php
					$sub_total += $item_row['amount'];
					endforeach;
				?>
			<?php endif; ?>
			<?php
				$excise = $sub_total * 2 / 100;
				$sub_total_excise = $sub_total + $excise;
				$cst_form_c = $sub_total_excise * 2 / 100;
				$transportation = 110000;
				$total_amount = $sub_total + $excise + $cst_form_c + $transportation;
			?>

			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;<br/>
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;<br/>
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					&nbsp;
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					Ex- Factory
				</td>
				<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
					<?=$sub_total?>
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;<br/>
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					&nbsp;
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					Excise @ 2%
				</td>
				<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
					<?=$excise;?>
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;<br/>
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 7;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					&nbsp;
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					C.S.T. @ 2% Against Form 'c'
				</td>
				<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
					<?=$cst_form_c;?>
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 3;?>" rowspan="3">
					Note: Client has to submit security Cheque for 3% CST, once
					client submits C form, We will return security Cheque.
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					C.S.T. @ 3%
				</td>
				<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
					-
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					Packing Forwarding Insurance (on Buyer)
				</td>
				<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
					-
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					Transportation (on buyer)
				</td>
				<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
					<?=$transportation;?>
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 3;?>">
					Words: thirty lacs seventy five thousand one hundred and forty only
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					Total INR.
				</td>
				<td class="text-bold text-right" colspan="<?=$column_cnt - 7;?>">
					<?=$total_amount;?>
				</td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 3;?>">
					Goods of Origin
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 5;?>">
					FOR, JAY KHODIYAR MACHINE TOOLS <br/>
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
				</td>
			</tr>
			<tr>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 3;?>">
					&nbsp;
				</td>
				<td class="text-bold no-border-top" colspan="<?=$column_cnt - 5;?>">
					AUTHORISED SIGNATORY
				</td>
			</tr>
		</table>
	</body>
</html>
<i class="fa fa-angle-down"></i>