<?php $column_cnt = 18; ?>
<html>
    <head>
        <title><?=$invoice_data->invoice_type?> Invoice</title>
        <style>
            .text-center{
            text-align: center;
            }
            table{
            border-spacing: 0;
            width: 100%;
            border-bottom: 1px solid;
            border-right: 1px solid;
            }
            td{
            padding: 1px 1px 1px 1px;
            border-left: 1px solid;
            border-top: 1px solid;
            font-size:10px;
            }
            tr > td:last-child{
            border-right: 1px solid !important;
            }
            tr:last-child > td{
            border-bottom: 1px solid !important;
            }
            .text-right{
            text-align: right;
            }
            .text-bold{
            font-weight: 900 !important;
            font-size:12px !important;
            }
            .text-header{
            font-size: 20px;
            }
            .no-border-top{
            border-top:0;
            }
            .no-border-bottom{
            border-bottom:0 !important;
            }
            .no-border-left{
            border-left:0;
            }
            .no-border-right{
            border-right:0;
            }
            .width-50-pr{
            width:50%;
            }
            td.footer-sign-area{
            height: 100px;
            vertical-align: bottom;
            width: 33.33%;
            text-align: center;
            }
            .no-border{
            border: 0!important;
            }
            .footer-detail-area{
            color: #698c66;
            font-size: 12px;
            }
        </style>
    </head>
    <body>
        <?php
            $CURRENCY = "INR";
            setlocale(LC_MONETARY, 'en_IN');
            if($invoice_data->currency != ''){
            	$CURRENCY = strtoupper($invoice_data->currency);
            }
		?>
        <table>
            <tr>
                <td class="text-center text-bold text-header" colspan="4"><?=$invoice_data->invoice_type?> Invoice</td>
            </tr>
            <tr>
                <td class="text-center text-bold"><b>Manufacturer & Supplier</b></td>
                <td class="text-center text-bold" colspan="3"><b>Cash / Debit Memo</b></td>
            </tr>
            <tr>
                <td class="text-bold" style="font-size:15px;"><b><?=$company_details['name'];?></b></td>
                <td class="text-bold"  align="left" width="340px">Invoice : </td>
                <td class="text-bold" colspan="2">
                    <b>Origanl &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp; Duplicate &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp; Triplicat</b>
                </td>
            </tr>
            <tr>
                <td class="no-border-top text-bold" rowspan="5" >
                    <?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
                    <b>GST No : <?=$company_details['gst_no'];?></b>
                </td>
                <td class="text-bold"  align="left">Invoice No. : </td>
                <td class="text-bold"  align="left"><?=sprintf("%04d", $invoice_data->invoice_no);?></td>
                <td class="text-bold"  align="left">
                    <?=strtotime($invoice_data->invoice_date) != 0?date('d/m/Y',strtotime($invoice_data->invoice_date)):'';?>
                </td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Challan No. : </td>
                <td class="text-bold" >
                    <?=$invoice_data->challan_no; ?> 
                </td>
                <td class="text-bold" >
                    <?=$invoice_data->challan_date; ?> 
                </td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Quotation No. : </td>
                <td class="text-bold" colspan="2">
                    <?=$this->applib->get_quotation_ref_no($invoice_data->quotation_no,$sales_order_item['item_code']);?>
                </td>
            </tr>
            <tr>
                <td class=" text-bold"  align="left">Purchase Order No.: </td>
                <td class=" text-bold" >
                    <?=$invoice_data->cust_po_no; ?> 
                </td>
                <td class="text-bold" >
                    <?=$invoice_data->po_date; ?> 
                </td>
            </tr>
            <tr>
                <td class="text-bold"  align="left">Salse Order No. : </td>
                <td class="text-bold">
                    <?=$this->applib->get_sales_order_no($invoice_data->sales_order_no);?>
                </td>
                <td class="text-bold">
                    <?=$invoice_data->sales_order_date;?>
                </td>
            </tr>
            <tr>
                <td class="text-center text-bold"><b>Purchaser</b></td>
                <td class="text-center text-bold" colspan="3"><b>Mode Of Shipment</b></td>
            </tr>
            <tr>
                <td class="text-bold" rowspan="10" valign="top">
                    <b style="font-size:15px;"><?=$invoice_data->party_name?></b><br />
                    <?=nl2br($invoice_data->address);?><br />
                    City : <?=$invoice_data->city?> -  <?=$invoice_data->pincode?> (<?=$invoice_data->state?>) <?=$invoice_data->country?>.<br />
                    Email : <?=explode(",", $invoice_data->party_email_id)[0];?><br />
                    Tel No.:  <?=$invoice_data->fax_no;?>,<br />
                    Contact No.:  <?=$invoice_data->p_phone_no;?><?= ($invoice_data->p_phone_no != $invoice_data->contact_person_phone) ? ', '.$invoice_data->contact_person_phone : '' ?><br />
                    Contact Person: <?=$invoice_data->contact_person_name?><br />
                    <b>GST No. : <?=$invoice_data->party_gst_no;?></b><?= isset($invoice_data->party_cin_no) && !empty($invoice_data->party_cin_no) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$invoice_data->party_cin_no.'</b>' : ''; ?>
                </td>
                <td class=" text-bold">Loading At : </td>
                <td class=" text-bold" colspan="2"><?=$invoice_data->loading_at?></td>
            </tr>
            <tr>
                <td class=" text-bold" >Port of Loading : </td>
                <td class=" text-bold" colspan="2"><?=$invoice_data->port_of_loading?> </td>
            </tr>
            <tr>
                <td class=" text-bold" >Port of  Discharge : </td>
                <td class=" text-bold" colspan="2"> <?=$invoice_data->port_of_discharge?></td>
            </tr>
            <tr>
                <td class=" text-bold" >Place of Delivery : </td>
                <td class=" text-bold" colspan="2"><?=$invoice_data->port_place_of_delivery?></td>
            </tr>
            <tr>
                <td class=" text-bold" >Delivery Through : </td>
                <td class=" text-bold" colspan="2"><?=$invoice_data->delivery_through?></td>
            </tr>
            <tr>
                <td class=" text-bold" >Name of shipment : </td>
                <td class=" text-bold" colspan="2"><?=$invoice_data->name_of_shipment?></td>
            </tr>
            <tr>
                <td class=" text-bold" >Truck / Container No. :</td>
                <td class=" text-bold" colspan="2"><?=$invoice_item['truck_container_no'];?></td>
            </tr>
            <tr>
                <td class=" text-bold">LR / BL No. : </td>
                <td class=" text-bold"><?=$invoice_item['lr_bl_no'];?> </td>
                <td class=" text-bold" ><?=$invoice_item['lr_date'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" >Contact Person :</td>
                <td class=" text-bold" colspan="2"><?=$invoice_item['contact_person'];?> - <?=$invoice_item['contact_no'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" >Driver Name : </td>
                <td class=" text-bold" colspan="2"><?=$invoice_item['driver_name'];?> - <?=$invoice_item['driver_contact_no'];?></td>
            </tr>
            <tr>
                <td class="text-left text-bold width-50-pr" style="font-size:15px;"><b> <?=$invoice_item['item_name'];?></b></td>
                <td class="text-left text-bold width-50-pr" colspan="3" style="font-size:12px;"><b>Serial No.: </b><?=$invoice_item['item_serial_no'];?> </td>
            </tr>
        </table>
        <?php
            $pure_amount = $invoice_item['quantity'] * $invoice_item['rate'];
            $pure_amount = !empty($pure_amount)?$pure_amount:'0';
            
            $invoice_item['disc_value'] = $pure_amount * $invoice_item['disc_per'] / 100;
            $invoice_item['disc_value'] = !empty($invoice_item['disc_value'])?$invoice_item['disc_value']:'0';
            
            $taxable_amount = $pure_amount - $invoice_item['disc_value'];
            $taxable_amount = !empty($taxable_amount)?$taxable_amount:'0';
            
            $invoice_item['igst'] = !empty($invoice_item['igst'])?$invoice_item['igst']:'0';
            $invoice_item['igst_amount'] = $taxable_amount * $invoice_item['igst'] / 100;
            $invoice_item['cgst'] = !empty($invoice_item['cgst'])?$invoice_item['cgst']:'0';
            $invoice_item['cgst_amount'] = $taxable_amount * $invoice_item['cgst'] / 100;
            $invoice_item['sgst'] = !empty($invoice_item['sgst'])?$invoice_item['sgst']:'0';
            $invoice_item['sgst_amount'] = $taxable_amount * $invoice_item['sgst'] / 100;
            $invoice_item['total_amount'] = $taxable_amount + $invoice_item['igst_amount'] + $invoice_item['cgst_amount'] + $invoice_item['sgst_amount'];
            
            $invoice_data->freight = !empty($invoice_data->freight)?$invoice_data->freight:'0';
            $invoice_data->packing_forwarding = !empty($invoice_data->packing_forwarding)?$invoice_data->packing_forwarding:'0';
            $invoice_data->transit_insurance = !empty($invoice_data->transit_insurance)?$invoice_data->transit_insurance:'0';
            $total_amount = $taxable_amount + $invoice_data->freight + $invoice_data->packing_forwarding + $invoice_data->transit_insurance;
            
            $invoice_data->freight_cgst = !empty($invoice_data->freight_cgst)?$invoice_data->freight_cgst:'0';
            $freight_cgst_amount = $invoice_data->freight * $invoice_data->freight_cgst / 100;
            $invoice_data->freight_sgst = !empty($invoice_data->freight_sgst)?$invoice_data->freight_sgst:'0';
            $freight_sgst_amount = $invoice_data->freight * $invoice_data->freight_sgst / 100;
            $invoice_data->freight_igst = !empty($invoice_data->freight_igst)?$invoice_data->freight_igst:'0';
            $freight_igst_amount = $invoice_data->freight * $invoice_data->freight_igst / 100;
            $freight_gst_amount = $freight_cgst_amount + $freight_sgst_amount + $freight_igst_amount;
            $freight_amount = $invoice_data->freight + $freight_gst_amount;
            
            $invoice_data->packing_forwarding_cgst = !empty($invoice_data->packing_forwarding_cgst)?$invoice_data->packing_forwarding_cgst:'0';
            $packing_forwarding_cgst_amount = $invoice_data->packing_forwarding * $invoice_data->packing_forwarding_cgst / 100;
            $invoice_data->packing_forwarding_sgst = !empty($invoice_data->packing_forwarding_sgst)?$invoice_data->packing_forwarding_sgst:'0';
            $packing_forwarding_sgst_amount = $invoice_data->packing_forwarding * $invoice_data->packing_forwarding_sgst / 100;
            $invoice_data->packing_forwarding_igst = !empty($invoice_data->packing_forwarding_igst)?$invoice_data->packing_forwarding_igst:'0';
            $packing_forwarding_igst_amount = $invoice_data->packing_forwarding * $invoice_data->packing_forwarding_igst / 100;
            $packing_forwarding_gst_amount = $packing_forwarding_cgst_amount + $packing_forwarding_sgst_amount + $packing_forwarding_igst_amount;
            $packing_forwarding_amount = $invoice_data->packing_forwarding + $packing_forwarding_gst_amount;
            
            $invoice_data->transit_insurance_cgst = !empty($invoice_data->transit_insurance_cgst)?$invoice_data->transit_insurance_cgst:'0';
            $transit_insurance_cgst_amount = $invoice_data->transit_insurance * $invoice_data->transit_insurance_cgst / 100;
            $invoice_data->transit_insurance_sgst = !empty($invoice_data->transit_insurance_sgst)?$invoice_data->transit_insurance_sgst:'0';
            $transit_insurance_sgst_amount = $invoice_data->transit_insurance * $invoice_data->transit_insurance_sgst / 100;
            $invoice_data->transit_insurance_igst = !empty($invoice_data->transit_insurance_igst)?$invoice_data->transit_insurance_igst:'0';
            $transit_insurance_igst_amount = $invoice_data->transit_insurance * $invoice_data->transit_insurance_igst / 100;
            $transit_insurance_gst_amount = $transit_insurance_cgst_amount + $transit_insurance_sgst_amount + $transit_insurance_igst_amount;
            $transit_insurance_amount = $invoice_data->transit_insurance + $transit_insurance_gst_amount;
            
            $cgst_amount = $invoice_item['cgst_amount'] + $freight_cgst_amount + $packing_forwarding_cgst_amount + $transit_insurance_cgst_amount;
            $sgst_amount = $invoice_item['sgst_amount'] + $freight_sgst_amount + $packing_forwarding_sgst_amount + $transit_insurance_sgst_amount;
            $igst_amount = $invoice_item['igst_amount'] + $freight_igst_amount + $packing_forwarding_igst_amount + $transit_insurance_igst_amount;
            $total_gst_amount = $invoice_item['igst_amount'] + $invoice_item['cgst_amount'] + $invoice_item['sgst_amount'] + $freight_gst_amount + $packing_forwarding_gst_amount + $transit_insurance_gst_amount;
            $grand_total = $total_amount + $total_gst_amount;
		?>
        <table>
            <tr>
                <td class=" text-bold" colspan="1" align="center" width="20px"><b>Sr.No.</b> </td>
                <td class=" text-bold" colspan="5" align="center" width="200px"><b>Item Name</b> </td>
                <td class=" text-bold" colspan="2" align="center">&nbsp;&nbsp;<b>HSN</b>&nbsp;&nbsp;</td>
                <td class=" text-bold" colspan="1" align="center" width="90px"><b>Unit / Nos</b></td>
                <td class=" text-bold" colspan="2" align="center" width="40px"><b>Rate</b></td>
                <td class=" text-bold" colspan="1" align="center" width="40px"><b>&nbsp;Discount&nbsp;</b> </td>
                <td class=" text-bold" colspan="1" align="center" width="22px"><b>&nbsp;CGST&nbsp;</b> </td>
                <td class=" text-bold" colspan="1" align="center" width="22px"><b>SGST</b> </td>
                <td class=" text-bold" colspan="1" align="center" width="22px"><b>&nbsp;IGST&nbsp;</b> </td>
                <td class=" text-bold" colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;<b>Amount</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">1</td>
                <td class=" text-bold" colspan="5"><?=$invoice_item['item_name'];?><br /><?=$invoice_item['item_extra_accessories'];?></td>
                <td class="text-center text-bold" colspan="2"><?=$invoice_item['hsn_no'];?></td>
                <td class="text-center text-bold" colspan="1"><?=$invoice_item['quantity'];?></td>
                <td class="text-center text-bold" colspan="2"><?=money_format('%!i', $invoice_item['rate']);?></td>
                <td class="text-center text-bold" colspan="1"><?=$invoice_item['disc_per'];?>%</td>
                <td class="text-center text-bold" colspan="1"><?=$invoice_item['cgst'];?>%</td>
                <td class="text-center text-bold" colspan="1"><?=$invoice_item['sgst'];?>%</td>
                <td class="text-center text-bold" colspan="1"><?=$invoice_item['igst'];?>%</td>
                <td class="text-center text-bold" colspan="3"><?=money_format('%!i', $invoice_item['total_amount']);?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="1" align="center">&nbsp;</td>
                <td class=" text-bold" colspan="5"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="2"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="1"></td>
                <td class=" text-bold" colspan="3" align="center"></td>
            </tr>
            <tr>
                <td class="" colspan="12" rowspan="5" valign="top">
                    <table class="no-border">
                        <tr>
                            <td align="center" class="no-border-top no-border-left" width="140px" >GST Summary</td>
                            <td align="center" class="no-border-top" >Taxable Amt</td>
                            <td align="center" class="no-border-top" >CGST</td>
                            <td align="center" class="no-border-top" >SGST</td>
                            <td align="center" class="no-border-top" >IGST</td>
                            <td align="center" class="no-border-top" width="60px" >Total</td>
                        </tr>
                        <tr>
                            <td class="no-border-left" ><?=$invoice_item['item_name'];?></td>
                            <td align="right"><?=money_format('%!i', $taxable_amount);?></td>
                            <td align="right"><?=money_format('%!i', $invoice_item['cgst_amount']);?> : @<?=$invoice_item['cgst'];?>%</td>
                            <td align="right"><?=money_format('%!i', $invoice_item['sgst_amount']);?> : @<?=$invoice_item['sgst'];?>%</td>
                            <td align="right"><?=money_format('%!i', $invoice_item['igst_amount']);?> : @<?=$invoice_item['igst'];?>%</td>
                            <td align="right"><?=money_format('%!i', $invoice_item['total_amount']);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" >Freight</td>
                            <td align="right"><?=money_format('%!i', $invoice_data->freight);?></td>
                            <td align="right"><?=money_format('%!i', $freight_cgst_amount);?> : @<?=$invoice_data->freight_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_sgst_amount);?> : @<?=$invoice_data->freight_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_igst_amount);?> : @<?=$invoice_data->freight_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" >Packing & Forwarding</td>
                            <td align="right"><?=money_format('%!i', $invoice_data->packing_forwarding);?></td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_cgst_amount);?> : @<?=$invoice_data->packing_forwarding_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_sgst_amount);?> : @<?=$invoice_data->packing_forwarding_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_igst_amount);?> : @<?=$invoice_data->packing_forwarding_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" >Transit Insurance</td>
                            <td align="right"><?=money_format('%!i', $invoice_data->transit_insurance);?></td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_cgst_amount);?> : @<?=$invoice_data->transit_insurance_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_sgst_amount);?> : @<?=$invoice_data->transit_insurance_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_igst_amount);?> : @<?=$invoice_data->transit_insurance_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" ><b>Total</b></td>
                            <td align="right"><?=money_format('%!i', $total_amount);?></td>
                            <td align="right"><?=money_format('%!i', $cgst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $sgst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $igst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $grand_total);?></td>
                        </tr>
                    </table>
                </td>
                <td class=" text-bold" colspan="3">Sub Total : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $pure_amount);?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Discount : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $invoice_item['disc_value']);?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Sub Total : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $taxable_amount);?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Freight : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $invoice_data->freight);?></td>
            </tr>
            <tr>
				<td class=" text-bold" colspan="3">P & F : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $invoice_data->packing_forwarding);?></td>
            </tr>
            <tr>
				<td class=" text-bold" colspan="12" valign="top">In Words <?=$CURRENCY?> : <?=money_to_word($grand_total)?> only.</td>
                <td class=" text-bold" colspan="3">Insurance : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $invoice_data->transit_insurance);?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="12" rowspan="5" valign="top"><b>Terms & Conditions : </b><br /><?=$invoice_data->terms_condition_purchase;?></td>
                <td class=" text-bold" colspan="3">Total : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $total_amount);?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">GST : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $total_gst_amount);?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Grand Total : </td>
                <td class=" text-bold" colspan="3" align="right"><?=money_format('%!i', $grand_total);?></td>
            </tr>
            <tr>
				<td class=" text-bold" colspan="6" align="right">
					<b>For, <?=$company_details['name'];?></b><br /><br /><br /><br /><br /><br /><br /><br />
				</td>
			</tr>
			<tr>
				<td class="no-border-top text-bold" colspan="6" align="center">
					<div style="text-align:center;">Authorized Signature</div>
				</td>
			</tr>
        </table>
    </body>
</html>
