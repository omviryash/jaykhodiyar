<div class="content-wrapper">
    <form class="form-horizontal" action="" method="post" id="form_payment">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Expected Payment</small>
                <button type="button" class="btn btn-info btn-xs pull-right btn-save-item" id="add_expected_payment" style="margin: 5px;">
                    <?php if(isset($id) && !empty($id)){ ?>Update 
                    <?php } else { ?>Save 
                    <?php } ?>
                </button>
                <button type="button" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Ex.Payment List</button>
                <button type="button" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Ex.Payment</button>
             </h1>
        </section>
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">Party</label>
													<div class="col-sm-8 dispaly-flex">
														<select name="payment_data[party_id]" id="party_id" class="form-control input-sm party_type"></select>
													</div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">
                                                    Date
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm disabled" name="payment_data[payment_date]" id="datepicker3" value="">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 input-sm">
                                                    Amount
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="number" class="form-control input-sm " name="payment_data[amount]" value="" id="amount">
                                                </div>
                                            </div>
                                           	<div class="form-group">
													<label for="inputEmail3" class="col-sm-4 input-sm">Note</label>
													<div class="col-sm-8 dispaly-flex">
														<textarea class="form-control" rows="3" name="payment_data[note]" id="note"></textarea>
													</div>
												</div>
											</div> 
                                        </div>
										<div class="col-md-12 text-right">
											<button type="button" class="btn btn-info btn-xs btn-show-follow-up-history">Follow Up History</button>
										 </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<!-- Start PopUp Form -->

<!--Modal-->
<div class="modal" id="modal-list-follow-up-history">
    <div class="modal-dialog" style="width: 80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title col-sm-9">Payment History</h4>
                <button type="button" class="close btn-close-follow-up-history"><span aria-hidden="true">&times;</span></button>
                <div class="col-sm-3 pull-right" id="btn-new">
                    <input type="button" class="form-control btn btn-primary pull-right" name="  followup_history_new_button" id="followup_history_new_button" value="New">
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="table_followup_history_data" class="table custome-table">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Date</th>
                                    <th>Process Name</th>
                                    <th>Followed Up By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($followup_history_data)) {
                                    foreach ($followup_history_data as $followup_history_data_row) {
                                        ?>
                                        <tr data-followup_id="<?=$followup_history_data_row->id;?>"> 
                                            <td>
                                                <a href="javascript:void(0);" class="followup_history_edit_button btn-primary btn-xs" data-id="<?= $followup_history_data_row->id; ?>"><i class="fa fa-edit"></i></a>&nbsp;
                                                 <?php echo " | "; ?>
                                                <a href="javascript:void(0);" class="followup_history_delete_button btn-danger btn-xs" data-id="<?= $followup_history_data_row->id; ?>"><i class="fa fa-trash"></i></a>
                                            </td>
                                            <td><?= date('d-m-Y',strtotime($followup_history_data_row->followup_date)); ?></td>
                                            <td><?= $followup_history_data_row->history; ?></td>
                                            <td><?= $followup_history_data_row->followup_by; ?></td>
                                        </tr>
                                        <?php        
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border text-primary text-bold">
                                Payment History
                            </legend>
                            <div class="col-md-6">
                                <form action="" class="" id="followup_history_form" name="followup_history_form" method="post">
                                    <input type="hidden" name="followup_history_payment_id" id="followup_history_payment_id" value="">
                                    <input type="hidden" name="followup_history_id" id="followup_history_id" value="0">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 input-sm">Date</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm pull-right input-datepicker" name="followup_history_date" id="followup_history_date" value="<?= date('d-m-Y'); ?>">
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 input-sm">History</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" rows="5" name="followup_history_history" id="followup_history_history"></textarea>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <br/>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-9 input-sm"></label>
                                        <div class="col-sm-3">  
                                            <input type="button" class="form-control btn btn-primary pull-right" name="followup_history_history_submit" id="followup_history_history_submit" value="Save">
                                            <input type="submit" class="hidden btn-submit-followup-form" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

<!-- End PopUp Form -->

<script>

    $(document).on("click","#add_expected_payment", function(e){

        
    });
    var PAYMENT_ID = 0;

    $(document).on('click','.btn-save-item',function(){
        $("#form_payment").submit();
    });

    $("#form_payment").on("submit",function(e){
        e.preventDefault();

        var party = $("#party_id").val();
        var amount = $("#amount").val();
        if (party_id == '') {
            show_notify('Fill value of Party code',false);
        }
        else if(amount == ''){
            show_notify('Fill value of Amount',false);
        }
    });

    $(document).on('click','.btn-show-follow-up-history',function () {
        $("#modal-list-follow-up-history").modal('show');
    });

    $(document).on('click', '.btn-close-follow-up-history', function () {
        if ($('textarea[name="followup_history_history"]').val() != '') {
            var confirm_value = confirm('Are you sure ?');
            if (confirm_value) {
                $("#modal-list-follow-up-history").modal('hide');
            }
        } else {
            $("#modal-list-follow-up-history").modal('hide');
        }
    });

    $(document).on ('click', '#followup_history_history_submit', function(){
        $(".btn-submit-followup-form").trigger('click');
    });

    $("#followup_history_form").on("submit", function(e){
        e.preventDefault();
        var FormVar = $(this);
        var history_id = ('#followup_history_id').val();
        if (PAYMENT_ID == 0) {
            FOLLOW_UP_HISTORY = 1;
            $("#form_payment").submit();
        }else{
            $.ajax({
                url:$core.url('payment/add_followup_history'),
                type:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:'json',
                success:function(data){
                    console.log(data);
                    if (data.status == true) {
                        show_notify(data.message, true);
                        FOLLOW_UP_HISTORY = 0;
                        if (history_id == 0) {
                            var html = '<tr><td>';
                            html += '<a href="javascript.void(0);" class="followup_history_edit_button btn-primary btn-xs" data-id="'+data.followup_history_id+'"><i class="fa fa-edit"></i></a>&nbsp';
                            html += '|';
                            html += '<a href="javascript.void(0);" class="followup_history_delete_button btn-danger btn-xs data-id="'+data.followup_history_id+'><i class="fa fa-trash"></i></a>';
                            html += '</td>';
                            html += '<td>' + $('#followup_history_date').val() + '</td>';
                            html += '<td>' + $('#followup_history_history').val() + '</td>';
                            $("table#table_followup_history_data > tbody ").append(html);

                                var m_names = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                $('#followup_history_history').val('');
                                $('#followup_history_history_submit').val('Add');
                                var d = new Date();
                                var curr_date = d.getDate();
                                var curr_month = d.getMonth();
                                var curr_year = d.getFullYear();
                                var date = curr_date + "-" + m_names[curr_month] + "-" + curr_year;
                                $('#followup_history_date').val(date);
                            } else {
                                var html = '<td>';
                                html += '<a href="javascript:void(0);" class="followup_history_edit_button btn-primary btn-xs" data-id="' + data.data.id + '"><i class="fa fa-edit"></i></a>&nbsp;';
                                html += ' | ';
                                html += '<a href="javascript:void(0);" class="followup_history_delete_button btn-danger btn-xs" data-id="' + data.data.id + '"><i class="fa fa-trash"></i></a>';
                                html += '</td>';
                                var date = convertDate(data.data.followup_date);
                                html += '<td>' + date + '</td>';
                                html += '<td>' + data.data.history + '</td>';
                                html += '<td>' + data.data.followup_by + '</td>';
                                $('.selected').html(html);
                                $('.selected').removeClass('selected');
                                $('#followup_history_id').val(0);
                                var m_names = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                var d = new Date();
                                var curr_date = d.getDate();
                                var curr_month = d.getMonth();
                                var curr_year = d.getFullYear();
                                var date = curr_date + "-" + m_names[curr_month] + "-" + curr_year;
                                $('#followup_history_date').val(date);
                                $('#followup_history_history').val('');
                                $('#followup_history_history_submit').val('Add');
                            }
                        }else if(data.status == false){
                            show_notify(data.message, false);
                        }
                    }
            });
        }
    });
</script>
