<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Invoice List</small>
            <?php
            $invoice_add_role = $this->app_model->have_access_role(INVOICE_MODULE_ID, "add");
            ?>

            <?php if($invoice_add_role): ?>
            <a href="<?=base_url('finance/invoice')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice</a>
            <?php endif;?>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="order_list_table" class="table custom-table table-striped">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Invoice No.</th>
                                <th>Challan No.</th>
                                <th>Sales Order No.</th>
                                <th>Quotation No.</th>
                                <th>Customer Name </th>
                                <th>Invoice Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <section class="content-header">
        <h1>
            <?php if($invoice_add_role): ?>
            <a href="<?=base_url('finance/invoice')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice</a>
            <?php endif;?>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>

</div>

<script>
    var table;
    $(document).ready(function() {

        table = $('#order_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('finance/invoice_datatable')?>",
                "type": "POST",
                "data":function(d){}
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=invoices',
                    success: function(data) {
                        show_notify("Invoice removed successfully",true);
                        tr.fadeOut(function(){
                            $(this).remove();
                        });
                    }
                });
            }
        });

    });
</script>
