<?php
$column_cnt = 8;
?>
<html>
<head>
	<title>Sales Order</title>
	<style>
		.text-center{
			text-align: center;
		}
		table{
			border-spacing: 0;
			width: 100%;
			border-bottom: 1px solid;
			border-right: 1px solid;
		}
		td{
			padding: 1px 1px 1px 1px;
			border-left: 1px solid;
			border-top: 1px solid;
			font-size:12px;
		}
		tr > td:last-child{
			border-right: 1px solid !important;
		}
		tr:last-child > td{
			border-bottom: 1px solid !important;
		}
		.text-right{
			text-align: right;
		}
		.text-bold{
			font-weight: 900 !important;
			font-size:12px !important;
		}
		.text-header {
			font-size: 20px;
			font-weight: 700;
		}
		.no-border-top{
			border-top:0;
		}
		.no-padding{
			padding:0;
		}
		.no-border-bottom{
			border-bottom:0 !important;
		}
		.no-border-left{
			border-left:0;
		}
		.no-border-right{
			border-right:0;
		}
		.width-50-pr{
			width:50%;
		}
		.width-40-pr{
			width:40%;
		}
		td.footer-sign-area{
			height: 100px;
			vertical-align: bottom;
			width: 33.33%;
			text-align: center;
		}
		.no-border{
			border: 0!important;
		}
		.footer-detail-area{
			color: #698c66;
			font-size: 12px;
		}
		.display-inline{
			display:inline-block ;
		}
		.width-50-pr{
			width: 50px;
		}
		.text-left{
			text-align: left;
		}
		.high-light-cell{
			background-color: #a9a9a6;
			font-weight: 700;
		}
		.padding-5px{
			padding: 5px;
		}
		.padding-left-5px{
			padding-left: 25px;
		}
	</style>
</head>
<body>
<?php
$CURRENCY = "INR";
if($invoice_data->currency != ''){
	$CURRENCY = strtoupper($invoice_data->currency);
}
?>
<table>
	<tbody>
	<tr>
		<td class="text-left text-bold" colspan="2" width="350">ECC No. : <?=$company_details['esic_no'];?></td>
		<td class="text-left text-bold" colspan="1"><strong>DEBIT</strong></td>
		<td class="text-right text-bold no-border-left" colspan="1"><strong>ORIGINAL</strong></td>
	</tr>
	<tr>
		<td class="text-left text-bold no-border-top" colspan="2">Range : </td>
		<td class="text-center text-bold text-header" colspan="2" rowspan="2"><?=strtoupper($invoice_data->invoice_type);?> INVOICE</td>
	</tr>
	<tr>
		<td class="text-left text-bold no-border-top" colspan="2">Division : </td>
	</tr>
	<tr>
		<td class="text-left text-bold no-border-top" colspan="2">Commissionerate : Rajkot</td>
		<td class="text-center text-bold no-border-top" colspan="2">For removal of goods from factory or warehouse (Duty Payable U/R, 11)</td>
	</tr>
	<tr>
		<td class="text-left text-bold no-border-top" colspan="2">VAT TIN No. : <?=$company_details['vat_no']?></td>
		<td class="text-center text-bold" colspan="2">Conseocinal rate of duty are claimed under Notification No. </td>
	</tr>
	<tr>
		<td class="text-left text-bold no-border-top" colspan="2">CST TIN No. : <?=$company_details['cst_no']?></td>
		<td class="text-center text-bold no-border-top" colspan="1">16/2012</td>
		<td class="text-center text-bold no-border-top no-border-left" colspan="1">Date : 17-03-2012</td>
	</tr>
	</tbody>
</table>

<table>
	<tbody>
	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 3 ;?>">Name & Address of Consignee:</td>
		<td class="text-left text-bold padding-5px high-light-cell" colspan="<?=$column_cnt - 5 ;?>" rowspan="2"><strong>INVOICE NO.: <?=$invoice_data->invoice_no;?></strong></td>
	</tr>
	<tr>
		<td class="text-left text-bold no-border-top padding-left-5px" colspan="<?=$column_cnt - 3 ;?>"><strong><?=$invoice_data->party_name;?></strong></td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top padding-left-5px" colspan="<?=$column_cnt - 3 ;?>"><?=$invoice_data->address;?></td>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 5 ;?>"><strong>Date & Time of Preparation 27/04/2016 14:02</strong></td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top padding-left-5px" colspan="<?=$column_cnt - 3 ;?>">M.No.: <?=$invoice_data->phone_no;?></td>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 5 ;?>"><strong>Date & Time of Removal 27/04/2016 14:02</strong></td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top padding-left-5px" colspan="<?=$column_cnt - 3 ;?>"><strong><?=$invoice_data->city;?> <?=$invoice_data->state;?></strong></td>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 7 ;?>">Order No:</td>
		<td class="text-left text-bold no-border-left" colspan="<?=$column_cnt - 7 ;?>"><?=$invoice_data->sales_order_no?></td>
		<td class="text-left text-bold no-border-left" colspan="<?=$column_cnt - 7 ;?>">Dt: <?=date("d/m/Y",strtotime($invoice_data->sales_order_date))?></td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 3 ;?>">C.Ex.Reg.No:</td>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 5 ;?>">Name of Excisable Goods & Tarrif Sub Heading No:</td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 5 ;?>"><b>GST TIN No:</b> <?=$invoice_data->tin_vat_no;?></td>
		<td class="text-left text-bold no-border-top no-border-left" colspan="<?=$column_cnt - 6 ;?>"><b>CST TIN No:</b>  <?=$invoice_data->tin_cst_no;?></td>
		<td class="text-center text-bold no-border-top" colspan="<?=$column_cnt - 5 ;?>" rowspan="2"><strong>23897797979</strong></td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 3 ;?>"><b>PAN No:</b> <?=$invoice_data->pan_no;?></td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 7 ;?>" width="30">Sr.</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 4 ;?>" width="400">Description of Goods</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">Total Qty</td>
		<td class="text-right text-bold" colspan="<?=$column_cnt - 7 ;?>">Net Price </td>
		<td class="text-right text-bold" colspan="<?=$column_cnt - 7 ;?>">Amount</td>
	</tr>
	<?php
	$sub_total = 0;
	if(!empty($invoice_items)):
		$i = 1;
		foreach($invoice_items as $item_row):
			$sub_total = $sub_total + $item_row['amount'];
			?>
			<tr>
				<td class="text-left text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=$i;?></td>
				<td class="text-left text-bold" colspan="<?=$column_cnt - 4 ;?>"><strong><?=$item_row['item_code']?></strong><br/><?=$item_row['item_name']?></td>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=$item_row['quantity']?></td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=moneyformat($item_row['rate'])?> </td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=moneyformat($item_row['amount'])?></td>
			</tr>
			<?php
			$i++;
		endforeach;
	endif;
	?>
	<tr>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 7 ;?>">&nbsp;<br/><br/><br/><br/><br/><br/><br/><br/></td>
		<td class="text-center text-bold no-border-top" colspan="<?=$column_cnt - 4 ;?>">&nbsp;</td>
		<td class="text-center text-bold no-border-top" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
		<td class="text-right text-bold no-border-top" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
		<td class="text-right text-bold no-border-top" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 4 ;?>">&nbsp;</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>"><strong>Sub Total</strong></td>
		<td class="text-right text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=moneyformat($sub_total)?></td>
	</tr>
	</tbody>
</table>

<table>
	<tbody>
	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 5 ;?>" width="200"><strong>Delivery AT:</strong></td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 5 ;?>">Serial No & Date of data of  Entry of Duty in</td>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 7 ;?>" width="200"><strong>Assessable Value</strong></td>
		<td class="text-right text-bold no-border-left" colspan="<?=$column_cnt - 7 ;?>"><strong><?=moneyformat($sub_total)?></strong></td>
	</tr>
	<?php
	$grand_total = $sub_total;
	?>
	<tr>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 5 ;?>"><strong>Dispatch Though:</strong> Direct</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">KLA</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">Count</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">Date</td>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 7 ;?>" rowspan="4">
		<?php 
		$taxes_amount_str = '';
		$last_sub_total = 0;
		if ($invoice_data->taxes_data) { 
			$taxes_data = unserialize($invoice_data->taxes_data);
			foreach ($taxes_data as $key => $value) {
				echo $value['name'].' '.moneyformat($value['pecentage']).'%';
				echo '<br>';
				$duty_payable = $sub_total * $value['pecentage'] / 100;
				$taxes_amount_str .= moneyformat($duty_payable).'<br>';
				$last_sub_total = $last_sub_total + $duty_payable;
			}
		}
		$duty_payable = $last_sub_total;
		$grand_total = $grand_total + $last_sub_total;
		?>
		</td>
		<td class="text-right text-bold no-border-left" colspan="<?=$column_cnt - 7 ;?>" rowspan="4"><?=$taxes_amount_str?></td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 5 ;?>">L.R. No 436 Date:23-04-2017</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
		<td class="text-center text-bold" colspan="<?=$column_cnt - 7 ;?>">&nbsp;</td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 5 ;?>" rowspan="2">Vehicle No : GJ3-AT-1605</td>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 6 ;?>">Rate of C.Ex.Duty</td>
		<td class="text-right text-bold no-border-left" colspan="<?=$column_cnt - 7 ;?>">2.00%</td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 5 ;?>"><strong>Against Form: C</strong></td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 2 ;?>" rowspan="2"><strong>Excise Duty:</strong><?=money_to_word($duty_payable)?></td>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 7 ;?>"></td>
		<td class="text-right text-bold no-border-left no-border-top" colspan="<?=$column_cnt - 7 ;?>"></td>
	</tr>

	<tr>
		<td class="text-left text-bold no-border-top" colspan="<?=$column_cnt - 7 ;?>"></td>
		<td class="text-right text-bold no-border-left no-border-top" colspan="<?=$column_cnt - 7 ;?>"></td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 2 ;?>"><strong>Rupees:<?=money_to_word($grand_total)?> only</strong></td>
		<td class="text-left text-bold high-light-cell" colspan="<?=$column_cnt - 7 ;?>" rowspan="2"><strong>Grand Total</strong></td>
		<td class="text-right text-bold no-border-left high-light-cell" colspan="<?=$column_cnt - 7 ;?>" rowspan="2"><?=moneyformat($grand_total)?></td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt - 2 ;?>">&nbsp;</td>
	</tr>

	<tr>
		<td class="text-left text-bold" colspan="<?=$column_cnt ;?>">
			<br/><br/><br/><br/>
		</td>
	</tr>

	<tr>
		<td class="text-right text-bold" colspan="<?=$column_cnt ;?>">
			<strong>
				For,Jay Khodiyar Machine Tools
				<br/><br/><br/><br/>
				Authorised Signatory
			</strong>
		</td>
	</tr>
	</tbody>
</table>
</body>
</html>