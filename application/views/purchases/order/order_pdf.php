<?php
	$column_cnt = 10;
?>
<html>
	<head>
		<title>Purchase Order</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:10px;
			}
			tr > td:last-child{
				border-right: 1px solid !important;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:12px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				width: 33.33%;
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #000000;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
		
	<?php
		$CURRENCY = "INR";
		if($purchases_order_data->currency != ''){
			$CURRENCY = strtoupper($purchases_order_data->currency);
		}
	?>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="<?=$column_cnt;?>"><b>Purchase Order</b></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 6 ;?>"><b>Purchase By</b></td>
				<td class=" text-bold" colspan="<?=$column_cnt - 7 ;?>">Date : </td>
				<td class=" text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=strtotime($purchases_order_data->order_date) != 0?date('d/m/Y',strtotime($purchases_order_data->order_date)):date('d/m/Y')?></td>
			</tr>
			<tr>
				<td class="text-left text-bold" rowspan="3" colspan="<?=$column_cnt - 6 ;?>">
                    <b><?=$company_details['name'];?></b><br>
                    <?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>) <?=$company_details['country'];?>.<br>
                    Email : <?=$company_details['email_id'];?><br>
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
				    <b>GST No. : <?=$company_details['gst_no'];?></b>
				</td>
				<td class="text-bold" colspan="<?=$column_cnt - 7 ;?>">Purchase Order : </td>
                <td class="text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=$this->applib->get_purchase_order_no($purchases_order_data->order_no);?></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 7 ;?>">Purchase Quote Ref. : </td>
				<td class="text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=$purchases_order_data->purchase_quote_ref?></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 7 ;?>">Purchase Executive : </td>
				<td class="text-bold" colspan="<?=$column_cnt - 7 ;?>"><?=$purchases_order_data->created_name?></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 6 ;?>"><B>Supplier By</B></td>
				<td class="text-center text-bold" colspan="<?=$column_cnt - 4 ;?>"><b>Material Required Of Delivery Date</b></td>
			</tr>
			<tr>
				<td class="text-left text-bold" rowspan="5" colspan="<?=$column_cnt - 6 ;?>">
					<b><?=$purchases_order_data->supplier_name?></b><br>
                    <?=nl2br($purchases_order_data->address);?><br />
					City : <?=$purchases_order_data->city?> - <?=$purchases_order_data->pincode?> (<?=$purchases_order_data->state?>) <?=$purchases_order_data->country?>.<br>
					Email : <?=$purchases_order_data->email_id?><br>
					Tel No. : <?=$purchases_order_data->tel_no?><br>
					Contact No. : <?=$purchases_order_data->contact_no?><br>
					Contact Person : <?=$purchases_order_data->cont_person?><br>
					<b>GST No. : <?=$purchases_order_data->gstin?></b><?= isset($purchases_order_data->cin_no) && !empty($purchases_order_data->cin_no) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$purchases_order_data->cin_no.'</b>' : ''; ?>
				</td>
                <td class="text-left text-bold" colspan="<?=$column_cnt - 8 ;?>"><b>Minimum</b> :</td>
                <td class="text-left text-bold" colspan="<?=$column_cnt - 8 ;?>">
                    &nbsp; <?=$purchases_order_data->mach_deli_min_weeks?>&nbsp;&nbsp; Days &nbsp;&nbsp;
                </td>
                <td class="text-left text-bold" colspan="<?=$column_cnt - 8 ;?>">
					<?php if(!empty($purchases_order_data->mach_deli_min_weeks)){
						echo strtotime($purchases_order_data->mach_deli_min_weeks_date) != 0?date('d/m/Y',strtotime($purchases_order_data->mach_deli_min_weeks_date)):date('d/m/Y');
					} ?>
                </td>
			</tr>
			<tr>
                <td class="text-left text-bold" colspan="<?=$column_cnt - 8 ;?>"><b>Maximum</b> :</td>
                <td class="text-left text-bold" colspan="<?=$column_cnt - 8 ;?>">
                    &nbsp; <?=$purchases_order_data->mach_deli_max_weeks?>&nbsp;&nbsp; Days &nbsp;&nbsp;
                </td>
                <td class="text-left text-bold" colspan="<?=$column_cnt - 8 ;?>">
					<?php if(!empty($purchases_order_data->mach_deli_max_weeks)){
						echo strtotime($purchases_order_data->mach_deli_max_weeks_date) != 0?date('d/m/Y',strtotime($purchases_order_data->mach_deli_max_weeks_date)):date('d/m/Y');
					} ?>
                </td>
			</tr>
			<tr>
				<td class="no-border-bottom text-center text-bold" colspan="<?=$column_cnt - 4 ;?>">&nbsp;</td>
			</tr>
			<tr>
				<td class="no-border-top text-center text-bold" colspan="<?=$column_cnt - 4 ;?>">&nbsp;</td>
			</tr>
			<tr>
				<td class="no-border-top text-center text-bold" colspan="<?=$column_cnt - 4 ;?>">&nbsp;</td>
			</tr>
			<tr>
				<td class="text-center text-bold" width="30px">Sr.No.</td>
                <td class="text-center text-bold" colspan="<?=$column_cnt - 8 ;?>" >Item Name</td>
                <td class="text-center text-bold" width="20px">Item Code</td>
				<td class="text-center text-bold" width="50px" >HSN</td>
				<td class="text-center text-bold" width="70px" >Qty</td>
				<!--<td class="text-center text-bold" width="70px" >Rate: <?=$CURRENCY?></td>-->
				<td class="text-center text-bold" width="50px" >Rate:</td>
				<td class="text-center text-bold" width="50px" >Disc</td>
				<td class="text-center text-bold" width="50px" >GST</td>
				<td class="text-center text-bold" width="80px" >Amount:</td>
				<!--<td class="text-center text-bold" width="80px" >Amount: <?=$CURRENCY?></td>-->
			</tr>
			<?php
				$sub_total = 0;
				$discount_total = 0;
				$second_sub_total = 0;
				$gst_total = 0;
				if(!empty($purchases_order_data->round_off)){
					$round_off = $purchases_order_data->round_off;
				} else {
					$round_off = 0;
				}
				$grand_total = 0;
				if (!empty($item_array)) {
					$i_inc = 1;
					foreach ($item_array as $key => $item_row) {
            ?>
                    <?php if($i_inc == 29 || $i_inc == 41 || $i_inc == 81 || $i_inc == 121 || $i_inc == 141 || $i_inc == 181){ // For Bleak Page ?>
                    </table>
                    <table style="page-break-inside: avoid;">
                        <tr>
                            <td class="text-center text-bold" width="30px">Sr.No.</td>
                            <td class="text-center text-bold" colspan="<?=$column_cnt - 8 ;?>" >Item Name</td>
                            <td class="text-center text-bold" width="20px">Item Code</td>
                            <td class="text-center text-bold" width="50px" >HSN</td>
                            <td class="text-center text-bold" width="70px" >Qty</td>
                            <!--<td class="text-center text-bold" width="70px" >Rate: <?=$CURRENCY?></td>-->
                            <td class="text-center text-bold" width="50px" >Rate:</td>
                            <td class="text-center text-bold" width="50px" >Disc</td>
                            <td class="text-center text-bold" width="50px" >GST</td>
                            <td class="text-center text-bold" width="80px" >Amount:</td>
                            <!--<td class="text-center text-bold" width="80px" >Amount: <?=$CURRENCY?></td>-->
                        </tr>
                    <?php } ?>
						<tr>
							<td class="text-center" ><?=$i_inc?></td>
                            <td class="text-left" colspan="<?=$column_cnt - 8 ;?>"><?=$item_row->item_name?></td>
                            <td class="text-center"><?=$item_row->item_code?></td>
							<td class="text-center" ><?=$item_row->hsn?></td>
                            <td class="text-center" style="white-space: nowrap" >
                                <?=$item_row->quantity.' '.$item_row->required_uom?><br />
                                <?=(isset($item_row->reference_qty) && !empty($item_row->reference_qty)) ? '( x '.$item_row->reference_qty. ' = '. $item_row->quantity * $item_row->reference_qty .' '.$item_row->uom.')' : ''; ?>
                            </td>
							<td class="text-right" ><?=moneyformat($item_row->item_rate)?></td>
							<td class="text-center" ><?=$item_row->disc_per?> %</td>
                            <td class="text-center" ><?=$item_row->cgst + $item_row->sgst + $item_row->igst ?> %</td>
							<!--<td class="text-center" ><?=$item_row->sgst?> %</td>
							<td class="text-center" ><?=$item_row->igst?> %</td>-->
							<td class="text-right" ><?=moneyformat($item_row->net_amount)?></td>
						</tr>
			<?php
						$i_inc++;
						$discount_total += $item_row->disc_value;
						$sub_total += $item_row->amount;
//						$sub_total += ($item_row->quantity * $item_row->item_rate * $item_row->reference_qty);
						$gst_total += ($item_row->cgst_amount + $item_row->sgst_amount + $item_row->igst_amount);
					}
				}
				$second_sub_total = $sub_total - $discount_total;
				$grand_total = $second_sub_total + $gst_total + $round_off;
			?>
            
			<tr>
                <td valign='top' class="text-left text-bold" rowspan="6" colspan="<?=$column_cnt - 4 ;?>">Notes : <?= nl2br($purchases_order_data->note); ?></td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>">Sub Total</td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>"> <?=moneyformat($sub_total)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>">Discount</td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>"><?=moneyformat($discount_total)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>">Sub Total</td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>"><?=moneyformat($second_sub_total)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>">GST</td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>"><?=moneyformat($gst_total)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>">Round Off</td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>"><?=moneyformat($round_off)?></td>
			</tr>
			<tr>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>">Grand Total</td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>"><?=moneyformat($grand_total)?></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 2 ;?>"><b>Order Value In <?=$CURRENCY?> : <?=money_to_word($grand_total)?> only.</b></td>
				<td class="text-right text-bold" colspan="<?=$column_cnt - 8 ;?>"><b><?=$CURRENCY?> <?=moneyformat($grand_total)?></b></td>
			</tr>
			<tr>
				<td class="text-bold " colspan="<?=$column_cnt;?>"> 
                    <div>
                        <b>Terms And Conditions</b><br />
                        <?=isset($purchases_order_data->terms_conditions) ? $purchases_order_data->terms_conditions : '' ?>
                    </div>
                </td>
			</tr>
			<tr>
				<td colspan="<?=$column_cnt;?>">
					<table class="no-border">
						<tr class="no-border">
							<td class="no-border-top footer-detail-area" align="right" style="border-left: 0!important;">
								<strong><?=$purchases_order_data->contact_person_name?></strong>
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
							<td class="no-border-top footer-detail-area" align="right">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
							<td class="no-border-top footer-detail-area" align="right">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
						</tr>
						<tr class="no-border">
							<td class="no-border footer-sign-area text-bold no-border-left no-border-top no-border-bottom">
                                <?=$purchases_order_data->created_name?><br>
                                Order Prepare By
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
								<?=$purchases_order_data->approved_by_name; ?><br>
                                Approver
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
								Authorize
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<i class="fa fa-angle-down"></i>
