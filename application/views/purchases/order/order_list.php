<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Purchase : View Orders</small>
			<?php
			$order_add_role = $this->app_model->have_access_role(PURCHASE_ORDER_MODULE_ID, "add"); 
			if($order_add_role): ?>
				<a href="<?=base_url('purchase_order/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Order</a>
			<?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                <!-- Start Advance Search -->
				<div class="col-md-6">
					<a href="javascript:void(0)" class="btn btn-info btn-sm filtration_div"> Advanced Search </a>
					<div class="filtration_fields" id="filtration_fields">
						<br>
						<div class="col-md-12">
							<div class="row">
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='1'> Order Number</a> -
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='2'> Supplier Name</a> -
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='4'> Created At</a> 
							</div>	
						</div>
						<br><br>
						<table cellpadding="3" cellspacing="0" border="0" style="width:100%;">
							<thead>
								<tr>
									<th>Target</th>
									<th style="text-align:center">Search text</th>
								</tr>
							</thead>
							<tbody>
								<tr id="filter_global">
									<td>Global search</td>
									<td align="center"><input type="text" class="form-control input-sm global_filter" id="global_filter"></td>
								</tr>
								<tr id="filter_col2" data-column="1">
									<td>Column - Order Number</td>
									<td align="center"><input type="text" class="form-control input-sm column_filter" id="col1_filter"></td>
								</tr>
								<tr id="filter_col4" data-column="2">
									<td>Column - Supplier Name</td>
									<!--<td align="center"><input type="text" class="column_filter" id="col3_filter"></td>-->
									<td align="center">
										<select name="supplier_name" id="supplier_name1" class="form-control input-sm" onchange='filterSuppliername()'>
										</select>
									</td>
								</tr>
								<tr id="filter_col6" data-column="4">
									<td>Column - Created At</td>
									<td align="center"><input type="text" class="form-control input-sm column_filter" id="col5_filter"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="clearfix"></div>
				<br />
				<!--End Advance Search-->
                    <form action="" method="POST">

                        <div class="row">
                            <?php 
                            	$current_status = 'all';
								/*if($this->input->get_post("status")){ 
									$current_status = $this->input->get_post("status"); 
								} else {
									$current_status = 'pending';
								}*/
                                if($this->input->get_post("user_id")){ 
									$current_staff = $this->input->get_post("user_id"); 
								} else {
									$current_staff = $this->session->userdata('is_logged_in')['staff_id'];
								}
							?>
                            <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
								<div class="col-md-4">
									Search by User:
									<select class="form-control" name="user_id" id="user_id">
										<option value="all" <?php if($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
										<?php

										if (!empty($users)) {
											foreach ($users as $client) {
												if (trim($client->name) != "") {
													//$selected = $current_staff == $client->staff_id ? 'selected' : '';
													$selected = '';
													echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
												}
											}
										}
										?>
									</select>
								</div>
                            <?php } ?>
                            <div class="col-md-4"><br/>
                                <!-- <button class="btn btn-info" type="submit">Search</button> -->
                                &nbsp;
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>

                    </form>
                    <div class="table-responsive" style="overflow-x:hidden">
                        <?php
                        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
                        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");
                        ?>
                        <table id="orders_list_table" class="table custom-table table-striped">
                            <thead>
                            <tr>
                                <th width="150px">Action</th>
                                <th width="120px">Order No.</th>
                                <th>Supplier Name</th>
                                <th>Order Date</th>
                                <th>Created At</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function() {
        $("#user_id").select2().each(function() {
        var selectedValue = $(this).val();
        $(this).html($("option", $(this)).sort(function(a, b) {
            return a.text < b.text ? -1 : 1
        }));
        $(this).val(selectedValue);
    });
	    initAjaxSelect2($("#supplier_name1"),"<?=base_url('app/supplier_select2_source')?>");
	    $('#filtration_fields').hide();
		$(document).on('click', '.filtration_div', function(){
			$(".filtration_fields").toggle(500);
		});
        table = $('#orders_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('purchase_order/orders_datatable')?>",
                "type": "POST",
                "data": function(d){
                    d.user_id = $("#user_id").val();
                }, 
            },
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
			}
		});
	});
	
	$(document).on("change", "#user_id", function(){        
        table.draw();
    });
	
	$('input.global_filter').on( 'keyup click', function () {
			filterGlobal();
	} );
	$('input.column_filter').on( 'keyup click', function () {
		filterColumn( $(this).parents('tr').attr('data-column') );
	} );
	$('.showhideColumn').on('click', function(){
		var tbl_column = table.column($(this).attr('data-columnindex'));
		tbl_column.visible(!tbl_column.visible());
	});
	function filterGlobal () {
		$('#orders_list_table').DataTable().search(
			$('#global_filter').val()
		).draw();
	}

	function filterColumn ( i ) {
		$('#orders_list_table').DataTable().column( i ).search(
			$('#col'+i+'_filter').val()
		).draw();
	}
	function filterSuppliername () {
		$('#orders_list_table').DataTable().column(2).search(
			$("#supplier_name1 option:selected").text()
		).draw();
	}
	
</script>
