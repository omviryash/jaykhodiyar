<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('purchase_order/save_purchase_order') ?>" method="post" id="save_purchase_order" novalidate>
        <?php if(isset($purchase_order_data->order_id) && !empty($purchase_order_data->order_id)){ ?>
            <input type="hidden" name="purchase_order_data[order_id]" id="order_id" value="<?=$purchase_order_data->order_id?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> 
                <small class="text-primary text-bold">Purchase Order <span id="partyname"></span></small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, Ctrl+S = Save</label></small>
                <?php if(isset($purchase_order_data->order_id) && !empty($purchase_order_data->order_id)){ ?>
                <?php if(isset($purchase_order_data->is_approved) && $purchase_order_data->is_approved == 1){ ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_PURCHASE_ORDER,"allow")){ ?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;">Disapprove</button>
                <?php } else { ?>
                    <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                <?php } ?>
                <?php } else { ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_PURCHASE_ORDER,"allow")){ ?>
                        <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                    <?php } else { ?>
                        <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                    <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
                    $purchase_order_view_role= $this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"view");
                    $purchase_order_add_role= $this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"add");
                    $purchase_order_edit_role= $this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"edit");
                ?>
                <?php if($purchase_order_add_role || $purchase_order_edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($purchase_order_view_role): ?>
                    <a href="<?= base_url()?>purchase_order/order_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Order List</a>
                <?php endif;?>
                <?php if($purchase_order_add_role): ?>
                    <a href="<?= base_url()?>purchase_order/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Order</a>
                <?php endif;?>
            </h1>
        </section>
        <div class="clearfix">
        <?php if($purchase_order_add_role || $purchase_order_edit_role): ?>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Purchase Order Details</a></li>
                        <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                        <li><a href="#tab_3" data-toggle="tab" id="tabs_3">P.O. General</a></li>
                        <?php if(isset($purchase_order_data->order_id) && !empty($purchase_order_data->order_id)){ ?>
                        <li><a href="#tab_4" data-toggle="tab" id="tabs_4">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Purchase Order Details</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="order_no" class="col-sm-3 input-sm">Purchase Order No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="purchase_order_data[order_no]" id="order_no" class="form-control input-sm" value="<?=(isset($purchase_order_data->order_no))? $purchase_order_data->order_no : $order_no; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="order_date" class="col-sm-3 input-sm">Purchase Order Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="purchase_order_data[order_date]" id="order_date" class="form-control input-sm pull-right input-datepicker" value="<?=(isset($purchase_order_data->order_date))? date('d-m-Y', strtotime($purchase_order_data->order_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="purchase_quote_ref" class="col-sm-3 input-sm">Purchase Quote Ref.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="purchase_order_data[purchase_quote_ref]" id="purchase_quote_ref" class="form-control input-sm" value="<?=(isset($purchase_order_data->purchase_quote_ref))? $purchase_order_data->purchase_quote_ref : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="round_off" class="col-sm-3 input-sm">Round Off</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="purchase_order_data[round_off]" id="round_off" class="form-control input-sm" value="<?=(isset($purchase_order_data->round_off))? $purchase_order_data->round_off : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="note" class="col-sm-3 input-sm">Notes</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="purchase_order_data[note]" id="note" rows="2" ><?=(isset($purchase_order_data->note))? $purchase_order_data->note : ''; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Supplier Details</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-6">
												<div class="form-group">
													<label for="supplier_id" class="col-sm-3 input-sm">Supplier Name<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
                                                        <select name="purchase_order_data[supplier_id]" id="supplier_id" class="form-control input-sm" onchange="supplier_details(this.value)"></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<input type="hidden" name="supplier[supplier_name]" id="supplier_name" class="form-control input-sm supplier_name" placeholder="">
												<input type="hidden" name="supplier[supplier_id]" id="supplier_supplier_id" class="form-control input-sm supplier_supplier_id" placeholder="">
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
                                                        <textarea name="supplier[address]" id="address" class="form-control" rows="2" disabled="" ></textarea>
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="city" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="supplier[city_id]" id="city" class="form-control input-sm select2 city" disabled="" ></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" name="supplier[pincode]" id="pincode" class="form-control input-sm pincode" placeholder="Pin Code" disabled="" >
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="state" class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="supplier[state_id]" id="state" class="form-control input-sm select2" disabled="" ></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="country" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="supplier[country_id]" id="country" class="form-control input-sm select2" disabled="" ></select>
													</div>
												</div>
												<div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_no" class="col-sm-3 input-sm">Contact No.</label>
                                                    <div class="col-sm-9">
                                                        <textarea type="text" class="form-control " name="supplier[contact_no]" id="contact_no" placeholder="" disabled="" ></textarea>
                                                        <small class="">Add multiple Contact number by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
											<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="tel_no" class="col-sm-3  input-sm">Tel No.</label>
                                                    <div class="col-sm-9">                                                        
                                                        <textarea class="form-control tel_no" id="tel_no" name="supplier[tel_no]" placeholder="" disabled=""></textarea>
                                                        <small class="">Add multiple telephone number by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
												<div class="form-group">
													<label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea name="supplier[email_id]" id="email_id" class="form-control email_id" disabled="" ></textarea>
														<small>Add multiple email id by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="cont_person" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9">
														<input type="text" name="supplier[cont_person]" id="cont_person" class="form-control input-sm " placeholder="" disabled="" >
													</div>
												</div>
											</div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="item_id" class="col-md-2 text-right"><strong>Select Project</strong></label>
                                        <div class="col-md-3">
                                            <select name="purchase_order_data[purchase_project_id]" id="purchase_project_id" class="form-control input-sm purchase_project_id" onchange=""></select>
                                        </div>
                                        <label class="col-md-1 text-right"> Project Set</label>
                                        <div class="col-md-1">
                                            <input type="text" name="purchase_order_data[purchase_project_qty]" id="purchase_project_qty" class="form-control input-sm num_only" placeholder="Qty" value="<?=(isset($purchase_order_data->purchase_project_qty))? $purchase_order_data->purchase_project_qty : '1'; ?>">
                                        </div>
                                        <label class="col-md-2 text-right">Select Item Group</label>
                                        <div class="col-md-3">
                                            <select name="purchase_order_data[item_group_id]" id="item_group_id" class="form-control input-sm item_group_id" onchange="get_item_from_group(this)"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <span class="text-blue"><small>On Project Or Qty Change, Below Item list will be replace by Selected Project Or Qty, So be careful.</small></span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($purchase_order_lineitems)){ ?>
                                            <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="item_id" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id" onchange="purchase_item_details(this.value)"></select>
                                                </div>                                                       
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_code]" id="item_code" class="form-control input-sm item_code" data-name="item_code" readonly="readonly">
                                                    <input type="hidden" class="item_name" id="item_name" name="line_items_data[item_name]" data-name="item_name" placeholder="">
                                                    <input type="hidden" class="item_make_id" id="item_make_id" name="line_items_data[item_make_id]" data-name="item_make_id"  onchange="get_item_make_name(this.value)">
                                                    <input type="hidden" name="line_items_data[item_make_name]" id="item_make_name" class="item_make_name" data-name="item_make_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_description]" id="item_description" class="form-control input-sm item_description" data-name="item_description" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity" class="col-sm-3 input-sm">Req. (Order) Qty<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="hidden" name="line_items_data[set_qty]" id="set_qty" class="form-control input-sm set_qty num_only" value="" data-name="set_qty" placeholder="">
                                                    <input type="text" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity num_only" value="1" data-name="quantity" placeholder="">
                                                </div>
                                                <label for="uom_id" class="col-sm-2 input-sm">Req. UOM <span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-4">
                                                    <select name="line_items_data[uom_id]" id="uom_id" class="form-control input-sm uom_id" onchange="get_uom_name(this.value)" ></select>
                                                    <input type="hidden" name="line_items_data[uom_name]" id="uom_name" class="uom_name" data-name="uom_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_rate" class="col-sm-3 input-sm">
                                                    <span style="float: left;" >Rate<span class="required-sign">&nbsp;*</span></span>
                                                </label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate"  value="0" placeholder="">
                                                </div>
                                                <label for="item_rate" class="col-sm-2 input-sm">For 1 UOM<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-4">
                                                    <select name="line_items_data[rate_uom]" id="rate_uom" class="form-control input-sm rate_uom" onchange="get_rate_uom_name(this.value)" ></select>
                                                    <input type="hidden" name="line_items_data[rate_uom_name]" id="rate_uom_name" class="rate_uom_name" data-name="rate_uom_name" placeholder="">
                                                    <!--<input type="text" name="line_items_data[rate_uom]" id="rate_uom" class="form-control input-sm rate_uom" data-name="rate_uom"  value="0" placeholder="">-->
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_rate" class="col-sm-3 input-sm">Relation</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[reference_qty]" id="reference_qty" class="form-control input-sm reference_qty" readonly="" data-name="reference_qty">
                                                </div>
                                                <label for="item_rate" class="col-sm-2 input-sm"><span id="selected_rate_uom"></span> For 1 <span id="selected_required_uom"></span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="disc_per" class="col-sm-3 input-sm">Disc(%)</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[disc_per]" id="disc_per" class="form-control input-sm disc_per num_only" data-input_name="disc_per" placeholder="">
                                                </div>
                                                <label for="disc_value" class="col-sm-2 input-sm">Disc(Value)</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[disc_value]" id="disc_value" class="form-control input-sm disc_value num_only" data-input_name="disc_value" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="igst" class="col-sm-3 input-sm">IGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[igst]" id="igst" class="form-control input-sm igst item_data" data-input_name="igst" placeholder="">
                                                </div>
                                                <label for="igst_amount" class="col-sm-2 input-sm">IGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[igst_amount]" id="igst_amount" class="form-control input-sm igst_amount item_data" readonly data-input_name="igst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="cgst" class="col-sm-3 input-sm">CGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[cgst]" id="cgst" class="form-control input-sm cgst numberwithoutzero item_data" data-input_name="cgst" placeholder="">
                                                </div>
                                                <label for="cgst_amount" class="col-sm-2 input-sm">CGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[cgst_amount]" id="cgst_amount" class="form-control input-sm cgst_amount numberwithoutzero item_data" readonly data-input_name="cgst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="sgst" class="col-sm-3 input-sm">SGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[sgst]" id="sgst" class="form-control input-sm sgst item_data" data-input_name="sgst" placeholder="">
                                                </div>
                                                <label for="sgst_amount" class="col-sm-2 input-sm">SGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[sgst_amount]" id="sgst_amount" class="form-control input-sm sgst_amount item_data" readonly data-input_name="sgst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                </div>
                                                <label for="net_amount" class="col-sm-2 input-sm">Net Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[net_amount]" id="net_amount" class="form-control input-sm net_amount disabled" data-input_name="net_amount" placeholder="" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <input type="hidden" name="line_items_data[po_reference_qty]" id="po_reference_qty" class="form-control input-sm po_reference_qty"  data-name="po_reference_qty">
                                            <input type="hidden" name="line_items_data[po_reference_qty_uom]" id="po_reference_qty_uom" class="form-control input-sm po_reference_qty_uom"  data-name="po_reference_qty_uom" onchange="get_ref_uom_name(this.value)">
                                            <input type="hidden" name="line_items_data[po_reference_qty_uom_name]" id="po_reference_qty_uom_name" class="po_reference_qty_uom_name" data-name="po_reference_qty_uom_name" placeholder="">
                                        </div>
                                        <div class="col-sm-12"><div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" <?=$this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "add") ? '' : 'disabled'?>/>
                                            </div>
                                        </div></div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <table style="" class="table custom-table item-table">
                                        <thead>
                                            <tr>
                                                <th width="55px">Action</th>
                                                <th>Item Name</th>
                                                <th>Item Code</th>
                                                <th>Item Make</th>
                                                <th class="text-right">Basic Qty</th>
                                                <th class="text-right">Order Qty</th>
                                                <th class="text-center">Required UOM</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-center">For 1 UOM</th>
                                                <th class="text-center">Relation</th>
                                                <th class="text-right">Amount</th>
                                                <th class="text-right">Disc(value)</th>
                                                <th class="text-right">IGST %</th>
                                                <th class="text-right">CGST %</th>
                                                <th class="text-right">SGST %</th>
                                                <th class="text-right">Net Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lineitem_list"></tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th class="text-right"><span class="set_qty_total"></span><input type="hidden" name="set_qty_total" id="set_qty_total" /></th>
                                                <th class="text-right"><span class="qty_total"></span><input type="hidden" name="qty_total" id="qty_total" /></th>
                                                <th class="text-center"></th>
                                                <th class="text-right"></th>
                                                <th class="text-center"></th>
                                                <th class="text-center"></th>
                                                <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                                <th class="text-right" class="text-right"><span class="disc_value_total"></span><input type="hidden" name="disc_value_total" id="disc_value_total" /></th>
                                                <th class="text-right" class="text-right"><span class="igst_value_total"></span></th>
                                                <th class="text-right" class="text-right"><span class="cgst_value_total"></span></th>
                                                <th class="text-right" class="text-right"><span class="sgst_value_total"></span></th>
                                                <th class="text-right" class="text-right"><span class="net_amount_total"></span><input type="hidden" name="net_amount_total" id="net_amount_total" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Material Required Of Delivery Date </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-5 input-sm"> Minimum (Days)<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control input-sm" id="minimum" name="purchase_order_data[mach_deli_min_weeks]" placeholder="" value="<?=(isset($purchase_order_data->mach_deli_min_weeks))? $purchase_order_data->mach_deli_min_weeks : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-5 input-sm"> Maximum (Days)<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control input-sm" id="maximum" name="purchase_order_data[mach_deli_max_weeks]" placeholder="" value="<?=(isset($purchase_order_data->mach_deli_max_weeks))? $purchase_order_data->mach_deli_max_weeks : ''; ?>" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm" id="minimum_weeks" name="purchase_order_data[mach_deli_min_weeks_date]" placeholder="" value="<?=(isset($purchase_order_data->mach_deli_min_weeks_date))? date('d-m-Y', strtotime($purchase_order_data->mach_deli_min_weeks_date)) : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control input-sm approved_date_time" id="maximum_weeks" name="purchase_order_data[mach_deli_max_weeks_date]" placeholder="" value="<?=(isset($purchase_order_data->mach_deli_max_weeks_date))? date('d-m-Y', strtotime($purchase_order_data->mach_deli_max_weeks_date)) : ''; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Terms & Conditions</legend>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6" style="margin-bottom: 10px;">
                                            <div class="form-group">
                                                <label for="terms_conditions_type" class="col-sm-3 text-right">Select Type<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <select name="purchase_order_data[terms_conditions_type]" id="terms_conditions_type" class="form-control select2" style="width: 400px;">
                                                        <option value=""> - Select Type - </option>
                                                        <option value="1" <?=(isset($purchase_order_data->terms_conditions_type) && $purchase_order_data->terms_conditions_type == '1')? ' Selected ' : ''; ?> >Purchase Order General Terms</option>
                                                        <option value="2" <?=(isset($purchase_order_data->terms_conditions_type) && $purchase_order_data->terms_conditions_type == '2')? ' Selected ' : ''; ?> >Purchase Order Electrict Components Terms</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea id="terms_conditions" class="form-control" style="height: 100px" name="purchase_order_data[terms_conditions]">
                                                    <?=(isset($purchase_order_data->terms_conditions))? $purchase_order_data->terms_conditions : ''; ?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <?php if(isset($purchase_order_data->order_id) && !empty($purchase_order_data->order_id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=(isset($purchase_order_data->created_by_name))? $purchase_order_data->created_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=(isset($purchase_order_data->created_at))? $purchase_order_data->created_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=(isset($purchase_order_data->updated_by_name))? $purchase_order_data->updated_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=(isset($purchase_order_data->updated_at))? $purchase_order_data->updated_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Approved By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_by" id="approved_by" value="<?=(isset($purchase_order_data->approved_by_name))? $purchase_order_data->approved_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_date_time" id="approved_date_time" value="<?=(isset($purchase_order_data->approved_at))? $purchase_order_data->approved_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        <?php endif;?>
        </div>
        <section class="content-header">
            <?php if(isset($purchase_order_data->order_id) && !empty($purchase_order_data->order_id)){ ?>
            <?php if(isset($purchase_order_data->is_approved) && $purchase_order_data->is_approved == 1){ ?>
            <?php if($this->applib->have_access_role(CAN_APPROVE_PURCHASE_ORDER,"allow")){ ?>
                <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;">Disapprove</button>
            <?php } else { ?>
            <h1> <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small></h1>
            <?php } ?>
            <?php } else { ?>
            <?php if($this->applib->have_access_role(CAN_APPROVE_PURCHASE_ORDER,"allow")){ ?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                <?php } else { ?>
            <h1><small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small></h1>
                <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php
                $purchase_order_view_role= $this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"view");
                $purchase_order_add_role= $this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID,"add");
            ?>
            <?php if($purchase_order_add_role || $purchase_order_edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
            <?php endif;?>
            <?php if($purchase_order_view_role): ?>
                <a href="<?= base_url()?>purchase_order/order_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Order List</a>
            <?php endif;?>
            <?php if($purchase_order_add_role): ?>
                <a href="<?= base_url()?>purchase_order/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Order</a>
            <?php endif;?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?=dist_url('js/sales_order.js');?>"></script>
<script type="text/javascript">
    var order_index = '';
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
    var lineitem_objectdata = [];
    <?php if(isset($purchase_order_lineitems)){ ?>
		var li_lineitem_objectdata = [<?php echo $purchase_order_lineitems; ?>];
		var lineitem_objectdata = [];
        if(li_lineitem_objectdata != ''){
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
	<?php } ?>
    display_lineitem_html(lineitem_objectdata);
    <?php if(isset($purchase_order_data->order_id) && !empty($purchase_order_data->order_id)){ ?>
        first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function(){
        $('.select2').select2();
        
        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
//        initAjaxSelect2($("#item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source')?>");
        initAjaxSelect2($("#supplier_id"),"<?=base_url('app/supplier_select2_source')?>");
        initAjaxSelect2($("#uom_id"),"<?=base_url('app/uom_select2_source')?>");
        initAjaxSelect2($("#rate_uom"),"<?=base_url('app/uom_select2_source')?>");
        initAjaxSelect2($("#item_group_id"),"<?=base_url('app/item_group_select2_source')?>");
        initAjaxSelect2($("#purchase_project_id"),"<?=base_url('app/purchase_project_select2_source')?>");
        <?php if(isset($purchase_order_data->supplier_id)){ ?>
			setSelect2Value($("#supplier_id"),"<?=base_url('app/set_supplier_select2_val_by_id/'.$purchase_order_data->supplier_id)?>");
            supplier_details(<?=$purchase_order_data->supplier_id; ?>);
		<?php } ?>
        <?php if(isset($purchase_order_data->purchase_project_id)){ ?>
			setSelect2Value($("#purchase_project_id"),"<?=base_url('app/set_purchase_project_select2_val_by_id/'.$purchase_order_data->purchase_project_id)?>");
//            get_purchase_project_items();
		<?php } ?>
        
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g ); 
        CKEDITOR.replace('terms_conditions',{
            removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        $(document).on('change', '#terms_conditions_type', function () {
            var terms_conditions_type = $('#terms_conditions_type').val();
            if(terms_conditions_type == '1'){
               terms_conditions_type = 'purchase_order_general_terms';
            } else if(terms_conditions_type == '2'){
               terms_conditions_type = 'purchase_order_electrict_components_terms';
            }
            if(terms_conditions_type != ''){
                $.ajax({
                    url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                    type: "POST",
                    data: {module: terms_conditions_type},
                    dataType: 'json',
                    success: function (data) {
                        CKEDITOR.instances['terms_conditions'].setData( data.terms_and_conditions );
                    }
                });
            } else {
                CKEDITOR.instances['terms_conditions'].setData('');
            }
        });
        
        $(document).on('change', '#purchase_project_id', function () {
            get_purchase_project_items();
        });
        $(document).on('input', '#purchase_project_qty', function () {
            get_purchase_project_items();
        });
        
        $(document).on('input', '#quantity', function () {
            get_po_reference_qty();
        });

        $(document).on('input','#item_rate',function () {
            if(this.value > 9900000){
                this.value = 9900000;
            }
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_purchase_order").submit();
                return false;
            }
        });
        
        $(document).on('submit', '#save_purchase_order', function () {
            if($.trim($("#supplier_id").val()) == ''){
                show_notify('Please Select Supplier.',false);
                return false;
            }
            if($.trim($("#minimum").val()) == '' || $.trim($("#minimum").val()) == 0){
                show_notify('Please Enter Delivery Schedule Minimum (Days).',false);
                return false;
            }
            if($.trim($("#maximum").val()) == '' || $.trim($("#maximum").val()) == 0){
                show_notify('Please Enter Delivery Schedule Maximum (Days).',false);
                return false;
            }
            if($.trim($("#terms_conditions_type").val()) == '' || $.trim($("#terms_conditions_type").val()) == 0){
                show_notify('Please select Terms & Condition Type.',false);
                return false;
            }
            if($.trim($("#item_id").val()) != ''){
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }
            if(lineitem_objectdata == ''){
				show_notify("Please select any one Item.", false);
				return false;
			}
            
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
			var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
			postData.append('line_items_data', lineitem_objectdata_stringify);
			$.ajax({
				url: "<?=base_url('purchase_order/save_purchase_order') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('purchase_order/order_list') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('purchase_order/order_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });

        $(document).on("click",".btn_approve_ord",function(){
            $("#ajax-loader").show();
            var order_id = $('#order_id').val();
            $.ajax({
                url: '<?=BASE_URL?>purchase_order/update_purchase_order_status_to_approved',
                type: "POST",
                data: {order_id:order_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        <?php if($this->applib->have_access_role(CAN_APPROVE_PURCHASE_ORDER,"allow")){?>
                        $(".btn_approve_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_disapprove_ord' style='margin: 5px;'>Disapprove</button>");
                        <?php }else{?>
                        $(".btn_approve_ord").after("<small class='text-primary text-bold pull-right app_lab' style='margin: 5px;'>Approved</small>");
                        <?php }?>
                        $('.btn_approve_ord').remove();
                        show_notify(data.message, true);
                    }
                    else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on("click",".btn_disapprove_ord",function(){
            $("#ajax-loader").show();
            var order_id = $('#order_id').val();
            $.ajax({
                url: '<?=BASE_URL?>purchase_order/update_purchase_order_status_to_disapproved',
                type: "POST",
                data: {order_id:order_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $(".btn_disapprove_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_approve_ord' style='margin: 5px;'>Approve</button>");
                        $('.btn_disapprove_ord').remove();
                        show_notify(data.message, true);
                    }
                    else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if(target == '#tab_2'){
                $('#item_group_id').select2('open');
            } else {
                $('#item_group_id').select2('close');
            }
        });

        $('#add_lineitem').on('click', function() {
            get_po_reference_qty();
			var item_id = $("#item_id").val();
			if(item_id == '' || item_id == null){
				show_notify("Please select Item!", false);
                $("#item_id").focus();
				return false;
			}
			var quantity = $("#quantity").val();
			if(quantity == '' || quantity == null){
				show_notify("Item quantity is required!", false);
                $("#quantity").focus();
				return false;
			}
            var uom_id = $("#uom_id").val();
			if(uom_id == '' || uom_id == null){
				show_notify("Req. UOM is required!", false);
                $("#uom_id").select2('open');
				return false;
			}
            var item_rate = $("#item_rate").val();
			if(item_rate == '' || item_rate == null){
				show_notify("Item Rate is required!", false);
                $("#item_rate").focus();
				return false;
			}
            var rate_uom = $("#rate_uom").val();
			if(rate_uom == '' || rate_uom == null){
				show_notify("For 1 UOM is required!", false);
                $("#rate_uom").select2('open');
				return false;
			}
			
			var key = '';
			var value = '';
			var lineitem = {};
            var is_validate = '0';
			$('select[name^="line_items_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			$('input[name^="line_items_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
            $('select[name^="line_items_data"]').each(function (index) {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                
                $.each(lineitem_objectdata, function (index, value) {
                    if (value.item_id == item_id && typeof (value.id) != "undefined" && value.id !== null && value.id !== '') {
                        $('input[name^="line_items_data"]').each(function (index) {
                            keys = $(this).attr('name');
                            keys = keys.replace("line_items_data[", "");
                            keys = keys.replace("]", "");
                            if (keys == 'id') {
                                if (value.id != $(this).val()) {
                                    is_validate = '1';
                                    show_notify("You cannot Add this Item. This Item has been used!", false);
                                    return false;
                                }
                            }
                        });
                    } else if (value.item_id == item_id) {
//                        alert('2');
                        if(order_index !== index){
                            is_validate = '1';
                            show_notify("You cannot Add this Item. This Item has been used!", false);
                            return false;
                        }
                    }
                });
                if (is_validate == '1') {
                    return false;
                }
            });
            if (is_validate != '1') {
                var new_lineitem = JSON.parse(JSON.stringify(lineitem));
                var line_items_index = $("#line_items_index").val();
                if(line_items_index != ''){
                    lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
                } else {
                    lineitem_objectdata.push(new_lineitem);
                }
                display_lineitem_html(lineitem_objectdata);
                $('#lineitem_id').val('');
                $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
                $("#item_id").val(null).trigger("change");
                $("#uom_id").val(null).trigger("change");
                $("#rate_uom").val(null).trigger("change");
                $("#set_qty").val('');
                $("#required_qty").val('');
                $("#quantity").val('1');
                $("#disc_per").val('0');
                $("#line_items_index").val('');
                if(on_save_add_edit_item == 1){
                    on_save_add_edit_item == 0;
                    $('#save_purchase_order').submit();
                }			
                if(edit_lineitem_inc == 1 && <?=$this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "add")?> != 1){
                    $('.add_lineitem').attr("disabled", "disabled");
                }
                edit_lineitem_inc = 0;
                order_index = '';
                $('#item_id').select2('open');
            }
        });
        
        $('form').on('keydown', 'input,select', function (event) {
            if (event.which == 13) {
                event.preventDefault();
                var $this = $(event.target);
                var index = parseFloat($this.attr('data-index'));
                var input_name = $this.attr('data-input_name');
//                alert(input_name);
                if(input_name != '' && input_name != null && input_name != 'undefined'){
                    if(input_name == 'add_lineitem'){
                        $('#add_lineitem').click();
                        $('#item_id').select2('open');
                    } else if(input_name == 'undefined') {
                        $('[data-index="' + (index + 1).toString() + '"]').focus();
                    }
                } else {
                    $('[data-index="' + (index + 1).toString() + '"]').focus();
                }
            }
            if (event.which == 27) {
                event.preventDefault();
                var $this = $(event.target);
                var index = parseFloat($this.attr('data-index'));
                $('#pf_amount').focus();
//                    $('[data-index="' + (index + 1).toString() + '"]').focus();
            }
        });
        
        
        
    });
    
    function display_lineitem_html(lineitem_objectdata){
//    console.log(lineitem_objectdata);
        $('#ajax-loader').show();
		var new_lineitem_html = '';
		var set_qty_total = 0;
		var qty_total = 0;
		var po_reference_qty_total = 0;
		var amount_total = 0;
		var disc_value_total = 0;
		var net_amount_total = 0;
		var cgst_total = 0;
		var sgst_total = 0;
		var igst_total = 0;
		$.each(lineitem_objectdata, function (index, value) {
            var lineitem_edit_btn = '';
            var lineitem_delete_btn = '';
            if(value.po_reference_qty_uom_name != null){ 
                var reference_qty_uom_name = value.po_reference_qty_uom_name; 
            } else { 
                var reference_qty_uom_name = '';
            }
			<?php
			if($this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "edit")): ?>
			lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
			<?php endif;?>
			<?php if($this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "delete")): ?>
			lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
			<?php endif;?>
			var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
			lineitem_edit_btn +
			lineitem_delete_btn +
			'</td>' +
			'<td>' + value.item_name + '</td>' +
            '<td>' + value.item_code + '</td>' +
            '<td>' + value.item_make_name + '</td>' +
            '<td class="text-right">' + value.set_qty + '</td>' + 
            '<td class="text-right">' + value.quantity + '</td>'+
            '<td class="text-center">' + value.uom_name + '</td>'+
            '<td class="text-right">' + value.item_rate + '</td>'+
            '<td class="text-center">' + value.rate_uom_name + '</td>'+
            '<td class="text-center">' + value.reference_qty + ' ' + value.rate_uom_name + ' For 1 ' + value.uom_name + '</td>'+
            '<td class="text-right">' + value.amount + '</td>' +
            '<td class="text-right">' + value.disc_value + '</td>' +
            '<td class="text-right">' + value.igst + '</td>' +
            '<td class="text-right">' + value.cgst + '</td>' +
            '<td class="text-right">' + value.sgst + '</td>' +
            '<td class="text-right">' + value.net_amount + '</td></tr>';
			new_lineitem_html += row_html;
			set_qty_total += +value.set_qty;
			qty_total += +value.quantity;
			po_reference_qty_total += +value.po_reference_qty;
			amount_total += +value.amount;
			disc_value_total += +value.disc_value;
			net_amount_total += +value.net_amount;
			cgst_total += +value.cgst_amount;
			sgst_total += +value.sgst_amount;
			igst_total += +value.igst_amount;
		});
		$('tfoot span.set_qty_total').html(set_qty_total); $('#set_qty_total').val(set_qty_total);
		$('tfoot span.qty_total').html(qty_total); $('#qty_total').val(qty_total);
		$('tfoot span.po_reference_qty_total').html(po_reference_qty_total); $('#po_reference_qty_total').val(po_reference_qty_total);
		$('tfoot span.amount_total').html(amount_total); $('#amount_total').val(amount_total);
		$('tfoot span.disc_value_total').html(disc_value_total); $('#disc_value_total').val(disc_value_total);
        $('tfoot span.net_amount_total').html(net_amount_total.toFixed(2)); $('#net_amount_total').val(net_amount_total.toFixed(2));
        $('tfoot span.igst_value_total').html(igst_total.toFixed(2));
        $('tfoot span.cgst_value_total').html(cgst_total.toFixed(2));
        $('tfoot span.sgst_value_total').html(sgst_total.toFixed(2));
		$('tbody#lineitem_list').html(new_lineitem_html);
        $('#ajax-loader').hide();
	}

    function edit_lineitem(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('#ajax-loader').show();
        if(edit_lineitem_inc == 0){
			edit_lineitem_inc = 1;
			$(".add_lineitem").removeAttr("disabled");
		}
		value = lineitem_objectdata[index];
        order_index = index;
		$("#line_items_index").val(index);
        
        get_item_from_group();
		setSelect2Value($("#item_id"),"<?=base_url('app/set_purchase_item_select2_val_by_id/')?>/" + value.item_id);
		initAjaxSelect2($("#uom_id"),"<?=base_url('app/uom_select2_source/')?>");
		setSelect2Value($("#uom_id"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + value.uom_id);
        initAjaxSelect2($("#rate_uom"),"<?=base_url('app/uom_select2_source/')?>");
		setSelect2Value($("#rate_uom"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + value.rate_uom);
		if(typeof(value.id) != "undefined" && value.id !== null && value.id !== '') {
			$("#lineitem_id").val(value.id);
		}
        $("#item_name").val(value.item_name);
        $("#item_code").val(value.item_code);
        $("#item_make_id").val(value.item_make_id).change();
        $("#item_description").val(value.item_description);
        $("#item_rate").val(value.item_rate);
        $("#set_qty").val(value.set_qty);
        $("#quantity").val(value.quantity);
        $("#reference_qty").val(value.reference_qty);
//        $("#po_reference_qty_uom").val(value.po_reference_qty_uom);
        $("#amount").val(value.amount);
        $("#disc_per").val(value.disc_per);
        $("#disc_value").val(value.disc_value);
        $("#igst").val(value.igst);
        $("#cgst").val(value.cgst);
        $("#sgst").val(value.sgst);
        $("#igst_amount").val(value.igst_amount);
        $("#cgst_amount").val(value.cgst_amount);
        $("#sgst_amount").val(value.sgst_amount);
        $("#net_amount").val(value.net_amount);
        $("#line_items_index").val(index);
		$('#ajax-loader').hide();
	}
	
	function remove_lineitem(index){
        if(confirm('Are you sure ?')){
            value = lineitem_objectdata[index];
            if(typeof(value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index,1);
            display_lineitem_html(lineitem_objectdata);
        }
	}
    
    function get_po_reference_qty(){
        var quantity = $('#quantity').val();
        var reference_qty = $('#reference_qty').val();
        if(quantity != '' && quantity != null && reference_qty != '' && reference_qty != null){
            var po_reference_qty = parseInt(quantity) * parseInt(reference_qty);
            $('#po_reference_qty').val(po_reference_qty);
        }
    }
    
    function supplier_details(id){
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>purchase_order/ajax_load_supplier/'+id,
            async: false,
            data: id='supplier_id',
            success: function(data){
                var json = $.parseJSON(data);
                if (json['address']) {
                    $("#address").val(json['address']);
                }else{
                    $("#address").val("");
                }
                if (json['supplier_name']) {
                    $('#supplier_name').val(json['supplier_name']);
                }
                if (json['supplier_id']) {
                    $("#supplier_id").val(json['supplier_id']);
                    $("#supplier_supplier_id").val(json['supplier_id']);
                }
                if (json['city_id']) {
                    setSelect2Value($("#city"),'<?=base_url()?>app/set_city_select2_val_by_id/'+json['city_id']);
                }else{
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                }else{
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                }else{
                    setSelect2Value($("#country"));
                }
                if (json['tel_no']) {
                    $("#tel_no").val(json['tel_no']);
                }else{
                    $("#tel_no").val("");
                }
                if (json['contact_no']) {
                    $("#contact_no").val(json['contact_no']);
                }else{
                    $("#contact_no").val("");
                }
                if (json['email_id']) {
                    $("#email_id").val(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }else{
                    $("#email_id").val("");
                }
                if (json['pincode']) {
                    $("#pincode").val(json['pincode']);
                }else{
                    $("#pincode").val("");
                }
                if (json['cont_person']) {
                    $("#cont_person").val(json['cont_person']);
                }else{
                    $("#cont_person").val("");
                }
                $("#ajax-loader").hide();
            }
        });
    }
    
    function purchase_item_details(id){
        if(edit_lineitem_inc == 0){
            if(id != '') {
                if($.trim($("#supplier_id").val()) == ''){
                    show_notify('Please Select Supplier. <br /> Because, On Supplier State We will decide which GST we will use : IGST or CGST & SGST.',false);
                    $("#item_id").val(null).trigger("change");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>purchase_order/ajax_load_purchase_item_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
//                        console.log(data);
                        var json = $.parseJSON(data);
                        if (json['item_name']) {
                            $(".item_name").val(json['item_name']);
                        } else {
                            $(".item_name").val("");
                        }

                        if (json['item_code']) {
                            $("#item_code").val(json['item_code']);
                        } else {
                            $("#itm_code").val('');
                        }
                        if (json['item_make_id']) {
                            $("#item_make_id").val(json['item_make_id']).change();;
                        } else {
                            $("#item_make_id").val('');
                        }
                        if (json['uom']) {
                            setSelect2Value($("#uom_id"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + json['uom']);
                        } else {
                            $("#uom_id").val(null).trigger("change");
                        }
                        if (json['quantity']) {
                            $("#quantity").val(json['quantity']);
                        } else {
                            $("#quantity").val("");
                        }
                        if (json['quantity']) {
                            $("#set_qty").val(json['quantity']);
                        } else {
                            $("#set_qty").val("");
                        }
                        if (json['reference_qty']) {
                            $("#reference_qty").val(json['reference_qty']);
                        } else {
                            $("#reference_qty").val("");
                        }
                        if (json['reference_qty_uom']) {
                        setSelect2Value($("#po_reference_qty_uom"),"<?=base_url('app/set_uom_select2_val_by_id')?>/" + json['reference_qty_uom']);
                        } else {
                            $("#po_reference_qty_uom").val(null).trigger("change");
                        }

                        if (json['rate']) {
                            $("#item_rate").val(json['rate']);
                        } else {
                            $("#item_rate").val('');
                        }
                        if (json['rate_uom']) {
                            setSelect2Value($("#rate_uom"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + json['rate_uom']);
                        } else {
                            $("#rate_uom").val('');
                        }

                        $(".igst").val('0');
                        $(".cgst").val('0');
                        $(".sgst").val('0');
                        var state_id = $('#state').val();
                        if(state_id == null || state_id == '<?=GUJARAT_STATE_ID; ?>'){
                            if (json['cgst']) {
                                $(".cgst").val(json['cgst']);
                            } else {
                                $(".cgst").val('');
                            }
                            if (json['sgst']) {
                                $(".sgst").val(json['sgst']);
                            } else {
                                $(".sgst").val('');
                            }
                        } else {
                            if (json['igst']) {
                                $(".igst").val(json['igst']);
                            } else {
                                $(".igst").val('');
                            }
                        }
                        input_value_get_amount();
                    }
                });
            } else {
                $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
                $("#uom_id").val(null).trigger("change");
                $("#po_reference_qty_uom").val(null).trigger("change");
                $("#rate_uom").val(null).trigger("change");
            }
        } else {
            if(id != '') {
                if($.trim($("#supplier_id").val()) == ''){
                    show_notify('Please Select Supplier. <br /> Because, On Supplier State We will decide which GST we will use : IGST or CGST & SGST.',false);
                    $("#item_id").val(null).trigger("change");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>purchase_order/ajax_load_purchase_item_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
    //                    console.log(data);
                        var json = $.parseJSON(data);
//                        if (json['reference_qty']) {
//                            $("#reference_qty").val(json['reference_qty']);
//                        } else {
//                            $("#reference_qty").val("");
//                        }
                        if (json['reference_qty_uom']) {
                            setSelect2Value($("#po_reference_qty_uom"),"<?=base_url('app/set_uom_select2_val_by_id')?>/" + json['reference_qty_uom']);
                        } else {
                            $("#po_reference_qty_uom").val(null).trigger("change");
                        }
                    }
                });
            } else {
                $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
                $("#uom_id").val(null).trigger("change");
                $("#po_reference_qty_uom").val(null).trigger("change");
                $("#rate_uom").val(null).trigger("change");
            }
        }
    }
    
    function get_purchase_project_items(){
        var purchase_project_id = $('#purchase_project_id').val();
        var purchase_project_qty = $('#purchase_project_qty').val(); 
        if(purchase_project_id != '' && purchase_project_id != null){
            if($.trim($("#supplier_id").val()) == ''){
                show_notify('Please Select Supplier. <br /> Because, On Supplier State We will decide which GST we will use : IGST or CGST & SGST.',false);
                $("#purchase_project_id").val(null).trigger("change");
                return false;
            }
            var state_id = $('#state').val();
            $.ajax({
                type: "POST",
                url: '<?=base_url();?>purchase_order/get_purchase_project_items/' + purchase_project_id + '/' + purchase_project_qty + '/' + state_id,
                data: purchase_project_id = 'purchase_project_id',
                success: function (data) {
                    var json = $.parseJSON(data);
                    setSelect2Value($("#item_group_id"),"<?=base_url('app/set_item_group_select2_val_by_id')?>/" + json[0].item_group_id);
                    initAjaxSelect2($("#item_group_id"),"<?=base_url('app/item_group_select2_source')?>");
                    get_item_from_group();
                    $('#lineitem_id').val('');
                    $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
                    $("#item_id").val(null).trigger("change");
                    $("#uom_id").val(null).trigger("change");
                    $("#set_qty").val('');
                    $("#quantity").val('');
                    $("#disc_per").val('0');
                    $("#line_items_index").val('');
                    edit_lineitem_inc = 0;
                    
//                        console.log(data);
                    var data = $.parseJSON(data);
                    var li_lineitem_objectdata = data;
                    lineitem_objectdata = [];
                    if(li_lineitem_objectdata != ''){
                        $.each(li_lineitem_objectdata, function (index, value) {
                            value.item_id = parseInt(value.item_id);
                            lineitem_objectdata.push(value);
                        });
                    }
                    display_lineitem_html(lineitem_objectdata);
                }
            });
        } else {
            $('#item_id').val('').trigger('change');
            $('#item_group_id').val('').trigger('change');
            $('tfoot span.set_qty_total').html('0'); $('#set_qty_total').val('0');
            $('tfoot span.qty_total').html('0'); $('#qty_total').val('0');
            $('tfoot span.po_reference_qty_total').html('0'); $('#po_reference_qty_total').val('0');
            $('tfoot span.amount_total').html('0'); $('#amount_total').val('0');
            $('tfoot span.disc_value_total').html('0'); $('#disc_value_total').val('0');
            $('tfoot span.net_amount_total').html('0.00'); $('#net_amount_total').val('0.00');
            $('tbody#lineitem_list').html('');
        }
    }
    
    function get_uom_name(id){
        $.ajax({
            url: "<?=base_url('app/set_uom_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if(response.text == '--select--'){ response.text = ''; }
                $('#uom_name').val(response.text);
                $('#selected_required_uom').html(response.text);
                check_uom_and_rate_uom();
            },
        });
    }
    
    function get_rate_uom_name(id){
        $.ajax({
            url: "<?=base_url('app/set_uom_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if(response.text == '--select--'){ response.text = ''; }
                $('#rate_uom_name').val(response.text);
                $('#selected_rate_uom').html(response.text);
                check_uom_and_rate_uom();
            },
        });
    }
    
    function check_uom_and_rate_uom(){
        var uom_id = $("#uom_id").val();
        var rate_uom = $("#rate_uom").val();
        if(uom_id == '' || uom_id == null && rate_uom == '' || rate_uom == null){ } else {
            if(uom_id == rate_uom){
                $('#reference_qty').val(1);
            } else {
                var item_id = $("#item_id").val();
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>purchase_order/ajax_load_purchase_item_details/' + item_id,
                    async: false,
                    data: item_id = 'item_id',
                    success: function (data) {
                        var json = $.parseJSON(data);
                        if (json['reference_qty']) {
                            $("#reference_qty").val(json['reference_qty']);
                        } else {
                            $("#reference_qty").val("");
                        }
                    }
                });
            }
            input_value_get_amount();
        }
    }
    
    function get_item_make_name(id){
        $.ajax({
            url: "<?=base_url('app/set_challan_item_make_id_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if(response.text == '--select--'){ response.text = ''; }
                $('#item_make_name').val(response.text);
            },
        });
    }
    
    function get_ref_uom_name(id){
        $.ajax({
            url: "<?=base_url('app/set_uom_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if(response.text == '--select--'){ response.text = ''; }
                $('#po_reference_qty_uom_name').val(response.text);
            },
        });
    }
    
    function get_item_from_group(){
        var group_id = $('#item_group_id').val();
        $("#item_id").val(null).trigger('change');
        if(group_id != '' && group_id != null){
            initAjaxSelect2($("#item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source')?>/" + group_id);
        } else {
            initAjaxSelect2($("#item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source/-1')?>");
        }
    }
    
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
    <?php } ?>
        
        
</script>