<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('purchase_invoice/save_purchase_invoice') ?>" method="post" id="save_purchase_invoice" novalidate>
        <?php if(isset($purchase_invoice_data->invoice_id) && !empty($purchase_invoice_data->invoice_id)){ ?>
            <input type="hidden" name="purchase_invoice_data[invoice_id]" id="invoice_id" value="<?=$purchase_invoice_data->invoice_id?>">
            <input type="hidden" name="purchase_invoice_data[order_id]" id="order_id" value="<?=$purchase_invoice_data->order_id?>">
        <?php } else { ?>
            <input type="hidden" name="purchase_invoice_data[order_id]" id="order_id" >
        <?php } ?> 
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> 
                <small class="text-primary text-bold">Purchase Invoice <span id="partyname"></span></small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, Ctrl+S = Save</label></small>
                <?php
                    $view_role= $this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"view");
                    $add_role= $this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"add");
                    $edit_role= $this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID,"edit");
                ?>
                <?php if($add_role || $edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($view_role): ?>
                    <a href="<?= base_url()?>purchase_invoice/invoice_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Invoice List</a>
                <?php endif;?>
                <?php if($add_role): ?>
                    <a href="<?= base_url()?>purchase_invoice/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice</a>
                <?php endif;?>
            </h1>
        </section>
        <div class="clearfix">
        <?php if($add_role || $edit_role): ?>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Purchase Invoice Details</a></li>
                        <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                        <?php if(isset($purchase_invoice_data->invoice_id) && !empty($purchase_invoice_data->invoice_id)){ ?>
                        <li><a href="#tab_3" data-toggle="tab" id="tabs_3">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Purchase Invoice Details</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="invoice_no" class="col-sm-3 input-sm">Purchase Invoice No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="purchase_invoice_data[invoice_no]" id="invoice_no" class="form-control input-sm" value="<?=(isset($purchase_invoice_data->invoice_no))? $purchase_invoice_data->invoice_no : $invoice_no; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="invoice_date" class="col-sm-3 input-sm">Purchase Invoice Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="purchase_invoice_data[invoice_date]" id="invoice_date" class="form-control input-sm pull-right input-datepicker" value="<?=(isset($purchase_invoice_data->invoice_date))? date('d-m-Y', strtotime($purchase_invoice_data->invoice_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="invoice_date" class="col-sm-3 input-sm">Debit / Cash</label>
                                                    <div class="col-sm-9">
                                                        <select name="purchase_invoice_data[debit_cash]" id="debit_cash" class="form-control input-sm pull-right select2">
                                                            <option value="1" <?=(isset($purchase_invoice_data->debit_cash)) && $purchase_invoice_data->debit_cash == '1' ? 'Selected' : ''; ?>>Debit</option>
                                                            <option value="2" <?=(isset($purchase_invoice_data->debit_cash)) && $purchase_invoice_data->debit_cash == '2' ? 'Selected' : ''; ?>>Cash</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="note" class="col-sm-3 input-sm">Notes</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="purchase_invoice_data[note]" id="note" rows="2" ><?=(isset($purchase_invoice_data->note))? $purchase_invoice_data->note : ''; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Supplier Details</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-6">
												<div class="form-group">
													<label for="supplier_id" class="col-sm-3 input-sm">Supplier Name<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
                                                        <select name="purchase_invoice_data[supplier_id]" id="supplier_id" class="form-control input-sm" onchange="supplier_details(this.value)"></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<input type="hidden" name="supplier[supplier_name]" id="supplier_name" class="form-control input-sm supplier_name" placeholder="">
												<input type="hidden" name="supplier[supplier_id]" id="supplier_supplier_id" class="form-control input-sm supplier_supplier_id" placeholder="">
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
                                                        <textarea name="supplier[address]" id="address" class="form-control" rows="2" disabled="" ></textarea>
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="city" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="supplier[city_id]" id="city" class="form-control input-sm select2 city" disabled="" ></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" name="supplier[pincode]" id="pincode" class="form-control input-sm pincode" placeholder="Pin Code" disabled="" >
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="state" class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="supplier[state_id]" id="state" class="form-control input-sm select2" disabled="" ></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="country" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="supplier[country_id]" id="country" class="form-control input-sm select2" disabled="" ></select>
													</div>
												</div>
												<div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_no" class="col-sm-3 input-sm">Contact No.</label>
                                                    <div class="col-sm-9">
                                                        <textarea type="text" class="form-control " name="supplier[contact_no]" id="contact_no" placeholder="" disabled="" ></textarea>
                                                        <small class="">Add multiple Contact number by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
											<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="tel_no" class="col-sm-3  input-sm">Tel No.</label>
                                                    <div class="col-sm-9">                                                        
                                                        <textarea class="form-control tel_no" id="tel_no" name="supplier[tel_no]" placeholder="" disabled=""></textarea>
                                                        <small class="">Add multiple telephone number by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
												<div class="form-group">
													<label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea name="supplier[email_id]" id="email_id" class="form-control email_id" disabled="" ></textarea>
														<small>Add multiple email id by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="cont_person" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9">
														<input type="text" name="supplier[cont_person]" id="cont_person" class="form-control input-sm " placeholder="" disabled="" >
													</div>
												</div>
											</div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($purchase_invoice_lineitems)){ ?>
                                            <input type="hidden" name="line_items_data[lineitem_id]" id="lineitem_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="item_id" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id select2" onchange="purchase_item_details(this.value)"></select>
                                                </div>                                                       
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_code]" id="item_code" class="form-control input-sm item_code disabled" data-name="item_code" readonly="readonly">
                                                    <input type="hidden" class="item_name" id="item_name" name="line_items_data[item_name]" data-name="item_name" placeholder="">
                                                    <input type="hidden" class="item_make_id" id="item_make_id" name="line_items_data[item_make_id]" data-name="item_make_id"  onchange="get_item_make_name(this.value)">
                                                    <input type="hidden" name="line_items_data[item_make_name]" id="item_make_name" class="item_make_name" data-name="item_make_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_description]" id="item_description" class="form-control input-sm item_description" data-name="item_description" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity" class="col-sm-3 input-sm">Req. Qty<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="hidden" name="line_items_data[set_qty]" id="set_qty" class="form-control input-sm set_qty num_only" value="" data-name="set_qty" placeholder="">
                                                    <input type="text" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity num_only" value="1" data-name="quantity" placeholder="">
                                                </div>
                                                <label for="uom_id" class="col-sm-2 input-sm">Req. UOM<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-4">
                                                    <select name="line_items_data[uom_id]" id="uom_id" class="form-control input-sm uom_id select2" onchange="get_uom_name(this.value)" ></select>
                                                    <input type="hidden" name="line_items_data[uom_name]" id="uom_name" class="uom_name" data-name="uom_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="item_rate" class="col-sm-3 input-sm">
                                                    <span style="float: left;" >Rate<span class="required-sign">&nbsp;*</span></span>
                                                </label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate" value="0" placeholder="">
                                                </div>
                                                <label for="rate_uom" class="col-sm-2 input-sm">For 1 UOM <span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-4">
                                                    <select name="line_items_data[rate_uom]" id="rate_uom" class="form-control input-sm rate_uom select2" onchange="get_rate_uom_name(this.value)" ></select>
                                                    <input type="hidden" name="line_items_data[rate_uom_name]" id="rate_uom_name" class="rate_uom_name" data-name="rate_uom_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_rate" class="col-sm-3 input-sm">Relation</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[reference_qty]" id="reference_qty" class="form-control input-sm reference_qty" readonly="" data-name="reference_qty">
                                                </div>
                                                <label for="item_rate" class="col-sm-2 input-sm"><span id="selected_rate_uom"></span> For 1 <span id="selected_required_uom"></span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="disc_per" class="col-sm-3 input-sm">Disc(%)</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[disc_per]" id="disc_per" class="form-control input-sm disc_per num_only" data-input_name="disc_per" placeholder="">
                                                </div>
                                                <label for="disc_value" class="col-sm-2 input-sm">Disc(Value)</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[disc_value]" id="disc_value" class="form-control input-sm disc_value num_only" data-input_name="disc_value" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="igst" class="col-sm-3 input-sm">IGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[igst]" id="igst" class="form-control input-sm igst item_data" data-input_name="igst" placeholder="" >
                                                </div>
                                                <label for="igst_amount" class="col-sm-2 input-sm">IGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[igst_amount]" id="igst_amount" class="form-control input-sm igst_amount item_data" readonly data-input_name="igst_amount" placeholder="" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="cgst" class="col-sm-3 input-sm">CGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[cgst]" id="cgst" class="form-control input-sm cgst numberwithoutzero item_data" data-input_name="cgst" placeholder="">
                                                </div>
                                                <label for="cgst_amount" class="col-sm-2 input-sm">CGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[cgst_amount]" id="cgst_amount" class="form-control input-sm cgst_amount numberwithoutzero item_data" readonly data-input_name="cgst_amount" placeholder="" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="sgst" class="col-sm-3 input-sm">SGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[sgst]" id="sgst" class="form-control input-sm sgst item_data" data-input_name="sgst" placeholder="">
                                                </div>
                                                <label for="sgst_amount" class="col-sm-2 input-sm">SGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[sgst_amount]" id="sgst_amount" class="form-control input-sm sgst_amount item_data" readonly data-input_name="sgst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                </div>
                                                <label for="net_amount" class="col-sm-2 input-sm">Net Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[net_amount]" id="net_amount" class="form-control input-sm net_amount disabled" data-input_name="net_amount" placeholder="" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-sm-12"><div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" data-input_name="add_lineitem" value="Add Item" <?=$this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "add") ? '' : 'disabled'?>/>
                                            </div>
                                        </div></div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <table style="" class="table custom-table item-table">
                                        <thead>
                                            <tr>
                                                <th width="55px">Action</th>
                                                <th>Item Name</th>
                                                <th>Item Code</th>
                                                <th>Item Make</th>
                                                <th class="text-right">Req. Qty</th>
                                                <th class="text-center">Req. UOM</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-center">For 1 UOM</th>
                                                <th class="text-center">Relation</th>
                                                <th class="text-right">Amount</th>
                                                <th class="text-right">Disc(value)</th>
                                                <th class="text-right">IGST %</th>
                                                <th class="text-right">CGST %</th>
                                                <th class="text-right">SGST %</th>
                                                <th class="text-right">Net Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lineitem_list"></tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th class="text-right"><span class="qty_total"></span><input type="hidden" name="qty_total" id="qty_total" /></th>
                                                <th class="text-center"></th>
                                                <th class="text-right"></th>
                                                <th class="text-center"></th>
                                                <th class="text-center"></th>
                                                <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                                <th class="text-right" class="text-right"><span class="disc_value_total"></span><input type="hidden" name="disc_value_total" id="disc_value_total" /></th>
                                                <th class="text-right" class="text-right"><span class="igst_total"></span></th>
                                                <th class="text-right" class="text-right"><span class="cgst_total"></span></th>
                                                <th class="text-right" class="text-right"><span class="sgst_total"></span></th>
                                                <th class="text-right" class="text-right"><span class="net_amount_total"></span><input type="hidden" name="net_amount_total" id="net_amount_total" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-2">
                                    <br /><label>Packing & Forwarding</label>
                                </div>
                                <div class="col-md-2">
                                    <label for="packing_forwarding_amount" class="input-sm"> Amount </label>
                                    <input type="text" class="form-control input-sm num_only" name="purchase_invoice_data[pf_amount]" id="pf_amount" placeholder="Packing & Forwarding Amount" value="<?=(isset($purchase_invoice_data->pf_amount))? $purchase_invoice_data->pf_amount : ''; ?>">
                                </div>
                                <div class="col-md-2">
                                    <label for="packing_forwarding_amount" class="input-sm"> CGST % </label>
                                    <input type="text" class="form-control input-sm " id="pf_cgst"  name="purchase_invoice_data[pf_cgst]" value="<?=(isset($purchase_invoice_data->pf_cgst))? $purchase_invoice_data->pf_cgst : '0'; ?>" readonly="">
                                </div>
                                <div class="col-md-2">
                                    <label for="packing_forwarding_amount" class="input-sm"> SGST % </label>
                                    <input type="text" class="form-control input-sm " id="pf_sgst"  name="purchase_invoice_data[pf_sgst]" value="<?=(isset($purchase_invoice_data->pf_sgst))? $purchase_invoice_data->pf_sgst : '0'; ?>" readonly="">
                                </div>
                                <div class="col-md-2">
                                    <label for="packing_forwarding_amount" class="input-sm"> IGST % </label>
                                    <input type="text" class="form-control input-sm " id="pf_igst"  name="purchase_invoice_data[pf_igst]" value="<?=(isset($purchase_invoice_data->pf_igst))? $purchase_invoice_data->pf_igst : '0'; ?>" readonly="">
                                </div>
                                <div class="col-md-2">
                                    <label for="packing_forwarding_amount" class="input-sm"> Amount </label>
                                    <input type="text" class="form-control input-sm num_only" id="pf_amount_with_gst" name="purchase_invoice_data[pf_amount_with_gst]" value="<?=(isset($purchase_invoice_data->pf_amount_with_gst))? $purchase_invoice_data->pf_amount_with_gst : ''; ?>" readonly="">
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-2">
                                    <label>Transportation</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control input-sm num_only" id="t_amount" name="purchase_invoice_data[t_amount]" placeholder="Transportation Amount" value="<?=(isset($purchase_invoice_data->t_amount))? $purchase_invoice_data->t_amount : ''; ?>" >
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control input-sm " id="t_cgst"  name="purchase_invoice_data[t_cgst]" value="<?=(isset($purchase_invoice_data->t_cgst))? $purchase_invoice_data->t_cgst : '0'; ?>" readonly="">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control input-sm " id="t_sgst"  name="purchase_invoice_data[t_sgst]" value="<?=(isset($purchase_invoice_data->t_sgst))? $purchase_invoice_data->t_sgst : '0'; ?>" readonly="">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control input-sm " id="t_igst"  name="purchase_invoice_data[t_igst]" value="<?=(isset($purchase_invoice_data->t_igst))? $purchase_invoice_data->t_igst : '0'; ?>" readonly="">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control input-sm num_only" id="t_amount_with_gst" name="purchase_invoice_data[t_amount_with_gst]" value="<?=(isset($purchase_invoice_data->t_amount_with_gst))? $purchase_invoice_data->t_amount_with_gst : ''; ?>" readonly="">
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-2 pull-right">
                                    <input type="text" class="form-control input-sm num_only" id="grand_total" name="purchase_invoice_data[grand_total]" value="<?=(isset($purchase_invoice_data->grand_total))? number_format($purchase_invoice_data->grand_total,2,".","") : ''; ?>" readonly="">
                                </div>
                                <div class="col-sm-2 pull-right">
                                    <label>Grand total</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <?php if(isset($purchase_invoice_data->invoice_id) && !empty($purchase_invoice_data->invoice_id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=(isset($purchase_invoice_data->created_by_name))? $purchase_invoice_data->created_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=(isset($purchase_invoice_data->created_at))? $purchase_invoice_data->created_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=(isset($purchase_invoice_data->updated_by_name))? $purchase_invoice_data->updated_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=(isset($purchase_invoice_data->updated_at))? $purchase_invoice_data->updated_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        <?php endif;?>
        </div>
        <section class="content-header">
            <?php if($add_role || $edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
            <?php endif;?>
            <?php if($view_role): ?>
                <a href="<?= base_url()?>purchase_invoice/invoice_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Invoice List</a>
            <?php endif;?>
            <?php if($add_role): ?>
                <a href="<?= base_url()?>purchase_invoice/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice</a>
            <?php endif;?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if(isset($purchase_invoice_data->invoice_id) && !empty($purchase_invoice_data->invoice_id)){ } else { ?>
<div class="modal fade" id="purchase_order_list_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h4 class="modal-title">Select Order
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                        <a href='javascript:void(0);' class="close_modal btn btn-primary btn-xs">Close</a>
                    </span>
                    <span class="pull-right text-danger text-center">If you do Not want to create invoice from Order, click on "Close" and create Manually &nbsp;</span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="purchase_order_datatable" class="table custom-table table-striped">
                        <thead>
                            <tr>
                                <th>Order Number#</th>
                                <th>Supplier Name</th>
                                <th>Order Date</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Order Number#</th>
                                <th>Supplier Name</th>
                                <th>Order Date</th>
                                <th>Created At</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?=dist_url('js/sales_order.js');?>"></script>
<script type="text/javascript">
    var invoice_index = '';
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
    var lineitem_objectdata = [];
    <?php if(isset($purchase_invoice_lineitems)){ ?>
		var li_lineitem_objectdata = [<?php echo $purchase_invoice_lineitems; ?>];
		var lineitem_objectdata = [];
        if(li_lineitem_objectdata != ''){
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
	<?php } ?>
    display_lineitem_html(lineitem_objectdata);
    $('#pf_amount').keyup();
    <?php if(isset($purchase_invoice_data->invoice_id) && !empty($purchase_invoice_data->invoice_id)){ ?>
        first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function(){
        $('.select2').select2();
        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
        initAjaxSelect2($("#item_id"),"<?=base_url('app/purchase_item_select2_source')?>");
        initAjaxSelect2($("#supplier_id"),"<?=base_url('app/supplier_select2_source')?>");
        initAjaxSelect2($("#uom_id"),"<?=base_url('app/uom_select2_source')?>");
        initAjaxSelect2($("#rate_uom"),"<?=base_url('app/uom_select2_source')?>");
        <?php if(isset($purchase_invoice_data->supplier_id)){ ?>
			setSelect2Value($("#supplier_id"),"<?=base_url('app/set_supplier_select2_val_by_id/'.$purchase_invoice_data->supplier_id)?>");
            supplier_details(<?=$purchase_invoice_data->supplier_id; ?>);
		<?php } ?>
        
        var table
        table = $('#purchase_order_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('purchase_invoice/get_purchase_order_datatable')?>",
                "type": "POST",
				"data":function(d){
					d.request_from = 'add_invoice';
				}
            },
            "scrollY": 400,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
        });
        
        $('#purchase_order_list_model').modal({backdrop: 'static', keyboard: false});
        $('#purchase_order_list_model').modal('show');
        $('#purchase_order_list_model').on('shown.bs.modal',function(){
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();	
        });
        
        $(document).on('click', '.order_row', function () {
            var tr = $(this).closest('tr');
            var order_id = $(this).data('order_id');
            $("#order_id").val(order_id);
            feedOrderData(order_id);
            $('#purchase_order_list_model').modal('hide');
            $('#invoice_no').focus();
        });
        
        $(document).on('click','.close_modal', function () {
			$('#purchase_order_list_model').modal('hide');
		});
        
        $(document).on('input','#item_rate',function () {
            if(this.value > 9900000){
                this.value = 9900000;
            }
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if(target == '#tab_2'){
                $('#item_id').select2('open');
            } else {
                $('#item_id').select2('close');
            }
        });
        
        $(document).on('keyup', '#pf_amount', function() {
            var pf_amount = $('#pf_amount').val();
            if(pf_amount != '' && pf_amount != null){
                var pf_cgst = $('#pf_cgst').val();
                var pf_sgst = $('#pf_sgst').val();
                var pf_igst = $('#pf_igst').val();
                var total_cgst = (parseFloat(pf_amount) * parseFloat(pf_cgst)) / 100;
                var total_sgst = (parseFloat(pf_amount) * parseFloat(pf_sgst)) / 100;
                var total_igst = (parseFloat(pf_amount) * parseFloat(pf_igst)) / 100;
                var pf_amount_with_gst = parseFloat(pf_amount) + total_cgst + total_sgst + total_igst;
            } else {
                var pf_amount_with_gst = 0;
            }
            $('#pf_amount_with_gst').val(pf_amount_with_gst.toFixed(2));
            var net_amount_total = $('#net_amount_total').val();
            var t_amount_with_gst = $('#t_amount_with_gst').val();
            var grand_total = (parseFloat(net_amount_total) || 0) + (parseFloat(pf_amount_with_gst) || 0) + (parseFloat(t_amount_with_gst) || 0);
            $('#grand_total').val(grand_total.toFixed(2));
        });
        
        $(document).on('keyup', '#t_amount', function() {
            var t_amount = $('#t_amount').val();
            if(t_amount != '' && t_amount != null){
                var t_cgst = $('#t_cgst').val();
                var t_sgst = $('#t_sgst').val();
                var t_igst = $('#t_igst').val();
                var total_cgst = (parseFloat(t_amount) * parseFloat(t_cgst)) / 100;
                var total_sgst = (parseFloat(t_amount) * parseFloat(t_sgst)) / 100;
                var total_igst = (parseFloat(t_amount) * parseFloat(t_igst)) / 100;
                var t_amount_with_gst = parseFloat(t_amount) + total_cgst + total_sgst + total_igst;
            } else {
                var t_amount_with_gst = 0;
            }
            $('#t_amount_with_gst').val(t_amount_with_gst.toFixed(2));
            var net_amount_total = $('#net_amount_total').val();
            var pf_amount_with_gst = $('#pf_amount_with_gst').val();
            var grand_total = (parseFloat(net_amount_total) || 0) + (parseFloat(pf_amount_with_gst) || 0) + (parseFloat(t_amount_with_gst) || 0);
            $('#grand_total').val(grand_total.toFixed(2));
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_purchase_invoice").submit();
                return false;
            }
        });
        
        $(document).on('submit', '#save_purchase_invoice', function () {
            if($.trim($("#supplier_id").val()) == ''){
                show_notify('Please Select Supplier.',false);
                return false;
            }
            if($.trim($("#item_id").val()) != ''){
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }
            if(lineitem_objectdata == ''){
				show_notify("Please select any one Item.", false);
				return false;
			}
            
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
			var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
			postData.append('line_items_data', lineitem_objectdata_stringify);
			$.ajax({
				url: "<?=base_url('purchase_invoice/save_purchase_invoice') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('purchase_invoice/invoice_list') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('purchase_invoice/invoice_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });

        $('#add_lineitem').on('click', function() {
            var item_id = $("#item_id").val();
			if(item_id == '' || item_id == null){
				show_notify("Please select Item!", false);
                $("#item_id").focus();
				return false;
			}
			var quantity = $("#quantity").val();
			if(quantity == '' || quantity == null){
				show_notify("Item quantity is required!", false);
                $("#quantity").focus();
				return false;
			}
            var uom_id = $("#uom_id").val();
			if(uom_id == '' || uom_id == null){
				show_notify("Req. UOM is required!", false);
                $("#uom_id").select2('open');
				return false;
			}
            var item_rate = $("#item_rate").val();
			if(item_rate == '' || item_rate == null){
				show_notify("Item Rate is required!", false);
                $("#item_rate").focus();
				return false;
			}
            var rate_uom = $("#rate_uom").val();
			if(rate_uom == '' || rate_uom == null){
				show_notify("For 1 UOM is required!", false);
                $("#rate_uom").select2('open');
				return false;
			}
			
			var key = '';
			var value = '';
			var lineitem = {};
            var is_validate = '0';
            lineitem['lineitem_id'] = $("#lineitem_id").val();
			$('select[name^="line_items_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			$('input[name^="line_items_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
            $('select[name^="line_items_data"]').each(function (index) {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                
                $.each(lineitem_objectdata, function (index, value) {
                    if (value.item_id == item_id && typeof (value.id) != "undefined" && value.id !== null && value.id !== '') {
                        $('input[name^="line_items_data"]').each(function (index) {
                            keys = $(this).attr('name');
                            keys = keys.replace("line_items_data[", "");
                            keys = keys.replace("]", "");
                            if (keys == 'id') {
                                if (value.id != $(this).val()) {
                                    is_validate = '1';
                                    show_notify("You cannot Add this Item. This Item has been used!", false);
                                    return false;
                                }
                            }
                        });
                    } else if (value.item_id == item_id) {
//                        alert('2');
                        if(invoice_index !== index){
                            is_validate = '1';
                            show_notify("You cannot Add this Item. This Item has been used!", false);
                            return false;
                        }
                    }
                });
                if (is_validate == '1') {
                    return false;
                }
            });
            if (is_validate != '1') {
                var new_lineitem = JSON.parse(JSON.stringify(lineitem));
                var line_items_index = $("#line_items_index").val();
                if(line_items_index != ''){
                    lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
                } else {
                    lineitem_objectdata.push(new_lineitem);
                }
                display_lineitem_html(lineitem_objectdata);
                $('#pf_amount').keyup();
                $('#lineitem_id').val('');
                $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
                $("#item_id").val(null).trigger("change");
                $("#uom_id").val(null).trigger("change");
                $("#set_qty").val('');
                $("#rate_uom").val(null).trigger("change");
                $("#reference_qty").val('');
                $("#quantity").val('1');
                $("#disc_per").val('0');
                $("#line_items_index").val('');
                if(on_save_add_edit_item == 1){
                    on_save_add_edit_item == 0;
                    $('#save_purchase_invoice').submit();
                }			
                if(edit_lineitem_inc == 1 && <?=$this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "add")?> != 1){
                    $('.add_lineitem').attr("disabled", "disabled");
                }
                edit_lineitem_inc = 0;
                invoice_index = '';
                $('#item_id').select2('open');
            }
        });
        
        $(document).on('change', '.pts_req_qty', function () {
            var current_req_qty = $(this).val();
            console.log(lineitem_objectdata);
            var pts_selected_index = $(this).data('pts_selected_index');
            lineitem_objectdata[pts_selected_index].quantity = current_req_qty;
//            console.log(lineitem_objectdata);
            var quantity = lineitem_objectdata[pts_selected_index].quantity;
            var item_rate = lineitem_objectdata[pts_selected_index].item_rate;
            var reference_qty = lineitem_objectdata[pts_selected_index].reference_qty;
            var before_discount_price = (quantity || 1) * (item_rate || 1) * (reference_qty || 1);
            var discount = lineitem_objectdata[pts_selected_index].disc_per;
            if(discount == ''){ discount = 0; }
            discount_amt = (before_discount_price * discount)/100;
            discounted_price = parseFloat(before_discount_price) - parseFloat(discount_amt);
            
            var cgst = lineitem_objectdata[pts_selected_index].cgst;
            var cgst_amount = (discounted_price * cgst)/100;
//            $("#cgst_amount").val(parseF(cgst_amount));
            var sgst = lineitem_objectdata[pts_selected_index].sgst;
            var sgst_amount = (discounted_price * sgst)/100;
//            $("#sgst_amount").val(parseF(sgst_amount));
            var igst = lineitem_objectdata[pts_selected_index].igst;
            var igst_amount = (discounted_price * igst)/100;
//            $("#igst_amount").val(parseF(igst_amount));
            
            var net_amount = parseInt(discounted_price) + parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
            net_amount = net_amount.toFixed(0);
            $("#total_amount").val(net_amount);
            lineitem_objectdata[pts_selected_index].disc_value = discount_amt;
            lineitem_objectdata[pts_selected_index].amount = before_discount_price;
            lineitem_objectdata[pts_selected_index].net_amount = net_amount;
            lineitem_objectdata[pts_selected_index].cgst_amount = cgst_amount;
            lineitem_objectdata[pts_selected_index].sgst_amount = sgst_amount;
            lineitem_objectdata[pts_selected_index].igst_amount = igst_amount;
            display_lineitem_html(lineitem_objectdata);
            $('#pf_amount').keyup();
        });
    });
    
    
    function feedOrderData(order_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>purchase_invoice/get_order_details',
            type: "POST",
            async: false,
            data: {order_id: order_id},
            success: function(data){
                $("#ajax-loader").hide();
                json = JSON.parse(data);
                var purchase_order_data = json.purchase_order_data;
                setSelect2Value($("#supplier_id"),"<?=base_url('app/set_supplier_select2_val_by_id')?>/"+purchase_order_data.supplier_id);
                $('#party_name').val($('#supplier_id').val());
                $('#party_supplier_id').val(purchase_order_data.supplier_id);
//                supplier_details($('#supplier_id').val());
                if(json.inquiry_lineitems != ''){
                    lineitem_objectdata = json.purchase_order_lineitems;
                    display_lineitem_html(lineitem_objectdata);
                    $('#pf_amount').keyup();
                }
            },
        });
    }
    
    function display_lineitem_html(lineitem_objectdata){
        $('#ajax-loader').show();
		var new_lineitem_html = '';
		var qty_total = 0;
		var amount_total = 0;
		var disc_value_total = 0;
		var net_amount_total = 0;
        var cgst_total = 0;
		var sgst_total = 0;
		var igst_total = 0;
		$.each(lineitem_objectdata, function (index, value) {
            var lineitem_edit_btn = '';
            var lineitem_delete_btn = '';			
			<?php
			if($this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "edit")): ?>
			lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
			<?php endif;?>
			<?php if($this->app_model->have_access_role(PURCHASE_ORDER_ITEMS, "delete")): ?>
			lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
			<?php endif;?>
			var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
			lineitem_edit_btn +
			lineitem_delete_btn +
			'</td>' +
			'<td>' + value.item_name + '</td>' +
            '<td>' + value.item_code + '</td>' +
            '<td>' + value.item_make_name + '</td>' +
//            '<td class="text-right">' + value.quantity + '</td>'+
            '<td class="text-right"><input type="text" id="req_qty_' + index + '" data-pts_selected_index="' + index + '" class="pts_req_qty num_only text-right" value="'+ value.quantity + '" style="width:100px;"></td>' +
            '<td class="text-right">' + value.uom_name + '</td>';
            row_html += '<td class="text-right">' + value.item_rate + '</td>';
            row_html += '<td class="text-right">' + value.rate_uom_name + '</td>';
            row_html += '<td class="text-center">' + value.reference_qty + ' ' + value.rate_uom_name + ' For 1 ' + value.uom_name + '</td>';
            row_html += '<td class="text-right">' + value.amount + '</td>' +
            '<td class="text-right">' + value.disc_value + '</td>' +
            '<td class="text-right">' + value.igst + '</td>' +
            '<td class="text-right">' + value.cgst + '</td>' +
            '<td class="text-right">' + value.sgst + '</td>' +
            '<td class="text-right">' + value.net_amount + '</td></tr>';
			new_lineitem_html += row_html;
			qty_total += +value.quantity;
			amount_total += +value.amount;
			disc_value_total += +value.disc_value;
			net_amount_total += +value.net_amount;
            cgst_total += +value.cgst_amount;
            sgst_total += +value.sgst_amount;
            igst_total += +value.igst_amount;
		});
		$('tfoot span.qty_total').html(qty_total); $('#qty_total').val(qty_total);
		$('tfoot span.amount_total').html(amount_total); $('#amount_total').val(amount_total);
		$('tfoot span.disc_value_total').html(disc_value_total); $('#disc_value_total').val(disc_value_total);
        $('tfoot span.net_amount_total').html(net_amount_total.toFixed(2)); $('#net_amount_total').val(net_amount_total.toFixed(2));
        $('tfoot span.igst_total').html(igst_total.toFixed(2));
        $('tfoot span.cgst_total').html(cgst_total.toFixed(2));
        $('tfoot span.sgst_total').html(sgst_total.toFixed(2));
        <?php if(isset($purchase_invoice_data->grand_total) && !empty($purchase_invoice_data->grand_total)){ } else { ?>
            $('#grand_total').val(net_amount_total.toFixed(2));
        <?php } ?>
		$('tbody#lineitem_list').html(new_lineitem_html);
        $('#ajax-loader').hide();
	}

    function edit_lineitem(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('#ajax-loader').show();
        if(edit_lineitem_inc == 0){
			edit_lineitem_inc = 1;
			$(".add_lineitem").removeAttr("disabled");
		}
		value = lineitem_objectdata[index];
        invoice_index = index;
		$("#line_items_index").val(index);
		
		initAjaxSelect2($("#item_id"),"<?=base_url('app/purchase_item_select2_source/')?>");
		setSelect2Value($("#item_id"),"<?=base_url('app/set_purchase_item_select2_val_by_id/')?>/" + value.item_id);
		initAjaxSelect2($("#uom_id"),"<?=base_url('app/uom_select2_source/')?>");
		setSelect2Value($("#uom_id"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + value.uom_id);
        initAjaxSelect2($("#rate_uom"),"<?=base_url('app/uom_select2_source/')?>");
		setSelect2Value($("#rate_uom"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + value.rate_uom);
		if(typeof(value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
			$("#lineitem_id").val(value.lineitem_id);
		}
        $("#item_name").val(value.item_name);
        $("#item_code").val(value.item_code);
        $("#item_make_id").val(value.item_make_id).change();
        $("#item_description").val(value.item_description);
        $("#item_rate").val(value.item_rate);
        $("#quantity").val(value.quantity);
        $("#reference_qty").val(value.reference_qty);
        $("#amount").val(value.amount);
        $("#disc_per").val(value.disc_per);
        $("#disc_value").val(value.disc_value);
        $("#igst").val(value.igst);
        $("#cgst").val(value.cgst);
        $("#sgst").val(value.sgst);
        $("#igst_amount").val(value.igst_amount);
        $("#cgst_amount").val(value.cgst_amount);
        $("#sgst_amount").val(value.sgst_amount);
        $("#net_amount").val(value.net_amount);
		$('#ajax-loader').hide();
	}
	
	function remove_lineitem(index){
        if(confirm('Are you sure ?')){
            value = lineitem_objectdata[index];
            if(typeof(value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
                $('#save_purchase_invoice').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.lineitem_id + '" />');
            }
            lineitem_objectdata.splice(index,1);
            display_lineitem_html(lineitem_objectdata);
            $('#pf_amount').keyup();
        }
	}
    
    function supplier_details(id){
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>purchase_order/ajax_load_supplier/'+id,
            async: false,
            data: id='supplier_id',
            success: function(data){
                var json = $.parseJSON(data);
                if (json['address']) {
                    $("#address").val(json['address']);
                }else{
                    $("#address").val("");
                }
                if (json['supplier_name']) {
                    $('#supplier_name').val(json['supplier_name']);
                }
                if (json['supplier_id']) {
                    $("#supplier_id").val(json['supplier_id']);
                    $("#supplier_supplier_id").val(json['supplier_id']);
                }
                if (json['city_id']) {
                    setSelect2Value($("#city"),'<?=base_url()?>app/set_city_select2_val_by_id/'+json['city_id']);
                }else{
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                }else{
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                }else{
                    setSelect2Value($("#country"));
                }
                if (json['tel_no']) {
                    $("#tel_no").val(json['tel_no']);
                }else{
                    $("#tel_no").val("");
                }
                if (json['contact_no']) {
                    $("#contact_no").val(json['contact_no']);
                }else{
                    $("#contact_no").val("");
                }
                if (json['email_id']) {
                    $("#email_id").val(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }else{
                    $("#email_id").val("");
                }
                if (json['pincode']) {
                    $("#pincode").val(json['pincode']);
                }else{
                    $("#pincode").val("");
                }
                if (json['cont_person']) {
                    $("#cont_person").val(json['cont_person']);
                }else{
                    $("#cont_person").val("");
                }
                $("#pf_igst").val('0');
                $("#pf_cgst").val('0');
                $("#pf_sgst").val('0');
                $("#t_igst").val('0');
                $("#t_cgst").val('0');
                $("#t_sgst").val('0');
                var state_id = $('#state').val();
                if(state_id == null || state_id == '<?=GUJARAT_STATE_ID; ?>'){
                    $("#pf_cgst").val('9');
                    $("#pf_sgst").val('9');
                    $("#t_cgst").val('9');
                    $("#t_sgst").val('9');
                } else {
                    $("#pf_igst").val('18');
                    $("#t_igst").val('18');
                }
                $("#ajax-loader").hide();
            }
        });
    }
    
    function purchase_item_details(id){
        if(edit_lineitem_inc == 0){
            if(id != '') {
                if($.trim($("#supplier_id").val()) == ''){
                    show_notify('Please Select Supplier. <br /> Because, On Supplier State We will decide which GST we will use : IGST or CGST & SGST.',false);
                    $("#item_id").val(null).trigger("change");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>purchase_order/ajax_load_purchase_item_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
    //                    console.log(data);
                        var json = $.parseJSON(data);
                        if (json['item_name']) {
                            $(".item_name").val(json['item_name']);
                        } else {
                            $(".item_name").val("");
                        }

                        if (json['item_code']) {
                            $("#item_code").val(json['item_code']);
                        } else {
                            $("#itm_code").val('');
                        }
                        if (json['item_make_id']) {
                            $("#item_make_id").val(json['item_make_id']).change();;
                        } else {
                            $("#item_make_id").val('');
                        }
                        if (json['uom']) {
                            setSelect2Value($("#uom_id"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + json['uom']);
                        } else {
                            $("#uom_id").val(null).trigger("change");
                        }

                        if (json['rate']) {
                            $("#item_rate").val(json['rate']);
                        } else {
                            $("#item_rate").val('');
                        }
                        if (json['quantity']) {
                            $("#set_qty").val(json['quantity']);
                        } else {
                            $("#set_qty").val("");
                        }
                        if (json['reference_qty']) {
                            $("#reference_qty").val(json['reference_qty']);
                        } else {
                            $("#reference_qty").val("");
                        }
                        if (json['rate_uom']) {
                            setSelect2Value($("#rate_uom"),"<?=base_url('app/set_uom_select2_val_by_id/')?>/" + json['rate_uom']);
                        } else {
                            $("#rate_uom").val('');
                        }

                        $(".igst").val('0');
                        $(".cgst").val('0');
                        $(".sgst").val('0');
                        var state_id = $('#state').val();
                        if(state_id == null || state_id == '<?=GUJARAT_STATE_ID; ?>'){
                            if (json['cgst']) {
                                $(".cgst").val(json['cgst']);
                            } else {
                                $(".cgst").val('');
                            }
                            if (json['sgst']) {
                                $(".sgst").val(json['sgst']);
                            } else {
                                $(".sgst").val('');
                            }
                        } else {
                            if (json['igst']) {
                                $(".igst").val(json['igst']);
                            } else {
                                $(".igst").val('');
                            }
                        }
                        input_value_get_amount();
                    }
                });
            } else {
                $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
                $("#uom_id").val(null).trigger("change");
                $("#rate_uom").val(null).trigger("change");
                $("#quantity").val('1');
                $("#disc_per").val('0');
                $("#line_items_index").val('');
            }
        } else {
            if(id != '') {
                if($.trim($("#supplier_id").val()) == ''){
                    show_notify('Please Select Supplier. <br /> Because, On Supplier State We will decide which GST we will use : IGST or CGST & SGST.',false);
                    $("#item_id").val(null).trigger("change");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>purchase_order/ajax_load_purchase_item_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
    //                    console.log(data);
                        var json = $.parseJSON(data);
                        
                    }
                });
            } else { 
                $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
                $("#uom_id").val(null).trigger("change");
                $("#set_qty").val('');
                $("#rate_uom").val(null).trigger("change");
                $("#quantity").val('1');
                $("#disc_per").val('0');
                $("#line_items_index").val('');
            } 
        }
    }
        
    function get_uom_name(id){
        $.ajax({
            url: "<?=base_url('app/set_uom_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if(response.text == '--select--'){ response.text = ''; }
                $('#uom_name').val(response.text);
                $('#selected_required_uom').html(response.text);
                check_uom_and_rate_uom();
            },
        });
    }
    
    function get_rate_uom_name(id){
        $.ajax({
            url: "<?=base_url('app/set_uom_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if(response.text == '--select--'){ response.text = ''; }
                $('#rate_uom_name').val(response.text);
                $('#selected_rate_uom').html(response.text);
                check_uom_and_rate_uom();
            },
        });
    }
    
    function check_uom_and_rate_uom(){
        var uom_id = $("#uom_id").val();
        var rate_uom = $("#rate_uom").val();
        if(uom_id == '' || uom_id == null && rate_uom == '' || rate_uom == null){ } else {
            if(uom_id == rate_uom){
                $('#reference_qty').val(1);
            } else {
                var item_id = $("#item_id").val();
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>purchase_order/ajax_load_purchase_item_details/' + item_id,
                    async: false,
                    data: item_id = 'item_id',
                    success: function (data) {
                        var json = $.parseJSON(data);
                        if (json['reference_qty']) {
                            $("#reference_qty").val(json['reference_qty']);
                        } else {
                            $("#reference_qty").val("");
                        }
                    }
                });
            }
            input_value_get_amount();
        }
    }
    
    function get_item_make_name(id){
        $.ajax({
            url: "<?=base_url('app/set_challan_item_make_id_select2_val_by_id/') ?>/" + id,
            type: "POST",
            dataType: 'json',
            async: false,
            cache: false,
            success: function (response) {
                if(response.text == '--select--'){ response.text = ''; }
                $('#item_make_name').val(response.text);
            },
        });
    }
    
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
    <?php } ?>
</script>