<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Purchase Invoice List</small>
			<?php
			$add_role = $this->app_model->have_access_role(PURCHASE_INVOICE_MODULE_ID, "add"); 
			if($add_role): ?>
				<a href="<?=base_url('purchase_invoice/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice</a>
			<?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                <!-- Start Advance Search -->
				<div class="col-md-6">
					<a href="javascript:void(0)" class="btn btn-info btn-sm filtration_div"> Advanced Search </a>
					<div class="filtration_fields" id="filtration_fields">
						<br>
						<div class="col-md-12">
							<div class="row">
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='1'> Invoice No.</a> -
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='3'> Supplier Name</a> -
								<a href="javascript:void(0)" class="showhideColumn" data-columnindex='5'> Created At</a> 
							</div>	
						</div>
						<br><br>
						<table cellpadding="3" cellspacing="0" border="0" style="width:100%;">
							<thead>
								<tr>
									<th>Target</th>
									<th style="text-align:center">Search text</th>
								</tr>
							</thead>
							<tbody>
								<tr id="filter_global">
									<td>Global search</td>
									<td align="center"><input type="text" class="form-control input-sm global_filter" id="global_filter"></td>
								</tr>
								<tr id="filter_col2" data-column="1">
									<td>Column - Invoice Number</td>
									<td align="center"><input type="text" class="form-control input-sm column_filter" id="col1_filter"></td>
								</tr>
                                <tr id="filter_col2" data-column="2">
									<td>Column - Order Number</td>
									<td align="center"><input type="text" class="form-control input-sm column_filter" id="col2_filter"></td>
								</tr>
								<tr id="filter_col4" data-column="3">
									<td>Column - Supplier Name</td>
									<!--<td align="center"><input type="text" class="column_filter" id="col3_filter"></td>-->
									<td align="center">
										<select name="supplier_name" id="supplier_name1" class="form-control input-sm" onchange='filterSuppliername()'>
										</select>
									</td>
								</tr>
								<tr id="filter_col6" data-column="4">
									<td>Column - Created At</td>
									<td align="center"><input type="text" class="form-control input-sm column_filter" id="col5_filter"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="clearfix"></div>
				<br />
				<!--End Advance Search-->
                    <form action="" method="POST">

                        <div class="row">
                            <?php 
                            	$current_status = 'all';
								/*if($this->input->get_post("status")){ 
									$current_status = $this->input->get_post("status"); 
								} else {
									$current_status = 'pending';
								}*/
                                if($this->input->get_post("user_id")){ 
									$current_staff = $this->input->get_post("user_id"); 
								} else {
									$current_staff = $this->session->userdata('is_logged_in')['staff_id'];
								}
							?>
                            <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
								<div class="col-md-4">
									Search by User:
                                    <select class="form-control" name="user_id" id="user_id" class="select2">
										<option value="all" <?php if($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
										<?php

										if (!empty($users)) {
											foreach ($users as $client) {
												if (trim($client->name) != "") {
													//$selected = $current_staff == $client->staff_id ? 'selected' : '';
													$selected = '';
													echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
												}
											}
										}
										?>
									</select>
								</div>
                            <?php } ?>
                            <div class="col-md-3">
                                From Date :
                                <input type="text" name="from_date" id="datepicker1" class="form-control input-sm"  autocomplete="off">
                            </div>
                            <div class="col-md-3">
                                To Date :
                                <input type="text" name="to_date" id="datepicker2" class="form-control input-sm" autocomplete="off">
                            </div>
                            <div class="col-md-1"><br/>
                                <button class="btn btn-info btn-sm input-sm search_list" type="button">Search</button> 
                                &nbsp;
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>

                    </form>
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="purchase_invoice_list_table" class="table custom-table table-striped">
                            <thead>
                            <tr>
                                <th width="100px">Action</th>
                                <th width="100px">Invoice No.</th>
                                <th width="100px">Order No.</th>
                                <th>Supplier Name</th>
                                <th>Invoice Date</th>
                                <th>Packing & Forwarding</th>
                                <th>Transportation</th>
                                <th>Bill Amount</th>
                                <th>Created At</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function() {
        $("#user_id").select2();
	    initAjaxSelect2($("#supplier_name1"),"<?=base_url('app/supplier_select2_source')?>");
	    $('#filtration_fields').hide();
		$(document).on('click', '.filtration_div', function(){
			$(".filtration_fields").toggle(500);
		});
        table = $('#purchase_invoice_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('purchase_invoice/purchase_invoice_datatable')?>",
                "type": "POST",
                "data": function(d){
                    d.user_id = $("#user_id").val();
                    d.from_date = $("#datepicker1").val();
                    d.to_date = $("#datepicker2").val();
                }, 
            },
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
			}
		});
	});
	
	$(document).on("click", ".search_list", function(){        
        table.draw();
    });
    
    $(document).on("click", ".delete_button", function () {
        var value = confirm('Are you sure delete this records?');
        var tr = $(this).closest("tr");
        if (value) {
            $.ajax({
                url: $(this).data('href'),
                type: "POST",
                processData: false,
				contentType: false,
				cache: false,
                data: '',
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json['success'] == 'false'){
                        show_notify(json['msg'],false); 
                        return false;
                    }
                    if (json['success'] == 'true'){
                        show_notify(json['msg'],true);
                        table.draw();
                        return false;
                    }
                }
            });
        }
    });
	
	$('input.global_filter').on( 'keyup click', function () {
			filterGlobal();
	} );
	$('input.column_filter').on( 'keyup click', function () {
		filterColumn( $(this).parents('tr').attr('data-column') );
	} );
	$('.showhideColumn').on('click', function(){
		var tbl_column = table.column($(this).attr('data-columnindex'));
		tbl_column.visible(!tbl_column.visible());
	});
	function filterGlobal () {
		$('#purchase_invoice_list_table').DataTable().search(
			$('#global_filter').val()
		).draw();
	}

	function filterColumn ( i ) {
		$('#purchase_invoice_list_table').DataTable().column( i ).search(
			$('#col'+i+'_filter').val()
		).draw();
	}
	function filterSuppliername () {
		$('#purchase_invoice_list_table').DataTable().column(3).search(
			$("#supplier_name1 option:selected").text()
		).draw();
	}
	
</script>
