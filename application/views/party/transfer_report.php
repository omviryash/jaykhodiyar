<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <small class="text-primary text-bold">Transfer List</small>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <table id="transfer_list_datatable" class="table custom-table agent-table" width="100%">
                        <thead>
                            <tr>
                                <!--<th>Action</th>-->
                                <th>Transfer Date</th>
                                <th>Party Code</th>
                                <th>Party Name</th>
                                <th>Transfer From</th>
                                <th>Transfer To</th>
                                <th>Contact No. </th>
                                <th>Email Id</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                            </tr>
                        </thead>
                        <tbody>
                       </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>

<script>
	var table;
	$(document).ready(function(){
		
		table = $('#transfer_list_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('party/transfer_report_list_datatable')?>",
                "type": "POST",
                "data":function(d){
                    d.party_status = $("#party_status").val();
                }
            },
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
        });
        
		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure Delete this Party?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "GET",
					data: null,
					success: function(data){
						show_notify("Party Deleted Successfully!",true);
						table.draw();
					}
				});
			}
		});
	});
</script>
