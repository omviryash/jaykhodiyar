<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		$isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "delete");
		$isAdd = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add");
		?>
		<h1>
			<small class="text-primary text-bold">City</small>
		</h1>
		<!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-7">
										<div class="panel panel-default">
											<div class="panel-heading">
												City Detail
											</div>
											<div style="margin: 10px;">
												<table id="city-table" class="table custom-table city-table">
													<thead>
													<tr>
														<th>Action</th>
														<th>City</th>
														<th>State</th>
														<th>Country</th>
													</tr>
													</thead>
													<tbody>

													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php if($isAdd || $isEdit) { ?>
									<div class="col-md-5">
										<div class="panel panel-default">
											<div class="panel-heading clearfix">
												<?php if(isset($city_id) && !empty($city_id)){ ?>Edit
												<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
												<?php } ?> City
											</div>
											<?php if(isset($city_id) && !empty($city_id)){
												$dropdown_data_state = $this->crud->get_all_with_where('state','state','ASC',array('country_id'=>$country_id));
											}?>
											<?php $dropdown_data = $this->crud->get_all_records('country','country','ASC'); ?>
											<div style="margin:20px">
												<form method="POST"
													<?php if(isset($city_id) && !empty($city_id)){ ?>
														action="<?=base_url('party/update_city') ?>"
													<?php } else { ?>
														action="<?=base_url('party/add_city') ?>"
													<?php } ?> id="form_city">
													<?php if(isset($city_id) && !empty($city_id)){ ?>
														<input type="hidden" class="form-control input-sm" name="city_id" id="city_id" value="<?php echo $city_id; ?>" >
													<?php } ?>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Select Country<span class="required-sign">*</span></label>
														<div class="col-sm-7">
															<select class="form-control input-sm select2" name="country_id" id="country_id" <?php echo $btn_disable;?>>
																<option value="">Select Country</option>
																<?php
																if(!empty($dropdown_data)) {
																	foreach ($dropdown_data as $dropdown_row) {
																		?>
																		<option value="<?=$dropdown_row->country_id ?>"
																			<?php if(isset($state_id) && !empty($state_id)){
																				if($dropdown_row->country_id == $country_id)
																				{
																					echo 'selected';
																				}
																			} ?>
																		><?=$dropdown_row->country ?></option>
																	<?php } } ?>
															</select>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Select State<span class="required-sign">*</span></label>
														<div class="col-sm-7">
															<select class="form-control input-sm select2" name="state_id" id="state_id" <?php echo $btn_disable;?>>
																<option value="">Select State</option>
																<?php
																if(!empty($dropdown_data_state)) {
																	foreach ($dropdown_data_state as $dropdown_row_state) {
																		?>
																		<option value="<?=$dropdown_row_state->state_id ?>"
																			<?php if(isset($city_id) && !empty($city_id)){
																				if($dropdown_row_state->state_id == $state_id)
																				{
																					echo 'selected';
																				}
																			} ?>
																		><?=$dropdown_row_state->state ?></option>
																	<?php } } ?>
															</select>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">City<span class="required-sign">*</span></label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="city" name="city" value="<?php echo $city_name; ?>" <?php echo $btn_disable;?>>
														</div>
													</div>
													<?php if(isset($city_id) && !empty($city_id)){ ?>
													<button type="submit" class="btn btn-info btn-block btn-xs">Edit City</button>
													<?php } else { ?>
													<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add City</button>
													<?php } ?>
												</form>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>
<script>
	var table;
	$(document).ready(function() {
		table = $('#city-table').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('party/city-datatable')?>",
				"type": "POST"
			},
			"scrollY": 300,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});
	});

	$(document).ready(function(){
		$("#country_id").on("change",function(){
			var selectedCountryID = $(this).val();
			$.ajax({
				type: "POST",
				url: '<?php echo base_url().'party/get_state/' ?>',
				data:'country_id='+selectedCountryID,
			}).done(function(value){
				$("#state_id").html(value);
			});
		});

		$("#form_city").on("submit",function(e){
			e.preventDefault();

			if($("#country_id").val() == ""){
				show_notify('Fill value Country.', false);
				return false;
			}
			if($("#state_id").val() == ""){
				show_notify('Fill value State.', false);
				return false;
			}
			if($("#city").val() == ""){
				show_notify('Fill value City.', false);
				return false;
			}
			<?php if(isset($city_id) && !empty($city_id)){ ?>
			var success_status = check_is_unique('city','city',$("#city").val(),'city_id','<?=$city_id?>','state_id',$("#state_id").val());										
			<?php } else { ?>
			var success_status = check_is_unique('city','city',$("#city").val(),'','','state_id',$("#state_id").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.unique-error').length > 0){
					$("p.unique-error").text('City already exist!');
				}else{
					$("#city").after("<p class='text-danger unique-error'>City already exist!</p>");
				}
				return false;	
			}else{
				$("p.unique-error").text(' ');
			}
            
          



			var url = '<?php echo base_url('party/delete/') ?>';
			var value = $("#city").val();
			var country_id = $("#country_id").val();
			var state_id = $("#state_id").val();
			if(value != '' && country_id != '' && state_id !='')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						window.location.href = "<?php echo base_url('party/city') ?>";
					}
				});
			}
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=city_id&table_name=city',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('party/city') ?>";
					}
				});
			}
		});
	});
</script>
