<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Party Log</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
					<div class="col-md-3">
                    <select name="party_id" id="party_id">
                        <option value=""> - Select Party - </option>
                    </select>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered party-table custom-table" style="width: 100%">
                                <thead>
                                <tr>
                                    <th width="10%">Log ID</th>
                                    <th width="10%">Operation</th>
                                    <th width="10%">Party ID</th>
                                    <th width="10%">Party Code</th>
                                    <th width="15%">Party Name</th>
                                    <th width="15%">Created By</th>
                                    <th width="20%">Date</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        initAjaxSelect2($("#party_id"),"<?=base_url('app/party_select2_source')?>");

        var table = $('.party-table').DataTable({
            "processing": true,
            "serverSide": true,
            "retrive": true,
            "order": ['6','desc'],
            "ajax": {
                "url": "<?php echo site_url('party/party_log_datatable')?>",
                "type": "POST",
                "data": function(d){
                    d.party_id = $("#party_id").val();
                },
            },
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "rowCallback": function( row, data, index ) {
                if ( data[1] == "insert" )
                {
                    $('td',row).css('background-color', '#e5e5e5');
                }
                else if ( data[1] == "update" )
                {
                }else{
                }
            }
        });
        $(document).on("change", "#party_id", function(){
            table.draw();
        });
    });
        
</script>
