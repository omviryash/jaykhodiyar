<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_REFERENCE_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_REFERENCE_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_REFERENCE_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Reference Type</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Reference Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table reference-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Reference</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                    	<?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('party/reference/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> 
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('party/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
                                                                    </td>
						                                            <td><?=$row->reference ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
														<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?> Reference Type
													</div>
													<div style="margin:20px">	
														<form method="POST" 
															<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('party/update_reference') ?>" 
															<?php } else { ?>
															action="<?=base_url('party/add_reference') ?>" 
															<?php } ?> id="reference-form">
															<?php if(isset($id) && !empty($id)){ ?>
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
															<?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-3 input-sm">Reference<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="reference" name="reference" value="<?php echo $reference; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															
                                                            <?php if(isset($id) && !empty($id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit References Type</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add References Type</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

        $("#reference-form").on("submit",function(e){
			e.preventDefault();

			if($("#reference").val() == ""){
				show_notify('Fill value Reference.', false);
				return false;
			}
			<?php if(isset($id) && !empty($id)){ ?>
				var success_status = check_is_unique('reference','reference',$("#reference").val(),'id','<?=$id?>');											
			<?php } else { ?>
			 	var success_status = check_is_unique('reference','reference',$("#reference").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.unique-error').length > 0){
					$("p.unique-error").text('Reference already exist!');
				}else{
					$("#reference").after("<p class='text-danger unique-error'>Reference already exist!</p>");
				}
				return false;	
			}else{
				$("p.unique-error").text(' ');
			}

			var url = '<?php echo base_url('party/delete/') ?>';
			var value = $("#reference").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.reference+'</td>';
		                TableRow += '</tr>';
		                $('.reference-table > tbody > tr:last ').after(TableRow);
		                $("#reference-form")[0].reset();
		                show_notify('Saved Successfully!',true);*/
		                window.location.href = "<?php echo base_url('party/reference') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=reference',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('party/reference') ?>";
					}
				});
			}
		});
		
    });
</script>
