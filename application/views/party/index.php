<?php
if(isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['edit'])){
	echo '<script> $(window).load(function(){ fill_party_data('.$_GET['id'].');  }); </script>';
}
?>
<input type="hidden" id="refresh" value="no">
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="custom content-header">
		<?php $this->load->view('shared/success_false_notify'); ?>
		<h1>
			<small class="text-primary text-bold">Master : Add Party</small>
			<?php
			$accessAdd  = $this->app_model->have_access_role(PARTY_MODULE_ID, "add"); 
			$access  = $this->app_model->have_access_role(PARTY_MODULE_ID, "view");
			$accessEnquiry  = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "add");
			$accessAdit = $this->app_model->have_access_role(PARTY_MODULE_ID, "edit");
			?>
			<?php if($accessAdd || $accessAdit):?>
			<button type="button" class="btn btn-info btn-xs pull-right btn_save_party">Save Party</button>
			<?php endif;?>
			<?php if($access):?>
			<a href="<?=BASE_URL?>party/party_list" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Party List</a>
			<?php endif;?>
			<?php if($accessAdd):?>
			<a href="<?=BASE_URL?>party" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Add Party</a>
			<?php endif;?>
			<?php if($accessEnquiry):?>
			<a href="<?=BASE_URL?>enquiry/add" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Go To Enquiry</a> 
			<?php endif;?>
		</h1>
	</section>
	<div class="clearfix">
		<?php if($accessAdd || $accessAdit):?>
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_1" data-toggle="tab">Party Details</a></li>
					<li><a href="#tab_2" data-toggle="tab">Contact Person</a></li>
					<li><a href="#tab_3" class="address-works-tab" data-toggle="tab">Delivery Address</a></li>
					<li><a href="#tab_4" class="com-address-tab" data-toggle="tab">Communication Address</a></li>
					<!-- <li><a href="#tab_5" data-toggle="tab">Tax & Excise</a></li> -->
					<?php if(isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['edit'])){ ?>
					<li><a href="#tab_6" data-toggle="tab">Login</a></li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<div class="row">
							<div class="col-md-12">
								<fieldset class="scheduler-border">
									<legend class="scheduler-border text-primary text-bold"> Party Details </legend>
									<div class="col-md-6">
										<!-- Horizontal Form -->
										<form class="form-horizontal" id="partycodeform" method="post">
											<input type="hidden" name="party_id" class="party_id" id="party_id" value="<?php if(isset($party)){ echo $party[0]['party_id'];} ?>">
											<div class="box-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label for="inputEmail3"  class="col-sm-3 input-sm">Party Code</label>
															<div class="col-sm-9">
																<input type="text" class="form-control input-sm party_code" name="party_code" id="party_code" placeholder="" value="<?php if(isset($party)){ echo $partycode; } else { echo ''; } ?>"  readonly="">
															</div>
														</div>

														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">Party Name<span class="required-sign">&nbsp;*</span></label>
															<div class="col-sm-9">
																<input type="text" class="form-control input-sm party_name" name="party_name" id="party_name" placeholder="" value="<?php if(isset($party)){ echo $party[0]['party_name'];} ?>">
																<span style="display:none;" class="party_namespan membererror">Party Name must be Required.</span>
															</div>
														</div>

														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">Address</label>
															<div class="col-sm-9">
																<textarea class="form-control" rows="3" name="address" id="main_address"><?php if(isset($party)){ echo $party[0]['address'];} ?></textarea>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">City</label>
															<div class="col-sm-9 dispaly-flex">
																<div class="col-md-6" style="padding:0px !important;">
																	<select name="city" id="city" class="form-control input-sm"></select>
																</div>
																<div class="col-md-2" style="padding:0px !important;text-align:center;">
																	<?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>
																	<small><a style="color:#1f7eba" href="<?= base_url() ?>party/city/" target="_blank">(Add City)</a></small>
																	<?php } ?>                               
																</div>
																<div class="col-md-4" style="padding-right:0px !important;">
																	<input type="text" class="form-control input-sm pincode" id="pincode" name="pincode" tabindex="1" placeholder="Pin Code" value="<?php if(isset($party)){ echo $party[0]['pincode'];} ?>">
																</div>
															</div>
															<!--<div style="text-align:right; width:100%; padding-right:15px"><small><a style="color:red" href="<?= base_url() ?>party/city/" target="_blank">(Create New City)</a></small></div>-->
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">State</label>
															<div class="col-sm-9 dispaly-flex">
																<div class="col-md-9" style="padding:0px !important;">
																	<select name="state" id="state" class="form-control input-sm"></select>
																</div>
																<div class="col-md-3" style="padding:0px !important;text-align:center;">
																	<?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "add")) { ?>    
																	<small><a style="color:#1f7eba" href="<?= base_url() ?>party/state/" target="_blank">(Add State)</a></small>
																	<?php } ?>
																</div>
															</div>
															<!--<div style="text-align:right; width:100%; padding-right:15px"><small><a style="color:red" href="<?= base_url() ?>party/state/" target="_blank">(Create New State)</a></small></div>-->
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">Country</label>
															<div class="col-sm-9 dispaly-flex">
																<div class="col-md-9" style="padding:0px !important;">
																	<select name="country" id="country" class="form-control input-sm"></select>
																</div>
																<div class="col-md-3" style="padding:0px !important;text-align:center;">
																	<?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_COUNTRY_MENU_ID, "add")) { ?>    
																	<small><a style="color:#1f7eba" href="<?= base_url() ?>party/country/" target="_blank">(Add Country)</a></small>
																	<?php } ?>
																</div>
															</div>
															<!--<div style="text-align:right; width:100%; padding-right:15px"><small><a style="color:red" href="<?= base_url() ?>party/country/" target="_blank">(Create New Country)</a></small></div>-->
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">Tel No.</label>
															<div class="col-sm-9">
																<input type="text" class="form-control input-sm fax_no" id="fax_no" name="fax_no" tabindex="2" placeholder="" value="<?php if(isset($party)){ echo $party[0]['fax_no'];} ?>">
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
															<div class="col-sm-9">
																<textarea rows="1" class="form-control phone_no" id="phone_no" name="phone_no" placeholder=""><?php if(isset($party)){ echo $party[0]['phone_no']; } ?></textarea>
																<small class="">Add multiple phone number by Comma separated.</small>
																<span style="display:none;" class="phone_nospan membererror">Phone No must be Required.</span>
															</div>
														</div>

														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">E-mail ID<span class="required-sign">&nbsp;*</span></label>

															<div class="col-sm-9">
																<textarea rows="1" class="form-control email_id" id="email_id" name="email_id"><?php if(isset($party)){ echo $party[0]['email_id']; } ?></textarea>
																<small class="">Add multiple email by Comma separated.</small>
																<span style="display:none;" class="email_idspan membererror">E-mail ID must be Required.</span>
																<span style="display:none;" class="notvalidemail membererror">Invalid Email Address...!</span>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 input-sm">Web Site</label>

															<div class="col-sm-9">
																<input type="text" class="form-control input-sm website" id="website" name="website"  placeholder="" value="<?php if(isset($party)){ echo $party[0]['website'];} ?>">
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div class="col-md-6">
										<form class="form-horizontal" id="partydetailform">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Party Type<span class="required-sign">&nbsp;*</span></label>
														<div class="col-sm-9 no-padding">
															<div class="col-sm-6">
																<?php
																$accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
																$accessDomestic  = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
																?>
																<?php if($accessExport == 1 && $accessDomestic == 1):?>
																<select name="party_type_1" id="party_type_1" class="form-control input-sm ">
																</select>
																<?php elseif($accessExport == 1):?>
																<input type="text" class="form-control input-sm" name="party_type_1_display" id="party_type_1_display" placeholder="" value="<?php if(isset($party)){ echo $party[0]['party_type_1'];}else{echo "Customer-Export";} ?>" readonly>
																<input type="hidden" name="party_type_1" id="party_type_1" value="5">
																<?php elseif($accessDomestic == 1):?>
																<input type="text" class="form-control input-sm" name="party_type_1_display" id="party_type_1_display" placeholder="" value="<?php if(isset($party)){ echo $party[0]['party_type_1'];}else{echo "Customer-Domestic";} ?>" readonly>
																<input type="hidden" name="party_type_1" id="party_type_1" value="6">
																<?php endif;?>
															</div>
															<div class="col-sm-6">
																<select name="party_type_2" id="party_type_2" class="form-control input-sm">
																</select>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Branch</label>
														<div class="col-sm-9 dispaly-flex">
															<select name="branch" id="branch" class="form-control input-sm">
															</select>
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Agent</label>
														<div class="col-sm-9 dispaly-flex">
															<select name="agent_id" id="agent_id" class="form-control input-sm">
															</select>
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
														<div class="col-sm-9">
															<select name="reference_id" id="reference_id" class="form-control input-sm">
															</select>
														</div>
														<div class="clearfix"></div>
														<label for="inputEmail3" class="col-sm-3 input-sm">Reference Detail</label>
														<div class="col-sm-9">
															<textarea class="form-control" placeholder="Reference" name="reference_description"><?php if(isset($party)){ echo $party[0]['reference_description'];} ?></textarea>
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">GST No.</label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" name="gst_no" id="gst_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['gst_no'];} ?>">
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">PAN No.</label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" name="pan_no" id="pan_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['pan_no'];} ?>">
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">UTR No.</label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" name="utr_no" id="utr_no" placeholder="" value="<?php if(isset($party)){ echo $party[0]['utr_no'];} ?>">
														</div>
													</div>

													<div class="form-group">
														<label for="party_created_date" class="col-sm-3 input-sm">Party Created Date</label>
														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" name="party_created_date" id="party_created_date" placeholder="" value="<?php if(isset($party_created_date) && strtotime($party_created_date) > 0){ echo date('d-m-Y',strtotime($party_created_date)); } else { echo date('d-m-Y'); } ?>" readonly="">
														</div>
													</div>

												</div>
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-12">
															<div class="row form-group">
																<a href="javascript:void(0);" style="display: none;" class="btn btn-info btn-xs pull-right button-width btn-print-envelope" target="_blank">Preview Envelope</a>
															</div>
															<div class="row form-group">
																<a href="javascript:void(0);" style="display: none;" class="btn btn-info btn-xs pull-right button-width btn-print-envelope">Print Envelope</a>
															</div>
														</div>
													</div>

												</div>
											</div>
										</form>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_2">
						<input type="hidden" name="party_id" class="party_id" id="party_id" value="<?php echo isset($party) && isset($party[0]['party_id']) ? $party[0]['party_id']:''?>">
						<input type="hidden" class="contact_person_id" id="contact_person_id" value="0">
						<div class="row">
							<div class="col-md-12">
								<fieldset class="scheduler-border">
									<legend class="scheduler-border text-primary text-bold"> Contact Person Info </legend>

									<form class="form-horizontal">

										<div class="row">
											<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Name<span class="required-sign">&nbsp;*</span></label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm contact_person_name" name="contact_person_name" id="contact_person_name" placeholder="">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Priority</label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm contact_person_priority" id="contact_person_priority" name="contact_person_priority" placeholder="" value="1">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Designation</label>

														<div class="col-sm-9 dispaly-flex">
															<select name="contact_person_designation_id" id="contact_person_designation_id" class="form-control input-sm">
															</select>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Department</label>
														<div class="col-sm-9 dispaly-flex">
															<select name="contact_person_department_id" id="contact_person_department_id" class="form-control input-sm">
															</select>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Mobile No.<span class="required-sign">&nbsp;*</span></label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm contact_person_mobile_no" name="contact_person_mobile_no" id="contact_person_mobile_no" placeholder="">
														</div>
													</div>
												</div>
												<!--
<div class="col-md-6">
<div class="form-group">
<label for="inputEmail3" class="col-sm-3 input-sm">Phone No.</label>

<div class="col-sm-9">
<input type="number" class="form-control input-sm contact_person_phone_no" name="contact_person_phone_no" id="contact_person_phone_no" placeholder="">
</div>
</div>
</div>
-->
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Email<span class="required-sign">&nbsp;*</span></label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm contact_person_email" name="contact_person_email" id="contact_person_email" placeholder="">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Fax No.</label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm contact_person_fax_no" name="contact_person_fax_no" id="contact_person_fax_no" placeholder="">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Note</label>

														<div class="col-sm-9">
															<input type="text" class="form-control input-sm contact_person_note" name="contact_person_note" id="contact_person_note" placeholder="">
														</div>
													</div>
												</div>
												<div class="col-md-6"><div class="form-group">&nbsp;</div></div>
												<div class="col-md-6 text-left">
													<!-- <button type="button" class="btn btn-info btn-xs btn_add_contact_person">Add Contact Person</button> -->
													<button type="button" class="btn btn-info btn-xs btn_add_contact_person">Add Contact Person</button>
												</div>
											</div>
											<div class="col-md-12">
												<br/>
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<table class="table custom-table tbl-contact-person">
														<thead>
															<tr>
																<th>Action</th>
																<th>Priority</th>
																<th>Name</th>
																<th>Designation</th>
																<th>Department</th>
																<th>Mobile No.</th>
																<th>Email</th>
															</tr>
														</thead>
														<tbody id="tbl-contact-person-body">
															<?php if(isset($party) && count($party[0]['contact_person']) > 0): ?>

															<?php foreach($party[0]['contact_person'] as $person): ?>
															<tr>
																<td><?php echo $person['priority']; ?></td>
																<td><?php echo $person['priority']; ?></td>
																<td><?php echo $person['name']; ?></td>
																<td><?php echo $this->app_model->get_id_by_val('designation','designation','designation_id',$person['designation_id'])?></td>
																<td><?php echo $this->app_model->get_id_by_val('department','department','department_id',$person['department_id'])?></td>
																<td><?php echo $person['mobile_no']; ?></td>
																<td><?php echo $person['email']; ?></td>
															</tr>
															<?php endforeach; ?>
															<?php endif; ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>

									</form>

								</fieldset>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_3">
						<input type="hidden" name="party_id" class="party_id" value="<?php echo isset($party) && isset($party[0]['party_id']) ? $party[0]['party_id']:''?>">
						<div class="row">
							<div class="col-md-12">
								<fieldset class="scheduler-border">
									<legend class="scheduler-border text-primary text-bold"> Delivery Address Info </legend>
									<form class="form-horizontal">
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-12">
													<div class="form-group">
														<label class="col-md-3 input-sm">
															Same As Party Details ?
														</label>
														<div class="col-md-4">
															<label class="no-padding">
																<input type="radio" name="address_work" id="address_work" class="address_work" value="1" checked="checked"> Yes &nbsp;&nbsp;
															</label>
															<label class="no-padding">
																<input type="radio" name="address_work" id="address_work" class="address_work" value="0"> No
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="delivery_party_name" class="col-sm-3 input-sm">Delivery Party Name</label>
														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" id="w_delivery_party_name" name="w_delivery_party_name" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_delivery_party_name'];} ?>">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Address</label>
														<div class="col-sm-9">
															<textarea class="form-control" id="w_address" rows="3" name="w_address"><?php if(isset($party)){ echo $party[0]['w_address'];} ?></textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">City</label>
														<div class="col-sm-9">
															<select name="w_city" id="w_city" class="form-control input-sm">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Pin Code</label>
														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" id="w_pin_code" name="w_pin_code" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_pin_code'];} ?>">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">State</label>
														<div class="col-sm-9">
															<select name="w_state" id="w_state" class="form-control input-sm">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Country</label>
														<div class="col-sm-9">
															<select name="w_country" id="w_country" class="form-control input-sm">
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Contact No.</label>
														<div class="col-sm-9">
															<?php /*
                                                                 * <input type="text" class="form-control input-sm" id="w_phone1" name="w_phone1" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_phone1'];} ?>">
                                                                */?>
															<textarea class="form-control" id="w_phone1" name="w_phone1" placeholder=""><?php if(isset($party)){ echo $party[0]['w_phone1'];} ?></textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Email</label>
														<div class="col-sm-9">
															<textarea class="form-control" id="w_email" name="w_email" placeholder=""><?php if(isset($party)){ echo $party[0]['w_email'];} ?></textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Web</label>
														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" id="w_web" name="w_web" placeholder="" value="<?php if(isset($party)){ echo $party[0]['w_web'];} ?>">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Note</label>
														<div class="col-sm-9">
															<textarea class="form-control" id="w_note" name="w_note" placeholder=""><?php if(isset($party)){ echo $party[0]['w_note'];} ?></textarea>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</form>

								</fieldset>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_4">
						<div class="row">
							<div class="col-md-12">
								<fieldset class="scheduler-border">
									<legend class="scheduler-border text-primary text-bold"> Communication Address Info </legend>
									<form class="form-horizontal">
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-12">
													<div class="form-group">
														<label class="col-md-3 input-sm">
															Same As Party Details ?
														</label>
														<div class="col-md-4">
															<label class="no-padding">
																<input type="radio" name="com_address" id="com_address" class="com_address" value="1" checked="checked"> Yes &nbsp;&nbsp;
															</label>
															<label class="no-padding">
																<input type="radio" name="com_address" id="com_address" class="com_address" value="0"> No
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="com_address_party_name" class="col-sm-3 input-sm">Communication Party Name</label>
														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" id="com_address_party_name" name="com_address_party_name" placeholder="" value="<?php if(isset($party)){ echo $party[0]['com_address_party_name'];} ?>">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Address</label>
														<div class="col-sm-9">
															<textarea class="form-control" id="com_address_address" rows="3" name="com_address_address"><?php if(isset($party)){ echo $party[0]['com_address_address'];} ?></textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">City</label>
														<div class="col-sm-9">
															<select name="com_address_city" id="com_address_city" class="form-control input-sm">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Pin Code</label>
														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" id="com_address_pin_code" name="com_address_pin_code" placeholder="" value="<?php if(isset($party)){ echo $party[0]['com_address_pin_code'];} ?>">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">State</label>
														<div class="col-sm-9">
															<select name="com_address_state" id="com_address_state" class="form-control input-sm">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Country</label>
														<div class="col-sm-9">
															<select name="com_address_country" id="com_address_country" class="form-control input-sm">
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Contact No.</label>
														<div class="col-sm-9">
															<textarea class="form-control" id="com_address_phone1" name="com_address_phone1" placeholder=""><?php if(isset($party)){ echo $party[0]['com_address_phone1'];} ?></textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Email</label>
														<div class="col-sm-9">
															<textarea class="form-control" id="com_address_email" name="com_address_email" placeholder=""><?php if(isset($party)){ echo $party[0]['com_address_email'];} ?></textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Web</label>
														<div class="col-sm-9">
															<input type="text" class="form-control input-sm" id="com_address_web" name="com_address_web" placeholder="" value="<?php if(isset($party)){ echo $party[0]['com_address_web'];} ?>">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 input-sm">Note</label>
														<div class="col-sm-9">
															<textarea class="form-control" id="com_address_note" name="com_address_note" placeholder=""><?php if(isset($party)){ echo $party[0]['com_address_note'];} ?></textarea>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</form>

								</fieldset>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_5">
						<div class="row">
							<div class="col-md-12">
								<fieldset class="scheduler-border">
									<legend class="scheduler-border text-primary text-bold"> Tax & Excise Info </legend>

								</fieldset>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
					<?php if(isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['edit'])){ ?>
					<div class="tab-pane" id="tab_6">
						<div class="row">
							<div class="col-md-12">
								<fieldset class="scheduler-border">
									<legend class="scheduler-border text-primary text-bold"> Login Details </legend>
									<div class="col-md-6">
										<div class="form-group">
											<label for="" class="col-sm-3 input-sm">
												Created By
											</label>
											<div class="col-sm-9">
												<input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="" readonly>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="form-group">
											<label for="" class="col-sm-3 input-sm">
												Created Date
											</label>
											<div class="col-sm-9">
												<input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="" readonly>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="form-group">
											<label for="" class="col-sm-3 input-sm">
												Updated By
											</label>
											<div class="col-sm-9">
												<input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="" readonly>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="form-group">
											<label for="" class="col-sm-3 input-sm">
												Updated Date
											</label>
											<div class="col-sm-9">
												<input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="" readonly>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
					<?php } ?>
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- nav-tabs-custom -->
		</div>
		<?php endif;?>
	</div>
	<section class="custom content-header">
		<h1>
			<?php if($accessAdd || $accessAdit):?>
			<button type="button" class="btn btn-info btn-xs pull-right btn_save_party">Save Party</button> 
			<?php endif;?>
			<?php if($access):?>
			<a href="<?=BASE_URL?>party/party_list" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Party List</a>
			<?php endif;?>
			<?php if($accessAdd):?>
			<a href="<?=BASE_URL?>party" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Add Party</a>
			<?php endif;?>
			<?php if($accessEnquiry):?>
			<a href="<?=BASE_URL?>enquiry/add" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Go To Enquiry</a> 
			<?php endif;?>
		</h1>
	</section>
	<div class="clearfix"></div>
</div>
<script>
	var PARTY_ID = 0;
	jQuery( document ).ready(function() {
		jQuery(".btn_save_party").click(function(){
			var party_name = document.forms["partycodeform"]["party_name"].value;
			if (party_name == "" || party_name == null) {
				jQuery(".party_namespan").show();
				show_notify(jQuery(".party_namespan").text(),false);
				jQuery("#party_name").focus();
				jQuery("a[href='#tab_1']").trigger("click");
				return false;
			}
			else{jQuery(".party_namespan").hide();}
		});
	});
	var ResponseStr = [];
	var table = '';
	$(document).ready(function(){
		$(".input-datepicker").datepicker({
			format: 'dd-mm-yyyy',
			todayBtn: "linked",
			todayHighlight: true,
			autoclose: true
		});
		set_contact_person_priority();
		<?php if($accessExport == 1 && $accessDomestic == 1):?>
		initAjaxSelect2($("#party_type_1"),"<?=base_url('app/sales_select2_source')?>");
		<?php endif;?>

		<?php if(empty($party)):?>
		setSelect2Value($("#party_type_2"),"<?=base_url('app/set_sales_type_select2_val_by_id/'.DEFAULT_PARTY_TYPE_2_ID)?>");
		setSelect2Value($("#agent_id"),"<?=base_url('app/set_agent_select2_val_by_id/'.DEFAULT_AGENT_ID)?>");
		<?php endif; ?>
		initAjaxSelect2($("#party_type_2"),"<?=base_url('app/sales_type_select2_source')?>");
		initAjaxSelect2($("#branch"),"<?=base_url('app/branch_select2_source')?>");
		setSelect2Value($("#branch"),"<?=base_url('app/set_branch_select2_val_by_id/'.DEFAULT_BRANCH_ID)?>");
		initAjaxSelect2($("#agent_id"),"<?=base_url('app/agent_select2_source')?>");
		initAjaxSelect2($("#reference_id"),"<?=base_url('app/reference_select2_source')?>");
		initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
		initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
		initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
		initAjaxSelect2($("#w_city"),"<?=base_url('app/city_select2_source')?>");
		initAjaxSelect2($("#w_state"),"<?=base_url('app/state_select2_source')?>");
		initAjaxSelect2($("#w_country"),"<?=base_url('app/country_select2_source')?>");
		initAjaxSelect2($("#com_address_city"),"<?=base_url('app/city_select2_source')?>");
		initAjaxSelect2($("#com_address_state"),"<?=base_url('app/state_select2_source')?>");
		initAjaxSelect2($("#com_address_country"),"<?=base_url('app/country_select2_source')?>");
		initAjaxSelect2($("#contact_person_designation_id"),"<?=base_url('app/designation_select2_source')?>");
		initAjaxSelect2($("#contact_person_department_id"),"<?=base_url('app/department_select2_source')?>");

		var $input = $('#refresh');
		$input.val() == 'yes' ? location.reload(true) : $input.val('yes');

		$(document).on("click",".btn-edit-contact-person",function(){
			var id = $(this).data('id');
			$.ajax({
				url: "<?=base_url();?>party/get-contact-person-by-id/"+id,
				type: "POST",
				data: null,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					if (data.success == true) {
						var cp_data = data.contact_person_data;
						$('.contact_person_id').val(cp_data.contact_person_id);
						$('.btn_add_contact_person').html('Save Contact Person');

						$('[name="contact_person_name"]').val(cp_data.name);
						if(cp_data.designation_id == 0){
							setSelect2Value($("#contact_person_designation_id"));
						}else{
							setSelect2Value($("#contact_person_designation_id"),'<?=base_url()?>app/set_designation_select2_val_by_id/'+cp_data.designation_id);
						}
						if(cp_data.department_id == 0){
							setSelect2Value($("#contact_person_department_id"));
						}else{
							setSelect2Value($("#contact_person_department_id"),'<?=base_url()?>app/set_department_select2_val_by_id/'+cp_data.department_id);
						}
						$('[name="contact_person_email"]').val(cp_data.email);
						$('[name="contact_person_note"]').val(cp_data.note);
						$('[name="contact_person_priority"]').val(cp_data.priority);
						$('[name="contact_person_mobile_no"]').val(cp_data.mobile_no);
						$('[name="contact_person_fax_no"]').val(cp_data.fax_no);
						console.log(cp_data);
					} else {
						show_notify("Something wrong!", false);
					}
				}
			});
		});
		$(document).on("click",".btn-delete-contact-person",function(){
			var ButtonObj = $(this);
			var is_confirm = confirm('Are you sure ?');
			if(is_confirm){
				if($('.contact_person_id').val() == ButtonObj.data('id')){
					$('.contact_person_id').val('0');
					$('.btn_add_contact_person').html('Add Contact Person');
					$('#tab_2').find('input').each(function(){
						if(!$(this).hasClass('party_id')){
							$(this).val('');
						}
					});
					setSelect2Value($("#contact_person_designation_id"));
					setSelect2Value($("#contact_person_department_id"));
				}
				$.ajax({
					url: "<?=base_url();?>party/delete-contact-person/"+ButtonObj.data('id'),
					type: "POST",
					data: null,
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function (data) {
						if (data.success == true) {
							ButtonObj.closest('tr').fadeOut('medium',function(){
								$(this).remove();
							});
							show_notify(data.message, true);
						} else {
							show_notify("Something wrong!", false);
						}
					}
				});
			}

		});

		$('#tab_3').find('input[type="text"],textarea,select').each(function(){
			$(this).attr('disabled','disabled');
		});
		$('#tab_4').find('input[type="text"],textarea,select').each(function(){
			$(this).attr('disabled','disabled');
		});

		$(document).on("click", ".delete_button", function() {
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if (value) {
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=party_id&table_name=party',
					success: function(data) {
						table.draw();
						show_notify('Deleted Successfully!', true);
					}
				});
			}
		});

		$(document).on('ifChanged','#address_work',function(){
			if($(this).val() == 1){
				$('#tab_3').find('input[type="text"],textarea,select').each(function(){
					$(this).attr('disabled','disabled');
					$(this).val('');
				});
			}else{
				$('#tab_3').find('input[type="text"],textarea,select').each(function(){
					$(this).removeAttr('disabled');
					//$(this).val('');
				});
			}
		});

		$(document).on('ifChanged','#address_work',function(){
			if($(this).val() == 1){
				$("#w_address").val($("#main_address").val());

				var city_html = $("#city").html();
				$("#w_city").html('');
				$("#w_city").html(city_html);
				$("#w_city").val($("#city").val());
				$("#s2id_w_city span:first").html($("#city option:selected").text());

				var state_html = $("#state").html();
				$("#w_state").html('');
				$("#w_state").html(state_html);
				$("#w_state").val($("#state").val());
				$("#s2id_w_state span:first").html($("#state option:selected").text());

				var country_html = $("#country").html();
				$("#w_country").html('');
				$("#w_country").html(country_html);
				$("#w_country").val($("#country").val());
				$("#s2id_w_country span:first").html($("#country option:selected").text());

				$("#w_delivery_party_name").val($("#party_name").val());
				$("#w_pin_code").val($("#pincode").val());
				$("#w_web").val($("#website").val());
				$("#w_phone1").val($("#phone_no").val());
				$("#w_email").val($("#email_id").val());
			}
		});

		$(document).on("click",".address-works-tab",function(){
			if($(".address_work:checked").size() > 0)
			{
				var $result = $(".address_work:checked").val();
				if($result == 1)
				{
					var city_html = $("#city").html();
					$("#w_city").html('');
					$("#w_city").html(city_html);
					$("#w_city").val($("#city").val());
					$("#s2id_w_city span:first").html($("#city option:selected").text());

					var state_html = $("#state").html();
					$("#w_state").html('');
					$("#w_state").html(state_html);
					$("#w_state").val($("#state").val());
					$("#s2id_w_state span:first").html($("#state option:selected").text());

					var country_html = $("#country").html();
					$("#w_country").html('');
					$("#w_country").html(country_html);
					$("#w_country").val($("#country").val());
					$("#s2id_w_country span:first").html($("#country option:selected").text());

					$("#w_web").val($("#website").val())
					$("#w_phone1").val($("#phone_no").val())
					$("#w_email").val($("#email_id").val())

					$("#w_address").val($("#main_address").val());
					$("#w_delivery_party_name").val($("#party_name").val());
					$("#w_pin_code").val($("#pincode").val());
					$("#w_web").val($("#website").val());
					$("#w_phone1").val($("#phone_no").val());
					$("#w_email").val($("#email_id").val());
				}
			}    

		});

		$(document).on('ifChanged','.com_address',function(){
			if($(this).val() == 1){
				$('#tab_4').find('input[type="text"],textarea,select').each(function(){
					$(this).attr('disabled','disabled');
					$(this).val('');
				});
			}else{
				$('#tab_4').find('input[type="text"],textarea,select').each(function(){
					$(this).removeAttr('disabled');
					//$(this).val('');
				});
			}
		});

		$(document).on('ifChanged','.com_address',function(){
			if($(this).val() == 1){
				$("#com_address_address").val($("#main_address").val());

				var city_html = $("#city").html();
				$("#com_address_city").html('');
				$("#com_address_city").html(city_html);
				$("#com_address_city").val($("#city").val());
				$("#s2id_com_address_city span:first").html($("#city option:selected").text());

				var state_html = $("#state").html();
				$("#com_address_state").html('');
				$("#com_address_state").html(state_html);
				$("#com_address_state").val($("#state").val());
				$("#s2id_com_address_state span:first").html($("#state option:selected").text());

				var country_html = $("#country").html();
				$("#com_address_country").html('');
				$("#com_address_country").html(country_html);
				$("#com_address_country").val($("#country").val());
				$("#s2id_com_address_country span:first").html($("#country option:selected").text());

				$("#com_address_delivery_party_name").val($("#party_name").val());
				$("#com_address_pin_code").val($("#pincode").val());
				$("#com_address_web").val($("#website").val());
				$("#com_address_phone1").val($("#phone_no").val());
				$("#com_address_email").val($("#email_id").val());
			}
		});

		$(document).on("click",".com-address-tab",function(){
			if($(".com_address:checked").size() > 0){
				var $result = $(".com_address:checked").val();
				if($result == 1){
					var city_html = $("#city").html();
					$("#com_address_city").html('');
					$("#com_address_city").html(city_html);
					$("#com_address_city").val($("#city").val());
					$("#s2id_com_address_city span:first").html($("#city option:selected").text());

					var state_html = $("#state").html();
					$("#com_address_state").html('');
					$("#com_address_state").html(state_html);
					$("#com_address_state").val($("#state").val());
					$("#s2id_com_address_state span:first").html($("#state option:selected").text());

					var country_html = $("#country").html();
					$("#com_address_country").html('');
					$("#com_address_country").html(country_html);
					$("#com_address_country").val($("#country").val());
					$("#s2id_com_address_country span:first").html($("#country option:selected").text());

					$("#com_address_web").val($("#website").val())
					$("#com_address_phone1").val($("#phone_no").val())
					$("#com_address_email").val($("#email_id").val())

					$("#com_address_address").val($("#main_address").val());
					$("#com_address_party_name").val($("#party_name").val());
					$("#com_address_pin_code").val($("#pincode").val());
					$("#com_address_web").val($("#website").val());
					$("#com_address_phone1").val($("#phone_no").val());
					$("#com_address_email").val($("#email_id").val());
				}
			}    
		});

		$(document).on('click','.feed-party-detail',function(){
			var party_id = $(this).data('party_id');
			window.location = '<?=BASE_URL?>'+'party/edit/'+party_id;
			/*$.get('<?=BASE_URL?>party/load_party_detail/'+party_id,null,function(data){
                $('.party_id').val(data);
            });
            $("#party_list_modal").modal('hide');
            */
		});

		$(document).on('click','.btn_save_party',function(){

			if ($('#party_type_1').val() == "" || $('#party_type_1').val() == null){
				show_notify('Party Type is required!', false);
				return false;
			}

			if($('#reference_id').val() == "" || $('#reference_id').val() == null){
				show_notify('Reference is required!', false);
				return false;
			}
			if($.trim($("#contact_person_name").val()) != '' ){
				if($.trim($("#contact_person_email").val()) == '' && $.trim($("#contact_person_mobile_no").val()) == ''){
					show_notify('Contact person email or mobile is required.',false);
					return false;
				}
			}
			var DataStr = $('#tab_1 :input').serialize();
			DataStr += '&';
			DataStr += $('#tab_5 :input').serialize();
			DataStr += '&';
			DataStr += $('#tab_3 :input').serialize();
			DataStr += '&';
			DataStr += $('#tab_4 :input').serialize();
			console.log(DataStr);
			var party_id = savePartyDetail(DataStr);

		});

		function check_form_validations(){
			var $status = 1;

			var party_name = document.forms["partycodeform"]["party_name"].value;
			if (party_name == "" || party_name == null) {
				jQuery(".party_namespan").show();
				show_notify(jQuery(".party_namespan").text(),false);
				jQuery("#party_name").focus();
				jQuery("a[href='#tab_1']").trigger("click");
				return 0;
			}
			else{jQuery(".party_namespan").hide();}

			return $status;
		}

		$(document).on('click','.btn_add_contact_person',function(){
			var party_id = $('.party_id[name="party_id"]:first').val();

			// check main validation
			var $status = check_form_validations();

			if($status == 0)
				return false;

			// alert("OK");
			if($.trim($("#contact_person_name").val()) == ''){
				show_notify('Contact name is required.',false);
				return false;
			}
			if($.trim($("#contact_person_email").val()) == '' && $.trim($("#contact_person_mobile_no").val()) == ''){
				show_notify('Contact person email or mobile is required.',false);
				return false;
			}

			if($('#reference_id').val() == ''){
				show_notify('Reference is required!', false);
				return false;
			}

			if(party_id > 0){
				if($("#contact_person_name").val() != ''){
					saveContactPerson($('#tab_2 :input').serialize());
				}
			}else{
				var DataStr = $('#tab_1 :input').serialize();
				DataStr += '&';
				DataStr += $('#tab_5 :input').serialize();
				DataStr += '&';
				DataStr += $('#tab_3 :input').serialize();
				DataStr += '&';
				DataStr += $('#tab_4 :input').serialize();
				$.ajax({
					method:"post",
					url: "<?=BASE_URL?>party/save_party_detail/",
					data: DataStr,
					success: function(data) {

						data = JSON.parse(data);

						if(data.status == 1){
							$('.party_id[name="party_id"]').each(function(){
								$(this).val(data.id);
							});
							if($("#contact_person_name").val() != ''){
								saveContactPerson($('#tab_2 :input').serialize());
							}                            
						}else{
							show_notify(data.msg,false);
							if(data.email_error == 1){
								jQuery("a[href='#tab_1']").trigger("click");
								jQuery("#email_id").focus();
							}
						}                        
					}
				});
			}
		});
	});

	$('#city,#w_city,#com_city').on('select2:select', function (e) {
		var data = e.params.data;
		var dom_id = e.currentTarget.id;
		console.log(dom_id);
		var id = data.id;
		if(id != '' && id != null){
			$.ajax({
				type: "POST",
				url: '<?=base_url();?>party/ajax_get_state/'+id,
				data: id='cat_id',
				success: function(data){
					var json = $.parseJSON(data);
					//console.log(data);
					if (json['state_id']) {
						if(dom_id == 'city')
							setSelect2Value($("select[name='state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
						if(dom_id == 'w_city')
							setSelect2Value($("select[name='w_state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
						if(dom_id == 'com_city')
							setSelect2Value($("select[name='com_state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
					}
					if (json['country_id']) {
						if(dom_id == 'city'){
							setSelect2Value($("select[name='country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
						}
						if(dom_id == 'w_city'){
							setSelect2Value($("select[name='w_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
						}
						if(dom_id == 'com_city'){
							setSelect2Value($("select[name='com_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
						}
					}					
				},
			});
		}
	});

	$('#state,#w_state,#com_state').on('select2:select', function (e) {
		var data = e.params.data;
		var dom_id = e.currentTarget.id;
		var id = data.id;
		if(id != '' && id != null){
			$.ajax({
				type: "POST",
				url: '<?=base_url();?>party/ajax_get_country/'+id,
				data: id='cat_id',
				success: function(data){
					var json = $.parseJSON(data);
					//console.log(data);
					if (json['country_id']) {
						if(dom_id == 'state'){
							setSelect2Value($("select[name='country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
						}
						if(dom_id == 'w_state'){
							setSelect2Value($("select[name='w_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
						}
						if(dom_id == 'com_state'){
							setSelect2Value($("select[name='com_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
						}
					}

				},
			});
		}
	});

	function fill_party_data(PARTY_ID){
		$("#ajax-loader").show();
		<?php if (isset($_GET['view'])){ ?>
		var ajax_url = '<?=base_url();?>party/get-party-by-id/' + PARTY_ID + '/view';
		<?php } else { ?>
		var ajax_url = '<?=base_url();?>party/get-party-by-id/' + PARTY_ID;
		<?php } ?>
		$.ajax({
			url: ajax_url,
			type: "POST",
			data: null,
			contentType: false,
			cache: false,
			processData: false,
			dataType: 'json',
			success: function (party_data) {
				//console.log(party_data);debugger;
				$("#ajax-loader").hide();
				$("#party_list_modal").modal("hide");
				$.each(party_data,function(index,value){
					if($("[name='"+index+"']").length > 0) {
						if ($("[name='" + index + "']").is(':radio')) {
							if ($("[name='" + index + "'][value='" + value + "']").length > 0) {
								$("[name='" + index + "'][value='" + value + "']").iCheck('check');
								$("input[type='radio'][value='" + value + "'].address_work").trigger('ifChanged');
								$("input[type='radio'][value='" + value + "'].com_address").trigger('ifChanged');
								$('a[href="#tab_1"]').trigger('click');
							}
						} else if ($("[name='" + index + "']").is(':input')) {
							$("[name='" + index + "']").val(value);
						}
					}
				})
				$("#created_by").val(party_data.created_by_name);
				$("#created_at").val(party_data.created_at);
				$("#updated_by").val(party_data.updated_by_name);
				$("#updated_at").val(party_data.updated_at);
				$('#tab_2').find('input').each(function() {
					if($(this).attr('id') != "party_id") {
						$(this).val('');
					}
				});

				$('.party_id').each(function(){
					$(this).val(party_data.party_id);
				});

				$(".btn-print-envelope").attr('href','<?=base_url()?>party/print-envelope/'+party_data.party_id);
				$(".btn-print-envelope").attr('style','display: block;');

				setSelect2Value($("select[name='city']"),'<?=base_url()?>app/set_city_select2_val_by_id/'+party_data.city_id);
				setSelect2Value($("select[name='state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+party_data.state_id);
				setSelect2Value($("select[name='country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+party_data.country_id);

				setSelect2Value($("select[name='w_city']"),'<?=base_url()?>app/set_city_select2_val_by_id/'+party_data.w_city);
				setSelect2Value($("select[name='w_state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+party_data.w_state_id);
				setSelect2Value($("select[name='w_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+party_data.w_country_id);

				setSelect2Value($("select[name='com_address_city']"),'<?=base_url()?>app/set_city_select2_val_by_id/'+party_data.com_address_city);
				setSelect2Value($("select[name='com_address_state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+party_data.com_address_state_id);
				setSelect2Value($("select[name='com_address_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+party_data.com_address_country_id);

				setSelect2Value($("select[name='reference_id']"),'<?=base_url()?>app/set_reference_select2_val_by_id/'+party_data.reference_id);

				<?php if($accessExport == 1 && $accessDomestic == 1){?>
				setSelect2Value($("select[name='party_type_1']"),'<?=base_url()?>app/set_sales_select2_val_by_id/'+party_data.party_type_1);
				<?php } else {?>
				$("#party_type_1").val(party_data.party_type_1);
				<?php } ?>

				setSelect2Value($("select[name='party_type_2']"),'<?=base_url()?>app/set_sales_type_select2_val_by_id/'+party_data.party_type_2);

				setSelect2Value($("select[name='branch']"),'<?=base_url()?>app/set_branch_select2_val_by_id/'+party_data.branch_id);

				setSelect2Value($("select[name='agent_id']"),'<?=base_url()?>app/set_agent_select2_val_by_id/'+party_data.agent_id);

				$('tbody#tbl-contact-person-body').html(party_data.contact_persons_html);

				set_contact_person_priority();

				setSelect2Value($("select[name='state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+party_data.state_id);
				setSelect2Value($("select[name='country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+party_data.country_id);
				setSelect2Value($("select[name='w_state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+party_data.w_state);
				setSelect2Value($("select[name='w_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+party_data.w_country);
				setSelect2Value($("select[name='com_address_state']"),'<?=base_url()?>app/set_state_select2_val_by_id/'+party_data.com_address_state);
				setSelect2Value($("select[name='com_address_country']"),'<?=base_url()?>app/set_country_select2_val_by_id/'+party_data.com_address_country);

			}
		});
	}

	function savePartyDetail(DataStr){
		var ReturnData = 0;
		$.ajax({
			method:"post",
			url: "<?=BASE_URL?>party/save_party_detail/",
			data: DataStr,
			success: function(data) {

				data = JSON.parse(data);

				if(data.status == 1){
					$('.party_id[name="party_id"]').each(function(){
						$(this).val(data.id);
					});
					show_notify('Saved Successfully!',true);
					//saveAddressWorks();
					var DataStrContactPerson = $('#tab_2 :input').serialize();
					if($("#contact_person_name").val() != ''){
						saveContactPerson(DataStrContactPerson);
					}
					table.draw();
				}else{
					show_notify(data.msg,false);
					if(data.email_error == 1){
						jQuery("a[href='#tab_1']").trigger("click");
						jQuery("#email_id").focus();
					} 
					if(data.phone_error == 1){
						jQuery("a[href='#tab_1']").trigger("click");
						jQuery("#phone_no").focus();
					}
				}
			}
		});
	}

	function check_priority_is_unique(contact_person_id = ''){
		var DataStr = "party_id="+$("#party_id").val()+"&priority="+$("#contact_person_priority").val()+"&contact_person_id="+contact_person_id;
		var response = '1';
		$.ajax({
			url: "<?=base_url()?>party/check_priority_is_unique/",
			type: "POST",
			data: DataStr,
			async:false
		}).done(function(data) {
			response = data;
		});
		return response;
	}

	function saveContactPerson(DataStr){
		var contact_person_id = $('.contact_person_id').val();
		var prio = $('[name="contact_person_priority"]').val() ? $('[name="contact_person_priority"]').val() : 0;
		if(contact_person_id == 0){

			if($("#contact_person_email").val() != ''){
				/*if(!isValidEmailAddress($("#contact_person_email").val())){
                    show_notify($("#contact_person_email").val()+' is not valid email.',false);
                    return false;
                }*/
			}

			var success_status = check_priority_is_unique();
			if(success_status == 0){
				show_notify('Priority you can not set same.');
				return false;
			}

			$.ajax({
				method:"post",
				url: "<?=BASE_URL?>party/add_contact_person/",
				data: DataStr,
				dataType:'json',
				success: function(data) {
					var row_html = '<tr>';
					row_html += '<td>' +
						'<a class="btn btn-xs btn-danger btn-delete-contact-person" href="javascript:void(0);" data-id="'+data.contact_person_id+'"><i class="fa fa-remove"></i></a> ' +
						' <a class="btn btn-xs btn-primary btn-edit-contact-person" href="javascript:void(0);" data-id="'+data.contact_person_id+'"><i class="fa fa-edit"></i></a></td>';
					row_html += '<td>'+prio+'</td>';
					row_html += '<td>'+$('[name="contact_person_name"]').val()+'</td>';
					if($('[name="contact_person_designation_id"]').val() == ''){
						row_html += '<td> </td>';
					}else{
						if($('#select2-contact_person_designation_id-container').attr('title') == null || $('#select2-contact_person_designation_id-container').attr('title') == ''){
							row_html += '<td></td>';
						} else {
							row_html += '<td>'+$('#select2-contact_person_designation_id-container').attr('title')+'</td>';
						}
					}
					if($('[name="contact_person_department_id"]').val() == ''){
						row_html += '<td> </td>';
					}else{
						if($('#select2-contact_person_department_id-container').attr('title') == null || $('#select2-contact_person_department_id-container').attr('title') == ''){
							row_html += '<td></td>';
						} else {
							row_html += '<td>'+$('#select2-contact_person_department_id-container').attr('title')+'</td>';
						}
					}
					row_html += '<td>'+$('[name="contact_person_mobile_no"]').val()+'</td>';
					row_html += '<td>'+$('[name="contact_person_email"]').val()+'</td>';;
					row_html += '</tr>';
					var count_row = $("#tbl-contact-person-body").children('tr').length;
					if(count_row == 0){
						$('.tbl-contact-person > tbody').append(row_html);
					}
					else {
						$('.tbl-contact-person > tbody > tr:last ').after(row_html);
					}
					$('#tab_2').find('input').each(function(){
						if(!$(this).hasClass('party_id')){
							$(this).val('');
						}
					});
					setSelect2Value($("#contact_person_designation_id"));
					setSelect2Value($("#contact_person_department_id"));
				}
			});
		}else{

			/*if($("#contact_person_email").val() != ''){
                if(!isValidEmailAddress($("#contact_person_email").val())){
                    show_notify($("#contact_person_email").val()+' is not valid email.',false);
                    return false;
                }
            }*/

			var success_status = check_priority_is_unique(contact_person_id);
			if(success_status == 0){
				show_notify('Priority you can not set same.');
				return false;
			}

			$.ajax({
				method:"post",
				url: "<?=BASE_URL?>party/edit_contact_person/"+contact_person_id,
				data: DataStr,
				dataType:'json',
				success: function(data) {
					$('.contact_person_id').val('0');
					$('.btn_add_contact_person').html('Add Contact Person');
					if (data.success == true) {
						show_notify(data.message, true);
						var row_html = '';
						row_html += '<td>' +
							'<a class="btn btn-xs btn-danger btn-delete-contact-person" href="javascript:void(0);" data-id="'+contact_person_id+'"><i class="fa fa-remove"></i></a> ' +
							' <a class="btn btn-xs btn-primary btn-edit-contact-person" href="javascript:void(0);" data-id="'+contact_person_id+'"><i class="fa fa-edit"></i></a></td>';
						row_html += '<td>'+$('[name="contact_person_priority"]').val()+'</td>';
						row_html += '<td>'+$('[name="contact_person_name"]').val()+'</td>';
						if($('[name="contact_person_designation_id"]').val() == ''){
							row_html += '<td> </td>';
						}else{
							row_html += '<td>'+$('[name="contact_person_designation_id"]').select2('data')[0].text+'</td>';
						}
						if($('[name="contact_person_department_id"]').val() == ''){
							row_html += '<td> </td>';
						}else{
							row_html += '<td>'+$('[name="contact_person_department_id"]').select2('data')[0].text+'</td>';
						}
						row_html += '<td>'+$('[name="contact_person_mobile_no"]').val()+'</td>';
						row_html += '<td>'+$('[name="contact_person_email"]').val()+'</td>';
						$('a[data-id="'+contact_person_id+'"]').closest('tr').html(row_html);
						$('#tab_2').find('input').each(function(){
							if(!$(this).hasClass('party_id')){
								$(this).val('');
							}
						});
						setSelect2Value($("#contact_person_designation_id"));
						setSelect2Value($("#contact_person_department_id"));
					} else {
						show_notify('Something wrong', false);
					}
				}
			});
		}
		set_contact_person_priority();
	}

	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	}

	function saveAddressWorks(){
		$.ajax({
			method:"post",
			url: "<?=BASE_URL?>party/save_address_works/",
			data: $('#tab_3 :input').serialize(),
			dataType:'json',
			success: function(data) {

			}
		});
	}

	function split(val) {
		return val.split(/,\s*/);
	}

	function extractLast(term) {
		return split(term).pop();
	}

	function set_contact_person_priority()
	{
		var party_id = $("#party_id").val();
		if(party_id == 0 || party_id == ''){
			$("#contact_person_priority").val(1);
		}else{
			$.ajax({
				type: "POST",
				url: '<?=base_url();?>party/get_max_priority/'+party_id,
				success: function(data){
					$("#contact_person_priority").val(data);
				}
			});
		}
	}

	function setStateAndCountryByCity(city_id, id)
	{
		$.ajax({
			url: "<?=BASE_URL?>party/fetch_state_country_by_city",
			dataType: "json",
			data: {city_id : city_id},
			success: function(data) 
			{
				// data = JSON.parse(data);
				if(id == "city")
				{
					$("#partydetailform #state").val(data.stateName);
					$("#partydetailform #country").val(data.countryName);                    
				}
				else
				{
					$("#w_state").val(data.stateName);
					$("#w_country").val(data.countryName);                    
				}    
			},
			error: function(error){
			}    
		});
	}

	function ValidateEmail(email_id) {
		var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		return expr.test(email_id);
	};

	<?php if (isset($_GET['view'])){ ?>
	$(window).load(function () {
		display_as_a_viewpage();
	});
	<?php } ?>
</script>
