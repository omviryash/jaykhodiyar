<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
        <h1>
			<small class="text-primary text-bold">Party Outstanding</small>
			<?php
			$accessAdd  = $this->app_model->have_access_role(PARTY_MODULE_ID, "add"); 
			?>
			<?php if($accessAdd):?>
			<a href="<?=base_url('party/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
			<?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
							<div style="margin: 10px;">
								<div class="col-md-12">
									<form id="frm_outstanding_party_filter">
										<div class="col-md-12">
											<div class="form-group col-md-2">
												<select id="outstanding_sort">
													<option value=0>All</option>
													<option value=1 selected>Debit Balance Only </option>
													<option value=3>0 Balance</option>
													<option value=4>Credit Only</option>
												</select>
											</div>
											<div class="form-group col-md-6">
												<div class="form-group col-md-6">
													<input type="checkbox" name="enquiry_created" id="enquiry_created" value="1" class="chk_filter"><label for="enquiry_created">Enquiry Created</label>
												</div>

												<div class="form-group col-md-6">
													<input type="checkbox" name="quotation_created" id="quotation_created" value="1" class="chk_filter"><label for="quotation_created">Quotation Created</label>
												</div>

												<div class="form-group col-md-6">
													<input type="checkbox" name="proforma_invoice_created" id="proforma_invoice_created" value="1" class="chk_filter"><label for="proforma_invoice_created">Proforma Invoice Created</label>
												</div>

												<div class="form-group col-md-6">
													<input type="checkbox" name="order_created" id="order_created" value="1" class="chk_filter"><label for="order_created">Order Created</label>
												</div>

												<div class="form-group col-md-6">
													<input type="checkbox" name="challan_created" id="challan_created" value="1" class="chk_filter"><label for="challan_created">Challan Created</label>
												</div>

												<div class="form-group col-md-6">
													<input type="checkbox" name="invoice_created" id="invoice_created" value="1" class="chk_filter"><label for="invoice_created">Invoice Created</label>
												</div>
											</div>
										</div>
									</form>
									<table id="party_list_datatable" class="table custom-table agent-table" width="100%">
										<thead>
											<tr>											
												<th>Party Code</th>
												<th>Party Name</th>
												<th>Outstanding Balance</th>												
											</tr>
										</thead>
										<tbody>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="panel-heading">
		<?php if($accessAdd):?>
		<a href="<?=base_url('party/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
		<?php endif;?>
	</div>
</div>

<script>
	var table;	
	$(document).ready(function(){
		table = $('#party_list_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[2, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('party/party_list_outstanding_datatable')?>",
                "type": "POST",
                "data": function(d){
					d.outstanding_sort = $("#outstanding_sort").val();
					d.enquiry_created = $("#enquiry_created:checked").val();
					d.quotation_created = $("#quotation_created:checked").val();
					d.proforma_invoice_created = $("#proforma_invoice_created:checked").val();
					d.order_created = $("#order_created:checked").val();
					d.challan_created = $("#challan_created:checked").val();
					d.invoice_created = $("#invoice_created:checked").val();
				},
            },
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
            }
        });
		$(document).on('change',".chk_filter",function(){
			table.draw();
		});

        $("#outstanding_sort").change(function() {
			table.draw();			
		});
        
        
		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=party_id&table_name=party',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('party/party_list') ?>";
					}
				});
			}
		});
	});
</script>
