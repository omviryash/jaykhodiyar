<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Party Type</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Party Type Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table party-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Party Type</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                    	<?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td><a href="<?= base_url('party/party_type2/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('party/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a></td>
						                                            <td><?=$row->type ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($id) && !empty($id)){ ?>Edit 
														<?php } else { ?>Add 
														<?php } ?> Party Type
													</div>
													<div style="margin:20px">	
														<form method="POST" 
															<?php if(isset($id) && !empty($id)){ ?>
															action="<?=base_url('party/update_party_type2') ?>" 
															<?php } else { ?>
															action="<?=base_url('party/add_party_type2') ?>" 
															<?php } ?> id="form_party_type2">
															<?php if(isset($id) && !empty($id)){ ?>
																<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>"
															<?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-3 input-sm">Party Type</label>
																<div class="col-sm-7">
			                                                        <input type="text" class="form-control input-sm" name="type" id="type" value="<?php echo $type; ?>" >
			                                                    </div>
															</div>
															<button type="submit" class="btn btn-info btn-block btn-xs">
															<?php if(isset($id) && !empty($id)){ ?>Edit 
															<?php } else { ?>Add 
															<?php } ?> Party Type</button>
														</form>
													</div>
												</div>                                            
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable();

        $("#form_party_type2").on("submit",function(e){
			e.preventDefault();

			<?php if(isset($id) && !empty($id)){ ?>
				var success_status = check_is_unique('party_type_2','type',$("#type").val(),'id','<?=$id?>');											
			<?php } else { ?>
			 	var success_status = check_is_unique('party_type_2','type',$("#type").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.unique-error').length > 0){
					$("p.unique-error").text('Party Type 2 already exist!');
				}else{
					$("#type").after("<p class='text-danger unique-error'>Party Type 2 already exist!</p>");
				}
				return false;	
			}else{
				$("p.unique-error").text(' ');
			}

			var url = '<?php echo base_url('party/delete/') ?>';
			var value = $("#type").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.type+'</td>';
		                TableRow += '</tr>';
		                $('.party-table > tbody > tr:last ').after(TableRow);
		                $("#form_party_type2")[0].reset();
		                show_notify('Saved Successfully!',true);*/
		                window.location.href = "<?php echo base_url('party/party_type2') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=party_type_2',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('party/party_type2') ?>";
					}
				});
			}
		});
		
    });
</script>
