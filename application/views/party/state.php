
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		$isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "delete");
		$isAdd = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "add");
		$btn_disable = '';
		?>
		<h1>
			<small class="text-primary text-bold">State</small>
		</h1>
		<!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="col-md-12">
						<div class="col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading">
									State Detail
								</div>
								<div style="margin: 10px;">
									<table id="example1" class="table custom-table state-table">
										<thead>
											<tr>
												<th>Action</th>
												<th>State</th>
												<th>Country</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(!empty($results)) {
												foreach ($results as $row) {
											?>
											<tr>
												<td>
													<?php if($isEdit) { ?>
													<a href="<?= base_url('party/state/'.$row->state_id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
													<?php } if($isDelete) { ?>
													<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('party/delete/'.$row->state_id);?>"><i class="fa fa-trash"></i></a>
													<?php } ?>
												</td>
												<td><?=$row->state ?></td>
												<td><?= $this->crud->get_column_value_by_id('country','country',array('country_id'=>$row->country_id)); ?></td>
											</tr>
											<?php 
												} }
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<?php if($isAdd || $isEdit) { ?>
						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-heading clearfix">
									<?php if(isset($state_id) && !empty($state_id)){ ?>Edit 
									<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
									<?php } ?> State
								</div>
								<?php $dropdown_data = $this->crud->get_all_records('country','country','ASC'); ?>
								<div style="margin:20px">	
									<form method="POST"
										  <?php if(isset($state_id) && !empty($state_id)){ ?>
										  action="<?=base_url('party/update_state') ?>" 
										  <?php } else { ?>
										  action="<?=base_url('party/add_state') ?>" 
										  <?php } ?> id="form_state">
										<?php if(isset($state_id) && !empty($state_id)){ ?>
										<input type="hidden" class="form-control input-sm" name="state_id" id="state_id" value="<?php echo $state_id; ?>" >
										<?php } ?>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-4 input-sm">Select Country</label>
											<div class="col-sm-7">
												<select class="form-control input-sm select2" name="country_id" id="country_id" <?php echo $btn_disable;?>>
													<?php if(empty($state_id)){ ?>
													<option value="" selected>-select-</option>
													<?php } ?>
													<?php 
													 if(!empty($dropdown_data)) {
														 foreach ($dropdown_data as $dropdown_row) {
													?>
													<option value="<?=$dropdown_row->country_id ?>"
															<?php if(isset($state_id) && !empty($state_id)){ 
														if($dropdown_row->country_id == $country_id)
														{
															echo 'selected';
														}
													} ?>
															><?=$dropdown_row->country ?></option>
													<?php } } ?>			
												</select>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-4 input-sm">State<span class="required-sign">*</span></label>
											<div class="col-sm-7">
												<input type="text" class="form-control input-sm" id="state" name="state" value="<?php echo $state_name; ?>" <?php echo $btn_disable;?>>
											</div>
										</div>

										<?php if(isset($state_id) && !empty($state_id)){ ?>
										<button type="submit" class="btn btn-info btn-block btn-xs">Edit State</button>
										<?php } else { ?>
										<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add State</button>
										<?php } ?>
									</form>
								</div>
							</div>                                            
						</div>
						<?php } ?>

					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>
<script>
	$(document).ready(function(){
		$("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

		$("#form_state").on("submit",function(e){
			e.preventDefault();

			if($("#country_id").val() == ""){
				show_notify('Fill value Country.', false);
				return false;
			}
			if($("#state").val() == ""){
				show_notify('Fill value State.', false);
				return false;
			}
			<?php if(isset($state_id) && !empty($state_id)){ ?>
			var success_status = check_is_unique('state','state',$("#state").val(),'state_id','<?=$state_id?>','country_id',$("#country_id").val());										
			<?php } else { ?>
			var success_status = check_is_unique('state','state',$("#state").val(),'','','country_id',$("#country_id").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.unique-error').length > 0){
					$("p.unique-error").text('State already exist!');
				}else{
					$("#state").after("<p class='text-danger unique-error'>State already exist!</p>");
				}
				return false;	
			}else{
				$("p.unique-error").text(' ');
			}

			var url = '<?php echo base_url('party/delete/') ?>';
			var value = $("#state").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.state_id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.state+'</td>';
		                TableRow += '<td>'+data.country+'</td>';
		                TableRow += '</tr>';
		                $('.state-table > tbody > tr:last ').after(TableRow);
		                $("#form_state")[0].reset();
		                show_notify('Saved Successfully!',true);*/
						window.location.href = "<?php echo base_url('party/state') ?>";
					}
				});
			}
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=state_id&table_name=state',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('party/state') ?>";
					}
				});
			}
		});

	});
</script>
