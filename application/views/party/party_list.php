<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <small class="text-primary text-bold">Party List</small>
			<?php $accessAdd  = $this->app_model->have_access_role(PARTY_MODULE_ID, "add");  ?>
			<?php if($accessAdd):?>
				<a href="<?=base_url('party/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
			<?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <?php if($this->app_model->have_access_role(PARTY_MODULE_ID, "is_management")){ ?>
                        <div class="col-md-4">
                            <label class="pull-left">Party Status: &nbsp;</label>
                            <select name="party_status" id="party_status" class="form-control input-sm select2" style="width: 60%;">
                                <option value="all">All</option>
                                <option value="1" selected="selected" >Active</option>
                                <option value="0" >Inactive</option>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="col-md-4">
                        <label class="pull-left">Search Contact No: &nbsp;</label>
                        <input type="text" name="mobile_no" id="party_mobile" class="form-control num_only input-sm" maxlength="10" style="width: 60%;">
                    </div>
                    <div class="col-md-4">
                        <label class="pull-left">Search Contect Email: &nbsp;</label>
                        <input type="text" name="email_id" id="party_email" class="form-control input-sm" style="width: 60%;">
                    </div>
                    <br>
                    <br>
                    <table id="party_list_datatable" class="table custom-table agent-table" width="100%">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Created Date</th>
                                <th>Party Code</th>
                                <th>Party Name</th>
                                <th>Contact No. </th>
                                <th>Email Id</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                                <th>Created Person</th>
                                <th>Current Person</th>
                            </tr>
                        </thead>
                        <tbody>
                       </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="panel-heading">
		<?php if($accessAdd):?>
		<a href="<?=base_url('party/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
		<?php endif;?>
	</div>
</div>
<div class="modal fade" id="transfer_party_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Transfer To</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="transfer_party_id" id="transfer_party_id" />
                <input type="hidden" name="transfer_from" id="transfer_from" />
                <ul class="users-list clearfix">
                    <?php
                        if (isset($agents) && is_array($agents)) {
                            foreach ($agents as $agent) {
                    ?>
                                <li style="width: 24%;">
                                    <a class="users-list-name transfer_to" href="javascript:void(0);" data-transfer_to="<?php echo $agent['staff_id']; ?>">
                                        <?php if (isset($agent['image']) && !empty($agent['image'])) { ?>
                                            <img src="<?php echo IMAGE_URL; ?>staff/<?php echo $agent['image']; ?>"
                                                 alt="User Image" width="50" height="50">
                                             <?php } else { ?>
                                                 <?php if (trim($agent['gender']) == 'f') { ?>
                                                <img src="<?php echo IMAGE_URL; ?>staff/default_f.png" alt="User Image" width="50" height="50">
                                            <?php } else if (trim($agent['gender']) == 'm') { ?>
                                                <img src="<?php echo IMAGE_URL; ?>staff/default_m.png" alt="User Image" width="50" height="50">
                                            <?php } else { ?>
                                                <img src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image" width="50" height="50">
                                            <?php } ?>
                                        <?php } ?>
                                    </a>
                                    <a class="users-list-name transfer_to" href="javascript:void(0);" data-transfer_to="<?php echo $agent['staff_id']; ?>" ><?php echo $agent['name']; ?></a>
                                </li>
                    <?php
                            }
                        }
                    ?>
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
	var table;
	$(document).ready(function(){
		$('.select2').select2();
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [1,2,3,4,5,6,7,8,9,10,11,12],
			}
		};
        
		table = $('#party_list_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('party/party_list_datatable')?>",
                "type": "POST",
                "data":function(d){
                    d.party_status = $("#party_status").val();
                    d.party_mobile = $("#party_mobile").val();
                    d.party_email = $("#party_email").val();
                }
            },
            <?php if($this->applib->have_access_current_user_rights(PARTY_MODULE_ID,"export_data")){ ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Parties', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Parties', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Parties', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Parties', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Parties', orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
        });
        
        $(document).on('change',"#party_status",function() {
            table.draw();
        });
        var party_contact_no = '';
        $(document).on('keyup','#party_mobile', function() {
            var r_party_contact_no = $('#party_mobile').val();
            if(r_party_contact_no != party_contact_no){
                party_contact_no = r_party_contact_no;
                table.draw();
            }
        });
        var party_email = '';
        $(document).on('keyup','#party_email', function() {
            var r_party_email = $('#party_email').val();
            if(r_party_email != party_email){
                party_email = r_party_email;
                table.draw();
            }
        });
        
		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure Delete this Party?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "GET",
					data: null,
					success: function(data){
						show_notify("Party Deleted Successfully!",true);
						table.draw();
					}
				});
			}
		});
        
        $(document).on("click",".restore_button",function(){
			var value = confirm('Are you sure Restore this Party?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "GET",
					data: null,
					success: function(data){
						show_notify("Party Restored Successfully!",true);
						table.draw();
					}
				});
			}
		});
        
        $(document).on('click', '.open_transfer_party_modal', function(){
            $('#transfer_party_modal').modal({backdrop: 'static', keyboard: false});
            $('#transfer_party_modal').modal('show');
			var transfer_party_id = $(this).data('transfer_party_id');
            $('#transfer_party_id').val(transfer_party_id);
			var transfer_from = $(this).data('transfer_from');
            $('#transfer_from').val(transfer_from);
        });
        
        $(document).on('click', '.modal-body .transfer_to', function () {
            $("#ajax-loader").show();
            var transfer_party_id = $('#transfer_party_id').val();
            var transfer_from = $('#transfer_from').val();
            var transfer_to = $(this).attr('data-transfer_to');
//            alert('transfer_party_id : ' + transfer_party_id + ' || transfer_from : ' + transfer_from + ' || transfer_to : ' + transfer_to);
            $.ajax({
                url: '<?=BASE_URL?>party/transfer_one_party_to_other',
                type: "POST",
                data: {
                    transfer_party_id : transfer_party_id,
                    transfer_from : transfer_from,
                    transfer_to : transfer_to,
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $('#transfer_party_modal').modal('hide');
                        show_notify(data.message, true);
                        table.draw();
                    }else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
            return false;
        });
        
	});
</script>
