<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('party/save_party') ?>" method="post" id="save_party" novalidate>
        <?php if(isset($party_data->party_id) && !empty($party_data->party_id)){ ?>
            <input type="hidden" name="party_data[party_id]" id="party_id" value="<?=$party_data->party_id?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="custom content-header">
            <h1>
                <small class="text-primary text-bold">Sales : Party</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, F5 = Tab 5, Ctrl+S = Save</label></small>
                <?php
                    $accessAdd  = $this->app_model->have_access_role(PARTY_MODULE_ID, "add"); 
                    $access  = $this->app_model->have_access_role(PARTY_MODULE_ID, "view");
                    $accessEnquiry  = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "add");
                    $accessAdit = $this->app_model->have_access_role(PARTY_MODULE_ID, "edit");
                ?>
                <?php if($accessAdd || $accessAdit):?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn_save_party">Save Party</button>
                <?php endif;?>
                <?php if($access):?>
                    <a href="<?=BASE_URL?>party/party_list" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Party List</a>
                <?php endif;?>
                <?php if($accessAdd):?>
                    <a href="<?=BASE_URL?>party/add" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Add Party</a>
                <?php endif;?>
                <?php if($accessEnquiry):?>
                    <a href="<?=BASE_URL?>enquiry/add" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Go To Enquiry</a> 
                <?php endif;?>
            </h1>
        </section>
        <div class="clearfix">
            <?php if($accessAdd || $accessAdit || $only_view_mode == 1):?>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" id="tabs_1" data-toggle="tab">Party Details</a></li>
                        <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Contact Person</a></li>
                        <li><a href="#tab_3" class="address-works-tab" data-toggle="tab" id="tabs_3">Delivery Address</a></li>
                        <li><a href="#tab_4" class="com-address-tab" data-toggle="tab" id="tabs_4">Communication Address</a></li>
                        <?php if(isset($party_data->party_id) && !empty($party_data->party_id)){ ?>
                        <li><a href="#tab_5" data-toggle="tab" id="tabs_5">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Party Details </legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="party_code" class="col-sm-3 input-sm">Party Code</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[party_code]" id="party_code" class="form-control input-sm party_code" placeholder="" value="<?php if(isset($party_data->party_code)){ echo $party_data->party_code; } else { echo ''; } ?>"  readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="party_name" class="col-sm-3 input-sm">Party Name<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[party_name]" id="party_name" class="form-control input-sm party_name" placeholder="" value="<?php if(isset($party_data->party_name)){ echo $party_data->party_name; } ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="main_address" class="col-sm-3 input-sm">Address</label>
                                                    <div class="col-sm-9">
                                                        <textarea name="party_data[address]" id="main_address" class="form-control" rows="3" required=""><?php if(isset($party_data->address)){ echo $party_data->address; } ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="city" class="col-sm-3 input-sm">City</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <div class="col-md-6" style="padding:0px !important;">
                                                            <select name="party_data[city_id]" id="city" class="form-control input-sm"></select>
                                                        </div>
                                                        <div class="col-md-2" style="padding:0px !important;text-align:center;">
                                                            <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "add")) { ?>
                                                            <small><a style="color:#1f7eba" href="<?= base_url() ?>party/city/" target="_blank">(Add City)</a></small>
                                                            <?php } ?>                               
                                                        </div>
                                                        <div class="col-md-4" style="padding-right:0px !important;">
                                                            <input type="text" name="party_data[pincode]" id="pincode" class="form-control input-sm pincode" tabindex="1" placeholder="Pin Code" value="<?php if(isset($party_data->pincode)){ echo $party_data->pincode; } ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="state" class="col-sm-3 input-sm">State</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <div class="col-md-9" style="padding:0px !important;">
                                                            <select name="party_data[state_id]" id="state" class="form-control input-sm"></select>
                                                        </div>
                                                        <div class="col-md-3" style="padding:0px !important;text-align:center;">
                                                            <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "add")) { ?>    
                                                            <small><a style="color:#1f7eba" href="<?= base_url() ?>party/state/" target="_blank">(Add State)</a></small>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="country" class="col-sm-3 input-sm">Country</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <div class="col-md-9" style="padding:0px !important;">
                                                            <select name="party_data[country_id]" id="country" class="form-control input-sm"></select>
                                                        </div>
                                                        <div class="col-md-3" style="padding:0px !important;text-align:center;">
                                                            <?php if($this->app_model->have_access_role(MASTER_GENERAL_MASTER_COUNTRY_MENU_ID, "add")) { ?>    
                                                            <small><a style="color:#1f7eba" href="<?= base_url() ?>party/country/" target="_blank">(Add Country)</a></small>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="fax_no" class="col-sm-3 input-sm">Tel No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[fax_no]" id="fax_no" class="form-control input-sm fax_no num_only" tabindex="2" placeholder="" value="<?php if(isset($party_data->fax_no)){ echo $party_data->fax_no; } ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9">
                                                        <textarea rows="1" name="party_data[phone_no]" id="phone_no" class="form-control phone_no a_to_z_not_allow" placeholder=""><?php if(isset($party_data->phone_no)){ echo $party_data->phone_no; } ?></textarea>
                                                        <small class="">Add multiple phone number by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email_id" class="col-sm-3 input-sm">E-mail ID<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9">
                                                        <textarea rows="1" name="party_data[email_id]" id="email_id" class="form-control email_id"><?php if(isset($party_data->email_id)){ echo $party_data->email_id; } ?></textarea>
                                                        <small class="">Add multiple email by Comma separated.</small>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="website" class="col-sm-3 input-sm">Web Site</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[website]" id="website" class="form-control input-sm website" placeholder="" value="<?php if(isset($party_data->website)){ echo $party_data->website; } ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="party_type_1" class="col-sm-3 input-sm">Party Type<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9 no-padding">
                                                        <div class="col-sm-6">
                                                            <?php
                                                                $accessExport = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "export");
                                                                $accessDomestic  = $this->app_model->have_access_role(PARTY_TYPE_MODULE_ID, "domestic");
                                                            ?>
                                                            <?php if($accessExport == 1 && $accessDomestic == 1):?>
                                                                <select name="party_data[party_type_1]" id="party_type_1" class="form-control input-sm "></select>
                                                            <?php elseif($accessExport == 1):?>
                                                                <input type="text" class="form-control input-sm" name="party_type_1_display" id="party_type_1_display" placeholder="" value="<?php if(isset($party_data->party_type_1)){ echo isset($party_data->party_type_1); } else { echo "Customer-Export"; } ?>" readonly>
                                                                <input type="hidden" name="party_data[party_type_1]" id="party_type_1" value="<?php echo PARTY_TYPE_EXPORT_ID; ?>">
                                                            <?php elseif($accessDomestic == 1):?>
                                                                <input type="text" class="form-control input-sm" name="party_type_1_display" id="party_type_1_display" placeholder="" value="<?php if(isset($party_data->party_type_1)){ echo isset($party_data->party_type_1); } else { echo "Customer-Domestic"; } ?>" readonly>
                                                                <input type="hidden" name="party_data[party_type_1]" id="party_type_1" value="<?php echo PARTY_TYPE_DOMESTIC_ID; ?>">
                                                            <?php endif;?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select name="party_data[party_type_2]" id="party_type_2" class="form-control input-sm"></select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="branch_id" class="col-sm-3 input-sm">Branch</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="party_data[branch_id]" id="branch_id" class="form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="agent_id" class="col-sm-3 input-sm">Agent</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="party_data[agent_id]" id="agent_id" class="form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9">
                                                        <select name="party_data[reference_id]" id="reference_id" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
                                                    <div class="col-sm-9">
                                                        <textarea name="party_data[reference_description]" id="reference_description" class="form-control" placeholder="Reference"><?php if(isset($party_data->reference_description)){ echo $party_data->reference_description; } ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="gst_no" class="col-sm-3 input-sm">GST No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[gst_no]" id="gst_no" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->gst_no)){ echo $party_data->gst_no; } ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pan_no" class="col-sm-3 input-sm">PAN No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[pan_no]" id="pan_no" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->pan_no)){ echo $party_data->pan_no; } ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="utr_no" class="col-sm-3 input-sm">CIN No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[utr_no]" id="utr_no" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->utr_no)){ echo $party_data->utr_no; } ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="party_created_date" class="col-sm-3 input-sm">Party Created Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[party_created_date]" id="party_created_date" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->party_created_date) && strtotime($party_data->party_created_date) > 0){ echo date('d-m-Y',strtotime($party_data->party_created_date)); } else { echo date('d-m-Y'); } ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="username" class="col-sm-3 input-sm">Username</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[username]" id="username" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->username)){ echo $party_data->username; } ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="password" class="col-sm-3 input-sm">Password</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="party_data[password]" id="password" class="form-control input-sm" placeholder="" value="<?= isset($party_data->password) ? $party_data->password : $password ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row form-group">
                                                            <a href="javascript:void(0);" style="display: none;" class="btn btn-info btn-xs pull-right button-width btn-print-envelope" target="_blank">Preview Envelope</a>
                                                        </div>
                                                        <div class="row form-group">
                                                            <a href="javascript:void(0);" style="display: none;" class="btn btn-info btn-xs pull-right button-width btn-print-envelope">Print Envelope</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border contact_person_form contact_person_fields_div">
                                        <input type="hidden" name="contact_person_index" id="contact_person_index" />
                                        <?php if(isset($party_contact_persons)){ ?>
                                            <input type="hidden" name="contact_person_data[contact_person_id]" id="contact_person_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Contact Person Information </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_name" class="col-sm-3 input-sm">Name<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="contact_person_data[contact_person_name]" id="contact_person_name" class="form-control input-sm" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_priority" class="col-sm-3 input-sm">Priority</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="contact_person_data[contact_person_priority]" id="contact_person_priority" class="form-control input-sm" placeholder="" value="1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_designation_id" class="col-sm-3 input-sm">Designation</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <select name="contact_person_data[contact_person_designation_id]" id="contact_person_designation_id" class="form-control input-sm">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_department_id" class="col-sm-3 input-sm">Department</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <select name="contact_person_data[contact_person_department_id]" id="contact_person_department_id" class="form-control input-sm">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="contact_person_data[contact_person_mobile_no]" id="contact_person_mobile_no" class="form-control input-sm a_to_z_not_allow" placeholder="">
                                                    <small class="">Add multiple Mobile number by Comma separated.</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_email" class="col-sm-3 input-sm">Email<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="contact_person_data[contact_person_email]" id="contact_person_email" class="form-control input-sm" placeholder="">
                                                    <small class="">Add multiple Email by Comma separated.</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_fax_no" class="col-sm-3 input-sm">Fax No.</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="contact_person_data[contact_person_fax_no]" id="contact_person_fax_no" class="form-control input-sm" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_person_note" class="col-sm-3 input-sm">Note</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="contact_person_data[contact_person_note]" id="contact_person_note" class="form-control input-sm contact_person_note" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"><div class="form-group">&nbsp;</div></div>
                                        <div class="col-md-6 text-left">
                                            <input type="button" id="add_contact_person" class="btn btn-info btn-xs pull-right add_contact_person" value="Add Contact Person" />
                                        </div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-md-10">
                                    <table class="table custom-table">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Priority</th>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Department</th>
                                                <th>Mobile No.</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody id="contact_person_list"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Delivery Address Info </legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 input-sm">Same As Party Details ?</label>
                                                <div class="col-md-4">
                                                    <label class="no-padding">
                                                        <input type="radio" name="party_data[address_work]" id="address_work" class="address_work" value="1" <?php echo isset($party_data->address_work) ? ($party_data->address_work == 1) ? 'checked="checked"' : '' : 'checked="checked"'; ?> > Yes &nbsp;&nbsp;
                                                    </label>
                                                    <label class="no-padding">
                                                        <input type="radio" name="party_data[address_work]" id="address_work" class="address_work" value="0" <?php echo isset($party_data->address_work) ? ($party_data->address_work == 0) ? 'checked="checked"' : '' : ''; ?>> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="w_delivery_party_name" class="col-sm-3 input-sm">Delivery Party Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm" id="w_delivery_party_name" name="party_data[w_delivery_party_name]" placeholder="" value="<?php if(isset($party_data->w_delivery_party_name)){ echo $party_data->w_delivery_party_name; } ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_address" class="col-sm-3 input-sm">Address</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[w_address]" id="w_address" class="form-control" rows="3" ><?php if(isset($party_data->w_address)){ echo $party_data->w_address; } ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_city" class="col-sm-3 input-sm">City</label>
                                                <div class="col-sm-9">
                                                    <select name="party_data[w_city]" id="w_city" class="form-control input-sm"></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_pin_code" class="col-sm-3 input-sm">Pin Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="party_data[w_pin_code]" id="w_pin_code" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->w_pin_code)){ echo $party_data->w_pin_code; } ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_state" class="col-sm-3 input-sm">State</label>
                                                <div class="col-sm-9">
                                                    <select name="party_data[w_state]" id="w_state" class="form-control input-sm"></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_country" class="col-sm-3 input-sm">Country</label>
                                                <div class="col-sm-9">
                                                    <select name="party_data[w_country]" id="w_country" class="form-control input-sm"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="w_phone1" class="col-sm-3 input-sm">Contact No.</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[w_phone1]" id="w_phone1" class="form-control" placeholder=""><?php if(isset($party_data->w_phone1)){ echo $party_data->w_phone1; } ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_email" class="col-sm-3 input-sm">Email</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[w_email]" id="w_email" class="form-control" placeholder=""><?php if(isset($party_data->w_email)){ echo $party_data->w_email; } ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_web" class="col-sm-3 input-sm">Web</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="party_data[w_web]" id="w_web" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->w_web)){ echo $party_data->w_web; } ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="w_note" class="col-sm-3 input-sm">Note</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[w_note]" id="w_note" class="form-control" placeholder=""><?php if(isset($party_data->w_note)){ echo $party_data->w_note; } ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Communication Address Info </legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 input-sm">
                                                    Same As Party Details ?
                                                </label>
                                                <div class="col-md-4">
                                                    <label class="no-padding">
                                                        <input type="radio" name="party_data[com_address]" id="com_address" class="com_address" value="1" <?php echo isset($party_data->com_address) ? ($party_data->com_address == 1) ? 'checked="checked"' : '' : 'checked="checked"'; ?> > Yes &nbsp;&nbsp;
                                                    </label>
                                                    <label class="no-padding">
                                                        <input type="radio" name="party_data[com_address]" id="com_address" class="com_address" value="0" <?php echo isset($party_data->com_address) ? ($party_data->com_address == 0) ? 'checked="checked"' : '' : ''; ?>> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="com_address_party_name" class="col-sm-3 input-sm">Com. Party Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="party_data[com_address_party_name]" id="com_address_party_name" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->com_address_party_name)){ echo $party_data->com_address_party_name; } ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_address" class="col-sm-3 input-sm">Address</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[com_address_address]" id="com_address_address" class="form-control" rows="3" ><?php if(isset($party_data->com_address_address)){ echo $party_data->com_address_address; } ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_city" class="col-sm-3 input-sm">City</label>
                                                <div class="col-sm-9">
                                                    <select name="party_data[com_address_city]" id="com_address_city" class="form-control input-sm">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_pin_code" class="col-sm-3 input-sm">Pin Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="party_data[com_address_pin_code]" id="com_address_pin_code" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->com_address_pin_code)){ echo $party_data->com_address_pin_code; } ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_state" class="col-sm-3 input-sm">State</label>
                                                <div class="col-sm-9">
                                                    <select name="party_data[com_address_state]" id="com_address_state" class="form-control input-sm">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_country" class="col-sm-3 input-sm">Country</label>
                                                <div class="col-sm-9">
                                                    <select name="party_data[com_address_country]" id="com_address_country" class="form-control input-sm">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="com_address_phone1" class="col-sm-3 input-sm">Contact No.</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[com_address_phone1]" id="com_address_phone1" class="form-control" placeholder=""><?php if(isset($party_data->com_address_phone1)){ echo $party_data->com_address_phone1; } ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_email" class="col-sm-3 input-sm">Email</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[com_address_email]" id="com_address_email" class="form-control" placeholder=""><?php if(isset($party_data->com_address_email)){ echo $party_data->com_address_email; } ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_web" class="col-sm-3 input-sm">Web</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="party_data[com_address_web]" id="com_address_web" class="form-control input-sm" placeholder="" value="<?php if(isset($party_data->com_address_web)){ echo $party_data->com_address_web; } ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="com_address_note" class="col-sm-3 input-sm">Note</label>
                                                <div class="col-sm-9">
                                                    <textarea name="party_data[com_address_note]" id="com_address_note" class="form-control" placeholder=""><?php if(isset($party_data->com_address_note)){ echo $party_data->com_address_note; } ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_5">
                            <?php if(isset($party_data->party_id) && !empty($party_data->party_id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=$party_data->created_by_name;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=$party_data->created_at;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=$party_data->updated_by_name;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=$party_data->updated_at;?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <?php endif;?>
        </div>
        <section class="custom content-header">
            <?php if($accessAdd || $accessAdit):?>
                <button type="button" class="btn btn-info btn-xs pull-right btn_save_party">Save Party</button>
            <?php endif;?>
            <?php if($access):?>
                <a href="<?=BASE_URL?>party/party_list" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Party List</a>
            <?php endif;?>
            <?php if($accessAdd):?>
                <a href="<?=BASE_URL?>party/add" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Add Party</a>
            <?php endif;?>
            <?php if($accessEnquiry):?>
                <a href="<?=BASE_URL?>enquiry/add" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Go To Enquiry</a> 
            <?php endif;?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<script>
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_contact_person_inc = 0;
	var contact_person_objectdata = [];
    <?php if(isset($party_contact_persons)){ ?>
		var cp_contact_person_objectdata = [<?php echo $party_contact_persons; ?>];
		var contact_person_objectdata = [];
        if(cp_contact_person_objectdata != ''){
            $.each(cp_contact_person_objectdata, function (index, value) {
                contact_person_objectdata.push(value);
            });
        }
	<?php } ?>
    display_contact_person_html(contact_person_objectdata);
    <?php if(isset($party_data->party_id) && !empty($party_data->party_id)){ ?>
        first_time_edit_mode = 0;
    <?php } ?>
	$(document).ready(function(){
		$(".input-datepicker").datepicker({
			format: 'dd-mm-yyyy',
			todayBtn: "linked",
			todayHighlight: true,
			autoclose: true
		});
		set_contact_person_priority();
		<?php if($accessExport == 1 && $accessDomestic == 1):?>
            initAjaxSelect2($("#party_type_1"),"<?=base_url('app/sales_select2_source')?>");
            <?php if(isset($party_data->party_type_1) && !empty($party_data->party_type_1)){ ?>
                setSelect2Value($("#party_type_1"),"<?=base_url('app/set_sales_select2_val_by_id/'.$party_data->party_type_1)?>");
            <?php } ?>
		<?php endif;?>

        initAjaxSelect2($("#party_type_2"),"<?=base_url('app/sales_type_select2_source')?>");
        <?php if(isset($party_data->party_type_2) && !empty($party_data->party_type_2)){ ?>
            setSelect2Value($("#party_type_2"),"<?=base_url('app/set_sales_type_select2_val_by_id/'.$party_data->party_type_2)?>");
        <?php } else { ?>
            setSelect2Value($("#party_type_2"),"<?=base_url('app/set_sales_type_select2_val_by_id/'.DEFAULT_PARTY_TYPE_2_ID)?>");
        <?php } ?>
            
		initAjaxSelect2($("#branch_id"),"<?=base_url('app/branch_select2_source')?>");
        <?php if(isset($party_data->branch_id) && !empty($party_data->branch_id)){ ?>
            setSelect2Value($("#branch_id"),"<?=base_url('app/set_branch_select2_val_by_id/'.$party_data->branch_id)?>");
        <?php } else { ?>
            setSelect2Value($("#branch_id"),"<?=base_url('app/set_branch_select2_val_by_id/'.DEFAULT_BRANCH_ID)?>");
        <?php } ?>
            
		initAjaxSelect2($("#agent_id"),"<?=base_url('app/agent_select2_source')?>");
        <?php if(isset($party_data->agent_id) && !empty($party_data->agent_id)){ ?>
            setSelect2Value($("#agent_id"),"<?=base_url('app/set_agent_select2_val_by_id/'.$party_data->agent_id)?>");
        <?php } else { ?>
            setSelect2Value($("#agent_id"),"<?=base_url('app/set_agent_select2_val_by_id/'.DEFAULT_AGENT_ID)?>");
        <?php } ?>
            
		initAjaxSelect2($("#reference_id"),"<?=base_url('app/reference_select2_source')?>");
        <?php if(isset($party_data->reference_id) && !empty($party_data->reference_id)){ ?>
            setSelect2Value($("#reference_id"),"<?=base_url('app/set_reference_select2_val_by_id/'.$party_data->reference_id)?>");
        <?php } ?>
                
		initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        <?php if(isset($party_data->city_id) && !empty($party_data->city_id)){ ?>
            setSelect2Value($("#city"),"<?=base_url('app/set_city_select2_val_by_id/'.$party_data->city_id)?>");
        <?php } ?>
            
		initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        <?php if(isset($party_data->state_id) && !empty($party_data->state_id)){ ?>
            setSelect2Value($("#state"),"<?=base_url('app/set_state_select2_val_by_id/'.$party_data->state_id)?>");
        <?php } ?>
            
		initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
        <?php if(isset($party_data->country_id) && !empty($party_data->country_id)){ ?>
            setSelect2Value($("#country"),"<?=base_url('app/set_country_select2_val_by_id/'.$party_data->country_id)?>");
        <?php } ?>
            
		initAjaxSelect2($("#w_city"),"<?=base_url('app/city_select2_source')?>");
        <?php if(isset($party_data->w_city) && !empty($party_data->w_city)){ ?>
            setSelect2Value($("#w_city"),"<?=base_url('app/set_city_select2_val_by_id/'.$party_data->w_city)?>");
        <?php } ?>
            
		initAjaxSelect2($("#w_state"),"<?=base_url('app/state_select2_source')?>");
        <?php if(isset($party_data->w_state) && !empty($party_data->w_state)){ ?>
            setSelect2Value($("#w_state"),"<?=base_url('app/set_state_select2_val_by_id/'.$party_data->w_state)?>");
        <?php } ?>
            
		initAjaxSelect2($("#w_country"),"<?=base_url('app/country_select2_source')?>");
        <?php if(isset($party_data->w_country) && !empty($party_data->w_country)){ ?>
            setSelect2Value($("#w_country"),"<?=base_url('app/set_country_select2_val_by_id/'.$party_data->w_country)?>");
        <?php } ?>
            
		initAjaxSelect2($("#com_address_city"),"<?=base_url('app/city_select2_source')?>");
        <?php if(isset($party_data->com_address_city) && !empty($party_data->com_address_city)){ ?>
            setSelect2Value($("#com_address_city"),"<?=base_url('app/set_city_select2_val_by_id/'.$party_data->com_address_city)?>");
        <?php } ?>

        initAjaxSelect2($("#com_address_state"),"<?=base_url('app/state_select2_source')?>");
        <?php if(isset($party_data->com_address_state) && !empty($party_data->com_address_state)){ ?>
            setSelect2Value($("#com_address_state"),"<?=base_url('app/set_state_select2_val_by_id/'.$party_data->com_address_state)?>");
        <?php } ?>
            
		initAjaxSelect2($("#com_address_country"),"<?=base_url('app/country_select2_source')?>");
        <?php if(isset($party_data->com_address_country) && !empty($party_data->com_address_country)){ ?>
            setSelect2Value($("#com_address_country"),"<?=base_url('app/set_country_select2_val_by_id/'.$party_data->com_address_country)?>");
        <?php } ?>
            
		initAjaxSelect2($("#contact_person_designation_id"),"<?=base_url('app/designation_select2_source')?>");
		initAjaxSelect2($("#contact_person_department_id"),"<?=base_url('app/department_select2_source')?>");
        setSelect2Value($("#contact_person_designation_id"),"<?=base_url('app/set_designation_select2_val_by_id/'.DEFAULT_DESIGNATION_ID)?>");
        setSelect2Value($("#contact_person_department_id"),"<?=base_url('app/set_department_select2_val_by_id/'.DEFAULT_DEPARTMENT_ID)?>");

        <?php if(isset($party_data->address_work) && $party_data->address_work == 1){ ?>
            $('#tab_3').find('input[type="text"],textarea,select').each(function(){
                $(this).attr('disabled','disabled');
            });
        <?php } else if(isset($party_data->address_work) && $party_data->address_work == 0){ ?>
        <?php } else { ?>
            $('#tab_3').find('input[type="text"],textarea,select').each(function(){
                $(this).attr('disabled','disabled');
            });
        <?php } ?>
        <?php if(isset($party_data->com_address) && $party_data->com_address == 1){ ?>
            $('#tab_4').find('input[type="text"],textarea,select').each(function(){
                $(this).attr('disabled','disabled');
            });
        <?php } else if(isset($party_data->com_address) && $party_data->com_address == 0){ ?>
        <?php } else { ?>
            $('#tab_4').find('input[type="text"],textarea,select').each(function(){
                $(this).attr('disabled','disabled');
            });
        <?php } ?>

		$(document).on('ifChanged','#address_work',function(){
			if($(this).val() == 1){
				$('#tab_3').find('input[type="text"],textarea,select').each(function(){
					$(this).attr('disabled','disabled');
					$(this).val('');
				});
			}else{
				$('#tab_3').find('input[type="text"],textarea,select').each(function(){
					$(this).removeAttr('disabled');
					//$(this).val('');
				});
			}
		});

		$(document).on('ifChanged','#address_work',function(){
			if($(this).val() == 1){
				$("#w_address").val($("#main_address").val());

				var city_html = $("#city").html();
				$("#w_city").html('');
				$("#w_city").html(city_html);
				$("#w_city").val($("#city").val());
				$("#s2id_w_city span:first").html($("#city option:selected").text());

				var state_html = $("#state").html();
				$("#w_state").html('');
				$("#w_state").html(state_html);
				$("#w_state").val($("#state").val());
				$("#s2id_w_state span:first").html($("#state option:selected").text());

				var country_html = $("#country").html();
				$("#w_country").html('');
				$("#w_country").html(country_html);
				$("#w_country").val($("#country").val());
				$("#s2id_w_country span:first").html($("#country option:selected").text());

				$("#w_delivery_party_name").val($("#party_name").val());
				$("#w_pin_code").val($("#pincode").val());
				$("#w_web").val($("#website").val());
				$("#w_phone1").val($("#phone_no").val());
				$("#w_email").val($("#email_id").val());
			}
		});

		$(document).on("click",".address-works-tab",function(){
			if($(".address_work:checked").size() > 0)
			{
				var $result = $(".address_work:checked").val();
				if($result == 1)
				{
					var city_html = $("#city").html();
					$("#w_city").html('');
					$("#w_city").html(city_html);
					$("#w_city").val($("#city").val());
					$("#s2id_w_city span:first").html($("#city option:selected").text());

					var state_html = $("#state").html();
					$("#w_state").html('');
					$("#w_state").html(state_html);
					$("#w_state").val($("#state").val());
					$("#s2id_w_state span:first").html($("#state option:selected").text());

					var country_html = $("#country").html();
					$("#w_country").html('');
					$("#w_country").html(country_html);
					$("#w_country").val($("#country").val());
					$("#s2id_w_country span:first").html($("#country option:selected").text());

					$("#w_web").val($("#website").val())
					$("#w_phone1").val($("#phone_no").val())
					$("#w_email").val($("#email_id").val())

					$("#w_address").val($("#main_address").val());
					$("#w_delivery_party_name").val($("#party_name").val());
					$("#w_pin_code").val($("#pincode").val());
					$("#w_web").val($("#website").val());
					$("#w_phone1").val($("#phone_no").val());
					$("#w_email").val($("#email_id").val());
				}
			}    

		});

		$(document).on('ifChanged','.com_address',function(){
			if($(this).val() == 1){
				$('#tab_4').find('input[type="text"],textarea,select').each(function(){
					$(this).attr('disabled','disabled');
					$(this).val('');
				});
			}else{
				$('#tab_4').find('input[type="text"],textarea,select').each(function(){
					$(this).removeAttr('disabled');
					//$(this).val('');
				});
			}
		});

		$(document).on('ifChanged','.com_address',function(){
			if($(this).val() == 1){
				$("#com_address_address").val($("#main_address").val());

				var city_html = $("#city").html();
				$("#com_address_city").html('');
				$("#com_address_city").html(city_html);
				$("#com_address_city").val($("#city").val());
				$("#s2id_com_address_city span:first").html($("#city option:selected").text());

				var state_html = $("#state").html();
				$("#com_address_state").html('');
				$("#com_address_state").html(state_html);
				$("#com_address_state").val($("#state").val());
				$("#s2id_com_address_state span:first").html($("#state option:selected").text());

				var country_html = $("#country").html();
				$("#com_address_country").html('');
				$("#com_address_country").html(country_html);
				$("#com_address_country").val($("#country").val());
				$("#s2id_com_address_country span:first").html($("#country option:selected").text());

				$("#com_address_party_name").val($("#party_name").val());
				$("#com_address_pin_code").val($("#pincode").val());
				$("#com_address_web").val($("#website").val());
				$("#com_address_phone1").val($("#phone_no").val());
				$("#com_address_email").val($("#email_id").val());
			}
		});

		$(document).on("click",".com-address-tab",function(){
			if($(".com_address:checked").size() > 0){
				var $result = $(".com_address:checked").val();
				if($result == 1){
					var city_html = $("#city").html();
					$("#com_address_city").html('');
					$("#com_address_city").html(city_html);
					$("#com_address_city").val($("#city").val());
					$("#s2id_com_address_city span:first").html($("#city option:selected").text());

					var state_html = $("#state").html();
					$("#com_address_state").html('');
					$("#com_address_state").html(state_html);
					$("#com_address_state").val($("#state").val());
					$("#s2id_com_address_state span:first").html($("#state option:selected").text());

					var country_html = $("#country").html();
					$("#com_address_country").html('');
					$("#com_address_country").html(country_html);
					$("#com_address_country").val($("#country").val());
					$("#s2id_com_address_country span:first").html($("#country option:selected").text());

					$("#com_address_web").val($("#website").val())
					$("#com_address_phone1").val($("#phone_no").val())
					$("#com_address_email").val($("#email_id").val())

					$("#com_address_address").val($("#main_address").val());
					$("#com_address_party_name").val($("#party_name").val());
					$("#com_address_pin_code").val($("#pincode").val());
					$("#com_address_web").val($("#website").val());
					$("#com_address_phone1").val($("#phone_no").val());
					$("#com_address_email").val($("#email_id").val());
				}
			}    
		});

        $(document).on('click', '.btn_save_party', function () {
            $('#save_party').submit();
        });
        
        $(document).on('focusout', '#party_name', function () {
            var party_id = $('#party_id').val();
            var party_name = $('#party_name').val();
            var phone_no = $('#phone_no').val();
            if($.trim(party_name) != ''){
                $("#ajax-loader").show();
                $.ajax({
                    url: "<?=base_url('party/check_party_name') ?>",
                    type: "POST",
                    cache: false,
                    data: {party_id : party_id, party_name : party_name, phone_no : phone_no},
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json['success'] == 'false'){
                            show_notify(json['msg'],false);
                        }
                        $("#ajax-loader").hide();
                        return false;
                    },
                });
            }
        });
        
        $(document).on('focusout', '#phone_no', function () {
            var party_id = $('#party_id').val();
            var phone_no = $('#phone_no').val();
            if($.trim(phone_no) != ''){
                $("#ajax-loader").show();
                $.ajax({
                    url: "<?=base_url('party/check_phone_no') ?>",
                    type: "POST",
                    cache: false,
                    data: {party_id : party_id, phone_no : phone_no},
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json['success'] == 'false'){
                            show_notify(json['msg'],false);
                        }
                        $("#ajax-loader").hide();
                        return false;
                    },
                });
            }
        });
        
        $(document).on('focusout', '#email_id', function () {
            var party_id = $('#party_id').val();
            var email_id = $('#email_id').val();
            if($.trim(email_id) != ''){
                $("#ajax-loader").show();
                $.ajax({
                    url: "<?=base_url('party/check_email_id') ?>",
                    type: "POST",
                    cache: false,
                    data: {party_id : party_id, email_id : email_id},
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json['success'] == 'false'){
                            show_notify(json['msg'],false);
                        }
                        $("#ajax-loader").hide();
                        return false;
                    },
                });
            }
        });
        
        $(document).on('focusout', '#fax_no', function () {
            var party_id = $('#party_id').val();
            var fax_no = $('#fax_no').val();
            if($.trim(fax_no) != ''){
                $("#ajax-loader").show();
                $.ajax({
                    url: "<?=base_url('party/check_fax_no') ?>",
                    type: "POST",
                    cache: false,
                    data: {party_id : party_id, fax_no : fax_no},
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json['success'] == 'false'){
                            show_notify(json['msg'],false);
                        }
                        $("#ajax-loader").hide();
                        return false;
                    },
                });
            }
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_party").submit();
                return false;
            }
        });
        
        $(document).on('submit', '#save_party', function () {
        
            if($.trim($("#party_name").val()) == ''){
                show_notify('Please Select Party Name.',false);
                $("#party_name").focus();
                return false;
            }
            if($.trim($("#party_type_1").val()) == ''){
                show_notify('Please Select Party Type.',false);
                return false;
            }
            if($.trim($("#reference_id").val()) == ''){
                show_notify('Please Select Reference.',false);
                return false;
            }
            var party_type = $("#party_type_1").val();
//            if(party_type == 5){
//                if($.trim($("#phone_no").val()) == ''){
//                    show_notify('Party Contact Number is Required.',false);
//                    $("#phone_no").focus();
//                    return false;
//                }
//                if($.trim($("#email_id").val()) == ''){
//                    show_notify('Party Email is Required.',false);
//                    $("#phone_no").focus();
//                    return false;
//                }
//            } else {
                    if($.trim($("#email_id").val()) == '' && $.trim($("#phone_no").val()) == ''){
                        show_notify('Party Email or Mobile is Required.',false);
                        return false;
                    }
//                }
            if($.trim($("#contact_person_name").val()) != '' ){
				if($.trim($("#contact_person_email").val()) == '' && $.trim($("#contact_person_mobile_no").val()) == ''){
					show_notify('Contact Person Email or Mobile is Required.',false);
					return false;
				}
			}
            
            if(contact_person_objectdata == ''){
				show_notify("Please Enter At least One Contact Person.", false);
				return false;
			}
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
			var contact_person_objectdata_stringify = JSON.stringify(contact_person_objectdata);
			postData.append('contact_person_data', contact_person_objectdata_stringify);
			$.ajax({
				url: "<?=base_url('party/save_party') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('party/add') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('party/party_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });
        
        $(document).on('click', '#add_contact_person', function() {
			if($.trim($("#contact_person_name").val()) == ''){
				show_notify('Contact name is required.',false);
                $("#contact_person_name").focus()
				return false;
			}
			if($.trim($("#contact_person_email").val()) == '' && $.trim($("#contact_person_mobile_no").val()) == ''){
				show_notify('Contact person email or mobile is required.',false);
				return false;
			}
            
            var exist_flag = 0;
            if($.trim($("#contact_person_email").val()) != ''){
                let party_id = "<?=isset($party_data->party_id) ? $party_data->party_id : '' ?>";
                let contact_person_email = $.trim($("#contact_person_email").val());
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?=base_url();?>party/check_ajax_cp_email/',
                    data: {party_id: party_id, contact_person_email: contact_person_email},
                    success: function(data){
                        var json = $.parseJSON(data);
                        if(json['success'] == 'false'){
                            exist_flag = 1;
                            if(json['msg']){
                                show_notify(json['msg'],false);
                                return false;
                            }
                            return false;
                        }
                    },
                });
            }
            
            if($.trim($("#contact_person_mobile_no").val()) != ''){
                let party_id = "<?=isset($party_data->party_id) ? $party_data->party_id : '' ?>";
                let contact_person_mobile_no = $.trim($("#contact_person_mobile_no").val());
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?=base_url();?>party/check_cp_phone/',
                    data: {party_id: party_id, contact_person_mobile_no: contact_person_mobile_no},
                    success: function(data){
                        var json = $.parseJSON(data);
                        if(json['success'] == 'false'){
                            exist_flag = 1;
                            if(json['msg']){
                                show_notify(json['msg'],false);
                                return false;
                            }
                            return false;
                        }
                    },
                });
            }
            
            if($.trim($("#contact_person_fax_no").val()) != ''){
                let party_id = "<?=isset($party_data->party_id) ? $party_data->party_id : '' ?>";
                let contact_person_fax_no = $.trim($("#contact_person_fax_no").val());
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?=base_url();?>party/check_cp_fax_no/',
                    data: {party_id: party_id, contact_person_fax_no: contact_person_fax_no},
                    success: function(data){
                        var json = $.parseJSON(data);
                        if(json['success'] == 'false'){
                            exist_flag = 1;
                            if(json['msg']){
                                show_notify(json['msg'],false);
                                return false;
                            }
                            return false;
                        }
                    },
                });
            }
            
            if(exist_flag == 1){
                exist_flag = 0;
                return false;
            }
			
			var key = '';
			var value = '';
			var contact_person_arr = {};
			$('select[name^="contact_person_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("contact_person_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				contact_person_arr[key] = value;
			});
			$('input[name^="contact_person_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("contact_person_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				contact_person_arr[key] = value;
			});
			var new_contact_person = JSON.parse(JSON.stringify(contact_person_arr));
			var contact_person_index = $("#contact_person_index").val();
			if(contact_person_index != ''){
				contact_person_objectdata.splice(contact_person_index, 1, new_contact_person);
			} else {
				contact_person_objectdata.push(new_contact_person);
			}
			display_contact_person_html(contact_person_objectdata);
			$('#contact_person_id').val('');
            $('.contact_person_fields_div').find('input[name^="contact_person_index"], input[name^="contact_person_data"], select[name^="contact_person_data"], textarea[name^="contact_person_data"]').val('');
			setSelect2Value($("#contact_person_designation_id"),"<?=base_url('app/set_designation_select2_val_by_id/'.DEFAULT_DESIGNATION_ID)?>");
            setSelect2Value($("#contact_person_department_id"),"<?=base_url('app/set_department_select2_val_by_id/'.DEFAULT_DEPARTMENT_ID)?>");
			$("#contact_person_index").val('');
            if(on_save_add_edit_item == 1){
                on_save_add_edit_item == 0;
                $('#save_enquiry').submit();
            }
        });
    
        $('#city,#w_city,#com_address_city').on('select2:select', function (e) {
            var data = e.params.data;
            var dom_id = e.currentTarget.id;
            console.log(dom_id);
            var id = data.id;
            if(id != '' && id != null){
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>party/ajax_get_state/'+id,
                    data: id='cat_id',
                    success: function(data){
                        var json = $.parseJSON(data);
                        //console.log(data);
                        if (json['state_id']) {
                            if(dom_id == 'city'){
                                setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                            }
                            if(dom_id == 'w_city')
                                setSelect2Value($("#w_state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                            if(dom_id == 'com_address_city')
                                setSelect2Value($("#com_address_state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                        }
                        if (json['country_id']) {
                            if(dom_id == 'city'){
                                setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                            }
                            if(dom_id == 'w_city'){
                                setSelect2Value($("#w_country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                            }
                            if(dom_id == 'com_address_city'){
                                setSelect2Value($("#com_address_country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                            }
                        }					
                    },
                });
            }
        });

        $('#state,#w_state,#com_address_state').on('select2:select', function (e) {
            var data = e.params.data;
            var dom_id = e.currentTarget.id;
            var id = data.id;
            if(id != '' && id != null){
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>party/ajax_get_country/'+id,
                    data: id='cat_id',
                    success: function(data){
                        var json = $.parseJSON(data);
                        //console.log(data);
                        if (json['country_id']) {
                            if(dom_id == 'state'){
                                setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                            }
                            if(dom_id == 'w_state'){
                                setSelect2Value($("#w_country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                            }
                            if(dom_id == 'com_address_state'){
                                setSelect2Value($("#com_address_country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                            }
                        }

                    },
                });
            }
        });
    });
    
    function display_contact_person_html(contact_person_objectdata){
		var new_contact_person_html = '';
		console.log(contact_person_objectdata);
		$.each(contact_person_objectdata, function (index, value) {
            var contact_person_designation;
			$.ajax({
				url: "<?=base_url('app/set_designation_select2_val_by_id/') ?>/" + value.contact_person_designation_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					contact_person_designation = response.text;
				},
			});
            var contact_person_department;
			$.ajax({
				url: "<?=base_url('app/set_department_select2_val_by_id/') ?>/" + value.contact_person_department_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					contact_person_department = response.text;
				},
			});
            
			var contact_person_edit_btn = '';
			contact_person_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_contact_person_' + index + '" href="javascript:void(0);" onclick="edit_contact_person(' + index + ')"><i class="fa fa-edit"></i></a> ';
			var row_html = '<tr class="contact_person_index_' + index + '"><td class="">' +
			contact_person_edit_btn +
			' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_contact_person(' + index + ')"><i class="fa fa-remove"></i></a> ' +
			'</td>' +
			'<td>' + value.contact_person_priority + '</td>' +
			'<td>' + value.contact_person_name + '</td>' +
            '<td>' + contact_person_designation + '</td>' +
            '<td>' + contact_person_department + '</td>' +
            '<td>' + value.contact_person_mobile_no + '</td>' +
            '<td>' + value.contact_person_email + '</td>';
            new_contact_person_html += row_html;
		});
		$('tbody#contact_person_list').html(new_contact_person_html);
	}
    
    function edit_contact_person(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_contact_person_inc == 0){  edit_contact_person_inc = 1; /*$('.edit_contact_person_'+ index).click();*/ }
		value = contact_person_objectdata[index];
		$("#contact_person_index").val(index);
		
		initAjaxSelect2($("#contact_person_designation_id"),"<?=base_url('app/designation_select2_source/')?>");
		setSelect2Value($("#contact_person_designation_id"),"<?=base_url('app/set_designation_select2_val_by_id/')?>/" + value.contact_person_designation_id);
        initAjaxSelect2($("#contact_person_department_id"),"<?=base_url('app/department_select2_source/')?>");
		setSelect2Value($("#contact_person_department_id"),"<?=base_url('app/set_department_select2_val_by_id/')?>/" + value.contact_person_department_id);
        if(typeof(value.contact_person_id) != "undefined" && value.contact_person_id !== null) {
			$("#contact_person_id").val(value.contact_person_id);
		}
		$("#contact_person_priority").val(value.contact_person_priority);
        $("#contact_person_name").val(value.contact_person_name);
        $("#contact_person_mobile_no").val(value.contact_person_mobile_no);
        $("#contact_person_email").val(value.contact_person_email);
        $("#contact_person_fax_no").val(value.contact_person_fax_no);
        $("#contact_person_note").val(value.contact_person_note);
		$('.overlay').hide();
	}
	
	function remove_contact_person(index){
        if(confirm('Are you sure ?')){
            value = contact_person_objectdata[index];
            if(typeof(value.contact_person_id) != "undefined" && value.contact_person_id !== null) {
                $('.contact_person_form').append('<input type="hidden" name="deleted_contact_person_id[]" id="deleted_contact_person_id" value="' + value.contact_person_id + '" />');
            }
            contact_person_objectdata.splice(index,1);
            display_contact_person_html(contact_person_objectdata);
        }
	}
    
    function check_form_validations(){
        var $status = 1;

        var party_name = document.forms["partycodeform"]["party_name"].value;
        if (party_name == "" || party_name == null) {
            jQuery(".party_namespan").show();
            show_notify(jQuery(".party_namespan").text(),false);
            jQuery("#party_name").focus();
            jQuery("a[href='#tab_1']").trigger("click");
            return 0;
        }
        else{jQuery(".party_namespan").hide();}

        return $status;
    }

	function check_priority_is_unique(contact_person_id = ''){
		var DataStr = "party_id="+$("#party_id").val()+"&priority="+$("#contact_person_priority").val()+"&contact_person_id="+contact_person_id;
		var response = '1';
		$.ajax({
			url: "<?=base_url()?>party/check_priority_is_unique/",
			type: "POST",
			data: DataStr,
			async:false
		}).done(function(data) {
			response = data;
		});
		return response;
	}

	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	}

	function split(val) {
		return val.split(/,\s*/);
	}

	function extractLast(term) {
		return split(term).pop();
	}

	function set_contact_person_priority(){
		var party_id = $("#party_id").val();
        if(party_id == '' || party_id == null || party_id == 'undefined'){
			$("#contact_person_priority").val('1');
		} else {
			$.ajax({
				type: "POST",
				url: '<?=base_url();?>party/get_max_priority/'+party_id,
				success: function(data){
					$("#contact_person_priority").val(data);
				}
			});
		}
	}

	function setStateAndCountryByCity(city_id, id){
		$.ajax({
			url: "<?=BASE_URL?>party/fetch_state_country_by_city",
			dataType: "json",
			data: {city_id : city_id},
			success: function(data) 
			{
				// data = JSON.parse(data);
				if(id == "city")
				{
					$("#partydetailform #state").val(data.stateName);
					$("#partydetailform #country").val(data.countryName);                    
				}
				else
				{
					$("#w_state").val(data.stateName);
					$("#w_country").val(data.countryName);                    
				}    
			},
			error: function(error){
			}    
		});
	}

	function ValidateEmail(email_id) {
		var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		return expr.test(email_id);
	};

	<?php if (isset($_GET['view'])){ ?>
	$(window).load(function () {
		display_as_a_viewpage();
	});
	<?php } ?>
</script>
