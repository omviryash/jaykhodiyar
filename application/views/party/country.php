<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_COUNTRY_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_COUNTRY_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_COUNTRY_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Country</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Country Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table country-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Country</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                    	<?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('party/country/'.$row->country_id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('party/delete/'.$row->country_id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
						                                            </td>
						                                            <td><?=$row->country ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
													<div class="col-sm-12">
                                                        <button type="button" class="btn btn-info btn-xs pull-right" style="margin-bottom:5px;">Country Log</button>
                                                    </div>
												<div class="clearfix"></div>
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($country_id) && !empty($country_id)){ ?>Edit 
														<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?>Country
													</div>
													<div style="margin:20px">	
														<form method="POST" 
															<?php if(isset($country_id) && !empty($country_id)){ ?>
															action="<?=base_url('party/update_country') ?>" 
															<?php } else { ?>
															action="<?=base_url('party/add_country') ?>" 
															<?php } ?>
															id="form_country">
															<?php if(isset($country_id) && !empty($country_id)){ ?>
																<input type="hidden" class="form-control input-sm" name="country_id" id="country_id" value="<?php echo $country_id; ?>" >
															<?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-2 input-sm">Country<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" name="country" id="country" value="<?php echo $country_name; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															
                                                            <?php if(isset($country_id) && !empty($country_id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Country</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Country</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
	$(document).ready(function(){
		$(".country-table").dataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});
		$("#form_country").on("submit",function(e){
			e.preventDefault();

			if($("#country").val() == ""){
				show_notify('Fill value Country.', false);
				return false;
			}
			<?php if(isset($country_id) && !empty($country_id)){ ?>
				var success_status = check_is_unique('country','country',$("#country").val(),'country_id','<?=$country_id?>');											
			<?php } else { ?>
			 	var success_status = check_is_unique('country','country',$("#country").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.unique-error').length > 0){
					$("p.unique-error").text('Country already exist!');
				}else{
					$("#country").after("<p class='text-danger unique-error'>Country already exist!</p>");
				}
				return false;	
			}else{
				$("p.unique-error").text(' ');
			}

			var url = '<?php echo base_url('party/delete/') ?>';
			var value = $("#country").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.country_id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="#" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.country+'</td>';
		                TableRow += '</tr>';
		                $('.country-table > tbody > tr:last ').after(TableRow);
		                $("#form_country")[0].reset();
		                show_notify('Saved Successfully!',true);*/
		                window.location.href = "<?php echo base_url('party/country') ?>";
					}
				});
			}
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=country_id&table_name=country',
					success: function(data){
						tr.remove();
						//show_notify('Deleted Successfully!',true);
		                window.location.href = "<?php echo base_url('party/country') ?>";
					}
				});
			}
		});
				
	
	});

</script>
