<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_COMPANY_BANKS_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_COMPANY_BANKS_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_COMPANY_BANKS_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Company Banks</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($isAdd || $isEdit) { ?>
                            <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php if(isset($id) && !empty($id)){ ?>Edit 
                                            <?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
                                            <?php } ?> Company Bank
                                        </div>
                                        <form method="POST" id="bank_form">
                                            <?php if(isset($id) && !empty($id)){ ?>
                                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                            <?php } ?>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-6" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label for="" class="col-md-3">Bank For :</label>
                                                    <div class="col-md-9">
                                                        <label for="for_domestic" class="col-md-2 input-sm" style="margin-left: 0px; padding-left: 0px; font-size: 13px;">Domestic</label>
                                                            <input type="checkbox" name="for_domestic" id="for_domestic" class="col-md-2" style="margin-left: 0px; padding-left: 0px; font-size: 13px;" value="1"  <?php if(isset($for_domestic) && $for_domestic == 1){ echo 'checked="checked"'; } else {} ?>>
                                                            <label for="for_export" class="col-md-2 input-sm" style="font-size: 13px;">Export</label>
                                                            <input type="checkbox" name="for_export" id="for_export" class="col-md-2" style="font-size: 13px;" value="1" <?php if(isset($for_export) && $for_export == 1){ echo 'checked="checked"'; } else {} ?>>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="margin-top: 15px;">
                                            <textarea name="detail" id="company_banks" class="form-control"><?php echo $company_banks; ?></textarea>
                                        </div>
                                            <?php if(isset($id) && !empty($id)){ ?>
                                            <button type="submit" class="btn btn-info btn-xs pull-right" style="margin: 1.5%;">Edit Company Bank</button>
                                            <?php } else { ?>
                                            <button type="submit" class="btn btn-info btn-xs pull-right" style="margin: 1.5%;" <?php echo $btn_disable;?>>Add Company Bank</button>
                                            <?php } ?>
                                        <div class="clearfix"></div>
                                        </form>
                                    </div>                                         
                            </div>
                            <?php } ?>
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Company Banks
                                    </div>
                                    <div style="margin: 10px;">
                                        <table id="example1" class="table custom-table terms-table">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Company Banks</th>
                                                    <th>Address For</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($results)) {
                                                    foreach ($results as $row) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php if ($isEdit) { ?>
                                                                    <a href="<?= base_url('party/company_banks/' . $row->id) ?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                                <?php } if ($isDelete) { ?>
                                                                    <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?= base_url('party/delete/' . $row->id); ?>"><i class="fa fa-trash"></i></a>
                                                                    <?php } ?>
                                                            </td>
                                                            <td><?= $row->detail ?></td>
                                                            <?php 
                                                                $add_for = '';
                                                                if($row->for_domestic == 1 && $row->for_export == 1){
                                                                    $add_for = 'Domestic, Export';
                                                                } else if ($row->for_domestic == 1 && $row->for_export == 0){
                                                                    $add_for = 'Domestic';
                                                                } else if ($row->for_export == 1 && $row->for_domestic == 0){
                                                                    $add_for = 'Export';
                                                                }
                                                            ?>
                                                            <td><?php echo $add_for; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.protectedSource.push(/<\?[\s\S]*?\?>/g);
        CKEDITOR.replace('company_banks', {
            removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        $("#example1").DataTable({
            "scrollY": "300px",
            "scrollCollapse": true,
            "aaSorting": [[1, 'asc']],
            "paging": false,
            //"order": [[ 1, "asc" ]]
        });
        $(document).on('submit', '#bank_form', function () {
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('party/save_company_banks') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                            var json = $.parseJSON(response);
                            if (json['success'] == 'false'){
                                    show_notify(json['msg'],false); 
                            }
                            if (json['success'] == 'Added'){
                                    window.location.href = "<?php echo base_url('party/company_banks') ?>";
                            }
                            if (json['success'] == 'Updated'){
                                    window.location.href = "<?php echo base_url('party/company_banks') ?>";
                            }
			},
                    });
                    return false;
            });
       
        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=company_banks',
                    success: function (data) {
                        tr.remove();
                        window.location.href = "<?php echo base_url('party/company_banks') ?>";
                    }
                });
            }
        });

    });
</script>

