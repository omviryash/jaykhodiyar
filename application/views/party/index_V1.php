<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Master : View Party</small><button type="button" class="btn btn-info btn-xs pull-right">Save Party</button>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Party Details</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Contact Person</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Address Works</a></li>
                    <li><a href="#tab_4" data-toggle="tab">Terms & Conds</a></li>
                    <li><a href="#tab_5" data-toggle="tab">Tax & Excise</a></li>
                    <li><a href="#tab_6" data-toggle="tab">General</a></li>
                    <li><a href="#tab_7" data-toggle="tab">Company Profile</a></li>
                    <li><a href="#tab_8" data-toggle="tab">Billing Terms</a></li>
                    <li><a href="#tab_9" data-toggle="tab">Delivery Addresses</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form action="index.php" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Horizontal Form -->
                                    <div class="box box-info">
                                        <form class="form-horizontal">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Party Code</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Party Name</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Party Type</label>

                                                            <div class="col-sm-5">
                                                                <select class="form-control input-sm">
                                                                    <option>Customer-Expert</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <select class="form-control input-sm">
                                                                    <option>Manufacturer</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Branch</label>

                                                            <div class="col-sm-4">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                            <div class="col-sm-5 dispaly-flex">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">&nbsp;
                                                                <img alt="" src="<?=image_url('1.png');?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group dispaly-flex">
                                                            <img alt="" src="<?=image_url('2.png');?>">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Active</label>

                                                            <div class="col-sm-9">
                                                                <input type="checkbox" class="input-sm" checked>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">OpBal</label>

                                                            <div class="col-sm-4">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="0.00">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <select class="form-control input-sm">
                                                                    <option>Cr</option>
                                                                    <option>Dr</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <button type="button" class="btn btn-info btn-xs">Reference</button>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Credit Limit</label>

                                                            <div class="col-sm-5">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="0.00">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <!-- <div class="box-footer">
                                              <button type="submit" class="btn btn-info pull-right">Sign in</button>
                                            </div> -->
                                            <!-- /.box-footer -->
                                        </form>
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <div class="col-md-12">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <fieldset class="scheduler-border">
                                                    <legend class="scheduler-border text-primary text-bold"> Party Details </legend>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Address</label>

                                                        <div class="col-sm-9">
                                                            <textarea class="form-control" rows="1"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Area</label>

                                                        <div class="col-sm-9 dispaly-flex">
                                                            <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="">&nbsp;
                                                            <img alt="" src="<?=image_url('1.png');?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Pin-Code</label>

                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">City</label>

                                                        <div class="col-sm-9 dispaly-flex">
                                                            <input type="text" class="form-control input-sm city" name="city" id="inputEmail3" placeholder="">&nbsp;
                                                            <img alt="" src="<?=image_url('1.png');?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">State</label>

                                                        <div class="col-sm-9 dispaly-flex">
                                                            <input type="text" class="form-control input-sm state" name="state" id="inputEmail3" placeholder="">&nbsp;
                                                            <img alt="" src="<?=image_url('1.png');?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Country</label>

                                                        <div class="col-sm-9 dispaly-flex">
                                                            <input type="text" class="form-control input-sm country" name="country" id="inputEmail3" placeholder="">&nbsp;
                                                            <img alt="" src="<?=image_url('1.png');?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone No.</label>

                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Fax No.</label>

                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">E-mail ID</label>

                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label input-sm">Web Site</label>

                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <fieldset class="scheduler-border">
                                                            <legend class="scheduler-border text-primary text-bold"> Get Party Details </legend>
                                                            <div class="form-group">
                                                                <div class="col-sm-6">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox"> From Quatation
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-9">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox"> From Sales Support
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-info btn-xs pull-right">Get Party Detail</button>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <fieldset class="scheduler-border">
                                                            <legend class="scheduler-border text-primary text-bold"> Account Details </legend>
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 control-label input-sm">Group Code</label>

                                                                <div class="col-sm-6 dispaly-flex">
                                                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">&nbsp;
                                                                    <img alt="" src="<?=image_url('3.png');?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 control-label input-sm">Group Name</label>

                                                                <div class="col-sm-9">
                                                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row form-group">
                                                            <button type="button" class="btn btn-info btn-xs pull-right button-width" style="margin-top: 7px;">Payment Schedule</button>
                                                        </div>
                                                        <div class="row form-group">
                                                            <button type="button" class="btn btn-info btn-xs pull-right button-width">Preview Envelope</button>
                                                        </div>
                                                        <div class="row form-group">
                                                            <button type="button" class="btn btn-info btn-xs pull-right button-width">Export Invoice Image</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form action="index.php" name="add_contact_person" method="post">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Name</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Priority</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Designation</label>

                                                            <div class="col-sm-9">
                                                                <select class="form-control input-sm">
                                                                    <option>Designation</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Department</label>

                                                            <div class="col-sm-9">
                                                                <select class="form-control input-sm">
                                                                    <option>Department</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone No.</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Mobile No.</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Fax No.</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Email</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Note</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <br/>
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-10">
                                                        <table class="table custom-table">
                                                            <thead>
                                                            <tr>
                                                                <th>Priority</th>
                                                                <th>Name</th>
                                                                <th>Designation</th>
                                                                <th>Department</th>
                                                                <th>Phone No.</th>
                                                                <th>Email</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Person 1</td>
                                                                <td>Web Dev</td>
                                                                <td>Dept 1</td>
                                                                <td>9874561234</td>
                                                                <td>person1@gmail.com</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>Person 2</td>
                                                                <td>Web Dev</td>
                                                                <td>Dept 2</td>
                                                                <td>9874561234</td>
                                                                <td>person2@gmail.com</td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td>Person 3</td>
                                                                <td>Web Dev</td>
                                                                <td>Dept 3</td>
                                                                <td>9874561234</td>
                                                                <td>person3@gmail.com</td>
                                                            </tr>
                                                            <tr>
                                                                <td>4</td>
                                                                <td>Person 4</td>
                                                                <td>Web Dev</td>
                                                                <td>Dept 4</td>
                                                                <td>9874561234</td>
                                                                <td>person4@gmail.com</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Horizontal Form -->
                                <div class="box box-info">
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-md-4 input-sm">
                                                                Same As Party Details ?
                                                            </label>
                                                            <label class="col-md-4">
                                                                <input type="radio" name="address_work" class="address_work" checked> Yes &nbsp;&nbsp;<input type="radio" name="address_work" class="address_work"> No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Address</label>

                                                            <div class="col-sm-9">
                                                                <textarea class="form-control input-sm" id="inputEmail3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">City</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">State</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Country</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone 1</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Email</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Web</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone 2</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Phone 3</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label input-sm">Note</label>

                                                            <div class="col-sm-9">
                                                                <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_4">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_5">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">TIN VAT No.</label>

                                <div class="col-sm-9">
                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">TIN CST No.</label>

                                <div class="col-sm-9">
                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">ECC No.</label>

                                <div class="col-sm-9">
                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">PAN No.</label>

                                <div class="col-sm-9">
                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">Range</label>

                                <div class="col-sm-9">
                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">Division</label>

                                <div class="col-sm-9">
                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-md-3 control-label input-sm">Service Tax No.</label>

                                <div class="col-sm-9">
                                    <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_6">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_7">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_8">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_9">

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".city").autocomplete({
            source: "<?=BASE_URL?>party/fetch_autocomplete_city",
            minLength: 1,
            select: function(event, ui) {
                $(".city").val(ui.item.value);
            }
        })
    });
</script>