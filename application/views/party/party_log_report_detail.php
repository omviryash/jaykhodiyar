<style>
    .party-table > thead > tr > th {
        white-space: nowrap;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Party Log Detail</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Party log</h3>
                            <div class="table-responsive">
                                <table class="table table-bordered party-table custom-table">
                                    <thead>
                                    <tr>
                                        <th width="10%">Log id</th>
                                        <th width="20%">Log Time</th>
                                        <th width="10%">Operation</th>
                                        <th width="10%">Party id</th>
                                        <th width="10%">Party code</th>
                                        <th width="15%">Party name</th>
                                        <th width="15%">Reference</th>
                                        <th width="15%">Ref. desc</th>
                                        <th width="15%">Party type 1</th>
                                        <th width="15%">Party type 2</th>
                                        <th width="15%">Branch</th>
                                        <th width="15%">Project</th>
                                        <th width="15%">Address</th>
                                        <th width="15%">Area</th>
                                        <th width="15%">Pin code</th>
                                        <th width="15%">City</th>
                                        <th width="15%">State</th>
                                        <th width="15%">Country</th>
                                        <th width="15%">Phone no</th>
                                        <th width="15%">Fax no</th>
                                        <th width="15%">Email id</th>
                                        <th width="15%">website</th>
                                        <th width="15%">Opening bal</th>
                                        <th width="15%">Credit limit</th>
                                        <th width="15%">Active</th>
                                        <th width="15%">Tin vat no</th>
                                        <th width="15%">Tin cst no</th>
                                        <th width="15%">Ecc no</th>
                                        <th width="15%">Pan no</th>
                                        <th width="15%">Range</th>
                                        <th width="15%">Division</th>
                                        <th width="15%">Service tax no</th>
                                        <th width="15%">Utr no</th>
                                        <th width="15%">Work address</th>
                                        <th width="15%">Work city</th>
                                        <th width="15%">Work state</th>
                                        <th width="15%">Work country</th>
                                        <th width="15%">Work email</th>
                                        <th width="15%">Work web</th>
                                        <th width="15%">Work phone1</th>
                                        <th width="15%">Work phone2</th>
                                        <th width="15%">Work phone3</th>
                                        <th width="15%">Work note</th>
                                        <th width="15%">Created by</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($party_log_data)):?>
                                        <?php foreach($party_log_data as $party_row):?>
                                            <tr>
                                                <td><?=$party_row['id'];?></td>
                                                <td><?=$party_row['created_at'];?></td>
                                                <td><?=$party_row['operation_type'];?></td>
                                                <td><?=$party_row['party_id'];?></td>
                                                <td><?=$party_row['party_code'];?></td>
                                                <td><?=$party_row['party_name'];?></td>
                                                <td><?=$party_row['reference'];?></td>
                                                <td><?=$party_row['reference_description'];?></td>
                                                <td><?=$party_row['sales'];?></td>
                                                <td><?=$party_row['sales_type'];?></td>
                                                <td><?=$party_row['branch'];?></td>
                                                <td><?=$party_row['project'];?></td>
                                                <td><?=$party_row['address'];?></td>
                                                <td><?=$party_row['area'];?></td>
                                                <td><?=$party_row['pincode'];?></td>
                                                <td><?=$party_row['city'];?></td>
                                                <td><?=$party_row['state'];?></td>
                                                <td><?=$party_row['country'];?></td>
                                                <td><?=$party_row['phone_no'];?></td>
                                                <td><?=$party_row['fax_no'];?></td>
                                                <td><?=$party_row['email_id'];?></td>
                                                <td><?=$party_row['website'];?></td>
                                                <td><?=$party_row['opening_bal'];?></td>
                                                <td><?=$party_row['credit_limit'];?></td>
                                                <td><?=$party_row['active'];?></td>
                                                <td><?=$party_row['tin_vat_no'];?></td>
                                                <td><?=$party_row['tin_cst_no'];?></td>
                                                <td><?=$party_row['ecc_no'];?></td>
                                                <td><?=$party_row['pan_no'];?></td>
                                                <td><?=$party_row['range'];?></td>
                                                <td><?=$party_row['division'];?></td>
                                                <td><?=$party_row['service_tax_no'];?></td>
                                                <td><?=$party_row['utr_no'];?></td>
                                                <td><?=$party_row['w_address'];?></td>
                                                <td><?=$party_row['wcity'];?></td>
                                                <td><?=$party_row['wstate'];?></td>
                                                <td><?=$party_row['wcountry'];?></td>
                                                <td><?=$party_row['w_email'];?></td>
                                                <td><?=$party_row['w_web'];?></td>
                                                <td><?=$party_row['w_phone1'];?></td>
                                                <td><?=$party_row['w_phone2'];?></td>
                                                <td><?=$party_row['w_phone3'];?></td>
                                                <td><?=$party_row['w_note'];?></td>
                                                <td><?=$party_row['created_by'];?></td>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h3>Contact person log</h3>
                            <table class="table table-bordered contact-person-table custom-table">
                                <thead>
                                    <tr>
                                        <th>Log Id</th>
                                        <th>Log Time</th>
                                        <th>Operation</th>
                                        <th>Priority</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Department</th>
                                        <th>Mobile No.</th>
                                        <th>Phone No.</th>
                                        <th>Email</th>
                                        <th>Fax No.</th>
                                        <th>Note</th>
                                        <th>Created By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('.party-table').DataTable({
            "order": ['1','desc'],
            "paging": false
        });
        var table = $('.contact-person-table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": ['1','desc'],
            "ajax": {
                "url": "<?php echo site_url('party/contact_person_log_detail_datatable')?>",
                "type": "POST",
                "data": function(d) {
                    d.party_id = '<?=$party_id;?>';
                }
            },
            "columnDefs": [
                {
//                    "targets": [0],
//                    "orderable": false,
                }
            ],
            "scrollY": 250,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "rowCallback": function( row, data, index ) {
                if ( data[1] == "insert" )
                {
                    $('td',row).css('background-color', '#e5e5e5');
                }
                else if ( data[1] == "update" )
                {
                }else{
                }
            }
        });
    });
</script>