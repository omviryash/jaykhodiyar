<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_SMS_TOPICS_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_SMS_TOPICS_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_SMS_TOPICS_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Sms Topics</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-7">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Sms Topics</div>
                                            <div style="margin: 10px;">
                                                <table id="example1" class="table custom-table sales-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Action</th>
                                                            <th>Sms Topic</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if (!empty($results)) {
                                                            foreach ($results as $row) {
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php if ($isEdit) { ?>
                                                                            <a href="<?= base_url('party/sms_topic/' . $row->id) ?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> 
                                                                        <?php } if ($isDelete) { ?>
                                                                            <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?= base_url('party/delete/' . $row->id); ?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td><?= $row->topic_name ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($isAdd || $isEdit) { ?>
                                        <div class="col-md-5">
                                            <div class="panel panel-default">
                                                <div class="panel-heading clearfix">
                                                    <?php if (isset($id) && !empty($id)) { ?>Edit 
                                                    <?php } else {
                                                        if ($isAdd) {
                                                            $btn_disable = null;
                                                        } else {
                                                            $btn_disable = 'disabled';
                                                        } ?>Add 
                                                          <?php } ?>	Sms Topic
                                                </div>
                                                <div style="margin:10px">	
                                                    <form method="POST" 
                                                        <?php if (isset($id) && !empty($id)) { ?>
                                                              action="<?= base_url('party/update_sms_topic') ?>" 
                                                        <?php } else { ?>
                                                              action="<?= base_url('party/add_sms_topic') ?>" 
                                                        <?php } ?> id="form_sms_topic">
                                                        <?php if (isset($id) && !empty($id)) { ?>
                                                            <input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
                                                        <?php } ?>
                                                        <div class="form-group">
                                                            <label for="sms_topic" class="col-sm-2 input-sm">Sms Topic<span class="required-sign">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control input-sm" id="sms_topic" name="topic_name" value="<?php echo $sms_topic; ?>" <?php echo $btn_disable; ?>>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 input-sm">Message</label>
                                                            <div class="col-sm-10">
                                                                <textarea name="message" id="sms_topic" class="form-control" <?php echo $btn_disable; ?> rows="6" ><?php echo $message; ?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group" style="margin:7px !important;"></div>
                                                        <?php if (isset($id) && !empty($id)) { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Sms Topic</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable; ?>">Add Sms Topic</button>
                                                        <?php } ?>
                                                    </form>
                                                </div>
                                            </div>                                            
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        $("#example1").DataTable({
            "scrollY": "300px",
            "scrollCollapse": true,
            "aaSorting": [[1, 'asc']],
            "paging": false,
        });

        $("#form_sms_topic").on("submit", function (e) {
            e.preventDefault();
            if ($("#sms_topic").val() == "") {
                show_notify('Fill value Sms Topic name.', false);
                return false;
            }
            <?php if (isset($id) && !empty($id)) { ?>
                var success_status = check_is_unique('sms_topic', 'topic_name', $("#sms_topic").val(), 'id', '<?= $id ?>');
            <?php } else { ?>
                var success_status = check_is_unique('sms_topic', 'topic_name', $("#sms_topic").val());
            <?php } ?>
            if (success_status == 0) {
                if ($('p.sms_topic-unique-error').length > 0) {
                    $("p.sms_topic-unique-error").text('This Sms Topic is already exist!');
                } else {
                    $("#sms_topic").after("<p class='text-danger reference-unique-error'>This Sms Topic is already exist!</p>");
                }
                return false;
            } else {
                $("p.sms_topic-unique-error").text(' ');
            }

            var value = $("#sms_topic").val();
            if (value != '') {
                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (datma) {
                        window.location.href = "<?php echo base_url('party/sms_topic') ?>";
                    }
                });
            }
        });

        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=sms_topic',
                    success: function (data) {
                        tr.remove();
                        window.location.href = "<?php echo base_url('party/sms_topic') ?>";
                    }
                });
            }
        });
    });
</script>
