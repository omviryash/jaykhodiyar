<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Party Log Detail</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>
                                    <small class="text-primary text-bold">Party Log Detail</small>
                                </h1>                                                                
                                <table class="table table-bordered">
                                    <tbody>
                                        <?php if(count($party_log_record)):?>
                                        <?php foreach($party_log_record[0] as $key => $val):?>
                                            <tr>
                                                <td width="20%"><?php echo $key; ?></td>
                                                <td width="80%"><?php echo $val; ?></td>
                                            </tr>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </tbody>
                                </table>
                                <h1>
                                    <small class="text-primary text-bold">Party Log: Contact Person Log</small>
                                </h1>   
                                <?php foreach($contact_log_record as $row):?>                                
                                <table class="table table-bordered">
                                    <tbody>                                
                                        <?php foreach($row as $key => $val):?>
                                            <tr <?=$key == 'id'?'style="background-color:#a7cbe2;"':''?>>
                                                <td width="20%"><?php echo $key; ?></td>
                                                <td width="80%"><?php echo $val; ?></td>
                                            </tr>                                        
                                        <?php endforeach;?>        
                                    </tbody>
                                </table>
                                <?php endforeach;?>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
    });
</script>