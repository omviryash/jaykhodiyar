<div class="box-body no-padding">
	<ul class="nav nav-pills nav-stacked">
		<li class="<?=$current_folder_name == 'important'?'active':''?>"><a href="javascript:void(0);" class="btn-view-folder-mail" data-folder_name="important"><i class="fa fa-circle-o text-red"></i> Important</a></li>
		<?php
		if(isset($mailboxes) && is_array($mailboxes) && count($mailboxes) > 0) {
			foreach ($mailboxes as $mailbox) {
				?>
				<li class="<?= $current_folder_name == $mailbox['mailbox_label'] ? 'active' : '' ?>">
					<a href="javascript:void(0);" class="btn-view-folder-mail" data-folder_name="<?=$mailbox['mailbox_label'];?>">
						<i class="fa fa-circle-o text-red"></i> <?= $mailbox['mailbox_label']; ?>
					</a>
				</li>
				<?php
			}
			?>
			<?php
		}else {
			?>
			<li class="<?=$current_folder_name == 'important'?'active':''?>"><a href="<?=base_url()?>mail-system/folder-mails/important"><i class="fa fa-circle-o text-red"></i> Important</a></li>
			<li class="<?=$current_folder_name == 'promotions'?'active':''?>"><a href="<?=base_url()?>mail-system/folder-mails/promotions"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
			<li class="<?=$current_folder_name == 'social'?'active':''?>"><a href="<?=base_url()?>mail-system/folder-mails/social"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
			<?php
		}
		?>
	</ul>
</div>
<form id="frm-view-folder-mail" action="<?=base_url();?>mail-system/folder-mails/" method="post">
	<input type="hidden" name="folder_name" id="folder_name">
</form>