<?php
	if($this->uri->segment(3) == 'compose'){
		$current_folder_name = "Inbox";
	}elseif(isset($curr_folder_name)){
		$current_folder_name = $curr_folder_name;
	}else{
		$current_folder_name = $this->uri->segment(2);
	}
?>
<style>
	td.mailbox-subject{
		white-space: nowrap;
		max-width: 620px !important;
		overflow: hidden;
	}
	td.mailbox-attachment{
		max-width: 30px !important;
	}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Mailbox
			<small> <span class="top-no-unread-mails"><?=$mails_data['count_unseen_mails']?></span> new messages</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Mailbox</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-2">
				<?php $this->load->view('mail_system/mailbox_list', array('mails_data'=>$mails_data,'current_folder_name'=>$current_folder_name));?>
			</div>
			<!-- /.col -->

			<div class="col-md-3" style="padding-left: 0px">
				<?php $this->load->view('mail_system/mail_list', array('mails_data'=>$mails_data,'current_folder_name'=>$current_folder_name));?>
			</div>

			<div class="col-md-7 email-detail-container" style="padding-left:0px;">
				<?php $this->load->view('shared/success_false_notify');?>

				<?php
					if(isset($mail_detail) && !empty($mail_detail) && $this->uri->segment(2) == 'read') {
						$this->load->view('mail_system/read_email', array('mail_detail' => $mail_detail,'message_no'=>$message_no,'current_folder_name'=>$current_folder_name));
					}elseif($this->uri->segment(3) == 'compose'){
						$this->load->view('mail_system/compose_email',array('action'=>'compose','current_folder_name'=>$current_folder_name));
					}elseif(isset($mail_detail) && !empty($mail_detail) && $this->uri->segment(3) == 'forward'){
						$this->load->view('mail_system/compose_email',array('mail_detail' => $mail_detail,'action'=>'forward','current_folder_name'=>$current_folder_name));
					}elseif(isset($mail_detail) && !empty($mail_detail) && $this->uri->segment(3) == 'reply'){
						$this->load->view('mail_system/compose_email',array('mail_detail' => $mail_detail,'action'=>'reply','current_folder_name'=>$current_folder_name));
					}
				?>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<form id="frm_mailbox" action="" method="post">
	<input type="hidden" name="action" class="action" value="">
	<input type="hidden" name="folder_name" class="folder_name" value="<?=$current_folder_name?>">
	<input type="hidden" name="message_no" class="message_no">
</form>
<script>
	var loader = "";
	loader += '<div class="box">';
		loader += '<div class="overlay">';
			loader += '<i class="fa fa-refresh fa-spin"></i>';
		loader += '</div>';
		loader += '<div class="box-header with-border">';
			loader += '<h3 class="box-title">Loading...</h3>';
		loader += '</div>';
	loader += '</div>';
	$(document).ready(function(){

		$(document).on('click','.btn-trash-mail',function(){
			$("#frm_mailbox").find('input[name="message_no"]').val($(this).data('message_no'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system/delete-mail');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.fileinput-remove',function(){
			$("input.attachment_url").each(function(index){
				if(index == 0){
					$(this).val('');
				}else{
					$(this).remove();
				}
			});
		});

		/*********** Forward Mail *********/
		$(document).on('click','.btn-forward-mail',function(){
			$("#frm_mailbox").find('input[name="message_no"]').val($(this).data('message_no'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system/compose-email/forward');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-view-folder-mail',function(){
			$("form#frm-view-folder-mail").find("#folder_name").val($(this).data('folder_name'));
			$("form#frm-view-folder-mail").submit();
		});

		$(document).on('click','.btn-reply-mail',function(){
			$("#frm_mailbox").find('input[name="message_no"]').val($(this).data('message_no'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system/compose-email/reply');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-read-mail',function(){
			if($('.email-detail-container').find('.box').length == 0){
				$('.email-detail-container').html(loader);
			}else{
				$('.email-detail-container').find('.overlay').removeClass('hidden');
			}
			var button = $(this);
			var td = $(this).closest('td');
			var formdata = new FormData();
			formdata.append("message_no",$(this).data('message_no'));
			if(td.find('input[name="folder_name"]').length > 0){
				formdata.append("folder_name",td.find('input[name="folder_name"]').val());
			}else {
				formdata.append("folder_name","<?=$current_folder_name;?>");
			}
			$.ajax({
				url: '<?=base_url()?>mail-system/read/',
				type: "POST",
				data: formdata,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					$('.email-detail-container').html(data.email_detail);
					if(button.hasClass('btn_view_unread_mail')) {
						var email_address = td.find('.btn-read-mail').text();
						td.find('.btn-read-mail').text(email_address);
						var mailbox_subject = td.find('.mailbox-subject').text();
						td.find('.mailbox-subject').text(mailbox_subject);
						if(data.no_unread_mails > 0){
							$('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">' + data.no_unread_mails + '</span>');
							$('.sidebar-unread-mails').append('<span class="pull-right-container"><span class="label label-primary pull-right">' + data.no_unread_mails + '</span></span>');
							$('.top-no-unread-mails').text(data.no_unread_mails);
							if ($('.mailbox-list-inbox-label').find('.no-unread-mails').length == 0) {
								$('.mailbox-list-inbox-label').append('<span class="label label-primary pull-right no-unread-mails">' + data.no_unread_mails + '</span>');
							} else {
								$('.mailbox-list-inbox-label').find('.no-unread-mails').text(data.no_unread_mails);
							}
							$('.messages-menu').find('ul.dropdown-menu').remove();
							$('.messages-menu').append(data.unread_mails);
						}else{
							$('.messages-menu').find('ul.dropdown-menu').remove();
							$('.mailbox-list-inbox-label').find('.no-unread-mails').remove();
							$('.unread-mail-icon').find('.inbox-unread-mails').remove();
							$('.sidebar-unread-mails').find('.pull-right-container').remove();
							$('.top-no-unread-mails').text(0);
						}
					}
				}
			});
		});

		$(document).on('click','.btn-star',function(){
			var star_button = $(this);
			if($(this).find('.fa-star-o').length == 0){
				var url = "<?=base_url()?>mail-system/set-starred-flag/";
			}else{
				var url = "<?=base_url()?>mail-system/remove-starred-flag/";
			}
			<?php
			if($this->uri->segment(2) == 'starred'){
			?>
			star_button.closest('tr').fadeOut('slow');
			<?php
			}
			?>
			var formdata = new FormData();
			formdata.append("message_no",$(this).data('mail_no'));
			formdata.append("folder_name","<?=$current_folder_name;?>");
			$.ajax({
				url: url,
				type: "POST",
				data: formdata,
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {

				}
			});
		});
	});
</script>
<?php
	if(in_array($this->uri->segment(3),array('compose','forward','reply'))) {
		?>
		<script src="<?= plugins_url('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
		<script>
			$(function () {
				$("#compose-textarea").wysihtml5();
				$('#discardbtnicon').click(function () {
					window.location.href = '<?=base_url()?>mail-system/inbox/';
				});
				$(document).on('change', '#upload_attachment', function () {
					$("form#frmComposeMail").find('.overlay').removeClass('hidden');
					var formdata = new FormData();
					formdata.append("upload_attachment", $('#upload_attachment')[0].files[0]);
					$.ajax({
						url: "<?=base_url('mail-system/upload_attachment/');?>",
						type: "POST",
						data: formdata,
						contentType: false,
						cache: false,
						processData: false,
						dataType: 'json',
						success: function (data) {
							$("form#frmComposeMail").find('.overlay').addClass('hidden');
							if (data.success == true) {
								show_notify(data.message, true);
								if($(".attachment_url:last").val() == ''){
									$(".attachment_url:last").val(data.attachment_url);
								}else{
									var attach_html = $(".attachment_url:last").clone();
									attach_html.insertAfter($(".attachment_url:last"));
									$(".attachment_url:last").val(data.attachment_url);
								}
							} else {
								show_notify(data.message, false);
							}
						}
					});
				});
			});
		</script>
		<?php
	}
?>