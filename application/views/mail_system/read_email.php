<div class="box box-primary">
	<div class="overlay hidden"><i class="fa fa-refresh fa-spin"></i></div>
	<div class="box-header with-border">
		<h3 class="box-title">Read Mail</h3>
		<div class="box-tools pull-right">

		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body no-padding">
		<div class="mailbox-read-info">
			<h3><?=$mail_detail['subject'];?></h3>
			<h5><?=$mail_detail['username']?></h5>
			<span class="mailbox-read-time pull-right"><?=isset($mail_detail['received_at'])?$mail_detail['received_at']:'';?></span></h5>
		</div>
		<!-- /.mailbox-read-info -->
		<div class="mailbox-controls with-border text-center">
			<div class="btn-group">
				<a href="javascript:void(0);" class="btn-trash-mail btn btn-default btn-sm" data-message_no="<?=$message_no?>" data-toggle="tooltip" data-container="body" title="Delete">
					<i class="fa fa-trash-o"></i></a>
				<a  href="javascript:void(0);" class="btn-reply-mail btn btn-default btn-sm" data-message_no="<?=$message_no?>" data-toggle="tooltip" data-container="body" title="Reply">
					<i class="fa fa-reply"></i></a>
				<a href="javascript:void(0);" class="btn-forward-mail btn btn-default btn-sm" data-message_no="<?=$message_no?>" data-toggle="tooltip" data-container="body" title="Forward">
					<i class="fa fa-share"></i></a>
			</div>
			<!-- /.btn-group -->
			<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
				<i class="fa fa-print"></i></button>
		</div>
		<!-- /.mailbox-controls -->
		<div class="mailbox-read-message">
			<?=isset($mail_detail['body'])?nl2br($mail_detail['body']):'';?>
		</div>
		<!-- /.mailbox-read-message -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<ul class="mailbox-attachments clearfix">
			<?php
			if(isset($mail_detail['attachments']) && count($mail_detail['attachments']) > 0 ) { //print_r($mail_detail['attachments']);
				foreach ($mail_detail['attachments'] as $attachment) {
					if(in_array($attachment['subtype'],array('png','jpg','jpeg','gif'))){
						$mime_type = 'image/'.$attachment['subtype'];
						?>
						<li>
							<?php
								if(isset($attachment['url'])) {
									?>
										<span class="mailbox-attachment-icon has-img"><img src="<?=base_url().$attachment['url'];?>" alt="Attachment"></span>
									<?php
								} else {
									?>
									<span class="mailbox-attachment-icon has-img"><img src="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" alt="Attachment"></span>
									<?php
								}
							?>
							<div class="mailbox-attachment-info">
								<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-camera"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
							</div>
						</li>
						<?php
					}elseif(in_array($attachment['subtype'],array('pdf'))){
						$mime_type = 'application/pdf';
						?>
						<li>
							<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
							<div class="mailbox-attachment-info">
								<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
							</div>
						</li>
						<?php
					}elseif(in_array($attachment['subtype'],array('plain','txt'))){
						$mime_type = 'text/plain';
						?>
						<li>
							<span class="mailbox-attachment-icon"><i class="fa fa-file-text-o"></i></span>
							<div class="mailbox-attachment-info">
								<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
							</div>
						</li>
						<?php
					}elseif(in_array($attachment['subtype'],array('zip'))){
						$mime_type = 'application/'.$attachment['subtype'];
						?>
						<li>
							<span class="mailbox-attachment-icon"><i class="fa fa-file-zip-o"></i></span>
							<div class="mailbox-attachment-info">
								<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
							</div>
						</li>
						<?php
					}
					?>
					<?php
				}
			}
			?>
		</ul>
	</div>
	<!-- /.box-footer -->
	<div class="box-footer">
		<div class="pull-right">
			<a  href="javascript:void(0);" data-message_no="<?=$message_no;?>" class="btn-reply-mail btn btn-default"><i class="fa fa-reply"></i> Reply</a>
			<a  href="javascript:void(0);" data-message_no="<?=$message_no;?>" class="btn-forward-mail btn btn-default"><i class="fa fa-share"></i> Forward</a>
		</div>
		<a href="javascript:void(0);" data-message_no="<?=$message_no;?>" class="btn-trash-mail btn btn-default"><i class="fa fa-trash-o"></i> Delete</a>
		<button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
	</div>
	<!-- /.box-footer -->
</div>