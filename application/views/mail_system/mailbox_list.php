<a href="<?=base_url()?>mail-system/compose-email/compose" class="btn btn-primary btn-block margin-bottom">Compose</a>
<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Folders</h3>

        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body no-padding">
        <?php
            $main_label_active = $current_folder_name;
        ?>
        <ul class="nav nav-pills nav-stacked">
            <li class="<?=$main_label_active == 'inbox'?'active':'';?>">
                <a href="<?=base_url()?>mail-system/inbox/" class="mailbox-list-inbox-label"><i class="fa fa-inbox"></i> Inbox <?php if(isset($mails_data['inbox_unseen_mails']) && $mails_data['inbox_unseen_mails'] > 0){ ?><span class="label label-primary pull-right no-unread-mails"><?=$mails_data['inbox_unseen_mails']?></span><?php }?></a>
            </li>
            <li class="<?=$main_label_active == 'starred'?'active':'';?>">
                <a href="<?=base_url()?>mail-system/starred/"><i class="fa fa-star-o"></i> Starred</a>
            </li>
            <li class="<?=$main_label_active == 'sent-mail'?'active':'';?>">
                <a href="<?=base_url()?>mail-system/sent-mail/"><i class="fa fa-envelope-o"></i> Sent Mail</a>
            </li>
            <li class="<?=$main_label_active == 'drafts'?'active':'';?>">
                <a href="<?=base_url()?>mail-system/drafts/"><i class="fa fa-file-text-o"></i> Drafts</a>
            </li>
            <li class="<?=$main_label_active == 'spam'?'active':'';?>">
                <a href="<?=base_url()?>mail-system/spam/"><i class="fa fa-filter"></i> Spam <!--<span class="label label-warning pull-right">65</span>--></a>
            </li>
            <li class="<?=$main_label_active == 'trash'?'active':'';?>">
                <a href="<?=base_url()?>mail-system/trash/"><i class="fa fa-trash-o"></i> Trash</a>
            </li>
        </ul>
    </div>
    <!-- /.box-body -->
</div>
<!-- /. box -->
<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Folder</h3>

        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked sidebar-mail-label" id="sidebar-mail-label">
            <?php
            if(isset($mails_data['mailboxes']) && is_array($mails_data['mailboxes']) && count($mails_data['mailboxes']) > 0) {
                $this->load->view('mail_system/sidebar_mail_label', array('mailboxes' => $mails_data['mailboxes'],'current_folder_name'=>$current_folder_name));
            }
            ?>
        </ul>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->