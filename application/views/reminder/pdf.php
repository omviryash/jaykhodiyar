<html>
	<head>
		<title>Sales Order</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:10px;
			}
			tr > td:last-child{
				border-right: 1px solid !important;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:12px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				width: 33.33%;
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #000000;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="5" ><?=$customer_reminder_detail->topic; ?></td>
			</tr>
			<tr>
                <td class="text-center text-bold" colspan="2"><b>Manufacturer & Supplier</b></td>
				<td class=" text-bold" colspan="1">Reference No : </td>
				<td class=" text-bold" colspan="1"><?=$this->applib->get_reminder_letter_no($customer_reminder_detail->letter_no);?></td>
                <td class="text-bold" colspan="1"><?=strtotime($customer_reminder_detail->created_at) != 0?date('d/m/Y',strtotime($customer_reminder_detail->created_at)):''; ?></td>
			</tr>
			<tr>
                <td class="text-bold" colspan="2" style="font-size:15px;"><b><?=$company_details['name'];?></b></td>
                <td class="text-bold" colspan="1">Quotation No. : </td>
				<td class="text-bold" colspan="2"><?=$this->applib->get_quotation_ref_no($sales_order_data->quotation_no,$sales_order_item['item_code']);?></td>
			</tr>
			<tr>
				<td class="no-border-top text-bold" rowspan="4" colspan="2">
					<?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
                    <b>GST No : <?=$company_details['gst_no'];?></b>
                </td>
                <td class="text-bold" colspan="1">Salse Order No. :</td>
				<td class="text-bold" colspan="2"><?=$this->applib->get_sales_order_no($sales_order_data->sales_order_no);?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="1">Purchase Order No.:</td>
				<td class=" text-bold" colspan="1">
					<?php 
						if(!empty($sales_order_data->cust_po_no)){
							echo $sales_order_data->cust_po_no;
						} else { 
							echo $sales_order_data->sales_order_pref;
						} 
					?>
				</td>
                <td class="text-bold" colspan="1"><?=strtotime($sales_order_data->po_date) != 0?date('d/m/Y',strtotime($sales_order_data->po_date)):date('d/m/Y')?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="1">Sales Executive :</td>
				<td class=" text-bold" colspan="2"><?=$sales_order_data->created_name?></td>
			</tr>
			<tr>
                <td class=" text-bold" colspan="1">Letter Validity : </td>
				<td class=" text-bold" colspan="2"><?=$customer_reminder_detail->letter_validity?> Days</td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="2" width="50%"><b>Purchaser</b></td>
                <td class="text-center text-bold" colspan="3" width="50%"><b>Received Payment Details</b></td>
			</tr>
			<tr>
                <td class="text-bold" colspan="2" width="50%">
					<span style="font-size:15px;"><b><?=$sales_order_data->party_name?></b></span><br />
					<?=nl2br($sales_order_data->address);?><br />
					City : <?=$sales_order_data->city?> -  <?=$sales_order_data->pincode?> (<?=$sales_order_data->state?>) <?=$sales_order_data->country?>.<br />
					Email : <?=explode(",", $sales_order_data->party_email_id)[0];?><br />
					Tel No.:  <?=$sales_order_data->fax_no;?>,<br />
					Contact No.:  <?=$sales_order_data->p_phone_no;?><?= ($sales_order_data->p_phone_no != $sales_order_data->contact_person_phone) ? ', '.$sales_order_data->contact_person_phone : '' ?><br />
					Contact Person: <?=$sales_order_data->contact_person_name?><br />
                    <b>GST No. : <?=$sales_order_data->p_gst_no;?></b><?= isset($sales_order_data->party_cin_no) && !empty($sales_order_data->party_cin_no) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$sales_order_data->party_cin_no.'</b>' : ''; ?>
				</td>
				<td class="" valign="top" colspan="3" width="50%">
                    <?=nl2br($customer_reminder_detail->received_payment_detail); ?>
                </td>
			</tr>
			<tr>
				<td class="text-bold text-center" colspan="5" style="font-size:15px;"><b>Subject : <?=$customer_reminder_detail->subject; ?></b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="5" ><?=$customer_reminder_detail->content_data; ?></td>
			</tr>
			<tr>
				<td colspan="5">
					<table class="no-border">
						<tr class="no-border">
							<td align="center" class="no-border-top footer-detail-area" style="border-left: 0!important;">
								<strong>Letter Issued By</strong><br />
							</td>
							<td align="center" class="no-border-top footer-detail-area">
								<strong>Approved By</strong>
							</td>
							<td align="center" class="no-border-top footer-detail-area">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
						</tr>
						<tr class="no-border">
							<td class="no-border footer-sign-area text-bold no-border-left no-border-top no-border-bottom">
                                <?=$customer_reminder_detail->created_name?><br />
								Sales Executive Sign
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
								Authorized Sign
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
								<?=$company_details['m_d_name'];?><br/>
								Managing Director
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<i class="fa fa-angle-down"></i>
