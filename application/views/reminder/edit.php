<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="custom content-header">
		<h1>
			<small class="text-primary text-bold">Edit Reminder</small>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Reminder</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<form action="" id="main-frm" class="main-frm" data-parsley-validate novalidate >
								<div class="col-md-12">
									<input type="hidden" name="user_id" value="<?php echo $reminder[0]->user_id;?>"/>
									<input type="hidden" name="reminder_id" value="<?php echo $reminder[0]->reminder_id;?>"/>

									<div class="form-group">

										<label for="inputEmail3" class="col-md-1 control-label input-sm text-right">Remind</label>
										<div class="col-sm-8">
											<textarea class="form-control" id="remind" name="remind" placeholder="" required ><?php echo $reminder[0]->remind;?></textarea>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="clearfix">&nbsp;</div>
									<div class="col-md-12 row" id="addmore_contact">
										<div class="corporat_contact row col-md-12">
											<div class="form-group col-md-2">
												<label for="inputEmail3" class="col-sm-1 control-label input-sm">Date</label>
												<div class="">
													<input type="text" name="reminder_date" class="form-control input-sm pull-right input-datepicker" value="<?= $reminder_date_1; ?>" required >
												</div>
											</div>
											<div class="form-group col-md-2">
												<label for="inputEmail3" class="col-sm-1 control-label input-sm">Time</label>

												<div class="bootstrap-timepicker" style="position:initial;">
													<input type="text" name="reminder_time" class="form-control input-sm pull-right timepicker add_tp" value="<?= $reminder_time_1; ?>" required>
												</div>

											</div>
											<div class="form-group  col-md-2">
												<label for="inputEmail3" class="col-sm- control-label input-sm">Assigned To</label>
												<div class="">
													<select name="assigned_to" id="assigned_to" class="form-control input-sm assigned_to" required >
														<option value="">--Select--</option>
														<?php foreach($agent_list as $agent): ?>
														<option value="<?= $agent->staff_id; ?>" <?=$reminder[0]->assigned_to == $agent->staff_id?'selected="selected"':''?>><?= $agent->name; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
											<div class="form-group  col-md-2">
												<label for="inputEmail3" class="col-sm- control-label input-sm">Status</label>
												<div class="">
													<select class="form-control input-sm select2" name="status" id="status">
														<option value="1" <?=$reminder[0]->status == 1 ?'selected="selected"':''?>>Open</option>
														<option value="0" <?=$reminder[0]->status == 0 ?'selected="selected"':''?>>Close</option>
													</select>
												</div>
											</div>
											<div class="form-group  col-md-2">
												<label for="inputEmail3" class="col-sm- control-label input-sm">Priority</label>
												<div class="">
													<select class="form-control input-sm select2" name="priority" id="priority">
														<option value="1" <?=$reminder[0]->priority == 1 ?'selected="selected"':''?>>High</option>
														<option value="2" <?=$reminder[0]->priority == 2 ?'selected="selected"':''?>>Medium</option>
														<option value="3" <?=$reminder[0]->priority == 3 ?'selected="selected"':''?>>Low</option>
													</select>
												</div>
											</div>
											<div class="form-group col-md-3">
												<div class="remove_contact" style="visibility: hidden; margin-top: 20px;"><span class="btn btn-primary" title="Remove Contact">x</span></div>
											</div>
										</div>
									</div>


								</div>
								<div class="col-md-12 row form-group"></div>
								<!--<input type="submit" value="submit" name="submit" class="btn btn-success btn-update-roles pull-right"> -->
								<div class="col-md-12 row form-group" style="margin-top: 20px;">
									<div class="col-md-12">
										<?php 
										$edit_role = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID, "edit"); 
										?>
										<?php if($edit_role): ?>
										<input type="submit" class="btn btn-primary btn-update-roles pull-right" value="Update" />
										<?php endif;?>
									</div>
								</div>

							</form>
						</div>
					</div>
					<div class="box-footer">
						
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#main-frm').parsley();
		$('select.assigned_to').select2();
		$(document).on('submit', '#main-frm', function(){
			$("#ajax-loader").show(); 
			$.ajax({
				type: 'post',
				url: '<?=base_url();?>reminder/edit/<?=$reminder[0]->reminder_id;?>',
				data: $('#main-frm').serialize(),
				success: function(data) {
					var data = JSON.parse(data);
					$msg = data.msg;
					$("#ajax-loader").hide(); 
					if(data.status == 1)
					{
						//$("#main-frm")[0].reset();
						setTimeout(location.reload.bind(location), 3000);
						$("#main-frm").trigger('reset');
						show_notify($msg,true);
					}
					else
					{
						show_notify($msg,false);
					}                    
				},
				error: function(e) {
					$("#ajax-loader").hide();
				}
			});          
			return false;
		});
	});
</script>
