<div class="content-wrapper">
	<?php if($this->session->flashdata('success') == true){?>
	<div><div class="col-sm-12">
		<div class="alert alert-success alert-dismissible" style="margin-top: 15px;" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
		</div>
	</div></div><div class="clearfix"></div>
	<?php }?>
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small class="text-primary text-bold">Customer Reminder</small>
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<?php if($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER,"add")) { ?>
			<div class="col-md-12">
				<div class="box box-primary">
					<form method="POST"
							  <?php if(isset($id) && !empty($id)){ ?>
							  action="<?=base_url('reminder/update_customer_reminder') ?>" 
							  <?php } else { ?>
							  action="<?=base_url('reminder/add_customer_reminder') ?>" 
							  <?php } ?> id="customer_reminder_form">
							<?php if(isset($id) && !empty($id)){ ?>
							<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
							<?php } ?>
						<div class="box-header with-border">
							<h3 class="box-title">
								<?php if(isset($id) && !empty($id)){ ?>
									Edit Reminder
								<?php } else { ?>
									Add Reminder
								<?php } ?>
							</h3>
							<a href="<?=base_url('reminder/customer_reminder') ?>" class="btn btn-info btn-xs pull-right">Add Reminder</a>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label for="topic" class="col-md-2 input-sm">Topic</label>
										<div class="col-md-10">
											<input type="text" class="form-control input-sm" id="topic" name="topic" value="<?php echo $topic; ?>" required >
										</div>
									</div>
								</div> 
								<div class="col-md-7">
									<div class="form-group">
										<label for="subject" class="col-md-2 input-sm">Subject</label>
										<div class="col-md-10">
											<input type="text" class="form-control input-sm" id="subject" name="subject" value="<?php echo $subject; ?>" required >
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="inputEmail3" class="col-md-1 input-sm">Variables</label>
										<div class="col-md-10">
											{{Party_Name}}, &nbsp;&nbsp;{{Address}}, &nbsp;&nbsp;{{City}}, &nbsp;&nbsp;{{Pin}}, &nbsp;&nbsp;{{State}}, &nbsp;&nbsp;{{Country}}, &nbsp;&nbsp;{{Email}}, &nbsp;&nbsp;{{Tel_No}}, &nbsp;&nbsp;{{Contact_No}}, &nbsp;&nbsp;{{Contact_Person}}, &nbsp;&nbsp;{{GST_No}}
										</div>
									</div>
								</div><br /><br /><br />
								<div class="col-md-12">
									<textarea id="editor1" name="content" rows="10" cols="80"> <?php echo $content_data; ?> </textarea>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<input type="submit" class="btn btn-info btn-submit pull-right" value="Submit" />
						</div>
					</form>
				</div>
            </div>
            <?php } ?>
			<?php if($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER,"view")) { ?>
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Customer Reminder List</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<table id="customer_reminder_table" class="table custom-table customer_reminder-table">
									<thead>
										<tr>
											<th width="50">Action</th>
											<th>Topic</th>
											<th>Subject</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
        </div>
	</section>

</div>
<script type="text/javascript" src="<?=base_url('resource/plugins/ckeditor/ckeditor.js');?>"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		CKEDITOR.replace('editor1')

		table = $('#customer_reminder_table').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'asc']],
			"ajax": {
				"url": "<?php echo base_url('reminder/customer_reminder_datatable')?>",
				"type": "POST"
			},
			"scrollY": '350px',
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%",
			"columnDefs": [
				{"targets": 0, "orderable": false }
			]
		});

		$(document).on("submit", "#customer_reminder_form", function(e){
			e.preventDefault();
			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('customer_reminder','topic',$("#topic").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('customer_reminder','topic',$("#topic").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.topic-unique-error').length > 0){
					$("p.topic-unique-error").text('Reminder Topic already exist!');
				}else{
					$("#topic").after("<p class='text-danger topic-unique-error'>Reminder Topic already exist!</p>");
				}
				return false;
			}else{
				$("p.topic-unique-error").text(' ');
			}
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success: function(data){
					window.location.href = "<?php echo base_url('reminder/customer_reminder') ?>";
				}
			});
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=customer_reminder',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('reminder/customer_reminder') ?>";
					}
				});
			}
		});

	});
</script>

