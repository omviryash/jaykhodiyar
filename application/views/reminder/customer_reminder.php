<div class="content-wrapper">
	<?php if($this->session->flashdata('success') == true){?>
	<div><div class="col-sm-12">
		<div class="alert alert-success alert-dismissible" style="margin-top: 15px;" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
		</div>
	</div></div><div class="clearfix"></div>
	<?php }?>
	<!-- Content Header (Page header) -->
	<section class="custom content-header">
		<h1>
			<small class="text-primary text-bold">
			<?php
				$page_name = $this->uri->segment(2);
				$page_action = 'Add';
				if(isset($id) && !empty($id)){
					$page_action = 'Edit';
				}
				if($page_name == 'delivery_reminder'){ echo $page_action.' Delivery Reminder'; }
                if($page_name == 'finance_balance_payment'){ echo $page_action.' Finance Balance Payment'; }
				if($page_name == 'general_reminder'){ echo $page_action.' General Reminder'; }
			?>
			</small>
		</h1>
	</section>
	<style> .table-bordered>tbody>tr>td { border: 1px solid #999999; } </style>
	<section class="content" style="margin-top: -15px;">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="overlay" style="display: none" ><i class="fa fa-refresh fa-spin"></i></div>
					<form method="POST" action="<?=base_url('reminder/customer_reminder_sent') ?>" id="customer_reminder_form">
						<?php if(isset($id) && !empty($id)){ ?>
							<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
						<?php } ?>
						<input type="hidden" id="party_id" name="party_id" value="<?php echo $party_id; ?>" >
						<?php if($page_name == 'delivery_reminder' || $page_name == 'order_confirmation_reminder' || $page_name == 'order_cancel_reminder' || $page_name == 'dispatch_reminder' || $page_name == 'price_upgrade_reminder' || $page_name == 'order_payment_reminder' || $page_name == 'balance_payment_reminder'){ ?>
							<input type="hidden" id="sales_order_id" name="sales_order_id" value="<?php echo $sales_order_id; ?>" >
						<?php } ?>
						<?php if($page_name == 'finance_balance_payment'){ ?>
							<input type="hidden" id="invoice_id" name="invoice_id" value="<?php echo $invoice_id; ?>" >
						<?php } ?>
						
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<?php if($page_name == 'delivery_reminder' || $page_name == 'order_confirmation_reminder' || $page_name == 'order_cancel_reminder' || $page_name == 'dispatch_reminder' || $page_name == 'price_upgrade_reminder' || $page_name == 'order_payment_reminder' || $page_name == 'balance_payment_reminder'){ ?>
									<div class="sales_order_details">
										<?php if(!empty($sales_order_data)){ ?>
										<b>Sales Order Details :</b>
										<table class="table table-bordered">
											<tr>
												<td>Quotation No. : <?=$sales_order_data->quotation_no;?></td>
												<td>Sales Order No. : <?=$sales_order_data->sales_order_no;?></td>
												<td>Sales Order Date : <?=strtotime($sales_order_data->sales_order_date) != 0?date('d-m-Y',strtotime($sales_order_data->sales_order_date)):'';?></td>
											</tr>
											<tr>
												<td>Purchase Order No. : <?=$sales_order_data->cust_po_no;?></td>
												<td>Purchase Order Date : <?=strtotime($sales_order_data->po_date) != 0?date('d-m-Y',strtotime($sales_order_data->po_date)):'';?></td>
												<td>Purchase Order Ref. : <?=$sales_order_data->sales_order_pref;?></td>
											</tr>
											<tr>
												<td>Sales : <?=$sales_order_data->sales;?></td>
												<td>Sales Executive : <?=$sales_order_data->created_name;?></td>
												<td>Conversation Rate : <?=$sales_order_data->conversation_rate;?></td>
											</tr>
											<tr>
												<td>Party Name : <?=$sales_order_data->party_name;?></td>
												<td>Party Code : <?=$sales_order_data->party_code;?></td>
												<td>Address : <?=nl2br($sales_order_data->address);?></td>
											</tr>
											<tr>
												<td>City : <?=$sales_order_data->city?></td>
												<td>Pincode : <?=$sales_order_data->pincode?></td>
												<td>State - Country : <?=$sales_order_data->state?> - <?=$sales_order_data->country?></td>
											</tr>
											<tr>
												<td>Email : <?=$sales_order_data->party_email_id;?></td>
												<td>Tel No. : <?=$sales_order_data->fax_no;?></td>
												<td>Contact No. : <?=$sales_order_data->p_phone_no;?></td>
											</tr>
											<tr>
												<td>Contact Person : <?=$sales_order_data->contact_person_name?></td>
												<td>Mobile No. : <?=$sales_order_data->contact_person_phone?></td>
												<td></td>
											</tr>
										</table>
										<?php } ?>
									</div>
									<?php } ?>
									<?php if($page_name == 'finance_balance_payment'){ ?>
									<div class="invoice_details">
										<?php if(!empty($invoice_data)){ ?>
										<b>Invoice Details :</b>
										<table class="table table-bordered">
											<tr>
												<td>Invoice No. : <?=$invoice_data->invoice_no;?></td>
												<td>Invoice Date : <?=strtotime($invoice_data->invoice_date) != 0?date('d-m-Y',strtotime($invoice_data->invoice_date)):'';?></td>
												<td>Challan No. : <?=$invoice_data->challan_id;?></td>
											</tr>
											<tr>
												<td>Quotation No. : <?=$invoice_data->quotation_no;?></td>
												<td>Sales Order No. : <?=$invoice_data->sales_order_no;?></td>
												<td>Sales Order Date : <?=strtotime($invoice_data->sales_order_date) != 0?date('d-m-Y',strtotime($invoice_data->sales_order_date)):'';?></td>
											</tr>
											<tr>
												<td>Purchase Order No. : <?=$invoice_data->cust_po_no;?></td>
												<td>Purchase Order Date : <?=strtotime($invoice_data->po_date) != 0?date('d-m-Y',strtotime($invoice_data->po_date)):'';?></td>
												<td>Purchase Order Ref. : <?=$invoice_data->sales_order_pref;?></td>
											</tr>
											<tr>
												<td>Sales : <?=$invoice_data->sales;?></td>
												<td>Sales Executive : <?=$invoice_data->created_name;?></td>
												<td></td>
											</tr>
											<tr>
												<td>Party Name : <?=$invoice_data->party_name;?></td>
												<td>Party Code : <?=$invoice_data->party_code;?></td>
												<td>Address : <?=nl2br($invoice_data->address);?></td>
											</tr>
											<tr>
												<td>City : <?=$invoice_data->city?></td>
												<td>Pincode : <?=$invoice_data->pincode?></td>
												<td>State - Country : <?=$invoice_data->state?> - <?=$invoice_data->country?></td>
											</tr>
											<tr>
												<td>Email : <?=$invoice_data->party_email_id;?></td>
												<td>Tel No. : <?=$invoice_data->fax_no;?></td>
												<td>Contact No. : <?=$invoice_data->p_phone_no;?></td>
											</tr>
											<tr>
												<td>Contact Person : <?=$invoice_data->contact_person_name?></td>
												<td>Mobile No. : <?=$invoice_data->contact_person_phone?></td>
												<td></td>
											</tr>
										</table>
										<?php } ?>
									</div>
									<?php } ?>
								</div>
								<div class="col-md-8">
									<?php if(isset($id) && !empty($id)){ ?>
									<div class="form-group">
										<label for="customer_reminder_id" class="col-md-3 input-sm">Letter No.</label>
										<div class="col-md-9"><?php echo $letter_no; ?></div>
									</div>
									<?php } ?>
									<div class="clearfix"></div>
									<div class="form-group">
										<label for="customer_reminder_id" class="col-md-3 input-sm">Topic</label>
										<div class="col-md-9">
											<select id="customer_reminder_id" class="form-control input-sm" required disabled="disabled"onchange="customer_reminder_details(this.value)" ></select>
											<input type="hidden" name="customer_reminder_id" id="customer_reminder_hidden_id" >
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="form-group">
										<label for="subject" class="col-md-3 input-sm">Subject</label>
										<div class="col-md-9">
											<input type="text" class="form-control input-sm" id="subject" name="subject" value="<?php echo $subject; ?>" required >
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="form-group">
										<label for="letter_validity" class="col-md-3 input-sm">Letter Validity</label>
										<div class="col-md-4"><input type="text" class="form-control input-sm" id="letter_validity" name="letter_validity" value="<?php echo $letter_validity; ?>" ></div>
										<div class="col-md-5" style="padding-left: 0px;">Days</div>
									</div>
									<?php if($page_name == 'delivery_reminder' || $page_name == 'order_confirmation_reminder' || $page_name == 'order_cancel_reminder' || $page_name == 'dispatch_reminder' || $page_name == 'price_upgrade_reminder' || $page_name == 'order_payment_reminder' || $page_name == 'balance_payment_reminder'){ ?>
										<div class="clearfix"></div>
										<div class="form-group">
											<label for="subject" class="col-md-3 input-sm">Received Payment Details</label>
											<div class="col-md-9">
												<textarea id="received_payment_detail" name="received_payment_detail" class="form-control" ><?php echo $received_payment_detail; ?></textarea>
											</div>
										</div>
									<?php } ?>
									<div class="clearfix"></div>
									<div class="form-group">
										<label for="inputEmail3" class="col-md-3 input-sm">Variables</label>
										<div class="col-md-9">
											{{Party_Name}}, &nbsp;&nbsp;{{Address}}, &nbsp;&nbsp;{{City}}, &nbsp;&nbsp;{{Pin}}, &nbsp;&nbsp;{{State}}, &nbsp;&nbsp;{{Country}}, &nbsp;&nbsp;{{Email}}, &nbsp;&nbsp;{{Tel_No}}, &nbsp;&nbsp;{{Contact_No}}, &nbsp;&nbsp;{{Contact_Person}}, &nbsp;&nbsp;{{GST_No}}
										</div>
									</div>
								</div>
								<div class="clearfix"></div><br />
								<div class="col-md-12">
									<textarea id="content_data" name="content_data" rows="10" cols="80"><?php echo $content_data; ?></textarea>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-info pull-right save_print_btn">Save & Print</button>
							<button type="submit" class="btn btn-info pull-right save_btn" style="margin-right:5px;">Send</button>
							<input type="hidden" id="save_print_button" name="save_print_button" value="0">
						</div>
					</form>
				</div>
            </div>
		</div>
	</section>
</div>
<?php if(isset($id) && !empty($id)){ } else { ?>
<?php if($page_name == 'delivery_reminder' || $page_name == 'order_confirmation_reminder' || $page_name == 'order_cancel_reminder' || $page_name == 'dispatch_reminder' || $page_name == 'price_upgrade_reminder' || $page_name == 'order_payment_reminder' || $page_name == 'balance_payment_reminder'){ ?>
<div class="modal fade" id="sales-order-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select Sales Order
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">

                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="sales-order-datatable" class="table custom-table table-striped">
                        <thead>
                        <tr>
							<th>Quotation No.</th>
							<th>Sales Order No.</th>
							<th>Item Code</th>
							<th>Party Name </th>
							<th>City</th>
							<th>State</th>
							<th>Sales Order Date</th>
						</tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr>
							<th>Quotation No.</th>
							<th>Sales Order No.</th>
							<th>Item Code</th>
							<th>Party Name </th>
							<th>City</th>
							<th>State</th>
							<th>Sales Order Date</th>
						</tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<?php if($page_name == 'finance_balance_payment'){ ?>
<div class="modal fade" id="invoice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select Invoice
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">

                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="invoice-datatable" class="table custom-table table-striped">
                        <thead>
                        <tr>
							<th>Invoice No.</th>
							<th>Challan No.</th>
							<th>Sales Order No.</th>
							<th>Quotation No.</th>
							<th>Customer Name </th>
							<th>Invoice Date</th>
						</tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr>
							<th>Invoice No.</th>
							<th>Challan No.</th>
							<th>Sales Order No.</th>
							<th>Quotation No.</th>
							<th>Customer Name </th>
							<th>Invoice Date</th>
						</tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<?php } ?>
<script type="text/javascript" src="<?=base_url('resource/plugins/ckeditor/ckeditor.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(){

		initAjaxSelect2($("#customer_reminder_id"),"<?=base_url('app/customer_reminder_select2_source')?>"); 
		var page_name = '<?php echo $this->uri->segment(2); ?>';
		if(page_name == 'delivery_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.DELIVERY_REMINDER_ID)?>");
		}
		if(page_name == 'order_confirmation_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.ORDER_CONFIRMATION_REMINDER_ID)?>");
		}
		if(page_name == 'order_cancel_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.ORDER_CANCEL_REMINDER_ID)?>");
		}
		if(page_name == 'dispatch_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.DISPATCH_REMINDER_ID)?>");
		}
		if(page_name == 'price_upgrade_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.PRICE_UPGRADE_REMINDER_ID)?>");
		}
		if(page_name == 'order_payment_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.ORDER_PAYMENT_REMINDER_ID)?>");
		}
		if(page_name == 'balance_payment_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.BALANCE_PAYMENT_REMINDER_ID)?>");
		}
		if(page_name == 'finance_balance_payment'){
            setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.FINANCE_BALANCE_PAYMENT_ID)?>");
		}
		if(page_name == 'general_reminder'){
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.GENERAL_REMINDER_ID)?>");
		}
		<?php if(isset($id) && !empty($id)){ ?>
			setSelect2Value($("#customer_reminder_id"),"<?=base_url('app/set_customer_reminder_select2_val_by_id/'.$customer_reminder_id)?>");
		<?php } ?>
		$('#customer_reminder_hidden_id').val($('#customer_reminder_id').val());
		
		//~ initAjaxSelect2($("#party_id"),"<?=base_url('app/party_select2_source')?>");git diff 
		
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		CKEDITOR.replace('content_data');
		
		//~ Start Select Sales Order Modal
		$('#sales-order-modal').modal({backdrop: 'static', keyboard: false});
        $('#sales-order-modal').modal('show');

        table = $('#sales-order-datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('reminder/get_sales_order_datatable')?>",
                "type": "POST",
                "data":function(d){
                    d.is_approved = 1;
                }
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
        $( "#sales-order-modal" ).on('shown.bs.modal',function(){
            $('#sales-order-datatable').DataTable().search('').draw();
        });
        
        $(document).on('click','.sales_order_select_link',function(){
            $('#sales-order-modal').modal('hide');
            var sales_order_id = $(this).data('sales_order_id');
            var party_id = $(this).data('sales_to_party_id');
            var sales_order_data = $(this).data('sales_order_data');
            $('#sales_order_id').val(sales_order_id);
            $('#party_id').val(party_id);
            $('#sales_order_data').html(sales_order_data);
            $.ajax({
				type: "POST",
				url: '<?=base_url();?>reminder/ajax_load_sales_order_detail/'+sales_order_id,
				data: 'id=' +sales_order_id,
				success: function(data){
					var json = $.parseJSON(data);
					if (json.received_payment) {
						$('#received_payment_detail').html(json.currency + ' ' + json.received_payment + '/- as an advance on Date ' + json.received_payment_date + ' by ' + json.received_payment_type + ' ' + json.received_payment_cheque_no)	;
					}
					if (json.sales_order_id) {
						var sales_order_detail = '<b>Sales Order Details :</b>';
						sales_order_detail += '<table class="table table-bordered">';
						sales_order_detail += '<tr>';
						sales_order_detail += '<td>Quotation No. : ' + json.quotation_no + '</td>';
						sales_order_detail += '<td>Sales Order No. : ' + json.sales_order_no + '</td>';
						sales_order_detail += '<td>Sales Order Date : ' + json.sales_order_date + '</td>';
						sales_order_detail += '</tr>';
						sales_order_detail += '<tr>';
						sales_order_detail += '<td>Purchase Order No. : ' + json.cust_po_no + '</td>';
						sales_order_detail += '<td>Purchase Order Date : ' + json.po_date + '</td>';
						sales_order_detail += '<td>Purchase Order Ref. : ' + json.sales_order_pref + '</td>';
						sales_order_detail += '</tr>';
						sales_order_detail += '<tr>';
						sales_order_detail += '<td>Sales : ' + json.sales + '</td>';
						sales_order_detail += '<td>Sales Executive : ' + json.created_name + '</td>';
						sales_order_detail += '<td>Conversation Rate : ' + json.conversation_rate + '</td>';
						sales_order_detail += '</tr>';
						sales_order_detail += '<tr>';
						sales_order_detail += '<td>Party Name : ' + json.party_name + '</td>';
						sales_order_detail += '<td>Party Code : ' + json.party_code + '</td>';
						sales_order_detail += '<td>Address : ' + json.address + '</td>';
						sales_order_detail += '</tr>';
						sales_order_detail += '<tr>';
						sales_order_detail += '<td>City : ' + json.city + '</td>';
						sales_order_detail += '<td>Pincode : ' + json.pincode + '</td>';
						sales_order_detail += '<td>State - Country : ' + json.state + ' - ' + json.country + '</td>';
						sales_order_detail += '</tr>';
						sales_order_detail += '<tr>';
						sales_order_detail += '<td>Email : ' + json.party_email_id + '</td>';
						sales_order_detail += '<td>Tel No. : ' + json.fax_no + '</td>';
						sales_order_detail += '<td>Contact No. : ' + json.p_phone_no + '</td>';
						sales_order_detail += '</tr>';
						sales_order_detail += '<tr>';
						sales_order_detail += '<td>Contact Person : ' + json.contact_person_name + '</td>';
						sales_order_detail += '<td>Mobile No. : ' + json.contact_person_phone + '</td>';
						sales_order_detail += '<td></td>';
						sales_order_detail += '</tr>';
						sales_order_detail += '</table>';
						$('.sales_order_details').html(sales_order_detail);
					}
				},
			});
        });
		//~ End Select Sales Order Modal
		
		//~ Start Select Invoice Modal
		$('#invoice-modal').modal({backdrop: 'static', keyboard: false});
        $('#invoice-modal').modal('show');

        table = $('#invoice-datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "aaSorting": [[0, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('reminder/get_invoice_datatable')?>",
                "type": "POST",
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
        $( "#invoice-modal" ).on('shown.bs.modal',function(){
            $('#invoice-datatable').DataTable().search('').draw();
        });
        $(document).on('click','.invoice_select_link',function(){
            $('#invoice-modal').modal('hide');
            var invoice_id = $(this).data('invoice_id');
            var party_id = $(this).data('party_id');
            $('#invoice_id').val(invoice_id);
            $('#party_id').val(party_id);
            $.ajax({
				type: "POST",
				url: '<?=base_url();?>reminder/ajax_load_invoice_detail/'+invoice_id,
				data: id='invoice_id',
				success: function(data){
					var json = $.parseJSON(data);
					if (json.invoice_id) {
						var invoice_detail = '<b>Invoice Details :</b>';
						invoice_detail += '<table class="table table-bordered">';
						invoice_detail += '<tr>';
						invoice_detail += '<td>Invoice No. : ' + json.invoice_no + '</td>';
						invoice_detail += '<td>Invoice Date : ' + json.invoice_date + '</td>';
						invoice_detail += '<td>Challan No. : ' + json.challan_id + '</td>';
						invoice_detail += '</tr>';
						invoice_detail += '<td>Quotation No. : ' + json.quotation_no + '</td>';
						invoice_detail += '<td>Sales Order No. : ' + json.sales_order_no + '</td>';
						invoice_detail += '<td>Sales Order Date : ' + json.sales_order_date + '</td>';
						invoice_detail += '</tr>';
						invoice_detail += '<tr>';
						invoice_detail += '<td>Purchase Order No. : ' + json.cust_po_no + '</td>';
						invoice_detail += '<td>Purchase Order Date : ' + json.po_date + '</td>';
						invoice_detail += '<td>Purchase Order Ref. : ' + json.sales_order_pref + '</td>';
						invoice_detail += '</tr>';
						invoice_detail += '<tr>';
						invoice_detail += '<td>Sales : ' + json.sales + '</td>';
						invoice_detail += '<td>Sales Executive : ' + json.created_name + '</td>';
						invoice_detail += '<td></td>';
						invoice_detail += '</tr>';
						invoice_detail += '<tr>';
						invoice_detail += '<td>Party Name : ' + json.party_name + '</td>';
						invoice_detail += '<td>Party Code : ' + json.party_code + '</td>';
						invoice_detail += '<td>Address : ' + json.address + '</td>';
						invoice_detail += '</tr>';
						invoice_detail += '<tr>';
						invoice_detail += '<td>City : ' + json.city + '</td>';
						invoice_detail += '<td>Pincode : ' + json.pincode + '</td>';
						invoice_detail += '<td>State - Country : ' + json.state + ' - ' + json.country + '</td>';
						invoice_detail += '</tr>';
						invoice_detail += '<tr>';
						invoice_detail += '<td>Email : ' + json.party_email_id + '</td>';
						invoice_detail += '<td>Tel No. : ' + json.fax_no + '</td>';
						invoice_detail += '<td>Contact No. : ' + json.p_phone_no + '</td>';
						invoice_detail += '</tr>';
						invoice_detail += '<tr>';
						invoice_detail += '<td>Contact Person : ' + json.contact_person_name + '</td>';
						invoice_detail += '<td>Mobile No. : ' + json.contact_person_phone + '</td>';
						invoice_detail += '<td></td>';
						invoice_detail += '</tr>';
						invoice_detail += '</table>';
						$('.invoice_details').html(invoice_detail);
					}
				},
			});
        });
		//~ End Select Invoice Modal
		
		
		$(document).on('click', '.save_btn', function () {
			$('#save_print_button').val('0');
		});
		$(document).on('click', '.save_print_btn', function () {
			$('#save_print_button').val('1');
		});
		
		var page_name = '<?php echo $this->uri->segment(2); ?>';
		var redirect_url = '';
		if(page_name == 'delivery_reminder' || page_name == 'order_confirmation_reminder' || page_name == 'order_cancel_reminder' || page_name == 'dispatch_reminder' || page_name == 'price_upgrade_reminder' || page_name == 'order_payment_reminder' || page_name == 'balance_payment_reminder'){
			redirect_url = '<?php echo base_url('sales_order/delivery_reminder_list/'); ?>';
		}
		if(page_name == 'finance_balance_payment'){
            redirect_url = '<?php echo base_url('finance/finance_reminder_list/'); ?>';
		}
		if(page_name == 'general_reminder'){
			redirect_url = '<?php echo base_url('service/general_reminder_list/'); ?>';
		}
		
		$(document).on("submit", "#customer_reminder_form", function(){
			$('.overlay').show();
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success: function(data){
					if (data.empty) {
						show_notify('Party Email Id is not available!', false);
						$('.overlay').hide();
						return false;
					}
					if (data.last_insert_id){
						window.open('<?php echo base_url('reminder/customer_reminder_print/'); ?>/'+data.last_insert_id, '_blank');
						window.location.href = redirect_url;
						return false;
					}
					if (data.send_mail){
						window.location.href = data.send_mail;
						return false;
					}
					$('.overlay').hide();
					//~ location.reload();
					window.location.href = redirect_url;
					return false;
				}
			});
			return false;
		});
		
	});

	<?php if(isset($id) && !empty($id)){ } else { ?>
	function customer_reminder_details(id){
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>reminder/ajax_load_customer_reminder/'+id,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);
                if (json['subject']) {
                    $('#subject').val(json['subject']);
                }
                if (json['content_data']) {
					CKEDITOR.instances['content_data'].setData(json['content_data'])
                }
            },
        });
    }
    <?php } ?>
</script>
