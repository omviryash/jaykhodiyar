<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="custom content-header">
		<h1>
			<small class="text-primary text-bold">Add Reminder</small>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Reminder</h3>
						<input type="submit" class="btn btn-info btn-xs btn-submit pull-right" value="Save" />
					</div>
					<div class="box-body">
						<div class="row">
							<form action="" id="main-frm" class="main-frm" data-parsley-validate novalidate >
								<div class="col-md-12">
									<div class="form-group">
										<label for="inputEmail3" class="col-md-1 control-label input-sm text-right">Remind<span class="required-sign">*</span></label>
										<div class="col-sm-8">
											<textarea class="form-control" id="remind" name="remind" placeholder="" required ></textarea>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="clearfix">&nbsp;</div>
									<div class="col-md-12 row" id="addmore_contact">
										<div class="corporat_contact row col-md-12">
											<div class="form-group col-md-2">
												<label for="inputEmail3" class="col-sm-1 control-label input-sm">Date</label>
												<div class="">
													<input type="text" class="form-control input-sm pull-right input-datepicker datepicker" name="reminder_date[0]" required value="<?= date('d-m-Y'); ?>">
												</div>
											</div>
											<div class="form-group col-md-2">
												<label for="inputEmail3" class="col-sm-1 control-label input-sm">Time</label>

												<div class="bootstrap-timepicker" style="position:initial;">
													<input type="text" class="form-control input-sm pull-right timepicker add_tp" name="reminder_time[0]" required >
												</div>

											</div>
											<div class="form-group  col-md-2">
												<label for="inputEmail3" class="col-sm- control-label input-sm">Assigned To</label>
												<div class="">
													<select name="assigned_to[0]" id="assigned_to" class="form-control input-sm assigned_to" required >
														<option value="">--Select--</option>
														<?php foreach($agent_list as $agent): ?>
														<option value="<?php echo $agent->staff_id; ?>" <?=$staff_id == $agent->staff_id?'selected="selected"':''?>><?php echo $agent->name; ?></option>
														<!-- <option value="<?php echo $agent->staff_id; ?>" ><?php echo $agent->name; ?></option> -->
														<?php endforeach; ?>
													</select>
												</div>
											</div>
											<div class="form-group  col-md-2">
												<label for="inputEmail3" class="col-sm- control-label input-sm">Status</label>
												<div class="">
													<select class="form-control input-sm select2" name="status[0]" id="status">
														<option value="1">Open</option>
														<option value="0">Close</option>
													</select>
												</div>
											</div>
											<div class="form-group  col-md-2">
												<label for="inputEmail3" class="col-sm- control-label input-sm">Priority</label>
												<div class="">
													<select class="form-control input-sm select2" name="priority[0]" id="priority">
														<option value="1">High</option>
														<option value="2">Medium</option>
														<option value="3">Low</option>
													</select>
												</div>
											</div>
											<div class="form-group col-md-3">
												<div class="remove_contact" style="visibility: hidden; margin-top: 20px;"><span class="btn btn-primary" title="Remove Contact">x</span></div>
											</div>
										</div>
									</div>


								</div>
								<div class="col-md-12 row form-group"></div>
								<div class="col-md-12 row form-group" style="margin-top: 20px;">
									<div class="col-md-10"><span class="btn btn-primary pull-right addnew" title="Add Contact" style="margin-right: 57px;"> + </span></div>
								</div>                                
							</form>
						</div>
					</div>
					<div class="box-footer">
						<input type="submit" class="btn btn-info btn-xs btn-submit pull-right" value="Save" />
					</div>
				</div>
			</div>	
		</div>
	</section>
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click",'.btn-submit',function(){
			$("#main-frm").submit();
		});

		$('#main-frm').parsley();
		$(document).on('click', '.remove_contact', function () {
			$(this).closest('div.corporat_contact').remove();
		});
		$('select.assigned_to').select2();
		$(document).on('click', '.addnew', function () {
			//$(this).closest('.corporat_contact').find(".assigned_to").select2("destroy");
			$('.assigned_to').select2("destroy");
			var section_index = $('div.corporat_contact').length;
			var newDiv = $(".corporat_contact:first").clone()
			.find(':input')
			.each(function () {
				this.name = this.name.replace(/\[(\d+)\]/, function (str) {
					return '[' + section_index + ']';
				});
			})
			.end()
			.appendTo("#addmore_contact");
			newDiv.find('.remove_contact').css('visibility', 'visible');
			newDiv.find(".input-datepicker").datepicker({
				format: 'dd-mm-yyyy',
				autoclose: true
			});
			newDiv.find("input.add_tp").timepicker({
				format: 'hh:mm:ss',
				showInputs: false,
				showSeconds: true,
				showMeridian: false
			});
			$('select.assigned_to').select2();
			newDiv.find('select.assigned_to').select2('val', '');
		});



		$(document).on('submit', '#main-frm', function(){
			$("#ajax-loader").show(); 
			$.ajax({
				type: 'post',
				url: '<?= base_url();?>reminder/add',
				data: $('#main-frm').serialize(),
				success: function(data) {
					var data = JSON.parse(data);
					$msg = data.msg;
					$("#ajax-loader").hide(); 
					if(data.status == 1)
					{
						//$("#main-frm")[0].reset();
						$("#main-frm").trigger('reset');
						show_notify($msg,true);
						setTimeout(location.reload.bind(location), 3000);
					}
					else
					{
						show_notify($msg,false);
					}
				},
				error: function(e) {
					$("#ajax-loader").hide();
				}
			});          
			return false;
		});
	});
</script>
