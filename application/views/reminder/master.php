<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small class="text-primary text-bold">Add Reminder</small>
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<form method="POST"
							  <?php if(isset($id) && !empty($id)){ ?>
							  action="<?=base_url('master/update_c_reminder') ?>" 
							  <?php } else { ?>
							  action="<?=base_url('master/add_c_reminder') ?>" 
							  <?php } ?> id="c_reminder_form">
							<?php if(isset($id) && !empty($id)){ ?>
							<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
							<?php } ?>
						<div class="box-header with-border">
							<h3 class="box-title">Add Reminder</h3>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label for="inputEmail3" class="col-md-2 input-sm">Topic</label>
										<div class="col-md-10">
											<input type="text" class="form-control input-sm" id="item_code1" name="item_code1" value="">
										</div>
									</div>
								</div> 
								<div class="col-md-7">
									<div class="form-group">
										<label for="inputEmail3" class="col-md-2 input-sm">Subject</label>
										<div class="col-md-10">
											<input type="text" class="form-control input-sm" id="item_code1" name="item_code1" value="">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									 Variables : {{To_Party_Name}}, {{To_Address}}, {{To_City}}, {{To_Pin}}, {{To_State}}, {{To_Country}}, {{To_Tel_No}}, {{To_Contact_No}}, {{To_Email}} 
								</div><br /><br />
								<div class="col-md-12">
									<textarea id="editor1" name="editor1" rows="10" cols="80"> This is my Content. </textarea>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<input type="submit" class="btn btn-info btn-submit pull-right" value="Submit" />
						</div>
					</form>
				</div>
            </div>
        </div>
	</section>

</div>
<script type="text/javascript" src="<?=base_url('resource/plugins/ckeditor/ckeditor.js');?>"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
  })
</script>
