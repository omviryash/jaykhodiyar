<div class="content-wrapper">
    <?php if ($this->session->flashdata('success') == true) { ?>
        <div><div class="col-sm-12">
                <div class="alert alert-success alert-dismissible" style="margin-top: 15px;" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
                </div>
            </div></div><div class="clearfix"></div>
    <?php } ?>
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <h1>
            <small class="text-primary text-bold">
                <?php
                $page_name = $this->uri->segment(2);
                echo 'Reminder List';
                ?>
            </small>
        </h1>
    </section>

    <section class="content" style="margin-top: -15px;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <label class="col-md-2">Reminder Topic :</label>
                        <div class="col-md-4">
                            <select name="customer_reminder_id" class="form-control input-sm" id="customer_reminder_id"></select>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <table id="reminder_table" class="table custom-table reminder-table">
                                    <thead>
                                        <tr>
                                            <th width="50">Action</th>
                                            <th>Letter No.</th>
                                            <th>Topic</th>
                                            <th>Subject</th>
                                            <?php
                                            if ($page_name == 'finance_reminder_list') {
                                                echo '<th>Invoice No.</th>';
                                            } else {
                                                echo '<th>Sales Order No.</th>';
                                            }
                                            ?>
                                            <th>Party Name</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Created Date</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        initAjaxSelect2($("#customer_reminder_id"),"<?=base_url('app/customer_reminder_select2_source')?>");
        var page_name = '<?php echo $this->uri->segment(2); ?>';
        table = $('#reminder_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [],
            "ajax": {
                "url": "<?php echo base_url('reminder/customer_reminder_sent_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                    d.reminder = page_name,
                    d.customer_reminder_id = $("#customer_reminder_id").val();
                },
            },
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columnDefs": [
                {"targets": 0, "orderable": false}
            ]
        });
        
        $(document).on('change','#customer_reminder_id',function(){
            table.draw();
        });

        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=customer_reminder_sent',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

    });
    
   
</script>

