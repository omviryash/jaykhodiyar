<style>
    .direct-chat-messages{
        height: 410px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <span class="pull-left">Messages </span>
            <div class="pull-left" style="font-size: 14px; margin-left: 20px; line-height: 28px;">
                <div class="form-group">
                    <?php
                    $currunt_date = strtotime(date('Y-m-d'));
                    $prev_date = strtotime(' -' . OLD_VISITOR_CHAT_DAYS . ' days', $currunt_date);
                    $prev_date = date('d-m-Y', $prev_date);
                    ?>
                    <label for="datepicker2" class="col-sm-5 control-label">Old Visitor Chat : </label>
                    <div class="col-sm-7">
                        <input type="text" name="old_visitor_chat_date" id="datepicker2" class="form-control" value="<?php echo $prev_date; ?>">
                    </div>
                </div>
            </div>
            <a href="<?= BASE_URL ?>messages/visitor_on_site/" target="_blank"  class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Visitor On Site</a>
            <a href="<?= BASE_URL ?>messages/staff_chat_report/" target="_blank"  class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Staff Chat Report</a>
            <?php /* <a href="<?=BASE_URL?>messages/chat_window/" target="_blank"  class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Chat Window</a> */ ?>
            <div class="clearfix"></div>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-3 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->

                <!-- USERS LIST -->
                <div class="box box-default">
                    <div class="nav-tabs-custom" style="cursor: move;">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right ui-sortable-handle">
                            <li class="active pull-left"><a href="#agents-chat" data-toggle="tab" aria-expanded="true">Agents</a></li>
                            <?php if ($this->applib->have_access_role(VISITOR_TAB_ID, "view")) { ?>
                                <li class="pull-left"><a href="#visitor-chat" data-toggle="tab" aria-expanded="false">Visitors</a></li>
                            <?php } ?>
                        <!--  <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
                        <span class="label label-danger"><?//=isset($agents) && is_array($agents)?count($agents):0; ?> Agents</span>-->
                        </ul>
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active" id="agents-chat" style="position: relative;height: 480px;overflow-y:scroll;">

                                <div class="box-body no-padding">
                                    <ul class="contacts-list" id="agent-list">
                                        <?php
                                        if (isset($agents) && is_array($agents)) {
                                            foreach ($agents as $agent) {
                                                if (in_array($agent['staff_id'], $chat_roles)) {
                                                    ?>
                                                    <li>
                                                        <a href="javascript:void(0);" onClick="msg_popup('<?php echo $agent['staff_id']; ?>', '<?php echo $agent['name']; ?>');">
                                                            <?php if (isset($agent['image']) && !empty($agent['image'])) { ?>
                                                                <img class="contacts-list-img" src="<?php echo IMAGE_URL; ?>staff/<?php echo $agent['image']; ?>" alt="User Image">
                                                            <?php } else { ?>
                                                                <img class="contacts-list-img" src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image">
                                                            <?php } ?>


                                                            <div class="contacts-list-info">
                                                                <span class="contacts-list-name">
                                                                    <?php echo $agent['name']; ?>
                                                                <!--  <small class="contacts-list-date pull-right">2/28/2015</small>-->
                                                                </span>
                                                                <span class="contacts-list-msg"><?php echo $agent['email']; ?></span>
                                                            </div>
                                                            <!-- /.contacts-list-info -->
                                                        </a>
                                                    </li>
                                                    <!-- End Contact Item -->
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <!-- /.contatcts-list -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <?php if ($this->applib->have_access_role(VISITOR_TAB_ID, "view")) { ?>
                                <div class="chart tab-pane" id="visitor-chat" style="position: relative; height: 480px;overflow-y:scroll;">
                                    <div class="box-body no-padding" id="visitor_list_by_date_html">
                                        <ul class="contacts-list" id="visitor-list">
                                            <?php
                                            if (isset($visitors) && is_array($visitors)) {
                                                foreach ($visitors as $visitor) {
                                                    if ($visitor->name != "") {
                                                        $visitorname = $visitor->name;
                                                    } else {
                                                        $visitorname = "Visitor";
                                                    }
                                                    ?>
                                                    <li class="<?php echo $visitor->session_id; ?>">
                                                        <a href="javascript:void(0);" onClick="visitor_msg_popup('<?php echo $visitor->session_id; ?>', '<?php echo $visitorname; ?>');">
                                                            <img class="contacts-list-img" src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image"/>
                                                            <div class="contacts-list-info">
                                                                <span class="contacts-list-name"><?php echo $visitorname; ?></span>
                                                                <span class="contacts-list-msg"><?php echo $visitor->email; ?></span>
                                                            </div>
                                                            <!-- /.contacts-list-info -->
                                                        </a>
                                                    </li>
                                                    <!-- End Contact Item -->
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                        <!-- /.contatcts-list -->
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                </div>
                <!--/.box -->

                <!-- /.col -->
                <!-- /.nav-tabs-custom -->

            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-6 connectedSortable" id="chatwin" style="padding-left: 0px;padding-right: 0px;">


            </section>

            <section class="col-lg-3 connectedSortable" style="position: relative; height: 500px;overflow-y:scroll;">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title user_details" visitor-id="">Agent Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <ul class="nav nav-pills nav-stacked user-details">
                                <!-- <li><a href="#">United States of America
                                   <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                                 <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                                 </li>
                                 <li><a href="#">China
                                   <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>-->
                            </ul>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>
            </section>
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    $(document).ready(function () {

        <?php if ($from_session_id) { ?>
            $('.nav-tabs a[href="#visitor-chat').tab('show');
            $('ul#visitor-list .<?php echo $from_session_id ?> a').trigger('click');
        <?php } else if (isset($agent_id)) { ?>
            msg_popup('<?= $agent_id ?>', '<?= $agent_name ?>');
        <?php } else { ?>
            $('ul#agent-list li:first-child a').trigger('click');
        <?php } ?>

        $(document).on('change', '#datepicker2', function () {
            var old_visitor_chat_date = $('#datepicker2').val();
            if (old_visitor_chat_date != '' || old_visitor_chat_date != null) {
                $.ajax({
                    type: "POST",
                    async: false,
                    data: {'old_visitor_chat_date': old_visitor_chat_date},
                    url: "<?= base_url('messages/old_visitor_chat_by_date'); ?>",
                    success: function (data) {
                        var json = $.parseJSON(data);
                        $('#visitor_list_by_date_html').html();
                        $('#visitor_list_by_date_html').html(json.visitor_list_by_date_html);
                    }
                });
            }
        });
    });
</script>
