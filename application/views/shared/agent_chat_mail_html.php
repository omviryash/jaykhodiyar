<!DOCTYPE html>
<html>
    <title>Chat Log</title>
    <head>
        <style>
            *{
                font-family: unset !important;
            }
            .maindiv {
                padding: 15px;

            }
            .div1 {
                background-color: #e3ebf9;
                padding-top: 50px;
                padding-left: 250px;
                padding-right: 250px;
                padding-bottom: 50px;

            }
            .div2 { 
                background-color: #b8d2f9;
                padding: 20px;
            }
        </style>
    </head>
    <body>
        <div class="maindiv">
            <div class="div1">
                <div class="div2">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td><center><img src="cid:<?= $cid ?>" border="0" style="width: 50px; height: 60px;"></center></td>
                        </tr>
                        <tr>
                            <td style="padding: 20px 20px 0px 20px; font-size: 35px; font-family: unset !important;"><center>Chat log Of Agent</center></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div style="background-color: white; padding: 5px 20px 5px 20px;">
                    <h4><b>Chat :</b></h4>
                    <?php
                    foreach ($messages as $key => $value) {
                        ?>
                        <?php if ($user_1 == $value['from_id']) { ?>
                            <div class="msg">
                                <div style="width: 80%; float: left; min-height: 45px; background-color: #c7d3e8; border-radius: 5px; margin-bottom: 8px;">
                                    <p style="float: left; margin-left: 7px;"><b><?= $value['from_name']; ?> : </b><?= $value['message']; ?></p>
                                </div>
                                <div style="width: 20%; float: right; margin-bottom: 3px;">
                                    <p><center>
                                        <?php
                                        if ($key == 0) {
                                            echo date('d M Y ', strtotime($value['date_time']));
                                            echo "<br/>";
                                            echo date('H:i A', strtotime($value['date_time']));
                                        } else {
                                            echo date('H:i A', strtotime($value['date_time']));
                                        }
                                        ?>
                                    </center></p>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="msg">
                                <div style="width: 20%; float: left;">
                                    <p><center>
                                        <?php
                                        if ($key == 0) {
                                            echo date('d M Y ', strtotime($value['date_time']));
                                            echo "<br/>";
                                            echo date('H:i A', strtotime($value['date_time']));
                                        } else {
                                            echo date('H:i A', strtotime($value['date_time']));
                                        }
                                        ?>
                                    </center></p>
                                </div>
                                <div style="width: 80%; float: right; min-height: 45px; background-color: #1f7eba; border-radius: 5px; margin-bottom: 8px;">
                                    <p style="float: left; margin-left: 7px; color: white;"><b><?= $value['from_name']; ?> : </b><?= $value['message']; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div style="clear:both;"></div>
                        <?php
                    }
                    ?>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>
    </body>
</html>
