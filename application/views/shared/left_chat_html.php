<div class="direct-chat-msg">
    <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-left"><?=$name;?></span>
        <span class="direct-chat-timestamp pull-right"><?=isset($date_time)?date('d M Y H:i:s',strtotime($date_time)):date('d M Y H:i:s');?></span>
    </div>
    <?php
        if(isset($image)){
            if($image != ''){
                echo '<img class="direct-chat-img" src="'.base_url().'resource/uploads/images/staff/'.$image.'" alt="message user image">';
            }else{
                echo '<img class="direct-chat-img" src="'.base_url().'resource/uploads/images/staff/default.png" alt="message user image">';
            }
        }
        else
        {
			echo '<img class="direct-chat-img" src="'.base_url().'resource/uploads/images/staff/default.png" alt="message user image">';
		}
    ?>
    <div class="direct-chat-text">
        <?=$message;?>
    </div>
</div>
