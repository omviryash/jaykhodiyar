<ul class="dropdown-menu">
    <li class="header">You have <span class="inbox-unread-mails"><?=$unread_mails?></span> messages</li>
    <li>
        <!-- inner menu: contains the actual data -->
        <ul class="menu">
            <?php
            if(!empty($unread_mails_data)) {
                foreach($unread_mails_data as $unread_mail_row) {
                    ?>
                    <li><!-- start message -->
                        <a href="<?=base_url()?>mail-system3/read-mail/<?=$unread_mail_row['mail_id'];?>" data-mail_id="<?=$unread_mail_row['mail_id'];?>" class="btn-header-read-mail">
                            <h4>
                                <?=$unread_mail_row['username'];?>
                                <small><i class="fa fa-clock-o"></i> <?=$unread_mail_row['received_at'];?></small>
                            </h4>
                            <p><?=$unread_mail_row['subject'];?></p>
                        </a>
                    </li>
                    <!-- end message -->
                    <?php
                }
            }
            ?>
        </ul>
    </li>
    <li class="footer"><a href="<?=base_url()?>mail-system3/inbox/">See All Messages</a></li>
</ul>