<!DOCTYPE html>
<html>
    <title>Chat Log</title>
    <head>
        <style>
            *{
                font-family: unset !important;
            }
            .chat_from_site_link:hover {
                text-decoration: underline;
            }
            .unsubscribe:hover{
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="maindiv" style="padding: 15px;">
            <div class="div1" style="background-color: #e3ebf9; padding-top: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 10px; width: 500px; align-content: center; margin: 0px auto;">
                <div class="div2" style="background-color: #b8d2f9; padding: 10px;">
                    <center>
                        <table>
                            <tbody>
                                <tr>
                                    <td><center><img src="cid:<?= $cid ?>" border="0" style="width: 50px; height: 60px;"></center></td>
                            </tr>
                            <tr>
                                <td style="padding: 30px; font-size: 35px; font-family: unset !important;"><center>Chat log from site</center></td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px; font-size: 28px;"><a href="http://<?= $visitordata['http_host_name']; ?>" class="chat_from_site_link" style="text-decoration: none;"><center><?= $visitordata['http_host_name']; ?></center></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </center>
                </div>
                <div style="background-color: white; padding: 10px 10px 0px 10px;">
                    <p style="color: #404042; margin-top: -5px;">A chat just finished on the website <a href="http://<?= $visitordata['http_host_name']; ?>" class="chat_from_site_link" style="text-decoration: none;"><?= $visitordata['http_host_name']; ?></a> between agent Sales Support and visitor 
                        <span style="color: black;"><b>Client <?= $visitordata['name'] . " " . $visitordata['id']; ?></b></span> </p>
                    <?php
                    $date_inc = 1;
                    $chat_start_date = '';
                    foreach ($messages as $key => $value) { 
                        if ($date_inc == 1){
                            $chat_start_date = date('d M Y / h:i A ', strtotime($value['date_time'])); 
                            ?>
                            <h4 style="text-align: left;"><b>Chat : </b><?php echo $chat_start_date; ?></h4>
                        <?php } $date_inc++; ?>
                        <?php if ('admin' == $value['from_table']) { ?>
                            <div>
                                <div class="msg" style="width: 80%; float: left; min-height: 45px; background-color: #c7d3e8; border-radius: 5px; margin-bottom: 8px;">
                                    <p style="float: left; margin-left: 7px;"><b>
                                        <?php
                                            if ($value['from_table'] == 'admin') {
                                                echo $value['name'];
                                            }
                                            ?> : </b>
                                        <?= $value['message']; ?></p>
                                </div>
                                <div class="msg_date" style="width: 20%; float: right; margin-bottom: 3px; text-align: center;">
                                    <p><center>
                                        <?php
                                            echo "<td nowrap>".date('h:i A', strtotime($value['date_time']))."</td>";
                                        ?>
                                    </center></p>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div>
                                <div class="msg_date" style="width: 20%; float: left; text-align: center;">
                                    <p><center>
                                        <?php
                                            echo "<td nowrap>".date('h:i A', strtotime($value['date_time']))."</td>";
                                        ?>
                                    </center></p>
                                </div>
                                <div class="msg"style="width: 80%; float: right; min-height: 45px; background-color: #1f7eba; border-radius: 5px; margin-bottom: 8px;">
                                    <p style="float: left; margin-left: 7px; color: white;"><b>
                                        <?php 
                                            if (isset($visitordata['name']) && (!empty($visitordata['name']))) {
                                                echo $visitordata['name'];
                                            } else {
                                                echo "Visitor";
                                            }
                                            ?> : </b>
                                        <?= $value['message']; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div style="clear:both;"></div>
                        <?php
                    }
                    ?>

                    <div style="clear:both;"></div>
                    <div style="padding: 10px 20px 10px 20px; background-color: #e3ebf9;">
                        <span style="text-align: left;">
                            <h3 style="text-align: left;">Dialog</h3>
                            <p><b><span style="color: #404042;">Chat started on page: </span></b><a href="<?= $visitordata['from_address']; ?>" class="chat_from_site_link" style="text-decoration: none;"><?= $visitordata['from_address']; ?></a> </p>
                            <p><b><span style="color: #404042;">Session Started: </span></b><span style="color: #646466;"><?= date('d M Y h:i A', strtotime($visitordata['created_at'])); ?></span></p>
                            <p><b><span style="color: #404042;">Waiting for Answer: </span></b><span style="color: #646466;">
                                    <?php
                                    foreach ($messages as $key => $value) {
                                        if ($value['from_table'] == 'visitors') {
                                            $datetime1 = $value['date_time'];
                                            break;
                                        }
                                    }
                                    foreach ($messages as $key => $value) {
                                        if ($value['from_table'] == 'admin') {
                                            $datetime2 = $value['date_time'];
                                            break;
                                        }
                                    }
                                    $datetime1 = new DateTime($datetime1);
                                    $datetime2 = new DateTime($datetime2);
                                    $interval = $datetime1->diff($datetime2);
                                    $elapsed = $interval->format('%d Days, %H hours %i minutes %s seconds');
                                    echo $elapsed;
                                    ?>
                                </span></p>
                        </span>
                    </div><br/>
                    <div style="padding: 10px 20px 10px 20px; background-color: #e3ebf9;">
                        <span style="text-align: left;">
                            <h3 style="color: #404042;">Visit Source :</h3>
                            <p><b><span style="color: #404042;">IP address : </span></b><span style="color: #646466;"><?= $visitordata['ip']; ?></span></p>
                            <p style="color: #646466;">Timezone is GMT <?= $visitordata['timezone']; ?> <br /> <?= $visitordata['city'].', '.$visitordata['state'].','.$visitordata['country']; ?></p>
                        </span>
                    </div><br/>
                </div>
                <div>
                    <p style="margin-top: 5px;">
                        <?php if (isset($for_visitor)){ ?>
                        <a href="<?php echo base_url(); ?>auth/unsubscribe/<?php echo $visitordata['unsubscribe_code']; ?>" class="unsubscribe text-center" style="text-decoration: none; color: #1f7eba;">Unsubscribe</a>
                        <?php } ?>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>