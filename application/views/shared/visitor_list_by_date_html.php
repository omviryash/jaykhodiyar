<ul class="contacts-list" id="visitor-list">
    <?php
    if (isset($visitors) && is_array($visitors)) {
        foreach ($visitors as $visitor) {
            if ($visitor->name != "") {
                $visitorname = $visitor->name;
            } else {
                $visitorname = "Visitor";
            }
            ?>
            <li class="<?php echo $visitor->session_id; ?>">
                <a href="javascript:void(0);" onClick="visitor_msg_popup('<?php echo $visitor->session_id; ?>', '<?php echo $visitorname; ?>');">
                    <img class="contacts-list-img" src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image"/>
                    <div class="contacts-list-info">
                        <span class="contacts-list-name"><?php echo $visitorname; ?></span>
                        <span class="contacts-list-msg"><?php echo $visitor->email; ?></span>
                    </div>
                    <!-- /.contacts-list-info -->
                </a>
            </li>
            <!-- End Contact Item -->
    <?php 
        }
    }
    ?>
</ul>