<table class="table">
    <tbody>
        <?php if(!empty($emails_data)): ?>
            <?php
                foreach($emails_data as $mail_row):
                    $star_class = $mail_row->is_starred == 1 ? 'fa-star' : 'fa-star-o';
                    $attachment_status = $mail_row->is_attachment == 1 ? '<i class="fa fa-paperclip"></i>' : '&nbsp;';
                    if ($mail_row->from_name != $this->session->userdata('is_logged_in')['mailbox_email']) {
                        $username = $mail_row->from_name;
                    } else {
                        $username = $mail_row->to_name;
                    }
            ?>

                <tr>
                    <td>
                        <a href="javascript:void(0);" class="btn-star mailbox-star" data-mail_id="<?=$mail_row->mail_id;?>"><i class="fa <?=$star_class?> text-yellow"></i></a>
                    </td>
                    <td>
                        <div class="is_unread">
                            <a href="javascript:void(0);" data-mail_id="<?=$mail_row->mail_id;?>" class="btn-read-mail drafts"><?=str_replace(',','<br/>',$username)?></a>
                            <br/>
                            <?=limit_character($mail_row->subject,45).'</br>'.time_ago($mail_row->received_at);?>
                        </div>
                    </td>
                    <td>
                        <?=$attachment_status;?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
        <tr>
            <td class="text-center">
                No Data
            </td>
        </tr>
        <?php endif; ?>
    </tbody>
</table>