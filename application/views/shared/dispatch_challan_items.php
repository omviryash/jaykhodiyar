<table class="table custom-table">
    <thead>
        <tr>
            <th class="text-center"><input type="checkbox" id="chk_all"></th>
            <th>Sr.No.</th>
            <th>Sales Order No.</th>
            <th>Date</th>
            <th>Bill no</th>
            <th>Model & Raw Material</th>
            <th>Qty</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($challan_items)):
                $i = 1;
                foreach($challan_items as $item_row):
        ?>
                    <tr>
                        <th class="text-center"><input type="checkbox" class="chk_challan_item" data-challan_id="<?=$item_row->challan_id?>" data-challan_item_id="<?=$item_row->challan_item_id?>"></th>
                        <td><?=$i?></td>
                        <td><?=$item_row->sales_order_no?></td>
                        <td><?=date('d-m-Y',strtotime($item_row->challan_date))?></td>
                        <td><?=$item_row->bill_no;?></td>
                        <td><strong><?=$item_row->item_code?></strong> - <?=$item_row->item_name?></td>
                        <td><?=$item_row->quantity?></td>
                    </tr>
        <?php
                    $i++;
                endforeach;
            endif;
        ?>
    </tbody>
</table>