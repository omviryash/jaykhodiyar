<div class="">
    <table role="presentation" class="table table-striped"><tbody class="files">
        <?php
        if(isset($item_files) && count($item_files) > 0) {
            foreach ($item_files as $key => $item_file_row) {
                $key = $key + 1;
                ?>
                <tr class="template-download fade in">
                    <td>
                        <span class="preview">
                            <a href="<?= base_url() . $item_file_row['file_url']; ?>"
                               title="File <?=$key?>"
                               download="File <?=$key?>" data-gallery="">
                                <img src="<?= base_url() . $item_file_row['file_url']; ?>">
                            </a>
                        </span>
                    </td>
                    <td>
                        <p class="name">
                            <a href="<?= base_url() . $item_file_row['file_url']; ?>"
                               title="File <?=$key?>"
                               download="File <?=$key?>" data-gallery="">
                                File <?=$key?>
                            </a>
                        </p>
                    </td>
                    <td>
                        <span class="size">30.55 KB</span>
                    </td>
                    <td>
                        <button class="btn btn-danger delete" data-type="DELETE" data-url="<?= base_url() . $item_file_row['file_url']; ?>">
                            <i class="glyphicon glyphicon-trash"></i>
                            <span>Delete</span>
                        </button>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
    <ul class="mailbox-attachments clearfix">
        <?php
        if(isset($item_files) && count($item_files) > 0){
            foreach($item_files as $key=>$item_file_row) {
                $key = $key +1;
                ?>
                <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-o"></i></span>
                    <div class="mailbox-attachment-info">
                        <a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> File <?=$key; ?></a>
                        <span class="mailbox-attachment-size text-right">
                            <a href="javascript:void(0);" class="btn btn-danger btn-xs btn-delete-file" data-file_id="<?= $item_file_row['id'];?>" data-file_url="<?= $item_file_row['file_url'];?>"><i class="fa fa-trash-o"></i></a>
                            <a href="<?= base_url() . $item_file_row['file_url']; ?>" class="btn btn-primary btn-xs" download="<?= $item_file_row['file_url']; ?>"><i class="fa fa-cloud-download"></i></a>
                        </span>
                    </div>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</div>
<div class="col-md-12 section_item_file_detail">
    <div class="col-md-6">
        <h4>File : 1</h4>
    </div>
    <div class="col-md-6 text-right">

    </div>
    <div class="clearfix"></div>
    <input type="file" name="item_files[]" class="form-control file-input">
    <br/>
</div>
<div class="col-md-12 text-right file-section-add-btn">
    <button type="button" class="btn btn-primary add_file_detail">Add more</button>
</div>
<div class="col-md-12 section_item_file_detail hidden">
    <div class="col-md-6">
        <h4>File : <span class="cnt-file-detail">2</span></h4>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-danger remove_file_detail">Remove File <span class="cnt-file-detail">2</span></button>
    </div>
    <div class="clearfix"></div>
    <input type="file" name="item_files[]" class="form-control file-input">
    <br/>
</div>
