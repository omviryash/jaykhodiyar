<table class="table custom-table">
    <thead>
        <tr>
            <!--<th class="text-center"><input type="checkbox" id="chk_all"></th>-->
            <th class="text-center"></th>
            <th>Sr.No.</th>
            <th>Challan No.</th>
            <th>Installation No.</th>
            <th>Installation Date</th>
            <th>Model & Raw Material</th>
            <th>Serial No.</th>
            <th>Qty</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($installation_with_items)):
                $i = 1;
                foreach($installation_with_items as $item_row):
        ?>
                    <tr>
                        <th class="text-center"><input type="checkbox" class="chk_challan_item" data-installation_id="<?=$item_row->installation_id?>" data-testing_report_id="<?=$item_row->testing_report_id?>" data-challan_id="<?=$item_row->challan_id?>"></th>
                        <td><?=$i?> <b><u><a href="<?= base_url(); ?>/installation/add/<?=$item_row->installation_id?>?view" target="_blank">View</a></u></b></td>
                        <td><?=$item_row->challan_no?></td>
                        <td><?=$item_row->installation_no_year?></td>
                        <td><?=date('d-m-Y',strtotime($item_row->installation_date))?></td>
                        <td><strong><?=$item_row->item_code?></strong> - <?=$item_row->item_name?></td>
                        <td><?=$item_row->item_serial_no?></td>
                        <td><?=$item_row->quantity?></td>
                    </tr>
        <?php
                    $i++;
                endforeach;
            endif;
        ?>
    </tbody>
</table>