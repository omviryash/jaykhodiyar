<?php foreach ($combine_array as $k => $v) { ?>
	<?php if($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"view")) { ?>
		<?php if(isset($v['reminder_id']) && !empty($v['reminder_id'])) { ?>
			<tr id="reminder_<?php echo $v['reminder_id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"edit")) { ?>
						<a href="<?= base_url(); ?>reminder/edit/<?= $v['reminder_id']; ?>"><i class="fa fa-edit"></i></a>
						<?php } ?>
						<?php if($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID,"delete")) { ?>
						<a href="javascript:void(0);" onclick="delete_reminder(<?php echo $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a>
						<?php } ?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['remind']; ?>
				</td>
				<td><span class="label label-primary">Reminder</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y h:i A", strtotime($v['reminder_date']));?>
				</td>
				<td>
					<?php if((isset($v['reminder_id']) && !empty($v['reminder_id'])) && ($v['user_id'] == $current_user_id)) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['name'];
					} ?>
				</td>
				<td>
					<?php if((isset($v['reminder_id']) && !empty($v['reminder_id'])) && ($v['assigned_to'] == $current_user_id)) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['assigned_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>

	<?php if($this->applib->have_access_role(ENQUIRY_MODULE_ID, "view")) { ?>
		<?php
		if(isset($v['lead_no']) && !empty($v['lead_no'])) {
			?>
			<tr id="lead_<?php echo $v['id']; ?>">
				<td>
					<div class="tools">
						<?php /*<a href="<?= base_url(); ?>reminder/edit/<?= '';//$v['reminder_id']; ?>"><i class="fa fa-edit"></i></a>
                        <a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';//$v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['lead_title']; ?>
				</td>
				<td><span class="label label-warning">Lead</span></td>
				<td>

				</td>
				<td>
					<?= $v['assigned_to_name'] ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>

	<?php if($this->applib->have_access_role(QUOTATION_MODULE_ID, "view")) { ?>
		<?php	if(isset($v['quotation_no']) && !empty($v['quotation_no'])) { ?>
			<tr id="quotation_<?php echo $v['id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(QUOTATION_MODULE_ID, "edit")) { ?>
						<a href="<?= base_url(); ?>quotation/add/<?= $v['id']; ?>"><i class="fa fa-edit"></i></a><?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';// $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
						<?php } ?>

					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['party_name']; ?>
				</td>
				<td><span class="label label-success">Quotation</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['created_at']));?>
				</td>

				<td>
					<?= $v['staff_name'] ?>
				</td>
			</tr>
		<?php } ?>
		<?php	if(isset($v['followup_date']) && !empty($v['followup_date'])) { ?>
			<tr id="quotation_<?php echo $v['id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(QUOTATION_MODULE_ID, "edit")) { ?>
						<a href="<?= base_url(); ?>quotation/add/<?= $v['quotation_id']; ?>/reminder"><i class="fa fa-edit"></i></a><?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';// $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
						<?php } ?>

					</div>
				</td>
                <td class="dont_break_out"><strong><span style="color: #3c8dbc;"><?= $v['party_name']; ?></span> - </strong> <?= $v['reminder_message']; ?></td>
				<td><span class="label label-success">Quot. Reminder</span></td>
				<td width="100px">
                    <?php if(strtotime($v['reminder_date']) > 0){ echo $newDate = date("d-m-Y h:i A", strtotime($v['reminder_date'])); } ?>
				</td>
				<td><?= $v['staff_name'] ?></td>
				<td><?= $v['assigned_to'] ?></td>
			</tr>
		<?php } ?>
	<?php } ?>

	<?php if($this->applib->have_access_role(SALES_ORDER_MODULE_ID,"view")) { ?>
        <?php if(isset($v['sales_order_no']) && !empty($v['sales_order_no']) && isset($v['approve']) && $v['approve'] == 1 ) { ?>
			<tr id="so_<?php echo $v['id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(SALES_ORDER_MODULE_ID,"edit")) { ?>
						<a href="<?= base_url(); ?>sales_order/add/<?= $v['id']; ?>"><i class="fa fa-edit"></i></a> <?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';// $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
						<?php } ?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['sales_order_party_name']; ?>
				</td>
				<td><span class="label label-danger">Pending Dispatch</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['committed_date']));?>
				</td>
				<td>
					<?php if($v['created_by'] == $current_user_id) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['created_by_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
        <!--Order To approve-->
<?php if(isset($v['sales_order_no']) && !empty($v['sales_order_no']) && isset($v['approve']) && $v['approve'] == 0) { ?>
        <tr id="so_<?php echo $v['id']; ?>">
            <td>
                <div class="tools">
                    <?php if($this->applib->have_access_role(SALES_ORDER_MODULE_ID,"edit")) { ?>
                    <a href="<?= base_url(); ?>sales_order/add/<?= $v['id']; ?>"><i class="fa fa-edit"></i></a> <?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';// $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
                    <?php } ?>
                </div>
            </td>
            <td class="dont_break_out">
                <?= $v['sales_order_party_name']; ?>
            </td>
			<td><span class="label label-danger">Approve Order</span></td>
            <td width="100px">
                <?=$newDate = date("d-m-Y", strtotime($v['committed_date']));?>
            </td>
            <td>
                <?php if($v['created_by'] == $current_user_id) { ?>
                Me
                <?php } else { ?>
                <?php echo $v['created_by_name'];
                             } ?>
            </td>
        </tr>
        <?php } ?>
        <!--Order To approve-->
	<?php } ?>
        
	<?php if($this->applib->have_access_role(CHALLAN_MODULE_ID,"view")) { ?>
        <?php if(isset($v['challan_id']) && !empty($v['challan_id'])) { ?>
			<tr id="pending_invoice_<?php echo $v['challan_id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(CHALLAN_MODULE_ID,"edit")) { ?>
						<a href="<?= base_url(); ?>challan/add/<?= $v['challan_id']; ?>"><i class="fa fa-edit"></i></a> <?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';// $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
						<?php } ?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['sales_order_party_name']; ?>
				</td>
				<td><span class="label label-danger">Pending Invoice</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['challan_date']));?>
				</td>
				<td>
					<?php if($v['created_by'] == $current_user_id) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['created_by_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
        <?php if(isset($v['t_challan_id']) && !empty($v['t_challan_id'])) { ?>
			<tr id="pending_testing_<?php echo $v['t_challan_id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(CHALLAN_MODULE_ID,"edit")) { ?>
						<a href="<?= base_url(); ?>challan/add/<?= $v['t_challan_id']; ?>"><i class="fa fa-edit"></i></a> <?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';// $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
						<?php } ?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['sales_order_party_name']; ?>
				</td>
				<td><span class="label bg-blue-gradient">Pending Testing</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['challan_date']));?>
				</td>
				<td>
					<?php if($v['created_by'] == $current_user_id) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['created_by_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
                        
	<?php if($this->applib->have_access_role(TESTING_REPORT_MODULE_ID,"view")) { ?>
        <?php if(isset($v['testing_report_id']) && !empty($v['testing_report_id'])) { ?>
			<tr id="pending_testing_<?php echo $v['testing_report_id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(TESTING_REPORT_MODULE_ID,"edit")) { ?>
						<a href="<?= base_url(); ?>testing_report/add/<?= $v['testing_report_id']; ?>"><i class="fa fa-edit"></i></a> <?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';// $v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
						<?php } ?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['sales_order_party_name']; ?>
				</td>
				<td><span class="label bg-yellow-active">Pending Installation</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['review_date']));?>
				</td>
				<td>
					<?php if($v['created_by'] == $current_user_id) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['created_by_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
	
	<?php if($this->applib->have_access_role(COMPLAIN_MODULE_ID, "view")) { ?>
		<?php if(isset($v['complain_id']) && !empty($v['complain_id']) && !isset($v['resolve_complain_id'])) { ?>
			<tr id="service_<?php echo $v['complain_id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(COMPLAIN_MODULE_ID, "edit")) { ?>
						<a href="<?=base_url("complain/add/" . $v['complain_id'])?>" data-complain_id="<?= $v['complain_id']; ?>"><i class="fa fa-edit"></i></a>
						<?php } ?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['party_name']; ?>
				</td>
				<td><span class="label label-danger">Unresolved Complain</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['complain_date']));?>
				</td>
				<td>
					<?php if($v['created_by'] == $current_user_id) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['created_by_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
            
    <?php if($this->applib->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "view")) { ?>
		<?php if(isset($v['resolve_complain_id']) && !empty($v['resolve_complain_id'])) { ?>
			<tr id="service_<?php echo $v['resolve_complain_id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "edit")) { ?>
						<a href="<?=base_url("complain/resolve_complain/" . $v['resolve_complain_id'])?>" data-resolve_complain_id="<?= $v['resolve_complain_id']; ?>"><i class="fa fa-edit"></i></a>
						<?php } ?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['party_name']; ?>
				</td>
				<td><span class="label label-danger">Resolved Complain</span></td>
				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['resolve_complain_date']));?>
				</td>
				<td>
					<?php if($v['created_by'] == $current_user_id) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['created_by_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>

	<?php if($this->applib->have_access_role(ENQUIRY_MODULE_ID, "view")) { ?>
		<?php if(isset($v['inquiry_id']) && !empty($v['inquiry_id'])) { ?>
			<tr id="<?php echo $v['inquiry_id']; ?>">
				<td>
					<div class="tools">
						<?php if($this->applib->have_access_role(ENQUIRY_MODULE_ID, "edit")) { ?>
						<a href="<?= base_url(); ?>enquiry/add/<?= $v['inquiry_id']; ?>"><i class="fa fa-edit"></i></a>
						<?php } ?>
						<?php /*<a href="javascript:void(0);" onclick="delete_reminder(<?php echo '';//$v['reminder_id']; ?>);"><i class="fa fa-trash-o"></i></a> */?>
					</div>
				</td>
				<td class="dont_break_out">
					<?= $v['inquiry_party_name']; ?>
				</td>

				<td>
					<?php if((isset($v['lead_or_inquiry']) && !empty($v['lead_or_inquiry'])) && ($v['lead_or_inquiry'] == 'lead')) { ?>
					 <span class="label bg-purple">Lead</span>
					<?php } else { ?>
					<span class="label bg-purple">Pending Enquiry</span>
					<?php } ?>
				</td>

				<td width="100px">
					<?=$newDate = date("d-m-Y", strtotime($v['due_date']));?>
				</td>
				<td>
					<?php if((isset($v['inquiry_no']) && !empty($v['inquiry_no'])) && ($v['created_by'] == $current_user_id)) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['created_by_name'];
					} ?>
				</td>
				<td>
					<?php if((isset($v['inquiry_no']) && !empty($v['inquiry_no'])) && ($v['assigned_to_id'] == $current_user_id)) { ?>
						Me
					<?php } else { ?>
						<?php echo $v['assigned_to_name'];
					} ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
<?php } ?>
