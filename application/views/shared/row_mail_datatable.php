<input type='hidden' name='folder' value='<?=$mail['folder']?>' class='folder'>
<div class="mailbox-name">
    <a class="btn-read-mail <?=$mail['is_unread']?'btn_view_unread_mail':'';?>" href="javascript:void(0);" data-mail_id="<?=$mail['mail_id'];?>">
        <?=$mail['is_unread'] == 1?"<b>".$mail['username']."</b>":$mail['username'];?>
    </a>
</div>
<div class="mailbox-attachment">
    <?=$mail['attachment_status'] == 1?'<i class="fa fa-paperclip"></i>':'&nbsp;';?>
</div>
<div class="mailbox-subject">
    <?=$mail['is_unread'] == 1?"<b>".$this->crud->limit_character($mail['subject'],25)."</b>":$this->crud->limit_character($mail['subject'],25);?>
</div>
<div class="mailbox-date">
    <?=$mail['received_at'];?>
</div>