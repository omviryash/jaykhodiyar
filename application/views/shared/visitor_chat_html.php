<div class="chat-popup" id="'+id+'">
    <div class="box box-warning direct-chat direct-chat-warning">
        <div class="box-header with-border">
            <h3 class="box-title">'+name+'</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="direct-chat-messages"></div>
        </div>
        <div class="box-footer">
            <div class="input-group">
                <input type="text" id="txt_'+id+'" userid="'+id+'" fromtable="admin" totable="admin" onfocus="set_read_msgs('+id+')" onkeyup="checkenter(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-warning btn-flat sendmsg_btn" id="btn_'+id+'" onclick="fileUpload('+id+');" >
                        <i class="fa fa-upload"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>