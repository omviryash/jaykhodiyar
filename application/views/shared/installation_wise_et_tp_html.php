<?php
    if(!empty($installation_with_items)):
        $i = 1;
        foreach($installation_with_items as $item_row):
?>
            <div class="hide_div_<?=$item_row->installation_id;?> hidden" id="hide_div_<?=$item_row->installation_id;?>">
                <fieldset class="scheduler-border" id="installation_item_id_<?=$item_row->installation_id;?>">
                    <legend class="scheduler-border text-primary text-bold"> Installation Item : <?=$item_row->installation_no_year;?> </legend>
                    <h5>Equipment Testing Normal & complete production Report at Customers Premises</h5>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="col-sm-2 input-sm"><strong>Testing Capacity</strong></label>
                            <label for="" class="col-sm-2 input-sm"><strong>Capacity Hrs</strong></label>
                            <label for="" class="col-sm-2 input-sm"><strong>Test of Raw Material</strong></label>
                            <label for="" class="col-sm-2 input-sm"><strong>Moisture of Raw Material</strong></label>
                            <label for="" class="col-sm-2 input-sm"><strong>After Procces of Moisture</strong></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="item_serial_no" class="col-sm-2 input-sm">Testing Normal Load</label>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_normal_capacity_hrs][<?=$item_row->installation_id;?>]" id="c_testing_normal_capacity_hrs_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_normal_capacity_hrs" placeholder="Kgs" ></div>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_normal_test_raw_material][<?=$item_row->installation_id;?>]" id="c_testing_normal_test_raw_material_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_normal_test_raw_material" placeholder="Name of Raw Material" ></div>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_normal_moisture_raw_material][<?=$item_row->installation_id;?>]" id="c_testing_normal_moisture_raw_material_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_normal_moisture_raw_material" placeholder="Moisture %" ></div>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_normal_after_procces_moisture][<?=$item_row->installation_id;?>]" id="c_testing_normal_after_procces_moisture_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_normal_after_procces_moisture" placeholder="Moisture %" ></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="item_serial_no" class="col-sm-2 input-sm">Testing Full Load</label>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_full_capacity_hrs][<?=$item_row->installation_id;?>]" id="c_testing_full_capacity_hrs_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_full_capacity_hrs" placeholder="Kgs" ></div>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_full_test_raw_material][<?=$item_row->installation_id;?>]" id="c_testing_full_test_raw_material_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_full_test_raw_material" placeholder="Name of Raw Material" ></div>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_full_moisture_raw_material][<?=$item_row->installation_id;?>]" id="c_testing_full_moisture_raw_material_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_full_moisture_raw_material" placeholder="Moisture %" ></div>
                            <div class="col-sm-2"><input type="text" name="equipment_testing_data[c_testing_full_after_procces_moisture][<?=$item_row->installation_id;?>]" id="c_testing_full_after_procces_moisture_<?=$item_row->installation_id;?>" class="form-control input-sm c_testing_full_after_procces_moisture" placeholder="Moisture %" ></div>
                        </div>
                    </div>
                    <h5>Temperature Parameters At Customers Premises</h5>
                    <div class="col-md-4">
                        <div class="form-group"><label for="c_temp_oli" class="col-sm-6 input-sm">Oil Temperature</label><div class="col-sm-6"><input type="text" name="temperature_parameters_data[c_temp_oli][<?=$item_row->installation_id;?>]" id="c_temp_oli_<?=$item_row->installation_id;?>" class="form-control input-sm c_temp_oli" ></div></div>
                        <div class="form-group"><label for="c_temp_eqipment" class="col-sm-6 input-sm">Equipment Temperature</label><div class="col-sm-6"><input type="text" name="temperature_parameters_data[c_temp_eqipment][<?=$item_row->installation_id;?>]" id="c_temp_eqipment_<?=$item_row->installation_id;?>" class="form-control input-sm c_temp_eqipment" ></div></div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><label for="c_temp_head" class="col-sm-6 input-sm">Head Temperature</label><div class="col-sm-6"><input type="text" name="temperature_parameters_data[c_temp_head][<?=$item_row->installation_id;?>]" id="c_temp_head_<?=$item_row->installation_id;?>" class="form-control input-sm c_temp_head" ></div></div>
                        <div class="form-group"><label for="c_temp_kiln" class="col-sm-6 input-sm">Kiln Temperature</label><div class="col-sm-6"><input type="text" name="temperature_parameters_data[c_temp_kiln][<?=$item_row->installation_id;?>]" id="c_temp_kiln_<?=$item_row->installation_id;?>" class="form-control input-sm c_temp_kiln" ></div></div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><label for="c_temp_hot_air" class="col-sm-6 input-sm">Hot Air Temperature</label><div class="col-sm-6"><input type="text" name="temperature_parameters_data[c_temp_hot_air][<?=$item_row->installation_id;?>]" id="c_temp_hot_air_<?=$item_row->installation_id;?>" class="form-control input-sm c_temp_hot_air" ></div></div>
                        <div class="form-group"><label for="c_temp_weather" class="col-sm-6 input-sm">Weather</label><div class="col-sm-6"><input type="text" name="temperature_parameters_data[c_temp_weather][<?=$item_row->installation_id;?>]" id="c_temp_weather_<?=$item_row->installation_id;?>" class="form-control input-sm c_temp_weather" ></div></div>
                    </div>
                </fieldset>
            </div>
<?php
            $i++;
        endforeach;
    endif;
?>
