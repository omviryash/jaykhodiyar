<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small class="text-primary text-bold">Proforma Invoice List</small>
			<?php
			$proforma_invoice_view_role= $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"view");
			$proforma_invoice_add_role= $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"add");
			?>

			<?php if($proforma_invoice_add_role):
			
			if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export")) { ?>
			<a href="<?=base_url('proforma_invoices/add_export')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Export Proforma Invoice</a>
			<?php } if($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic")) { ?>
			<a href="<?=base_url('proforma_invoices/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Proforma Invoice</a>
			<?php } 
			 endif;?>

		</h1>
		<!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="col-md-12">
                        <label class="col-md-1">Status:</label>
                        <div class="col-md-2">
                            <select name="pi_status" id="pi_status" class="form-control input-sm select2">
                                <option value="all" <?=isset($_GET['status']) && $_GET['status'] == 'all' ? 'selected="selected"' : ''?>>All</option>
                                <option value="<?php echo PENDING_DISPATCH_ORDER_ID; ?>" <?=isset($_GET['status']) && $_GET['status'] == PENDING_DISPATCH_ORDER_ID ? 'selected="selected"' : ''?>>Pending Dispatch</option>
                                <option value="<?php echo DISPATCHED_ORDER_ID; ?>" <?=isset($_GET['status']) && $_GET['status'] == DISPATCHED_ORDER_ID ? 'selected="selected"' : ''?>>Dispatched</option>
                                <option value="<?php echo CANCELED_ORDER_ID; ?>" <?=isset($_GET['status']) && $_GET['status'] == CANCELED_ORDER_ID ? 'selected="selected"' : ''?>>Canceled</option>
                                <option value="<?php echo CHALLAN_CREATED_FROM_SALES_ORDER_ID; ?>" <?=isset($_GET['status']) && $_GET['status'] == CHALLAN_CREATED_FROM_SALES_ORDER_ID ? 'selected="selected"' : ''?>>Challan Created from Sales Order</option>
                            </select>
                        </div>
						<div class="col-md-4">
							<label class="pull-left">Party Type: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<select name="party_type" id="party_type" class="form-control input-sm select2" style="width: 60%;">                                
								<?php
                                $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
                                $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
                                $isManagement = $this->applib->have_access_current_user_rights(PROFORMA_MANAGEMENT_USER,"allow");
								if($isManagement == 1){
									if($cu_accessExport == 1 && $cu_accessDomestic == 1){
									?>
									<option value="all">All</option>
									<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
									<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
									<?php
									}elseif($cu_accessExport == 1 && $cu_accessDomestic != 1){
									?>
										<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
									<?php
									}else if($cu_accessDomestic == 1 && $cu_accessExport != 1){
									?>
										<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
									<?php
									}
								}else{
									if($cu_accessExport == 1 && $cu_accessDomestic == 1){
									?>
									<option value="all">All</option>
									<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
									<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
									<?php
									} else if($cu_accessExport == 1){
									?>
										<option value="export" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'export'?'selected="selected"':''?>>Export</option>
									<?php
									} else if($cu_accessDomestic == 1){
									?>
										<option value="domestic" <?=isset($_GET['party_type']) && $_GET['party_type'] == 'domestic'?'selected="selected"':''?>>Domestic</option>
									<?php
									} else {}                                        
								}
                                ?>
							</select>
						</div>
						<div class="col-md-4">
							<label class="pull-left">Is Approved: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<select name="is_approved" id="is_approved" class="form-control input-sm select2" style="width: 60%;">
								<option value="all">All</option>
								<option value="1" <?=isset($_GET['is_approved']) && $_GET['is_approved'] == 1?'selected="selected"':''?>>Approved</option>
								<option value="0" <?=isset($_GET['is_approved']) && $_GET['is_approved'] == 0?'selected="selected"':''?>>Not Approved</option>
							</select>
						</div>
                        <div class="clearfix"></div><br />
                        <?php 
                            if($this->input->get_post("status")){ 
                                    $current_status = $this->input->get_post("status"); 
                            } else {
                                    $current_status = 'pending';
                            }
                            if($this->input->get_post("user_id")){ 
                                    $current_staff = $this->input->get_post("user_id"); 
                            } else {
                                    $current_staff = $this->session->userdata('is_logged_in')['staff_id'];
                            }
                            ?>
                    <div class="col-md-3">
                        <label class="pull-left">From Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" name="from_date" id="datepicker1" class="form-control input-sm from_date" style="width: 60%;" value="<?php echo isset($from_date) && !empty($from_date) ? date('d-m-Y', strtotime($from_date)) : ''; ?>">
                    </div>
                    <div class="col-md-3">
                        <label class="pull-left">To Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" name="to_date" id="datepicker2" class="form-control input-sm to_date" style="width: 60%;" value="<?php echo isset($to_date) && !empty($to_date) ? date('d-m-Y', strtotime($to_date)) : ''; ?>">
                    </div>
                    <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
                        <div class="col-md-4">
                            <label class="pull-left">Sales Person: &nbsp;&nbsp;</label>
                            <select class="form-control select_user_id input-sm select2" name="user_id" id="user_id" style="width: 60%;">
                                <option value="all" <?php if(isset($staff_id) && $staff_id == '0'){ echo ' Selected '; } elseif ($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
                                    <?php
                                        if (!empty($users)) {
                                            foreach ($users as $client) {
                                                if (trim($client->name) != "") {
                                                    $selected = '';
                                                    if(isset($staff_id)){
                                                        if(!empty($staff_id) && $staff_id == $client->staff_id){
                                                            $selected = $staff_id == $client->staff_id ? 'selected' : '';
                                                        } 
                                                    } else {
                                                        $selected = $current_staff == $client->staff_id ? 'selected' : '';
                                                    }
                                                        echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
                                                }
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                    <?php } ?>
					</div>
                    <div class="clearfix"></div><br />
					<div class="col-md-12">
						<div class="table-responsive" style="overflow-x:hidden">
							<table id="order_list_table" class="table custom-table table-striped">
								<thead>
									<tr>
										<th>Action</th>
										<th>Proforma Invoice No.</th>
										<th>Quotation No.</th>
										<th>Status</th>
										<th>Is Approved</th>
										<th>Customer Name </th>
										<th>Sales Person</th>
										<th>Sales Order Date</th>
										<th>City</th>
										<th>State</th>
										<th>Country</th>
										<th>Party Current Person</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>

<script>
	var table;
	$(document).ready(function() {
        $(".select2").select2();
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [1,2,3,4,5,6,7,8,9,10,11],
			}
		};
        
		table = $('#order_list_table').DataTable({
            "serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'desc']],
			"ajax": {
				"url": "<?php echo site_url('proforma_invoices/proforma_invoices_datatable')?>",
				"type": "POST",
				"data":function(d){
					d.pi_status = $("#pi_status").val();
					d.is_approved = $("#is_approved").val();
					d.invoice_not_created = $('input[name="invoice_not_created"]').prop('checked');
					d.party_type = $("#party_type").val();
                    d.from_date = $('#datepicker1').val();
                    d.to_date = $('#datepicker2').val();
                    d.user_id = $('#user_id').val();
				}
			},
            <?php if($this->applib->have_access_current_user_rights(PROFORMA_INVOICE_MODULE_ID,"export_data")){ ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: 'Profoma Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: 'Profoma Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: 'Profoma Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: 'Profoma Invoices', action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: 'Profoma Invoices', orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
			"scrollY": 500,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});
		$(document).on('change',"#pi_status, #party_type, #is_approved, .select_user_id, .from_date, .to_date",function() {
			table.draw();
		});
		
		$(document).on("click", ".delete_button", function() {
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if (value) {
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=proforma_invoices',
					success: function(data) {
					}
				});
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=proforma_invoice_id&table_name=proforma_invoice_items',
					success: function(data) {
						tr.remove();
						show_notify('Deleted Successfully!', true);
					}
				});
			}
		});

	});
</script>
