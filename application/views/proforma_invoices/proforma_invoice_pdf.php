<?php
	$CURRENCY = "INR";
	setlocale(LC_MONETARY, 'en_IN');
	$column_cnt = 10;
?>
<html>
	<head>
		<title>Proforma Invoice</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:16px;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:11px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-padding{
				padding:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				width: 33.33%;
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #000000;
				font-size: 12px;
			}
			.gst_summary tr td{
				font-size: 11px;
				line-height: 20px;
			}
		</style>
	</head>
	<body>
	<?php
		$CURRENCY = "";
		if($proforma_invoice_data->currency != ''){
			$CURRENCY = strtoupper($proforma_invoice_data->currency);
		}
	?>
        <?php
            if(isset($letterpad_details->header_logo)){
				echo '<div style="text-align: '.$letterpad_details->header_logo_alignment.';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>';
			}
        ?>
		<?php if ($proforma_invoice_data->party_type_1 == PARTY_TYPE_DOMESTIC_ID) { ?>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="<?=$column_cnt;?>">PROFORMA INVOICE</td>
			</tr>
			<tr>
				<td class="text-center text-left text-bold" colspan="<?=$column_cnt - 6;?>"><b>Consigner</b></td>
                <td class="text-bold" colspan="<?=$column_cnt - 8;?>">Proforma No. :</td>
                <td class="text-bold" colspan="<?=$column_cnt - 7;?>"><b>
					<?php
						$party_first_letters = first_letters($proforma_invoice_data->party_name);
						echo $this->applib->get_proforma_invoice_no($party_first_letters,$proforma_invoice_data->proforma_invoice_no);
					?>
                    </b></td>
                    <td class="text-bold" colspan="<?=$column_cnt - 9;?>"><b><?=date('d/m/Y',strtotime($proforma_invoice_data->sales_order_date))?></b></td>
			</tr>
			<tr>
				<td class="text-bold" rowspan="4" colspan="<?=$column_cnt - 6;?>">
					<span style="font-size:15px;"><b><?=$company_details['name'];?></b></span><br>
					<?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?><br />
                    <b>GST No : <?=$company_details['gst_no'];?></b>
                </td>
				<td class="text-bold" colspan="<?=$column_cnt - 8;?>">Quotation No. : </td>
                                <td class="text-bold" colspan="<?=$column_cnt - 6;?>"><b>
					<?php
						$item_code = $proforma_invoice_items[0]['item_code'] or '';
						echo $this->applib->get_quotation_ref_no($proforma_invoice_data->quotation_no, $item_code);
					?>
                                    </b></td>
			</tr>
                        <tr><td class="text-bold" colspan="<?=$column_cnt - 8;?>">Purchase Order No.:</td>
                        <td class="text-bold" colspan="<?=$column_cnt - 7;?>"><b>
					<?php
						if(!empty($proforma_invoice_data->cust_po_no)){
							echo $proforma_invoice_data->cust_po_no;
						} else {
							echo $proforma_invoice_data->sales_order_pref;
						}
					?>
                        </b></td>
                    <td class="text-bold" colspan="<?=$column_cnt - 9;?>"><b><?=date('d/m/Y',strtotime($proforma_invoice_data->po_date))?></b></td></tr>
			<tr>
				<?php $item_code = $proforma_invoice_items[0]['item_code'] or ''; ?>
				<td class="text-bold text-center" colspan="<?=$column_cnt - 4;?>">
					<strong>Terms of Payment </strong>				
				</td>
			</tr>
			<tr>
				<td class="text-bold" rowspan="" colspan="<?=$column_cnt - 4;?>">
					<?=nl2br($proforma_invoice_data->supplier_payment_terms);?>
				</td>
			</tr>
			<tr>
				<td class="text-center text-left text-bold" colspan="<?=$column_cnt - 6;?>"><b>Consignee</b></td>
				<td class="text-bold text-center" colspan="<?=$column_cnt - 4;?>"><strong>Our Banks Account Details</strong><br/></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					<strong style="font-size:15px;" ><?=$proforma_invoice_data->party_name;?></strong><br />
					<?=nl2br($proforma_invoice_data->address);?><br />
					City : <?=$proforma_invoice_data->city?> -  <?=$proforma_invoice_data->pincode?> (<?=$proforma_invoice_data->state?>) <?=$proforma_invoice_data->country?>.<br />
					Email : <?=explode(",", $proforma_invoice_data->email_id)[0];?><br />
					Tel No.:  <?=$proforma_invoice_data->fax_no;?>,<br />
					Contact No.:  <?=$proforma_invoice_data->p_phone_no;?>, <?php //$proforma_invoice_data->contact_person_phone?><br />
					Contact Person: <?=$proforma_invoice_data->contact_person_name?><br />
                    <b>GST No. : <?=$proforma_invoice_data->p_gst_no;?></b><?= isset($proforma_invoice_data->party_cin_no) && !empty($proforma_invoice_data->party_cin_no) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$proforma_invoice_data->party_cin_no.'</b>' : ''; ?>
				</td>
                                <td class="text-bold " rowspan="3" colspan="<?=$column_cnt - 4;?>" style="vertical-align: top">
					<?php
                        $bank_inc = 1;
                        foreach ($company_banks as $company_bank_detail){
                            echo $bank_inc . ') ' . $company_bank_detail->detail. '<br />';
                            $bank_inc++;
                        }
                    ?>
				</td>
			</tr>
                        <tr><td class="text-center text-left text-bold" colspan="<?=$column_cnt - 6;?>"><b>Purchaser Delivery Address</b></td></tr>
			<tr>
				<td class="text-bold" colspan="<?=$column_cnt - 6;?>">
					<?php if($proforma_invoice_data->address_work == 0){ ?>
						<b style="font-size: 15px"><?=$proforma_invoice_data->delivery_party_name?></b><br />
						<?=nl2br($proforma_invoice_data->delivery_address)?><br />
						City : <?=$proforma_invoice_data->delivery_city?> -  <?=$proforma_invoice_data->delivery_oldw_pincode?> (<?=$proforma_invoice_data->delivery_state?>) <?=$proforma_invoice_data->delivery_country?>.<br />
						Email : <?=$proforma_invoice_data->delivery_email_id;?><br />
						Contact No.:  <?=$proforma_invoice_data->delivery_contact_no;?>
					<?php } else { ?>
						<b style="font-size: 15px"><?=$proforma_invoice_data->party_name?></b><br />
						<?=nl2br($proforma_invoice_data->address);?><br />
						City : <?=$proforma_invoice_data->city?> -  <?=$proforma_invoice_data->pincode?> (<?=$proforma_invoice_data->state?>) <?=$proforma_invoice_data->country?>.<br />
						Email : <?=explode(",", $proforma_invoice_data->party_email_id)[0];?><br />
						Contact No.:  <?=$proforma_invoice_data->p_phone_no;?><?= ($proforma_invoice_data->p_phone_no != $proforma_invoice_data->contact_person_phone) ? ', '.$proforma_invoice_data->contact_person_phone : '' ?><br />
					<?php } ?>
                                </td></tr>
			<tr>
				<td class="text-bold" colspan="2">Loading At :</td>
                                <td class="text-bold" colspan="1"><b><?=$proforma_invoice_data->loading_at; ?></b></td>
                                <td class="text-bold" colspan="2">Port of Loading :</td>
                                <td class="text-bold" colspan="1"><b><?=$proforma_invoice_data->port_of_loading_city; ?></b></td>
                                <td class="text-bold" colspan="2">Port of Discharge :</td>
                                <td class="text-bold" colspan="2"><b><?=$proforma_invoice_data->port_of_discharge_city; ?></b></td>
			</tr>
			<tr>
                                <td class="text-bold" colspan="2">Place of Delivery :</td>
                                <td class="text-bold" colspan="3"><b><?=$proforma_invoice_data->place_of_delivery_city; ?></b></td>
                                <td class="text-bold" colspan="2">Delivery Through :</td>
                                <td class="text-bold" colspan="3"><b><?=$proforma_invoice_data->delivery_through; ?></b></td>
			</tr>
			<tr>
				<td class="text-center text-bold" width="6%"><b>Sr.No.</b></td>
				<td class="text-center text-bold" width="29%"><b>Item Name</b></td>
				<td class="text-center text-bold" width="7%"><b>HSN</b></td>
				<td class="text-center text-bold" width="5%"><b>Unit / Nos</b></td>
				<td class="text-center text-bold" width="9%"><b>Rate</b></td>
				<td class="text-center text-bold" width="8%"><b>Discount</b></td>
				<td class="text-center text-bold" width="7%"><b>CGST</b></td>
				<td class="text-center text-bold" width="7%"><b>SGST</b></td>
				<td class="text-center text-bold" width="7%"><b>IGST</b></td>
				<td class="text-center text-bold" width="12%"><b>Amount</b></td>
			</tr>
			<?php
				$pure_amount = 0;
				$discount_total = 0;
				$taxable_amount = 0;
				$invoice_cgst_amount = 0;
				$invoice_sgst_amount = 0;
				$invoice_igst_amount = 0;
				$gst_total = 0;
				$grand_total = 0;
				if (!empty($proforma_invoice_items)) {
					$i_inc = 1;
					foreach ($proforma_invoice_items as $key => $item_row) {
			?>
						<tr>
							<td class="text-center text-bold" ><?=$i_inc?></td>
							<td class="text-left text-bold" ><?=$item_row['item_name'];?> <br /> <?=$item_row['item_extra_accessories'];?></td>
							<td class="text-center text-bold" ><?=$item_row['hsn'];?></td>
							<td class="text-center text-bold" ><?=$item_row['quantity'].' '.$item_row['uom'];?></td>
							<td class="text-right text-bold" ><?=money_format('%!i', $item_row['rate']);?></td>
							<td class="text-center text-bold" ><?=$item_row['disc_per'];?> %</td>
							<td class="text-center text-bold" ><?=$item_row['cgst'];?> %</td>
							<td class="text-center text-bold" ><?=$item_row['sgst'];?> %</td>
							<td class="text-center text-bold" ><?=$item_row['igst'];?> %</td>
							<td class="text-right text-bold" ><?=money_format('%!i', $item_row['net_amount'])?></td>
						</tr>
			<?php
						$i_inc++;
                        $discount_total += $item_row['disc_value'];
                        $pure_amount += $item_row['quantity'] * $item_row['rate'];
						$invoice_cgst_amount += $item_row['cgst_amount'];
						$invoice_sgst_amount += $item_row['sgst_amount'];
						$invoice_igst_amount += $item_row['igst_amount'];
						$gst_total += $item_row['cgst_amount'] + $item_row['sgst_amount'] + $item_row['igst_amount'];
					}
				}
				$taxable_amount = $pure_amount - $discount_total;
				
				$proforma_invoice_data->freight = !empty($proforma_invoice_data->freight)?$proforma_invoice_data->freight:'0';
				$proforma_invoice_data->packing_forwarding = !empty($proforma_invoice_data->packing_forwarding)?$proforma_invoice_data->packing_forwarding:'0';
				$proforma_invoice_data->transit_insurance = !empty($proforma_invoice_data->transit_insurance)?$proforma_invoice_data->transit_insurance:'0';
				$total_amount = $taxable_amount + $proforma_invoice_data->freight + $proforma_invoice_data->packing_forwarding + $proforma_invoice_data->transit_insurance;
				
				$proforma_invoice_data->freight_cgst = !empty($proforma_invoice_data->freight_cgst)?$proforma_invoice_data->freight_cgst:'0';
				$freight_cgst_amount = $proforma_invoice_data->freight * $proforma_invoice_data->freight_cgst / 100;
				$proforma_invoice_data->freight_sgst = !empty($proforma_invoice_data->freight_sgst)?$proforma_invoice_data->freight_sgst:'0';
				$freight_sgst_amount = $proforma_invoice_data->freight * $proforma_invoice_data->freight_sgst / 100;
				$proforma_invoice_data->freight_igst = !empty($proforma_invoice_data->freight_igst)?$proforma_invoice_data->freight_igst:'0';
				$freight_igst_amount = $proforma_invoice_data->freight * $proforma_invoice_data->freight_igst / 100;
				$freight_gst_amount = $freight_cgst_amount + $freight_sgst_amount + $freight_igst_amount;
				$freight_amount = $proforma_invoice_data->freight + $freight_gst_amount;
				
				$proforma_invoice_data->packing_forwarding_cgst = !empty($proforma_invoice_data->packing_forwarding_cgst)?$proforma_invoice_data->packing_forwarding_cgst:'0';
				$packing_forwarding_cgst_amount = $proforma_invoice_data->packing_forwarding * $proforma_invoice_data->packing_forwarding_cgst / 100;
				$proforma_invoice_data->packing_forwarding_sgst = !empty($proforma_invoice_data->packing_forwarding_sgst)?$proforma_invoice_data->packing_forwarding_sgst:'0';
				$packing_forwarding_sgst_amount = $proforma_invoice_data->packing_forwarding * $proforma_invoice_data->packing_forwarding_sgst / 100;
				$proforma_invoice_data->packing_forwarding_igst = !empty($proforma_invoice_data->packing_forwarding_igst)?$proforma_invoice_data->packing_forwarding_igst:'0';
				$packing_forwarding_igst_amount = $proforma_invoice_data->packing_forwarding * $proforma_invoice_data->packing_forwarding_igst / 100;
				$packing_forwarding_gst_amount = $packing_forwarding_cgst_amount + $packing_forwarding_sgst_amount + $packing_forwarding_igst_amount;
				$packing_forwarding_amount = $proforma_invoice_data->packing_forwarding + $packing_forwarding_gst_amount;
				
				$proforma_invoice_data->transit_insurance_cgst = !empty($proforma_invoice_data->transit_insurance_cgst)?$proforma_invoice_data->transit_insurance_cgst:'0';
				$transit_insurance_cgst_amount = $proforma_invoice_data->transit_insurance * $proforma_invoice_data->transit_insurance_cgst / 100;
				$proforma_invoice_data->transit_insurance_sgst = !empty($proforma_invoice_data->transit_insurance_sgst)?$proforma_invoice_data->transit_insurance_sgst:'0';
				$transit_insurance_sgst_amount = $proforma_invoice_data->transit_insurance * $proforma_invoice_data->transit_insurance_sgst / 100;
				$proforma_invoice_data->transit_insurance_igst = !empty($proforma_invoice_data->transit_insurance_igst)?$proforma_invoice_data->transit_insurance_igst:'0';
				$transit_insurance_igst_amount = $proforma_invoice_data->transit_insurance * $proforma_invoice_data->transit_insurance_igst / 100;
				$transit_insurance_gst_amount = $transit_insurance_cgst_amount + $transit_insurance_sgst_amount + $transit_insurance_igst_amount;
				$transit_insurance_amount = $proforma_invoice_data->transit_insurance + $transit_insurance_gst_amount;
				
				$cgst_amount = $invoice_cgst_amount + $freight_cgst_amount + $packing_forwarding_cgst_amount + $transit_insurance_cgst_amount;
				$sgst_amount = $invoice_sgst_amount + $freight_sgst_amount + $packing_forwarding_sgst_amount + $transit_insurance_sgst_amount;
				$igst_amount = $invoice_igst_amount + $freight_igst_amount + $packing_forwarding_igst_amount + $transit_insurance_igst_amount;
				$total_gst_amount = $invoice_cgst_amount + $invoice_sgst_amount + $invoice_igst_amount + $freight_gst_amount + $packing_forwarding_gst_amount + $transit_insurance_gst_amount;
				$grand_total = $total_amount + $total_gst_amount;
				
			?>
			<tr>
				<td class="" rowspan="8" colspan="6">
					<table class="no-border gst_summary">
                        <tr>
                            <td align="center" class="no-border-top no-border-left" width="120px" >GST Summary</td>
                            <td align="center" class="no-border-top" >Taxable Amt</td>
                            <td align="center" class="no-border-top" >CGST</td>
                            <td align="center" class="no-border-top" >SGST</td>
                            <td align="center" class="no-border-top" >IGST</td>
                            <td align="center" class="no-border-top" width="60px" >Total</td>
                        </tr>
                        <?php
							if (!empty($proforma_invoice_items)) {
								foreach ($proforma_invoice_items as $key => $i_row) {
									$p_amount = $i_row['quantity'] * $i_row['rate'];
									$t_amount = $p_amount - $i_row['disc_value'];
						?>
									<tr>
										<td class="no-border-left" ><?=$i_row['item_name'];?></td>
										<td align="right"><?=money_format('%!i', $t_amount);?></td>
										<td align="right"><?=money_format('%!i', $i_row['cgst_amount']);?> : @<?=$i_row['cgst'];?>%</td>
										<td align="right"><?=money_format('%!i', $i_row['sgst_amount']);?> : @<?=$i_row['sgst'];?>%</td>
										<td align="right"><?=money_format('%!i', $i_row['igst_amount']);?> : @<?=$i_row['igst'];?>%</td>
										<td align="right"><?=money_format('%!i', $i_row['net_amount']);?></td>
									</tr>
                        <?php 
								}
							}
						?>
                        <tr>
                            <td class="no-border-left" >Freight</td>
                            <td align="right"><?=money_format('%!i', $proforma_invoice_data->freight);?></td>
                            <td align="right"><?=money_format('%!i', $freight_cgst_amount);?> : @<?=$proforma_invoice_data->freight_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_sgst_amount);?> : @<?=$proforma_invoice_data->freight_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_igst_amount);?> : @<?=$proforma_invoice_data->freight_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $freight_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" >Packing & Forwarding</td>
                            <td align="right"><?=money_format('%!i', $proforma_invoice_data->packing_forwarding);?></td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_cgst_amount);?> : @<?=$proforma_invoice_data->packing_forwarding_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_sgst_amount);?> : @<?=$proforma_invoice_data->packing_forwarding_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_igst_amount);?> : @<?=$proforma_invoice_data->packing_forwarding_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $packing_forwarding_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" >Transit Insurance</td>
                            <td align="right"><?=money_format('%!i', $proforma_invoice_data->transit_insurance);?></td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_cgst_amount);?> : @<?=$proforma_invoice_data->transit_insurance_cgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_sgst_amount);?> : @<?=$proforma_invoice_data->transit_insurance_sgst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_igst_amount);?> : @<?=$proforma_invoice_data->transit_insurance_igst;?>%</td>
                            <td align="right"><?=money_format('%!i', $transit_insurance_amount);?></td>
                        </tr>
                        <tr>
                            <td class="no-border-left" ><b>Total</b></td>
                            <td align="right"><?=money_format('%!i', $total_amount);?></td>
                            <td align="right"><?=money_format('%!i', $cgst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $sgst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $igst_amount);?></td>
                            <td align="right"><?=money_format('%!i', $grand_total);?></td>
                        </tr>
                    </table>
				</td>
				<td class=" text-bold" colspan="3">Sub Total : </td>
                                <td class="text-right text-bold" colspan="1"><b> <?=money_format('%!i', $pure_amount)?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="3">Discount : </td>
                                <td class="text-right text-bold" colspan="1"><b><?=money_format('%!i', $discount_total)?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="3">Sub Total : </td>
                                <td class="text-right text-bold" colspan="1"><b><?=money_format('%!i', $taxable_amount)?></b></td>
			</tr>
			<tr>
                <td class=" text-bold" colspan="3">Freight <?= (!empty($proforma_invoice_data->freight_note)) ? '('.$proforma_invoice_data->freight_note.')' : ''; ?> : </td>
                <td class=" text-bold" colspan="1" align="right"><b><?=money_format('%!i', $proforma_invoice_data->freight);?></b></td>
            </tr>
            <tr>
				<td class=" text-bold" colspan="3">P & F <?= (!empty($proforma_invoice_data->packing_forwarding_note)) ? '('.$proforma_invoice_data->packing_forwarding_note.')' : ''; ?> : </td>
                                <td class=" text-bold" colspan="1" align="right"><b><?=money_format('%!i', $proforma_invoice_data->packing_forwarding);?></b></td>
            </tr>
            <tr>
				<td class=" text-bold" colspan="3">Insurance <?= (!empty($proforma_invoice_data->transit_insurance_note)) ? '('.$proforma_invoice_data->transit_insurance_note.')' : ''; ?> : </td>
                                <td class=" text-bold" colspan="1" align="right"><b><?=money_format('%!i', $proforma_invoice_data->transit_insurance);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Total : </td>
                <td class=" text-bold" colspan="1" align="right"><b><?=money_format('%!i', $total_amount);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">GST : </td>
                <td class=" text-bold" colspan="1" align="right"><b><?=money_format('%!i', $total_gst_amount);?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6"><b>In Words <?=$CURRENCY?> : </b><?=money_to_word($grand_total)?> only.</td>
                                <td class=" text-bold" colspan="3"><b>Grand Total : </b></td>
                                <td class=" text-bold" colspan="1" align="right"><b><?=money_format('%!i', $grand_total);?></b></td>
            </tr>
			<tr>
				<td valign="top" class="text-bold" rowspan="3" colspan="<?=$column_cnt - 4;?>">
					Note : <?=nl2br($proforma_invoice_data->note);?>
				</td>
				<td align="center" class="text-bold footer-detail-area" colspan="<?=$column_cnt - 6;?>">
					<strong>For, <?=$company_details['name'];?></strong><br/>
				</td>
			</tr>
			<tr>
				<td align="center" class="text-bold no-border-top" colspan="<?=$column_cnt - 6;?>">
					<img src="<?php echo BASE_URL;?>/resource/image/jk_symbol.jpg" style="margin-bottom:5px">
				</td>
			</tr>
			<tr>
				<td align="center" class="text-bold no-border-top" colspan="<?=$column_cnt - 6;?>">
					AUTHORISED SIGNATORY <b><?php //=date('d/m/Y',strtotime($proforma_invoice_data->created_at))?></b>
				</td>
			</tr>
		</table>
		<?php }elseif($proforma_invoice_data->party_type_1 == PARTY_TYPE_EXPORT_ID){ ?>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="<?=$column_cnt;?>">
					PROFORMA INVOICE
				</td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="5"><b>Consigner</b></td>
				<td class="text-bold" colspan="1" width="120px">Proforma Invoice No.:</td>
                                <td class="text-bold" colspan="3" width="190px"><b>
					<?php
						$party_first_letters = first_letters($proforma_invoice_data->party_name);
						echo $this->applib->get_proforma_invoice_no($party_first_letters,$proforma_invoice_data->proforma_invoice_no);
                                                ?></b></td>

                                <td class=" text-bold" colspan="1" width="80px"><b><?=strtotime($proforma_invoice_data->sales_order_date) != 0?date('d/m/Y',strtotime($proforma_invoice_data->sales_order_date)):date('d/m/Y')?></b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="5" style="font-size:15px;"><b>Jay Khodiyar Machine Tools</b></td>
				<td class="text-bold" colspan="1">Quotation No. : </td>
                                <td class="text-bold" colspan="3"><b>
					<?php
						$item_code = $proforma_invoice_items[0]['item_code'] or '';
						echo $this->applib->get_quotation_ref_no($proforma_invoice_data->quotation_no, $item_code);
                                                ?></b>
				</td>
                                <td class="text-bold" colspan="1"><b><?=strtotime($proforma_invoice_data->quotation_date) != 0 ? date('d/m/Y',strtotime($proforma_invoice_data->quotation_date)) : date('d/m/Y')?></b></td>
			</tr>
			<tr>
				<td class="no-border-top text-bold" rowspan="4" colspan="5" valign="top">
					<?=nl2br($company_details['address']);?><br />
					City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
					Email: <?=$company_details['email_id'];?><br />
					Tel No. : <?=$company_details['contact_no'];?>,<br />
					Contact No. : <?=$company_details['cell_no'];?><br />
				</td>
				<td class="text-bold" colspan="1">Purchase Order No.:</td>
                                <td class="text-bold" colspan="3"><b>
					<?php
				  if(!empty($proforma_invoice_data->cust_po_no)){
					  echo $proforma_invoice_data->cust_po_no;
				  } else {
					  echo $proforma_invoice_data->sales_order_pref;
				  }
                                  ?></b></td>
				
                                <td class="text-bold" colspan="1"><b><?=strtotime($proforma_invoice_data->po_date) != 0?date('d/m/Y',strtotime($proforma_invoice_data->po_date)):date('d/m/Y')?></b> </td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="1">IEC No. : </td>
                                <td class=" text-bold" colspan="4"><b><?=$company_details['iec_no'];?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="1">GST No. : </td>
                                <td class=" text-bold" colspan="4"><b><?=$company_details['gst_no'];?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="1">PAN No. : </td>
                                <td class=" text-bold" colspan="4"><b><?=$company_details['pan_no'];?></b></td>
			</tr>
			
			<tr>
				<td class="text-center text-bold " colspan="5"><b>Purchaser</b></td>
				<td class="text-center text-bold " colspan="5"><b>Terms of Payment & Delivery</b></td>
			</tr>
			
			<tr>
				<td valign="top"  class=" text-bold" rowspan="3" colspan="5">
                                        <b style="font-size:15px;"><?=$proforma_invoice_data->party_name?></b><br/>
					<?=nl2br($proforma_invoice_data->address);?><br />
					City : <?=$proforma_invoice_data->city?> -  <?=$proforma_invoice_data->pincode?> (<?=$proforma_invoice_data->state?>) <?=$proforma_invoice_data->country?>.<br />
					Email : <?=explode(",", $proforma_invoice_data->email_id)[0];?><br />
					Tel No.:  <?=$proforma_invoice_data->fax_no;?>,<br />
					Contact No.:  <?=$proforma_invoice_data->p_phone_no;?>, <?php //$proforma_invoice_data->contact_person_phone?><br />
					Contact Person: <?=$proforma_invoice_data->contact_person_name?>
				</td>
                                <td  class="text-bold" colspan="5"><?=$proforma_invoice_data->supplier_payment_terms;?></td>
			</tr>
                        <tr>
                                <td class="text-center text-bold" colspan="5"><b>Our Bank Accounts Details </b></td>
			</tr>
			<tr>
                            <td class="text-bold " rowspan="3" colspan="5" style="vertical-align: top">
					<?php
                        $bank_inc = 1;
                        foreach ($company_banks as $company_bank_detail){
                            echo $bank_inc . ') ' . $company_bank_detail->detail. '<br />';
                            $bank_inc++;
                        }
                    ?>
				</td>
			</tr>
                        <tr><td class="text-center text-left text-bold" colspan="5"><b>Purchaser Delivery Address</b></td></tr>
			<tr>
				<td class="text-bold" colspan="5">
					<?php if($proforma_invoice_data->address_work == 0){ ?>
						<b style="font-size: 15px"><?=$proforma_invoice_data->delivery_party_name?></b><br />
						<?=nl2br($proforma_invoice_data->delivery_address)?><br />
						City : <?=$proforma_invoice_data->delivery_city?> -  <?=$proforma_invoice_data->delivery_oldw_pincode?> (<?=$proforma_invoice_data->delivery_state?>) <?=$proforma_invoice_data->delivery_country?>.<br />
						Email : <?=$proforma_invoice_data->delivery_email_id;?><br />
						Contact No.:  <?=$proforma_invoice_data->delivery_contact_no;?>
					<?php } else { ?>
						<b style="font-size: 15px"><?=$proforma_invoice_data->party_name?></b><br />
						<?=nl2br($proforma_invoice_data->address);?><br />
						City : <?=$proforma_invoice_data->city?> -  <?=$proforma_invoice_data->pincode?> (<?=$proforma_invoice_data->state?>) <?=$proforma_invoice_data->country?>.<br />
						Email : <?=explode(",", $proforma_invoice_data->party_email_id)[0];?><br />
						Contact No.:  <?=$proforma_invoice_data->p_phone_no;?><?= ($proforma_invoice_data->p_phone_no != $proforma_invoice_data->contact_person_phone) ? ', '.$proforma_invoice_data->contact_person_phone : '' ?><br />
					<?php } ?>
                                </td></tr>
			<tr>
                            <td class="text-center text-bold" colspan="10"><b>&nbsp;</b></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10"><b>Dispatch Detials</b></td>
			</tr>
			
			<tr>
				<td class="text-bold" colspan="2">Loading At : </td>
                                <td class="text-bold" colspan="3"><b><?=$proforma_invoice_data->loading_at;?></b></td>
				<td class="text-bold" colspan="1">Port of  Discharge:</td>
                                <td class="text-bold" colspan="2"><b><?=$proforma_invoice_data->port_of_discharge_city;?></b></td>
                                <td class="text-bold" colspan="2"><b><?=$proforma_invoice_data->port_of_discharge_country;?></b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="2">Port of Loading : </td>
                                <td class="text-bold" colspan="1"><b><?=$proforma_invoice_data->port_of_loading_city;?></b></td>
                                <td class="text-bold" colspan="2"><b><?=$proforma_invoice_data->port_of_loading_country;?></b></td>
				<td class="text-bold" colspan="1">Place of Delivery :</td>
                                <td class="text-bold" colspan="2"><b><?=$proforma_invoice_data->place_of_delivery_city;?></b></td>
                                <td class="text-bold" colspan="2"><b><?=$proforma_invoice_data->place_of_delivery_country;?></b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="2">Delivery Through :</td>
                                <td class="text-bold" colspan="3"><b><?=$proforma_invoice_data->delivery_through;?></b></td>
				<td class="text-bold" colspan="1">Shipping Line Name :</td>
                                <td class="text-bold" colspan="4"><b><?=$proforma_invoice_data->shipping_line_name;?></b></td>
			</tr>
                        <tr>
                            <td class="text-center text-bold" colspan="10"><b>&nbsp;</b></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="1" width="50px"><b>Sr.No.</b></td>
				<td class="text-center text-bold"colspan="3" width="250px"><b>Item Name</b></td>
				<td class="text-center text-bold" colspan="1" width="50px"><b>HSN</b></td>
				<td class="text-center text-bold" colspan="1"><b>Qty</b></td>
				<td class="text-center text-bold" colspan="2"><b>Rate Per Unit <?=$CURRENCY?></b></td>
				<td class="text-center text-bold" colspan="2"><b>Amount <?=$CURRENCY?></b></td>
			</tr>
                        

			<?php
				  $sub_total = 0;
				  $discount_total = 0;
				  $second_sub_total = 0;
				  $round_off = 0;
				  $packing_forwarding = 0;
				  $transport_amount = 0;
				  $insurance_amount = 0;
				  $other_amount = 0;
				  $other_gst = 0;
				  $grand_total = 0;
				  $sea_freight = 0;
				  if (!empty($proforma_invoice_items)) {
					  $i_inc = 1;
					  foreach ($proforma_invoice_items as $key => $item_row) {
						  $before_discount_price = $item_row['quantity'] * $item_row['rate'];
						  //$discount_amt = ($before_discount_price * $item_row['disc_per'])/100;
                          $discount_amt = $item_row['disc_value'];
						  $discounted_price = $before_discount_price - $discount_amt;
						  $net_amount = $discounted_price ;

                        ?>
                        <tr>
                            <td class="text-center text-bold" colspan="1" width="50px"><?=$i_inc?></td>
                            <td class="text-left text-bold" colspan="3"><b><?=$item_row['item_name'];?> <br /> <?=$item_row['item_extra_accessories'];?></b></td>
                            <td class="text-center text-bold" colspan="1" ><?=$item_row['hsn'];?></td>
                            <td class="text-center text-bold" colspan="1" ><?=$item_row['quantity'].' '.$item_row['uom'];?></td>
                            <td class="text-right text-bold" colspan="2"><?=number_format((float)$item_row['rate'], 2, '.', ''); ?></td>
                            <!--<td class="text-right" ><?=$item_row['net_amount']?></td>-->
                            <td class="text-right text-bold" colspan="2"><?=number_format((float)$before_discount_price, 2, '.', ''); ?></td>
                        </tr>
                <?php
                        $i_inc++;
                        $sub_total += ($item_row['quantity'] * $item_row['rate']);
					  }
				  }
				  $second_sub_total = $sub_total - $discount_total;
				  $packing_forwarding = !empty($proforma_invoice_data->packing_forwarding) ? $proforma_invoice_data->packing_forwarding : '0.00';
                  //$sea_freight = $proforma_invoice_data->sea_freight;
				  //$sea_freight = (!empty($proforma_invoice_data->sea_freight) ? $proforma_invoice_data->sea_freight : null);
				  $sea_freight = !empty($proforma_invoice_data->sea_freight) ? $proforma_invoice_data->sea_freight : '0.00';
																				

				  
				  $grand_total = $second_sub_total + $round_off + $other_gst + $packing_forwarding + $sea_freight;
			?>
			<tr>
				<td valign='top' class="text-left text-bold" rowspan="4" colspan="6">
                    <strong>In Words <?=$CURRENCY?> :</strong> <?=money_to_word($grand_total)?> only.
				</td>
                <td class="text-right text-bold" colspan="2">Ex-Factory</td>
                <td class="text-right text-bold" colspan="2"> <b><?=number_format((float)$sub_total, 2, '.', ''); ?></b></td>
			</tr>
            <?php 
            $packing_forwarding_by = '';
            $sea_freight_by = '';
            if($proforma_invoice_data->packing_forwarding_by == '1'){
                $packing_forwarding_by = 'Excluding';
            } else {
                $packing_forwarding_by = 'Including';
            }
            if($proforma_invoice_data->sea_freight_by == '1'){
                $sea_freight_by = 'Excluding';
            } else {
                $sea_freight_by = 'Including';
            }
            
            ?>
			<tr>
                            <td class="text-right text-bold" colspan="2">Packing & Forwarding <?= (!empty($packing_forwarding_by)) ? '('.$packing_forwarding_by.')' : ''; ?></td>
                            <td class="text-right text-bold" colspan="2"><b><?=number_format((float)$packing_forwarding, 2, '.', ''); ?></b></td>
			</tr>
			<tr>
                            <td class="text-right text-bold" colspan="2">Sea Freight (<?=$proforma_invoice_data->sea_freight_type?>) <?= (!empty($sea_freight_by)) ? '('.$sea_freight_by.')' : ''; ?> <?=$proforma_invoice_data->port_of_discharge_city;?></td>
                            <td class="text-right text-bold" colspan="2"><b><?=number_format((float)$sea_freight, 2, '.', ''); ?></b></td>
			</tr>
			<tr>
                            <td class="text-right text-bold" colspan="2"><b>Total <?= $CURRENCY?></b></td>
                            <td class="text-right text-bold" colspan="2"><b><?=number_format((float)$grand_total, 2, '.', ''); ?></b></td>
			</tr>
			<tr>
				<td colspan="<?=$column_cnt;?>">
					<table class="no-border">
						<tr class="no-border">
							<td valign='top' class="text-left text-bold no-border-top footer-detail-area" style="border-left: 0!important;" rowspan="2">
								<b>Note :</b> <?=nl2br($proforma_invoice_data->note);?>
							</td>
							<td align="center" class="no-border-top footer-detail-area">
								<strong>For, <?=$company_details['name'];?></strong>
							</td>
						</tr>
						<tr class="no-border">
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                <img src="<?php echo BASE_URL;?>/resource/image/jk_symbol.jpg" style="margin-bottom:5px"><br />
								<?=$company_details['m_d_name'];?><br/>
								Managing Director
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<?php } ?>
	</body>
</html>
<i class="fa fa-angle-down"></i>
