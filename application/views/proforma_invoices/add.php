<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?=base_url('proforma_invoices/save_proforma_invoices') ?>" method="post" id="save_proforma_invoices" novalidate>
        <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ ?>
            <input type="hidden" name="proforma_invoices_data[proforma_invoice_id]" id="proforma_invoice_id" value="<?=$proforma_invoices_data->id?>">
        <?php } ?>
        <input type="hidden" name="proforma_invoices_data[quotation_id]" id="quotation_id" value="<?=(isset($proforma_invoices_data->quotation_id))? $proforma_invoices_data->quotation_id : ''; ?>" />
        <input type="hidden" name="proforma_invoices_data[currency_id]" id="so_currency_id" value="<?=(isset($proforma_invoices_data->currency_id))? $proforma_invoices_data->currency_id : ''; ?>" />
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> 
                <small class="text-primary text-bold">Domestic Proforma Invoices <span id="partyname"></span></small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, Ctrl+S = Save</label></small>
                <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ ?>
                <?php if(isset($proforma_invoices_data->isApproved) && $proforma_invoices_data->isApproved == 1){ ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_PROFORMA_INVOICE,"allow")){ ?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;" <?=($proforma_invoices_data->delete_not_allow == 1) ? ' Disabled ' : ''; ?>>Disapprove</button>
                <?php } else { ?>
                    <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                <?php } ?>
                <?php } else { ?>
                    <?php if($this->applib->have_access_role(CAN_APPROVE_PROFORMA_INVOICE,"allow")){ ?>
                        <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                    <?php } else { ?>
                        <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                    <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
                    $proforma_invoices_view_role= $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"view");
                    $proforma_invoices_add_role= $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"add");
                    $proforma_invoices_edit_role= $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"edit");
                ?>
                <?php if($proforma_invoices_add_role || $proforma_invoices_edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($proforma_invoices_view_role): ?>
				<a href="<?= base_url()?>proforma_invoices/proforma_invoices_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Proforma Invoice List</a>
                <?php endif;?>
                <?php if($proforma_invoices_add_role): ?>
				<a href="<?= base_url()?>proforma_invoices/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Proforma Invoice</a>
                <?php endif;?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Proforma Invoices Details</a></li>
                        <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                        <li><a href="#tab_3" data-toggle="tab" id="tabs_3">General</a></li>
                        <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ ?>
                        <li><a href="#tab_4" data-toggle="tab" id="tabs_4">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Proforma Invoices Details</legend>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="proforma_invoice_no" <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ } else { echo 'style="display:none;"'; } ?> class="col-sm-3 input-sm">Proforma Invoices No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="proforma_invoices_data[proforma_invoice_no]" id="proforma_invoice_no" class="form-control input-sm" <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ } else { echo 'style="display:none;"'; } ?> readonly="" value="<?=(isset($proforma_invoices_data->proforma_invoice_no))? $proforma_invoices_data->proforma_invoice_no : $proforma_invoice_no; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="branch_id" class="col-sm-3 input-sm">Branch</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="proforma_invoices_data[branch_id]" id="branch_id" class="branch_id form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="agent_id" class="col-sm-3 input-sm">Agent</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="proforma_invoices_data[agent_id]" id="agent_id" class="agent_id form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_order_date" class="col-sm-3 input-sm">Proforma Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="proforma_invoices_data[sales_order_date]" id="sales_order_date" class="form-control input-sm pull-right input-datepicker" value="<?=(isset($proforma_invoices_data->sales_order_date))? date('d-m-Y', strtotime($proforma_invoices_data->sales_order_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="committed_date" class="col-sm-3 input-sm">Committed Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="proforma_invoices_data[committed_date]" id="committed_date" class="form-control input-sm pull-right input-datepicker" value="<?=(isset($proforma_invoices_data->committed_date))? date('d-m-Y', strtotime($proforma_invoices_data->committed_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_id" class="col-sm-3 input-sm">Sales</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <select name="proforma_invoices_data[sales_id]" id="sales_id" class="form-control input-sm select2 sales_id" disabled="disabled"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cust_po_no" class="col-sm-3 input-sm">Purchase Order No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="proforma_invoices_data[cust_po_no]" id="cust_po_no" class="form-control input-sm" value="<?=(isset($proforma_invoices_data->cust_po_no))? $proforma_invoices_data->cust_po_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="po_date" class="col-sm-3 input-sm">Purchase Order Date</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <input type="text" name="proforma_invoices_data[po_date]" id="po_date" class="form-control input-sm input-datepicker" value="<?=(isset($proforma_invoices_data->po_date))? date('d-m-Y', strtotime($proforma_invoices_data->po_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sales_order_pref_id" class="col-sm-3 input-sm">Purchase Order Ref</label>
                                                    <div class="col-sm-9">
                                                        <select name="proforma_invoices_data[sales_order_pref_id]" id="sales_order_pref_id" class="form-control input-sm select2" >
                                                            <option value="" selected>--Select--</option>
                                                            <?php foreach($sales_order_pref as $sales_order_pref_row): ?>
                                                            <option value="<?=$sales_order_pref_row->id?>" <?=(isset($proforma_invoices_data->sales_order_pref_id) && $proforma_invoices_data->sales_order_pref_id == $sales_order_pref_row->id)? ' Selected ' : ''; ?> >
                                                                <?=$sales_order_pref_row->sales_order_pref;?>
                                                            </option>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="note" class="col-sm-3 input-sm">Notes</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="proforma_invoices_data[note]" id="note" rows="2" ><?=(isset($proforma_invoices_data->note))? $proforma_invoices_data->note : ''; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="is_dispatched" class="col-sm-3 input-sm">Status</label>
                                                    <div class="col-sm-9 dispaly-flex">
                                                        <?php 
                                                            if(isset($proforma_invoices_data->is_dispatched) && $proforma_invoices_data->is_dispatched == DISPATCHED_ORDER_ID){
                                                                echo '<label>Dispatched</label>';
                                                            } else if(isset($proforma_invoices_data->not_any_dispatched) && $proforma_invoices_data->not_any_dispatched == 1){
                                                                echo '<label>Pending Dispatch</label>';
                                                            } else if(isset($proforma_invoices_data->is_dispatched) && $proforma_invoices_data->is_dispatched != DISPATCHED_ORDER_ID){
                                                        ?>
                                                                <select name="proforma_invoices_data[is_dispatched]" class="is_dispatched form-control input-sm select2" id="is_dispatched" >
                                                                    <option value="<?php echo PENDING_DISPATCH_ORDER_ID; ?>" <?php echo ($proforma_invoices_data->is_dispatched == PENDING_DISPATCH_ORDER_ID) ? ' Selected ' : ''; ?> > Pending Dispatch </option>
                                                                    <option value="<?php echo CANCELED_ORDER_ID; ?>" <?php echo ($proforma_invoices_data->is_dispatched == CANCELED_ORDER_ID) ? ' Selected ' : ''; ?> > Canceled Order </option>
                                                                    <option value="<?php echo CHALLAN_CREATED_FROM_SALES_ORDER_ID; ?>" <?php echo ($proforma_invoices_data->is_dispatched == CHALLAN_CREATED_FROM_SALES_ORDER_ID) ? ' Selected ' : ''; ?> > Challan Created from Sales Order </option>
                                                                </select>
                                                        <?php 
                                                            } else { 
                                                                echo '<label>Pending Dispatch</label>';
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="<?php echo (isset($proforma_invoices_data->is_dispatched) && $proforma_invoices_data->is_dispatched == CANCELED_ORDER_ID) ? '' : ' hidden '; ?> cancel_reason_div">
                                                    <div class="form-group">
                                                        <label for="sales_order_cancel_reason" class="col-sm-3 input-sm">Cancel Reason</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <textarea rows="2" class="form-control" name="proforma_invoices_data[cancel_reason]" id="cancel_reason_text"><?=(isset($proforma_invoices_data->cancel_reason))? $proforma_invoices_data->cancel_reason : ''; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                    <label for="proof_of_cancellation" class="col-sm-3 input-sm">Proof of Cancellation</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <input type="text" name="proforma_invoices_data[proof_of_cancellation]" id="proof_of_cancellation" class="form-control input-sm" value="<?=(isset($proforma_invoices_data->proof_of_cancellation))? $proforma_invoices_data->proof_of_cancellation : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-12 <?php echo (isset($proforma_invoices_data->is_dispatched) && $proforma_invoices_data->is_dispatched == CANCELED_ORDER_ID) ? '' : ' hidden '; ?> cancel_reason_div">
                                                <fieldset class="scheduler-border">
                                                    <legend class="scheduler-border text-primary text-bold"> Cancel Letter </legend>
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <textarea name="proforma_invoices_data[cancel_letter]" id="cancel_letter" class="form-control">
                                                                <?=(isset($proforma_invoices_data->cancel_letter))? $proforma_invoices_data->cancel_letter : ''; ?>
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Party Detail</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
                                                        <select name="proforma_invoices_data[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" placeholder="" readonly>
													</div>
												</div>
												<div class="clearfix"></div>
												<input type="hidden" name="party[party_name]" id="party_name" class="form-control input-sm party_name" placeholder="">
												<input type="hidden" name="party[party_id]" id="party_party_id" class="form-control input-sm party_party_id" placeholder="">
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="form-group">
													<label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
													<div class="col-sm-9">
														<textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
														<textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="city" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="state" class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="country" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
														<small>Add multiple email id by Comma separated.</small>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9">
														<textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
														<small>Add multiple phone number by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_website" class="col-sm-3 input-sm">Website</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm website" id="party_website" name="party[website]" placeholder="" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<?php /*<div class="form-group">
													<label for="agent_id" class="col-sm-3 input-sm">Agent</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[agent_id]" id="agent_id" class="form-control input-sm"></select>
													</div>
												</div>
												<div class="clearfix"></div>*/ ?>
												<div class="form-group">
													<label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="proforma_invoices_data[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
															<option value="">--Select--</option>
                                                            <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ ?>
                                                                <?php foreach($party_contact_person as $pcp_key => $pcp_value): ?>
                                                                <option value="<?= $pcp_value->contact_person_id; ?>"  <?=$proforma_invoices_data->kind_attn_id == $pcp_value->contact_person_id ?'selected="selected"':'';?>><?= $pcp_value->name; ?></option>
                                                                <?php endforeach; ?>
                                                            <?php } ?>
                                                        </select>
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Designation</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_designation"><?php echo isset($contact_person->designation) ? $contact_person->designation : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Department</label>
                                                    <div class="col-sm-9">
                                                        <div id="contact_person_department"><?php echo isset($contact_person->department) ? $contact_person->department : '' ;?></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person->mobile_no) ? $contact_person->mobile_no : '' ;?>" disabled="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person->email) ? $contact_person->email : '' ;?>" disabled="">
                                                    </div>
                                                </div>
											</div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border item-detail item_fields_div">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($proforma_invoices_lineitems)){ ?>
                                            <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                        <?php } ?>
                                        <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="item_id" class="col-sm-3 input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id" onchange="item_details(this.value)"></select>
                                                </div>                                                       
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_code]" id="item_code" class="form-control input-sm item_code disabled" data-name="item_code" readonly="readonly">
                                                    <input type="hidden" class="item_name" id="item_name" name="line_items_data[item_name]" data-name="item_name" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[item_description]" id="item_description" class="form-control input-sm item_description" data-name="item_description" >
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="item_extra_accessories_id" class="col-sm-3 input-sm">Item Extra Accessories</label>
                                                <div class="col-sm-9">
                                                    <select name="line_items_data[item_extra_accessories_id]" id="item_extra_accessories_id" class="item_extra_accessories_id form-control input-sm select2 item_data" data-input_name="item_extra_accessories_id" ></select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity num_only" value="1" data-name="quantity" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-3 input-sm">Rate<span class="required-sign">&nbsp;*</span></label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <select name="line_items_data[currency_id]" id="currency_id" class="form-control input-sm currency_id" ></select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate"  value="0" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="disc_per" class="col-sm-3 input-sm">Disc(%)</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[disc_per]" id="disc_per" class="form-control input-sm disc_per num_only" data-input_name="disc_per" placeholder="">
                                                </div>
                                                <label for="disc_value" class="col-sm-2 input-sm">Disc(Value)</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[disc_value]" id="disc_value" class="form-control input-sm disc_value num_only" data-input_name="disc_value" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="igst" class="col-sm-3 input-sm">IGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[igst]" id="igst" class="form-control input-sm igst item_data" data-input_name="igst" placeholder="">
                                                </div>
                                                <label for="igst_amount" class="col-sm-2 input-sm">IGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[igst_amount]" id="igst_amount" class="form-control input-sm igst_amount item_data" readonly data-input_name="igst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="cgst" class="col-sm-3 input-sm">CGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[cgst]" id="cgst" class="form-control input-sm cgst numberwithoutzero item_data" data-input_name="cgst" placeholder="">
                                                </div>
                                                <label for="cgst_amount" class="col-sm-2 input-sm">CGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[cgst_amount]" id="cgst_amount" class="form-control input-sm cgst_amount numberwithoutzero item_data" readonly data-input_name="cgst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="sgst" class="col-sm-3 input-sm">SGST %</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[sgst]" id="sgst" class="form-control input-sm sgst item_data" data-input_name="sgst" placeholder="">
                                                </div>
                                                <label for="sgst_amount" class="col-sm-2 input-sm">SGST Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[sgst_amount]" id="sgst_amount" class="form-control input-sm sgst_amount item_data" readonly data-input_name="sgst_amount" placeholder="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                <div class="col-sm-3" style="padding-right: 0px;">
                                                    <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                </div>
                                                <label for="net_amount" class="col-sm-2 input-sm">Net Amount</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="line_items_data[net_amount]" id="net_amount" class="form-control input-sm net_amount disabled" data-input_name="net_amount" placeholder="" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-sm-12"><div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" />
                                            </div>
                                        </div></div>
                                    </fieldset>
                                </div>
                            </div> <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <table style="" class="table custom-table item-table">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Amount</th>
                                                <th class="text-right">Disc(value)</th>
                                                <th class="text-right">IGST %</th>
                                                <th class="text-right">CGST %</th>
                                                <th class="text-right">SGST %</th>
                                                <th class="text-right">Net Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lineitem_list"></tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th class="text-right"><span class="qty_total"></span><input type="hidden" name="qty_total" id="qty_total" /></th>
                                                <th class="text-right"></th>
                                                <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                                <th class="text-right" class="text-right"><span class="disc_value_total"></span><input type="hidden" name="disc_value_total" id="disc_value_total" /></th>
                                                <th class="text-right"></th>
                                                <th class="text-right"></th>
                                                <th class="text-right"></th>
                                                <th class="text-right" class="text-right"><span class="net_amount_total"></span><input type="hidden" name="net_amount_total" id="net_amount_total" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Genaral Fields To Be Set For Proforma Print </legend>
                                        <div class="col-md-6">
											<div class="form-group">
												<label for="freight" class="col-sm-5 input-sm"> Freight </label>
												<div class="col-sm-4">
													<input type="text" class="form-control input-sm num_only" id="proforma_invoices_data[freight]" name="proforma_invoices_data[freight]" placeholder="Freight Amount" value="<?=(isset($proforma_invoices_data->freight))? $proforma_invoices_data->freight : ''; ?>">
												</div>
												<div class="col-sm-3" style="padding-left: 0px;">
													<input type="text" class="form-control input-sm" id="proforma_invoices_data[freight_note]" name="proforma_invoices_data[freight_note]" value="<?=(isset($proforma_invoices_data->freight_note))? $proforma_invoices_data->freight_note : ''; ?>">
												</div>
											</div>
											<div class="form-group">
												<label for="packing_forwarding" class="col-sm-5 input-sm"> Packing & Forwarding </label>
												<div class="col-sm-4">
													<input type="text" class="form-control input-sm num_only" id="proforma_invoices_data[packing_forwarding]" name="proforma_invoices_data[packing_forwarding]" placeholder="Packing & Forwarding Amount" value="<?=(isset($proforma_invoices_data->packing_forwarding))? $proforma_invoices_data->packing_forwarding : ''; ?>">
												</div>
												<div class="col-sm-3" style="padding-left: 0px;">
													<input type="text" class="form-control input-sm" id="proforma_invoices_data[packing_forwarding_note]" name="proforma_invoices_data[packing_forwarding_note]" value="<?=(isset($proforma_invoices_data->packing_forwarding_note))? $proforma_invoices_data->packing_forwarding_note : ''; ?>">
												</div>
											</div>
											<div class="form-group">
												<label for="transit_insurance" class="col-sm-5 input-sm"> Transit Insurance </label>
												<div class="col-sm-4">
													<input type="text" class="form-control input-sm num_only" id="proforma_invoices_data[transit_insurance]" name="proforma_invoices_data[transit_insurance]" placeholder="Transit Insurance Amount" value="<?=(isset($proforma_invoices_data->transit_insurance))? $proforma_invoices_data->transit_insurance : ''; ?>">
												</div>
                                                <div class="col-sm-3" style="padding-left: 0px;">
													<input type="text" class="form-control input-sm" id="proforma_invoices_data[transit_insurance_note]" name="proforma_invoices_data[transit_insurance_note]" value="<?=(isset($proforma_invoices_data->transit_insurance_note))? $proforma_invoices_data->transit_insurance_note : ''; ?>">
												</div>
											</div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-5 input-sm"> Loading At </label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control input-sm" id="loading_at" name="proforma_invoices_data[loading_at]" placeholder="" value="<?=(isset($proforma_invoices_data->loading_at))? $proforma_invoices_data->loading_at : 'Factory-Rajkot'; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-5 input-sm"> Port of Loading</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control input-sm" placeholder='Port' id="port_of_loading_city" name="proforma_invoices_data[port_of_loading_city]" placeholder="" value="<?=(isset($proforma_invoices_data->port_of_loading_city))? $proforma_invoices_data->port_of_loading_city : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="delivery_through_id" class="col-sm-5 input-sm">Delivery Through</label>
                                                <div class="col-sm-7">
                                                    <select name="proforma_invoices_data[delivery_through_id]" class="delivery_through_id form-control input-sm select2" id="delivery_through_id" ></select>
                                                </div>
                                            </div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">CGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="freight_cgst"  name="proforma_invoices_data[freight_cgst]" value="<?=(isset($proforma_invoices_data->freight_cgst))? $proforma_invoices_data->freight_cgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">SGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="freight_sgst"  name="proforma_invoices_data[freight_sgst]" value="<?=(isset($proforma_invoices_data->freight_sgst))? $proforma_invoices_data->freight_sgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">IGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="freight_igst"  name="proforma_invoices_data[freight_igst]" value="<?=(isset($proforma_invoices_data->freight_igst))? $proforma_invoices_data->freight_igst : '0'; ?>" >
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">CGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="packing_forwarding_cgst"  name="proforma_invoices_data[packing_forwarding_cgst]" value="<?=(isset($proforma_invoices_data->packing_forwarding_cgst))? $proforma_invoices_data->packing_forwarding_cgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">SGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="packing_forwarding_sgst"  name="proforma_invoices_data[packing_forwarding_sgst]" value="<?=(isset($proforma_invoices_data->packing_forwarding_sgst))? $proforma_invoices_data->packing_forwarding_sgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">IGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="packing_forwarding_igst"  name="proforma_invoices_data[packing_forwarding_igst]" value="<?=(isset($proforma_invoices_data->packing_forwarding_igst))? $proforma_invoices_data->packing_forwarding_igst : '0'; ?>" >
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">CGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="transit_insurance_cgst"  name="proforma_invoices_data[transit_insurance_cgst]" value="<?=(isset($proforma_invoices_data->transit_insurance_cgst))? $proforma_invoices_data->transit_insurance_cgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">SGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="transit_insurance_sgst"  name="proforma_invoices_data[transit_insurance_sgst]" value="<?=(isset($proforma_invoices_data->transit_insurance_sgst))? $proforma_invoices_data->transit_insurance_sgst : '0'; ?>" >
													</div>
												</div>
												<div class="col-sm-4" style="padding-left: 0px;">
													<label for="" class="col-sm-5 input-sm">IGST %</label>
													<div class="col-sm-7" style="padding-left: 0px;">
														<input type="text" class="form-control input-sm " id="transit_insurance_igst"  name="proforma_invoices_data[transit_insurance_igst]" value="<?=(isset($proforma_invoices_data->transit_insurance_igst))? $proforma_invoices_data->transit_insurance_igst : '0'; ?>" >
													</div>
												</div>
											</div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Port of Discharge</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" placeholder='Port' id="port_of_discharge_city" name="proforma_invoices_data[port_of_discharge_city]" placeholder="" value="<?=(isset($proforma_invoices_data->port_of_discharge_city))? $proforma_invoices_data->port_of_discharge_city : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 input-sm"> Place of Delivery</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm" placeholder='Place' id="place_of_delivery_city" name="proforma_invoices_data[place_of_delivery_city]" placeholder="" value="<?=(isset($proforma_invoices_data->place_of_delivery_city))? $proforma_invoices_data->place_of_delivery_city : ''; ?>" required="">
                                                </div>
                                            </div>
										</div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Supplier Payment Terms </legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea id="supplier_payment_terms" class="form-control" style="height: 100px" name="proforma_invoices_data[supplier_payment_terms]">
                                                    <?=(isset($proforma_invoices_data->supplier_payment_terms))? $proforma_invoices_data->supplier_payment_terms : ''; ?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Terms And Condition</legend>
										<div class="col-md-12">
											<div class="form-group">
												<textarea id="terms_condition_purchase" class="form-control" style="height: 100px" name="proforma_invoices_data[terms_condition_purchase]">
													<?=(isset($proforma_invoices_data->terms_condition_purchase))? $proforma_invoices_data->terms_condition_purchase : ''; ?>
												</textarea>
											</div>
										</div>
									</fieldset>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=(isset($login_data->created_by_name))? $login_data->created_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=(isset($login_data->created_at))? $login_data->created_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=(isset($login_data->updated_by_name))? $login_data->updated_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=(isset($login_data->updated_at))? $login_data->updated_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Approved By</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_by" id="approved_by" value="<?=(isset($login_data->approved_by_name))? $login_data->approved_by_name : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control input-sm approved_date_time" id="approved_date_time" value="<?=(isset($login_data->approved_at))? $login_data->approved_at : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        </div>
        <section class="content-header">
            <h1> 
                <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ ?>
                <?php if(isset($proforma_invoices_data->isApproved) && $proforma_invoices_data->isApproved == 1){ ?>
                <?php if($this->applib->have_access_role(CAN_APPROVE_PROFORMA_INVOICE,"allow")){ ?>
                    <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove_ord" style="margin: 5px;" <?=($proforma_invoices_data->delete_not_allow == 1) ? ' Disabled ' : ''; ?>>Disapprove</button>
                <?php } else { ?>
                    <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                <?php } ?>
                <?php } else { ?>
                    <?php if($this->applib->have_access_role(CAN_APPROVE_PROFORMA_INVOICE,"allow")){ ?>
                        <button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">Approve</button>
                    <?php } else { ?>
                        <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                    <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
                    $proforma_invoices_view_role= $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"view");
                    $proforma_invoices_add_role= $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID,"add");
                ?>
                <?php if($proforma_invoices_add_role || $proforma_invoices_edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif;?>
                <?php if($proforma_invoices_view_role): ?>
				<a href="<?= base_url()?>proforma_invoices/proforma_invoices_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Proforma Invoice List</a>
                <?php endif;?>
                <?php if($proforma_invoices_add_role): ?>
				<a href="<?= base_url()?>proforma_invoices/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Domestic Proforma Invoice</a>
                <?php endif;?>
            </h1>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ } else { ?>
<div class="modal fade" id="select_quotation_modal" tabindex="-1" role="dialog" aria-labelledby="select_quotation_label" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h4 class="modal-title">Select Quotation
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <table id="quotation_datatable" class="table custom-table table-striped">
                    <thead>
                        <tr>
                            <th class="hidden">Quotation No.</th>
                            <th>Quotation No.</th>
                            <th>Revision</th>
                            <th>Quotation Date</th>
                            <th>Customer Name</th>
							<th>City</th>
							<th>State</th>
							<th>Country</th>
                        </tr>
                    </thead>
					<tfoot>
                        <tr>
                            <th class="hidden">Quotation No.</th>
                            <th>Quotation No.</th>
                            <th>Revision</th>
                            <th>Quotation Date</th>
                            <th>Customer Name</th>
							<th>City</th>
							<th>State</th>
							<th>Country</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?=dist_url('js/sales_order.js');?>"></script>
<script type="text/javascript">
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
	var lineitem_objectdata = [];
    <?php if(isset($proforma_invoices_lineitems)){ ?>
		var li_lineitem_objectdata = [<?php echo $proforma_invoices_lineitems; ?>];
		var lineitem_objectdata = [];
        if(li_lineitem_objectdata != ''){
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
	<?php } ?>
    display_lineitem_html(lineitem_objectdata);
    
    <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ ?>
        first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function(){
        $("#sales_order_pref_id").select2().each(function() {
        var selectedValue = $(this).val();
        $(this).html($("option", $(this)).sort(function(a, b) {
            return a.text < b.text ? -1 : 1
        }));
        $(this).val(selectedValue);
    });
        initAjaxSelect2($("#sales_id"),"<?=base_url('app/sales_select2_source')?>");
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source')?>");
        initAjaxSelect2($("#branch_id"),"<?=base_url('app/branch_select2_source')?>");
        <?php if(isset($proforma_invoices_data->branch_id)){ ?>
			setSelect2Value($("#branch_id"),"<?=base_url('app/set_branch_select2_val_by_id/'.$proforma_invoices_data->branch_id)?>");
        <?php } else { ?>
            setSelect2Value($("#branch_id"),"<?=base_url('app/set_branch_select2_val_by_id/'.DEFAULT_BRANCH_ID)?>");
        <?php } ?>
        initAjaxSelect2($("#agent_id"),"<?=base_url('app/agent_select2_source')?>");
        <?php if(isset($proforma_invoices_data->agent_id)){ ?>
			setSelect2Value($("#agent_id"),"<?=base_url('app/set_agent_select2_val_by_id/'.$proforma_invoices_data->agent_id)?>");
        <?php } ?>
        
        initAjaxSelect2($("#city"),"<?=base_url('app/city_select2_source')?>");
        initAjaxSelect2($("#state"),"<?=base_url('app/state_select2_source')?>");
        initAjaxSelect2($("#country"),"<?=base_url('app/country_select2_source')?>");
        initAjaxSelect2($("#item_id"),"<?=base_url('app/item_select2_source')?>");
        initAjaxSelect2($("#reference_id"),"<?=base_url('app/reference_select2_source')?>");
        initAjaxSelect2($("#item_extra_accessories_id"),"<?=base_url('app/item_extra_accessories_select2_source')?>");
        setSelect2Value($("#item_extra_accessories_id"),"<?=base_url('app/set_item_extra_accessories_select2_val_by_id/'.DEFAULT_ITEM_EXTRA_ACCESSORIES_ID)?>");
        initAjaxSelect2($("#delivery_through_id"),"<?=base_url('app/delivery_through_select2_source')?>");
        <?php if(isset($proforma_invoices_data->delivery_through_id)){ ?>
			setSelect2Value($("#delivery_through_id"),"<?=base_url('app/set_delivery_through_select2_val_by_id/'.$proforma_invoices_data->delivery_through_id)?>");
        <?php } ?>
        
        <?php if(isset($proforma_invoices_data->sales_to_party_id)){ ?>
			setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id/'.$proforma_invoices_data->sales_to_party_id)?>");
            party_details(<?=$proforma_invoices_data->sales_to_party_id; ?>);
		<?php } ?>
        
        var table
        table = $('#quotation_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('proforma_invoices/quotation_datatable_for_proforma_invoices_create')?>",
                "type": "POST",
				"data":function(d){
					d.proforma_invoices_for = 'domestic';
				}
            },
            "scrollY": 400,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columns": [
                { "class": "hidden" },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ]
        });
		
		CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g ); 
        CKEDITOR.replace('supplier_payment_terms',{
            removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        CKEDITOR.replace('terms_condition_purchase',{
            removePlugins:'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        CKEDITOR.replace('cancel_letter', {
            height: 120,
            removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        $('#select_quotation_modal').modal({backdrop: 'static', keyboard: false});
        $('#select_quotation_modal').modal('show');
        $('#select_quotation_modal').on('shown.bs.modal',function(){
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();	
        });
        
        $('#is_dispatched').on("change", function (e) {
            if($(this).val() == "<?=CANCELED_ORDER_ID;?>"){
                $(".cancel_reason_div").removeClass("hidden");
                $.ajax({
                    url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                    type: "POST",
                    data: {
                        module: 'domestic_proforma_invoice_cancel_order_letter'
                    },
                    dataType: 'json',
                    success: function(data) {
                        CKEDITOR.instances['cancel_letter'].setData(data.terms_and_conditions);
                    }
                });
            }else{
                $(".cancel_reason_div").addClass("hidden");
            }
        });
                
        $(document).on('click', '.quotation_row', function () {
            var tr = $(this).closest('tr');
            var quotation_id = $(this).data('quotation_id');
            $("#quotation_id").val(quotation_id);
            feedQuotationData(quotation_id);
            $('#select_quotation_modal').modal('hide');
            $('#sales_order_date').focus();
        });
    
        $(document).on('input','#item_rate',function () {
            if(this.value > 9900000){
                this.value = 9900000;
            }
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href"); // activated tab
            if (target == '#tab_1') {
                $('#item_id').select2('close');
                $('#sales_order_date').off("focus").datepicker("show");
            } else if (target == '#tab_2') {
                $('#item_id').select2('open');
                $('#sales_order_date').off("focus").datepicker("hide");
            } else {
                $('#item_id').select2('close');
                $('#sales_order_date').off("focus").datepicker("hide");
            }
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_proforma_invoices").submit();
                return false;
            }
        });
        
        $(document).on('submit', '#save_proforma_invoices', function () {
            
            if($($('#is_dispatched')).val() == "<?=CANCELED_ORDER_ID;?>"){
                if ($.trim($("#cancel_reason_text").val()) == '') {
                    show_notify("Please give reason for cancel order.", false);
                    $("#cancel_reason_text").focus();
                    return false;
                }
                if ($.trim($("#proof_of_cancellation").val()) == '') {
                    show_notify('Please Give Proof Of Cancellation.', false);
                    $("#proof_of_cancellation").focus();
                    return false;
                }
                if ($.trim($("#cancel_letter").val()) == '') {
                    show_notify('Please Type Cancel Letter.', false);
                    $("#cancel_letter").focus();
                    return false;
                }
            }
            
            if($.trim($("#party_id").val()) == ''){
                show_notify('Please Select Party.',false);
                return false;
            }
            if($.trim($("#reference_id").val()) == ''){
                show_notify('Please Select Reference.',false);
                return false;
            }
            if($.trim($("#item_id").val()) != ''){
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }
            
			if(lineitem_objectdata == ''){
				show_notify("Please select any one Item.", false);
				return false;
			}
            $('.module_save_btn').attr('disabled','disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
			var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
			postData.append('line_items_data', lineitem_objectdata_stringify);
			$.ajax({
				url: "<?=base_url('proforma_invoices/save_proforma_invoices') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
                    if(json.status == 0){
                        show_notify(json.msg,false);
                        $('.module_save_btn').removeAttr('disabled','disabled');
                        return false;
                    }
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
                        $('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('proforma_invoices/proforma_invoices_list') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('proforma_invoices/proforma_invoices_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
        });

        $(document).on("change",'#contact_person_id',function() {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?=base_url();?>party/get-contact-person-by-id/"+contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });
        
        $(document).on("change",'#currency_id',function(){
            if(edit_lineitem_inc == 0){
                var currency_id = $("#currency_id").val();
                $("#so_currency_id").val(currency_id);
                var item_id = $("#item_id").val();
                if(item_id != '' && item_id != null) {
                    $.ajax({
                        type: "POST",
                        url: '<?=base_url();?>sales/ajax_load_item_details/' + item_id,
                        data: id = 'item_id',
                        success: function (data) {
                            var json = $.parseJSON(data);
                            if (json['item_rate_in']) {
                                var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                                $("#item_rate").val(item_rate_in_json[currency_id]);
                            } else {
                                $("#item_rate").val('');
                            }
                            input_value_get_amount();
                        }
                    });
                }
            } else { edit_lineitem_inc = 0; }
        });
        
        $(document).on("click",".btn_approve_ord",function(){
            $("#ajax-loader").show();
            var proforma_invoice_id = $('#proforma_invoice_id').val();
            $.ajax({
                url: '<?=BASE_URL?>proforma_invoices/update_proforma_invoices_status_to_approved',
                type: "POST",
                data: {proforma_invoice_id:proforma_invoice_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        <?php if($this->applib->have_access_role(CAN_APPROVE_PROFORMA_INVOICE,"allow")){?>
                        $(".btn_approve_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_disapprove_ord' style='margin: 5px;'>Disapprove</button>");
                        <?php }else{?>
                        $(".btn_approve_ord").after("<small class='text-primary text-bold pull-right app_lab' style='margin: 5px;'>Approved</small>");
                        <?php }?>
                        $('.btn_approve_ord').remove();
                        show_notify(data.message, true);
                    }
                    else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on("click",".btn_disapprove_ord",function(){
            $("#ajax-loader").show();
            var proforma_invoice_id = $('#proforma_invoice_id').val();
            $.ajax({
                url: '<?=BASE_URL?>proforma_invoices/update_proforma_invoices_status_to_disapproved',
                type: "POST",
                data: {proforma_invoice_id:proforma_invoice_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $(".btn_disapprove_ord").after("<button type='button' class='btn btn-info btn-xs pull-right btn_approve_ord' style='margin: 5px;'>Approve</button>");
                        //$('.btn_approve_ord').show();
                        $('.btn_disapprove_ord').remove();
                        show_notify(data.message, true);
                    }
                    else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $('#add_lineitem').on('click', function() {
			var item_id = $("#item_id").val();
			if(item_id == '' || item_id == null){
				show_notify("Please select Item!", false);
				return false;
			}
			var quantity = $("#quantity").val();
			if(quantity == '' || quantity == null){
				show_notify("Item quantity is required!", false);
				return false;
			}
			var item_rate = $("#item_rate").val();
			if(item_rate == '' || item_rate == null){
				show_notify("Item Rate is required!", false);
				return false;
			}
			
			var key = '';
			var value = '';
			var lineitem = {};
			$('select[name^="line_items_data"]').each(function(e) {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			$('input[name^="line_items_data"]').each(function() {
				key = $(this).attr('name');
				key = key.replace("line_items_data[", "");
				key = key.replace("]", "");
				value = $(this).val();
				lineitem[key] = value;
			});
			var new_lineitem = JSON.parse(JSON.stringify(lineitem));
			var line_items_index = $("#line_items_index").val();
			if(line_items_index != ''){
				lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
			} else {
				lineitem_objectdata.push(new_lineitem);
			}
			display_lineitem_html(lineitem_objectdata);
			$('#lineitem_id').val('');
            $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
			$("#item_id").val(null).trigger("change");
			$("#item_extra_accessories_id").val(null).trigger("change");
            $("#quantity").val('1');
			$("#disc_per").val('0');
			$("#line_items_index").val('');
            if(on_save_add_edit_item == 1){
                on_save_add_edit_item == 0;
                $('#save_proforma_invoices').submit();
            }
            var sales_id = $("#sales_id").val();
            if(sales_id == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
            }
            if(sales_id == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
            }
		});
        
    });
    
    function feedQuotationData(quotation_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>proforma_invoices/get_quotation',
            type: "POST",
            async: false,
            data: {id: quotation_id},
            success: function(data){
                $("#ajax-loader").hide();
                json = JSON.parse(data);
                var quotation_data = json.quotation_data;
                setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id')?>/"+quotation_data.party_id);
                $('#party_name').val($('#party_id').val());
                $('#party_party_id').val(quotation_data.party_id);
                party_details($('#party_id').val());
                $('#quotation_no').val(quotation_data.quotation_no);
                $('#quotation_rev').val(quotation_data.rev);
                $('select[name="proforma_invoices_data[kind_attn_id]"]').val(quotation_data.kind_attn_id).change();
                if(json.inquiry_lineitems != ''){
                    lineitem_objectdata = json.quotation_lineitems;
                    display_lineitem_html(lineitem_objectdata);
                }
                if (quotation_data.party_type_1 != null){
                    $.ajax({
                        url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                        type: "POST",
						data: {party_type: quotation_data.party_type_1, module:'proforma_invoice_payment_terms'},
                        dataType: 'json',
                        success: function (data) {
                            CKEDITOR.instances['supplier_payment_terms'].setData( data.terms_and_conditions );
                        }
                    });
                    $.ajax({
                        url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                        type: "POST",
                        data: {party_type: quotation_data.party_type_1, module:'proforma_invoice_terms_and_conditions'},
                        dataType: 'json',
                        success: function (data) {
                            CKEDITOR.instances['terms_condition_purchase'].setData( data.terms_and_conditions );
                        }
                    });
                }
            },
        });
    }
    
    function display_lineitem_html(lineitem_objectdata){
		var new_lineitem_html = '';
		var qty_total = 0;
		var amount_total = 0;
		var disc_value_total = 0;
		var net_amount_total = 0;
//		console.log(lineitem_objectdata);
		$.each(lineitem_objectdata, function (index, value) {
            var value_currency;
			$.ajax({
				url: "<?=base_url('app/set_currency_select2_val_by_id/') ?>/" + value.currency_id,
				type: "POST",
				dataType: 'json',
				async: false,
				cache: false,
				success: function (response) {
					if(response.text == '--select--'){ response.text = ''; }
					value_currency = response.text;
				},
			});
            
			var lineitem_edit_btn = '';
			lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
			var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
			lineitem_edit_btn +
			' <a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a> ' +
			'</td>' +
			'<td>' + value.item_code + '</td>' +
			'<td>' + value.item_name + '</td>' +
            '<td class="text-right">' + value.quantity + '</td>';
            if(value_currency != ''){
                row_html += '<td class="text-right">' + value_currency + ' - ' + value.item_rate + '</td>';
            } else {
                row_html += '<td class="text-right">' + value.item_rate + '</td>';
            }
            row_html += '<td class="text-right">' + value.amount + '</td>' +
            '<td class="text-right">' + value.disc_value + '</td>' +
            '<td class="text-right">' + value.igst + '</td>' +
            '<td class="text-right">' + value.cgst + '</td>' +
            '<td class="text-right">' + value.sgst + '</td>' +
            '<td class="text-right">' + value.net_amount + '</td></tr>';
			new_lineitem_html += row_html;
			qty_total += +value.quantity;
			amount_total += +value.amount;
			disc_value_total += +value.disc_value;
			net_amount_total += +value.net_amount;
		});
		$('tfoot span.qty_total').html(qty_total); $('#qty_total').val(qty_total);
		$('tfoot span.amount_total').html(amount_total); $('#amount_total').val(amount_total);
		$('tfoot span.disc_value_total').html(disc_value_total); $('#disc_value_total').val(disc_value_total);
        $('tfoot span.net_amount_total').html(net_amount_total.toFixed(2)); $('#net_amount_total').val(net_amount_total.toFixed(2));
		$('tbody#lineitem_list').html(new_lineitem_html);
	}
                
    function edit_lineitem(index){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('.box-body .overlay').show();
		if(edit_lineitem_inc == 0){  edit_lineitem_inc = 1; /*$('.edit_lineitem_'+ index).click();*/ }
		value = lineitem_objectdata[index];
		$("#line_items_index").val(index);
		
		initAjaxSelect2($("#item_id"),"<?=base_url('app/item_select2_source/')?>");
		setSelect2Value($("#item_id"),"<?=base_url('app/set_li_item_select2_val_by_id/')?>/" + value.item_id);
		initAjaxSelect2($("#item_extra_accessories_id"),"<?=base_url('app/item_extra_accessories_select2_source/')?>");
        setSelect2Value($("#item_extra_accessories_id"),"<?=base_url('app/set_item_extra_accessories_select2_val_by_id/')?>/" + value.item_extra_accessories_id);
		initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source/')?>");
		setSelect2Value($("#currency_id"),"<?=base_url('app/set_currency_select2_val_by_id/')?>/" + value.currency_id);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#lineitem_id").val(value.id);
		}
        $("#item_code").val(value.item_code);
        $("#item_name").val(value.item_name);
        $("#item_description").val(value.item_description);
        $("#item_rate").val(value.item_rate);
        $("#quantity").val(value.quantity);
        $("#amount").val(value.amount);
        $("#disc_per").val(value.disc_per);
        $("#disc_value").val(value.disc_value);
        $("#igst").val(value.igst);
        $("#cgst").val(value.cgst);
        $("#sgst").val(value.sgst);
        $("#igst_amount").val(value.igst_amount);
        $("#cgst_amount").val(value.cgst_amount);
        $("#sgst_amount").val(value.sgst_amount);
        $("#net_amount").val(value.net_amount);
		$('.overlay').hide();
	}
	
	function remove_lineitem(index){
        if(confirm('Are you sure ?')){
            value = lineitem_objectdata[index];
            if(typeof(value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index,1);
            display_lineitem_html(lineitem_objectdata);
        }
	}
    
    function party_details(id){
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>sales/ajax_load_party_with_cnt_person/'+id,
            async: false,
            data: id='party_id',
            success: function(data){
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                }else{
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $(".party_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"),'<?=base_url()?>app/set_city_select2_val_by_id/'+json['city_id']);
                }else{
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"),'<?=base_url()?>app/set_state_select2_val_by_id/'+json['state_id']);
                }else{
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"),'<?=base_url()?>app/set_country_select2_val_by_id/'+json['country_id']);
                }else{
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                }else{
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                }else{
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                }else{
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                }else{
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                }else{
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"),'<?=base_url()?>app/set_reference_select2_val_by_id/'+json['reference_id']);
                }else{
                    setSelect2Value($("#reference_id"));
                }
                if(json['reference_description']){
                    $("#reference_description").html(json['reference_description']);
                }else{
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"),'<?=base_url()?>app/set_sales_select2_val_by_id/'+json['party_type_1_id']);
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_DOMESTIC_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=INR_CURRENCY_ID; ?>);
                    }
                    if(json['party_type_1_id'] == '<?=PARTY_TYPE_EXPORT_ID;?>'){
                        setSelect2Value($("#currency_id"),'<?=base_url()?>app/set_currency_select2_val_by_id/'+<?=USD_CURRENCY_ID; ?>);
                    }
                    var currency_title = $('#select2-currency_id-container').attr('title');
                    $('#received_payment_currency').html(currency_title);
                }else{
                    setSelect2Value($("#sales_id"));
                }

                <?php if(isset($proforma_invoices_data->id) && !empty($proforma_invoices_data->id)){ } else { ?>
                    if (json['branch_id']) {
                        setSelect2Value($("#branch_id"),"<?=base_url('app/set_branch_select2_val_by_id')?>/"+json['branch_id']);
                    }else{
                        setSelect2Value($("#branch_id"));
                    }

                    if (json['agent_id']) {
                        setSelect2Value($("#agent_id"),'<?=base_url()?>app/set_agent_select2_val_by_id/'+json['agent_id']);
                    }else{
                        setSelect2Value($("#agent_id"));
                    }
                <?php } ?>

                if(first_time_edit_mode == 1){
                    if (json['contact_persons_array']) {
                        var option_html = '';
                        if(json['contact_persons_array'].length > 0){
//                            console.log(json['contact_persons_array']);
                            $.each(json['contact_persons_array'],function(index,value){
                                option_html += "<option value='"+value.contact_person_id+"'>"+value.name+"</option>";
                            })
                            $('select[name="proforma_invoices_data[kind_attn_id]"]').html(option_html).select2();
                        } else {
                            $('select[name="proforma_invoices_data[kind_attn_id]"]').html('');
                        }
                        $('select[name="proforma_invoices_data[kind_attn_id]"]').change();
                    }
                } else { first_time_edit_mode = 1; }
                $("#ajax-loader").hide();
            }
        });
    }
    
    function item_details(id){
        if(edit_lineitem_inc == 0){
            if(id != '') {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>sales/ajax_load_item_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
    //                    console.log(data);
                        var json = $.parseJSON(data);
                        if (json['item_name']) {
                            $(".item_name").val(json['item_name']);
                        } else {
                            $(".item_name").val("");
                        }

                        if (json['item_code']) {
                            $(".item_code").val(json['item_code']);
                        } else {
                            $(".itm_code").val('');
                        }
                        
                        if (json['item_rate_in']) {
                            var currency_id = $("#currency_id").val();
                            var item_rate_in_json = $.parseJSON(json['item_rate_in']);
                            $(".item_rate").val(item_rate_in_json[currency_id]);
                        } else {
                            $(".item_rate").val('');
                        }
                        
                        $(".igst").val('0');
                        $(".cgst").val('0');
                        $(".sgst").val('0');
                        var state_id = $('#state').val();
                        if(state_id == null || state_id == '<?=GUJARAT_STATE_ID; ?>'){
                            if (json['cgst']) {
                                $(".cgst").val(json['cgst']);
                            } else {
                                $(".cgst").val('');
                            }
                            if (json['sgst']) {
                                $(".sgst").val(json['sgst']);
                            } else {
                                $(".sgst").val('');
                            }
                        } else {
                            if (json['igst']) {
                                $(".igst").val(json['igst']);
                            } else {
                                $(".igst").val('');
                            }
                        }
                        input_value_get_amount();
                    }
                });
            }
        }
    }
    
    <?php if (isset($_GET['view'])){ ?>
		$(window).load(function () {
			display_as_a_viewpage();
		});
    <?php } ?>
</script>