<?php $column_cnt = 8; ?>
<html>
	<head>
		<title>Sales Order</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:10px;
			}
			tr > td:last-child{
				border-right: 1px solid !important;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:12px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				/*width: 33.33%;*/
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #000000;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
	<?php
		$CURRENCY = "INR";
		if($proforma_invoices_data->currency != ''){
			$CURRENCY = strtoupper($proforma_invoices_data->currency);
		}
	?>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="<?=$column_cnt;?>">WORK ORDER</td>
			</tr>
			<tr>
                <td class="text-center text-bold" colspan="5"><b>Manufacturer & Supplier</b></td>
				<td class=" text-bold" colspan="1">Date : </td>
				<td class=" text-bold" colspan="2"><?=strtotime($proforma_invoices_data->proforma_invoices_date) != 0?date('d/m/Y',strtotime($proforma_invoices_data->proforma_invoices_date)):date('d/m/Y')?></td>
			</tr>
			<tr>
                <td class="text-bold" colspan="5" style="font-size:15px;"><b><?=$company_details['name'];?></b></td>
				<td class="text-bold" colspan="1">Work Order No. :</td>
				<td class="text-bold" colspan="2"><?=$this->applib->get_work_order_no($proforma_invoices_data->proforma_invoices_no);?></td>
				
			</tr>
			<tr>
				<td class="no-border-top text-bold" rowspan="5" colspan="5">
					<?=nl2br($company_details['address']);?><br />
                    City : <?=$company_details['city'];?> - <?=$company_details['pincode'];?> (<?=$company_details['state'];?>), Country : <?=$company_details['country'];?>.<br />
                    Email: <?=$company_details['email_id'];?><br />
                    Tel No. : <?=$company_details['contact_no'];?>,<br />
                    Contact No. : <?=$company_details['cell_no'];?>
                </td>
                <td class="text-bold" colspan="1">Salse Order No. :</td>
				<td class="text-bold" colspan="2"><?=$this->applib->get_proforma_invoices_no($proforma_invoices_data->proforma_invoices_no);?></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="1">Quotation No. : </td>
				<td class="text-bold" colspan="2"><?=$this->applib->get_quotation_ref_no($proforma_invoices_data->quotation_no,$proforma_invoices_item['item_code']);?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="1">Purchase Order No. :</td>
				<td class=" text-bold" colspan="2">
					<?php 
						if(!empty($proforma_invoices_data->cust_po_no)){
							echo $proforma_invoices_data->cust_po_no;
						} else { 
							echo $proforma_invoices_data->sales_order_pref;
						} 
					?>
				</td>
           </tr>
			<tr>
				<td class=" text-bold" colspan="1">Sales Executive :</td>
				<td class=" text-bold" colspan="2"><?=$proforma_invoices_data->created_name?></td>
			</tr>
			<tr>
                <td class=" text-bold" colspan="1">Validity of Order : </td>
				<td class=" text-bold" colspan="2"><?=$proforma_invoices_data->proforma_invoices_validaty?> Days</td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="8"><b>Supplier Terms</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Transportation By : </td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->transportation_by;?></td>
				<td class="text-center"><?=$proforma_invoices_data->transport_amount;?></td>
				<td class=" text-bold" colspan="1">Foundation Drawing Provide</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->foundation_drawing_required;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Road Insurance By</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->road_insurance_by;?></td>
				<td class="text-center"><?=$proforma_invoices_data->insurance_amount;?></td>
				<td class=" text-bold" colspan="1">Installation & Commissioning</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->erection_commissioning;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Oil Barrel (Drum 210 Liter)</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->oil_barrel;?></td>
				<td class="text-center"><?=$proforma_invoices_data->oil_barrel_amount;?></td>
				<td class=" text-bold" colspan="1">Loading By : </td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->loading_by;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Third party inspection cost</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->third_party_inspection_cost;?></td>
				<td class="text-center"><?=$proforma_invoices_data->third_party_inspection_cost_amount;?></td>
				<td class=" text-bold" colspan="1">Unloading By</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->unloading_by;?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Machine To All Motor Cable</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->machine_to_all_motor_cable;?></td>
				<td class="text-center"><?=$proforma_invoices_data->machine_to_all_motor_cable_amount;?></td>
				<td class=" text-bold" colspan="1">Inspection Required</td>
				<td class="text-center" colspan="2"><?=$proforma_invoices_data->inspection_required;?></td>
			</tr>
			<tr>
				<td class="text-center text-bold width-50-pr" colspan="5"><b>Purchaser</b></td>
                <td class="text-center text-bold width-50-pr" colspan="3"><b>Mode Of Shipment</b></td>
			</tr>
			<tr>
                <td class="text-bold" colspan="5" style="font-size:15px;"><b><?=$proforma_invoices_data->party_name?></b></td>
				<td class=" text-bold" colspan="3">Name : <?=$proforma_invoices_data->mode_of_shipment_name?>, Truck No: <?=$proforma_invoices_data->mode_of_shipment_truck_number?></td>
			</tr>
			<tr>
				<td class="no-border-top text-bold" rowspan="3" colspan="5">&nbsp;</td>
				<td class="text-center text-bold" colspan="3"><b>Delivery Schedule</b></td>
			</tr>
			<tr>
				<td class=" text-bold" width="24%"><b>Minimum</b></td>
				<td class="text-center text-bold" width="13%"><?=$proforma_invoices_data->mach_deli_min_weeks;?> Days</td>
				<td class="text-center text-bold" width="13%">
					<?php
						if($proforma_invoices_data->mach_deli_min_weeks != 0){
							echo strtotime($proforma_invoices_data->mach_deli_min_weeks_date) != 0?date('d/m/Y',strtotime($proforma_invoices_data->mach_deli_min_weeks_date)):date('d/m/Y');
						}
					?>
				</td>
			</tr>
			<tr>
                <td class=" text-bold"><b>Maximum</b></td>
				<td class="text-center text-bold"><?=$proforma_invoices_data->mach_deli_max_weeks;?> Days</td>
				<td class="text-center text-bold">
					<?php 
						if($proforma_invoices_data->mach_deli_max_weeks != 0){
							echo strtotime($proforma_invoices_data->mach_deli_max_weeks_date) != 0?date('d/m/Y',strtotime($proforma_invoices_data->mach_deli_max_weeks_date)):date('d/m/Y');
						}
					?>
				</td>
			</tr>
			<tr>
				<td class="text-center text-bold" width="9%" colspan="1"><b>Sr.No.</b></td>
				<td class="text-center text-bold" width="18%" colspan="1"><b>Item Code</b></td>
				<td class="text-center text-bold" colspan="4"><b>Item Name</b></td>
				<td class="text-center text-bold" colspan="1"><b>HSN</b></td>
				<td class="text-center text-bold" colspan="1"><b>Qty</b></td>
			</tr>
            
            <?php
                $sub_total = 0;
				$discount_total = 0;
				$second_sub_total = 0;
				$gst_total = 0;
				$round_off = 0;
				$grand_total = 0;
				if (!empty($proforma_invoices_items)) {
					$i_inc = 1;
					foreach ($proforma_invoices_items as $key => $item_row) {
			?>
						<tr>
							<td class="text-center" colspan="1" ><?=$i_inc?></td>
							<td class="text-left" colspan="1" ><?=$item_row['item_code'];?></td>
							<td class="text-left" colspan="4" ><?=$item_row['item_name'];?></td>
							<td class="text-center" colspan="1" ><?=$item_row['hsn'];?></td>
							<td class="text-center" colspan="1" ><?=$item_row['quantity'].' '.$item_row['uom'];?></td>
						</tr>
			<?php
						$i_inc++;
						$discount_total += $item_row['disc_value'];
						$sub_total += ($item_row['quantity'] * $item_row['rate']);
						$gst_total += ($item_row['cgst_amount'] + $item_row['sgst_amount'] + $item_row['igst_amount']);
					}
				}
				$second_sub_total = $sub_total - $discount_total;
				$grand_total = $second_sub_total + $gst_total + $round_off;
			?>
			<tr>
				<td valign='top' class="text-bold" colspan="8"><b>Note :</b> <?=nl2br($proforma_invoices_data->work_order_notes);?></td>
			</tr>
            <tr>
				<td colspan="<?=$column_cnt;?>">
					<table class="no-border">
						<tr class="no-border">
							<td align="center" class="no-border-top no-border-left footer-detail-area">Work Order Received</td>
							<td align="center" class="no-border-top footer-detail-area">Order Received</td>
							<td align="center" class="no-border-top footer-detail-area">Order Aproved</td>
							<td align="center" class="no-border-top footer-detail-area">For, <?=$company_details['name'];?></td>
						</tr>
						<tr class="no-border">
							<td class="footer-sign-area text-bold no-border-left no-border-top no-border-bottom">
                                Production Manager Sign
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
                                <?=$proforma_invoices_data->sales_executive_name?><br />
								Sales Executive Sign
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
								<?=$company_details['ceo_name'];?><br/>
								Authorized Sign
							</td>
							<td class="footer-sign-area text-bold no-border-top no-border-bottom">
								<?=$company_details['m_d_name'];?><br/>
								Managing Director
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<i class="fa fa-angle-down"></i>
