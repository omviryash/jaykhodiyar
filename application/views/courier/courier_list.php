<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class='text-primary text-bold'>Courier List</small>
            <?php if ($this->applib->have_access_role(COURIER_MENU_ID, "add")) { ?>
                <a href="<?= base_url() ?>courier/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Courier</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="row">
            <div style="margin: 15px;">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="row">
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <table id="courier_list" class="table custom-table agent-table" width="100%">
                                        <thead>
                                            <tr>      		 
                                                <th>Action</th>
                                                <th>Party</th>
                                                <th>Address Type</th>
                                                <th>Courier Name</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Docket No</th>
                                                <th>Created at</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#courier_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('courier/courier_list_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                }
            },
            "scroller": {
                "loadingIndicator": true,
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "deferRender": true,
            "scrollY": 500,
        });
        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: '',
                    success: function (data) {
                        table.draw();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });
    });
</script>
