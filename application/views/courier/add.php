<div class="content-wrapper">
    <style>
        .select2-container--open .select2-dropdown--below { border: 1px solid #2b2d3f; top: 0px; left: -1px; }
    </style>
    <form class="form-horizontal" action="<?= base_url('courier/save_courier') ?>" method="post" id="save_courier" novalidate>
        <?php if(isset($courier_id) && !empty($courier_id)) { ?>
            <input type="hidden" class="form-control input-sm" id="courier_id" name="courier_id" value="<?= $courier_id; ?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Courier</small>
                <?php
                    $view_role = $this->applib->have_access_role(COURIER_MENU_ID, "view");
                    $add_role = $this->applib->have_access_role(COURIER_MENU_ID, "add");
                ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" id="save_courier" style="margin: 5px;">Save</button>
                <?php if ($view_role): ?>
                    <a href="<?= base_url() ?>courier/courier_list" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Courier List</a>
                <?php endif; ?>
                <?php if ($add_role): ?>
                    <a href="<?= BASE_URL ?>courier/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add Courier</a>
                <?php endif; ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Courier Details</a></li>   
                        <?php if (isset($courier_id) && !empty($courier_id)) { ?>
                            <li class=""><a href="#tab_2" data-toggle="tab">Login Details</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <!-- Horizontal Form -->
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> Courier Details</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="party_id" class="col-md-3 input-sm">Party</label>
                                                    <div class="col-md-9">  
                                                        <select name="party_id" id="party_id" class="form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="address_type" class="col-md-3 input-sm">Address Type</label>
                                                    <div class="col-md-9">  
                                                        <select name="address_type" id="address_type" class="form-control input-sm select2">
                                                            <option value=""> - Select Address Type - </option>
                                                            <option value="1" <?php echo (isset($address_type) && $address_type == '1') ? ' Selected ' : ''; ?>>Party Details Address</option>
                                                            <option value="2" <?php echo (isset($address_type) && $address_type == '2') ? ' Selected ' : ''; ?>>Delivery Address</option>
                                                            <option value="3" <?php echo (isset($address_type) && $address_type == '3') ? ' Selected ' : ''; ?>>Communication Address</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="courier_name" class="col-md-3 input-sm pull-left">Courier Name<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="courier_name" id="courier_name" class="form-control input-sm" value="<?php echo (isset($courier_name) && !empty($courier_name)) ? $courier_name : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="datepicker2" class="col-md-3 input-sm pull-left">Date<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="courier_date" id="datepicker2" class="form-control input-sm pull-left" value="<?php echo (isset($courier_date) && !empty($courier_date)) ? date('d-m-Y', strtotime($courier_date)) : date('d-m-Y'); ?>" style="width:100px;" >
                                                        <div class="pull-left col-md-7" style="width:100px;">
                                                            <div class="bootstrap-timepicker" style="position:initial;">
                                                                <input type="text" name="courier_time" id="courier_time" class="form-control input-sm pull-left timepicker" value="<?php echo (isset($courier_time) && !empty($courier_time)) ? $courier_time : date('H:i:s'); ?>" style="width:100px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="docket_no" class="col-md-3 input-sm pull-left">Docket No.</label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="docket_no" id="docket_no" class="form-control input-sm" value="<?php echo (isset($docket_no) && !empty($docket_no)) ? $docket_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="parcel_details" class="col-md-3 input-sm pull-left">Parcel Details</label>
                                                    <div class="col-md-9">  
                                                        <textarea type="text" name="parcel_details" id="parcel_details" class="form-control" rows="2"><?php echo (isset($parcel_details) && !empty($parcel_details)) ? $parcel_details : ''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="party_name" class="col-md-3 input-sm">Party Name</label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="party_name" id="party_name" class="form-control input-sm" value="<?php echo (isset($party_name) && !empty($party_name)) ? $party_name : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="party_address" class="col-md-3 input-sm">Party Address</label>
                                                    <div class="col-md-9">  
                                                        <textarea type="text" name="party_address" id="party_address" class="form-control" rows="2"><?php echo (isset($party_address) && !empty($party_address)) ? $party_address : ''; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="party_address" class="col-md-3 input-sm">Party Contact No</label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="party_contact_no" id="party_contact_no" class="form-control input-sm num_only" value="<?php echo (isset($party_contact_no) && !empty($party_contact_no)) ? $party_contact_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="contact_person_name" class="col-md-3 input-sm">Contact Person</label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="contact_person_name" id="contact_person_name" class="form-control input-sm" value="<?php echo (isset($contact_person_name) && !empty($contact_person_name)) ? $contact_person_name : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="contact_person_no" class="col-md-3 input-sm">Contact Person No</label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="contact_person_no" id="contact_person_name" class="form-control input-sm num_only" value="<?php echo (isset($contact_person_no) && !empty($contact_person_no)) ? $contact_person_no : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>										
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            <?php if (isset($courier_id) && !empty($courier_id)) { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Created By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?= (isset($created_by_name)) ? $created_by_name : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?= (isset($created_at)) ? $created_at : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?= (isset($updated_by_name)) ? $updated_by_name : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?= (isset($updated_at)) ? $updated_at : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#address_type').select2().each(function() {
            var selectedValue = $(this).val();
            $(this).html($("option", $(this)).sort(function(a, b) {
                return a.text < b.text ? -1 : 1
            }));
            $(this).val(selectedValue);
        });
        initAjaxSelect2($("#party_id"), "<?= base_url('app/party_select2_source') ?>");
        <?php if (isset($party_id)) { ?>
            setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id/' . $party_id) ?>");
        <?php } ?>
        
        $(document).on('submit', '#save_courier', function () {
            if ($.trim($("#party_id").val()) == '' && $.trim($("#party_name").val()) == '') {
                show_notify('Please Select Party OR Enter Party Name', false);
                return false;
            }
            if ($.trim($("#party_id").val()) != '') {
                if ($.trim($("#address_type").val()) == '') {
                    show_notify('Please Select Address Type.', false);
                    return false;
                }
            }
            if ($.trim($("#party_name").val()) != '') {
                if ($.trim($("#party_address").val()) == '') {
                    show_notify('Please Enter Party Address.', false);
                    return false;
                }
            }
            if ($.trim($("#courier_name").val()) == '') {
                show_notify('Please Enter Courier Name.', false);
                return false;
            }
            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('courier/save_courier') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('courier/courier_list') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('courier/courier_list') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });


    });
</script>