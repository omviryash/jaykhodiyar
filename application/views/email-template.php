<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Reset your mychampp password</title>
<style type="text/css">
body{
	margin: 0;
}
</style>
</head>
<body>
<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#e1e8ed" style="background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px">
<tbody>
<tr>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px">
<table align="center" width="500" style="width:500px;padding:0;margin:0;line-height:1px;font-size:1px" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr align="center">
<td style="min-width:500px;height:1px;padding:0;margin:0;line-height:1px;font-size:1px"> <img src="<?=base_url();?>images/logo_small.png" style="max-width:200px;min-width:100px;min-height:1px;margin:0;padding:0;display:block;border:none;outline:none" class="CToWUd"> </td>
</tr>
</tbody>
</table> </td>
</tr>
<tr>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px">
<table align="center" width="500" style="width:500px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td height="15" style="height:15px;padding:0;margin:0;line-height:1px;font-size:1px"> &nbsp; </td>
</tr>
</tbody>
</table>

<table align="center" width="500" style="width:500px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td colspan="2" height="1" style="line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
</tbody>
</table>

<table align="center" width="500" style="width:500px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td width="50" style="width:50px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0;line-height:1px;font-size:1px">
<tbody>
<tr>
<td height="30" style="height:45px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
<tr>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:22px;line-height:30px;font-weight:300;color:#292f33">Forgot your password ?</td>
</tr>
<tr>
<td height="30" align="center" style="height:30px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
<tr>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;line-height:22px;font-weight:400;color:#292f33">To reset your password, Click on Reset Password button below.</td>
</tr>
<tr>
<td height="25" style="height:25px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>

<tr>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px">
<table border="0" cellspacing="0" cellpadding="0" style="padding:0;margin:0;line-height:1px;font-size:1px">
<tbody>
<tr>
<td style="padding:0;margin:0;line-height:1px;font-size:1px">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:0;margin:0;line-height:1px;font-size:1px">
<tbody>
<tr>
<td style="padding:0;margin:0;line-height:1px;font-size:1px">
<table border="0" cellspacing="0" cellpadding="0" style="padding:0;margin:0;line-height:1px;font-size:1px">
<tbody>
<tr>
<td align="center" bgcolor="#55acee" style="padding:0;margin:0;line-height:1px;font-size:1px;border-radius:4px;line-height:18px"><a href="<?=base_url();?>chemist_change_password.php?u=<?php echo $unique_code?>" style="text-decoration:none;border-style:none;border:0;padding:0;margin:0;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;line-height:22px;font-weight:500;color:#ffffff;text-align:center;text-decoration:none;border-radius:4px;padding:11px 30px;border:1px solid #55acee;display:inline-block" target="_blank">
 <strong>Reset password</strong>
 </a></td>
</tr>
</tbody>
</table> </td>
</tr>
</tbody>
</table> </td>
</tr>
</tbody>
</table> </td>
</tr>

<tr>
<td height="55" style="height:55px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
</tbody>
</table> </td>
<td width="50" style="width:50px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
</tbody>
</table>

<table align="center" width="500" style="width:500px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td colspan="2" height="1" style="line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
</tbody>
</table>
<table align="center" width="500" style="width:500px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td width="50" style="width:50px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px">
 </td>
<td width="50" style="width:50px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
<tr>
<td width="50" style="width:50px;padding:0;margin:0;line-height:1px;font-size:1px"></td>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px">
<table align="center" width="500" style="width:500px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td height="1" style="line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
<tr>
<td height="20" style="height:20;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
<tr>
<td align="center" style="padding:0;margin:0;line-height:1px;font-size:1px"> <span> <a style="text-decoration:none;border-style:none;border:0;padding:0;margin:0;font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;color:#8899a6;font-size:12px;padding:0px;margin:0px;font-weight:normal;line-height:12px">MyChampp - My Choice to Help in Adding My Product Purchase</a> </span> </td>
</tr>
<tr>
<td height="26" style="height:26;padding:0;margin:0;line-height:1px;font-size:1px"></td>
</tr>
</tbody>
</table>
 </td>
</tr>
</tbody>
</table>

</body>
