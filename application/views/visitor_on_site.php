<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Visitor On Site</small>
        </h1>
    </section>
    <section class="content">
        <div class="box box-info" >
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="btn pull-left btn-info text_white" style="margin: 5px;margin-right: 5px;">All</a>
                        <a href="#" class="btn pull-left btn-success" style="margin: 5px;margin-right: 5px;">Chat On Site</a>
                        <a href="#" class="btn pull-left btn-warning" style="margin: 5px;margin-right: 5px;">Mobile SDK</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h3>www.jaykhodiyar.in</h3>
                        <p><i class="fa fa-bar-chart"></i> Statics</p>
                        <div class="col-md-2">
                            <i class="fa fa-circle pull-right" style="color: green;"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sales</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle pull-right" style="color: green;"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sales Support</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle-o pull-right"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sanjay</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle-o pull-right"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sanjay Tilala</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle pull-right" style="color: green;"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Support</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle-o pull-right"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Support</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-6">
                        <!-- LINE CHART -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h5><i class="fa fa-circle" style="color: green;"></i> Accepted Chats &nbsp; &nbsp; &nbsp;
                                    <i class="fa fa-circle"  style="color: red;"></i> Missed Chats &nbsp; &nbsp; &nbsp;
                                    <i class="fa fa-envelope"></i> 0 &nbsp; &nbsp; &nbsp;
                                    <i class="fa fa-envelope"></i> 0 &nbsp; &nbsp; &nbsp;
                                </h5>
                            </div>
                            <div class="box-body chart-responsive">
                                <div class="chart" id="line-chart" style="height: 300px;"></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h3>www.jaykhodiyar.in</h3>
                        <p><i class="fa fa-bar-chart"></i> Statics</p>
                        <div class="col-md-2">
                            <i class="fa fa-circle pull-right" style="color: green;"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sales</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle pull-right" style="color: green;"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sales Support</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle-o pull-right"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sanjay</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle-o pull-right"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Sanjay Tilala</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle pull-right" style="color: green;"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Support</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-circle-o pull-right"></i>
                            <img height="60px" width="60px"  src="<?= base_url('resource/image/rsz_jk_logo.png'); ?>" style="border-radius: 50%;"/>
                            <p align="center">Support</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-6">
                        <!-- LINE CHART -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h5><i class="fa fa-circle" style="color: green;"></i> Accepted Chats &nbsp; &nbsp; &nbsp;
                                    <i class="fa fa-circle"  style="color: red;"></i> Missed Chats &nbsp; &nbsp; &nbsp;
                                    <i class="fa fa-envelope"></i> 0 &nbsp; &nbsp; &nbsp;
                                    <i class="fa fa-envelope"></i> 0 &nbsp; &nbsp; &nbsp;
                                </h5>
                            </div>
                            <div class="box-body chart-responsive">
                                <div class="chart" id="line-chart2" style="height: 300px;"></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?= base_url('resource/bootstrap/js/raphael-min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('resource/bootstrap/js/morris.min.js'); ?>"></script>
<script>
    $(function () {
        "use strict";

        var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: [
                {y: '2011 Q5', item1: 2666},
                {y: '2011 Q2', item1: 2778},
                {y: '2011 Q3', item1: 4912},
                {y: '2011 Q4', item1: 3767},
                {y: '2012 Q1', item1: 6810},
                {y: '2012 Q2', item1: 5670},
                {y: '2012 Q3', item1: 4820},
                {y: '2012 Q4', item1: 15073},
                {y: '2013 Q1', item1: 10687},
                {y: '2013 Q2', item1: 8432}
            ],
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Item 1'],
            lineColors: ['#3c8dbc'],
            hideHover: 'auto'
        });
        var line = new Morris.Line({
            element: 'line-chart2',
            resize: true,
            data: [
                {y: '2011 Q5', item1: 2666},
                {y: '2011 Q2', item1: 2778},
                {y: '2011 Q3', item1: 4912},
                {y: '2011 Q4', item1: 3767},
                {y: '2012 Q1', item1: 6810},
                {y: '2012 Q2', item1: 5670},
                {y: '2012 Q3', item1: 4820},
                {y: '2012 Q4', item1: 15073},
                {y: '2013 Q1', item1: 10687},
                {y: '2013 Q2', item1: 8432}
            ],
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Item 1'],
            lineColors: ['#3c8dbc'],
            hideHover: 'auto'
        });
    });
</script>

