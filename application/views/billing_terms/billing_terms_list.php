<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Billing Terms List</small>
            <?php
            $access_add  = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "add");
            ?>
            <?php if($access_add):?>
            <a href="<?=base_url('master/billing_terms')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
            <?php endif;?>
            
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin: 10px;">
                                <div class="col-md-12">
                                    <table id="billing_terms_datatable" class="table custom-table agent-table" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="panel-heading">
        <?php if($access_add):?>
        <a href="<?=base_url('master/billing_terms')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
        <?php endif;?>
    </div>
</div>

<script>
    var table;
    $(document).ready(function(){

        table = $('#billing_terms_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?=base_url()?>master/billing_terms_datatable",
                "type": "POST"
            },
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
        $(document).on("click",".delete_button",function(){
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if(value){
                $.ajax({
                    url: $(this).data('href'),
                    type: "GET",
                    data: null,
                    success: function(data){
                        show_notify("Billings terms deleted successfully!",true);
                        table.draw();
                    }
                });
            }
        });
    });
</script>
