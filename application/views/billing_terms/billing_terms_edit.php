<?php if($this->session->flashdata('success') == true){?>
<div class="col-sm-4 pull-right">
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
    </div>
</div>
<?php }?>
<div class="content-wrapper">
    <form id="frm_billing_terms">
        <input type="hidden" name="billing_terms_id" class="billing_terms_id" id="billing_terms_id" value="<?=$billing_terms_data['id']?>">
        <!-- Content Header (Page header) -->
        <section class="custom content-header">
            <h1>
                <small class="text-primary text-bold">Master : Billing Terms</small>
                <?php
                $access_edit  = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "edit");
                $access_view  = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "view");
                $access_add  = $this->app_model->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "add");
                ?>
                
                <?php if($access_edit):?>
                <button type="submit" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Save Billing Terms</button>
                <?php endif;?>
                
                <?php if($access_view):?>
                <a href="<?=BASE_URL?>master/billing_terms_list" class="btn btn-info btn-xs pull-right" style="margin: 5px;">List Billing Terms</a>
                <?php endif;?>
                
                <?php if($access_add):?>
                <a href="<?=BASE_URL?>master/billing_terms/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Billing Terms</a>
                <?php endif;?>
            </h1>
        </section>
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Name<span class="required-sign">*</span></label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <input type="text" class="form-control input-sm billing_terms_name" name="billing_terms_name" id="billing_terms_name" value="<?=$billing_terms_data['billing_terms_name']?>">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Date</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <input type="text" class="form-control input-sm input-datepicker billing_terms_date" name="billing_terms_date" id="billing_terms_date" value="<?=date('d-m-Y',strtotime($billing_terms_data['billing_terms_date']));?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <br/>
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-10">
                                        <table class="table custom-table" id="terms_detail_table">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Cal Code</th>
                                                    <th>Cal Narration</th>
                                                    <th>Percentage</th>
                                                    <th>Value</th>
                                                    <th>GL A/c</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(!empty($billing_terms_details)):?>
                                                <?php foreach($billing_terms_details as $row):?>
                                                <tr>
                                                    <td>
                                                        <a class="btn btn-xs btn-danger btn_delete_terms_detail" href="javascript:void(0);" data-terms_detail_id="<?=$row->id?>"><i class="fa fa-remove"></i></a>
                                                        <a class="btn btn-xs btn-primary btn_edit_terms_detail" href="javascript:void(0);" data-terms_detail_id="<?=$row->id?>"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                    <td><?=$row->cal_code?></td>
                                                    <td><?=$row->narration?></td>
                                                    <td><?=$row->percentage?></td>
                                                    <td><?=$row->value?></td>
                                                    <td><?=$row->gl_acc?></td>
                                                </tr>
                                                <?php endforeach;?>
                                                <?php endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <input type="hidden" class="terms_detail_data terms_detail_id" name="terms_detail_id" id="terms_detail_id" value="0">
                                <div class="row">
                                    <br/>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Cal Code<span class="required-sign">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control terms_detail_data input-sm cal_code" name="cal_code" id="cal_code">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button" class="btn btn-block btn-primary btn-xs btn_add_terms_detail">Add</button>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Description</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <textarea name="narration" class="form-control terms_detail_data narration" id="narration" rows="2"></textarea>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Percentage</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <input type="text" class="form-control terms_detail_data input-sm percentage" name="percentage" id="percentage">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Value</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <input type="text" class="form-control terms_detail_data input-sm value" name="value" id="value">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">GL A/c</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <input type="text" class="form-control terms_detail_data input-sm gl_acc" name="gl_acc" id="gl_acc">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 input-sm">Cal Definition</label>
                                                <div class="col-sm-9 dispaly-flex">
                                                    <input type="text" class="form-control input-sm cal_definition_label disabled" id="cal_definition_label" readonly="readonly">
                                                    <input type="hidden" class="form-control terms_detail_data input-sm cal_definition" name="cal_definition" id="cal_definition">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Cal Definition</legend>
                                            <div class="cal_defi_section">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-2 input-sm">Select Code</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control input-sm first-select-code billing_terms_cal_codes cal_definition_data">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="cal_defi_part hidden">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-2 input-sm">Select Code</label>
                                                        <div class="col-sm-2">
                                                            <select class="form-control input-sm cal_definition_data cal_operation">
                                                                <option value="+">+ (Addition)</option>
                                                                <option value="-">- (Subtract)</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <select class="form-control input-sm billing_terms_cal_codes cal_definition_data">
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button type="button" class="btn btn-danger btn-xs btn_remove_code"><i class="fa fa-trash"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-4">
                                                <button type="button" class="btn btn-info btn-xs btn_add_more_code">Add more code</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.col -->
        <section class="custom content-header">
            <?php if($access_edit):?>
            <button type="submit" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Save Billing Terms</button>
            <?php endif;?>

            <?php if($access_view):?>
            <a href="<?=BASE_URL?>master/billing_terms_list" class="btn btn-info btn-xs pull-right" style="margin: 5px;">List Billing Terms</a>
            <?php endif;?>

            <?php if($access_add):?>
            <a href="<?=BASE_URL?>master/billing_terms/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Billing Terms</a>
            <?php endif;?>
        </section>
    </form>
</div>
<script>
    var ADD_ITEM = 0;
    var EDIT_ITEM = 0;
    var BILLING_TERMS_ID = '<?=$billing_terms_data['id']?>';
    var cnt = 1;
    $(document).ready(function() {

        initAjaxSelect2($('select.first-select-code'),"<?=base_url('app/billing_terms_cal_codes_select2_source')?>/"+BILLING_TERMS_ID);

        $(document).on("click", ".btn_add_more_code", function () {
            var cal_defi_part = $(".cal_defi_part.hidden").clone();
            cal_defi_part.removeClass('hidden');
            cal_defi_part.find("select.billing_terms_cal_codes").attr("id","select_billing_terms_cal_codes_"+cnt);
            $(".cal_defi_section").append(cal_defi_part);
            initAjaxSelect2($("#select_billing_terms_cal_codes_"+cnt),"<?=base_url('app/billing_terms_cal_codes_select2_source')?>/"+BILLING_TERMS_ID);
            cnt++;
        });

        $(document).on("click", ".btn_remove_code", function () {
            var confirm_value = confirm("Are you sure ?");
            if (confirm_value) {
                $(this).closest('.cal_defi_part').fadeOut(function () {
                    $(this).remove();
                    set_cal_definition();
                });
            }
        });

        $(document).on("change", ".cal_definition_data", function () {
            set_cal_definition();
        });

        $(document).on("input", ".cal_code", function () {
            set_cal_definition();
        });

        $(document).on("change", ".first-select-code", function () {
            set_cal_definition();
        });

        $(document).on("click", ".btn_add_terms_detail", function () {
            if ($("#cal_code").val() == '') {
                show_notify('Cal Code field is required!');
                return false;
            }
            if ($("#narration").val() == '') {
                show_notify('Cal Description field is required!');
                return false;
            }

            if (BILLING_TERMS_ID == 0) {
                ADD_ITEM = 1;
                $("#frm_billing_terms").submit();
                return false;
            }
            var terms_detail_data = new FormData();
            $('.terms_detail_data').each(function() {
                terms_detail_data.append($(this).attr('name'),$(this).val());
            });

            terms_detail_data.append('cal_code_definition[0][cal_code_id]',$('select.first-select-code').val());
            var ctn_cal_code_definition = 1;
            $(".cal_defi_section").find(".cal_defi_part").each(function (index, value) {
                terms_detail_data.append('cal_code_definition['+ctn_cal_code_definition+'][cal_operation]',$(this).find('.cal_operation').val());
                terms_detail_data.append('cal_code_definition['+ctn_cal_code_definition+'][cal_code_id]',$(this).find('.billing_terms_cal_codes').val());
                ctn_cal_code_definition++;
            });

            if ($("#terms_detail_id").val() == 0) {
                $.ajax({
                    url: "<?=base_url()?>master/add_billing_terms_detail/"+BILLING_TERMS_ID,
                    type: "POST",
                    data: terms_detail_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success == true) {
                            var terms_detail_id = data.terms_detail_id;
                            var row_html ='<tr><td>' +
                                '<a class="btn btn-xs btn-danger btn_delete_terms_detail" href="javascript:void(0);" data-terms_detail_id="'+terms_detail_id+'"><i class="fa fa-remove"></i></a>' +
                                ' <a class="btn btn-xs btn-primary btn_edit_terms_detail" href="javascript:void(0);" data-terms_detail_id="'+terms_detail_id+'"><i class="fa fa-edit"></i></a>' +
                                '</td>'+
                                '<td>'+$(".cal_code").val()+'</td>'+
                                '<td>'+$(".narration").val()+'</td>'+
                                '<td>'+$(".percentage").val()+'</td>'+
                                '<td>'+$(".value").val()+'</td>'+
                                '<td>'+$(".gl_acc").val()+'</td><tr>';
                            $('table#terms_detail_table > tbody').append(row_html);
                            $('.terms_detail_data').each(function() {
                                $(this).val('');
                            });
                            $('.cal_definition_label').val('');
                            $(".cal_defi_section").find('.cal_defi_part').each(function(){
                                $(this).remove();
                            });
                            setSelect2Value($('select.first-select-code'));
                            show_notify("Terms detail added successfully !", true);
                        } else {
                            show_notify("Add operation fail !", true);
                        }
                    }
                });
            } else {
                var terms_detail_id = $("#terms_detail_id").val();
                $.ajax({
                    url: "<?=base_url()?>master/save_billing_terms_detail/"+terms_detail_id,
                    type: "POST",
                    data: terms_detail_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success == true) {
                            $("#terms_detail_id").val(0);
                            var row_html ='<td>' +
                                '<a class="btn btn-xs btn-danger btn_delete_terms_detail" href="javascript:void(0);" data-terms_detail_id="'+terms_detail_id+'"><i class="fa fa-remove"></i></a>' +
                                ' <a class="btn btn-xs btn-primary btn_edit_terms_detail" href="javascript:void(0);" data-terms_detail_id="'+terms_detail_id+'"><i class="fa fa-edit"></i></a>' +
                                '</td>'+
                                '<td>'+$(".cal_code").val()+'</td>'+
                                '<td>'+$(".narration").val()+'</td>'+
                                '<td>'+$(".percentage").val()+'</td>'+
                                '<td>'+$(".value").val()+'</td>'+
                                '<td>'+$(".gl_acc").val()+'</td>';
                            $('a[data-terms_detail_id="'+terms_detail_id+'"]').closest('tr').html(row_html);
                            $('.terms_detail_data').each(function() {
                                $(this).val('');
                            });
                            $('.cal_definition_label').val('');
                            $(".cal_defi_section").find('.cal_defi_part').each(function(){
                                $(this).remove();
                            });
                            setSelect2Value($('select.first-select-code'));
                            show_notify("Terms detail saved successfully !", true);
                        } else {
                            show_notify("Save operation fail !", true);
                        }
                    }
                });
            }
            $('.btn_add_terms_detail').html('Add');
        });


        $(document).on("click", ".btn_delete_terms_detail", function () {
            var tr = $(this).closest('tr');
            var terms_detail_id = $(this).data("terms_detail_id");
            if(BILLING_TERMS_ID != 0){
                var confirm_value = confirm('Are you sure ?');
                if(confirm_value) {
                    $.ajax({
                        url: '<?=BASE_URL?>master/delete_billing_terms_detail/' + terms_detail_id,
                        type: "POST",
                        data: null,
                        success: function (data) {
                            tr.fadeOut('medium', function () {
                                $(this).remove();
                            });
                            show_notify('Terms detail removed successfully!', true)
                        }
                    });
                    $('.terms_detail_data').each(function() {
                        $(this).val('');
                    });
                    $('.cal_definition_label').val('');
                    $(".cal_defi_section").find('.cal_defi_part').each(function(){
                        $(this).remove();
                    });
                    setSelect2Value($('select.first-select-code'));
                    $('.btn_add_terms_detail').html('Add');
                }
            }
        });

        $(document).on("click", ".btn_edit_terms_detail", function () {
            if (BILLING_TERMS_ID == 0) {
                ADD_ITEM = 1;
                $("#frm_billing_terms").submit();
                return false;
            }

            var terms_detail_id = $(this).data('terms_detail_id');
            $('.cal_definition_label').val('');
            setSelect2Value($('select.first-select-code'));
            $(".cal_defi_section").find('.cal_defi_part').each(function(){
                $(this).remove();
            });
            $.ajax({
                url: "<?=base_url()?>master/get_billing_terms_detail/" + terms_detail_id,
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('.btn_add_terms_detail').html('Save');
                        var terms_detail_data = data.terms_detail_data;
                        $.each(terms_detail_data,function(index,value) {
                            if ($('.terms_detail_data.' + index).length > 0) {
                                $('.terms_detail_data.' + index).val(value);
                            }
                        });
                        $("#terms_detail_id").val(terms_detail_data.terms_detail_id);
                        var cal_definition_detail = terms_detail_data.cal_definition_detail;
                        $.each(cal_definition_detail,function(index,value) {
                            if(index == 0){
                                setSelect2Value($("select.first-select-code"),'<?=base_url()?>app/set_billing_terms_cal_codes_select2_val_by_id/'+value.cal_code_id);
                            }else{
                                var cal_defi_part = $(".cal_defi_part.hidden").clone();
                                cal_defi_part.removeClass('hidden');
                                cal_defi_part.find("select.billing_terms_cal_codes").attr("id","select_billing_terms_cal_codes_"+cnt);
                                cal_defi_part.find("select.cal_operation").attr("id","select_cal_operation_"+cnt);
                                $(".cal_defi_section").append(cal_defi_part);
                                if(value.is_first_element == false){
                                    $("#select_cal_operation_"+cnt).val(value.cal_operation).change();
                                }
                                initAjaxSelect2($("#select_billing_terms_cal_codes_"+cnt),"<?=base_url('app/billing_terms_cal_codes_select2_source')?>/"+BILLING_TERMS_ID);
                                setSelect2Value($("#select_billing_terms_cal_codes_"+cnt),'<?=base_url()?>app/set_billing_terms_cal_codes_select2_val_by_id/'+value.cal_code_id);
                                cnt++;
                            }
                        });
                        set_cal_definition();
                    } else {
                        show_notify("Save operation fail !", true);
                    }
                }
            });
        });

        $(document).on("submit", "#frm_billing_terms", function (e) {
            e.preventDefault();
            if (ADD_ITEM == 0) {
                if ($("#billing_terms_name").val() == '') {
                    show_notify("Billing terms name is required");
                    return false;
                }
            }
            var billing_terms_data = new FormData();
            billing_terms_data.append('billing_terms_id',$("#billing_terms_id").val());
            billing_terms_data.append('billing_terms_name',$("#billing_terms_name").val());
            billing_terms_data.append('billing_terms_date',$("#billing_terms_date").val());
            $.ajax({
                url: "<?=base_url()?>master/save_billing_terms",
                type: "POST",
                data: billing_terms_data,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('#billing_terms_id').val(data.billing_terms_id);
                        BILLING_TERMS_ID = data.billing_terms_id;
                        if (ADD_ITEM == 1) {
                            ADD_ITEM = 0;
                            $(".btn_add_terms_detail").trigger('click');
                        } else {
                            show_notify("Billing terms saved successfully !", true);
                        }
                    } else {
                        show_notify("Save operation fail !", true);
                    }
                }
            });
        });
    });
    function set_cal_definition() {
        var cal_definition = '';
        var cal_definition_label = '';
        $(".cal_defi_section").find(".cal_definition_data").each(function (index, value) {
            if (index == 0) {
                cal_definition += " " + $(this).val();
            } else {
                if ($(this).hasClass('cal_operation')) {
                    cal_definition += "|" + $(this).val();
                } else {
                    cal_definition += " " + $(this).val();
                }
            }
            if ($(this).hasClass('first-select-code') && ($('select.first-select-code').val() == '' || $('select.first-select-code').val() == null)) {
                cal_definition_label += $(".cal_code").val();
            } else if ($(this).hasClass('cal_operation')) {
                cal_definition_label += " " + $(this).val();
            } else {
                cal_definition_label += " " + $(this).find("option:selected").text();
            }
        });
        $(".cal_definition").val(cal_definition);
        $(".cal_definition_label").val(cal_definition_label);
    }
</script>
