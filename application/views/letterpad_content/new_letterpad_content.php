<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold"></small>
            <button type="button" class="btn btn-primary pull-right letterpad_content_save_btn" style="margin: 5px;">
                Save
            </button>
        </h1>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title form_title">Letter pad Content</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <form method="post" action="<?php echo base_url('letterpad_content/'); ?>">
                                        <select name="modules" id="modules" class="form-control select2" onchange="this.form.submit()">
                                            <?php foreach ($modules as $module) { ?>
                                                <option value="<?php echo $module->id; ?>" <?php echo (isset($module_id) ? ($module->id == $module_id ? 'selected' : '') : ''); ?>><?php echo $module->module_label; ?></option>
                                            <?php } ?>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-8"></div>
                        </div>
                        <form method="POST" action="<?php echo base_url('letterpad_content/save_letterpad_content') ?>" id="form_letterpad_content" enctype="multipart/form-data">
                            <input type="hidden" id="id" name="module_id" value="<?php echo isset($module_id) ? $module_id : '' ?>">
                            <input type="hidden" id="header_logo_removed" name="header_logo_removed" value="0">
                            <div class="row">
                                <div class="col-md-6">
                                    <br/>
                                    <label>Header Logo :</label>
                                    <input type="hidden" name="header_logo_old" id="header_logo_old" class="form-control input-sm" value="<?php echo (isset($module_id) ? ($module_data->id == $module_id ? $module_data->header_logo : '') : ''); ?>">
                                    <input type="file" name="header_logo" id="header_logo">
                                    <br/>
                                    <label>Header Logo Alignment :</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="header_logo_alignment" id="header_logo_alignment_1" value="left" <?php echo (isset($module_id) ? ($module_data->id == $module_id && $module_data->header_logo_alignment == 'left' ? 'Checked' : '') : ''); ?>>
                                    <label for="header_logo_alignment_1" class="">Left</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="header_logo_alignment" id="header_logo_alignment_2" value="center" <?php echo (isset($module_id) ? ($module_data->id == $module_id && $module_data->header_logo_alignment == 'center' ? 'Checked' : '') : ''); ?>>
                                    <label for="header_logo_alignment_2" class="">Center</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="header_logo_alignment" id="header_logo_alignment_3" value="right" <?php echo (isset($module_id) ? ($module_data->id == $module_id && $module_data->header_logo_alignment == 'right' ? 'Checked' : '') : ''); ?>>
                                    <label for="header_logo_alignment_3" class="">Right</label>
                                    <br/><br/>
                                </div>
                                <div class="col-md-6">
                                    <br/>
                                    <?php 
                                        if(isset($module_id) && $module_data->id == $module_id && !empty($module_data->header_logo)){
                                            echo '<img src="' . base_url() . image_dir('letterpad_header_logo/') . $module_data->header_logo . '" id="view_header_logo" width="100px">&nbsp;&nbsp;';
                                            echo ' <a href="javascript:void(0);" id="remove_header_logo" class="btn btn-default">Remove</a>';
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Footer Detail :</label>
                                    <textarea class="footer_detail" id="footer_detail" style="visibility: hidden; display: none;"><?php echo (isset($module_id) ? ($module_data->id == $module_id ? $module_data->footer_detail : '') : ''); ?></textarea>
                                    <br/>
                                </div>
                            </div>
                            <input type="submit" class="hidden btn-submit-lead-form" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('.footer_detail').each(function () {
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.font_names = 'SerpentineDBol Regular/SerpentineDBol Regular, serpentinedbol_regular;SerpentineDBol Italic/SerpentineDBol Italic, serpentinedbol_italic;' + CKEDITOR.config.font_names;
            CKEDITOR.config.protectedSource.push(/<\?[\s\S]*?\?>/g);
            CKEDITOR.replace('footer_detail', {
                filebrowserBrowseUrl: '<?= base_url(); ?>mail-system3/attachment_browse_from_server',
                filebrowserUploadUrl: '<?= base_url(); ?>mail-system3/upload_attachment_to_server',
                extraPlugins: 'lineheight',
                removePlugins: ''
                        //removePlugins:'forms,iframe,preview,a11yhelp,about,link,pagebreak,flash,save,print,newpage'
            });
        });

        $(document).on('click', '#remove_header_logo', function () {
            $("#view_header_logo").remove();
            $("#remove_header_logo").remove();
            $("#header_logo_removed").val('1');
            $("#header_logo_old").val('');
        });

        $(document).on('click', '.letterpad_content_save_btn', function () {
            $("#form_letterpad_content").submit();
        });

        $("#form_letterpad_content").on("submit", function (e) {
            e.preventDefault();
            if ($('#modules').val() == "" || $('#modules').val() == null) {
                show_notify('Choose Module!', false);
                return false;
            }

            var form_data = new FormData(this);
            form_data.append('footer_detail', CKEDITOR.instances.footer_detail.getData());
            $("#ajax-loader").show();
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        console.log(data);
                        MODULE_ID = data.module_id;
                        $("#id").val(MODULE_ID);
                        show_notify(data.message, true);
                        $("#ajax-loader").hide();
                    }
                }
            });
        });

    });
</script>
