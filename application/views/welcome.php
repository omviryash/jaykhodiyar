<!-- Content Wrapper. Contains page content -->
<style>
    .users-list>li img {
        border-radius: 50%;
        max-width: 100%;
        height: 50px;
    }
</style>
<div class="content-wrapper">
    <div id="ajax-loader-content" style="display: none;">
        <div style="width:100%; height:100%; left:230px; top:50px; position:fixed; opacity:0; filter:alpha(opacity=40); background:#000000;z-index:110;">
        </div>
        <div style="float:left;width:100%; left:230px; top:45%; text-align:center; position:fixed; padding:0px; z-index:110;">
            <img src="<?php echo BASE_URL;?>/resource/image/loader.gif" width="150" height="150">
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <?php if ($this->applib->have_access_role(MAIL_SYSTEM_COMPOSE_MENU_ID, "view")) { ?>
            <div class="text-center pull-right">
                <a href="<?= base_url() ?>mail-system3/compose-mail/compose" class="btn btn-primary">Compose Mail</a>
            </div>
            <?php } ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content dashboard_content">
        <div class="row">
            <?php if ($this->session->flashdata('error_message')): ?>
            <div class="col-lg-12">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            </div>
            <?php endif; ?>
            <!-- ./col -->
            <?php if ($this->applib->have_access_role(ENQUIRY_MODULE_ID, "view")) { ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 class="noc_total_pending_enquiry_leads">0</h3>
                        <p>Pending Enquiry</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?= base_url() ?>enquiry/enquiry_list/" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <?php } ?>
            <?php if ($this->applib->have_access_role(QUOTATION_MODULE_ID, "view")) { ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 class="noc_total_pending_quatation">0</h3>
                        <p>Pending Quotation</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?= base_url() ?>quotation/quotation_list" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <?php } ?>

            <?php if ($this->applib->have_access_role(SALES_ORDER_MODULE_ID, "view")) { ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 class="noc_total_pending_dispatch">0</h3>
                        <p>Pending Dispatch (Items qty.)</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="<?= base_url() ?>sales_order/order_list/?is_approved=1&status=0" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <?php } ?>

            <?php if ($this->applib->have_access_role(CHALLAN_MODULE_ID, "view")) { ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 class="noc_dispatched">0</h3>
                        <p>Dispatched</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="<?= base_url() ?>challan/challan_list/" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <?php } ?>
        </div>
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <!-- USERS LIST -->
                <div class="row">
                    <div class="col-md-4" style="padding-right:0px;">
                        <div class="box box-danger">
                            <div class="nav-tabs-custom" style="cursor: move;">
                                <!-- Tabs within a box -->
                                <ul class="nav nav-tabs pull-right ui-sortable-handle">
                                    <li class="active pull-left"><a href="#agents-chat" data-toggle="tab" aria-expanded="true">Agents</a></li>
                                    <?php if ($this->applib->have_access_role(VISITOR_TAB_ID, "view")) { ?>
                                    <li class="pull-left"><a href="#visitor-chat" data-toggle="tab" aria-expanded="false">Visitors</a></li>
                                    <?php } ?>
                                    <?php if (isset($chat_roles) && count($chat_roles) != 0) { ?>
                                        <li class="pull-right"><a href="<?= base_url() ?>messages" class="uppercase">View All Agents</a></li>
                                    <?php } ?>
                                    <!--  <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
<span class="label label-danger"><?//=isset($agents) && is_array($agents)?count($agents):0; ?> Agents</span>-->
                                </ul>
                                <div class="tab-content no-padding">             
                                    <div class="chart tab-pane active" id="agents-chat" style="position: relative;height: 274px;overflow-y:scroll;">
                                        <div class="box-body no-padding">                       
                                            <ul class="users-list clearfix">
                                                <?php
                                                if (isset($agents) && is_array($agents)) {
                                                    foreach ($agents as $agent) {
                                                        if (in_array($agent['staff_id'], $chat_roles)) {
                                                ?>
                                                <li style="width: 24%;">
                                                    <a class="users-list-name" href="javascript:void(0);"
                                                       onclick="register_popup('<?php echo $agent['staff_id']; ?>', '<?php echo $agent['name']; ?>');">
                                                        <?php if (isset($agent['image']) && !empty($agent['image'])) { ?>
                                                        <img src="<?php echo IMAGE_URL; ?>staff/<?php echo $agent['image']; ?>"
                                                             alt="User Image" width="50" height="50">
                                                        <?php } else { ?>
                                                        <?php if (trim($agent['gender']) == 'f') { ?>
                                                        <img src="<?php echo IMAGE_URL; ?>staff/default_f.png" alt="User Image" width="50" height="50">
                                                        <?php } else if (trim($agent['gender']) == 'm') { ?>
                                                        <img src="<?php echo IMAGE_URL; ?>staff/default_m.png" alt="User Image" width="50" height="50">
                                                        <?php } else { ?>
                                                        <img src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image" width="50" height="50">
                                                        <?php } ?>
                                                        <?php } ?>
                                                    </a>
                                                    <a class="users-list-name" href="javascript:void(0);"
                                                       onClick="register_popup('<?php echo $agent['staff_id']; ?>', '<?php echo $agent['name']; ?>');"><?php echo $agent['name']; ?></a>
                                                    <!-- <span class="users-list-date"><?php /* if ($agent['is_login'] == 1) {
                                                          echo "Online";
                                                          } else {
                                                          echo "Offline";
                                                          } */ ?></span>-->
                                                </li>
                                                <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                            <!-- /.contatcts-list -->                  
                                        </div>
                                        
                                    </div>
                                    <?php if ($this->applib->have_access_role(VISITOR_TAB_ID, "view")) { ?>
                                    <div class="chart tab-pane" id="visitor-chat" style="position: relative; height: 274px;overflow-y:scroll;">
                                        <div class="box-body no-padding"> 
                                            <ul class="users-list clearfix">
                                                <?php
                                                if (isset($visitors) && is_array($visitors)) {
                                                    foreach ($visitors as $visitor) {
                                                        if ($visitor->name != "") {
                                                            $visitorname = $visitor->name;
                                                        } else {
                                                            $visitorname = "Visitor";
                                                        }
                                                ?>
                                                <li style="width: 24%;">
                                                    <a class="users-list-name" href="javascript:void(0);"
                                                       onclick="visitor_register_popup('<?php echo $visitor->session_id; ?>', '<?php echo $visitorname; ?>');">
                                                        <img src="<?php echo IMAGE_URL; ?>staff/default-user.png" alt="User Image" width="50" height="50">
                                                    </a>
                                                    <a class="users-list-name" href="javascript:void(0);"
                                                       onClick="visitor_register_popup('<?php echo $visitor->session_id; ?>', '<?php echo $visitorname; ?>');">
                                                        <?php echo $visitorname; ?>
                                                    </a>
                                                </li>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </ul>                      
                                            <!-- /.contatcts-list -->                  
                                        </div>
                                        
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding-right:0px;">
                        <div class="box box-info action_1_box">
                            <div class="overlay_div overlay_count_div"><i class=""></i></div>
                            <div class="box-body" style="padding-top:0px !important">
                                <div class="table-responsive">
                                    <table class="table no-margin">
                                        <thead>
                                            <tr>
                                                <th>Actions - 1</th>
                                                <th>Today</th>
                                                <th>Overdue</th>
                                            </tr>
                                        </thead>
                                        <tbody id="reminder-count-table-body">
                                            <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID, "view")) { ?>
                                            <tr>
                                                <td>Reminder</td>
                                                <td>
                                                    <b class="noc_today_reminder_no">
                                                        <a class='a_reminder' name='a_reminder' data-type='reminder' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_reminder_no">
                                                        <a class='a_reminder' name='a_reminder' data-today='false' data-type='reminder' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php if ($this->applib->have_access_role(ENQUIRY_MODULE_ID, "view")) { ?>
                                            <tr>
                                                <td>Leads</td>
                                                <td>
                                                    <b class="noc_today_pending_lead">
                                                        <a class='a_reminder' name='a_reminder' data-type='lead' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_pending_lead">
                                                        <a class='a_reminder' name='a_reminder' data-type='lead' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Enquiry</td>
                                                <td>
                                                    <b class="noc_today_pending_inquiry">
                                                        <a class='a_reminder' name='a_reminder' data-type='inquiry' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_pending_inquiry">
                                                        <a class='a_reminder' name='a_reminder' data-type='inquiry' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php if ($this->applib->have_access_role(QUOTATION_MODULE_ID, "view")) { ?>
                                            <tr>
                                                <td>Quotations</td>
                                                <td>
                                                    <b class="noc_today_pending_quotation">
                                                        <a class='a_reminder' name='a_reminder' data-type='quotation' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_pending_quotation">
                                                        <a class='a_reminder' name='a_reminder' data-type='quotation' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Quotation Reminder</td>
                                                <td>
                                                    <b class="noc_today_pending_quotation_followup">
                                                        <a class='a_reminder' name='a_reminder' data-type='quotation_followup' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_pending_quotation_followup">
                                                        <a class='a_reminder' name='a_reminder' data-type='quotation_followup' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php if ($this->applib->have_access_role(SALES_ORDER_MODULE_ID, "view")) { ?>
                                            <tr>
                                                <td>Approve Order</td>
                                                <td>
                                                    <b class="noc_today_sales_order_to_approve">
                                                        <a class='a_reminder' name='a_reminder' data-type='order_to_approve' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_sales_order_to_approve">
                                                        <a class='a_reminder' name='a_reminder' data-type='order_to_approve' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pending Dispatch (Orders)</td>
                                                <td>
                                                    <b class="noc_today_pending_dispatch">
                                                        <a class='a_reminder' name='a_reminder' data-type='dispatch' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_pending_dispatch">
                                                        <a class='a_reminder' name='a_reminder' data-type='dispatch' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            
                        </div>	
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info action_2_box">
                            <div class="overlay_div overlay_count_div"><i class=""></i></div>
                            <div class="box-body" style="padding-top:0px !important">
                                <div class="table-responsive">
                                    <table class="table no-margin">
                                        <thead>
                                            <tr>
                                                <th>Actions - 2</th>
                                                <th>Today</th>
                                                <th>Overdue</th>
                                            </tr>
                                        </thead>
                                        <tbody id="">
                                            <?php if ($this->applib->have_access_role(CHALLAN_MODULE_ID, "view")) { ?>
                                                <tr>
                                                    <td>Pending Invoice</td>
                                                    <td>
                                                        <b class="today_pending_invoice">
                                                            <a class='a_reminder' name='a_reminder' data-type='pending_invoice' data-today='true' href='javascript:void(0)'>0</a>
                                                        </b>
                                                    </td>
                                                    <td>
                                                        <b class="overdue_pending_invoice">
                                                            <a class='a_reminder' name='a_reminder' data-type='pending_invoice' data-today='false' href='javascript:void(0)'>0</a>
                                                        </b>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php if ($this->applib->have_access_role(CHALLAN_MODULE_ID, "view")) { ?>
                                                <tr>
                                                    <td>Pending Testing</td>
                                                    <td>
                                                        <b class="today_pending_testing">
                                                            <a class='a_reminder' name='a_reminder' data-type='pending_testing' data-today='true' href='javascript:void(0)'>0</a>
                                                        </b>
                                                    </td>
                                                    <td>
                                                        <b class="overdue_pending_testing">
                                                            <a class='a_reminder' name='a_reminder' data-type='pending_testing' data-today='false' href='javascript:void(0)'>0</a>
                                                        </b>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php if ($this->applib->have_access_role(TESTING_REPORT_MODULE_ID, "view")) { ?>
                                                <tr>
                                                    <td>Pending Installation</td>
                                                    <td>
                                                        <b class="today_pending_install">
                                                            <a class='a_reminder' name='a_reminder' data-type='pending_install' data-today='true' href='javascript:void(0)'>0</a>
                                                        </b>
                                                    </td>
                                                    <td>
                                                        <b class="overdue_pending_install">
                                                            <a class='a_reminder' name='a_reminder' data-type='pending_install' data-today='false' href='javascript:void(0)'>0</a>
                                                        </b>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php if ($this->applib->have_access_role(COMPLAIN_MENU_ID, "view")) { ?>
                                            <tr>
                                                <td>Unresolved Complain</td>
                                                <td>
                                                    <b class="noc_today_unresolve_complain">
                                                        <a class='a_reminder' name='a_reminder' data-type='unresolve_complain' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_unresolve_complain">
                                                        <a class='a_reminder' name='a_reminder' data-type='unresolve_complain' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>Resolved Complain</td>
                                                <td>
                                                    <b class="noc_today_resolve_complain">
                                                        <a class='a_reminder' name='a_reminder' data-type='resolve_complain' data-today='true' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                                <td>
                                                    <b class="noc_overdue_resolve_complain">
                                                        <a class='a_reminder' name='a_reminder' data-type='resolve_complain' data-today='false' href='javascript:void(0)'>0</a>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pending Complain Invocie</td>
                                                <td><b>0</b></td>
                                                <td><b>0</b></td>
                                            </tr>
                                            <tr>
                                                <td>Pending Complain Payment</td>
                                                <td><b>0</b></td>
                                                <td><b>0</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            
                        </div>	
                    </div>
                </div>

                <?php 
                /* <div class="box box-danger">
                  <div class="box-header with-border">
                  <h3 class="box-title">Agents</h3>

                  <div class="box-tools pull-right">
                  <span
                  class="label label-danger"><?= isset($chat_roles) && is_array($chat_roles) ? count($chat_roles) : 0; ?>
                  Agents</span>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                  class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                  class="fa fa-times"></i>
                  </button>
                  </div>
                  </div>
                  
                  <div class="box-body no-padding">
                  <ul class="users-list clearfix">
                  <?php
                  if (isset($agents) && is_array($agents)) {
                  foreach ($agents as $agent) {
                  if(in_array($agent['staff_id'], $chat_roles)){
                ?>
                  <li>
                  <a class="users-list-name"
                  href="javascript:void(0);"
                  onclick="register_popup('<?php echo $agent['staff_id']; ?>', '<?php echo $agent['name']; ?>');">
                  <?php if (isset($agent['image']) && !empty($agent['image'])) { ?>
                  <img src="<?php echo IMAGE_URL; ?>staff/<?php echo $agent['image']; ?>"
                  alt="User Image" width="50" height="50">
                  <?php } else { ?>
                  <img src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image"
                  width="50" height="50">
                  <?php } ?>
                  </a>
                  <a class="users-list-name"
                  href="javascript:void(0);"
                  onClick="register_popup('<?php echo $agent['staff_id']; ?>', '<?php echo $agent['name']; ?>');"><?php echo $agent['name']; ?></a>
                  <!-- <span class="users-list-date"></span>-->

                  </li>
                  <?php
                  }
                  }
                  }
                ?>
                  </ul>
                  
                  </div>
                  
                  <div class="box-footer text-center">
                  <?php if(isset($chat_roles) && count($chat_roles) != 0){ ?>
                  <a href="<?=base_url()?>messages" class="uppercase">View All Agents</a>
                  <?php } ?>
                  </div>
                  
                  </div> */ ?>
                
            <!-- <div class="box box-danger">
            <div class="box-header with-border">
            <h3 class="box-title">Mail System</h3>

            <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
            class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
            class="fa fa-times"></i>
            </button>
            </div>
            </div>

            <div class="box-body text-center">
            <div class="row">
            <div class="col-md-12">
            <div class="box-body no-padding">
            <a href="<?= base_url() ?>mail-system3/compose-mail/compose"
            class="btn btn-primary">Compose Mail</a>
            </div>
            </div>
            </div>
            </div>
            <div class="box-footer text-center">
            &nbsp;
            </div>
            </div> -->
                <div class="row">
                    <div class="col-md-4" style="padding-right:0px;">
                        <div class="box box-solid bg-green-gradient" style="height: 360px;">
                            <div class="box-header">
                                <i class="fa fa-calendar"></i>
                                <h3 class="box-title">Calendar</h3>
                                <div class="pull-right box-tools">
                                </div>
                            </div>
                            <div class="box-body no-padding">
                                <div id="calendar" style="width:100%" ></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box box-info reminder_dashboard action_board_box">
                            <div class="overlay_div"><i class=""></i></div>
                            <div class="box-header text-center with-border">
                                <i class="ion ion-ios-bell pull-left"></i>
                                <h3 class="box-title reminder_count pull-left">Action Board (<?= isset($reminder_count) ? $reminder_count : 0 ?>)</h3>
                                <div class="col-sm-5 dispaly-flex">
                                    <select id="created_by" class="form-control select2 input-sm">
                                        <option value="">- Created By - </option>
                                                <?php foreach($users as $user):?>
                                                <option value="<?php echo $user->staff_id; ?>"><?php echo $user->name; ?></option>
                                                <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="box-tools pull-right">
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID, "add")) { ?>
                                    <a href="<?= base_url(); ?>reminder" class="btn btn-info btn-xs pull-right">Add Reminder</a>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table no-margin">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th class="text-center">Task</th>
                                                <th>Type</th>
                                                <th>Date & Time</th>
                                                <th>Created By</th>
                                                <th>Assign To</th>
                                            </tr>
                                        </thead>
                                        <tbody id="reminder-table-body"></tbody>
                                    </table>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>


                <?php 
                /* <!-- TO DO List -->
                  <div class="box box-primary">
                  <div class="box-header">
                  <i class="ion ion-ios-bell"></i>

                  <h3 class="box-title">Reminder</h3>
                  <a href="<?= base_url();?>reminder" class="btn btn-info btn-xs pull-right">Add Reminder</a>
                  </div>
                  
                  <div class="box-body">
                  <ul class="todo-list">
                  <?php
                  foreach($combine_array as $k=>$v)
                  {
                  if(isset($v['reminder_id']) && !empty($v['reminder_id']))
                  {
                ?>
                  <li id="reminder_<?php echo $v['reminder_id'];?>">
                  <small class="label label-primary"><?php echo date('d-m-Y',strtotime($v['reminder_date']))?></small>
                  <!-- todo text -->
                  <span class="text"><?= $v['remind'];?></span>
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                  <a href="<?= base_url();?>reminder/edit/<?=$v['reminder_id'];?>"><i class="fa fa-edit"></i></a>
                  <a href="javascript:void(0);" onclick="delete_reminder(<?php echo $v['reminder_id'];?>);"><i class="fa fa-trash-o"></i></a>
                  </div>
                  </li>
                  <?php } ?>
                  <?php
                  if(isset($v['lead_no']) && !empty($v['lead_no']))
                  {
                ?>
                  <li id="lead_<?php echo $v['id'];?>">
                  <!-- <small class="label label-warning"><?php echo date('d-m-Y',strtotime($v['review_date']))?></small> -->
                  <small class="label label-warning">Lead</small>
                  <!-- todo text -->
                  <span class="text"><?= $v['lead_title'];?></span>
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                  <!-- <a href="<?= base_url();?>reminder/edit/<?=$v['reminder_id'];?>"><i class="fa fa-edit"></i></a>
                  <a href="javascript:void(0);" onclick="delete_reminder(<?php echo $v['reminder_id'];?>);"><i class="fa fa-trash-o"></i></a> -->
                  </div>
                  </li>
                  <?php } ?>

                  <?php
                  if(isset($v['quotation_no']) && !empty($v['quotation_no']))
                  {
                ?>
                  <li id="quotation_<?php echo $v['id'];?>">
                  <!-- <small class="label label-warning"><?php echo date('d-m-Y',strtotime($v['review_date']))?></small> -->
                  <small class="label label-success">Quotation</small>
                  <!-- todo text -->
                  <span class="text"><?= $v['party_name'];?></span>
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                  <!-- <a href="<?= base_url();?>reminder/edit/<?=$v['reminder_id'];?>"><i class="fa fa-edit"></i></a>
                  <a href="javascript:void(0);" onclick="delete_reminder(<?php echo $v['reminder_id'];?>);"><i class="fa fa-trash-o"></i></a> -->
                  </div>
                  </li>
                  <?php } ?>
                  <?php } ?>
                  </ul>
                  </div>
                  
                  </div>
                  <!-- /.box -->
                 */ ?>
                
                
            </section>
            
        </div>
        
        <?php 
        /*if ($this->applib->have_access_role(SALES_CHART_MODULE_ID, "view")){
        ?>
  <div class="row">
  <section class="col-md-12">
  <!-- Map box -->
  <div class="box box-solid ">
  <div class="box-header">
  <!-- tools box -->
  <div class="pull-right box-tools">
  <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
  data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
  <i class="fa fa-minus"></i></button>
  </div>
  <!-- /. tools -->

  <i class="fa"></i>

  <h3 class="box-title">
  Sales
  </h3>
  </div>
  <div class="box-body">
  <!--<div id="world-map" style="height: 350px; width: 100%;"></div>-->
  <div class="chart tab-pane active">
  <!-- Sales Chart Canvas -->
  <canvas id="salesChart" style="position: relative; height: 350px;"></canvas>
  </div>
  </div>
  <!-- /.box-body-->
  <div class="box-footer no-border">
  <div class="row">
  <!-- ./col -->
  </div>
  <!-- /.row -->
  </div>
  </div>
  <!-- /.box -->
  </section>
  </div>
  <?php } ?>
  <!-- /.col -->
  <?php
  if ($this->applib->have_access_role(DASHBOARD_DEMO_MENU_ID, "view")){
        ?>
  <!-- /.visitor map box -->
  <!--<div class="row">
  <section class="col-md-12">
  <div class="box box-solid bg-light-blue-gradient">
  <div class="box-header">
  <div class="pull-right box-tools">
  <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
  </div>
  <i class="fa fa-map-marker"></i>
  <h3 class="box-title">Visitors</h3>
  </div>
  <div class="box-body">
  <div id="world-map" style="height: 350px; width: 100%;"></div>
  </div>
  <div class="box-footer no-border">
  <div class="row"></div>
  </div>
  </div>
  </section>
  </div>-->
  <!-- /.visitor map box end-->
  <?php } */ ?>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript" src="<?= base_url('resource/plugins/chartjs/Chart.min.js'); ?>"></script>
<script type="text/javascript">
    $(window).load(function () {
//        $(".select2").select2({width: '100%'});
        is_onload = '1';
        is_first_load = '';
        $('#created_by').on("change", function () {
           get_reminder_by_date();
        });
//    $(window).bind("load", function () {
//        $("#ajax-loader-content").show();
        $(".overlay_div").addClass('overlay');
        $(".overlay_div i").addClass('fa fa-refresh fa-spin');
        
        $.ajax({
            url: "<?= base_url('welcome/get_modules_num_of_count/'); ?>",
            type: "POST",
            data: {},
            cache: false,
            async: true,
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $('.noc_total_pending_enquiry_leads').html(data.total_pending_enquiry_leads);
                    $('.noc_total_pending_quatation').html(data.total_pending_quatation);
                    $('.noc_total_pending_dispatch').html(data.total_pending_dispatch);
                    $('.noc_dispatched').html(data.dispatched);
                    
                    if(data.today_reminder_no != '0'){
                        $('.noc_today_reminder_no a').html(data.today_reminder_no);
                    } else {
                        $('.noc_today_reminder_no').html(data.today_reminder_no);
                    }
                    if(data.overdue_reminder_no != '0'){
                        $('.noc_overdue_reminder_no a').html(data.overdue_reminder_no);
                    } else {
                        $('.noc_overdue_reminder_no').html(data.overdue_reminder_no);
                    }
                    
                    if(data.today_pending_lead != '0'){
                        $('.noc_today_pending_lead a').html(data.today_pending_lead);
                    } else {
                        $('.noc_today_pending_lead').html(data.today_pending_lead);
                    }
                    if(data.overdue_pending_lead != '0'){
                        $('.noc_overdue_pending_lead a').html(data.overdue_pending_lead);
                    } else {
                        $('.noc_overdue_pending_lead').html(data.overdue_pending_lead);
                    }
                    
                    if(data.today_pending_inquiry != '0'){
                        $('.noc_today_pending_inquiry a').html(data.today_pending_inquiry);
                    } else {
                        $('.noc_today_pending_inquiry').html(data.today_pending_inquiry);
                    }
                    if(data.overdue_pending_inquiry != '0'){
                        $('.noc_overdue_pending_inquiry a').html(data.overdue_pending_inquiry);
                    } else {
                        $('.noc_overdue_pending_inquiry').html(data.overdue_pending_inquiry);
                    }
                    
                    if(data.today_pending_invoice != '0'){
                        $('.today_pending_invoice a').html(data.today_pending_invoice);
                    } else {
                        $('.today_pending_invoice').html(data.today_pending_invoice);
                    }
                    if(data.overdue_pending_invoice != '0'){
                        $('.overdue_pending_invoice a').html(data.overdue_pending_invoice);
                    } else {
                        $('.overdue_pending_invoice').html(data.overdue_pending_invoice);
                    }
                    
                    if(data.today_pending_testing != '0'){
                        $('.today_pending_testing a').html(data.today_pending_testing);
                    } else {
                        $('.today_pending_testing').html(data.today_pending_testing);
                    }
                    if(data.overdue_pending_testing != '0'){
                        $('.overdue_pending_testing a').html(data.overdue_pending_testing);
                    } else {
                        $('.overdue_pending_testing').html(data.overdue_pending_testing);
                    }
                    
                    if(data.today_pending_install != '0'){
                        $('.today_pending_install a').html(data.today_pending_install);
                    } else {
                        $('.today_pending_install').html(data.today_pending_install);
                    }
                    if(data.overdue_pending_install != '0'){
                        $('.overdue_pending_install a').html(data.overdue_pending_install);
                    } else {
                        $('.overdue_pending_install').html(data.overdue_pending_install);
                    }
                    
                    if(data.today_pending_quotation != '0'){
                        $('.noc_today_pending_quotation a').html(data.today_pending_quotation);
                    } else {
                        $('.noc_today_pending_quotation').html(data.today_pending_quotation);
                    }
                    if(data.overdue_pending_quotation != '0'){
                        $('.noc_overdue_pending_quotation a').html(data.overdue_pending_quotation);
                    } else {
                        $('.noc_overdue_pending_quotation').html(data.overdue_pending_quotation);
                    }
                    
//                    if(data.today_pending_quotation_followup != '0'){
//                        $('.noc_today_pending_quotation_followup a').html(data.today_pending_quotation_followup);
//                    } else {
//                        $('.noc_today_pending_quotation_followup').html(data.today_pending_quotation_followup);
//                    }
//                    if(data.overdue_pending_quotation_followup != '0'){
//                        $('.noc_overdue_pending_quotation_followup a').html(data.overdue_pending_quotation_followup);
//                    } else {
//                        $('.noc_overdue_pending_quotation_followup').html(data.overdue_pending_quotation_followup);
//                    }
                    
                    if(data.today_sales_order_to_approve != '0'){
                        $('.noc_today_sales_order_to_approve a').html(data.today_sales_order_to_approve);
                    } else {
                        $('.noc_today_sales_order_to_approve').html(data.today_sales_order_to_approve);
                    }
                    if(data.overdue_sales_order_to_approve != '0'){
                        $('.noc_overdue_sales_order_to_approve a').html(data.overdue_sales_order_to_approve);
                    } else {
                        $('.noc_overdue_sales_order_to_approve').html(data.overdue_sales_order_to_approve);
                    }
                    
                    if(data.today_pending_dispatch != '0'){
                        $('.noc_today_pending_dispatch a').html(data.today_pending_dispatch);
                    } else {
                        $('.noc_today_pending_dispatch').html(data.today_pending_dispatch);
                    }
                    if(data.overdue_pending_dispatch != '0'){
                        $('.noc_overdue_pending_dispatch a').html(data.overdue_pending_dispatch);
                    } else {
                        $('.noc_overdue_pending_dispatch').html(data.overdue_pending_dispatch);
                    }
                    
                    if(data.today_unresolve_complain != '0'){
                        $('.noc_today_unresolve_complain a').html(data.today_unresolve_complain);
                    } else {
                        $('.noc_today_unresolve_complain').html(data.today_unresolve_complain);
                    }
                    if(data.overdue_unresolve_complain != '0'){
                        $('.noc_overdue_unresolve_complain a').html(data.overdue_unresolve_complain);
                    } else {
                        $('.noc_overdue_unresolve_complain').html(data.overdue_unresolve_complain);
                    }
                    
                    if(data.today_resolve_complain != '0'){
                        $('.noc_today_resolve_complain a').html(data.today_resolve_complain);
                    } else {
                        $('.noc_today_resolve_complain').html(data.today_resolve_complain);
                    }
                    if(data.overdue_resolve_complain != '0'){
                        $('.noc_overdue_resolve_complain a').html(data.overdue_resolve_complain);
                    } else {
                        $('.noc_overdue_resolve_complain').html(data.overdue_resolve_complain);
                    }
                    $(".overlay_div.overlay_count_div").removeClass('overlay');
                    $(".overlay_div.overlay_count_div i").removeClass('fa fa-refresh fa-spin');
                }
            }
        });
        
        get_reminder_by_date();
        
        <?php /*$.ajax({
            url: "<?= base_url('app/get_email_report'); ?>",
            type: "POST",
            data: "date=<?php echo date('Y-m-d'); ?>",
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $(".email_report_body").html(data.email_report_html);
                }
            }
        }); */ ?>
    });
    
    
    $("a[name=a_reminder]").on("click", function () {
        var today = $(this).data("today");
        var typename = $(this).data("type");
        var type = $(".a_reminder").data("type");
        console.log(today);
        console.log(typename);
        //return false;

        $("#ajax-loader-content").show();
        $.ajax({
            url: "<?= base_url('welcome/get_reminder_by_anchor/'); ?>",
            type: "POST",
            data: "today=" + today + "&typename=" + typename,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $(".reminder_count").html('Action Board (' + data.reminder_count + ')');
                    $("tbody#reminder-table-body").html(data.reminder_html);
                    $("#ajax-loader-content").hide();
                }
            }
        });
    });

    function delete_reminder(id) {
        if (confirm("Are you sure to delete reminder?")) {
            $("#ajax-loader-content").show();
            $.ajax({
                type: 'post',
                url: '<?= BASE_URL ?>reminder/delete/' + id,
                success: function (data) {
                    var data = JSON.parse(data);
                    $msg = data.msg;
                    $("#ajax-loader-content").hide();
                    if (data.status == 1) {
                        show_notify($msg, true);
                        $("#reminder_" + id).animate({backgroundColor: "#fbc7c7"}, "fast").animate({opacity: "hide"}, "slow");
                    } else {
                        show_notify($msg, false);
                    }

                },
                error: function (e) {
                    $("#ajax-loader-content").hide();
                }
            });
            return false;
        }
        return false;
    }
    $(document).ready(function () {
        //$("#calendar").datepicker().datepicker("setDate", new Date());
        var today_date = new Date();
        //$("#calendar").datepicker("setDate", <?= date('m-d-Y') ?>);
        $("#calendar").datepicker({
            dateFormat: 'dd-mm-yyyy',
            todayHighlight: true
        }).on('changeDate', function (dateText) {
            var selected_date_obj = $('#calendar').datepicker('getDate');
            var selected_date = convert(selected_date_obj);
            if (selected_date == 'NaN-aN-aN') {
                return false;
            }
            is_onload = '0';
            var created_by = $("#created_by").val();
            $("#ajax-loader-content").show();
            $.ajax({
                url: "<?= base_url('welcome/get_reminder_by_date'); ?>/" + created_by,
                type: "POST",
                data: "date=" + selected_date,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $(".reminder_count").html('Action Board (' + data.reminder_count + ')');
                        $("tbody#reminder-table-body").html(data.reminder_html);
                        $("#ajax-loader-content").hide();
                    }
                }
            });
            //            $.ajax({
            //                url: "<?= base_url('app/get_email_report'); ?>",
            //                type: "POST",
            //                data: "date=" + selected_date,
            //                cache: false,
            //                processData: false,
            //                dataType: 'json',
            //                success: function (data) {
            //                    if (data.success == true) {
            //                        $(".email_report_body").html(data.email_report_html);
            //                    }
            //                }
            //            });
        });
        $.ajax({
            url: '<?php echo site_url('welcome/getVisitors'); ?>',
            dataType: 'json',
            success: function (json) {
                data = [];
                var countries = [];
                //console.log(json);
                for (i in json) {
                    data[i] = json[i]['total'];
                    countries.push(json[i]['country']);
                }
                //countries = countries.join(',');
                //console.log(countries);
                $('#world-map').vectorMap({
                    backgroundColor: '#4d97c3',
                    borderColor: '#035d92',
                    color: '#9FD5F1',
                    hoverOpacity: 0.7,
                    series: {
                        regions: [{
                            scale: {
                                '1': '#4169E1'
                            },
                            attribute: 'fill',
                            values: {countries}
                        }]
                    },
                    selectedColor: '#666666',
                    enableZoom: true,
                    showTooltip: true,
                    values: data,
                    normalizeFunctiodateFormat: 'mm-dd-yy',
                    n: 'polynomial',
                    onRegionLabelShow: function (event, label, code) {
                        //console.log(code);
                        if (json[code]) {
                            label.html('<strong>' + label.text() + '</strong><br>Visitors:' + json[code]['total']);
                        }
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });


        <?php /*
      if ($this->applib->have_access_role(SALES_CHART_MODULE_ID, "view")){
        ?>

      var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
      // This will get the first returned node in the jQuery collection.
      var salesChart = new Chart(salesChartCanvas);

      var salesChartData = {
      labels: [

      <?php foreach($chart_data as $chart_row):?>
      "<?=$chart_row['y'];?>",
      <?php endforeach;?>
      ],
      datasets: [
      {
      label: "Sales",
      fillColor: "rgb(210, 214, 222)",
      strokeColor: "rgb(210, 214, 222)",
      pointColor: "rgb(210, 214, 222)",
      pointStrokeColor: "#c1c7d1",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgb(220,220,220)",
      data: [
      <?php foreach($chart_data as $chart_row):?>
      <?=$chart_row['item1'];?>,
      <?php endforeach;?>
      ]
      },
      {
      label: "Quotation",
      fillColor: "rgba(60,141,188,0.9)",
      strokeColor: "rgba(60,141,188,0.8)",
      pointColor: "#3b8bba",
      pointStrokeColor: "rgba(60,141,188,1)",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(60,141,188,1)",
      data: [
      <?php foreach($chart_data as $chart_row):?>
      <?=$chart_row['item2'];?>,
      <?php endforeach;?>
      ]
      }
      ]
      };

      var salesChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: true,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
      };

      //Create the line chart
      salesChart.Line(salesChartData, salesChartOptions);
      <?php } */ ?>

    });

    function convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    
    function get_reminder_by_date(){
        var date = "<?php echo date('Y-m-d'); ?>";
        var created_by = $("#created_by").val();
        $(".overlay_div").addClass('overlay');
        $(".overlay_div i").addClass('fa fa-refresh fa-spin');
        if(is_onload == '1'){
//            alert("d");
            $.ajax({
            url: "<?= base_url('welcome/get_reminder_by_date'); ?>/" + created_by,
            type: "POST",
            data: {date: date, from: 'onLoad'},
            cache: false,
            async: true,
            //processData: false,
            dataType: 'json',
            success: function (data) {
//                console.log(data);
                if (data.success == true) {
                    $(".reminder_count").html('Action Board (' + data.reminder_count + ')');
                    $("tbody#reminder-table-body").html('');
                    $("tbody#reminder-table-body").html(data.reminder_html);
                    
                    if(is_first_load == ''){
                        is_first_load = '1';
                        if(data.today_pending_quotation_followup != '0'){
                            $('.noc_today_pending_quotation_followup a').html(data.today_pending_quotation_followup);
                        } else {
                            $('.noc_today_pending_quotation_followup').html(data.today_pending_quotation_followup);
                        }
                        if(data.overdue_pending_quotation_followup != '0'){
                            $('.noc_overdue_pending_quotation_followup a').html(data.overdue_pending_quotation_followup);
                        } else {
                            $('.noc_overdue_pending_quotation_followup').html(data.overdue_pending_quotation_followup);
                        }
                    }
                        
                    $("#ajax-loader-content").hide();
                    $(".overlay_div").removeClass('overlay');
                    $(".overlay_div i").removeClass('fa fa-refresh fa-spin');
                } else {
                    $("tbody#reminder-table-body").html('');
                    $("#ajax-loader-content").hide();
                    $(".overlay_div").removeClass('overlay');
                    $(".overlay_div i").removeClass('fa fa-refresh fa-spin');
                }
            }
        });
        } else {
            var selected_date_objc = $('#calendar').datepicker('getDate');
            var select_date = convert(selected_date_objc);
            if (select_date == 'NaN-aN-aN') {
                return false;
            }
            $.ajax({
                url: "<?= base_url('welcome/get_reminder_by_date'); ?>/" + created_by,
                type: "POST",
                data: "date=" + select_date,
                cache: false,
                async: true,
                //processData: false,
                dataType: 'json',
                success: function (data) {
//                    console.log(data);
                    if (data.success == true) {
                        $(".reminder_count").html('Action Board (' + data.reminder_count + ')');
                        $("tbody#reminder-table-body").html('');
                        $("tbody#reminder-table-body").html(data.reminder_html);

                        $("#ajax-loader-content").hide();
                        $(".overlay_div").removeClass('overlay');
                        $(".overlay_div i").removeClass('fa fa-refresh fa-spin');
                    } else {
                        $("tbody#reminder-table-body").html('');
                        $("#ajax-loader-content").hide();
                        $(".overlay_div").removeClass('overlay');
                        $(".overlay_div i").removeClass('fa fa-refresh fa-spin');
                    }
                }
            });
            
        }
        
    }
</script>
