<li class="dropdown-header">Move To</li>
<?php
if(isset($mailboxes) && is_array($mailboxes) && count($mailboxes) > 0){?>
	<li class="<?=strtolower($current_folder_name) == 'inbox'?'active':''?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(2) == strtolower('inbox')?'':'btn-mails-move-to'?>" data-folder_name="inbox"><?='Inbox';?></a></li>
	<li class="<?=strtolower($current_folder_name) == 'important'?'active':''?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == strtolower('important')?'':'btn-mails-move-to'?>" data-folder_name="important"><?='Important';?></a></li>
	<li class="<?=strtolower($current_folder_name) == 'trash'?'active':''?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == strtolower('trash')?'':'btn-mails-move-to'?>" data-folder_name="trash"><?='Trash';?></a></li>
	<li class="<?=strtolower($current_folder_name) == 'spam'?'active':''?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == strtolower('spam')?'':'btn-mails-move-to'?>" data-folder_name="spam"><?='Spam';?></a></li>
	<?php foreach($mailboxes as $mailbox) {
		?>
		<li class="<?=$current_folder_name == $mailbox['mailbox_label']?'active':''?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == strtolower($mailbox['mailbox_label'])?'':'btn-mails-move-to'?>" data-folder_name="<?=$mailbox['mailbox_label'];?>"><?=$mailbox['mailbox_label'];?></a></li>
		<?php
	}
	?>
	<?php
}else {
	?>
	<li class="<?=$this->uri->segment(3) == 'important'?'active':'btn-mails-move-to'?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == 'important'?'active':'btn-mails-move-to'?>" data-folder_name="important">Important</a></li>
	<li class="<?=$this->uri->segment(3) == 'promotions'?'active':'btn-mails-move-to'?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == 'promotions'?'active':'btn-mails-move-to'?>" data-folder_name="promotions">Promotions</a></li>
	<li class="<?=$this->uri->segment(3) == 'social'?'active':'btn-mails-move-to'?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == 'social'?'active':'btn-mails-move-to'?>" data-folder_name="social">Social</a></li>
	<?php
}
?>
<li><a href="#" data-toggle="modal" data-target="#modal-create-folder" class="btn-create-folder">Create new</a></li>