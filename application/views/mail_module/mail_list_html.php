<div class="table-responsive mailbox-messages">
    <form method="post" action="" id="frm-mail-inbox">
        <input type="hidden" name="from_folder_name" value="<?=$current_folder_name;?>">
        <input type="hidden" name="folder_name" value="<?=$current_folder_name;?>">
        <table class="table table-hover table-striped">
            <tbody>
            <?php
            if(isset($mails_data['mails']) && is_array($mails_data['mails']) && count($mails_data['mails']) > 0) {
                foreach($mails_data['mails'] as $mail){
                    ?>
                    <tr>
                        <td>
                            <?php
                            if($current_folder_name == 'starred' && isset($mail['folder_name'])) {
                                echo "<input type='hidden' name='folder_name' value='".$mail['folder_name']."' class='folder_name'>";
                            }
                            ?>
                            <input type="checkbox" class="chk-select-mail" name="mails_no[]" value="<?=$mail['mail_no'];?>">
                        </td>
                        <td class="mailbox-star"><a href="javascript:void(0);" class="btn-star" data-mail_no="<?=$mail['mail_no'];?>"><i class="fa <?=$mail['is_starred'] == 1?'fa-star':'fa-star-o'?> text-yellow"></i></a></td>
                        <td class="mailbox-list">
                            <?php
                            if($current_folder_name == 'starred' && isset($mail['folder_name'])) {
                                echo "<input type='hidden' name='folder_name' value='".$mail['folder_name']."' class='folder_name'>";
                            }
                            ?>
                            <div class="mailbox-name">
                                <a class="btn-read-mail <?=$mail['is_unread']?'btn_view_unread_mail':'';?>" href="javascript:void(0);" data-message_no="<?=$mail['mail_no'];?>">
                                    <?=$mail['is_unread'] == 1?"<b>".$mail['username']."</b>":$mail['username'];?>
                                </a>
                            </div>
                            <div class="mailbox-attachment">
                                <?=$mail['attachment_status'] == 1?'<i class="fa fa-paperclip"></i>':'&nbsp;';?>
                            </div>
                            <div class="mailbox-subject">
                                <?=$mail['is_unread'] == 1?"<b>".$this->crud->limit_character($mail['subject'],25)."</b>":$this->crud->limit_character($mail['subject'],25);?>
                            </div>
                            <div class="mailbox-date">
                                <?=$mail['received_at'];?>
                            </div>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </form>
    <!-- /.table -->
</div>