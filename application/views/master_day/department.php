<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		$isEdit = $this->app_model->have_access_role(MASTER_HR_DEPARTMENT_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_HR_DEPARTMENT_MENU_ID, "delete");
		$isAdd = $this->app_model->have_access_role(MASTER_HR_DEPARTMENT_MENU_ID, "add");
		?>
		<h1>
			<small class="text-primary text-bold">Department</small>
		</h1>
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="col-md-12">
						<div class="col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading">
									Department Detail
								</div>
								<div style="margin: 10px;">
									<table id="example1" class="table custom-table department-table">
										<thead>
											<tr>
												<th>Action</th>
												<th>Department</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(!empty($results)) {
												foreach ($results as $row) {
											?>
											<tr>
												<td>
													<?php if($isEdit) { ?>
													<a href="<?= base_url('master_hr/department/'.$row->department_id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
													<?php } if($isDelete) { ?>
													<?php if($row->delete_not_allow == 0) { ?>
														<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('party/delete/'.$row->department_id);?>"><i class="fa fa-trash"></i></a>
													<?php }  } ?>
												</td>
												<td><?=$row->department ?></td>
											</tr>
											<?php 
												} }
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<?php if($isAdd || $isEdit) { ?>
						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-heading clearfix">
									<?php if(isset($department_id) && !empty($department_id)){ ?>Edit 
									<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
									<?php } ?> Department
								</div>
								<div style="margin:20px">	
									<form method="POST" 
										  <?php if(isset($department_id) && !empty($department_id)){ ?>
										  action="<?=base_url('master_hr/update_department') ?>" 
										  <?php } else { ?>
										  action="<?=base_url('master_hr/add_department') ?>" 
										  <?php } ?>
										  id="department-form">
										<?php if(isset($department_id) && !empty($department_id)){ ?>
										<input type="hidden" class="form-control input-sm" name="department_id" id="department_id" value="<?php echo $department_id; ?>" >
										<?php } ?>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-3 input-sm">Department<span class="required-sign">*</span></label>
											<div class="col-sm-7">
												<input type="text" class="form-control input-sm" id="department" name="department" value="<?php echo $department_name; ?>" <?php echo $btn_disable;?>>
											</div>
										</div>

										<?php if(isset($department_id) && !empty($department_id)){ ?>
										<button type="submit" class="btn btn-info btn-block btn-xs">Edit Department</button>
										<?php } else { ?>
										<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Department</button>
										<?php } ?>
									</form>
								</div>
							</div>                                            
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>

<script>
	$(document).ready(function(){
		$("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

		$("#department-form").on("submit",function(e){
			e.preventDefault();

			if($("#department").val() == ""){
				show_notify('Fill value Department.', false);
				return false;
			}
			<?php if(isset($department_id) && !empty($department_id)){ ?>
			var success_status = check_is_unique('department','department',$("#department").val(),'department_id','<?=$department_id?>');											
			<?php } else { ?>
			var success_status = check_is_unique('department','department',$("#department").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.unique-error').length > 0){
					$("p.unique-error").text('Department already exist!');
				}else{
					$("#department").after("<p class='text-danger unique-error'>Department already exist!</p>");
				}
				return false;	
			}else{
				$("p.unique-error").text(' ');
			}

			var url = '<?php echo base_url('master_hr/delete/') ?>';
			var value = $("#department").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
                                            window.location.href = "<?php echo base_url('master_hr/department') ?>";
					}
				});
			}
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=department_id&table_name=department',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('master_hr/department') ?>";
					}
				});
			}
		});

	});
</script>
