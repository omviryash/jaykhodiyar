<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		$isEdit = $this->app_model->have_access_role(MASTER_PAYMENT_MODE_MENU_ID, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_PAYMENT_MODE_MENU_ID, "delete");
		$isAdd = $this->app_model->have_access_role(MASTER_PAYMENT_MODE_MENU_ID, "add");
		?>
		<h1>
			<small class="text-primary text-bold">Payment Mode</small>
		</h1>
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="col-md-12">
						<div class="col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading">
									Payment Mode Detail
								</div>
								<div style="margin: 10px;">
									<table id="example1" class="table custom-table payment_mode-table">
										<thead>
											<tr>
												<th>Action</th>
												<th>Payment Mode</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(!empty($results)) {
												foreach ($results as $row) {
											?>
											<tr>
												<td>
													<?php if($isEdit) { ?>
													<a href="<?= base_url('master_hr/payment_mode/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
													<?php } if($isDelete) { ?>
													<?php if($row->delete_not_allow == 0) { ?>
														<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('party/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a>
													<?php }  } ?>
												</td>
												<td><?=$row->name ?></td>
											</tr>
											<?php 
												} }
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<?php if($isAdd || $isEdit) { ?>
						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-heading clearfix">
									<?php if(isset($id) && !empty($id)){ ?>Edit 
									<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
									<?php } ?> Payment Mode
								</div>
								<div style="margin:20px">	
									<form method="POST" 
										  <?php if(isset($id) && !empty($id)){ ?>
										  action="<?=base_url('master_hr/update_payment_mode') ?>" 
										  <?php } else { ?>
										  action="<?=base_url('master_hr/add_payment_mode') ?>" 
										  <?php } ?>
										  id="payment_mode-form">
										<?php if(isset($id) && !empty($id)){ ?>
										<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
										<?php } ?>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-3 input-sm">Payment Mode<span class="required-sign">*</span></label>
											<div class="col-sm-7">
												<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo $name; ?>" <?php echo $btn_disable;?>>
											</div>
										</div>

										<?php if(isset($id) && !empty($id)){ ?>
										<button type="submit" class="btn btn-info btn-block btn-xs">Edit Payment Mode</button>
										<?php } else { ?>
										<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Payment Mode</button>
										<?php } ?>
									</form>
								</div>
							</div>                                            
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>

<script>
	$(document).ready(function(){
		$("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"aaSorting": [[1, 'asc']],
			"paging":         false
		});

		$("#payment_mode-form").on("submit",function(e){
			e.preventDefault();

			if($("#name").val() == ""){
				show_notify('Fill value Payment Mode.', false);
				return false;
			}
            <?php if (isset($id) && !empty($id)) { ?>
    			var success_status = check_is_unique('payment_by','name',$("#name").val(),'id','<?= $id ?>');											
            <?php } else { ?>
    			var success_status = check_is_unique('payment_by','name',$("#name").val());
            <?php } ?>
			if(success_status == 0){
				if($('p.unique-error').length > 0){
					$("p.unique-error").text('Payment Mode already exist!');
				}else{
					$("#name").after("<p class='text-danger unique-error'>Payment Mode already exist!</p>");
				}
				return false;	
			}else{
				$("p.unique-error").text(' ');
			}

			var value = $("#name").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
                        window.location.href = "<?php echo base_url('master_hr/payment_mode') ?>";
					}
				});
			}
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=payment_by',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('master_hr/payment_mode') ?>";
					}
				});
			}
		});

	});
</script>
