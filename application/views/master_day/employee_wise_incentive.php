<div class="content-wrapper">
    <form class="form-horizontal" action="" method="post" id="save_employee_wise_incentive" data-parsley-validate="">
        <section class="custom content-header"  >
            <?php $this->load->view('shared/success_false_notify'); ?>
            <h1>
                <small class="text-primary text-bold">Employee Wise Incentive</small>
<!--                <a href="<?= BASE_URL; ?>master_hr/save_employee_wise_incentive" class="btn btn-info btn-xs pull-right" >Save</a>-->
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn">Save</button>
                <?php if (isset($data['only_view_mode']) && $data['only_view_mode'] == '1'){ ?>
                    <a href="<?= BASE_URL; ?>hr/email-salary-report/<?= $employee_data->staff_id ?>/<?= $year ?>/<?= $month ?>" class="btn btn-info btn-xs pull-right margin-r-5">Email</a> 
                    <a href="<?= BASE_URL; ?>hr/print-salary-report/<?= $employee_data->staff_id ?>/<?= $year ?>/<?= $month ?>" target="_blank" class="btn btn-info btn-xs pull-right margin-r-5">Print</a>
                <?php } ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Employee</label>
                                    <select name="employee_id[]" id="employee_id" multiple="" class="form-control input-sm select2"></select>
                                </div>
                            </div>
                            <div class="clearfix"></div><br />
                                <div class="line_item_form item_fields_div">
                                    <div class="row">
                                        <input type="hidden" name="line_items_index" id="line_items_index" />
                                        <?php if(isset($employee_wise_incentive)){ ?>
                                            <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                        <?php } ?>
                                        <div class="col-md-4">
                                            <label for="item_id" class="input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                            <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id"></select>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="no_of_item" class="input-sm">Select Count<span class="required-sign">&nbsp;*</span></label>
                                            <select name="line_items_data[no_of_item]" id="no_of_item" class="form-control input-sm select2" >
                                                <option value="1">1st</option>
                                                <option value="2">2nd</option>
                                                <option value="3">3rd</option>
                                                <option value="4">4 & 4+</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="line_item_incentive" class="input-sm">Incentive %<span class="required-sign">&nbsp;*</span></label>
                                            <input type="text" name="line_items_data[incentive]" id="line_item_incentive" class="form-control input-sm num_only" >
                                        </div>
                                        <div class="col-md-1"><br/>
                                            <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" />
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-sm-12"><br />
                                        <table style="" class="table custom-table item-table">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Item Name</th>
                                                    <th>Count</th>
                                                    <th>Incentive</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lineitem_list"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
<script>
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
    var lineitem_objectdata = [];
    <?php if (isset($employee_wise_incentive)) { ?>
        var li_lineitem_objectdata = [<?php echo $employee_wise_incentive; ?>];
        first_time_edit_mode = 0;
        var lineitem_objectdata = [];
        if (li_lineitem_objectdata != '') {
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
    <?php } ?>
    display_lineitem_html(lineitem_objectdata);
	$(document).ready(function(){
        initAjaxSelect2($("#employee_id"), "<?= base_url('app/staff_select2_source') ?>");
        initAjaxSelect2($("#item_id"),'<?=base_url()?>app/sales_item_select2_source/');
        
        $('#add_lineitem').on('click', function () {
            
            var item_id = $("#item_id").val();
            if (item_id == '' || item_id == null) {
                $("#item_id").select2('open');
                show_notify("Please Select Item!", false);
                return false;
            }
            var no_of_item = $("#no_of_item").val();
            if (no_of_item == '' || no_of_item == null) {
                $("#no_of_item").select2('open');
                show_notify("Please Select Count!", false);
                return false;
            }
            var line_item_incentive = $("#line_item_incentive").val();
            if (line_item_incentive == '' || line_item_incentive == null) {
                $("#line_item_incentive").focus();
                show_notify('Please Enter Incentive.', false);
                return false;
            }
            var key = '';
            var value = '';
            var lineitem = {};
            $('select[name^="line_items_data"]').each(function (e) {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            $('input[name^="line_items_data"]').each(function () {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            var no_of_item_data = $('#no_of_item option:selected').html();
            var item_data = $('#item_id option:selected').html();
            
            lineitem['no_of_item_data'] = no_of_item_data;
            lineitem['item_name'] = item_data;
            var new_lineitem = JSON.parse(JSON.stringify(lineitem));
            var line_items_index = $("#line_items_index").val();
            if (line_items_index != '') {
                lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
            } else {
                lineitem_objectdata.push(new_lineitem);
            } 
//            console.log(lineitem_objectdata);
            display_lineitem_html(lineitem_objectdata);
            $('#lineitem_id').val('');
            $('#line_items_index').val('');
            $("#item_id").val(null).trigger("change");
            $("#no_of_item").val(null).trigger("change");
            $("#line_item_incentive").val('');
            if (on_save_add_edit_item == 1) {
                on_save_add_edit_item == 0;
                $('#save_employee').submit();
            }
            edit_lineitem_inc = 0;
        });
        
        $(document).on('submit', '#save_employee_wise_incentive', function () { 
            if (lineitem_objectdata == '') {
                show_notify("Please Add Atleast One Item.", false);
                return false;
            }
            var is_validate = 0;
            if($('#employee_id').val() == "" || $('#employee_id').val() == null){
                if (confirm('Are you sure, you want to update Incentive% for all employee ? ')) {
                    is_validate = 1;
                } else {
                    is_validate = 0;
                }
			} else {
                is_validate = 1;
            }
            
            if(is_validate == 1){
                $('.module_save_btn').attr('disabled', 'disabled');
                $("#ajax-loader").show();
                var postData = new FormData(this);
                var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
                postData.append('line_items_data', lineitem_objectdata_stringify);
                $.ajax({
                    url: "<?= base_url('master_hr/save_employee_wise_incentive') ?>",
                    type: "POST",
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: postData,				
                    success: function (response) {
                        var json = $.parseJSON(response);

                        if (json['success'] == 'Added'){
                            window.location.href = "<?php echo base_url('master_hr/employee_wise_incentive') ?>";
                        }

                    },
                });
                return false;
            }
            return false;
        });
	});
    
    function display_lineitem_html(lineitem_objectdata) { 
        $('#ajax-loader').show();
        var new_lineitem_html = '';
        $.each(lineitem_objectdata, function (index, value) {
            var lineitem_edit_btn = '';
            var lineitem_delete_btn = '';
            lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
            lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
            var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
                    lineitem_edit_btn +
                    lineitem_delete_btn +
                    '</td>' +
                    '<td>' + value.item_name + '</td>' +
                    '<td>' + value.no_of_item_data + '</td>' + 
                    '<td>' + value.incentive + '</td> </tr>';
            new_lineitem_html += row_html;
        });
        $('tbody#lineitem_list').html(new_lineitem_html);
        $('#ajax-loader').hide();
    }
    
    function edit_lineitem(index) {
        $('#ajax-loader').show();
        if (edit_lineitem_inc == 0) {
            edit_lineitem_inc = 1;
            $(".add_lineitem").removeAttr("disabled");
        }
        var value = lineitem_objectdata[index];
//        console.log(value);
        $("#line_items_index").val(index);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#lineitem_id").val(value.id);
		}
        $("#item_id").val(null).trigger("change");
        setSelect2Value($("#item_id"), "<?= base_url('app/set_sales_item_select2_val_by_id') ?>/" + value.item_id);
        $("#no_of_item").val(value.no_of_item).trigger("change");
        $("#line_item_incentive").val(value.incentive);
        $('#ajax-loader').hide();
    }

    function remove_lineitem(index) {
        value = lineitem_objectdata[index];
        if (confirm('Are you sure ?')) {
            if (typeof (value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index, 1);
            display_lineitem_html(lineitem_objectdata);
        }
    }
</script>
