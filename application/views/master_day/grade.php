<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
            $this->load->view('shared/success_false_notify');
            $isEdit = $this->app_model->have_access_role(MASTER_HR_GRADE_MENU_ID, "edit");
            $isDelete = $this->app_model->have_access_role(MASTER_HR_GRADE_MENU_ID, "delete");
            $isAdd = $this->app_model->have_access_role(MASTER_HR_GRADE_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Grade</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-7">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														Grade Detail
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table sales-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Grade</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                        <?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td>
                                                                        <?php if($isEdit) { ?>
                                                                        <a href="<?= base_url('master_hr/grade/'.$row->grade_id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                                        <?php } if($isDelete) { ?>
                                                                        <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('master_hr/delete/'.$row->grade_id);?>"><i class="fa fa-trash"></i></a>
                                                                        <?php } ?>
                                                                    </td>
						                                            <td><?=$row->grade ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                            <?php if($isAdd || $isEdit) { ?>
                                            <div class="col-md-5">
												<div class="panel panel-default">
													<div class="panel-heading clearfix">
														<?php if(isset($grade_id) && !empty($grade_id)){ ?>Edit 
                                                        <?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
														<?php } ?>	Grade 
													</div>
													<div style="margin:10px">	
														<form method="POST" 
															<?php if(isset($grade_id) && !empty($grade_id)){ ?>
															action="<?=base_url('master_hr/update_grade') ?>" 
															<?php } else { ?>
															action="<?=base_url('master_hr/add_grade') ?>" 
															<?php } ?> id="form_grade">
															<?php if(isset($grade_id) && !empty($grade_id)){ ?>
                                                            <input type="hidden" class="form-control input-sm" name="grade_id" id="grade_id" value="<?php echo $grade_id; ?>">
															<?php } ?>
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-4 input-sm">Grade<span class="required-sign">*</span></label>
																<div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="grade_name" name="grade" value="<?php echo $grade; ?>" <?php echo $btn_disable;?>>
			                                                    </div>
															</div>
															<div class="clearfix"></div>
															<div class="form-group" style="margin:7px !important;"></div>
															
                                                            <?php if(isset($grade_id) && !empty($grade_id)){ ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs">Edit Grade</button>
                                                            <?php } else { ?>
                                                            <button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add Grade</button>
                                                            <?php } ?>
														</form>
													</div>
												</div>                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"paging":         false
		});

        $("#form_grade").on("submit",function(e){
			e.preventDefault();
			if($("#grade_name").val() == ""){
				show_notify('Fill value Grade.', false);
				return false;
			}
			var url = '<?php echo base_url('master_hr/delete/') ?>';
			var value = $("#grade").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.grade+'</td>';
		                TableRow += '</tr>';
		                $('.grade-table > tbody > tr:last ').after(TableRow);
		                $("#form_grade")[0].reset();
		                show_notify('Saved Successfully!',true);*/
		                window.location.href = "<?php echo base_url('master_hr/grade') ?>";
					}
				});
			}
		});

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=grade_id&table_name=grade',
					success: function(data){
						tr.remove();
		                window.location.href = "<?php echo base_url('master_hr/grade') ?>";
					}
				});
			}
		});
		
    });
</script>
