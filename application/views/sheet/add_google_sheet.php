<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <h1>
            <small class="text-primary text-bold">Add Google Sheet</small>
            <?php 
				$sheet_edit_role = $this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "edit");
				$sheet_view_role = $this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "view");
				$sheet_add_role = $this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "add"); 
            ?>
            <?php if($sheet_add_role): ?>
            <button type="button" onclick="submit_form()" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Save</button>
            <?php endif;?>
            <?php if($sheet_edit_role): ?>
            <a href="<?= base_url()?>sheet/google_sheet_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Sheet List</a>
            <?php endif;?>
            <?php if($sheet_add_role): ?>
            <a href="<?= base_url()?>sheet/add_google_sheet/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add New Sheet</a>
            <?php endif;?>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <!--<a href="#tab_1" data-toggle="tab">Staff Roles</a>-->
                    </li>
                </ul>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="main-frm" action="<?=base_url()?>sheet/save_sheet" method="post" id="sheet_form">
                                    <input type="hidden" class="" name="sheet_id" id="sheet_id" value="<?php if(isset($sheet)){ echo $sheet[0]['id'];} ?>">
                                    <div class="form-group col-sm-12">
                                        <label for="inputEmail3" class="col-sm-1 control-label input-sm" for="sheet_id">Sheet Name<span class="required-sign">&nbsp;*</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-sm" name="name" id="name" required value="<?php if(isset($sheet)){ echo $sheet[0]['name'];} ?>">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inputEmail3" class="col-sm-1 control-label input-sm" for="sheet_url">Sheet URL<span class="required-sign">&nbsp;*</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-sm" name="url" id="sheet_url" required value="<?php if(isset($sheet)){ echo $sheet[0]['url'];} ?>">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inputEmail3" class="col-sm-1 control-label input-sm" for="sequence">Sequence</label>
                                        <div class="col-sm-4">
                                            <input type="number" class="form-control input-sm" name="sequence" id="sequence" required value="<?php if(isset($sheet)){ echo $sheet[0]['sequence'];} ?>">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Select Staff Acces Role for Display GoogleSheet
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                        <a class="btn btn-xs btn-danger un-chk-all pull-right">Un Select ALL</a>
                                                        <a class="btn btn-xs btn-primary chk-all pull-right" style="margin-right: 5px;">Select ALL</a>
                                                    </div>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="col-md-12">

                                                    <?php if(count($users) > 0):?>
                                                    <?php foreach($users as $row):?>
                                                    <div class="form-group col-md-6">
                                                        <div class="col-md-6">
                                                            <label><input type="checkbox" <?php if(!empty($sheet_user)){ echo in_array($row->staff_id, $sheet_user) ? 'checked="checked"':''; } ?> class="chkids" name="staff_ids[]" value="<?php echo $row->staff_id;?>">&nbsp;&nbsp;&nbsp;&nbsp;<?=$row->name;?></label>
                                                        </div>
                                                    </div>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                                </div>
                                                <div class="col-md-12"></div>
                                                <div class="col-md-12"></div>
                                                <div class="col-md-12"></div>
                                                <div class="col-md-12"></div>
                                            </div>
                                        </div>
                                    </div>                                           
                                </form>
                            </div>
                        </div>
                    </div>    
                </div>                
            </div>
        </div>
    </div>
    <?php if($sheet_add_role): ?>
    <button type="button" onclick="submit_form()" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Save</button>
    <?php endif;?>
    <?php if($sheet_view_role): ?>
    <a href="<?= base_url()?>sheet/google_sheet_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Sheet List</a>
    <?php endif;?>
    <?php if($sheet_add_role): ?>
    <a href="<?= base_url()?>sheet/add_google_sheet/" class="btn btn-info btn-xs pull-right load_quotations" style="margin: 5px;">Add New Sheet</a>
    <?php endif;?>
</div>    

<script type="text/javascript">

    function submit_form() {
        if ($.trim($("#name").val()) == '') {
            show_notify('Sheet name is required !', false);
            $("#name").focus();
            return false;
        }
        if ($.trim($("#sheet_url").val()) == '') {
            show_notify('Please Enter google sheet url.', false);
            $("#sheet_url").focus();
            return false;
        }
        $("#sheet_form").submit();
    }
    $(document).ready(function() {
        $(".chk-all").click(function () {
            $(".chkids").prop("checked", true);
        });


        $(".un-chk-all").click(function () {
            $(".chkids").prop("checked", false);
        });

        $(document).on("submit","#sheet_form",function(e){
            e.preventDefault();
            if(!$('input[name="staff_ids[]"]:checked').length > 0){
                show_notify('Please select at least one staff!', false);
                return false;
            }
            $.ajax({
                url: "<?=base_url('sheet/save_sheet');?>",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                //dataType: 'json',
                success: function (data) {

                    var data = JSON.parse(data);
                    console.log(data);
                    if(data.status == 1)
                    {
                        $("#sheet_id").val(data.sheet_id);
                        <?php if(isset($sheet)){ } else { ?>
							$('#sheet_form')[0].reset();
						<?php } ?>
                        show_notify('Google Sheet save successfully!', true);

                    }
                    else
                    {
                        show_notify(data.msg,false);
                        if(data.name_error == 1)
                        {
                            jQuery("#email_id").focus();
                        } 
                        if(data.url_error == 1)
                        {
                            jQuery("#phone_no").focus();
                        }    

                        if(data.sequence_error == 1)
                        {
                            jQuery("#sequence").focus();
                        }

                    }
                }
            });
        });
    });
</script>
