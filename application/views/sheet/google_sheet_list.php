<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php $this->load->view('shared/success_false_notify'); ?>
        <h1>
            <small class="text-primary text-bold">Google Sheet List</small>
			<?php $sheet_add_role = $this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "add");  ?>
            <?php if($sheet_add_role): ?>
				<a href="<?=base_url('sheet/add_google_sheet/')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
            <?php endif;?>
        </h1>
    </section>
    <section class="content">
        <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin: 10px;">
                                <div class="col-md-12">
                                    <table id="sheet_list_datatable" class="table custom-table agent-table" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Sheet Name</th>
                                                <th>Sequence</th>
                                                <th>Sheet URL</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
        <h1>
            <?php 
               $sheet_add_role = $this->app_model->have_access_role(GOOGLE_SHEET_MODULE_ID, "add"); 
            ?>
            <?php if($sheet_add_role): ?>
            <a href="<?=base_url('sheet/add_google_sheet/')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add New</a>
            <?php endif;?>
            
            
        </h1>
    </section>
    <section class="content-header"></section>
</div>

<script>
    var table;
    $(document).ready(function(){

        table = $('#sheet_list_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[2, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('sheet/sheet_list_datatable')?>",
                "type": "POST"
            },
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
        $(document).on("click",".delete_button",function(){
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if(value){
                $.ajax({
                    url: $(this).data('href'),
                    type: "GET",
                    data: null,
                    success: function(data){
                        show_notify("Sheet deleted successfully!",true);
                        table.draw();
                    }
                });
            }
        });
    });
</script>
