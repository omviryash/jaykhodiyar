<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?=$sheet_data->name;?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
               <iframe height="750" style="width:100%" src="<?=$sheet_data->url;?>"></iframe>
			</section>
		</div>
	</section>
</div>

