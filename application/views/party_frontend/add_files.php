<div class="content-wrapper">
    <style>
        .select2-container--open .select2-dropdown--below { border: 1px solid #2b2d3f; top: 0px; left: -1px; }
    </style>
    <form class="form-horizontal" action="<?= base_url('party_frontend/save_file') ?>" method="post" id="save_file" novalidate>
        <?php if(isset($file_id) && !empty($file_id)) { ?>
            <input type="hidden" class="form-control input-sm" id="file_id" name="file_id" value="<?= $file_id; ?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Files</small>
                <?php
                    $view_role = $this->applib->have_access_role(PARTY_FRONTEND_MENU_ID, "view");
                ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" id="save_courier" style="margin: 5px;">Save</button>
                <?php if ($view_role): ?>
                    <a href="<?= base_url() ?>party_frontend/file_list" class="btn btn-info btn-xs pull-right" style="margin: 5px;">File List</a>
                <?php endif; ?>
                <?php if ($view_role): ?>
                    <a href="<?= BASE_URL ?>party_frontend/files/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add File</a>
                <?php endif; ?>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">File Details</a></li>   
                        <?php if (isset($file_id) && !empty($file_id)) { ?>
                            <li class=""><a href="#tab_2" data-toggle="tab">Login Details</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <!-- Horizontal Form -->
                            <div class="row">
                                <input type="hidden" name="exist_file_name" id ="exist_file_name" value="" />
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold"> File Details</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="item_id" class="col-md-3 input-sm">Item</label>
                                                    <div class="col-md-9">  
                                                        <select  name="item_id" id="item_id" class="form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="caption" class="col-md-3 input-sm">Caption <span class="required-sign">*</span></label>
                                                    <div class="col-md-9">  
                                                        <input type="text" name="caption" id="caption" class="form-control input-sm" value="<?= (isset($caption)) ? $caption : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="file_type_id" class="col-md-3 input-sm">File Type <span class="required-sign">*</span></label>
                                                    <div class="col-md-9">  
                                                        <label class="no-padding">
                                                            <input type="radio" name="file_type_id" id="file_type_files" class="file_type_files" value="1" <?php if(isset($file_type_id) && $file_type_id == 1){ echo 'checked="checked"'; } else {} ?>> General Files &nbsp;&nbsp; &nbsp; &nbsp;
                                                        </label>
                                                        <label class="no-padding">
                                                            <input type="radio" name="file_type_id" id="file_type_videos" class="file_type_videos" value="2" <?php if(isset($file_type_id) && $file_type_id == 2){ echo 'checked="checked"'; } else {} ?>> Videos &nbsp;&nbsp; &nbsp; &nbsp;
                                                        </label>
                                                        <label class="no-padding">
                                                            <input type="radio" name="file_type_id" id="file_type_photos" class="file_type_photos" value="3" <?php if(isset($file_type_id) && $file_type_id == 3){ echo 'checked="checked"'; } else {} ?>> Photos &nbsp;&nbsp; &nbsp; &nbsp;
                                                        </label>
                                                        <label class="no-padding">
                                                            <input type="radio" name="file_type_id" id="file_type_foundation_detail" class="file_type_foundation_detail" value="4" <?php if(isset($file_type_id) && $file_type_id == 4){ echo 'checked="checked"'; } else {} ?>> Foundation Detail &nbsp;&nbsp; &nbsp; &nbsp;
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="file" class="col-md-3 input-sm">File</label>
                                                    <div class="col-md-6">
                                                        <input type="file" name="file" id="file" class="form-control input-sm">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="button" name="select_file" id="select_file" class="from-control input-sm" value="Select File">
                                                        <span id="display_filename"></span>
                                                    </div>
                                                    <div class="col-md-3"></div>
                                                    <div class="col-md-9">
                                                        <label class="text-danger"><small>Max Upload file size is 2048MB (2GB)</small></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="text-danger">Note : If you have not select any Party, then above file display in All Party Login.</label>
                                        <table id="party_list_datatable" class="table custom-table agent-table" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Party Code</th>
                                                    <th>Party Name</th>
                                                    <th>Contact No. </th>
                                                    <th>Email Id</th>
                                                    <th>Party Type</th>
                                                    <th>City</th>
                                                    <th>State</th>
                                                    <th>Country</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                           </tbody>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            <?php if (isset($file_id) && !empty($file_id)) { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label  class="col-sm-3 input-sm">Created By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?= (isset($created_by_name)) ? $created_by_name : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 input-sm">Created Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?= (isset($created_at)) ? $created_at : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 input-sm">Updated By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?= (isset($updated_by_name)) ? $updated_by_name : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label  class="col-sm-3 input-sm">Updated Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?= (isset($updated_at)) ? $updated_at : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!--Model Start-->
<div id="file_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title">Your <span id="file_type_heading"</span></h4>
            </div>
            <div class="modal-body">
                <table id="fileupload_table" class="table custom-table table-striped" width="100%">
                <thead>
                        <tr>
                            <th></th>  
                            <th>Files</th>  
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" id="select_modal_files" class="btn btn-info" data-dismiss="modal" >Select</button>
                <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--Model End-->

<script type="text/javascript">
    $(document).ready(function () {
        $('#address_type').select2();
        initAjaxSelect2($("#item_id"), "<?= base_url('app/item_select2_source') ?>");
            
        table = $('#party_list_datatable').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[3, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('party_frontend/party_list_datatable')?>",
                "type": "POST",
                "data":function(d){
                }
            },
            "scrollY": 500,
            "scrollX": '100%',
            "scroller": {
                "loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
        });

        fileupload_table = $('#fileupload_table').DataTable({
            "serverSide": true,
            "search": true,
            "ordering":[1, "desc"],
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('party_frontend/fileupload_datatable') ?>",
                "type": "POST",
                "data": function (d) {                    
                    d.file_type_id = $('input[name=file_type_id]:checked').val();
                },
            },
            "scrollY": "480px",
            "scroller": {
                "loadingIndicator": true
            },
        });
    
        $(document).on('click', '#select_file', function () {
            if ($(".file_type_files").prop('checked') == false && $(".file_type_videos").prop('checked') == false && $(".file_type_photos").prop('checked') == false && $(".file_type_foundation_detail").prop('checked') == false) {
                show_notify('Please Select File Type', false);
                $(".file_type_files").focus();               
                return false;
            }
            if ($(".file_type_files").prop('checked') == true) {
                $('#file_type_heading').html('General Files');
            }
            if ($(".file_type_videos").prop('checked') == true) {
                $('#file_type_heading').html('Videos');
            }
            if ($(".file_type_photos").prop('checked') == true) {
                $('#file_type_heading').html('Photos');
            }
            if ($(".file_type_foundation_detail").prop('checked') == true) {
                $('#file_type_heading').html('Foundation Detail');
            }
            $('#file_modal').modal('show');             
            fileupload_table.draw();
        });
        
        $('#file_modal').on('shown.bs.modal', function (e) {
            fileupload_table.draw();
            $(".dataTables_scrollHeadInner").css({"width": "100%"});
        });
    
        $(document).on('click', '#select_modal_files', function () {
            var file = $('input[name=file_name_radio_btn]:checked').val();
            $('#display_filename').html(file);
            $('#exist_file_name').val(file);          
            return false;
        });
        
        $(document).on('change', '.party_id', function () {
            if ($(this).prop('checked') == true){ 
                $('#save_file').append('<input type="text" name="selected_party_id[]" id="selected_party_id_'+ $(this).val() +'" value="'+ $(this).val() +'" />');
            } else {
                $('#selected_party_id_'+ $(this).val()).remove();
            }
        });
        
        $(document).on('submit', '#save_file', function () {
            if ($.trim($("#caption").val()) == '') {
                show_notify('Please Enter Caption.', false);
                $("#caption").focus();
                return false;
            }
            if ($(".file_type_files").prop('checked') == false && $(".file_type_videos").prop('checked') == false  && $(".file_type_foundation_detail").prop('checked') == false) {
                show_notify('Please Select File Type', false);
                $(".file_type_files").focus();
                return false;
            }
            if ($.trim($("#file").val()) == '' && $.trim($('#exist_file_name').val()) == '') {
                show_notify('Please Upload or Select file.', false);
                return false;
            }
            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('party_frontend/save_file') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('party_frontend/file_list') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('party_frontend/file_list') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });


    });
</script>