<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class='text-primary text-bold'>File List</small>
            <?php if ($this->applib->have_access_role(COURIER_MENU_ID, "add")) { ?>
                <a href="<?= base_url() ?>party_frontend/files/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add File</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="row">
            <div style="margin: 15px;">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="row">
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <label>Item</label>
                                    <select  name="item_id" id="item_id" class="form-control input-sm"></select>
                                </div>
                                <div class="col-md-2">
                                    <label>File Type</label>
                                    <select name="file_type_id" id="file_type_id" class="form-control">
                                        <option value="0">All</option>
                                        <option value="1">Files</option>
                                        <option value="2">Videos</option>
                                        <option value="3">Photos</option>
                                        <option value="4">Foundation Detail</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Party</label>
                                    <select  name="party_id" id="party_id" class="form-control input-sm"></select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <table id="file_list" class="table custom-table agent-table" width="100%">
                                        <thead>
                                            <tr>      		 
                                                <th width="55px">Action</th>
                                                <th>Item</th>
                                                <th>Caption</th>
                                                <th>File Type</th>
                                                <th>File For Party</th>
                                                <th>Created at</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        initAjaxSelect2($("#item_id"), "<?= base_url('app/item_select2_source') ?>");
        $('#file_type_id').select2();
        initAjaxSelect2($("#party_id"), "<?= base_url('app/party_select2_source') ?>");
        
        table = $('#file_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('party_frontend/file_list_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                    d.item_id = $("#item_id").val();
                    d.file_type_id = $("#file_type_id").val();
                    d.party_id = $("#party_id").val();
                }
            },
            "scroller": {
                "loadingIndicator": true,
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "deferRender": true,
            "scrollY": 500,
        });
        
        $(document).on('change', '#item_id, #file_type_id, #party_id', function() {
            table.draw();
        });
    
        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: '',
                    success: function (data) {
                        table.draw();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });
    });
</script>
