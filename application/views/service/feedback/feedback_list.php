<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class='text-primary text-bold'> Feedback List </small>
            <?php $add_role = $this->app_model->have_access_role(FEEDBACK_MODULE_ID, "add"); ?>
            <?php if($add_role): ?>
                <a href="<?=base_url()?>feedback/add" class="btn btn-info btn-xs pull-right">Add Feedback</a>
            <?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_feedback_list" class="table custom-table agent-table">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Feedback No</th>
                                    <th>Feedback Date</th>
                                    <th>Party Name</th>
                                    <th>Contact No</th>
                                    <th>Email Id</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        table = $('#table_feedback_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('feedback/feedback_datatable')?>",
                "type": "POST",
                "data": function(d){
                    //d.city_id = $("#city_id").val();
                },
            },
            "scrollY": 550,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=feedback',
                    success: function(data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });
    });
</script>
