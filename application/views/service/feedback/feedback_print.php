<html>
    <head>
        <title>Customer Feedback</title>
        <style>
            .text-center{
                text-align: center;
            }
            table{
                border-spacing: 0;
                width: 100%;
                border-bottom: 1px solid;
                border-right: 1px solid;
            }
            td{
                padding: 1px 1px 1px 1px;
                border-left: 1px solid;
                border-top: 1px solid;
                font-size:10px;
            }
            tr > td:last-child{
                border-right: 1px solid !important;
            }
            tr:last-child > td{
                border-bottom: 1px solid !important;
            }
            .text-right{
                text-align: right;
            }
            .text-bold{
                font-weight: 900 !important;
                font-size:12px !important;
            }
            .text-header{
                font-size: 20px;
            }
            .no-border-top{
                border-top:0;
            }
            .no-border-bottom{
                border-bottom:0 !important;
            }
            .no-border-left{
                border-left:0;
            }
            .no-border-right{
                border-right:0;
            }
            .width-50-pr{
                width:50%;
            }
            td.footer-sign-area{
                height: 82x;
                vertical-align: bottom;
                /*width: 33.33%;*/
                text-align: center;
            }
            .no-border{
                border: 0!important;
            }
            .footer-detail-area{
                color: #000000;
                font-size: 12px;
            }
        </style>
    </head>
    <body>
        <table style="page-break-inside:avoid">
            <tr>
                <td class="text-center text-bold text-header" colspan="11">Customer Feedback</td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="5" width="250px"><b>Manufacturer & Supplier</b></td>
                <td class="text-bold" colspan="3" width="100px">Feedback  No.</td>
                <td class="text-bold text-left" colspan="2" width="120px"><b><?=$feedback_data->feedback_no_year;?></b></td>
                <td class="text-bold text-left" colspan="1" width="120px"><b><?=$feedback_data->feedback_date;?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" rowspan="7" colspan="5" >
                    <span style="font-size:15px;"><b><?= $company_details['name'] ?></b></span><br />
                    <?= nl2br($company_details['address']); ?><br />
                    City : <?= $company_details['city']; ?> - <?= $company_details['pincode']; ?> (<?= $company_details['state']; ?>), Country : <?= $company_details['country']; ?>.<br />
                    Email: <?= $company_details['email_id']; ?><br />
                    Tel No. : <?= $company_details['contact_no']; ?>,<br />
                    Contact No. : <?= $company_details['cell_no']; ?><br />
                    <b>GST No : <?= $company_details['gst_no']; ?></b>
                </td>
                <td class="text-bold" align="left" colspan="3">Installation No.</td>
                <td class="text-bold text-left" colspan="2" ><b><?=$installation_data->installation_no_year;?></b></td>
				<td class="text-bold text-left" colspan="1"><b><?=$installation_data->installation_date;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" align="left" colspan="3">Testing Report No.</td>
                <td class="text-bold text-left" colspan="2" ><b><?=$testing_report_data->testing_report_no_year;?></b></td>
				<td class="text-bold text-left" colspan="1"><b><?=$testing_report_data->testing_report_date;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" align="left" colspan="3">Challan No.</td>
                <td class="text-bold text-left" colspan="2" ><b><?=$challan_data->challan_no;?></b></td>
				<td class="text-bold text-left" colspan="1"><b><?=$challan_data->challan_date;?></b></td>
            </tr>
            <tr>
				<td class="text-bold" align="left" colspan="3">Invoice No.</td>
				<td class="text-bold text-left" colspan="2" ><b><?=$challan_data->invoice_no;?></b></td>
				<td class="text-bold text-left" colspan="1"><b><?=$challan_data->invoice_date;?></b></td>
			</tr>
			<tr>
				<td class="text-bold" align="left" colspan="3">Quotation No</td>
				<td class="text-bold text-left" colspan="2"><b><?=$challan_data->quotation_no;?></b></td>
				<td class="text-bold text-left" colspan="1"><b><?=$challan_data->quotation_date;?></b></td>
			</tr>
			<tr>
				<td class="text-bold" align="left" colspan="3">Purchase Order No. </td>
				<td class="text-bold text-left" colspan="2"><b><?=$challan_data->cust_po_no;?></b></td>
				<td class="text-bold text-left" colspan="1"><b><?=$challan_data->po_date;?></b></td>
			</tr>
			<tr>
				<td class="text-bold" colspan="3" align="left">Sales Order No.</td>
				<td class="text-bold text-left" colspan="2"><b><?=$challan_data->sales_order_no;?></b></td>
				<td class="text-bold text-left" colspan="1"><b><?=$challan_data->sales_order_date;?></b></td>
			</tr>
            <tr>
                <td class="text-center text-bold" colspan="5"><b>Purchaser</b></td>
                <td class="text-left text-bold" colspan="3">Feedback Call By</td>
                <td class="text-left text-bold" colspan="2"><b><?=$feedback_data->feedback_call_by;?></b></td>
                <td class="text-left text-bold" colspan="1"><b><?=$feedback_data->feedback_call_by_contact;?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" rowspan="4" colspan="5" >
                    <span style="font-size:15px;"><b><?= $party_data['party_name'] ?></b><br /></span>
                    <?= nl2br($party_data['address']); ?><br />
                    City : <?= $party_data['city'] ?> -  <?= $party_data['pincode'] ?> (<?= $party_data['state'] ?>) <?= $party_data['country'] ?>.<br />
                    Email : <?= $party_data['email_id']; ?><br />
                    Tel No.:  <?= $party_data['fax_no']; ?>,<br />
                    Contact No.:  <?= $party_data['phone_no']; ?><br />
                    Contact Person: <?= $challan_data->contact_person_name ?><br />
                    <?php if ($party_data['party_type_1'] == PARTY_TYPE_DOMESTIC_ID) { ?>
                        <b>GST No. : <?= $party_data['gst_no']; ?></b><?= isset($party_data['party_cin_no']) && !empty($party_data['party_cin_no']) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$party_data['party_cin_no'].'</b>' : ''; ?>
                    <?php } ?>
                </td>
                <td class="text-left text-bold" colspan="3">Call Received By</td>
                <td class="text-left text-bold" colspan="2"><b><?=$feedback_data->feedback_call_received_by;?></b></td>
                <td class="text-left text-bold" colspan="1"><b><?=$feedback_data->feedback_call_received_by_contact;?></b></td>
            </tr>
            <tr>
                <td class="text-left text-bold" colspan="3">Technical Operator Name</td>
                <td class="text-left text-bold" colspan="2"><b><?=$feedback_data->technical_operator_name;?></b></td>
                <td class="text-left text-bold" colspan="1"><b><?=$feedback_data->technical_operator_contact;?></b></td>
            </tr>
            <tr>
                <td class=" text-bold " colspan="6"><b>Model &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Item Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Equipment Serial No </b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="6"><b>
                    <?php
                        if(!empty($challan_items_data)){
                            foreach($challan_items_data as $challan_item){
                                echo $challan_item->item_code .'&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;'. $challan_item->item_name .'&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;'. $challan_item->item_serial_no .'<br />';
                            }
                        }
                    ?></b><br/><br/>
                </td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="11"><b>Customer Satisfaction Survey</b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="8"><b>Question Box</b></td>
                <td class="text-center text-bold" colspan="1"><b>Client Feedback</b></td>
                <td class="text-center text-bold" colspan="2"><b>Remark</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="8">Hello Sir, How are you?</b></td>
                <td class="text-center text-bold" colspan="1"><b><?=$feedback_data->how_are_you;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">I wanted to speak to you regarding the feedbback and quality of the plant. Can we talk right now?</td>
                <td class="text-bold text-center" colspan="1"><b><?php if($feedback_data->can_we_talk == '1') { echo 'Yes'; } else if($feedback_data->can_we_talk == '2') { echo 'No'; } else if($feedback_data->can_we_talk == '3') { echo 'After'; } ?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->can_we_talk_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Are you satisfied with the quality of our machine?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->satisfied_with_quality) && $feedback_data->satisfied_with_quality != 0)? ($feedback_data->satisfied_with_quality == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->satisfied_with_quality_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did you receive the delivery of the machinery on time?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->delivery_on_time) && $feedback_data->delivery_on_time != 0)? ($feedback_data->delivery_on_time == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->delivery_on_time_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did our technician come on time at your place to install the machine?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->technician_come_on_time) && $feedback_data->technician_come_on_time != 0)? ($feedback_data->technician_come_on_time == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->technician_come_on_time_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did our technician properly explain you and your technical person how to operate the maachine and how to change the spare parts of the machine?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->technician_properly_explain) && $feedback_data->technician_properly_explain != 0)? ($feedback_data->technician_properly_explain == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->technician_properly_explain_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did our technician ask for money from you to start the machine or to install the machine?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->ask_money_for_install;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did our technician install the machinery properly?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->install_machine_properly) && $feedback_data->install_machine_properly != 0)? ($feedback_data->install_machine_properly == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->install_machine_properly_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Which raw material are you using and how much is the moisture content in your raw material?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->raw_material_using;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Does the machinery give you proper production? How much is your production?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->give_proper_production;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Has our technician properly done service of your machinery?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->properly_done_service) && $feedback_data->properly_done_service != 0)? ($feedback_data->properly_done_service == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->properly_done_service_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did our technician visit you on time after receiving your complain?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->visit_on_time) && $feedback_data->visit_on_time != 0)? ($feedback_data->visit_on_time == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->visit_on_time_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Were all the problems of your machinery solved? Could you understand why were the problems arised?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->were_problems_arised) && $feedback_data->were_problems_arised != 0)? ($feedback_data->were_problems_arised == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->were_problems_arised_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Do you have the idea regarding the problem  in your machinery for which you have called our technician?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->idea_regarding_problem;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">How was the behaviour of our technician with you and your technical team?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->behaviour_of_technician;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did our technician ask for any type of facility from you?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->ask_for_facility) && $feedback_data->ask_for_facility != 0)? ($feedback_data->ask_for_facility == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->ask_for_facility_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Did our technician ask for money from you? </td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->ask_for_money;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Have you given our technician pick up and drop facility?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->pick_up_facility) && $feedback_data->pick_up_facility != 0)? ($feedback_data->pick_up_facility == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->pick_up_facility_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">How did you find the work performance of our technician?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->work_perfomance;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">When our technician came at your place, did he worked himself or did he got the work done from your technician?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->when_technician_came;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">How did you find the support of our office staff in regard of technical support?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->support_of_staff;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Were you able to understand the technical problem of your machinery which our office staff tried to solve?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->understand_technical_problem;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Was our technician able to solve your machinery problem from the office itself?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->able_to_solve) && $feedback_data->able_to_solve != 0)? ($feedback_data->able_to_solve == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->able_to_solve_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Are you satisfied after purchasing our product?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->satisfied_our_product) && $feedback_data->satisfied_our_product != 0)? ($feedback_data->satisfied_our_product == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->satisfied_our_product_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Will you do business with jay Khodiyar Group in future if you plan to buy more machines?</td>
                <td class="text-bold text-center" colspan="1"><b><?=(!empty($feedback_data->business_with_jk) && $feedback_data->business_with_jk != 0)? ($feedback_data->business_with_jk == '1') ? 'Yes' : 'No' : '';?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$feedback_data->business_with_jk_remark;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Do you wish to say something for Jay Khodiyar products?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->say_something_for_product;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">Do you wish to say something for the team of jay Khodiyar?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->say_something_for_team;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="8">How did you find the behaviour of Jay Khodiyar team towards you?</td>
                <td class="text-bold text-center" colspan="1"><b><?=$feedback_data->behaviour_of_jk;?></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="11"><b>Any suggestions or complains regarding Jay Khodiyar Group, the team or the technician? </b><br/><?= nl2br($feedback_data->any_suggestion);?><br/></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="6" width="150px"><?= $party_data['party_name'] ?></td>
                <td class="text-center text-bold" colspan="5" width="310px"><?=$company_details['name'];?></td>
            </tr>
            <tr>
                <td class="text-center text-bold footer-sign-area" colspan="6" width="150px"> &nbsp; </td>
                <td class="text-center text-bold footer-sign-area" colspan="5" width="310px"> &nbsp; </td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="6" width="150px"><?=$feedback_data->feedback_call_received_by;?></td>
                <td class="text-center text-bold" colspan="5" width="310px"><?=$feedback_data->feedback_call_by;?></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="6" width="150px"><b>Authorized Signature Stamp</b></td>
                <td class="text-center text-bold" colspan="5" width="310px"><b>Authorized Signature Stamp</b></td>
            </tr> 

        </table>

    </body>
</html>