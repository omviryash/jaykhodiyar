<?php $this->load->view('success_false_notify'); ?>
<style>
    /*label span { font-weight: bold; }*/
    label.display_value_in_label { border-radius: 0; box-shadow: none; border: 1px solid #d2d6de; background-color: #eee;}
</style>
<div class="content-wrapper">
    <form class="form-horizontal" action="" method="post" id="save_feedback" novalidate>
        <?php if (isset($feedback_data->feedback_id) && !empty($feedback_data->feedback_id)) { ?>
            <input type="hidden" name="feedback_data[feedback_id]" id="feedback_id" value="<?= $feedback_data->feedback_id ?>">
        <?php } ?>
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Service : Feedback</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, Ctrl+S = Save</label></small>
                <?php
                $add_role = $this->app_model->have_access_role(FEEDBACK_MODULE_ID, "add");
                $edit_role = $this->app_model->have_access_role(FEEDBACK_MODULE_ID, "edit");
                $view_role = $this->app_model->have_access_role(FEEDBACK_MODULE_ID, "view");
                ?>
                <?php if ($add_role || $edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
                <?php endif; ?>
                <?php if ($view_role): ?>
                    <a href="<?= base_url() ?>feedback/feedback_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Feedback List</a>
                <?php endif; ?>
                <?php if ($add_role): ?>
                    <a href="<?= base_url() ?>feedback/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Feedback</a>
                <?php endif; ?>
                <span class="pull-right" style="margin-right: 20px;">
                    <div class="form-group">
                        <label for="is_received_post" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                            <input type="checkbox" name="feedback_data[send_sms]" id="send_sms" class="send_sms" <?= (isset($feedback_data->feedback_id) && !empty($feedback_data->feedback_id)) ? '' : 'checked=""'; ?> >  &nbsp; Send SMS
                        </label>
                    </div>
                </span>
            </h1>
        </section>
        <div class="clearfix">
            <?php if ($add_role || $edit_role): ?>
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Feedback Details</a></li>
                            <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                            <?php if (isset($feedback_data->feedback_id) && !empty($feedback_data->feedback_id)) { ?>
                                <li><a href="#tab_3" data-toggle="tab" id="tabs_3">Login</a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 input-sm">Feedback No</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control col-sm-12 input-sm" value="<?= (isset($feedback_data->feedback_no_year)) ? $feedback_data->feedback_no_year : ''; ?>" readonly=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="feedback_date" class="col-sm-3 input-sm">Feedback Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="feedback_data[feedback_date]" id="feedback_date" class="form-control input-sm input-datepicker" value="<?= (isset($feedback_data->feedback_date)) ? date('d-m-Y', strtotime($feedback_data->feedback_date)) : date('d-m-Y'); ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Party Detail </legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="feedback_data[party_id]" id="party_id" class="form-control input-sm" onchange="party_details(this.value)"></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-3 input-sm">Address</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="city" class="col-sm-3 input-sm">City</label>
                                                        <div class="col-sm-9">
                                                            <div class="col-md-6" style="padding:0px !important;">
                                                                <select name="party[city_id]" id="city" class="form-control input-sm select2" disabled=""></select>
                                                            </div>
                                                            <div class="col-md-6" style="padding-right:0px !important;">
                                                                <input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="state" class="col-sm-3 input-sm">State</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="country" class="col-sm-3 input-sm">Country</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
                                                            <small>Add multiple email id by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
                                                            <small>Add multiple phone number by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_website" class="col-sm-3 input-sm">Website</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm website" id="party_website" name="party[website]" disabled="">
                                                        </div>
                                                    </div>  
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Designation</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_designation"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Department</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_department"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="" disabled="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Select Installation Items</legend>
                                            <div class="col-md-12 installation_items_section"></div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold">Feedback Calling By</legend>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="feedback_call_by" class="col-sm-4 input-sm">Feedback Call By<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-4">
                                                        <select name="feedback_data[feedback_call_by]" id="feedback_call_by" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="feedback_data[feedback_call_by_contact]" id="feedback_call_by_contact" class=" form-control input-sm" value="<?= (isset($feedback_data->feedback_call_by_contact)) ? $feedback_data->feedback_call_by_contact : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold">Call Receiver By</legend>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="feedback_call_received_by" class="col-sm-4 input-sm">Call Received By<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="feedback_data[feedback_call_received_by]" id="feedback_call_received_by" class=" form-control input-sm" value="<?= (isset($feedback_data->feedback_call_received_by)) ? $feedback_data->feedback_call_received_by : ''; ?>">
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="feedback_data[feedback_call_received_by_contact]" id="feedback_call_received_by_contact" class=" form-control input-sm" value="<?= (isset($feedback_data->feedback_call_received_by_contact)) ? $feedback_data->feedback_call_received_by_contact : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="technical_operator_name" class="col-sm-4 input-sm">Technical Operator Name<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="feedback_data[technical_operator_name]" id="technical_operator_name" class=" form-control input-sm" value="<?= (isset($feedback_data->technical_operator_name)) ? $feedback_data->technical_operator_name : ''; ?>">
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="feedback_data[technical_operator_contact]" id="technical_operator_contact" class=" form-control input-sm" value="<?= (isset($feedback_data->technical_operator_contact)) ? $feedback_data->technical_operator_contact : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Customer Satisfaction Survey</legend>
                                            <div class="row">
                                            <div class="col-md-12">
                                                <label for="how_are_you" class="col-md-7 input-sm">Hello Sir, How are you?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[how_are_you]" id="how_are_you" class="form-control input-sm" value="<?= (isset($feedback_data->how_are_you)) ? $feedback_data->how_are_you : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="can_we_talk" class="col-md-7 input-sm">I wanted to speak to you regarding the feedback and quality of the plant. Can we talk right now? </label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[can_we_talk]" id="can_we_talk" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->can_we_talk) && $feedback_data->can_we_talk == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->can_we_talk) && $feedback_data->can_we_talk == '2') ? ' Selected ' : ''; ?>> No </option>
                                                        <option value="3" <?= (isset($feedback_data->can_we_talk) && $feedback_data->can_we_talk == '3') ? ' Selected ' : ''; ?>> After </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[can_we_talk_remark]" id="can_we_talk_remark" class="form-control input-sm" value="<?= (isset($feedback_data->can_we_talk_remark)) ? $feedback_data->can_we_talk_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="satisfied_with_quality" class="col-md-7 input-sm">Are you satisfied with the quality of our machine?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[satisfied_with_quality]" id="satisfied_with_quality" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->satisfied_with_quality) && $feedback_data->satisfied_with_quality == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->satisfied_with_quality) && $feedback_data->satisfied_with_quality == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[satisfied_with_quality_remark]" id="satisfied_with_quality_remark" class="form-control input-sm" value="<?= (isset($feedback_data->satisfied_with_quality_remark)) ? $feedback_data->satisfied_with_quality_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="delivery_on_time" class="col-md-7 input-sm">Did you receive the delivery of the machinery on time?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[delivery_on_time]" id="delivery_on_time" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->delivery_on_time) && $feedback_data->delivery_on_time == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->delivery_on_time) && $feedback_data->delivery_on_time == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[delivery_on_time_remark]" id="delivery_on_time_remark" class="form-control input-sm" value="<?= (isset($feedback_data->delivery_on_time_remark)) ? $feedback_data->delivery_on_time_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="technician_come_on_time" class="col-md-7 input-sm">Did our technician come on time at your place to install the machine?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[technician_come_on_time]" id="technician_come_on_time" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->technician_come_on_time) && $feedback_data->technician_come_on_time == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->technician_come_on_time) && $feedback_data->technician_come_on_time == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[technician_come_on_time_remark]" id="technician_come_on_time_remark" class="form-control input-sm" value="<?= (isset($feedback_data->technician_come_on_time_remark)) ? $feedback_data->technician_come_on_time_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="technician_properly_explain" class="col-md-7 input-sm">Did our technician properly explain you and your technical person how to operate the maachine and how to change the spare parts of the machine? </label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[technician_properly_explain]" id="technician_properly_explain" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->technician_properly_explain) && $feedback_data->technician_properly_explain == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->technician_properly_explain) && $feedback_data->technician_properly_explain == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[technician_properly_explain_remark]" id="technician_properly_explain_remark" class="form-control input-sm" value="<?= (isset($feedback_data->technician_properly_explain_remark)) ? $feedback_data->technician_properly_explain_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br/>
                                            <div class="col-md-12">
                                                <label for="ask_money_for_install" class="col-md-7 input-sm">Did our technician ask for money from you to start the machine or to install the machine?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[ask_money_for_install]" id="ask_money_for_install" class="form-control input-sm" value="<?= (isset($feedback_data->ask_money_for_install)) ? $feedback_data->ask_money_for_install : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="install_machine_properly" class="col-md-7 input-sm">Did our technician install the machinery properly?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[install_machine_properly]" id="install_machine_properly" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->install_machine_properly) && $feedback_data->install_machine_properly == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->install_machine_properly) && $feedback_data->install_machine_properly == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[install_machine_properly_remark]" id="install_machine_properly_remark" class="form-control input-sm" value="<?= (isset($feedback_data->install_machine_properly_remark)) ? $feedback_data->install_machine_properly_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="raw_material_using" class="col-md-7 input-sm">Which raw material are you using and how much is the moisture content in your raw material?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[raw_material_using]" id="raw_material_using" class="form-control input-sm" value="<?= (isset($feedback_data->raw_material_using)) ? $feedback_data->raw_material_using : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="give_proper_production" class="col-md-7 input-sm">Does the machinery give you proper production? How much is your production?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[give_proper_production]" id="give_proper_production" class="form-control input-sm" value="<?= (isset($feedback_data->give_proper_production)) ? $feedback_data->give_proper_production : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="properly_done_service" class="col-md-7 input-sm">Has our technician properly done service of your machinery? </label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[properly_done_service]" id="properly_done_service" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->properly_done_service) && $feedback_data->properly_done_service == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->properly_done_service) && $feedback_data->properly_done_service == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[properly_done_service_remark]" id="properly_done_service_remark" class="form-control input-sm" value="<?= (isset($feedback_data->properly_done_service_remark)) ? $feedback_data->properly_done_service_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="visit_on_time" class="col-md-7 input-sm">Did our technician visit you on time after receiving your complain?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[visit_on_time]" id="visit_on_time" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->visit_on_time) && $feedback_data->visit_on_time == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->visit_on_time) && $feedback_data->visit_on_time == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[visit_on_time_remark]" id="visit_on_time_remark" class="form-control input-sm" value="<?= (isset($feedback_data->visit_on_time_remark)) ? $feedback_data->visit_on_time_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="were_problems_arised" class="col-md-7 input-sm">Were all the problems of your machinery solved? Could you understand why were the problems arised?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[were_problems_arised]" id="were_problems_arised" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->were_problems_arised) && $feedback_data->were_problems_arised == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->were_problems_arised) && $feedback_data->were_problems_arised == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[were_problems_arised_remark]" id="were_problems_arised_remark" class="form-control input-sm" value="<?= (isset($feedback_data->were_problems_arised_remark)) ? $feedback_data->were_problems_arised_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="idea_regarding_problem" class="col-md-7 input-sm">Do you have the idea regarding the problem in your machinery for which you have called our technician?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[idea_regarding_problem]" id="idea_regarding_problem" class="form-control input-sm" value="<?= (isset($feedback_data->idea_regarding_problem)) ? $feedback_data->idea_regarding_problem : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="behaviour_of_technician" class="col-md-7 input-sm">How was the behaviour of our technician with you and your technical team? </label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[behaviour_of_technician]" id="behaviour_of_technician" class="form-control input-sm customer_feedback"></select>
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="ask_for_facility" class="col-md-7 input-sm">Did our technician ask for any type of facility from you?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[ask_for_facility]" id="ask_for_facility" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->ask_for_facility) && $feedback_data->ask_for_facility == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->ask_for_facility) && $feedback_data->ask_for_facility == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[ask_for_facility_remark]" id="ask_for_facility_remark" class="form-control input-sm" value="<?= (isset($feedback_data->ask_for_facility_remark)) ? $feedback_data->ask_for_facility_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="ask_for_money" class="col-md-7 input-sm">Did our technician ask for money from you?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[ask_for_money]" id="ask_for_money" class="form-control input-sm" value="<?= (isset($feedback_data->ask_for_money)) ? $feedback_data->ask_for_money : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="pick_up_facility" class="col-md-7 input-sm">Have you given our technician pick up and drop facility?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[pick_up_facility]" id="pick_up_facility" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->pick_up_facility) && $feedback_data->pick_up_facility == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->pick_up_facility) && $feedback_data->pick_up_facility == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[pick_up_facility_remark]" id="pick_up_facility_remark" class="form-control input-sm" value="<?= (isset($feedback_data->pick_up_facility_remark)) ? $feedback_data->pick_up_facility_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="work_perfomance" class="col-md-7 input-sm">How did you find the work performance of our technician?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[work_perfomance]" id="work_perfomance" class="form-control input-sm customer_feedback"></select>
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="when_technician_came" class="col-md-7 input-sm">When our technician came at your place, did he worked himself or did he got the work done from your technician?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[when_technician_came]" id="when_technician_came" class="form-control input-sm" value="<?= (isset($feedback_data->when_technician_came)) ? $feedback_data->when_technician_came : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br/>
                                            <div class="col-md-12">
                                                <label for="support_of_staff" class="col-md-7 input-sm">How did you find the support of our office staff in regard of technical support?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[support_of_staff]" id="support_of_staff" class="form-control input-sm customer_feedback"></select>
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="understand_technical_problem" class="col-md-7 input-sm">Were you able to understand the technical problem of your machinery which our office staff tried to solve?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[understand_technical_problem]" id="understand_technical_problem" class="form-control input-sm customer_feedback"></select>
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="able_to_solve" class="col-md-7 input-sm">Was our technician able to solve your machinery problem from the office itself?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[able_to_solve]" id="able_to_solve" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->able_to_solve) && $feedback_data->able_to_solve == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->able_to_solve) && $feedback_data->able_to_solve == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[able_to_solve_remark]" id="able_to_solve_remark" class="form-control input-sm" value="<?= (isset($feedback_data->able_to_solve_remark)) ? $feedback_data->able_to_solve_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="satisfied_our_product" class="col-md-7 input-sm">Are you satisfied after purchasing our product? </label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[satisfied_our_product]" id="satisfied_our_product" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->satisfied_our_product) && $feedback_data->satisfied_our_product == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->satisfied_our_product) && $feedback_data->satisfied_our_product == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[satisfied_our_product_remark]" id="satisfied_our_product_remark" class="form-control input-sm" value="<?= (isset($feedback_data->satisfied_our_product_remark)) ? $feedback_data->satisfied_our_product_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="business_with_jk" class="col-md-7 input-sm">Will you do business with jay Khodiyar Group in future if you plan to buy more machines?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[business_with_jk]" id="business_with_jk" class="form-control input-sm select2">
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($feedback_data->business_with_jk) && $feedback_data->business_with_jk == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($feedback_data->business_with_jk) && $feedback_data->business_with_jk == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="feedback_data[business_with_jk_remark]" id="business_with_jk_remark" class="form-control input-sm" value="<?= (isset($feedback_data->business_with_jk_remark)) ? $feedback_data->business_with_jk_remark : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="say_something_for_product" class="col-md-7 input-sm">Do you wish to say something for Jay Khodiyar products?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[say_something_for_product]" id="say_something_for_product" class="form-control input-sm" value="<?= (isset($feedback_data->say_something_for_product)) ? $feedback_data->say_something_for_product : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="say_something_for_team" class="col-md-7 input-sm">Do you wish to say something for the team of jay Khodiyar?</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="feedback_data[say_something_for_team]" id="say_something_for_team" class="form-control input-sm" value="<?= (isset($feedback_data->say_something_for_team)) ? $feedback_data->say_something_for_team : ''; ?>">
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="behaviour_of_jk" class="col-md-7 input-sm">How did you find the behaviour of Jay Khodiyar team towards you?</label>
                                                <div class="col-md-2">
                                                    <select name="feedback_data[behaviour_of_jk]" id="behaviour_of_jk" class="form-control input-sm customer_feedback"></select>
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>
                                            </div>
                                            <label for="any_suggestion" class="col-md-12 input-sm">Any suggestions or complains regarding Jay Khodiyar Group, the team or the technician :</label>
                                            <div class="col-md-12">
                                                <textarea name="feedback_data[any_suggestion]" id="any_suggestion" class="form-control" rows="4"><?= (isset($feedback_data->any_suggestion)) ? $feedback_data->any_suggestion : ''; ?></textarea>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Installation Detail</legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Installation No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="installation_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Installation Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="installation_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Testing Report No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="testing_report_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Testing Report Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="testing_report_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Challan No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="challan_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Challan Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="challan_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="invoice_no" class="col-sm-5 input-sm">Invoice No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="invoice_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="invoice_date" class="col-sm-5 input-sm">Invoice Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="invoice_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="quotation_no" class="col-sm-5 input-sm">Quotation No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="quotation_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="quotation_date" class="col-sm-5 input-sm">Quotation Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="quotation_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="purchase_order_no" class="col-sm-5 input-sm">Purchase Order No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="cust_po_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="purchase_order_date" class="col-sm-5 input-sm">Purchase Order Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="po_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_no" class="col-sm-5 input-sm">Sales Order No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="sales_order_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_date" class="col-sm-5 input-sm">Sales Order Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="sales_order_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="manufacturing_m_y" class="col-sm-5 input-sm">Manufacturing Month & Year</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="manufacturing_m_y"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="delivery_of_equipment" class="col-sm-5 input-sm">Delivery of Equipment</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="delivery_of_equipment"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="installation_commissioning_by" class="col-sm-5 input-sm">Installation & Commissioning By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="installation_commissioning_by"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="installation_commissioning_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="jk_technical_person" class="col-sm-5 input-sm">JK Technical Person</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="jk_technical_person"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="jk_technical_person_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="project_training_provide_by" class="col-sm-5 input-sm">Project Training Provide By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="project_training_provide_by"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="project_training_provide_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_site_incharge_person" class="col-sm-5 input-sm">Customer Site Incharge Person</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_site_incharge_person"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_site_incharge_person_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_site_quality_checked_by_name" class="col-sm-5 input-sm">Customer Site Quality Checked By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_site_quality_checked_by_name"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_site_quality_checked_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_technical_operator_person" class="col-sm-5 input-sm">Customer Technical Operator Person</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_technical_operator_person"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_technical_operator_person_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_site_equipment_inspection_by_name" class="col-sm-5 input-sm">Customer Site Equipment Inspe. By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_site_equipment_inspection_by_name"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_site_equipment_inspection_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                            <div class="col-md-6">
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_name" class="col-sm-3 input-sm">Equipment Name</label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_name"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_code"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_description"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="quantity" class="col-sm-3 input-sm">Quantity</label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="quantity"></span></label>
                                                    <label for="item_extra_accessories_id" class="col-sm-3 input-sm text-right">Item Extra Accessories</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="item_extra_accessories_id"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-3 input-sm"> Item Serial No. </label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_serial_no"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Customer Premises</legend>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="land_building_factory_area" class="col-sm-6 input-sm" style="padding: 2px 9px !important;">Land Building & Factory Area</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="land_building_factory_area"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="plant_room_size" class="col-sm-6 input-sm">Plant Room Size</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="plant_room_size"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="prepared_foundation" class="col-sm-6 input-sm">Prepared Foundation</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="prepared_foundation"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="foundation_marking_by" class="col-sm-6 input-sm">Foundation Marking By</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="foundation_marking_by"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="no_of_foundation" class="col-sm-6 input-sm">No of Foundation</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="no_of_foundation"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="unloading_date" class="col-sm-6 input-sm">Unloading Date</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="unloading_date"></span></label>
                                                </div>
                                            </div>    
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="filling_of_foundation_boxes" class="col-sm-6 input-sm">Filling of Foundation Boxes</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="filling_of_foundation_boxes"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="installation_start" class="col-sm-6 input-sm">Installation Start</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="installation_start"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="installation_finished" class="col-sm-6 input-sm">Installation Finished</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="installation_finished"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">    
                                                <div class="form-group">
                                                    <label for="electric_power" class="col-sm-6 input-sm">Electric Power</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electric_power"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="electric_connection" class="col-sm-6 input-sm">Electric Connection</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electric_connection"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="elec_rm2_panel_board_cable" class="col-sm-6 input-sm">Elec Rm 2 Panel Board Cable</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="elec_rm2_panel_board_cable"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="connection_cable_1" class="col-sm-5 input-sm" style="">All Connection Cable</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="connection_cable_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="connection_cable_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-5 input-sm display_value_in_label"><span id="water_tank_chiller_system_label"></span></label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="water_tank_chiller_system_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="water_tank_chiller_system_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-5 input-sm display_value_in_label"><span id="water_plumbing_chiller_conn_label"></span></label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="water_plumbing_chiller_conn_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="water_plumbing_chiller_conn_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="lubrication_gear_oil_1" class="col-sm-5 input-sm">Lubrication Gear Oil</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="lubrication_gear_oil_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="lubrication_gear_oil_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="gear_box_oil_1" class="col-sm-5 input-sm">Gear Box Oil</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="gear_box_oil_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="gear_box_oil_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="raw_material_1" class="col-sm-5 input-sm">Raw Material</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="raw_material_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="raw_material_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="run_start_date" class="col-sm-6 input-sm">No Load Free Run Start Date</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="run_start_date"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="run_hrs" class="col-sm-6 input-sm">No Load Total Free Run Hrs</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="run_hrs"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="production_start" class="col-sm-6 input-sm">Production Start</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="production_start"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name_of_raw_material" class="col-sm-6 input-sm">Name of Raw Material</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="name_of_raw_material"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="moisture_of_raw_material" class="col-sm-6 input-sm">Moisture of Raw Material</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="moisture_of_raw_material"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="after_drying_moisture_content" class="col-sm-6 input-sm" style="padding: 2px 7px !important;">After Drying Moisture Content</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="after_drying_moisture_content"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="approx_production_per_hr" class="col-sm-6 input-sm">Approx Production Per Hr</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="approx_production_per_hr"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="product_quality" class="col-sm-6 input-sm">Product Quality</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="product_quality"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="electric_consumption_unit_hr" class="col-sm-6 input-sm" style="padding: 2px 7px !important;">Electric Consumption Unit/Hr</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electric_consumption_unit_hr"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="electricity_rate_per_unit" class="col-sm-6 input-sm">Electricity Rate Per Unit</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electricity_rate_per_unit"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="unskilled_labour" class="col-sm-6 input-sm">Unskilled Labour</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="unskilled_labour"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="skilled_labour" class="col-sm-6 input-sm">Skilled Labour</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="skilled_labour"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="tractor_loader" class="col-sm-6 input-sm">Do you have tractor loader?</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="tractor_loader"></span></label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Electric Motor & Equipment Testing Report At Customer's Premises</legend>
                                            <div class="col-md-12">
                                                <div class="table-responsive">          
                                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th><strong>User</strong></th>
                                                                <th><strong>Make</strong></th>
                                                                <th><strong>HP</strong></th>
                                                                <th><strong>KW</strong></th>
                                                                <th><strong>Frequency</strong></th>
                                                                <th><strong>RPM</strong></th>
                                                                <th><strong>Volts</strong></th>
                                                                <th><strong>Serial No.</strong></th>
                                                                <th><strong>Normal Amps</strong></th>
                                                                <th><strong>Working Amps</strong></th>
                                                                <th><strong>Equipment RPM</strong></th>
                                                                <th><strong>Total WorkingHours</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="motor_testing_details_c"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="table-responsive">          
                                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th><strong>User</strong></th>
                                                                <th><strong>Make</strong></th>
                                                                <th><strong>Gear Type</strong></th>
                                                                <th><strong>Model</strong></th>
                                                                <th><strong>Ratio</strong></th>
                                                                <th><strong>Serial No.</strong></th>
                                                                <th><strong>Normal Amps</strong></th>
                                                                <th><strong>Working Amps</strong></th>
                                                                <th><strong>Equipment RPM</strong></th>
                                                                <th><strong>Total WorkingHours</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="gearbox_testing_details_c"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Equipment Testing Normal & complete production Report at Customer's Premises</legend>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-2 input-sm"><strong>Testing Capacity</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Capacity Hrs</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Test of Raw Material</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Moisture of Raw Material</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>After Process of Moisture</strong></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-2 input-sm">Testing Normal Load</label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_capacity_hrs"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_test_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_moisture_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_after_procces_moisture"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-2 input-sm">Testing Full Load</label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_capacity_hrs"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_test_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_moisture_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_after_procces_moisture"></span></label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold"> Temperature Parameters At Customer's Premises</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_temp_oli" class="col-sm-6 input-sm">Oil Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_oli"></span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="c_temp_eqipment" class="col-sm-6 input-sm">Equipment Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_eqipment"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_temp_head" class="col-sm-6 input-sm">Head Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_head"></span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="c_temp_kiln" class="col-sm-6 input-sm">Kiln Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_kiln"></span></label>
                                                    </div>
                                                </div>    
                                                <div class="col-md-4">    
                                                    <div class="form-group">
                                                        <label for="c_temp_hot_air" class="col-sm-6 input-sm">Hot Air Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_hot_air"></span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="c_temp_weather" class="col-sm-6 input-sm">weather</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_weather"></span></label>
                                                    </div>
                                                </div>											
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Equipment Testing</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_et_equipment_rpm" class="col-sm-6 input-sm">Equipment RPM</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_et_equipment_rpm"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_et_testing" class="col-sm-6 input-sm">Testing</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_et_testing"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_et_stop_run_time" class="col-sm-6 input-sm">Stop Run Time (Minutes)</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_et_stop_run_time"></span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Control Panel Board</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_cpb_testing" class="col-sm-6 input-sm">Testing</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_cpb_testing"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_cpb_working_hours" class="col-sm-6 input-sm">Working Hours</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_cpb_working_hours"></span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_3">
                                <?php if (isset($feedback_data->feedback_id) && !empty($feedback_data->feedback_id)) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_by" id="created_by" value="<?= (isset($feedback_data->created_by_name)) ? $feedback_data->created_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_at" id="created_at" value="<?= (isset($feedback_data->created_at)) ? $feedback_data->created_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_by" id="updated_by" value="<?= (isset($feedback_data->updated_by_name)) ? $feedback_data->updated_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_at" id="updated_at" value="<?= (isset($feedback_data->updated_at)) ? $feedback_data->updated_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
            <?php endif; ?>
        </div>
        <section class="content-header">
            <?php if ($add_role || $edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
            <?php endif; ?>
            <?php if ($view_role): ?>
                <a href="<?= base_url() ?>feedback/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Feedback List</a>
            <?php endif; ?>
            <?php if ($add_role): ?>
                <a href="<?= base_url() ?>feedback/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Feedback</a>
            <?php endif; ?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('input[type="checkbox"].send_sms').iCheck({
            checkboxClass: 'icheckbox_flat-green',
        });

        initAjaxSelect2($("#city"), "<?= base_url('app/city_select2_source') ?>");
        initAjaxSelect2($("#state"), "<?= base_url('app/state_select2_source') ?>");
        initAjaxSelect2($("#country"), "<?= base_url('app/country_select2_source') ?>");
        initAjaxSelect2($("#reference_id"), "<?= base_url('app/reference_select2_source') ?>");
        initAjaxSelect2($("#party_id"), "<?= base_url('app/instalation_party_select2_source') ?>");
        <?php if (isset($feedback_data->party_id)) { ?>
            setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id/' . $feedback_data->party_id) ?>");
            party_details(<?= $feedback_data->party_id; ?>);
            get_installation_with_item_list(<?= $feedback_data->party_id; ?>);
        <?php } ?>
        initAjaxSelect2($("#feedback_call_by"), "<?= base_url('app/staff_select2_source') ?>");
        <?php if (isset($feedback_data->feedback_call_by)) { ?>
            setSelect2Value($("#feedback_call_by"), "<?= base_url('app/set_staff_select2_val_by_id/' . $feedback_data->feedback_call_by) ?>");
        <?php } ?>
        initAjaxSelect2($(".customer_feedback"), "<?= base_url('app/customer_feedback_select2_source') ?>");
        <?php if (isset($feedback_data->behaviour_of_technician)) { ?>
            setSelect2Value($("#behaviour_of_technician"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $feedback_data->behaviour_of_technician) ?>");
        <?php } ?>
        <?php if (isset($feedback_data->work_perfomance)) { ?>
            setSelect2Value($("#work_perfomance"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $feedback_data->work_perfomance) ?>");
        <?php } ?>
        <?php if (isset($feedback_data->support_of_staff)) { ?>
            setSelect2Value($("#support_of_staff"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $feedback_data->support_of_staff) ?>");
        <?php } ?>
        <?php if (isset($feedback_data->understand_technical_problem)) { ?>
            setSelect2Value($("#understand_technical_problem"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $feedback_data->understand_technical_problem) ?>");
        <?php } ?>
        <?php if (isset($feedback_data->behaviour_of_jk)) { ?>
            setSelect2Value($("#behaviour_of_jk"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $feedback_data->behaviour_of_jk) ?>");
        <?php } ?>

        $(document).on('change', "#party_id", function () {
            var id = $(this).val();
            $('label.display_value_in_label span').html('');
            $('tbody#motor_testing_details_c').html('');
            $('tbody#gearbox_testing_details_c').html('');
            if(id != '' && id != null){
                get_installation_with_item_list(id);
            }
        });

        $(document).on('change', ".chk_challan_item", function () {
            if ($('.chk_challan_item:checked').data('installation_id')) {
                var installation_id = $('.chk_challan_item:checked').data('installation_id');
                var testing_report_id = $('.chk_challan_item:checked').data('testing_report_id');
                var challan_id = $('.chk_challan_item:checked').data('challan_id');
                feed_installation_data(installation_id, testing_report_id, challan_id);
            } else {
                $('label.display_value_in_label span').html('');
                $('tbody#motor_testing_details_c').html('');
                $('tbody#gearbox_testing_details_c').html('');
            }
        });

        $(document).on("change", '#contact_person_id', function () {
            var contact_person_id = $(this).val();
            $.ajax({
                url: "<?= base_url(); ?>party/get-contact-person-by-id/" + contact_person_id,
                type: "POST",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
                        $('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
                        $('#contact_person_designation').html(data.contact_person_data.designation);
                        $('#contact_person_department').html(data.contact_person_data.department);
                    } else {
                        $('input[name="contact_person[contact_person_mobile_no]"]').val('');
                        $('input[name="contact_person[contact_person_email_id]"]').val('');
                        $('#contact_person_designation').html('');
                        $('#contact_person_department').html('');
                    }
                }
            });
        });

        $(document).on("change", '#feedback_call_by, #technical_support_by', function () {
            var id = $(this).val();
            var id_attr = $(this).attr('id');
            if (id != '' && id != null) {
                $.ajax({
                    url: "<?= base_url(); ?>testing_report/get_staff_contact_no/" + id,
                    type: "POST",
                    success: function (data) {
                        $('#' + id_attr + '_contact').val('');
                        $('#' + id_attr + '_contact').val(data);
                    }
                });
            }
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_feedback").submit();
                return false;
            }
        });
        $(document).on('submit', '#save_feedback', function () {
            if ($.trim($("#party_id").val()) == '') {
                show_notify('Please Select Party.', false);
                $("#party_id").focus();
                return false;
            }
            
            if ($("input.chk_challan_item:checked").length == 0) {
                show_notify('Please select at least one item.', false);
                return false;
            }

            if ($.trim($("#feedback_call_by").val()) == '') {
                show_notify('Please Select Feedback Call By Name.', false);
                $("#feedback_call_by").focus();
                return false;
            }

            if ($.trim($("#feedback_call_received_by").val()) == '') {
                show_notify('Please Enter Feedback Call Received By.', false);
                $("#feedback_call_received_by").focus();
                return false;
            }

            if ($.trim($("#technical_operator_name").val()) == '') {
                show_notify('Please Enter Technical Operator Name.', false);
                $("#technical_operator_name").focus();
                return false;
            }

            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
            $("input.chk_challan_item:checked").each(function (index, value) {
                postData.append('feedback_items[' + index + '][installation_id]', $(this).data('installation_id'));
                postData.append('feedback_items[' + index + '][testing_report_id]', $(this).data('testing_report_id'));
                postData.append('feedback_items[' + index + '][challan_id]', $(this).data('challan_id'));
            });
            $.ajax({
                url: "<?= base_url('feedback/save_feedback') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.status == 0) {
                        show_notify(json.msg, false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                        return false;
                    }
                    if (json['success'] == 'false') {
                        show_notify(json['msg'], false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                    }
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('feedback/feedback_list') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('feedback/feedback_list') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });

    });

    function feed_installation_data(installation_id, testing_report_id, challan_id) {
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>complain/get_installation_details',
            type: "POST",
            async: false,
            data: {installation_id: installation_id, testing_report_id: testing_report_id, challan_id: challan_id},
            success: function (data) {
                json = JSON.parse(data);
                //console.log(json);
                var installation_data = json.installation_data;
                var testing_report_data = json.testing_report_data;
                var challan_data = json.challan_data;
                var challan_item = json.challan_item_data;

                $.each(installation_data, function (index, value) {
                    if (index != 'party_id') {
                        $("#" + index).html(value);
                    }
                });
                $.each(testing_report_data, function (index, value) {
                    if (index != 'party_id') {
                        $("#" + index).html(value);
                    }
                });
                $.each(challan_data, function (index, value) {
                    if (index != 'party_id') {
                        $("#" + index).html(value);
                    }
                });
                $('#item_id').html(challan_item.item_id);
                $('#item_name').html(challan_item.item_name);
                $('#item_code').html(challan_item.item_code);
                $('#quantity').html(challan_item.quantity);
                $('#item_serial_no').html(challan_item.item_serial_no);
                $('#item_extra_accessories_id').html(challan_item.item_extra_accessories);
                $('#item_description').html(challan_item.item_description);
                //$('select[name="party[kind_attn_id]"]').val(challan_data.kind_attn_id).change();

                var li_motor_serial_objectdata = json.motor_data;
                var motor_serial_objectdata = [];
                if (li_motor_serial_objectdata != '') {
                    $.each(li_motor_serial_objectdata, function (index, value) {
                        motor_serial_objectdata.push(value);
                    });
                }
                display_motor_serial_html(motor_serial_objectdata);
                var li_gearbox_serial_objectdata = json.gearbox_data;
                var gearbox_serial_objectdata = [];
                if (li_gearbox_serial_objectdata != '') {
                    $.each(li_gearbox_serial_objectdata, function (index, value) {
                        gearbox_serial_objectdata.push(value);
                    });
                }
                display_gearbox_serial_html(gearbox_serial_objectdata);
                $("#ajax-loader").hide();
            },
        });
    }

    function display_motor_serial_html(motor_serial_objectdata) {
        var motor_testing_details_c = '';
        $.each(motor_serial_objectdata, function (index, value) {

            var motor_testing_no_c = '<tr class="motor_serial_index_' + index + '">';
            motor_testing_no_c += '<td>' + value.motor_user + '</td>';
            motor_testing_no_c += '<td>' + value.motor_make + '</td>';
            motor_testing_no_c += '<td>' + value.motor_hp + '</td>';
            motor_testing_no_c += '<td>' + value.motor_kw + '</td>';
            motor_testing_no_c += '<td>' + value.motor_frequency + '</td>';
            motor_testing_no_c += '<td>' + value.motor_rpm + '</td>';
            motor_testing_no_c += '<td>' + value.motor_volts_cycles + '</td>';
            motor_testing_no_c += '<td>' + value.motor_serial_no + '</td>';
            var normal_amps_c = value.normal_amps_c == null ? '' : value.normal_amps_c;
            var working_amps_c = value.working_amps_c == null ? '' : value.working_amps_c;
            var equipment_rpm_c = value.equipment_rpm_c == null ? '' : value.equipment_rpm_c;
            var total_workinghours_c = value.total_workinghours_c == null ? '' : value.total_workinghours_c;
            motor_testing_no_c += '<td>' + normal_amps_c + '</td>';
            motor_testing_no_c += '<td>' + working_amps_c + '</td>';
            motor_testing_no_c += '<td>' + equipment_rpm_c + '</td>';
            motor_testing_no_c += '<td>' + total_workinghours_c + '</td>';
            motor_testing_details_c += motor_testing_no_c;

        });
        $('tbody#motor_testing_details_c').html(motor_testing_details_c);
    }

    function display_gearbox_serial_html(gearbox_serial_objectdata) {
        var gearbox_testing_details_c = '';
        $.each(gearbox_serial_objectdata, function (index, value) {

            var gearbox_testing_no_c = '<tr class="gearbox_serial_index_' + index + '">';
            gearbox_testing_no_c += '<td>' + value.gearbox_user + '</td>';
            gearbox_testing_no_c += '<td>' + value.gearbox_make + '</td>';
            gearbox_testing_no_c += '<td>' + value.gearbox_gear_type + '</td>';
            gearbox_testing_no_c += '<td>' + value.gearbox_model + '</td>';
            gearbox_testing_no_c += '<td>' + value.gearbox_ratio + '</td>';
            gearbox_testing_no_c += '<td>' + value.gearbox_serial_no + '</td>';
            var normal_amps_c = value.normal_amps_c == null ? '' : value.normal_amps_c;
            var working_amps_c = value.working_amps_c == null ? '' : value.working_amps_c;
            var equipment_rpm_c = value.equipment_rpm_c == null ? '' : value.equipment_rpm_c;
            var total_workinghours_c = value.total_workinghours_c == null ? '' : value.total_workinghours_c;
            gearbox_testing_no_c += '<td>' + normal_amps_c + '</td>';
            gearbox_testing_no_c += '<td>' + working_amps_c + '</td>';
            gearbox_testing_no_c += '<td>' + equipment_rpm_c + '</td>';
            gearbox_testing_no_c += '<td>' + total_workinghours_c + '</td>';
            gearbox_testing_details_c += gearbox_testing_no_c;

        });
        $('tbody#gearbox_testing_details_c').html(gearbox_testing_details_c);
    }

    function get_installation_with_item_list(party_id) {
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>complain/get_installation_with_item_list/' + party_id,
            type: "POST",
            data: null,
            async: false,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('div.installation_items_section').html(data.installation_with_item_html);

                <?php if (isset($feedback_items_data) && !empty($feedback_items_data)) { ?>
                    <?php foreach ($feedback_items_data as $feedback_item) { ?>
                        var selected_installation_id = [];
                        selected_installation_id = <?= json_encode($feedback_item->installation_id); ?>;
                        $("input.chk_challan_item[data-installation_id='" + selected_installation_id + "']").prop('checked', true);
                    <?php } ?>
                    feed_installation_data(<?= $feedback_items_data[0]->installation_id; ?>, <?= $feedback_items_data[0]->testing_report_id; ?>, <?= $feedback_items_data[0]->challan_id; ?>);
                <?php } ?>
                
                $("#ajax-loader").hide();
            },
            error: function (msg) {
                $("#ajax-loader").hide();
            }
        });
    }

    function party_details(id) {
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?= base_url(); ?>sales/ajax_load_party_with_cnt_person/' + id,
            async: false,
            data: 'party_id=' + id,
            success: function (data) {
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                } else {
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                } else {
                    $("#party_code").val("");
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $("#p_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"), '<?= base_url() ?>app/set_city_select2_val_by_id/' + json['city_id']);
                } else {
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"), '<?= base_url() ?>app/set_state_select2_val_by_id/' + json['state_id']);
                } else {
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"), '<?= base_url() ?>app/set_country_select2_val_by_id/' + json['country_id']);
                } else {
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                } else {
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                } else {
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                } else {
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                } else {
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                } else {
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"), '<?= base_url() ?>app/set_reference_select2_val_by_id/' + json['reference_id']);
                } else {
                    setSelect2Value($("#reference_id"));
                }
                if (json['reference_description']) {
                    $("#reference_description").html(json['reference_description']);
                } else {
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"), '<?= base_url() ?>app/set_sales_select2_val_by_id/' + json['party_type_1_id']);
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_DOMESTIC_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= INR_CURRENCY_ID; ?>);
                    }
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_EXPORT_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= USD_CURRENCY_ID; ?>);
                    }
                    var currency_title = $('#select2-currency_id-container').attr('title');
                    $('#received_payment_currency').html(currency_title);
                } else {
                    setSelect2Value($("#sales_id"));
                }

                if (json['contact_persons_array']) {
                    var option_html = '';
                    if (json['contact_persons_array'].length > 0) {
                        //                            console.log(json['contact_persons_array']);
                        $.each(json['contact_persons_array'], function (index, value) {
                            option_html += "<option value='" + value.contact_person_id + "'>" + value.name + "</option>";
                            $('input[name="contact_person[contact_person_mobile_no]"]').val(value.mobile_no);
                            $('input[name="contact_person[contact_person_email_id]"]').val(value.email);
                            $('#contact_person_designation').html(value.designation);
                            $('#contact_person_department').html(value.department);
                        })
                        $('select[name="party[kind_attn_id]"]').html(option_html).select2();
                    } else {
                        $('select[name="party[kind_attn_id]"]').html('');
                    }
                    $('select[name="party[kind_attn_id]"]').change();
                } else {
                    $('select[name="party[kind_attn_id]"]').html('');
                    $('select[name="party[kind_attn_id]"]').change();
                }

                $("#ajax-loader").hide();
            }
        });
    }
</script>
