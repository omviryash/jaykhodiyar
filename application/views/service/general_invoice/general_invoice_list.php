<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">General Invoice List</small>
            <?php if ($this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID, "add")) { ?>
                <a href="<?= base_url() ?>general_invoice/add_invoice_from_party_pitem/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Purchase Item Invoice</a>
                <a href="<?= base_url() ?>general_invoice/add_invoice_from_party/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Parts Invoice</a>
                <a href="<?= base_url() ?>general_invoice/add_invoice_from_complain/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice From Complain</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <label class="pull-left">Invoice Created From: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <select name="created_from" id="created_from" class="form-control input-sm select2" style="width: 60%;">                                
                                    <option value="all">All</option>
                                    <option value="1" <?=isset($_GET['created_from']) && $_GET['created_from'] == '1'?'selected="selected"':''?>>Created from Complain</option>
                                    <option value="2" <?=isset($_GET['created_from']) && $_GET['created_from'] == '2'?'selected="selected"':''?>>Created from Dispatch Party</option>
                                </select>
                            </div>						
                        </div>

                        <div class="col-md-12">
                            <div style="margin: 10px;">
                                <table id="example1" class="table display custom-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="80px">Action</th>
                                            <th>Invoice No.</th>
                                            <th>Invoice Date</th>
                                            <th>Party Name</th>
                                            <th>Contact No</th>
                                            <th>Email Id</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Country</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<div class="modal fade" id="with_without_letterpad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select With or Without Letter pad in Genral Invoice Print
                    <span class="pull-right">
                        <a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
                        <a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body text-center">
                <a href="#" target="_blank" class="btn-default btn" id="with_letterpad" ><i class="fa fa-print"></i> With Letter pad</a><br /><br />
                <a href="#" target="_blank" class="btn-default btn" id="without_letterpad" ><i class="fa fa-print"></i> Without Letter pad</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var table;
    $(document).ready(function () {
        $('.select2').select2();
        table = $('#example1').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('general_invoice/general_invoice_datatable') ?>",
                "type": "POST",
                "data":function(d){
					d.created_from = $("#created_from").val();
				}
            },
            "scrollY": 550,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
        
        $(document).on('click',".with_without_letterpad",function() {
            $('#with_without_letterpad').modal('show');
            var invoice_id = $(this).data('invoice_id');
            $('#with_letterpad').attr('href', '<?=base_url('general_invoice/invoice_print/');?>/' + invoice_id + '/wlp');
            $('#without_letterpad').attr('href', '<?=base_url('general_invoice/invoice_print/');?>/' + invoice_id + '/');
        });

        $(document).on('click',"#with_letterpad, #without_letterpad",function() {
            $('#with_without_letterpad').modal('hide');
        });
        
        $(document).on('change',"#created_from",function() {
			table.draw();
		});
        
        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=general_invoice_id&table_name=general_invoice',
                    success: function (data) {
                        table.draw();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

    });
</script>
