<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Parts Master</small>
            <?php if ($this->applib->have_access_role(PARTS_MODULE_ID, "add")) { ?>
                <a href="<?= base_url() ?>general_invoice/add_parts" class="btn btn-info btn-xs pull-right">Add Part</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin: 10px;">
                                <table id="example1" class="table display custom-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="80px">Action</th>
                                            <th>Part Name</th>
                                            <th width="300px">Part Code</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#example1').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('general_invoice/part_datatable') ?>",
                "type": "POST"
            },
            "scrollY": 550,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=parts',
                    success: function (data) {
                        table.draw();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

    });
</script>
