<?php $this->load->view('success_false_notify'); ?>
<style>
    .modal-dialog { width: 100%; height: 100%; margin: 0; padding: 0; }
    .modal-content { height: auto; min-height: 100%; border-radius: 0; }
</style>
<div class="content-wrapper">
    <form class="form-horizontal" action="<?= base_url('general_invoice/save_invoice_from_complain') ?>" method="post" id="save_invoice_from_complain" novalidate>
        <?php if (isset($general_invoice_data->general_invoice_id) && !empty($general_invoice_data->general_invoice_id)) { ?>
            <input type="hidden" name="general_invoice_data[general_invoice_id]" id="general_invoice_id" value="<?= $general_invoice_data->general_invoice_id ?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">General Invoice <span id="partyname"></span></small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, Ctrl+S = Save</label></small>
                <?php if(isset($general_invoice_data->general_invoice_id) && !empty($general_invoice_data->general_invoice_id)){ ?>
                    <?php if(isset($general_invoice_data->is_approved) && $general_invoice_data->is_approved == 1){ ?>
                        <?php if($this->applib->have_access_role(CAN_APPROVE_GENERAL_INVOICE,"allow")){ ?>
                            <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove" style="margin: 5px;">Disapprove</button>
                        <?php } else { ?>
                            <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if($this->applib->have_access_role(CAN_APPROVE_GENERAL_INVOICE,"allow")){ ?>
                            <button type="button" class="btn btn-info btn-xs pull-right btn_approve" style="margin: 5px;">Approve</button>
                        <?php } else { ?>
                            <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <?php
                    $view_role = $this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID, "view");
                    $add_role = $this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID, "add");
                    $edit_role = $this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID, "edit");
                ?>
                <?php if ($add_role || $edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
                <?php endif; ?>
                <?php if ($view_role): ?>
                    <a href="<?= base_url() ?>general_invoice/general_invoice_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">General Invoice List</a>
                <?php endif; ?>
                <?php if ($add_role): ?>
                    <a href="<?= base_url() ?>general_invoice/add_invoice_from_complain/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice From Complain</a>
                <?php endif; ?>
            </h1>
        </section>
        <div class="clearfix">
            <?php if ($add_role || $edit_role): ?>
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">General Invoice Details</a></li>
                            <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Complain Details</a></li>
                            <li><a href="#tab_3" data-toggle="tab" id="tabs_3">General</a></li>
                            <?php if (isset($general_invoice_data->general_invoice_id) && !empty($general_invoice_data->general_invoice_id)) { ?>
                                <li><a href="#tab_4" data-toggle="tab" id="tabs_4">Login</a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> General Invoice Details </legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-6">
                                                    <label for="general_invoice_no" class="col-sm-4 input-sm">General Invoice No</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="" id="general_invoice_no" class="form-control input-sm" value="<?= (isset($general_invoice_data->general_invoice_no)) ? $general_invoice_data->general_invoice_no : ''; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="general_invoice_date" class="col-sm-4 input-sm">General Invoice Date</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="general_invoice_data[general_invoice_date]" id="general_invoice_date" class="form-control input-sm pull-right input-datepicker" value="<?= (isset($general_invoice_data->general_invoice_date)) ? date('d-m-Y', strtotime($general_invoice_data->general_invoice_date)) : date('d-m-Y'); ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-6">
                                                    <label for="purchase_order_no" class="col-sm-4 input-sm">Purchase Order No</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="general_invoice_data[purchase_order_no]" id="purchase_order_no" class="form-control input-sm" value="<?= (isset($general_invoice_data->purchase_order_no)) ? $general_invoice_data->purchase_order_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="purchase_order_date" class="col-sm-4 input-sm">Purchase Order Date</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="general_invoice_data[purchase_order_date]" id="purchase_order_date" class="form-control input-sm input-datepicker" value="<?= (isset($general_invoice_data->purchase_order_date)) ? date('d-m-Y', strtotime($general_invoice_data->purchase_order_date)) : ''; ?>">
                                                    </div>
                                                </div>    
                                                <div class="clearfix"></div>
                                                <div class="col-md-6">
                                                    <label for="sales_order_no" class="col-sm-4 input-sm">Sales Order No</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="general_invoice_data[sales_order_no]" id="sales_order_no" class="form-control input-sm" value="<?= (isset($general_invoice_data->sales_order_no)) ? $general_invoice_data->sales_order_no : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="sales_order_date" class="col-sm-4 input-sm">Sales Order Date</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="general_invoice_data[sales_order_date]" id="sales_order_date" class="form-control input-sm input-datepicker" value="<?= (isset($general_invoice_data->sales_order_date)) ? date('d-m-Y', strtotime($general_invoice_data->sales_order_date)) : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="col-md-6">
                                                    <label for="note" class="col-sm-4 input-sm"> Note </label>
                                                    <div class="col-sm-8">
                                                        <textarea name="general_invoice_data[note]" id="note" class="form-control" ><?= (isset($general_invoice_data->note)) ? $general_invoice_data->note : ''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Party Detail</legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <input type="hidden" name="party[party_party_id]" id="party_party_id" class="party_party_id" placeholder="">
                                                    <input type="hidden" name="party[party_type_1_id]" id="party_type_1_id" class="party_type_1_id" placeholder="">
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-3 input-sm">Address</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="city" class="col-sm-3 input-sm">City</label>
                                                        <div class="col-sm-9">
                                                            <div class="col-md-6" style="padding:0px !important;">
                                                                <select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
                                                            </div>
                                                            <div class="col-md-6" style="padding-right:0px !important;">
                                                                <input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="state" class="col-sm-3 input-sm">State</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="country" class="col-sm-3 input-sm">Country</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
                                                            <small>Add multiple email id by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
                                                            <small>Add multiple phone number by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_website" class="col-sm-3 input-sm">Website</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm website" id="party_website" name="party[website]" disabled="">
                                                        </div>
                                                    </div>  
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Designation</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_designation"><?php echo isset($contact_person) ? $contact_person->designation : ''; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Department</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_department"><?php echo isset($contact_person) ? $contact_person->department : ''; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person) ? $contact_person->mobile_no : ''; ?>" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person) ? $contact_person->email : ''; ?>" disabled="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border line_item_form item_fields_div">
                                            <input type="hidden" name="line_items_index" id="line_items_index" />
                                            <?php if(isset($general_invoice_lineitems)){ ?>
                                                <input type="hidden" name="line_items_data[lineitem_id]" id="lineitem_id" />
                                            <?php } ?>
                                            <legend class="scheduler-border text-primary text-bold"> Complain Details</legend>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="complain_id" class="col-sm-3 input-sm">Select Complain<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-9">
                                                        <select name="line_items_data[complain_id]" id="complain_id" class="form-control input-sm" onchange="complain_details(this.value)"></select>
                                                        <input type="hidden" name="line_items_data[complain_no_year]" id="complain_no_year" class="form-control input-sm" >
                                                    </div>                                                       
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="item_rate" class="col-sm-3 input-sm">Rate<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-3" style="padding-right: 0px;">
                                                        <select name="line_items_data[currency_id]" id="currency_id" class="form-control input-sm currency_id" ></select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="hidden" name="line_items_data[quantity]" id="quantity" class="form-control input-sm quantity" data-name="quantity"  value="1" placeholder="">
                                                        <input type="text" name="line_items_data[item_rate]" id="item_rate" class="form-control input-sm item_rate" data-name="item_rate"  value="0" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="disc_per" class="col-sm-3 input-sm">Disc(%)</label>
                                                    <div class="col-sm-3" style="padding-right: 0px;">
                                                        <input type="text" name="line_items_data[disc_per]" id="disc_per" class="form-control input-sm disc_per num_only" data-input_name="disc_per" placeholder="">
                                                    </div>
                                                    <label for="disc_value" class="col-sm-2 input-sm">Disc(Value)</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="line_items_data[disc_value]" id="disc_value" class="form-control input-sm disc_value num_only" data-input_name="disc_value" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="cgst" class="col-sm-3 input-sm">CGST %</label>
                                                    <div class="col-sm-3" style="padding-right: 0px;">
                                                        <input type="text" name="line_items_data[cgst]" id="cgst" class="form-control input-sm cgst numberwithoutzero item_data" data-input_name="cgst" placeholder="">
                                                    </div>
                                                    <label for="cgst_amount" class="col-sm-2 input-sm">CGST Amount</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="line_items_data[cgst_amount]" id="cgst_amount" class="form-control input-sm cgst_amount numberwithoutzero item_data" readonly data-input_name="cgst_amount" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="sgst" class="col-sm-3 input-sm">SGST %</label>
                                                    <div class="col-sm-3" style="padding-right: 0px;">
                                                        <input type="text" name="line_items_data[sgst]" id="sgst" class="form-control input-sm sgst item_data" data-input_name="sgst" placeholder="">
                                                    </div>
                                                    <label for="sgst_amount" class="col-sm-2 input-sm">SGST Amount</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="line_items_data[sgst_amount]" id="sgst_amount" class="form-control input-sm sgst_amount item_data" readonly data-input_name="sgst_amount" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="igst" class="col-sm-3 input-sm">IGST %</label>
                                                    <div class="col-sm-3" style="padding-right: 0px;">
                                                        <input type="text" name="line_items_data[igst]" id="igst" class="form-control input-sm igst item_data" data-input_name="igst" placeholder="">
                                                    </div>
                                                    <label for="igst_amount" class="col-sm-2 input-sm">IGST Amount</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="line_items_data[igst_amount]" id="igst_amount" class="form-control input-sm igst_amount item_data" readonly data-input_name="igst_amount" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="amount" class="col-sm-3 input-sm">Amount</label>
                                                    <div class="col-sm-3" style="padding-right: 0px;">
                                                        <input type="text" name="line_items_data[amount]" id="amount" class="form-control input-sm item_rate " data-name="amount" placeholder="" readonly >
                                                    </div>
                                                    <label for="net_amount" class="col-sm-2 input-sm">Net Amount</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="line_items_data[net_amount]" id="net_amount" class="form-control input-sm net_amount disabled" data-input_name="net_amount" placeholder="" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Complain" />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div> <br />
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table style="" class="table custom-table item-table">
                                            <thead>
                                                <tr>
                                                    <th width="55px">Action</th>
                                                    <th>Complain No</th>
                                                    <th class="text-right">Rate</th>
                                                    <th class="text-right">Amount</th>
                                                    <th class="text-right">Disc(value)</th>
                                                    <th class="text-right">CGST %</th>
                                                    <th class="text-right">SGST %</th>
                                                    <th class="text-right">IGST %</th>
                                                    <th class="text-right">Net Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lineitem_list"></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="">Total</th>
                                                    <th></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right" class="text-right"><span class="amount_total"></span><input type="hidden" name="amount_total" id="amount_total" /></th>
                                                    <th class="text-right" class="text-right"><span class="disc_value_total"></span><input type="hidden" name="disc_value_total" id="disc_value_total" /></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right" class="text-right"><span class="net_amount_total"></span><input type="hidden" name="net_amount_total" id="net_amount_total" /></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Mode Of Shipment </legend>
                                            <div class="col-md-6">
                                                <label for="loading_at" class="col-sm-4 input-sm"> Loading At </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[loading_at]" id="loading_at" class="form-control input-sm"  placeholder="" value="<?= (isset($general_invoice_data->loading_at)) ? $general_invoice_data->loading_at : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="port_of_discharge" class="col-sm-4 input-sm"> Port of Discharge</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[port_of_discharge]" id="port_of_discharge" class="form-control input-sm" value="<?= (isset($general_invoice_data->port_of_discharge)) ? $general_invoice_data->port_of_discharge : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="place_of_delivery" class="col-sm-4 input-sm"> Place of Delivery</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[place_of_delivery]" id="place_of_delivery" class="form-control input-sm" value="<?= (isset($general_invoice_data->place_of_delivery)) ? $general_invoice_data->place_of_delivery : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="delivery_through_id" class="col-sm-4 input-sm">Delivery Through</label>
                                                <div class="col-sm-8">
                                                    <select name="general_invoice_data[delivery_through_id]" id="delivery_through_id" class="delivery_through_id form-control input-sm" ></select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="" class="col-sm-4 input-sm"> Name of Shipment</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[name_of_shipment]" id="name_of_shipment" class="form-control input-sm"  value="<?= (isset($general_invoice_data->name_of_shipment)) ? $general_invoice_data->name_of_shipment : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="truck_container_no" class="col-sm-4 input-sm"> Truck / Container No. </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[truck_container_no]" id="truck_container_no" class=" form-control input-sm item_data" value="<?= (isset($general_invoice_data->truck_container_no)) ? $general_invoice_data->truck_container_no : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="lr_no" class="col-sm-4 input-sm"> LR / BL No. </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[lr_no]" id="lr_no" class=" form-control input-sm item_data" value="<?= (isset($general_invoice_data->lr_no)) ? $general_invoice_data->lr_no : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="lr_date" class="col-sm-4 input-sm">LR Date</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[lr_date]" id="lr_date" class="form-control input-sm input-datepicker" value="<?= (isset($general_invoice_data->lr_date)) ? date('d-m-Y', strtotime($general_invoice_data->lr_date)) : ''; ?>">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> General Fields </legend>
                                            <div class="col-md-6">
                                                <label for="transport" class="col-sm-4 input-sm">Transport</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[transport]" id="transport" class="form-control input-sm num_only" placeholder="Amount" value="<?= (isset($general_invoice_data->transport)) ? $general_invoice_data->transport : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="p_and_f" class="col-sm-4 input-sm">Packing & Forwarding</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[p_and_f]" id="p_and_f" class="form-control input-sm num_only" placeholder="Amount" value="<?= (isset($general_invoice_data->p_and_f)) ? $general_invoice_data->p_and_f : ''; ?>" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="insurance" class="col-sm-4 input-sm">Insurance</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="general_invoice_data[insurance]" id="insurance" class="form-control input-sm num_only" placeholder="Amount" value="<?= (isset($general_invoice_data->insurance)) ? $general_invoice_data->insurance : ''; ?>" required="">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Terms & Conditions</legend>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea id="terms_conditions" class="form-control" style="height: 100px" name="general_invoice_data[terms_conditions]">
                                                        <?= (isset($general_invoice_data->terms_conditions)) ? $general_invoice_data->terms_conditions : ''; ?>
                                                    </textarea>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_4">
                                <?php if (isset($general_invoice_data->general_invoice_id) && !empty($general_invoice_data->general_invoice_id)) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?= (isset($general_invoice_data->created_by_name)) ? $general_invoice_data->created_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?= (isset($general_invoice_data->created_at)) ? $general_invoice_data->created_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?= (isset($general_invoice_data->updated_by_name)) ? $general_invoice_data->updated_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?= (isset($general_invoice_data->updated_at)) ? $general_invoice_data->updated_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Approved By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm approved_by" id="approved_by" value="<?= (isset($general_invoice_data->approved_by_name)) ? $general_invoice_data->approved_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm approved_date_time" id="approved_date_time" value="<?= (isset($general_invoice_data->approved_at)) ? $general_invoice_data->approved_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
            <?php endif; ?>
        </div>
        <section class="content-header">
            <?php if(isset($general_invoice_data->general_invoice_id) && !empty($general_invoice_data->general_invoice_id)){ ?>
                <?php if(isset($general_invoice_data->is_approved) && $general_invoice_data->is_approved == 1){ ?>
                    <?php if($this->applib->have_access_role(CAN_APPROVE_GENERAL_INVOICE,"allow")){ ?>
                        <button type="button" class="btn btn-info btn-xs pull-right btn_disapprove" style="margin: 5px;">Disapprove</button>
                    <?php } else { ?>
                        <small class="text-primary text-bold pull-right app_lab" style="margin: 5px;">Approved</small>
                    <?php } ?>
                <?php } else { ?>
                    <?php if($this->applib->have_access_role(CAN_APPROVE_GENERAL_INVOICE,"allow")){ ?>
                        <button type="button" class="btn btn-info btn-xs pull-right btn_approve" style="margin: 5px;">Approve</button>
                    <?php } else { ?>
                        <small class="text-primary text-bold pull-right" style="margin: 5px;">Not Approved</small>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <?php if ($add_role || $edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;"> Save </button>
            <?php endif; ?>
            <?php if ($view_role): ?>
                <a href="<?= base_url() ?>general_invoice/general_invoice_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">General Invoice List</a>
            <?php endif; ?>
            <?php if ($add_role): ?>
                <a href="<?= base_url() ?>general_invoice/add_invoice_from_complain/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Invoice From Complain</a>
            <?php endif; ?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<?php if(isset($general_invoice_data->general_invoice_id) && !empty($general_invoice_data->general_invoice_id)){ } else { ?>
<div class="modal fade" id="select_receive_complain" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<!--	<div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">-->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Select Paid Complain
					<span class="pull-right">
						<a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
						<a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
					</span>
				</h4>
			</div>
			<div class="modal-body">
                <!-- Custom Tabs -->
                <table id="pick_receive_complain_datatable" class="table custom-table table-striped">
                    <thead>
                        <tr>
                            <th>Complain No</th>
                            <th>Complain Date</th>
                            <th>Party</th>
                            <th>Contact No</th>
                            <th>Email Id</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                        </tr>
                    </thead>
                    <tbody></tbody>                        
                </table>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?= dist_url('js/general_invoice.js'); ?>"></script>
<script type="text/javascript">
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
    var lineitem_objectdata = [];
    <?php if (isset($general_invoice_lineitems)) { ?>
        var li_lineitem_objectdata = [<?php echo $general_invoice_lineitems; ?>];
        var lineitem_objectdata = [];
        if (li_lineitem_objectdata != '') {
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
    <?php } ?>
    display_lineitem_html(lineitem_objectdata);
    <?php if (isset($general_invoice_data->general_invoice_id) && !empty($general_invoice_data->general_invoice_id)) { ?>
        first_time_edit_mode = 0;
    <?php } ?>
    $(document).ready(function () {
        $('.select2').select2();
        initAjaxSelect2($("#city"), "<?= base_url('app/city_select2_source') ?>");
        initAjaxSelect2($("#state"), "<?= base_url('app/state_select2_source') ?>");
        initAjaxSelect2($("#country"), "<?= base_url('app/country_select2_source') ?>");
        initAjaxSelect2($("#uom_id"), "<?= base_url('app/uom_select2_source') ?>");
        initAjaxSelect2($("#currency_id"), "<?= base_url('app/currency_select2_source') ?>");

        initAjaxSelect2($("#delivery_through_id"),"<?=base_url('app/delivery_through_select2_source')?>");
        <?php if(isset($general_invoice_data->delivery_through_id)){ ?>
			setSelect2Value($("#delivery_through_id"),"<?=base_url('app/set_delivery_through_select2_val_by_id/'.$general_invoice_data->delivery_through_id)?>");
        <?php } ?>
        <?php if(isset($general_invoice_data->party_id)){ ?>
            initAjaxSelect2($("#complain_id"),"<?=base_url('app/complain_select2_source/'.$general_invoice_data->party_id)?>");
            setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id/'.$general_invoice_data->party_id)?>");
            $('#party_party_id').val(<?=$general_invoice_data->party_id;?>);
			party_details(<?=$general_invoice_data->party_id;?>);
        <?php } ?>

        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.protectedSource.push(/<\?[\s\S]*?\?>/g);
        CKEDITOR.replace('terms_conditions', {
            removePlugins: 'forms,iframe,preview,a11yhelp,stylescombo,div,showblocks,about,link,pagebreak,flash,smiley,templates,save,print,newpage,table,tabletools,indentblock,htmlwriter,htmldataprocessor,htmlwriter,floatingspace'
        });
        
        var table
		table = $('#pick_receive_complain_datatable').DataTable({
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('general_invoice/pick_receive_complain_datatable') ?>",
				"type": "POST",
				"data": function (d) {
					d.request_from = "complain";
				}
			},
			"scrollY": 550,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});
        
        $('#select_receive_complain').modal({backdrop: 'static', keyboard: false});
		$('#select_receive_complain').modal('show');
		$('#select_receive_complain').on('shown.bs.modal', function () {
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
		});

		$(document).on('click', '.complain_row', function () {
			var tr = $(this).closest('tr');
			var complain_id = $(this).data('complain_id');
			var party_id = $(this).data('party_id');
			feedComplainData(complain_id);
            $('#select_receive_complain').modal('hide');
		});

        $(document).on("change", '#contact_person_id', function () {
			var contact_person_id = $(this).val();
            $.ajax({
				url: "<?= base_url(); ?>party/get-contact-person-by-id/" + contact_person_id,
				type: "POST",
				data: null,
                async: false,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					if (data.success == true) {
						$('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
						$('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
						$('#contact_person_designation').html(data.contact_person_data.designation);
						$('#contact_person_department').html(data.contact_person_data.department);
					} else {
						$('input[name="contact_person[contact_person_mobile_no]"]').val('');
						$('input[name="contact_person[contact_person_email_id]"]').val('');
						$('#contact_person_designation').html('');
						$('#contact_person_department').html('');
					}
				}
			});
		});

        $(document).on('input', '#quantity', function () {
        });

        $(document).on('input', '#item_rate', function () {
            if (this.value > 9900000) {
                this.value = 9900000;
            }
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_invoice_from_complain").submit();
                return false;
            }
        });
        $(document).on('submit', '#save_invoice_from_complain', function () {
            if ($.trim($("#party_party_id").val()) == '') {
                show_notify('Please Select Party.', false);
                return false;
            }
            if ($.trim($("#complain_id").val()) != '') {
                on_save_add_edit_item = 1;
                $('#add_lineitem').click();
                return false;
            }
            if (lineitem_objectdata == '') {
                show_notify("Please select any one Complain.", false);
                return false;
            }

            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
            var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
            postData.append('line_items_data', lineitem_objectdata_stringify);
            $.ajax({
                url: "<?= base_url('general_invoice/save_invoice_from_complain') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.status == 0) {
                        show_notify(json.msg, false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                        return false;
                    }
                    if (json['success'] == 'false') {
                        show_notify(json['msg'], false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                    }
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('general_invoice/general_invoice_list') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('general_invoice/general_invoice_list') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });

        $(document).on("click",".btn_approve",function(){
            $("#ajax-loader").show();
            var general_invoice_id = $('#general_invoice_id').val();
            $.ajax({
                url: '<?=BASE_URL?>general_invoice/update_general_invoice_status_to_approved',
                type: "POST",
                data: {general_invoice_id:general_invoice_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        <?php if($this->applib->have_access_role(CAN_APPROVE_PURCHASE_ORDER,"allow")){?>
                        $(".btn_approve").after("<button type='button' class='btn btn-info btn-xs pull-right btn_disapprove' style='margin: 5px;'>Disapprove</button>");
                        <?php }else{?>
                        $(".btn_approve").after("<small class='text-primary text-bold pull-right app_lab' style='margin: 5px;'>Approved</small>");
                        <?php }?>
                        $('.btn_approve').remove();
                        show_notify(data.message, true);
                    } else {
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on("click",".btn_disapprove",function(){
            $("#ajax-loader").show();
            var general_invoice_id = $('#general_invoice_id').val();
            $.ajax({
                url: '<?=BASE_URL?>general_invoice/update_general_invoice_status_to_disapproved',
                type: "POST",
                data: {general_invoice_id:general_invoice_id},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $(".btn_disapprove").after("<button type='button' class='btn btn-info btn-xs pull-right btn_approve' style='margin: 5px;'>Approve</button>");
                        $('.btn_disapprove').remove();
                        show_notify(data.message, true);
                    } else {
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
        });

        $('#add_lineitem').on('click', function () {
            var complain_id = $("#complain_id").val();
            if (complain_id == '' || complain_id == null) {
                show_notify("Please select Complain!", false);
                return false;
            }
            var item_rate = $("#item_rate").val();
            if (item_rate == '' || item_rate == null) {
                show_notify("Rate is required!", false);
                return false;
            }

            var key = '';
            var value = '';
            var lineitem = {};
            $('select[name^="line_items_data"]').each(function (e) {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            $('input[name^="line_items_data"]').each(function () {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            var party_type_1_id = $("#party_type_1_id").val();
            var new_lineitem = JSON.parse(JSON.stringify(lineitem));
            var line_items_index = $("#line_items_index").val();
            if (line_items_index != '') {
                lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
            } else {
                lineitem_objectdata.push(new_lineitem);
            }
            display_lineitem_html(lineitem_objectdata);
            $('#lineitem_id').val('');
            $('.item_fields_div').find('input[name^="line_items_index"], input[name^="line_items_data"], select[name^="line_items_data"], textarea[name^="line_items_data"]').val('');
            $("#complain_id").val(null).trigger("change");
            $("#quantity").val('1');
            $("#disc_per").val('0');
            if (party_type_1_id == '<?= PARTY_TYPE_DOMESTIC_ID; ?>') {
                setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= INR_CURRENCY_ID; ?>);
            }
            if (party_type_1_id == '<?= PARTY_TYPE_EXPORT_ID; ?>') {
                setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= USD_CURRENCY_ID; ?>);
            }
            $("#line_items_index").val('');
            if (on_save_add_edit_item == 1) {
                on_save_add_edit_item == 0;
                $('#save_invoice_from_complain').submit();
            }
            edit_lineitem_inc = 0;
        });

    });
    
    function feedComplainData(complain_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>general_invoice/get_complain',
            type: "POST",
            async: false,
            data: {complain_id: complain_id},
            success: function(data){
                $("#ajax-loader").hide();
                json = JSON.parse(data);

                var complain_data = json.complain_data;
                initAjaxSelect2($("#complain_id"),"<?=base_url('app/complain_select2_source')?>/"+complain_data.party_id);
                setSelect2Value($("#party_id"),"<?=base_url('app/set_party_select2_val_by_id')?>/"+complain_data.party_id);
                $('#party_party_id').val(complain_data.party_id);
                party_details($('#party_id').val());
                $('#complain_no').val(complain_data.complain_no);
                if(json.complain_item != ''){
                    lineitem_objectdata.push(json.complain_item);
                    display_lineitem_html(lineitem_objectdata);
                }
                $.ajax({
                    url: '<?php echo BASE_URL; ?>app/get_terms_and_conditions',
                    type: "POST",
                    data: {module:'general_invoice_from_complain_terms_and_conditions'},
                    dataType: 'json',
                    success: function (data) {
                        CKEDITOR.instances['terms_conditions'].setData( data.terms_and_conditions );
                    }
                });
            },
        });
    }

    function display_lineitem_html(lineitem_objectdata) {
        $('#ajax-loader').show();
        var new_lineitem_html = '';
        var set_qty_total = 0;
        var qty_total = 0;
        var amount_total = 0;
        var disc_value_total = 0;
        var net_amount_total = 0;
        console.log(lineitem_objectdata);
        $.each(lineitem_objectdata, function (index, value) {
            var lineitem_edit_btn = '';
            var lineitem_delete_btn = '';
            lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
            lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
            var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
                    lineitem_edit_btn +
                    lineitem_delete_btn +
                    '</td>' +
                    '<td>' + value.complain_no_year + '</td>' +
                    '<td class="text-right">' + value.item_rate + '</td>' +
                    '<td class="text-right">' + value.amount + '</td>' +
                    '<td class="text-right">' + value.disc_value + '</td>' +
                    '<td class="text-right">' + value.cgst + '</td>' +
                    '<td class="text-right">' + value.sgst + '</td>' +
                    '<td class="text-right">' + value.igst + '</td>' +
                    '<td class="text-right">' + value.net_amount + '</td></tr>';
            new_lineitem_html += row_html;
            amount_total += +value.amount;
            disc_value_total += +value.disc_value;
            net_amount_total += +value.net_amount;
        });
        $('tfoot span.amount_total').html(amount_total);
        $('#amount_total').val(amount_total);
        $('tfoot span.disc_value_total').html(disc_value_total);
        $('#disc_value_total').val(disc_value_total);
        $('tfoot span.net_amount_total').html(net_amount_total.toFixed(2));
        $('#net_amount_total').val(net_amount_total.toFixed(2));
        $('tbody#lineitem_list').html(new_lineitem_html);
        $('#ajax-loader').hide();
    }

    function edit_lineitem(index) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $('#ajax-loader').show();
        if (edit_lineitem_inc == 0) {
            edit_lineitem_inc = 1;
            $(".add_lineitem").removeAttr("disabled");
        }
        value = lineitem_objectdata[index];
        console.log(value);
        $("#line_items_index").val(index);
        setSelect2Value($("#complain_id"),"<?=base_url('app/set_complain_select2_val_by_id/')?>/" + value.complain_id);
        initAjaxSelect2($("#currency_id"),"<?=base_url('app/currency_select2_source/')?>");
		setSelect2Value($("#currency_id"),"<?=base_url('app/set_currency_select2_val_by_id/')?>/" + value.currency_id);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#lineitem_id").val(value.id);
		}
        $("#complain_no_year").val(value.complain_no_year);
        $("#item_rate").val(value.item_rate);
        $("#amount").val(value.amount);
        $("#disc_per").val(value.disc_per);
        $("#disc_value").val(value.disc_value);
        $("#cgst").val(value.cgst);
        $("#sgst").val(value.sgst);
        $("#igst").val(value.igst);
        $("#cgst_amount").val(value.cgst_amount);
        $("#sgst_amount").val(value.sgst_amount);
        $("#igst_amount").val(value.igst_amount);
        $("#net_amount").val(value.net_amount);
        $('#ajax-loader').hide();
    }

    function remove_lineitem(index) {
        if (confirm('Are you sure ?')) {
            value = lineitem_objectdata[index];
            if (typeof (value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.lineitem_id + '" />');
            }
            lineitem_objectdata.splice(index, 1);
            display_lineitem_html(lineitem_objectdata);
        }
    }
    
    function complain_details(id){
    
        if(edit_lineitem_inc == 0){
            if(id != '') {
                $.ajax({
                    type: "POST",
                    url: '<?=base_url();?>general_invoice/ajax_load_complain_details/' + id,
                    data: id = 'item_id',
                    success: function (data) {
    //                    console.log(data);
                        var json = $.parseJSON(data);
                        var complain_data = json.complain_data;
                        var complain_item = json.complain_item;
                        $("#complain_no_year").val(complain_data.complain_no_year);
                        $("#quantity").val('1');
                        $("#item_rate").val(complain_item.item_rate);
                        $("#disc_per").val(complain_item.disc_per);
                        $("#disc_value").val(complain_item.disc_value);
                        
                        $("#cgst").val(complain_item.cgst);
                        $("#cgst_amount").val(complain_item.cgst_amount);
                        $("#sgst").val(complain_item.sgst);
                        $("#sgst_amount").val(complain_item.sgst_amount);
                        $("#igst").val(complain_item.igst);
                        $("#igst_amount").val(complain_item.igst_amount);
                        
                        $("#amount").val(complain_item.amount);
                        $("#net_amount").val(complain_item.net_amount);
                    }
                });
            }
        }
    }

    function party_details(id) {
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?= base_url(); ?>sales/ajax_load_party_with_cnt_person/' + id,
            async: false,
            data: 'party_id='+id,
            success: function (data) {
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                } else {
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                } else {
                    $("#party_code").val("");
                }
                
                if (json['party_id']) {
                    $("#party_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"), '<?= base_url() ?>app/set_city_select2_val_by_id/' + json['city_id']);
                } else {
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"), '<?= base_url() ?>app/set_state_select2_val_by_id/' + json['state_id']);
                } else {
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"), '<?= base_url() ?>app/set_country_select2_val_by_id/' + json['country_id']);
                } else {
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                } else {
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                } else {
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                } else {
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                } else {
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                } else {
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"), '<?= base_url() ?>app/set_reference_select2_val_by_id/' + json['reference_id']);
                } else {
                    setSelect2Value($("#reference_id"));
                }
                if (json['reference_description']) {
                    $("#reference_description").html(json['reference_description']);
                } else {
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {+
                    $("#party_type_1_id").val(json['party_type_1_id']);
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_DOMESTIC_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= INR_CURRENCY_ID; ?>);
                    }
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_EXPORT_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= USD_CURRENCY_ID; ?>);
                    }
                    var currency_title = $('#select2-currency_id-container').attr('title');
                } else {
                    $("#party_type_1_id").val('');
                }

                if (json['contact_persons_array']) {
                    var option_html = '';
                    if (json['contact_persons_array'].length > 0) {
//                        console.log(json['contact_persons_array']);
                        $.each(json['contact_persons_array'], function (index, value) {
                            option_html += "<option value='" + value.contact_person_id + "'>" + value.name + "</option>";
                            $('input[name="contact_person[contact_person_mobile_no]"]').val(value.mobile_no);
                            $('input[name="contact_person[contact_person_email_id]"]').val(value.email);
                            $('#contact_person_designation').html(value.designation);
                            $('#contact_person_department').html(value.department);
                        })
                        $('select[name="party[kind_attn_id]"]').html(option_html).select2();
                    } else {
                        $('select[name="party[kind_attn_id]"]').html('');
                    }
                    $('select[name="party[kind_attn_id]"]').change();
                } else {
                    $('select[name="party[kind_attn_id]"]').html('');
                    $('select[name="party[kind_attn_id]"]').change();
                }
                
                $("#ajax-loader").hide();
            }
        });
    }

    <?php if (isset($_GET['view'])) { ?>
        $(window).load(function () {
            display_as_a_viewpage();
        });
    <?php } ?>
</script>