<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Master : Parts</small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, Ctrl+S = Save</label></small>
            <button type="button" class="btn btn-info btn-xs pull-right btn-save-parts" style="margin: 5px;">Save</button>
            <?php if($this->applib->have_access_role(PARTS_MODULE_ID,"view")){ ?>
			<a href="<?= base_url() ?>general_invoice/parts_list/" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Parts List</a>
            <?php } ?>
            <?php if($this->applib->have_access_role(PARTS_MODULE_ID,"add")){ ?>
			<a href="<?= base_url() ?>general_invoice/add_parts" style="margin: 5px;" class="btn btn-info btn-xs pull-right">Add Parts</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <form method="POST" 
                <?php if(isset($id) && !empty($id)){ ?>
                action="<?= base_url() ?>general_invoice/save_parts" 
                <?php } else { ?>
                action="<?= base_url() ?>general_invoice/save_parts" 
                <?php } ?> id="add_parts_form">
                <input type="hidden" class="form-control input-sm" id="id" name="id" value="<?=isset($id)?$id:''?>">
                <div class="nav-tabs-custom">
                    <?php
                        $cu_accessExport = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"export");
                        $cu_accessDomestic = $this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID,"domestic");
                    ?>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1"><?php if(isset($id) && !empty($id)){ echo 'Edit'; } else { echo 'Add'; } ?> Parts</a></li>
                        <?php if (isset($id) && !empty($id)) { ?>
                                <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Login</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold">Part Details</legend>
                                    <div class="col-md-6">
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="part_name" class="col-sm-4 input-sm">Part Name<span class="required-sign">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="part_name" name="part_name" value="<?=isset($part_name)?$part_name:''; ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <?php
                                            if(!empty($rates_in_val)){
                                                foreach ($rates_in_val as $rate_in_val) {
                                                    if(!empty($rate_in_val['currency'])){
                                            ?>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-4 input-sm">Rate in <?=$rate_in_val['currency']?></label>
                                                <div class="col-sm-7">
                                                    <input type="number" class="form-control input-sm" id="rate_<?=$rate_in_val['id'];?>" name="rate_in[<?=$rate_in_val['id'];?>]" value="<?=isset($rate_in_val['value']) ? $rate_in_val['value'] : '' ?>" />
                                                </div>
                                            </div>
                                            <?php        
                                                    } }
                                            }elseif(!empty($rates_in)){
                                                foreach ($rates_in as $rate_in) {                                                    
                                            ?>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="rate" class="col-sm-4 input-sm">Rate in <?=$rate_in->currency?></label>
                                                <div class="col-sm-7">
                                                    <input type="number" class="form-control input-sm" id="rate_<?=$rate_in->id;?>" name="rate_in[<?=strtolower($rate_in->id);?>]" value="" />
                                                </div>
                                            </div>
                                            <?php
                                                }
                                            }
                                            ?>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="uom" class="col-sm-4 input-sm">UOM<span class="required-sign">*</span></label>
                                            <div class="col-sm-7">
                                                <select class="input-sm pull-left" name="uom" id="uom"></select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="part_code" class="col-sm-4 input-sm">Part Code</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="part_code" name="part_code" value="<?=isset($part_code)?$part_code:'';?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="hsn_code" class="col-sm-4 input-sm">HSN Code<span class="required-sign">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm" id="hsn_code" name="hsn_code" value="<?=isset($hsn_code)?$hsn_code:''?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="stock" class="col-sm-4 input-sm">Batch Wise Stock ?</label>
                                            <div class="col-sm-7">
                                                <select class="form-control input-sm select2" name="stock" id="stock">
                                                    <option value="0"
                                                        <?php if(isset($id) && !empty($id)){ 
                                                            if(isset($stock) && $stock == 0) { echo 'selected'; }
                                                        } ?>>Yes</option>
                                                    <option value="1"
                                                        <?php if(isset($id) && !empty($id)){ 
                                                            if(isset($stock) && $stock == 1) { echo 'selected'; }
                                                        } ?>>No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="igst" class="col-sm-4 input-sm">IGST</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control input-sm num_only" id="igst" name="igst" value="<?=isset($igst) ? $igst : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="cgst" class="col-sm-4 input-sm">CGST</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control input-sm num_only" id="cgst" name="cgst" value="<?=isset($cgst) ? $cgst : '' ?>" />
                                            </div>
                                            <label for="sgst" class="col-sm-1 input-sm">SGST</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control input-sm num_only" id="sgst" name="sgst" value="<?=isset($sgst) ? $sgst : '' ?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                        </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <?php if (isset($id) && !empty($id)) { ?>
                            <div class="row">
                                    <div class="col-md-12">
                                            <fieldset class="scheduler-border">
                                                    <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 input-sm">Created By</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm created_by" id="created_by" value="<?=isset($created_by)?$created_by:''; ?>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm created_at" id="created_at" value="<?=isset($created_at)?$created_at:''; ?>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm updated_by" id="updated_by" value="<?=isset($updated_by)?$updated_by:''; ?>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control input-sm updated_at" id="updated_at" value="<?=isset($updated_at)?$updated_at:''; ?>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                            </fieldset>
                                    </div>
                            </div>
                            <?php } ?>
                    </div>
                        
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
                <input type="submit" class="hidden btn-submit-lead-form" />
            </form>
        </div>
        <!-- /.col -->
    </div>
</div>
<script>
    $(document).ready(function(){

        initAjaxSelect2($("#uom"),"<?=base_url('app/uom_select2_source')?>");
        <?php if(isset($uom)){ ?>
            setSelect2Value($("#uom"),"<?=base_url('app/set_uom_select2_val_by_id/'.$uom)?>");
        <?php } ?>
        $(document).on('click','.btn-save-parts',function(){
            $("#add_parts_form").submit();
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#add_parts_form").submit();
                return false;
            }
        });
        $("#add_parts_form").on("submit",function(e){
            e.preventDefault();
            if($('#part_name').val() == "" || $('#part_name').val() == null){
                show_notify('Part Name is required!', false);
                $('#part_name').focus();
                return false;
            }
            if($('#hsn_code').val() == "" || $('#hsn_code').val() == null){
                show_notify('HSN Code is required!', false);
                $('#hsn_code').focus();
                return false;
            }
            if($('#uom').val() == "" || $('#uom').val() == null){
                show_notify('Part UOM is required!', false);
                $('#uom').focus();
                return false;
            }
            
            <?php if(isset($id) && !empty($id)){ ?>
                var success_status = check_is_unique('parts','part_name',$("#part_name").val(),'id','<?=$id?>');
            <?php } else { ?>
                var success_status = check_is_unique('parts','part_name',$("#part_name").val(),'id',$('#id').val());
            <?php } ?>
            if(success_status == 0){
                if($('p.part_name-unique-error').length > 0){
                    $("p.part_name-unique-error").text('Part name already exist!');
                }else{
                    $("#part_name").after("<p class='text-danger part_name-unique-error'>Part name already exist!</p>");
                }
                show_notify('Part Name already exist!',false);
                return false;   
            }else{
                $("p.part_name-unique-error").text(' ');
            }

            $(document).on('submit', '#add_parts_form', function () {
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('general_invoice/save_parts') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                            var json = $.parseJSON(response);
                            if (json['success'] == 'false'){
                                    show_notify(json['msg'],false); 
                            }
                            if (json['success'] == 'Added'){
                                    window.location.href = "<?= base_url() ?>general_invoice/parts_list";
                            }
                            if (json['success'] == 'Updated'){
                                    window.location.href = "<?= base_url() ?>general_invoice/parts_list";
                            }
			},
                    });
                    return false;
                });
        });
    });
</script>
