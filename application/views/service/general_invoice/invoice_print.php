<html>
    <head>
        <title>General Invoice</title>
        <style>
            .text-center{
                text-align: center;
            }
            table{
                border-spacing: 0;
                width: 100%;
                border-bottom: 1px solid;
                border-right: 1px solid;
            }
            td{
                padding: 1px 1px 1px 1px;
                border-left: 1px solid;
                border-top: 1px solid;
                font-size:10px;
            }
            tr > td:last-child{
                border-right: 1px solid !important;
            }
            tr:last-child > td{
                border-bottom: 1px solid !important;
            }
            .text-right{
                text-align: right;
            }
            .text-bold{
                font-weight: 900 !important;
                font-size:12px !important;
            }
            .text-header{
                font-size: 20px;
            }
            .no-border-top{
                border-top:0;
            }
            .no-border-bottom{
                border-bottom:0 !important;
            }
            .no-border-left{
                border-left:0;
            }
            .no-border-right{
                border-right:0;
            }
            .width-50-pr{
                width:50%;
            }
            td.footer-sign-area{
                height: 82x;
                vertical-align: bottom;
                /*width: 33.33%;*/
                text-align: center;
            }
            .no-border{
                border: 0!important;
            }
            .footer-detail-area{
                color: #000000;
                font-size: 12px;
            }
            .color{
                color:red;
            }
        </style>
    </head>
    <body>
        <?php
            if(isset($letterpad_details->header_logo)){
				echo '<div style="text-align: '.$letterpad_details->header_logo_alignment.';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>';
			}
        ?>
        <?php if ($party_data['party_type_1'] == PARTY_TYPE_DOMESTIC_ID) { ?>
        <table>
            <tr>
                <td class="text-center text-bold text-header" colspan="22"><b>General Invoice</b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="10" width="350px"><b>Manufacturer & Supplier</b></td>
                <td class="text-bold" colspan="4" width="130px">Invoice Type</td>
                <td class="text-left text-bold" colspan="4" width="130px"><b>Original</b></td>
                <td class="text-left text-bold" colspan="4" width="130px"><b>Cash Memo</b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="10" rowspan="5">
                    <span style="font-size:15px;"><b><?= $company_details['name'] ?></b></span><br />
                    <?= nl2br($company_details['address']); ?><br />
                    City: <?= $company_details['city']; ?> - <?= $company_details['pincode']; ?> (<?= $company_details['state']; ?>), Country: <?= $company_details['country']; ?>.<br />
                    Email: <?= $company_details['email_id']; ?><br />
                    Tel No.: <?= $company_details['contact_no']; ?>,<br />
                    Contact No.: <?= $company_details['cell_no']; ?><br />
                    <b>GST No: <?= $company_details['gst_no']; ?></b>
                </td>
                <td class="text-bold" colspan="4">Invoice No.</td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->general_invoice_no;?></b></td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->general_invoice_date;?></b></td> 
            </tr>
            <tr>
                <td class="text-bold" colspan="4">Purchase Order No.</td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->purchase_order_no;?></b></td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->purchase_order_date;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4">Sales Order No.</td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->sales_order_no;?></b></td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->sales_order_date;?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="12"><b>Mode Of Shipment</b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4">Loading At:</td>
                <td class="text-bold text-left" colspan="8"><b><?=$general_invoice_data->loading_at;?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="10"><b>Purchaser</b></td>
                <td class="text-bold" colspan="4">Discharge:</td>
                <td class="text-bold text-left" colspan="8"><b><?=$general_invoice_data->port_of_discharge;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="10" rowspan="5">
                    <span style="font-size:15px;"><b><?= $party_data['party_name'] ?></b><br /></span>
                    <?= nl2br($party_data['address']); ?><br />
                    City: <?= $party_data['city'] ?> -  <?= $party_data['pincode'] ?> (<?= $party_data['state'] ?>) <?= $party_data['country'] ?>.<br />
                    Email: <?= $party_data['email_id']; ?><br />
                    Tel No.:  <?= $party_data['fax_no']; ?>,<br />
                    Contact No.:  <?= $party_data['phone_no']; ?><br />
                    Contact Person: <?= $challan_data->contact_person_name ?><br />
                    <?php if ($party_data['party_type_1'] == PARTY_TYPE_DOMESTIC_ID) { ?>
                    <b>GST No.: <?= $party_data['gst_no']; ?></b><?= isset($party_data['party_cin_no']) && !empty($party_data['party_cin_no']) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$party_data['party_cin_no'].'</b>' : ''; ?>
                    <?php } ?>
                </td>
                <td class="text-bold" colspan="4">Place of Delivery:</td>
                <td class="text-bold text-left" colspan="8"><b><?=$general_invoice_data->place_of_delivery;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4">Delivery Through:</td>
                <td class="text-bold text-left" colspan="8"><b><?=$general_invoice_data->delivery_through_id;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4">Name of shipment:</td>
                <td class="text-bold text-left" colspan="8"><b><?=$general_invoice_data->name_of_shipment;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4">Truck / Container No.:</td>
                <td class="text-bold text-left" colspan="8"><b><?=$general_invoice_data->truck_container_no;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4">LR / BL No.: </td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->lr_no;?></b></td>
                <td class="text-bold text-left" colspan="4"><b><?=$general_invoice_data->lr_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="2"><b>Sr.No.</b></td>
                <td class="text-bold text-center" colspan="6"><b>Description</b></td>
                <td class="text-bold text-center" colspan="2"><b>HSN</b></td>
                <td class="text-bold text-center" colspan="2"><b>Unit / Nos</b></td>
                <td class="text-bold text-center" colspan="2"><b>Rate</b></td>
                <td class="text-bold text-center" colspan="2"><b>CGST</b></td>
                <td class="text-bold text-center" colspan="2"><b>SGST</b></td>
                <td class="text-bold text-center" colspan="2"><b>IGST</b></td>
                <td class="text-bold text-center" colspan="2"><b>Amount</b></td>
            </tr>
            <?php
              $discount_total = 0;
              $pure_amount = 0;
              $taxable_amount = 0;
              $invoice_cgst_amount = 0;
              $invoice_sgst_amount = 0;
              $invoice_igst_amount = 0;
              $gst_total = 0;
              $grand_total = 0;
              if (!empty($invoice_items)) {
                  $invoice_item_count = count($invoice_items);
                  $i_inc = 1;
                  foreach ($invoice_items as $key => $item_row) {
            ?>
            <tr>
                <td class="text-bold" colspan="2">&nbsp;<?=$i_inc?></td>
                <?php if(isset($item_row->complain_no_year) && !empty($item_row->complain_no_year)){ ?>
                <td class="text-bold" colspan="6">&nbsp; Complain No.: <?=$item_row->complain_no_year;?></td>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold" colspan="2">&nbsp;<?=$item_row->quantity;?></td>
                <?php } else if(isset($item_row->part_name) && !empty($item_row->part_name)){ ?>
                <td class="text-bold" colspan="6">&nbsp;<?=$item_row->part_name;?></td>
                <td class="text-bold" colspan="2">&nbsp;<?=$item_row->hsn_code;?></td>
                <td class="text-bold" colspan="2">&nbsp;<?=$item_row->quantity . ' '. $item_row->uom_name;?></td>
                <?php } else if(isset($item_row->item_name) && !empty($item_row->item_name)){ ?>
                <td class="text-bold" colspan="6">&nbsp;<?=$item_row->item_name;?></td>
                <td class="text-bold" colspan="2">&nbsp;<?=$item_row->pi_hsn_code;?></td>
                <td class="text-bold" colspan="2">&nbsp;<?=$item_row->quantity . ' '. $item_row->uom_name;?></td>
                <?php } ?>
                <td class="text-bold text-right" colspan="2"><?=number_format((float)$item_row->item_rate, 2, '.', ''); ?></td>
                <td class="text-bold text-right" colspan="2"><?=$item_row->cgst;?></td>
                <td class="text-bold text-right" colspan="2"><?=$item_row->sgst;?></td>
                <td class="text-bold text-right" colspan="2"><?=$item_row->igst;?></td>
                <td class="text-bold text-right" colspan="2"><?=number_format((float)$item_row->amount, 2, '.', ''); ?></td>
            </tr>
            <?php
                      $i_inc++;
                      $discount_total += $item_row->disc_value;
                      $pure_amount += $item_row->quantity * $item_row->item_rate;
                      $gst_total += $item_row->cgst_amount + $item_row->sgst_amount + $item_row->igst_amount;
                  }
              }
              $taxable_amount = $pure_amount - $discount_total;
              $total_amount = $taxable_amount + $general_invoice_data->transport + $general_invoice_data->p_and_f + $general_invoice_data->insurance;
              $grand_total = $total_amount + $gst_total;
            ?>
            <?php
                 for($blank_row = 10; $blank_row > $invoice_item_count; $blank_row--){
            ?>
            <tr>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold" colspan="6">&nbsp;</td>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold" colspan="2">&nbsp;</td>
                <td class="text-bold text-right" colspan="2">&nbsp;</td>
            </tr>
            <?php } ?>

            <tr>
                <td valign="top" class="text-bold" colspan="16" rowspan="6"><b>Terms And Conditions:</b><br/><?= nl2br($general_invoice_data->terms_conditions);?></td>
                <td class="text-bold text-right" colspan="2">Sub Total</td>
                <td class="text-bold text-right" colspan="4"><b><?=number_format((float)$taxable_amount, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-right" colspan="2">Transport</td>
                <td class="text-bold text-right" colspan="4"><b><?=number_format((float)$general_invoice_data->transport, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-right" colspan="2">P & F</td>
                <td class="text-bold text-right" colspan="4"><b><?=number_format((float)$general_invoice_data->p_and_f, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-right" colspan="2">Insurance</td>
                <td class="text-bold text-right" colspan="4"><b><?=number_format((float)$general_invoice_data->insurance, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-right" colspan="2">Total</td>
                <td class="text-bold text-right" colspan="4"><b><?=number_format((float)$total_amount, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-right" colspan="2">GST</td>
                <td class="text-bold text-right" colspan="4"><b><?=number_format((float)$gst_total, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="16" width="70%"><b>In Words <?=$item_row->currency;?>: </b><?=money_to_word($grand_total)?></td>
                <td class="text-bold text-right" colspan="2" width="10%"><b>Grand Total</b></td>
                <td class="text-bold text-right" colspan="4" width="20%"><b><?=number_format((float)$grand_total, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="16" rowspan="3"><b>Note:</b><br/><?= nl2br($general_invoice_data->note);?></td>
                <td class="text-bold text-center" colspan="6"><b>For , <?= $company_details['name'] ?></b></td>
            </tr>
            <tr>
                <td class="no-border-bottom text-center" colspan="6">&nbsp;<br/><br/><br/><br/><br/><br/><br/></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="6" ><b>Authorised Signature</b></td>
            </tr>

        </table>
        <?php }elseif($party_data['party_type_1'] == PARTY_TYPE_EXPORT_ID) { ?>
        <table>
            <tr>
                <td class="text-center text-bold text-header" colspan="10">GENERAL INVOICE</td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="4" width=""><b>Consigner</b></td>
                <td class="text-bold" colspan="2">Invoice No.:</td>
                <td class="text-bold" colspan="2"><b><?=$general_invoice_data->general_invoice_no;?></b></td>
                <td class="text-center text-bold" colspan="2"><b><?=$general_invoice_data->general_invoice_date;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4" style="font-size:15px;"><b><?= $company_details['name'] ?></b></td>
                <td class="text-bold" colspan="2"  align="left" width="140px">Quotation No.:</td>
                <td class="text-bold" colspan="2"><b></b></td>
                <td class="text-center text-bold" colspan="2"><b></b></td>
            </tr>
            <tr>
                <td class="no-border-top text-bold" colspan="4" rowspan="4">
                    <?= nl2br($company_details['address']); ?><br />
                    City : <?= $company_details['city']; ?> - <?= $company_details['pincode']; ?> (<?= $company_details['state']; ?>), Country : <?= $company_details['country']; ?>.<br />
                    Email: <?= $company_details['email_id']; ?><br />
                    Tel No. : <?= $company_details['contact_no']; ?>,<br />
                    Contact No. : <?= $company_details['cell_no']; ?><br />
                    <b>GST No : <?= $company_details['gst_no']; ?></b>
                </td>
                <td class="text-bold"  align="left" colspan="2" >Purchase Order No.:</td>
                <td class="text-bold"  align="left" colspan="2"><b><?=$general_invoice_data->purchase_order_no;?></b></td>
                <td class="text-center text-bold" align="left" colspan="2"><center><b><?=$general_invoice_data->purchase_order_date;?></b></center></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="2" align="left" >Salse Order No.: </td>
                <td class=" text-bold" colspan="2"><b><?=$general_invoice_data->sales_order_no;?></b></td>
                <td class="text-bold" colspan="2"> <b><center><?=$general_invoice_data->sales_order_date;?></center></b></td>
            </tr>
            <tr>
                <td class=" text-bold"  align="left" colspan="2">IEC No.:</td>
                <td class=" text-bold" colspan="4" ><b><?=$company_details['iec_no'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold"  align="left" colspan="2">GST No.:</td>
                <td class=" text-bold" colspan="4" ><b><?=$company_details['gst_no'];?></b></td>
            </tr>
            <tr>
                <td class="text-bold"  align="left" colspan="2">END USE CODE.:</td>
                <td class="text-bold"  align="left" colspan="2"><b><?=$company_details['end_use_code'];?></b></td>
                <td class="text-bold" colspan="2">PAN No.:</td>
                <td class="  text-bold" colspan="4"><b><?=$company_details['pan_no'];?></b></td>
            </tr>
            <tr>
                <td class="text-bold"  align="left" colspan="2" rowspan="2">LUT.:</td>
                <td class="text-bold"  align="left" colspan="2" rowspan="2"><b><?=$company_details['lut'];?></b></td>				
                <td class="text-bold" colspan="2">L.C.No.:</td>
                <td class=" text-bold" colspan="2"><b><?=$general_invoice_data->lc_no;?></b></td>
                <td class=" text-bold" colspan="2"><center><b><?=$general_invoice_data->lc_date;?></b></center></td>
            </tr>
            <tr>
                <td class="text-bold"  align="left" colspan="2">Sea Freight:</td>
                <td class="text-bold"  align="left" colspan="4"><b><?=$general_invoice_data->sea_freight_type;?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="4"><b>Consignee</b></td>
                <td class="text-center text-bold" colspan="6"><b>Terms of Payment & Delivery:</b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="4" rowspan="3" valign="top">
                    <span style="font-size:15px;"><b><?= $party_data['party_name'] ?></b><br /></span>
                    <?= nl2br($party_data['address']); ?><br />
                    City : <?= $party_data['city'] ?> -  <?= $party_data['pincode'] ?> (<?= $party_data['state'] ?>) <?= $party_data['country'] ?>.<br />
                    Email : <?= $party_data['email_id']; ?><br />
                    Tel No.:  <?= $party_data['fax_no']; ?>,<br />
                    Contact No.:  <?= $party_data['phone_no']; ?><br />
                    Contact Person: <?= $challan_data->contact_person_name ?><br /><br /><br />
                </td>
                <td class="text-bold" colspan="6" valign="top"><?= nl2br($general_invoice_data->terms_conditions); ?><br /></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="6"><b>Our Bank Accounts Details</b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="6" valign="top">
                    <?=$general_invoice_data->bank_detail?>
                </td>
            </tr>
            
            <tr>
                <td class="text-bold text-center" colspan="10"><b>Dispatch Details</b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="2" width="100px">Loading At:</td>
                <td class="text-bold text-center" colspan="2"><b><?=$general_invoice_data->loading_at;?></b></td>
                <td class="text-bold" colspan="2" width="100px">Port Of Discharge:</td>
                <td class="text-bold text-center" colspan="4" width="100px"><b><?=$general_invoice_data->port_of_discharge;?></b></td>                
            </tr>
            <tr>
                <td class="text-bold" colspan="2">Port Of Loading:</td>
                <td class="text-bold text-center" colspan="1"><b><?=$general_invoice_data->port_of_loading;?></b></td>
                <td class="text-bold text-center" colspan="1"><b><?=$general_invoice_data->port_of_loading_country_id;?></b></td>
                <td class="text-bold" colspan="2">Place Of Delivery:</td>
                <td class="text-bold text-center" colspan="4"><b><?=$general_invoice_data->place_of_delivery;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="2">Delivery through:</td>
                <td class="text-bold text-center" colspan="2"><b><?=$general_invoice_data->delivery_through_id;?></b></td>
                <td class="text-bold" colspan="2" nowrap>Shipping Line Name:</td>
                <td class="text-bold text-center" colspan="4"><b><?=$general_invoice_data->name_of_shipment;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="2" >Country of Origin of Goods:</td>
                <td class="text-bold text-center" colspan="2"><b><?=$general_invoice_data->origin_of_goods_country_id;?></b></td>
                <td class="text-bold" colspan="2">Country of final Destination:</td>
                <td class="text-bold text-center" colspan="4"><b><?=$general_invoice_data->final_destination_country_id;?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="2">Container No.:</td>
                <td class="text-bold text-center" colspan="1"><b><?=$general_invoice_data->truck_container_no;?></b></td>
                <td class="text-bold" colspan="2">Line Seal No.:</td>
                <td class="text-bold text-center" colspan="2"><b><?=$general_invoice_data->line_seal_no;?></b></td>
                <td class="text-bold" colspan="2">Exporter Seal No.:</td>
                <td class="text-bold text-center" colspan="1"><b><?=$general_invoice_data->exporter_seal_no;?></b></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan=""><b>Sr.No.</b></td>
                <td class="text-bold text-center" colspan=""><b>Description</b></td>
                <td class="text-bold text-center" colspan=""><b>HSN</b></td>
                <td class="text-bold text-center" colspan=""><b>Unit / Nos</b></td>
                <td class="text-bold text-center" colspan=""><b>Rate</b></td>
                <td class="text-bold text-center" colspan=""><b>CGST</b></td>
                <td class="text-bold text-center" colspan=""><b>SGST</b></td>
                <td class="text-bold text-center" colspan=""><b>IGST</b></td>
                <td class="text-bold text-center" colspan=""><b>Discount</b></td>
                <td class="text-bold text-center" colspan=""><b>Amount</b></td>
            </tr>
            
            <?php
                $discount_total = 0;
                $pure_amount = 0;
                $taxable_amount = 0;
                $invoice_cgst_amount = 0;
                $invoice_sgst_amount = 0;
                $invoice_igst_amount = 0;
                $gst_total = 0;
                $grand_total = 0;
                if (!empty($invoice_items)) {
                    $invoice_item_count = count($invoice_items);
                    $i_inc = 1;
                    foreach ($invoice_items as $key => $item_row) {
            ?>
            <tr>
                <td class="text-bold" colspan="">&nbsp;<?=$i_inc?></td>
                <?php if(isset($item_row->complain_no_year) && !empty($item_row->complain_no_year)){ ?>
                <td class="text-bold" colspan="">&nbsp; Complain No. : <?=$item_row->complain_no_year;?></td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;<?=$item_row->quantity;?></td>
                <?php } else if(isset($item_row->part_name) && !empty($item_row->part_name)){ ?>
                <td class="text-bold" colspan="">&nbsp;<?=$item_row->part_name;?></td>
                <td class="text-bold" colspan="">&nbsp;<?=$item_row->hsn_code;?></td>
                <td class="text-bold" colspan="">&nbsp;<?=$item_row->quantity . ' '. $item_row->uom_name;?></td>
                <?php } ?>
                <td class="text-bold text-right" colspan=""><?=number_format((float)$item_row->item_rate, 2, '.', ''); ?></td>
                <td class="text-bold text-right" colspan=""><?=$item_row->cgst;?></td>
                <td class="text-bold text-right" colspan=""><?=$item_row->sgst;?></td>
                <td class="text-bold text-right" colspan=""><?=$item_row->igst;?></td>
                <td class="text-bold text-right" colspan=""><?=$item_row->disc_per;?></td>
                <td class="text-bold text-right" colspan=""><?=number_format((float)$item_row->amount, 2, '.', ''); ?></td>
            </tr>
            <?php
                    $i_inc++;
                    $discount_total += $item_row->disc_value;
                    $pure_amount += $item_row->quantity * $item_row->item_rate;
                    $gst_total += $item_row->cgst_amount + $item_row->sgst_amount + $item_row->igst_amount;
                }
            }
            $taxable_amount = $pure_amount - $discount_total;
            $total_amount = $taxable_amount + $general_invoice_data->transport + $general_invoice_data->p_and_f + $general_invoice_data->insurance;
            $grand_total = $total_amount + $gst_total;
            ?>
            <?php
            for($blank_row = 8; $blank_row > $invoice_item_count; $blank_row--){
            ?>
            <tr>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold" colspan="">&nbsp;</td>
                <td class="text-bold text-right" colspan="">&nbsp;</td>
            </tr>
            <?php } ?>
            <tr>
                <td class=" text-bold" colspan="5"><b>In Words <?=$item_row->currency;?> : </b> <?=money_to_word($grand_total)?></td>
                <td class=" text-bold" colspan="3" align="right">Ex-Factory</td>
                <td class=" text-bold" colspan="2" align="right"><b>16000.00</b></td>
            </tr>
            <tr>
                <td class="text-bold no-border-bottom" colspan="5" rowspan="6" valign="top"><b>Declaration:</b> <?=nl2br($general_invoice_data->declaration);?></td>
                <td class=" text-bold" colspan="3" align="right">Packing & Forwarding</td>
                <td class=" text-bold" colspan="2" align="right"><b><?=$general_invoice_data->p_and_f;?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3" align="right">Sea Freight (CIF) <?=$general_invoice_data->port_of_loading;?></td>
                <td class=" text-bold" colspan="2" align="right"><b><?=$general_invoice_data->sea_freight;?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3" align="right"><b>Total <?=$item_row->currency;?></b></td>
                <td class=" text-bold" colspan="2" align="right"><b><?=number_format((float)$grand_total, 2, '.', '');?></b></td>
            </tr>
            <tr>
                <td class=" text-bold no-border-bottom" colspan="5" align="center" valign="top"><b>For, Jay Khodiyar Machine Tools</b> </td>
            </tr> 
            <tr>
                <td class=" text-bold no-border-top no-border-bottom text-center" colspan="5">
                    <br /><br /><br /><br /><br /><br /><br />
                </td>
            </tr>
            <tr>
                <td class=" text-bold no-border-top" colspan="5" align="center">Authorized Signature</td>
            </tr>

        </table>
        <?php } ?>
    </body>
</html>
