<?php $this->load->view('success_false_notify'); ?>
<style>
    /*label span { font-weight: bold; }*/
    label.display_value_in_label { border-radius: 0; box-shadow: none; border: 1px solid #d2d6de; background-color: #eee;}
</style>
<div class="content-wrapper">
    <form class="form-horizontal" action="" method="post" id="save_complain" novalidate>
        <?php if (isset($complain_data->complain_id) && !empty($complain_data->complain_id)) { ?>
            <input type="hidden" name="complain_data[complain_id]" id="complain_id" value="<?= $complain_data->complain_id ?>">
        <?php } ?>
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Service : Receive Complain</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, Ctrl+S = Save</label></small>
                <?php
                $add_role = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "add");
                $edit_role = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "edit");
                $view_role = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "view");
                ?>
                <?php if ($add_role || $edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
                <?php endif; ?>
                <?php if ($view_role): ?>
                    <a href="<?= base_url() ?>complain/complain_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Complain List</a>
                <?php endif; ?>
                <?php if ($add_role): ?>
                    <a href="<?= base_url() ?>complain/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Complain</a>
                <?php endif; ?>
                <span class="pull-right" style="margin-right: 20px;">
                    <div class="form-group">
                        <label for="is_received_post" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                            <input type="checkbox" name="complain_data[send_sms]" id="send_sms" class="send_sms" <?=(isset($complain_data->complain_id) && !empty($complain_data->complain_id)) ? '' : 'checked=""'; ?> >  &nbsp; Send SMS
                        </label>
                    </div>
                </span>
            </h1>
        </section>
        <div class="clearfix">
            <?php if ($add_role || $edit_role): ?>
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom" style="margin-bottom: 5px;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Complain Details</a></li>
                            <li><a href="#tab_2" data-toggle="tab" id="tabs_2">Item Details</a></li>
                            <li><a href="#tab_3" data-toggle="tab" id="tabs_3">Previous Complain</a></li>
                            <?php if (isset($complain_data->complain_id) && !empty($complain_data->complain_id)) { ?>
                                <li><a href="#tab_4" data-toggle="tab" id="tabs_4">Login</a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 input-sm">Complain No</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control col-sm-12 input-sm" value="<?= (isset($complain_data->complain_no_year)) ? $complain_data->complain_no_year : ''; ?>" readonly=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="complain_date" class="col-sm-3 input-sm">Complain Date</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="complain_data[complain_date]" id="complain_date" class="form-control input-sm input-datepicker" value="<?= (isset($complain_data->complain_date)) ? date('d-m-Y', strtotime($complain_data->complain_date)) : date('d-m-Y'); ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Party Detail </legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="complain_data[party_id]" id="party_id" class="form-control input-sm" onchange="party_details(this.value)"></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-3 input-sm">Address</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="city" class="col-sm-3 input-sm">City</label>
                                                        <div class="col-sm-9">
                                                            <div class="col-md-6" style="padding:0px !important;">
                                                                <select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
                                                            </div>
                                                            <div class="col-md-6" style="padding-right:0px !important;">
                                                                <input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="state" class="col-sm-3 input-sm">State</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="country" class="col-sm-3 input-sm">Country</label>
                                                        <div class="col-sm-9">
                                                            <select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
                                                            <small>Add multiple email id by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
                                                            <small>Add multiple phone number by Comma separated.</small>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="party_website" class="col-sm-3 input-sm">Website</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm website" id="party_website" name="party[website]" disabled="">
                                                        </div>
                                                    </div>  
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
                                                        <div class="col-sm-9 dispaly-flex">
                                                            <select name="party[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2" disabled=""></select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Designation</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_designation"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Department</label>
                                                        <div class="col-sm-9">
                                                            <div id="contact_person_department"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="" disabled="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold">Complain Received</legend>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="complain_by" class="col-sm-4 input-sm">Complain By<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="complain_data[complain_by]" id="complain_by" class="form-control input-sm" value="<?= (isset($complain_data->complain_by)) ? $complain_data->complain_by : ''; ?>" placeholder="Name of the person">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="received_by" class="col-sm-4 input-sm">Received By<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-4" style="padding-right: 0px;">
                                                        <select type="text" name="complain_data[received_by]" id="received_by" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-sm-4" style="padding-left: 5px;">
                                                        <input type="text" name="complain_data[received_by_details]" id="received_by_details" class="form-control input-sm" value="<?= (isset($complain_data->received_by_details)) ? $complain_data->received_by_details : ''; ?>" placeholder="Details">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-5 input-sm">Complain Call Received By</label>
                                                    <div class="col-sm-4" style="padding: 0px;">
                                                        <input type="text" class="form-control input-sm"  value="<?=(isset($complain_data->complain_id)) ? $complain_data->created_by_name : $this->session->userdata('is_logged_in')['name']; ?>" readonly="">
                                                    </div>
                                                    <div class="col-sm-3" style="padding: 0px;">
                                                        <input type="text" class="form-control input-sm"  value="<?=(isset($complain_data->complain_id)) ? $complain_data->created_by_contact : $this->session->userdata('is_logged_in')['contact_no']; ?>"readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="technical_support_by" class="col-sm-4 input-sm">Technical Support By</label>
                                                    <div class="col-sm-4" style="padding-right: 0px;">
                                                        <select name="complain_data[technical_support_by]" id="technical_support_by" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-sm-4" style="padding-left: 5px;">
                                                        <input type="text" name="complain_data[technical_support_by_contact]" id="technical_support_by_contact" class="form-control input-sm" value="<?= (isset($complain_data->technical_support_by_contact)) ? $complain_data->technical_support_by_contact : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="service_type" class="col-sm-4 input-sm">Service Type<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-8">
                                                        <select name="complain_data[service_type]" id="service_type" class="form-control input-sm"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="service_charge" class="col-sm-5 input-sm">Service Charge</label>
                                                    <div class="col-sm-7" style="padding: 0px;">
                                                        <input type="text" name="complain_data[service_charge]" id="service_charge" class="form-control input-sm" value="<?= (isset($complain_data->service_charge)) ? $complain_data->service_charge : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold">Complain Type</legend>
                                            <div class="">
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="padding: 0px 2px;">
                                                        <div class="form-group text-center">
                                                            <label for="" class="input-sm">Date</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding: 0px 2px;">
                                                        <div class="form-group text-center">
                                                            <label for="" class="input-sm">Type of Complain</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="padding: 0px 2px;">
                                                        <div class="form-group text-center">
                                                            <label for="" class="input-sm">Complain Box</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div id="complain_type_detail_fields">
                                                    <div id="complain_type_fields">
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="complain_type_details[complain_type_id][]" id="complain_type_id" class="complain_type_id" >
                                                            <div class="col-md-2" style="padding: 0px 2px;">
                                                                <div class="form-group">
                                                                    <input type="text" name="complain_type_details[complain_type_date][]" id="complain_type_date" class="complain_type_date form-control input-sm input-datepicker" value="<?= (isset($complain_type_details->complain_type_date)) ? date('d-m-Y', strtotime($complain_type_details->complain_type_date)) : date('d-m-Y'); ?>"> 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3" style="padding: 0px 2px;">
                                                                <div class="form-group">
                                                                    <input type="text" name="complain_type_details[type_of_complain][]" id="type_of_complain" class="form-control input-sm type_of_complain" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" style="padding: 0px 2px;">
                                                                <div class="form-group">
                                                                    <input type="text" name="complain_type_details[complain_box][]" id="complain_box" class="form-control input-sm complain_box" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button type="button" class="btn btn-danger btn-xs pull-center remove_complain_type_detail" style="display: none;"> <i class="fa fa-close"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-11">
                                                    <button type="button" class="btn btn-info btn-xs pull-right add_complain_type_detail"> <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div> 
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Select Installation Items</legend>
                                            <div class="col-md-12 installation_items_section"></div>
                                        </fieldset>
                                        <div class="installation_wise_et_tp" id="installation_wise_et_tp"></div>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Installation Detail</legend>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Installation No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="installation_no_year"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Installation Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="installation_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Testing Report No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="testing_report_no_year"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Testing Report Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="testing_report_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Challan No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="challan_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-5 input-sm">Challan Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="challan_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="invoice_no" class="col-sm-5 input-sm">Invoice No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="invoice_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="invoice_date" class="col-sm-5 input-sm">Invoice Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="invoice_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="quotation_no" class="col-sm-5 input-sm">Quotation No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="quotation_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="quotation_date" class="col-sm-5 input-sm">Quotation Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="quotation_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="purchase_order_no" class="col-sm-5 input-sm">Purchase Order No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="cust_po_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="purchase_order_date" class="col-sm-5 input-sm">Purchase Order Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="po_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_no" class="col-sm-5 input-sm">Sales Order No</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="sales_order_no"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sales_order_date" class="col-sm-5 input-sm">Sales Order Date</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="sales_order_date"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="manufacturing_m_y" class="col-sm-5 input-sm">Manufacturing Month & Year</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="manufacturing_m_y"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="delivery_of_equipment" class="col-sm-5 input-sm">Delivery of Equipment</label>
                                                            <label class="col-sm-7 input-sm display_value_in_label"><span id="delivery_of_equipment"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="installation_commissioning_by" class="col-sm-5 input-sm">Installation & Commissioning By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="installation_commissioning_by"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="installation_commissioning_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="jk_technical_person" class="col-sm-5 input-sm">JK Technical Person</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="jk_technical_person"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="jk_technical_person_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="project_training_provide_by" class="col-sm-5 input-sm">Project Training Provide By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="project_training_provide_by"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="project_training_provide_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_site_incharge_person" class="col-sm-5 input-sm">Customer Site Incharge Person</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_site_incharge_person"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_site_incharge_person_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_site_quality_checked_by_name" class="col-sm-5 input-sm">Customer Site Quality Checked By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_site_quality_checked_by_name"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_site_quality_checked_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_technical_operator_person" class="col-sm-5 input-sm">Customer Technical Operator Person</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_technical_operator_person"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_technical_operator_person_contact"></span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_site_equipment_inspection_by_name" class="col-sm-5 input-sm">Customer Site Equipment Inspe. By</label>
                                                            <label class="col-sm-4 input-sm display_value_in_label"><span id="customer_site_equipment_inspection_by_name"></span></label>
                                                            <label class="col-sm-3 input-sm display_value_in_label"><span id="customer_site_equipment_inspection_by_contact"></span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                            <div class="col-md-6">
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_name" class="col-sm-3 input-sm">Equipment Name</label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_name"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_code" class="col-sm-3 input-sm">Item Code</label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_code"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_description" class="col-sm-3 input-sm">Item Description</label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_description"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="quantity" class="col-sm-3 input-sm">Quantity</label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="quantity"></span></label>
                                                    <label for="item_extra_accessories_id" class="col-sm-3 input-sm text-right">Item Extra Accessories</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="item_extra_accessories_id"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-3 input-sm"> Item Serial No. </label>
                                                    <label class="col-sm-9 input-sm display_value_in_label"><span id="item_serial_no"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Customer Premises</legend>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="land_building_factory_area" class="col-sm-6 input-sm" style="padding: 2px 9px !important;">Land Building & Factory Area</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="land_building_factory_area"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="plant_room_size" class="col-sm-6 input-sm">Plant Room Size</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="plant_room_size"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="prepared_foundation" class="col-sm-6 input-sm">Prepared Foundation</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="prepared_foundation"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="foundation_marking_by" class="col-sm-6 input-sm">Foundation Marking By</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="foundation_marking_by"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="no_of_foundation" class="col-sm-6 input-sm">No of Foundation</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="no_of_foundation"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="unloading_date" class="col-sm-6 input-sm">Unloading Date</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="unloading_date"></span></label>
                                                </div>
                                            </div>    
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="filling_of_foundation_boxes" class="col-sm-6 input-sm">Filling of Foundation Boxes</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="filling_of_foundation_boxes"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="installation_start" class="col-sm-6 input-sm">Installation Start</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="installation_start"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="installation_finished" class="col-sm-6 input-sm">Installation Finished</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="installation_finished"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">    
                                                <div class="form-group">
                                                    <label for="electric_power" class="col-sm-6 input-sm">Electric Power</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electric_power"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="electric_connection" class="col-sm-6 input-sm">Electric Connection</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electric_connection"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="elec_rm2_panel_board_cable" class="col-sm-6 input-sm">Elec Rm 2 Panel Board Cable</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="elec_rm2_panel_board_cable"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="connection_cable_1" class="col-sm-5 input-sm" style="">All Connection Cable</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="connection_cable_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="connection_cable_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-5 input-sm display_value_in_label"><span id="water_tank_chiller_system_label"></span></label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="water_tank_chiller_system_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="water_tank_chiller_system_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-5 input-sm display_value_in_label"><span id="water_plumbing_chiller_conn_label"></span></label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="water_plumbing_chiller_conn_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="water_plumbing_chiller_conn_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="lubrication_gear_oil_1" class="col-sm-5 input-sm">Lubrication Gear Oil</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="lubrication_gear_oil_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="lubrication_gear_oil_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="gear_box_oil_1" class="col-sm-5 input-sm">Gear Box Oil</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="gear_box_oil_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="gear_box_oil_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="raw_material_1" class="col-sm-5 input-sm">Raw Material</label>
                                                    <label class="col-sm-4 input-sm display_value_in_label"><span id="raw_material_1"></span></label>
                                                    <label class="col-sm-3 input-sm display_value_in_label"><span id="raw_material_2"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="run_start_date" class="col-sm-6 input-sm">No Load Free Run Start Date</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="run_start_date"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="run_hrs" class="col-sm-6 input-sm">No Load Total Free Run Hrs</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="run_hrs"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="production_start" class="col-sm-6 input-sm">Production Start</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="production_start"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name_of_raw_material" class="col-sm-6 input-sm">Name of Raw Material</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="name_of_raw_material"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="moisture_of_raw_material" class="col-sm-6 input-sm">Moisture of Raw Material</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="moisture_of_raw_material"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="after_drying_moisture_content" class="col-sm-6 input-sm" style="padding: 2px 7px !important;">After Drying Moisture Content</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="after_drying_moisture_content"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="approx_production_per_hr" class="col-sm-6 input-sm">Approx Production Per Hr</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="approx_production_per_hr"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="product_quality" class="col-sm-6 input-sm">Product Quality</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="product_quality"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="electric_consumption_unit_hr" class="col-sm-6 input-sm" style="padding: 2px 7px !important;">Electric Consumption Unit/Hr</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electric_consumption_unit_hr"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="electricity_rate_per_unit" class="col-sm-6 input-sm">Electricity Rate Per Unit</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="electricity_rate_per_unit"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="unskilled_labour" class="col-sm-6 input-sm">Unskilled Labour</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="unskilled_labour"></span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="skilled_labour" class="col-sm-6 input-sm">Skilled Labour</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="skilled_labour"></span></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="tractor_loader" class="col-sm-6 input-sm">Do you have tractor loader?</label>
                                                    <label class="col-sm-6 input-sm display_value_in_label"><span id="tractor_loader"></span></label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Electric Motor & Equipment Testing Report At Customer's Premises</legend>
                                            <div class="col-md-12">
                                                <div class="table-responsive">          
                                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th><strong>User</strong></th>
                                                                <th><strong>Make</strong></th>
                                                                <th><strong>HP</strong></th>
                                                                <th><strong>KW</strong></th>
                                                                <th><strong>Frequency</strong></th>
                                                                <th><strong>RPM</strong></th>
                                                                <th><strong>Volts</strong></th>
                                                                <th><strong>Serial No.</strong></th>
                                                                <th><strong>Normal Amps</strong></th>
                                                                <th><strong>Working Amps</strong></th>
                                                                <th><strong>Equipment RPM</strong></th>
                                                                <th><strong>Total WorkingHours</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="motor_testing_details_c"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="table-responsive">          
                                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th><strong>User</strong></th>
                                                                <th><strong>Make</strong></th>
                                                                <th><strong>Gear Type</strong></th>
                                                                <th><strong>Model</strong></th>
                                                                <th><strong>Ratio</strong></th>
                                                                <th><strong>Serial No.</strong></th>
                                                                <th><strong>Normal Amps</strong></th>
                                                                <th><strong>Working Amps</strong></th>
                                                                <th><strong>Equipment RPM</strong></th>
                                                                <th><strong>Total WorkingHours</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="gearbox_testing_details_c"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Equipment Testing Normal & complete production Report at Customer's Premises</legend>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-2 input-sm"><strong>Testing Capacity</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Capacity Hrs</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Test of Raw Material</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>Moisture of Raw Material</strong></label>
                                                    <label for="" class="col-sm-2 input-sm"><strong>After Process of Moisture</strong></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-2 input-sm">Testing Normal Load</label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_capacity_hrs"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_test_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_moisture_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_normal_after_procces_moisture"></span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="item_serial_no" class="col-sm-2 input-sm">Testing Full Load</label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_capacity_hrs"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_test_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_moisture_raw_material"></span></label>
                                                    <label class="col-sm-2 input-sm display_value_in_label"><span id="c_testing_full_after_procces_moisture"></span></label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold"> Temperature Parameters At Customer's Premises</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_temp_oli" class="col-sm-6 input-sm">Oil Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_oli"></span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="c_temp_eqipment" class="col-sm-6 input-sm">Equipment Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_eqipment"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_temp_head" class="col-sm-6 input-sm">Head Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_head"></span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="c_temp_kiln" class="col-sm-6 input-sm">Kiln Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_kiln"></span></label>
                                                    </div>
                                                </div>    
                                                <div class="col-md-4">    
                                                    <div class="form-group">
                                                        <label for="c_temp_hot_air" class="col-sm-6 input-sm">Hot Air Temperature</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_hot_air"></span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="c_temp_weather" class="col-sm-6 input-sm">Weather</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_temp_weather"></span></label>
                                                    </div>
                                                </div>											
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Equipment Testing</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_et_equipment_rpm" class="col-sm-6 input-sm">Equipment RPM</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_et_equipment_rpm"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_et_testing" class="col-sm-6 input-sm">Testing</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_et_testing"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_et_stop_run_time" class="col-sm-6 input-sm">Stop Run Time (Minutes)</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_et_stop_run_time"></span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="scheduler-border">										   
                                            <legend class="scheduler-border text-primary text-bold">Control Panel Board</legend>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_cpb_testing" class="col-sm-6 input-sm">Testing</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_cpb_testing"></span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="c_cpb_working_hours" class="col-sm-6 input-sm">Working Hours</label>
                                                        <label class="col-sm-6 input-sm display_value_in_label"><span id="c_cpb_working_hours"></span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3">
                                <div class="row">
                                    <div class="col-md-12">
<!--                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary text-bold"> Item Details</legend>
                                        </fieldset>-->
                                        <div class="previous_complain_data"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_4">
                                <?php if (isset($complain_data->complain_id) && !empty($complain_data->complain_id)) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset class="scheduler-border">
                                                <legend class="scheduler-border text-primary text-bold"> Login Details </legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_by" id="created_by" value="<?= (isset($complain_data->created_by_name)) ? $complain_data->created_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Created Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm created_at" id="created_at" value="<?= (isset($complain_data->created_at)) ? $complain_data->created_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated By</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_by" id="updated_by" value="<?= (isset($complain_data->updated_by_name)) ? $complain_data->updated_by_name : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 input-sm">Updated Date</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control input-sm updated_at" id="updated_at" value="<?= (isset($complain_data->updated_at)) ? $complain_data->updated_at : ''; ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
            <?php endif; ?>
        </div>
        <section class="content-header">
            <?php if ($add_role || $edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
            <?php endif; ?>
            <?php if ($view_role): ?>
                <a href="<?= base_url() ?>complain/complain_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Complain List</a>
            <?php endif; ?>
            <?php if ($add_role): ?>
                <a href="<?= base_url() ?>complain/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Complain</a>
            <?php endif; ?>
        </section>
    </form>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[type="checkbox"].send_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        
        initAjaxSelect2($("#received_by"), "<?= base_url('app/reference_select2_source') ?>");
        <?php if (isset($complain_data->received_by)) { ?>
            setSelect2Value($("#received_by"), "<?= base_url('app/set_reference_select2_val_by_id/' . $complain_data->received_by) ?>");
        <?php } ?>
        
        initAjaxSelect2($("#city"), "<?= base_url('app/city_select2_source') ?>");
        initAjaxSelect2($("#state"), "<?= base_url('app/state_select2_source') ?>");
        initAjaxSelect2($("#country"), "<?= base_url('app/country_select2_source') ?>");
        initAjaxSelect2($("#reference_id"), "<?= base_url('app/reference_select2_source') ?>");
        initAjaxSelect2($("#party_id"),"<?=base_url('app/instalation_party_select2_source')?>");
        <?php if (isset($complain_data->party_id)) { ?>
            setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id/' . $complain_data->party_id) ?>");
            party_details(<?= $complain_data->party_id; ?>);
            get_installation_with_item_list(<?= $complain_data->party_id; ?>);
        <?php } ?>
        initAjaxSelect2($("#technical_support_by"), "<?= base_url('app/technical_staff_select2_source') ?>");
        <?php if (isset($complain_data->technical_support_by)) { ?>
            setSelect2Value($("#technical_support_by"), "<?= base_url('app/set_staff_select2_val_by_id/' . $complain_data->technical_support_by) ?>");
        <?php } ?>
        initAjaxSelect2($("#service_type"), "<?= base_url('app/service_type_select2_source') ?>");
        <?php if (isset($complain_data->service_type)) { ?>
            setSelect2Value($("#service_type"), "<?= base_url('app/set_service_type_select2_val_by_id/' . $complain_data->service_type) ?>");
        <?php } ?>

        $(document).on('click', '.add_complain_type_detail', function () {
            $("#complain_type_fields").clone().appendTo("#complain_type_detail_fields");
            $(".remove_complain_type_detail:last").css('display', 'block');
            $(".complain_type_id:last ").val('');
            $(".complain_type_date:last, .complain_type_date:last, .type_of_complain:last, .complain_box:last").val('');
            $(".complain_type_date:last").datepicker({format: 'dd-mm-yyyy', todayBtn: "linked", todayHighlight: true, autoclose: true}).val('<?=date('d-m-Y');?>');
        });

        $(document).on('click', '.remove_complain_type_detail', function () {
            $(this).closest("#complain_type_fields").remove();
            var complain_type_id = $(this).closest("#complain_type_fields").find("#complain_type_id").val();
            $('#save_complain').append('<input type="hidden" name="deleted_complain_type_id[]" id="deleted_complain_type_id" value="' + complain_type_id + '" />');
        });
        
        $(document).on('change',"#party_id",function(){
            var id = $(this).val();
            $('label.display_value_in_label span').html('');
            $('tbody#motor_testing_details_c').html('');
            $('tbody#gearbox_testing_details_c').html('');
            $('#installation_wise_et_tp').html('');
            if(id != '' && id != null){
                get_installation_with_item_list(id);
            }
		});
        
//        $(document).on('change',"#chk_all",function(){
//        	$("input.chk_challan_item").prop('checked', $(this).prop("checked"));
//            change_checkbox_value();
//		});
        $(document).on('change',".chk_challan_item",function(){
            var installation_id = $(this).data('installation_id');
            change_checkbox_value();
            if(this.checked) {
				$('#hide_div_'+installation_id).removeClass("hidden");
            } else {
                $('#hide_div_'+installation_id).addClass("hidden");
				$('#hide_div_'+installation_id).find('input:text').val(''); 
				$("#vr_div_"+installation_id).empty();
            }
        });
        
        $(document).on("change", '#contact_person_id', function () {
			var contact_person_id = $(this).val();
			$.ajax({
				url: "<?= base_url(); ?>party/get-contact-person-by-id/" + contact_person_id,
				type: "POST",
				data: null,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					if (data.success == true) {
						$('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
						$('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
						$('#contact_person_designation').html(data.contact_person_data.designation);
						$('#contact_person_department').html(data.contact_person_data.department);
					} else {
						$('input[name="contact_person[contact_person_mobile_no]"]').val('');
						$('input[name="contact_person[contact_person_email_id]"]').val('');
						$('#contact_person_designation').html('');
						$('#contact_person_department').html('');
					}
				}
			});
		});
        
        $(document).on("change", '#technical_support_by', function () {
            var id = $(this).val();
            var id_attr = $(this).attr('id');
            if(id != '' && id != null){
                $.ajax({
                    url: "<?= base_url(); ?>testing_report/get_staff_contact_no/" + id,
                    type: "POST",
                    success: function (data) {
                        $('#'+id_attr+'_contact').val('');
                        $('#'+id_attr+'_contact').val(data);
                    }
                });
            }
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_complain").submit();
                return false;
            }
        });        
        $(document).on('submit', '#save_complain', function () {
			if ($.trim($("#party_id").val()) == '') {
				show_notify('Please Select Party.', false);
                $("#party_id").focus();
				return false;
			}
            
			if ($.trim($("#complain_by").val()) == '') {
				show_notify('Please Enter Complain By Name.', false);
                $("#complain_by").focus();
				return false;
			}
            
			if ($.trim($("#received_by").val()) == '') {
				show_notify('Please Select Received By.', false);
                $("#received_by").focus();
				return false;
			}
            
			if ($.trim($("#received_by_details").val()) == '') {
				show_notify('Please Enter Received Details.', false);
                $("#received_by_details").focus();
				return false;
			}
            
			if ($.trim($("#service_type").val()) == '') {
				show_notify('Please Select Service Type.', false);
                $("#service_type").focus();
				return false;
			}
            
            if ($("input.chk_challan_item:checked").length == 0) {
                show_notify('Please select at least one item.', false);
                $("#ajax-loader").hide();
                return false;
            }
            
			$('.module_save_btn').attr('disabled', 'disabled');
			$("#ajax-loader").show();
			var postData = new FormData(this);
            $("input.chk_challan_item:checked").each(function(index,value){
				postData.append('complain_items['+index+'][installation_id]',$(this).data('installation_id'));
				postData.append('complain_items['+index+'][testing_report_id]',$(this).data('testing_report_id'));
				postData.append('complain_items['+index+'][challan_id]',$(this).data('challan_id'));
			});
			$.ajax({
				url: "<?= base_url('complain/save_complain') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
					if (json.status == 0) {
						show_notify(json.msg, false);
						$('.module_save_btn').removeAttr('disabled', 'disabled');
						return false;
					}
					if (json['success'] == 'false') {
						show_notify(json['msg'], false);
						$('.module_save_btn').removeAttr('disabled', 'disabled');
					}
					if (json['success'] == 'Added') {
						window.location.href = "<?php echo base_url('complain/complain_list') ?>";
					}
					if (json['success'] == 'Updated') {
						window.location.href = "<?php echo base_url('complain/complain_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
		});
        
        <?php if(isset($complain_data->complain_id) && !empty($complain_data->complain_id)){ ?>
            var complain_type_details = <?=$complain_type_details;?>;
            //console.log(complain_type_details);
            $('#complain_type_detail_fields #complain_type_fields').not('#complain_type_fields:first').remove();
            var complain_type_details_rows = Object.keys(complain_type_details).length;
            var row_inc = 1;
            $.each(complain_type_details, function(complain_type_details_i,complain_type_details_v){
                $.each(complain_type_details_v, function(index,value){
                    if(index == 'id'){
                        complain_type_details_v['complain_type_id'] = complain_type_details_v[index];
                        index = 'complain_type_id';
                    }
                    if(index == 'complain_id'){
                        delete complain_type_details_v[index];
                    }
                    if(value == null){ value = ''; }
                    $('#complain_type_detail_fields #complain_type_fields:nth-last-child(1) #'+ index).val(value);
                });
                if(row_inc < complain_type_details_rows){
                    $('.add_complain_type_detail').click();
                }
                row_inc++;
            });
        <?php } ?>
        
    });

    function change_checkbox_value() {
        if($('.chk_challan_item:checked').data('installation_id')){
            var installation_id = $('.chk_challan_item:checked').data('installation_id');
            var testing_report_id = $('.chk_challan_item:checked').data('testing_report_id');
            var challan_id = $('.chk_challan_item:checked').data('challan_id');
            feed_installation_data(installation_id, testing_report_id, challan_id);
        } else {
            $('label.display_value_in_label span').html('');
            $('tbody#motor_testing_details_c').html('');
            $('tbody#gearbox_testing_details_c').html('');
        }
    }

    function feed_installation_data(installation_id, testing_report_id, challan_id) {
		$("#ajax-loader").show();
		$.ajax({
			url: '<?php echo BASE_URL; ?>complain/get_installation_details',
			type: "POST",
			async: false,
			data: {installation_id: installation_id, testing_report_id: testing_report_id, challan_id: challan_id},
			success: function (data) {
				json = JSON.parse(data);
				//console.log(json);
				var installation_data = json.installation_data;
				var testing_report_data = json.testing_report_data;
				var challan_data = json.challan_data;
				var challan_item = json.challan_item_data;

                $.each(installation_data,function(index,value){
                    if(index != 'party_id'){
                        $("#" + index).html(value);
                    }
				});
                $.each(testing_report_data,function(index,value){
                    if(index != 'party_id'){
                        $("#" + index).html(value);
                    }
				});
                $.each(challan_data,function(index,value){
                    if(index != 'party_id'){
                        $("#" + index).html(value);
                    }
				});
                $('#item_id').html(challan_item.item_id);
				$('#item_name').html(challan_item.item_name);
				$('#item_code').html(challan_item.item_code);
				$('#quantity').html(challan_item.quantity);
				$('#item_serial_no').html(challan_item.item_serial_no);
				$('#item_extra_accessories_id').html(challan_item.item_extra_accessories);
				$('#item_description').html(challan_item.item_description);
                //$('select[name="party[kind_attn_id]"]').val(challan_data.kind_attn_id).change();

				var li_motor_serial_objectdata = json.motor_data;
				var motor_serial_objectdata = [];
				if (li_motor_serial_objectdata != '') {
					$.each(li_motor_serial_objectdata, function (index, value) {
						motor_serial_objectdata.push(value);
					});
				}
				display_motor_serial_html(motor_serial_objectdata);
				var li_gearbox_serial_objectdata = json.gearbox_data;
				var gearbox_serial_objectdata = [];
				if (li_gearbox_serial_objectdata != '') {
					$.each(li_gearbox_serial_objectdata, function (index, value) {
						gearbox_serial_objectdata.push(value);
					});
				}
				display_gearbox_serial_html(gearbox_serial_objectdata);
				$("#ajax-loader").hide();
			},
		});
	}
    
    function display_motor_serial_html(motor_serial_objectdata){
		var motor_testing_details_c = '';
		$.each(motor_serial_objectdata, function (index, value) {
			
			var motor_testing_no_c = '<tr class="motor_serial_index_' + index + '">';
			motor_testing_no_c += '<td>' + value.motor_user + '</td>';
			motor_testing_no_c += '<td>' + value.motor_make + '</td>';
			motor_testing_no_c += '<td>' + value.motor_hp + '</td>';
			motor_testing_no_c += '<td>' + value.motor_kw + '</td>';
			motor_testing_no_c += '<td>' + value.motor_frequency + '</td>';
			motor_testing_no_c += '<td>' + value.motor_rpm + '</td>';
			motor_testing_no_c += '<td>' + value.motor_volts_cycles + '</td>';
			motor_testing_no_c += '<td>' + value.motor_serial_no + '</td>';
			var normal_amps_c = value.normal_amps_c == null ? '' : value.normal_amps_c;
			var working_amps_c = value.working_amps_c == null ? '' : value.working_amps_c;
			var equipment_rpm_c = value.equipment_rpm_c == null ? '' : value.equipment_rpm_c;
			var total_workinghours_c = value.total_workinghours_c == null ? '' : value.total_workinghours_c;
			motor_testing_no_c += '<td>' + normal_amps_c + '</td>';
			motor_testing_no_c += '<td>' + working_amps_c + '</td>';
			motor_testing_no_c += '<td>' + equipment_rpm_c + '</td>';
			motor_testing_no_c += '<td>' + total_workinghours_c + '</td>';
			motor_testing_details_c += motor_testing_no_c;

		});
		$('tbody#motor_testing_details_c').html(motor_testing_details_c);
	}
    
	function display_gearbox_serial_html(gearbox_serial_objectdata){
		var gearbox_testing_details_c = '';
		$.each(gearbox_serial_objectdata, function (index, value) {
			
			var gearbox_testing_no_c = '<tr class="gearbox_serial_index_' + index + '">';
			gearbox_testing_no_c += '<td>' + value.gearbox_user + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_make + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_gear_type + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_model + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_ratio + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_serial_no + '</td>';
			var normal_amps_c = value.normal_amps_c == null ? '' : value.normal_amps_c;
			var working_amps_c = value.working_amps_c == null ? '' : value.working_amps_c;
			var equipment_rpm_c = value.equipment_rpm_c == null ? '' : value.equipment_rpm_c;
			var total_workinghours_c = value.total_workinghours_c == null ? '' : value.total_workinghours_c;
			gearbox_testing_no_c += '<td>'+ normal_amps_c + '</td>';
			gearbox_testing_no_c += '<td>'+ working_amps_c + '</td>';
			gearbox_testing_no_c += '<td>'+ equipment_rpm_c + '</td>';
			gearbox_testing_no_c += '<td>'+ total_workinghours_c + '</td>';
			gearbox_testing_details_c += gearbox_testing_no_c;

		});
		$('tbody#gearbox_testing_details_c').html(gearbox_testing_details_c);
	}

    function get_installation_with_item_list(party_id){
        $("#ajax-loader").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>complain/get_installation_with_item_list/'+party_id,
            type: "POST",
            data: null,
            async: false,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('div.installation_items_section').html(data.installation_with_item_html);
                $('div.installation_wise_et_tp').html(data.installation_wise_et_tp_html);
                
                <?php if(isset($complain_items_data) && !empty($complain_items_data)){ ?>
                    <?php foreach ($complain_items_data as $complain_item){ ?>
                        var selected_installation_id = [];
                        selected_installation_id = <?=json_encode($complain_item->installation_id);?>;
                        $("input.chk_challan_item[data-installation_id='"+selected_installation_id+"']").prop('checked',true);
                        $('#hide_div_'+selected_installation_id).removeClass("hidden");
                        $('#c_testing_normal_capacity_hrs_'+selected_installation_id).val('<?=$complain_item->c_testing_normal_capacity_hrs;?>');
                        $('#c_testing_normal_test_raw_material_'+selected_installation_id).val('<?=$complain_item->c_testing_normal_test_raw_material;?>');
                        $('#c_testing_normal_moisture_raw_material_'+selected_installation_id).val('<?=$complain_item->c_testing_normal_moisture_raw_material;?>');
                        $('#c_testing_normal_after_procces_moisture_'+selected_installation_id).val('<?=$complain_item->c_testing_normal_after_procces_moisture;?>');
                        $('#c_testing_full_capacity_hrs_'+selected_installation_id).val('<?=$complain_item->c_testing_full_capacity_hrs;?>');
                        $('#c_testing_full_test_raw_material_'+selected_installation_id).val('<?=$complain_item->c_testing_full_test_raw_material;?>');
                        $('#c_testing_full_moisture_raw_material_'+selected_installation_id).val('<?=$complain_item->c_testing_full_moisture_raw_material;?>');
                        $('#c_testing_full_after_procces_moisture_'+selected_installation_id).val('<?=$complain_item->c_testing_full_after_procces_moisture;?>');
                        $('#c_temp_oli_'+selected_installation_id).val('<?=$complain_item->c_temp_oli;?>');
                        $('#c_temp_eqipment_'+selected_installation_id).val('<?=$complain_item->c_temp_eqipment;?>');
                        $('#c_temp_head_'+selected_installation_id).val('<?=$complain_item->c_temp_head;?>');
                        $('#c_temp_kiln_'+selected_installation_id).val('<?=$complain_item->c_temp_kiln;?>');
                        $('#c_temp_hot_air_'+selected_installation_id).val('<?=$complain_item->c_temp_hot_air;?>');
                        $('#c_temp_weather_'+selected_installation_id).val('<?=$complain_item->c_temp_weather;?>');
                    <?php } ?>
                    feed_installation_data(<?=$complain_items_data[0]->installation_id; ?>, <?=$complain_items_data[0]->testing_report_id; ?>, <?=$complain_items_data[0]->challan_id; ?>);
                <?php } ?>
                    
                var previous_complain_html = '';
                var c_inc = 1;
                $.each(data.previous_complain_data, function (index, value) {
                    previous_complain_html += '<div class="box box-default box-solid"><div class="box-header with-border"><h3 class="box-title">'+c_inc+'. Complain No. '+value.complain_no+'</h3><div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button></div></div>';
                    previous_complain_html += '<div class="box-body" style="">';
                    previous_complain_html += '<table class="table table-bordered">';
                    previous_complain_html += '<tr>';
                    previous_complain_html += '<td width="200px">Complain By</td>';
                    previous_complain_html += '<th>: '+value.complain_by+'</th>';
                    previous_complain_html += '<td width="200px">Complain Call Receive By</td>';
                    previous_complain_html += '<th>: '+value.created_by_name+'</th>';
                    previous_complain_html += '</tr>';
                    previous_complain_html += '<tr>';
                    previous_complain_html += '<td>Complain No.</td>';
                    previous_complain_html += '<th>: '+value.complain_no+'</th>';
                    previous_complain_html += '<td>Complain Date</td>';
                    previous_complain_html += '<th>: '+value.complain_date+'</th>';
                    previous_complain_html += '</tr>';
                    previous_complain_html += '<tr>';
                    previous_complain_html += '<td>Resolve Complain No.</td>';
                    if(value.resolve_complain_no != null){
                        previous_complain_html += '<th>: '+value.resolve_complain_no+'</th>';
                    } else {
                        previous_complain_html += '<th>: </th>';
                    }
                    previous_complain_html += '<td>Resolve Complain Date</td>';
                    if(value.resolve_complain_date != null){
                        previous_complain_html += '<th>: '+value.resolve_complain_date+'</th>';
                    } else {
                        previous_complain_html += '<th>: </th>';
                    }
                    previous_complain_html += '</tr>';
                    previous_complain_html += '<tr>';
                    previous_complain_html += '<td>Complain Invoice No.</td>';
                    previous_complain_html += '<th>: </th>';
                    previous_complain_html += '<td>Payment Receipt No.</td>';
                    previous_complain_html += '<th>: </th>';
                    previous_complain_html += '</tr>';
                    previous_complain_html += '</table>';
                    previous_complain_html += '</div></div>';
                    c_inc++;
                });
                $('div.previous_complain_data').html(previous_complain_html);
                $("#ajax-loader").hide();
            },
            error: function (msg) {
                $("#ajax-loader").hide();
            }
        });
    }

    function party_details(id) {
        $("#ajax-loader").show();
        $.ajax({
            type: "POST",
            url: '<?= base_url(); ?>sales/ajax_load_party_with_cnt_person/' + id,
            async: false,
            data: 'party_id='+id,
            success: function (data) {
                var json = $.parseJSON(data);

                if (json['address']) {
                    $(".address").html(json['address']);
                } else {
                    $(".address").html("");
                }

                if (json['party_code']) {
                    $('#party_code').val(json['party_code']);
                } else {
                    $("#party_code").val("");
                }
                if (json['party_name']) {
                    $('#party_name').val(json['party_name']);
                }
                if (json['party_id']) {
                    $(".party_id").val(json['party_id']);
                    $("#p_party_id").val(json['party_id']);
                }

                if (json['city_id']) {
                    setSelect2Value($("#city"), '<?= base_url() ?>app/set_city_select2_val_by_id/' + json['city_id']);
                } else {
                    setSelect2Value($("#city"));
                }
                if (json['state_id']) {
                    setSelect2Value($("#state"), '<?= base_url() ?>app/set_state_select2_val_by_id/' + json['state_id']);
                } else {
                    setSelect2Value($("#state"));
                }
                if (json['country_id']) {
                    setSelect2Value($("#country"), '<?= base_url() ?>app/set_country_select2_val_by_id/' + json['country_id']);
                } else {
                    setSelect2Value($("#country"));
                }

                if (json['fax_no']) {
                    $(".fax_no").val(json['fax_no']);
                } else {
                    $(".fax_no").val("");
                }

                if (json['email_id']) {
                    $(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
                } else {
                    $(".email_id").html("");
                }

                if (json['website']) {
                    $(".website").val(json['website']);
                } else {
                    $(".website").val("");
                }
                if (json['pincode']) {
                    $(".pincode").val(json['pincode']);
                } else {
                    $(".pincode").val("");
                }
                if (json['phone_no']) {
                    $(".phone_no").val(json['phone_no']);
                } else {
                    $(".phone_no").val("");
                }
                if (json['reference_id']) {
                    setSelect2Value($("#reference_id"), '<?= base_url() ?>app/set_reference_select2_val_by_id/' + json['reference_id']);
                } else {
                    setSelect2Value($("#reference_id"));
                }
                if (json['reference_description']) {
                    $("#reference_description").html(json['reference_description']);
                } else {
                    $("#reference_description").html('');
                }

                if (json['party_type_1_id'] != '') {
                    setSelect2Value($("#sales_id"), '<?= base_url() ?>app/set_sales_select2_val_by_id/' + json['party_type_1_id']);
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_DOMESTIC_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= INR_CURRENCY_ID; ?>);
                    }
                    if (json['party_type_1_id'] == '<?= PARTY_TYPE_EXPORT_ID; ?>') {
                        setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= USD_CURRENCY_ID; ?>);
                    }
                    var currency_title = $('#select2-currency_id-container').attr('title');
                    $('#received_payment_currency').html(currency_title);
                } else {
                    setSelect2Value($("#sales_id"));
                }

                if (json['contact_persons_array']) {
                    var option_html = '';
                    if (json['contact_persons_array'].length > 0) {
                        //                            console.log(json['contact_persons_array']);
                        $.each(json['contact_persons_array'], function (index, value) {
                            option_html += "<option value='" + value.contact_person_id + "'>" + value.name + "</option>";
                            $('input[name="contact_person[contact_person_mobile_no]"]').val(value.mobile_no);
                            $('input[name="contact_person[contact_person_email_id]"]').val(value.email);
                            $('#contact_person_designation').html(value.designation);
                            $('#contact_person_department').html(value.department);
                        })
                        $('select[name="party[kind_attn_id]"]').html(option_html).select2();
                    } else {
                        $('select[name="party[kind_attn_id]"]').html('');
                    }
                    $('select[name="party[kind_attn_id]"]').change();
                } else {
                    $('select[name="party[kind_attn_id]"]').html('');
                    $('select[name="party[kind_attn_id]"]').change();
                }
                
                $("#ajax-loader").hide();
            }
        });
    }
</script>
