<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class='text-primary text-bold'> Complain List </small>
            <?php $add_role = $this->app_model->have_access_role(COMPLAIN_MODULE_ID, "add"); ?>
            <?php if($add_role): ?>
                <a href="<?=base_url()?>complain/add" class="btn btn-info btn-xs pull-right">Add Complain</a>
            <?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_complain_list" class="table custom-table agent-table">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Complain No</th>
                                    <th>Complain Date</th>
                                    <th>Party Name</th>
                                    <th>Contact No</th>
                                    <th>Email Id</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<div class="modal fade" id="open_show_itemprint_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:60%;height: auto;max-height: 100%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">
					Installation Item Print
				</h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive" style="overflow-x:hidden">
					<table id="example1" class="table custom-table table-striped dataTable">
						<thead>
							<tr>
								<th width="20%">Installation No.</th>
								<th width="50%">Installation Item</th>
								<th width="30%">Print</th>
							</tr>                        
						</thead>
						<tbody id="items_td">							                  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function() {

        table = $('#table_complain_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('complain/complain_datatable')?>",
                "type": "POST",
                "data": function(d){
                    //d.city_id = $("#city_id").val();
                },
            },
            "scrollY": 550,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=complain',
                    success: function(data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });
        
        $(document).on('click', '.show_itemprint_modal', function(e){
            e.preventDefault();
            var complain_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();
            $( "#items_td" ).html('');
            $.ajax({
                url: '<?php echo base_url('complain/feed_complain_items/') ?>/'+complain_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.item_data) {
                        jQuery.each( data.item_data, function( i, val ) {
//                            console.log(val);
                            $( "#items_td" ).append('<tr><td>'+val.installation_no_year+'</td><td>'+val.item_name+'</td><td><a href="<?php echo base_url('complain/received_complain/'); ?>/'+complain_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        });
                    }
                }
            });
        });

    });
</script>
