<html>
    <head>
        <title>Received Complain</title>
        <style>
            .text-center{
                text-align: center;
            }
            table{
                border-spacing: 0;
                width: 100%;
                border-bottom: 1px solid;
                border-right: 1px solid;
            }
            td{
                padding: 1px 1px 1px 1px;
                border-left: 1px solid;
                border-top: 1px solid;
                font-size:10px;
                margin:0px; 
            }
            tr > td:last-child{
                border-right: 1px solid !important;
            }
            tr:last-child > td{
                border-bottom: 1px solid !important;
            }
            .text-right{
                text-align: right;
            }
            .text-bold{
                font-weight: 900 !important;
                font-size:12px !important;
            }
            .text-header{
                font-size: 20px;
            }
            .no-border-top{
                border-top:0;
            }
            .no-border-bottom{
                border-bottom:0 !important;
            }
            .no-border-left{
                border-left:0;
            }
            .no-border-right{
                border-right:0;
            }
            .width-50-pr{
                width:50%;
            }
            td.footer-sign-area{
                height: 82x;
                vertical-align: bottom;
                /*width: 33.33%;*/
                text-align: center;
            }
            .no-border{
                border: 0!important;
            }
            .footer-detail-area{
                color: #000000;
                font-size: 12px;
            }
        </style>
    </head>
    <body>
        <table style="page-break-inside:avoid">
            <tr>
                <td class="text-center text-bold text-header" colspan="11">Received Complain</td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="5" ><b>Manufacturer & Supplier</b></td>
                <td class="text-bold" colspan="3" width="150px">Complain  No. </td>
                <td class="text-bold text-left" colspan="2" width="120px"><b><?= $complain_data->complain_no_year; ?></b></td>
                <td class="text-bold text-left" colspan="1" width="120px"><b><?= $complain_data->complain_date; ?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" rowspan="8" colspan="5" >
                    <span style="font-size:15px;"><b><?= $company_details['name'] ?></b></span><br />
                    <?= nl2br($company_details['address']); ?><br />
                    City : <?= $company_details['city']; ?> - <?= $company_details['pincode']; ?> (<?= $company_details['state']; ?>), Country : <?= $company_details['country']; ?>.<br />
                    Email: <?= $company_details['email_id']; ?><br />
                    Tel No. : <?= $company_details['contact_no']; ?>,<br />
                    Contact No. : <?= $company_details['cell_no']; ?><br />
                    <b>GST No : <?= $company_details['gst_no']; ?></b>
                </td>
                <td class="text-bold" align="left" colspan="3">Installation No.</td>
                <td class="text-bold text-left" colspan="2" ><b><?= $installation_data->installation_no_year; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->installation_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" align="left" colspan="3">Testing Report No.</td>
                <td class="text-bold text-left" colspan="2" ><b><?= $testing_report_data->testing_report_no_year; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $testing_report_data->testing_report_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" align="left" colspan="3">Challan No.</td>
                <td class="text-bold text-left" colspan="2" ><b><?= $challan_data->challan_no; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $challan_data->challan_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" align="left" colspan="3">Invoice No.</td>
                <td class="text-bold text-left" colspan="2" ><b><?= $challan_data->invoice_no; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $challan_data->invoice_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" align="left" colspan="3">Quotation No</td>
                <td class="text-bold text-left" colspan="2"><b><?= $challan_data->quotation_no; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $challan_data->quotation_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" align="left" colspan="3">Purchase Order No. </td>
                <td class="text-bold text-left" colspan="2"><b><?= $challan_data->cust_po_no; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $challan_data->po_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3" align="left">Sales Order No.</td>
                <td class="text-bold text-left" colspan="2"><b><?= $challan_data->sales_order_no; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $challan_data->sales_order_date; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3">Manufacturing Year </td>
                <td class="text-bold text-left" colspan="3"><b><?= $testing_report_data->manufacturing_m_y; ?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="5"><b>Purchaser</b></td>
                <td class="text-bold" colspan="3">Delivery of Equipment</td>
                <td class="text-bold text-left" colspan="3"><b><?= $testing_report_data->delivery_of_equipment; ?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" rowspan="7" colspan="5" >
                    <span style="font-size:15px;"><b><?= $party_data['party_name'] ?></b><br /></span>
                    <?= nl2br($party_data['address']); ?><br />
                    City : <?= $party_data['city'] ?> -  <?= $party_data['pincode'] ?> (<?= $party_data['state'] ?>) <?= $party_data['country'] ?>.<br />
                    Email : <?= $party_data['email_id']; ?><br />
                    Tel No.:  <?= $party_data['fax_no']; ?>,<br />
                    Contact No.:  <?= $party_data['phone_no']; ?><br />
                    Contact Person: <?= $challan_data->contact_person_name ?><br />
                    <?php if ($party_data['party_type_1'] == PARTY_TYPE_DOMESTIC_ID) { ?>
                        <b>GST No. : <?= $party_data['gst_no']; ?></b><?= isset($party_data['party_cin_no']) && !empty($party_data['party_cin_no']) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$party_data['party_cin_no'].'</b>' : ''; ?>
                    <?php } ?>
                </td>
                <td class="text-bold" colspan="3">Installation & Commissioning By</td>
                <td class="text-bold text-left" colspan="2"><b><?= $installation_data->installation_commissioning_by; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->installation_commissioning_by_contact; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3">JK Technical Person </td>
                <td class="text-bold text-left" colspan="2"><b><?= $installation_data->jk_technical_person; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->jk_technical_person_contact; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3">Project Training Provide By </td>
                <td class="text-bold text-left" colspan="2"><b><?= $installation_data->project_training_provide_by; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->project_training_provide_by_contact; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3">Customer Site Incharge Person </td>
                <td class="text-bold text-left" colspan="2"><b><?= $installation_data->customer_site_incharge_person; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->customer_site_incharge_person_contact; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3">Customer Site Technical Ope. Person</td>
                <td class="text-bold text-left" colspan="2"><b><?= $installation_data->customer_technical_operator_person; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->customer_technical_operator_person_contact; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3">Customer Site Quality Checked By</td>
                <td class="text-bold text-left" colspan="2"><b><?= $installation_data->customer_site_quality_checked_by_name; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->customer_site_quality_checked_by_contact; ?></b></td>
            </tr>
            <tr>
                <td class="text-bold" colspan="3">Customer Site Equipment Inspe. By</td>
                <td class="text-bold text-left" colspan="2"><b><?= $installation_data->customer_site_equipment_inspection_by_name; ?></b></td>
                <td class="text-bold text-left" colspan="1"><b><?= $installation_data->customer_site_equipment_inspection_by_contact; ?></b></td>
            </tr>
        </table>
        <table>
            <tr>
                <td class=" text-bold no-border-top" colspan="10" align="center"><b>Model</b></td>
                <td class=" text-bold no-border-top" colspan="10" align="center"><b>Item Name</b></td>
                <td class=" text-bold no-border-top" colspan="10" align="center"><b>Equipment Serial No.</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="10" align="center"><b><?= $challan_item_data->item_code; ?></b></td>
                <td class=" text-bold" colspan="10" align="center"><b><?= $challan_item_data->item_name; ?></b></td>
                <td class=" text-bold" colspan="10" align="center"><b><?= $challan_item_data->item_serial_no; ?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>&nbsp;</b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>Electric Motor & Equipment Testing Report At Customer's Premises</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3" align="center"><b>User</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>HP</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>KW</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Frequency</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>RPM</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>Volts</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>Serial No.</b></td>
                <td class=" text-bold" colspan="4" align="center"><b>Normal Amps</b></td>
                <td class=" text-bold" colspan="4" align="center"><b>Working Amps</b></td>
                <td class=" text-bold" colspan="5" align="center"><b>Total Working Hours</b></td>
            </tr>
            <?php if (!empty($motor_data)) {
                foreach ($motor_data as $motor) { ?>
                    <tr>
                        <td class=" text-bold" colspan="3" align="center"><?= $motor->motor_user; ?></td>
                        <td class=" text-bold" colspan="2" align="center"><?= $motor->motor_hp; ?></td>
                        <td class=" text-bold" colspan="3" align="center"><?= $motor->motor_kw; ?></td>
                        <td class=" text-bold" colspan="3" align="center"><?= $motor->motor_frequency; ?></td>
                        <td class=" text-bold" colspan="2" align="center"><?= $motor->motor_rpm; ?></td>
                        <td class=" text-bold" colspan="2" align="center"><?= $motor->motor_volts_cycles; ?></td>
                        <td class=" text-bold" colspan="2" align="center"><?= $motor->motor_serial_no; ?></td>
                        <td class=" text-bold" colspan="4" align="center"><?= $motor->normal_amps_c; ?></td>
                        <td class=" text-bold" colspan="4" align="center"><?= $motor->working_amps_c; ?></td>
                        <td class=" text-bold" colspan="5" align="center"><?= $motor->total_workinghours_c; ?></td>
                    </tr>
    <?php }
} ?>
            <tr>
                <td class=" text-bold" colspan="3" align="center"><b>User</b></td>
                <td class=" text-bold" colspan="3" width="80px" align="center"><b>Make</b></td>
                <td class=" text-bold" colspan="5" align="center"><b>Gear Type</b></td>
                <td class=" text-bold" colspan="4" width="150px" align="center"><b>Model</b></td>
                <td class=" text-bold" colspan="2" align="center"><b>Ratio</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Serial No.</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Normal Sound</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Working Sound</b></td>
                <td class=" text-bold" colspan="4" width="100px" align="center"><b>Total Working Hours</b></td>
            </tr>
<?php if (!empty($gearbox_data)) {
    foreach ($gearbox_data as $gearbox) { ?>
                    <tr>
                        <td class=" text-bold" colspan="3" align="center"><?= $gearbox->gearbox_user; ?></td>
                        <td class=" text-bold" colspan="3" align="center"><?= $gearbox->gearbox_make; ?></td>
                        <td class=" text-bold" colspan="5" align="center"><?= $gearbox->gearbox_gear_type; ?></td>
                        <td class=" text-bold" colspan="4" align="center"><?= $gearbox->gearbox_model; ?></td>
                        <td class=" text-bold" colspan="2" align="center"><?= $gearbox->gearbox_ratio; ?></td>
                        <td class=" text-bold" colspan="3" align="center"><?= $gearbox->gearbox_serial_no; ?></td>
                        <td class=" text-bold" colspan="3" align="center"><?= $gearbox->normal_amps_c; ?></td>
                        <td class=" text-bold" colspan="3" align="center"><?= $gearbox->working_amps_c; ?></td>
                        <td class=" text-bold" colspan="4" align="center"><?= $gearbox->total_workinghours_c; ?></td>
                    </tr>
    <?php }
} else { ?>
                <tr>
                    <td class="text-bold" colspan="3" align="center">&nbsp;</td>
                    <td class="text-bold" style="white-space:nowrap;" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="4" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="2" align="center"></td>
                    <td class="text-bold" colspan="2" align="center"></td>
                    <td class="text-bold" colspan="2" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="5" align="center"></td>
                </tr>
<?php } ?>

            <tr>
                <td class="text-center text-bold" colspan="30"><b>Equipment Testing Normal & complete production Report at Customer's Premises</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="7" align="center"><b>Testing Capacity</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Capacity Hrs</b></td>
                <td class=" text-bold" colspan="6" align="center"><b>Test of Raw Material</b></td>
                <td class=" text-bold" colspan="6" align="center"><b>Moisture of raw material</b></td>
                <td class=" text-bold" colspan="8" align="center"><b>After Procces of Moisture</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="7" align="center"><b>Testing Normal Load</b></td>
                <td class=" text-bold" colspan="3" align="center"><?= $testing_report_data->c_testing_normal_capacity_hrs; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $testing_report_data->c_testing_normal_test_raw_material; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $testing_report_data->c_testing_normal_moisture_raw_material; ?></td>
                <td class=" text-bold" colspan="8" align="center"><?= $testing_report_data->c_testing_normal_after_procces_moisture; ?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="7" align="center"><b>Testing Full Load</b></td>
                <td class=" text-bold" colspan="3" align="center"><?= $testing_report_data->c_testing_full_capacity_hrs; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $testing_report_data->c_testing_full_test_raw_material; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $testing_report_data->c_testing_full_moisture_raw_material; ?></td>
                <td class=" text-bold" colspan="8" align="center"><?= $testing_report_data->c_testing_full_after_procces_moisture; ?></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>Temperature Parameters At Customer's Premises</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="4" align="center"> <b>Oil Temperature</b> </td>
                <td class=" text-bold" colspan="2" align="center"><?= $testing_report_data->c_temp_oli; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Equipment Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $testing_report_data->c_temp_eqipment; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Head Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $testing_report_data->c_temp_head; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Kiln Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $testing_report_data->c_temp_kiln; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Hot Air Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $testing_report_data->c_temp_hot_air; ?></td>
                <td class=" text-bold" colspan="2" align="center"> <b>Weather</b> </td>
                <td class=" text-bold" colspan="2" align="center"><?= $testing_report_data->c_temp_weather; ?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="30" align="center"><b>Equipment Testing</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="5" align="center"><b>Equipment RPM</b></td>
                <td class=" text-bold" colspan="5" align="center"><?= $testing_report_data->c_et_equipment_rpm; ?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Testing</b></td>
                <td class=" text-bold" colspan="5" align="center"><?= $testing_report_data->c_et_testing; ?></td>
                <td class=" text-bold" colspan="6" align="center"><b>Stop Run Time (Minutes)</b></td>
                <td class=" text-bold" colspan="4" align="center"><?= $testing_report_data->c_et_stop_run_time; ?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="30" align="center"><b>Control Panel Board</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="5" align="center"><b>Testing</b></td>
                <td class=" text-bold" colspan="10" align="center"><?= $testing_report_data->c_cpb_testing; ?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Working Hours</b></td>
                <td class=" text-bold" colspan="10" align="center"><?= $testing_report_data->c_cpb_working_hours; ?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="30" align="center"><b>&nbsp;</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="30" align="center"><b>Installation Item : <?= $installation_data->installation_no_year; ?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>Equipment Testing Normal & complete production Report at Customer's Premises</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="7" align="center"><b>Testing Capacity</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Capacity Hrs</b></td>
                <td class=" text-bold" colspan="6" align="center"><b>Test of Raw Material</b></td>
                <td class=" text-bold" colspan="6" align="center"><b>Moisture of raw material</b></td>
                <td class=" text-bold" colspan="8" align="center"><b>After Procces of Moisture</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="7" align="center"><b>Testing Normal Load</b></td>
                <td class=" text-bold" colspan="3" align="center"><?= $complain_item_data->c_testing_normal_capacity_hrs; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $complain_item_data->c_testing_normal_test_raw_material; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $complain_item_data->c_testing_normal_moisture_raw_material; ?></td>
                <td class=" text-bold" colspan="8" align="center"><?= $complain_item_data->c_testing_normal_after_procces_moisture; ?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="7" align="center"><b>Testing Full Load</b></td>
                <td class=" text-bold" colspan="3" align="center"><?= $complain_item_data->c_testing_full_capacity_hrs; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $complain_item_data->c_testing_full_test_raw_material; ?></td>
                <td class=" text-bold" colspan="6" align="center"><?= $complain_item_data->c_testing_full_moisture_raw_material; ?></td>
                <td class=" text-bold" colspan="8" align="center"><?= $complain_item_data->c_testing_full_after_procces_moisture; ?></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>Temperature Parameters At Customer's Premises</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="4" align="center"> <b>Oil Temperature</b> </td>
                <td class=" text-bold" colspan="2" align="center"><?= $complain_item_data->c_temp_oli; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Equipment Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $complain_item_data->c_temp_eqipment; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Head Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $complain_item_data->c_temp_head; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Kiln Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $complain_item_data->c_temp_kiln; ?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Hot Air Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?= $complain_item_data->c_temp_hot_air; ?></td>
                <td class=" text-bold" colspan="2" align="center"> <b>Weather</b> </td>
                <td class=" text-bold" colspan="2" align="center"><?= $complain_item_data->c_temp_weather; ?></td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="text-bold text-center" colspan="11"><b>Complain Received</b></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="2"><b>Complain By</b></td>
                <td class="text-bold text-center" colspan="2"><?= $complain_data->complain_by; ?></td>
                <td class="text-bold text-center" colspan="2"><b>Received by</b></td>
                <td class="text-bold text-center" colspan="2"><?= $complain_data->received_by; ?></td>
                <td class="text-bold text-center" colspan="3"><?= $complain_data->received_by_details; ?></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="2" width="150px"><b>Complain Call Received By</b></td>
                <td class="text-bold text-center" colspan="2" width="130px"><?= $complain_data->created_by_name; ?></td>
                <td class="text-bold text-center" colspan="2" width="130px"><?= $complain_data->created_by_contact; ?></td>
                <td class="text-bold text-center" colspan="2"><b>Technical Support By</b></td>
                <td class="text-bold text-center" colspan="2"><?= $complain_data->technical_support_by; ?></td>
                <td class="text-bold text-center" colspan="1"><?= $complain_data->technical_support_by_contact; ?></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="2"><b>Service Type</b></td>
                <td class="text-bold text-center" colspan="2"><?= $complain_data->service_type; ?></td>
                <td class="text-bold text-center" colspan="7"><?= $complain_data->service_charge; ?></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="11"><b>Complain Type</b></td>
            </tr>
            <tr>
                <td class="text-bold text-center" colspan="1"><b>Date</b></td>
                <td class="text-bold text-center" colspan="3"><b>Type of Complain</b></td>
                <td class="text-bold text-center" colspan="7"><b>Complain Box</b></td>
            </tr>
<?php if (!empty($complain_type_details)) { ?>
    <?php foreach ($complain_type_details as $complain_type) { ?>
                    <tr>
                        <td class=" text-bold" colspan="1" align="center"><b><?= (!empty(strtotime($complain_type->complain_type_date))) ? date('d/m/Y', strtotime($complain_type->complain_type_date)) : ''; ?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?= $complain_type->type_of_complain; ?></b></td>
                        <td class=" text-bold" colspan="7" align="center"><b><?= $complain_type->complain_box; ?></b></td>
                    </tr>
    <?php } ?>
<?php } else { ?>
                <tr>
                    <td class=" text-bold" colspan="1" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="7" align="center"><b>&nbsp;</b></td>
                </tr>
<?php } ?>

        </table>
    </body>
</html>