<html>
    <head>
        <title>Installation & Commissioning Report</title>
        <style>
            .text-center{
                text-align: center;
            }
            table{
                border-spacing: 0;
                width: 100%;
                border-bottom: 1px solid;
                border-right: 1px solid;
            }
            td{
                padding: 1px 1px 1px 1px;
                border-left: 1px solid;
                border-top: 1px solid;
                font-size:10px;
            }
            tr > td:last-child{
                border-right: 1px solid !important;
            }
            tr:last-child > td{
                border-bottom: 1px solid !important;
            }
            .text-right{
                text-align: right;
            }
            .text-bold{
                font-weight: 900 !important;
                font-size:12px !important;
            }
            .text-header{
                font-size: 20px;
            }
            .no-border-top{
                border-top:0;
            }
            .no-border-bottom{
                border-bottom:0 !important;
            }
            .no-border-left{
                border-left:0;
            }
            .no-border-right{
                border-right:0;
            }
            .width-50-pr{
                width:50%;
            }
            td.footer-sign-area{
                height: 82x;
                vertical-align: bottom;
                /*width: 33.33%;*/
                text-align: center;
            }
            .no-border{
                border: 0!important;
            }
            .footer-detail-area{
                color: #000000;
                font-size: 12px;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td class="text-center text-bold text-header" colspan="11">Installation & Commissioning Report</td>
            </tr>
            <tr>
                <td class=" text-bold text-center" colspan="5" ><b>Manufacturer & Supplier</b></td>
                <td class="text-bold" colspan="2" >Installation No.  </td>
                <td class="text-bold" colspan="2" ><b><?=$installation_data['installation_no_year'];?></b></td>
                <td class="text-bold text-center" colspan="2" ><b><?=$installation_data['installation_date'];?></b></td>                        
            </tr>
            <tr>
                <td class="text-bold" rowspan="7" colspan="5" >
                    <span style="font-size:15px;"><b><?= $company_details['name'] ?></b></span><br />
                    <?= nl2br($company_details['address']); ?><br />
                    City : <?= $company_details['city']; ?> - <?= $company_details['pincode']; ?> (<?= $company_details['state']; ?>), Country : <?= $company_details['country']; ?>.<br />
                    Email: <?= $company_details['email_id']; ?><br />
                    Tel No. : <?= $company_details['contact_no']; ?>,<br />
                    Contact No. : <?= $company_details['cell_no']; ?><br />
                    <b>GST No : <?= $company_details['gst_no']; ?></b>
                </td>
                <td class="text-bold"  align="left" colspan="2">Testing Report No.	</td>
                <td class="text-bold" colspan="2" ><b><?=$installation_data['testing_report_no_year'];?></b></td>
				<td class="text-bold text-center" colspan="2"><b><?=(!empty(strtotime($installation_data['testing_report_date']))) ? date('d/m/Y',strtotime($installation_data['testing_report_date'])):'';?>	</b></td>
            </tr>
            <tr>
                <td class="text-bold"  align="left" colspan="2">Challan No.	</td>
                <td class="text-bold" colspan="2" ><b><?=$installation_data['challan_no'];?></b></td>
				<td class="text-bold text-center" colspan="2"><b><?=(!empty(strtotime($installation_data['challan_date']))) ? date('d/m/Y',strtotime($installation_data['challan_date'])):'';?>	</b></td>
            </tr>
            <tr>
                <td class="text-bold"  align="left" colspan="2">Invoice No.	</td>
                <td class="text-bold" colspan="2" ><b><?=$installation_data['invoice_no'];?></b></td>
				<td class="text-bold text-center" colspan="2"><b><?=(!empty(strtotime($installation_data['invoice_date']))) ? date('d/m/Y',strtotime($installation_data['invoice_date'])):'';?>	</b></td>
            </tr>
            <tr>
				<td class="text-bold"  align="left" colspan="2">Quotation No. </td>
				<td class="text-bold" colspan="4"><b>
					<?=$installation_data['quotation_no'];?></b>
				</td>				
			</tr>
            <tr>
				<td class="text-bold"  align="left" colspan="2">Purchase Order No.  </td>
				<td class="text-bold" colspan="2"><b>
					<?=$installation_data['cust_po_no'];?></b>
				</td>
				<td class="text-bold text-center" colspan="2"><b>
					<?=strtotime($installation_data['po_date']) != 0 ? date('d/m/Y',strtotime($installation_data['po_date'])):'';?>	
				</b></td>
			</tr>
            <?php if(!empty($installation_data['sales_order_no'])){ ?>
			<tr>
				<td class=" text-bold" colspan="2" align="left">Sales Order No.</td>
				<td class=" text-bold" colspan="2"><b>
					<?=$this->applib->get_sales_order_no($installation_data['sales_order_no']);?></b>
				</td>
				<td class="text-bold text-center" colspan="2"><b>
					<?=strtotime($installation_data['sales_order_date']) != 0 ? date('d/m/Y',strtotime($installation_data['sales_order_date'])):'';?>
				</b></td>
			</tr>
			<?php }else{ ?>
			<tr>
				<td class=" text-bold" colspan="2" align="left">Proforma Invoice No.</td>
				<td class=" text-bold" colspan="2"><b>
					<?=$installation_data['proforma_invoice_no'];?></b>
				</td>
				<td class="text-bold" colspan="2"><b>
					<?=strtotime($installation_data['proforma_invoice_date']) != 0 ? date('d/m/Y',strtotime($installation_data['proforma_invoice_date'])):'';?>
				</b></td>
			</tr>
			<?php }?>
            <tr>
                <td class=" text-bold" colspan="2">Manufacturing Year </td>
                <td class=" text-bold" colspan="4"><b><?=$installation_data['manufacturing_m_y'];?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="5"><b>Purchaser</b></td>
                <td class=" text-bold" colspan="2">Delivery of Equipment </td>
                <td class=" text-bold" colspan="4"><b><?=(!empty(strtotime($installation_data['delivery_of_equipment']))) ? date('d/m/Y',strtotime($installation_data['delivery_of_equipment'])):'';?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" rowspan="8" colspan="5" >
					<span style="font-size:15px;"><b><?=$installation_data['party_name']?></b><br /></span>
					<?=nl2br($installation_data['address']);?>
					City : <?=$installation_data['city']?> -  <?=$installation_data['pincode']?> (<?=$installation_data['state']?>) <?=$installation_data['country']?>.<br />
					Email : <?=$installation_data['party_email_id'];?> <?php //<?=explode(",", $installation_data->party_email_id)[0];?><br />
					Tel No.:  <?=$installation_data['fax_no'];?>,<br />
					Contact No.:  <?=$installation_data['p_phone_no'];?>, <?php //$installation_data->contact_person_phone?><br />
					Contact Person: <?=$installation_data['contact_person_name']?><br />
                    <?php if($installation_data['party_type_1'] == PARTY_TYPE_DOMESTIC_ID){ ?>
                        <b>GST No. : <?=$installation_data['party_gst_no'];?></b><?= isset($installation_data['party_cin_no']) && !empty($installation_data['party_cin_no']) ? '&nbsp;&nbsp;&nbsp;&nbsp;<b>CIN No : '.$installation_data['party_cin_no'].'</b>' : ''; ?>
                    <?php } ?>
				</td>
                <td class=" text-bold" colspan="3">Installation & Commissioning By  </td>
                <td class=" text-bold" colspan="3"><b><?=(!empty($installation_data['installation_commissioning_by_name'])) ? $installation_data['installation_commissioning_by_name'] : $installation_data['installation_commissioning_by'] ;?> - <?=$installation_data['installation_commissioning_by_contact'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">JK Technical Person </td>
                <td class=" text-bold" colspan="3"><b><?=(!empty($installation_data['jk_technical_person_name'])) ? $installation_data['jk_technical_person_name'] : $installation_data['jk_technical_person'] ;?> - <?=$installation_data['jk_technical_person_contact'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Project Training Provide By </td>
                <td class=" text-bold" colspan="3"><b><?=(!empty($installation_data['project_training_provide_by_name'])) ? $installation_data['project_training_provide_by_name'] : $installation_data['project_training_provide_by'];?> - <?=$installation_data['project_training_provide_by_contact'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold no-border-bottom" colspan="3">Customer Site Incharge Person </td>
                <td class=" text-bold no-border-bottom" colspan="3"><b><?=$installation_data['customer_site_incharge_person'];?> - <?=$installation_data['customer_site_incharge_person_contact'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold no-border-bottom" colspan="3">Customer Site Technical Ope. Person </td>
                <td class=" text-bold no-border-bottom" colspan="3"><b><?=$installation_data['customer_technical_operator_person'];?> - <?=$installation_data['customer_technical_operator_person_contact'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Customer Site Quality Checked  By</td>
                <td class=" text-bold" colspan="3"><b><?=$installation_data['customer_site_quality_checked_by_name'];?> - <?=$installation_data['customer_site_quality_checked_by_contact'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3">Customer Site Equipment Inspe. By</td>
                <td class=" text-bold" colspan="3"><b><?=$installation_data['customer_site_equipment_inspection_by_name'];?> - <?=$installation_data['customer_site_equipment_inspection_by_contact'];?></b></td>
            </tr>
        </table>
        <table>
            <tr>
                <td class=" text-bold" colspan="10" align="center"><b>Model</b></td>
                <td class=" text-bold" colspan="10" align="center"><b>Item Name</b></td>
                <td class=" text-bold" colspan="10" align="center"><b>Equipment Serial No.</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="10" align="center"><b><?=$challan_item_data->item_code;?></b></td>
                <td class=" text-bold" colspan="10" align="center"><?=$challan_item_data->item_name;?></td>
                <td class=" text-bold" colspan="10" align="center"><?=$challan_item_data->item_serial_no;?></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>Mode Of Shipment</b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="4">Loading At :</td>
                <td class="text-center text-bold" colspan="5"><b><?=$installation_data['loading_at'];?></b></td>
                <td class="text-center text-bold" colspan="4">Port of Loading :</td>
                <td class="text-center text-bold" colspan="3"><b><?=$installation_data['port_of_loading'];?></b></td>
                <td class="text-center text-bold" colspan="4">Port of  Discharge :</td>
                <td class="text-center text-bold" colspan="3"><b><?=$installation_data['port_of_discharge'];?></b></td>
                <td class="text-center text-bold" colspan="4">Place of Delivery :</td>
                <td class="text-center text-bold" colspan="3"><b><?=$installation_data['port_place_of_delivery'];?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="4">Name of shipment :</td>
                <td class="text-center text-bold" colspan="5"><b><?=$installation_data['name_of_shipment'];?></b></td>
                <td class="text-center text-bold" colspan="4">Truck/Container No.:</td>
                <td class="text-center text-bold" colspan="3"><b><?=$challan_item_data->truck_container_no;?></b></td>
                <td class="text-center text-bold" colspan="4">LR/BL No. :</td>
                <td class="text-center text-bold" colspan="3"><b><?=$challan_item_data->lr_bl_no;?></b></td>
                <td class="text-center text-bold" colspan="4">LR/BL Date :</td>
                <td class="text-center text-bold" colspan="3"><b><?=$challan_item_data->lr_date;?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="4">Contact Person :</td>
                <td class="text-center text-bold" colspan="5"><b><?=$challan_item_data->contact_person;?></b></td>
                <td class="text-center text-bold" colspan="4">Contact No.:</td>
                <td class="text-center text-bold" colspan="3"><b><?=$challan_item_data->contact_no;?></b></td>
                <td class="text-center text-bold" colspan="4">Driver Name :</td>
                <td class="text-center text-bold" colspan="3"><b><?=$challan_item_data->driver_name;?></b></td>
                <td class="text-center text-bold" colspan="4">Contact No. :</td>
                <td class="text-center text-bold" colspan="3"><b><?=$challan_item_data->driver_contact_no   ;?></b></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>&nbsp;</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Land Building & Factory Area</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['land_building_factory_area'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Plant Room Size</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['plant_room_size'];?></td>
                <td class=" text-bold" colspan="6" align="center"><b>Prepared Foundation</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['prepared_foundation'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Foundation Marking By</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['foundation_marking_by'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>No of Foundation</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['no_of_foundation'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Unloading Date</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=(!empty(strtotime($installation_data['unloading_date']))) ? date('d/m/Y',strtotime($installation_data['unloading_date'])):'';?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Filling of Foundation Boxes</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=(!empty(strtotime($installation_data['filling_of_foundation_boxes']))) ? date('d/m/Y',strtotime($installation_data['filling_of_foundation_boxes'])):'';?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Installation Start</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=(!empty(strtotime($installation_data['installation_start']))) ? date('d/m/Y',strtotime($installation_data['installation_start'])):'';?></td>
                <td class=" text-bold" colspan="6" align="center"><b>Installation Finished</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=(!empty(strtotime($installation_data['installation_finished']))) ? date('d/m/Y',strtotime($installation_data['installation_finished'])):'';?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Electric Power</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['electric_power'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Elec. Connection</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['electric_connection'];?></td>
                <td class=" text-bold" colspan="6" align="center"><b>Elec. Rm 2 Panel Board Cable</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['elec_rm2_panel_board_cable'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>All  Connection Cable</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['connection_cable_1'];?> - <?=$installation_data['connection_cable_2'];?></td>
            <?php if($installation_data['water_tank_chiller_system_label'] == 1 ) { ?>
                <td class=" text-bold" colspan="5" align="center"><b>Water Tank</b></td>
            <?php } else { ?>
                <td class=" text-bold" colspan="5" align="center"><b>Chiller System</b></td>
            <?php } ?>    
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['water_tank_chiller_system_1'];?> - <?=$installation_data['water_tank_chiller_system_2'];?></td>
            <?php if($installation_data['water_plumbing_chiller_conn_label'] == 1 ) { ?>    
                <td class=" text-bold" colspan="5" align="center"><b>Water Plumbing</b></td>
            <?php } else { ?>
                <td class=" text-bold" colspan="5" align="center"><b>Chiller Conn</b></td>
            <?php } ?>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['water_plumbing_chiller_conn_1'];?> - <?=$installation_data['water_plumbing_chiller_conn_2'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Lubrication Gear Oil</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['lubrication_gear_oil_1'];?> - <?=$installation_data['lubrication_gear_oil_2'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Gear Box Oil</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['gear_box_oil_1'];?> - <?=$installation_data['gear_box_oil_2'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Raw Material</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['raw_material_1'];?> - <?=$installation_data['raw_material_2'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>No load free run Start Date</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=(!empty(strtotime($installation_data['run_start_date']))) ? date('d/m/Y',strtotime($installation_data['run_start_date'])):'';?></td>
                <td class=" text-bold" colspan="7" align="center"><b>No load Total Free Run Hrs</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['run_hrs'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Production Start</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=(!empty(strtotime($installation_data['production_start']))) ? date('d/m/Y',strtotime($installation_data['production_start'])):'';?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Name of Raw Material</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['name_of_raw_material'];?></td>
                <td class=" text-bold" colspan="7" align="center"><b>Moisture of Raw Material</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['moisture_of_raw_material'];?></td>
                <td class=" text-bold" colspan="7" align="center"><b>After Drying Moisture Content</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['after_drying_moisture_content'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Approx Production Per Hr</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['approx_production_per_hr'];?></td>
                <td class=" text-bold" colspan="7" align="center"><b>Product Quality</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['product_quality'];?></td>
                <td class=" text-bold" colspan="7" align="center"><b>Electric Consumption Unit/Hr</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['electric_consumption_unit_hr'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="6" align="center"><b>Electricity Rate Per Unit</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['electricity_rate_per_unit'];?></td>
                <td class=" text-bold" colspan="4" align="center"><b>Unskilled Labour</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['unskilled_labour'];?></td>
                <td class=" text-bold" colspan="4" align="center"><b>Skilled Labour</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=$installation_data['skilled_labour'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Do you have tractor loader?</b></td>
                <td class=" text-bold" colspan="3" align="center"><?=(!empty($installation_data['tractor_loader'])) ? (($installation_data['tractor_loader'] == 1) ? 'Yes' : 'No') :'';?></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>&nbsp;</b></td>
            </tr>
            <tr>
				<td class="text-center text-bold" colspan="30"><b>Electric Motor & Equipment Testing Report At Customer's Premises</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="3" align="center"><b>User</b></td>
				<td class=" text-bold" colspan="2" align="center"><b>HP</b></td>
				<td class=" text-bold" colspan="3" align="center"><b>KW</b></td>
				<td class=" text-bold" colspan="2" align="center"><b>Frequency</b></td>
				<td class=" text-bold" colspan="2" align="center"><b>RPM</b></td>
				<td class=" text-bold" colspan="4" align="center"><b>Volts</b></td>
				<td class=" text-bold" colspan="2" align="center"><b>Serial No.</b></td>
				<td class=" text-bold" colspan="3" align="center"><b>Normal Amps</b></td>
				<td class=" text-bold" colspan="4" align="center"><b>Working Amps</b></td>
				<td class=" text-bold" colspan="5" align="center"><b>Total Working Hours</b></td>
			</tr>
			<?php if(!empty($motor_data)){ foreach($motor_data as $motor){ ?>
			<tr>
				<td class=" text-bold" colspan="3" align="center"><?=$motor->motor_user;?></td>
				<td class=" text-bold" colspan="2" align="center"><?=$motor->motor_hp;?></td>
				<td class=" text-bold" colspan="3" align="center"><?=$motor->motor_kw;?></td>
				<td class=" text-bold" colspan="2" align="center"><?=$motor->motor_frequency;?></td>
				<td class=" text-bold" colspan="2" align="center"><?=$motor->motor_rpm;?></td>
				<td class=" text-bold" colspan="4" align="center"><?=$motor->motor_volts_cycles;?></td>
                <td class=" text-bold" colspan="2" align="center"><?=$motor->motor_serial_no;?></td>
				<td class=" text-bold" colspan="3" align="center"><?=$motor->normal_amps_c;?></td>
				<td class=" text-bold" colspan="4" align="center"><?=$motor->working_amps_c;?></td>
				<td class=" text-bold" colspan="5" align="center"><?=$motor->total_workinghours_c;?></td>
			</tr>
			<?php } } ?>
			<tr>
				<td class=" text-bold" colspan="3" align="center"><b>User</b></td>
				<td class=" text-bold" colspan="3" align="center"><b>Make</b></td>
				<td class=" text-bold" colspan="3" align="center"><b>Gear Type</b></td>
				<td class=" text-bold" colspan="4" align="center"><b>Model</b></td>
				<td class=" text-bold" colspan="3" align="center"><b>Ratio</b></td>
				<td class=" text-bold" colspan="2" align="center"><b>Serial No.</b></td>
				<td class=" text-bold" colspan="3" align="center"><b>Normal Sound</b></td>
				<td class=" text-bold" colspan="4" align="center"><b>Working Sound</b></td>
				<td class=" text-bold" colspan="5" align="center"><b>Total Working Hours</b></td>
			</tr>
			<?php if(!empty($gearbox_data)){ foreach($gearbox_data as $gearbox){ ?>
			<tr>
				<td class=" text-bold" colspan="3" align="center"><?=$gearbox->gearbox_user;?></td>
				<td class=" text-bold" colspan="3" align="center"><?=$gearbox->gearbox_make;?></td>
				<td class=" text-bold" colspan="3" align="center"><?=$gearbox->gearbox_gear_type;?></td>
				<td class=" text-bold" colspan="4" align="center"><?=$gearbox->gearbox_model;?></td>
				<td class=" text-bold" colspan="3" align="center"><?=$gearbox->gearbox_ratio;?></td>
				<td class=" text-bold" colspan="2" align="center"><?=$gearbox->gearbox_serial_no;?></td>
				<td class=" text-bold" colspan="3" align="center"><?=$gearbox->normal_amps_c;?></td>
				<td class=" text-bold" colspan="4" align="center"><?=$gearbox->working_amps_c;?></td>
				<td class=" text-bold" colspan="5" align="center"><?=$gearbox->total_workinghours_c;?></td>
			</tr>
			<?php } } else { ?>
                <tr>
                    <td class="text-bold" colspan="3" align="center">&nbsp;</td>
                    <td class="text-bold" style="white-space:nowrap;" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="4" align="center"></td>
                    <td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="2" align="center"></td>
					<td class="text-bold" colspan="3" align="center"></td>
                    <td class="text-bold" colspan="4" align="center"></td>
                    <td class="text-bold" colspan="5" align="center"></td>
                </tr>
			<?php } ?>
            
            <tr>
				<td class="text-center text-bold" colspan="30"><b>Equipment Testing Normal & complete production Report at Customer's Premises</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="7" align="center"><b>Testing Capacity</b></td>
				<td class=" text-bold" colspan="3" align="center"><b>Capacity Hrs</b></td>
				<td class=" text-bold" colspan="6" align="center"><b>Test of Raw Material</b></td>
				<td class=" text-bold" colspan="6" align="center"><b>Moisture of raw material</b></td>
				<td class=" text-bold" colspan="8" align="center"><b>After Procces of Moisture</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="7" align="center"><b>Testing Normal Load</b></td>
				<td class=" text-bold" colspan="3" align="center"><?=$installation_data['c_testing_normal_capacity_hrs'];?></td>
				<td class=" text-bold" colspan="6" align="center"><?=$installation_data['c_testing_normal_test_raw_material'];?></td>
				<td class=" text-bold" colspan="6" align="center"><?=$installation_data['c_testing_normal_moisture_raw_material'];?></td>
				<td class=" text-bold" colspan="8" align="center"><?=$installation_data['c_testing_normal_after_procces_moisture'];?></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="7" align="center"><b>Testing Full Load</b></td>
				<td class=" text-bold" colspan="3" align="center"><?=$installation_data['c_testing_full_capacity_hrs'];?></td>
				<td class=" text-bold" colspan="6" align="center"><?=$installation_data['c_testing_full_test_raw_material'];?></td>
				<td class=" text-bold" colspan="6" align="center"><?=$installation_data['c_testing_full_moisture_raw_material'];?></td>
				<td class=" text-bold" colspan="8" align="center"><?=$installation_data['c_testing_full_after_procces_moisture'];?></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="30"><b>Temperature Parameters At Customer's Premises</b></td>
			</tr>
			<tr>
                <td class=" text-bold" colspan="4" align="center"> <b>Oil Temperature</b> </td>
                <td class=" text-bold" colspan="2" align="center"><?=$installation_data['c_temp_oli'];?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Equipment Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?=$installation_data['c_temp_eqipment'];?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Head Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?=$installation_data['c_temp_head'];?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Kiln Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?=$installation_data['c_temp_kiln'];?></td>
                <td class=" text-bold" colspan="4" align="center"> <b>Hot Air Temperature</b> </td>
                <td class=" text-bold" colspan="1" align="center"><?=$installation_data['c_temp_hot_air'];?></td>
                <td class=" text-bold" colspan="2" align="center"> <b>Weather</b> </td>
                <td class=" text-bold" colspan="2" align="center"><?=$installation_data['c_temp_weather'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="30" align="center"><b>Equipment Testing</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="5" align="center"><b>Equipment RPM</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['c_et_equipment_rpm'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Testing</b></td>
                <td class=" text-bold" colspan="5" align="center"><?=$installation_data['c_et_testing'];?></td>
                <td class=" text-bold" colspan="6" align="center"><b>Stop Run Time (Minutes)</b></td>
                <td class=" text-bold" colspan="4" align="center"><?=$installation_data['c_et_stop_run_time'];?></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="30" align="center"><b>Control Panel Board</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="5" align="center"><b>Testing</b></td>
                <td class=" text-bold" colspan="10" align="center"><?=$installation_data['c_cpb_testing'];?></td>
                <td class=" text-bold" colspan="5" align="center"><b>Working Hours</b></td>
                <td class=" text-bold" colspan="10" align="center"><?=$installation_data['c_cpb_working_hours'];?></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_incharge_person'];?></td>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_quality_checked_by_name'];?></td>
                <td class="text-center text-bold" colspan="7"><?=(!empty($installation_data['jk_technical_person_name'])) ? $installation_data['jk_technical_person_name'] : $installation_data['jk_technical_person'] ;?></td>
                <td class="text-center text-bold" colspan="9"><?=$installation_data['party_name'];?></td>
            </tr>
            <tr>
                <td class="text-center text-bold footer-sign-area" colspan="7"> &nbsp; </td>
                <td class="text-center text-bold footer-sign-area" colspan="7"> &nbsp; </td>
                <td class="text-center text-bold footer-sign-area" colspan="7"> &nbsp; </td>
                <td class="text-center text-bold footer-sign-area" colspan="9"> &nbsp; </td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_incharge_person_contact'];?></td>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_quality_checked_by_contact'];?></td>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['jk_technical_person_contact'];?></td>
                <td class="text-center text-bold no-border-top no-border-bottom" colspan="9">&nbsp; </td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="7" width="25%"><b>Incharge Person</b></td>
                <td class="text-center text-bold" colspan="7" width="25%"><b>Checked By</b></td>
                <td class="text-center text-bold" colspan="7" width="25%"><b>JK Technical Person</b></td>
                <td class="text-center text-bold" colspan="9" width="25%"><b>Authorized Signature Stamp</b></td>
            </tr>
        </table>
        <table style='page-break-inside: avoid'>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>Daily Working Report</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="3" align="center"><b>Date</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Operating Person</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Site Incharge Person</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Use Raw Material</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Moisture</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Production Hr</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>After Process Moisture</b></td>
                <td class=" text-bold" colspan="3" align="center"><b>Working Hrs</b></td>
                <td class=" text-bold" colspan="6" align="center"><b>Remark</b></td>
            </tr>
            <?php if(!empty($daily_working_report_data)){ ?>
                <?php foreach ($daily_working_report_data as $daily_working_report){ ?>
                    <tr>
                        <td class=" text-bold" colspan="3" align="center"><b><?=(!empty(strtotime($daily_working_report->work_date))) ? date('d/m/Y',strtotime($daily_working_report->work_date)):'';?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?=$daily_working_report->oper_person; ?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?=$daily_working_report->dwr_incharge_person; ?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?=$daily_working_report->use_raw_mater; ?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?=$daily_working_report->moisture; ?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?=$daily_working_report->produ_hour; ?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?=$daily_working_report->after_mois; ?></b></td>
                        <td class=" text-bold" colspan="3" align="center"><b><?=$daily_working_report->working_hour; ?></b></td>
                        <td class=" text-bold" colspan="6" align="center"><b><?=$daily_working_report->remark; ?></b></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="3" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="6" align="center"><b>&nbsp;</b></td>
                </tr>
            <?php } ?>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>&nbsp;</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="8" align="center"><b>Problem / Resolution</b></td>
                <td class=" text-bold" colspan="22" align="center"><b>Detail</b></td>
            </tr>
            <?php if(!empty($problem_resolution_data)){ ?>
                <?php foreach ($problem_resolution_data as $problem_resolution){ ?>
                    <tr>
                        <td class=" text-bold" colspan="8" align="center"><?=$problem_resolution->problem_resolution_option;?></td>
                        <td class=" text-bold" colspan="22" align="center"><?=$problem_resolution->problem_resolution_text; ?></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class=" text-bold" colspan="8" align="center"><b>&nbsp;</b></td>
                    <td class=" text-bold" colspan="22" align="center"><b>&nbsp;</b></td>
                </tr>
            <?php } ?>
            <tr>
                <td class="text-center text-bold" colspan="30"><b>Customer Satisfaction Survey</b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="14" align="left">How many of Equipments from Jay Khodiyar Group do you have? </td>
                <td class=" text-bold" colspan="16" align="left"><b><?=$installation_data['num_of_equipments'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="14" align="left">Which Equipments have you Purchased? </td>
                <td class=" text-bold" colspan="16" align="left"><b><?=$installation_data['item_name'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="27" align="left">Has The Technician of Jay Khodiyar Installed Your Plant Properly? Rate The Installation of The Plant According To Your Requirement.</td>
                <td class=" text-bold" colspan="3" align="center"><b><?=$installation_data['technician_of_jk_installed'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="27" align="left">Had The Technician From Jay Khodiyar Accompanied Your Technical Operator to Train and Inform ?</td>
                <td class=" text-bold" colspan="3" align="center"><b><?=$installation_data['technician_from_jk_accompanied'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="27" align="left">Rate From our Technician From Jay Khodiyar Helped And Guided Your Technical Operator When There Occurs Any Problem With Operating The Machine.</td>
                <td class=" text-bold" colspan="3" align="center"><b><?=$installation_data['technician_from_jk_helped'];?></b></td>
            </tr>
            <tr>
                <td class=" text-bold" colspan="27" align="left">Please Rate From Below Given Option For Performance Of Our Equipment After Complete Installation.</td>
                <td class=" text-bold" colspan="3" align="center"><b><?=$installation_data['performance_of_our_equipment'];?></b></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="30" align="left" style="height: 70px;"><u><b>Describe in General The Overall Work, Technical Support And Behaviour Of Jay Khodiyar Technician At Your Place :</b></u><br/><?=nl2br($installation_data['overall_work_desc']);?><br/></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="30" align="left" style="height: 70px;"><u><b>Describe The Quantity Of Production in Tons/Day wise Under The Supervision And Guidance Of Jay Khodiyar Technician and at present :</b></u><br/><?=nl2br($installation_data['quantity_of_production_desc']);?><br/></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="30" align="left" style="height: 70px;"><u><b>According To You, Where We Should Improve So That We Can Increase Your Satisfaction Level? :</b></u><br/><?=nl2br($installation_data['satisfaction_level_desc']);?><br/></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="30" align="left" style="height: 70px;"><u><b>Any other Query/Suggestion do you have regarding our Equipment or Service? :</b></u><br/><?=nl2br($installation_data['query_suggestion_desc']);?><br/></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="30" align="left" style="height: 70px;"><u><b>Feedback for equipment Quality :</b></u><br/><?= nl2br($installation_data['feedback_for_quality']);?><br/></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="30" align="left" style="height: 70px;"><b><u>Feedback for our Technical Team :</u></b><br/><?=nl2br($installation_data['feedback_for_team']);?><br/></td>
            </tr>
            <tr>
                <td valign="top" class="text-bold" colspan="30" align="left" style="height: 40px;"><b>Declaration :</b> <?=nl2br($installation_data['installation_declaration']);?><br/></td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_incharge_person'];?></td>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_quality_checked_by_name'];?></td>
                <td class="text-center text-bold" colspan="7"><?=(!empty($installation_data['jk_technical_person_name'])) ? $installation_data['jk_technical_person_name'] : $installation_data['jk_technical_person'] ;?></td>
                <td class="text-center text-bold" colspan="9"><?=$installation_data['party_name'];?></td>
            </tr>
            <tr>
                <td class="text-center text-bold footer-sign-area" colspan="7"> &nbsp; </td>
                <td class="text-center text-bold footer-sign-area" colspan="7"> &nbsp; </td>
                <td class="text-center text-bold footer-sign-area" colspan="7"> &nbsp; </td>
                <td class="text-center text-bold footer-sign-area" colspan="9"> &nbsp; </td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_incharge_person_contact'];?></td>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['customer_site_quality_checked_by_contact'];?></td>
                <td class="text-center text-bold" colspan="7"><?=$installation_data['jk_technical_person_contact'];?></td>
                <td class="text-center text-bold no-border-top no-border-bottom" colspan="9">&nbsp; </td>
            </tr>
            <tr>
                <td class="text-center text-bold" colspan="7" width="25%"><b>Incharge Person</b></td>
                <td class="text-center text-bold" colspan="7" width="25%"><b>Checked By</b></td>
                <td class="text-center text-bold" colspan="7" width="25%"><b>JK Technical Person</b></td>
                <td class="text-center text-bold" colspan="9" width="25%"><b>Authorized Signature Stamp</b></td>
            </tr>
        </table>
    </body>
</html>