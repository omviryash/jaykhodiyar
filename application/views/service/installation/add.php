<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<form class="form-horizontal" action="" method="post" id="save_installation" novalidate>
		<?php if (isset($installation_data->installation_id) && !empty($installation_data->installation_id)) { ?>
		<input type="hidden" name="installation_data[installation_id]" id="installation_id" value="<?= $installation_data->installation_id ?>">
		<?php } ?>
		<section class="content-header">
			<h1>
				<small class="text-primary text-bold">Service : Installation</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, F3 = Tab 3, F4 = Tab 4, Ctrl+S = Save</label></small>
				<?php
				$installation_add_role = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "add");
				$installation_edit_role = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "edit");
				$installation_view_role = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "view");
				?>
				<?php if ($installation_add_role || $installation_edit_role): ?>
				<button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
				<?php endif; ?>
				<?php if ($installation_view_role): ?>
				<a href="<?= base_url() ?>installation/installation_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Installation List</a>
				<?php endif; ?>
				<?php if ($installation_add_role): ?>
				<a href="<?= base_url() ?>installation/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Installation</a>
				<?php endif; ?>
                <?php if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view")) { ?>    
                    <span class="pull-right" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="installation_data[send_sms]" id="send_sms" class="send_sms">  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } ?>
			</h1>
		</section>
		<div class="clearfix">
			<?php if ($installation_add_role || $installation_edit_role): ?>
			<div class="col-md-12">
				<!-- Custom Tabs -->
				<div class="nav-tabs-custom" style="margin-bottom: 5px;">
					<ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Installation Details</a></li>
						<li><a href="#tab_2" data-toggle="tab" id="tabs_2">Customer's Premises</a></li>
						<li><a href="#tab_3" data-toggle="tab" id="tabs_3">Jay Khodiyar Factory</a></li>
						<?php if (isset($installation_data->installation_id) && !empty($installation_data->installation_id)) { ?>
						<li><a href="#tab_5" data-toggle="tab" id="tabs_4">Login</a></li>
						<?php } ?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="row">
								<div class="col-md-12">
									<input type="hidden" name="installation_data[testing_report_id]" class="testing_report_id" id="testing_report_id" value="<?= (isset($installation_data->testing_report_id)) ? $installation_data->testing_report_id : ''; ?>">
									<input type="hidden" name="installation_data[challan_id]" class="challan_id" id="challan_id" value="<?= (isset($installation_data->challan_id)) ? $installation_data->challan_id : ''; ?>">
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Installation Detail</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
														<label for="installation_no" style="" class="col-sm-5 input-sm">Installation No.</label>
														<div class="col-sm-7">
															<input type="text" class="installation_no form-control input-sm" id="installation_no" name="installation_data[installation_no]" value="<?= (isset($installation_data->installation_no)) ? $installation_data->installation_no : ''; ?>" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="" class="col-sm-5 input-sm">Installation Date</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm input-datepicker installation_date" id="installation_date" name="installation_data[installation_date]" value="<?= (isset($installation_data->installation_date)) ? date('d-m-Y', strtotime($installation_data->installation_date)) : date('d-m-Y'); ?>">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="testing_report_no" class="col-sm-5 input-sm">Testing Report No.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="testing_report_no" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="testing_report_date" class="col-sm-5 input-sm">Testing Report Date</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="testing_report_date" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="challan_no" class="col-sm-5 input-sm">Challan No.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="challan_no" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="challan_date" class="col-sm-5 input-sm">Challan Date</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="challan_date" readonly>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="quotation_no" class="col-sm-5 input-sm">Quotation No.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="quotation_no" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="purchase_order_no" class="col-sm-5 input-sm">Purchase Order No.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="cust_po_no" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="purchase_order_date" class="col-sm-5 input-sm">Purchase Order Date.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="po_date" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="sales_order_no" class="col-sm-5 input-sm">Sales Order No.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="sales_order_no" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="sales_order_date" class="col-sm-5 input-sm">Sales Order Date.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="sales_order_date" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="proforma_invoice_no" style="" class="col-sm-5 input-sm">Proforma Invoice No.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="proforma_invoice_no" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="proforma_invoice_date" style="" class="col-sm-5 input-sm">Proforma Invoice Date.</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="proforma_invoice_date" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="manufacturing_m_y" class="col-sm-5 input-sm">Manufacturing Month & Year</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm " id="manufacturing_m_y" value="<?= (isset($installation_data->manufacturing_m_y)) ? $installation_data->manufacturing_m_y : ''; ?>" readonly="readonly" >
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="delivery_of_equipment" class="col-sm-5 input-sm">Delivery of Equipment</label>
														<div class="col-sm-7">
															<input type="text" class="form-control input-sm" id="delivery_of_equipment" value="<?= (isset($installation_data->delivery_of_equipment)) ? date('d-m-Y', strtotime($installation_data->delivery_of_equipment)) : ''; ?>" readonly="" >
														</div>
													</div>
												</div>
                                                <div class="col-md-6">
													<div class="form-group">
														<label for="installation_commissioning_by" class="col-sm-5 input-sm">Installation & Commissioning By</label>
														<div class="col-sm-7">
															<select name="installation_data[installation_commissioning_by]" class="installation_commissioning_by form-control input-sm select2" id="installation_commissioning_by" ></select>
														</div>
													</div>
                                                    <div class="form-group">
                                                        <label for="installation_commissioning_by_name" class="col-sm-5 input-sm">&nbsp;</label>
                                                        <div class="col-sm-4" style="padding-right: 0px;">
															<input type="text" name="installation_data[installation_commissioning_by_name]" id="installation_commissioning_by_name" class="form-control input-sm" value="<?= (isset($installation_data->installation_commissioning_by_name)) ? $installation_data->installation_commissioning_by_name : ''; ?>" placeholder="Installation & Commissioning Name" >
														</div>
														<div class="col-sm-3">
															<input type="text" name="installation_data[installation_commissioning_by_contact]" id="installation_commissioning_by_contact" class="form-control input-sm" value="<?= (isset($installation_data->installation_commissioning_by_contact)) ? $installation_data->installation_commissioning_by_contact : ''; ?>" placeholder="Mobile No." >
														</div>
													</div>
												</div>
                                                <div class="col-md-6">
													<div class="form-group">
														<label for="jk_technical_person" class="col-sm-5 input-sm">JK Technical Person</label>
														<div class="col-sm-7">
															<select name="installation_data[jk_technical_person]" id="jk_technical_person" class="form-control input-sm select2" ></select>
														</div>
													</div>
													<div class="form-group">
														<label for="jk_technical_person_name" class="col-sm-5 input-sm">&nbsp;</label>
														<div class="col-sm-4" style="padding-right: 0px;">
                                                            <input type="text" name="installation_data[jk_technical_person_name]" id="jk_technical_person_name" class="form-control input-sm jk_technical_person_name" value="<?= (isset($installation_data->jk_technical_person_name)) ? $installation_data->jk_technical_person_name : ''; ?>" placeholder="JK Technical Person Name" >
														</div>
														<div class="col-sm-3">
                                                            <input type="text" name="installation_data[jk_technical_person_contact]" id="jk_technical_person_contact" class="form-control input-sm jk_technical_person_contact" value="<?= (isset($installation_data->jk_technical_person_contact)) ? $installation_data->jk_technical_person_contact : ''; ?>" placeholder="Mobile No." >
														</div>
													</div>
												</div>
                                                <div class="col-md-6">
													<div class="form-group">
														<label for="project_training_provide_by" class="col-sm-5 input-sm">Project Training Provide By</label>
														<div class="col-sm-7">
                                                            <select name="installation_data[project_training_provide_by]" id="project_training_provide_by" class="project_training_provide_by form-control input-sm select2" ></select>
														</div>
													</div>
													<div class="form-group">
                                                        <label for="project_training_provide_by_name" class="col-sm-5 input-sm">&nbsp;</label>
														<div class="col-sm-4" style="padding-right: 0px;">
															<input type="text" name="installation_data[project_training_provide_by_name]" id="project_training_provide_by_name" class="form-control input-sm" value="<?= (isset($installation_data->project_training_provide_by_name)) ? $installation_data->project_training_provide_by_name : ''; ?>" placeholder="Project Training Provide Name">
														</div>
														<div class="col-sm-3">
															<input type="text" name="installation_data[project_training_provide_by_contact]" id="project_training_provide_by_contact" class="form-control input-sm project_training_provide_by_contact" value="<?= (isset($installation_data->project_training_provide_by_contact)) ? $installation_data->project_training_provide_by_contact : ''; ?>" placeholder="Mobile No." >
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="customer_site_incharge_person" class="col-sm-5 input-sm">Customer Site Incharge Person</label>
														<div class="col-sm-4" style="padding-right: 0px;">
															<input type="text" name="installation_data[customer_site_incharge_person]" id="customer_site_incharge_person" class="form-control input-sm" value="<?= (isset($installation_data->customer_site_incharge_person)) ? $installation_data->customer_site_incharge_person : ''; ?>" placeholder="Name">
														</div>
														<div class="col-sm-3">
															<input type="text" name="installation_data[customer_site_incharge_person_contact]" id="customer_site_incharge_person_contact" class="form-control input-sm" value="<?= (isset($installation_data->customer_site_incharge_person_contact)) ? $installation_data->customer_site_incharge_person_contact : ''; ?>" placeholder="Mobile No.">
														</div>
													</div>
												</div>
                                                <div class="col-md-6">
													<div class="form-group">
														<label for="customer_technical_operator_person" class="col-sm-5 input-sm">Customer Technical Operator Person</label>
														<div class="col-sm-4" style="padding-right: 0px;">
															<input type="text" name="installation_data[customer_technical_operator_person]" class="form-control input-sm" id="customer_technical_operator_person" value="<?= (isset($installation_data->customer_technical_operator_person)) ? $installation_data->customer_technical_operator_person : ''; ?>" placeholder="Name" >
														</div>
														<div class="col-sm-3">
															<input type="text" name="installation_data[customer_technical_operator_person_contact]" class="form-control input-sm" id="customer_technical_operator_person_contact" value="<?= (isset($installation_data->customer_technical_operator_person_contact)) ? $installation_data->customer_technical_operator_person_contact : ''; ?>" placeholder="Mobile No." >
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="customer_site_quality_checked_by_name" class="col-sm-5 input-sm">Customer Site Quality Checked By</label>
														<div class="col-sm-4" style="padding-right: 0px;">
															<input type="text" name="installation_data[customer_site_quality_checked_by_name]" class="form-control input-sm" id="customer_site_quality_checked_by_name" value="<?= (isset($installation_data->customer_site_quality_checked_by_name)) ? $installation_data->customer_site_quality_checked_by_name : ''; ?>" placeholder="Name">
														</div>
														<div class="col-sm-3">
															<input type="text" name="installation_data[customer_site_quality_checked_by_contact]" class="form-control input-sm" id="customer_site_quality_checked_by_contact" value="<?= (isset($installation_data->customer_site_quality_checked_by_contact)) ? $installation_data->customer_site_quality_checked_by_contact : ''; ?>" placeholder="Mobile No." >
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="customer_site_equipment_inspection_by_name" class="col-sm-5 input-sm">Customer Site Equipment Inspection By</label>
														<div class="col-sm-4" style="padding-right: 0px;">
															<input type="text" name="installation_data[customer_site_equipment_inspection_by_name]" id="customer_site_equipment_inspection_by_name" class="form-control input-sm" value="<?= (isset($installation_data->customer_site_equipment_inspection_by_name)) ? $installation_data->customer_site_equipment_inspection_by_name : ''; ?>" placeholder="Name">
														</div>
														<div class="col-sm-3">
															<input type="text" name="installation_data[customer_site_equipment_inspection_by_contact]" id="customer_site_equipment_inspection_by_contact" class="form-control input-sm" value="<?= (isset($installation_data->customer_site_equipment_inspection_by_contact)) ? $installation_data->customer_site_equipment_inspection_by_contact : ''; ?>" placeholder="Mobile No.">
														</div>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Party Detail</legend>
										<div class="row" style="margin-top: 10px;">
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_id" class="col-sm-3 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select name="installation_data[party_id]" id="party_id" class="form-control input-sm" disabled="disabled"></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_code" class="col-sm-3 input-sm  text-danger">Party Code</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm party_code" id="party_code" name="party[party_code]" readonly>
													</div>
												</div>
												<div class="clearfix"></div>
												<input type="hidden" name="party[party_name]" id="party_name" class="party_name" placeholder="">
												<input type="hidden" name="party[party_id]" id="p_party_id" class="p_party_id" placeholder="">
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="reference_id" class="col-sm-3 input-sm">Reference<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9 dispaly-flex">
														<select name="party[reference_id]" id="reference_id" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="form-group">
													<label for="reference_description" class="col-sm-3 input-sm">Reference Detail</label>
													<div class="col-sm-9">
														<textarea class="form-control reference_description" id="reference_description" rows="1" name="party[reference_description]" disabled=""></textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-3 input-sm">Address</label>
													<div class="col-sm-9">
														<textarea class="form-control address" rows="2" name="party[address]" disabled=""></textarea>
													</div>
												</div>

												<div class="clearfix"></div>
												<div class="form-group">
													<label for="city" class="col-sm-3 input-sm">City</label>
													<div class="col-sm-9">
														<div class="col-md-6" style="padding:0px !important;">
															<select name="party[city_id]" id="city" class="form-control input-sm select2 city" disabled=""></select>
														</div>
														<div class="col-md-6" style="padding-right:0px !important;">
															<input type="text" class="form-control input-sm pincode" id="" name="party[pincode]" placeholder="Pin Code" disabled="">
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="state" class="col-sm-3 input-sm">State</label>
													<div class="col-sm-9">
														<select name="party[state_id]" id="state" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="country" class="col-sm-3 input-sm">Country</label>
													<div class="col-sm-9">
														<select name="party[country_id]" id="country" class="form-control input-sm select2" disabled=""></select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_email_id" class="col-sm-3 input-sm">Email Id</label>
													<div class="col-sm-9">
														<textarea class="form-control email_id" id="party_email_id" name="party[email_id]" disabled=""></textarea>
														<small>Add multiple email id by Comma separated.</small>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="party_fax_no" class="col-sm-3 input-sm">Tel No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm fax_no" id="party_fax_no" name="party[fax_no]" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_phone_no" class="col-sm-3 input-sm">Contact No.<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-9">
														<textarea class="form-control phone_no" id="party_phone_no" name="party[phone_no]" rows="3" disabled=""></textarea>
														<small>Add multiple phone number by Comma separated.</small>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="party_website" class="col-sm-3 input-sm">Website</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm website" id="party_website" name="party[website]" disabled="">
													</div>
												</div>  
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="kind_attn_id" class="col-sm-3 input-sm">Contact Person</label>
													<div class="col-sm-9 dispaly-flex">
														<select name="installation_data[kind_attn_id]" id="contact_person_id" class="form-control input-sm select2">
															<option value="">--Select--</option>
															<?php if (isset($installation_data->installation_id) && !empty($installation_data->installation_id)) { ?>
															<?php foreach ($party_contact_person as $pcp_key => $pcp_value): ?>
															<option value="<?= $pcp_value->contact_person_id; ?>"  <?= $installation_data->kind_attn_id == $pcp_value->contact_person_id ? 'selected="selected"' : ''; ?>><?= $pcp_value->name; ?></option>
															<?php endforeach; ?>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Designation</label>
													<div class="col-sm-9">
														<div id="contact_person_designation"><?php echo isset($contact_person) ? $contact_person->designation : ''; ?></div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-3 input-sm">Department</label>
													<div class="col-sm-9">
														<div id="contact_person_department"><?php echo isset($contact_person) ? $contact_person->department : ''; ?></div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="contact_person_mobile_no" class="col-sm-3 input-sm">Mobile No.</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm" id="contact_person_mobile_no" name="contact_person[contact_person_mobile_no]" value="<?php echo isset($contact_person) ? $contact_person->mobile_no : ''; ?>" disabled="">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="contact_person_email_id" class="col-sm-3 input-sm">Email</label>
													<div class="col-sm-9">
														<input type="text" class="form-control input-sm" id="contact_person_email_id" name="contact_person[contact_person_email_id]" value="<?php echo isset($contact_person) ? $contact_person->email : ''; ?>" disabled="">
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="tab_2">
							<div class="row">
								<div class="col-md-12">
									<fieldset class="scheduler-border installation-item-section">
										<legend class="scheduler-border text-primary text-bold"> Item Details</legend>
										<div class="col-md-6">
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Equipment Name</label>
												<div class="col-sm-9">
													<input type="hidden" id="item_id" class="input-sm form-control" value="<?= (isset($installation_data->item_id)) ? $installation_data->item_id : ''; ?>" readonly="readonly">
													<input type="text" class="input-sm form-control item_name" value="<?= (isset($installation_data->item_name)) ? $installation_data->item_name : ''; ?>" readonly="readonly">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Item Code</label>
												<div class="col-sm-9">
													<input type="text" class="input-sm form-control" id="item_code" value="<?= (isset($installation_data->item_code)) ? $installation_data->item_code : ''; ?>" readonly="readonly">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Item Description</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm" id="item_description" value="<?= (isset($installation_data->item_description)) ? $installation_data->item_description : ''; ?>" readonly="readonly">
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Quantity<span class="required-sign">&nbsp;*</span></label>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm" id="quantity" value="1" readonly="readonly">
												</div>
												<label for="item_extra_accessories_id" class="col-sm-3 input-sm text-right">Item Extra Accessories</label>
												<div class="col-sm-4">
													<input type="text" class="form-control input-sm" id="item_extra_accessories_id" value="" readonly="readonly">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="item_serial_no" class="col-sm-3 input-sm"> Item Serial No. </label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm" id="item_serial_no" value="<?= (isset($installation_data->item_serial_no)) ? $installation_data->item_serial_no : ''; ?>" readonly="readonly">
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">										   
										<legend class="scheduler-border text-primary text-bold">Mode Of Shipment</legend>
										<div class="col-md-4">
											<div class="form-group">
												<label for="loading_at" class="col-sm-6 input-sm">Loading At</label>
												<div class="col-sm-6"> 
													<input type="text" id="loading_at" class="form-control input-sm" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="place_of_delivery" class="col-sm-6 input-sm">Place of Delivery</label>
												<div class="col-sm-6">
													<input type="text" id="place_of_delivery" class="form-control input-sm" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="lr_no" class="col-sm-6 input-sm">LR / BL No</label>
												<div class="col-sm-6">
													<input type="text" id="lr_no" class="form-control input-sm" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="contact_no" class="col-sm-6 input-sm">Contact Person No</label>
												<div class="col-sm-6">
													<input type="text"  id="contact_no" class="form-control input-sm" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div>        
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="port_loading" class="col-sm-6 input-sm">Port of Loading</label>
												<div class="col-sm-6">
													<input type="text" id="port_of_loading" class="form-control input-sm" value="" readonly="readonly">
												</div>
											</div>
											<div class="clearfix"></div> 
											<div class="form-group">
												<label for="ship_name" class="col-sm-6 input-sm">Name of shipment</label>
												<div class="col-sm-6">
													<input type="text" id="name_of_shipment" class="form-control input-sm" value="" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div> 
											<div class="form-group">
												<label for="lr_date" class="col-sm-6 input-sm">LR Date</label>
												<div class="col-sm-6">
													<input type="text" id="lr_date" class="form-control input-sm" value="" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div> 
											<div class="form-group">
												<label for="driver_name" class="col-sm-6 input-sm">Driver Name</label>
												<div class="col-sm-6">
													<input type="text" id="driver_name" class="form-control input-sm" value="" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div>     
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="port_discharge" class="col-sm-6 input-sm">Port of  Discharge</label>
												<div class="col-sm-6">
													<input type="text" id="port_of_discharge" class="form-control input-sm" value="" readonly="readonly">
												</div>
											</div>
											<div class="clearfix"></div> 
											<div class="form-group">
												<label for="truck_container_no" class="col-sm-6 input-sm">Truck / Container No</label>
												<div class="col-sm-6">
													<input type="text" id="truck_container_no" class="form-control input-sm" value="" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div> 
											<div class="form-group">
												<label for="contact_person" class="col-sm-6 input-sm">Contact Person</label>
												<div class="col-sm-6">
													<input type="text" id="contact_person" class="form-control input-sm" value="" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div>  
											<div class="form-group">
												<label for="driver_contact_no" class="col-sm-6 input-sm">Driver Contact No</label>
												<div class="col-sm-6">
													<input type="text" id="driver_contact_no" class="form-control input-sm" value="" readonly="readonly"> 
												</div>
											</div>
											<div class="clearfix"></div>    
										</div>
									</fieldset>
									<fieldset class="scheduler-border">										   
										<legend class="scheduler-border text-primary text-bold">Customer Premises</legend>
										<div class="col-md-4">
											<div class="form-group">
												<label for="land_building_factory_area" class="col-sm-6 input-sm" style="padding: 2px 9px !important;">Land Building & Factory Area</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[land_building_factory_area]" id="land_building_factory_area" class="form-control input-sm" value="<?= (isset($installation_data->land_building_factory_area)) ? $installation_data->land_building_factory_area : ''; ?>" > 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="plant_room_size" class="col-sm-6 input-sm">Plant Room Size</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[plant_room_size]" id="plant_room_size" class="form-control input-sm" value="<?= (isset($installation_data->plant_room_size)) ? $installation_data->plant_room_size : ''; ?>" > 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="prepared_foundation" class="col-sm-6 input-sm">Prepared Foundation</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[prepared_foundation]" id="prepared_foundation" class="form-control input-sm" value="<?= (isset($installation_data->prepared_foundation)) ? $installation_data->prepared_foundation : ''; ?>" > 
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="" class="col-sm-6 input-sm">Foundation Marking By</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[foundation_marking_by]" id="foundation_marking_by" class="form-control input-sm" value="<?= (isset($installation_data->foundation_marking_by)) ? $installation_data->foundation_marking_by : ''; ?>" > 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="no_of_foundation" class="col-sm-6 input-sm">No of Foundation</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[no_of_foundation]" id="no_of_foundation" class="form-control input-sm" value="<?= (isset($installation_data->no_of_foundation)) ? $installation_data->no_of_foundation : ''; ?>" > 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="unloading_date" class="col-sm-6 input-sm">Unloading Date</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[unloading_date]" id="unloading_date" class="form-control input-sm input-datepicker" value="<?= (isset($installation_data->installation_id)) ? (!empty(strtotime($installation_data->unloading_date)) ? date('d-m-Y', strtotime($installation_data->unloading_date)) : '') : date('d-m-Y'); ?>"> 
												</div>
											</div>
										</div>    
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="filling_of_foundation_boxes" class="col-sm-6 input-sm">Filling of Foundation Boxes</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[filling_of_foundation_boxes]" id="filling_of_foundation_boxes" class="form-control input-sm input-datepicker" value="<?= (isset($installation_data->installation_id)) ? (!empty(strtotime($installation_data->filling_of_foundation_boxes)) ? date('d-m-Y', strtotime($installation_data->filling_of_foundation_boxes)) : '') : date('d-m-Y'); ?>"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="installation_start" class="col-sm-6 input-sm">Installation Start</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[installation_start]" id="installation_start" class="form-control input-sm input-datepicker" value="<?= (isset($installation_data->installation_id)) ? (!empty(strtotime($installation_data->installation_start)) ? date('d-m-Y', strtotime($installation_data->installation_start)) : '') : date('d-m-Y'); ?>"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="installation_finished" class="col-sm-6 input-sm">Installation Finished</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[installation_finished]" id="installation_finished" class="form-control input-sm input-datepicker" value="<?= (isset($installation_data->installation_id)) ? (!empty(strtotime($installation_data->installation_finished)) ? date('d-m-Y', strtotime($installation_data->installation_finished)) : '') : date('d-m-Y'); ?>"> 
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">    
											<div class="form-group">
												<label for="electric_power" class="col-sm-6 input-sm">Electric Power</label>
												<div class="col-sm-6">
													<select  name="installation_data[electric_power]" id="electric_power" class="form-control input-sm" value=""></select>    
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="electric_connection" class="col-sm-6 input-sm">Electric Connection</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[electric_connection]" id="electric_connection" class="form-control input-sm" value="<?= (isset($installation_data->electric_connection)) ? $installation_data->electric_connection : ''; ?>" > 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="elec_rm2_panel_board_cable" class="col-sm-6 input-sm">Elec Rm 2 Panel Board Cable</label>
												<div class="col-sm-6">
													<select  name="installation_data[elec_rm2_panel_board_cable]" id="elec_rm2_panel_board_cable" class="form-control input-sm available_or_not" ></select>    
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="connection_cable_1" class="col-sm-5 input-sm" style="">All Connection Cable</label>
												<div class="col-sm-4" style="padding-right: 2px;">
													<select  name="installation_data[connection_cable_1]" id="connection_cable_1" class="form-control input-sm available_or_not" style="padding: 2px 0px !important;" ></select>
												</div>
												<div class="col-sm-3" style="padding-left: 2px;">
													<input type="text" name="installation_data[connection_cable_2]" id="connection_cable_1" class="form-control input-sm" style="padding: 2px 2px !important;" value="<?= (isset($installation_data->connection_cable_2)) ? $installation_data->connection_cable_2 : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<div class="col-sm-5" style="padding-left: 8px;">
													<select name="installation_data[water_tank_chiller_system_label]" id="water_tank_chiller_system_label" class="form-control input-sm">
														<option value="1" <?= (isset($installation_data->water_tank_chiller_system_label) && $installation_data->water_tank_chiller_system_label == '1') ? ' Selected ' : ''; ?> >Water Tank</option>
														<option value="2" <?= (isset($installation_data->water_tank_chiller_system_label) && $installation_data->water_tank_chiller_system_label == '2') ? ' Selected ' : ''; ?>>Chiller System</option>
													</select>    
												</div>
												<div class="col-sm-4" style="padding-right: 2px;">
													<select name="installation_data[water_tank_chiller_system_1]" id="water_tank_chiller_system_1" class="form-control input-sm" style="padding: 2px 0px !important;"></select>
												</div>
												<div class="col-sm-3" style="padding-left: 2px;">
													<input type="text" name="installation_data[water_tank_chiller_system_2]" id="water_tank_chiller_system_2" class="form-control input-sm" style="padding: 2px 2px !important;" value="<?= (isset($installation_data->water_tank_chiller_system_2)) ? $installation_data->water_tank_chiller_system_2 : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<div class="col-sm-5" style="padding-left: 8px;">
													<select name="installation_data[water_plumbing_chiller_conn_label]" id="water_plumbing_chiller_conn_label" class="form-control input-sm">
														<option value="1" <?= (isset($installation_data->water_plumbing_chiller_conn_label) && $installation_data->water_plumbing_chiller_conn_label == '1') ? ' Selected ' : ''; ?>>Water Plumbing</option>
														<option value="2" <?= (isset($installation_data->water_plumbing_chiller_conn_label) && $installation_data->water_plumbing_chiller_conn_label == '2') ? ' Selected ' : ''; ?>>Chiller Conn</option>
													</select>
												</div>
												<div class="col-sm-4" style="padding-right: 2px;">
													<select  name="installation_data[water_plumbing_chiller_conn_1]" id="water_plumbing_chiller_conn_1" class="form-control input-sm" style="padding: 2px 0px !important;"></select>
												</div>
												<div class="col-sm-3" style="padding-left: 2px;">
													<input type="text" name="installation_data[water_plumbing_chiller_conn_2]" id="water_plumbing_chiller_conn_2" class="form-control input-sm" style="padding: 2px 2px !important;" value="<?= (isset($installation_data->water_plumbing_chiller_conn_2)) ? $installation_data->water_plumbing_chiller_conn_2 : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="lubrication_gear_oil_1" class="col-sm-5 input-sm">Lubrication Gear Oil</label>
												<div class="col-sm-4" style="padding-right: 2px;">
													<select  name="installation_data[lubrication_gear_oil_1]" id="lubrication_gear_oil_1" class="form-control input-sm" style="padding: 2px 0px !important;"></select>    
												</div>
												<div class="col-sm-3" style="padding-left: 2px;">
													<input type="text" name="installation_data[lubrication_gear_oil_2]" id="lubrication_gear_oil_2" class="form-control input-sm" style="padding: 2px 2px !important;" value="<?= (isset($installation_data->lubrication_gear_oil_2)) ? $installation_data->lubrication_gear_oil_2 : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="gear_box_oil_1" class="col-sm-5 input-sm">Gear Box Oil</label>
												<div class="col-sm-4" style="padding-right: 2px;">
													<select  name="installation_data[gear_box_oil_1]" id="gear_box_oil_1" class="form-control input-sm available_or_not" style="padding: 2px 0px !important;"></select>    
												</div>
												<div class="col-sm-3" style="padding-left: 2px;">
													<input type="text" name="installation_data[gear_box_oil_2]" id="gear_box_oil_2" class="form-control input-sm" style="padding: 2px 2px !important;" value="<?= (isset($installation_data->gear_box_oil_2)) ? $installation_data->gear_box_oil_2 : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="raw_material_1" class="col-sm-5 input-sm">Raw Material</label>
												<div class="col-sm-4" style="padding-right: 2px;">
													<select  name="installation_data[raw_material_1]" id="raw_material_1" class="form-control input-sm available_or_not" style="padding: 2px 0px !important;"></select>    
												</div>
												<div class="col-sm-3" style="padding-left: 2px;">
													<input type="text" name="installation_data[raw_material_2]" id="raw_material_2" class="form-control input-sm" style="padding: 2px 2px !important;" value="<?= (isset($installation_data->raw_material_2)) ? $installation_data->raw_material_2 : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="run_start_date" class="col-sm-6 input-sm">No Load Free Run Start Date</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[run_start_date]" id="run_start_date" class="form-control input-sm input-datepicker" value="<?= (isset($installation_data->installation_id)) ? (!empty(strtotime($installation_data->run_start_date)) ? date('d-m-Y', strtotime($installation_data->run_start_date)) : '') : date('d-m-Y'); ?>">  
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="run_hrs" class="col-sm-6 input-sm">No Load Total Free Run Hrs</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[run_hrs]" id="run_hrs" class="form-control input-sm" value="<?= (isset($installation_data->run_hrs)) ? $installation_data->run_hrs : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="production_start" class="col-sm-6 input-sm">Production Start</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[production_start]" id="production_start" class="form-control input-sm input-datepicker" value="<?= (isset($installation_data->installation_id)) ? (!empty(strtotime($installation_data->production_start)) ? date('d-m-Y', strtotime($installation_data->production_start)) : '') : date('d-m-Y'); ?>">  
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="name_of_raw_material" class="col-sm-6 input-sm">Name of Raw Material</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[name_of_raw_material]" id="name_of_raw_material" class="form-control input-sm" value="<?= (isset($installation_data->name_of_raw_material)) ? $installation_data->name_of_raw_material : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="moisture_of_raw_material" class="col-sm-6 input-sm">Moisture of Raw Material</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[moisture_of_raw_material]" id="moisture_of_raw_material" class="form-control input-sm" value="<?= (isset($installation_data->moisture_of_raw_material)) ? $installation_data->moisture_of_raw_material : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="after_drying_moisture_content" class="col-sm-6 input-sm" style="padding: 2px 7px !important;">After Drying Moisture Content</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[after_drying_moisture_content]" id="after_drying_moisture_content" class="form-control input-sm" value="<?= (isset($installation_data->after_drying_moisture_content)) ? $installation_data->after_drying_moisture_content : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="approx_production_per_hr" class="col-sm-6 input-sm">Approx Production Per Hr</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[approx_production_per_hr]" id="approx_production_per_hr" class="form-control input-sm" value="<?= (isset($installation_data->approx_production_per_hr)) ? $installation_data->approx_production_per_hr : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="product_quality" class="col-sm-6 input-sm">Product Quality</label>
												<div class="col-sm-6">
													<select  name="installation_data[product_quality]" id="product_quality" class="form-control input-sm customer_feedback"></select>    
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="electric_consumption_unit_hr" class="col-sm-6 input-sm" style="padding: 2px 7px !important;">Electric Consumption Unit/Hr</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[electric_consumption_unit_hr]" id="electric_consumption_unit_hr" class="form-control input-sm" value="<?= (isset($installation_data->electric_consumption_unit_hr)) ? $installation_data->electric_consumption_unit_hr : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="electricity_rate_per_unit" class="col-sm-6 input-sm">Electricity Rate Per Unit</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[electricity_rate_per_unit]" id="electricity_rate_per_unit" class="form-control input-sm" value="<?= (isset($installation_data->electricity_rate_per_unit)) ? $installation_data->electricity_rate_per_unit : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="unskilled_labour" class="col-sm-6 input-sm">Unskilled Labour</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[unskilled_labour]" id="unskilled_labour" class="form-control input-sm " value="<?= (isset($installation_data->unskilled_labour)) ? $installation_data->unskilled_labour : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="skilled_labour" class="col-sm-6 input-sm">Skilled Labour</label>
												<div class="col-sm-6">
													<input type="text" name="installation_data[skilled_labour]" id="skilled_labour" class="form-control input-sm " value="<?= (isset($installation_data->skilled_labour)) ? $installation_data->skilled_labour : ''; ?>" >
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tractor_loader" class="col-sm-6 input-sm">Do you have tractor loader?</label>
                                                <div class="col-sm-6">
                                                    <select name="installation_data[tractor_loader]" id="tractor_loader" class="form-control input-sm select2" >
                                                        <option value=""> - Select - </option>
                                                        <option value="1" <?= (isset($installation_data->tractor_loader) && $installation_data->tractor_loader == '1') ? ' Selected ' : ''; ?>> Yes </option>
                                                        <option value="2" <?= (isset($installation_data->tractor_loader) && $installation_data->tractor_loader == '2') ? ' Selected ' : ''; ?>> No </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
									</fieldset>
									<fieldset class="scheduler-border">										   
										<legend class="scheduler-border text-primary text-bold">Electric Motor & Equipment Testing Report At Customer's Premises</legend>
										<div class="col-md-12">
											<div class="table-responsive">          
												<table class="table table-striped table-bordered table-hover table-condensed">
													<thead>
														<tr>
															<th><strong>User</strong></th>
															<th><strong>Make</strong></th>
															<th><strong>HP</strong></th>
															<th><strong>KW</strong></th>
															<th><strong>Frequency</strong></th>
															<th><strong>RPM</strong></th>
															<th><strong>Volts</strong></th>
															<th><strong>Serial No.</strong></th>
															<th><strong>Normal Amps</strong></th>
															<th><strong>Working Amps</strong></th>
															<th><strong>Total WorkingHours</strong></th>
														</tr>
													</thead>
													<tbody id="motor_testing_details_c"></tbody>
												</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="table-responsive">          
												<table class="table table-striped table-bordered table-hover table-condensed">
													<thead>
														<tr>
															<th><strong>User</strong></th>
															<th><strong>Make</strong></th>
															<th><strong>Gear Type</strong></th>
															<th><strong>Model</strong></th>
															<th><strong>Ratio</strong></th>
                                                            <th><strong>Serial No.</strong></th>
															<th><strong>Normal Sound</strong></th>
															<th><strong>Working Sound</strong></th>
															<th><strong>Total WorkingHours</strong></th>
														</tr>
													</thead>
													<tbody id="gearbox_testing_details_c"></tbody>
												</table>
											</div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Equipment Testing Normal & complete production Report at Customer's Premises</legend>
										<div class="col-md-12">
											<div class="form-group">
												<label for="" class="col-sm-2 input-sm"><strong>Testing Capacity</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>Capacity Hrs</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>Test of Raw Material</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>Moisture of Raw Material</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>After Procces of Moisture</strong></label>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="item_serial_no" class="col-sm-2 input-sm">Testing Normal Load</label>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_normal_capacity_hrs]" class="form-control input-sm item_data" id="c_testing_normal_capacity_hrs" value="<?= (isset($testing_report_data->c_testing_normal_capacity_hrs)) ? $testing_report_data->c_testing_normal_capacity_hrs : ''; ?>" placeholder="Kgs">
												</div>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_normal_test_raw_material]" class="form-control input-sm item_data" id="c_testing_normal_test_raw_material" value="<?= (isset($testing_report_data->c_testing_normal_test_raw_material)) ? $testing_report_data->c_testing_normal_test_raw_material : ''; ?>" placeholder="Name of Raw Material">
												</div>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_normal_moisture_raw_material]" class="form-control input-sm item_data" id="c_testing_normal_moisture_raw_material" value="<?= (isset($testing_report_data->c_testing_normal_moisture_raw_material)) ? $testing_report_data->c_testing_normal_moisture_raw_material : ''; ?>" placeholder="Moisture %">
												</div>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_normal_after_procces_moisture]" class="form-control input-sm item_data" id="c_testing_normal_after_procces_moisture" value="<?= (isset($testing_report_data->c_testing_normal_after_procces_moisture)) ? $testing_report_data->c_testing_normal_after_procces_moisture : ''; ?>" placeholder="Moisture %">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="item_serial_no" class="col-sm-2 input-sm">Testing Full Load</label>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_full_capacity_hrs]" class="form-control input-sm item_data" id="c_testing_full_capacity_hrs" value="<?= (isset($testing_report_data->c_testing_full_capacity_hrs)) ? $testing_report_data->c_testing_full_capacity_hrs : ''; ?>" placeholder="Kgs">
												</div>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_full_test_raw_material]" class="form-control input-sm item_data" id="c_testing_full_test_raw_material" value="<?= (isset($testing_report_data->c_testing_full_test_raw_material)) ? $testing_report_data->c_testing_full_test_raw_material : ''; ?>" placeholder="Name of Raw Material">
												</div>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_full_moisture_raw_material]" class="form-control input-sm item_data" id="c_testing_full_moisture_raw_material" value="<?= (isset($testing_report_data->c_testing_full_moisture_raw_material)) ? $testing_report_data->c_testing_full_moisture_raw_material : ''; ?>" placeholder="Moisture %">
												</div>
												<div class="col-sm-2">
													<input type="text" name="testing_report_data[c_testing_full_after_procces_moisture]" class="form-control input-sm item_data" id="c_testing_full_after_procces_moisture" value="<?= (isset($testing_report_data->c_testing_full_after_procces_moisture)) ? $testing_report_data->c_testing_full_after_procces_moisture : ''; ?>" placeholder="Moisture %">
												</div>
											</div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">										   
										<legend class="scheduler-border text-primary text-bold"> Temperature Parameters At Customer's Premises</legend>
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="form-group">
													<label for="c_temp_" class="col-sm-6 input-sm">Oil Temperature</label>
													<div class="col-sm-6">
														<input type="text" name="testing_report_data[c_temp_oli]" class="form-control input-sm item_data" id="c_temp_oli" value="<?= (isset($testing_report_data->c_temp_oli)) ? $testing_report_data->c_temp_oli : ''; ?>">
													</div>
												</div>
												<div class="form-group">
													<label for="c_temp_eqipment" class="col-sm-6 input-sm">Equipment Temperature</label>
													<div class="col-sm-6">
														<input type="text" name="testing_report_data[c_temp_eqipment]" class="form-control input-sm item_data" id="c_temp_eqipment" value="<?= (isset($testing_report_data->c_temp_eqipment)) ? $testing_report_data->c_temp_eqipment : ''; ?>">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="c_temp_" class="col-sm-6 input-sm">Head Temperature</label>
													<div class="col-sm-6">
														<input type="text" name="testing_report_data[c_temp_head]" class="form-control input-sm item_data" id="c_temp_head" value="<?= (isset($testing_report_data->c_temp_head)) ? $testing_report_data->c_temp_head : ''; ?>">
													</div>
												</div>
												<div class="form-group">
													<label for="c_temp_" class="col-sm-6 input-sm">Kiln Temperature</label>
													<div class="col-sm-6">
														<input type="text" name="testing_report_data[c_temp_kiln]" class="form-control input-sm item_data" id="c_temp_kiln" value="<?= (isset($testing_report_data->c_temp_kiln)) ? $testing_report_data->c_temp_kiln : ''; ?>">
													</div>
												</div>
											</div>    
											<div class="col-md-4">    
												<div class="form-group">
													<label for="c_temp_" class="col-sm-6 input-sm">Hot Air Temperature</label>
													<div class="col-sm-6">
														<input type="text" name="testing_report_data[c_temp_hot_air]" class="form-control input-sm item_data" id="c_temp_hot_air" value="<?= (isset($testing_report_data->c_temp_hot_air)) ? $testing_report_data->c_temp_hot_air : ''; ?>">
													</div>
												</div>
												<div class="form-group">
													<label for="c_temp_" class="col-sm-6 input-sm">weather</label>
													<div class="col-sm-6">
														<input type="text" name="testing_report_data[c_temp_weather]" class="form-control input-sm item_data" id="c_temp_weather" value="<?= (isset($testing_report_data->c_temp_weather)) ? $testing_report_data->c_temp_weather : ''; ?>">
													</div>
												</div>
											</div>											
										</div>
									</fieldset>
                                    <fieldset class="scheduler-border">										   
                                        <legend class="scheduler-border text-primary text-bold">Equipment Testing</legend>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="c_et_equipment_rpm" class="col-sm-6 input-sm">Equipment RPM</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="testing_report_data[c_et_equipment_rpm]" id="c_et_equipment_rpm"  class="form-control input-sm" value="<?= (isset($testing_report_data->c_et_equipment_rpm)) ? $testing_report_data->c_et_equipment_rpm : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="c_et_testing" class="col-sm-6 input-sm">Testing</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="testing_report_data[c_et_testing]" id="c_et_testing" class="form-control input-sm" value="<?= (isset($testing_report_data->c_et_testing)) ? $testing_report_data->c_et_testing : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="c_et_stop_run_time" class="col-sm-6 input-sm">Stop Run Time (Minutes)</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="testing_report_data[c_et_stop_run_time]" id="c_et_stop_run_time" class="form-control input-sm" value="<?= (isset($testing_report_data->c_et_stop_run_time)) ? $testing_report_data->c_et_stop_run_time : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">										   
                                        <legend class="scheduler-border text-primary text-bold">Control Panel Board</legend>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="c_cpb_testing" class="col-sm-6 input-sm">Testing</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="testing_report_data[c_cpb_testing]" id="c_cpb_testing" class="form-control input-sm" value="<?= (isset($testing_report_data->c_cpb_testing)) ? $testing_report_data->c_cpb_testing : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="c_cpb_working_hours" class="col-sm-6 input-sm">Working Hours</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="testing_report_data[c_cpb_working_hours]" id="c_cpb_working_hours" class="form-control input-sm" value="<?= (isset($testing_report_data->c_cpb_working_hours)) ? $testing_report_data->c_cpb_working_hours : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
									<fieldset class="scheduler-border">
                                        <legend class="scheduler-border text-primary text-bold">&nbsp;</legend>
										<div class="col-md-11">
											<h5 class="text-center"><b>Daily Working Report</b></h5>
										</div>
										<div class="col-md-12">
											<div class="col-md-1" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Date </label>
												</div>
											</div>
											<div class="col-md-1" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Operating Person </label>
												</div>
											</div>
											<div class="col-md-2" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Site Incharge Person </label>
												</div>
											</div>
											<div class="col-md-1" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Use Raw Material </label>
												</div>
											</div>
											<div class="col-md-1" style="margin-left: -10px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Moisture  </label>
												</div>
											</div>
											<div class="col-md-1" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Production Hr </label>
												</div>
											</div>
											<div class="col-md-2" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> After Process Moisture </label>
												</div>
											</div>
											<div class="col-md-1" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Working Hrs  </label>
												</div>
											</div>
											<div class="col-md-1" style="padding: 0px 2px;">
												<div class="form-group text-center">
													<label for="" class="input-sm"> Remark </label>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div id="daily_working_report">
											<div id="working_fields">
												<div class="col-md-12">
                                                    <input type="hidden" name="daily_working_report_data[dwr_id][]" id="dwr_id" class="dwr_id" >
													<div class="col-md-1" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[work_date][]" id="work_date" class="work_date form-control input-sm input-datepicker" value="<?= (isset($daily_working_report_data->work_date)) ? date('d-m-Y', strtotime($daily_working_report_data->work_date)) : date('d-m-Y'); ?>"> 
														</div>
													</div>
													<div class="col-md-1" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[oper_person][]" id="oper_person" class="oper_person form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-2" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[dwr_incharge_person][]" id="dwr_incharge_person" class="dwr_incharge_person form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-1" style="padding: 0px 15px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[use_raw_mater][]" id="use_raw_mater" class="use_raw_mater form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-1" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[moisture][]" id="moisture" class="moisture form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-1" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[produ_hour][]" id="produ_hour" class="produ_hour form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-2" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[after_mois][]" id="after_mois" class="after_mois form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-1" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[working_hour][]" id="working_hour" class="working_hour form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-1" style="padding: 0px 2px;">
														<div class="form-group text-center">
															<input type="text" name="daily_working_report_data[remark][]" id="remark" class="remark form-control input-sm" value="">
														</div>
													</div>
													<div class="col-md-1">
														<button type="button" class="btn btn-danger btn-xs pull-center remove_working_detail" style="display: none;"> <i class="fa fa-close"></i></button>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-11">
											<button type="button" class="btn btn-info btn-xs pull-right add_working_detail"> <i class="fa fa-plus"></i></button>
										</div>
										<div class="">
                                            <div class="col-md-11">
                                                <h5 class="text-center"><b>Problem / Resolution</b></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group text-center">
                                                        <label for="problem_resolution_option" class="input-sm"> Problem / Resolution </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-9" style="padding: 0px 2px;">
                                                    <div class="form-group text-center">
                                                        <label for="problem_resolution_text" class="input-sm"> Detail </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="problem_resolution_detail_fields">
                                                <div id="problem_resolution_fields">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="problem_resolution_details[problem_resolution_id][]" id="problem_resolution_id" class="problem_resolution_id" >
                                                    <div class="col-md-2" style="padding: 0px 2px;">
                                                        <div class="form-group">
                                                            <select name="problem_resolution_details[problem_resolution_option][]" id="problem_resolution_option" class="form-control input-sm select2 problem_resolution_option" >
                                                                <option value=""> - Select - </option>
                                                            <?php foreach($problem_resolutions as $problem_resolution_row){ ?>
                                                                <option value="<?= $problem_resolution_row->id; ?>" <?= isset($problem_resolution_option_id) && $problem_resolution_row->id == $problem_resolution_option_id?'selected="selected"':'';?>><?= $problem_resolution_row->label; ?></option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9" style="padding: 0px 2px;">
                                                        <div class="form-group">
                                                            <input type="text" name="problem_resolution_details[problem_resolution_text][]" id="problem_resolution_text" class="form-control input-sm problem_resolution_text" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button type="button" class="btn btn-danger btn-xs pull-center remove_problem_resolution_detail" style="display: none;"> <i class="fa fa-close"></i></button>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-11">
                                                <button type="button" class="btn btn-info btn-xs pull-right add_problem_resolution_detail"> <i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
										<div class="clearfix"></div> 
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold">Customer Satisfaction Survey</legend>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="num_of_equipments" class="col-sm-8 input-sm">How many of Equipments from Jay Khodiyar Group do you have?</label>
													<div class="col-sm-4">
														<input type="text" name="installation_data[num_of_equipments]" id="num_of_equipments" class="form-control input-sm" value="<?= (isset($installation_data->num_of_equipments)) ? $installation_data->num_of_equipments : ''; ?>">
													</div>
												</div>
                                            </div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="" class="col-sm-7 input-sm">Which Equipments have you Purchased?</label>
                                                    <div class="col-sm-5 text-right"><label class="item_name_label input-sm"></label></div>
												</div>
                                            </div>
                                        </div>
                                        <div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="technician_of_jk_installed" class="col-sm-9 input-sm">Has The Technician of Jay Khodiyar Installed Your Plant Properly? Rate The Installation of The Plant According To Your Requirement.</label>
													<div class="col-sm-3">
														<select name="installation_data[technician_of_jk_installed]" id="technician_of_jk_installed" class="form-control input-sm customer_feedback"></select>
													</div><br /><br />
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="technician_from_jk_accompanied" class="col-sm-9 input-sm">Had The Technician From Jay Khodiyar Accompanied Your Technical Operator to Train and Inform ?</label>
													<div class="col-sm-3">
														<select name="installation_data[technician_from_jk_accompanied]" id="technician_from_jk_accompanied" class="form-control input-sm customer_feedback"></select>
													</div>
												</div>
											</div>
                                        </div>
                                        <div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="technician_from_jk_helped" class="col-sm-9 input-sm">Rate From our Technician From Jay Khodiyar Helped And Guided Your Technical Operator When There Occurs Any Problem With Operating The Machine. </label>
													<div class="col-sm-3">
														<select name="installation_data[technician_from_jk_helped]" id="technician_from_jk_helped" class="form-control input-sm customer_feedback"></select>
													</div><br /><br /><br />
												</div>
                                            </div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="performance_of_our_equipment" class="col-sm-9 input-sm">Please Rate From Below Given Option For Performance Of Our Equipment After Complete Installation.</label>
													<div class="col-sm-3">
														<select name="installation_data[performance_of_our_equipment]" id="performance_of_our_equipment" class="form-control input-sm customer_feedback"></select>
													</div>
												</div>
											</div>
										</div>
                                        <div class="row">
											<div class="col-md-4">
                                                <label for="overall_work_desc" class="input-sm">Describe in General The Overall Work, Technical Support And Behaviour Of Jay Khodiyar Technician At Your Place : </label>
                                                <div class="">
                                                    <textarea rows="4" class="form-control" name="installation_data[overall_work_desc]" id="overall_work_desc"><?= (isset($installation_data->overall_work_desc)) ? $installation_data->overall_work_desc : ''; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="quantity_of_production_desc" class="input-sm">Describe The Quantity Of Production in Tons/Day wise Under The Supervision And Guidance Of Jay Khodiyar Technician and at present : </label>
                                                <div class="">
                                                    <textarea rows="4" class="form-control" name="installation_data[quantity_of_production_desc]" id="quantity_of_production_desc"><?= (isset($installation_data->quantity_of_production_desc)) ? $installation_data->quantity_of_production_desc : ''; ?></textarea>
                                                </div>
                                            </div>
                                        	<div class="col-md-4">
                                                <label for="satisfaction_level_desc" class="input-sm">According To You, Where We Should Improve So That We Can Increase Your Satisfaction Level? :</label>
                                                <div class="">
                                                    <textarea rows="4" class="form-control" name="installation_data[satisfaction_level_desc]" id="satisfaction_level_desc"><?= (isset($installation_data->satisfaction_level_desc)) ? $installation_data->satisfaction_level_desc : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
										<div class="clearfix"></div>
										<div class="row">
                                            <div class="col-md-4">
                                                <label for="query_suggestion_desc" class="input-sm">Any other Query/Suggestion do you have regarding our Equipment or Service? :</label>
                                                <div class="">
                                                    <textarea rows="4" class="form-control" name="installation_data[query_suggestion_desc]" id="query_suggestion_desc"><?= (isset($installation_data->query_suggestion_desc)) ? $installation_data->query_suggestion_desc : ''; ?></textarea>
                                                </div>
                                            </div>
											<div class="col-md-4">
                                                <label for="feedback_for_quality" class="input-sm">Feedback for equipment Quality :</label>
                                                <div class="">
                                                    <textarea rows="4" class="form-control" name="installation_data[feedback_for_quality]" id="feedback_for_quality"><?= (isset($installation_data->feedback_for_quality)) ? $installation_data->feedback_for_quality : ''; ?></textarea>
                                                </div>
                                            </div>
											<div class="col-md-4">
                                                <label for="feedback_for_team" class="input-sm">Feedback for our Technical Team : </label>
                                                <div class="">
                                                    <textarea rows="4" class="form-control" name="installation_data[feedback_for_team]" id="feedback_for_team"><?= (isset($installation_data->feedback_for_team)) ? $installation_data->feedback_for_team : ''; ?></textarea>
                                                </div>
                                            </div>
										</div>
										<div class="clearfix"></div>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_3">
							<div class="row">
								<div class="col-md-12">
									<fieldset class="scheduler-border">										   
										<legend class="scheduler-border text-primary text-bold">Electric Motor & Equipment Testing No Load Report At Jay Khodiyar Factory</legend>
										<div class="col-md-12">
											<div class="table-responsive">          
												<table class="table table-striped table-bordered table-hover table-condensed">
													<thead>
														<tr>
															<th><strong>User</strong></th>
															<th><strong>Make</strong></th>
															<th><strong>HP</strong></th>
															<th><strong>KW</strong></th>
															<th><strong>Frequency</strong></th>
															<th><strong>RPM</strong></th>
															<th><strong>Volts</strong></th>
                                                            <th><strong>Serial No.</strong></th>
															<th><strong>Normal Amps</strong></th>
															<th><strong>Working Amps</strong></th>
															<th><strong>Total WorkingHours</strong></th>
														</tr>
													</thead>
													<tbody id="motor_testing_details_jk"></tbody>												
												</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="table-responsive">          
												<table class="table table-striped table-bordered table-hover table-condensed">
													<thead>
														<tr>
															<th><strong>User</strong></th>
															<th><strong>Make</strong></th>
															<th><strong>Gear Type</strong></th>
															<th><strong>Model</strong></th>
															<th><strong>Ratio</strong></th>
                                                            <th><strong>Serial No.</strong></th>
															<th><strong>Normal Sound</strong></th>
															<th><strong>Working Sound</strong></th>
															<th><strong>Total WorkingHours</strong></th>
														</tr>
													</thead>
													<tbody id="gearbox_testing_details_jk"></tbody>
												</table>
											</div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">										   
										<legend class="scheduler-border text-primary text-bold"> Equipment Testing Normal Load Report At Jay Khodiyar Factory</legend>
										<div class="col-md-12">
											<div class="form-group">
												<label for="" class="col-sm-2 input-sm"><strong>Testing Capacity</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>Capacity Hrs</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>Test of Raw Material</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>Moisture of Raw Material</strong></label>
												<label for="" class="col-sm-2 input-sm"><strong>After Procces of Moisture</strong></label>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="item_serial_no" class="col-sm-2 input-sm">Testing Normal Load</label>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_normal_capacity_hrs" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_normal_capacity_hrs)) ? $testing_report_data->jk_testing_normal_capacity_hrs : ''; ?>" placeholder="Kgs">
												</div>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_normal_test_raw_material" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_normal_test_raw_material)) ? $testing_report_data->jk_testing_normal_test_raw_material : ''; ?>" placeholder="Name of Raw Material">
												</div>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_normal_moisture_raw_material" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_normal_moisture_raw_material)) ? $testing_report_data->jk_testing_normal_moisture_raw_material : ''; ?>" placeholder="Moisture %">
												</div>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_normal_after_procces_moisture" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_normal_after_procces_moisture)) ? $testing_report_data->jk_testing_normal_after_procces_moisture : ''; ?>" placeholder="Moisture %">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="item_serial_no" class="col-sm-2 input-sm">Testing Full Load</label>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_full_capacity_hrs" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_full_capacity_hrs)) ? $testing_report_data->jk_testing_full_capacity_hrs : ''; ?>" placeholder="Kgs">
												</div>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_full_test_raw_material" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_full_test_raw_material)) ? $testing_report_data->jk_testing_full_test_raw_material : ''; ?>" placeholder="Name of Raw Material">
												</div>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_full_moisture_raw_material" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_full_moisture_raw_material)) ? $testing_report_data->jk_testing_full_moisture_raw_material : ''; ?>" placeholder="Moisture %">
												</div>
												<div class="col-sm-2">
													<input type="text" class="form-control input-sm item_data" id="jk_testing_full_after_procces_moisture" readonly="readonly" value="<?= (isset($testing_report_data->jk_testing_full_after_procces_moisture)) ? $testing_report_data->jk_testing_full_after_procces_moisture : ''; ?>" placeholder="Moisture %">
												</div>
											</div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">										   
										<legend class="scheduler-border text-primary text-bold"> Temperature Parameters At Jay Khodiyar Factory</legend>
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="form-group">
													<label for="jk_temp_" class="col-sm-6 input-sm">Oil Temperature</label>
													<div class="col-sm-6">
														<input type="text" class="form-control input-sm item_data" id="jk_temp_oli" readonly="readonly" value="<?= (isset($testing_report_data->jk_temp_oli)) ? $testing_report_data->jk_temp_oli : ''; ?>">
													</div>
												</div>
												<div class="form-group">
													<label for="jk_temp_eqipment" class="col-sm-6 input-sm">Equipment Temperature</label>
													<div class="col-sm-6">
														<input type="text" class="form-control input-sm item_data" id="jk_temp_eqipment" readonly="readonly" value="<?= (isset($testing_report_data->jk_temp_eqipment)) ? $testing_report_data->jk_temp_eqipment : ''; ?>">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="jk_temp_" class="col-sm-6 input-sm">Head Temperature</label>
													<div class="col-sm-6">
														<input type="text" class="form-control input-sm item_data" id="jk_temp_head" readonly="readonly" value="<?= (isset($testing_report_data->jk_temp_head)) ? $testing_report_data->jk_temp_head : ''; ?>">
													</div>
												</div>
												<div class="form-group">
													<label for="jk_temp_" class="col-sm-6 input-sm">Kiln Temperature</label>
													<div class="col-sm-6">
														<input type="text" class="form-control input-sm item_data" id="jk_temp_kiln" readonly="readonly" value="<?= (isset($testing_report_data->jk_temp_kiln)) ? $testing_report_data->jk_temp_kiln : ''; ?>">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="jk_temp_" class="col-sm-6 input-sm">Hot Air Temperature</label>
													<div class="col-sm-6">
														<input type="text" class="form-control input-sm item_data" id="jk_temp_hot_air" readonly="readonly" value="<?= (isset($testing_report_data->jk_temp_hot_air)) ? $testing_report_data->jk_temp_hot_air : ''; ?>">
													</div>
												</div>
												<div class="form-group">
													<label for="jk_temp_" class="col-sm-6 input-sm">Weather</label>
													<div class="col-sm-6">
														<input type="text" class="form-control input-sm item_data" id="jk_temp_weather" readonly="readonly" value="<?= (isset($testing_report_data->jk_temp_weather)) ? $testing_report_data->jk_temp_weather : ''; ?>">
													</div>
												</div>
											</div>
										</div>
									</fieldset>
                                    <fieldset class="scheduler-border">										   
                                        <legend class="scheduler-border text-primary text-bold">Equipment Testing</legend>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="jk_et_equipment_rpm" class="col-sm-6 input-sm">Equipment RPM</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="jk_et_equipment_rpm"  class="form-control input-sm" readonly="readonly" value="<?= (isset($testing_report_data->jk_et_equipment_rpm)) ? $testing_report_data->jk_et_equipment_rpm : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="jk_et_testing" class="col-sm-6 input-sm">Testing</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="jk_et_testing" class="form-control input-sm" readonly="readonly" value="<?= (isset($testing_report_data->jk_et_testing)) ? $testing_report_data->jk_et_testing : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="jk_et_stop_run_time" class="col-sm-6 input-sm">Stop Run Time (Minutes)</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="jk_et_stop_run_time" class="form-control input-sm" readonly="readonly" value="<?= (isset($testing_report_data->jk_et_stop_run_time)) ? $testing_report_data->jk_et_stop_run_time : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="scheduler-border">										   
                                        <legend class="scheduler-border text-primary text-bold">Control Panel Board</legend>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="jk_cpb_testing" class="col-sm-6 input-sm">Testing</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="jk_cpb_testing" class="form-control input-sm" readonly="readonly" value="<?= (isset($testing_report_data->jk_cpb_testing)) ? $testing_report_data->jk_cpb_testing : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="jk_cpb_working_hours" class="col-sm-6 input-sm">Working Hours</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="jk_cpb_working_hours" class="form-control input-sm" readonly="readonly" value="<?= (isset($testing_report_data->jk_cpb_working_hours)) ? $testing_report_data->jk_cpb_working_hours : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold">Testing Report Data</legend>
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label for="assembling_equipment" class="col-sm-5 input-sm">Assembling Equipment</label>
													<div class="col-sm-7">
														<input type="text" class="assembling_equipment form-control input-sm" id="assembling_equipment" value="<?= (isset($installation_data->assembling_equipment)) ? date('d-m-Y', strtotime($installation_data->assembling_equipment)) : ''; ?>" readonly="">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="equipment_testing_date" class="col-sm-5 input-sm">Equipment Testing</label>
													<div class="col-sm-7">
														<input type="text" class="form-control input-sm" id="equipment_testing_date" value="<?= (isset($installation_data->equipment_testing_date)) ? date('d-m-Y', strtotime($installation_data->equipment_testing_date)) : ''; ?>" readonly="">
													</div>
												</div>
											</div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="jk_technical_person" class="col-sm-5 input-sm">JK Technical Person</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm select2 jk_technical_person" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
												<div class="form-group">
													<label for="equipment_testing_by" class="col-sm-5 input-sm">Equipment Testing By</label>
													<div class="col-sm-7">
														<input type="text" class="form-control input-sm select2" id="equipment_testing_by" readonly="">
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="tab_5">
							<?php if (isset($installation_data->installation_id) && !empty($installation_data->installation_id)) { ?>
							<div class="row">
								<div class="col-md-12">
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Login Details </legend>
										<div class="col-md-6">
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Created By</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm created_by" id="created_by" value="<?= (isset($installation_data->created_by_name)) ? $installation_data->created_by_name : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Created Date</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm created_at" id="created_at" value="<?= (isset($installation_data->created_at)) ? $installation_data->created_at : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Updated By</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm updated_by" id="updated_by" value="<?= (isset($installation_data->updated_by_name)) ? $installation_data->updated_by_name : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Updated Date</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm updated_at" id="updated_at" value="<?= (isset($installation_data->updated_at)) ? $installation_data->updated_at : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</fieldset>
								</div>
							</div>
							<?php } ?>
						</div>
						<!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				</div>
				<!-- nav-tabs-custom -->
			</div>
			<?php endif; ?>
		</div>
		<section class="content-header">
			<?php if ($installation_add_role || $installation_edit_role): ?>
			<button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
			<?php endif; ?>
			<?php if ($installation_view_role): ?>
			<a href="<?= base_url() ?>installation/installation_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Installation List</a>
			<?php endif; ?>
			<?php if ($installation_add_role): ?>
			<a href="<?= base_url() ?>installation/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Installation</a>
			<?php endif; ?>
		</section>
	</form>
	<div class="clearfix"></div>
</div>
<?php if (isset($installation_data->installation_id) && !empty($installation_data->installation_id)) { } else { ?>
<style>
    .modal-dialog { width: 100%; height: 100%; margin: 0; padding: 0; }
    .modal-content { height: auto; min-height: 100%; border-radius: 0; }
</style>
<div class="modal fade" id="select_testing_report_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<!--<div class="modal-dialog" style="width:80%;height: auto;max-height: 100%">-->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Select Testing Report
					<span class="pull-right">
						<a href='javascript:void(0);' class="btn_go_back btn btn-primary btn-xs">Go Back</a>
						<a href='<?php echo base_url(); ?>' class="btn btn-primary btn-xs">Back To Dashboard</a>
					</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive" style="overflow-x:hidden">
					<table id="pick_testing_report_datatable" class="table custom-table table-striped">
						<thead>
							<tr>
								<th>Testing Report No</th>
								<th>Challan No</th>
								<th>Order / Proforma No</th>
								<th>Quotation No</th>
								<th>Enquiry No</th>
								<th>Item Code</th>
                                <th>Party</th>
								<th>phone No </th>
								<th>Email Id</th>
								<th>City</th>
								<th>State</th>
								<th>Country</th>
							</tr>
						</thead>
						<tbody></tbody>                        
					</table>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
<script type="text/javascript">
	var first_time_edit_mode = 1;
	<?php if (isset($installation_data->installation_id) && !empty($installation_data->installation_id)) { ?>
        first_time_edit_mode = 0;
        feed_testing_report_data(<?= $installation_data->testing_report_id; ?>, <?= $installation_data->challan_id; ?>);
        $("#ajax-loader").show();
	<?php } ?>
	$(document).ready(function () {
        $('input[type="checkbox"].send_sms').iCheck({
			checkboxClass: 'icheckbox_flat-green',
		});
        initAjaxSelect2($("#installation_commissioning_by"), "<?= base_url('app/technical_staff_select2_source') ?>");
		<?php if (isset($installation_data->installation_commissioning_by)) { ?>
		setSelect2Value($("#installation_commissioning_by"), "<?= base_url('app/set_staff_select2_val_by_id/' . $installation_data->installation_commissioning_by) ?>");
		<?php } ?>
            
        initAjaxSelect2($("#jk_technical_person"), "<?= base_url('app/technical_staff_select2_source') ?>");
        <?php if (isset($installation_data->jk_technical_person)) { ?>
            setSelect2Value($("#jk_technical_person"), "<?= base_url('app/set_staff_select2_val_by_id/' . $installation_data->jk_technical_person) ?>");
        <?php } ?>

        initAjaxSelect2($("#project_training_provide_by"), "<?= base_url('app/technical_staff_select2_source') ?>");
        <?php if (isset($installation_data->project_training_provide_by)) { ?>
            setSelect2Value($("#project_training_provide_by"), "<?= base_url('app/set_staff_select2_val_by_id/' . $installation_data->project_training_provide_by) ?>");
        <?php } ?>

		initAjaxSelect2($("#city"), "<?= base_url('app/city_select2_source') ?>");
		initAjaxSelect2($("#state"), "<?= base_url('app/state_select2_source') ?>");
		initAjaxSelect2($("#country"), "<?= base_url('app/country_select2_source') ?>");
		initAjaxSelect2($("#reference_id"), "<?= base_url('app/reference_select2_source') ?>");

		<?php if (isset($installation_data->party_id)) { ?>
		setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id/' . $installation_data->party_id) ?>");
		party_details(<?= $installation_data->party_id; ?>);
		<?php } ?>

		initAjaxSelect2($("#electric_power"), "<?= base_url('app/electric_power_select2_source') ?>");
		<?php if (isset($installation_data->electric_power)) { ?>
		setSelect2Value($("#electric_power"), "<?= base_url('app/set_electric_power_select2_val_by_id/' . $installation_data->electric_power) ?>");
		<?php } ?>

		initAjaxSelect2($(".available_or_not"), "<?= base_url('app/available_or_not_select2_source') ?>");
		<?php if (isset($installation_data->elec_rm2_panel_board_cable)) { ?>
		setSelect2Value($("#elec_rm2_panel_board_cable"), "<?= base_url('app/set_available_or_not_select2_val_by_id/' . $installation_data->elec_rm2_panel_board_cable) ?>");
		<?php } ?>
		<?php if (isset($installation_data->connection_cable_1)) { ?>
		setSelect2Value($("#connection_cable_1"), "<?= base_url('app/set_available_or_not_select2_val_by_id/' . $installation_data->connection_cable_1) ?>");
		<?php } ?>
		<?php if (isset($installation_data->gear_box_oil_1)) { ?>
		setSelect2Value($("#gear_box_oil_1"), "<?= base_url('app/set_available_or_not_select2_val_by_id/' . $installation_data->gear_box_oil_1) ?>");
		<?php } ?>
		<?php if (isset($installation_data->raw_material_1)) { ?>
		setSelect2Value($("#raw_material_1"), "<?= base_url('app/set_available_or_not_select2_val_by_id/' . $installation_data->raw_material_1) ?>");
		<?php } ?>

		initAjaxSelect2($("#water_tank_chiller_system_1"), "<?= base_url('app/water_tank_or_chiller_system_select2_source') ?>");
		<?php if (isset($installation_data->water_tank_chiller_system_1)) { ?>
		setSelect2Value($("#water_tank_chiller_system_1"), "<?= base_url('app/set_water_tank_or_chiller_system_select2_val_by_id/' . $installation_data->water_tank_chiller_system_1) ?>");
		<?php } ?>

		initAjaxSelect2($("#water_plumbing_chiller_conn_1"), "<?= base_url('app/water_plumbing_or_chiller_conn_select2_source') ?>");
		<?php if (isset($installation_data->water_plumbing_chiller_conn_1)) { ?>
		setSelect2Value($("#water_plumbing_chiller_conn_1"), "<?= base_url('app/set_water_plumbing_or_chiller_conn_select2_val_by_id/' . $installation_data->water_plumbing_chiller_conn_1) ?>");
		<?php } ?>

		initAjaxSelect2($("#lubrication_gear_oil_1"), "<?= base_url('app/lubrication_gear_oil_select2_source') ?>");
		<?php if (isset($installation_data->lubrication_gear_oil_1)) { ?>
		setSelect2Value($("#lubrication_gear_oil_1"), "<?= base_url('app/set_lubrication_gear_oil_select2_val_by_id/' . $installation_data->lubrication_gear_oil_1) ?>");
		<?php } ?>

		initAjaxSelect2($(".customer_feedback"), "<?= base_url('app/customer_feedback_select2_source') ?>");
		<?php if (isset($installation_data->product_quality)) { ?>
		setSelect2Value($("#product_quality"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $installation_data->product_quality) ?>");
		<?php } ?>
		<?php if (isset($installation_data->technician_of_jk_installed)) { ?>
		setSelect2Value($("#technician_of_jk_installed"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $installation_data->technician_of_jk_installed) ?>");
		<?php } ?>
		<?php if (isset($installation_data->technician_from_jk_accompanied)) { ?>
		setSelect2Value($("#technician_from_jk_accompanied"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $installation_data->technician_from_jk_accompanied) ?>");
		<?php } ?>
		<?php if (isset($installation_data->technician_from_jk_helped)) { ?>
		setSelect2Value($("#technician_from_jk_helped"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $installation_data->technician_from_jk_helped) ?>");
		<?php } ?>
		<?php if (isset($installation_data->performance_of_our_equipment)) { ?>
		setSelect2Value($("#performance_of_our_equipment"), "<?= base_url('app/set_customer_feedback_select2_val_by_id/' . $installation_data->performance_of_our_equipment) ?>");
		<?php } ?>

		var table
		table = $('#pick_testing_report_datatable').DataTable({
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('installation/pick_testing_report_datatable') ?>",
				"type": "POST",
				"data": function (d) {
					d.request_from = "installation";
				}
			},
			"scrollY": 450,
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%"
		});

		$('#select_testing_report_modal').modal({backdrop: 'static', keyboard: false});
		$('#select_testing_report_modal').modal('show');
		$('#select_testing_report_modal').on('shown.bs.modal', function () {
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
		});

		$(document).on('click', '.testing_report_row', function () {
			var tr = $(this).closest('tr');
			var testing_report_id = $(this).data('testing_report_id');
			var challan_id = $(this).data('challan_id');
			$("#testing_report_id").val(testing_report_id);
			$("#challan_id").val(challan_id);
			feed_testing_report_data(testing_report_id, challan_id);
			$('#select_testing_report_modal').modal('hide');
		});
		$(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_installation").submit();
                return false;
            }
        });
        $(document).on('submit', '#save_installation', function () {
			//var query_suggestion_desc = $("#query_suggestion_desc").attr("name");

			if ($.trim($("#party_id").val()) == '') {
				show_notify('Please Select Party.', false);
				return false;
			}
			//$('.module_save_btn').attr('disabled', 'disabled');
			//$("#ajax-loader").show();
			var postData = new FormData(this);
			$.ajax({
				url: "<?= base_url('installation/save_installation') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
					if (json.status == 0) {
						show_notify(json.msg, false);
						$('.module_save_btn').removeAttr('disabled', 'disabled');
						return false;
					}
					if (json['success'] == 'false') {
						show_notify(json['msg'], false);
						$('.module_save_btn').removeAttr('disabled', 'disabled');
					}
					if (json['success'] == 'Added') {
						window.location.href = "<?php echo base_url('installation/installation_list') ?>";
					}
					if (json['success'] == 'Updated') {
						window.location.href = "<?php echo base_url('installation/installation_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
		});

		$(document).on("change", '#contact_person_id', function () {
			var contact_person_id = $(this).val();
			$.ajax({
				url: "<?= base_url(); ?>party/get-contact-person-by-id/" + contact_person_id,
				type: "POST",
				data: null,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					if (data.success == true) {
						$('input[name="contact_person[contact_person_mobile_no]"]').val(data.contact_person_data.mobile_no);
						$('input[name="contact_person[contact_person_email_id]"]').val(data.contact_person_data.email);
						$('#contact_person_designation').html(data.contact_person_data.designation);
						$('#contact_person_department').html(data.contact_person_data.department);
					} else {
						$('input[name="contact_person[contact_person_mobile_no]"]').val('');
						$('input[name="contact_person[contact_person_email_id]"]').val('');
						$('#contact_person_designation').html('');
						$('#contact_person_department').html('');
					}
				}
			});
		});
        
        $(document).on("change", '#jk_technical_person, #installation_commissioning_by, #project_training_provide_by', function () {
            var id = $(this).val();
            var id_attr = $(this).attr('id');
            if(id != '' && id != null){
                $.ajax({
                    url: "<?= base_url(); ?>testing_report/get_staff_contact_no/" + id,
                    type: "POST",
                    success: function (data) {
                        $('#'+id_attr+'_contact').val('');
                        $('#'+id_attr+'_contact').val(data);
                    }
                });
            }
        });

		$(document).on('click', '.add_working_detail', function () {
			$("#working_fields").clone().appendTo("#daily_working_report");
			$(".remove_working_detail:last").css('display', 'block');
            $(".dwr_id:last ").val('');
			$(".work_date:last, .oper_person:last, .dwr_incharge_person:last, .use_raw_mater:last, .moisture:last, .produ_hour:last, .after_mois:last, .working_hour:last, .remark:last").val('');
			$(".work_date:last").datepicker({format: 'dd-mm-yyyy', todayBtn: "linked", todayHighlight: true, autoclose: true}).val('<?=date('d-m-Y');?>');
		});

		$(document).on('click', '.remove_working_detail', function () {
			$(this).closest("#working_fields").remove();
            var dwr_id = $(this).closest("#working_fields").find("#dwr_id").val();
            $('#save_installation').append('<input type="hidden" name="deleted_dwr_id[]" id="deleted_dwr_id" value="' + dwr_id + '" />');
		});
        
        $(document).on('click','.add_problem_resolution_detail',function() {
			$("#problem_resolution_fields").clone().appendTo("#problem_resolution_detail_fields");
			$(".remove_problem_resolution_detail:last").css('display', 'block');
			$(".problem_resolution_id:last ").val('');
            $(".problem_resolution_text:last ").val('');
		});
		
		$(document).on('click','.remove_problem_resolution_detail',function() {
			$(this).closest("#problem_resolution_fields").remove();
            var problem_resolution_id = $(this).closest("#problem_resolution_fields").find("#problem_resolution_id").val();
            $('#save_installation').append('<input type="hidden" name="deleted_problem_resolution_id[]" id="deleted_problem_resolution_id" value="' + problem_resolution_id + '" />');
		});
        
        <?php if(isset($installation_data->installation_id) && !empty($installation_data->installation_id)){ ?>
            var daily_working_report_data = <?=$daily_working_report_data;?>;
            //console.log(daily_working_report_data);
            $('#daily_working_report #working_fields').not('#working_fields:first').remove();
            var daily_working_report_data_rows = Object.keys(daily_working_report_data).length;
            var row_inc = 1;
            $.each(daily_working_report_data, function(daily_working_report_data_i,daily_working_report_data_v){
                $.each(daily_working_report_data_v, function(index,value){
                    if(index == 'id'){
                        daily_working_report_data_v['dwr_id'] = daily_working_report_data_v[index];
                        index = 'dwr_id';
                    }
                    if(index == 'installation_id'){
                        delete daily_working_report_data_v[index];
                    }
                    if(value == null){ value = ''; }
                    $('#daily_working_report #working_fields:nth-last-child(1) #'+ index).val(value);
                });
                if(row_inc < daily_working_report_data_rows){
                    $('.add_working_detail').click();
                }
                row_inc++;
            });
            var problem_resolution_data = <?=$problem_resolution_data;?>;
            $('#problem_resolution_detail_fields #problem_resolution_fields').not('#problem_resolution_fields:first').remove();
            var problem_resolution_data_rows = Object.keys(problem_resolution_data).length;
            var row_inc = 1;
            $.each(problem_resolution_data, function(problem_resolution_data_i,problem_resolution_data_v){
                $.each(problem_resolution_data_v, function(index,value){
                    if(index == 'id'){
                        problem_resolution_data_v['problem_resolution_id'] = problem_resolution_data_v[index];
                        index = 'problem_resolution_id';
                    }
                    if(index == 'installation_id'){
                        delete problem_resolution_data_v[index];
                    }
                    if(value == null){ value = ''; }
                    $('#problem_resolution_detail_fields #problem_resolution_fields:nth-last-child(1) #'+ index).val(value);
                });
                if(row_inc < problem_resolution_data_rows){
                    $('.add_problem_resolution_detail').click();
                }
                row_inc++;
            });
        <?php } ?>
        
	});

	function feed_testing_report_data(testing_report_id, challan_id) {
		$("#ajax-loader").show();
		$.ajax({
			url: '<?php echo BASE_URL; ?>installation/get_testing_report',
			type: "POST",
			async: false,
			data: {testing_report_id: testing_report_id, challan_id: challan_id},
			success: function (data) {
				json = JSON.parse(data);
				//console.log(json);
				var testing_report_data = json.testing_report_data;
				var challan_data = json.challan_data;
				var challan_item = json.challan_item_data;

				$('#testing_report_no').val(testing_report_data.testing_report_no_year);
				$('#testing_report_date').val(testing_report_data.testing_report_date);
				$('#challan_no').val(challan_data.challan_no);
				$('#challan_date').val(challan_data.challan_date);
				$('#quotation_no').val(challan_data.quotation_no);
				$('#sales_order_no').val(challan_data.sales_order_no);
				$('#sales_order_date').val(challan_data.sales_order_date);
				$('#proforma_invoice_no').val(challan_data.proforma_invoice_no);
				$('#proforma_invoice_date').val(challan_data.proforma_invoice_date);
				$('#cust_po_no').val(challan_data.cust_po_no);
				$('#po_date').val(challan_data.po_date);

				$('#manufacturing_m_y').val(testing_report_data.manufacturing_m_y);
				$('#assembling_equipment').val(testing_report_data.assembling_equipment);
				$('#equipment_testing_date').val(testing_report_data.equipment_testing_date);
				$('#equipment_inspection_by').val(testing_report_data.equipment_inspection_by);
				$('#equipment_testing_by').val(testing_report_data.equipment_testing_by);
				$('#delivery_of_equipment').val(testing_report_data.delivery_of_equipment);
                
                <?php if (isset($installation_data->installation_id)) { } else { ?>
                    setSelect2Value($("#jk_technical_person"), "<?= base_url('app/set_staff_select2_val_by_id/') ?>/" + testing_report_data.our_site_incharge_person);
                <?php } ?>
                $('.jk_technical_person').val(testing_report_data.our_site_incharge_person_value);
                
                <?php if (isset($installation_data->party_id)) { } else { ?>
                    setSelect2Value($("#party_id"), "<?= base_url('app/set_party_select2_val_by_id') ?>/" + challan_data.sales_to_party_id);
                    $('#p_party_id').val(challan_data.sales_to_party_id);
                    party_details(challan_data.sales_to_party_id);
                <?php } ?>
				$('select[name="installation_data[kind_attn_id]"]').val(challan_data.kind_attn_id).change();
				$('#item_id').val(challan_item.item_id);
				$('.item_name').val(challan_item.item_name);
				$('.item_name_label').html(challan_item.item_name);
				$('#item_code').val(challan_item.item_code);
				$('#item_serial_no').val(challan_item.item_serial_no);
				$('#item_extra_accessories_id').val(challan_item.item_extra_accessories);
				$('#item_description').val(challan_item.item_description);

				$('#loading_at').val(challan_data.loading_at);
				$('#port_of_loading').val(challan_data.port_of_loading);
				$('#port_of_discharge').val(challan_data.port_of_discharge);
				$('#place_of_delivery').val(challan_data.port_place_of_delivery);
				$('#name_of_shipment').val(challan_data.name_of_shipment);
				$('#truck_container_no').val(challan_item.truck_container_no);
				$('#lr_no').val(challan_item.lr_bl_no);
				$('#lr_date').val(challan_item.lr_date);
				$('#contact_person').val(challan_item.contact_person);
				$('#contact_no').val(challan_item.contact_no);
				$('#driver_name').val(challan_item.driver_name);
				$('#driver_contact_no').val(challan_item.driver_contact_no);

				var li_motor_serial_objectdata = json.motor_data;
				var motor_serial_objectdata = [];
				if (li_motor_serial_objectdata != '') {
					$.each(li_motor_serial_objectdata, function (index, value) {
						motor_serial_objectdata.push(value);
					});
				}
				display_motor_serial_html(motor_serial_objectdata);
				var li_gearbox_serial_objectdata = json.gearbox_data;
				var gearbox_serial_objectdata = [];
				if (li_gearbox_serial_objectdata != '') {
					$.each(li_gearbox_serial_objectdata, function (index, value) {
						gearbox_serial_objectdata.push(value);
					});
				}
				display_gearbox_serial_html(gearbox_serial_objectdata);

				$('#jk_testing_normal_capacity_hrs').val(testing_report_data.jk_testing_normal_capacity_hrs);
				$('#jk_testing_normal_test_raw_material').val(testing_report_data.jk_testing_normal_test_raw_material);
				$('#jk_testing_normal_moisture_raw_material').val(testing_report_data.jk_testing_normal_moisture_raw_material);
				$('#jk_testing_normal_after_procces_moisture').val(testing_report_data.jk_testing_normal_after_procces_moisture);
				$('#jk_testing_full_capacity_hrs').val(testing_report_data.jk_testing_full_capacity_hrs);
				$('#jk_testing_full_test_raw_material').val(testing_report_data.jk_testing_full_test_raw_material);
				$('#jk_testing_full_moisture_raw_material').val(testing_report_data.jk_testing_full_moisture_raw_material);
				$('#jk_testing_full_after_procces_moisture').val(testing_report_data.jk_testing_full_after_procces_moisture);

				$('#jk_temp_oli').val(testing_report_data.jk_temp_oli);
				$('#jk_temp_head').val(testing_report_data.jk_temp_head);
				$('#jk_temp_hot_air').val(testing_report_data.jk_temp_hot_air);
				$('#jk_temp_eqipment').val(testing_report_data.jk_temp_eqipment);
				$('#jk_temp_kiln').val(testing_report_data.jk_temp_kiln);
				$('#jk_temp_weather').val(testing_report_data.jk_temp_weather);
                
				$('#jk_et_equipment_rpm').val(testing_report_data.jk_et_equipment_rpm);
				$('#jk_et_testing').val(testing_report_data.jk_et_testing);
				$('#jk_et_stop_run_time').val(testing_report_data.jk_et_stop_run_time);
				$('#jk_cpb_testing').val(testing_report_data.jk_cpb_testing);
				$('#jk_cpb_working_hours').val(testing_report_data.jk_cpb_working_hours);
                
                $('#c_testing_normal_capacity_hrs').val(testing_report_data.c_testing_normal_capacity_hrs);
				$('#c_testing_normal_test_raw_material').val(testing_report_data.c_testing_normal_test_raw_material);
				$('#c_testing_normal_moisture_raw_material').val(testing_report_data.c_testing_normal_moisture_raw_material);
				$('#c_testing_normal_after_procces_moisture').val(testing_report_data.c_testing_normal_after_procces_moisture);
				$('#c_testing_full_capacity_hrs').val(testing_report_data.c_testing_full_capacity_hrs);
				$('#c_testing_full_test_raw_material').val(testing_report_data.c_testing_full_test_raw_material);
				$('#c_testing_full_moisture_raw_material').val(testing_report_data.c_testing_full_moisture_raw_material);
				$('#c_testing_full_after_procces_moisture').val(testing_report_data.c_testing_full_after_procces_moisture);

				$('#c_temp_oli').val(testing_report_data.c_temp_oli);
				$('#c_temp_head').val(testing_report_data.c_temp_head);
				$('#c_temp_hot_air').val(testing_report_data.c_temp_hot_air);
				$('#c_temp_eqipment').val(testing_report_data.c_temp_eqipment);
				$('#c_temp_kiln').val(testing_report_data.c_temp_kiln);
				$('#c_temp_weather').val(testing_report_data.c_temp_weather);
                
                $('#c_et_equipment_rpm').val(testing_report_data.c_et_equipment_rpm);
				$('#c_et_testing').val(testing_report_data.c_et_testing);
				$('#c_et_stop_run_time').val(testing_report_data.c_et_stop_run_time);
				$('#c_cpb_testing').val(testing_report_data.c_cpb_testing);
				$('#c_cpb_working_hours').val(testing_report_data.c_cpb_working_hours);

				$("#ajax-loader").hide();
			},
		});
	}

	function display_motor_serial_html(motor_serial_objectdata){
		var motor_testing_details_jk = '';
		var motor_testing_details_c = '';
		$.each(motor_serial_objectdata, function (index, value) {
			var motor_testing_no_jk = '<tr class="motor_serial_index_' + index + '">';
			motor_testing_no_jk += '<td>' + value.motor_user + '</td>';
			motor_testing_no_jk += '<td>' + value.motor_make + '</td>';
			motor_testing_no_jk += '<td>' + value.motor_hp + '</td>';
			motor_testing_no_jk += '<td>' + value.motor_kw + '</td>';
			motor_testing_no_jk += '<td>' + value.motor_frequency + '</td>';
			motor_testing_no_jk += '<td>' + value.motor_rpm + '</td>';
			motor_testing_no_jk += '<td>' + value.motor_volts_cycles + '</td>';
			motor_testing_no_jk += '<td>' + value.motor_serial_no + '</td>';
			var normal_amps_jk = value.normal_amps_jk == null ? '' : value.normal_amps_jk;
			var working_amps_jk = value.working_amps_jk == null ? '' : value.working_amps_jk;
			var total_workinghours_jk = value.total_workinghours_jk == null ? '' : value.total_workinghours_jk;
			motor_testing_no_jk += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][normal_amps_jk]" class="form-control col-sm-1 input-sm" id="normal_amps_jk_'+ value.id +'" value="' + normal_amps_jk + '" placeholder="Sound" readonly=""></td>';
			motor_testing_no_jk += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][working_amps_jk]" class="form-control col-sm-1 input-sm" id="working_amps_jk_'+ value.id +'" value="' + working_amps_jk + '" placeholder="Sound" readonly=""></td>';
			motor_testing_no_jk += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][total_workinghours_jk]" class="form-control col-sm-1 input-sm" id="total_workinghours_jk_'+ value.id +'" value="' + total_workinghours_jk + '" placeholder="Hrs" readonly=""></td>';
			motor_testing_details_jk += motor_testing_no_jk;

			var motor_testing_no_c = '<tr class="motor_serial_index_' + index + '">';
			motor_testing_no_c += '<td>' + value.motor_user + '</td>';
			motor_testing_no_c += '<td>' + value.motor_make + '</td>';
			motor_testing_no_c += '<td>' + value.motor_hp + '</td>';
			motor_testing_no_c += '<td>' + value.motor_kw + '</td>';
			motor_testing_no_c += '<td>' + value.motor_frequency + '</td>';
			motor_testing_no_c += '<td>' + value.motor_rpm + '</td>';
			motor_testing_no_c += '<td>' + value.motor_volts_cycles + '</td>';
			motor_testing_no_c += '<td>' + value.motor_serial_no + '</td>';
			var normal_amps_c = value.normal_amps_c == null ? '' : value.normal_amps_c;
			var working_amps_c = value.working_amps_c == null ? '' : value.working_amps_c;
			var total_workinghours_c = value.total_workinghours_c == null ? '' : value.total_workinghours_c;
			motor_testing_no_c += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][normal_amps_c]" class="form-control col-sm-1 input-sm" id="normal_amps_c_'+ value.id +'" value="' + normal_amps_c + '" placeholder="Sound"></td>';
			motor_testing_no_c += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][working_amps_c]" class="form-control col-sm-1 input-sm" id="working_amps_c_'+ value.id +'" value="' + working_amps_c + '" placeholder="Sound"></td>';
			motor_testing_no_c += '<td><input type="text" name="challan_items_motor_details[' + value.id + '][total_workinghours_c]" class="form-control col-sm-1 input-sm" id="total_workinghours_c_'+ value.id +'" value="' + total_workinghours_c + '" placeholder="Hrs"></td>';
			motor_testing_details_c += motor_testing_no_c;

		});
		$('tbody#motor_testing_details_jk').html(motor_testing_details_jk);
		$('tbody#motor_testing_details_c').html(motor_testing_details_c);
	}
	function display_gearbox_serial_html(gearbox_serial_objectdata){
		var gearbox_testing_details_jk = '';
		var gearbox_testing_details_c = '';
		$.each(gearbox_serial_objectdata, function (index, value) {
			var gearbox_testing_no_jk = '<tr class="gearbox_serial_index_' + index + '">';
			gearbox_testing_no_jk += '<td>' + value.gearbox_user + '</td>';
			gearbox_testing_no_jk += '<td>' + value.gearbox_make + '</td>';
			gearbox_testing_no_jk += '<td>' + value.gearbox_gear_type + '</td>';
			gearbox_testing_no_jk += '<td>' + value.gearbox_model + '</td>';
			gearbox_testing_no_jk += '<td>' + value.gearbox_ratio + '</td>';
			gearbox_testing_no_jk += '<td>' + value.gearbox_serial_no + '</td>';
			var normal_amps_jk = value.normal_amps_jk == null ? '' : value.normal_amps_jk;
			var working_amps_jk = value.working_amps_jk == null ? '' : value.working_amps_jk;
			var total_workinghours_jk = value.total_workinghours_jk == null ? '' : value.total_workinghours_jk;
			gearbox_testing_no_jk += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][normal_amps_jk]" class="form-control col-sm-1 input-sm" id="normal_amps_jk_'+ value.id +'" value="' + normal_amps_jk + '" placeholder="Sound" readonly=""></td>';
			gearbox_testing_no_jk += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][working_amps_jk]" class="form-control col-sm-1 input-sm" id="working_amps_jk_'+ value.id +'" value="' + working_amps_jk + '" placeholder="Sound" readonly=""></td>';
			gearbox_testing_no_jk += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][total_workinghours_jk]" class="form-control col-sm-1 input-sm" id="total_workinghours_jk_'+ value.id +'" value="' + total_workinghours_jk + '" placeholder="Hrs" readonly=""></td>';
			gearbox_testing_details_jk += gearbox_testing_no_jk;

			var gearbox_testing_no_c = '<tr class="gearbox_serial_index_' + index + '">';
			gearbox_testing_no_c += '<td>' + value.gearbox_user + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_make + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_gear_type + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_model + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_ratio + '</td>';
			gearbox_testing_no_c += '<td>' + value.gearbox_serial_no + '</td>';
			var normal_amps_c = value.normal_amps_c == null ? '' : value.normal_amps_c;
			var working_amps_c = value.working_amps_c == null ? '' : value.working_amps_c;
			var total_workinghours_c = value.total_workinghours_c == null ? '' : value.total_workinghours_c;
			gearbox_testing_no_c += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][normal_amps_c]" class="form-control col-sm-1 input-sm" id="normal_amps_c_'+ value.id +'" value="' + normal_amps_c + '" placeholder="Sound"></td>';
			gearbox_testing_no_c += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][working_amps_c]" class="form-control col-sm-1 input-sm" id="working_amps_c_'+ value.id +'" value="' + working_amps_c + '" placeholder="Sound"></td>';
				gearbox_testing_no_c += '<td><input type="text" name="challan_items_gearbox_details[' + value.id + '][total_workinghours_c]" class="form-control col-sm-1 input-sm" id="total_workinghours_c_'+ value.id +'" value="' + total_workinghours_c + '" placeholder="Hrs"></td>';
			gearbox_testing_details_c += gearbox_testing_no_c;

		});
		$('tbody#gearbox_testing_details_jk').html(gearbox_testing_details_jk);
		$('tbody#gearbox_testing_details_c').html(gearbox_testing_details_c);
	}

	function party_details(id) {
		$("#ajax-loader").show();
		$.ajax({
			type: "POST",
			url: '<?= base_url(); ?>sales/ajax_load_party_with_cnt_person/' + id,
			async: false,
			data: id = 'party_id',
			success: function (data) {
				var json = $.parseJSON(data);

				if (json['address']) {
					$(".address").html(json['address']);
				} else {
					$(".address").html("");
				}

				if (json['party_code']) {
					$('#party_code').val(json['party_code']);
				}
				if (json['party_name']) {
					$('#party_name').val(json['party_name']);
				}
				if (json['party_id']) {
					$(".party_id").val(json['party_id']);
					$("#p_party_id").val(json['party_id']);
				}

				if (json['city_id']) {
					setSelect2Value($("#city"), '<?= base_url() ?>app/set_city_select2_val_by_id/' + json['city_id']);
				} else {
					setSelect2Value($("#city"));
				}
				if (json['state_id']) {
					setSelect2Value($("#state"), '<?= base_url() ?>app/set_state_select2_val_by_id/' + json['state_id']);
				} else {
					setSelect2Value($("#state"));
				}
				if (json['country_id']) {
					setSelect2Value($("#country"), '<?= base_url() ?>app/set_country_select2_val_by_id/' + json['country_id']);
				} else {
					setSelect2Value($("#country"));
				}

				if (json['fax_no']) {
					$(".fax_no").val(json['fax_no']);
				} else {
					$(".fax_no").val("");
				}

				if (json['email_id']) {
					$(".email_id").html(json['email_id'].replace(/<br *\/?>/gi, '\n'));
				} else {
					$(".email_id").html("");
				}

				if (json['website']) {
					$(".website").val(json['website']);
				} else {
					$(".website").val("");
				}
				if (json['pincode']) {
					$(".pincode").val(json['pincode']);
				} else {
					$(".pincode").val("");
				}
				if (json['phone_no']) {
					$(".phone_no").val(json['phone_no']);
				} else {
					$(".phone_no").val("");
				}
				if (json['reference_id']) {
					setSelect2Value($("#reference_id"), '<?= base_url() ?>app/set_reference_select2_val_by_id/' + json['reference_id']);
				} else {
					setSelect2Value($("#reference_id"));
				}
				if (json['reference_description']) {
					$("#reference_description").html(json['reference_description']);
				} else {
					$("#reference_description").html('');
				}

				if (json['party_type_1_id'] != '') {
					setSelect2Value($("#sales_id"), '<?= base_url() ?>app/set_sales_select2_val_by_id/' + json['party_type_1_id']);
					if (json['party_type_1_id'] == '<?= PARTY_TYPE_DOMESTIC_ID; ?>') {
						setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= INR_CURRENCY_ID; ?>);
										}
										if (json['party_type_1_id'] == '<?= PARTY_TYPE_EXPORT_ID; ?>') {
							setSelect2Value($("#currency_id"), '<?= base_url() ?>app/set_currency_select2_val_by_id/' +<?= USD_CURRENCY_ID; ?>);
											}
											var currency_title = $('#select2-currency_id-container').attr('title');
							$('#received_payment_currency').html(currency_title);
						} else {
							setSelect2Value($("#sales_id"));
						}

						<?php if (isset($installation_data->installation_id) && !empty($installation_data->installation_id)) {

} else { ?>
						if (json['branch_id']) {
							setSelect2Value($("#branch_id"), "<?= base_url('app/set_branch_select2_val_by_id') ?>/" + json['branch_id']);
						} else {
							setSelect2Value($("#branch_id"));
						}

						if (json['agent_id']) {
							setSelect2Value($("#agent_id"), '<?= base_url() ?>app/set_agent_select2_val_by_id/' + json['agent_id']);
						} else {
							setSelect2Value($("#agent_id"));
						}
						<?php } ?>

						if (first_time_edit_mode == 1) {
							if (json['contact_persons_array']) {
								var option_html = '';
								if (json['contact_persons_array'].length > 0) {
									//                            console.log(json['contact_persons_array']);
									$.each(json['contact_persons_array'], function (index, value) {
										option_html += "<option value='" + value.contact_person_id + "'>" + value.name + "</option>";
									})
									$('select[name="installation_data[kind_attn_id]"]').html(option_html).select2();
								} else {
									$('select[name="installation_data[kind_attn_id]"]').html('');
								}
								$('select[name="installation_data[kind_attn_id]"]').change();
							}
						} else {
							first_time_edit_mode = 1;
						}
						$("#ajax-loader").hide();
					}
				});
			}

			<?php if (isset($_GET['view'])) { ?>
			$(window).load(function () {
			display_as_a_viewpage();
		});
		<?php } ?>
</script>