<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class='text-primary text-bold'> Installation List </small>
            <?php $installation_add_role = $this->app_model->have_access_role(INSTALLATION_MODULE_ID, "add"); ?>
            <?php if($installation_add_role): ?>
                <a href="<?=base_url()?>installation/add" class="btn btn-info btn-xs pull-right">Add Installation</a>
            <?php endif;?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_installation_list" class="table custom-table agent-table">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Installation No</th>
                                    <th>Testing Report No</th>
                                    <th>Challan No</th>
                                    <th>Order / Proforma No</th>
                                    <th>Quotation No</th>
                                    <th>Enquiry No</th>
                                    <th>Item Code</th>
                                    <th>Party</th>
                                    <th>Contact No</th>
                                    <th>Email Id</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        table = $('#table_installation_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('installation/installation_datatable')?>",
                "type": "POST",
                "data": function(d){
                    //d.city_id = $("#city_id").val();
                },
            },
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=installation',
                    success: function(data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

    });
</script>
