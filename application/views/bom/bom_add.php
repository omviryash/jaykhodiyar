<div class="content-wrapper">
    <form class="form-horizontal" action="" method="post" id="save_bom">
        <?php if (isset($bom_data->bom_id) && !empty($bom_data->bom_id)) { ?>
            <input type="hidden" name="bom_data[bom_id]" id="bom_id" value="<?= $bom_data->bom_id ?>">
        <?php } ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">BOM</small>
                <?php
                    $add_role = $this->app_model->have_access_role(MASTER_BOM_MENU_ID, "add");
                    $edit_role = $this->app_model->have_access_role(MASTER_BOM_MENU_ID, "edit");
                    $view_role = $this->app_model->have_access_role(MASTER_BOM_MENU_ID, "view");
                ?>
                <?php if ($add_role || $edit_role): ?>
                    <button type="submit" class="btn btn-info btn-xs pull-right btn-save-item" id="btn_save_bom" style="margin: 5px;">Save </button>
                <?php endif; ?>
                <?php if ($view_role): ?>
                    <a href="<?= base_url() ?>master/bom_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">BOM List</a>
                <?php endif; ?>
                <?php if ($add_role): ?>
                    <a href="<?= base_url() ?>master/bom_add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add BOM</a>
                <?php endif; ?>
            </h1>
        </section>
        <div class="clearfix">
            <?php if ($add_role || $edit_role): ?>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">BOM</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <!-- Horizontal Form -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 input-sm">Sales Item<span class="required-sign">&nbsp;*</span></label>
                                            <div class="col-sm-6">
                                                <select name="bom_data[sales_item_id]" id="sales_item_id" class="form-control input-sm" onchange="get_item_from_group(this)"></select>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" id="item_code" class="form-control input-sm" value="<?php if(isset($bom_data->item_code)){ echo $bom_data->item_code; } ?>" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="effective_date" class="col-sm-3 input-sm">Effective Date<span class="required-sign">&nbsp;*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="bom_data[effective_date]" id="effective_date" class="form-control input-sm input-datepicker" autocomplete="off" value="<?= (isset($bom_data->effective_date)) ? date('d-m-Y', strtotime($bom_data->effective_date)) : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-8" style="padding: 0px 2px;">
                                                <div class="form-group text-center">
                                                    <label for="" class="input-sm">Item Expense</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="padding: 0px 2px;">
                                                <div class="form-group text-center">
                                                    <label for="" class="input-sm">Expense</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="bom_item_expense_fields">
                                                <?php 
                                                    if(isset($bom_item_expenses) && !empty($bom_item_expenses)){ 
                                                        foreach ($bom_item_expenses as $key => $bom_item_expense){
                                                ?>
                                                    <div id="item_expense_fields">
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="bom_item_expenses[bom_item_expense_id][]" id="bom_item_expense_id" class="form-control input-sm bom_item_expense_id" value="<?php echo $bom_item_expense->bom_item_expense_id; ?>" >
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <input type="text" name="bom_item_expenses[item_expense][]" id="item_expense" class="form-control input-sm item_expense" value="<?php echo $bom_item_expense->item_expense; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3" style="padding-left: 20px;">
                                                                <div class="form-group">
                                                                    <input type="text" name="bom_item_expenses[expense][]" id="expense" class="form-control input-sm expense num_only" value="<?php echo $bom_item_expense->expense; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button type="button" class="btn btn-danger btn-xs pull-center remove_bom_item_expense" <?php echo ($key == 0) ? 'style="display: none;"' : ''; ?> > <i class="fa fa-close"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } } else { ?>
                                                    <div id="item_expense_fields">
                                                        <div class="col-md-12">
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <input type="text" name="bom_item_expenses[item_expense][]" id="item_expense" class="form-control input-sm item_expense">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3" style="padding-left: 20px;">
                                                                <div class="form-group">
                                                                    <input type="text" name="bom_item_expenses[expense][]" id="expense" class="form-control input-sm expense num_only">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button type="button" class="btn btn-danger btn-xs pull-center remove_bom_item_expense" style="display: none;"> <i class="fa fa-close"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="col-md-11">
                                                <button type="button" class="btn btn-info btn-xs pull-right add_bom_item_expenses"> <i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><br />
                                    </div>
                                    <div class="clearfix"></div>
                                    <fieldset class="scheduler-border">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-8" style="padding: 0px 2px;">
                                                    <div class="form-group text-center">
                                                        <label for="" class="input-sm">Project</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3" style="padding: 0px 2px;">
                                                    <div class="form-group text-center">
                                                        <label for="" class="input-sm">Qty</label>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12">
                                                    <input type="hidden" name="project_index" id="project_index" />
                                                    <?php if(isset($bom_projects)){ ?>
                                                        <input type="hidden" name="bom_projects[bom_project_id]" id="bom_project_id" />
                                                    <?php } ?>
                                                        <input type="hidden" name="bom_projects[project_name]" id="project_name" />
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select name="bom_projects[project_id][]" id="project_id" class="form-control input-sm project_id" >
                                                                <option value="">- Select Project - </option>
                                                                <?php foreach ($projects as $project): ?>
                                                                    <option value="<?php echo $project->project_id; ?>"><?php echo $project->project_name; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="padding-left: 20px;">
                                                        <div class="form-group">
                                                            <input type="text" name="bom_projects[project_qty][]" id="project_qty" class="form-control input-sm project_qty num_only">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="padding-right: 10px;">
                                                        <button type="button" id="add_projects_line" class="btn btn-info btn-xs pull-right add_projects_line"> <i class="fa fa-plus"></i> Add Project</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div><br />  
                                            <div class="col-sm-12">
                                                <table style="" class="table custom-table item-table">
                                                    <thead>
                                                        <tr>
                                                            <th width="55px">Action</th>
                                                            <th width="90px">Sr. No.</th>
                                                            <th>Project</th>
                                                            <th class="text-right">Qty</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="project_lineitem_list"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="purchase_item_index" id="purchase_item_index" />
                                                    <?php if(isset($bom_item_details)){ ?>
                                                        <input type="hidden" name="bom_item_details[lineitem_id]" id="lineitem_id" />
                                                    <?php } ?>
                                                    <input type="hidden" name="bom_item_details[purchase_project_id]" id="purchase_project_id" />
                                                    <input type="hidden" name="bom_item_details[purchase_project_name]" id="purchase_project_name" />
                                                    <input type="hidden" name="bom_item_details[purchase_project_qty]" id="purchase_project_qty" />
                                                    <input type="hidden" name="bom_item_details[item_code]" id="purchase_item_code" />
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="purchase_item_id" class="input-sm">Purchase Items<span class="required-sign">&nbsp;*</span></label>
                                                            <select name="bom_item_details[purchase_item_id][]" id="purchase_item_id" class="form-control input-sm purchase_item_id" >
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label for="purchase_item_qty" class="input-sm">Qty<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="form-group">
                                                            <input type="text" name="bom_item_details[purchase_item_qty][]" id="purchase_item_qty" class="form-control input-sm purchase_item_qty num_only" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding-left: 20px;">
                                                        <label for="uom_name" class="input-sm">UOM<span class="required-sign">&nbsp;*</span></label>
                                                        <div class="form-group">
                                                            <select class="input-sm pull-left" name="bom_item_details[uom_id][]" id="uom_name"></select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding-left: 20px;">
                                                        <label for="uom_name" class="input-sm">Loss Qty</label>
                                                        <div class="form-group">
                                                            <input type="text" name="bom_item_details[loss][]" id="loss" class="form-control input-sm loss num_only" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding-left: 20px;">
                                                        <label for="uom_name" class="input-sm">Job Expense</label>
                                                        <div class="form-group">
                                                            <input type="text" name="bom_item_details[job_expense][]" id="job_expense" class="form-control input-sm job_expense num_only" >
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12">
                                                        <label for="" class="input-sm">&nbsp;</label>
                                                        <button type="button" id="add_purchase_item" class="btn btn-info btn-xs add_purchase_item pull-right"> <i class="fa fa-plus"></i> Add Item</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div> 
                                        </div>
                                    </fieldset>
                                    <br/><br/>
                                    <div class="col-sm-12">
                                        <table style="" class="table custom-table item-table">
                                            <thead>
                                                <tr>
                                                    <th width="55px">Action</th>
                                                    <th>Sr. No.</th>
                                                    <th>Name</th>
                                                    <th>Item Code</th>
                                                    <th class="text-right">Basic Qty</th>
                                                    <th class="text-center">UOM</th>
                                                    <th class="text-right">Loss Qty</th>
                                                    <th class="text-right">Job Expense</th>
                                                    <th>Project</th>
                                                    <th class="text-right">Project Qty</th>
                                                    <th class="text-right">Total Qty</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lineitem_list"></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-center"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th></th>
                                                    <th class="text-right">Total</th>
                                                    <th class="text-right"><span class="set_qty_total"></span><input type="hidden" name="set_qty_total" id="set_qty_total" /></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>										
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            
            <?php if ($add_role || $edit_role): ?>
                <button type="submit" class="btn btn-info btn-xs pull-right btn-save-item" id="btn_save_bom" style="margin: 5px;">Save </button>
            <?php endif; ?>
            <?php if ($view_role): ?>
                <a href="<?= base_url() ?>master/bom_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">BOM List</a>
            <?php endif; ?>
            <?php if ($add_role): ?>
                <a href="<?= base_url() ?>master/bom_add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add BOM</a>
            <?php endif; ?>
        </div>
    </form>
</div>
<script type="text/javascript">
    var lineitem_objectdata = [];
    var edit_lineitem_inc = 0;
    var project_item_group = '';
    var remove_all_project_iemts = '';
    <?php if (isset($bom_item_details)) { ?>
        var li_lineitem_objectdata = [<?php echo $bom_item_details; ?>];
        var lineitem_objectdata = [];
        if (li_lineitem_objectdata != '') {
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
    <?php } ?>
    display_lineitem_html(lineitem_objectdata);
    
    var project_lineitem_objectdata = [];
    var edit_project_lineitem_inc = 0;
    <?php if (isset($bom_projects)) { ?>
        var li_project_lineitem_objectdata = [<?php echo $bom_projects; ?>];
        var project_lineitem_objectdata = [];
        if (li_project_lineitem_objectdata != '') {
            $.each(li_project_lineitem_objectdata, function (index, value) {
                project_lineitem_objectdata.push(value);
            });
        }
    <?php } ?>
    display_project_lineitem_html(project_lineitem_objectdata);
    $(document).ready(function () {

        initAjaxSelect2($("#sales_item_id"), "<?= base_url('app/sales_item_select2_source') ?>");
        <?php if(isset($bom_data->sales_item_id)){ ?>
			setSelect2Value($("#sales_item_id"),"<?=base_url('app/set_sales_item_select2_val_by_id/'.$bom_data->sales_item_id)?>");
        <?php } ?>
            get_item_from_group();
            
        initAjaxSelect2($("#uom_name"),"<?=base_url('app/uom_select2_source')?>");
        <?php if(isset($reference_qty_uom)){ ?>
            setSelect2Value($("#uom_name"),"<?=base_url('app/set_uom_select2_val_by_id/'.$reference_qty_uom)?>");
        <?php } ?>

        $(document).on('change', '#sales_item_id', function () {
            var sales_item_id = $('#sales_item_id').val();
            if(sales_item_id != '' && sales_item_id != null){
                $.ajax({
                    url: "<?=base_url('app/get_item_code_by_id/') ?>/" + sales_item_id,
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    cache: false,
                    success: function (response) {
                        if(response.text == '--select--'){ response.text = ''; }
                        $('#item_code').val(response.text);
                    },
                });
                remove_all_project_iemts = '';
                removeByIndex(lineitem_objectdata);
                get_purchase_items(project_item_group);
                display_lineitem_html(lineitem_objectdata);
            } else {
                $('#item_code').val('');
                remove_all_project_iemts = 1;
                removeByIndex(lineitem_objectdata);
                display_lineitem_html(lineitem_objectdata);
            }
        });

        $(document).on('click', '.add_bom_item_expenses', function () {
            $("#item_expense_fields").clone().appendTo("#bom_item_expense_fields");
            $(".remove_bom_item_expense:last").css('display', 'block');
            $(".bom_item_expense_id:last, .item_expense:last,.expense:last").val('');
            $(".item_expense:last").focus();
        });

        $(document).on('click', '.remove_bom_item_expense', function () {
            $(this).closest("#item_expense_fields").remove();
            var bom_item_expense_id = $(this).closest("#item_expense_fields").find("#bom_item_expense_id").val();
            $('#save_bom').append('<input type="hidden" name="deleted_bom_item_expense_id[]" id="deleted_bom_item_expense_id" value="' + bom_item_expense_id + '" />');
        });

        $(document).on('change', '#purchase_item_id', function () {
            $('#purchase_item_qty').val('');
            $('#uom_name').val('').trigger('change');
            var purchase_item_id = $('#purchase_item_id').val();
            if(purchase_item_id != '' && purchase_item_id != null){
                $.ajax({
                    url: "<?=base_url('master/get_qty_and_uom_by_item') ?>/" + purchase_item_id,
                    type: "GET",
                    async: false,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        var json = $.parseJSON(response);
                        $('#purchase_item_qty').val(json.purchase_item_qty);
                        setSelect2Value($("#uom_name"),"<?=base_url('app/set_uom_select2_val_by_id')?>/" + json.uom_name);
                        $('#purchase_item_code').val(json.item_code);
                    },
                });
            } else {
                $('#purchase_item_qty').val('');
                $('#loss').val('');
                $('#job_expense').val('');
                $('#uom_name').val('').trigger('change');
            }
        });
        
        $(document).on('click', '#add_projects_line', function () {
            if ($.trim($("#project_id").val()) == '') {
                show_notify('Please Select Project.', false);
                $("#project_id").focus();
                return false;
            }
            if ($.trim($("#project_qty").val()) == '') {
                show_notify('Please Enter Project Qty.', false);
                $("#project_qty").focus();
                return false;
            }
            var project_index = $("#project_index").val();
            if (project_index != '') { } else {
                for (var index = 0; index < project_lineitem_objectdata.length; ++index) {
                    var project_data = project_lineitem_objectdata[index];
                    if(project_data.project_id == $.trim($("#project_id").val())){
                        show_notify('Project Already Exist.', false);
                        $("#project_id").focus();
                        return false;
                    }
                }
            }
            
            var project_lineitem = {};
            project_lineitem['bom_project_id'] = $("#bom_project_id").val();
            project_lineitem['project_id'] = $("#project_id").val();
            project_lineitem['project_name'] = $("#project_id option:selected").text();
            project_lineitem['project_qty'] = $("#project_qty").val();
            var new_project_lineitem = JSON.parse(JSON.stringify(project_lineitem));
            var project_index = $("#project_index").val();
            if (project_index != '') {
                project_lineitem_objectdata.splice(project_index, 1, new_project_lineitem);
                
                /* Update Project Qty in Item Table. */
                var pro_id = $("#project_id").val();
                var pro_qty = $("#project_qty").val();
                var lineitem_count = lineitem_objectdata.length;
//                console.log(lineitem_objectdata);
                if(lineitem_count != 0){
                    for (var index_il = 0; index_il < lineitem_count; ++index_il) {
                        var project_data = lineitem_objectdata[index_il];
                        if(project_data.project_id === pro_id){
//                            alert(project_data.project_id +'=='+ pro_id + ' -- ' + index_il + ' --  ' + lineitem_objectdata[index_il].project_qty + ' == ' + pro_qty);
                            lineitem_objectdata[index_il].project_qty = pro_qty;
                            lineitem_objectdata[index_il].purchase_project_qty = pro_qty;
                            lineitem_objectdata[index_il].quantity = lineitem_objectdata[index_il].set_qty * pro_qty;
                        }
                    }
                }
                
            } else {
                project_lineitem_objectdata.push(new_project_lineitem);
                
                var pro_id = $("#project_id").val();
                var pro_qty = $("#project_qty").val();
                get_purchase_project_items(pro_id, pro_qty);
            }
            
            display_project_lineitem_html(project_lineitem_objectdata);
            display_lineitem_html(lineitem_objectdata);
            
            $('#bom_project_id').val('');
            $('#project_id').val('');
            $("#project_id").val(null).trigger("change");
            $("#project_id").removeAttr('disabled', 'disabled');
            $("#project_name").val('');
            $("#project_qty").val('');
            $("#project_index").val('');
            edit_project_lineitem_inc = 0;
        });
   
        $(document).on('click', '#add_purchase_item', function () {
            if ($.trim($("#purchase_item_id").val()) == '') {
                show_notify('Please Select Purchase Item.', false);
                $("#purchase_item_id").focus();
                return false;
            }
            if ($.trim($("#purchase_item_qty").val()) == '') {
                show_notify('Please Enter Purchase Item Qty.', false);
                $("#purchase_item_qty").focus();
                return false;
            }
             if($.trim($("#uom_name").val()) == '') {
                show_notify('Please select UOM', false);
                $("#uom_name").focus();
                return false;
            }
            var lineitem = {};
            lineitem['lineitem_id'] = $("#lineitem_id").val();
            lineitem['item_id'] = $("#purchase_item_id").val();
            lineitem['item_name'] = $("#purchase_item_id option:selected").text();
            lineitem['item_code'] = $("#purchase_item_code").val();
            lineitem['item_group'] = project_item_group;
            var uom_name = $('#uom_name').select2('data');
            lineitem['uom_id'] = $('#uom_name').val();
            lineitem['uom_name'] = uom_name[0].text;
            lineitem['project_id'] = $("#purchase_project_id").val();
            lineitem['project_name'] = $("#purchase_project_name").val();
            lineitem['loss'] = $("#loss").val();
            lineitem['job_expense'] = $("#job_expense").val();
            
            var purchase_project_qty = $("#purchase_project_qty").val();
            lineitem['purchase_project_qty'] = purchase_project_qty;
            lineitem['set_qty'] = $.trim($("#purchase_item_qty").val());
            
            var purchase_item_qty = $.trim($("#purchase_item_qty").val());
            if(purchase_project_qty == '' || purchase_project_qty == 0 || purchase_project_qty == null){ purchase_project_qty = 1; }
            purchase_item_qty = purchase_item_qty * purchase_project_qty;
            lineitem['quantity'] = purchase_item_qty;
            var new_lineitem = JSON.parse(JSON.stringify(lineitem));
            var purchase_item_index = $("#purchase_item_index").val();
            if (purchase_item_index != '') {
                lineitem_objectdata.splice(purchase_item_index, 1, new_lineitem);
            } else {
                lineitem_objectdata.push(new_lineitem);
            }
            display_lineitem_html(lineitem_objectdata);
            $('#lineitem_id').val('');
            $("#purchase_item_id").val(null).trigger("change");
            $("#purchase_project_id").val('');
            $("#purchase_project_name").val('');
            $("#purchase_project_qty").val('');
            $("#purchase_item_qty").val('');
            $("#purchase_item_index").val('');
            $("#loss").val('');
            $("#job_expense").val('');
            edit_lineitem_inc = 0;
        });

        $(document).on('submit', '#save_bom', function () {
            if ($.trim($("#sales_item_id").val()) == '') {
                show_notify('Please Select Sales Item.', false);
                $("#sales_item_id").focus();
                return false;
            }
            if ($.trim($("#effective_date").val()) == '') {
                show_notify('Please Enter Effective Date.', false);
                $("#effective_date").focus();
                return false;
            }
            if (lineitem_objectdata == '') {
                show_notify("Please select any one Purchase Item.", false);
                return false;
            }

            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
            
            var project_lineitem_objectdata_stringify = JSON.stringify(project_lineitem_objectdata);
            postData.append('project_line_items_data', project_lineitem_objectdata_stringify);
            
            var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
            postData.append('line_items_data', lineitem_objectdata_stringify);
            $.ajax({
                url: "<?= base_url('master/save_bom') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.status == 0) {
                        show_notify(json.msg, false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                        return false;
                    }
                    if (json['success'] == 'false') {
                        show_notify(json['msg'], false);
                        $('.module_save_btn').removeAttr('disabled', 'disabled');
                    }
                    if (json['success'] == 'Added') {
                        window.location.href = "<?php echo base_url('master/bom_list') ?>";
                    }
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('master/bom_list') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });
        
    });
    
    $(document).on('change', '.pts_loss', function () {
        var current_los = $(this).val();
        var pts_selected_index = $(this).data('pts_selected_index');
        lineitem_objectdata[pts_selected_index].loss = current_los;
    });
    
    $(document).on('change', '.pts_job_expense', function () {
        var current_job_expense = $(this).val();
        var pts_selected_index = $(this).data('pts_selected_index');
        lineitem_objectdata[pts_selected_index].job_expense = current_job_expense;
    });

    function display_project_lineitem_html(project_lineitem_objectdata) {
        $('#ajax-loader').show();
        var new_project_lineitem_html = '';
        var tr_inc = 1;
        $.each(project_lineitem_objectdata, function (index, value) {
            var project_lineitem_edit_btn = '';
            var project_lineitem_delete_btn = '';
            project_lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_project_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_project_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
            project_lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_project_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
            var row_html = '<tr class="project_lineitem_index_' + index + '"><td class="">' +
                    project_lineitem_edit_btn +
                    project_lineitem_delete_btn +
                    '</td>' +
                    '<td>' + tr_inc + '</td>' +
                    '<td>' + value.project_name + '</td>' +
                    '<td class="text-right">' + value.project_qty + '</td>';
            new_project_lineitem_html += row_html;
            tr_inc++;
        });
        $('tbody#project_lineitem_list').html(new_project_lineitem_html);
        $('#ajax-loader').hide();
    }
    
    function edit_project_lineitem(index) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $('#ajax-loader').show();
        if (edit_project_lineitem_inc == 0) {
            edit_project_lineitem_inc = 1;
            $(".add_project_lineitem").removeAttr("disabled");
        }
        value = project_lineitem_objectdata[index];
        if(typeof(value.bom_project_id) != "undefined" && value.bom_project_id !== null) {
			$("#bom_project_id").val(value.bom_project_id);
		}
        $("#project_index").val(index);
        $("#project_id").val(value.project_id).trigger("change");
        $("#project_id").attr('disabled', 'disabled');
        $("#project_name").val(value.project_name);
        $("#project_qty").val(value.project_qty);
        $("#loss").val(value.loss);
        $("#job_expense").val(value.job_expense);
        $("#uom_name").val(value.uom_id).trigger("change");
        setSelect2Value($("#uom_name"),"<?=base_url('app/set_uom_select2_val_by_id')?>/" + value.uom_id);
        $('#ajax-loader').hide();
    }

    function remove_project_lineitem(index) {
        if (confirm('Are you sure ?')) {
            value = project_lineitem_objectdata[index];
            if (typeof (value.bom_project_id) != "undefined" && value.bom_project_id !== null) {
                $('#save_bom').append('<input type="hidden" name="deleted_bom_project_id[]" id="deleted_bom_project_id" value="' + value.bom_project_id + '" />');
            }
            project_lineitem_objectdata.splice(index, 1);
            display_project_lineitem_html(project_lineitem_objectdata);
            
            /* Removed Project Item form Item Table. */
            var pro_id = value.project_id;
            var lineitem_count = lineitem_objectdata.length;
//            console.log(lineitem_objectdata);
            var remove_index = [];
            if(lineitem_count != 0){
                for (var index_il = 0; index_il < lineitem_count; ++index_il) {
                    var project_data = lineitem_objectdata[index_il];
                    if(project_data.project_id == pro_id){
                        remove_index.push(index_il);
//                        alert(project_data.project_id +'=='+ pro_id + ' -- ' + index_il);
                    }
                }
            }
            for (var i = remove_index.length -1; i >= 0; i--){
                value = lineitem_objectdata[remove_index[i]];
                if (typeof (value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
                    $('#save_bom').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.lineitem_id + '" />');
                }
                lineitem_objectdata.splice(remove_index[i],1);
            }
            display_lineitem_html(lineitem_objectdata);
            
        }
    }

    function display_lineitem_html(lineitem_objectdata) { 
        $('#ajax-loader').show();
        var new_lineitem_html = '';
        var set_qty_total = 0;
        var set_basic_qty_total = 0;
        var tr_inc = 1;
        $.each(lineitem_objectdata, function (index, value) {
            var value_uom_name = '';
            if(typeof(value.uom_name) != "undefined" && value.uom_name !== null) {
                value_uom_name = value.uom_name;
            } else {
                $.ajax({
                    url: "<?=base_url('app/set_uom_select2_val_by_id/') ?>/" + value.uom_id,
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    cache: false,
                    success: function (response) {
                        if(response.text == '--select--'){ response.text = ''; }
                        value_uom_name = response.text;
                    },
                });
            }
            if(value.project_name != '' && value.project_name != null){
                value.project_name = value.project_name;
            } else {
                value.project_name = '';
            }
            var lineitem_edit_btn = '';
            var lineitem_delete_btn = '';
            lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
            lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
            var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
                    lineitem_edit_btn +
                    lineitem_delete_btn +
                    '</td>' +
                    '<td>' + tr_inc + '</td>' + 
                    '<td>' + value.item_name + '</td>';
                if(typeof(value.item_code) != "undefined" && value.item_code !== null) {
                    row_html += '<td>' + value.item_code + '</td>';
                } else {
                    row_html += '<td></td>';
                }
                    row_html += '<td class="text-right">' + value.set_qty + '</td>' +
                    '<td class="text-center">' + value_uom_name + '</td>' +
                    '<td class="text-right"><input type="text" id="loss_' + index + '" data-pts_selected_index="' + index + '" class="pts_loss text-right" value="'+ value.loss + '" style="width:100px;"></td>' +
                    '<td class="text-right"><input type="text" id="pts_job_expense_' + index + '" data-pts_selected_index="' + index + '" class="pts_job_expense text-right" value="'+ value.job_expense + '" style="width:100px;"></td>' + 
                    '<td>' + value.project_name + '</td>';
                if(typeof(value.purchase_project_qty) != "undefined" && value.purchase_project_qty !== null) {
                    row_html += '<td class="text-right">' + value.purchase_project_qty + '</td>';
                } else {
                    row_html += '<td class="text-right"></td>';
                }
                row_html += '<td class="text-right">' + value.quantity + '</td>';
            new_lineitem_html += row_html;
            set_basic_qty_total += +value.set_qty;
            set_qty_total += +value.quantity;
            tr_inc++;
        });
        $('tbody#lineitem_list').html(new_lineitem_html);
        $('tfoot span.set_basic_qty_total').html(set_basic_qty_total);
        $('#set_basic_qty_total').val(set_basic_qty_total);
        $('tfoot span.set_qty_total').html(set_qty_total);
        $('#set_qty_total').val(set_qty_total);
        $('#ajax-loader').hide();
    }
    
    function edit_lineitem(index) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $('#ajax-loader').show();
        if (edit_lineitem_inc == 0) {
            edit_lineitem_inc = 1;
            $(".add_lineitem").removeAttr("disabled");
        }
        value = lineitem_objectdata[index];
        console.log(value);
        if(typeof(value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
			$("#lineitem_id").val(value.lineitem_id);
		}
        $("#purchase_item_index").val(index);
        setSelect2Value($("#purchase_item_id"),"<?=base_url('app/set_purchase_item_select2_val_by_id')?>/" + value.item_id);
        get_item_from_group();
        $("#purchase_item_id").val(value.item_id).trigger("change");
        $("#purchase_project_id").val(value.project_id);
        $("#purchase_project_name").val(value.project_name);
        $("#purchase_project_qty").val(value.purchase_project_qty);
        $("#purchase_item_qty").val(value.set_qty);
        $("#loss").val(value.loss);
        $("#job_expense").val(value.job_expense);
        $("#uom_name").val(value.uom_id).trigger("change");
        setSelect2Value($("#uom_name"),"<?=base_url('app/set_uom_select2_val_by_id')?>/" + value.uom_id);
        $('#ajax-loader').hide();
    }
    
    function remove_lineitem(index) {
        if (confirm('Are you sure ?')) {
            value = lineitem_objectdata[index];
            if (typeof (value.lineitem_id) != "undefined" && value.lineitem_id !== null) {
                $('#save_bom').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.lineitem_id + '" />');
            }
            lineitem_objectdata.splice(index, 1);
            display_lineitem_html(lineitem_objectdata);
        }
    }

    function get_purchase_project_items(project_id, project_qty) {
        if (project_id != '' && project_id != null) {
            $.ajax({
                type: "POST",
                async: false,
                url: '<?= base_url(); ?>master/get_purchase_project_items/' + project_id + '/' + project_qty,
                data: project_id = 'project_id',
                success: function (data) {
                    edit_lineitem_inc = 0;
//                    console.log(data);
                    var data = $.parseJSON(data);
                    var li_lineitem_objectdata = data;
                    if (li_lineitem_objectdata != '') {
                        $.each(li_lineitem_objectdata, function (index, value) {
                            value.item_id = parseInt(value.item_id);
                            lineitem_objectdata.push(value);
                        });
                    }
                }
            });
        }
    }
    
    function get_item_from_group(){
        var group_id = $('#sales_item_id').val();
        $("#purchase_item_id").val('').trigger("change");
        if (group_id != '' && group_id != null) {
            $.ajax({
                type: "GET",
                async: false,
                url: '<?= base_url(); ?>master/get_purchase_items/' + group_id,
                success: function (data) {
                    var data = $.parseJSON(data);
                    initAjaxSelect2($("#purchase_item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source')?>/" + data);
                    project_item_group = data;
                }
            });
        } else {
            $("#purchase_item_id").val('').trigger("change");
            initAjaxSelect2($("#purchase_item_id"),"<?=base_url('app/purchase_item_group_wise_select2_source')?>");
        }
    }
    
    function get_purchase_items(item_group) {
        if (item_group != '' && item_group != null) {
            $.ajax({
                type: "POST",
                async: false,
                url: '<?= base_url(); ?>master/get_purchase_items_lineitems/' + item_group,
                success: function (data) {
                    edit_lineitem_inc = 0;
//                    console.log(data);
                    var data = $.parseJSON(data);
                    var li_lineitem_objectdata = data;
                    if (li_lineitem_objectdata != '') {
                        $.each(li_lineitem_objectdata, function (index, value) {
                            value.item_id = parseInt(value.item_id);
                            lineitem_objectdata.push(value);
                        });
                    }
                }
            });
        }
    }
    
    function removeByIndex(lineitem_objectdata) {
        if(project_item_group != '' || remove_all_project_iemts == '1'){
            if(remove_all_project_iemts == '1'){
                project_item_group = 0;
            } 
            var remove_arr = [];
            $.each(lineitem_objectdata, function (lio_index, lio_value) {
                var value_item_group = lio_value.item_group;
                if(typeof(value_item_group) !== "undefined" && value_item_group !== null && value_item_group != project_item_group) {
                    remove_arr.push(lio_index);
                    if (typeof (lio_value.lineitem_id) != "undefined" && lio_value.lineitem_id !== null) {
                        $('#save_bom').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + lio_value.lineitem_id + '" />');
                    }
                }
            });
            if (remove_arr.length != 0) {
                var remove_arr_inc = 0;
                $.each(remove_arr, function (index, value) {
                    var remove_index = value - remove_arr_inc;
                    lineitem_objectdata.splice(remove_index, 1);
                    remove_arr_inc++;
                });
            }
        }
    }
</script>