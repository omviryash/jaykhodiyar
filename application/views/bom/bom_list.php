<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">BOM List</small>
            <?php if ($this->applib->have_access_role(MASTER_BOM_MENU_ID, "add")) { ?>
                <a href="<?= base_url() ?>master/bom_add" class="btn btn-info btn-xs pull-right">Add BOM</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin: 10px;">
                                <table id="example1" class="table display custom-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="80px">Action</th>
                                            <th>Sales Item Name</th>
                                            <th>Item Code</th>
                                            <th>Effective Date</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#example1').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('master/bom_master_datatable') ?>",
                "type": "POST"
            },
            "scrollY": 550,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });

    });
</script>
