<!-- Main content -->
<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

		<!-- Advanced login -->
		<?php echo form_open($this->uri->uri_string()); ?>
		<div class="panel panel-body login-form">
			<div class="text-center">
				<div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
				<h5 class="content-group">Reset Password <small class="display-block">We'll send you instructions in email</small></h5>
			</div>

			<div class="form-group has-feedback has-feedback-left">
				<input type="text" class="form-control" placeholder="New Password" name="new_password" id="new_password">
				<div class="form-control-feedback">
					<i class="icon-lock2 text-muted"></i>
				</div>
				<label id="new_password-error" class="validation-error-label" for="new_password"><?php echo isset($errors['new_password'])?$errors['new_password']:''; ?></label>
			</div>

			<div class="form-group has-feedback has-feedback-left">
				<input type="text" class="form-control" placeholder="Repeat Password" name="confirm_new_password">
				<div class="form-control-feedback">
					<i class="icon-lock2 text-muted"></i>
				</div>
				<label id="confirm_new_password-error" class="validation-error-label" for="confirm_new_password"><?php echo isset($errors['confirm_new_password'])?$errors['confirm_new_password']:''; ?></label>
			</div>

			<div class="form-group">
				<button type="submit" class="btn bg-blue btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</div>
		<?php echo form_close(); ?>
		<!-- /advanced login -->

	</div>
	<!-- /content area -->

</div>
<!-- /main content -->
