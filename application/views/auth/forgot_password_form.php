<!-- Main content -->
<div class="content-wrapper">
	<!-- Content area -->
	<div class="content">
		<!-- Password recovery -->
		<form action="<?=base_url()?>auth/forgot_password/" method="post">
			<div class="panel panel-body login-form">
				<div class="text-center">
					<div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
					<h5 class="content-group">Password recovery <small class="display-block">We'll send you instructions in email</small></h5>
				</div>

				<div class="form-group has-feedback">
					<input type="email" class="form-control" placeholder="Your email" name="email" id="email" value="<?=set_value('email');?>">
					<div class="form-control-feedback">
						<i class="icon-mail5 text-muted"></i>
					</div>
					<label id="username-error" class="validation-error-label" for="username"><?php echo isset($errors['email'])?$errors['email']:''; ?></label>
				</div>

				<button type="submit" class="btn bg-blue btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</form>
		<!-- /password recovery -->
	</div>
	<!-- /content area -->
</div>
<!-- /main content -->