<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Unsubscribe | Jay Khodiyar</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?=bootstrap_url('css/bootstrap.min.css');?>">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=dist_url('css/AdminLTE.min.css');?>">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="<?=base_url();?>"><b>Jay</b> Khodiyar</a>
	</div>
	<!-- /.login-logo -->
	<div class="login-box-body <?php echo $bg_class; ?> text-center">
        <h4><?php echo $dis_message; ?></h4>
	</div>
	<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?=plugins_url('jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=bootstrap_url('js/bootstrap.min.js');?>"></script>
</body>
</html>
