<!-- Main content -->
<div class="content-wrapper">
	<!-- Content area -->
	<div class="content">
		<!-- Advanced login -->
		<?=form_open($this->uri->uri_string());?>
			<div class="panel panel-body login-form">
				<div class="text-center">
					<h5 class="content-group">Create account <small class="display-block">All fields are required</small></h5>
				</div>

				<div class="content-divider text-muted form-group"><span>Your credentials</span></div>

				<div class="form-group has-feedback has-feedback-left">
					<input type="text" class="form-control" placeholder="Username" name="username" id="username" value="<?=set_value('username')?>">
					<div class="form-control-feedback">
						<i class="icon-user-check text-muted"></i>
					</div>
					<label id="username-error" class="validation-error-label" for="username"><?=isset($errors['username'])?$errors['username']:''?></label>
				</div>

				<div class="form-group has-feedback has-feedback-left">
					<input type="text" class="form-control" placeholder="Your email"  name="email" id="email" value="<?=set_value('email')?>">
					<div class="form-control-feedback">
						<i class="icon-mention text-muted"></i>
					</div>
					<label id="email-error" class="validation-error-label" for="email"><?=isset($errors['email'])?$errors['email']:''?></label>
				</div>

				<div class="content-divider text-muted form-group"><span>Your privacy</span></div>

				<div class="form-group has-feedback has-feedback-left">
					<input type="text" class="form-control" placeholder="Create password"  name="password" id="password" value="<?=set_value('password')?>">
					<div class="form-control-feedback">
						<i class="icon-user-lock text-muted"></i>
					</div>
					<label id="password-error" class="validation-error-label" for="password"><?=isset($errors['password'])?$errors['password']:''?></label>
				</div>

				<div class="form-group has-feedback has-feedback-left">
					<input type="text" class="form-control" placeholder="Repeat password"  name="confirm_password" id="confirm_password" value="<?=set_value('confirm_password')?>">
					<div class="form-control-feedback">
						<i class="icon-user-lock text-muted"></i>
					</div>
					<label id="confirm_password-error" class="validation-error-label" for="confirm_password"><?=isset($errors['confirm_password'])?$errors['confirm_password']:''?></label>
				</div>

				<button type="submit" class="btn bg-teal btn-block btn-lg">Register <i class="icon-circle-right2 position-right"></i></button>
			</div>
		<?=form_close();?>
		<!-- /advanced login -->

	</div>
	<!-- /content area -->

</div>
<!-- /main content -->