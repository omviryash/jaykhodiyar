<?php
    $segment1 = $this->uri->segment(1);
    $segment2 = $this->uri->segment(2);
    $segment3 = $this->uri->segment(3);
?>
<style>
    .treeview-menu {
        position: absolute;
        top: inherit;
        left: 230px;
        z-index: 1000;
        display: inline-block;
        text-overflow: clip;
        float: left;
        width: 240px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        text-overflow: clip;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }
    .treeview-menu {
        background: #e8e8e8;
        top: 0px;
    }
    .treeview-menu .treeview-menu {
        top: inherit;
        margin-top: -30px;
        padding-left: -20px;
        text-overflow: clip;
        /*width: 260px;*/
    }
    .sidebar-menu > li:hover > .treeview-menu {
        display: block !important;
        position: absolute;
    }
    .sidebar-menu .treeview-menu>li:hover>.treeview-menu {
        display: block !important;
        position: absolute;
    }
    @media (min-width: 768px){
        .sidebar-mini.sidebar-collapse .sidebar-menu > li:hover > a > span:not(.pull-right), .sidebar-mini.sidebar-collapse .sidebar-menu > li:hover > .treeview-menu {
            display: block !important;
            position: absolute;
            width: 240px;
            left: 50px;
        }
        .sidebar-mini.sidebar-collapse .sidebar-menu > li:hover > a > .pull-right-container {
            float: right;
            width: auto!important;
            left: 260px!important;
            top: 10px!important;
        }
    }
    .dropdown-menu li, .dropdown-menu li a {
        white-space: normal;
        /*float: left;*/
        width: 100%;
        height: auto;
        word-wrap: break-word;
    }
/*    .dropdown-menu > li > a {
        width: 100%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: clip;
    }
    .sidebar-menu .treeview-menu > li > a {
        padding: 5px 5px 5px 15px;
        display: block;
        font-size: 14px;
    }
    .treeview-menu > li:hover > a {
        z-index : 1;
        width   : auto;
        overflow: visible;
    }*/
/*    li > a:hover {
        width: auto;
        max-width: 340px;
        overflow: visible;
    }*/
    
</style>
<!-- Main sidebar -->
<aside class="main-sidebar" style="height: 100%; top: -50px;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="sidebar-logo">
                <a href="<?= base_url(); ?>">
                    <?php if(isset($this->session->userdata()['sidebar_logo'])){ ?>
                        <img src="<?php echo base_url() . image_dir($this->session->userdata()['sidebar_logo']); ?>" class="img-sidebar-logo" alt="JayKhodiyar">
                    <?php } else { ?>
                        <img src="<?= base_url('resource/image/jk-sidebar-logo.png') ?>" class="img-sidebar-logo" alt="JayKhodiyar">
                    <?php } ?>
                </a>
            </div>
<!--            <div class="pull-left image">
                <img src="<?/*=isset($image) && file_exists(image_dir('staff/'.$image))?image_url('staff/'.$image):dist_url('img/user2-160x160.jpg');*/?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?/*=$name?$name:'Admin';*/?></p>
                <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
            </div>-->
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu ">
            <!-- <li class="header">MAIN NAVIGATION</li>-->
            <?php if ($this->applib->have_access_role(DASHBOARD_MENU_ID, "view")) { ?>
                <li <?= $segment1 == '' ? 'class="active"' : ''; ?>>
                    <a href="<?= base_url(); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(MASTER_MENU_ID, "view")) { ?>
            <li class="treeview <?= ($segment1 == 'master_hr' || $segment1 == 'master_challan' || $segment2 == 'day' || $segment2 == 'grade' || $segment2 == 'department' || $segment2 == 'payment_mode') || $segment1 == 'master' && ($segment2 == 'company_detail' || $segment2 == 'edit-billing-terms' || $segment2 == 'billing-terms' || $segment2 == 'billing_terms_list' || $segment2 == 'agent') || $segment1 == 'master_purchase' && ($segment2 == 'order_terms') || $segment1 == 'item' || $segment1 == 'master_sales' || $segment1 == 'terms_condition' || $segment1 == 'general_master' || $segment1 == 'setting' || $segment1 == 'reminder' || ( $segment1 == 'party' && $segment2 == 'country' || $segment2 == 'state' || $segment2 == 'city' || $segment2 == 'reference' || $segment2 == 'designation' || $segment2 == 'terms_group' || $segment2 == 'company_banks' || $segment2 == 'sms_topic' || $segment2 == 'bom_add' || $segment2 == 'bom_list' || $segment1 == 'letterpad_content') ? 'active' : '' ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-circle"></i> <span>Master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php if ($this->applib->have_access_role(COMPANY_DETAIL_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'master' && $segment2 == 'company_detail' ? 'active' : '' ?>"><a href="<?= base_url() ?>master/company_detail/"><i class="fa fa-circle-o"></i> Company Detail</a></li>
                            <li class="<?= $segment1 == 'master' && $segment2 == 'settings' ? 'active' : '' ?>"><a href="<?= base_url() ?>master/settings/"><i class="fa fa-circle-o"></i> Settings</a></li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_COMPANY_BANKS_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'party' && $segment2 == 'company_banks' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/company_banks/"><i class="fa fa-circle-o"></i> Company Banks</a></li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_ITEM_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'item' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Item
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_MASTER_MENU_ID, "view")) { ?>
                                        <li class="<?= ($segment1 == 'item' && $segment2 == 'item_master' || $segment1 == 'item' && $segment2 == 'add_item_master') ? 'active' : '' ?>"><a href="<?= base_url() ?>item/item_master/"><i class="fa fa-circle-o"></i> Sales Item</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_PURCHASE_ITEM_MENU_ID, "view")) { ?>
                                        <li class="<?= ($segment1 == 'item' && $segment2 == 'purchase_item' || $segment1 == 'item' && $segment2 == 'add_purchase_item') ? 'active' : '' ?>"><a href="<?= base_url() ?>item/purchase_item/"><i class="fa fa-circle-o"></i> Purchase Item</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_PURCHASE_PROJECT_MENU_ID, "view")) { ?>
                                        <li class="<?= ($segment1 == 'item' && $segment2 == 'purchase_project' || $segment1 == 'item' && $segment2 == 'add_purchase_project') ? 'active' : '' ?>"><a href="<?= base_url() ?>item/purchase_project/"><i class="fa fa-circle-o"></i> Purchase Project</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_CATEGORY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'item_category' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/item_category/"><i class="fa fa-circle-o"></i> Item Category</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_GROUP_CODE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'item_group_code' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/item_group_code/"><i class="fa fa-circle-o"></i> Item Group Code</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SUPPLIER_MAKE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'supplier_make' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/supplier_make/"><i class="fa fa-circle-o"></i> Supplier Make</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_RAW_MATERIAL_GROUP_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'raw_material_group' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/raw_material_group/"><i class="fa fa-circle-o"></i> Raw Material Group</a></li>
                                    <?php } ?>
                                    <?php /* if($this->applib->have_access_role(MASTER_PRODUCT_GROUP_MENU_ID,"view")) { ?>
                                      <li class="<?=$segment1=='item' && $segment2 == 'product_group'?'active':''?>"><a href="<?=base_url()?>item/product_group/"><i class="fa fa-circle-o"></i> Product Group</a></li>
                                      <?php } */ ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_TYPE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'item_type' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/item_type/"><i class="fa fa-circle-o"></i> Item Type</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_STATUS_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'item_status' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/item_status/"><i class="fa fa-circle-o"></i> Item Status</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_MATERIAL_PROCESS_TYPE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'material_process_type' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/material_process_type/"><i class="fa fa-circle-o"></i> Material Process Type</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_MAIN_GROUP_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'main_group' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/main_group/"><i class="fa fa-circle-o"></i> Main Group</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_SUB_GROUP_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'sub_group' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/sub_group/"><i class="fa fa-circle-o"></i> Sub Group</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_MAKE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'make' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/make/"><i class="fa fa-circle-o"></i> Make</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_DRAWING_NUMBER_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'drawing_number' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/drawing_number/"><i class="fa fa-circle-o"></i> Drawing Number</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_MATERIAL_SPECIFICATION_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'material_specification' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/material_specification/"><i class="fa fa-circle-o"></i> Material Specification</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_CLASS_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'item_class' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/item_class/"><i class="fa fa-circle-o"></i> Class</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_ITEM_EXCISE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'item' && $segment2 == 'excise' ? 'active' : '' ?>"><a href="<?= base_url() ?>item/excise/"><i class="fa fa-circle-o"></i> Excise</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>

                        <?php if ($this->applib->have_access_role(MASTER_BOM_MENU_ID, "view")) { ?>
                            <li class="<?= ($segment1 == 'master' && $segment2 == 'bom_add') || ($segment1 == 'master' && $segment2 == 'bom_list') ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> BOM
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <li class="<?= ($segment1 == 'master' && $segment2 == 'bom_add') ? 'active' : '' ?>"><a href="<?= base_url() ?>master/bom_add/"><i class="fa fa-circle-o"></i> Add BOM</a></li>
                                    <li class="<?= ($segment1 == 'master' && $segment2 == 'bom_list') ? 'active' : '' ?>"><a href="<?= base_url() ?>master/bom_list/"><i class="fa fa-circle-o"></i> BOM List</a></li>
                                </ul>
                            </li>
                        <?php } ?>

                        <?php if ($this->applib->have_access_role(MASTER_AGENT_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'master' && $segment2 == 'agent' ? 'active' : '' ?>"><a href="<?= base_url() ?>master/agent/"><i class="fa fa-circle-o"></i> Agent</a></li>
                        <?php } ?>

                        <?php if ($this->applib->have_access_role(MASTER_SALES_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'master_sales' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Sales
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_REFERENCE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'reference' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>master_sales/reference/"><i class="fa fa-circle-o"></i> Reference</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_INQUIRY_STAGE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'inquiry_stage' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>master_sales/inquiry_stage/"><i class="fa fa-circle-o"></i> Enquiry Stage</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_INDUSTRY_TYPE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'industry_type' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>master_sales/industry_type/"><i class="fa fa-circle-o"></i> Industry Type</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_INQUIRY_STATUS_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'inquiry_status' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/inquiry_status/"><i class="fa fa-circle-o"></i> Enquiry Status</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_PRIORITY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'priority' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/priority/"><i class="fa fa-circle-o"></i> Priority</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_UOM_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'uom' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/uom/"><i class="fa fa-circle-o"></i> UOM</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_QUOTATION_TYPE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'quotation_type' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/quotation_type/"><i class="fa fa-circle-o"></i> Quotation Type</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_SALES_TYPE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'sales_type' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/sales_type/"><i class="fa fa-circle-o"></i> Sales Type</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_QUOTATION_REASON_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'quotation_reason' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/quotation_reason/"><i class="fa fa-circle-o"></i>Reason</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_CURRENCY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'currency' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/currency/"><i class="fa fa-circle-o"></i> Currency</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_SALES_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'sales' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/sales/"><i class="fa fa-circle-o"></i> Sales</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_ACTION_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'action' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/action/"><i class="fa fa-circle-o"></i> Action</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_QUOTATION_STAGE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'stage' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/stage/"><i class="fa fa-circle-o"></i>Quot Stage</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_STATUS_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'status' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/status/"><i class="fa fa-circle-o"></i> Status</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_INQUIRY_CONFIRMATION_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'inquiry_confirmation' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/inquiry_confirmation/"><i class="fa fa-circle-o"></i> Enquiry Confirmation</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_ORDER_STAGE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'order_stage' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/order_stage/"><i class="fa fa-circle-o"></i> Order Stage</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_FOLLOWUP_CATEGORY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'followup_category' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/followup_category/"><i class="fa fa-circle-o"></i> Followup Category</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_PURCHASE_ORDER_REF_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'purchase_order_ref' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/purchase_order_ref/"><i class="fa fa-circle-o"></i> Purchase Order Ref</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_SALES_QUOTATION_STATUS_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'quotation_status' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/quotation_status/"><i class="fa fa-circle-o"></i> Quotation Status</a></li>
                                    <?php } ?>

                                    <?php if ($this->applib->have_access_role(MASTER_SALES_SEA_FREIGHT_TYPE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_sales' && $segment2 == 'sea_freight_type' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_sales/sea_freight_type/"><i class="fa fa-circle-o"></i> Sea Freight Type</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_CHALLAN_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'master_challan' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Challan
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_USER, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'user' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/user') ?>"><i class="fa fa-circle-o"></i>Challan Item User</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_MAKE, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'make' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/make') ?>"><i class="fa fa-circle-o"></i>Challan Item Make</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_HP, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'hp' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/hp') ?>"><i class="fa fa-circle-o"></i>Challan Item HP</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_KW, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'kw' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/kw') ?>"><i class="fa fa-circle-o"></i>Challan Item KW</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_FREQUENCY, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'frequency' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/frequency') ?>"><i class="fa fa-circle-o"></i>Challan Item Frequency</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_RPM, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'rpm' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/rpm') ?>"><i class="fa fa-circle-o"></i>Challan Item RPM</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_VOLTS, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'volts' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/volts') ?>"><i class="fa fa-circle-o"></i>Challan Item Volts</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_GEAR_TYPE, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'gear' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/gear') ?>"><i class="fa fa-circle-o"></i>Challan Item Gear Type</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_MODEL, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'model' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/model') ?>"><i class="fa fa-circle-o"></i>Challan Item Model</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_ITEM_RATIO, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'ratio' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/ratio') ?>"><i class="fa fa-circle-o"></i>Challan Item Ratio</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_CHALLAN_DEFAULT_DETAILS, "view") || $this->applib->have_access_role(MASTER_CHALLAN_DEFAULT_DETAILS, "edit")) { ?>
                                        <li class="<?= $segment1 == 'master_challan' && $segment2 == 'default_challan_detail' ? 'active' : '' ?>"><a href="<?= base_url('master_challan/default_challan_detail') ?>"><i class="fa fa-circle-o"></i>Default Challan Details</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_HR_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'master_hr' || $segment1 == 'master-hr' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Hr
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(MASTER_HR_DAY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_hr' && $segment2 == 'day' ? 'active' : '' ?>"><a href="<?= base_url('master_hr/day') ?>"><i class="fa fa-circle-o"></i> Days</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_HR_GRADE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_hr' && $segment2 == 'grade' ? 'active' : '' ?>"><a href="<?= base_url('master_hr/grade') ?>"><i class="fa fa-circle-o"></i> Grade</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_HR_DEPARTMENT_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_hr' && $segment2 == 'department' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_hr/department/"><i class="fa fa-circle-o"></i> Department</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_HR_EMPLOYEE_WISE_INCENTIVE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_hr' && $segment2 == 'employee_wise_incentive' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_hr/employee_wise_incentive/"><i class="fa fa-circle-o"></i> Employee Wise Incentive</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_PAYMENT_MODE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'master_hr' && $segment2 == 'payment_mode' ? 'active' : '' ?>"><a href="<?= base_url() ?>master_hr/payment_mode/"><i class="fa fa-circle-o"></i> Payment Mode</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_TERMS_CONDITION_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'terms_condition' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Terms & Condition
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(MASTER_TERMS_CONDITIONS_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'terms_condition' && $segment2 == 'terms_conditions' ? 'active' : '' ?>"><a href="<?= base_url() ?>terms_condition/terms_conditions/"><i class="fa fa-circle-o"></i> Terms & Conditions</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_TERMS_CONDITION_GROUP_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'terms_condition' && $segment2 == 'terms_condition_group' ? 'active' : '' ?>"><a href="<?= base_url() ?>terms_condition/terms_condition_group/"><i class="fa fa-circle-o"></i> Terms & Condition Group</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_TERMS_CONDITION_DETAIL_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'terms_condition' && $segment2 == 'terms_condition_detail' ? 'active' : '' ?>"><a href="<?= base_url() ?>terms_condition/terms_condition_detail/"><i class="fa fa-circle-o"></i> Terms & Condition Details</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_DEFAULT_TERMS_CONDITION_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'terms_condition' && $segment2 == 'default_terms_condition' ? 'active' : '' ?>"><a href="<?= base_url() ?>terms_condition/default_terms_condition/"><i class="fa fa-circle-o"></i> Default Terms & Condition</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_TERMS_CONDITION_TEMPLATE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'terms_condition' && $segment2 == 'terms_condition_template' ? 'active' : '' ?>"><a href="<?= base_url() ?>terms_condition/terms_condition_template/"><i class="fa fa-circle-o"></i> Terms & Condition Templates</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'general_master' || $segment1 == 'party' && $segment2 == 'country' || $segment1 == 'party' && $segment2 == 'state' || $segment1 == 'party' && $segment2 == 'city' || $segment1 == 'party' && $segment2 == 'reference' || $segment1 == 'party' && $segment2 == 'designation' || $segment1 == 'party' && $segment2 == 'terms_group' || $segment1 == 'party' && $segment2 == 'sms_topic' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> General Master
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_BRANCH_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'general_master' && $segment2 == 'branch' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>general_master/branch/"><i class="fa fa-circle-o"></i>
                                                Branch</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_PROJECT_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'general_master' && $segment2 == 'project' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>general_master/project/"><i class="fa fa-circle-o"></i>
                                                Project</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_BILLING_TEMPLATE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'general_master' && $segment2 == 'billing_template' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>general_master/billing_template/"><i class="fa fa-circle-o"></i> Billing Template</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_COUNTRY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'country' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/country/"><i class="fa fa-circle-o"></i> Country</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_STATE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'state' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/state/"><i class="fa fa-circle-o"></i> State</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_CITY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'city' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/city/"><i class="fa fa-circle-o"></i> City</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_REFERENCE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'reference' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/reference/"><i class="fa fa-circle-o"></i> Reference</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_DESIGNATION_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'designation' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/designation/"><i class="fa fa-circle-o"></i> Designation</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_TERMS_GROUP_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'terms_group' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/terms_group/"><i class="fa fa-circle-o"></i> Terms Group</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_SMS_TOPICS_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'sms_topic' ? 'active' : '' ?>"><a href="<?= base_url() ?>party/sms_topic/"><i class="fa fa-circle-o"></i> Sms Topics</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_STAFF_ROLES_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'setting' && $segment2 == '' ? 'active' : '' ?>"><a href="<?= base_url() ?>setting"><i class="fa fa-key"></i> Staff Roles</a></li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_CHAT_ROLES_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'setting' && $segment2 == 'chat_access_role' ? 'active' : '' ?>"><a href="<?= base_url() ?>setting/chat_access_role"><i class="fa fa-key"></i> Chat Roles</a></li>
                        <?php } ?>

                        <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_QUOTATION_CHAT_ROLES_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'setting' && $segment2 == 'quotation_reminder_roles' ? 'active' : '' ?>"><a href="<?= base_url() ?>setting/quotation_reminder_roles"><i class="fa fa-key"></i>Quotation Reminder Roles</a></li>
                        <?php } ?>

                        <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_PRINT_LETTER_ROLES_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'setting' && $segment2 == 'print_letter_roles' ? 'active' : '' ?>"><a href="<?= base_url() ?>setting/print_letter_roles"><i class="fa fa-key"></i>Print Letter Roles</a></li>
                        <?php } ?>

                        <?php if ($this->applib->have_access_role(MASTER_GENERAL_MASTER_REMINDER_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'reminder' && $segment2 == '' ? 'active' : '' ?>"><a href="<?= base_url() ?>reminder"><i class="fa fa-bell"></i> Reminder</a></li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER, "view")) { ?>
                            <li class="<?= $segment1 == 'reminder' && $segment2 == 'customer_reminder' ? 'active' : '' ?>"><a href="<?= base_url() ?>reminder/customer_reminder"><i class="fa fa-bell"></i>Customer Reminder</a></li>
                        <?php } ?>
                        <?php
                            $BILLING_TERMS_view = $this->applib->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "view");
                            $BILLING_TERMS_add = $this->applib->have_access_role(MASTER_GENERAL_MASTER_BILLING_TERMS, "add");
                        ?>
                        <?php if ($BILLING_TERMS_view || $BILLING_TERMS_add) { ?>
                            <li class="<?= $segment1 == 'master' && $segment2 == 'billing_terms' || $segment1 == 'master' && $segment2 == 'billing_terms_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-pie-chart"></i>
                                    <span>Billing Terms</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($BILLING_TERMS_add) { ?>
                                        <li class="<?= $segment1 == 'master' && $segment2 == 'billing_terms' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>master/billing_terms"><i class="fa fa-circle-o"></i> Add Billing Terms</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($BILLING_TERMS_view) { ?>
                                        <li class="<?= $segment1 == 'master' && $segment2 == 'billing_terms_list' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>master/billing_terms_list"><i class="fa fa-circle-o"></i> List Billing Terms</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <li class="<?= $segment1 == 'letterpad_content'? 'active' : '' ?>"><a href="<?= base_url() ?>letterpad_content/"><i class="fa fa-bell"></i>Letter pad Content</a></li>

                        <?php if ($this->applib->have_access_role(MASTER_TERMS_CONDITION_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'terms_condition' && $segment2 == 'terms_conditions_table'? 'active' : '' ?>"><a href="<?= base_url() ?>terms_condition/terms_conditions_table"><i class="fa fa-circle-o"></i>Terms & Condition New</a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(PURCHASE_MODULE_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'purchase' ? 'active' : '' || $segment1 == 'purchases' ? 'active' : '' || $segment1 == 'purchase_order' ? 'active' : '' || $segment1 == 'purchase_invoice' ? 'active' : '' || $segment1 == 'supplier' ? 'active' : '' ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-shopping-cart"></i>
                        <span>Purchase</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php
                            $purchase_supplier_view_role = $this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "view");
                            $purchase_supplier_add_role = $this->applib->have_access_role(PURCHASE_SUPPLIER_MODULE_ID, "add");
                            if ($purchase_supplier_view_role || $purchase_supplier_add_role){
                        ?>
                                <li class="<?= $segment1 == 'supplier' && ( $segment2 == 'add' || $segment2 == 'supplier_list') ? 'active' : '' ?>">
                                    <a href="">
                                        <i class="fa fa-circle-o"></i> <span> Supplier</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu dropdown-menu">
                                        <?php if ($purchase_supplier_add_role) { ?>
                                            <li class="<?= $segment1 == 'supplier' && $segment2 == 'add' ? 'active' : '' ?>">
                                                <a href="<?= base_url() ?>supplier/add"><i class="fa fa-circle-o"></i> Add Supplier</a>
                                            </li>
                                        <?php } if ($purchase_supplier_view_role) { ?>
                                            <li class="<?= $segment1 == 'supplier' && $segment2 == 'supplier_list' ? 'active' : '' ?>">
                                                <a href="<?= base_url() ?>supplier/supplier_list/"><i class="fa fa-circle-o"></i> Supplier List</a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                        <?php } ?>
                        <?php
                            $purchase_order_view_role = $this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID, "view");
                            $purchase_order_add_role = $this->applib->have_access_role(PURCHASE_ORDER_MODULE_ID, "add");
                            if ($purchase_order_view_role || $purchase_order_add_role){
                        ?>
                            <li class="<?= $segment1 == 'purchase_order' && ($segment2 == 'add' || $segment2 == 'order_list') ? 'active' : '' ?>">
                                <a href="">
                                    <i class="fa fa-circle-o"></i> <span> Order</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($purchase_order_add_role) { ?>
                                        <li class="<?= $segment1 == 'purchase_order' && $segment2 == 'add' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>purchase_order/add/"><i class="fa fa-circle-o"></i> Add Order</a>
                                        </li>
                                    <?php } if ($purchase_order_view_role) { ?>
                                        <li class="<?= $segment1 == 'purchase_order' && $segment2 == 'order_list' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>purchase_order/order_list/"><i class="fa fa-circle-o"></i> Order List</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $purchase_invoice_view_role = $this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID, "view");
                            $purchase_invoice_add_role = $this->applib->have_access_role(PURCHASE_INVOICE_MODULE_ID, "add");
                            if ($purchase_invoice_view_role || $purchase_invoice_add_role){
                        ?>
                            <li class="<?= $segment1 == 'purchase_invoice' && ($segment2 == 'add' || $segment2 == 'invoice_list') ? 'active' : '' ?>">
                                <a href="">
                                    <i class="fa fa-circle-o"></i> <span> Invoice</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($purchase_order_add_role) { ?>
                                        <li class="<?= $segment1 == 'purchase_invoice' && $segment2 == 'add' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>purchase_invoice/add/"><i class="fa fa-circle-o"></i> Add Invoice</a>
                                        </li>
                                    <?php } if ($purchase_order_view_role) { ?>
                                        <li class="<?= $segment1 == 'purchase_invoice' && $segment2 == 'invoice_list' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>purchase_invoice/invoice_list/"><i class="fa fa-circle-o"></i> Invoice List</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
                
            <?php if ($this->applib->have_access_role(SALES_MODULE_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'sales' || ($segment1 == 'party' && ($segment2 == 'add' || $segment2 == 'party_list' || $segment2 == 'party_log_report' || $segment2 == 'party_log_view' || $segment2 == 'party_list_outstanding' || $segment2 == 'transfer_report')) || $segment1 == 'proforma_invoices' || $segment2 == 'proforma_invoice' || $segment2 == 'proforma_invoice_list' || $segment1 == 'enquiry' || $segment1 == 'quotation' || $segment1 == 'sales_order' ? 'active' : ''; ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-cube"></i>
                        <span>Sales</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php
                            $sales_party_view_role = $this->applib->have_access_role(PARTY_MODULE_ID, "view");
                            $sales_party_add_role = $this->applib->have_access_role(PARTY_MODULE_ID, "add");
                            $sales_party_log_view_role = $this->applib->have_access_role(SALES_PARTY_LOG_MENU_ID, "view");
                            $sales_party_outstanding_view_role = $this->applib->have_access_role(PARTY_OUTSTANDING_MODULE_ID, "view");
                            if ($sales_party_view_role || $sales_party_add_role || $sales_party_log_view_role || $sales_party_outstanding_view_role){
                        ?>
                            <li class=" <?= $segment1 == 'party' && ($segment2 == 'add' || $segment2 == 'party_list' || $segment2 == 'party_log_report' || $segment2 == 'party_list_outstanding' || $segment2 == 'transfer_report') ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Party
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($sales_party_add_role) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'add' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>party/add"><i class="fa fa-circle-o"></i> Add Party</a>
                                        </li>
                                    <?php } if ($sales_party_view_role) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'party_list' ? 'active' : ' '; ?>">
                                            <a href="<?= base_url() ?>party/party_list/"><i class="fa fa-circle-o"></i>Party List</a>
                                        </li>
                                    <?php } if ($sales_party_log_view_role) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'party_log_report' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>party/party_log_report"><i class="fa fa-circle-o"></i> Party Log</a>
                                        </li>
                                    <?php } if ($sales_party_outstanding_view_role) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'party_list_outstanding' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>party/party_list_outstanding"><i class="fa fa-circle-o"></i>Party Outstanding</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(PARTY_MODULE_ID, "is_management") == 1) { ?>
                                        <li class="<?= $segment1 == 'party' && $segment2 == 'transfer_report' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>party/transfer_report"><i class="fa fa-circle-o"></i>Transfer Report</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $sales_enquiry_view_role = $this->applib->have_access_role(ENQUIRY_MODULE_ID, "view");
                            $sales_enquiry_add_role = $this->applib->have_access_role(ENQUIRY_MODULE_ID, "add");
                            if ($sales_enquiry_view_role || $sales_enquiry_add_role){
                        ?>
                            <li class=" <?= $segment1 == 'enquiry' && ($segment2 == 'add' || $segment2 == 'enquiry_list') ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Enquiry
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($sales_enquiry_add_role) { ?>
                                        <li class="<?= $segment1 == 'enquiry' && $segment2 == 'add' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>enquiry/add"><i class="fa fa-circle-o"></i> Add Enquiry</a>
                                        </li>
                                    <?php } if ($sales_enquiry_view_role) { ?>
                                        <li class="<?= $segment1 == 'enquiry' && $segment2 == 'enquiry_list' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>enquiry/enquiry_list/"><i class="fa fa-circle-o"></i>Enquiry List</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $sales_quotation_view_role = $this->applib->have_access_role(QUOTATION_MODULE_ID, "view");
                            $sales_quotation_add_role = $this->applib->have_access_role(QUOTATION_MODULE_ID, "add");
                            if ($sales_quotation_view_role || $sales_quotation_add_role){
                        ?>
                            <li class=" <?= $segment1 == 'quotation' && ($segment2 == 'add' || $segment2 == 'quotation_list') ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Quotation
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($sales_quotation_add_role) { ?>
                                        <li class="<?= $segment1 == 'quotation' && $segment2 == 'add' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>quotation/add/"><i class="fa fa-circle-o"></i> Add Quotation</a>
                                        </li>
                                    <?php } if ($sales_quotation_view_role) { ?>
                                        <li class="<?= $segment1 == 'quotation' && $segment2 == 'quotation_list' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>quotation/quotation_list/"><i class="fa fa-circle-o"></i> Quotation List</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $sales_order_view_role = $this->applib->have_access_role(SALES_ORDER_MODULE_ID, "view");
                            $sales_order_add_role = $this->applib->have_access_role(SALES_ORDER_MODULE_ID, "add");
                            if ($sales_order_view_role || $sales_order_add_role){
                        ?>
                            <li class=" <?= ($segment1 == 'sales_order') && ($segment2 == 'add' || $segment2 == 'add_export' || $segment2 == 'order_list' || $segment2 == 'proforma_invoices_list' || $segment2 == 'delivery_reminder' || $segment2 == 'order_confirmation_reminder' || $segment2 == 'order_cancel_reminder' || $segment2 == 'dispatch_reminder' || $segment2 == 'price_upgrade_reminder' || $segment2 == 'order_payment_reminder' || $segment2 == 'balance_payment_reminder' || $segment2 == 'delivery_reminder_list') ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Sales Order
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($sales_order_add_role) { ?>
                                        <?php if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic")) { ?>
                                            <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'add' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>sales_order/add/"><i class="fa fa-circle-o"></i> Add Domestic Order</a>
                                            </li>
                                        <?php } if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export")) { ?>
                                            <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'add_export' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>sales_order/add_export/"><i class="fa fa-circle-o"></i> Add Export Order</a>
                                            </li>
                                        <?php } ?>
                                    <?php } if ($sales_order_view_role) { ?>
                                        <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'order_list' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>sales_order/order_list/"><i class="fa fa-circle-o"></i> Order List</a>
                                        </li>
                                    <?php } if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "view")) { ?>
                                        <li class=" <?= $segment1 == 'sales_order' && ($segment2 == 'delivery_reminder' || $segment2 == 'order_confirmation_reminder' || $segment2 == 'order_cancel_reminder' || $segment2 == 'dispatch_reminder' || $segment2 == 'price_upgrade_reminder' || $segment2 == 'order_payment_reminder' || $segment2 == 'balance_payment_reminder' || $segment2 == 'delivery_reminder_list') ? 'active' : ''; ?>">
                                            <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Sales Reminder
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu dropdown-menu">
                                                <?php if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "add")) { ?>
                                                    <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'delivery_reminder' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>sales_order/delivery_reminder/"><i class="fa fa-circle-o"></i> Delivery Reminder</a>
                                                    </li>
                                                    <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'order_confirmation_reminder' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>sales_order/order_confirmation_reminder/"><i class="fa fa-circle-o"></i> Order Confirmation</a>
                                                    </li>
                                                    <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'order_cancel_reminder' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>sales_order/order_cancel_reminder/"><i class="fa fa-circle-o"></i> Order Cancel Reminder</a>
                                                    </li>
                                                    <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'dispatch_reminder' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>sales_order/dispatch_reminder/"><i class="fa fa-circle-o"></i> Dispatch Reminder</a>
                                                    </li>
                                                    <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'price_upgrade_reminder' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>sales_order/price_upgrade_reminder/"><i class="fa fa-circle-o"></i> Price Upgrade Reminder</a>
                                                    </li>
                                                    <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'order_payment_reminder' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>sales_order/order_payment_reminder/"><i class="fa fa-circle-o"></i> Order Payment Remin.</a>
                                                    </li>
                                                    <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'balance_payment_reminder' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>sales_order/balance_payment_reminder/"><i class="fa fa-circle-o"></i> Balance Payment Remin.</a>
                                                    </li>
                                                <?php } ?>
                                                <li class=" <?= $segment1 == 'sales_order' && $segment2 == 'delivery_reminder_list' ? 'active' : ''; ?>">
                                                    <a href="<?= base_url() ?>sales_order/delivery_reminder_list/"><i class="fa fa-circle-o"></i> Reminder List</a>
                                                </li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $sales_profome_invoice_view_role = $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID, "view");
                            $sales_profome_invoice_add_role = $this->applib->have_access_role(PROFORMA_INVOICE_MODULE_ID, "add");
                            if ($sales_profome_invoice_view_role || $sales_profome_invoice_add_role){
                        ?>
                            <li class="<?= $segment1 == 'proforma_invoices' && ($segment2 == 'add' || $segment2 == 'add_export' || $segment2 == 'proforma_invoices_list') ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Proforma Invoice
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($sales_profome_invoice_add_role) { ?>
                                        <?php if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic")) { ?>
                                            <li class=" <?= $segment1 == 'proforma_invoices' && $segment2 == 'add' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>proforma_invoices/add/"><i class="fa fa-circle-o"></i> Add Domestic Proforma</a>
                                            </li>
                                        <?php } if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export")) { ?>
                                            <li class=" <?= $segment1 == 'proforma_invoices' && $segment2 == 'add_export' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>proforma_invoices/add_export/"><i class="fa fa-circle-o"></i> Add Export Proforma</a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($sales_profome_invoice_view_role) { ?>
                                        <li class=" <?= $segment1 == 'proforma_invoices' && $segment2 == 'proforma_invoices_list' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>proforma_invoices/proforma_invoices_list/"><i class="fa fa-circle-o"></i> Proforma Invoice List</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(SALES_TRACK_SALE_MENU_ID, "view")) { ?>
                            <li class="<?= $segment2 == 'track_sale' || $segment2 == 'track_sale' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>sales/track_sale"><i class="fa fa-circle-o"></i> Track Sale</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php
                $start_production_view_role = $this->applib->have_access_role(START_PRODUCTION_MODULE_ID, "view");
                if ($start_production_view_role){
            ?>
                <li class="<?= $segment1 == 'production_start' && ($segment2 == 'production_item_detail' || $segment2 == 'production_start_list' ) ? 'active' : '' ?>">
                    <a href="javascript:void(0);"><i class="fa fa-industry"></i><span>Production</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <li class=" <?= $segment1 == 'production_start' && $segment2 == 'production_item_detail' ? 'active' : ''; ?>">
                            <a href="<?= base_url() ?>production_start/production_item_detail/"><i class="fa fa-circle-o"></i>Production Start</a>
                        </li>
                        <?php if ($sales_profome_invoice_view_role) { ?>
                            <li class=" <?= $segment1 == 'production_start' && $segment2 == 'production_start_list' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>production_start/production_start_list/"><i class="fa fa-circle-o"></i>Production List</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(DISPATCH_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'challan' || $segment1 == 'testing_report' || $segment1 == 'invoice' || $segment1 == 'finance' || $segment1 == 'dispatch' && ($segment2 == 'add' || $segment2 == 'testing_report_list' || $segment2 == 'payment_reminder' || $segment2 == 'finance_reminder_list' || $segment2 == 'finance_balance_payment' || $segment2 == 'our-cliant') ? 'active' : '' ?>">
                    <a href="javascript:void(0);"><i class="fa fa-sticky-note"></i>
                        <span>Dispatch</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php
                            $challan_view_role = $this->applib->have_access_role(CHALLAN_MODULE_ID, "view");
                            $challan_add_role = $this->applib->have_access_role(CHALLAN_MODULE_ID, "add");
                        ?>
                        <?php if ($challan_view_role || $challan_add_role){ ?>
                            <li class="<?= $segment1 == 'challan' && $segment2 == 'add' || $segment2 == 'challan_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    Challan<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                <?php if ($challan_add_role) { ?>
                                    <li class="<?= $segment1 == 'challan' && $segment2 == 'add' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>challan/add/"><i class="fa fa-circle-o"></i> Add Challan</a>
                                    </li>
                                <?php } if ($challan_view_role) { ?>
                                    <li class="<?= $segment1 == 'challan' && $segment2 == 'challan_list' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>challan/challan_list/"><i class="fa fa-circle-o"></i> Challan List</a>
                                    </li>
                                <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $testing_report_view_role = $this->applib->have_access_role(TESTING_REPORT_MODULE_ID, "view");
                            $testing_report_add_role = $this->applib->have_access_role(TESTING_REPORT_MODULE_ID, "add");
                        ?>
                        <?php if ($testing_report_view_role || $testing_report_add_role){ ?>
                            <li class="<?= $segment1 == 'testing_report' && ($segment2 == 'add' || $segment2 == 'testing_report_list') ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    Testing Report<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($testing_report_add_role) { ?>
                                        <li class="<?= $segment1 == 'testing_report' && $segment2 == 'add' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>testing_report/add/"><i class="fa fa-circle-o"></i>Add Testing Report</a>
                                        </li>
                                    <?php } if ($testing_report_view_role) { ?>
                                        <li class="<?= $segment1 == 'testing_report' && $segment2 == 'testing_report_list' ? 'active' : '' ?>">
                                            <a href="<?= base_url() ?>testing_report/testing_report_list/"><i class="fa fa-circle-o"></i>Testing Report List</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $invoice_view_role = $this->applib->have_access_role(INVOICE_MODULE_ID, "view");
                            $invoice_add_role = $this->applib->have_access_role(INVOICE_MODULE_ID, "add");
                        ?>
                        <?php if ($invoice_view_role || $invoice_add_role){ ?>
                            <li class="<?= $segment2 == 'add_domestic' || $segment2 == 'add_export' || $segment2 == 'invoice_list' || $segment2 == 'finance_reminder_list' || $segment2 == 'finance_balance_payment' || $segment2 == 'finance_general_notice' ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    Invoice<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($invoice_add_role) { ?>
                                        <?php if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic")) { ?>
                                            <li class="<?= $segment2 == 'add_domestic' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>invoice/add_domestic"><i class="fa fa-circle-o"></i> Add Domestic Invoice</a>
                                            </li>
                                        <?php } if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export")) { ?>
                                            <li class="<?= $segment2 == 'add_export' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>invoice/add_export"><i class="fa fa-circle-o"></i> Add Export Invoice</a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($invoice_view_role) { ?>
                                        <li class="<?= $segment2 == 'invoice_list' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>invoice/invoice_list"><i class="fa fa-circle-o"></i> Invoice List</a>
                                        </li>
                                    <?php } if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "view")) { ?>
                                        <li class=" <?= $segment1 == 'finance' && ($segment2 == 'finance_balance_payment' || $segment2 == 'finance_reminder_list') ? 'active' : ''; ?>">
                                            <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Finance Reminder
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu dropdown-menu">
                                                <?php if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "add")) { ?>
                                                    <li class=" <?= $segment1 == 'finance' && $segment2 == 'finance_balance_payment' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>finance/finance_balance_payment/"><i class="fa fa-circle-o"></i> Balance Payment</a>
                                                    </li>
                                                <?php } ?>
                                                <li class=" <?= $segment1 == 'finance' && $segment2 == 'finance_reminder_list' ? 'active' : ''; ?>">
                                                    <a href="<?= base_url() ?>finance/finance_reminder_list/"><i class="fa fa-circle-o"></i> Reminder List</a>
                                                </li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(OUR_CLIANT_MODULE_ID, "view")){ ?>
                            <li class="<?= $segment2 == 'our-cliant' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>dispatch/our-cliant/"><i class="fa fa-circle-o"></i> Customer List</a>
                            </li>
                        <?php } ?>
                        <?php if ($invoice_view_role && $this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "add")) { ?>
                            <li class="<?= $segment1 == 'finance' && in_array($segment2, array('payment_reminder')) ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>finance/payment_reminder/"><i class="fa fa-circle-o"></i> Payment Reminder</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(SERVICE_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'service' || $segment1 == 'installation' || $segment1 == 'feedback' || $segment1 == 'complain' || $segment1 == 'general_invoice' ? 'active' : ''; ?>">
                    <a href="javascript:void(0);"><i class="fa fa-cogs"></i>
                        <span>Service</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php
                            $installation_view_role = $this->applib->have_access_role(INSTALLATION_MODULE_ID, "view");
                            $installation_add_role = $this->applib->have_access_role(INSTALLATION_MODULE_ID, "add");
                        ?>
                        <?php if ($installation_view_role || $installation_add_role){ ?>
                            <li class="<?= $segment1 == 'installation' && $segment2 == 'add' || $segment2 == 'installation_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    <span>Installation</span>
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($installation_add_role){ ?>
                                        <li class="<?= $segment1 == 'installation' && $segment2 == 'add' ? 'active' : ''; ?>"><a href="<?= base_url() ?>installation/add"><i class="fa fa-circle-o"></i>Add Installation</a></li>
                                    <?php } ?>
                                    <?php if ($installation_view_role){ ?>
                                        <li class="<?= $segment1 == 'installation' && $segment2 == 'installation_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>installation/installation_list"><i class="fa fa-circle-o"></i>Installation List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $feedback_view_role = $this->applib->have_access_role(FEEDBACK_MODULE_ID, "view");
                            $feedback_add_role = $this->applib->have_access_role(FEEDBACK_MODULE_ID, "add");
                        ?>
                        <?php if ($feedback_view_role || $feedback_add_role){ ?>
                            <li class="<?= $segment1 == 'feedback' && $segment2 == 'add' || $segment2 == 'feedback_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    <span>Feedback</span>
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($feedback_add_role){ ?>
                                        <li class="<?= $segment1 == 'feedback' && $segment2 == 'add' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>feedback/add"><i class="fa fa-circle-o"></i>Add Feedback</a></li>
                                    <?php } ?>
                                    <?php if ($feedback_view_role){ ?>
                                        <li class="<?= $segment1 == 'feedback' && $segment2 == 'feedback_list' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>feedback/feedback_list"><i class="fa fa-circle-o"></i>Feedback List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(COMPLAIN_MENU_ID, "view")) { ?>
                            <?php
                                $complain_view_role = $this->applib->have_access_role(COMPLAIN_MODULE_ID, "view");
                                $complain_add_role = $this->applib->have_access_role(COMPLAIN_MODULE_ID, "add");
                                $resolve_complain_view_role = $this->applib->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "view");
                                $resolve_complain_add_role = $this->applib->have_access_role(RESOLVE_COMPLAIN_MODULE_ID, "add");
                            ?>
                            <?php if ($complain_view_role || $complain_add_role || $resolve_complain_view_role || $resolve_complain_add_role){ ?>
                                <li class="<?= $segment1 == 'complain' && $segment2 == 'add' || $segment2 == 'complain_list' || $segment2 == 'resolve_complain' || $segment2 == 'resolve_complain_list' ? 'active' : '' ?>">
                                    <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                        <span>Complain</span>
                                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                    </a>
                                    <ul class="treeview-menu dropdown-menu">
                                        <?php if ($complain_add_role){ ?>
                                            <li class="<?= $segment1 == 'complain' && $segment2 == 'add' ? 'active' : ''; ?>"><a href="<?= base_url() ?>complain/add"><i class="fa fa-circle-o"></i>Add Complain</a></li>
                                        <?php } ?>
                                        <?php if ($complain_view_role){ ?>
                                            <li class="<?= $segment1 == 'complain' && $segment2 == 'complain_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>complain/complain_list"><i class="fa fa-circle-o"></i>Complain List</a></li>
                                        <?php } ?>
                                        <?php if ($resolve_complain_add_role){ ?>
                                            <li class="<?= $segment1 == 'complain' && $segment2 == 'resolve_complain' ? 'active' : ''; ?>"><a href="<?= base_url() ?>complain/resolve_complain"><i class="fa fa-circle-o"></i>Resolve Complain</a></li>
                                        <?php } ?>
                                        <?php if ($resolve_complain_view_role){ ?>
                                            <li class="<?= $segment1 == 'complain' && $segment2 == 'resolve_complain_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>complain/resolve_complain_list"><i class="fa fa-circle-o"></i>Resolve List</a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(GENERAL_INVOICE_MENU_ID, "view")) { ?>
                            <?php
                                $parts_view_role = $this->applib->have_access_role(PARTS_MODULE_ID, "view");
                                $parts_add_role = $this->applib->have_access_role(PARTS_MODULE_ID, "add");
                            ?>
                            <li class="<?= $segment1 == 'general_invoice' && $segment2 == 'add_invoice_from_complain' || $segment2 == 'add_invoice_from_party' || $segment2 == 'add_invoice_from_party_pitem' || $segment2 == 'general_invoice_list' || $segment2 == 'add_parts' || $segment2 == 'parts_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    <span>General Invoice</span>
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($parts_add_role){ ?>
                                        <li class="<?= $segment2 == 'add_parts' ? 'active' : ''; ?>"><a href="<?= base_url() ?>general_invoice/add_parts"><i class="fa fa-circle-o"></i>Add Parts</a></li>
                                    <?php } ?>
                                    <?php if ($parts_view_role){ ?>
                                        <li class="<?= $segment2 == 'parts_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>general_invoice/parts_list"><i class="fa fa-circle-o"></i>Parts List</a></li>
                                    <?php } ?>
                                    <?php
                                        $general_invoice_view_role = $this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID, "view");
                                        $general_invoice_add_role = $this->applib->have_access_role(GENERAL_INVOICE_MODULE_ID, "add");
                                    ?>
                                    <?php if ($general_invoice_add_role){ ?>
                                        <li class="<?= $segment2 == 'add_invoice_from_complain' ? 'active' : ''; ?>"><a href="<?= base_url() ?>general_invoice/add_invoice_from_complain"><i class="fa fa-circle-o"></i>Add Invoice From Complain</a></li>
                                    <?php } ?>
                                    <?php if ($general_invoice_add_role){ ?>
                                        <li class="<?= $segment2 == 'add_invoice_from_party' ? 'active' : ''; ?>"><a href="<?= base_url() ?>general_invoice/add_invoice_from_party"><i class="fa fa-circle-o"></i>Add Parts Invoice</a></li>
                                    <?php } ?>
                                    <?php if ($general_invoice_add_role){ ?>
                                        <li class="<?= $segment2 == 'add_invoice_from_party_pitem' ? 'active' : ''; ?>"><a href="<?= base_url() ?>general_invoice/add_invoice_from_party_pitem"><i class="fa fa-circle-o"></i>Add Purchase Item Invoice</a></li>
                                    <?php } ?>
                                    <?php if ($general_invoice_view_role){ ?>
                                        <li class="<?= $segment2 == 'general_invoice_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>general_invoice/general_invoice_list" ><i class="fa fa-circle-o"></i>General Invoice List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "add")) { ?>
                            <li class="<?= $segment2 == 'general_reminder' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>service/general_reminder/"><i class="fa fa-circle-o"></i> General Reminder</a>
                            </li>
                        <?php } if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "view")) { ?>
                            <li class="<?= $segment2 == 'general_reminder_list' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>service/general_reminder_list/"><i class="fa fa-circle-o"></i> General Reminder List</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(FINANCE_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'finance' || $segment1 == 'payment' && ($segment2 == 'search_invoices' || $segment2 == 'add' || $segment2 == 'payment_list' || $segment2 == 'ledger') ? 'active' : '' ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-rupee"></i><span>Accounts</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php
                            $invoice_view_role = $this->applib->have_access_role(INVOICE_MODULE_ID, "view");
                            $invoice_add_role = $this->applib->have_access_role(INVOICE_MODULE_ID, "add");
                        ?>
                        <?php if ($invoice_view_role || $invoice_add_role){ ?>
                            <li class="<?= $segment2 == 'add_domestic' || $segment2 == 'add_export' || $segment2 == 'invoice_list' || $segment2 == 'finance_reminder_list' || $segment2 == 'finance_balance_payment' || $segment2 == 'finance_general_notice' ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    Invoice<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($invoice_add_role) { ?>
                                        <?php if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "domestic")) { ?>
                                            <li class="<?= $segment2 == 'add_domestic' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>invoice/add_domestic"><i class="fa fa-circle-o"></i> Add Domestic Invoice</a>
                                            </li>
                                        <?php } if ($this->applib->have_access_current_user_rights(PARTY_TYPE_MODULE_ID, "export")) { ?>
                                            <li class="<?= $segment2 == 'add_export' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>invoice/add_export"><i class="fa fa-circle-o"></i> Add Export Invoice</a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($invoice_view_role) { ?>
                                        <li class="<?= $segment2 == 'invoice_list' ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>invoice/invoice_list"><i class="fa fa-circle-o"></i> Invoice List</a>
                                        </li>
                                    <?php } if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "view")) { ?>
                                        <li class=" <?= $segment1 == 'finance' && ($segment2 == 'finance_balance_payment' || $segment2 == 'finance_reminder_list') ? 'active' : ''; ?>">
                                            <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Finance Reminder
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu dropdown-menu">
                                                <?php if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "add")) { ?>
                                                    <li class=" <?= $segment1 == 'finance' && $segment2 == 'finance_balance_payment' ? 'active' : ''; ?>">
                                                        <a href="<?= base_url() ?>finance/finance_balance_payment/"><i class="fa fa-circle-o"></i> Balance Payment</a>
                                                    </li>
                                                <?php } ?>
                                                <li class=" <?= $segment1 == 'finance' && $segment2 == 'finance_reminder_list' ? 'active' : ''; ?>">
                                                    <a href="<?= base_url() ?>finance/finance_reminder_list/"><i class="fa fa-circle-o"></i> Reminder List</a>
                                                </li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $payment_view_role = $this->applib->have_access_role(PAYMENT_MODULE_ID, "view");
                            $payment_add_role = $this->applib->have_access_role(PAYMENT_MODULE_ID, "add");
                        ?>
                        <?php if ($payment_view_role || $payment_add_role){ ?>
                            <li class="<?= $segment2 == 'add' || $segment2 == 'payment_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    <span>Payment</span>
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($payment_add_role){ ?>
                                        <li class="<?= $segment2 == 'add' ? 'active' : ''; ?>"><a href="<?= base_url() ?>payment/add"><i class="fa fa-circle-o"></i> Add Payment</a></li>
                                    <?php } ?>
                                    <?php if ($payment_view_role){ ?>
                                        <li class="<?= $segment2 == 'payment_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>payment/payment_list"><i class="fa fa-circle-o"></i> Payment List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(LEDGER_MODULE_ID, "view")) { ?>
                            <li class="<?= $segment2 == 'ledger' ? 'active' : '' ?>"><a href="<?= base_url() ?>finance/ledger"><i class="fa fa-circle-o"></i> Ledger</a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(REPORT_MENU_ID, "view")) { ?>
                <li class="treeview <?= ($segment1 == 'report') ? 'active' : '' ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-bar-chart "></i> <span>Report</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu" >
                        <li class="<?= $segment1 == 'report' && in_array($segment2, array('list_of_inquiry', 'list_of_quotation', 'list_of_sales_order', 'delay_quotation_report', 'commited_sales_order', 'sales_order_status', 'quotation_summary', 'inquiry_followup_history', 'quotation_followup_history',)) ? 'active' : '' ?>">
                            <a href="<?= BASE_URL ?>report/list_of_inquiry/?status=pending&user_id=<?= $staff_id ?>">
                                <i class="fa fa-circle-o"></i> Module wise reports
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu dropdown-menu">
                                <?php if ($this->applib->have_access_role(REPORT_LIST_OF_INQUIRY_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'list_of_inquiry' ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/list_of_inquiry/?status=pending&user_id=<?= $staff_id ?>"><i class="fa fa-circle-o"></i> List Of Enquiry</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_LIST_OF_QUOTATION_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'list_of_quotation' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/list_of_quotation/"><i class="fa fa-circle-o"></i> List Of Quotation</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_LIST_OF_SALES_ORDER_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'list_of_sales_order' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/list_of_sales_order/"><i class="fa fa-circle-o"></i> List Of Sales Order</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_DELAY_QUOTATION_REPORT_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'delay_quotation_report' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/delay_quotation_report/"><i class="fa fa-circle-o"></i> Delay Quotation Report</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_LIST_OF_DUE_SALES_ORDER_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'commited_sales_order' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/commited_sales_order"><i class="fa fa-circle-o"></i> List Of Due Sales Order</a>
                                    </li>
                                <?php } ?>
                                <?php /*if ($this->applib->have_access_role(REPORT_SALES_ORDER_STATUS_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'sales_order_status' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/sales_order_status/"><i class="fa fa-circle-o"></i> Sales Order Status</a>
                                    </li>
                                <?php }*/ ?>
                                <?php if ($this->applib->have_access_role(REPORT_QUOTATION_MANAGEMENT_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'quotation_summary' ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/quotation_summary"><i class="fa fa-circle-o"></i> Quotation Management</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_ENQUIRY_FOLLOWUP_HISTORY_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'inquiry_followup_history' ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/inquiry_followup_history"><i class="fa fa-circle-o"></i> Enquiry Followup History</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_QUOTATION_FOLLOWUP_HISTORY_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'quotation_followup_history' ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/quotation_followup_history"><i class="fa fa-circle-o"></i> Quotation followup history</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="<?= $segment1 == 'report' && in_array($segment2, array('sales_lead_summary', 'not_invoice_created_sales_order_report', 'pending-report', 'bom-info', 'sales_summary', 'production_schedule', 'stock_summary', 'pending_report', 'bom_info')) ? 'active' : '' ?>">
                            <a href="<?= BASE_URL ?>report/list_of_inquiry/?status=pending&user_id=<?= $staff_id ?>">
                                <i class="fa fa-circle-o"></i> Common reports
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu dropdown-menu">
                                <?php if ($this->applib->have_access_role(REPORT_SALES_LEAD_SUMMARY_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'sales_lead_summary' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/sales_lead_summary"><i class="fa fa-circle-o"></i> Sales Leads Summary</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_SALES_SUMMARY_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'sales_summary' ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/sales_summary"><i class="fa fa-circle-o"></i> Sales Summary</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_PRODUCTION_SCHEDULE_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'production_schedule' ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/production_schedule"><i class="fa fa-circle-o"></i> Production Schedule</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_STOCK_MANAGEMENT_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'stock_summary' ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/stock_summary"><i class="fa fa-circle-o"></i> Stock Management</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_PENDING_REPORT_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && ($segment2 == 'pending_report' || $segment2 == 'pending-report' || $segment2 == 'not_invoice_created_sales_order_report') ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/pending-report"><i class="fa fa-circle-o"></i> Pending Report</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(REPORT_BOM_INFO_REPORT_MENU_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && ($segment2 == 'bom_info' || $segment2 == 'bom-info') ? 'active' : '' ?>">
                                        <a href="<?= BASE_URL ?>report/bom-info"><i class="fa fa-circle-o"></i> BOM Info Report</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php if ($this->applib->have_access_role(SUMMARY_REPORT_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'report' && $segment2 == 'summary_report' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>report/summary_report"><i class="fa fa-circle-o"></i> Summary Report</a>
                            </li>
                    <?php } ?>
                        <li class="<?= $segment1 == 'report' && $segment2 == 'counts' ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>report/counts"><i class="fa fa-circle-o"></i> Counts</a>
                        </li>
                        <li class="<?= $segment1 == 'report' && in_array($segment2, array('employee_performance_chart', 'sales_chart')) ? 'active' : '' ?>">
                            <a href="#">
                                <i class="fa fa-circle-o"></i> Chart
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu dropdown-menu">
                                <?php if ($this->applib->have_access_role(EMPLOYEE_PERFORMANCE_CHART_MODULE_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'employee_performance_chart' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/employee_performance_chart"><i class="fa fa-circle-o"></i> Employee Performance</a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->applib->have_access_role(SALES_CHART_MODULE_ID, "view")) { ?>
                                    <li class="<?= $segment1 == 'report' && $segment2 == 'sales_chart' ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>report/sales_chart"><i class="fa fa-circle-o"></i> Sales Chart</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php if ($this->applib->have_access_role(ITEM_COSTING_REPORT_MODULE_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'report' && $segment2 == 'item_costing_report' || $segment2 == 'item_costing_details' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>report/item_costing_report"><i class="fa fa-circle-o"></i> Item Costing Report</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(ITEM_DIFF_REPORT_MODULE_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'report' && $segment2 == 'item_diff_report' || $segment2 == 'item_diff_details' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>report/item_diff_report"><i class="fa fa-circle-o"></i> Item Diff Report</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(PURCHASE_ITEM_STOCK_REPORT_MODULE_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'report' && $segment2 == 'purchase_item_stock_report' || $segment2 == 'purchase_item_stock_report' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>report/purchase_item_stock_report"><i class="fa fa-circle-o"></i> Purchase Item Stock Report</a>
                            </li>
                            <li class="<?= $segment1 == 'report' && $segment2 == 'purchase_item_stock_sheet_report' || $segment2 == 'purchase_item_stock_sheet_report' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>report/purchase_item_stock_sheet_report"><i class="fa fa-circle-o"></i> Purchase Item Stock Sheet</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(GENERAL_MENU_ID, "view")) { ?>
                <li class="treeview <?= (($segment1 == 'master' && $segment2 == 'daily_work_entry') || $segment1 == 'sms' || $segment1 == 'courier' || $segment1 == 'party_frontend' || $segment1 == 'feedback_reply') ? 'active' : '' ?>">
                    <a href="javascript:void(0);"><i class="fa fa-square-o"></i>
                        <span>General</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php if ($this->applib->have_access_role(GENERAL_DAILY_WORK_ENTRY_MENU_ID, "view") || $this->applib->have_access_role(GENERAL_DAILY_WORK_ENTRY_MENU_ID, "edit")) { ?>
                            <li class="<?= $segment1 == 'master' && $segment2 == 'daily_work_entry' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>master/daily_work_entry"><i class="fa fa-circle-o"></i> Daily Work Entry</a>
                            </li>
                        <?php } ?>
                        <?php
                            $sms_view_role = $this->applib->have_access_role(COMMON_PLACE_SMS_MENU_ID, "view");
                            $sms_add_role = $this->applib->have_access_role(COMMON_PLACE_SMS_MENU_ID, "add");
                        ?>
                        <?php if ($sms_view_role || $sms_add_role){ ?>
                            <li class="<?= $segment1 == 'sms' && $segment2 == 'add' || $segment2 == 'sent_sms_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    <span>SMS</span>
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($sms_add_role){ ?>
                                        <li class="<?= $segment2 == 'add' ? 'active' : ''; ?>"><a href="<?= base_url() ?>sms/add"><i class="fa fa-circle-o"></i>Add SMS</a></li>
                                    <?php } ?>
                                    <?php if ($sms_view_role){ ?>
                                        <li class="<?= $segment2 == 'sent_sms_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>sms/sent_sms_list"><i class="fa fa-circle-o"></i>Sent SMS List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $courier_view_role = $this->applib->have_access_role(COURIER_MENU_ID, "view");
                            $courier_add_role = $this->applib->have_access_role(COURIER_MENU_ID, "add");
                            if ($courier_view_role || $courier_add_role){
                        ?>
                            <li class="<?= $segment1 == 'courier' && $segment2 == 'add' || $segment2 == 'courier_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    <span>Courier</span>
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($courier_add_role){ ?>
                                        <li class="<?= $segment2 == 'add' ? 'active' : ''; ?>"><a href="<?= base_url() ?>courier/add"><i class="fa fa-circle-o"></i>Add Courier</a></li>
                                    <?php } ?>
                                    <?php if ($courier_view_role){ ?>
                                        <li class="<?= $segment2 == 'courier_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>courier/courier_list"><i class="fa fa-circle-o"></i>Courier List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $frontend_view_role = $this->applib->have_access_role(PARTY_FRONTEND_MENU_ID, "view");
                            if ($frontend_view_role){
                        ?>
                            <li class="<?= $segment1 == 'party_frontend' && $segment2 == 'files' || $segment2 == 'file_list' ? 'active' : '' ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                    <span>Front-End</span>
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <li class="<?= $segment1 == 'party_frontend' && $segment2 == 'files' || $segment2 == 'file_list' ? 'active' : '' ?>">
                                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>
                                            <span>Files</span>
                                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                        </a>
                                        <ul class="treeview-menu dropdown-menu">
                                            <li class="<?= $segment2 == 'files' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>party_frontend/files"><i class="fa fa-circle-o"></i>Add Files</a></li>
                                            <li class="<?= $segment2 == 'file_list' ? 'active' : ''; ?>">
                                                <a href="<?= base_url() ?>party_frontend/file_list"><i class="fa fa-circle-o"></i>File List</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                            $courier_view_role = $this->applib->have_access_role(COURIER_MENU_ID, "view");
                            $courier_add_role = $this->applib->have_access_role(COURIER_MENU_ID, "add");
                            if ($courier_view_role || $courier_add_role){
                        ?>
                            <li class="<?= $segment2 == 'add' ? 'active' : ''; ?>"><a href="<?= base_url() ?>feedback_reply/add"><i class="fa fa-circle-o"></i>Feedback</a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(HR_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'hr' ? 'active' : ''; ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-users"></i> <span>HR</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php if ($this->applib->have_access_role(HR_EMPLOYEE_MASTER_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'hr' && ($segment2 == 'add' || $segment2 == 'employee_master' || $segment2 == 'list-salary' || $segment2 == 'salary-report') ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Employee Master
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "add")) { ?>
                                        <li class="<?= $segment1 == 'hr' && $segment2 == 'add' ? 'active' : ''; ?>"><a href="<?= base_url() ?>hr/add"><i class="fa fa-circle-o"></i> Add Employee</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'hr' && $segment2 == 'employee_master' ? 'active' : ''; ?>"><a href="<?= base_url() ?>hr/employee_master"><i class="fa fa-circle-o"></i> Employee List</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'hr' && $segment2 == 'salary-report' ? 'active' : ''; ?>"><a href="<?= base_url() ?>hr/salary-report"><i class="fa fa-circle-o"></i> Calculate Salary</a></li>
                                        <li class="<?= $segment1 == 'hr' && $segment2 == 'salary_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>hr/salary_list"><i class="fa fa-circle-o"></i> Calculate Salary List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(HR_LEAVE_MASTER_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'hr' && in_array($segment2, array('total-free-leaves', 'weekly-leaves', 'yearly-leaves')) ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Leave Master
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(HR_TOTAL_FREE_LEAVES_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'hr' && in_array($segment2, array('total-free-leaves')) ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>hr/total-free-leaves"><i class="fa fa-circle-o"></i>Total Free Leaves</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(HR_WEEKLY_LEAVES_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'hr' && in_array($segment2, array('weekly-leaves')) ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>hr/weekly-leaves"><i class="fa fa-circle-o"></i>Weekly Leaves</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(HR_YEARLY_LEAVES_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'hr' && in_array($segment2, array('yearly-leaves')) ? 'active' : ''; ?>">
                                            <a href="<?= base_url() ?>hr/yearly-leaves"><i class="fa fa-circle-o"></i>Yearly Leaves</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'hr' && in_array($segment2, array('expected-interview')) ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>hr/expected-interview"><i class="fa fa-circle-o"></i> Expected Interview</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(HR_LETTER_MANAGEMENT_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'hr' && in_array($segment2, array('letter', 'print_letter', 'issued_letter_list')) ? 'active' : ''; ?>">
                                <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Letter Management
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu dropdown-menu">
                                    <?php if ($this->applib->have_access_role(HR_ADD_LETTER_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'hr' && $segment2 == 'letter' ? 'active' : ''; ?>"><a href="<?= base_url() ?>hr/letter"><i class="fa fa-circle-o"></i> Add Letter</a></li>
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(HR_PRINT_LETTER_MENU_ID, "print")) { ?>
                                        <li class="<?= $segment1 == 'hr' && $segment2 == 'print_letter' ? 'active' : ''; ?>"><a href="<?= base_url() ?>hr/print_letter"><i class="fa fa-circle-o"></i> Print Letter</a></li> 
                                    <?php } ?>
                                    <?php if ($this->applib->have_access_role(HR_ISSUED_LETTER_MENU_ID, "view")) { ?>
                                        <li class="<?= $segment1 == 'hr' && $segment2 == 'issued_letter_list' ? 'active' : ''; ?>"><a href="<?= base_url() ?>hr/issued_letter_list"><i class="fa fa-circle-o"></i> Issued Letter</a></li> 
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(HR_LEAVE_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'hr' && in_array($segment2, array('leave')) ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>hr/leave/"><i class="fa fa-circle-o"></i> Leave</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(MAIL_SYSTEM_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'mail-system3' && in_array($segment3, array('inbox', 'compose', 'read')) ? 'active' : ''; ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-envelope"></i> <span>Mail System</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php if ($this->applib->have_access_role(MAIL_SYSTEM_INBOX_MENU_ID, "view")) { ?>
                            <li class="<?= $segment2 == 'inbox' ? 'active' : ''; ?> offlinemails">
                                <a href="<?= base_url() ?>mail-system3/inbox/" class="sidebar-unread-mails">Inbox</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MAIL_SYSTEM_COMPOSE_MENU_ID, "view")) { ?>
                            <li class="<?= $segment2 == 'compose-mail' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>mail-system3/compose-mail/compose">Compose</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MAIL_SYSTEM_SEND_OUTBOX_MAILS_MENU_ID, "view")) { ?>
                            <li class="">
                                <a href="<?= base_url() ?>mail-system3/send-outbox-mails/">Send Outbox Mails</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(APPLY_FOR_LEAVE_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'hr' && in_array($segment2, array('apply-leave')) ? 'active' : ''; ?>">
                    <a href="<?= base_url() ?>hr/apply-leave"><i class="fa fa-fw fa-medkit"></i>
                        <span> Apply For Leave</span>
                    </a>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(GOOGLE_SHEET_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'sheet' ? 'active' : ''; ?>">
                    <a href="javascript:void(0);"><i class="fa fa-google"></i> <span>Google Sheet</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu dropdown-menu">
                        <?php if ($this->applib->have_access_role(GOOGLE_SHEET_MODULE_ID, "add")) { ?>
                            <li class="<?= $segment1 == 'sheet' && in_array($segment2, array('add_google_sheet')) ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>sheet/add_google_sheet/"><i class="fa fa-plus"></i>Add New Sheet</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(GOOGLE_SHEET_MODULE_ID, "edit")) { ?>
                            <li class="<?= $segment1 == 'sheet' && in_array($segment2, array('google_sheet_list')) ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>sheet/google_sheet_list/"><i class="fa fa-list-alt "></i>Sheet List</a>
                            </li>
                        <?php } ?>
                        <?php
                            $sheet_data = $this->applib->get_login_user_google_sheet();
                            if (!empty($sheet_data)){
                                foreach ($sheet_data as $sheet_row){
                        ?>
                                    <li class="<?= $segment1 == 'sheet' && in_array($segment2, array('google_sheet')) && $segment3 == $sheet_row['id'] ? 'active' : ''; ?>">
                                        <a href="<?= base_url() ?>sheet/google_sheet/<?= $sheet_row['id'] ?>" ><i class="fa fa-file-excel-o "></i> <?= $sheet_row['name'] ?></a>
                                        <?php /* <a href="<?= $sheet_row['url'] ?>" target="_blank"><i class="fa fa-file-excel-o "></i> <?=$sheet_row['name']?></a> */ ?>
                                    </li>
                        <?php
                                }
                            }
                        ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(CRON_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'DayCroneForAgentToAgentChat' && in_array($segment2, array('DayCroneForAgentToAgentChat')) ? 'active' : ''; ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-envelope"></i> <span>Cron</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu " role="menu" aria-labelledby="dLabel">
                        <?php if ($this->applib->have_access_role(CRON_AGENT_CHAT_MENU_ID, "view")) { ?>
                            <li class="<?= $segment2 == 'DayCroneForAgentToAgentChat' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>frontend/DayCroneForAgentToAgentChat/"
                                   class="sidebar-unread-mails">Agent Chat</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(CRON_LOAD_ALL_STAFFS_UNREAD_MAILS_MENU_ID, "view")) { ?>
                            <li class="">
                                <a href="<?= base_url() ?>mail-system3/save-unread-mails-for-all/" target="_blank">Load
                                    All Staff's Unread Mails</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(CRON_LOAD_REMINDERS_MENU_ID, "view")) { ?>
                            <li class="">
                                <a href="<?= base_url() ?>Cron_reminder/reminderStatus/" target="_blank">Load
                                    Reminders</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(CRON_REMOVE_OLD_FILES_MENU_ID, "view")) { ?>
                            <li class="">
                                <a href="<?= base_url() ?>app/delete_old_files" target="_blank">Remove old files</a>
                            </li>
                        <?php } ?>
                        <li><a href="<?= base_url() ?>sales_order/sales_order_notification" target="_blank">Sales Order Notification</a></li>
                        <li><a href="<?= base_url() ?>app/db_backup" target="_blank">Take a Data Backup</a></li>
                        <li><a href="<?= base_url() ?>app/software_backup" target="_blank">Take a Software Backup</a></li>
                        <li><a href="<?= base_url() ?>app/logs_backup" target="_blank">Take a Logs Backup</a></li>
                        <li><a href="<?= base_url() ?>app/get_quotation_reminder_notify" target="_blank">Quot. Reminder Notify</a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(LOGS_MENU, "view")) { ?>
                <li <?= $segment1 == 'logs' ? 'class="active"' : ''; ?>>
                    <a href="<?= base_url('logs'); ?>">
                        <i class="fa fa-history"></i> <span> Logs</span>
                    </a>
                </li>
            <?php } ?>
            <?php if ($this->applib->have_access_role(DASHBOARD_DEMO_MENU_ID, "view")) { ?>
                <li class="treeview <?= $segment1 == 'demo' && in_array($segment3, array('dashboarddemo', 'payment_receipt_details', 'dispatch_summary', 'payment_reminder_list', 'finance_general_notice')) ? 'active' : ''; ?>">
                    <a href="javascript:void(0);">
                        <i class="fa fa-envelope"></i> <span>Demo</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <?php if ($this->applib->have_access_role(DASHBOARD_DEMO_MENU_ID, "view")) { ?>
                            <li <?= $segment2 == 'dashboarddemo' ? 'class="active"' : ''; ?>>
                                <a href="<?= base_url(); ?>welcome/dashboarddemo"><i class="fa fa-circle-o"></i> Dashboard Demo</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(REPORT_PAYMENT_RECEIPT_DETAILS_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'report' && $segment2 == 'payment_receipt_details' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>report/payment_receipt_details"><i class="fa fa-circle-o"></i> Payment Receipt Details</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(REPORT_DISPATCH_SUMMARY_MENU_ID, "view")) { ?>
                            <li class="<?= $segment1 == 'report' && $segment2 == 'dispatch_summary' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>report/dispatch_summary"><i class="fa fa-circle-o"></i> Dispatch Summary</a>
                            </li>
                        <?php } ?>
                        <?php if ($invoice_view_role && $this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "view")) { ?>
                            <li class=" <?= $segment1 == 'finance' && $segment2 == 'payment_reminder_list' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>finance/payment_reminder_list/"><i class="fa fa-circle-o"></i> Payment Reminder List</a>
                            </li>
                        <?php } ?>
                        <?php if ($this->applib->have_access_role(MASTER_CUSTOMER_REMINDER_SENT, "add")) { ?>
                            <li class=" <?= $segment1 == 'finance' && $segment2 == 'finance_general_notice' ? 'active' : ''; ?>">
                                <a href="<?= base_url() ?>finance/finance_general_notice/"><i class="fa fa-circle-o"></i> General Notice</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<script>
    $(document).ready(function () {
        $(".sidebar-menu").hover(function(){
            $('.treeview-menu').hide();
        });

        $('.sidebar-menu li.active').hover(function(){
            $(this).find('.treeview-menu:first').show();
        });
        
        $('.sidebar-menu li.active').mouseleave(function(){
            $(this).find('.treeview-menu:first').hide();
        });
    });
    
    function determineDropDirection(){
        $(".treeview-menu").each( function(){
            $(this).css({
              "visibility": "hidden",
              "display": "block",
            });
            $(this).parent().removeClass("dropup");
            if ($(this).offset().top + $(this).outerHeight() > $(window).innerHeight() + $(window).scrollTop()){
                $(this).parent().addClass("dropup");
            }
            $(this).removeAttr("style");
        });
        $(".treeview-menu").css("margin-bottom","-45px");
    }
    $(window).on('scroll',function(){
        determineDropDirection();
    });
//    $(window).scroll(determineDropDirection);
</script>