<div class="col-md-3">
	<a href="<?=base_url('mail-system3/compose-mail/compose/')?>" class="btn btn-primary btn-block">Compose</a>
	<a href="<?=base_url('mail-system3/settings/')?>" class="btn btn-primary btn-block margin-bottom">Settings</a>

	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Folders</h3>

			<div class="box-tools">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body no-padding">
			<ul class="nav nav-pills nav-stacked">
				<li class="<?=$current_folder == 'inbox'?'active':''?>">
					<a href="<?=base_url()?>mail-system3/inbox/" data-folder="inbox" class="mailbox-list-inbox-label">
						<i class="fa fa-inbox"></i> Inbox
					</a>
				</li>
				<li class="<?=$current_folder == 'starred'?'active':''?>">
					<a href="<?=base_url()?>mail-system3/starred/" data-folder="starred">
						<i class="fa fa-star-o"></i> Starred
					</a>
				</li>
				<li class="<?=$current_folder == 'sent'?'active':''?>">
					<a href="<?=base_url()?>mail-system3/sent/" data-folder="sent"><i class="fa fa-envelope-o"></i> Sent</a>
				</li>
				<li class="<?=$current_folder == 'drafts'?'active':''?>">
					<a href="<?=base_url()?>mail-system3/drafts/" data-folder="drafts"><i class="fa fa-file-text-o"></i> Drafts</a>
				</li>
				<li class="<?=$current_folder == 'outbox'?'active':''?>">
					<a href="<?=base_url()?>mail-system3/outbox/" data-folder="outbox"><i class="fa fa-archive"></i> Outbox</a>
				</li>
				<li class="<?=$current_folder == 'junk'?'active':''?>">
					<a href="<?=base_url()?>mail-system3/junk/" data-folder="junk"><i class="fa fa-filter"></i> Junk</a>
				</li>
				<li class="<?=$current_folder == 'trash'?'active':''?>">
					<a href="<?=base_url()?>mail-system3/trash/" data-folder="trash"><i class="fa fa-trash-o"></i> Trash</a>
				</li>
			</ul>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /. box -->
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Labels</h3>

			<div class="box-tools">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body no-padding">
			<ul class="nav nav-pills nav-stacked" id="sidebar-mail-label">
				<?php
				$folders = $this->applib->get_staff_mail_folders();
				?>
				<?php
				foreach ($folders as $folder_row) {
					?>
					<li class="<?= $current_folder == $folder_row->folder ? 'active' :''?>">
						<a href="<?=base_url()?>mail-system3/folder-mails/<?=space_to_dash($folder_row->folder);?>" data-folder="<?=space_to_dash($folder_row->folder);?>">
							<i class="fa fa-circle-o text-red"></i> <?=$folder_row->folder;?>
						</a>
					</li>
					<?php
				}
				?>
			</ul>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</div>