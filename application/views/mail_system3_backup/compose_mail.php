<div class="col-md-9">
	<form id="frmComposeMail" action="<?=base_url("mail-system3/compose-mail/$action/")?>" method="post" enctype="multipart/form-data">
		<?php
		if(isset($mail_detail['mail_id'])){
			?>
			<input type="hidden" name="mail_id" value="<?=$mail_detail['mail_id'];?>">
			<?php
		}
		?>
		<div class="box box-primary">
			<div class="overlay hidden">
				<i class="fa fa-refresh fa-spin"></i>
			</div>
			<div class="box-header with-border">
				<?php
				if($action == 'compose'){
					echo '<h3 class="box-title">Compose New Message</h3>';
				}elseif($action == 'forward'){
					echo '<h3 class="box-title">Forward To : </h3>';
				}elseif($action == 'reply'){
					echo '<h3 class="box-title">Reply To : '.$mail_detail['username'].'</h3>';
				}
				?>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="form-group">
					<?php
					if($action == 'compose' || $action == 'forward') {
						echo '<input class="form-control" placeholder="To:" name="to" required="required">';
					} elseif($action == 'reply') {
						echo '<input class="form-control" placeholder="To: ' . $mail_detail['email'] . '" name="to" value="' .$mail_detail['email'].'" required="required">';
					}
					?>
				</div>
				<div class="form-group">
					<?php
					if($action == 'compose'){
						echo '<input class="form-control" placeholder="Subject:" name="subject" required="required">';
					}elseif($action == 'forward'){
						echo '<input class="form-control" placeholder="Subject:" name="subject" value="Fwd: '.$mail_detail['subject'].'" required="required">';
					}elseif($action == 'reply'){
						echo '<input class="form-control" placeholder="Subject:" name="subject" value="Re: '.$mail_detail['subject'].'" required="required">';
					}
					?>
				</div>
				<?php
				if(isset($mail_detail['cc']) && count($mail_detail['cc']) > 0) {
					foreach($mail_detail['cc'] as $cc) {
						?>
						<div class="form-group input-group cc-section">
							<input type="text" class="form-control cc-input" placeholder="Cc:" name="cc[]" value="<?= $cc ?>">
							<span class="input-group-btn">
						  		<button type="button" class="btn btn-danger btn-flat btn-remove-cc">
									<i class="fa fa-trash-o"></i>
							 	</button>
							</span>
						</div>
						<?php
					}
				}else {
					?>
					<div class="form-group cc-section">
						<input type="text" class="form-control" placeholder="Cc:" name="cc[]" value="">
					</div>
					<?php
				}
				?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="request_read_receipt"> Request read receipt
								</label>
							</div>
						</div>
						<div class="col-md-6 text-right">
							<button type="button" class="btn btn-primary btn-flat btn-add-cc">Add more cc</button>
						</div>
					</div>
				</div>
				<div class="form-group input-group cc-section hidden">
					<input type="text" class="form-control cc-input" placeholder="Cc:" value="">
					<span class="input-group-btn">
					  <button type="button" class="btn btn-danger btn-flat btn-remove-cc"><i class="fa fa-trash-o"></i></button>
					</span>
				</div>
				<div class="form-group">
					<?php
					if($action == 'compose'){
						if(isset($settings['signature'])){
							echo '<textarea id="compose-textarea" class="form-control" style="height: 300px" name="body">'.$settings['signature'].'</textarea>';
						}else{
							echo '<textarea id="compose-textarea" class="form-control" style="height: 300px" name="body"></textarea>';
						}
					}elseif($action == 'forward' || $action == 'reply'){
						if (isset($settings['signature'])) {
							echo '<textarea id="compose-textarea" class="form-control" style="height: 300px" name="body">'.$settings['signature'].$mail_detail['body'].'</textarea>';
						} else {
							echo '<textarea id="compose-textarea" class="form-control" style="height: 300px" name="body">' . $mail_detail['body'] . '</textarea>';
						}
					}
					?>
				</div>

				<?php
				if($action == 'forward' || $action == 'reply' || isset($document_url)) {
					?>
					<!--Attachments Files Lists-->
					<ul class="mailbox-attachments clearfix">
						<?php
						if(isset($mail_detail['attachments']) && count($mail_detail['attachments']) > 0 ) { //print_r($mail_detail['attachments']);
							foreach ($mail_detail['attachments'] as $attachment) {
								if(in_array($attachment['subtype'],array('png','jpg','jpeg','gif'))){
									$mime_type = 'image/'.$attachment['subtype'];
									?>
									<li>
										<?php
										if(isset($attachment['url'])) {
											?>
											<span class="mailbox-attachment-icon has-img"><img src="<?=base_url().$attachment['url'];?>" alt="Attachment"></span>
											<?php
										} else {
											?>
											<span class="mailbox-attachment-icon has-img"><img src="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" alt="Attachment"></span>
											<?php
										}
										?>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-camera"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
										</div>
									</li>
									<?php
								}elseif(in_array($attachment['subtype'],array('pdf'))){
									$mime_type = 'application/pdf';
									?>
									<li>
										<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
										</div>
									</li>
									<?php
								}elseif(in_array($attachment['subtype'],array('plain','txt'))){
									$mime_type = 'text/plain';
									?>
									<li>
										<span class="mailbox-attachment-icon"><i class="fa fa-file-text-o"></i></span>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
										</div>
									</li>
									<?php
								}elseif(in_array($attachment['subtype'],array('zip'))){
									$mime_type = 'application/'.$attachment['subtype'];
									?>
									<li>
										<span class="mailbox-attachment-icon"><i class="fa fa-file-zip-o"></i></span>
										<div class="mailbox-attachment-info">
											<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=$attachment['filename'];?></a>
							<span class="mailbox-attachment-size">
								&nbsp;
								<?php
								if(isset($attachment['url'])) {
									?>
									<a href="<?=base_url().$attachment['url'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								} else {
									?>
									<a href="data:<?=$mime_type?>;base64,<?=$attachment['file'];?>" class="btn btn-default btn-xs pull-right" download="<?=$attachment['filename'];?>"><i class="fa fa-cloud-download"></i></a>
									<?php
								}
								?>
							</span>
										</div>
									</li>
									<?php
								}
								?>
								<?php
							}
						}
						?>
						<?php
							if(isset($document_url)){
								?>
								<li>
									<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
									<div class="mailbox-attachment-info">
										<a href="javascript:void(0);" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?=basename($document_url,PATHINFO_FILENAME);?></a>
										<span class="mailbox-attachment-size">
											&nbsp;
											<a href="<?=base_url().$document_url;?>" class="btn btn-default btn-xs pull-right" download="<?=$document_url;?>"><i class="fa fa-cloud-download"></i></a>
										</span>
									</div>
								</li>
								<input type="hidden" name="attachments[]" class="attachment_url" value="<?=$document_url;?>">
						<?php
							}
						?>
					</ul>
					<!--End Attachments Files Lists-->
					<?php
				}
				?>
				<div class="form-group">
					<input type="file" class="file-input-ajax" id="upload_attachment" multiple="multiple">
					<input type="hidden" name="attachments[]" class="attachment_url">
					<p class="help-block">Max. 32MB</p>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<div class="pull-right">
					<button type="submit" class="btn btn-default" name="btn_draft"><i class="fa fa-pencil"></i> Draft</button>
					<button type="submit" class="btn btn-primary" name="btn_send"><i class="fa fa-envelope-o"></i> Send</button>
				</div>
				<button type="reset" id="discardbtnicon" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
			</div>
			<!-- /.box-footer -->
		</div>
	</form>
</div>
<script>
	$(document).ready(function(){
		$(".file-input-ajax").fileinput({
			uploadUrl: "<??>", // server upload action
			uploadAsync: true,
			maxFileCount: 5,
			showPreview:false,
			showUpload:false,
			browseIcon:"<i class='fa fa-paperclip'></i>&nbsp;",
			browseLabel:"Attachment",
			initialPreview: [],
			fileActionSettings: {
				removeIcon: '<i class="icon-bin"></i>',
				removeClass: 'btn btn-link btn-xs btn-icon',
				uploadIcon: '<i class="icon-upload"></i>',
				uploadClass: 'btn btn-link btn-xs btn-icon',
				indicatorNew: '<i class="icon-file-plus text-slate"></i>',
				indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
				indicatorError: '<i class="icon-cross2 text-danger"></i>',
				indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
			}
		});
	});
</script>