<style>
	td.mailbox-subject{
		white-space: nowrap;
		max-width: 620px !important;
		overflow: hidden;
	}
	td.mailbox-attachment{
		max-width: 30px !important;
	}
</style>
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
			$this->load->view('shared/success_false_notify');
		?>
		<h1>
			Mailbox
			<small> <span class="top-no-unread-mails"><?=isset($count_unseen_mails)?$count_unseen_mails:0?></span> new messages</small>
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<?php
				if($this->uri->segment(2) == 'settings' || $this->uri->segment(2) == 'read-mail' || $this->uri->segment(2) == 'read_mail' || isset($action)){
					$this->load->view('mail_system3/folder_list',array('current_folder'=>$current_folder));
				}else{
					$this->load->view('mail_system3/folder_list_ajax',array('current_folder'=>$current_folder));
				}
				echo "<div class='mail_system_detail_area'>";
					if($this->uri->segment(2) == 'read-mail' || $this->uri->segment(2) == 'read_mail') {
						$this->load->view('mail_system3/read_mail', array('mail_detail' => $mail_detail,'mail_id'=>$mail_id,'current_folder'=>$current_folder));
					}elseif(isset($action) && $action == 'compose'){
						if(isset($document_url)){
							$this->load->view('mail_system3/compose_mail',array('action'=>'compose','settings' => $settings,'document_url'=>$document_url));
						}else{
							$this->load->view('mail_system3/compose_mail',array('action'=>'compose','settings' => $settings));
						}
					}elseif(isset($action) && $action == 'forward'){
						$this->load->view('mail_system3/compose_mail',array('action'=>'forward','settings' => $settings,'mail_detail' => $mail_detail));
					}elseif(isset($action) && $action == 'reply'){
						$this->load->view('mail_system3/compose_mail',array('action'=>'reply','settings' => $settings,'mail_detail' => $mail_detail));
					} elseif($this->uri->segment(2) == 'settings'){
						$this->load->view('mail_system3/settings',array('settings' => $settings));
					} else {
						$this->load->view('mail_system3/mail_list',array('current_folder'=>$current_folder));
					}
				echo "</div>";
			?>
		</div>
	</section>
</div>
<div id="modal-create-folder" class="modal fade" role="dialog">
	<div class="modal-dialog" style="max-width: 480px">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-create-folder-header">Create Folder</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="<?=base_url()?>mail-system3/create-folder/" id="frm-create-folder" class="form-horizontal frm-create-folder">
						<input type="hidden" name="current_folder" value="<?=$current_folder?>" >
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label input-sm">Folder Name</label>
										<div class="col-sm-8">
											<input type="text" class="form-control input-sm folder" id="folder" name="folder" required>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm btn-close-add-folder-modal" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-default btn-sm sbt-create-folder">Create</button>
			</div>
		</div>

	</div>
</div>
<form id="frm_mailbox" action="" method="post">
	<input type="hidden" name="action" class="action" value="">
	<input type="hidden" name="mail_id" class="mail_id">
	<input type="hidden" name="current_folder" class="current_folder" id="current_folder" value="<?=$current_folder?>">
	<input type="hidden" name="folder" class="folder" id="folder">
</form>
<script>
	var current_folder = '<?=$current_folder?>';
	var loader = "";
	loader += '<div class="box">';
	loader += '<div class="overlay">';
	loader += '<i class="fa fa-refresh fa-spin"></i>';
	loader += '</div>';
	loader += '<div class="box-header with-border">';
	loader += '<h3 class="box-title">Loading...</h3>';
	loader += '</div>';
	loader += '</div>';

	$(document).ready(function(){
		set_unread_mails();
		var MailListTable = $('#table-mail-list').DataTable({
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				"url": "<?=base_url("mail-system3/mails-datatable/")?>",
				"type": "POST",
				"data": function ( d ) {
					d.folder = $("#current_folder").val();
				}
			},
			"columnDefs": [
				{
					"targets": [0,1,2,3,4,5],
					"orderable": false
				}
			],
			"columns": [
				{ },
				{ "className":'mailbox-star'},
				{ "className":'mailbox-name'},
				{ "className":'mailbox-subject'},
				{ "className":'mailbox-attachment'},
				{ "className":'mailbox-date'}
			],
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"pagingType": "simple",
			"language": {
				"paginate": {
					"previous": '<i class="fa fa-chevron-left"></i>',
					"next": '<i class="fa fa-chevron-right"></i>'
				}
			},
			"drawCallback":function(){
				$('.mailbox-messages input[type="checkbox"]').iCheck({
					checkboxClass: 'icheckbox_flat-blue',
					radioClass: 'iradio_flat-blue'
				});
			}
		});

		$('#table-mail-list_filter').hide();

		$(document).on('keyup','.txt-search-mail',function(){
			MailListTable.search($(this).val()).draw() ;
		});
		$(document).on('click','.btn-mails-move-to',function(){
			console.log($(this).data('folder'));
			$("#frm-mail-list").find('input[name="to_folder"]').val($(this).data('folder'));
			$("#frm-mail-list").attr('action','<?=base_url()?>mail-system3/mail_move_to/').submit();
		});

		$(document).on('submit','#frm-mail-list',function(e){
			e.preventDefault();
			$.ajax({
				url:$(this).attr('action'),
				type: 'POST',
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success:function(data){
					MailListTable.draw();
					show_notify(data.message,data.success);
				}
			});
		});

		$(document).on('click',".checkbox-toggle",function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$(".mailbox-messages input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks",!clicks);
		});

		$(document).on('click',".mailbox-star",function (e) {
			e.preventDefault();
			var $this = $(this).find("a > i");
			var glyph = $this.hasClass("glyphicon");
			var fa = $this.hasClass("fa");
			//Switch states
			if (glyph) {
				$this.toggleClass("glyphicon-star");
				$this.toggleClass("glyphicon-star-empty");
			}
			if (fa) {
				$this.toggleClass("fa-star");
				$this.toggleClass("fa-star-o");
			}
		});

		$(document).on('click','.btn-star',function(){
			var star_button = $(this);
			var mail_id = $(this).data('mail_id');
			if($(this).find('.fa-star').length == 0){
				var url = "<?=base_url()?>mail-system3/set-starred-flag/"+mail_id;
			}else{
				var url = "<?=base_url()?>mail-system3/remove-starred-flag/"+mail_id;
			}
			if(current_folder == 'starred'){
				star_button.closest('tr').fadeOut('slow');
			}
			$.ajax({
				url: url,
				type: "GET",
				data: null,
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {

				}
			});
		});

		$(document).on('click','.btn-view-folder',function(){
			$('.btn-folder').each(function(){
				$(this).addClass('btn-view-folder');
				$(this).closest('li').removeClass('active');
			});
			$(this).removeClass('btn-view-folder').closest('li').addClass('active');
			current_folder = $(this).data('folder');
			$('.folder-name').text(current_folder);
			$("#current_folder").val(current_folder);
			MailListTable.draw();
		});

		$(document).on('click','.sbt-create-folder',function(){
			$('#frm-create-folder').submit();
		});

		$(document).on('click','.btn-mail-pagination',function(e){
			$("#ajax-loader").show();
			var mail_id = $(this).data('mail_id');
			$.ajax({
				url: "<?=base_url()?>mail-system3/read-mail/"+mail_id,
				type: "POST",
				data: null,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					set_unread_mails();
					$(".mail_system_detail_area").html(data.mail_system_detail_area);
					$("#ajax-loader").hide();
				},
				error: function (msg) {
					$("#ajax-loader").hide();
				}
			});
		});

		$(document).on('submit','#frm-create-folder',function(e){
			e.preventDefault();
			$("#modal-create-folder-header").text('Processing...');
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					console.log(data);
					if(data.success == true){
						$('#select-folder-header').html(data.move_to_folder);
						$('#select-folder-footer').html(data.move_to_folder);
						$('#sidebar-mail-label').html(data.sidebar_mail_label);
						show_notify(data.message,true);
						$('#modal-create-folder').modal('hide');
					}else{
						show_notify(data.message,false);
					}
					$("#modal-create-folder-header").text('Create Folder');
				}
			});
		});

		$(document).on('click','.btn-trash-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/delete-mail');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-forward-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/forward');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-reply-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/reply');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-refresh-mail',function(){
			var ButtonObj = $(this);
			ButtonObj.html('<i class="fa fa-refresh fa-spin"></i> Refreshing...');
			$.ajax({
				url: '<?=base_url()?>mail-system3/save-unread-mails',
				type: "GET",
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					set_unread_mails();
					MailListTable.draw();
					ButtonObj.html('<i class="fa fa-refresh"></i>');
				}
			});
		});

		$(document).on('click','.btn-reply-mail-list',function(){
			var i = 1;
			$('.chk-select-mail:checked').each(function() {
				if(i == 1){
					$("form#frm_mailbox").find('.mail_id').val(this.value);
					$("form#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/reply');
					$("form#frm_mailbox").submit();
					return false;
				}
				i++;
			});
		});
		$(document).on('click','.btn-forward-mail-list',function(){
			var i = 1;
			$('.chk-select-mail:checked').each(function() {
				if(i == 1){
					$("form#frm_mailbox").find('.mail_id').val(this.value);
					$("form#frm_mailbox").attr('action','<?=base_url()?>mail-system3/compose-mail/forward');
					$("form#frm_mailbox").submit();
					return false;
				}
				i++;
			});
		});

		$(document).on('submit','#frm-settings',function(e){
			e.preventDefault();
			$('.btn-save-settings').html('Saving...');
			$.ajax({
				url: '<?=base_url()?>mail-system3/save-settings/',
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					$('.btn-save-settings').html('Save');
					if(data.success == true){
						show_notify(data.message,true);
					}else{
						show_notify('Something wrong!',false);
					}
				}
			});
		});

		$(document).on('click','.fileinput-remove',function(){
			$("input.attachment_url").each(function(index){
				if(index == 0){
					$(this).val('');
				}else{
					$(this).remove();
				}
			});
		});
		$(document).on('click','.btn-remove-cc',function(){
			$(this).closest('.cc-section').fadeOut('medium',function(){$(this).remove();});
		});
		$(document).on('click','.btn-add-cc',function(){
			var cc_section = $('.cc-section.hidden').clone();
			cc_section.removeClass('hidden').find('input.cc-input').attr('name','cc[]');
			cc_section.insertAfter('.cc-section:not(.hidden):last');
		});
	});
	function set_unread_mails() {
		$.ajax({
			url: "<?=base_url('app/get_inbox_unread_mails/');?>",
			type: "GET",
			data: null,
			contentType: false,
			cache: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.no_unread_mails > 0){
					$('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">' + data.no_unread_mails + '</span>');
					$('.sidebar-unread-mails').append('<span class="pull-right-container"><span class="label label-primary pull-right">' + data.no_unread_mails + '</span></span>');
					$('.top-no-unread-mails').text(data.no_unread_mails);
					if ($('.mailbox-list-inbox-label').find('.no-unread-mails').length == 0) {
						$('.mailbox-list-inbox-label').append('<span class="label label-primary pull-right no-unread-mails">' + data.no_unread_mails + '</span>');
					} else {
						$('.mailbox-list-inbox-label').find('.no-unread-mails').text(data.no_unread_mails);
					}
					$('.messages-menu').find('ul.dropdown-menu').remove();
					$('.messages-menu').append(data.unread_mails);
				}else{
					$('.messages-menu').find('ul.dropdown-menu').remove();
					$('.mailbox-list-inbox-label').find('.no-unread-mails').remove();
					$('.unread-mail-icon').find('.inbox-unread-mails').remove();
					$('.sidebar-unread-mails').find('.pull-right-container').remove();
					$('.top-no-unread-mails').text(0);
				}

			}
		});
	}
</script>
<?php
if($this->uri->segment(2) == 'settings') {
	?>
	<script>
		$(function () {
			$("#signature-textarea").wysihtml5();
		});
	</script>
	<?php
}
if($this->uri->segment(2) == 'compose-mail' || $this->uri->segment(2) == 'compose_mail'
|| $this->uri->segment(2) == 'document-mail' || $this->uri->segment(2) == 'document_mail') {
	?>
	<script src="<?= plugins_url('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
	<script>
		$(function () {
			$("#compose-textarea").wysihtml5();
			$('#discardbtnicon').click(function () {
				window.location.href = '<?=base_url()?>mail-system3/inbox/';
			});
			$(document).on('change', '#upload_attachment', function () {
				$("form#frmComposeMail").find('.overlay').removeClass('hidden');
				var formdata = new FormData();
				formdata.append("upload_attachment", $('#upload_attachment')[0].files[0]);
				$.ajax({
					url: "<?=base_url('mail-system3/upload_attachment/');?>",
					type: "POST",
					data: formdata,
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function (data) {
						$("form#frmComposeMail").find('.overlay').addClass('hidden');
						if (data.success == true) {
							show_notify(data.message, true);
							if($(".attachment_url:last").val() == ''){
								$(".attachment_url:last").val(data.attachment_url);
							}else{
								var attach_html = $(".attachment_url:last").clone();
								attach_html.insertAfter($(".attachment_url:last"));
								$(".attachment_url:last").val(data.attachment_url);
							}
						} else {
							show_notify(data.message, false);
						}
					}
				});
			});
		});
	</script>
	<?php
}
?>