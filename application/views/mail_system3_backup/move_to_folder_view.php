<li class="dropdown-header">Move To</li>
<li class=""><a href="javascript:void(0);" class="btn-mails-move-to" data-folder="inbox">Inbox</a></li>
<?php
	$folders = $this->applib->get_staff_mail_folders();
?>
<?php foreach($folders as $folder_row) {
	?>
	<li class=""><a href="javascript:void(0);" class="btn-mails-move-to" data-folder="<?=$folder_row->folder;?>"><?=$folder_row->folder;?></a></li>
	<?php
}
?>
<li><a href="#" data-toggle="modal" data-target="#modal-create-folder" class="btn-create-folder">Create new</a></li>