<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form class="form-horizontal" action="<?= base_url('master_challan/save_default_challan_detail') ?>" method="post" id="save_default_challan_detail" novalidate>
        <section class="content-header">
            <h1>
                <small class="text-primary text-bold">Default Challan Details</small>
            </h1>
        </section>
        <div class="clearfix">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Horizontal Form -->
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="col-md-11">
                                        <h5 class="text-center"><b>Motor Serial No. Details</b></h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_user" class="input-sm"> User </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_make" class="input-sm"> Make </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_hp" class="input-sm"> HP </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_kw" class="input-sm"> KW </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_frequency" class="input-sm"> Frequency </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_rpm" class="input-sm"> RPM </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="motor_volts_cycles" class="input-sm"> Volts </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="motor_detail_fields">
                                        <div id="motor_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_user_1" id="m_user_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_user as $motor_user_row) { ?>
                                                                <option value="<?= $motor_user_row->id; ?>" <?= isset($default_details_arr['m_user_1']) && $motor_user_row->id == $default_details_arr['m_user_1'] ? 'selected="selected"' : ''; ?>><?= $motor_user_row->user_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_make_1" id="m_make_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_make as $motor_make_row) { ?>
                                                                <option value="<?= $motor_make_row->id; ?>" <?= isset($default_details_arr['m_make_1']) && $motor_make_row->id == $default_details_arr['m_make_1'] ? 'selected="selected"' : ''; ?>><?= $motor_make_row->make_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_hp_1" id="m_hp_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_hp as $motor_hp_row) { ?>
                                                                <option value="<?= $motor_hp_row->id; ?>" <?= isset($default_details_arr['m_hp_1']) && $motor_hp_row->id == $default_details_arr['m_hp_1'] ? 'selected="selected"' : ''; ?>><?= $motor_hp_row->hp_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_kw_1" id="m_kw_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_kw as $motor_kw_row) { ?>
                                                                <option value="<?= $motor_kw_row->id; ?>" <?= isset($default_details_arr['m_kw_1']) && $motor_kw_row->id == $default_details_arr['m_kw_1'] ? 'selected="selected"' : ''; ?>><?= $motor_kw_row->kw_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_frequency_1" id="m_frequency_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_frequency as $motor_frequency_row) { ?>
                                                                <option value="<?= $motor_frequency_row->id; ?>" <?= isset($default_details_arr['m_frequency_1']) && $motor_frequency_row->id == $default_details_arr['m_frequency_1'] ? 'selected="selected"' : ''; ?>><?= $motor_frequency_row->frequency_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_rpm_1" id="m_rpm_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_rpm as $motor_rpm_row) { ?>
                                                                <option value="<?= $motor_rpm_row->id; ?>" <?= isset($default_details_arr['m_rpm_1']) && $motor_rpm_row->id == $default_details_arr['m_rpm_1'] ? 'selected="selected"' : ''; ?>><?= $motor_rpm_row->rpm_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_volts_1" id="m_volts_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_volts_cycles as $motor_volts_cycles_row) { ?>
                                                                <option value="<?= $motor_volts_cycles_row->id; ?>" <?= isset($default_details_arr['m_volts_1']) && $motor_volts_cycles_row->id == $default_details_arr['m_volts_1'] ? 'selected="selected"' : ''; ?>><?= $motor_volts_cycles_row->volts_cycles_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="motor_detail_fields">
                                        <div id="motor_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_user_2" id="m_user_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_user as $motor_user_row) { ?>
                                                                <option value="<?= $motor_user_row->id; ?>" <?= isset($default_details_arr['m_user_2']) && $motor_user_row->id == $default_details_arr['m_user_2'] ? 'selected="selected"' : ''; ?>><?= $motor_user_row->user_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_make_2" id="m_make_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_make as $motor_make_row) { ?>
                                                                <option value="<?= $motor_make_row->id; ?>" <?= isset($default_details_arr['m_make_2']) && $motor_make_row->id == $default_details_arr['m_make_2'] ? 'selected="selected"' : ''; ?>><?= $motor_make_row->make_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_hp_2" id="m_hp_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_hp as $motor_hp_row) { ?>
                                                                <option value="<?= $motor_hp_row->id; ?>" <?= isset($default_details_arr['m_hp_2']) && $motor_hp_row->id == $default_details_arr['m_hp_2'] ? 'selected="selected"' : ''; ?>><?= $motor_hp_row->hp_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_kw_2" id="m_kw_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_kw as $motor_kw_row) { ?>
                                                                <option value="<?= $motor_kw_row->id; ?>" <?= isset($default_details_arr['m_kw_2']) && $motor_kw_row->id == $default_details_arr['m_kw_2'] ? 'selected="selected"' : ''; ?>><?= $motor_kw_row->kw_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_frequency_2" id="m_frequency_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_frequency as $motor_frequency_row) { ?>
                                                                <option value="<?= $motor_frequency_row->id; ?>" <?= isset($default_details_arr['m_frequency_2']) && $motor_frequency_row->id == $default_details_arr['m_frequency_2'] ? 'selected="selected"' : ''; ?>><?= $motor_frequency_row->frequency_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_rpm_2" id="m_rpm_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_rpm as $motor_rpm_row) { ?>
                                                                <option value="<?= $motor_rpm_row->id; ?>" <?= isset($default_details_arr['m_rpm_2']) && $motor_rpm_row->id == $default_details_arr['m_rpm_2'] ? 'selected="selected"' : ''; ?>><?= $motor_rpm_row->rpm_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_volts_2" id="m_volts_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_volts_cycles as $motor_volts_cycles_row) { ?>
                                                                <option value="<?= $motor_volts_cycles_row->id; ?>" <?= isset($default_details_arr['m_volts_2']) && $motor_volts_cycles_row->id == $default_details_arr['m_volts_2'] ? 'selected="selected"' : ''; ?>><?= $motor_volts_cycles_row->volts_cycles_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="motor_detail_fields">
                                        <div id="motor_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_user_3" id="m_user_3" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_user as $motor_user_row) { ?>
                                                                <option value="<?= $motor_user_row->id; ?>" <?= isset($default_details_arr['m_user_3']) && $motor_user_row->id == $default_details_arr['m_user_3'] ? 'selected="selected"' : ''; ?>><?= $motor_user_row->user_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_make_3" id="m_make_3" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_make as $motor_make_row) { ?>
                                                                <option value="<?= $motor_make_row->id; ?>" <?= isset($default_details_arr['m_make_3']) && $motor_make_row->id == $default_details_arr['m_make_3'] ? 'selected="selected"' : ''; ?>><?= $motor_make_row->make_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_hp_3" id="m_hp_3" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_hp as $motor_hp_row) { ?>
                                                                <option value="<?= $motor_hp_row->id; ?>" <?= isset($default_details_arr['m_hp_3']) && $motor_hp_row->id == $default_details_arr['m_hp_3'] ? 'selected="selected"' : ''; ?>><?= $motor_hp_row->hp_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_kw_3" id="m_kw_3" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_kw as $motor_kw_row) { ?>
                                                                <option value="<?= $motor_kw_row->id; ?>" <?= isset($default_details_arr['m_kw_3']) && $motor_kw_row->id == $default_details_arr['m_kw_3'] ? 'selected="selected"' : ''; ?>><?= $motor_kw_row->kw_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_frequency_3" id="m_frequency_3" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_frequency as $motor_frequency_row) { ?>
                                                                <option value="<?= $motor_frequency_row->id; ?>" <?= isset($default_details_arr['m_frequency_3']) && $motor_frequency_row->id == $default_details_arr['m_frequency_3'] ? 'selected="selected"' : ''; ?>><?= $motor_frequency_row->frequency_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_rpm_3" id="m_rpm_3" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_rpm as $motor_rpm_row) { ?>
                                                                <option value="<?= $motor_rpm_row->id; ?>" <?= isset($default_details_arr['m_rpm_3']) && $motor_rpm_row->id == $default_details_arr['m_rpm_3'] ? 'selected="selected"' : ''; ?>><?= $motor_rpm_row->rpm_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_volts_3" id="m_volts_3" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_volts_cycles as $motor_volts_cycles_row) { ?>
                                                                <option value="<?= $motor_volts_cycles_row->id; ?>" <?= isset($default_details_arr['m_volts_3']) && $motor_volts_cycles_row->id == $default_details_arr['m_volts_3'] ? 'selected="selected"' : ''; ?>><?= $motor_volts_cycles_row->volts_cycles_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="motor_detail_fields">
                                        <div id="motor_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_user_4" id="m_user_4" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_user as $motor_user_row) { ?>
                                                                <option value="<?= $motor_user_row->id; ?>" <?= isset($default_details_arr['m_user_4']) && $motor_user_row->id == $default_details_arr['m_user_4'] ? 'selected="selected"' : ''; ?>><?= $motor_user_row->user_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_make_4" id="m_make_4" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_make as $motor_make_row) { ?>
                                                                <option value="<?= $motor_make_row->id; ?>" <?= isset($default_details_arr['m_make_4']) && $motor_make_row->id == $default_details_arr['m_make_4'] ? 'selected="selected"' : ''; ?>><?= $motor_make_row->make_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_hp_4" id="m_hp_4" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_hp as $motor_hp_row) { ?>
                                                                <option value="<?= $motor_hp_row->id; ?>" <?= isset($default_details_arr['m_hp_4']) && $motor_hp_row->id == $default_details_arr['m_hp_4'] ? 'selected="selected"' : ''; ?>><?= $motor_hp_row->hp_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_kw_4" id="m_kw_4" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_kw as $motor_kw_row) { ?>
                                                                <option value="<?= $motor_kw_row->id; ?>" <?= isset($default_details_arr['m_kw_4']) && $motor_kw_row->id == $default_details_arr['m_kw_4'] ? 'selected="selected"' : ''; ?>><?= $motor_kw_row->kw_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_frequency_4" id="m_frequency_4" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_frequency as $motor_frequency_row) { ?>
                                                                <option value="<?= $motor_frequency_row->id; ?>" <?= isset($default_details_arr['m_frequency_4']) && $motor_frequency_row->id == $default_details_arr['m_frequency_4'] ? 'selected="selected"' : ''; ?>><?= $motor_frequency_row->frequency_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_rpm_4" id="m_rpm_4" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_rpm as $motor_rpm_row) { ?>
                                                                <option value="<?= $motor_rpm_row->id; ?>" <?= isset($default_details_arr['m_rpm_4']) && $motor_rpm_row->id == $default_details_arr['m_rpm_4'] ? 'selected="selected"' : ''; ?>><?= $motor_rpm_row->rpm_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="m_volts_4" id="m_volts_4" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($motor_volts_cycles as $motor_volts_cycles_row) { ?>
                                                                <option value="<?= $motor_volts_cycles_row->id; ?>" <?= isset($default_details_arr['m_volts_4']) && $motor_volts_cycles_row->id == $default_details_arr['m_volts_4'] ? 'selected="selected"' : ''; ?>><?= $motor_volts_cycles_row->volts_cycles_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <h5 class="text-center"><b>Gearbox Serial No. Details</b></h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_user" class="input-sm"> User </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_make" class="input-sm"> Make </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_gear_type" class="input-sm"> Gear Type </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_model" class="input-sm"> Model </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding: 0px 2px;">
                                            <div class="form-group text-center">
                                                <label for="gearbox_ratio" class="input-sm"> Ratio </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="gearbox_detail_fields">
                                        <div id="gearbox_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_user_1" id="g_user_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_user as $gearbox_user_row) { ?>
                                                                <option value="<?= $gearbox_user_row->id; ?>" <?= isset($default_details_arr['g_user_1']) && $gearbox_user_row->id == $default_details_arr['g_user_1'] ? 'selected="selected"' : ''; ?>><?= $gearbox_user_row->user_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_make_1" id="g_make_1" class="gearbox_make form-control input-sm select2 item_data" id="gearbox_make" >
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_make as $gearbox_make_row) { ?>
                                                                <option value="<?= $gearbox_make_row->id; ?>" <?= isset($default_details_arr['g_make_1']) && $gearbox_make_row->id == $default_details_arr['g_make_1'] ? 'selected="selected"' : ''; ?>><?= $gearbox_make_row->make_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_gear_type_1" id="g_gear_type_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_gear_type as $gearbox_gear_type_row) { ?>
                                                                <option value="<?= $gearbox_gear_type_row->id; ?>" <?= isset($default_details_arr['g_gear_type_1']) && $gearbox_gear_type_row->id == $default_details_arr['g_gear_type_1'] ? 'selected="selected"' : ''; ?>><?= $gearbox_gear_type_row->gear_type_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_model_1" id="g_model_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_model as $gearbox_model_row) { ?>
                                                                <option value="<?= $gearbox_model_row->id; ?>" <?= isset($default_details_arr['g_model_1']) && $gearbox_model_row->id == $default_details_arr['g_model_1'] ? 'selected="selected"' : ''; ?>><?= $gearbox_model_row->model_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_ratio_1" id="g_ratio_1" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_ratio as $gearbox_ratio_row) { ?>
                                                                <option value="<?= $gearbox_ratio_row->id; ?>" <?= isset($default_details_arr['g_ratio_1']) && $gearbox_ratio_row->id == $default_details_arr['g_ratio_1'] ? 'selected="selected"' : ''; ?>><?= $gearbox_ratio_row->ratio_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="gearbox_detail_fields">
                                        <div id="gearbox_fields">
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_user_2" id="g_user_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_user as $gearbox_user_row) { ?>
                                                                <option value="<?= $gearbox_user_row->id; ?>" <?= isset($default_details_arr['g_user_2']) && $gearbox_user_row->id == $default_details_arr['g_user_2'] ? 'selected="selected"' : ''; ?>><?= $gearbox_user_row->user_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_make_2" id="g_make_2" class="gearbox_make form-control input-sm select2 item_data" id="gearbox_make" >
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_make as $gearbox_make_row) { ?>
                                                                <option value="<?= $gearbox_make_row->id; ?>" <?= isset($default_details_arr['g_make_2']) && $gearbox_make_row->id == $default_details_arr['g_make_2'] ? 'selected="selected"' : ''; ?>><?= $gearbox_make_row->make_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_gear_type_2" id="g_gear_type_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_gear_type as $gearbox_gear_type_row) { ?>
                                                                <option value="<?= $gearbox_gear_type_row->id; ?>" <?= isset($default_details_arr['g_gear_type_2']) && $gearbox_gear_type_row->id == $default_details_arr['g_gear_type_2'] ? 'selected="selected"' : ''; ?>><?= $gearbox_gear_type_row->gear_type_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_model_2" id="g_model_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_model as $gearbox_model_row) { ?>
                                                                <option value="<?= $gearbox_model_row->id; ?>" <?= isset($default_details_arr['g_model_2']) && $gearbox_model_row->id == $default_details_arr['g_model_2'] ? 'selected="selected"' : ''; ?>><?= $gearbox_model_row->model_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 0px 2px;">
                                                    <div class="form-group">
                                                        <select name="g_ratio_2" id="g_ratio_2" class="form-control input-sm select2">
                                                            <option value=""> - Select - </option>
                                                            <?php foreach ($gearbox_ratio as $gearbox_ratio_row) { ?>
                                                                <option value="<?= $gearbox_ratio_row->id; ?>" <?= isset($default_details_arr['g_ratio_2']) && $gearbox_ratio_row->id == $default_details_arr['g_ratio_2'] ? 'selected="selected"' : ''; ?>><?= $gearbox_ratio_row->ratio_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($this->applib->have_access_role(MASTER_CHALLAN_DEFAULT_DETAILS,"edit")) { ?>
                                        <button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Update</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        $(document).on('submit', '#save_default_challan_detail', function () {
            $('.module_save_btn').attr('disabled', 'disabled');
            $("#ajax-loader").show();
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('master_challan/save_default_challan_detail') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json['success'] == 'Updated') {
                        window.location.href = "<?php echo base_url('master_challan/default_challan_detail') ?>";
                    }
                    $("#ajax-loader").hide();
                    return false;
                },
            });
            return false;
        });
    });
</script>
