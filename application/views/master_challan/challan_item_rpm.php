<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		$isEdit = $this->app_model->have_access_role(MASTER_CHALLAN_ITEM_RPM, "edit");
		$isDelete = $this->app_model->have_access_role(MASTER_CHALLAN_ITEM_RPM, "delete");
		$isAdd = $this->app_model->have_access_role(MASTER_CHALLAN_ITEM_RPM, "add");
		?>
		<h1>
			<small class="text-primary text-bold">RPM</small>
		</h1>
		<!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->

	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					<div class="col-md-12">
						<div class="col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading">
									RPM Detail
								</div>
								<div style="margin: 10px;">
									<table id="example1" class="table custom-table sales-table">
										<thead>
											<tr>
												<th>Action</th>
												<th>RPM Name</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(!empty($results)) {
												foreach ($results as $row) {
											?>
											<tr>
												<td>
													<?php if($isEdit) { ?>
													<a href="<?= base_url('master_challan/rpm/'.$row->id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a>
													<?php } if($isDelete) { ?>
													<a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('master_challan/delete/'.$row->id);?>"><i class="fa fa-trash"></i></a>
													<?php } ?>
												</td>
												<td><?=$row->rpm_name ?></td>
											</tr>
											<?php 
												} }
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<?php if($isAdd || $isEdit) { ?>
						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-heading clearfix">
									<?php if(isset($id) && !empty($id)){ ?>Edit 
									<?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
									<?php } ?>RPM
								</div>
								<div style="margin:10px">	
									<form method="POST" 
										  <?php if(isset($id) && !empty($id)){ ?>
										  action="<?=base_url('master_challan/update_rpm') ?>" 
										  <?php } else { ?>
										  action="<?=base_url('master_challan/add_rpm') ?>" 
										  <?php } ?> id="form_day">
										<?php if(isset($id) && !empty($id)){ ?>
										<input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo $id; ?>" >
										<?php } ?>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-4 input-sm">RPM<span class="required-sign">*</span></label>
											<div class="col-sm-7">
												<input type="text" class="form-control input-sm" id="rpm_name" name="rpm_name" value="<?php echo $rpm_name;?>" <?php echo $btn_disable;?>>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="form-group" style="margin:7px !important;"></div>
										<?php if(isset($id) && !empty($id)){ ?>
										<button type="submit" class="btn btn-info btn-block btn-xs">Edit RPM</button>
										<?php } else { ?>
										<button type="submit" class="btn btn-info btn-block btn-xs <?php echo $btn_disable;?>">Add RPM</button>
										<?php } ?>
									</form>
								</div>
							</div>                                            
						</div>
						<?php } ?>

					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>

</div>

<script>
	$(document).ready(function(){
		$("#example1").DataTable({
			"scrollY":        "300px",
			"scrollCollapse": true,
			"paging":         false,
			"order": [[ 1, "asc" ]]
		});

		$("#form_day").on("submit",function(e){
			e.preventDefault();
			if($("#rpm_name").val() == ""){
				show_notify('Fill value User.', false);
				return false;
			}
			<?php if(isset($id) && !empty($id)){ ?>
			var success_status = check_is_unique('challan_item_rpm','rpm_name',$("#rpm_name").val(),'id','<?=$id?>');
			<?php } else { ?>
			var success_status = check_is_unique('challan_item_rpm','rpm_name',$("#rpm_name").val());
			<?php } ?>	
			if(success_status == 0){
				if($('p.rpm_name-unique-error').length > 0){
					$("p.rpm_name-unique-error").text('RPM Name already exist!');
				}else{
					$("#rpm_name").after("<p class='text-danger rpm_name-unique-error'>RPM Name already exist!</p>");
				}
				return false;
			}else{
				$("p.rpm_name-unique-error").text(' ');
			}
			var url = '<?php echo base_url('master_hr/delete/') ?>';
			var value = $("#rpm_name").val();
			if(value != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
						/*url += '/' + data.id;
						var TableRow = '<tr>';
		                TableRow += '<td><a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="'+url+'"><i class="fa fa-trash"></i></a></td>';
		                TableRow += '<td>'+data.day_name+'</td>';
		                TableRow += '</tr>';
		                $('.days-table > tbody > tr:last ').after(TableRow);
		                $("#form_day")[0].reset();
		                show_notify('Saved Successfully!',true);*/
						window.location.href = "<?php echo base_url('master_challan/rpm') ?>";
					}
				});
			}
		});

		$(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=challan_item_rpm',
					success: function(data){
						tr.remove();
						window.location.href = "<?php echo base_url('master_challan/rpm') ?>";
					}
				});
			}
		});

	});
</script>
