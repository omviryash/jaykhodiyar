<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$folder?></h3>
        <div class="col-md-7 box-tools pull-right">
            <form action="<?=base_url()?>mail-system2/search-mails/" method="post">
                <input type="hidden" name="mailbox_name" value="<?=$folder?>">
                <div class="input-group margin">
                    <input type="text" name="search" class="form-control input-sm txt-search-mail" placeholder="Search Mail" value="<?=isset($search)?$search:''?>" required="required">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-xs btn-search-mail"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="mailbox-controls">
            <!-- Check all button -->
            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
            </button>
            <div class="btn-group">
                <?php
                if($folder != 'starred') {
                    ?>
                    <button type="button" class="btn btn-default btn-sm btn-mails-move-to" data-folder="trash">
                        <i class="fa fa-trash-o"></i>
                    </button>
                    <?php
                }
                ?>
                <button type="button" class="btn btn-default btn-sm btn-reply-mail-list"><i class="fa fa-reply"></i></button>
                <button type="button" class="btn btn-default btn-sm btn-forward-mail-list"><i class="fa fa-share"></i></button>
            </div>
            <!-- /.btn-group -->
            <button type="button" class="btn dropdown-toggle btn-default btn-sm btn-refresh-mail" data-toggle="dropdown"><i class="fa fa-refresh"></i></button>
            <?php
            if($folder != 'starred') {
                ?>
            <span class="dropdown">
                <button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown"><i
                        class="fa fa-folder-o"></i></button>
                <ul id="select-folder-header" class="dropdown-menu select-folder">
                    <?php
                        $this->load->view('mail_system2/move_to_folder_view', array('current_folder' => $folder));
                    ?>
                </ul>
            </span>
                <?php
            }
            ?>
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                </div>
                <!-- /.btn-group -->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="mailbox-messages">
            <form method="post" action="" id="frm-mail-inbox">
                <input type="hidden" name="from_folder" value="<?=$folder;?>">
                <input type="hidden" name="folder" value="<?=$folder;?>">
                <table class="table table-hover table-striped table-mail-list" id="table-mail-list">
                    <thead>
                    </thead>
                </table>
            </form>
            <!-- /.table -->
        </div>
        <!-- /.mail-box-messages -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer no-padding">
    </div>
</div>
<!-- /. box -->
<script>
    var table;
    $(document).ready(function(){
        table = $('#table-mail-list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('mail-system2/mails-datatable/')?>",
                "type": "POST",
                "data": {'folder':'<?=$folder;?>'}
            },
            "columnDefs": [
                {
                   "targets": [0,1],
                   "orderable": false
                }
            ],
            "columns": [
                { "className":'mailbox-star'},
                { "className":'mailbox-list'}
            ],
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "pagingType": "simple",
            "language": {
                "paginate": {
                    "previous": '<i class="fa fa-chevron-left"></i>',
                    "next": '<i class="fa fa-chevron-right"></i>'
                }
            },
            "initComplete":function(){
                $('.mailbox-messages input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue'
                });
            }
        });


        $(document).on('click','.btn-refresh-mail',function(){
            var ButtonObj = $(this);
            ButtonObj.html('<i class="fa fa-refresh fa-spin"></i> Refreshing');
            $.ajax({
                url: '<?=base_url()?>mail-system2/save-unread-mails',
                type: "GET",
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    table.draw();
                    ButtonObj.html('<i class="fa fa-refresh"></i> Refresh');
                }
            });
        });

        $('.dataTables_filter').addClass('hidden');

        $('#table-mail-list').find('thead').addClass('hidden');

        $(document).on('click','.btn-search-mail',function(){
            $('.txt-search-mail').trigger('keyup');
        });
        $(document).on('keyup','.txt-search-mail',function(){
            table.search(this.value).draw();
        });

        $(document).on('click','.btn-reply-mail-list',function(){
            var i = 1;
            var folder = '<?=$folder;?>';
            $('.chk-select-mail:checked').each(function() {
                if(i == 1){
                    $("form#frm_mailbox").find('.mail_id').val(this.value);
                    $("form#frm_mailbox").find('.action').val('reply_mail');
                    $("form#frm_mailbox").attr('action','<?=base_url()?>mail-system2/compose-email/reply');
                    $("form#frm_mailbox").submit();
                    return false;
                }
                i++;
            });
        });
        $(document).on('click','.btn-forward-mail-list',function(){
            var i = 1;
            var folder = '<?=$folder;?>';
            $('.chk-select-mail:checked').each(function() {
                if(i == 1){
                    $("form#frm_mailbox").find('.mail_id').val(this.value);
                    $("form#frm_mailbox").find('.action').val('forward_mail');
                    $("form#frm_mailbox").attr('action','<?=base_url()?>mail-system2/compose-email/forward');
                    $("form#frm_mailbox").submit();
                    return false;
                }
                i++;
            });
        });


        $(document).on('click','.btn-delete-multi-mail',function(){
            $('#frm-mail-inbox').attr('action','<?=base_url()?>mail-system2/delete_multi_mails/');
            $('#frm-mail-inbox').submit();
        });

        $(document).on('click','.btn-mails-move-to',function(){
            var folder = $(this).data('folder');
            $('#frm-mail-inbox').find('input[name="folder"]').val(folder);
            $('#frm-mail-inbox').attr('action','<?=base_url()?>mail-system2/mail_move_to/').submit();
        });
    });
</script>
<script>
    $(function () {
        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
            e.preventDefault();
            //detect type
            var $this = $(this).find("a > i");
            var glyph = $this.hasClass("glyphicon");
            var fa = $this.hasClass("fa");
            //Switch states
            if (glyph) {
                $this.toggleClass("glyphicon-star");
                $this.toggleClass("glyphicon-star-empty");
            }
            if (fa) {
                $this.toggleClass("fa-star");
                $this.toggleClass("fa-star-o");
            }
        });
    });
</script>