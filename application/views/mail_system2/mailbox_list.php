<a href="<?=base_url()?>mail-system2/compose-email/compose" class="btn btn-primary btn-block">Compose</a>
<a href="javascript:void(0);" class="btn btn-primary btn-block margin-bottom btn-load-settings">Settings</a>
<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Folders</h3>

        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li class="<?=$folder == 'inbox'?'active':'';?>">
                <a href="<?=base_url()?>mail-system2/inbox/" class="mailbox-list-inbox-label"><i class="fa fa-inbox"></i> Inbox <?php if(isset($mails_data['inbox_unseen_mails']) && $mails_data['inbox_unseen_mails'] > 0){ ?><span class="label label-primary pull-right no-unread-mails"><?=$mails_data['inbox_unseen_mails']?></span><?php }?></a>
            </li>
            <li class="<?=$folder == 'drafts'?'active':'';?>">
                <a href="<?=base_url()?>mail-system2/drafts/"><i class="fa fa-file-text-o"></i> Drafts</a>
            </li>
            <li class="<?=$folder == 'outbox'?'active':'';?>">
                <a href="<?=base_url()?>mail-system2/outbox/"><i class="fa fa-folder-o"></i> Outbox</a>
            </li>
            <li class="<?=$folder == 'sent'?'active':'';?>">
                <a href="<?=base_url()?>mail-system2/sent/"><i class="fa fa-send-o"></i> Sent</a>
            </li>
            <li class="<?=$folder == 'trash'?'active':'';?>">
                <a href="<?=base_url()?>mail-system2/trash/"><i class="fa fa-trash-o"></i> Trash</a>
            </li>
        </ul>
    </div>
    <!-- /.box-body -->
</div>

<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Folder</h3>

        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked sidebar-mail-label" id="sidebar-mail-label">
            <?php
                $this->load->view('mail_system2/sidebar_mail_label', array('current_folder'=>$folder));
            ?>
        </ul>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->