<li class="dropdown-header">Move To</li>
<?php
	$folders = $this->applib->get_staff_mail_folders();
?>
<?php foreach($folders as $folder_row) {
	?>
	<li class="<?=$current_folder == $folder_row->folder?'active':''?>"><a href="javascript:void(0);" class="<?=$this->uri->segment(3) == strtolower($folder_row->folder)?'':'btn-mails-move-to'?>" data-folder="<?=$folder_row->folder;?>"><?=$folder_row->folder;?></a></li>
	<?php
}
?>
<li><a href="#" data-toggle="modal" data-target="#modal-create-folder" class="btn-create-folder">Create new</a></li>