<div class="box box-primary">
    <div class="box-header with-border">
        <?php
        $box_title = ucfirst($this->uri->segment(2));
        if($this->uri->segment(3) == 'compose'){
            $box_title = "Inbox";
        }elseif(isset($curr_folder_name)){
            $box_title = ucfirst($curr_folder_name);
        }
        ?>
        <h3 class="box-title"><?=$box_title?></h3>
        <div class="col-md-7 box-tools pull-right">
            <form action="<?=base_url()?>mail-system2/search-mails/" method="post">
                <input type="hidden" name="mailbox_name" value="<?=$current_folder_name?>">
                <div class="input-group margin">
                    <input type="text" name="search" class="form-control input-sm txt-search-mail" placeholder="Search Mail" value="<?=isset($search)?$search:''?>" required="required">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-xs btn-search-mail"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="clearfix"></div>
        <div class="table-responsive mailbox-messages">
            <form method="post" action="" id="frm-mail-inbox">
                <input type="hidden" name="from_folder_name" value="<?=$current_folder_name;?>">
                <input type="hidden" name="folder_name" value="<?=$current_folder_name;?>">
                <table class="table table-hover table-striped table-mail-list">
                    <tbody>
                    <?php
                    if(isset($mails_data['mails']) && is_array($mails_data['mails']) && count($mails_data['mails']) > 0) {
                        foreach($mails_data['mails'] as $mail){
                            ?>
                            <tr>
                                <td>
                                    <?php
                                        if($current_folder_name == 'starred' && isset($mail['folder_name'])) {
                                            echo "<input type='hidden' name='folder_name' value='".$mail['folder_name']."' class='folder_name'>";
                                        }
                                    ?>
                                    <input type="checkbox" class="chk-select-mail" name="mails_no[]" value="<?=$mail['mail_no'];?>">
                                </td>
                                <td class="mailbox-star"><a href="javascript:void(0);" class="btn-star" data-mail_no="<?=$mail['mail_no'];?>"><i class="fa <?=$mail['is_starred'] == 1?'fa-star':'fa-star-o'?> text-yellow"></i></a></td>
                                <td class="mailbox-list">
                                    <?php
                                        if($current_folder_name == 'starred' && isset($mail['folder_name'])) {
                                            echo "<input type='hidden' name='folder_name' value='".$mail['folder_name']."' class='folder_name'>";
                                        }
                                    ?>
                                    <div class="mailbox-name">
                                        <a class="btn-read-mail <?=$mail['is_unread']?'btn_view_unread_mail':'';?>" href="javascript:void(0);" data-message_no="<?=$mail['mail_no'];?>">
                                            <?=$mail['is_unread'] == 1?"<b>".$mail['username']."</b>":$mail['username'];?>
                                        </a>
                                    </div>
                                    <div class="mailbox-attachment">
                                        <?=$mail['attachment_status'] == 1?'<i class="fa fa-paperclip"></i>':'&nbsp;';?>
                                    </div>
                                    <div class="mailbox-subject">
                                        <?=$mail['is_unread'] == 1?"<b>".$this->crud->limit_character($mail['subject'],25)."</b>":$this->crud->limit_character($mail['subject'],25);?>
                                    </div>
                                    <div class="mailbox-date">
                                        <?=$mail['received_at'];?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </form>
            <!-- /.table -->
        </div>
        <!-- /.mail-box-messages -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer no-padding">
    </div>
</div>
<!-- /. box -->

<!-- Modal -->
<div id="modal-create-folder" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-create-folder-header">Create Folder</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="<?=base_url()?>mail-system2/create-folder/" id="frm-create-folder" class="form-horizontal frm-create-folder">
                        <input type="hidden" name="current_folder_name" <?=$current_folder_name?>>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label input-sm">Folder Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control input-sm folder_name" id="folder_name" name="folder_name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label input-sm">Parent Folder</label>
                                        <div class="col-sm-8">
                                            <select class="form-control input-sm" name="parent_folder">
                                                <option value="1">--</option>
                                                <?php
                                                if(isset($mails_data['mailboxes']) && is_array($mails_data['mailboxes']) && count($mails_data['mailboxes']) > 0){
                                                    foreach($mails_data['mailboxes'] as $mailbox) {
                                                        ?>
                                                        <option value="<?=$mailbox['mailbox_label']?>"><?=$mailbox['mailbox_label']?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-close-add-folder-modal" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default btn-sm sbt-create-folder">Create</button>
            </div>
        </div>

    </div>
</div>
<!-- /Modal -->

<script>
    $(document).ready(function(){
        $(document).on('click','.btn-reply-mail-list',function(){
            var i = 1;
            var folder_name = '<?=$current_folder_name;?>';
            $('.chk-select-mail:checked').each(function() {
                if(i == 1){
                    $("form#frm_mailbox").find('.message_no').val(this.value);
                    if($(this).closest('td').find("input[name='folder_name']").length > 0){
                        $("form#frm_mailbox").find('.folder_name').val($(this).closest('td').find("input[name='folder_name']").val());
                    }else{
                        $("form#frm_mailbox").find('.folder_name').val(folder_name);
                    }
                    $("form#frm_mailbox").find('.action').val('reply_mail');
                    $("form#frm_mailbox").attr('action','<?=base_url()?>mail-system2/compose-email/reply');
                    $("form#frm_mailbox").submit();
                    return false;
                }
                i++;
            });
        });
        $(document).on('click','.btn-forward-mail-list',function(){
            var i = 1;
            var folder_name = '<?=$current_folder_name;?>';
            $('.chk-select-mail:checked').each(function() {
                if(i == 1){
                    $("form#frm_mailbox").find('.message_no').val(this.value);
                    if($(this).closest('td').find("input[name='folder_name']").length > 0){
                        $("form#frm_mailbox").find('.folder_name').val($(this).closest('td').find("input[name='folder_name']").val());
                    }else{
                        $("form#frm_mailbox").find('.folder_name').val(folder_name);
                    }
                    $("form#frm_mailbox").find('.action').val('forward_mail');
                    $("form#frm_mailbox").attr('action','<?=base_url()?>mail-system2/compose-email/forward');
                    $("form#frm_mailbox").submit();
                    return false;
                }
                i++;
            });
        });
        $(document).on('click','.btn-refresh-mail',function(){
            location.reload();
        });
        $(document).on('click','.btn-delete-multi-mail',function(){
            $('#frm-mail-inbox').attr('action','<?=base_url()?>mail-system2/delete_multi_mails/');
            $('#frm-mail-inbox').submit();
        });
        $(document).on('click','.sbt-create-folder',function(){
            $('#frm-create-folder').submit();
        });

        $(document).on('submit','#frm-create-folder',function(e){
            e.preventDefault();
            $("#modal-create-folder-header").text('Processing...');
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if(data.success == true){
                        $('#select-folder-header').html(data.move_to_folder);
                        $('#select-folder-footer').html(data.move_to_folder);
                        $('#sidebar-mail-label').html(data.sidebar_mail_label);
                        $('select[name="parent_folder"]').html(data.folder_dropdown_option);
                        show_notify(data.message,true);
                        $('#modal-create-folder').modal('hide');
                    }else{
                        show_notify(data.message,false);
                    }
                    $("#modal-create-folder-header").text('Create Folder');
                }
            });
        });

        $(document).on('click','.btn-mails-move-to',function(){
            var folder_name = $(this).data('folder_name');
            $('#frm-mail-inbox').find('input[name="folder_name"]').val(folder_name);
            $('#frm-mail-inbox').attr('action','<?=base_url()?>mail-system2/mail_move_to/').submit();
        });
    });
</script>
<script>
    $(function () {
        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
            e.preventDefault();
            //detect type
            var $this = $(this).find("a > i");
            var glyph = $this.hasClass("glyphicon");
            var fa = $this.hasClass("fa");
            //Switch states
            if (glyph) {
                $this.toggleClass("glyphicon-star");
                $this.toggleClass("glyphicon-star-empty");
            }
            if (fa) {
                $this.toggleClass("fa-star");
                $this.toggleClass("fa-star-o");
            }
        });
    });
</script>