<style>
	td.mailbox-subject{
		white-space: nowrap;
		max-width: 620px !important;
		overflow: hidden;
	}
	td.mailbox-attachment{
		max-width: 30px !important;
	}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Mailbox
			<small> <span class="top-no-unread-mails"><?=isset($count_unseen_mails)?$count_unseen_mails:0?></span> new messages</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Mailbox</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-2">
				<?php $this->load->view('mail_system2/mailbox_list',array('folder'=>$folder));?>
			</div>
			<!-- /.col -->

			<div class="col-md-3" style="padding-left: 0px">
				<?php $this->load->view('mail_system2/mail_list',array('folder'=>$folder));?>
			</div>

			<div class="col-md-7 email-detail-container" style="padding-left:0px;">
				<?php $this->load->view('shared/success_false_notify');?>

				<?php
					if(isset($mail_detail) && !empty($mail_detail) && $this->uri->segment(2) == 'read') {
						$this->load->view('mail_system2/read_email', array('mail_detail' => $mail_detail,'mail_id'=>$mail_id));
					}elseif($this->uri->segment(3) == 'compose'){
						$this->load->view('mail_system2/compose_email',array('action'=>'compose','settings' => $settings));
					}elseif(isset($mail_detail) && !empty($mail_detail) && $this->uri->segment(3) == 'forward'){
						$this->load->view('mail_system2/compose_email',array('mail_detail' => $mail_detail,'settings' => $settings,'action'=>'forward'));
					}elseif(isset($mail_detail) && !empty($mail_detail) && $this->uri->segment(3) == 'reply'){
						$this->load->view('mail_system2/compose_email',array('mail_detail' => $mail_detail,'settings' => $settings,'action'=>'reply'));
					}
				?>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<form id="frm_mailbox" action="" method="post">
	<input type="hidden" name="action" class="action" value="">
	<input type="hidden" name="mail_id" class="mail_id">
	<input type="hidden" name="folder" class="folder">
</form>
<div id="modal-create-folder" class="modal fade" role="dialog">
	<div class="modal-dialog" style="max-width: 480px">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-create-folder-header">Create Folder</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="<?=base_url()?>mail-system2/create-folder/" id="frm-create-folder" class="form-horizontal frm-create-folder">
						<input type="hidden" name="current_folder" value="<?=$current_folder;?>">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label input-sm">Folder Name</label>
										<div class="col-sm-8">
											<input type="text" class="form-control input-sm folder" id="folder" name="folder" required>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm btn-close-add-folder-modal" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-default btn-sm sbt-create-folder">Create</button>
			</div>
		</div>

	</div>
</div>
<script>
	var loader = "";
	loader += '<div class="box">';
		loader += '<div class="overlay">';
			loader += '<i class="fa fa-refresh fa-spin"></i>';
		loader += '</div>';
		loader += '<div class="box-header with-border">';
			loader += '<h3 class="box-title">Loading...</h3>';
		loader += '</div>';
	loader += '</div>';
	$(document).ready(function(){

		$(document).on('click','.sbt-create-folder',function(){
			$('#frm-create-folder').submit();
		});

		$(document).on('submit','#frm-create-folder',function(e){
			e.preventDefault();
			$("#modal-create-folder-header").text('Processing...');
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					console.log(data);
					if(data.success == true){
						$('#select-folder-header').html(data.move_to_folder);
						$('#select-folder-footer').html(data.move_to_folder);
						$('#sidebar-mail-label').html(data.sidebar_mail_label);
						$('select[name="parent_folder"]').html(data.folder_dropdown_option);
						show_notify(data.message,true);
						$('#modal-create-folder').modal('hide');
					}else{
						show_notify(data.message,false);
					}
					$("#modal-create-folder-header").text('Create Folder');
				}
			});
		});

		$(document).on('submit','#frm-settings',function(e){
			e.preventDefault();
			$('.btn-save-settings').html('Saving...');
			$.ajax({
				url: '<?=base_url()?>mail-system2/save-settings/',
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					$('.btn-save-settings').html('Save');
					if(data.success == true){
						show_notify(data.message,true);
					}else{
						show_notify('Something wrong!',false);
					}
				}
			});
		});

		$(document).on('click','.btn-load-settings',function(){
			if($('.email-detail-container').find('.box').length == 0){
				$('.email-detail-container').html(loader);
			}else{
				$('.email-detail-container').find('.overlay').removeClass('hidden');
			}
			$.ajax({
				url: '<?=base_url()?>mail-system2/settings/',
				type: "GET",
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					if(data.success == true){
						var settings_html = data.settings_html;
						$('.email-detail-container').html(settings_html);
						$("#signature-textarea").wysihtml5();
					}else{
						show_notify('Something wrong!',false);
					}
				}
			});
		});



		$(document).on('click','.fileinput-remove',function(){
			$("input.attachment_url").each(function(index){
				if(index == 0){
					$(this).val('');
				}else{
					$(this).remove();
				}
			});
		});

		/*********** Forward Mail *********/
		$(document).on('click','.btn-trash-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system2/delete-mail');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-forward-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system2/compose-email/forward');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-reply-mail',function(){
			$("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
			$("#frm_mailbox").attr('action','<?=base_url()?>mail-system2/compose-email/reply');
			$("#frm_mailbox").submit();
		});

		$(document).on('click','.btn-view-folder-mail',function(){
			window.location.href = '<?=base_url()?>mail-system2/folder-mails/'+$(this).data('folder');
		});

		$(document).on('click','.btn-read-mail',function(){
			if($('.email-detail-container').find('.box').length == 0){
				$('.email-detail-container').html(loader);
			}else{
				$('.email-detail-container').find('.overlay').removeClass('hidden');
			}
			var button = $(this);
			var td = $(this).closest('td');
			var formdata = new FormData();
			formdata.append("mail_id",$(this).data('mail_id'));
			$.ajax({
				url: '<?=base_url()?>mail-system2/read/',
				type: "POST",
				data: formdata,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					$('.email-detail-container').html(data.email_detail);
					if(button.hasClass('btn_view_unread_mail')) {
						var email_address = td.find('.btn-read-mail').text();
						td.find('.btn-read-mail').text(email_address);
						var mailbox_subject = td.find('.mailbox-subject').text();
						td.find('.mailbox-subject').text(mailbox_subject);
						if(data.no_unread_mails > 0){
							$('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">' + data.no_unread_mails + '</span>');
							$('.sidebar-unread-mails').append('<span class="pull-right-container"><span class="label label-primary pull-right">' + data.no_unread_mails + '</span></span>');
							$('.top-no-unread-mails').text(data.no_unread_mails);
							if ($('.mailbox-list-inbox-label').find('.no-unread-mails').length == 0) {
								$('.mailbox-list-inbox-label').append('<span class="label label-primary pull-right no-unread-mails">' + data.no_unread_mails + '</span>');
							} else {
								$('.mailbox-list-inbox-label').find('.no-unread-mails').text(data.no_unread_mails);
							}
							$('.messages-menu').find('ul.dropdown-menu').remove();
							$('.messages-menu').append(data.unread_mails);
						}else{
							$('.messages-menu').find('ul.dropdown-menu').remove();
							$('.mailbox-list-inbox-label').find('.no-unread-mails').remove();
							$('.unread-mail-icon').find('.inbox-unread-mails').remove();
							$('.sidebar-unread-mails').find('.pull-right-container').remove();
							$('.top-no-unread-mails').text(0);
						}
					}
				}
			});
		});

		$(document).on('click','.btn-star',function(){
			var star_button = $(this);
			if($(this).find('.fa-star-o').length == 0){
				var url = "<?=base_url()?>mail-system2/set-starred-flag/";
			}else{
				var url = "<?=base_url()?>mail-system2/remove-starred-flag/";
			}
			<?php
			if($this->uri->segment(2) == 'starred'){
			?>
			star_button.closest('tr').fadeOut('slow');
			<?php
			}
			?>
			var formdata = new FormData();
			formdata.append("mail_id",$(this).data('mail_no'));
			formdata.append("folder","<?=$folder;?>");
			$.ajax({
				url: url,
				type: "POST",
				data: formdata,
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {

				}
			});
		});
	});
</script>
<?php
	if(in_array($this->uri->segment(3),array('compose','forward','reply'))) {
		?>
		<script src="<?= plugins_url('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
		<script>
			$(function () {
				$("#compose-textarea").wysihtml5();
				$('#discardbtnicon').click(function () {
					window.location.href = '<?=base_url()?>mail-system2/inbox/';
				});
				$(document).on('change', '#upload_attachment', function () {
					$("form#frmComposeMail").find('.overlay').removeClass('hidden');
					var formdata = new FormData();
					formdata.append("upload_attachment", $('#upload_attachment')[0].files[0]);
					$.ajax({
						url: "<?=base_url('mail-system2/upload_attachment/');?>",
						type: "POST",
						data: formdata,
						contentType: false,
						cache: false,
						processData: false,
						dataType: 'json',
						success: function (data) {
							$("form#frmComposeMail").find('.overlay').addClass('hidden');
							if (data.success == true) {
								show_notify(data.message, true);
								if($(".attachment_url:last").val() == ''){
									$(".attachment_url:last").val(data.attachment_url);
								}else{
									var attach_html = $(".attachment_url:last").clone();
									attach_html.insertAfter($(".attachment_url:last"));
									$(".attachment_url:last").val(data.attachment_url);
								}
							} else {
								show_notify(data.message, false);
							}
						}
					});
				});
			});
		</script>
		<?php
	}
?>