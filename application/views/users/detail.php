<?php
    $user = '';
    if(isset($user_type) && $user_type == 2){
        $user = 'contestant';
    }else{
        $user = 'voter';
    }
?>
<!-- Main content -->
<div class="content-wrapper">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?=ucwords($user)?></span> - Detail</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="<?=base_url();?>"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="<?=base_url();?>users/users_list/<?=$user_type?>"><?=ucwords($user)?></a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <!-- Page length options -->
        <div class="row">
            <div class="<?=$user_type == 2?'col-md-6':'col-md-7'?>">
                <div class="panel panel-info border-right-info border-right-xlg">
                    <div class="panel-heading">
                        <h5 class="panel-title"><?=ucwords($user)?> details</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body custom-panel-body">
                        <div class="<?=$user_type == 2?'col-md-6 col-sm-12':'col-md-5  col-sm-12'?>">
                            <div class="thumbnail no-padding profile_pic_card image_card">
                                <div class="thumb">
                                    <img src="<?=$profile_pic?>" alt="">
                                    <div class="caption-overflow">
                            <span>
                                <a href="<?=$profile_pic?>" class="btn bg-success-400 btn-icon btn-xs" data-popup="lightbox"><i class="icon-plus2"></i></a>
                            </span>
                                    </div>
                                </div>
                                <div class="caption text-center">
                                    <h6 class="text-semibold no-margin"><?=ucwords($username)?><small class="display-block"><i class="icon-pin-alt text-size-base"></i> <?=ucwords($city)?></small></h6>
                                </div>
                            </div>
                        </div>
                        <div class="<?=$user_type == 2?'col-md-6 col-sm-12':'col-md-7 col-sm-12'?>">
                            <h1 class="no-margin text-light"><?=ucwords($username);?></h1>
                            <br/>
                            <dl class="dl-horizontal">
                                <dt>Email</dt>
                                <dd><?=$email;?></dd>
                                <dt>Contact no</dt>
                                <dd></dd>
                                <dt>Address</dt>
                                <dd><?=ucwords($address);?></dd>
                                <dt>City</dt>
                                <dd><?=ucwords($city);?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="<?=$user_type == 2?'col-md-3':'col-md-5'?>">
                <div class="panel panel-info border-right-info border-right-xlg">
                    <div class="panel-heading">
                        <h5 class="panel-title">Given Votes (<?=$given_votes;?>)</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body custom-panel-body-scrollable" style="height:268px;">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="text-center">Contestant</th>
                                <th class="text-center">Votes</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(isset($history['voters']) && $history['voters'] != null) {
                                foreach ($history['voters'] as $VoterRow) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= ucwords($VoterRow['contestant']); ?></td>
                                        <td class="text-center"><?= $VoterRow['votes']; ?></td>
                                    </tr>
                                <?php }
                            }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <h5 class="panel-title">Remaining Votes (<?=$given_votes_quota - $given_votes;?>)</h5>
                    </div>
                </div>
            </div>
            <?php if($user_type == 2){?>
                <div class="col-md-3">
                    <div class="panel panel-info border-right-info border-right-xlg">
                        <div class="panel-heading">
                            <h5 class="panel-title">Received Votes (<?=$received_votes;?>)</h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body custom-panel-body-scrollable">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">Voter</th>
                                    <th class="text-center">Votes</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($history['contestants']) && $history['contestants'] != null) {
                                    foreach ($history['contestants'] as $VoterRow) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= ucwords($VoterRow['voter']); ?></td>
                                            <td class="text-center"><?= $VoterRow['votes']; ?></td>
                                        </tr>
                                    <?php }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <div class="col-md-12 no-padding-left no-padding-right">
            <?php if($user_type == 2){?>
                <div class="col-md-6">
                    <div class="panel panel-info border-right-info border-right-xlg">
                        <div class="panel-heading">
                            <h5 class="panel-title">Comments On <?=ucwords($user)?></h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="comments-block">
                                <?php
                                if(isset($comments_on_me) && $comments_on_me != null) {
                                    foreach ($comments_on_me as $CmtOnMeRow) {
                                        ?>
                                        <blockquote class="no-margin">
                                            <img class="img-circle"
                                                 src="<?= profile_pics_url($CmtOnMeRow['profile_pic']); ?>"
                                                 alt="">
                                            <?= $CmtOnMeRow['comment']; ?>
                                            <footer>By <?= ucwords($CmtOnMeRow['commented_by']); ?><cite
                                                    title="Source Title"> At <?= $CmtOnMeRow['commented_at']; ?></cite>
                                            </footer>
                                        </blockquote>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
                <div class="col-md-6">
                    <div class="panel panel-info border-right-info border-right-xlg">
                        <div class="panel-heading">
                            <h5 class="panel-title">Comments By <?=ucwords($user)?></h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="comments-block">
                                <?php if(isset($comments_by_me) && $comments_by_me != null) {
                                    foreach ($comments_by_me as $CmtByMeRow) {
                                        ?>
                                        <blockquote class="no-margin">
                                            <img class="img-circle" src="<?= profile_pics_url($CmtByMeRow['profile_pic']); ?>"
                                                 alt="">
                                            <?= $CmtByMeRow['comment']; ?>
                                            <footer>On <?=ucwords($CmtByMeRow['commented_by']); ?><cite
                                                    title="Source Title"> At <?= $CmtByMeRow['commented_at']; ?></cite>
                                            </footer>
                                        </blockquote>
                                        <?php
                                    }
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    if($user_type == 2) {
                        for ($i = 0; $i < 10; $i++) {
                            ?>
                            <div class="col-lg-3 col-sm-3">
                                <div class="thumbnail image_card bg-info">
                                    <div class="thumb">
                                        <img src="<?=isset($images[$i]['thumb_image'])?$images[$i]['thumb_image']:base_url('resource/assets/images/placeholder.jpg')?>" alt="">
                                        <div class="caption-overflow">
                                        <span>
                                            <a href="<?=isset($images[$i]['image'])?$images[$i]['image']:base_url('resource/assets/images/placeholder.jpg')?>" data-popup="lightbox"
                                               class="btn border-white text-white btn-flat btn-icon btn-rounded"><i
                                                    class="icon-plus3"></i></a>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="no-margin"><a href="#" class="text-default">Lorem ipusm
                                                lorem</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>
        <!-- /page length options -->
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('users/datatable/3')?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false,
                },
            ],
            "columns": [
                null,
                null,
                null,
                null,
                { className: "text-center" },
                { className: "text-center" }
            ]
        });

        $('select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $(document).on('click','.delete-user',function(){
            var user_id = $(this).data('user_id');
            var tr = $(this).closest('tr');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this record !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plz!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.get('<?=base_url()?>users/delete_user/'+user_id,null,function(response){
                            if(response == 'success'){
                                tr.find("td").fadeOut(1000, function(){ $(this).parent().remove();});
                                swal({
                                    title: "Deleted!",
                                    text: "Your have successfully delete record.",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success"
                                });
                            }else{
                                swal({
                                    title: "Oops...",
                                    text: "Something went wrong!",
                                    confirmButtonColor: "#EF5350",
                                    type: "error"
                                });
                            }
                        })
                    }else {

                    }
                });

        });
    });
</script>