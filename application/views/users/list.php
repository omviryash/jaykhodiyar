<?php
    $user = '';
    if(isset($user_type) && $user_type == 2){
        $user = 'contestant';
    }else{
        $user = 'voter';
    }
    if(!isset($competition_id)){
        $competition_id = 1;
    }
?>
<!-- Main content -->
<div class="content-wrapper">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?=ucwords($user)?></span> - List</h4>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="<?=base_url();?>"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="javascript:void(0);"><?=ucwords($user)?></a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <!-- Page length options -->
        <div class="panel panel-info border-right-info border-right-xlg">
            <div class="panel-heading">
                <h5 class="panel-title"><?=ucwords($user)?> list</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload" class="reload"></a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body no-padding-top">
                <table class="table table-striped media-library table-lg" id="table">
                    <thead>
                    <tr>
                        <th class="text-center">User Id</th>
                        <th>Profile Pics</th>
                        <?php if($user_type == 2){?><th class="text-center" >Votes</th><?php } ?>
                        <th>Username</th>
                        <th>Email</th>
                        <th>City</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /page length options -->
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#table').DataTable({
            "lengthMenu": [ [10,25,50,100], [10,25,50,100] ],
            "pageLength": <?=$this->config->item('contestant_list_range');?>,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?=base_url('users/datatable/'.$user_type)?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false,
                },
            ],
            "columns": [
                { className: "text-center" },
                { className: "text-center" },
                <?php if($user_type == 2){?>{ className: "text-center" },<?php } ?>
                null,
                null,
                null,
                { className: "text-center" }
            ]
        });

        $(document).on('change','select[name="table_length"]',function(){
            $.post('<?=base_url();?>data-management/update_config_item',{'config_key':'contestant_list_range','config_value':$(this).val()},null);
        });

        $('select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $(document).on('click','.reload',function(){
            table.draw();
        });

        $(document).on('click','.delete-user',function(){
            var user_id = $(this).data('user_id');
            var tr = $(this).closest('tr');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this record !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plz!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.get('<?=base_url()?>users/delete_user/'+user_id,null,function(response){
                            if(response == 'success'){
                                tr.find("td").fadeOut(1000, function(){ $(this).parent().remove();});
                                swal({
                                    title: "Deleted!",
                                    text: "Your have successfully delete record.",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success"
                                });
                            }else {
                                swal({
                                    title: "Oops...",
                                    text: "Something went wrong!",
                                    confirmButtonColor: "#EF5350",
                                    type: "error"
                                });
                            }
                        })
                    }else {

                    }
                });

        });
    });
</script>