<html>
<head>
    <title>Salary Report - <?=ucfirst($employee_data->name).'('.date('F-Y',strtotime("$year-$month-01")).')';?></title>
    <style>
        .text-center{
            text-align: center;
        }
        .text-right{
            text-align: right;
        }
        .table{
            width: 100%;
            font-size:13px;
            margin: 20px;
        }
        .panel-heading{
            font-size: 25px;
            font-weight: 800;
            text-align: center;
            background-color: #6c6b6b;
            padding: 10px;
            color: #ffffff;
        }
        table{
            border-spacing: 0;
            width: 100%;
            border-bottom: 1px solid;
            border-right: 1px solid;
        }
        td{
            padding: 5px 2px;
            border-left: 1px solid;
            border-top: 1px solid;
        }
        tr > td:last-child{
            border-right: 1px solid !important;
        }
        tr:last-child > td{
            border-bottom: 1px solid !important;
        }
        .main-section-title{
            font-size: 24px;
            text-align: center;
        }
        .section-title{
            font-size: 18px;
            text-align: center;
        }
        .text-bold{
            font-weight: 900 !important;
            font-size:12px !important;
        }
        .light_blue{
            /*background-color: #C3DFFC;*/
        }
        .dark_blue{
            /*color: #0A487B;*/
        }
        .text_bold{
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="panel panel-default">
    <div class="panel-body">
        <?php
        if(isset($employee_data) && count($employee_data) > 0 && isset($month_total_leaves)) {
            $salary = $employee_data->salary;
            $yearly_traveling_cost = 9600;
            $yearly_education_cost = 2400;
            $yearly_medical_cost = 15000;
            $yearly_other_cost = 46176;
            $basic_pay = round(($salary * 40)/100,2);
            $house_rent = round(($basic_pay * 40)/100,2);
            $monthly_traveling_cost = $yearly_traveling_cost/12;
            $monthly_education_cost = $yearly_education_cost/12;
            $pf = round(($basic_pay * 12)/100,2);
            $bonus = round(($basic_pay * 8.33)/100,2);
            $monthly_medical_cost = $yearly_medical_cost/12;
            $monthly_other_cost = $yearly_other_cost/12;
            $total = $basic_pay + $house_rent + $monthly_traveling_cost + $monthly_education_cost + $pf + $bonus + $monthly_medical_cost + $monthly_other_cost;
            $deduction_pf = round(($basic_pay * 12)/100,2);
            $deduction_bonus = round(($basic_pay * 8.33)/100,2);
            $monthly_gross = $total - ($deduction_pf + $deduction_bonus);
            $pf_basic = $pf;
            $pt = 200;
            $total_statutory_components = $pf_basic + $pt;
            $cost_to_company = $monthly_gross - $total_statutory_components;
            $salary_per_day = round($salary/date('t',strtotime("$year-$month-01")),2);
            $leave_deduction = $month_total_leaves * $salary_per_day;
            $final_cost_to_company = $cost_to_company - $leave_deduction;
            ?>
            <?php
                if(isset($letterpad_details->header_logo)){
                    echo '<div style="padding-right: 20px; text-align: '.$letterpad_details->header_logo_alignment.';"><img src="' . base_url() . image_dir('letterpad_header_logo/'. $letterpad_details->header_logo) .'" width="100px"></div>';
                }
            ?>
            <table class="table">
                <tr class="light_blue">
                    <td colspan="12" class="main-section-title dark_blue"><b>Salary Slip</b></td>
                </tr>
                <tr class="light_blue">
                    <td colspan="12" class="section-title dark_blue"><b>Employee Details</b></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Code</td>
                    <td colspan="3"><?php echo 'JK'.$employee_data->employee_code; ?></td>
                    <td colspan="3" class="light_blue dark_blue text_bold">Name</td>
                    <td colspan="3"><?=$employee_data->name?></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Designation</td>
                    <td colspan="3"><?=$employee_data->designation?></td>
                    <td colspan="3" class="light_blue dark_blue text_bold">Month</td>
                    <td colspan="3"><?=date('M',strtotime("$year-$month-01")).'-'.$year;?></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Payable Days</td>
                    <td colspan="3" ></td>
                    <td colspan="3" class="light_blue dark_blue text_bold">Pay Mode</td>
                    <td colspan="3"><?php echo $employee_data->pay_mode; ?></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">W/P Leave</td>
                    <td colspan="3">0</td>
                    <td colspan="3" class="light_blue dark_blue text_bold">UAN No.</td>
                    <td colspan="3"><?=$employee_data->uan_no?></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Paid Leave</td>
                    <td colspan="3">0</td>
                    <td colspan="3" class="light_blue dark_blue text_bold">PF No.</td>
                    <td colspan="3"><?=$company_pf_no .'/'.$employee_data->pf_no?></td>
                </tr>
                <tr>
                    <td colspan="12" class="section-title light_blue dark_blue text_bold"><b>Salary Details</b></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold"><b>Earning</b></td>
                    <td colspan="3" class="text-center"><b>INR</b></td>
                    <td colspan="3" class="light_blue dark_blue text_bold"><b>Deduction</b></td>
                    <td colspan="3" class="text-center"><b>INR</b></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Basic</td>
                    <td colspan="3" class="text-right"><?=number_format($employee_data->basic_pay ,2);?></td>
                    <td colspan="3" class="light_blue dark_blue text_bold">EPF</td>
                    <td colspan="2" class="text-right"><?=number_format($employee_data->pf_amount ,2);?></td>
                    <td colspan="1" class="text-center light_blue dark_blue text_bold"><b>Net Pay</b></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">HRA</td>
                    <td colspan="3" class="text-right"><?=number_format(isset($employee_data->hra) ? $employee_data->hra : '',2);?></td>
                    <td colspan="3" class="light_blue dark_blue text_bold">ESIC</td>
                    <td colspan="2" class="text-right"><?=number_format(isset($employee_data->esi_amount) ? $employee_data->esi_amount : '',2);?></td>
                    <?php $money = money_to_word($employee_data->final_total); ?>
                    <?php $money = wordwrap($money, 15, '<br/>', true); ?>
                    <td colspan="1" rowspan="4" class="text-center light_blue dark_blue text_bold"><?php echo $money; ?><?php if(strpos($money, 'only')  !== true) { echo ' only.'; } ?></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Conveyance</td>
                    <td colspan="3" class="text-right"><?=number_format(isset($employee_data->conveyance) ? $employee_data->conveyance : '',2);?></td>
                    <td colspan="3" class="light_blue dark_blue text_bold">Profession Tax</td>
                    <td colspan="2" class="text-right"><?=number_format(isset($employee_data->professional_tax) ? $employee_data->professional_tax : '',2);?></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Mediclaim</td>
                    <td colspan="3" class="text-right"><?=number_format(isset($employee_data->mediclaim) ? $employee_data->mediclaim : '',2);?></td>
                    <td colspan="3" class="light_blue dark_blue text_bold">Other</td>
                    <td colspan="2" class="text-right"><?=number_format(isset($employee_data->other) ? $employee_data->other : '',2);?></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold">Incentive After TDS</td>
                    <td colspan="3" class="text-right"><?=number_format(isset($employee_data->incentive_after_tds) ? $employee_data->incentive_after_tds : '',2);?></td>
                    <td colspan="3"></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="3" class="light_blue dark_blue text_bold"><b>Gross</b></td>
                    <td colspan="3" class="text-right light_blue dark_blue text_bold"><b><?=number_format(isset($employee_data->left_total) ? $employee_data->left_total : '',2);?></b></td>
                    <td colspan="3" class="light_blue dark_blue text_bold"><b>Total</b></td>
                    <td colspan="2" class="text-right light_blue dark_blue text_bold"><b><?=number_format(isset($employee_data->right_total) ? $employee_data->right_total : '',2);?></b></td>
                    <td colspan="1" class="text-right light_blue dark_blue text_bold"><b><?=number_format(isset($employee_data->final_total) ? $employee_data->final_total : '',2);?></b></td>
                </tr>
                <tr>
                    <td colspan="6" class="text-center light_blue dark_blue text_bold"><b>Employee's Signature</b></td>
                    <td colspan="6" class="text-center light_blue dark_blue text_bold"><b>Authorised Signature</b></td>
                </tr>
                <tr>
                    <td colspan="6"><br /><br /><br /><br /><br /><br /><br /><br /><br /></td>
                    <td colspan="6"><br /><br /><br /><br /><br /><br /><br /><br /><br /></td>
                </tr>
            </table>
            <?php /*<div style="background:url(<?php echo base_url() ?>resource/image/Salaryslip_Head_footer.jpg) no-repeat bottom center; background-size:100% font-size:10px; color:#333; clear:both; height:175px;"></div>*/ ?>
            <?php
                if(isset($letterpad_details->footer_detail)){
                    echo $letterpad_details->footer_detail;
                }
            ?>
        <?php } ?>
    </div>
</div>
</body>
</html>
