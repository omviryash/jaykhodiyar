<?php $segment3 = $this->uri->segment(3); ?>
<div class="content-wrapper">
    <section class="custom content-header"  >
        <?php $this->load->view('shared/success_false_notify'); ?>
        <h1>
            <small class="text-primary text-bold">Monthly Salary Report</small>
            <small class="text-center"><label class="label label-warning">Ctrl+S = Save</label></small>
            <?php if($this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "view")){ ?>
                <a style="margin-left: 8px;" href="<?= BASE_URL; ?>hr/employee_master" class="btn btn-info btn-xs pull-right">View Employee List</a>
            <?php } ?>
            <a href="<?= BASE_URL; ?>hr/salary_list" class="btn btn-info btn-xs pull-right">Calculate Salary List</a>
            <?php if (isset($data['only_view_mode']) && $data['only_view_mode'] == '1'){ ?>
                <!--<a href="<?= BASE_URL; ?>hr/email-salary-report/<?= $employee_data->staff_id ?>/<?= $year ?>/<?= $month ?>" class="btn btn-info btn-xs pull-right margin-r-5">Email</a>--> 
                <a href="<?= BASE_URL; ?>hr/print-salary-report/<?= $employee_data->staff_id ?>/<?= $year ?>/<?= $month ?>" target="_blank" class="btn btn-info btn-xs pull-right margin-r-5 print_btn">Print</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <?php if(!isset($segment3) && empty($segment3)){ ?>
                    <form action="<?= base_url('hr/salary-report/') ?>" method="post">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Employee</label>
                                <select name="employee_id" id="employee_id" class="form-control input-sm select2"></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select Month</label>
                                <select name="month" class="form-control input-sm select2">
                                    <?php for ($m = 1; $m < 13; $m++) { ?>
                                        <?php if(isset($month)){ ?>
                                            <option value="<?= $m; ?>" <?= isset($month) && $m == $month ? 'selected="selected"' : '' ?>><?= date('F', mktime(0, 0, 0, $m)); ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $m; ?>" <?= $m == date('m') ? 'selected="selected"' : '' ?>><?= date('F', mktime(0, 0, 0, $m)); ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select Year</label>
                                <select name="year" class="form-control input-sm select2">
                                    <?php for ($y = date('Y') - 2; $y < date('Y') + 3; $y++) { ?>
                                        <?php if(isset($year)){ ?>
                                            <option value="<?= $y; ?>" <?= isset($year) && $y == $year ? 'selected="selected"' : ''; ?>><?= $y; ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $y; ?>" <?= $y == date('Y') ? 'selected="selected"' : ''; ?>><?= $y; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <br/>
                            <input type="submit" value="Calculate Salary" class="btn btn-primary pull-right btn-sm pull-left">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                    <?php } ?>
                    <?php
//                        if (isset($employee_data) && count($employee_data) > 0 && isset($month_total_leaves)) {                                                                                 
                        if (isset($employee_data) && count($employee_data) > 0) {                                                                                 
                            $salary = $employee_data->salary;
                            $yearly_traveling_cost = 9600;
                            $yearly_education_cost = 2400;
                            $yearly_medical_cost = 15000;
                            $yearly_other_cost = 46176;
                            $basic_pay = round(($salary * 40) / 100, 2);
                            $house_rent = round(($basic_pay * 40) / 100, 2);                             
                            $monthly_traveling_cost = $yearly_traveling_cost / 12;
                            $monthly_education_cost = $yearly_education_cost / 12;
                            $pf = round(($basic_pay * 12) / 100, 2);
                            $bonus = round(($basic_pay * 8.33) / 100, 2);
                            $monthly_medical_cost = $yearly_medical_cost / 12;
                            $monthly_other_cost = $yearly_other_cost / 12;
                            $total = $basic_pay + $house_rent + $monthly_traveling_cost + $monthly_education_cost + $pf + $bonus + $monthly_medical_cost + $monthly_other_cost;
                            $deduction_pf = round(($basic_pay * 12) / 100, 2);
                            $deduction_bonus = round(($basic_pay * 8.33) / 100, 2);
                            $monthly_gross = $total - ($deduction_pf + $deduction_bonus);
                            $pf_basic = $pf;
                            $pt = 200;
                            $total_statutory_components = $pf_basic + $pt;
                            $cost_to_company = $monthly_gross - $total_statutory_components;
                            $salary_per_day = round($salary / date('t', strtotime("$year-$month-01")), 2);
                            $leave_deduction = $month_total_leaves * $salary_per_day;
                            $final_cost_to_company = $cost_to_company - $leave_deduction;
                    ?>
                    <div class="col-md-12 show_salary">
                        <?php if (isset($employee_id) && isset($year) && isset($month)) { ?>
                            <form action="" method="post" id="save_salary_data" data-parsley-validate="">
                                <table class="table" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Month</th>
                                            <th><?= date('M', strtotime("$year-$month-01")) . '-' . $year; ?></th>
                                            <th>Designation</th>
                                            <th><?= $employee_data->designation ?></th>
                                        </tr>
                                        <tr>
                                            <th>Salary</th>
                                            <th><?= $salary ?></th>
                                            <th>Grade</th>
                                            <th><?= $employee_data->grade ?></th>
                                        </tr>
                                        <tr>
                                            <th>Name</th>
                                            <th><?= $employee_data->name ?></th>
                                            <th>Department</th>
                                            <th><?= $employee_data->department ?></th>
                                        </tr>
                                        <tr>
                                            <th>Date of Joining</th>
                                            <th><?= date('d-m-Y', strtotime($employee_data->date_of_joining)) ?></th>
                                            <th>Location</th>
                                            <th><?= $employee_data->address ?></th>
                                        </tr>
                                    </thead>
                                </table>
                            </form>
                        <?php } ?>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-6 show_salary">
                        <?php if (isset($employee_id) && isset($year) && isset($month)) { ?>
                            <form action="" method="post" id="save_salary_data" data-parsley-validate="">
                                <?php if(isset($segment3) && !empty($segment3)){ ?>
                                    <input type="hidden" name="employee_salary_id" value="<?php echo $segment3 ?>">
                                <?php } ?>
                                <table class="table" id="dataTables-example">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center">Compensation Details</th>
                                        </tr>
                                        <tr>
                                            <td>Actual Salary</td>
                                            <td class="text-right"><?= number_format($employee_data->actual_salary, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Basic Salary</td>
                                            <td class="text-right"><?= number_format($employee_data->basic_pay, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>CTC PF <?= (!empty($employee_data->pf_amount)) ? $employee_data->pf_per.' %' : ''; ?> </td>
                                            <td class="text-right"><?= number_format($employee_data->pf_amount, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Salary</td>
                                            <td class="text-right"><?php echo number_format($employee_data->salary, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>ESI <?= (!empty($employee_data->esi_amount)) ? $employee_data->esi_per.' %' : ''; ?> </td>
                                            <td class="text-right"><?= number_format($employee_data->esi_amount, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>EPF <?= (!empty($employee_data->pf_amount)) ? $employee_data->pf_per.' %' : ''; ?> </td>
                                            <td class="text-right"><?= number_format($employee_data->pf_amount, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Professional Tax <br />
                                                <label class="label label-warning">Actual Salary : 0 to 5999 : 0, 6000 to 8999 : 80, 9000 to 11999 : 150, >= 12000 : 200</label>
                                            </td>
                                            <td class="text-right"><?= number_format($employee_data->professional_tax, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Other Deduction</td>
                                            <td class="text-right"><input type="text" name="other_deduction" id="other_deduction" class="select_all num_only text-right" value="<?= isset($employee_data->other_deduction) && !empty($employee_data->other_deduction) ? number_format($employee_data->other_deduction, '2', '.', '') : '0'?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Late Coming Charge</td>
                                            <td class="text-right"><input type="text" name="late_coming_charge" id="late_coming_charge"  class="select_all num_only text-right" value="<?= isset($employee_data->late_coming_charge) && !empty($employee_data->late_coming_charge) ? number_format($employee_data->late_coming_charge, '2', '.', '') : '0'?>"></td>
                                        </tr>
                                        <tr>
                                            <td>HRA</td>
                                            <td class="text-right"><input type="text" id="hra" name="hra" class="select_all num_only text-right" value="<?= isset($employee_data->hra) && !empty($employee_data->hra) ? number_format($employee_data->hra, '2', '.', '') : '0'?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Conveyance</td>
                                            <td class="text-right"><input type="text" id="conveyance" name="conveyance" class="select_all num_only text-right" value="<?= isset($employee_data->conveyance) && !empty($employee_data->conveyance) ? number_format($employee_data->conveyance, '2', '.', '') : '0'?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Mediclaim</td>
                                            <td class="text-right"><input type="text" id="mediclaim" name="mediclaim" class="select_all num_only text-right" value="<?= isset($employee_data->mediclaim) && !empty($employee_data->mediclaim) ? number_format($employee_data->mediclaim, '2', '.', '') : '0'?>"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table border="1" width="100%">
                                                    <tr>
                                                        <th>Item Name</th>
                                                        <th class="text-right">Rate</th>
                                                        <th class="text-right">%</th>
                                                        <th class="text-right">Amount</th>
                                                    </tr>
                                                    <?php if(!empty($incentive_item_arr)){
                                                        foreach ($incentive_item_arr as $incentive_item_array){ ?>
                                                            <tr>
                                                                <td><?php echo $incentive_item_array->item_name; ?></td>
                                                                <td class="text-right"><?= $incentive_item_array->item_rate; ?></td>
                                                                <td class="text-right"><?= $incentive_item_array->incentive_pr; ?></td>
                                                                <td class="text-right"><?= number_format($incentive_item_array->incentive_pr_amt, '2', '.', ''); ?></td>
                                                            </tr>
                                                        <?php }
                                                    } ?>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Incentive</td>
                                            <?php $employee_data->incentive_amount = round($employee_data->incentive_amount); ?>
                                            <td class="text-right"><?= number_format($employee_data->incentive_amount, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>TDS on Incentive</td>
                                            <?php $employee_data->tds_on_incentive = round($employee_data->tds_on_incentive); ?>
                                            <td class="text-right"><?= number_format($employee_data->tds_on_incentive, '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Net Pay</th>
                                            <th id="net_pay" class="text-right"><?= number_format($employee_data->net_pay, '2', '.', ''); ?></th>
                                        </tr>
                                        <tr>
                                            <td>Pay Mode</td>
                                            <td class="" style="width: 100px;">
                                                <select name="pay_mode" id="pay_mode" class="form-control input-sm" ></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Narration</td>
                                            <td class="text-right"><textarea id="narration" name="narration" class="" ><?= isset($employee_data->narration) && !empty($employee_data->narration) ? $employee_data->narration : ''?></textarea></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <input name="staff_id" type="hidden" value="<?= $employee_id; ?>">
                                <input name="professional_tax" type="hidden" value="<?= $employee_data->professional_tax; ?>">
                                <input name="actual_salary" type="hidden" value="<?= $employee_data->actual_salary; ?>">
                                <input name="basic_salary" type="hidden" value="<?= $employee_data->basic_pay; ?>">
                                <input name="allow_pf" type="hidden" value="<?= $employee_data->allow_pf; ?>">
                                <input name="allow_esi" type="hidden" value="<?= $employee_data->allow_esi; ?>">
                                <input name="pf_per" type="hidden" value="<?= $employee_data->pf_per; ?>">
                                <input name="ctc_pf" type="hidden" value="<?= $employee_data->pf_amount; ?>">
                                <input name="salary" type="hidden" value="<?= $employee_data->salary; ?>">
                                <input name="esi_per" type="hidden" value="<?= $employee_data->esi_per; ?>">
                                <input name="esi" type="hidden" value="<?= $employee_data->esi_amount; ?>">
                                <input name="pf" type="hidden" value="<?= $employee_data->pf_amount; ?>">
                                <input name="incentive" type="hidden" value="<?= $employee_data->incentive_amount; ?>"> 
                                <input name="tds_on_incentive" type="hidden" value="<?= $employee_data->tds_on_incentive; ?>">
                                <input name="net_pay" type="hidden" id="net_pay_input" value="<?= $employee_data->net_pay; ?>">
                                <input name="staff_id" type="hidden" value="<?= $employee_id; ?>">
                                <input name="year" type="hidden" value="<?= $year; ?>">
                                <input name="month" type="hidden" value="<?= $month; ?>">
                                <input type="submit" value="Save Salary" class="btn btn-primary pull-right btn-sm pull-left">
                            </form>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        calculate_netpay();
        <?php if (!isset($month) || !isset($year)) { ?>
            $('#myModal').modal({backdrop: 'static', keyboard: false});
            $('#myModal').modal('show');
        <?php } ?>
            
        initAjaxSelect2($("#employee_id"), "<?= base_url('app/staff_select2_source') ?>");
        <?php if (isset($employee_id)) { ?>
            setSelect2Value($("#employee_id"), "<?= base_url('app/set_staff_select2_val_by_id/' . $employee_id) ?>");
        <?php } ?>
            
        initAjaxSelect2($("#pay_mode"),"<?=base_url('app/payment_by_select2_source')?>");
        <?php if(isset($employee_data->pay_mode)){ ?>
            setSelect2Value($("#pay_mode"),"<?=base_url('app/set_payment_by_select2_val_by_id/'.$employee_data->pay_mode)?>");
		<?php } ?>
                
        <?php 
            if ($this->applib->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "manage")) { } else {
                $employee_id = $this->staff_id = $this->session->userdata('is_logged_in')['staff_id']; 
        ?>
                setSelect2Value($("#employee_id"), "<?= base_url('app/set_staff_select2_val_by_id/' . $employee_id) ?>");
                $("#employee_id").prop("disabled", true);
        <?php } ?>
            
        $(document).on('keyup', '#other_deduction, #late_coming_charge, #hra, #conveyance, #mediclaim',function(){
            calculate_netpay();
        });
        
        $(".select_all").attr("autocomplete", "off");
        
        $(".select_all").focus(function () {
            $(this).select();
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_salary_data").submit();
                return false;
            }
        }); 
        $(document).on('submit', '#save_salary_data', function (){
            var postData = new FormData(this);
             $.ajax({
                 url: "<?= base_url('hr/save_salary_data') ?>",
                 type: "POST",
                 processData: false,
                 contentType: false,
                 cache: false,
                 data: postData,
                 success: function (response) {
                     var json = $.parseJSON(response);
                     if (json['success'] == 'Added'){
                         window.location.href = "<?php echo base_url('hr/salary-report') ?>";
                     }
                 },
             });
             return false;
         }); 
    });
    
    function calculate_netpay(){
        var other_deduction = $('#other_deduction').val();
        var late_coming_charge = $('#late_coming_charge').val();
        var hra = $('#hra').val();
        var conveyance = $('#conveyance').val();
        var mediclaim = $('#mediclaim').val();
        var net_pay = <?= isset($employee_data->net_pay) && !empty($employee_data->net_pay) ? $employee_data->net_pay : 0 ; ?>;
        var total_net_pay = parseFloat(net_pay) - parseFloat(other_deduction || 0) - parseFloat(late_coming_charge || 0) + parseFloat(hra || 0) + parseFloat(conveyance || 0) + parseFloat(mediclaim || 0);
        $('#net_pay').html(Math.round(total_net_pay).toFixed(2));
        $('#net_pay_input').val(Math.round(total_net_pay).toFixed(2));
    }
    
    function display_as_a_viewpage_salary() {
        $("#pay_mode").attr('disabled', 'disabled');
        $("#save_salary_data input,textarea").prop("disabled", true);
        $("#save_salary_data input,textarea").css("background-color", '#fff');
        $("#save_salary_data input,textarea").css("border", 'none');
        $("#save_salary_data input,textarea").css("box-shadow", 'none');
        $("#save_salary_data .select2-selection").css("background-color", '#fff !important');
        $("#save_salary_data .select2-selection").css("border", 'none');
        $("#save_salary_data .select2-selection").css("box-shadow", 'none');
        $("#save_salary_data .select2-selection__arrow").css("display", 'none');
    }
    
    <?php if (isset($data['only_view_mode']) && $data['only_view_mode'] == '1' && !isset($segment3)){ ?>
	$(window).load(function () {
		display_as_a_viewpage_salary();
	});
	<?php } else {  ?>
        <?php if ($this->applib->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "manage")) { } else { ?>
            $('.show_salary').hide();
        <?php } ?>
    <?php } ?>
    <?php if (isset($data['only_view_mode']) && $data['only_view_mode'] == '1' && isset($segment3)){ ?>
	$(".print_btn").hide();
    <?php } ?>
</script>
