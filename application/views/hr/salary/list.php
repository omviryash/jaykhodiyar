<div class="content-wrapper">
    <section class="custom content-header"  >
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Employee Master : Salary List</small>
            <a href="<?= BASE_URL; ?>hr/salary_report" class="btn btn-info btn-xs pull-right">Calculate Salary</a>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Employee</label>
                            <select name="employee_id" id="employee_id" class="form-control input-sm select2"></select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Select Month</label>
                            <select name="month" id="month" class="form-control input-sm select2">
                                <option value="0">-- Select -- </option>
                                <?php for ($m = 1; $m < 13; $m++) { ?>
                                    <option value="<?= $m; ?>"><?= date('F', mktime(0, 0, 0, $m)); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Select Year</label>
                            <select name="year" id="year" class="form-control input-sm select2">
                                <option value="0">-- Select -- </option>
                                <?php for ($y = date('Y') - 2; $y < date('Y') + 3; $y++) { ?>
                                    <option value="<?= $y; ?>"><?= $y; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <br/>
                        <input type="button" value="Search" class="btn btn-primary pull-right btn-sm pull-left search_report">
                    </div><div class="clearfix"></div><br />
                    <table class="table display custom-table" id="salary_list">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Month-Year</th>
                                <th>Basic</th>
                                <th>ESIC</th>
                                <th>EPF</th>
                                <th>Professional Tax</th>
                                <th>Other</th>
                                <th>HRA</th>
                                <th>Conveyance</th>
                                <th>Mediclaim</th>
                                <th>Incentive After TDS</th>
                                <th>Net Pay</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Total : </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var table;
    $(document).ready(function() {
        $('.select2').select2();
        initAjaxSelect2($("#employee_id"), "<?= base_url('app/staff_select2_source') ?>");
        <?php if ($this->applib->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "manage")) { ?>
            
        <?php } else {
            $employee_id = $this->staff_id = $this->session->userdata('is_logged_in')['staff_id']; ?>
            setSelect2Value($("#employee_id"), "<?= base_url('app/set_staff_select2_val_by_id/' . $employee_id) ?>");
            $("#employee_id").prop("disabled", true);
        <?php } ?>
        table = $('#salary_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [0,'desc'],
            "ajax": {
                "url": "<?php echo site_url('hr/salary_list_datatable')?>",
                "type": "POST",
                "data": function (d) {
                    d.staff_id = $('#employee_id').val();
                    d.month = $('#month').val();
                    d.year = $('#year').val();
                },
            },
            "columnDefs": [{
                "className": "dt-right",
                "targets": [3,4,5,6,7,8,9,10,11,12],
            }],
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                pageTotal1 = api
                    .column( 3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 3 ).footer() ).html(
                    pageTotal1.toFixed(2)
                );
                pageTotal2 = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 4 ).footer() ).html(
                    pageTotal2.toFixed(2)
                );
                pageTotal3 = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 4 ).footer() ).html(
                    pageTotal3.toFixed(2)
                );
                pageTotal4 = api
                    .column( 5, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 5 ).footer() ).html(
                    pageTotal4.toFixed(2)
                );
                pageTotal5 = api
                    .column( 6, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 6 ).footer() ).html(
                    pageTotal5.toFixed(2)
                );
                pageTotal6 = api
                    .column( 7, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 7 ).footer() ).html(
                    pageTotal6.toFixed(2)
                );
                pageTotal7 = api
                    .column( 8, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 8 ).footer() ).html(
                    pageTotal7.toFixed(2)
                );
                pageTotal8 = api
                    .column( 9, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 9 ).footer() ).html(
                    pageTotal8.toFixed(2)
                );
                pageTotal9 = api
                    .column( 10, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 10 ).footer() ).html(
                    pageTotal9.toFixed(2)
                );
                pageTotal10 = api
                    .column( 11, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 11 ).footer() ).html(
                    pageTotal10.toFixed(2)
                );
        
                pageTotal = api
                    .column( 12, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 12 ).footer() ).html(
                    pageTotal.toFixed(2)
                );

            }
        });
        
        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=employee_salary_id&table_name=employee_salary',
                    success: function (data) {
                        table.draw();
                        show_notify('Salary Deleted Successfully!', true);
                    }
                });
            }
        });
        
        $(document).on('click', '.search_report', function(){
            table.draw();
        });
    });
</script>