<table class="table table-bordered" id="employees-leaves-table">
    <thead>
    <tr>
        <th>Action</th>
        <th>Name</th>
        <th>Email</th>
        <th>From Date</th>
        <th>To Date</th>
        <th>Status</th>
    </tr>
    </thead>
</table>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#employees-leaves-table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('hr/employees-leaves-datatable')?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });
    });
</script>