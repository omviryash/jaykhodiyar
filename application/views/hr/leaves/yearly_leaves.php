<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Leave Master : Weekly Leave</small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Full year
                </div>
                <div class="panel-body">
                    <div id="full-year"></div>
                    <p class="description">
                        <input type="hidden" id="altField" value="" name="dates" style="width:300px !important;height:300px  !important">
                    </p>
                    <div class="code-box">
                    </div>
                    <div class="form-group">
                        <div class="col-md-10">
                        </div>
                        <div class="col-md-2">
                            <?php
                            $access_edit  = $this->app_model->have_access_role(HR_YEARLY_LEAVES_MENU_ID, "edit");
                            $access_view  = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "view");
                            ?>
                            <?php if($access_edit):?>
                            <button type="button" class="btn btn-info btn-xs btn-block btn-save-yearly-leave">submit</button>
                            <?php endif;?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $(document).bind("keydown", function(e){
        if(e.ctrlKey && e.which == 83){
            $('.btn-save-yearly-leave').click();
            return false;
        }
    });  
    $(document).ready(function() {
        $(document).on('click', '.btn-save-yearly-leave', function () {
            var dates = $('#full-year').multiDatesPicker('getDates');
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>/hr/yearly-leaves/",
                data: {"dates": dates},
                cache: false,
                success: function (data) {
                    show_notify('Leave successfully saved!', true);
                }
            });
        });
    });
</script>