<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="custom content-header"  >
		<?php
		$this->load->view('shared/success_false_notify');
		?>
		<h1>
			<small class="text-primary text-bold">Leave Master : Total Free Leave For Employee</small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
			<?php
			$access_edit  = $this->app_model->have_access_role(HR_TOTAL_FREE_LEAVES_MENU_ID, "edit");
			$access_view  = $this->app_model->have_access_role(HR_TOTAL_FREE_LEAVES_MENU_ID, "view");
			?>
			<?php if($access_edit):?>
			<button type="button" class="btn btn-info btn-xs pull-right btn-submit save-leaves">submit</button>
			<?php endif;?>

		</h1>
	</section>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Total Free Leave For Employee
				</div>
				<div class="panel-body leave-inputs-wrapper">

					<div class="col-md-4">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label input-sm">Total Sl.</label>
							<div class="col-sm-8">
								<input type="number" class="form-control input-sm total_sl" name="total_sl" id="total_sl" placeholder="" value="<?=isset($leaves->total_sl)?$leaves->total_sl:0; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label input-sm">Total Cl.</label>
							<div class="col-sm-8">
								<input type="number" class="form-control input-sm total_cl" name="total_cl" id="total_cl" placeholder="" value="<?=isset($leaves->total_cl)?$leaves->total_cl:0; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label input-sm">Total El.Pl.</label>
							<div class="col-sm-8">
								<input type="number" class="form-control input-sm total_el_pl" name="total_el_pl" id="total_el_pl" placeholder="" value="<?=isset($leaves->total_el_pl)?$leaves->total_el_pl:0; ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col -->
	</div>
</div>
<script>
    
    $(document).bind("keydown", function(e){
        if(e.ctrlKey && e.which == 83){
            $('.save-leaves').click();
            return false;
        }
    });  

	$('.save-leaves').click(function(){
		var DataStr = $('.leave-inputs-wrapper :input').serialize();
		updateLeavesDetail(DataStr);
	});

	function updateLeavesDetail(DataStr) {
		var ReturnData = 0;
		$.ajax({
			method: "post",
			url: "<?=BASE_URL?>hr/update_total_leave/",
			data: DataStr,
			success: function (data) {
				data = JSON.parse(data);
				if (data.status == 1) {
					show_notify('Saved Successfully!', true);
				} else {
					show_notify(data.msg, false);
				}
			}
		});
	}
</script>
