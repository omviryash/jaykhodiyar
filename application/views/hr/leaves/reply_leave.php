<div class="content-wrapper">
    <section class="custom content-header"  >
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Leave Master : Reply Leave</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Leave Detail
                    <a href="<?=BASE_URL;?>hr/apply-leave" class="btn btn-info btn-xs pull-right">Add New</a>
                </div>
                <div class="panel-body">
                    <?php $this->load->view('hr/leaves/list_leave');?>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reply Leave
                </div>
                <div class="panel-body">
                    <form role="form" action="<?=base_url('hr/reply-leave');?>/<?=$leave->leave_id;?>" name="reply_leave" method="POST">
                        <input type="hidden" name="employee_id" value="<?=$leave->employee_id?>">
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Name</label>
                            <div class="col-md-9">
                                <input type="text" disabled value="<?=$leave->name;?>"required class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Email</label>
                            <div class="col-md-9">
                                <input type="text" disabled  value="<?=$leave->email;?>" required class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">From Date</label>
                            <div class="col-md-9">
                                <input type="text" value="<?=date('d-m-Y',strtotime($leave->from_date));?>" name="from_date" required class="form-control input-sm from_date input-datepicker">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">To Date</label>
                            <div class="col-md-9">
                                <input type="text" value="<?=date('d-m-Y',strtotime($leave->to_date));?>" name="to_date" required class="form-control input-sm to_date input-datepicker">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Day</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="day">
                                    <option value=""> - Select Day - </option>
                                    <?php
                                    if(!empty($days)):
                                        foreach ($days as $day) { ?>
                                            <option value="<?php echo $day->day_id; ?>" <?=$leave->day == $day->day_id?'selected':'';?>><?php echo $day->day_name; ?></option>
                                        <?php }
                                    endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Leave Type</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="leave_type">
                                    <option value="">Select</option>
                                    <option value="sl" <?=$leave->leave_type == 'sl'?'selected':'';?>>Seek Leave</option>
                                    <option value="cl" <?=$leave->leave_type == 'cl'?'selected':'';?>>Casual Leave</option>
                                    <option value="el/pl" <?=$leave->leave_type == 'el/pl'?'selected':'';?>>EL/PL Leave</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Reason</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="leave_detail" rows="1"><?=$leave->leave_detail;?></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Status</label>
                            <div class="col-md-9">
                                <select name="leave_status" class="form-control input-sm margin-top-5px">
                                    <option  value="pending" <?=$leave->leave_status == 'pending'?'selected':'';?>>Pending</option>
                                    <option  value="approve" <?=$leave->leave_status == 'approve'?'selected':'';?>>Approve</option>
                                    <option  value="notapprove" <?=$leave->leave_status == 'notapprove'?'selected':'';?>>Not Approve</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Note</label>
                            <div class="col-md-9">
                                <textarea class="form-control margin-top-5px" name="leave_note" rows="1"><?=$leave->leave_note;?></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <button type="submit" style="" class="btn btn-block btn-info btn-xs">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>