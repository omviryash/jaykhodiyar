<div class="content-wrapper">
    <section class="custom content-header"  >
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Leave Master : Apply Leave</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reason
                </div>
                <div class="panel-body">
                    <?php $this->load->view('hr/leaves/list_leave');?>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Leave
                </div>
                <div class="panel-body">
                    <form role="form" action="<?=base_url('hr/apply-leave');?>" name="add_leave" method="POST" id="apply_leave_form">
                        <input type="hidden" name="employee_id" value="<?=$this->session->userdata('is_logged_in')['staff_id']?>">
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Name</label>
                            <div class="col-md-9">
                                <input type="text" disabled value="<?=$this->session->userdata('is_logged_in')['name']?>" required class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Email</label>
                            <div class="col-md-9">
                                <input type="text" disabled  value="<?=$this->session->userdata('is_logged_in')['email']?>" required class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">From Date</label>
                            <div class="col-md-9">
                                <input type="text" value="" name="from_date" required class="form-control input-sm input-datepicker">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">To Date</label>
                            <div class="col-md-9">
                                <input type="text" value="" name="to_date" required class="form-control input-sm  input-datepicker">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Day</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm select2" name="day">
                                    <option value=""> - Select Day - </option>
                                    <?php
                                    if(!empty($days)):
                                        foreach ($days as $day) { ?>
                                        <option value="<?php echo $day->day_id; ?>"><?php echo $day->day_name; ?></option>
                                    <?php }
                                    endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Leave Type</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm select2" name="leave_type">
                                    <option value="">Select</option>
                                    <option value="sl">Seek Leave</option>
                                    <option value="cl">Casual Leave</option>
                                    <option value="el/pl">EL/PL Leave</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-3 input-sm">Reason</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="leave_detail" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <button type="submit" style="" class="btn btn-block btn-info btn-xs">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

