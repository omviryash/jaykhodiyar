<?php
    $weekly_holiday_day = '';
    $weekly_holiday_day = $this->applib->getWeeklyHoliday();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header"  >
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Leave Master : Weekly Leave</small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Weekly Leave Detail
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Weekly Holiday</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td><?=ucwords($weekly_holiday_day);?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        $access_edit  = $this->app_model->have_access_role(HR_WEEKLY_LEAVES_MENU_ID, "edit");
        $access_view  = $this->app_model->have_access_role(HR_WEEKLY_LEAVES_MENU_ID, "view");
        ?>
        <?php if($access_edit):?>
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Weekly Holiday
                </div>
                <div class="panel-body">
                    <form action="<?=base_url()?>hr/weekly_leaves" method="post" id="weekly_leave_form">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label input-sm">Day</label>
                            <div class="col-md-9">
                                <select name="weekly_holiday_day" class="form-control input-sm select2">
                                    <option  <?=$weekly_holiday_day == 'sunday'?'selected':''?> value="sunday">Sunday</option>
                                    <option  <?=$weekly_holiday_day == 'monday'?'selected':''?> value="monday">Monday</option>
                                    <option  <?=$weekly_holiday_day == 'tuesday'?'selected':''?> value="tuesday">Tuesday</option>
                                    <option  <?=$weekly_holiday_day == 'wednesday'?'selected':''?> value="wednesday">Wednesday</option>
                                    <option  <?=$weekly_holiday_day == 'thursday'?'selected':''?> value="thursday">Thursday</option>
                                    <option  <?=$weekly_holiday_day == 'friday'?'selected':''?> value="friday">Friday</option>
                                    <option  <?=$weekly_holiday_day == 'saturday'?'selected':''?> value="saturday">Saturday</option>
                                </select>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-info btn-xs btn-block">submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>
</div>
<script type="text/javascript">
    console.log('alert');
    $(document).ready(function() {
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $(document).find('#weekly_leave_form').submit();
                return false;
            }
        });  
    });
</script>