<div class="content-wrapper">
	<form class="form-horizontal" action="" method="post" id="save_employee" data-parsley-validate="">
		<?php if (isset($employee_data->staff_id) && !empty($employee_data->staff_id)) { ?>
		<input type="hidden" name="staff_id" id="staff_id" value="<?= $employee_data->staff_id ?>">
		<?php } ?>
		<section class="custom content-header">
			<h1>
				<small class="text-primary text-bold">Employee Master : Employee</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : F1 = Tab 1, F2 = Tab 2, Ctrl+S = Save</label></small>
				<div class="pull-right">
					<?php
					$access_view = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "view");
					$access_add = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "add");
					$access_edit = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "edit");
					$access_change_password = $this->app_model->have_access_role(HR_CHANGE_EMPLOYEE_PASSWORD_MENU_ID, "allow");
					?>
					<?php if ($access_add || $access_edit): ?>
					<button type="submit" class="btn btn-info btn-xs pull-right module_save_btn" style="margin: 5px;">Save</button>
					<?php endif; ?>

					<?php if ($access_view): ?>
					<a href="<?= base_url() ?>hr/employee_master/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Employee List</a>
					<?php endif;?>

					<?php if ($access_add): ?>
					<a href="<?= base_url() ?>hr/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Employee</a>
					<?php endif; ?>
				</div>
			</h1>
		</section>
		<div class="clearfix">
			<?php if ($access_add || $access_edit): ?>
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Add Employee</a></li> 
						<?php if (isset($employee_data->staff_id) && !empty($employee_data->staff_id)) { ?>
						<li><a href="#tab_2" data-toggle="tab" id="tabs_2">Login</a></li>
						<?php } ?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="row"> 
								<div class="col-md-12">
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Personal Information </legend>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="name" class="col-sm-5 input-sm">Name<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[name]" id="name" class="form-control input-sm" value="<?= (isset($employee_data->name)) ? $employee_data->name : ''; ?>" required="required">
													</div>
												</div>
												<div class="clearfix"></div>
												<?php if($access_change_password) { ?>
												<div class="form-group">
													<label for="pass" class="col-sm-5 input-sm">Login Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" name="employee_data[pass]" id="pass" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="retype_login_password" class="col-sm-5 input-sm">Retype Login Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" id="retype_login_password" class="form-control input-sm" >
													</div>
												</div>
												<div class="clearfix"></div>
												<?php } elseif (empty($employee_data->staff_id)) { ?>
												<div class="form-group">
													<label for="pass" class="col-sm-5 input-sm">Login Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" name="employee_data[pass]" id="pass" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="retype_login_password" class="col-sm-5 input-sm">Retype Login Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" id="retype_login_password" class="form-control input-sm" >
													</div>
												</div>
												<div class="clearfix"></div>
												<?php } ?>
												<div class="form-group">
													<label for="birth_date" class="col-sm-5 input-sm">Birth Date</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[birth_date]" id="birth_date" class="form-control input-sm input-datepicker" value="<?= (isset($employee_data->birth_date)) ? date('d-m-Y', strtotime($employee_data->birth_date)) : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="qualification" class="col-sm-5 input-sm">Qualification</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[qualification]" id="qualification" class="form-control input-sm" value="<?= (isset($employee_data->qualification)) ? $employee_data->qualification : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="father_name" class="col-sm-5 input-sm">Father Name</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[father_name]" id="father_name" class="form-control input-sm" value="<?= (isset($employee_data->father_name)) ? $employee_data->father_name : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="father_mobile" class="col-sm-5 input-sm">Mobile No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[father_mobile]" id="father_mobile" class="form-control input-sm" value="<?= (isset($employee_data->father_mobile)) ? $employee_data->father_mobile : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="husband_name" class="col-sm-5 input-sm">Husband Name</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[husband_name]" id="husband_name" class="form-control input-sm" value="<?= (isset($employee_data->husband_name)) ? $employee_data->husband_name : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="husband_mobile" class="col-sm-5 input-sm">Mobile No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[husband_mobile]" id="husband_mobile" class="form-control input-sm" value="<?= (isset($employee_data->husband_mobile)) ? $employee_data->husband_mobile : ''; ?>" >
													</div>
												</div>
                                                <div class="clearfix"></div>
                                                <?php if (isset($employee_data->employee_code)){ ?>
                                                    <div class="form-group">
                                                        <label for="chat_title" class="col-sm-5 input-sm">Employee Code</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control input-sm" value="<?= (isset($employee_data->employee_code)) ? 'JK'.$employee_data->employee_code : ''; ?>" readonly="">
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="image" class="col-sm-5 input-sm">Profile Image</label>
													<div class="col-sm-7">
														<input type="file" name="image" id="image" class="form-control input-sm">
														<?php if(isset($employee_data->image)){ ?>
														<input type="hidden" name="employee_data[image_old]" class="form-control input-sm" value="<?= (isset($employee_data->image)) ? $employee_data->image : ''; ?>">
														<?php } ?>
													</div>
												</div>
                                                <div class="clearfix"></div>
												<div class="form-group">
													<label for="" class="col-sm-5 input-sm"></label>
													<div class="col-sm-7 text-right">
														<img height="50px" width="50px" src="<?= isset($employee_data->image) ? image_url('staff/'.$employee_data->image) : base_url('resource/dist/img/default-user.png');?>">
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label for="email" class="col-sm-5 input-sm">Email<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="email" name="employee_data[email]" id="email" class="form-control input-sm" value="<?= (isset($employee_data->email)) ? $employee_data->email : ''; ?>" required="required" >
													</div>
												</div>
												<div class="clearfix"></div>
												<?php if($access_change_password){ ?>
												<div class="form-group">
													<label for="mailbox_password" class="col-sm-5 input-sm">Email Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" name="employee_data[mailbox_password]" id="mailbox_password" class="form-control input-sm" value="" <?= (isset($employee_data->mailbox_password)) ? '': 'required'; ?>>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="retype_email_password" class="col-sm-5 input-sm">Retype Email Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" id="retype_email_password" class="form-control input-sm">
													</div>
												</div>
												<div class="clearfix"></div>
												<?php } elseif (empty($employee_data->staff_id)) { ?>
												<div class="form-group">
													<label for="mailbox_password" class="col-sm-5 input-sm">Email Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" name="employee_data[mailbox_password]" id="mailbox_password" class="form-control input-sm" value="" <?= (isset($employee_data->mailbox_password)) ? '': 'required'; ?>>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="retype_email_password" class="col-sm-5 input-sm">Retype Email Password<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
														<input type="password" id="retype_email_password" class="form-control input-sm">
													</div>
												</div>
												<div class="clearfix"></div>
												<?php } ?>
												<div class="form-group">
													<label for="marriage_date" class="col-sm-5 input-sm">Anniversary</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[marriage_date]" id="marriage_date" class="form-control input-sm input-datepicker" value="<?= (isset($employee_data->marriage_date)) ? date('d-m-Y', strtotime($employee_data->marriage_date)) : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="blood_group" class="col-sm-5 input-sm">Blood Group</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[blood_group]" id="blood_group" class="form-control input-sm" value="<?= (isset($employee_data->blood_group)) ? $employee_data->blood_group : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="mother_name" class="col-sm-5 input-sm">Mother Name</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[mother_name]" id="mother_name" class="form-control input-sm" value="<?= (isset($employee_data->mother_name)) ? $employee_data->mother_name : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="mother_mobile" class="col-sm-5 input-sm">Mobile No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[mother_mobile]" id="mother_mobile" class="form-control input-sm" value="<?= (isset($employee_data->mother_mobile)) ? $employee_data->mother_mobile : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="wife_name" class="col-sm-5 input-sm">Wife Name</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[wife_name]" id="wife_name" class="form-control input-sm" value="<?= (isset($employee_data->wife_name)) ? $employee_data->wife_name : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="wife_mobile" class="col-sm-5 input-sm"> Mobile No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[wife_mobile]" id="wife_mobile" class="form-control input-sm" value="<?= (isset($employee_data->wife_mobile)) ? $employee_data->wife_mobile : ''; ?>" >
													</div>
												</div>
												<div class="form-group">
													<label for="signature" class="col-sm-5 input-sm">Signature Image</label>
													<div class="col-sm-7">
														<input type="file" name="signature" class="form-control input-sm" src="<?= isset($employee_data->signature) ? image_url('staff/signature/'.$employee_data->signature) : '' ;?>">
														<?php if(isset($employee_data->signature)){ ?>
														<input type="hidden" name="employee_data[signature_old]" class="form-control input-sm" value="<?=$employee_data->signature;?>">
														<?php } ?>
													</div>
												</div>
												<div class="clearfix"></div> 
												<div class="form-group">
                                                    <label for="" class="col-sm-8 input-sm text-danger">Only jpg image allowed to upload.</label>
													<div class="col-sm-4 text-right">
														<img height="50px" width="50px" src="<?= isset($employee_data->signature) ? image_url('staff/signature/'.$employee_data->signature) : '';?>">
													</div>
												</div>
											</div>
                                            <div class="col-md-3">
                                                <div class="clearfix"></div>
												<div class="form-group">
													<label for="reference_person" class="col-sm-5 input-sm">Reference Person</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[reference_person]" id="reference_person" class="form-control input-sm" value="<?= (isset($employee_data->reference_person)) ? $employee_data->reference_person : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="address" class="col-sm-5 input-sm">Address</label>
													<div class="col-sm-7">
														<textarea class="form-control" name="employee_data[address]" id="address"><?= (isset($employee_data->address)) ? $employee_data->address : ''; ?></textarea>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="contact_no" class="col-sm-5 input-sm">Company Contact</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[contact_no]" id="contact_no" class="form-control input-sm" value="<?= (isset($employee_data->contact_no)) ? $employee_data->contact_no : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="whatsapp_no" class="col-sm-5 input-sm">Whatsapp No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[whatsapp_no]" id="whatsapp_no" class="form-control input-sm" value="<?= (isset($employee_data->whatsapp_no)) ? $employee_data->whatsapp_no : ''; ?>" >
													</div>
												</div>
												<div class="form-group">
													<label for="driving_licence_no" class="col-sm-5 input-sm">Driving licence No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[driving_licence_no]" id="driving_licence_no" class="form-control input-sm" value="<?= (isset($employee_data->driving_licence_no)) ? $employee_data->driving_licence_no : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="passport_issue_by" class="col-sm-5 input-sm">Issue By</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[passport_issue_by]" id="passport_issue_by" class="form-control input-sm" value="<?= (isset($employee_data->passport_issue_by)) ? $employee_data->passport_issue_by : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="passport_expired_date" class="col-sm-5 input-sm">Expired Date</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[passport_expired_date]" id="passport_expired_date" class="form-control input-sm input-datepicker" value="<?= (isset($employee_data->passport_expired_date)) ? date('d-m-Y', strtotime($employee_data->passport_expired_date)) : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group margin-top-5px">
                                                    <label for="gender" class="col-sm-5 input-sm">Gender<span class="required-sign">&nbsp;*</span></label>
													<div class="col-sm-7">
                                                        <label class="input-sm"><input type="radio" value="m" name="employee_data[gender]" required <?= (isset($employee_data->gender)) ? ($employee_data->gender == 'm' ? 'checked' : '') : ''; ?>> Male</label>
														<label class="input-sm"><input type="radio" value="f" name="employee_data[gender]" required <?= (isset($employee_data->gender)) ? ($employee_data->gender == 'f' ? 'checked' : '') : ''; ?>> Female</label>
													</div>
												</div>
												<div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
													<label for="ref_per_mobile_no" class="col-sm-5 input-sm">Ref Per Mobile No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[ref_per_mobile_no]" id="ref_per_mobile_no" class="form-control input-sm" value="<?= (isset($employee_data->ref_per_mobile_no)) ? $employee_data->ref_per_mobile_no : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="permanent_address" class="col-sm-5 input-sm">Permanent Address</label>
													<div class="col-sm-7">
														<textarea class="form-control" name="employee_data[permanent_address]" id="permanent_address"><?= (isset($employee_data->permanent_address)) ? $employee_data->permanent_address : ''; ?></textarea>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="person_mobile_no" class="col-sm-5 input-sm">Person Mobile No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[person_mobile_no]" id="person_mobile_no" class="form-control input-sm" value="<?= (isset($employee_data->person_mobile_no)) ? $employee_data->person_mobile_no : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="personal_email" class="col-sm-5 input-sm">Personal Email</label>
													<div class="col-sm-7">
														<input type="email" name="employee_data[personal_email]" id="personal_email" class="form-control input-sm" value="<?= (isset($employee_data->personal_email)) ? $employee_data->personal_email : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="passport_no" class="col-sm-5 input-sm">Passport No.</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[passport_no]" id="passport_no" class="form-control input-sm" value="<?= (isset($employee_data->passport_no)) ? $employee_data->passport_no : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="passport_issue_date" class="col-sm-5 input-sm">Issue Date</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[passport_issue_date]" id="passport_issue_date" class="form-control input-sm input-datepicker" value="<?= (isset($employee_data->passport_issue_date)) ? date('d-m-Y', strtotime($employee_data->passport_issue_date)) : ''; ?>" >
													</div>
												</div>
                                                 <div class="clearfix"></div>
                                                <div class="form-group">
													<label for="chat_title" class="col-sm-5 input-sm">Chat Title</label>
													<div class="col-sm-7">
														<input type="text" name="employee_data[chat_title]" id="chat_title" class="form-control input-sm" value="<?= (isset($employee_data->chat_title)) ? $employee_data->chat_title : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
                                                <div class="form-group margin-top-5px">
													<label for="married_status" class="col-sm-5 input-sm">Marriage Status</label>
													<div class="col-sm-7">
														<label class="input-sm"><input type="radio" value="1" name="employee_data[married_status]" <?= (isset($employee_data->married_status)) ? ($employee_data->married_status == '1' ? 'checked' : '') : ''; ?>>Married</label>
														<label class="input-sm"><input type="radio" value="0" name="employee_data[married_status]" <?= (isset($employee_data->married_status)) ? ($employee_data->married_status == '0' ? 'checked' : '') : ''; ?>>Unmarried</label>
													</div>
												</div>
												<div class="clearfix"></div>
                                            </div>
											<div class="clearfix"></div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Current Position </legend> 

										<div class="col-md-4">
											<div class="form-group">
												<label for="designation_id" class="col-sm-5 input-sm">Designation</label>
												<div class="col-sm-7 dispaly-flex">
													<select name="employee_data[designation_id]" id="designation_id" class="form-control input-sm select2"></select>
												</div>
											</div>
											<div class="clearfix"></div>

											<div class="form-group">
												<label for="date_of_joining" class="col-sm-5 input-sm">Date of Joining</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[date_of_joining]" id="date_of_joining" class="form-control input-sm input-datepicker" value="<?= (isset($employee_data->date_of_joining)) ? date('d-m-Y', strtotime($employee_data->date_of_joining)) : ''; ?>" >
												</div>
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">
												<label for="department_id" class="col-sm-5 input-sm">Department</label>
												<div class="col-sm-7 dispaly-flex">
													<select name="employee_data[department_id]" id="department_id" class="form-control input-sm select2"></select>
												</div>
											</div>
											<div class="clearfix"></div>

											<div class="form-group">
												<label for="date_of_leaving" class="col-sm-5 input-sm">Date of Leaving</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[date_of_leaving]" class="form-control input-sm input-datepicker" value="<?= (isset($employee_data->date_of_leaving)) ? date('d-m-Y', strtotime($employee_data->date_of_leaving)) : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
										</div>

										<div class="col-md-4">
											<div class="form-group">
												<label for="grade_id" class="col-sm-5 input-sm">Grade</label>
												<div class="col-sm-7 dispaly-flex">
													<select name="employee_data[grade_id]" id="grade_id" class="form-control input-sm select2"></select>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</fieldset>
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Salary Detail </legend> 
										<div class="col-md-3">
											<div class="form-group">
												<label for="allow_pf" class="col-sm-5 input-sm">Allow For PF.</label>
												<div class="col-sm-7">
                                                    <label class="input-sm"><input type="radio" class="allow_pf" value="yes" name="allow_pf" <?= (isset($employee_data->allow_pf)) ? ($employee_data->allow_pf == 'yes' ? 'checked' : '') : ''; ?>> Yes</label>
													<label class="input-sm"><input type="radio" class="allow_pf" value="no" name="allow_pf" <?= (isset($employee_data->allow_pf)) ? ($employee_data->allow_pf == 'no' ? 'checked' : '') : ''; ?>> No</label>
												</div>
											</div>
                                            <div class="form-group">
												<label for="allow_esi" class="col-sm-5 input-sm">Allow For ESI.</label>
												<div class="col-sm-7">
                                                    <label class="input-sm"><input type="radio" class="allow_esi" value="yes" name="allow_esi" <?= (isset($employee_data->allow_esi)) ? ($employee_data->allow_esi == 'yes' ? 'checked' : '') : ''; ?>> Yes</label>
													<label class="input-sm"><input type="radio" class="allow_esi" value="no" name="allow_esi" <?= (isset($employee_data->allow_esi)) ? ($employee_data->allow_esi == 'no' ? 'checked' : '') : ''; ?>> No</label>
												</div>
											</div>
											<div class="clearfix"></div>

                                            <div class="form-group">
												<label for="salary" class="col-sm-5 input-sm">Actual Salary</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[salary]" id="salary" class="form-control input-sm num_only" value="<?= (isset($employee_data->salary)) ? $employee_data->salary : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            
                                            <div class="form-group">
												<label for="basic_pay" class="col-sm-5 input-sm">Basic Pay</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[basic_pay]" id="basic_pay" class="form-control input-sm" value="<?= (isset($employee_data->basic_pay)) ? $employee_data->basic_pay : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
												<label for="pf_per" class="col-sm-5 input-sm">PF %</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[pf_per]" id="pf_per" class="form-control input-sm num_only" value="<?= (isset($employee_data->pf_per)) ? $employee_data->pf_per : $c_pf_per; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="pf_amount" class="col-sm-5 input-sm">PF Amount</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[pf_amount]" id="pf_amount" class="form-control input-sm num_only" value="<?= (isset($employee_data->pf_amount)) ? $employee_data->pf_amount : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            
                                            <div class="form-group">
												<label for="pf_amount" class="col-sm-5 input-sm">ESI %</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[esi_per]" id="esi_per" class="form-control input-sm num_only" value="<?= (isset($employee_data->esi_per)) ? $employee_data->esi_per : $c_esi_per; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            
                                            <div class="form-group">
												<label for="pf_amount" class="col-sm-5 input-sm">ESI Amount</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[esi_amount]" id="esi_amount" class="form-control input-sm num_only" value="<?= (isset($employee_data->esi_amount)) ? $employee_data->esi_amount : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            
										</div>
                                        <div class="col-md-3">
                                            <div class="form-group">
												<label for="aadhaar_no" class="col-sm-5 input-sm">Aadhaar Card No.</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[aadhaar_no]" id="aadhaar_no" class="form-control input-sm" value="<?= (isset($employee_data->aadhaar_no)) ? $employee_data->aadhaar_no : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="pan_no" class="col-sm-5 input-sm">Pan No.</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[pan_no]" id="pan_no" class="form-control input-sm" value="<?= (isset($employee_data->pan_no)) ? $employee_data->pan_no : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="uan_no" class="col-sm-5 input-sm">UAN No.</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[uan_no]" id="uan_no" class="form-control input-sm" value="<?= (isset($employee_data->uan_no)) ? $employee_data->uan_no : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="pf_no" class="col-sm-5 input-sm">PF No.</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[pf_no]" id="pf_no" class="form-control input-sm" value="<?= (isset($employee_data->pf_no)) ? $employee_data->pf_no : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="esic_no" class="col-sm-5 input-sm">ESIC No.</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[esic_no]" id="esic_no" class="form-control input-sm" value="<?= (isset($employee_data->esic_no)) ? $employee_data->esic_no : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
												<label for="bank_name" class="col-sm-5 input-sm">Bank Name</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[bank_name]" id="bank_name" class="form-control input-sm" value="<?= (isset($employee_data->bank_name)) ? $employee_data->bank_name : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="ifsc_code" class="col-sm-5 input-sm">IFSC Code</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[ifsc_code]" id="ifsc_code" class="form-control input-sm" value="<?= (isset($employee_data->ifsc_code)) ? $employee_data->ifsc_code : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="bank_branch" class="col-sm-5 input-sm">Branch Name</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[bank_branch]" id="bank_branch" class="form-control input-sm" value="<?= (isset($employee_data->bank_branch)) ? $employee_data->bank_branch : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="bank_acc_no" class="col-sm-5 input-sm">Bank Account Name</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[bank_acc_name]" id="bank_acc_name" class="form-control input-sm" value="<?= (isset($employee_data->bank_acc_name)) ? $employee_data->bank_acc_name : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="form-group">
												<label for="bank_acc_no" class="col-sm-5 input-sm">Bank Acc. No.</label>
												<div class="col-sm-7">
													<input type="text" name="employee_data[bank_acc_no]" id="bank_acc_no" class="form-control input-sm" value="<?= (isset($employee_data->bank_acc_no)) ? $employee_data->bank_acc_no : ''; ?>" >
												</div>
											</div>
											<div class="clearfix"></div>
                                        </div>
									</fieldset>
                                    <fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Employee Wise Setting </legend>
                                            <div class="line_item_form item_fields_div">
                                                <div class="row">
                                                    <input type="hidden" name="line_items_index" id="line_items_index" />
                                                    <?php if(isset($employee_wise_incentive)){ ?>
                                                        <input type="hidden" name="line_items_data[id]" id="lineitem_id" />
                                                    <?php } ?>
                                                    <div class="col-md-4">
                                                        <label for="item_id" class="input-sm">Select Item<span class="required-sign">&nbsp;*</span></label>
                                                        <select name="line_items_data[item_id]" id="item_id" class="form-control input-sm item_id"></select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label for="no_of_item" class="input-sm">Select Count<span class="required-sign">&nbsp;*</span></label>
                                                        <select name="line_items_data[no_of_item]" id="no_of_item" class="form-control input-sm select2" >
                                                            <option value="1">1st</option>
                                                            <option value="2">2nd</option>
                                                            <option value="3">3rd</option>
                                                            <option value="4">4 & 4+</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label for="line_item_incentive" class="input-sm">Incentive %<span class="required-sign">&nbsp;*</span></label>
                                                        <input type="text" name="line_items_data[incentive]" id="line_item_incentive" class="form-control input-sm num_only" >
                                                    </div>
                                                    <div class="col-md-1"><br/>
                                                        <input type="button" id="add_lineitem" class="btn btn-info btn-xs pull-right add_lineitem" value="Add Item" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-sm-12"><br />
                                                    <table style="" class="table custom-table item-table">
                                                        <thead>
                                                            <tr>
                                                                <th>Action</th>
                                                                <th>Item Name</th>
                                                                <th>Count</th>
                                                                <th>Incentive</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="lineitem_list"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_2">
							<div class="row">
								<div class="col-md-12">
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Login Details </legend>
										<div class="col-md-6">
											<div class="form-group">
												<label for="created_by" class="col-sm-3 input-sm">Created By</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm created_by" id="created_by" value="<?= (isset($employee_data->created_by_name)) ? $employee_data->created_by_name : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="created_at" class="col-sm-3 input-sm">Created Date</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm created_at" id="created_at" value="<?= (isset($employee_data->created_at)) ? $employee_data->created_at : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="updated_by" class="col-sm-3 input-sm">Updated By</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm updated_by" id="updated_by" value="<?= (isset($employee_data->updated_by_name)) ? $employee_data->updated_by_name : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="updated_at" class="col-sm-3 input-sm">Updated Date</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm updated_at" id="updated_at" value="<?= (isset($employee_data->updated_at)) ? $employee_data->updated_at : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</form>
</div>
<script type="text/javascript">
    var first_time_edit_mode = 1;
    var on_save_add_edit_item = 0;
    var edit_lineitem_inc = 0;
    var lineitem_objectdata = [];
    <?php if (isset($employee_wise_incentive)) { ?>
        var li_lineitem_objectdata = [<?php echo $employee_wise_incentive; ?>];
        first_time_edit_mode = 0;
        var lineitem_objectdata = [];
        if (li_lineitem_objectdata != '') {
            $.each(li_lineitem_objectdata, function (index, value) {
                lineitem_objectdata.push(value);
            });
        }
    <?php } ?>
    display_lineitem_html(lineitem_objectdata);
	$(document).ready(function() {
		initAjaxSelect2($("#designation_id"),"<?=base_url('app/designation_select2_source')?>");
		<?php if(isset($employee_data->designation_id)){ ?>
		setSelect2Value($("#designation_id"),"<?=base_url('app/set_designation_select2_val_by_id/'.$employee_data->designation_id)?>");
		<?php } ?>
		initAjaxSelect2($("#department_id"),"<?=base_url('app/department_select2_source')?>");
		<?php if(isset($employee_data->department_id)){ ?>
		setSelect2Value($("#department_id"),"<?=base_url('app/set_department_select2_val_by_id/'.$employee_data->department_id)?>");
		<?php } ?>
		initAjaxSelect2($("#grade_id"),"<?=base_url('app/grade_select2_source')?>");
		<?php if(isset($employee_data->grade_id)){ ?>
		setSelect2Value($("#grade_id"),"<?=base_url('app/set_grade_select2_val_by_id/'.$employee_data->grade_id)?>");
		<?php } ?> 

        initAjaxSelect2($("#item_id"),'<?=base_url()?>app/sales_item_select2_source/');
        $('#no_of_item').select2();
        
        $(document).on('keyup', '#basic_pay, #pf_per', function () {
            calculate_pf_amount();
        });
        
        $(document).on('keyup, change', '#salary, #pf_amount, #esi_per, #basic_pay', function () {
            calculate_esi_amount();
        });
        
        $('input[type=radio][name=allow_pf]').change(function () {
            var allow_pf = $("input[name=allow_pf]:checked").val();
            if(allow_pf == 'no'){
                $('#pf_amount').val('');
                calculate_esi_amount();
            } else {
                calculate_pf_amount();
            }
        });
        
        $('input[type=radio][name=allow_esi]').change(function () {
            var allow_esi = $("input[name=allow_esi]:checked").val();
            if(allow_esi == 'no'){
                $('#esi_amount').val('');
            } else {
                calculate_esi_amount();
            }
        });
        
        $('#add_lineitem').on('click', function () {
            
            var item_id = $("#item_id").val();
            if (item_id == '' || item_id == null) {
                $("#item_id").select2('open');
                show_notify("Please Select Item!", false);
                return false;
            }
            var no_of_item = $("#no_of_item").val();
            if (no_of_item == '' || no_of_item == null) {
                $("#no_of_item").select2('open');
                show_notify("Please Select Count!", false);
                return false;
            }
            var line_item_incentive = $("#line_item_incentive").val();
            if (line_item_incentive == '' || line_item_incentive == null) {
                $("#line_item_incentive").focus();
                show_notify('Please Enter Incentive.', false);
                return false;
            }
            var key = '';
            var value = '';
            var lineitem = {};
            $('select[name^="line_items_data"]').each(function (e) {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            $('input[name^="line_items_data"]').each(function () {
                key = $(this).attr('name');
                key = key.replace("line_items_data[", "");
                key = key.replace("]", "");
                value = $(this).val();
                lineitem[key] = value;
            });
            var no_of_item_data = $('#no_of_item option:selected').html();
            var item_data = $('#item_id option:selected').html();
            
            lineitem['no_of_item_data'] = no_of_item_data;
            lineitem['item_name'] = item_data;
            var new_lineitem = JSON.parse(JSON.stringify(lineitem));
            var line_items_index = $("#line_items_index").val();
            if (line_items_index != '') {
                lineitem_objectdata.splice(line_items_index, 1, new_lineitem);
            } else {
                lineitem_objectdata.push(new_lineitem);
            } 
//            console.log(lineitem_objectdata);
            display_lineitem_html(lineitem_objectdata);
            $('#lineitem_id').val('');
            $('#line_items_index').val('');
            $("#item_id").val(null).trigger("change");
            $("#no_of_item").val(null).trigger("change");
            $("#line_item_incentive").val('');
            if (on_save_add_edit_item == 1) {
                on_save_add_edit_item == 0;
                $('#save_employee').submit();
            }
            edit_lineitem_inc = 0;
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#save_employee").submit();
                return false;
            }
        });        

		$(document).on('submit', '#save_employee', function () { 
			if($('#name').val() == "" || $('#name').val() == null){
				show_notify('Name is required!', false);
				return false;
			}
			if($('#email').val() == "" || $('#email').val() == null){
				show_notify('Email is required!', false);
				return false;
			}
			if($("#email").val() != ''){
				if(!isValidEmailAddress($("#email").val())){
					show_notify($("#email").val()+' is not valid email.',false);
					return false;
				}
			}
            <?php if(isset($employee_data->staff_id) && !empty($employee_data->staff_id)) { ?>
			var success_status = check_is_unique('staff','email',$("#email").val(),'staff_id','<?=$employee_data->staff_id;?>');
            <?php } else { ?>
			var success_status = check_is_unique('staff','email',$("#email").val());
			<?php } ?>
			if(success_status == 0){
				if($('p.reference-unique-error').length > 0){
					$("p.reference-unique-error").text('Email already exist!');
				}else{
					$("#email").after("<p class='text-danger reference-unique-error'>Email already exist!</p>");
				}
				return false;
			}else{
				$("p.reference-unique-error").text(' ');
			}
			<?php if(empty($employee_data)){ ?>
			if($('#pass').val() == "" || $('#pass').val() == null){
				show_notify('Log In Password is required!', false);
				return false;
			}
			if($('#mailbox_password').val() == "" || $('#mailbox_password').val() == null){
				show_notify('Email Password is required!', false);
				return false;
			}
			
			<?php } ?>
			if($('#pass').val() != "" || $('#pass').val() != null){
				if($('#retype_login_password').val() != $('#pass').val()){
					show_notify('Retype login Password is must be same!', false);
					return false;
				}
			}
			
			if($('#mailbox_password').val() != "" || $('#mailbox_password').val() != null){
				if($('#mailbox_password').val() != $('#retype_email_password').val()){
					show_notify('Mailbox Password is must be same!', false);
					return false;
				}
			}
            var postData = new FormData(this);
            var lineitem_objectdata_stringify = JSON.stringify(lineitem_objectdata);
            postData.append('line_items_data', lineitem_objectdata_stringify);
			$.ajax({
				url: "<?= base_url('hr/save_employee') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('hr/employee_master') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('hr/employee_master') ?>";
					}
				},
			});
			return false;
		});
	});
    
	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	}

    function display_lineitem_html(lineitem_objectdata) { 
        $('#ajax-loader').show();
        var new_lineitem_html = '';
        $.each(lineitem_objectdata, function (index, value) {
            var incentive = parseFloat(value.incentive);
            incentive = incentive.toFixed(2);
            var lineitem_edit_btn = '';
            var lineitem_delete_btn = '';
            lineitem_edit_btn = '<a class="btn btn-xs btn-primary btn-edit-item edit_lineitem_' + index + '" href="javascript:void(0);" onclick="edit_lineitem(' + index + ')"><i class="fa fa-edit"></i></a> ';
            lineitem_delete_btn = '<a class="btn btn-xs btn-danger btn-delete-item" href="javascript:void(0);" onclick="remove_lineitem(' + index + ')"><i class="fa fa-remove"></i></a>';
            var row_html = '<tr class="lineitem_index_' + index + '"><td class="">' +
                    lineitem_edit_btn +
                    lineitem_delete_btn +
                    '</td>' +
                    '<td>' + value.item_name + '</td>' +
                    '<td>' + value.no_of_item_data + '</td>' + 
                    '<td>' + incentive + '</td> </tr>';
            new_lineitem_html += row_html;
        });
        $('tbody#lineitem_list').html(new_lineitem_html);
        $('#ajax-loader').hide();
    }
    
    function edit_lineitem(index) {
        $('#ajax-loader').show();
        if (edit_lineitem_inc == 0) {
            edit_lineitem_inc = 1;
            $(".add_lineitem").removeAttr("disabled");
        }
        var value = lineitem_objectdata[index];
//        console.log(value);
        $("#line_items_index").val(index);
		if(typeof(value.id) != "undefined" && value.id !== null) {
			$("#lineitem_id").val(value.id);
		}
        $("#item_id").val(null).trigger("change");
        setSelect2Value($("#item_id"), "<?= base_url('app/set_sales_item_select2_val_by_id') ?>/" + value.item_id);
        $("#no_of_item").val(value.no_of_item).trigger("change");
        $("#line_item_incentive").val(value.incentive);
        $('#ajax-loader').hide();
    }

    function remove_lineitem(index) {
        value = lineitem_objectdata[index];
        if (confirm('Are you sure ?')) {
            if (typeof (value.id) != "undefined" && value.id !== null) {
                $('.line_item_form').append('<input type="hidden" name="deleted_lineitem_id[]" id="deleted_lineitem_id" value="' + value.id + '" />');
            }
            lineitem_objectdata.splice(index, 1);
            display_lineitem_html(lineitem_objectdata);
        }
    }
    
    function calculate_esi_amount(){
        var actual_salary = $('#salary').val();
        var esi_per = $('#esi_per').val();
        if(esi_per != '' && esi_per != null){
            var allow_esi = $("input[name=allow_esi]:checked").val();
            if(allow_esi == 'yes'){
                var esi_amount = (parseFloat(actual_salary || 1) * parseFloat(esi_per || 1))/ 100;
                $('#esi_amount').val(Math.round(esi_amount).toFixed(2));
            } else {
                $('#esi_amount').val('');
            }
        } else {
            $('#esi_amount').val('');
        }
    }
    
    function calculate_pf_amount(){
        var pf_per = $('#pf_per').val();
        var basic_pay = $('#basic_pay').val();
        if(pf_per != '' && pf_per != null){
            var pf_amount = (parseFloat(basic_pay || 1) * parseFloat(pf_per || 1))/ 100;
            $('#pf_amount').val(Math.round(pf_amount).toFixed(2)).trigger('change');
        } else {
            $('#pf_amount').val('').trigger('change');
        }
    }

</script>