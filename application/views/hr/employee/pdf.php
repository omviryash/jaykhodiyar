<html>
	<head>
		<title>
			Employee
		</title>
		<style>
			.text-center{
				text-align: center;
			}
			table{
				border-spacing: 0;
				width: 100%;
				border-bottom: 1px solid;
				border-right: 1px solid;
			}
			td{
				padding: 1px 1px 1px 1px;
				border-left: 1px solid;
				border-top: 1px solid;
				font-size:10px;
			}
			tr > td:last-child{
				border-right: 1px solid !important;
			}
			tr:last-child > td{
				border-bottom: 1px solid !important;
			}
			.text-right{
				text-align: right;
			}
			.text-bold{
				font-weight: 900 !important;
				font-size:12px !important;
			}
			.text-header{
				font-size: 20px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.width-50-pr{
				width:50%;
			}
			td.footer-sign-area{
				height: 100px;
				vertical-align: bottom;
				/*width: 33.33%;*/
				text-align: center;
			}
			.no-border{
				border: 0!important;
			}
			.footer-detail-area{
				color: #000000;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
		<table>
			<tr>
				<td class="text-center text-bold text-header" colspan="10">Employee </td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10"><b>Personal Information</b></td>
			</tr>
			<tr>
				<td class="no-border-right" style="height:100px;" colspan="8"></td>
				<td class="no-border-left" style="height:100px;" colspan="2">
                    <img height="100px" max-width="100%" src="<?=isset($employee_detail->image) ? image_url('staff/'.$employee_detail->image) : base_url('resource/dist/img/default-user.png');?>"></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2" width="145px">Name </td>
				<td class=" text-bold" colspan="3" width="190px"><b><?=$employee_detail->name;?></b></td>
				<td class=" text-bold" colspan="2" width="145px">Email </td>
				<td class=" text-bold" colspan="3" width="220px"><b><?=$employee_detail->email;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Birth Date :</td>
				<td class=" text-bold" colspan="3"><b><?= (isset($employee_detail->birth_date)) ? date('d/m/Y', strtotime($employee_detail->birth_date)) : ''; ?></b></td>
				<td class=" text-bold" colspan="2">Anniversary :</td>
				<td class=" text-bold" colspan="3"><b><?= (isset($employee_detail->marriage_date)) ? date('d/m/Y', strtotime($employee_detail->marriage_date)) : ''; ?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Qualification : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->qualification;?></b></td>
				<td class=" text-bold" colspan="2">Blood Group : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->blood_group;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Father Name : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->father_name;?></b></td>
				<td class=" text-bold" colspan="2">Mobile No. : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->father_mobile;?></b></td>
			</tr><tr>
			<td class=" text-bold" colspan="2">Mother Name : </td>
			<td class=" text-bold" colspan="3"><b><?=$employee_detail->mother_name;?></b></td>
			<td class=" text-bold" colspan="2">Mobile No. : </td>
			<td class=" text-bold" colspan="3"><b><?=$employee_detail->mother_mobile;?></b></td>
			</tr>

			<tr>
				<td class=" text-bold" colspan="2">Gender : </td>
				<td class=" text-bold" colspan="3"><b>
					<?php 
						if ($employee_detail->gender == 'm'){
							echo "Male";
						} elseif ($employee_detail->gender == 'f'){
							echo "Female";
						};
					?>
				</b></td>
				<td class=" text-bold" colspan="2">Marriage Status : </td>
				<td class=" text-bold" colspan="3"><b>
					<?php	
						if ($employee_detail->married_status == null){
						} elseif ($employee_detail->married_status == 1){
							echo "Married";
						} elseif ($employee_detail->married_status == 0){
							echo "Unmarried";
						}
					?>
				</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Husband Name : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->husband_name;?></b></td>
				<td class=" text-bold" colspan="2">Mobile No. : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->husband_mobile;?></b></td>
			</tr>
			<!--if merrird and gender is male-->
			<tr>
				<td class=" text-bold" colspan="2">Wife Name : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->wife_name;?></b></td>
				<td class=" text-bold" colspan="2">Mobile No. : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->wife_mobile;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Reference Person : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->reference_person;?></b></td>
				<td class=" text-bold" colspan="2">Ref Per Mobile No : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->ref_per_mobile_no;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Address : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->address;?></b></td>
				<td class=" text-bold" colspan="2">Permanent Address : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->permanent_address;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Company Contact : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->contact_no;?></b></td>
				<td class=" text-bold" colspan="2">Person Mobile No. : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->person_mobile_no;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Whatsapp No. : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->whatsapp_no;?></b></td>
				<td class=" text-bold" colspan="2">Personal Email : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->personal_email;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Driving licence No. : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->driving_licence_no;?></b></td>
				<td class=" text-bold" colspan="2">Passport No. : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->passport_no;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Issue By : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->passport_issue_by;?></b></td>
				<td class=" text-bold" colspan="2">Issue Date : </td>
				<td class=" text-bold" colspan="3"><b><?= (isset($employee_detail->passport_issue_date)) ? date('d/m/Y', strtotime($employee_detail->passport_issue_date)) : ''; ?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Expired Date : </td>
				<td class=" text-bold" colspan="3"><b><?= (isset($employee_detail->passport_expired_date)) ? date('d/m/Y', strtotime($employee_detail->passport_expired_date)) : ''; ?></b></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10"><b>Current Position</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Designation : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->designation_id;?></b></td>
				<td class=" text-bold" colspan="2">Department : </td>
				<td class=" text-bold" colspan="1" width="70px"><b><?=$employee_detail->department_id;?></b></td>
				<td class=" text-bold" colspan="1" width="40px">Grade : </td>
				<td class=" text-bold" colspan="1" width="40px"><b><?=$employee_detail->grade_id;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Date of Joining : </td>
				<td class=" text-bold" colspan="3"><b><?= (isset($employee_detail->date_of_joining)) ? date('d/m/Y', strtotime($employee_detail->date_of_joining)) : ''; ?></b></td>
				<td class=" text-bold" colspan="2">Date of Leaving : </td>
				<td class=" text-bold" colspan="3"><b><?= (isset($employee_detail->date_of_leaving)) ? date('d/m/Y', strtotime($employee_detail->date_of_leaving)) : ''; ?></b></td>
			</tr>
			<tr>
				<td class="text-center text-bold" colspan="10"><b>Salary Detail</b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Allow For PF : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->allow_pf;?></b></td>
				<td class=" text-bold" colspan="2">Salary : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->salary;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Basic Pay. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->basic_pay;?></b></td>
				<td class=" text-bold" colspan="2">House Rent : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->house_rent;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Traveling Allowance. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->traveling_allowance;?></b></td>
				<td class=" text-bold" colspan="2">Education Allowance : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->education_allowance;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Pf Amount. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->pf_amount;?></b></td>
				<td class=" text-bold" colspan="2">Bonus Amount : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->bonus_amount;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Medical Reimbursement. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->medical_reimbursement;?></b></td>
				<td class=" text-bold" colspan="2">Other Allotment : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->other_allotment;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Monthly Gross. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->monthly_gross;?></b></td>
				<td class=" text-bold" colspan="2">Pf Employee : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->pf_employee;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Professional Tax. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->professional_tax;?></b></td>
				<td class=" text-bold" colspan="2">Cost To Company : </td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->cost_to_company;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Aadhaar Card No.. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->aadhaar_no;?></b></td>
				<td class=" text-bold" colspan="2">Incentive %</td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->incentive;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Incentive Fix. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->incentive_fix;?></b></td>
				<td class=" text-bold" colspan="2">Bank Name</td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->bank_name;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">Bank Acc. No.</td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->bank_acc_no;?></b></td>
				<td class=" text-bold" colspan="2">Branch Name : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->bank_branch;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">IFSC Code. : </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->ifsc_code;?></b></td>
				<td class=" text-bold" colspan="2">Pan No.</td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->pan_no;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">PF No. </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->pf_no;?></b></td>
				<td class=" text-bold" colspan="2">UAN No.</td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->uan_no;?></b></td>
			</tr>
			<tr>
				<td class=" text-bold" colspan="2">ESIC No.</td>
				<td class=" text-bold" colspan="3"><b><?=$employee_detail->esic_no;?></b></td>
				<td class=" text-bold" colspan="2">Employee ID </td>
				<td class=" text-bold" colspan="3"> <b><?=$employee_detail->emp_id;?></b></td>
			</tr>
		</table>
	</body>
</html>