<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <section class="custom content-header"  >
        <h1>
            <small class="text-primary text-bold">Employee Master : Employee</small>
            <?php
            $access_add = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "add");
            $access_add_salary = $this->app_model->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "view");
            ?>
            <?php if ($access_add): ?>
                <a href="<?=base_url();?>hr/add" class="btn btn-info btn-xs pull-right">Add New</a>
            <?php endif; ?>
            <?php if($this->app_model->have_access_role(HR_CALCULATE_SALARY_MENU_ID, "view")){ ?>
                <a href="<?=base_url();?>hr/salary-report" class="btn btn-info btn-xs pull-right" style="margin-right: 5px;">Calculate Salary</a>
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
							<!--<div style="margin: 10px;">-->
                                <!--<div class="col-md-12">-->
                                <div class="col-md-4">
                                    <select id="have_party_or_not" class="form-group select2">
                                        <option value="0">All</option>
                                        <option value="1">Does Not Have Party</option>
                                        <option value="2">Have Party</option>
                                    </select>
                                </div>
                                    <table class="table table-bordered" id="employees-table">
                                        <thead>
                                            <tr>
                                                <th>Updated At</th>
                                                <th>Action</th>
                                                <th>Name</th>
                                                <th>Employee Code</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Salary</th>
                                                <th>Image</th>
                                                <th>Birth Date</th>
                                                <th>Qualification</th>
                                                <th>Date Of Joining</th>
                                            </tr>
                                        </thead>
                                    </table>
                                <!--</div>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="transfer_party_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">All Party Transfer To</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="transfer_from" id="transfer_from" />
                <ul class="users-list clearfix">
                    <?php
                        if (isset($agents) && is_array($agents)) {
                            foreach ($agents as $agent) {
                    ?>
                                <li style="width: 24%;">
                                    <a class="users-list-name transfer_to" href="javascript:void(0);" data-transfer_to="<?php echo $agent['staff_id']; ?>">
                                        <?php if (isset($agent['image']) && !empty($agent['image'])) { ?>
                                            <img src="<?php echo IMAGE_URL; ?>staff/<?php echo $agent['image']; ?>"
                                                 alt="User Image" width="50" height="50">
                                             <?php } else { ?>
                                                 <?php if (trim($agent['gender']) == 'f') { ?>
                                                <img src="<?php echo IMAGE_URL; ?>staff/default_f.png" alt="User Image" width="50" height="50">
                                            <?php } else if (trim($agent['gender']) == 'm') { ?>
                                                <img src="<?php echo IMAGE_URL; ?>staff/default_m.png" alt="User Image" width="50" height="50">
                                            <?php } else { ?>
                                                <img src="<?php echo IMAGE_URL; ?>staff/default.png" alt="User Image" width="50" height="50">
                                            <?php } ?>
                                        <?php } ?>
                                    </a>
                                    <a class="users-list-name transfer_to" href="javascript:void(0);" data-transfer_to="<?php echo $agent['staff_id']; ?>" ><?php echo $agent['name']; ?></a>
                                </li>
                    <?php
                            }
                        }
                    ?>
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var table;
    $('#have_party_or_not').select2();
    $(document).ready(function() {
        table = $('#employees-table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [0,'desc'],
            "ajax": {
                "url": "<?php echo site_url('hr/employee-datatable')?>",
                "type": "POST",
                "data": function (d) {
                    d.have_party_or_not = $('#have_party_or_not').val();
                },
            },
            "columnDefs": [
                {
                    "targets": [1,5],
                    "orderable": false
                },
                {
                    className: "hidden", "targets": [ 0 ]
                }
            ],
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
        
        $(document).on('click', '.open_transfer_party_modal', function(){
            $('#transfer_party_modal').modal({backdrop: 'static', keyboard: false});
            $('#transfer_party_modal').modal('show');
			var transfer_from = $(this).data('transfer_from');
            $('#transfer_from').val(transfer_from);
        });
        
        $(document).on('change', '#have_party_or_not', function (){
            table.draw();
        });
        
        $(document).on('click', '.modal-body .transfer_to', function () {
            $("#ajax-loader").show();
            var transfer_from = $('#transfer_from').val();
            var transfer_to = $(this).attr('data-transfer_to');
//            alert('transfer_from : ' + transfer_from + ' || transfer_to : ' + transfer_to);
            $.ajax({
                url: '<?=BASE_URL?>party/transfer_all_party_to_other',
                type: "POST",
                data: {
                    transfer_from : transfer_from,
                    transfer_to : transfer_to,
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $('#transfer_party_modal').modal('hide');
                        show_notify(data.message, true);
                        table.draw();
                    }else{
                        show_notify(data.message, false);
                    }
                    $("#ajax-loader").hide();
                }
            });
            return false;
        });
    });
</script>
