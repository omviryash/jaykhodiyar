<div class="content-wrapper">
    <section class="custom content-header"  >
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Employee Master : Employee</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Account Password
                                            </div>
                                            <div style="margin: 10px;">
                                                <form role="form" name="frmChangePassword" id="frmChangePassword" method="post" style="padding:20px;text-align:center;">
                                                    <input type="hidden" name="staff_id" value="<?=isset($employee->staff_id)?$employee->staff_id:'';?>">
                                                    <input type="hidden" name="ac_pass" value="ac_pass">
                                                    <div class="panel panel-flat">
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label input-sm">Employee Name</label>
                                                                <div class="col-sm-8 dispaly-flex">
                                                                    <!-- <input type="password" class="form-control input-sm" name="old_pass" required> -->
                                                                    <?php echo $employee->name?>
                                                                </div>
                                                                <!-- <?php if(isset($errors['old_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="old_pass"><?=$errors['old_pass']?></label><?php } ?> -->
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label input-sm">New Password</label>
                                                                <div class="col-sm-8 dispaly-flex">
                                                                    <input type="password" id="new_pass_login" class="form-control input-sm" name="new_pass" required>
                                                                </div>
                                                                <?php if(isset($errors['new_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="old_pass"><?=$errors['new_pass']?></label><?php } ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label input-sm">Confirm Password</label>
                                                                <div class="col-sm-8 dispaly-flex">
                                                                    <input type="password" class="form-control input-sm"  name="confirm_pass" id="conf_pass_login" required>
                                                                </div>
                                                                <?php if(isset($errors['confirm_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="confirm_pass"><?=$errors['confirm_pass']?></label><?php } ?>
                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <?php
                                                                    $access_edit  = $this->app_model->have_access_role(HR_MANAGE_EMPLOYEE_MENU_ID, "edit");
                                                                    ?>
                                                                    <?php if($access_edit):?>
                                                                    <button type="submit" class="btn btn-info btn-xs pull-right">Change Password</button>
                                                                    <?php endif;?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                E-mail Password
                                            </div>
                                            <div style="margin: 10px;">
                                                <form role="form" name="frmChangePassword_email" id="frmChangePassword_email" method="post" style="padding:20px;text-align:center;">
                                                    <input type="hidden" name="staff_id" value="<?=isset($employee->staff_id)?$employee->staff_id:'';?>">
                                                    <input type="hidden" name="email_pass" value="email_pass">
                                                    <div class="panel panel-flat">
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label input-sm">Employee Name</label>
                                                                <div class="col-sm-8 dispaly-flex">
                                                                    <!-- <input type="password" class="form-control input-sm" name="old_pass_email" required> -->
                                                                    <?php echo $employee->name?>
                                                                </div>
                                                                <!-- <?php if(isset($errors['old_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="old_pass"><?=$errors['old_pass']?></label><?php } ?> -->
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label input-sm">New Password</label>
                                                                <div class="col-sm-8 dispaly-flex">
                                                                    <input type="password" id="new_pass_email" class="form-control input-sm" name="new_pass_email" required>
                                                                </div>
                                                                <?php if(isset($errors['new_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="old_pass"><?=$errors['new_pass']?></label><?php } ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label input-sm">Confirm Password</label>
                                                                <div class="col-sm-8 dispaly-flex">
                                                                    <input type="password" class="form-control input-sm"  name="confirm_pass_email" id="conf_pass_email" required>
                                                                </div>
                                                                <?php if(isset($errors['confirm_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="confirm_pass"><?=$errors['confirm_pass']?></label><?php } ?>
                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <?php if($access_edit):?>
                                                                    <button type="submit" class="btn btn-info btn-xs pull-right">Change Password</button>
                                                                    <?php endif;?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('submit','#frmChangePassword',function(e){
            var new_password = $('#new_pass_login').val();
            var confirm_password = $('#conf_pass_login').val();
            if(new_password != confirm_password){
                show_notify('New password and Confirm password did not match!',false);
                //alert('New password and Confirm password not match!');
                return false;
            }else{
                return true;
            }
        });
        $(document).on('submit','#frmChangePassword_email',function(e){
            var new_password = $('#new_pass_email').val();
            var confirm_password = $('#conf_pass_email').val();
            if(new_password != confirm_password){
                show_notify('New password and Confirm password did not match!',false);
                //alert('New password and Confirm password not match!');
                return false;
            }else{
                return true;
            }
        });
    })
</script>
