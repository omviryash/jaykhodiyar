<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Print Letter</small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
            <?php
            $issued_letter_list_role = $this->applib->have_access_role(HR_ISSUED_LETTER_MENU_ID, "view");
            $print_role= $this->applib->have_access_role(HR_PRINT_LETTER_MENU_ID,"print");
            ?>
            <?php if ($issued_letter_list_role) { ?>
                <a href="<?= base_url() ?>hr/issued_letter_list/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Issued Letter List</a>
            <?php } ?>
            <?php if ($print_role) { ?>    
                <a href="<?= base_url() ?>hr/print_letter/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Print Letter</a>        
            <?php } ?>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <!-- Horizontal Form -->
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border text-primary text-bold"> Print Letter</legend>
                                <form method="POST" id="form_print_letter">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                                <div class="form-group">
                                                    <label for="letter_for" class="col-sm-2 input-sm">Letter For<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-4">
                                                        <?php if (isset($id) && !empty($id)) { ?>
                                                            <label class="input-sm"><?= $letter_for; ?></label>
                                                        <?php } else { ?>
                                                            <select name="letter_for_id" id="letter_for" class="form-control input-sm select2" required></select> 
                                                        <?php } ?>    
                                                    </div>
                                                    <label for="select_letter" class="col-sm-2 input-sm">Select Letter<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-4">
                                                        <?php if (isset($id) && !empty($id)) { ?>
                                                            <label class="input-sm"><?= $letter_topic; ?></label>
                                                        <?php } else { ?>
                                                            <select name="letter_topic_id" id="select_letter" class="form-control input-sm select2" required></select> 
                                                        <?php } ?>    
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="select_person" class="col-sm-2 input-sm">Person<span class="required-sign">&nbsp;*</span></label>
                                                    <div class="col-sm-4">
                                                        <?php if (isset($id) && !empty($id)) { ?>
                                                            <label class="input-sm"><?= $staff; ?></label>
                                                        <?php } else { ?>
                                                            <select name="person_id" id="select_person" class="form-control input-sm select2" required></select> 
                                                        <?php } ?>    
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <br/>
                                                    <table border="1" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td>&nbsp; For : Employee Name</td>
                                                                <td>&nbsp; Add this <b>"{(Employee Name)}"</b> in place of employee name</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Designation</td>
                                                                <td>&nbsp; Add this <b>"{(Designation)}"</b> in place of Designation</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Current Date</td>
                                                                <td>&nbsp; Add this <b>"{{Current Date}}"</b> in place of Current Date</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Date of Joining</td>
                                                                <td>&nbsp; Add this <b>"{{Date of Joining}}"</b> in place of Joining Date</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Date of relieving</td>
                                                                <td>&nbsp; Add this <b>"{{Date of relieving}}"</b> in place of relieving Date</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Reference No</td>
                                                                <td>&nbsp; Add this <b>"{{Reference No}}"</b> in place of Reference No</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div><br/><br/>
                                        <div class="col-md-12">
                                            <textarea name="letter_template" id="letter_template" class="form-control"><?= $letter_template; ?></textarea><br/>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-info btn-xs pull-right btn-save-item submit_button" id="form_print_letter" >Print Letter</button>
                                        </div>
                                    </div>
                                </form>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        CKEDITOR.replace('letter_template');
        initAjaxSelect2($("#letter_for"), "<?= base_url('app/letter_for_select2_source') ?>");


        $(document).on('change', '#letter_for', function () {
            var letter_for = $(this).val();
            initAjaxSelect2_letters($("#select_letter"), "<?= base_url('app/letter_select2_source/') ?>", letter_for);

            $('#select_letter').val(null).trigger('change');
            $("#select_letter").select2("val", "");
            if (letter_for == <?= LETTER_FOR_STAFF_ID ?>) {
                initAjaxSelect2($("#select_person"), "<?= base_url('app/staff_select2_source/') ?>");
                $('#select_person').val(null).trigger('change');
            } else if (letter_for == <?= LETTER_FOR_INTERVIEWED_PERSON_ID ?>) {
                initAjaxSelect2($("#select_person"), "<?= base_url('app/interviewed_person_select2_source/') ?>");
                $('#select_person').val(null).trigger('change');
            } else if (letter_for == null || letter_for == '') {
                initAjaxSelect2_letters($("#select_person"), "<?= base_url('app/letter_select2_source/') ?>", letter_for);
                $('#select_person').val(null).trigger('change');
                $("#select_person").select2("val", "");
            }
        });

        $(document).on('change', '#select_letter', function () {
            var select_letter = $(this).val();
            $.ajax({
                url: '<?php echo BASE_URL; ?>hr/get_letter_template',
                type: "POST",
                data: {letter: select_letter},
                dataType: 'json',
                success: function (data) {
                    if (data != null || data != '') {
                        CKEDITOR.instances['letter_template'].setData(data);
                    } else {
                        CKEDITOR.instances['letter_template'].setData('');
                    }
                }
            });
        });
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $('.submit_button').click();
                return false;
            }
        });

        $(document).on('submit', '#form_print_letter', function () {
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('hr/save_print_letter') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json['success'] == 'false') {
                        show_notify(json['msg'], false);
                    }
                    if (json['success'] == 'Added') {
                        if (json['print_letter_id']) {
                            window.open('<?php echo base_url('hr/letter_pdf/'); ?>' + '/' + json['print_letter_id'], '_blank');
                        }
                        window.location.href = "<?php echo base_url('hr/issued_letter_list') ?>";
                        return false;
                    } else if (json['success'] == 'Updated') {
                        if (json['print_letter_id']) {
                            window.open('<?php echo base_url('hr/letter_pdf/'); ?>' + '/' + json['print_letter_id'], '_blank');
                        }
                        window.location.href = "<?php echo base_url('hr/issued_letter_list') ?>";
                        return false;
                    }
                },
            });
            return false;
        });

    });

    function initAjaxSelect2_letters($selector, $source_url, letter_for) {
        $selector.select2({
            placeholder: " --Select-- ",
            allowClear: true,
            width: "100%",
            ajax: {
                url: $source_url,
                dataType: 'json',
                delay: 250,
                async: false,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        letter_for: letter_for
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * 5) < data.total_count
                        }
                    };
                },
                cache: true
            }
        });
    }
</script>