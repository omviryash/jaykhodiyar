<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php
			$print_role= $this->applib->have_access_role(HR_PRINT_LETTER_MENU_ID,"print");
			?>
			<small class='text-primary text-bold'>Issued Letter List</small>
			<?php if ($print_role) { ?>
				<a href="<?=base_url()?>hr/print_letter/" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Print Letter</a>
			<?php } ?>
		</h1>
	</section>
	<div class="clearfix">
		<div class="row">
			<div style="margin: 15px;">
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-body">
							<div class="row">
								<div class="clearfix"></div>
								<div class="col-md-12">
									<table id="issued_letter_list" class="table custom-table agent-table" width="100%">
										<thead>
											<tr>      		 
												<th>Action</th>
												<th>Reference No</th>
												<th>Letter For</th>
												<th>Person Name</th>
												<th>Letter Topic</th>
												<th>Created By</th>
												<th>Created Date</th>
											</tr>
										</thead>
										<tbody>                                                
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var table;
	$(document).ready(function() {

		table = $('#issued_letter_list').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[5, 'desc']],
			"ajax": {
				"url": "<?php echo site_url('hr/issued_letter_list_datatable')?>",
				"type": "POST",    
			},
			"scroller": {
				"loadingIndicator": true,
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%",
			"deferRender": true,
			"scrollY": 500,            
		}); 
            $(document).on("click",".delete_button",function(){
		var value = confirm('Are you sure delete this records?');
		var tr = $(this).closest("tr");
		if(value){
			$.ajax({
				url: $(this).data('href'),
				type: "POST",
				data: 'id_name=id&table_name=print_letter',
				success: function(data){
					table.draw();
					show_notify('Letter Deleted Successfully!', true);
				}
			});
		}
            });	
	});

</script>
