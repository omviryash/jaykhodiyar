<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        $isEdit = $this->app_model->have_access_role(HR_ADD_LETTER_MENU_ID, "edit");
        $isDelete = $this->app_model->have_access_role(HR_ADD_LETTER_MENU_ID, "delete");
        $isAdd = $this->app_model->have_access_role(HR_ADD_LETTER_MENU_ID, "add");
        ?>
        <h1>
            <small class="text-primary text-bold">Add Letter</small>
            <small class="text-center"><label class="label label-warning">Ctrl+S = Save</label></small>
            <?php if($isAdd) { ?>
            <a href="<?=base_url()?>hr/letter" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 10px;">Add Letter</a>
            <?php } ?>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if($isAdd || $isEdit) { ?>
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading clearfix">
                                                <?php if(isset($letter_id) && !empty($letter_id)){ ?>Edit 
                                                <?php } else { if($isAdd) { $btn_disable = null; }else{ $btn_disable = 'disabled';} ?>Add 
                                                <?php } ?>	Letter
                                            </div>
                                            <div style="margin:10px">	
                                                <form method="POST" id="form_letter">
                                                    <input type="hidden" class="form-control input-sm" name="letter_id" id="letter_id" value="<?php echo $letter_id; ?>" >
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="letter_for" class="col-sm-4 input-sm">Letter For<span class="required-sign">*</span></label>
                                                                <div class="col-sm-7">
                                                                    <select name="letter_for" id="letter_for" class="form-control input-sm"></select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="letter_name" class="col-sm-4 input-sm">Letter Name<span class="required-sign">*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" class="form-control input-sm" id="letter_name" name="letter_topic" value="<?php echo $letter_topic; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12">
                                                    <br/>
                                                    <table border="1" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td>&nbsp; For : Employee Name</td>
                                                                <td>&nbsp; Add this <b>"{(Employee Name)}"</b> in place of employee name</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Designation</td>
                                                                <td>&nbsp; Add this <b>"{(Designation)}"</b> in place of Designation</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Current Date</td>
                                                                <td>&nbsp; Add this <b>"{{Current Date}}"</b> in place of Current Date</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Date of Joining</td>
                                                                <td>&nbsp; Add this <b>"{{Date of Joining}}"</b> in place of Joining Date</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Date of relieving</td>
                                                                <td>&nbsp; Add this <b>"{{Date of relieving}}"</b> in place of relieving Date</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp; For : Reference No</td>
                                                                <td>&nbsp; Add this <b>"{{Reference No}}"</b> in place of Reference No</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                        <div class="col-md-12"><br/>
                                                            <textarea name="letter_template" id="editor1" class="form-control"><?=$letter_template;?></textarea><br/>
                                                        </div>
                                                        <?php 
                                                                 if(isset($letter_id) && !empty($letter_id)){ ?>
                                                        <button type="submit" style="width: 150px; margin: 1.5%;" class="btn btn-info btn-block btn-xs pull-right">Edit Letter</button>
                                                        <?php } else { ?>
                                                        <button type="submit" style="width: 150px; margin: 1.5%;" class="btn btn-info btn-block btn-xs pull-right" <?php echo $btn_disable;?>>Save</button>
                                                        <?php } ?>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>                                            
                                    </div>
                                    <?php } ?>
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Add Letter</div>
                                            <div style="margin: 10px;">
                                                <table id="example1" class="table custom-table sales-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Action</th>
                                                            <th>Letter For</th>
                                                            <th>Letters</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        if(!empty($results)) {
                                                            foreach ($results as $row) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php if($isEdit) { ?>
                                                                <a href="<?= base_url('hr/letter/'.$row->letter_id)?>" class="btn-primary btn-xs"><i class="fa fa-edit"></i></a> 
                                                                <?php } if($isDelete) { ?>
                                                                <a href="javascript:void(0);" class="delete_button btn-danger btn-xs" data-href="<?=base_url('hr/delete/'.$row->letter_id);?>"><i class="fa fa-trash"></i></a>
                                                                <?php } ?>
                                                            </td>
                                                            <td><?= $this->crud->get_column_value_by_id('letter_for','label',array('id'=>$row->letter_for)); ?></td>
                                                            <td><?=$row->letter_topic ?></td>

                                                        </tr>
                                                        <?php } } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
<script>
    $(document).ready(function(){ 
        <?php if($isAdd || $isEdit) { ?>
        CKEDITOR.replace('editor1');
        <?php } ?>
        initAjaxSelect2($("#letter_for"),"<?=base_url('app/letter_for_select2_source')?>");
        <?php if(isset($letter_for)){ ?>
        setSelect2Value($("#letter_for"),"<?=base_url('app/set_letter_for_select2_val_by_id/'.$letter_for)?>");
        <?php } ?>  
        $("#example1").DataTable({
            "scrollY":        "300px",
            "scrollCollapse": true,
            "aaSorting": [[1, 'asc']],
            "paging":         false,
        });
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#form_letter").submit();
                return false;
            }
        });   
        $(document).on('submit', '#form_letter', function (e) { 
            if($("#letter_for").val() == ""){
                show_notify('Select Letter For.', false);
                return false;
            }
            if($("#letter_name").val() == ""){
                show_notify('Fill value Letter name.', false);
                return false;
            }

            <?php if(isset($letter_id) && !empty($letter_id)){ ?>
            var success_status = check_is_unique('letters','letter_topic',$("#letter_name").val(),'letter_id','<?=$letter_id?>');
            <?php } else { ?>
            var success_status = check_is_unique('letters','letter_topic',$("#letter_name").val());
            <?php } ?>
            if(success_status == 0){
                if($('p.letter_topic-unique-error').length > 0){
                    $("p.letter_topic-unique-error").text('This Letter is already exist!');
                }else{
                    $("#letter_name").after("<p class='text-danger reference-unique-error'>This Letter is already exist!</p>");
                }
                return false;
            }else{
                $("p.letter_topic-unique-error").text(' ');
            }

            var value = $("#letter_name").val();
            var postData = new FormData(this);
            $.ajax({
                url: "<?= base_url('hr/save_letter') ?>",
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json['success'] == 'false'){
                        show_notify(json['msg'],false); 
                    }
                    if (json['success'] == 'Added'){
                        window.location.href = "<?php echo base_url('hr/letter') ?>";
                    }
                    if (json['success'] == 'Updated'){
                        window.location.href = "<?php echo base_url('hr/letter') ?>";
                    }
                },
            });
            return false;
        });

        $(document).on("click",".delete_button",function(){
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if(value){
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=letter_id&table_name=letters',
                    success: function(data){
                        tr.remove();
                        window.location.href = "<?php echo base_url('hr/letter') ?>";
                    }
                });
            }
        });

    });
</script>
