<div class="content-wrapper">
    <section class="custom content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Master : Expected Interview</small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
        </h1>
    </section>
    <div class="row">
        <?php if($this->app_model->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID, "view")) { ?>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Interview Detail
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" id="expected-interview-table">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Qualification</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if($this->app_model->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID, "add")){ ?>
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Interview Info
                </div>
                <div class="panel-body">
                    <form role="form" name="add_leave" method="POST" enctype="multipart/form-data" id="expected_interview_form">
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Name<span class="required-sign">&nbsp;*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="name" value="" class="form-control input-sm" required>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Email<span class="required-sign">&nbsp;*</span></label>
                            <div class="col-sm-8">
                                <input type="email" name="email" value="" class="form-control input-sm" required>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Image</label>
                            <div class="col-sm-8">
                                <input type="file" name="image"  class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Birth Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="birth_date"  value="<?= date('d-m-Y'); ?>" class="form-control input-sm input-datepicker" id="input-datepicker">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Qualification</label>
                            <div class="col-sm-8">
                                <input class="form-control input-sm" value="" type="text" name="qualification">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Gender</label>
                            <div class="col-sm-8">
                                <input type="radio" value="m"  name="gender"> Male
                                <input type="radio" value="f" name="gender"> Female
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Father Name</label>
                            <div class="col-sm-8">
                                <input type="text"  value="" name="father_name" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Mother Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="mother_name" value="" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Marriage Status</label>
                            <div class="col-sm-8">
                                <input type="radio"  value="1" name="married_status"> Married

                                <input type="radio"  value="0" name="married_status"> Unmarried
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Husband/wife Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="husband_wife_name" value="" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Blood Group</label>
                            <div class="col-sm-8">
                                <input type="text" name="blood_group" value="" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Contact No.</label>
                            <div class="col-sm-8">
                                <input type="number" name="contact_no" value=""  class="form-control input-sm">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-sm-4 input-sm">Address</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" name="address" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group margin-top-5px">
                            <label class="col-sm-4 input-sm">Permanent Address</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" name="permanent_address" rows="1"> </textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group margin-top-5px">
                            <label class="col-sm-4 input-sm">Interview Review</label>
                            <div class="col-sm-8">
                                <textarea name="interview_review" class="form-control" value="" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <button type="submit" style="" class="btn btn-block btn-info btn-xs submit_button">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#expected-interview-table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('hr/expected-interview-datatable')?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });
    });
    $(document).bind("keydown", function(e){
        if(e.ctrlKey && e.which == 83){
            $('.submit_button').click();
            return false;
        }
    });  
</script>
