<div class="content-wrapper">
	<form role="form" name="frm_interview_result" method="POST" enctype="multipart/form-data">
    <section class="custom content-header">
        <?php
            $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Interview Master : Expected Interview Result</small>
            <div class="pull-right">
                <?php if($this->app_model->have_access_role(HR_EXPECTED_INTERVIEW_MENU_ID, "edit")) { ?>
                <button type="submit" class="btn btn-info btn-xs">Save Interview Result</button>
                <?php } ?>
            </div>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Interview Result
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Personal Information</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Name<span class="required-sign">&nbsp;*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" name="name" value="<?=$interview->name;?>" class="form-control input-sm" required>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Birth Date</label>
                                <div class="col-sm-7">
                                    <input type="text" name="birth_date" value="<?php if(!empty($interview->birth_date) && strtotime($interview->birth_date) != 0){ echo date('d-m-Y',strtotime($interview->birth_date)); }else{ echo date('d-m-Y');} ?>" class="form-control input-sm input-datepicker">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Father Name</label>
                                <div class="col-sm-7">
                                        <input type="text"  value="<?=$interview->father_name;?>" name="father_name" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Husband/wife Name</label>
                                <div class="col-sm-7">
                                    <input type="text" name="husband_wife_name" value="<?=$interview->husband_wife_name;?>" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Address</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="1" name="address"><?=$interview->address;?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group margin-top-5px">
                                <label for="" class="col-sm-5 input-sm">Interview Review</label>
                                <div class="col-sm-7">
                                    <textarea name="interview_review" rows="1" class="form-control"><?=$interview->interview_review;?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Email<span class="required-sign">&nbsp;*</span></label>
                                <div class="col-sm-7">
                                    <input type="email" name="email" value="<?=$interview->email;?>" class="form-control input-sm" required>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Login Password</label>
                                <div class="col-sm-7">
                                    <input type="password" name="pass" value="" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Email Password</label>
                                <div class="col-sm-7">
                                    <input type="password" name="mailbox_password" value="" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Qualification</label>
                                <div class="col-sm-7">
                                    <input class="form-control input-sm" value="<?=$interview->qualification;?>" type="text" name="qualification">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Mother Name</label>
                                <div class="col-sm-7">
                                    <input type="text" name="mother_name" value="<?=$interview->mother_name;?>" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Blood Group</label>
                                <div class="col-sm-7">
                                    <input type="text" name="blood_group" value="<?=$interview->blood_group;?>" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Permanent Address</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="1" name="permanent_address"><?=$interview->permanent_address;?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group margin-top-5px">
                                <label for="" class="col-sm-5 input-sm">Interview Decision</label>
                                <div class="col-sm-7">
                                    <select name="interview_decision" class="form-control input-sm decision_select select2">
                                        <option value="">Select Decision</option>
                                        <option value="0" <?=$interview->interview_decision == '0'?'selected':'';?>>Employee Rejected</option>
                                        <option value="1" <?=$interview->interview_decision == '1'?'selected':'';?>>Employee Selected</option>
                                        <option value="2" <?=$interview->interview_decision == '2'?'selected':'';?>>Employee Hold</option>
                                        <option value="3" <?=$interview->interview_decision == '3'?'selected':'';?>>Employee Pipeline</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm">Image</label>
                                <div class="col-sm-7">
                                    <input type="file" name="image"  class="form-control input-sm">
                                    <input type="hidden" name="image_old"  class="form-control input-sm" value="<?=$interview->image;?>">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="" class="col-sm-5 input-sm"></label>
                                <div class="col-sm-7 text-right">
                                    <img height="50px" width="50px" src="<?=base_url()?><?=$interview->image;?>">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group margin-top-5px">
                                <label for="" class="col-sm-5 input-sm">Gender</label>
                                <div class="col-sm-7">
                                    <input type="radio" value="m"  name="gender" <?=$interview->gender == 'm'?'checked':'';?>> Male
                                    <input type="radio" value="f" name="gender" <?=$interview->gender == 'f'?'checked':'';?>> Female
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group margin-top-5px">
                                <label for="" class="col-sm-5 input-sm">Marriage Status</label>
                                <div class="col-sm-7">
                                    <input type="radio"  value="1" name="married_status" <?=$interview->married_status == '1'?'checked':'';?>>Married

                                    <input type="radio"  value="0" name="married_status" <?=$interview->married_status == '0'?'checked':'';?>>Unmarried
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group margin-top-5px">
                                <label for="" class="col-sm-5 input-sm">Contact No.</label>
                                <div class="col-sm-7">
                                    <input type="text" name="contact_no" value="<?=$interview->contact_no;?>" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="selected_info <?=$interview->interview_decision == '1'?'':'hidden';?>">
                            <div class="col-md-12">
                                <h4>Current Position</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Designation</label>
                                    <div class="col-sm-7">
                                        <select name="designation_id" id="" class="form-control input-sm select2">
                                            <option value="">--Select--</option>
                                            <?php foreach($designation as $dgn): ?>
                                                <option value="<?=$dgn->designation_id;?>"<?php if (isset($interview)) {if($interview->designation_id==$dgn->designation_id){ echo 'selected';} } ?>><?=$dgn->designation;?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Date of Joining</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="date_of_joining" value="<?=strtotime($interview->date_of_joining) != 0?date('d-m-Y',strtotime($interview->date_of_joining)):date('d-m-Y');?>" class="form-control input-sm input-datepicker">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Department</label>
                                    <div class="col-sm-7">
                                        <select name="department_id" id="" class="form-control input-sm select2">
                                            <option value="">--Select--</option>
                                            <?php foreach($department as $dep): ?>
                                                <option value="<?=$dep->department_id;?>"<?php if (isset($interview)) {if($interview->department_id==$dep->department_id){ echo 'selected';} } ?>><?=$dep->department;?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Date of Leaving</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="date_of_leaving" value="<?=strtotime($interview->date_of_leaving) != 0?date('d-m-Y',strtotime($interview->date_of_leaving)):'';?>" class="form-control input-sm input-datepicker" data-input_field="date_of_leaving">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Grade</label>
                                    <div class="col-sm-7">
                                        <select name="grade_id" id="" class="form-control input-sm select2">
                                            <option value="">--Select--</option>
                                            <?php foreach($grade as $grad): ?>
                                                <option value="<?=$grad->grade_id;?>"<?php if($interview->grade_id == $grad->grade_id){ echo 'selected';}?>><?=$grad->grade;?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-12">
                                <h4>Salary Detail</h4>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Allow For PF.</label>
                                    <div class="col-sm-7">
                                        <input type="radio" class="allow_pf" value="yes" <?=$interview->allow_pf == "yes"?'checked':'';?> name="allow_pf"> Yes
                                        <input type="radio" class="allow_pf" value="no" <?=$interview->allow_pf == "no"?'checked':'';?> name="allow_pf"> No
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">House Rent</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="house_rent" value="<?=$interview->house_rent;?>" type="text" name="house_rent">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Pf Amount</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="pf_amount" value="<?=$interview->pf_amount;?>" type="text" name="pf_amount">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Other Allotment</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="other_allotment" value="<?=$interview->other_allotment;?>" type="text" name="other_allotment">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Professional Tax</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="professional_tax" value="<?=$interview->professional_tax;?>" type="text" name="professional_tax">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Salary</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="salary" value="<?=$interview->salary;?>" type="text" name="salary">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Traveling Allowance</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="traveling_allowance" value="<?=$interview->traveling_allowance;?>" type="text" name="traveling_allowance">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Bonus Amount</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="bonus_amount" value="<?=$interview->bonus_amount;?>" type="text" name="bonus_amount">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Monthly Gross</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="monthly_gross" value="<?=$interview->monthly_gross;?>" type="text" name="monthly_gross">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Cost To Company</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="cost_to_company" value="<?=$interview->cost_to_company;?>" type="text" name="cost_to_company">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Basic Pay</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="basic_pay" value="<?=$interview->basic_pay;?>" type="text" name="basic_pay">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Education Allowance</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="education_allowance" value="<?=$interview->education_allowance;?>" type="text" name="education_allowance">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Medical Reimbursement</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="medical_reimbursement" value="<?=$interview->medical_reimbursement;?>" type="text" name="medical_reimbursement">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Pf Employee</label>
                                    <div class="col-sm-7">
                                        <input class="form-control calculation input-sm" id="pf_employee" value="<?=$interview->pf_employee;?>" type="text" name="pf_employee">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--
                            <div class="col-md-12">
                                <h4>Employee Detail</h4>
                            </div>-->
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Bank Name</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->bank_name;?>" type="text" name="bank_name">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">ESIC No.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->esic_no;?>" type="text" name="esic_no">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Employee ID.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->emp_id;?>" type="text" name="emp_id">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Pan No.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->pan_no;?>" type="text" name="pan_no">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Bank Acc. No.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->bank_acc_no;?>" type="text" name="bank_acc_no">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">UAN No.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->uan_id;?>" type="text" name="uan_id">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">PF No.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->pf_no;?>" type="text" name="pf_no">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">Employee No.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->emp_no;?>" type="text" name="emp_no">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="" class="col-sm-5 input-sm">ESIC ID</label>
                                    <div class="col-sm-7">
                                        <input class="form-control input-sm" value="<?=$interview->esic_id;?>" type="text" name="esic_id">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $(document).on('change','.decision_select',function(){
            if($(this).val() == 1){
                $('div.selected_info').removeClass('hidden');
            }else{
                $('div.selected_info').addClass('hidden');
            }
        });
    });
</script>
