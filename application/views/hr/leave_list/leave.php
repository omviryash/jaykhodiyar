<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="custom content-header">
        <?php
        $this->load->view('shared/success_false_notify');
        ?>
        <h1>
            <small class="text-primary text-bold">Leave Master : Leave</small>
            <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
        </h1>
    </section>
    <?php if($this->app_model->have_access_role(HR_LEAVE_MENU_ID, "edit")) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Employee Leave Information
                </div>
                <form class="frm-add-real-leave" method="post">
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 input-sm">Select Employee</label>
                                <div class="col-sm-8">
                                    <?php
                                        $staff = $this->applib->getStaff();
                                    ?>
                                    <select class="form-control input-sm disabled select2" name="employee_id">
                                        <?php
                                            if(!empty($staff)) {
                                                foreach ($staff as $staff_row) {
                                                    ?>
                                                    <option value="<?= $staff_row->staff_id; ?>"><?= $staff_row->name; ?></option>
                                                    <?php
                                                }
                                            }else{
                                                echo "<option>No any employee</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 input-sm">Leave Type</label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm select2" id="leave_type" name="leave_type">
                                        <option value="SL">SL</option>
                                        <option value="CL">CL</option>
                                        <option value="PL">EL/PL</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 input-sm">Leave Date</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="Select Confirmation Date" id="leave_date" class="form-control input-sm">
                                    <input type="hidden" name="from_date" id="from_date" value="<?=date('d-m-Y')?>">
                                    <input type="hidden" name="to_date" id="to_date" value="<?=date('d-m-Y')?>">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-7 pull-right">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-xs btn-block btn-add-real-leave" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            List of Employee Leave
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <form name="report-filter-form" method="post">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-5 input-sm">Select Employee</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm select2" name="employee_id" id="filter-employee">
                                    <?php
                                    if(!empty($staff)) {
                                        foreach ($staff as $staff_row) {
                                            ?>
                                            <option value="<?= $staff_row->staff_id; ?>"><?= $staff_row->name; ?></option>
                                            <?php
                                        }
                                    }else{
                                        echo "<option>No any employee</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-5 input-sm">Select Month</label>
                            <div class="col-sm-7">
                                <select class="form-control input-sm select2" name="month" id="filter-month">
                                    <option value="all">All</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-5 input-sm">Select Year</label>
                            <div class="col-sm-7">
                                <input type="text" id="filter-year" name="year_filter" class="form-control input-sm" value="<?=date('Y')?>">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <br/>
            <div class="clearfix"></div>
            <br/>
            <div class="table-responsive">
                <table class="padding-td-th-5px table table-bordered" id="table-leave">
                    <thead>
                    <tr>
                        <th>Month</th>
                        <th>01</th>
                        <th>02</th>
                        <th>03</th>
                        <th>04</th>
                        <th>05</th>
                        <th>06</th>
                        <th>07</th>
                        <th>08</th>
                        <th>09</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        <th>20</th>
                        <th>21</th>
                        <th>22</th>
                        <th>23</th>
                        <th>24</th>
                        <th>25</th>
                        <th>26</th>
                        <th>27</th>
                        <th>28</th>
                        <th>29</th>
                        <th>30</th>
                        <th>31</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var table = $('#table-leave').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('hr/leave')?>",
                "type": "POST",
                "data": function (d) {
                    d.employee_id = $("#filter-employee").val();
                    d.month = $("#filter-month").val();
                    d.year = $("#filter-year").val();
                }
            },
            "ordering": false,
            "bPaginate": false,
            "bFilter": false,
        });

        $('#leave_date').daterangepicker({
            "locale": {
                "format": "DD-MM-YYYY"
            }
        });

        $('#leave_date').on('apply.daterangepicker',function(ev,picker) {
            $('#from_date').val(picker.startDate.format('DD-MM-YYYY'));
            $('#to_date').val(picker.endDate.format('DD-MM-YYYY'));
        });

        $("#filter-year").datepicker({
            autoclose: true,
            format: "yyyy",
            startView: 2,
            minViewMode: 2,
            maxViewMode: 2
        });

        $(document).on('change','#filter-employee,#filter-month,#filter-year',function(){
            table.draw();
        });

        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $('.frm-add-real-leave').submit();
                return false;
            }
        });  
      
        $(document).on('submit','.frm-add-real-leave',function(e){
            e.preventDefault();
            $.ajax({
                url: $.core.url('hr/add-real-leave'),
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.status == true) {
                        show_notify(data.msg, true);
                        table.draw();
                    } else if (data.status == false) {
                        show_notify(data.msg, false);
                    }
                }
            });
        })
    })


</script>
