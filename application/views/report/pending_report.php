<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Report
			<small>Pending Report</small>
		</h1>
	</section>

    <div class="row">
		<br/>
        <div class="col-md-12">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-fuchsia">
					<div class="inner">
						<h3><?=isset($total_pending_enquiry)?$total_pending_enquiry:0?></h3>
						<p>Pending Enquiry</p>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
					<a href="<?=base_url()?>report/list_of_inquiry/?status=pending&user_id=<?=$staff_id?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3><?=isset($total_pending_quotation_for_followup)?$total_pending_quotation_for_followup:0?></h3>
						<p>Pending quotation for followup</p>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
					<a href="<?=base_url()?>report/pending_quotation_for_followup_report" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3><?=isset($total_due_dispatch)?$total_due_dispatch:0?></h3>
						<p>Due dispatch</p>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
					<a href="<?=base_url()?>report/commited_sales_order" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3><?=isset($total_upcoming_dispatch)?$total_upcoming_dispatch:0?></h3>
						<p>Upcoming Dispatches</p>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
					<a href="<?=base_url()?>report/upcoming_dispatches_report" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-orange">
					<div class="inner">
						<h3><?=isset($total_pending_sales_order_for_approval)?$total_pending_sales_order_for_approval:0?></h3>
						<p>Pending sales order for approval</p>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
					<a href="<?=base_url()?>sales_order/order_list?is_approved=0" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-purple">
					<div class="inner">
						<h3><?=isset($total_not_invoice_created_sales_order)?$total_not_invoice_created_sales_order:0?></h3>
						<p>Orders for which Invoice Not created yet</p>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
					<a href="<?=base_url()?>report/not_invoice_created_sales_order_report" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

            <!-- Horizontal Form -->
        </div>
    </div>
</div>