<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Employee Performance Chart </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <section class="col-md-12">			
                <!-- Map box -->
                <div class="box box-solid ">
                    <div class="box-body">
                        <!--<div id="world-map" style="height: 350px; width: 100%;"></div>-->
                        <div class="tab-pane active">
                            <div class="col-md-2">
                                <label>Employee<span style="color: red;">&nbsp;*</span></label>
                                <select name="staff_id" id="staff_id" class="form-control select2" ></select>
                            </div>
                            <?php 
                                $first_day = date("d-m-Y", strtotime("first day of this month"));
                                $last_day = date("d-m-Y", strtotime("last day of this month"));
                            ?>
                            <div class="col-md-2">
                                <label>From Date<span style="color: red;">&nbsp;*</span></label>
                                <input type="text" name="from_date" id="datepicker1" class="form-control input-datepicker from_date" value="<?php echo $first_day; ?>" >
                            </div>
                            <div class="col-md-2">
                                <label>To Date<span style="color: red;">&nbsp;*</span></label>
                                <input type="text" name="to_date" id="datepicker2" class="form-control input-datepicker to_date" value="<?php echo $last_day; ?>" >
                            </div>
                            <div class="col-md-2">
                                <label>Month</label>
                                <input type="text" name="" id="datepicker_month" class="form-control input-datepicker" value="" autocomplete="off">
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <button type="button" class="form-control btn btn-primary btn-sm view_chart">View Chart</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body"><hr />
                        <table class="table table-bordered text-center" >
                            <tr>
                                <th>Enquiry</th>
                                <th>Quotation</th>
                                <th>Total Order</th>
                                <th>Pending Order</th>
                                <th>Dispached Order</th>
                                <th>Canceled Order</th>
                                <th>Proforma Invoice</th>
                                <th>Challan</th>
                                <th>Invoice</th>
                            </tr>
                            <tr>
                                <td><span id="enquiry"></span></td>
                                <td><span id="quotation"></span></td>
                                <td><span id="sales_order"></span></td>
                                <td><span id="pending_sales_order"></span></td>
                                <td><span id="dispach_sales_order"></span></td>
                                <td><span id="canceled_sales_order"></span></td>
                                <td><span id="proforma_invoice"></span></td>
                                <td><span id="challan"></span></td>
                                <td><span id="invoice"></span></td>
                            </tr>
                        </table><hr />
                        <div class="barChartDiv tab-pane active" >
                            <canvas id="barChart" style="position: relative; height: 500px;"></canvas>
                        </div>
                    </div>
                    <!-- /.box-body-->
                    <div class="box-footer no-border">
                        <div class="row">
                            <!-- ./col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.box -->
            </section>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- ChartJS -->
<script src="<?php echo base_url(); ?>resource/plugins/chartjs/Chart.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initAjaxSelect2($("#staff_id"), "<?= base_url('app/staff_select2_source') ?>");
        $('.select2-selection__placeholder').html('All');
        $('#datepicker_month').datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months",
            showButtonPanel: true,
            autoclose: true,
        });

        $('#datepicker_month').datepicker().on('hide', function (e) {
            var old_date = $('#datepicker_month').val();
            set_to_date(old_date);
        });
        
        $(document).on('change', '#staff_id', function () {
            var staff_id = $("#staff_id").val();
            if(staff_id == null){
                $('.select2-selection__placeholder').html('All');
            }
        });
        $(document).on('click', '.view_chart', function () {
            if ($.trim($(".from_date").val()) == '') {
                show_notify('Please Enter From Date', false);
                $(".from_date").focus();
                return false;
            }
            if ($.trim($(".to_date").val()) == '') {
                show_notify('Please Enter To Date', false);
                $(".to_date").focus();
                return false;
            }
            var staff_id = $("#staff_id").val();
            var from_date = $("#datepicker1").val();
            var to_date = $("#datepicker2").val();
            if(staff_id == null){
                staff_id = '0';
            }
            $.ajax({
                url: "<?= base_url('report/get_employee_performance_chart_value/') ?>",
                method: "POST",
                data: {staff_id: staff_id, from_date : from_date, to_date : to_date},
                success: function (response) {
                    var json = $.parseJSON(response);
                    $('#enquiry').html('<a href="<?php echo base_url();?>enquiry/enquiry_list/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['enquiry'] + '</a>');
                    $('#quotation').html('<a href="<?php echo base_url();?>quotation/quotation_list/0/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['quotation'] + '</a>');
                    $('#sales_order').html('<a href="<?php echo base_url();?>sales_order/order_list/all/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['sales_order'] + '</a>');
                    $('#pending_sales_order').html('<a href="<?php echo base_url();?>sales_order/order_list/0/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['pending_sales_order'] + '</a>');
                    $('#dispach_sales_order').html('<a href="<?php echo base_url();?>sales_order/order_list/1/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['dispach_sales_order'] + '</a>');
                    $('#canceled_sales_order').html('<a href="<?php echo base_url();?>sales_order/order_list/2/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['canceled_sales_order'] + '</a>');
                    $('#proforma_invoice').html('<a href="<?php echo base_url();?>proforma_invoices/proforma_invoices_list/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['proforma_invoice'] + '</a>');
                    $('#challan').html('<a href="<?php echo base_url();?>challan/challan_list/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['challan'] + '</a>');
                    $('#invoice').html('<a href="<?php echo base_url();?>invoice/invoice_list/' +staff_id + '/' + from_date + '/' + to_date + '"  target="_blank">' + json['invoice'] + '</a>');
                    var areaChartData = {
                        labels: ['Enquiry', 'Quotation', 'Total Order', 'Pending Order', 'Dispached Order', 'Canceled order', 'Proforma Invoice', 'Challan', 'Invoice'],
                        datasets: [
                            {
                                label: 'Incoming',
                                fillColor: "#1f7eba", 
                                strokeColor: "#1f7eba", 
                                highlightFill: "#4692c1",
                                highlightStroke: "#4692c1",
                                data: [json['enquiry'], json['quotation'], json['sales_order'], json['pending_sales_order'], json['dispach_sales_order'], json['canceled_sales_order'], json['proforma_invoice'], json['challan'], json['invoice']]
                            }
                        ]
                    };
                    load_bar_chart(areaChartData);
                },

            });
            return false;
        });
        $('.view_chart').click();
    });

    function set_to_date(data) {
        var date1 = data;
        var month_year = date1.split('-');
        var year = month_year[0];
        var month = month_year[1];
        var date = new Date();
        date.setFullYear(month, year, 0);
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        $('#datepicker1').datepicker('setDate', firstDay);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        $('#datepicker2').datepicker('setDate', lastDay);
    }

    //-------------
    //- BAR CHART -
    //-------------
    function load_bar_chart(areaChartData){
        $('#barChart').remove(); // this is my <canvas> element
        $('.barChartDiv').append('<canvas id="barChart" style="position: relative; height: 500px;"></canvas>');
        var barChartCanvas = $('#barChart').get(0).getContext('2d');
        var barChart = new Chart(barChartCanvas).Bar(areaChartData, {
                responsive : true
        });
        barChart.update();
        var barChartOptions = {
            //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Scale label font colour
            scaleFontColor : "#FFFFFF",
            //String - Colour of the grid lines
            scaleGridLineColor: '#342e73',
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - If there is a stroke on each bar
            barShowStroke: true,
            //Number - Pixel width of the bar stroke
            barStrokeWidth: 2,
            //Number - Spacing between each of the X value sets
            barValueSpacing: 20,
            //Number - Spacing between data sets within X values
            barDatasetSpacing: 1,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //Boolean - whether to make the chart responsive
            responsive: true,
            maintainAspectRatio: true
        };

        barChartOptions.datasetFill = false;
    }
</script>
