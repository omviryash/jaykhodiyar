<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Item Diff Report</small>
            <small class="text-danger" style="font-size: 12px; font-weight: bold;">Cost : Count as per Purchase item Current Rate</small>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>From Date<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" name="from_date" id="datepicker1" class="form-control input-datepicker" value="<?php echo $this->applib->get_financial_start_date_by_date(date('d-m-Y')); ?>" >
                        </div>
                        <div class="col-md-2">
                            <label>To Date<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" name="to_date" id="datepicker2" class="form-control input-datepicker" value="<?php echo date('d-m-Y'); ?>" >
                        </div>
                        <div class="col-md-2">
                            <br/><button class="btn btn-primary btn-sm view_item_diff_report" style="margin: 5px;" >View Report</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="table-responsive" style="overflow-x:hidden">
                                <table id="production_item_diff_datatable" class="table custom-table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Item Code</th>
                                            <th class="text-center">Cost</th>
                                            <th class="text-center">Sell Amount</th>
                                            <th class="text-center">Margin</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
<!--                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th>0</th>
                                            <th>0</th>
                                            <th>0</th>
                                        </tr>
                                    </tfoot>-->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table;
        table = $('#production_item_diff_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "aaSorting": [[0,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('report/production_item_diff_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                    d.from_date = $("#datepicker1").val(),
                    d.to_date = $("#datepicker2").val()
                }
            },
            "columnDefs": [
                {"className": "text-right", "targets": 2},
                {"className": "text-right", "targets": 3},
                {"className": "text-right", "targets": 4}
            ],
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        }); 
        
        $(document).on('click', '.view_item_diff_report', function(){
            
            if ($.trim($("#datepicker1").val()) == '') {
                show_notify('Please Select From Date.', false);
                return false;
            }
            if ($.trim($("#datepicker2").val()) == '') {
                show_notify('Please Select To Date.', false);
                return false;
            }
            
            table.draw(); 
        });
    });
</script>