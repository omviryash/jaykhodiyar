<?php if ($this->session->flashdata('success') == true) { ?>
<div class="col-sm-4 pull-right">
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
    </div>
</div>
<?php } ?>
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Report : Sales Leads Summary</small>
            <!--<a href="<?=base_url('sales/inquiry')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Enquiry</a>-->
        </h1>
    </section>

    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <?php
                    if ($this->session->userdata('success_message')) {
                        echo '<div class="alert alert-success"><a class="close" data-close="alert"></a><strong>Success!</strong> ' . $this->session->userdata('success_message') . '</div>';
                        $success_message = array('success_message' => "");
                        $this->session->unset_userdata('success_message');
                    }
                    ?>
                    <!-- Start Advance Search -->
                    <div class="col-md-6">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm filtration_div"> Advanced Search </a>
                        <div class="filtration_fields" id="filtration_fields">
                            <br>
                            <div class="col-md-12">
                                <div class="row">
                                    <a href="javascript:void(0)" class="showhideColumn" data-columnindex='1'> Enquiry Number</a> -
                                    <a href="javascript:void(0)" class="showhideColumn" data-columnindex='2'> Customer Code</a> -
                                    <a href="javascript:void(0)" class="showhideColumn" data-columnindex='3'> Customer Name</a> -
                                    <a href="javascript:void(0)" class="showhideColumn" data-columnindex='4'> Enquiry Status</a> -
                                    <a href="javascript:void(0)" class="showhideColumn" data-columnindex='5'> Created At</a> 
                                </div>	
                            </div>
                            <br><br>


                            <table cellpadding="3" cellspacing="0" border="0" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>Target</th>
                                        <th style="text-align:center">Search text</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="filter_global">
                                        <td>Global search</td>
                                        <td align="center"><input type="text" class="form-control input-sm global_filter" id="global_filter"></td>
                                    </tr>
                                    <tr id="filter_col2" data-column="1">
                                        <td>Column - Enquiry Number</td>
                                        <td align="center"><input type="text" class="form-control input-sm column_filter" id="col1_filter"></td>
                                    </tr>
                                    <tr id="filter_col3" data-column="2">
                                        <td>Column - Customer Code</td>
                                        <td align="center"><input type="text" class="form-control input-sm column_filter" id="col2_filter"></td>
                                    </tr>
                                    <tr id="filter_col4" data-column="3">
                                        <td>Column - Customer Name</td>
                                        <!--<td align="center"><input type="text" class="column_filter" id="col3_filter"></td>-->
                                        <td align="center">
                                            <select name="customer_name" id="customer_name1" class="form-control input-sm" onchange='filterCustomername()'>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="filter_col5" data-column="4">
                                        <td>Column -  Enquiry Status</td>
                                        <td align="center"><input type="text" class="form-control input-sm column_filter" id="col4_filter"></td>
                                    </tr>
                                    <tr id="filter_col6" data-column="5">
                                        <td>Column - Created At</td>
                                        <td align="center"><input type="text" class="form-control input-sm column_filter" id="col5_filter"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <!--End Advance Search-->
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                search by status: 
                                <?php 
                                if($this->input->get_post("status")){ 
                                    $current_status = $this->input->get_post("status"); 
                                } else {
                                    $current_status = 'pending';
                                }
                                if($this->input->get_post("user_id")){ 
                                    $current_staff = $this->input->get_post("user_id"); 
                                } else {
                                    $current_staff = $this->session->userdata('is_logged_in')['staff_id'];
                                }
                                ?>
                                <select class="form-control select_status" name="status">
                                    <option value="all" <?php if($current_status == 'all'){ echo ' Selected '; } ?> >ALL</option>
                                    <?php
                                    if (!empty($enquiry_status)) {
                                        foreach ($enquiry_status as $status) {
                                            if (trim($status->inquiry_status) != "") {
                                                $selected = $current_status == strtolower($status->inquiry_status) ? 'selected' : '';
                                                echo '<option value="' . strtolower($status->inquiry_status) . '" ' . $selected . '>' . $status->inquiry_status . '</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php if($this->applib->have_access_current_user_rights(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
                            <div class="col-md-4">
                                search by user:
                                <select class="form-control select_user_id" name="user_id">
                                    <option value="all" <?php if($current_staff == 'all'){ echo ' Selected '; } ?> >ALL</option>
                                    <?php
                                        if (!empty($users)) {
                                            foreach ($users as $client) {
                                                if (trim($client->name) != "") {
                                                    $selected = $current_staff == $client->staff_id ? 'selected' : '';
                                                    echo '<option value="' . $client->staff_id . '" ' . $selected . '>' . $client->name . '</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <?php } ?>
                            <div class="col-md-4">
                                <!-- <button class="btn btn-info" type="submit">Search</button> -->
                                search by Lead or Inquiry:
                                <select class="form-control select_lead_inquiry" name="select_lead_inquiry">
                                    <option value="all">ALL</option>
                                    <option value="lead" selected>Lead</option>
                                    <option value="enquiry" >Inquiry</option>
                                </select>

                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>

                    </form>
                    <div class="table-responsive" style="overflow-x:hidden">
                        <?php
                        $role_delete = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "delete");
                        $role_edit = $this->app_model->have_access_role(ENQUIRY_MODULE_ID, "edit");
                        ?>
                        <table id="enquiry_list_table" class="table custom-table table-striped">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Enquiry Number#</th>
                                    <th>Customer Code</th>
                                    <th>Customer Name</th>
                                    <th>Enquiry Status</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function() {
        initAjaxSelect2($("#customer_name1"), "<?=base_url('app/party_select2_source')?>");
        $('#filtration_fields').hide();
        $(document).on('click', '.filtration_div', function () {
            $(".filtration_fields").toggle(500);
        });
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [2,3,4,5,6,7,8,9,10,11],
			}
		};
        
        var title = 'Quotation Followup History';
        
        table = $('#enquiry_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('report/sales_lead_summary_datatable')?>",
                "type": "POST",
                "data": function (d) {
                    d.user_id = $("select[name='user_id']").val();
                    d.inquiry_status = $("select[name='status']").val();
                    d.select_lead_inquiry = $("select[name='select_lead_inquiry']").val();
                }
            },
            <?php if ($this->applib->have_access_role(REPORT_SALES_LEAD_SUMMARY_MENU_ID, "export_data")) { ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: title, orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
            }
        });
    });

    $(document).on('change','.select_status,.select_user_id,.select_lead_inquiry',function(){
        //alert($("select[name='select_lead_inquiry']").val());
        table.draw();
    });

    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
    $('.showhideColumn').on('click', function(){
        var tbl_column = table.column($(this).attr('data-columnindex'));
        tbl_column.visible(!tbl_column.visible());
    });
    function filterGlobal () {
        $('#enquiry_list_table').DataTable().search(
            $('#global_filter').val()
        ).draw();
    }

    function filterColumn ( i ) {
        $('#enquiry_list_table').DataTable().column( i ).search(
            $('#col'+i+'_filter').val()
        ).draw();
    }
    function filterCustomername () {
        $('#enquiry_list_table').DataTable().column(3).search(
            $("#customer_name1 option:selected").text()
        ).draw();
    }

</script>
