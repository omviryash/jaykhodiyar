<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
        <h1>
            <small class="text-primary text-bold">Quotation Followup History</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
			<br/>
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label for="exampleInputName2">From Date</label>
								<input type="text" class="form-control input-sm input-datepicker" id="filter_from_date" name="filter_from_date" value="<?=date('d-m-Y',strtotime('-1 month'));?>">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label for="exampleInputEmail2">To Date</label>
								<input type="text" class="form-control input-sm input-datepicker" id="filter_to_date" name="filter_to_date" value="<?=date('d-m-Y');?>">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label for="exampleInputEmail2">Quotation Status</label>
								<select name="filter_status" id="filter_status" class="form-control input-sm">
									<option value="all">All</option>
									<?php
										if(!empty($quotation_status_data)):
											foreach($quotation_status_data as $quotation_status_row):
												?>
												<option value="<?=$quotation_status_row->id?>;"><?=$quotation_status_row->quotation_status;?></option>
									<?php
											endforeach;
										endif;
									?>
								</select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label for="exampleInputEmail2">Followup By</label>
								<select name="filter_followup_by" id="filter_followup_by" class="form-control input-sm">
									<option value="all">All</option>
									<?php
									if(!empty($staff_data)):
										foreach($staff_data as $staff__row):
											?>
											<option value="<?=$staff__row->staff_id?>;"><?=$staff__row->name;?></option>
											<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<table id="quotation_followup_history_datatable" class="table custom-table" width="100%">
								<thead>
								<tr>
									<!--<th>Sr. No.</th>-->
									<th>Action</th>
									<th>Sr.No.</th>
									<th>Date</th>
									<th>Followup History</th>
									<th>Status</th>
									<th>Quotation no</th>
									<th>Enquiry no</th>
									<th>Party</th>
									<th>Email Id</th>
									<th>Contact No</th>
									<th>City</th>
									<th>State</th>
									<!--<th>Country</th>
									<th>Staff</th>
									<th>Email</th>
									<th>Contact No</th>-->
								</tr>
								</thead>
							</table>
						</div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
	var table;
	$(document).ready(function(){
            $("#filter_followup_by").select2().each(function() {
            var selectedValue = $(this).val();
            $(this).html($("option", $(this)).sort(function(a, b) {
                return a.text < b.text ? -1 : 1
            }));
            $(this).val(selectedValue);
        });

		$("#filter_status,#filter_followup_by").select2();
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [2,3,4,5,6,7,8,9,10,11],
			}
		};
        
        var title = 'Quotation Followup History';

		table = $('#quotation_followup_history_datatable').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[0, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('report/quotation_followup_history_datatable')?>",
				"type": "POST",
				"data": function (d) {
					d.filter_status = $("#filter_status").val();
					d.filter_followup_by = $("#filter_followup_by").val();
					d.filter_from_date = $("#filter_from_date").val();
					d.filter_to_date = $("#filter_to_date").val();
				}

			},
            <?php if ($this->applib->have_access_role(REPORT_QUOTATION_FOLLOWUP_HISTORY_MENU_ID, "export_data")) { ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: title, orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
			"scrollY": 500,
			"scrollX": '100%',
			"scroller": {
				"loadingIndicator": true
			}
		});

		$(document).on("change","#filter_status,#filter_followup_by",function(){
			table.draw();
		});

		$('#filter_to_date,#filter_from_date').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy'
		}).on('changeDate', function(ev) {
			table.draw();
		});
	});
</script>