<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Report :  Delay Quotation Report</small><br>
        </h1>
    </section>
    <br>
    <div class="box box-info">
        <div class="box-body">
            <br>
            <div class="row">
                <div class="col-md-4">
                    <input type="text" class="form-control input-sm pull-right" id="filter_datepicker" value="<?=date('d-m-Y');?>">
                </div>
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="order_list_table" class="table custom-table table-striped">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Quotation No.</th>
                                <th>Enquiry No.</th>
                                <th>Customer Name </th>
                                <th>Quotation Date</th>
                                <th>Followup Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
    
</div>
<div class="modal fade" id="open_show_itemprint_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    Item Print
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="example1" class="table custom-table table-striped dataTable">
                        <thead>
                            <tr>
                                <th width="50%">
                                    Item
                                </th>
                                <th width="50%">
                                    Print
                                </th>
                                <!-- <th width="20%" class="text-center">
                                    Action
                                </th> -->
                            </tr>                        
                        </thead>
                        <tbody id="items_td">
                        </tbody>
                    </table>
                </div>
            </div>
       </div>
     </div>
</div>

<script>
	var table;
    $(document).ready(function() {

		/*$('#datepicker').change(function(){
			 //Change code!
			 //alert("on change");
			 table.draw();
		});*/
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [1,2,3,4,5],
			}
		};
        
        var title = 'Delay Quotation Report';
        
		table = $('#order_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'desc']],
            "ajax": {
                "url": "<?php echo site_url('report/delay_quotation_datatables')?>",
                "type": "POST",
                "data":function(d){
					d.review_date = $("#filter_datepicker").val();
				}
            },
            <?php if ($this->applib->have_access_role(REPORT_DELAY_QUOTATION_REPORT_MENU_ID, "export_data")) { ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: title, orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
			}
		});

        $('#filter_datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        }).on('changeDate', function(ev){
            table.draw();
        });
		
        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=sales_order',
                    success: function(data) {
                    }
                });
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=sales_order_id&table_name=sales_order_item',
                    success: function(data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

        $(document).on('click', '.show_itemprint_modal', function(e){
            e.preventDefault();
            var saleorder_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();

            $( "#items_td" ).html('');
            $.ajax({
                url: '<?php echo base_url('sales/feed_saleorders_items/') ?>/'+saleorder_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    if (data.item_data) {
                        jQuery.each( data.item_data, function( i, val ) {
                            $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('sales/sales_order_print/'); ?>/'+saleorder_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        });
                    }
                }
            });
        });

        $(document).on('click', '.show_work_order_itemprint_modal', function(e){
            e.preventDefault();
            var saleorder_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();

            $( "#items_td" ).html('');
            $.ajax({
                url: '<?php echo base_url('sales/feed_saleorders_items/') ?>/'+saleorder_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    if (data.item_data) {
                        jQuery.each( data.item_data, function( i, val ) {
                            $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('sales/work_order/'); ?>/'+saleorder_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        });
                    }
                }
            });
        });

    });
</script>
