<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
        <h1>
            <small class="text-primary text-bold">Enquiry Followup History</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
        	<form class="form-inline" method="post" style="margin: 15px;">
				<div class="form-group">
					<label for="exampleInputName2">From Date</label>
					<input type="text" class="form-control" id="datepicker1" name="from_date" value="<?=date('d-m-Y', strtotime($from_date));?>">
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2">To Date</label>
					<input type="text" class="form-control" id="datepicker2" name="to_date" value="<?=date('d-m-Y', strtotime($to_date));?>">
				</div>
				<button type="submit" class="btn btn-default">Filter</button>
			</form>
			<div class="clearfix"></div>
        	
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
							<div style="margin: 10px;">
								<div class="col-md-12">
									<table id="supplier_list_datatable" class="table custom-table stock-table" width="100%">
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Date</th>
												<th>Party</th>
												<th>Contact Person</th>
												<th>Contact No</th>
												<th>Email Id</th>
												<th>Address</th>
												<th>City</th>
												<th>State</th>
												<th>Country</th>
												<th>Enquiry Reference</th>
												<th>Staff</th>
												<th>Item</th>
												<th>Followup History</th>
											</tr>
										</thead>
										<tbody>
									<?php 
										$i = 1;
										if ($followup_history) {
											foreach ($followup_history as $f_history) { 
									?>
											<tr>
												<td><?=$i++;?></td>
												<td><?=$f_history['followup_date']?></td>
												<td><?=$f_history['party_name'].' ('.$f_history['party_code'].')';?></td>
												<td><?=$f_history['contact_person_name'];?></td>
												<td><?=$f_history['contact_person_phone'];?></td>
												<td><?=$f_history['contact_person_email'];?></td>
												<td><?=$f_history['address'];?></td>
												<td><?=$f_history['city'];?></td>
												<td><?=$f_history['state'];?></td>
												<td><?=$f_history['country'];?></td>
												<td><?=$f_history['reference_description'];?></td>
												<td><?=$f_history['created_name'];?></td>
												<td><?=$f_history['items'];?></td>
												<td><?=$f_history['history'];?></td>
											</tr>
									<?php	}
										} ?>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>