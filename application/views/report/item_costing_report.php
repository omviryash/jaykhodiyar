<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Item Costing Report</small>
            <small class="text-danger" style="font-size: 12px; font-weight: bold;">Purchase Amount : Count as per Purchase item Current Rate</small>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="bom_item_diff_datatable" class="table custom-table table-striped">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Item Code</th>
                                    <th>BOM Created Date</th>
                                    <th>BOM Effective Date</th>
                                    <th class="text-center">Purchase Amount</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table;
        table = $('#bom_item_diff_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "aaSorting": [[0,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('report/item_costing_datatable') ?>",
                "type": "POST",
            },
            "columnDefs": [
                {"className": "text-right", "targets": 4}
            ],
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        }); 
    });
</script>