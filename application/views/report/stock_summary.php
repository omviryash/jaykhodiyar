<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
        <h1>
            <small class="text-primary text-bold">Stock Report</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
        	<form class="form-inline" method="post" style="margin: 15px;">
				<div class="form-group">
					<label for="exampleInputName2">From Date</label>
					<input type="text" class="form-control" id="datepicker1" name="from_date" value="<?=date('d-m-Y', strtotime($from_date));?>">
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2">To Date</label>
					<input type="text" class="form-control" id="datepicker2" name="to_date" value="<?=date('d-m-Y', strtotime($to_date));?>">
				</div>
				<button type="submit" class="btn btn-default">Filter</button>
			</form>
			<div class="clearfix"></div>
        	
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
							<div style="margin: 10px;">
								<div class="col-md-12">
									<table id="supplier_list_datatable" class="table custom-table stock-table" width="100%">
										<thead>
											<tr>
												<th>Action</th>
												<th>Sr. No</th>
												<th>Item Name</th>
												<th>Opening</th>
												<th>Purchase</th>
												<th>Sold</th>	
												<th>Closing</th>
											</tr>
										</thead>
										<tbody>
									<?php 
										$i = 1;
										if ($items) {
											foreach ($items as $item_id => $item) { 
												$purchase_before = (array_key_exists($item_id, $purchase_stock['before_current'])) ? $purchase_stock['before_current'][$item_id] : 0;
												$sales_before = (array_key_exists($item_id, $sales_stock['before_current'])) ? $sales_stock['before_current'][$item_id] : 0;
												$purchase_current = (array_key_exists($item_id, $purchase_stock['current'])) ? $purchase_stock['current'][$item_id] : 0;
												$sales_current = (array_key_exists($item_id, $sales_stock['current'])) ? $sales_stock['current'][$item_id] : 0;
									?>
											<tr>
												<td>
												<?php
													$isEdit = $this->app_model->have_access_role(QUOTATION_MODULE_ID, "edit");
													if($isEdit){
														echo '<a href="' . base_url('item/add_item_master/' . $item['id']) . '" target="_blank" class="edit_button btn-primary btn-xs" data-href="#"><i class="fa fa-edit"></i></a>';
													}
												?>
												</td>
												<td><?=$i++;?></td>
												<td><?=$item['item_name'] .' ('.$item['item_code1'].')'?></td>
                                                <td class="text-right"><?php echo $purchase_before - $sales_before; ?></td>
												<td class="text-right"><?php echo $purchase_current; ?></td>
												<td class="text-right"><?php echo $sales_current; ?></td>
												<td class="text-right"><?php echo (($purchase_before - $sales_before) + $purchase_current) - $sales_current; ?></td>
											</tr>
									<?php	}	
										} ?>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>