<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Purchase Item Stock Sheet</small>
        </h1>
    </section>
    <div class="clearfix">
		<div class="col-md-12">
            <div class="box box-info">
				<div class="box-body">
                    <?php 
                        $first_day = date("d-m-Y", strtotime("first day of this month"));
                        $last_day = date("d-m-Y");
                    ?>
                    <div class="col-md-2">
                        <label for="from_date">From Date</label>
                        <input type="text" name="from_date" id="datepicker1" class="form-control input-datepicker from_date" value="<?php echo $first_day; ?>" >
                    </div>
                    <div class="col-md-2">
                        <label for="to_date">To Date</label>
                        <input type="text" name="to_date" id="datepicker2" class="form-control input-datepicker to_date" value="<?php echo $last_day; ?>" >
                    </div>
                    <div class="col-md-3">
                        <label for="purchase_project_id">Project</label>
                        <select name="purchase_project_id" id="purchase_project_id" class="form-control input-sm"></select>
                    </div>
                    <div class="col-md-3">
                        <label for="item_group">Item Group</label>
                        <select name="item_group" id="item_group" class="form-control input-sm"></select>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="form-control btn btn-primary btn-xs search_result">Search</button>
                    </div>
                    <table id="purchase_item_stock_datatable" class="table display custom-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Item Code</th>
                                <th>Item Make</th>
                                <th width="100px" class="text-right">Opening(Qty)</th>
                                <th width="100px" class="text-right">Receive(Qty)</th>
                                <th width="100px" class="text-right">Issue(Qty)</th>
                                <th width="100px" class="text-right">Closing(Qty)</th>
                                <th width="100px" class="text-right">Rate</th>
                                <th width="100px" class="text-right">Closing(Rs.)</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        initAjaxSelect2($("#purchase_project_id"),"<?=base_url('app/purchase_project_select2_source')?>");
        initAjaxSelect2($("#item_group"),"<?=base_url('app/item_group_select2_source')?>");
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [0,1,2,3,4,5,6,7,8],
			}
		};
        
        var title = 'Purchase Item Stock Report';
        
        table = $('#purchase_item_stock_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "aaSorting": [[0,'asc'] ],
            "ajax": {
                "url": "<?php echo site_url('report/purchase_item_stock_sheet_datatable') ?>",
                "type": "POST",
                "data":function(d){
                    $("#ajax-loader").show();
                    d.from_date = $("#datepicker1").val();
                    d.to_date = $("#datepicker2").val();
                    d.purchase_project_id = $("#purchase_project_id").val();
                    d.item_group = $("#item_group").val();
                },
                "complete": function () { $("#ajax-loader").hide(); },
            },
            <?php if ($this->applib->have_access_role(PURCHASE_ITEM_STOCK_REPORT_MODULE_ID, "export_data")) { ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: title, orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columnDefs": [{
                "className": "dt-right",
                "targets": [3, 4, 5, 6, 7, 8],
                "orderable": false
            }]
        });
        
        $(document).on('click',".search_result",function() {
            table.draw();
        });
        
        $(document).on('click', '.view_item_diff_report', function(){
            
            if ($.trim($("#datepicker1").val()) == '') {
                show_notify('Please Select From Date.', false);
                return false;
            }
            if ($.trim($("#datepicker2").val()) == '') {
                show_notify('Please Select To Date.', false);
                return false;
            }
            
            table.draw(); 
        });
    });
</script>