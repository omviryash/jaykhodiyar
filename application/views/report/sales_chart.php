<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
            <h1> Sales Chart </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <section class="col-md-12">			
                <!-- Map box -->
                <div class="box box-solid ">
                    <div class="box-body">
                        <!--<div id="world-map" style="height: 350px; width: 100%;"></div>-->
                        <div class="tab-pane active">
                            <?php 
                                $first_day = date("d-m-Y", strtotime("-11 month first day of this month"));
                                $last_day = date("d-m-Y", strtotime("last day of this month"));
                            ?>
                            <div class="col-md-2">
                                <label>From Date<span style="color: red;">&nbsp;*</span></label>
                                <input type="text" name="from_date" id="datepicker1" class="form-control input-datepicker from_date" value="<?php echo $first_day; ?>" >
                            </div>
                            <div class="col-md-2">
                                <label>To Date<span style="color: red;">&nbsp;*</span></label>
                                <input type="text" name="to_date" id="datepicker2" class="form-control input-datepicker to_date" value="<?php echo $last_day; ?>" >
                            </div>
                            <div class="col-md-2 hide">
                                <label>Month</label>
                                <input type="text" name="" id="datepicker_month" class="form-control input-datepicker" value="" autocomplete="off">
                            </div>
                            <div class="col-md-2">
                                <label>Order Status</label>
                                <select name="order_status" id="order_status" class="form-control select2">
                                    <option value="all" >All</option>
                                    <option value="<?php echo PENDING_DISPATCH_ORDER_ID; ?>" >Pending Dispatch</option>
                                    <option value="<?php echo DISPATCHED_ORDER_ID; ?>" >Dispatched</option>
                                    <option value="<?php echo CANCELED_ORDER_ID; ?>" >Canceled Order</option>
                                    <option value="<?php echo CHALLAN_CREATED_FROM_PROFORMA_INVOICE_ID; ?>" >Challan Created from Proforma Invoice</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Quotation Status</label>
                                <select name="quotation_status_id" class="form-control input-sm" id="quotation_status_id" ></select>
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <button type="button" class="form-control btn btn-primary btn-sm view_chart">View Chart</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <!--<div id="world-map" style="height: 350px; width: 100%;"></div>-->
                        <div class="salesChartDiv tab-pane active">
                            <!-- Sales Chart Canvas -->
                            <canvas id="salesChart" style="position: relative; height: 350px;"></canvas>
                        </div>
                    </div>
                    <!-- /.box-body-->
                    <div class="box-footer no-border">
                        <div class="row">
                            <!-- ./col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.box -->
            </section>
        </div>
    <!-- /.row -->
       
    </section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript" src="<?=base_url('resource/plugins/chartjs/Chart.min.js');?>"></script>
<script type="text/javascript">
	
    $(document).ready(function (){
        $('.select2').select2();
        initAjaxSelect2($("#quotation_status_id"),"<?=base_url('app/quotation_status_select2_source')?>");
        $('.select2-selection__placeholder').html('All');
        
        $('#datepicker_month').datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months",
            showButtonPanel: true,
            autoclose: true,
        });

        $('#datepicker_month').datepicker().on('hide', function (e) {
            var old_date = $('#datepicker_month').val();
            set_to_date(old_date);
        });
        
        $(document).on('change', '#quotation_status_id', function () {
            var quotation_status_id = $("#quotation_status_id").val();
            if(quotation_status_id == null){
                $('.select2-selection__placeholder').html('All');
            }
        });
        
		$(document).on('click','.view_chart',function(){
            if ($.trim($(".from_date").val()) == '') {
                show_notify('Please Enter From Date', false);
                $(".from_date").focus();
                return false;
            }
			if ($.trim($(".to_date").val()) == '') {
                show_notify('Please Enter To Date', false);
                $(".to_date").focus();
                return false;
            }
            var from_date = $("#datepicker1").val();
            var to_date = $("#datepicker2").val();
            var order_status = $("#order_status").val();
			var quotation_status_id = $("#quotation_status_id").val();
            $.ajax({
                url: "<?= base_url('report/get_chart_data_from_date/') ?>",
                method: "POST",
                data: {from_date : from_date, to_date : to_date, order_status : order_status, quotation_status_id : quotation_status_id},
                success: function (response) {
                    var json = $.parseJSON(response);
                    var chart_data = json['chart_data'];
//                    console.log(chart_data);
                    
                    var y_data = [];
                    var item1_data = [];
                    var item2_data = [];
                    $.each(chart_data, function (index, value) {
                        y_data.push(value.y)
                        item1_data.push(value.item1)
                        item2_data.push(value.item2)
                    });
                    console.log(y_data);
                    console.log(item1_data);
                    console.log(item2_data);
                    var salesChartData = {
                        labels: y_data,
                        datasets: [
                            {
                                label: "Sales Order",
                                fillColor: "rgb(210, 214, 222)",
                                strokeColor: "rgb(210, 214, 222)",
                                pointColor: "rgb(210, 214, 222)",
                                pointStrokeColor: "#c1c7d1",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgb(220,220,220)",
                                data: item1_data
                            },
                            {
                                label: "Quotation",
                                fillColor: "rgba(60,141,188,0.3)",
                                strokeColor: "rgba(60,141,188,0.8)",
                                pointColor: "#3b8bba",
                                pointStrokeColor: "rgba(60,141,188,1)",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(60,141,188,1)",
                                data: item2_data
                            }
                        ]
                    };
                    
                    load_chart(salesChartData);
                },

            });
            return false;
		});
        $('.view_chart').click();
    });    
    
    function set_to_date(data) {
        var date1 = data;
        var month_year = date1.split('-');
        var year = month_year[0];
        var month = month_year[1];
        var date = new Date();
        date.setFullYear(month, year, 0);
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        $('#datepicker1').datepicker('setDate', firstDay);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        $('#datepicker2').datepicker('setDate', lastDay);
    }

    //-------------
    //- CHART -
    //-------------
    function load_chart(salesChartData){
        $('#salesChart').remove(); // this is my <canvas> element
        $('.salesChartDiv').append('<canvas id="salesChart" style="position: relative; height: 350px;"></canvas>');
        var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var salesChart = new Chart(salesChartCanvas);

        var salesChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        salesChart.Line(salesChartData, salesChartOptions);
        
    }
</script>
