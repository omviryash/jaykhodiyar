<?php if ($this->session->flashdata('success') == true) { ?>
<div class="col-sm-4 pull-right">
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
    </div>
</div>
<?php } ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold"></small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Report : Counts
                    </h3>
                </div>
                <div class="box-body">

                    <div class="row">
                        <br/>
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Module</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Party</td>
                                        <td><?=isset($pending_inquiry)?$pending_inquiry:0?></td>
                                    </tr>
									<tr>
                                        <td></td>
                                        <td>No. Of Enquiry</td>
                                        <td><?=isset($pending_lead)?$pending_lead:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Hot Quotation</td>
                                        <td><?=isset($pending_quotation_folloup)?$pending_quotation_folloup:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Cold Quotation</td>
                                        <td><?=isset($pending_quotation_folloup)?$pending_quotation_folloup:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Lost Quotation</td>
                                        <td><?=isset($pending_quotation_folloup)?$pending_quotation_folloup:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Received Quotation</td>
                                        <td><?=isset($pending_quotation_folloup)?$pending_quotation_folloup:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Regret Quotation</td>
                                        <td><?=isset($pending_quotation_folloup)?$pending_quotation_folloup:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Orders</td>
                                        <td><?=isset($due_dispatch)?$due_dispatch:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>No. Of Order Items</td>
                                        <td><?=isset($due_dispatch)?$due_dispatch:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Total Proforma Invoice Export</td>
                                        <td><?=isset($upcoming_dispatch)?$upcoming_dispatch:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Total Proforma Invoice Domastic Export</td>
                                        <td><?=isset($sales_approval)?$sales_approval:0?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Total Items In Production<?=isset($inquiry_followup)?$inquiry_followup:0?></td>
                                        <td>Pending enquiry for followup</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><?=isset($pending_invoice)?$pending_invoice:0?></td>
                                        <td>Orders for which Invoice Not created yet</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    var table;
    $(document).ready(function () {
        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=quotations',
                    success: function (data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

    });

    $(document).on('click', '.show_itemprint_modal', function(e){
        e.preventDefault();
        var quotation_id = $(this).attr('id');
        $("#open_show_itemprint_modal").modal();
        $( "#items_td" ).html('');
        $.ajax({
            url: '<?php echo base_url('quotation/feed_quotation_items/') ?>/'+quotation_id,
            type: "POST",
            data: '',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data.item_data) {
                    jQuery.each( data.item_data, function( i, val ) {
                        console.log(val);
                        $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('quotation/quotation_print/'); ?>/'+quotation_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                    });
                }
            }
        });
    });


</script>
