<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Item Diff Report : Party wise</small>
            <small class="text-danger" style="font-size: 12px; font-weight: bold;">Cost : Count as per Purchase item Current Rate</small>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>From Date</label>
                            <input type="text" name="from_date" id="" class="form-control" value="<?php echo $from_date; ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label>To Date</label>
                            <input type="text" name="to_date" id="" class="form-control" value="<?php echo $to_date; ?>" readonly>
                        </div>
                        <div class="col-md-4">
                            <label>Item Name</label>
                            <input type="text" class="form-control" value="<?php echo $item_name; ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label>Item Code</label>
                            <input type="text" class="form-control" value="<?php echo $item_code; ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label>Number of Sales Item</label>
                            <input type="text" class="form-control" value="<?php echo $item_count; ?>" readonly>
                        </div>
                        <div class="clearfix"></div><br />
                        <div class="col-md-12">
                            <div class="table-responsive" style="overflow-x:hidden">
                                <table id="production_item_diff_party_wise_datatable" class="table custom-table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Party</th>
                                            <th>SO/PI No.</th>
                                            <th>Production finish date</th>
                                            <th class="text-center">Cost</th>
                                            <th class="text-center">Sell Amount</th>
                                            <th class="text-center">Margin</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
<!--                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th>0</th>
                                            <th>0</th>
                                            <th>0</th>
                                        </tr>
                                    </tfoot>-->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table;
        table = $('#production_item_diff_party_wise_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "aaSorting": [[0,'desc'] ],
            "ajax": {
                "url": "<?php echo site_url('report/production_item_diff_party_wise_datatable') ?>",
                "type": "POST",
                "data": function (d) {
                    d.item_id = <?php echo $item_id; ?>,
                    d.from_date = '<?php echo $from_date; ?>',
                    d.to_date = '<?php echo $to_date; ?>'
                }
            },
            "columnDefs": [
                {"className": "text-right", "targets": 3},
                {"className": "text-right", "targets": 4},
                {"className": "text-right", "targets": 5}
            ],
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%"
        });
    });
</script>