<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Purchase Item Stock Report</small>
        </h1>
    </section>
    <div class="clearfix">
		<div class="col-md-12">
            <div class="box box-info">
				<div class="box-body">
                    <div class="form-group">
                        <label for="item_group" class="col-md-1">Project</label>
                        <div class="col-md-3" style="margin-right: 1px; padding-right: 0px;">
                            <select name="purchase_project_id" id="purchase_project_id" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="item_group" class="col-md-1">Item Group</label>
                        <div class="col-md-3" style="margin-right: 1px; padding-right: 0px;">
                            <select name="item_group" id="item_group" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <table id="purchase_item_stock_datatable" class="table display custom-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Item Code</th>
                                <th>Item Make</th>
                                <th width="100px" class="text-right">Current Stock</th>
                                <th width="100px" class="text-right">In Production</th>
                                <th width="100px" class="text-right">Effective Stock</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        initAjaxSelect2($("#purchase_project_id"),"<?=base_url('app/purchase_project_select2_source')?>");
        initAjaxSelect2($("#item_group"),"<?=base_url('app/item_group_select2_source')?>");
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [0,1,2,3,4,5],
			}
		};
        
        var title = 'Purchase Item Stock Report';
        
        table = $('#purchase_item_stock_datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "aaSorting": [[0,'asc'] ],
            "ajax": {
                "url": "<?php echo site_url('report/purchase_item_stock_datatable') ?>",
                "type": "POST",
                "data":function(d){
                    d.purchase_project_id = $("#purchase_project_id").val();
                    d.item_group = $("#item_group").val();
                }
            },
            <?php if ($this->applib->have_access_role(PURCHASE_ITEM_STOCK_REPORT_MODULE_ID, "export_data")) { ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: title, orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
            "scrollY": 500,
            "scroller": {
                "loadingIndicator": true
            },
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "columnDefs": [{
                "className": "dt-right",
                "targets": [3, 4, 5],
                "orderable": false
            }]
        });
        
        $(document).on('change',"#purchase_project_id, #item_group",function() {
            table.draw();
        });
        
        $(document).on('click', '.view_item_diff_report', function(){
            
            if ($.trim($("#datepicker1").val()) == '') {
                show_notify('Please Select From Date.', false);
                return false;
            }
            if ($.trim($("#datepicker2").val()) == '') {
                show_notify('Please Select To Date.', false);
                return false;
            }
            
            table.draw(); 
        });
    });
</script>