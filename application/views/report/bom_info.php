<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
        <h1>
            <small class="text-primary text-bold">BOM Info</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
			<br/>
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label for="exampleInputEmail2">Module</label>
								<select name="filter_module" id="filter_module" class="form-control input-sm">
									<?php
									if(!empty($module_data)):
										foreach($module_data as $key=>$module_data_row):
											?>
											<option value="<?=$key?>"><?=$module_data_row;?></option>
											<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label for="exampleInputName2">From Date</label>
								<input type="text" class="form-control input-sm input-datepicker" id="filter_from_date" name="filter_from_date" value="<?=date('d-m-Y',strtotime('-1 month'));?>">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label for="exampleInputEmail2">To Date</label>
								<input type="text" class="form-control input-sm input-datepicker" id="filter_to_date" name="filter_to_date" value="<?=date('d-m-Y');?>">
							</div>
						</div>
						<div class="col-md-12">
							<table id="bom_info_datatable" class="table custom-table" width="100%">
								<thead>
								<tr>
									<th>Item</th>
									<th>Quantity</th>
									<th>1 Qty Amount</th>
									<th>Amount</th>
									<th>Sell Amount</th>
									<th>Profit</th>
								</tr>
								</thead>
							</table>
						</div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<script>
	var table;
	$(document).ready(function(){

		$("#filter_module").select2();
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.toString().replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [0,1,2,3,4,5],
			}
		};
        
        var title = 'BOM Info';

		table = $('#bom_info_datatable').DataTable({
			"serverSide": true,
			"ordering": false,
			"searching": true,
			"ajax": {
				"url": "<?php echo site_url('report/bom_info_datatable')?>",
				"type": "POST",
				"data": function (d) {
					d.filter_module = $("#filter_module").val();
					d.filter_from_date = $("#filter_from_date").val();
					d.filter_to_date = $("#filter_to_date").val();
				}

			},
            <?php if ($this->applib->have_access_role(REPORT_BOM_INFO_REPORT_MENU_ID, "export_data")) { ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: title, orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
			"scrollY": 500,
			"scrollX": '100%',
			"scroller": {
				"loadingIndicator": true
			},
            "columnDefs": [
                {"className": "text-right", "targets": 1},
                {"className": "text-right", "targets": 2},
                {"className": "text-right", "targets": 3},
                {"className": "text-right", "targets": 4},
                {"className": "text-right", "targets": 5}
            ],
		});

		$(document).on("change","#filter_module",function(){
			table.draw();
		});

		$('#filter_to_date,#filter_from_date').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy'
		}).on('changeDate', function(ev) {
			table.draw();
		});
	});
</script>