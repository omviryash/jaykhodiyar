<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Orders for which Invoice Not created yet</small>
            <a href="<?=base_url('sales_order/add')?>" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Order</a>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="col-md-12">
                        <label class="col-md-1">Status:</label>
                        <div class="col-md-2">
                            <select name="order_status" id="order_status" class="form-control input-sm">
                                <option value="all">All</option>
                                <option value="dispatched">Dispatched</option>
                                <option value="not_dispatched">Pending Dispatch</option>
                            </select>
                        </div>
                        <label class="col-md-1">Is Approved</label>
                        <div class="col-md-2">
                            <select name="is_approved" id="is_approved" class="form-control input-sm">
                                <option value="all">All</option>
                                <option value="1" <?=isset($_GET['is_approved']) && $_GET['is_approved'] == 1?'selected="selected"':''?>>Yes</option>
                                <option value="0" <?=isset($_GET['is_approved']) && $_GET['is_approved'] == 0?'selected="selected"':''?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive" style="overflow-x:hidden">
                            <table id="order_list_table" class="table custom-table table-striped">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Quotation No.</th>
                                    <th>Sales Order No.</th>
                                    <th>Status</th>
                                    <th>Is Approved</th>
                                    <th>Customer Name </th>
                                    <th>Sales Order Date</th>
                                    <th>Committed Date</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    
</div>
<div class="modal fade" id="open_show_itemprint_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    Item Print
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="example1" class="table custom-table table-striped dataTable">
                        <thead>
                            <tr>
                                <th width="50%">
                                    Item
                                </th>
                                <th width="50%">
                                    Print
                                </th>
                                <!-- <th width="20%" class="text-center">
                                    Action
                                </th> -->
                            </tr>                        
                        </thead>
                        <tbody id="items_td">
                        </tbody>
                    </table>
                </div>
            </div>
       </div>
     </div>
</div>

<script>
	var table;
    $(document).ready(function() {
		
		table = $('#order_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('report/not_invoice_created_sales_order_report_datatable')?>",
                "type": "POST",
                "data":function(d){
                    d.order_status = $("#order_status").val();
                    d.is_approved = $("#is_approved").val();
                }
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
			}
		});

        $("#order_status").select2();

        $(document).on('change',"#order_status,#is_approved",function() {
            table.draw();
        });
		
        $(document).on("click", ".delete_button", function() {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=id&table_name=sales_order',
                    success: function(data) {
                    }
                });
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=sales_order_id&table_name=sales_order_item',
                    success: function(data) {
                        tr.remove();
                        show_notify('Deleted Successfully!', true);
                    }
                });
            }
        });

        $(document).on('click', '.show_itemprint_modal', function(e){
            e.preventDefault();
            var saleorder_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();

            $( "#items_td" ).html('');
            $.ajax({
                url: '<?php echo base_url('sales/feed_saleorders_items/') ?>/'+saleorder_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    if (data.item_data) {
                        jQuery.each( data.item_data, function( i, val ) {
                            $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('sales/sales_order_print/'); ?>/'+saleorder_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        });
                    }
                }
            });
        });

        $(document).on('click', '.show_work_order_itemprint_modal', function(e){
            e.preventDefault();
            var saleorder_id = $(this).attr('id');
            $("#open_show_itemprint_modal").modal();

            $( "#items_td" ).html('');
            $.ajax({
                url: '<?php echo base_url('sales/feed_saleorders_items/') ?>/'+saleorder_id,
                type: "POST",
                data: '',
                dataType: 'json',
                success: function (data) {
                    if (data.item_data) {
                        jQuery.each( data.item_data, function( i, val ) {
                            $( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('sales/work_order/'); ?>/'+saleorder_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
                        });
                    }
                }
            });
        });

    });
</script>
