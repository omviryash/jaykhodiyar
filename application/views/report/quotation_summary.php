<?php if ($this->session->flashdata('success') == true) { ?>
<div class="col-sm-4 pull-right">
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
	</div>
</div>
<?php } ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small class="text-primary text-bold">Report : Quotation</small>
		</h1>
		<!-- <ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol> -->
	</section>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-body">
					
					<div class="row">
						<div class="col-md-6" style="display: none;">
							<fieldset class="scheduler-border">
								<legend class="scheduler-border text-primary text-bold"> Report Criteria </legend>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Quotation No. Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Party Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Item Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Region Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Qtn. Type Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> User Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Branch Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Cat./ Group Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Qtn. Stage Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Reason Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Follow Up Hist. Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Follow Up User Wise
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="radio">
										<label>
											<input type="radio" name="shorting_by" value="1"> Sales Type Wise
										</label>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-md-6" style="display: none;">
							<fieldset class="scheduler-border">
								<legend class="scheduler-border text-primary text-bold"> Advance Criteria </legend>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Quotation Status</label>
									<div class="col-sm-6">
										<select name="quotation_status" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
									<div class="col-sm-3">
										<div class="checkbox no-margin">
											<label>
												<input type="checkbox" name="summary" checked="checked"> Summary
											</label>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Item Status</label>
									<div class="col-sm-6">
										<select name="item_status" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
									<div class="col-sm-3">
										<div class="checkbox no-margin">
											<label>
												<input type="checkbox" name="detail" checked="checked"> Detail
											</label>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Stage</label>
									<div class="col-sm-6">
										<select name="stage" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Date Wise</label>
									<label for="" class="col-sm-2 control-label input-sm">From Date</label>
									<div class="col-sm-4">
										<input type="text" class="form-control input-sm input-datepicker" da>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Review Date</label>
									<label for="" class="col-sm-2 control-label input-sm">To Date</label>
									<div class="col-sm-4">
										<input type="text" class="form-control input-sm input-datepicker">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Qtn. Amount</label>
									<div class="col-sm-4">
										<select name="qtn_type" class="form-control input-sm select2">

										</select>
									</div>
									<div class="col-sm-4">
										<input type="text" class="form-control input-sm">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">&nbsp;</label>
									<div class="col-sm-4">
										<select name="qtn_type" class="form-control input-sm select2">

										</select>
									</div>
									<div class="col-sm-4">
										<input type="text" class="form-control input-sm">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Qtn. Type</label>
									<div class="col-sm-6">
										<select name="qtn_type" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">User</label>
									<div class="col-sm-6">
										<select name="user" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Qtn. Stage</label>
									<div class="col-sm-6">
										<select name="qtn_stage" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Sales Type</label>
									<div class="col-sm-6">
										<select name="sales_type" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label input-sm">Branch</label>
									<div class="col-sm-4">
										<select name="branch_id_1" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
									<div class="col-sm-4">
										<select name="branch_id_2" class="form-control input-sm select2">
											<option value="all">All</option>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
							</fieldset>
						</div>

						<div class="col-md-7">
							<!-- <a href="javascript:void(0)" class="btn btn-info btn-sm filtration_div"> Advanced Search </a> -->
							<div class="filtration_fields" id="filtration_fields">
								<table cellpadding="3" cellspacing="0" border="0" style="width:100%;">
									<tbody>
										<tr id="filter_global">
											
											<td align="right">City&nbsp;&nbsp;</td>
											<td align="center" data-column="5">
											<select class="form-control input-sm select2 city column_filter" onChange="getstatedetails(this.value)" id="col5_filter"></select></td>
										
											<td align="right">State&nbsp;&nbsp;</td>
											<td align="center" data-column="6"> <select id="col6_filter" class="form-control input-sm select2 column_filter"></select></td>
										
											<td align="right">Country&nbsp;&nbsp;</td>
											<td align="center" data-column="7"><select id="col7_filter" class="form-control input-sm select2 column_filter"></select></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<br/>
						<div class="col-md-12">
							<table id="quotation_datatable" class="table custom-table agent-table">
								<thead>
									<tr>
										<th class="hidden">Updated at</th>
										<th>Sr. No.</th>
										<th>Action</th>
										<th>Date</th>
										<th>Quotation No.</th>
										<th>Revision</th>
										<th>Party</th>
										<th>Model (Item Code) (Quantity)</th>
										<th>City</th>
										<th>State</th>
										<th>Country</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>
	
</div>

<div class="modal fade" id="open_show_itemprint_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    Item Print
                </h4>
            </div>
            <div class="modal-body">
				<div class="table-responsive" style="overflow-x:hidden">
					<table id="example1" class="table custom-table table-striped dataTable">
						<thead>
							<tr>
								<th width="50%">
									Item
								</th>
								<th width="50%">
									Print
								</th>
								<!-- <th width="20%" class="text-center">
									Action
								</th> -->
							</tr>                        
						</thead>
						<tbody id="items_td">
							<?php /*foreach($leads as $row):?>
							<tr data-id="<?= $row->id;?>">
								<td>
									<a href="javascript:void(0);" class="lead_row" ><?= $row->lead_no;?></a>
								</td>
								<td>
									<input type="hidden" id="lead_<?= $row->id;?>" value="<?= $row->lead_title;?>" />
									<a href="javascript:void(0);" class="lead_row" ><?= $row->lead_title;?></a>
								</td>
								<!-- <td class="text-center">
									<a class="btn btn-primary btn-xs btn-select-lead" data-id="<?= $row->id;?>">Select</a>
								</td> --> 
							</tr>
							<?php endforeach; */?>                        
						</tbody>
					</table>
                </div>
            </div>
       </div>
     </div>
</div>
<script>

	var table;
	$(document).ready(function () {
		initAjaxSelect2($("#customer_name1"),"<?=base_url('app/party_select2_source')?>");
		initAjaxSelect2($("#col5_filter"),"<?=base_url('app/city_select2_source')?>");
		initAjaxSelect2($("#col6_filter"),"<?=base_url('app/state_select2_source')?>");
		initAjaxSelect2($("#col7_filter"),"<?=base_url('app/country_select2_source')?>");
		$('#filtration_fields').hide();
		//$(document).on('click', '.filtration_div', function(){
			$(".filtration_fields").toggle(500);
		//});
        
        var buttonCommon = {
			exportOptions: {
				format: { body: function ( data, row, column, node ) { return data.replace(/(&nbsp;|<([^>]+)>)/ig, ""); } },
                columns: [2,3,4,5,6,7,8,9,10],
			}
		};
        
        var title = 'Quotation';
        
		var table = $('#quotation_datatable').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[0,'desc'] ],
			"ajax": {
				"url": "<?php echo site_url('report/quotation_datatable')?>",
				"type": "POST"
			},
            <?php if ($this->applib->have_access_role(REPORT_QUOTATION_MANAGEMENT_MENU_ID, "export_data")) { ?>
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, { extend: 'copy', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'csv', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'excel', title: title, action: newExportAction } ),
                    $.extend( true, {}, buttonCommon, { extend: 'print', title: title, orientation: 'landscape', action: newExportAction } ),
                ],
            <?php } ?>
			"scrollY": 400,
			"scroller": {
				"loadingIndicator": true
			},
			"columns": [
				{ "class": "hidden" },
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null
			]
		});

		$('input.global_filter').on( 'keyup click', function () {
			filterGlobal();
		} );

		$('select.column_filter').on( 'change', function () {
			filterColumn( $(this).parent('td').attr('data-column') );
		} );

		$('.showhideColumn').on('click', function(){
			var tbl_column = table.column($(this).attr('data-columnindex'));
			tbl_column.visible(!tbl_column.visible());
		});

		$(document).on("click", ".delete_button", function () {
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if (value) {
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=quotations',
					success: function (data) {
						tr.remove();
						show_notify('Deleted Successfully!', true);
					}
				});
			}
		});

	});

	function getstatedetails(id)
    {
       //alert('this id value :'+id);
        $.ajax({
            type: "POST",
            url: '<?=base_url();?>party/ajax_get_state/'+id,
            data: id='cat_id',
            success: function(data){
                var json = $.parseJSON(data);
                //alert(data);
                if (json['state']) {
                    $("#col6_filter").html('');
                    $("#col6_filter").html(json['state']);
                    $("#s2id_state span:first").html($("#col6_filter option:selected").text());    
                }
                if (json['country']) {
                    $("#col7_filter").html('');
                    $("#col7_filter").html(json['country']);
                    $("#s2id_country span:first").html($("#col7_filter option:selected").text());    
                }
                
                
            },
         });
    }

	$(document).on('click', '.show_itemprint_modal', function(e){
		e.preventDefault();
		var quotation_id = $(this).attr('id');
		$("#open_show_itemprint_modal").modal();
		$( "#items_td" ).html('');
		$.ajax({
			url: '<?php echo base_url('quotation/feed_quotation_items/') ?>/'+quotation_id,
			type: "POST",
			data: '',
			dataType: 'json',
			success: function (data) {
				console.log(data);
				if (data.item_data) {
					jQuery.each( data.item_data, function( i, val ) {
						console.log(val);
						$( "#items_td" ).append('<tr><td>'+val.item_name+'</td><td><a href="<?php echo base_url('quotation/quotation_print/'); ?>/'+quotation_id+'?item='+val.id+'" target="_blank" class="print_button btn-primary btn-xs"><i class="fa fa-print"></i></a></td></tr>' );
					});
				}
			}
		});
	});


	function filterGlobal () {
		$('#quotation_datatable').DataTable().search(
			$('#global_filter').val()
		).draw();
	}

	function filterColumn ( i ) {
		$('#quotation_datatable').DataTable().column( i ).search(
			$('#col'+i+'_filter').val()
		).draw();
	}
	function filterCustomername () {
		
		$('#quotation_datatable').DataTable().column(3).search(
			$("#customer_name1 option:selected").text()
		).draw();
	}
	
</script>
