<?php if ($this->session->flashdata('success') == true) { ?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
<?php } ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Report :  Pending quotation for followup</small><br>
        </h1>
    </section>
    <br>
    <div class="box box-info">
        <div class="box-body">
            <br>
            <div class="row">
                <div class="col-md-4">
                    <input type="text" class="form-control input-sm pull-right" id="filter_datepicker" value="<?=date('d-m-Y');?>">
                </div>
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="table-responsive" style="overflow-x:hidden">
                        <table id="order_list_table" class="table custom-table table-striped">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Quotation No.</th>
                                <th>Enquiry No.</th>
                                <th>Customer Name </th>
                                <th>Quotation Date</th>
                                <th>Followup Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
    
</div>
<div class="modal fade" id="open_show_itemprint_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;height: auto;max-height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    Item Print
                </h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-x:hidden">
                    <table id="example1" class="table custom-table table-striped dataTable">
                        <thead>
                            <tr>
                                <th width="50%">
                                    Item
                                </th>
                                <th width="50%">
                                    Print
                                </th>
                                <!-- <th width="20%" class="text-center">
                                    Action
                                </th> -->
                            </tr>                        
                        </thead>
                        <tbody id="items_td">
                        </tbody>
                    </table>
                </div>
            </div>
       </div>
     </div>
</div>

<script>
	var table;
    $(document).ready(function() {

		table = $('#order_list_table').DataTable({
            "serverSide": true,
            "ordering": true,
            "searching": true,
            "aaSorting": [[1, 'asc']],
            "ajax": {
                "url": "<?php echo site_url('report/pending_quotation_for_followup_datatables')?>",
                "type": "POST",
                "data":function(d){
					d.review_date = $("#filter_datepicker").val();
				}
            },
            "scrollY": 300,
            "scroller": {
                "loadingIndicator": true
			}
		});

        $('#filter_datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        }).on('changeDate', function(ev){
            table.draw();
        });

    });
</script>
