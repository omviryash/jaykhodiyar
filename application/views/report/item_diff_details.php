<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Item Diff Details</small>
            <small class="text-danger" style="font-size: 12px; font-weight: bold;">Cost : Count as per Purchase item Current Rate</small>
        </h1>
    </section>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>From Date</label>
                            <input type="text" name="from_date" id="" class="form-control" value="<?php echo $from_date; ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label>To Date</label>
                            <input type="text" name="to_date" id="" class="form-control" value="<?php echo $to_date; ?>" readonly>
                        </div>
                        <div class="col-md-4">
                            <label>Item Name</label>
                            <input type="text" class="form-control" value="<?php echo $item_name; ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label>Item Code</label>
                            <input type="text" class="form-control" value="<?php echo $item_code; ?>" readonly>
                        </div>
                        <div class="clearfix"></div><br />
                        <div class="col-md-12">
                            <table style="" class="table custom-table">
                                <thead>
                                    <tr>
                                        <th style="width: 40%;">Purchase Item</th>
                                        <th style="width: 10%;" class="text-right">Qty</th>
                                        <th style="width: 10%;" class="text-right">Rate - For 1 UOM</th>
                                        <th style="width: 10%;" class="text-right">Req. Qty - UOM</th>
                                        <th style="width: 10%;" class="text-center">Relation</th>
                                        <th style="width: 10%;" class="text-right">Job Expense</th>
                                        <th style="width: 10%;" class="text-right">Cost</th>
                                    </tr>
                                </thead>
                                <tbody id="lineitem_list">
                                    <?php
                                        $purchase_amount = 0;
                                        $purchase_amount = $purchase_amount + ($expense_amount * 1);
                                        foreach ($purchase_items as $purchase_item){
                                            $qty = $purchase_item->quantity * 1;
                                            if($qty > 0){
                                                $amount = ($purchase_item->quantity * $purchase_item->rate * 1 * $purchase_item->reference_qty) + ($purchase_item->job_expense * 1);
                                                $purchase_amount = $purchase_amount + $amount;
                                            }
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $purchase_item->item_name; ?><br />
                                                <?php echo (!empty($purchase_item->project_name)) ? '<small>Project - ' . $purchase_item->project_name . '</small>' : ''; ?>
                                            </td>
                                            <td class="text-right"><?php echo $qty; ?></td>
                                            <td class="text-right"><?php echo $purchase_item->rate .'-'. $purchase_item->pi_uom; ?></td>
                                            <td class="text-right"><?php echo $purchase_item->required_qty .'-'. $purchase_item->pi_required_uom; ?></td>
                                            <td class="text-center"><?php echo $purchase_item->reference_qty . ' ' . $purchase_item->pi_uom . ' For 1 ' . $purchase_item->pi_required_uom; ?></td>
                                            <td class="text-right"><?php echo $purchase_item->job_expense; ?></td>
                                            <td class="text-right"><?php echo number_format($amount, '2', '.', ''); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Total Item Expenses (As per BOM)</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right"><?php echo number_format($expense_amount, '2', '.', ''); ?></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr><th colspan="7">&nbsp;</th></tr>
                                    <tr>
                                        <th>Total Cost</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-right"><?php echo number_format($purchase_amount, '2', '.', ''); ?></th>
                                    </tr>
                                    <tr>
                                        <th>Sell Amount</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-right"><?php echo number_format($sell_amount, '2', '.', ''); ?></th>
                                    </tr>
                                    <tr>
                                        <th>Margin</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-right"><?php echo number_format($difference_amount = $sell_amount - $purchase_amount, '2', '.', ''); ?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
    });
</script>