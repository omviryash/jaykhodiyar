<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<?php
		$this->load->view('shared/success_false_notify');
		?>
        <h1>
            <small class="text-primary text-bold">Report : Sales Summary</small>
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12">
        	<form class="form-inline" method="post" style="margin: 15px;">
				<div class="form-group">
					<label for="exampleInputName2">From Date</label>
					<input type="text" class="form-control" id="datepicker1" name="from_date" value="<?=date('d-m-Y', strtotime($from_date));?>">
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2">To Date</label>
					<input type="text" class="form-control" id="datepicker2" name="to_date" value="<?=date('d-m-Y', strtotime($to_date));?>">
				</div>
				<button type="submit" class="btn btn-default">Filter</button>
			</form>
			<div class="clearfix"></div>
        	
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
							<div style="margin: 10px;">
								<div class="col-md-12">
									<table id="supplier_list_datatable" class="table custom-table stock-table" width="100%">
										<thead>
											<tr>
												<th>Quotations</th>
												<th>Quotations Qty</th>
												<th>Quotations Amount</th>
												<th>Orders</th>
												<th>Orders Qty</th>
												<th>Orders Amount</th>
												<th>Dispatch</th>
												<th>Dispatch Qty</th>
												<th>Dispatch Amount</th>
											</tr>
										</thead>
										<tbody>
									<?php 
										$i = 1;
										if ($sales_data) {
											foreach ($sales_data as $data) { 
									?>
											<tr>
												<td><?=$data['quotation_id']?></td>
												<td><?=$data['quotation_items'];?></td>
												<td><?=moneyformat($data['quotation_items_total']);?></td>
												<td><?=$data['sale_order_id'];?></td>
												<td><?=$data['sale_order_items'];?></td>
												<td><?=moneyformat($data['sale_order_items_total']);?></td>
												<td><?=$data['challan_id'];?></td>
												<td><?=$data['challan_items'];?></td>
												<td><?=moneyformat($data['challan_items_total']);?></td>
											</tr>
									<?php	}
										} ?>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>