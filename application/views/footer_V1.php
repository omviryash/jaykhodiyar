        <div class="control-sidebar-bg"></div>
    </div>

        <?php
            if($this->uri->segment(1) != 'mail-system2') {
                ?>
                <form id="frm_mailbox" action="" method="post">
                    <input type="hidden" name="action" class="action" value="">
                    <input type="hidden" name="mail_id" class="mail_id" value="">
                </form>
                <?php
            }
        ?>
    <!-- ./wrapper -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>

    <script src="<?=dist_url('js/auto_complete.js');?>"></script>
    <!-- Morris.js charts -->
    <script src="<?=bootstrap_url('js/raphael-min.js');?>"></script>

        <?php if($this->uri->segment(1) == ''){?>
            <script src="<?=plugins_url('morris/morris.min.js');?>"></script>
            <script src="<?=dist_url('js/pages/dashboard.js');?>"></script>
        <?php }?>
    <!-- Sparkline -->
    <script src="<?=plugins_url('sparkline/jquery.sparkline.min.js');?>"></script>
    <!-- jvectormap -->
    <script src="<?=plugins_url('jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script src="<?=plugins_url('jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=plugins_url('knob/jquery.knob.js');?>"></script>
    <!-- daterangepicker -->
    <script src="<?=bootstrap_url('js/moment.min.js');?>"></script>
    <script src="<?=plugins_url('daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?=plugins_url('datepicker/bootstrap-datepicker.js');?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=plugins_url('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?=plugins_url('slimScroll/jquery.slimscroll.min.js');?>"></script>

    <!--Page Specific Js-->


        <?php //if($this->uri->segment(2) == 'quotation' || $this->uri->segment(1) == 'party'){?>
            <script src="<?=plugins_url('datatables/jquery.dataTables.min.js');?>"></script>
            <script src="<?=plugins_url('datatables/dataTables.bootstrap.min.js');?>"></script>
        <?php //} ?>

    <script src="<?=plugins_url('iCheck/icheck.min.js');?>"></script>

    <!--Page Specific Js-->

    <!-- FastClick -->
    <script src="<?=plugins_url('fastclick/fastclick.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=dist_url('js/app.min.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=dist_url('js/demo.js');?>"></script>
    <script src="<?php echo base_url('node_modules/socket.io/node_modules/socket.io-client/socket.io.js');?>"></script>
    <script>
    var port_number = '<?php echo PORT_NUMBER; ?>';
	var isActive;

	//window.onfocus = function () { 
	  isActive = true; 
	//}; 

	window.onblur = function () { 
	  isActive = false; 
	};
		var userdata_limitoffset = [];
		
		var socket = io.connect( 'http://'+window.location.hostname+':'+port_number );
		var beat = 0;
		var reciver_id = 0;
		var conn_usr_array = {};
		function receivesHeartbeat(){
			setTimeout(receivesHeartbeat, 90000);
			//console.log(conn_usr_array);
			$.each(conn_usr_array, function (index, value)
			{
				if(value == 1)
				{
					conn_usr_array[index] = 0;
					//console.log("Connected"+index);
				}
				else if(value == 0)
				{
					chkpopupval = chckuserpopup(index);								
					if(chkpopupval)
					{
						close_popup(index);
						send_email_chat_visitor(index);
					}
				}
			});
		}
		socket.on('ping1', function(data){
				socket.emit('pong1', {beat: <?=$this->session->userdata['is_logged_in']['staff_id'];?>});
				beat=data.beat;
				sender_id=data.sender_id;
				conn_usr_array[sender_id] = beat;
				beat=1;
		});
		setTimeout(receivesHeartbeat, 90000);
		socket.on( 'cron_mail_notification', function( data ) {
			if(data.staff_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>" && data.count_mail > 0)
			{
				$('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">'+data.count_mail+'</span>');
			}
		});
		socket.on( 'new_message_client', function( data ) {
			
			if(data.to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
			{
				if(isActive==false)
				{
					notifyBrowser(data.from_name,data.msg,'<?php echo base_url(); ?>');
				}
				chkpopupval = chckuserpopup(data.from_id);								
				if(chkpopupval)
				{									
					$("#"+data.from_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+data.from_name+'</span> <span class="direct-chat-timestamp pull-right">'+data.date_time+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+data.from_image+'" alt="message user image"> <div class="direct-chat-text"> '+data.msg+' </div></div>');
					$('.direct-chat-messages').scroll();
					$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
					$('#txt_'+data.from_id).trigger('focus');
				}
				else
				{
					register_popup(data.from_id, data.from_name);					
					$("#"+data.from_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+data.from_name+'</span> <span class="direct-chat-timestamp pull-right">'+data.date_time+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+data.from_image+'" alt="message user image"> <div class="direct-chat-text"> '+data.msg+' </div></div>');
					$('.direct-chat-messages').scroll();
					$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
					$('#txt_'+data.from_id).trigger('focus');
					
				}
			}
			if(data.from_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
			{
				$("#"+data.to_id+" .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'+data.from_name+'</span><span class="direct-chat-timestamp pull-left">'+data.date_time+'</span></div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+data.from_image+'" alt="message user image"><div class="direct-chat-text">'+data.msg+'</div></div>');
				$('#txt_'+data.from_id).trigger('focus');
				
			}
		});

		socket.on( 'new_message_visitor_to_client', function( data ) {
				formattedDate =  getDateString(new Date(data.date_time), "d M y hh:m:s b");
				if(isActive==false)
				{
					notifyBrowser(data.from_name,data.msg,'<?php echo base_url(); ?>messages/index/'+data.from_session_id);
				}
				chkpopupval = chckuserpopup(data.from_session_id);								
				if(chkpopupval)
				{									
					$("#"+data.from_session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+data.from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+data.from_image+'" alt="message user image"> <div class="direct-chat-text"> '+data.msg+' </div></div>');
					$('.direct-chat-messages').scroll();
					$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
					$('#txt_visitor_'+data.from_session_id).trigger('focus');
				}
				else
				{					
					visitor_register_popup(data.from_session_id, data.from_name,0,0);
					$("#"+data.from_session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+data.from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+data.from_image+'" alt="message user image"> <div class="direct-chat-text"> '+data.msg+' </div></div>');
					$('.direct-chat-messages').scroll();
					$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
					$('#txt_visitor_'+data.from_session_id).trigger('focus');				
				}
			
		});
		
		socket.on( 'add_visitor_data', function( data ) {
			//console.log(data);
			$("#"+data.session_id+" .box-title").html(data.name + ' (Visitor)');
			 $("#"+data.session_id+" .user-details").html('<div>User<span class="pull-right text-red">'+data.name+'</span></div><div>Email<span class="pull-right text-red">'+data.email+'</span></div><div>Phone<span class="pull-right text-yellow">'+data.phone+'</span></div><div>IP<span class="pull-right text-yellow">'+data.ip+'</span></div><div>From<span class="pull-right text-yellow">'+'<?php echo base_url();?>'+'</span></div><div>City<span class="pull-right text-yellow">'+data.city+'</span></div><div> Country<span class="pull-right text-yellow">'+data.country+'</span></div><div>Currency<span class="pull-right text-yellow">'+data.currency+'</span></div><div>Date <span class="pull-right text-green">'+data.date+'</span></div>');
			 
			 //for message details page infor update
			 $("."+data.session_id+" .contacts-list-name").html(data.name + ' (Visitor)');
			 $("."+data.session_id+" .contacts-list-msg").html(data.email);

			 vis_id_msg = $('.user_details').attr('visitor-id');
			 if(vis_id_msg == data.session_id)
			 {
				 get_visitor_info(data.session_id);
			 }
		});

		socket.on( 'new_message_client_to_visitor', function( data ) {
			formattedDate =  getDateString(new Date(data.date_time), "d M y hh:m:s b");
			/*if(isActive==false)
			{
				notifyBrowser(data.from_name,data.msg,'<?php echo base_url(); ?>');
			}*/			
			chkpopupval = chckuserpopup(data.to_session_id);								
			if(chkpopupval)
			{
				if(data.from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
				{
					$("#"+data.to_session_id).find('.direct-chat-messages').append(data.chat_html);
					$("#"+data.to_session_id).find('.direct-chat-messages').animate({ scrollTop: 2000000000 }, 1000);
				}
				else
				{
					$("#"+data.to_session_id).find('.direct-chat-messages').append(data.other_agent_chat_html);
					$("#"+data.to_session_id).find('.direct-chat-messages').animate({ scrollTop: 2000000000 }, 1000);
				}
			}
			else
			{
				visitor_register_popup(data.to_session_id, data.to_name,0,0);
				$("#"+data.to_session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+data.to_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+data.to_image+'" alt="message user image"> <div class="direct-chat-text"> '+data.msg+' </div></div>');
				$('.direct-chat-messages').scroll();
				$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
				
			}
		});

		socket.on( 'new_file_client_to_client', function( data ) {
			if(data.to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
			{
				chkpopupval = chckuserpopup(data.from_id);								
				if(chkpopupval)
				{									
					get_messages(data.from_id,1);
				}
				else
				{
					register_popup(data.from_id, data.from_name,0,0);
				}
			}
			if(data.from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
			{
				get_messages(data.to_id,1);
			}
		});

		socket.on( 'new_file_visitor_to_client', function( data ) {
			if(data.to_table == "staff,admin")
			{
				formattedDate =  getDateString(new Date(data.date_time), "d M y hh:m:s b");
				chkpopupval = chckuserpopup(data.from_session_id);								
				if(chkpopupval)
				{									
					get_visitor_messages(data.from_session_id,1,data.from_name);
				}
				else
				{					
					visitor_register_popup(data.from_session_id, data.from_name,0,0);		
				}
			}
		});

		socket.on( 'new_file_client_to_visitor', function( data ) {
			formattedDate =  getDateString(new Date(data.date_time), "d M y hh:m:s b");
			chkpopupval = chckuserpopup(data.to_session_id);								
			if(chkpopupval)
			{
				if(data.from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
				{
					$("#"+data.to_session_id).find('.direct-chat-messages').append(data.chat_html);
					$("#"+data.to_session_id).find('.direct-chat-messages').animate({ scrollTop: 2000000000 }, 1000);
					$('#txt_visitor_'+data.to_session_id).trigger('focus');
				}
				else
				{
					$("#"+data.to_session_id).find('.direct-chat-messages').append(data.other_agent_chat_html);
					$("#"+data.to_session_id).find('.direct-chat-messages').animate({ scrollTop: 2000000000 }, 1000);
					$('#txt_visitor_'+data.to_session_id).trigger('focus');
				}
			}
			else
			{
				visitor_register_popup(data.to_session_id, data.to_name,0,0);
				$("#"+data.to_session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+data.to_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+data.to_image+'" alt="message user image"> <div class="direct-chat-text"> '+data.msg+' </div></div>');
				$('.direct-chat-messages').scroll();
				$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
				$('#txt_visitor_'+data.to_session_id).trigger('focus');
				
			}
		});
   
        $(document).ready(function(){
            $(document).on('click','.close',function(){
                console.log('Closed!');
                $(this).closest('div.alert-div').hide();
            });
            $(".input-datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
            $('input[type="radio"].address_work').iCheck({
                radioClass: 'iradio_minimal-green'
            });
            $('input[type="checkbox"].enquiry_received_by').iCheck({
                checkboxClass: 'icheckbox_flat-green',
            });
            $('#datepicker1,#datepicker2,#datepicker3').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true
            });
            $(document).on('click','.btn-header-read-mail',function(){
                $("#frm_mailbox").find('input[name="mail_id"]').val($(this).data('mail_id'));
                $("#frm_mailbox").attr('action','<?=base_url()?>mail-system2/read');
                $("#frm_mailbox").submit();
            });
			getInboxUnreadMails();
        });
    </script>
        <?php
        if($this->uri->segment(1) == 'hr' && ($this->uri->segment(2) == "offer-letter" || $this->uri->segment(2) == "appointment-letter" || $this->uri->segment(2) == "confirmation-letter" || $this->uri->segment(2) == "experience-letter" || $this->uri->segment(2) == "noc-letter")) {
            ?>
            <script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
            <script>
                $(document).ready(function(){
                    CKEDITOR.replace('editor1');
                    $(document).on('change','.select_latter',function(){
                        window.location.href = '<?=base_url()?>hr/'+$(this).val();
                    });
                    //select_latter
                });
            </script>
            <?php
        }
        ?>

        <?php
            if($this->uri->segment(1) == 'hr' && $this->uri->segment(2) == "yearly-leaves") {
                ?>
                <script src="<?= plugins_url("multi-dates-picker/js/jquery-ui-1.11.1.js"); ?>"></script>
                <script src="<?= plugins_url("multi-dates-picker/jquery-ui.multidatespicker.js"); ?>"></script>
                <script src="<?= plugins_url("multi-dates-picker/js/prettify.js"); ?>"></script>
                <script src="<?= plugins_url("multi-dates-picker/js/lang-css.js"); ?>"></script>
                <script>
                    var today = new Date();
                    var y = today.getFullYear();
                    var Holidays = <?=json_encode($YearlyHoliday)?>;
                    console.log(Holidays);
                    $('#full-year').multiDatesPicker({
                        addDates: Holidays,
                        numberOfMonths: [3, 4],
                        defaultDate: '1/1/' + y
                    });
                </script>
                <!--      Multi Dates Pickers   -->
                <?php
            }
        ?>

        <?php
            if($this->uri->segment(1) == 'hr' && $this->uri->segment(2) == "expected-interview") {
        ?>
            <script>
                $(document).ready(function(){
                    $('#datepicker1,#datepicker2,#datepicker3').datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true
                    });
                });
            </script>
        <?php
            }
        ?>

    <!----------------- NOTIFICATION ----------------->
    <script src="<?=plugins_url('notify/jquery.growl.js');?>"></script>
    <script>
        function show_notify(notify_msg,notify_type)
        {
            if(notify_type == true){
                $.growl.notice({ title:"Success!",message:notify_msg});
            }else{
                $.growl.error({ title:"False!",message:notify_msg});
            }
        }
    </script>
    <!----------------- NOTIFICATION ----------------->
    
    <!----------------- Chat ----------------->
    <script>
        //this function can remove a array element.
        Array.remove = function(array, from, to) {
            var rest = array.slice((to || from) + 1 || array.length);
            array.length = from < 0 ? array.length + from : from;
            return array.push.apply(array, rest);
        };
        //this variable represents the total number of popups can be displayed according to the viewport width
        var total_popups = 0;
        //arrays of popups ids
        var popups = [];
        //this is used to close a popup
        $.removeFromArray = function(value, arr) {
			return $.grep(arr, function(elem, index) {
				return elem !== value;
			});
		};
        function close_popup(id,visitorpopup=0){	
			if(visitorpopup==1)
			{
				var chathtml = $("#"+id+" .direct-chat-messages").html();	
				send_chat_mail(id,chathtml);
			}
			userdata_limitoffset.splice( $.inArray(id, userdata_limitoffset), 1 );
			//userdata_limitoffset = $.removeFromArray(8, userdata_limitoffset);
            for(var iii = 0; iii < popups.length; iii++){
                if(id == popups[iii]){
                    Array.remove(popups, iii);
                    document.getElementById(id).remove();
                    calculate_popups();
                    return;
                }
            }   
        }
        function send_email_chat_visitor(userid)
		{
			$.ajax({
				type:"POST",
				data: {'userid' : userid},
				url:"<?=base_url('frontend/sendEmailToadminForOldChatOfVisitor');?>",
				success:function(data)
				{
				}
			});
		}

		function send_chat_mail(id,html)
		{
			$.ajax({
				type:"POST",
				data: {'id':id},
				url:"<?=base_url('chat/send_chat_mail');?>",
				success:function(data)
				{
					return true;
				}
			});			
		}
		
        function getInboxUnreadMails(){
            $.ajax({
                url: "<?=base_url('app/get_inbox_unread_mails/');?>",
                type: "GET",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if(data.no_unread_mails > 0){
                        $('.unread-mail-icon').append('<span class="label label-success inbox-unread-mails">'+data.no_unread_mails+'</span>');
                        $('.sidebar-unread-mails').append('<span class="pull-right-container"><span class="label label-primary pull-right">'+data.no_unread_mails+'</span></span>');
                        $('.messages-menu').append(data.unread_mails);
                    }

                }
            });
        }
		
		function notifyBrowser(title, desc, url) {
			if (!Notification) {
				console.log('Desktop notifications not available in your browser..');
				return;
			}
			if (Notification.permission !== "granted") {
				Notification.requestPermission();
			} else {
				var notification = new Notification(title, {
					icon: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR5BV-b53B0-PUrfVIaGauiUZlPPMMcAUJcQb8Uv3aQZzfKryBDmQ',
					tag : 'chat-notify',
					body: desc,
				});
				isActive==false;

				// Remove the notification from Notification Center when clicked.
				notification.onclick = function() {
					window.open(url);
				};

				// Callback function when the notification is closed.
				notification.onclose = function() {
					console.log('Notification closed');
				};
			}
		}

        function display_popups(){
            var right = 10;
            var iii = 0;
            for(iii; iii < total_popups; iii++){
                if(popups[iii] != undefined){
                    var element = document.getElementById(popups[iii]);
                    element.style.right = right + "px";
                    right = right + 320;
                    element.style.display = "block";
                }
            }
            for(var jjj = iii; jjj < popups.length; jjj++){
                var element = document.getElementById(popups[jjj]);
                element.style.display = "none";
            }
        }
		
		// for fileupload in chat box
		var file_upload_to_id = "";
		var file_upload_to_visitor_id = "";
		
		function fileUpload(userid)
		{
			var w = 400;
			var h = 200;
			var left = (screen.width/2)-(w/2);
			var top = (screen.height/2)-(h/2);
			file_upload_to_id = userid;
			window.open("<?= base_url();?>upload","Upload File",'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		}

		function fileUploadtovisitor(userid)
		{
			var w = 400;
			var h = 200;
			var left = (screen.width/2)-(w/2);
			var top = (screen.height/2)-(h/2);
			file_upload_to_visitor_id = userid;
			window.open("<?= base_url();?>upload/fileuploadtovisitor","Upload File",'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		}
		
		function uploadfile(temp)
		{
			sendchatimage(file_upload_to_id,temp,"admin","admin");
		}

		function uploadfilefromadmintovisitor(temp)
		{
			sendchatimagefromadmintovisitor(file_upload_to_visitor_id,temp,"admin","visitors");
		}

		function sendchatimage(to_id,msg,from_table,to_table)
        {
			$.ajax({
				type:"POST",
				data: {'to_id':to_id,'msg':msg,'from_table':from_table,'to_table':to_table},
				url:"<?=base_url('chat/addAdminImage');?>",
				success:function(data)
				{
					return true;
				}
			});
		}

		function sendchatimagefromadmintovisitor(to_id,msg,from_table,to_table)
        {
			$.ajax({
				type:"POST",
				data: {'to_id':to_id,'msg':msg,'from_table':from_table,'to_table':to_table},
				url:"<?=base_url('chat/addAdminToVisitorImage');?>",
				success:function(data)
				{
					return true;
				}
			});
		}
		
		$('.visitor_pop').click(function(){
			alert('df')
		});
		
        //creates markup for a new popup. Adds the id to popups array.
        function register_popup(id, name, lastid,getusermsgs = 0){
			if (typeof(lastid)==='undefined') lastid = 0;
            for(var iii = 0; iii < popups.length; iii++){   
                //already registered. Bring it to front.
                if(id == popups[iii]){
                    Array.remove(popups, iii);
                    popups.unshift(id);
                    calculate_popups();
                    return true;
                }
            }   
            var element = '<div class="popup-box chat-popup" id="'+id+'"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">'+name+'</h3> <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button><button onClick="javascript:close_popup('+id+');" type="button" class="btn btn-box-tool" data-widget="remove"><a href="javascript:close_popup('+id+');"><i class="fa fa-times"></a></i> </button> </div></div><div class="box-body"> <div class="direct-chat-messages"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_'+id+'" userid="'+id+'" fromtable="admin" totable="admin"  onfocus="set_read_msgs('+id+')" onkeyup="checkenter(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn" id="btn_'+id+'" onclick="fileUpload('+id+');" ><i class="fa fa-upload"></i></button> </span> </div></div></div></div>';
            //document.getElementsByTagName("body")[0].innerHTML = document.getElementsByTagName("body")[0].innerHTML + element; 
			$('body').append(element);

			userdata_limitoffset[id] = [];
			userdata_limitoffset[id]['limit'] = 5;
			userdata_limitoffset[id]['offset'] = 5;
			console.log(userdata_limitoffset);
			var limit = 5;
			var offset = 5;
			$('.direct-chat-messages').on('scroll', function () {
			   var scroll = $('.direct-chat-messages').scrollTop();
			   if(scroll==0)
			   {
					console.log("on top");
					//busy = true;
					// start to load the first set of data
					//busy = true;
					offset = limit + offset;
					//displayRecords(limit, offset);
					$.ajax({
					  type: "POST",
					  url:"<?=base_url('messages/getScrollUserMsg');?>",
					  data: {'userid':id,'limit':limit,'offset':offset},
					  cache: false,
					  beforeSend: function() {
						
					  },
					  success: function(html) {
						if(html != 'false')
						{
							var oldmsg = $("#"+id+" .direct-chat-messages").html();						
							$.each(JSON.parse(html), function (index, value) {
								
								from_id = value.from_id;
								to_id = value.to_id;
								from_name = value.from_name;
								to_name = value.to_name;
								from_table = value.from_table;
								message = value.message;
								file = value.file;
								date = value.date_time;
								image = value.image;
								lastTimeID = value.id;
								
								formattedDate =  getDateString(new Date(value.date_time), "d M y hh:m:s b");
								if(message == "" || message == null)
								{
									message = "<a class='chatfilecreceive' target='_blank' href='<?php echo base_url();?>file/download/"+file+"'>"+file+"</a>";
								}
								if(to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
								{                                
									$("#"+from_id+" .direct-chat-messages").prepend('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
									$('#txt_'+from_id).trigger('focus');
								}
								else
								{                                
									$("#"+to_id+" .direct-chat-messages").prepend('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'+from_name+'</span><span class="direct-chat-timestamp pull-left">'+formattedDate+'</span></div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"><div class="direct-chat-text">'+message+'</div></div>');
									$('#txt_'+to_id).trigger('focus');
															
								}
							});
						}

					  }
					});
			   }
		   });
			popups.unshift(id);
			calculate_popups();  
			if(getusermsgs == 0)
			{
				get_messages(id,1);
			}
			$('#txt_'+id).trigger('focus');
			return true;
        }
		
		function visitor_register_popup(id, name, lastid,getusermsgs = 0){
			if (typeof(lastid)==='undefined') lastid = 0;
            for(var iii = 0; iii < popups.length; iii++){   
                //already registered. Bring it to front.
                if(id == popups[iii]){
                    Array.remove(popups, iii);
                    popups.unshift(id);
                    calculate_popups();
                    return true;
                }
            }   
			var element = '<div class="popup-box chat-popup" id="'+id+'"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">'+name+'</h3> <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button><button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Details"><i class="fa fa-user"></i></button><button onClick="javascript:close_popup('+id+',1);" type="button" class="btn btn-box-tool visitor_pop" data-widget="remove"><a href="javascript:close_popup('+id+',1);"><i class="fa fa-times"></a></i> </button> </div></div><div class="box-body"> <div class="direct-chat-messages"></div><div style="padding:10px;" class="direct-chat-contacts user-details"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_visitor_'+id+'" fromid="<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>" toid="'+id+'" fromtable="admin" totable="visitors"  onfocus="set_visitor_read_msgs('+id+')" onkeyup="checkenterfrmvisitor(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn" id="btn_'+id+'" onclick="fileUploadtovisitor('+id+');" ><i class="fa fa-upload"></i></button> </span> </div></div></div></div>';
			get_visitor_info_popup(id);
            //document.getElementsByTagName("body")[0].innerHTML = document.getElementsByTagName("body")[0].innerHTML + element; 
			$('body').append(element);
			popups.unshift(id);
			calculate_popups();  
			if(getusermsgs == 0)
			{
				get_visitor_messages(id,1,name);
			}
			var hgt = $("#"+id+" .direct-chat-messages").height();		
			$("#"+id+" .direct-chat-messages").animate({ scrollTop: hgt * 10 }, 500);
			$('#txt_visitor_'+id).trigger('focus');
			return true;
        }
		
        function set_read_msgs(userid)
        {
           if (typeof(userid)==='undefined') userid = 0;
            /*$.ajax({
                type:"POST",
                data: {'userid' : userid},
                url:"<?=base_url();?>messages/updateReadMsg",               
                success:function(data)
                {
                }
            });*/
        }
		
		function set_visitor_read_msgs(userid)
        {
           if (typeof(userid)==='undefined') userid = 0;
            /*$.ajax({
                type:"POST",
                data: {'userid' : userid},
                url:"<?=base_url();?>messages/updateVisitorReadMsg",               
                success:function(data)
                {                
                }
            });*/
        }

        function get_staff_info(userid)
        {
            if (typeof(userid)==='undefined') userid = 0;
            $.ajax({
                type:"POST",
                data: {'userid' : userid},
                url:"<?=base_url();?>messages/getStaffById",               
                success:function(data)
                {
                    if(data != 'false')
                    {
                        //console.log(data);
                        $.each(JSON.parse(data), function (index, value) {
                            user = value.name;
                            email = value.email;
                            birthdate = value.birth_date;
                            contactno = value.contact_no;
                            ip = '';
                            message_from = '';
                            city = '';
                            country = '';
                            currency = '';
                            date = '';
                            
                            $(".user-details").html('<li><a href="#">User<span class="pull-right text-red">'+user+'</span></a></li><li><a href="#">Email<span class="pull-right text-red">'+email+'</span></a></li><li><a href="#">Phone<span class="pull-right text-yellow">'+contactno+'</span></a></li><li><a href="#">IP<span class="pull-right text-yellow">'+ip+'</span></a></li><li><a href="#">From<span class="pull-right text-yellow">'+message_from+'</span></a></li><li><a href="#">City<span class="pull-right text-yellow">'+city+'</span></a></li><li><a href="#">Country<span class="pull-right text-yellow">'+country+'</span></a></li><li><a href="#">Currency<span class="pull-right text-yellow">'+currency+'</span></a></li><li><a href="#">Date <span class="pull-right text-green">'+date+'</span></a></li>');
                            $(".user_details").html('Agent Details');
                                                     
                        });
                    }
                }
            });
        }
		function get_visitor_info(userid)
        {
            if (typeof(userid)==='undefined') userid = 0;
            $.ajax({
                type:"POST",
                data: {'userid' : userid},
                url:"<?=base_url('messages/getVisitorById');?>",               
                success:function(data)
                {
                    if(data != 'false')
                    {
                        //console.log(data);
                        $.each(JSON.parse(data), function (index, value) {
                            user = value.name;
                            email = value.email;
                            phone = value.phone;                          
                            ip = value.ip;							
                            message_from = '<?php echo base_url(); ?>';
                            city = value.city;
                            country = value.country;
                            currency = value.currency;
							others = value.others;
                            date = value.created_at;
                            
                            $(".user-details").html('<li><a href="#">User<span class="pull-right text-red">'+user+'</span></a></li><li><a href="#">Email<span class="pull-right text-red">'+email+'</span></a></li><li><a href="#">Phone<span class="pull-right text-yellow">'+phone+'</span></a></li><li><a href="#">IP<span class="pull-right text-yellow">'+ip+'</span></a></li><li><a href="#">From<span class="pull-right text-yellow">'+message_from+'</span></a></li><li><a href="#">City<span class="pull-right text-yellow">'+city+'</span></a></li><li><a href="#">Country<span class="pull-right text-yellow">'+country+'</span></a></li><li><a href="#">Currency<span class="pull-right text-yellow">'+currency+'</span></a></li><li><a href="#">Others<span class="pull-right text-yellow">'+others+'</span></a></li><li><a href="#">Date <span class="pull-right text-green">'+date+'</span></a></li>');
                            $(".user_details").html('Visitor Details')
                                                     
                        });
                    }
                }
            });
        }
		
		//function for display visitor details in popup
		function get_visitor_info_popup(userid)
        {
            if (typeof(userid)==='undefined') userid = 0;
            $.ajax({
                type:"POST",
                data: {'userid' : userid},
                url:"<?=base_url('messages/getVisitorById');?>",               
                success:function(data)
                {
                    if(data != 'false')
                    {
                        //console.log(data);
                        $.each(JSON.parse(data), function (index, value) {
                            user = value.name;
                            email = value.email;
                            phone = value.phone;                          
                            ip = value.ip;							
                            message_from = '<?php echo base_url(); ?>';
                            city = value.city;
                            country = value.country;
                            currency = value.currency;                                                       
                            date = value.created_at;
                            
                            $("#"+userid+" .user-details").html('<div>User<span class="pull-right text-red">'+user+'</span></div><div>Email<span class="pull-right text-red">'+email+'</span></div><div>Phone<span class="pull-right text-yellow">'+phone+'</span></div><div>IP<span class="pull-right text-yellow">'+ip+'</span></div><div>From<span class="pull-right text-yellow">'+message_from+'</span></div><div>City<span class="pull-right text-yellow">'+city+'</span></div><div> Country<span class="pull-right text-yellow">'+country+'</span></div><div>Currency<span class="pull-right text-yellow">'+currency+'</span></div><div>Date <span class="pull-right text-green">'+date+'</span></div>');
                                                     
                        });
                    }
                }
            });
        }
		
        function get_unread_msg(userid)
        {
            if (typeof(userid)==='undefined') userid = 0;
            return $.ajax({
                type:"POST",
                data: {'userid' : userid},
                url:"<?=base_url('messages/getUnreadMsg');?>"
                
            });
        }

        function get_messages(userid,newmsg=0)
        {
            if (typeof(userid)==='undefined') userid = 0;
            $.ajax({
                type:"POST",
                data: {'lastid':lastTimeID,'userid' : userid},
                url:"<?=base_url('messages/getUserMsg');?>",               
                success:function(data)
                {
                    if(data != 'false')
                    {
						var oldmsg = $("#"+userid+" .direct-chat-messages").html();						
						if(newmsg == 1 && oldmsg != "")
						{							
							$("#"+userid+" .direct-chat-messages").html("");							
						}
                        //console.log(data);						
                        $.each(JSON.parse(data), function (index, value) {
							
                            from_id = value.from_id;
                            to_id = value.to_id;
                            from_name = value.from_name;
                            to_name = value.to_name;
                            from_table = value.from_table;
                            message = value.message;
                            file = value.file;
                            date = value.date_time;
                            image = value.image;
                            lastTimeID = value.id;
							
							formattedDate =  getDateString(new Date(value.date_time), "d M y hh:m:s b");
                            if(message == "" || message == null)
                            {
								message = "<a class='chatfilecreceive' target='_blank' href='<?php echo base_url();?>file/download/"+file+"'>"+file+"</a>";
                            }
                            if(to_id == "<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>")
                            {                                
                                $("#"+from_id+" .direct-chat-messages").prepend('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
                                $('#txt_'+from_id).trigger('focus');													
                            }
                            else
                            {                                
                                $("#"+to_id+" .direct-chat-messages").prepend('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'+from_name+'</span><span class="direct-chat-timestamp pull-left">'+formattedDate+'</span></div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"><div class="direct-chat-text">'+message+'</div></div>');
                                $('#txt_'+to_id).trigger('focus');	
														
                            }
                        });
						$("#"+userid+" .direct-chat-messages").scroll();
						$("#"+userid+" .direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);						
                    }
                }
            });
        }
		
		function get_visitor_messages(session_id,newmsg=0,name)
        {		
            $.ajax({
                type:"POST",
                data: {'session_id' : session_id},
                url:"<?=base_url('messages/getVisitorMsg');?>",               
                success:function(data)
                {
                    if(data != 'false')
                    {
						$('h3').attr('visitor-id',session_id);
						var oldmsg = $("#"+session_id+" .direct-chat-messages").html();						
						if(newmsg == 1 && oldmsg != "")
						{							
							$("#"+session_id+" .direct-chat-messages").html("");							
						}
                        //console.log(data);						
                        $.each(JSON.parse(data), function (index, value) {
							
                            from_id = value.from_id;
                            to_id = value.to_id;
                            from_name = value.from_name;
                            to_name = value.to_name;
                            from_table = value.from_table;
                            message = value.message;
                            file = value.file;
                            date = value.date_time;
                            image = value.image;
                            lastTimeID = value.id;
							
							formattedDate =  getDateString(new Date(value.date_time), "d M y hh:m:s b");
                            if(message == "" || message == null)
                            {
                                message = "<a class='chatfilecreceive' target='_blank' href='<?php echo base_url();?>file/download/"+file+"'>"+file+"</a>";
                            }
							
                            if(from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?> && from_table == 'admin')
                            {                                
                                $("#"+session_id+" .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'+value.name+'</span><span class="direct-chat-timestamp pull-left">'+formattedDate+'</span></div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"><div class="direct-chat-text">'+message+'</div></div>');
                                /*$("#"+session_id+" .direct-chat-messages").scroll();
								$("#"+session_id+" .direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
								$('#txt_visitor_'+session_id).trigger('focus');*/
                            }
                            else
                            {                                
                                $("#"+session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+value.name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
                                /*$("#"+session_id+" .direct-chat-messages").scroll();
								$("#"+session_id+" .direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
								$('#txt_visitor_'+session_id).trigger('focus');*/
                            }
                        });

						$("#"+session_id+" .direct-chat-messages").scroll();
						$("#"+session_id+" .direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
						$('#txt_'+session_id).trigger('focus');
                    }
                }
            });
        }
		
         function msg_popup(id, name, lastid) {
             if (typeof(lastid) === 'undefined') lastid = 0;
             close_popup(id);
             var element = '<div class="chat-popup" id="' + id + '"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">' + name + '</h3> <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button></div></div><div class="box-body"> <div class="direct-chat-messages"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_' + id + '" userid="' + id + '" fromtable="admin" totable="admin" onfocus="set_read_msgs(' + id + ')" onkeyup="checkenter(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn" id="btn_' + id + '" onclick="fileUpload(' + id + ');" ><i class="fa fa-upload"></i></button> </span> </div></div></div></div>';
             document.getElementById("chatwin").innerHTML = element;
             get_messages(id);
             get_staff_info(id);           
             var hgt = $("#" + id + " .direct-chat-messages").height();
             $("#" + id + " .direct-chat-messages").animate({scrollTop: hgt * 10}, 500);
             $('#txt_' + id).trigger('focus');             
             return true;
         }
		 
		 function visitor_msg_popup(id, name, lastid) {
             if (typeof(lastid) === 'undefined') lastid = 0;
             close_popup(id);
             var element = '<div class="chat-popup" id="' + id + '"><div class="box box-warning direct-chat direct-chat-warning"> <div class="box-header with-border"> <h3 class="box-title">' + name + '</h3> <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button></div></div><div class="box-body"> <div class="direct-chat-messages"></div></div><div class="box-footer"> <div class="input-group"> <input type="text" id="txt_visitor_'+id+'" fromid="<?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>" toid="'+id+'" fromtable="admin" totable="visitors"  onfocus="set_visitor_read_msgs('+id+')" onkeyup="checkenterfrmvisitor(this,event)" name="message" placeholder="Type Message ..." class="form-control textbox_send"> <span class="input-group-btn"> <button type="button" class="btn btn-warning btn-flat sendmsg_btn" id="btn_' + id + '" onclick="fileUpload(' + id + ');" ><i class="fa fa-upload"></i></button> </span> </div></div></div></div>';
             document.getElementById("chatwin").innerHTML = element;
             get_visitor_messages(id);
             get_visitor_info(id);             
             var hgt = $("#" + id + " .direct-chat-messages").height();
             $("#" + id + " .direct-chat-messages").animate({scrollTop: hgt * 10}, 500);
             $('#txt_' + id).trigger('focus');             
             return true;
         }

        //calculate the total number of popups suitable and then populate the toatal_popups variable.
        function calculate_popups(){
            var width = window.innerWidth;
            if(width < 540){
                total_popups = 0;
            } else {
                width = width - 0;
                //320 is width of a single popup box
                total_popups = parseInt(width/320);
            }
            display_popups();
        }
		function checkenter(obj,e)
		{
			if (e.keyCode == 13) {
				sendmsg(obj)
			}
		}
		function checkenterfrmvisitor(obj,e)
		{
			if (e.keyCode == 13) {
				sendmsgtovisitor(obj)
			}
		}
		function sendmsg(obj){
            var to_id = $(obj).attr("userid");
            var from_table = $(obj).attr("fromtable");
            var to_table = $(obj).attr("totable");
            if(to_table == "visitors"){
                var msg = $('#txt_visitor_'+to_id).val();
            }else{
                var msg = $('#txt_'+to_id).val();
            }
            if(msg != '')
            {
				// we have commented the code for adding message after hittinh enter,it will automatically display as functino call for new message
                if(to_table == "visitors"){
                    $('#txt_visitor_'+to_id).val('');
                    send_visitor_msg(to_id,msg,from_table,to_table);
                }else{
                    $('#txt_'+to_id).val('');
                    sendchatmsg(to_id,msg,from_table,to_table);
                }
			}
        }
        function sendmsgtovisitor(obj){
            var to_id = $(obj).attr("toid");
            var from_table = $(obj).attr("fromtable");
            var to_table = $(obj).attr("totable");
            if(to_table == "visitors"){
                var msg = $('#txt_visitor_'+to_id).val();
            }else{
                var msg = $('#txt_'+to_id).val();
            }
            if(msg != '')
            {
				// we have commented the code for adding message after hittinh enter,it will automatically display as functino call for new message
                if(to_table == "visitors"){
                    $('#txt_visitor_'+to_id).val('');
                    send_visitor_msg(to_id,msg,from_table,to_table);
                }else{
                    $('#txt_'+to_id).val('');
                    sendchatmsg(to_id,msg,from_table,to_table);
                }
			}
        }

			var  getDateString = function(date, format) {
				var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				getPaddedComp = function(comp) {
					return ((parseInt(comp) < 10) ? ('0' + comp) : comp)
			},
			formattedDate = format,
			o = {
				"y+": date.getFullYear(), // year
				"M+": months[date.getMonth()], //month
				"d+": getPaddedComp(date.getDate()), //day
				"h+": getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
				"H+": getPaddedComp(date.getHours()), //hour
				"m+": getPaddedComp(date.getMinutes()), //minute
				"s+": getPaddedComp(date.getSeconds()), //second
				"S+": getPaddedComp(date.getMilliseconds()), //millisecond,
				"b+": (date.getHours() >= 12) ? 'PM' : 'AM'
			};

			for (var k in o) {
				if (new RegExp("(" + k + ")").test(format)) {
					formattedDate = formattedDate.replace(RegExp.$1, o[k]);
				}
			}
			return formattedDate;
		};
        function sendchatmsg(to_id,msg,from_table,to_table)
        {
			$.ajax({
				type:"POST",
				data: {'to_id':to_id,'msg':msg,'from_table':from_table,'to_table':to_table},
				url:"<?=base_url('chat/addAdminChat');?>",
				success:function(data)
				{
					$("#"+to_id+" .direct-chat-messages").scroll();
					$("#"+to_id+" .direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
					get_messages(to_id,1);
					return true;
				}
			});
		}

		

        function send_visitor_msg(to_id,msg,from_table,to_table){
            var formdata = new FormData();
            formdata.append("to_id",to_id);
            formdata.append("msg",msg);
            formdata.append("from_table",from_table);
            formdata.append("to_table",to_table);
            $.ajax({
                type:"POST",
                data:formdata,
                url:"<?=base_url('chat/send_visitor_msg');?>",
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success:function(data)
                {
                    if(data.success = true){
                    }
                }
            });
        }

		
		function chckuserpopup(id)
		{
			var f = 0;
			for(var iii = 0; iii < popups.length; iii++){   
				//already registered. Bring it to front.
				if(id == popups[iii]){
					Array.remove(popups, iii);
					popups.unshift(id);
					calculate_popups();
					f = 1;
				}
			}
			return f;
		}
        
        //recalculate when window is loaded and also when window is resized.
        window.addEventListener("resize", calculate_popups);
        window.addEventListener("load", calculate_popups);
		var lastTimeID = 0;
        setInterval(function()
		{
			//get_chat();
		},2000);
        setInterval(function()
		{
           // get_visitor_chat();
		},3000);
        
        
		function get_chat(userid)
		{
			if (typeof(userid)==='undefined') userid = 0;
			$.ajax({
				type:"POST",
				data: {'lastid':lastTimeID,'userid' : userid},
				url:"<?=base_url('chat/getAdminChat');?>",				
				success:function(data)
				{
					if(data != 'false')
					{
						console.log(data);
						$.each(JSON.parse(data), function (index, value) {
							from_id = value.from_id;
							to_id = value.to_id;
							from_name = value.from_name;
							to_name = value.to_name;
							from_table = value.from_table;
							message = value.message;
                            image = value.image;
							file = value.file;
							date = value.date_time;
							lastTimeID = value.id;
							isread = value.is_read;
							formattedDate =  getDateString(new Date(value.date_time), "d M y hh:m:s b");
							if(message == "" || message == null)
							{
								message = "<a target='_blank' href='uploads/"+file+"'>"+file+"</a>";
							}
							//check if message is send by current user, then check to_id and if that id popup is open then add message else open popup
							//alert(isread + "-" + message);
							if(to_id == "")
							{
								chkpopupval = chckuserpopup(from_id);								
								if(chkpopupval)
								{									
									$("#"+from_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
									$('.direct-chat-messages').scroll();
									$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
								}
								else
								{  
									if(isread == 0)
									{										
										register_popup(from_id, from_name,lastTimeID,1);
										get_messages(from_id,1);
									}
									else
									{
										$("#"+from_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
										$('.direct-chat-messages').scroll();
										$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
									}
									
								}
							}
							else
							{					
								if(from_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
								{
									chkpopupval = chckuserpopup(to_id);									
									if(chkpopupval)
									{										
										$("#"+to_id+" .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'+from_name+'</span><span class="direct-chat-timestamp pull-left">'+formattedDate+'</span></div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"><div class="direct-chat-text">'+message+'</div></div>');
										$('.direct-chat-messages').scroll();
										$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
									}
									else
									{	
										if(isread == 0)
										{										
											register_popup(to_id, to_name,lastTimeID,1);
											get_messages(to_id,1);
										}
										else
										{
											$("#"+to_id+" .direct-chat-messages").append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'+from_name+'</span><span class="direct-chat-timestamp pull-left">'+formattedDate+'</span></div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"><div class="direct-chat-text">'+message+'</div></div>');
											$('.direct-chat-messages').scroll();
											$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
										}
									}
								}
							}
						});
					}
				}
			});
		}

        function get_visitor_chat(){
            $.ajax({
                type:"POST",
                data: {'lastid':lastTimeID},
                url:"<?=base_url('chat/getBackendSideVisitorChat');?>",               
                success:function(data)
                {
					if(data != 'false')
					{
						//console.log(data);
						$.each(JSON.parse(data), function (index, value) {
							from_id = value.from_id;
							to_id = value.to_id;
							from_name = value.name;
							to_name = value.to_name;
							from_table = value.from_table;
							message = value.message;						
							image = value.image;
							file = value.file;
							date = value.date_time;
							lastTimeID = value.id;
							isread = value.is_read;
							name = value.name;						
							session_id = value.session_id
							formattedDate =  getDateString(new Date(value.date_time), "d M y hh:m:s b");
							if(message == "" || message == null)
							{
								message = "<a target='_blank' href='uploads/"+file+"'>"+file+"</a>";
							}
							//check if message is send by current user, then check to_id and if that id popup is open then add message else open popup
							//if(to_id == <?php echo $this->session->userdata['is_logged_in']['staff_id']; ?>)
							if(from_table == 'visitors')
							{
								chkpopupval = chckuserpopup(session_id);	
								if(chkpopupval)
								{									
									$("#"+session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
									$('.direct-chat-messages').scroll();
									$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
								}
								else
								{  
									if(isread == 0)
									{															
										visitor_register_popup(session_id, name,lastTimeID,1);
										get_visitor_messages(session_id,1,name);
									}
									else
									{
										$("#"+session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+from_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
										$('.direct-chat-messages').scroll();
										$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
									}
									
								}
							}
							else
							{
								chkpopupval = chckuserpopup(session_id);	
								if(chkpopupval)
								{									
									$("#"+session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+to_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
									$('.direct-chat-messages').scroll();
									$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
								}
								else
								{  
									if(isread == 0)
									{															
										visitor_register_popup(session_id, name,lastTimeID,1);
										//get_visitor_messages(from_id,1,name);
										get_visitor_messages(session_id,1,name);
									}
									else
									{
										$("#"+session_id+" .direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left">'+to_name+'</span> <span class="direct-chat-timestamp pull-right">'+formattedDate+'</span> </div><img class="direct-chat-img" src="<?php echo base_url();?>resource/uploads/images/staff/'+image+'" alt="message user image"> <div class="direct-chat-text"> '+message+' </div></div>');
										$('.direct-chat-messages').scroll();
										$(".direct-chat-messages").animate({ scrollTop: 2000000000 }, 1000);
									}
									
								}
							}
						});
					}
                }
            });
        }
    </script>
    <!----------------- / Chat ----------------->
    
    </body>
</html>
