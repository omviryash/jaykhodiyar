<?php
$currUrl = $this->uri->segment(2);
if ($currUrl == '' || $currUrl == 'index') {
    $currUrl = 'Home';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> <?= ucwords($currUrl) ?> | Jay Khodiyar</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?= bootstrap_url('css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= bootstrap_url('css/custom.css'); ?>">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= bootstrap_url('font-awesome/font-awesome.min.css'); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= bootstrap_url('fonts/ionicons.min.css'); ?>">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?= dist_url('css/AdminLTE.min.css'); ?>">

        <!-- jQuery 2.2.3 -->
        <script src="<?= plugins_url('jQuery/jquery-2.2.3.min.js'); ?>"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?= bootstrap_url('js/jquery-ui.min.js'); ?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?= bootstrap_url('js/bootstrap.min.js'); ?>"></script>

        <!-------- App Core Js --------->
        <script src="<?= dist_url('js/core.js'); ?>"></script>
        <!-------- /App Core Js --------->

        <!-------- Chat --------->
        <link rel="stylesheet" href="<?= dist_url('css/custom.css'); ?>">
        <!-------- /Chat --------->

        <script type="text/javascript">
            var urls = '{"base":"<?= rtrim(base_url(), '/') ?>","css":"<?= base_url() ?>/dist/css","js":"<?= base_url() ?>/dist/js"}';
        </script>
        <script src="<?php echo base_url('node_modules/socket.io-client/dist/socket.io.js'); ?>"></script>
        <style>

            body{
                background: none transparent !important;
            }
            .popup-box{
                width: 100%;
            }
            .no-padding-right{
                padding-right: 0;
            }
            .btn_minimize:hover{
                color: white;
            }
            .direct-chat-success .right >.direct-chat-text{
                background: #1f7eba;
                border-color: #1f7eba;
            }
            .direct-chat-success .right>.direct-chat-text:after, .direct-chat-success .right>.direct-chat-text:before{
                border-left-color: #1f7eba;
            }
        </style>
    </head>
    <body>
        <?php if ($anystaff_login) { ?>
            <div class="popup-box chat-popup" style="right: 0px; display: block; border: 1px solid #1f7eba;">
                <div id="chatbox" class="box box-success direct-chat direct-chat-success collapsed-box" style="border:none;">
                    <div class="box-header with-border bg-success btnmin" style="background-color: #1f7eba;">
                        <h3 class="box-title">Let's Chat - We are Online!</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool btn_minimize" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="visitor-detail-section" id="visitor-detail-section">
                            <?php
                            $session_data = $this->session->userdata('is_visitor_logged_in');
                            //echo "<pre>";print_R($session_data);echo "</pre>";
                            if ($session_data['name'] == "") {
                            ?>
                                <div class="col-md-12 form-group">
                                    <label class="">
                                        Please introduce yourself
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <input type="text" name="visitor_name" id="visitor_name" class="form-control visitor_name visitor-input" placeholder="Your Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="visitor_phone" id="visitor_phone" class="form-control visitor_phone hidden visitor-input" placeholder="Your Phone">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="visitor_email" id="visitor_email" class="form-control visitor_email hidden visitor-input" placeholder="Your Email">
                                </div>
                                <div class="form-group">
                                    <input type="button" class="btn btn-success btn-flat col-md-4 pull-right hidden btn-submit-visitor-detail" value="Save" style="background-color: #1f7eba; border-color:#1f7eba;">
                                </div>
                                <div class="clearfix"></div>
                            <?php } else { ?>
                                <div class="col-md-12 form-group">
                                    <label class="">You are logged in as: <?php echo $session_data['name']; ?></label>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="direct-chat-messages frontendchat-back">
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="input-group">
                            <input type="text" name="message" placeholder="Type Message ..." class="inputmsg form-control" onkeyup="check_enter(this, event)">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success btn-flat btn-send-message" style="background-color: #1f7eba; border-color:#1f7eba;">Send</button>
                                <button type="button" class="btn btn-success btn-flat btn-send-file" id="btn_<?php echo $visitor_id; ?>" onclick="fileUploadfront('<?php echo $visitor_id; ?>');" ><i class="fa fa-upload"></i></button>
                                <!--<button type="button" class="btn btn-success btn-flat btn-test-mail" value="MAIL">Mail</button>-->
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="popup-box chat-popup" style="right: 10px; display: block;">
                <div id="mailbox" class="box box-success direct-chat direct-chat-success collapsed-box">
                    <div class="box-header with-border bg-success btnmin">
                        <h3 class="box-title">Leave your message</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool btn_minimize" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12"> Leave your message in the form below, and we will receive it by e-mail! <br/></div>
                        <div class="col-md-12 form-group">
                            <textarea rows="3" name="message" class="form-control message visitor-input" placeholder="Your message*"></textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="text" name="visitor_name" class="form-control visitor_name visitor-input" placeholder="Your name*">
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="text" name="visitor_phone" class="form-control visitor_phone visitor-input" placeholder="Your phone*">
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="text" name="visitor_email" class="form-control visitor_email visitor-input" placeholder="Your email*">
                        </div>
                        <div class="col-md-12">
                            <b>We will contact you soon.</b>
                        </div>
                        <br/>
                        <div class="col-md-12 form-group">
                            <input type="button" class="col-md-3 btn btn-success pull-right btn-send-mail" value="SEND">
                        </div>
                    </div>
                </div>
                <div id="loader_image" style="display:none;"><img src="<?php echo base_url(); ?>resource/image/loader.gif" alt="" width="24" height="24"> Loading...please wait</div>
                <!-- for message if data is avaiable or not -->
                <div id="loader_message"></div>
            </div>
        <?php } ?>
        <!----------------- Chat ----------------->
        <script>
            var port_number = '<?php echo PORT_NUMBER; ?>';
            var parent_site_url = (window.location != window.parent.location) ? document.referrer : document.location.href;
            var session_id = '<?php echo $session_id ?>';
            
            var visitor_id = '<?php echo $visitor_id ?>';
            var visitor_name = '';
            var visitor_email = '';
            var lastTimeID = 0;
            var disp_def_msg = 0;
            $(document).ready(function () {
                $(".direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left"></span> <span class="direct-chat-timestamp pull-right"></span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/default.png" alt="message user image"> <div class="direct-chat-text">Hello! Thanks for visiting Jay Khodiyar! How may I help you?  </div></div>');
                disp_def_msg = 1;
                $(".btnmin").click(function () {
                    $('.direct-chat').toggleClass('collapsed-box');
                    $('.inputmsg').focus();
                });
                setTimeout(function () {
                    if ($("#chatbox").hasClass("collapsed-box")) {
                        $(".btnmin").click();
                        if (disp_def_msg == 0)
                        {
                            $(".direct-chat-messages").append('<div class="direct-chat-msg"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-left"></span> <span class="direct-chat-timestamp pull-right"></span> </div><img class="direct-chat-img" src="<?php echo base_url(); ?>resource/uploads/images/staff/default.png" alt="message user image"> <div class="direct-chat-text">Hello! Thanks for visiting Jay Khodiyar! How may I help you?  </div></div>');
                            disp_def_msg = 1;
                        }
                    }
                    if ($("#mailbox").hasClass("collapsed-box")) {
                        $(".btnmin").click();
                    }
                }, 5000);

                $("body").click(function (e) {

                    if (e.target.id == "visitor-detail-section" || $(e.target).parents("#visitor-detail-section").size()) {
                        if ($("#visitor-detail-section").find('.visitor_name').length > 0 && !$('.visitor_name').hasClass('required')) {
                            $('.visitor_name').addClass('required');
                            $('.visitor_phone').removeClass('hidden').addClass('required');
                            $('.visitor_email').removeClass('hidden').addClass('required');
                            $('.btn-submit-visitor-detail').removeClass('hidden');
                        }
                    } else {
                        if ($("#visitor-detail-section").find('.visitor_name').length > 0 && $('.visitor_name').hasClass('required')) {
                            $('.visitor_name').removeClass('required');
                            $('.visitor_phone').removeClass('required').addClass('hidden');
                            $('.visitor_email').removeClass('required').addClass('hidden');
                            $('.btn-submit-visitor-detail').addClass('hidden');
                        }
                    }
                });

                $(document).on('click', '.btn-submit-visitor-detail', function () {
                    var error = 0;
                    $('.visitor-input').each(function () {
                        if ($(this).val() == '') {
                            $(this).css('border', 'solid 1px #8B0000');
                            error = 1;
                        } else {
                            $(this).css('border', 'solid 1px #CCC');
                        }
                    });
                    visitor_name
                    var visitor_name = $('#visitor_name').val();
                    var email = $('#visitor_email').val();
                    var visitor_phone = $('#visitor_phone').val();
                    var phone_regex = /^[0-9]+$/;
                    var name_regex = /^[a-zA-Z\s]+$/;
                    var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                    if (!visitor_name.match(name_regex) || visitor_name.length == 0) {
                        $('#visitor_name').css('border', 'solid 1px #8B0000');
                        $("#visitor_name").focus();
                        error = 1;
                    }
                    if (!email.match(email_regex) || email.length == 0) {
                        $('#visitor_email').css('border', 'solid 1px #8B0000');
                        $("#visitor_email").focus();
                        error = 1;
                    }
                    if (!visitor_phone.match(phone_regex) || visitor_phone.length == 0) {
                        $('#visitor_phone').css('border', 'solid 1px #8B0000');
                        $("#visitor_phone").focus();
                        error = 1;
                    }
                    if (error == 0) {
                        var formdata = new FormData();
                        formdata.append("visitor_name", $('.visitor_name').val());
                        formdata.append("visitor_phone", $('.visitor_phone').val());
                        formdata.append("visitor_email", $('.visitor_email').val());
                        formdata.append("session_id",<?php echo $session_id; ?>);
                        $.ajax({
                            url: "<?= base_url('frontend/save_visitor_detail/'); ?>",
                            type: "POST",
                            data: formdata,
                            contentType: false,
                            cache: false,
                            processData: false,
                            dataType: 'json',
                            success: function (data) {
                                visitor_id = data.visitor_id;
                                visitor_name = data.visitor_name;
                                visitor_email = data.visitor_email;
                                $('.visitor-detail-section').html('<h5 style="padding-left:10px;"><b>' + visitor_name + '</b></h5>');
                                $('.direct-chat-messages').prepend(data.messages);
                                $('.direct-chat-messages').animate({scrollTop: 2000000000}, 1000);
                                get_chat();
                            }
                        });
                    }
                });

                $(document).on('click', '.btn-send-mail', function () {
                    var error = 0;
                    $('.visitor-input').each(function () {
                        if ($(this).val() == '') {
                            $(this).css('border', 'solid 1px #FF0000');
                            error = 1;
                        } else {
                            $(this).css('border', 'solid 1px #CCC');
                        }
                    });
                    if (error == 0) {
                        var formdata = new FormData();
                        formdata.append("visitor_name", $('.visitor_name').val());
                        formdata.append("visitor_phone", $('.visitor_phone').val());
                        formdata.append("visitor_email", $('.visitor_email').val());
                        formdata.append("message", $('.message').val());
                        $.ajax({
                            url: "<?= base_url('frontend/send_mail/'); ?>",
                            type: "POST",
                            data: formdata,
                            contentType: false,
                            cache: false,
                            processData: false,
                            dataType: 'json',
                            beforeSend: function () {
                                $("#loader_message").html("").hide();
                                $('#loader_image').show();
                            },
                            success: function (data) {
                                $('#loader_image').hide();
                                $('.visitor-input').each(function () {
                                    $(this).val('');
                                });
                                $('.popup-box').hide();
                                alert(data.admin_message);
                            }
                        });
                    }
                });
                $(document).on('click', '.btn-send-message', function () {
                    var error = 0;
                    if ($('input[name="message"]').val() == '') {
                        $('input[name="message"]').css('border', 'solid 1px #8B0000');
                        error = 1;
                    } else {
                        $('input[name="message"]').css('border', '1px solid #CCC');
                    }

                    if (error == 0) {
                        var formdata = new FormData();
                        var message = $('input[name="message"]').val();
                        formdata.append("visitor_id", visitor_id);
                        formdata.append("visitor_email", visitor_email);
                        formdata.append("visitor_name", visitor_name);
                        formdata.append("visitor_phone", $('.visitor_phone').val());
                        formdata.append("message", message);
                        $('input[name="message"]').val('');
                        $.ajax({
                            url: "<?= base_url('frontend/send_message/'); ?>",
                            type: "POST",
                            data: formdata,
                            contentType: false,
                            cache: false,
                            processData: false,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success == true) {
                                    visitor_id = data.visitor_id;
                                    //$('input[name="message"]').val('');
                                    $('.direct-chat-messages').append(data.chat_html);
                                    $('.direct-chat-messages').animate({scrollTop: 2000000000}, 1000);
                                }
                            }
                        });
                    }
                });
                setInterval(function ()
                {
                    //get_chat() ;
                }, 2000);
            });

            function check_enter(obj, e)
            {
                if (e.keyCode == 13) {
                    $('.btn-send-message').trigger('click');
                }
            }

            get_chat();
            function get_chat()
            {
                var message = $('input[name="message"]').val();
                var formdata = new FormData();
                formdata.append("visitor_id", visitor_id);
                if (visitor_id != 0) {
                    $.ajax({
                        type: "POST",
                        data: formdata,
                        url: "<?= base_url(); ?>frontend/getVisitorChat",
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data)
                        {
                            if (data.success == true) {
                                $('.direct-chat-messages').html(data.chat_html);
                                $('.direct-chat-messages').animate({scrollTop: 2000000000}, 1000);
                                $('.inputmsg').trigger('focus');
                            }
                        }
                    });
                }
            }

            // for fileupload in chat box from frontside
            var file_upload_to_id = "0";

            function fileUploadfront(userid)
            {
                var w = 400;
                var h = 200;
                var left = (screen.width / 2) - (w / 2);
                var top = (screen.height / 2) - (h / 2);
                file_upload_to_id = userid;
                window.open("<?php echo base_url(); ?>upload/frontupload", "Upload File", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
            }

            function uploadfileFront(temp)
            {
                sendchatimagefront(file_upload_to_id, temp, "visitors", "staff,admin",<?php echo $visitor_id; ?>);
            }
            function sendchatimagefront(to_id, msg, from_table, to_table, from_id)
            {
                //alert('<?php echo base_url() . 'chat/addFrontImage'; ?>');return false;
                $.ajax({
                    type: "POST",
                    data: {'to_id': to_id, 'msg': msg, 'from_table': from_table, 'to_table': to_table, 'from_id': from_id},
                    url: "<?= base_url('frontend/addFrontImage'); ?>",
                    success: function (data)
                    {
                        return true;
                    }
                });
            }
        </script>
        <!----------------- / Chat ----------------->

        <script>
            <?php if ($anystaff_login) { ?>
                function sendHeartbeat() {
                    setTimeout(sendHeartbeat, 8000);
                    socket.emit('ping1', {beat: 1, sender_id: '<?= $session_data['session_id']; ?>'});
                    //console.log("every 8 seconds");
                }

                var socket = io.connect('http://' + window.location.hostname + ':'+port_number);
                socket.on('new_message_visitor_to_client', function (data) {
                    if (data.from_session_id == '<?php echo $this->session->userdata['is_visitor_logged_in']['session_id']; ?>')
                    {
                        /*$('input[name="message"]').val('');
                         $('.direct-chat-messages').append(data.chat_html);
                         $('.direct-chat-messages').animate({ scrollTop: 2000000000 }, 1000);*/
                    }
                });

                socket.on('new_message_client_to_visitor', function (data) {
                    if (data.to_session_id == '<?php echo $this->session->userdata['is_visitor_logged_in']['session_id']; ?>')
                    {
                        get_chat();
                    }
                });
                socket.on('new_file_client_to_visitor', function (data) {
                    if (data.to_session_id == '<?php echo $this->session->userdata['is_visitor_logged_in']['session_id']; ?>')
                    {
                        get_chat();
                    }
                });
                socket.on('new_file_visitor_to_client', function (data) {
                    if (data.from_session_id == '<?php echo $this->session->userdata['is_visitor_logged_in']['session_id']; ?>')
                    {
                        get_chat();
                    }
                });

                socket.on('pong1', function (data) {
                    //console.log(data);
                });
                
                socket.on('visitor_public_ip', function (data) {
                    $.ajax({
                        url: "<?= base_url('frontend/update_from_address/'); ?>",
                        type: "POST",
                        data: {'session_id': session_id, 'ip': data.ip, 'from_address': parent_site_url, 'socket_id': socket.id},
                        dataType: 'json',
                        success: function (data) {
                            
                        }
                    });
                    return false;
                });
                
                socket.on('connect', function(data) {
                    $.ajax({
                        url: "<?= base_url('frontend/update_from_address/'); ?>",
                        type: "POST",
                        data: {'session_id': session_id, 'ip': '', 'from_address': parent_site_url, 'socket_id': socket.id},
                        dataType: 'json',
                        success: function (data) {
                            socket.emit( 'visitor_is_back', {
                                session_id: session_id,
                            });
                        }
                    });
                    return false;
                });

                setTimeout(sendHeartbeat, 40000);

            <?php } ?>
        </script>
    </body>
</html>
