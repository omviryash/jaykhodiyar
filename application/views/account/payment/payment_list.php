<?php
if (!$this->app_model->have_access_role(PAYMENT_MODULE_ID, "view")) {
	$this->session->set_flashdata('error_message', 'You have not permission to access this page.');
	redirect("/");
}
?>
<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php
			$payment_add_role = $this->app_model->have_access_role(PAYMENT_MODULE_ID, "add");
			if($this->uri->segment(1) == 'report'){
				echo "<small class='text-primary text-bold'>Report : Payment Receipt Details</small>";
			}
			//echo $this->uri->segment(1);
			if($this->uri->segment(1) == 'payment'){
				echo "<small class='text-primary text-bold'>Account : Payment</small>";
				if($payment_add_role){
					echo '<a href="'.base_url().'payment/add" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Add Payment</a>';
				}
			}            
			?>
		</h1>
	</section>
	<div class="clearfix">
		<div class="row">
			<div style="margin: 15px;">
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-body">
							<div class="row">
								<form class="form-horizontal" action="<?php echo base_url('payment/saving_payment_data');?>" method="post" id="form_payment">
									<div class="col-md-12"> 
										<div class="form-group col-md-4">
											<label for="inputEmail3" class="col-sm-3 input-sm">Party</label>
											<div class="col-sm-9 dispaly-flex">
												<select name="party_id" id="customer_name1" class="form-control input-sm">
												</select> 
											</div>
										</div>
										<div class="form-group col-md-4">
											<label for="inputEmail3" class="col-sm-3 input-sm">From Date</label>
											<div class="col-sm-9">
												<input type="text" class="form-control input-sm disabled" name="from_date" id="datepicker3" value="">
											</div>
										</div>
										<div class="form-group col-md-4">
											<label for="inputEmail3" class="col-sm-3 input-sm">To Date</label>
											<div class="col-sm-9">
												<input type="text" class="form-control input-sm disabled" name="to_date" id="datepicker3"  value="">
											</div>
										</div>
									</div>
									<br />
									<br />
									<br />
									<div class="clearfix"></div>
									<div class="col-md-12">
										<table id="payment_list" class="table custom-table agent-table" width="100%">
											<thead>
												<tr>      		 
													<th>Action</th>
													<th>Receipt No.</th>
													<th>Party Name</th>
													<th>Order / Quote / Complain No.</th>
													<th>Bank</th>
													<th>Amount</th>
													<th>Payment Date</th>
													<th>Payment Type</th>
												</tr>
											</thead>
											<tbody>                                                
											</tbody>
										</table>
									</div>
								</form>										
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var table;
	$(document).ready(function() {
		initAjaxSelect2($("#customer_name1"),"<?=base_url('app/party_select2_source')?>");
		table = $('#payment_list').DataTable({
			"serverSide": true,
			"ordering": true,
			"searching": true,
			"aaSorting": [[1, 'desc']],
			"ajax": {
				"url": "<?php echo site_url('payment/payment_list_datatable')?>",
				"type": "POST",    
				"data": function(d){
					d.party_id = $("#customer_name1").val();                    
					d.from_date = $("input[name=from_date]").val();
					d.to_date = $("input[name=to_date]").val();
				},    
				"ajax": function ( data, callback, settings ) {
					setTimeout( function () {
						var out = [];             
						for ( var i=data.start, ien=data.start+data.length ; i<ien ; i++ ) {
							out.push(  [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5',  i+'-6' ] );
						}             
						callback( {
							"draw": data.draw,
							"data": out,
							"recordsTotal": data.recordsTotal,
							"recordsFiltered": data.recordsFiltered
						});
					}, 50 );
				},         
			},
			"scroller": {
				"loadingIndicator": true
			},
			"sScrollX": "100%",
			"sScrollXInner": "110%",
			"deferRender": true,
			"scrollY": 500,            
		}); 
	});
	$(document).on("change", "#customer_name1", function(){        
		table.draw();
	}); 
	$(document).on("change", "input[name=from_date]", function(){
		table.draw();
	});
	$(document).on("change", "input[name=to_date]", function(){
		table.draw();
	});
	$(document).on("click",".delete_button",function(){
		var value = confirm('Are you sure delete this records?');
		var tr = $(this).closest("tr");
		if(value){
			$.ajax({
				url: $(this).data('href'),
				type: "POST",
				data: 'id_name=payment_id&table_name=payment',
				success: function(data){
					tr.remove();
					show_notify('Payment Deleted Successfully!', true);
				}
			});
		}
	});	
</script>
