<div class="content-wrapper">
	<form class="form-horizontal" action="<?=base_url('payment/save_payment_data')?>" method="post" id="form_payment">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<small class="text-primary text-bold">Payment</small>
                <small class="text-center"><label class="label label-warning">Shortcut Keys : Ctrl+S = Save</label></small>
				<?php
				$payment_view_role= $this->applib->have_access_role(PAYMENT_MODULE_ID,"view");
				$payment_add_role= $this->applib->have_access_role(PAYMENT_MODULE_ID,"add");
				$payment_edit_role= $this->applib->have_access_role(PAYMENT_MODULE_ID,"edit");
				?>
				<?php if(isset($payment_id)): ?>			

				<?php endif;?>

				<?php if(isset($payment_id) && !empty($payment_id)){ ?>

				<?php if($this->applib->have_access_role(CAN_APPROVE_PAYMENT_RECEIPT,"allow")){
					echo '<button type="button" class="btn btn-info btn-xs pull-right btn_approve_ord" style="margin: 5px;">';
					if($is_approved == 1){
						echo 'Disapprove';
					}elseif($is_approved == 0){
						echo 'Approve';
					}
					echo '</button>';
					}else{
						echo "<small class='text-primary text-bold pull-right app_lab' style='margin: 5px;'>";
					if($is_approved == 1){
						echo "Approved";
					}elseif($is_approved == 0){
						echo "Disapproved";
					}
						echo "</small>";
					} 
				?>

				<?php if($payment_edit_role): ?>
				<button type="submit" class="btn btn-info btn-xs pull-right btn-save-item" id="form_payment" style="margin: 5px;">Update </button>
				<?php endif;?>
				<?php } else { ?>
				<?php if($payment_add_role): ?>
				<button type="submit" class="btn btn-info btn-xs pull-right btn-save-item" id="form_payment" style="margin: 5px;">Save </button>
				<?php endif;?>
				<?php } ?>

				<?php if($payment_view_role): ?>
				<a href="<?=base_url()?>payment/payment_list" class="btn btn-info btn-xs pull-right" style="margin: 5px;">Payment List</a>
				<?php endif;?>
				<?php if($payment_add_role): ?>
				<a href="<?=BASE_URL?>payment/add/" class="btn btn-info btn-xs pull-right" style="margin: 5px;margin-right: 5px;">Add Payment</a>
				<?php endif;?>
                <?php if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view") && isset($is_approved) && $is_approved == 1) { ?>
                    <span class="pull-right send_sms_div" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="send_sms" id="send_sms" class="send_sms" >  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } else if($this->app_model->have_access_role(DISPLAY_SEND_SMS_CHECKBOX, "view")) { ?>
                    <span class="pull-right send_sms_div hidden" style="margin-right: 20px;">
                        <div class="form-group">
                            <label for="send_sms" class="col-sm-12 input-sm" style="font-size: 16px; line-height: 25px;">
                                <input type="checkbox" name="send_sms" id="send_sms" class="send_sms" >  &nbsp; Send SMS
                            </label>
                        </div>
                    </span>
                <?php } ?>
			</h1>
		</section>
		<div class="clearfix">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" id="tabs_1">Payment Details</a></li>                     
						<li class=""><a href="#tab_2" data-toggle="tab" id="tabs_2">Login Details</a></li>                     
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<!-- Horizontal Form -->
							<div class="row">
								<div class="col-md-12">
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Payment Details</legend>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">
															Receipt No. 
														</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="receipt_no" id="receipt_no" value="<?=isset($receipt_no) ? $receipt_no : '';?>" readonly="readonly">
														</div>
													</div>
													
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Reference From<span class="required-sign">&nbsp;*</span></label>
														<div class="col-sm-8">
															<select name="referance_from" id="referance_from" class="form-control input-sm" required>
																<option value="">--Select--</option>
																<option value="sales_order" <?=(isset($referance_from) ? ($referance_from == 'sales_order' ? 'selected' : '') : '')?>>Sales Order</option>
																<option value="quotation" <?=(isset($referance_from) ? ($referance_from == 'quotation' ? 'selected' : '') : '')?>>Quotation</option>
																<option value="complain" <?=(isset($referance_from) ? ($referance_from == 'complain' ? 'selected' : '') : '')?>>Complain</option>
															</select> 
														</div>
													</div>
													
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Party<span class="required-sign">&nbsp;*</span></label>
														<input type="hidden" name="payment_id" id="payment_id" value="<?=isset($payment_id) ? $payment_id : ''; ?>" >
														<input type="hidden" name="payment_edit" value="<?=isset($payment_id) ? 'payment_edit' : ''; ?>" >
														<div class="col-sm-8 dispaly-flex">  
															<select name="party_id" id="party_name" class="form-control input-sm" onchange="" required>
															</select> 
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Order / Quote / Complain No.<span class="required-sign">&nbsp;*</span></label>
														<div class="col-sm-8">
															<select name="referance_no" id="referance_no" class="form-control input-sm" onchange="" required>											
															</select> 
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Bank<span class="required-sign">&nbsp;*</span>														</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="payment_bank" value="<?=isset($payment_bank) ? $payment_bank : ''; ?>" id="payment_bank" required>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">By<span class="required-sign">&nbsp;*</span></label>
														<div class="col-sm-8">
															<select name="payment_by" id="payment_by" class="form-control input-sm" onchange="" required></select> 
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Reference No.<span class="required-sign">&nbsp;*</span></label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="payment_by_no" id="payment_by_no" value="<?=isset($payment_by_no) ? $payment_by_no : ''; ?>" required>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Receive Date</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="receipt_date_2" id="datepicker3" value="<?=isset($receipt_date_2) ? date('d-m-Y',strtotime($receipt_date_2)) : date('d-m-Y'); ?>">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Receipt Date</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm disabled" name="payment_date" id="datepicker3" required value="<?=isset($payment_date) ? date('d-m-Y',strtotime($payment_date)) : date('d-m-Y'); ?>">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Amount<span class="required-sign">&nbsp;*</span>
														</label>
														<div class="col-sm-8">
															<input type="number"  required class="form-control input-sm " name="amount" value="<?php echo isset($amount) ? $amount : ''; ?>" id="amount">
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Credit/Debit<span class="required-sign">&nbsp;*</span>														</label>
														<div class="col-sm-8 dispaly-flex">
															<select name="payment_type" required id="payment_type" class="form-control input-sm payment_type">
																<option value="">-- Select --</option>
																<option <?php if(isset($payment_type) AND $payment_type == 'credit') { echo 'selected'; } ?> value="credit">Credit</option>
																<option <?php if(isset($payment_type) AND $payment_type == 'debit') { echo 'selected'; } ?> value="debit">Debit</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 input-sm">Receiver<span class="required-sign">&nbsp;*</span></label>
														<div class="col-sm-8 dispaly-flex">
															<select name="reciver_id" required id="reciver_id" class="form-control input-sm reciver_id"></select>
														</div>
													</div>
												</div>
											</div> 
										</div>										
									</fieldset>
								</div>
							</div>
						</div>

						<div class="tab-pane" id="tab_2">
							<?php if(isset($payment_id) && !empty($payment_id)){ ?>
							<div class="row">
								<div class="col-md-12">
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Login Details </legend>
										<div class="col-md-6">
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Created By</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm created_by" id="created_by" placeholder="" value="<?=(isset($created_by_name))? $created_by_name : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Created Date</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm created_at" id="created_at" placeholder="" value="<?=(isset($created_at))? $created_at : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Updated By</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm updated_by" id="updated_by" placeholder="" value="<?=(isset($updated_by_name))? $updated_by_name : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Updated Date</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm updated_at" id="updated_at" placeholder="" value="<?=(isset($updated_at))? $updated_at : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Approved By</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm approved_by" id="approved_by" value="<?=(isset($approved_by_name))? $approved_by_name : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label for="" class="col-sm-3 input-sm">Approved Date</label>
												<div class="col-sm-9">
													<input type="text" class="form-control input-sm approved_date_time" id="approved_date_time" value="<?=(isset($approved_at))? $approved_at : ''; ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</fieldset>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		<?php if(isset($referance_from) && $referance_from == 'sales_order'){ ?>
		setSelect2Value($("#referance_no"),"<?=base_url('app/set_sales_order_no_select2_val_by_id/'.$referance_no)?>");	
		<?php }elseif(isset($referance_from) && $referance_from == 'quotation'){ ?>
		setSelect2Value($("#referance_no"),"<?=base_url('app/set_quotation_no_select2_val_by_id/'.$referance_no)?>");
		<?php } elseif(isset($referance_from) && $referance_from == 'complain'){ ?>
		setSelect2Value($("#referance_no"),"<?=base_url('app/set_complain_select2_val_by_id/'.$referance_no)?>");
		<?php } ?>

		<?php if(isset($party_id)){ ?>
		setSelect2Value($("#party_name"),"<?=base_url('app/set_party_select2_val_by_id/'.$party_id)?>");
		<?php } ?>

		<?php if(isset($payment_by)){ ?>
		setSelect2Value($("#payment_by"),"<?=base_url('app/set_payment_by_select2_val_by_id/'.$payment_by)?>");
		<?php } ?>

		<?php if(isset($reciver_id)){ ?>
		setSelect2Value($("#reciver_id"),"<?=base_url('app/set_staff_select2_val_by_id/'.$reciver_id)?>");
		<?php } ?>

		initAjaxSelect2($("#party_name"),"<?=base_url('app/so_or_proforma_party_select2_source')?>");
		initAjaxSelect2($("#payment_by"),"<?=base_url('app/payment_by_select2_source')?>");
		initAjaxSelect2($("#reciver_id"),"<?=base_url('app/staff_select2_source')?>");
        
        $('input[type="checkbox"].send_sms').iCheck({
            checkboxClass: 'icheckbox_flat-green',
        });
        
		get_referance_no($('#referance_from').val());

		$(document).on('change','#referance_from', function(){
			$("#referance_no").val(null).trigger("change");
			$("#party_name").val(null).trigger("change");
			var ref_val = $(this).val();
			var referance_from = $(this).val();
			
			if(ref_val == 'sales_order'){				
				initAjaxSelect2($("#party_name"),"<?=base_url('app/party_select2_source_from_sales_order')?>");
			}else if(ref_val == 'quotation'){				
				initAjaxSelect2($("#party_name"),"<?=base_url('app/party_select2_source_from_quotation')?>");
			}else if(ref_val == 'complain'){				
				initAjaxSelect2($("#party_name"),"<?=base_url('app/party_select2_source_from_complain')?>");
			}
		});

		$(document).on('click','#add_expected_payment', function () {});

		$(document).on('change','#party_name', function () {
			$("#referance_no").val(null).trigger("change");
			var ref_val = $('#referance_from').val();
			var party_id = $(this).val();
			get_referance_no(ref_val);
			
		});
        
        $(document).bind("keydown", function(e){
            if(e.ctrlKey && e.which == 83){
                $("#form_payment").submit();
                return false;
            }
        });        

		$(document).on('submit', '#form_payment', function () {
			if($.trim($("#party_name").val()) == ""){
				show_notify('Please select Party !', false);
				return false;
			}
			if($.trim($("#amount").val()) == ""){
				show_notify('Please Eneter Amount !', false);
				return false;
			}
			if($.trim($("#payment_type").val()) == ""){
				show_notify('Please Select Payment Type !', false);
				return false;
			}
			$("#ajax-loader").show();
			var postData = new FormData(this);
			$.ajax({
				url: "<?=base_url('payment/save_payment_data') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
					if(json.status == 0){
						show_notify(json.msg,false);
						$('.module_save_btn').removeAttr('disabled','disabled');
						return false;
					}
					if (json['success'] == 'false'){
						show_notify(json['msg'],false); 
						$('.module_save_btn').removeAttr('disabled','disabled');
					}
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('payment/payment_list') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('payment/payment_list') ?>";
					}
					$("#ajax-loader").hide();
					return false;
				},
			});
			return false;
		});

		$(document).on("click",".btn_approve_ord",function(){
			$("#ajax-loader").show();
			var payment_id = $('#payment_id').val();
			$.ajax({
				url: '<?=BASE_URL?>payment/update_payment_status_to_approve',
				type: "POST",
				data: {payment_id:payment_id},
				dataType: 'json',
				success: function(data) {
					if (data.success == true) {
						if(data.status == 1){
							$(".btn_approve_ord").html('Disapprove');
                            $(".send_sms_div").removeClass('hidden');
						}else if(data.status == 0){
							$(".btn_approve_ord").html('Approve');
                            $(".send_sms_div").addClass('hidden');
						}
						show_notify(data.message, true);
					}else{
						show_notify(data.message, false);
					}
					$("#ajax-loader").hide();
				}
			});
		});
	});

	function get_referance_no(ref_val){
		//alert(ref_val);
		var party_id = $('#party_name').val()
		if(party_id == ''){
			/*show_notify('Please select Party !', false);
			return false;*/
		}
		if(ref_val == 'sales_order'){
			initAjaxSelect2_by_party($("#referance_no"),"<?=base_url('app/sales_order_no_select2_source/')?>",party_id);
		}else if(ref_val == 'quotation'){
			initAjaxSelect2_by_party($("#referance_no"),"<?=base_url('app/quotation_no_select2_source/')?>",party_id);
		} else if(ref_val == 'complain'){
			initAjaxSelect2_by_party($("#referance_no"),"<?=base_url('app/complain_no_select2_source/')?>",party_id);
		}
	}

	function feedLeedData(id){
		$("#ajax-loader").show();
		$.ajax({
			url: '<?php echo BASE_URL; ?>sales/get_lead',
			type: "POST",
			data: {id: id},
			success: function(data)
			{
				$("#ajax-loader").hide();
				data = JSON.parse(data);
				//console.log(data);
				if (data.status == 1)
				{
					setSelect2Value($("#party_name"),'<?=base_url()?>app/set_party_select2_val_by_id/'+data.party.party_id);                    
					party_details(data.party.party_id); 
				}
			},
			error: function(msg) {
				$("#ajax-loader").hide();
			} 
		});
	}

	function initAjaxSelect2_by_party($selector,$source_url,party_id){
		$selector.select2({
			placeholder: " --Select-- ",
			allowClear: true,
			width:"100%",
			ajax: {
				url: $source_url,
				dataType: 'json',
				delay: 250,
				async: false,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page,
						party_id:party_id
					};
				},
				processResults: function (data,params) {
					params.page = params.page || 1;
					return {
						results: data.results,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			}
		});
	}
	
	function initAjaxSelect2_letters($selector, $source_url, letter_for) {
		$selector.select2({
			placeholder: " --Select-- ",
			allowClear: true,
			width: "100%",
			ajax: {
				url: $source_url,
				dataType: 'json',
				delay: 250,
				async: false,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page,
						letter_for: letter_for
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;
					return {
						results: data.results,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			}
		});
	}

</script>