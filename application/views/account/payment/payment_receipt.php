<?php
$column_cnt = 9;
?>
<html>
	<head>
		<title>JayKhodiyar</title>
		<style>
			@media print {
                                table{
					border-spacing: 0;
				}
				.text-center {
					text-align: center !important;
				}
				.text-right {
					text-align: right !important;
				}
				.text-left {
					text-align: left !important;
				}
				.text-underline {
					text-decoration: underline;
				}
				.section-title{
					font-size: 20px;
					font-weight: 900;
					margin: 15px 10px;
				}
				.no-margin{
					margin: 0;
				}
				table.table-item-detail > thead > tr > th:last-child{
					border-right: solid 0.5px #000000;
				}
				table.table-item-detail > thead > tr > th:first-child{
					border-left: solid 0.5px #000000;
				}
				table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
					border-right: solid 0.5px #000000;
				}
				table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
					border-left: solid 0.5px #000000;
				}
				table.table-item-detail > thead > tr > th{
					border-top: solid 0.5px #000000;
					border-bottom: solid 0.5px #000000;
				}
				table.table-item-detail > tbody > tr > th{
					border-top: solid 0.5px #000000;
					border-bottom: solid 0.5px #000000;
				}
			}
			table.table-item-detail > thead > tr > th:last-child{
				border-right: solid 0.5px #000000;
			}
			table.table-item-detail > thead > tr > th:first-child{
				border-left: solid 0.5px #000000;
			}
			table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
				border-right: solid 0.5px #000000;
			}
			table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
				border-left: solid 0.5px #000000;
			}
			table.table-item-detail > thead > tr > th{
				border-top: solid 0.5px #000000;
				border-bottom: solid 0.5px #000000;
			}
			table.table-item-detail > tbody > tr > th{
				border-top: solid 0.5px #000000;
				border-bottom: solid 0.5px #000000;
			}
			table{
				border-spacing: 0;
			}
			table tr td {
				padding-left: 5px;
				padding-right: 5px;
			}
			.text-center {
				text-align: center !important;
			}
			.text-right {
				text-align: right !important;
			}
			.text-left {
				text-align: left !important;
			}
			.text-underline {
				text-decoration: underline;
			}
			.section-title{
				font-size: 20px;
				font-weight: 900;
				margin: 15px 10px;
			}
			.no-margin{
				margin: 0;
			}
			.content {
				padding: 10px;
			}
                        .no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
                        .no-border {
                                border: 0;
                        }
                        .border-right {
                            border-right: 1px solid black;
                            border-left: 0;
                            border-top: 0;
                            border-bottom: 0;
                        }
			
                </style>
	</head>
	<body>
                <table class="table-company-detail" border="1" style="font-size: 13px; width: 100%;">
			<tr>
                            <td class="no-border-top no-border-left no-border-right" colspan="<?=$column_cnt;?>" align="center"><h1 style="text-align: center;">Payment Receipt</h1></td>
			</tr>	
			<tr>
				<td colspan="1" class="text-left border-right" width="150px">Receipt No. :</td>
				<td colspan="6" class="text-left no-border" width="350px"><strong><?=isset($payment_data->sales_order_no) ? $this->applib->get_payment_recipt_no($payment_data->receipt_no) : ''; ?></strong></td>
				<td colspan="1" class="text-left no-border-bottom no-border-top">Date :</td>
				<td colspan="1" class="text-left no-border"><strong><?=date('d/m/Y', strtotime($payment_data->payment_date))?></strong></td>
			</tr>
                        <?php if (isset($payment_data->complain_no_year)) { ?>
                        <tr>
				<td colspan="1" class="text-left no-border-left no-border-bottom">Complain No. :</td>
				<td colspan="2" class="text-left no-border-left no-border-bottom no-border-right"><strong><?=isset($payment_data->complain_no_year) ? $payment_data->complain_no_year : ''; ?></strong></td>
                                <td colspan="6" class="text-left no-border-bottom no-border-right"><strong><?=isset($payment_data->complain_date) ? date('d/m/Y',strtotime($payment_data->complain_date)) : ''; ?></strong></td>
			</tr>
                        <?php } else { ?>
			<tr>
				<td colspan="1" class="text-left no-border-left no-border-bottom">Quotation No. :</td>
				<td colspan="3" class="text-left no-border-left no-border-bottom no-border-right"><strong><?=(isset($payment_data->sales_order_quotation_no) ? $payment_data->sales_order_quotation_no : (isset($payment_data->referance_no) ? $payment_data->referance_no : ''))?></strong></td>
				<td colspan="1" class="text-left no-border-bottom no-border-right">Order No.:</td>
				<td colspan="3" class="text-left no-border-bottom"><strong><?=isset($payment_data->sales_order_no) ? $this->applib->get_sales_order_no($payment_data->sales_order_no) : ''; ?></strong></td>
				<td colspan="1" class="text-left no-border-bottom no-border-right no-border-left"><strong><?=isset($payment_data->sales_order_date) ? date('d/m/Y',strtotime($payment_data->sales_order_date)) : ''; ?></strong></td>
			</tr>
                        <?php } ?>
			<tr>
                            <td colspan="1" class="no-border-left no-border-bottom ">Received from:</td>
                            <td colspan="4" class="no-border-bottom no-border-left no-border-right"><strong><?=isset($payment_data->party_name) ? $payment_data->party_name : ''; ?></strong></td>
				<td colspan="1" class="text-left no-border-bottom no-border-right">City :</td>
				<td colspan="1" class="text-left no-border-bottom no-border-right"><strong><?=isset($payment_data->city) ? $payment_data->city : ''; ?></strong></td>
				<td colspan="1" class="text-left no-border-bottom ">State :</td>
				<td colspan="1" class="text-left no-border-bottom no-border-right no-border-left"><strong><?=isset($payment_data->state) ? $payment_data->state : ''; ?></strong></td>
			</tr>
			<tr>
                            <td colspan="1" class="no-border-left no-border-bottom ">Bank :</td>
                            <td colspan="3" class="no-border-left no-border-right no-border-bottom"><strong><?=isset($payment_data->payment_bank) ? $payment_data->payment_bank : ''; ?></strong></td>
                            <td colspan="1" width="75px;" class="no-border-bottom no-border-right">By :</td>
                            <td colspan="1" class="no-border-right no-border-bottom"><strong><?=isset($payment_data->payment_by) ? $payment_data->payment_by : ''; ?></strong></td>
                            <td colspan="1" class="no-border-bottom no-border-right">No. :</td>
                            <td colspan="1" class="no-border-bottom"><strong><?=isset($payment_data->payment_by_no) ? $payment_data->payment_by_no : ''; ?></strong></td>
                            <td colspan="1" class="no-border-left no-border-right no-border-bottom"><strong><?=isset($payment_data->receipt_date_2) ? date('d/m/Y',strtotime($payment_data->receipt_date_2)) : ''; ?></strong></td>
			</tr>
			<tr>
                            <td colspan="1" class="no-border-left no-border-bottom "><strong>In Words :</strong></td>
                            <td colspan="6" class="no-border-left no-border-right no-border-bottom"><?=money_to_word(isset($payment_data->amount) ? $payment_data->amount : '')?></td>
				<td colspan="1" class="text-right no-border-bottom">INR :</td>
                                <td colspan="1" class="no-border-left no-border-right no-border-bottom"><strong><center><?=money_format('%!i', isset($payment_data->amount) ? $payment_data->amount : '')?></center></strong></td>
			</tr>
			<tr>
                            <td colspan="9" class="no-border-left no-border-right"><strong>Declaration : </strong><?=isset($payment_declaration) ? $payment_declaration : ''; ?><br/><br/><br/><br/><br/></td>
			</tr>
			<tr>
				<td valign="top" class="text-bold no-border" colspan="3"><center>
					<strong><?=isset($payment_data->reciver_name) ? $payment_data->reciver_name : ''; ?></strong><br/><img height="80px" width="80px" src="<?=image_url('staff/signature/'.$payment_data->reciver_signature);?>"></center></td>
				<td valign="top" class="text-bold no-border-top no-border-bottom" colspan="3"><center>
			    		<strong><?=isset($payment_data->approver_name) ? $payment_data->approver_name : '';?></strong><br/><img height="80px" width="80px" src="<?=image_url('staff/signature/'.$payment_data->approver_signature);?>"></center></td>
                                <td valign="top" class="text-bold no-border" colspan="3"><center><strong>For, Jay Khodiyar Machine Tools</strong><br />
                                            <img src="<?php echo BASE_URL;?>/resource/image/jk_symbol.jpg" style="margin-bottom:5px"></center></td>
			</tr>
                        <tr>
                            <td valign="bottom" class="text-bold no-border" colspan="3"><center>Receiver Signature</center></td>
                            <td valign="bottom" class="text-bold no-border-top no-border-bottom" colspan="3"><center>Approved Signature</center></td>
                            <td valign="bottom" class="text-bold no-border" colspan="3"><center><strong>Authorized Signature</strong><br />
                        </tr>
                </table>
		<?php /*<div style="background:url('<?= base_url(); ?>resource/image/Head_footer.jpg') no-repeat bottom center; background-size:100%; font-size:10px; color:#333; clear:both; height:230px;"></div>*/ ?>
            </body>
</html>
