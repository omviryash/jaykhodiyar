<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Billing Terms Log
				</h1>
			</section>
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Log List</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!---content start --->
								<div class="table-responsive">
									<table class="table table-striped table-bordered billing_terms-table">
										<thead>
											<tr>
												<?php 
												foreach($fields as $field)
												{
													echo "<th>".$field["field"]."</th>";
												}
?>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<!---content end--->
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
				<!-- END ALERTS AND CALLOUTS -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<script type="text/javascript">
			var table;
			$(document).ready(function(){
				table = $(".billing_terms-table").DataTable({
					"serverSide": true,
					"ordering": true,
					"searching": false,
					"aaSorting": [[1, "asc"]],
					"ajax": {
						"url": "<?php echo base_url("logs/billing_terms_datatable")?>",
						"type": "POST"
					},
					"scrollY": "350px",
					"scroller": {
						"loadingIndicator": true
					},
					"columnDefs": [
						{"targets": 0, "orderable": false },
						//{ className: "text-right", "targets": [6,7,8,9,10,11] },
					],
					"sScrollX": "100%",
					"sScrollXInner": "110%"
				});
				$(document).on("click",".delete_button",function(){
					var value = confirm("Are you sure delete this Log?");
					var tr = $(this).closest("tr");
					if(value){
						$.ajax({
							url: $(this).data("href"),
							type: "POST",
							data: "id_name=log_id&table_name=billing_terms_log",
							success: function(data){
								table.draw();
								show_notify("Billing_terms Log Deleted Successfully!",true);
							}
						});
					}
				});
			});
		</script>
		