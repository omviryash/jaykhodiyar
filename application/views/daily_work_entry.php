<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" action="<?= base_url('master/add_daily_work_entry') ?>" method="post" id="add_daily_work_entry">
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Daily Work Entry</small>
			<span class="pull-right">
				<?php if($this->applib->have_access_role(GENERAL_DAILY_WORK_ENTRY_MENU_ID,"edit" )) { ?>
				<button type="submit" class="btn btn-info btn-xs">Save</button>
				<button type="reset" class="btn btn-info btn-xs">Reset</button>
				<?php } ?>
			</span>
		</h1>
    </section>
    <!-- Main content -->
    <section class="content">
		<?php if ($this->session->flashdata('success') == true) { ?>
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
			</div>
		<?php } ?>
		<input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>">
		<input type="hidden" name="id" id="id" value="<?php echo $recordid;?>">
			<div class="row">
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-body">
							<div class="col-md-12">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 input-sm">User</label>
									<div class="col-sm-9">
										<?php if($this->applib->have_access_user_dropdown_roles(USER_DROPDOWN_HEADER_MODULE_ID,"view")){ ?>
											<select class="form-control select2" name="select_user" id="select_user" style="width:280px; height: 25px; padding: 0px 5px;" >
												<option value="">- Select User - </option>
												<?php foreach($users as $user):?>
													<option <?php echo $userid == $user->staff_id ? 'selected="selected"':''; ?> value="<?php echo $user->staff_id; ?>"><?php echo $user->name; ?></option>
												<?php endforeach;?>
											</select>
										<?php } else { ?>
											<label class="col-sm-3 input-sm"><?php echo $username;?></label>
											<input type="hidden" class="form-control input-sm" id="username" name="username" value="<?php echo $username;?>" readonly="readonly">
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-sm-3 input-sm">Date</label>
									<div class="col-sm-3">
										<input type="text" class="form-control input-sm" name="entry_date" value="<?= date('d-m-Y'); ?>" id="entry_date">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 input-sm">Activity Details</label>
									<div class="col-sm-9">
										<textarea class="form-control" rows="10" name="activity_details" id="activity_details" style="resize:none;"><?php echo $activity_details;?></textarea>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<!-- /.box -->
					<?php if($this->applib->have_access_role(GENERAL_DAILY_WORK_ENTRY_MENU_ID,"view" )) { ?>
					<div class="box box-info">
						<div class="box-body">
							<div class="col-md-12">
								<div style="float:right;"><span class="badge" id="mail_sent"></span> Mail Sent (<span id="selected_date"></span>)</div>
								<table class="table" id="table_follow_history">
									<thead>
										<tr>
											<th>Sr. No.</th>
											<th>Type</th>
											<th>Party</th>
											<th>Phone</th>
											<th>Email</th>
											<th>Follow up history</th>
										</tr>
									</thead>
								</table>
							</div>	
						</div>
					</div>
					<!-- /.box -->
					<?php } ?>
				</div>
			</div>

	</section>
</form>
</div>
<script>
	$(document).ready(function(){

		var table_follow_history = $("#table_follow_history").DataTable({
			"serverSide": true,
			"ordering": false,
			"searching": false,
			"aaSorting": [[0, 'asc']],
			"ajax": {
				"url": "<?php echo site_url('master/follow_history_datatable')?>",
				"type": "POST",
				"data": function(d){
					d.select_user = $("#select_user").val();
					d.entry_date = $("#entry_date").val();
				}
			},
			"scrollY": 200,
			"scroller": {
				"loadingIndicator": true
			}
		});

		$("#entry_date").datepicker({
			format: 'dd-mm-yyyy',
			todayBtn: "linked",
			todayHighlight: true,
			autoclose: true
		}).on('changeDate', function(en) {
			var entry_date = $("#entry_date").val();
			var select_user = $("#select_user").val();
			$.ajax({
				url: "<?=base_url('master/get_daily_work_entry/');?>",
				type: "POST",
				data: {entry_date : entry_date, select_user : select_user},
				dataType: 'json',
				success: function (data) {
					if(data.success == true) {
						$('#activity_details').val(data.activity_details);
					}
				}
			});
			table_follow_history.draw();
			show_usersent_mailcount($("#entry_date").val());
		});

		$(document).on("change","#select_user",function(){
			var entry_date = $("#entry_date").val();
			var select_user = $("#select_user").val();
			$.ajax({
				url: "<?=base_url('master/get_daily_work_entry/');?>",
				type: "POST",
				data: {entry_date : entry_date, select_user : select_user},
				dataType: 'json',
				success: function (data) {
					if(data.success == true) {
						$('#activity_details').val(data.activity_details);
						$('#userid').val(select_user);
					}
				}
			});
			table_follow_history.draw();
			show_usersent_mailcount($("#entry_date").val());
		});
		
		$(document).on("submit","#add_daily_work_entry",function(e){
			e.preventDefault();
			var form_data = new FormData(this);
			$.ajax({
				url: "<?=base_url('master/add_daily_work_entry/');?>",
				type: "POST",
				data: form_data,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (data) {
					if(data.success == true) {
						show_notify('Work detail successfully saved',true);
					}
				}
			});
		});

		show_usersent_mailcount('<?php echo date('d-m-Y'); ?>');
		function show_usersent_mailcount(date) {
			$.ajax({
				url: "<?=base_url('master/get_usersent_mailcount/');?>",
				type: "POST",
				data: {date:date},
				dataType: 'json',
				success: function (data) {
					$('#mail_sent').html(data.count);
					$('#selected_date').html(date);
				}
			});
		}
	});
</script>

