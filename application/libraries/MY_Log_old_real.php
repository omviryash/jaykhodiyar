<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'third_party/ci_log4php/Logger.php';
class MY_Log extends CI_Log {
	
	protected $logger;
	protected $initialized = false;
	
	public function __construct() {
		parent::__construct();

		//$this->_levels	= array('ERROR' => '1', 'INFO' => '2',  'DEBUG' => '3', 'ALL' => '4');
		if ($this->initialized === false) {
			$this->initialized = true;
			$config_file = APPPATH . 'config/log4php.properties';
			if ( defined('ENVIRONMENT') && file_exists( APPPATH . 'config/' . ENVIRONMENT . '/log4php.properties' ) ) {
				$config_file = APPPATH . 'config/' . ENVIRONMENT . '/log4php.properties';
			}
			Logger::configure($config_file);
			$this->logger = Logger::getRootLogger();
		}
	}
	
	/*public function write_log($level = 'error', $msg, $php_error = FALSE) {
		if ($this->_enabled === FALSE) {
			return FALSE;
		}
		
		$level = strtoupper($level);

		if ( ! isset($this->_levels[$level]) OR ($this->_levels[$level] > $this->_threshold) ) {
			return FALSE;
		}
		
		switch ($level) {
			case 'ERROR':
				$this->logger->error($msg);
				break;
			case 'INFO':
				$this->logger->info($msg);
				break;
			case 'DEBUG':
				$this->logger->debug($msg);
				break;
			default:
				$this->logger->debug($msg);
				break;
		}
		
		return TRUE;
	}*/

	public function write_log($level = 'error', $msg, $php_error = FALSE)
    {
        if ($this->_enabled === FALSE)
        {
            return FALSE;
        }

        $level = strtoupper($level);

        if ( ! isset($this->_levels[$level]))
        {
            return FALSE;
        }
        if (($this->_levels[$level] > $this->_threshold) OR (is_array($this->_threshold) && !in_array($this->_levels[$level], $this->_threshold)))
        {
            return FALSE;
        }



        $filepath = $this->_log_path.'log-'.date('Y-m-d').'.php';
        $message  = '';

        if ( ! file_exists($filepath))
        {
            $message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
        }

        if ( ! $fp = @fopen($filepath, FOPEN_WRITE_CREATE))
        {
            return FALSE;
        }

        $message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.date($this->_date_fmt). ' --> '.$msg."\n";

        flock($fp, LOCK_EX);
        fwrite($fp, $message);
        flock($fp, LOCK_UN);
        fclose($fp);

        @chmod($filepath, FILE_WRITE_MODE);
        return TRUE;
    }
}
