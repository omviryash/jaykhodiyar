<?php

/**
 * Class Log_datatable
 */
class Log_datatable
{
	var $select = '';
	var $table = '';
	var $joins = array();
	var $wheres = array();
	var $column_search = array();
	var $column_order = array();
	var $order = array();
	var $likes = array();
	var $where_string = array();
	var $custom_where = array();
	var $in_not_in_where = array();
	var $group_by = 0;


	/**
     * Log_datatable constructor.
     * @param array $config
     */
	function __construct(array $config = array())
	{
		$this->CI =& get_instance();
		$this->db_log = $this->CI->load->database('log', TRUE);
		if (count($config) > 0){
			$this->initialize($config);
		}
	}
	
	/**
     * Initialize preferences
     *
     * @param	array
     * @return	CI_Log_datatable
     */
	public function initialize($config = array())
	{
		foreach ($config as $key => $val)
		{
			if (isset($this->$key))
			{
				$this->$key = $val;
			}
		}
		return $this;
	}

	/**
     *
     */
	private function _get_datatables_query()
	{
		if($this->select != ''){
			$this->db_log->select($this->select);
		}

		$this->db_log->from($this->table);

		if(!empty($this->joins)){
			foreach($this->joins as $join){
				if(isset($join['join_table']) &&  isset($join['join_by']) && $join['join_table'] != '' && $join['join_by'] != ''){
					$join_table = $join['join_table'];
					$join_by = $join['join_by'];
					$join_type = isset($join['join_type'])?$join['join_type']:'inner';
					$this->db_log->join("$join_table","$join_by","$join_type");
				}
			}
		}

		if(!empty($this->custom_where)){
			$this->db_log->where($this->custom_where);
		}

		if(!empty($this->in_not_in_where)){
			$this->db_log->where($this->in_not_in_where, NULL, FALSE);
		}

		if(!empty($this->wheres)){
			foreach($this->wheres as $where){
				if(isset($where['column_name']) &&  isset($where['column_value']) && $where['column_name'] != '' && $where['column_value'] != ''){
					$column_name = $where['column_name'];
					$column_value = $where['column_value'];
					$this->db_log->where($column_name,$column_value);
				}
			}
		}

		if(!empty($this->where_string)){
			$this->db_log->or_where($this->where_string);
		}

		if(!empty($this->likes)){
			foreach($this->likes as $like){
				$this->db_log->like(key($like), $like[key($like)]);
			}
		}

		$i = 0;
		foreach ($this->column_search as $item) // loop column
		{
			if(isset($_POST['search']['value'])) // if datatable send POST for search
			{
				if($i===0)
				{
					$this->db_log->group_start();
					$this->db_log->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db_log->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i)
					$this->db_log->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			if(isset($this->column_order[$_POST['order']['0']['column']])){
				$this->db_log->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			if(isset($order[key($order)])){
				$this->db_log->order_by(key($order), $order[key($order)]);
			}
		}

		if(!empty($this->group_by)) {
			$this->db_log->group_by($this->group_by);
		}

	}

	/**
     * @return mixed
     */
	function get_datatables()
	{
		$this->_get_datatables_query();
		if(isset($_POST['length']) && $_POST['length'] != -1)
			$this->db_log->limit($_POST['length'],$_POST['start']);
		$query = $this->db_log->get();
		//echo $this->db_log->last_query();
		return $query->result();
	}

	/**
     * @return mixed
     */
	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_log->get();
		return $query->num_rows();
	}

	/**
     * @return mixed
     */
	public function count_all()
	{
		$this->db_log->from($this->table);

		if(!empty($this->joins)){
			foreach($this->joins as $join){
				if(isset($join['join_table']) &&  isset($join['join_by']) && $join['join_table'] != '' && $join['join_by'] != ''){
					$join_table = $join['join_table'];
					$join_by = $join['join_by'];
					$join_type = isset($join['join_type'])?$join['join_type']:'inner';
					$this->db_log->join("$join_table","$join_by","$join_type");
				}
			}
		}

		if(!empty($this->custom_where)){
			$this->db_log->where($this->custom_where);
		}

		if(!empty($this->in_not_in_where)){
			$this->db_log->where($this->in_not_in_where, NULL, FALSE);
		}

		if(!empty($this->wheres)){
			foreach($this->wheres as $where){
				if(isset($where['column_name']) &&  isset($where['column_value']) && $where['column_name'] != '' && $where['column_value'] != ''){
					$column_name = $where['column_name'];
					$column_value = $where['column_value'];
					$this->db_log->where($column_name,$column_value);
				}
			}
		}

		if(!empty($this->where_string)){
			$this->db_log->or_where($this->where_string);
		}



		if(!empty($this->group_by)) {
			$this->db_log->group_by($this->group_by);
		}

		return $this->db_log->count_all_results();
	}
}
