<?php

/**
 * Class AppLib
 * &@property CI_Controller $ci
 */

use Twilio\Rest\Client;

class AppLib
{
    private $email_address = SMTP_EMAIL_ADDRESS;
    private $email_password = SMTP_EMAIL_PASSWORD;
    private $email_server = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';
    private $per_page = 10;

    function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();
        $this->ci->load->library('session');
        //$data = $this->ci->session->userdata('is_logged_in');
        //$this->email_address = isset($data['mailbox_email']) ? trim($data['mailbox_email']):'';
        //$this->email_password = isset($data['mailbox_password']) ? trim($data['mailbox_password']):'';
        
        $this->staff_id = $this->ci->session->userdata('is_logged_in')['staff_id'];
        $this->now_time = date('Y-m-d H:i:s');
    }


    /**
     * @param $module
     * @param $role
     * @return int
     */
    function have_access_role($module, $role)
    {
        $status = 0;
        $user_roles = $this->ci->session->userdata('user_roles');
        //echo '<pre>';print_r($user_roles);die();
        if(isset($user_roles[$module]) && in_array($role, $user_roles[$module]))
        {
            $status = 1;
        }
        return $status;
        //return 1;
    }
    function have_access_user_dropdown_roles($module, $role)
    {
        $status = 0;
        if ($this->ci->session->userdata('logged_in_as_user_roles')) {
			$user_roles = $this->ci->session->userdata('logged_in_as_user_roles');
			if(isset($user_roles[$module]) && in_array($role, $user_roles[$module]))
			{
				$status = 1;
			}
		}
        return $status;
    }
    function have_access_current_user_rights($module, $role)
    {
        $status = 0;
        if ($this->ci->session->userdata('user_roles')) {
			$user_roles = $this->ci->session->userdata('user_roles');
			if(isset($user_roles[$module]) && in_array($role, $user_roles[$module]))
			{
				$status = 1;
			}
		}
        return $status;
    }

    function checkIsAdmin()
    {
        $status = 0;
        $logged_in = $this->ci->session->userdata('is_logged_in');
        $id = $logged_in['staff_id'];
        $this->ci->db->select('*');
        $this->ci->db->from('staff');
        $this->ci->db->where('staff_id',$id);
        $query = $this->ci->db->get();
        //echo $this->ci->db->last_query();exit;
        if($query->num_rows() > 0){
            $data = $query->row();
            if($data->user_type == 'Administrator')
				$status = 1;
        }
        return $status;
    }

    /**
     * @param $tbl_name
     * @param $condition
     * @return mixed
     */
    function counts_rows($tbl_name, $condition = array(1 => 1))
    {
        $this->ci->db->select('1', true);
        $this->ci->db->from($tbl_name);
        $this->ci->db->where($condition);
        $query = $this->ci->db->get();
        return $query->num_rows();
    }

    /**
     * @return mixed
     */
    function getCurrCompetition()
    {
        $this->ci->db->select('competition_id');
        $this->ci->db->from('competitions');
        $this->ci->db->where('start_datetime <= ', date('Y-m-d H:i:s'));
        $this->ci->db->where('end_datetime >= ', date('Y-m-d H:i:s'));
        $this->ci->db->order_by('competition_id', 'DESC');
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->competition_id;
        } else {
            $this->ci->session->set_flashdata('success', false);
            $this->ci->session->set_flashdata('message', 'No any competition currently runnig.');
            redirect('competition/');
        }
    }


    /**
     * @param $competition_id
     * @return mixed
     */
    function getCompetitionDetail($competition_id)
    {
        $this->ci->db->select('*');
        $this->ci->db->from('competitions');
        $this->ci->db->where('competition_id', $competition_id);
        $query = $this->ci->db->get();
        return $query->row();
    }

    /**
     * @param $competition_id
     * @return string
     */
    function getCompetitionStatus($competition_id)
    {
        $this->ci->db->select('start_datetime,end_datetime');
        $this->ci->db->from('competitions');
        $this->ci->db->where('competition_id', $competition_id);
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        if ($query->row()->start_datetime < date('Y-m-d H:i:s') AND $query->row()->end_datetime < date('Y-m-d H:i:s')) {
            return 'completed';
        } elseif ($query->row()->start_datetime > date('Y-m-d H:i:s') AND $query->row()->end_datetime > date('Y-m-d H:i:s')) {
            return 'pending';
        } else {
            return 'running';
        }
    }

    function getActiveCompetition()
    {
        $this->ci->db->select('competition_id');
        $this->ci->db->from('competitions');
        $this->ci->db->where('is_active', 1);
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        return $query->row()->competition_id;
    }

    function getWeeklyHoliday()
    {
        $this->ci->db->limit('1');
        $query = $this->ci->db->get('weekly_holiday');
        return $query->row()->day;
    }

    function getYearlyHoliday()
    {
        $this->ci->db->order_by('date');
        $query = $this->ci->db->get('yearly_leaves');
        $YearlyHolidayDates = array();
        foreach ($query->result() as $Row) {
            $YearlyHolidayDates[] = date('m/d/Y', strtotime(str_replace('-', '/', $Row->date)));
        }
        $WeekendHolidayDates = $this->getWeekendHolidayDates();
        foreach ($WeekendHolidayDates as $Row) {
            $YearlyHolidayDates[] = $Row;
        }
        return $YearlyHolidayDates;
    }

    function getWeekendHolidayDates()
    {
        $WeeklyHoliday = $this->getWeeklyHoliday();
        $WeekendHolidayDates = array();
        $curr_year = date('Y');
        $start_year = $curr_year - 1;
        $end_year = $curr_year + 2;
        $DatePeriod = new DatePeriod(
            new DateTime("first $WeeklyHoliday of $start_year-01"),
            DateInterval::createFromDateString("next $WeeklyHoliday"),
            new DateTime("last day of $end_year-12")
        );
        foreach ($DatePeriod as $day) {
            if ($day->format("Ymd") > date('Ymd')) {
                $WeekendHolidayDates[] = $day->format("m/d/Y");
            }
        }
        return $WeekendHolidayDates;

    }

    /***
     * @param $file_url
     * @return bool
     */
    function unlink_file($file_url)
    {
        if (file_exists($file_url)) {
            if (unlink($file_url)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $image
     * @param $upload_url
     * @param bool $return_with_url
     * @return bool|string
     */
    function upload_image($image, $upload_url, $return_with_url = true)
    {
        $config['upload_path'] = $upload_url;
		if($image == 'signature'){
			$config['allowed_types'] = 'jpg|jpeg';	
		}else{
			$config['allowed_types'] = '*';	
		}
        $config['encrypt_name'] = TRUE;
        $this->ci->load->library('upload', $config);
        $this->ci->upload->initialize($config);
        if (!$this->ci->upload->do_upload($image)) {
			$error = array('error' => $this->ci->upload->display_errors());
			return $error;
        }
        $data = $this->ci->upload->data();
        $file_name = $data ['file_name'];
        if ($return_with_url) {
            return $upload_url . $file_name;
        } else {
            return $file_name;
        }

    }

    /**
     * @param string $date
     * @return bool|string
     * DD/MM/YYYY To YYYY-MM-DD
     */
    function to_sql_date($date = '')
    {
        return date('Y-m-d', strtotime($date));
    }

    /**
     * @param string $date
     * @return bool|string
     * YYYY-MM-DD To DD/MM/YYYY
     */
    function to_simple_date($date = '')
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $date)));
    }

    /**
     * @param $letter_code
     * @return mixed
     */
    function getLetterTemplate($letter_code)
    {
        $this->ci->db->select('letter_template');
        $this->ci->db->from('letters');
        $this->ci->db->where('letter_id', $letter_code);
        $this->ci->db->limit(1);
        $query = $this->ci->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->letter_template;
        } else {
            return false;
        }
    }
    
    function getTermsandConditions($module)
    {
        $this->ci->db->select('detail');
        $this->ci->db->from('terms_and_conditions');
        $this->ci->db->where('module', $module);
        $this->ci->db->limit(1);
        $query = $this->ci->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->detail;
        } else {
            return false;
        }
    }

    function getEmployee()
    {
        $this->ci->db->select('employee_id,name');
        $this->ci->db->from('employees');
        $query = $this->ci->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $response[] = $row;
            }
        }
        return $response;
    }

    function getStaff()
    {
        $this->ci->db->select('staff_id,name');
        $this->ci->db->from('staff');
        $this->ci->db->where('active !=',0);
        $this->ci->db->order_by('name','ASC');
        $query = $this->ci->db->get();
        $response = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $response[] = $row;
            }
        }
        return $response;
    }

    function check_internet_conn()
    {
        return checkdnsrr('php.net') ? true : false;
    }

    function count_inbox_unread_mail_v1()
    {
        $mb = imap_open($this->email_server . "INBOX", $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
        $mailbox_info = imap_mailboxmsginfo($mb);
        return $mailbox_info->Unread;
    }

    /**
     * @return mixed
     */
    function count_inbox_unread_mail()
    {
        $logged_in = $this->ci->session->userdata("is_logged_in");
        $this->ci->db->select('mail_id');
        $this->ci->db->from('mail_system');
        $this->ci->db->where('staff_id',$logged_in['staff_id']);
        $this->ci->db->where('is_unread',1);
        $query = $this->ci->db->get();
        return $query->num_rows();
    }

    function fetch_unread_mails()
    {
        $mails_data = array();
        $logged_in = $this->ci->session->userdata("is_logged_in");
        $this->ci->db->select('from_address,to_address,subject,is_attachment,received_at,mail_id');
        $this->ci->db->from('mail_system');
        $this->ci->db->where('staff_id',$logged_in['staff_id']);
        $this->ci->db->where('is_unread',1);
        $this->ci->db->order_by('mail_id','desc');
        $query = $this->ci->db->get();
        if($query->num_rows() > 0){
            foreach ($query->result() as $key=>$mail_row) {
                if($mail_row->from_address != $logged_in['mailbox_email']){
                    $mails_data[$key]['username'] = $mail_row->from_address;
                }else{
                    $mails_data[$key]['username'] = $mail_row->to_address;
                }
                $mails_data[$key]['subject'] = $this->limit_character($mail_row->subject,25);
                $mails_data[$key]['attachment_status'] = $mail_row->is_attachment;;
                $mails_data[$key]['received_at'] = $this->timeAgo($mail_row->received_at);
                $mails_data[$key]['mail_id'] = $mail_row->mail_id;
            }
        }
        return $mails_data;
    }

    function get_staff_mail_folders(){
        $staff_id = $this->ci->session->userdata("is_logged_in")['staff_id'];
        $this->ci->db->select('folder');
        $this->ci->db->from('mail_system_folder');
        $this->ci->db->where('staff_id',$staff_id);
        $query  = $this->ci->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return array();
        }
    }

    function fetch_unread_mails_v1()
    {
        $mails_data = array();
        if ($this->check_internet_conn()) {
            $mb = imap_open($this->email_server."INBOX", $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!");
            $message_count = imap_num_msg($mb);
            if ($message_count > 0) {
                $sort_results = imap_sort($mb,SORTDATE, 1);
                $search_results = imap_search($mb,'UNSEEN');
                $message_nos = $this->order_search($search_results, $sort_results);
                foreach ($message_nos as $key => $message_no) {
                    $email_headers = imap_headerinfo($mb, $message_no);
                    if ($email_headers->fromaddress != $this->email_address) {
                        if (isset($email_headers->from[0]->personal)) {
                            $mails_data[$key]['username'] = $email_headers->from[0]->personal;
                        } else {
                            $mails_data[$key]['username'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
                        }
                    } else {
                        if (isset($email_headers->to[0]->personal)) {
                            $mails_data[$key]['username'] = $email_headers->to[0]->personal;
                        } else {
                            $mails_data[$key]['username'] = $email_headers->to[0]->mailbox . '@' . $email_headers->from[0]->host;
                        }
                    }
                    $mails_data[$key]['subject'] = $this->limit_character($email_headers->subject, 15);
                    $mails_data[$key]['attachment_status'] = $this->get_mail_attachment_status($mb, $message_no);;
                    $mails_data[$key]['received_at'] = $this->timeAgo($email_headers->date);
                    $mails_data[$key]['message_no'] = $message_no;
                }
            }
        }
        return $mails_data;
    }

    /**
     * @param $mb
     * @param $message_no
     * @return int
     */
    function get_mail_attachment_status($mb, $message_no)
    {
        $attachments = 0;
        $structure = imap_fetchstructure($mb, $message_no);
        if (isset($structure->parts) && count($structure->parts)) {
            foreach ($structure->parts as $key => $parts) {
                if (isset($parts->disposition) && $parts->disposition == "ATTACHMENT") {
                    $attachments = 1;
                    break;
                }
            }
        }
        return $attachments;
    }

    /**
     * @param $SearchResults
     * @param $SortResults
     * @return array
     */
    function order_search($SearchResults, $SortResults)
    {
        return array_values(array_intersect($SearchResults, $SortResults));
    }

    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        } //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        } //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        } //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        } //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        } //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        } //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    function limit_character($string, $character_limit = 30)
    {
        if (strlen($string) > $character_limit) {
            return substr($string, 0, $character_limit) . '...';
        } else {
            return $string;
        }
    }

    function is_internet_connected()
    {
        $connected = fopen("http://www.google.com:80/", "r");
        if ($connected) {
            return true;
        } else {
            return false;
        }

    }

    function employee_month_leaves($employee_id, $month, $year)
    {
        $this->ci->db->select('leave_id,from_date,to_date');
        $this->ci->db->from('real_leave');
        $this->ci->db->where('employee_id', $employee_id);
        $this->ci->db->group_start();
        $this->ci->db->where('CONCAT(YEAR(from_date),MONTH(from_date))', $year . $month);

        $this->ci->db->or_where('CONCAT(YEAR(to_date),MONTH(to_date))', $year . $month);

        $this->ci->db->or_group_start();
        $this->ci->db->where('CONCAT(YEAR(from_date),MONTH(from_date)) <', $year . $month);
        $this->ci->db->where('CONCAT(YEAR(to_date),MONTH(to_date)) >', $year . $month);
        $this->ci->db->group_end();

        $this->ci->db->group_end();

        $query = $this->ci->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    /**
     * @return bool
     */
    function save_unread_mails()
    {
        if ($this->check_internet_conn()) {
            $mailbox = $this->email_server . "INBOX";
            $mb = imap_open($mailbox, $this->email_address, $this->email_password) or die(imap_last_error() . "<br>Connection Faliure!3");
            $mail_status = imap_status($mb, $this->email_server . "INBOX", SA_ALL) or die(imap_last_error() . "<br>Connection Faliure!4");
            if ($mail_status->messages > 0) {
                $sort_results = imap_sort($mb, SORTDATE, 1);
                $search_results = imap_search($mb, 'UNSEEN');
                if (is_array($search_results)) {
                    $message_nos = $this->order_search($search_results, $sort_results);
                    foreach ($message_nos as $key => $message_no) {
                        $email_headers = imap_headerinfo($mb, $message_no);
                        $mail_data = array();
                        $mail_data['mail_no'] = $email_headers->Msgno;
                        $mail_data['uid'] = imap_uid($mb, $message_no);
                        if (isset($email_headers->from[0]->mailbox) && $email_headers->from[0]->mailbox != '') {
                            $mail_data['from_address'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
                            if (isset($email_headers->from[0]->personal)) {
                                $mail_data['from_name'] = $email_headers->from[0]->personal;
                            } else {
                                $mail_data['from_name'] = $email_headers->from[0]->mailbox . '@' . $email_headers->from[0]->host;
                            }
                        }

                        if (isset($email_headers->to[0]->mailbox) && $email_headers->to[0]->mailbox != '') {
                            $mail_data['to_address'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
                            if (isset($email_headers->to[0]->personal)) {
                                $mail_data['to_name'] = $email_headers->to[0]->personal;
                            } else {
                                $mail_data['to_name'] = $email_headers->to[0]->mailbox . '@' . $email_headers->to[0]->host;
                            }
                        }

                        if (isset($email_headers->reply_to[0]->mailbox) && $email_headers->reply_to[0]->mailbox != '') {
                            $mail_data['reply_to'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
                            if (isset($email_headers->reply_to[0]->personal)) {
                                $mail_data['reply_to_name'] = $email_headers->reply_to[0]->personal;
                            } else {
                                $mail_data['reply_to_name'] = $email_headers->reply_to[0]->mailbox . '@' . $email_headers->reply_to[0]->host;
                            }
                        }

                        if (isset($email_headers->sender[0]->mailbox) && $email_headers->sender[0]->mailbox != '') {
                            $mail_data['sender_address'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
                            if (isset($email_headers->sender[0]->personal)) {
                                $mail_data['sender_name'] = $email_headers->sender[0]->personal;
                            } else {
                                $mail_data['sender_name'] = $email_headers->sender[0]->mailbox . '@' . $email_headers->sender[0]->host;
                            }
                        }

                        $mail_data['cc'] = '';
                        $mail_data['bcc'] = '';
                        $mail_data['subject'] = $email_headers->subject;
                        $mail_data['body'] = $this->get_mail_body($mb, $message_no);
                        $attachments = $this->get_mail_attachments($mb, $message_no);
                        if (count($attachments) > 0) {
                            $mail_data['is_attachment'] = 1;
                            $attachment_url = array();
                            $time = time();
                            mkdir('resource/uploads/attachments/' . $time);
                            foreach ($attachments as $attachment) {
                                $file = "resource/uploads/attachments/$time/" . $attachment['filename'];
                                file_put_contents($file, base64_decode($attachment['file']));
                                $attachment_url[] = array(
                                    'filename' => $attachment['filename'],
                                    'url' => $file,
                                    'subtype' => $attachment['subtype'],
                                );
                            }
                            $mail_data['attachments'] = serialize($attachment_url);
                        }
                        $mail_data['received_at'] = $email_headers->date;
                        $mail_data['folder_label'] = 'Inbox';
                        $mail_data['folder_name'] = $mailbox;
                        $mail_data['is_unread'] = $email_headers->Unseen == 'U' ? 1 : 0;
                        $mail_data['is_starred'] = $email_headers->Flagged == 'F' ? 1 : 0;
                        $mail_data['created_at'] = date('Y-m-d H:i:s');
                        $this->ci->db->insert('inbox', $mail_data);
                    }
                }
            }
            imap_close($mb);
        }
        return true;
    }

    /**
     * @return bool
     */
    function sent_drafts_mails()
    {
        if ($this->check_internet_conn()) {
            $this->ci->db->select('*');
            $this->ci->db->from('inbox');
            $this->ci->db->where('folder_name', $this->email_server . '[Gmail]/Drafts');
            $this->ci->db->where('is_sent', 0);
            $query = $this->ci->db->get();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $mail_row) {
                    $attach = array();
                    if ($mail_row->attachments != null) {
                        $attachments = unserialize($mail_row->attachments);
                        foreach ($attachments as $attachment_row) {
                            $attach[] = $attachment_row['url'];
                        }
                    }
                    if ($this->send_mail($mail_row->to_address, $mail_row->subject, $mail_row->body, $attach)) {
                        $update_data = array('is_sent' => 1, 'mail_no' => 1 + $this->get_max_mail_no_in_folder('Sent mail'), 'folder_label' => 'Sent mail', 'folder_name' => $this->email_server . $this->getServerFolderName('Sent Mail'));
                        $this->ci->db->where('mail_id', $mail_row->mail_id);
                        $this->ci->db->update('inbox', $update_data);
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param $folder_name
     * @return string
     */
    function getServerFolderName($folder_name)
    {
        $default_folder_name = array(
            'All Mail',
            'Drafts',
            'Important',
            'Sent Mail',
            'Spam',
            'Starred',
            'Trash',
        );
        if (strtolower($folder_name) == "inbox") {
            return "INBOX";
        } elseif (in_array(ucfirst($folder_name), $default_folder_name)) {
            return "[Gmail]/" . ucfirst($folder_name);
        } else {
            $mailbox_label_array = explode('/', $folder_name);
            if (count($mailbox_label_array) > 1) {
                $folder_name = '';
                foreach ($mailbox_label_array as $key => $item) {
                    if ($key > 0) {
                        $folder_name .= "/";
                    }
                    $folder_name .= $item;
                }
            }
            return $folder_name;
        }
    }

    /**
     * @param $folder_label
     * @return mixed
     */
    function get_max_mail_no_in_folder($folder_label)
    {
        $this->ci->db->select('MAX(mail_no) as mail_no');
        $this->ci->db->from('inbox');
        $this->ci->db->where('LOWER(folder_label)', strtolower($folder_label));
        $query = $this->ci->db->get();
        return $query->num_rows() > 0 ? $query->row()->mail_no : 0;
    }

    function send_mail($to, $subject, $message, $attach = null)
    {
        if ($this->check_internet_conn()) {
            /*$this->ci->load->library('email');*/
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => "$this->email_address",
                'smtp_pass' => "$this->email_password",
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            );
            $this->ci->load->library('email', $config);
            $this->ci->email->set_newline("\r\n");
            $this->ci->email->from('JayKhodiyar', "$this->email_address");
            $this->ci->email->to($to);
            $this->ci->email->subject($subject);
            $this->ci->email->message($message);
            if ($attach != null && $attach != '') {
                if (is_array($attach) && count($attach) > 0) {
                    foreach ($attach as $attach_row) {
                        $this->ci->email->attach($attach_row);
                    }
                }
            }
            $this->ci->email->bcc(BCC_EMAIL_ADDRESS);
            if ($this->ci->email->send()) {
                if ($attach != null && $attach != '') {
                    if (is_array($attach)) {
                        foreach ($attach as $attach_row) {
                            if (file_exists($attach_row)) {
                                unlink($attach_row);
                            }
                        }
                    }
                }
                return true;
            } else {
                if ($attach != null && $attach != '') {
                    if (is_array($attach)) {
                        foreach ($attach as $attach_row) {
                            if (file_exists($attach_row)) {
                                unlink($attach_row);
                            }
                        }
                    }
                }
                /*echo $this->email->print_debugger();*/
                return false;
            }
        }
    }


    /**
     * @param $mb
     * @param $message_no
     * @return mixed|string
     */
    function get_mail_body($mb, $message_no)
    {
        $body = imap_fetchbody($mb, $message_no, 1.2);
        if (!strlen($body) > 0) {
            $body = imap_fetchbody($mb, $message_no, 1);
        }
        $body = preg_replace('/^\>/m', '', $body);
        return $body;
    }


    /**
     * @param $mb
     * @param $message_no
     * @return array
     */
    function get_mail_attachments($mb, $message_no)
    {
        $attachments = array();
        $structure = imap_fetchstructure($mb, $message_no);
        if (isset($structure->parts) && count($structure->parts)) {
            for ($i = 0; $i < count($structure->parts); $i++) {
                $attachments[$i] = array('is_attachment' => false, 'filename' => '', 'name' => '', 'attachment' => '', 'subtype' => '');
                $attachments[$i]['subtype'] = strtolower($structure->parts[$i]->subtype);
                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                        }
                    }
                }
                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }
                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($mb, $message_no, $i + 1);
                    if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                    } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }
        }
        $response_data = array();
        if (count($attachments) != 0) {
            foreach ($attachments as $at) {
                if ($at['is_attachment'] == 1) {
                    $response_data[] = array(
                        'filename' => $at['name'],
                        'file' => $at['attachment'],
                        'subtype' => $at['subtype'],
                    );
                }
            }
        }
        return $response_data;
    }

    function get_quotation_ref_no($quotation_no,$item_code){
        return $item_code.'/'.$quotation_no.'/'.$this->get_financial_year();
    }

    function get_sales_order_no($sales_order_no){
		$sales_order_no = str_replace('SO', '', $sales_order_no);
        return $this->get_company_code()."/SO/".$sales_order_no."/".$this->get_financial_year();
    }
    
    function get_work_order_no($sales_order_no){
		$sales_order_no = str_replace('SO', '', $sales_order_no);
        return $this->get_company_code()."/WO/".$sales_order_no."/".$this->get_financial_year();
    }
    
    function get_purchase_order_no($purchase_order_no){
        return $this->get_company_code()."/PO/$purchase_order_no/".$this->get_financial_year();
    }

    function get_challan_no($challan_no, $challan_date){
//        return $challan_no."/".$this->get_financial_year();
        return $challan_no."/".$this->get_financial_year_by_date($challan_date);
    }

    function get_proforma_invoice_no($party_code,$proforma_invoice_no){
        return "JK/$party_code/$proforma_invoice_no/".$this->get_financial_year();
    }

    function get_invoice_no($invoice_no, $invoice_date){
//        return $this->get_company_code()."/SRT/$invoice_no/".$this->get_financial_year();
        return $invoice_no."/".$this->get_financial_year_by_date($invoice_date);
    }
    
    function get_feedback_no($feedback_no, $feedback_date){
        return $feedback_no."/".$this->get_financial_year_by_date($feedback_date);
    }
    
    function get_complain_no($complain_no, $complain_date){
        return $complain_no."/".$this->get_financial_year_by_date($complain_date);
    }
    
    function get_resolve_complain_no($resolve_complain_no, $resolve_complain_date){
        return $resolve_complain_no."/".$this->get_financial_year_by_date($resolve_complain_date);
    }
    
    function get_reminder_letter_no($letter_no){
		$letter_no = sprintf("%04d", $letter_no);
		return $this->get_company_code().$letter_no."/".$this->get_financial_year();
    }
	
	function get_payment_recipt_no($receipt_no){
		return "$receipt_no/".$this->get_financial_year();
	}

    function get_company_code(){
        $this->ci->db->select('code');
        $this->ci->db->from('company');
        $this->ci->db->where('id',COMPANY_ID);
        $this->ci->db->limit(1);
        $query = $this->ci->db->get();
        if($query->num_rows() > 0){
            return $query->row()->code;
        }else{
            return '';
        }
    }

    function get_login_user_google_sheet()
    {
        $staff_id = $this->ci->session->userdata('is_logged_in')['staff_id'];
        $this->ci->db->select('gs.*');
        $this->ci->db->from('google_sheets gs');
        $this->ci->db->join('google_sheet_staff gss','gss.google_sheet_id = gs.id');
        $this->ci->db->where('gss.staff_id',$staff_id);
        $this->ci->db->order_by('gs.sequence');
        $query = $this->ci->db->get();
        $response = array();
        if($query->num_rows() > 0){
           foreach($query->result() as $row){
               $response[] = array(
                   'id' => $row->id,
                   'name' => $row->name,
                   'url' => $row->url,
               );
           }
        }
        return $response;
    }

    function get_financial_year(){
        if (date('m') <= 3) {
            $financial_year = (date('y')-1) . '/' . date('y');
        } else {
            $financial_year = date('y') . '/' . (date('y') + 1);
        }
        return $financial_year;
    }
    function get_financial_year_by_date($date){
        $time = strtotime($date);
        $month = date("m",$time);
        $year = date("y",$time);
        if ($month <= 3) {
            $financial_year = ($year-1) . '/' . $year;
        } else {
            $financial_year = $year . '/' . ($year + 1);
        }
        return $financial_year;
    }
    
    function get_financial_start_date_by_date($date){
        $time = strtotime($date);
        $month = date("m",$time);
        $year = date("Y",$time);
        if ($month <= 3) {
            $financial_start_date = '01-04-'.($year-1);
        } else {
            $financial_start_date = '01-04-'.$year;
        }
        return $financial_start_date;
    }

    function update_outstanding_payment($party_id)
    {
        if (!$party_id) {
            return;
        }

        $payment = $this->ci->db->query("SELECT received_payment_date as created, total_cmount as amount, 'debit' as payment_type, 'Invoice' as tableName
                                        FROM invoices where sales_to_party_id = ".$party_id." 
                                        UNION ALL SELECT payment_date as created, amount as amount, payment_type, 'Payment' as tableName 
                                        FROM payment where party_id = ".$party_id." 
                                        ORDER BY created ASC");

        $total_credit = $total_debit = 0;
        foreach ($payment->result_array() as $row)
        {
            if ($row['payment_type'] == 'debit') {
                $total_debit = $total_debit + $row['amount'];
            } else {
                $total_credit = $total_credit + $row['amount'];
            }
        }

        $balance = $total_credit - $total_debit;

        $update_data = array('outstanding_balance' => $balance);
        $this->ci->db->where('party_id', $party_id);
        $this->ci->db->update('party', $update_data);
    }
    
    function check_party_validations($party_id){
		// echo "<pre>";print_r($_POST);exit;
		$party_data = $this->ci->input->post('party');
		$party_code = isset($party_data['party_code']) ? $party_data['party_code'] : '';
		$phone_no = isset($party_data['phone_no']) ? $party_data['phone_no'] : '';
		$email_id = isset($party_data['email_id']) ? $party_data['email_id'] : '';
		$party_name = isset($party_data['party_name']) ? $party_data['party_name'] : '';
		$address = isset($party_data['address']) ? $party_data['address'] : '';
		$city_id = isset($party_data['city_id']) ? $party_data['city_id'] : '';
		$state_id = isset($party_data['state_id']) ? $party_data['state_id'] : '';
		$country_id = isset($party_data['country_id']) ? $party_data['country_id'] : '';
		$fax_no = isset($party_data['fax_no']) ? $party_data['fax_no'] : '';
		$pincode = isset($party_data['pincode']) ? $party_data['pincode'] : '';
		$website = isset($party_data['website']) ? $party_data['website'] : '';
		$project = isset($party_data['project']) ? $party_data['project'] : '';
		$reference_description = isset($party_data['reference_description']) ? $party_data['reference_description'] : '';

		$returnStatus = 1;
		$returnMsg = "";

		$temp = $email_id;
		$emails = explode(",", $temp);
		$email_status = 1;
		$email_msg = "";

		$email_ids = array();
		// multiple email validation
		if (is_array($emails) && count($emails) > 0) {
			foreach ($emails as $email) {
				if (trim($email) != "") {
					$email = trim($email);
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
						$email_msg .= "$email is not valid email.&nbsp;";
						$email_status = 0;
					} 
					$this->ci->db->select('*');
					$this->ci->db->from('party');
					$this->ci->db->where('party_id !=' ,$party_id);
					$this->ci->db->where('active !=' ,0);
					$this->ci->db->where("FIND_IN_SET( '".$email."', email_id) ");
					$res = $this->ci->db->get();
					$rows = $res->num_rows();
					if ($rows > 0) {
						$result = $res->result();
 						$email_msg .= "$email email already exist.&nbsp; <br/> Original Party : " . $result[0]->party_name;
						$email_status = 0;
					} else {
						$email_ids[] = $email;
					}
				}
			}
		}

		if ($email_status == 0) {
			return array('status' => 0, 'msg' => $email_msg);
		}

		$partyName = strtolower(trim($party_name));

		if ($partyName == "") {
			return array('status' => 0, 'msg' => "Customer name is required.");
		}

		// party name unique validation
		$sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(party_name)) = '" . addslashes($partyName) . "' AND phone_no='".$phone_no."' AND party_id != $party_id";
		$query = $this->ci->db->query($sql_list_query);
		$rows = $query->result_array();
		if (count($rows) > 0) {
			return array('status' => 0, 'msg' => "Party name already exist!");
		}

		if (trim($phone_no) == "" && count($email_ids) == 0) {
			$email_msg = "Please enter phone number or email id !";
			return array('status' => 0, 'msg' => $email_msg);
		}

		// get phone numbers
		$phone_msg = '';
		$phone_numbers = explode(",", $phone_no);
		$phone_status = 1;
		$phone_nos = array();
		foreach ($phone_numbers as $phone) {
			$phone = trim($phone);
			$this->ci->db->select('*');
			$this->ci->db->from('party');
			$this->ci->db->where('party_id !=' ,$party_id);
			$this->ci->db->where('active !=' ,0);
			$this->ci->db->where("FIND_IN_SET( '".$phone."', phone_no) ");
			$res = $this->ci->db->get();
			$rows = $res->num_rows();
			//echo $phone.'<br>';echo $this->ci->db->last_query().'<br>';
			if ($rows > 0 && trim($phone) != "") {
				$result = $res->result();
				$phone_msg .= "$phone mobile already exist.&nbsp; &nbsp; <br/> Original Party : ". $result[0]->party_name;
				$phone_status = 0;
			}
		}
		if($phone_status == 0){
			echo json_encode(array("status" => 0, 'msg' => $phone_msg, 'phone_error' => 1));
			exit;
		}
		$phone_numbers = $phone_no;
		$email_ids = implode(',', $email_ids);
		
		$party_data = array(
			'address' => $address,
			'pincode' => $pincode,
			'phone_no' => $phone_numbers,
			'fax_no' => $fax_no,
			'email_id' => $email_ids,
			'website' => $website,
			'city_id' => $city_id,
			'state_id' => $state_id,
			'country_id' => $country_id,
			'project' => $project,
			'reference_description' => $reference_description,
			'oldApprovedDate' => '2001-01-01'
		);

		if ($party_code != "")
			$party_data['party_code'] = $party_code;

		if ($party_name != "")
			$party_data['party_name'] = $party_name;

		//echo '<pre>';print_r($party_data);exit;
		$this->ci->db->where('party_id', $party_id);
		$this->ci->db->update('party', $party_data);

		return array('status' => $returnStatus, 'msg' => $returnMsg);
	}
    
    function get_company_detail(){
		$this->ci->db->select('*');
		$this->ci->db->from('company');
		$this->ci->db->where('id', COMPANY_ID);
		$this->ci->db->limit(1);
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
			return $query->row_array(0);
		} else {
			return array();
		}
	}
    
    function get_company_ceo_detail(){
		$this->ci->db->select('*');
		$this->ci->db->from('staff');
		$this->ci->db->where('designation_id', COMPANY_DESIGNATION_CEO_ID);
		$this->ci->db->limit(1);
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
			return $query->row_array(0);
		} else {
			return array();
		}
	}
    
    function get_staff_contact_no($id){
		$this->ci->db->select('contact_no');
		$this->ci->db->from('staff');
		$this->ci->db->where('staff_id', $id);
		$this->ci->db->limit(1);
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->contact_no;
		} else {
			return array();
		}
	}
    
    function send_sms($module_name, $module_id, $send_to, $sms_topic_id){
        $this->ci->db->select('*');
		$this->ci->db->from('sms_topic');
		$this->ci->db->where('id', $sms_topic_id);
		$this->ci->db->limit(1);
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
			$sms_topic = $query->row();
//            $send_to = '9724887520';
            $send_to = str_replace(' ','', $send_to);
            
            if($module_name == 'resolve_complain'){
                $this->ci->db->select('rc.resolve_complain_no, rc.resolve_complain_date, rc.rc_jk_technical_person_contact, s.name as rc_jk_technical_person');
                $this->ci->db->from('resolve_complain rc');
                $this->ci->db->join('staff s','s.staff_id = rc.rc_jk_technical_person');
                $this->ci->db->where('rc.resolve_complain_id',$module_id);
                $query = $this->ci->db->get();
                $resolve_complain_data = $query->result();
                if(!empty($resolve_complain_data)){
                    $resolve_complain_data = $resolve_complain_data[0];
                    $vars = array(
                        '{{resolve_person_name}}' => $resolve_complain_data->rc_jk_technical_person,
                        '{{resolve_person_no}}' => $resolve_complain_data->rc_jk_technical_person_contact,
                    );
                    $sms_topic->message = strtr($sms_topic->message, $vars);
                }
            } else if($module_name == 'installation'){
                $this->ci->db->select('i.installation_no, i.installation_date, i.jk_technical_person_contact, s.name as jk_technical_person,i.jk_technical_person_name');
                $this->ci->db->from('installation i');
                $this->ci->db->join('staff s','s.staff_id = i.jk_technical_person','left');
                $this->ci->db->where('i.installation_id',$module_id);
                $query = $this->ci->db->get();
                $installation_data = $query->result();
                if(!empty($installation_data)){
                    $installation_data = $installation_data[0];
                    $vars = array();
                    if (!empty($installation_data->jk_technical_person_name)) {
                        $vars['{{technician_name}}'] = $installation_data->jk_technical_person_name;
                    } else {
                        $vars['{{technician_name}}'] = $installation_data->jk_technical_person;
                    }
                    $vars['{{technician_mobile_no}}'] = $installation_data->jk_technical_person_contact;
                    $sms_topic->message = strtr($sms_topic->message, $vars);
                }
            }
//            http://sms.infisms.co.in/API/SendSMS.aspx?UserID=XXXXXX&amp;UserPassword=XXXXX&amp;PhoneNumber=XXXXX&amp;Text=TestMessaeg&amp;SenderId=XXXXXX &amp;AccountType=X&amp;MessageType=X
            $api_params = '?UserID='.SEND_SMS_USERID.'&UserPassword='.SEND_SMS_USERPASSWORD.'&PhoneNumber='.$send_to.'&Text='.urlencode($sms_topic->message).'&SenderId='.SEND_SMS_SENDERID.'&AccountType='.SEND_SMS_ACCOUNTTYPE.'&MessageType='.SEND_SMS_MESSAGETYPE;  
            $smsGatewayUrl = "http://sms.infisms.co.in/API/SendSMS.aspx";  
            $url = $smsgatewaydata = $smsGatewayUrl.$api_params;
            
//            $this->ci->load->library('curl');
//            $this->ci->curl->create($url);
//            $result = $this->ci->curl->execute();

            $ch = curl_init();                       // initialize CURL
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);                         // Close CURL

            // Use file get contents when CURL is not installed on server.
            if(!$result){
               $result =  file_get_contents($url);  
            }
            
            if($result){
                $result = explode('</br>', $result);
                $delivery_report_id = $result[0];

                $sent_sms_details = array();
                $sent_sms_details['module_name'] = $module_name;
                $sent_sms_details['module_id'] = $module_id;
                $sent_sms_details['send_to'] = $send_to;
                $sent_sms_details['sms_topic_id'] = $sms_topic_id;
                $sent_sms_details['message'] = $sms_topic->message;
                $sent_sms_details['delivery_report_id'] = $delivery_report_id;
                $sent_sms_details['created_by'] = $this->staff_id;
                $sent_sms_details['created_at'] = $this->now_time;
                $this->ci->db->insert('sent_sms', $sent_sms_details);
            }
		}
    }
    
    function send_whatsapp_sms($module_name, $module_id, $sms_topic_id){
        $this->ci->db->select('*');
		$this->ci->db->from('sms_topic');
		$this->ci->db->where('id', $sms_topic_id);
		$this->ci->db->limit(1);
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
			$sms_topic = $query->row();
            
            if($module_name == 'resolve_complain'){
                $this->ci->db->select('rc.resolve_complain_no, rc.resolve_complain_date, rc.rc_jk_technical_person_contact, s.name as rc_jk_technical_person');
                $this->ci->db->from('resolve_complain rc');
                $this->ci->db->join('staff s','s.staff_id = rc.rc_jk_technical_person');
                $this->ci->db->where('rc.resolve_complain_id',$module_id);
                $query = $this->ci->db->get();
                $resolve_complain_data = $query->result();
                if(!empty($resolve_complain_data)){
                    $resolve_complain_data = $resolve_complain_data[0];
                    $vars = array(
                        '{{resolve_person_name}}' => $resolve_complain_data->rc_jk_technical_person,
                        '{{resolve_person_no}}' => $resolve_complain_data->rc_jk_technical_person_contact,
                    );
                    $sms_topic->message = strtr($sms_topic->message, $vars);
                }
            } else if($module_name == 'installation'){
                $this->ci->db->select('i.installation_no, i.installation_date, i.jk_technical_person_contact, s.name as jk_technical_person,i.jk_technical_person_name');
                $this->ci->db->from('installation i');
                $this->ci->db->join('staff s','s.staff_id = i.jk_technical_person','left');
                $this->ci->db->where('i.installation_id',$module_id);
                $query = $this->ci->db->get();
                $installation_data = $query->result();
                if(!empty($installation_data)){
                    $installation_data = $installation_data[0];
                    $vars = array();
                    if (!empty($installation_data->jk_technical_person_name)) {
                        $vars['{{technician_name}}'] = $installation_data->jk_technical_person_name;
                    } else {
                        $vars['{{technician_name}}'] = $installation_data->jk_technical_person;
                    }
                    $vars['{{technician_mobile_no}}'] = $installation_data->jk_technical_person_contact;
                    $sms_topic->message = strtr($sms_topic->message, $vars);
                }
            }
            return $sms_topic->message;
		}
    }
    
    function common_place_to_send_sms($module_name, $module_id, $send_to, $sms_topic_id,$receiver_id,$receiver_type){
        $this->ci->db->select('*');
		$this->ci->db->from('sms_topic');
		$this->ci->db->where('id', $sms_topic_id);
		$this->ci->db->limit(1);
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
			$sms_topic = $query->row();
            $send_to = str_replace(' ','', $send_to);
            
//            http://sms.infisms.co.in/API/SendSMS.aspx?UserID=XXXXXX&amp;UserPassword=XXXXX&amp;PhoneNumber=XXXXX&amp;Text=TestMessaeg&amp;SenderId=XXXXXX &amp;AccountType=X&amp;MessageType=X
            $api_params = '?UserID='.SEND_SMS_USERID.'&UserPassword='.SEND_SMS_USERPASSWORD.'&PhoneNumber='.$send_to.'&Text='.urlencode($sms_topic->message).'&SenderId='.SEND_SMS_SENDERID.'&AccountType='.SEND_SMS_ACCOUNTTYPE.'&MessageType='.SEND_SMS_MESSAGETYPE;  
            $smsGatewayUrl = "http://sms.infisms.co.in/API/SendSMS.aspx";  
            $url = $smsgatewaydata = $smsGatewayUrl.$api_params;
            
            $ch = curl_init();                       // initialize CURL
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);                         // Close CURL

            if(!$result){
               $result =  file_get_contents($url);  
            }
            
            if($result){
                $result = explode('</br>', $result);
                $delivery_report_id = $result[0];
                
                $sent_sms_details = array();
                $sent_sms_details['module_name'] = $module_name;
                $sent_sms_details['module_id'] = $module_id;
                $sent_sms_details['send_to'] = $send_to;
                if($receiver_type == 'party'){
                    $sent_sms_details['party_id'] = $receiver_id;
                } else if($receiver_type == 'supplier'){
                    $sent_sms_details['supplier_id'] = $receiver_id;
                } else if($receiver_type == 'staff'){
                    $sent_sms_details['staff_id'] = $receiver_id;
                }
                $sent_sms_details['sms_topic_id'] = $sms_topic_id;
                $sent_sms_details['message'] = $sms_topic->message;
                $sent_sms_details['delivery_report_id'] = $delivery_report_id;
                $sent_sms_details['created_by'] = $this->staff_id;
                $sent_sms_details['created_at'] = $this->now_time;
                $this->ci->db->insert('sent_sms', $sent_sms_details);
            }
		}
    }
    
    function get_new_invoice_no($invoice_date){
        $year = $this->get_financial_year_by_date($invoice_date);
        
//        $sql = "SELECT `id`, `invoice_no` FROM `invoices` 
//                    WHERE `invoice_no` = (SELECT MAX(`invoice_no`) FROM `invoices`) AND `invoice_no` LIKE '%".$year."%'
//                    UNION
//                        SELECT `general_invoice_id`, `general_invoice_no` FROM `general_invoice` 
//                    WHERE `general_invoice_no` = (SELECT MAX(`general_invoice_no`) FROM `general_invoice`) AND `general_invoice_no` LIKE '%".$year."%'";
//        $result = $this->ci->db->query($sql)->result();
//        print_r($result); exit;
        
        $new_invoice_no = 0;
		$this->ci->db->select_max('invoice_no');
		$this->ci->db->from('invoices');
        $this->ci->db->like('invoice_no', $year);
		$this->ci->db->limit(1);
		$this->ci->db->order_by('id', 'desc');
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
            $invoice_no = $query->row()->invoice_no;
            $invoice_no = explode('/', $invoice_no);
            $new_invoice_no = $invoice_no[0] + 1;
		} else {
			$new_invoice_no = 0;
		}
        $new_general_invoice_no = 0;
        $this->ci->db->select_max('general_invoice_no');
		$this->ci->db->from('general_invoice');
        $this->ci->db->like('general_invoice_no', $year);
		$this->ci->db->limit(1);
		$this->ci->db->order_by('general_invoice_id', 'desc');
		$query = $this->ci->db->get();
		if ($query->num_rows() > 0) {
            $general_invoice_no = $query->row()->general_invoice_no;
            $general_invoice_no = explode('/', $general_invoice_no);
            $new_general_invoice_no = $general_invoice_no[0] + 1;
		} else {
			$new_general_invoice_no = 0;
		}
        
        if($new_invoice_no > $new_general_invoice_no){
            return $new_invoice_no;
        } else {
            return $new_general_invoice_no;
        }
    }
    
    function search_in_multidimensional_array($array, $key, $value){
        $results = array();
        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }
            foreach ($array as $subarray) {
                $results = array_merge($results, $this->search_in_multidimensional_array($subarray, $key, $value));
            }
        }
        return $results;
    }
    
    function incrementalHash($len = 10) {
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';

        $now = explode(' ', microtime())[1];
        while ($now >= $base) {
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }
        return substr($result, -10);
    }
    
    function get_max_effective_date_bom_id_by_item_id($item_id){
        $this->ci->db->select('*');
        $this->ci->db->from('bom_master');
        $this->ci->db->where('sales_item_id', $item_id);
        $this->ci->db->where('effective_date <=', date('Y-m-d'));
        $this->ci->db->order_by('effective_date', 'desc');
        $this->ci->db->limit(1);
        $query = $this->ci->db->get();
        return $query->row();
    }
    
    function get_purchase_amount_by_item_id($item_id){
        
        $latest_bom = $this->get_max_effective_date_bom_id_by_item_id($item_id);
        $this->ci->db->select('bm.*,bid.item_id, bid.quantity, bid.job_expense, pi.rate, pi.reference_qty');
		$this->ci->db->from('bom_master bm');
		$this->ci->db->join('bom_item_details bid', 'bid.bom_id = bm.bom_id', 'left');
		$this->ci->db->join('purchase_items pi', 'bid.item_id = pi.id', 'left');
        $this->ci->db->where('bm.sales_item_id', $item_id);
        if(!empty($latest_bom)){
            $this->ci->db->where('bm.bom_id', $latest_bom->bom_id);
        }
		$bom_query = $this->ci->db->get();
		$bom_result = $bom_query->result();
        $purchase_amount = 0;
        foreach ($bom_result as $bom_row){
            
            $amount = 0;
            $purchase_item_id = $bom_row->item_id;
            $purchase_item_quantity = $bom_row->quantity;
            $purchase_item_reference_qty = $bom_row->reference_qty;
            $purchase_item_job_expense = $bom_row->job_expense;
            
            if($purchase_item_quantity > 0){
                $amount = $amount + ($purchase_item_quantity * $bom_row->rate * $purchase_item_reference_qty) + $purchase_item_job_expense;
            }
            $purchase_amount = $purchase_amount + $amount;
//            if(!empty($purchase_amount)) {
//                echo $purchase_amount . ' || <br> '; 
//                exit;
//            }
        }
        $this->ci->db->select('bm.*, SUM(bie.`expense`) as expense_amount');
		$this->ci->db->from('bom_master bm');
		$this->ci->db->join('bom_item_expenses bie', 'bie.bom_id = bm.bom_id', 'left');
		$this->ci->db->where('bm.sales_item_id', $item_id);
        if(!empty($latest_bom)){
            $this->ci->db->where('bm.bom_id', $latest_bom->bom_id);
        }
		$query = $this->ci->db->get();
		$row = $query->row_array();
        $expense_amount = $row['expense_amount'];
//        echo $expense_amount . ' || <br> '; 
        $purchase_amount = $purchase_amount + $expense_amount;
        
        $return = array();
        $return['purchase_amount'] = $purchase_amount;
        $return['latest_bom'] = $latest_bom;
        return $return;
    }
    
    function get_purchase_amount_by_item_id_for_production($item_id, $from_date, $to_date){
        
        $this->ci->db->select('p.*,pid.purchase_item_id AS item_id, pid.quantity, pid.job_expense, pi.rate, pi.reference_qty');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_details pid', 'pid.production_id = p.production_id', 'left');
		$this->ci->db->join('purchase_items pi', 'pid.purchase_item_id = pi.id', 'left');
        $this->ci->db->where('p.item_id', $item_id);
        $this->ci->db->where('p.finished_at >=', $from_date);
        $this->ci->db->where('p.finished_at <=', $to_date);
		$bom_query = $this->ci->db->get();
		$bom_result = $bom_query->result();
        $purchase_amount = 0;
//        echo $this->ci->db->last_query(); exit;
//        echo '<pre>'; print_r($bom_result); exit;
        foreach ($bom_result as $bom_row){
            
            $amount = 0;
            $purchase_item_id = $bom_row->item_id;
            $purchase_item_quantity = $bom_row->quantity;
            $purchase_item_reference_qty = $bom_row->reference_qty;
            $purchase_item_job_expense = $bom_row->job_expense;
            
            if($purchase_item_quantity > 0){
                $amount = $amount + ($purchase_item_quantity * $bom_row->rate * $purchase_item_reference_qty) + $purchase_item_job_expense;
            }
            $purchase_amount = $purchase_amount + $amount;
//            if(!empty($purchase_amount)) {
//                echo $purchase_amount . ' || <br> '; 
//                exit;
//            }
        }
        $this->ci->db->select('p.*, SUM(pie.`expense`) as expense_amount');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_expenses pie', 'pie.production_id = p.production_id', 'left');
		$this->ci->db->where('p.item_id', $item_id);
        $this->ci->db->where('p.finished_at >=', $from_date);
        $this->ci->db->where('p.finished_at <=', $to_date);
		$query = $this->ci->db->get();
		$row = $query->row_array();
        $expense_amount = $row['expense_amount'];
        $purchase_amount = $purchase_amount + $expense_amount;
        
        $return = array();
        $return['purchase_amount'] = $purchase_amount;
        return $return;
    }
    
    function get_purchase_amount_by_item_id_for_production_wise($item_id, $production_id, $from_date, $to_date){
        
        $this->ci->db->select('p.*,pid.purchase_item_id AS item_id, pid.quantity, pid.job_expense, pi.rate, pi.reference_qty');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_details pid', 'pid.production_id = p.production_id', 'left');
		$this->ci->db->join('purchase_items pi', 'pid.purchase_item_id = pi.id', 'left');
        $this->ci->db->where('p.item_id', $item_id);
        $this->ci->db->where('p.production_id', $production_id);
        $this->ci->db->where('p.finished_at >=', $from_date);
        $this->ci->db->where('p.finished_at <=', $to_date);
		$bom_query = $this->ci->db->get();
		$bom_result = $bom_query->result();
        $purchase_amount = 0;
        $amount = 0;
//        echo $this->ci->db->last_query(); exit;
//        echo '<pre>'; print_r($bom_result); exit;
        foreach ($bom_result as $bom_row){
            $amount = 0;
            $purchase_item_id = $bom_row->item_id;
            $purchase_item_quantity = $bom_row->quantity;
            $purchase_item_reference_qty = $bom_row->reference_qty;
            $purchase_item_job_expense = $bom_row->job_expense;
            
            if($purchase_item_quantity > 0){
                $amount = $amount + ($purchase_item_quantity * $bom_row->rate * $purchase_item_reference_qty) + $purchase_item_job_expense;
            }
            $purchase_amount = $purchase_amount + $amount;
        }
        
        $this->ci->db->select('p.*, SUM(pie.`expense`) as expense_amount');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_expenses pie', 'pie.production_id = p.production_id', 'left');
		$this->ci->db->where('p.item_id', $item_id);
        $this->ci->db->where('p.production_id', $production_id);
        $this->ci->db->where('p.finished_at >=', $from_date);
        $this->ci->db->where('p.finished_at <=', $to_date);
		$query = $this->ci->db->get();
		$row = $query->row_array();
        $expense_amount = $row['expense_amount'];
        $purchase_amount = $purchase_amount + $expense_amount;
        
        $return = array();
        $return['purchase_amount'] = $purchase_amount;
        return $return;
    }
    
    function get_purchase_items_by_item_id($item_id){
        $return = array();
        $latest_bom = $this->get_max_effective_date_bom_id_by_item_id($item_id);
        $return['latest_bom'] = $latest_bom;
        $this->ci->db->select('bm.*, bid.basic_qty, bid.project_qty, bid.quantity, bid.job_expense, pi.id, pi.item_name, pi.rate, pi.uom, pi.reference_qty, pi.required_qty, pi.required_uom, pi.effective_stock, pp.project_name, uom1.uom as pi_uom, uom2.uom as pi_required_uom');
		$this->ci->db->from('bom_master bm');
		$this->ci->db->join('bom_item_details bid', 'bid.bom_id = bm.bom_id', 'left');
		$this->ci->db->join('purchase_project pp', 'pp.project_id = bid.project_id', 'left');
		$this->ci->db->join('purchase_items pi', 'bid.item_id = pi.id', 'left');
		$this->ci->db->join('uom uom1', 'pi.uom = uom1.id', 'left');
		$this->ci->db->join('uom uom2', 'pi.required_uom = uom2.id', 'left');
        $this->ci->db->where('bm.sales_item_id', $item_id);
        if(!empty($latest_bom)){
            $this->ci->db->where('bm.bom_id', $latest_bom->bom_id);
        }
		$query = $this->ci->db->get();
		$return['purchase_items'] = $query->result();
                
        $this->ci->db->select('bm.*, SUM(bie.`expense`) as expense_amount');
		$this->ci->db->from('bom_master bm');
		$this->ci->db->join('bom_item_expenses bie', 'bie.bom_id = bm.bom_id', 'left');
		$this->ci->db->where('bm.sales_item_id', $item_id);
        if(!empty($latest_bom)){
            $this->ci->db->where('bm.bom_id', $latest_bom->bom_id);
        }
		$query = $this->ci->db->get();
		$row = $query->row_array();
        $return['expense_amount'] = $row['expense_amount'];
        
        return $return;
    }
    
    function get_purchase_items_by_item_id_for_production($item_id, $production_id, $from_date, $to_date){
        $return = array();
        $this->ci->db->select('p.*, pid.basic_qty, pid.project_qty, pid.quantity, pid.job_expense, pi.id, pi.item_name, pi.rate, pi.uom, pi.reference_qty, pi.required_qty, pi.required_uom, pi.effective_stock, pp.project_name, uom1.uom as pi_uom, uom2.uom as pi_required_uom');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_details pid', 'pid.production_id = p.production_id', 'left');
		$this->ci->db->join('purchase_project pp', 'pp.project_id = pid.project_id', 'left');
		$this->ci->db->join('purchase_items pi', 'pid.purchase_item_id = pi.id', 'left');
		$this->ci->db->join('uom uom1', 'pi.uom = uom1.id', 'left');
		$this->ci->db->join('uom uom2', 'pi.required_uom = uom2.id', 'left');
        $this->ci->db->where('p.item_id', $item_id);
        $this->ci->db->where('p.production_id', $production_id);
        $this->ci->db->where('p.finished_at >=', $from_date);
        $this->ci->db->where('p.finished_at <=', $to_date);
		$query = $this->ci->db->get();
        
		$return['purchase_items'] = $query->result();
                
        $this->ci->db->select('p.*, SUM(pie.`expense`) as expense_amount');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_expenses pie', 'pie.production_id = p.production_id', 'left');
		$this->ci->db->where('p.item_id', $item_id);
		$this->ci->db->where('p.production_id', $production_id);
        $this->ci->db->where('p.finished_at >=', $from_date);
        $this->ci->db->where('p.finished_at <=', $to_date);
		$query1 = $this->ci->db->get();
		$row = $query1->row_array();
//        echo $this->ci->db->last_query(); exit;
        $return['expense_amount'] = $row['expense_amount'];
        
        return $return;
    }

    function get_purchase_projects_by_bom_id($bom_id){
        $this->ci->db->select('bid.project_id,bid.project_qty,pp.project_name');
		$this->ci->db->from('bom_item_details bid');
		$this->ci->db->join('purchase_project pp', 'pp.project_id = bid.project_id', 'left');
        $this->ci->db->where('bid.bom_id', $bom_id);
        $this->ci->db->where('bid.project_id !=', 0);
        $this->ci->db->group_by('bid.project_id');
		$query = $this->ci->db->get();
		$return = $query->result();
        return $return;
    }
    
    function update_purchase_stock_by_purchase_invoice_id($invoice_id, $operator){
        $this->ci->db->select('pi.id, pi.uom, pi.reference_qty, pii.quantity, pii.item_data, pi.current_item_stock, pi.effective_stock');
		$this->ci->db->from('purchase_items pi');
		$this->ci->db->join('purchase_invoice_items pii', 'pii.item_id = pi.id', 'left');
		$this->ci->db->where('pii.invoice_id', $invoice_id);
		$query = $this->ci->db->get();
		$purchase_items = $query->result();
        
        $update_purchase_items = array();
        foreach ($purchase_items as $key => $purchase_item){
            $item_data = json_decode($purchase_item->item_data);
            $pi_uom = $purchase_item->uom;
            $pii_uom_id = $item_data->uom_id;
            if($pi_uom != $pii_uom_id){
                $pi_relation = $purchase_item->reference_qty;
                $quantity = $purchase_item->quantity * $pi_relation;
            } else {
                $quantity = $purchase_item->quantity;
            }
            $update_purchase_items[$key]['id'] = $purchase_item->id;
            if($operator == '-'){
                $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock - $quantity;
                $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock - $quantity;
            } else if($operator == '+'){
                $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock + $quantity;
                $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock + $quantity;
            }
        }
        $this->ci->db->update_batch('purchase_items', $update_purchase_items, 'id');
    }
    
    function update_purchase_order_qty_stock_by_purchase_invoice_id($invoice_id, $operator){
        $where = 'poi.item_id = pii.item_id';
        $this->ci->db->select('poi.id, pii.quantity, pii.item_id, pii.item_data, poi.invoice_qty');
		$this->ci->db->from('purchase_invoice pi');
		$this->ci->db->join('purchase_invoice_items pii', 'pii.invoice_id = pi.invoice_id', 'left');
		$this->ci->db->join('purchase_order_items poi', 'poi.order_id = pi.order_id', 'left');
		$this->ci->db->where('pi.invoice_id', $invoice_id);
		$this->ci->db->where($where);
		$query = $this->ci->db->get();
		$purchase_items = $query->result();
//        echo $this->ci->db->last_query(); exit;
//        echo '<pre>'; print_r($purchase_items); exit;
        
        $update_purchase_items = array();
        foreach ($purchase_items as $key => $purchase_item){
            
            $item_data = json_decode($purchase_item->item_data);
            $query = $this->ci->db->select("*")->from('purchase_items')->where('id', $purchase_item->item_id)->get();
            $purchase_item_pre = $query->result();
            $purchase_item_pre = $purchase_item_pre[0];
            $pi_uom = $purchase_item_pre->uom;
            $pii_uom_id = $item_data->uom_id;
            if($pi_uom != $pii_uom_id){
                $pi_relation = $purchase_item_pre->reference_qty;
                $quantity = $purchase_item->quantity * $pi_relation;
            } else {
                $quantity = $purchase_item->quantity;
            }
            
            $update_purchase_items[$key]['id'] = $purchase_item->id;
            if($operator == '-'){
                $update_purchase_items[$key]['invoice_qty'] = abs($purchase_item->invoice_qty - $quantity);
            } else if($operator == '+'){
                $update_purchase_items[$key]['invoice_qty'] = $purchase_item->invoice_qty + $quantity;
            }
        }
        $this->ci->db->update_batch('purchase_order_items', $update_purchase_items, 'id');
    }
    
    function update_purchase_stock_from_production_by_sales_item_id($production_id, $operator){
        $this->ci->db->select('pid.purchase_item_id as item_id, pid.quantity, pi.in_production, pi.effective_stock, pi.uom, pi.reference_qty');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_details pid', 'p.production_id = pid.production_id', 'left');
		$this->ci->db->join('purchase_items pi', 'pid.purchase_item_id = pi.id', 'left');
        $this->ci->db->where('p.production_id', $production_id);
		$query = $this->ci->db->get();
		$purchase_items = $query->result();
        
        if(!empty($purchase_items)){
            $update_purchase_items = array();
            foreach ($purchase_items as $key => $purchase_item){
                $update_purchase_items[$key]['id'] = $purchase_item->item_id;
                if($operator == '-'){
                    $update_purchase_items[$key]['in_production'] = $purchase_item->in_production - $purchase_item->quantity;
                    $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock + $purchase_item->quantity;
                    $this->purchase_item_fifo_from_delete_production($production_id, $purchase_item->item_id, $purchase_item->quantity, $purchase_item->uom, $purchase_item->reference_qty);
                } else if($operator == '+'){
                    $update_purchase_items[$key]['in_production'] = $purchase_item->in_production + $purchase_item->quantity;
                    $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock - $purchase_item->quantity;
                    $this->purchase_item_fifo_from_start_production($production_id, $purchase_item->item_id, $purchase_item->quantity, $purchase_item->uom, $purchase_item->reference_qty);
                }
            }
            $this->ci->db->update_batch('purchase_items', $update_purchase_items, 'id');
        }
    }
    
    function update_purchase_stock_on_production_finish_by_sales_item_id($production_id, $operator){
        $this->ci->db->select('pid.purchase_item_id as item_id, pid.quantity, pi.in_production, pi.current_item_stock');
		$this->ci->db->from('production p');
		$this->ci->db->join('production_item_details pid', 'p.production_id = pid.production_id', 'left');
		$this->ci->db->join('purchase_items pi', 'pid.purchase_item_id = pi.id', 'left');
        $this->ci->db->where('p.production_id', $production_id);
		$query = $this->ci->db->get();
		$purchase_items = $query->result();
//        echo $this->ci->db->last_query(); exit;
        
        if(!empty($purchase_items)){
            $update_purchase_items = array();
            foreach ($purchase_items as $key => $purchase_item){
                $update_purchase_items[$key]['id'] = $purchase_item->item_id;
                if($operator == '-'){
                    $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock - $purchase_item->quantity;
                    $update_purchase_items[$key]['in_production'] = $purchase_item->in_production - $purchase_item->quantity;
                } else if($operator == '+'){
                    $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock + $purchase_item->quantity;
                    $update_purchase_items[$key]['in_production'] = $purchase_item->in_production + $purchase_item->quantity;
                }
            }
            $this->ci->db->update_batch('purchase_items', $update_purchase_items, 'id');
        }
    }
    
    function update_purchase_stock_on_direct_challan_by_sales_item_id($inserted_production_id, $challan_id, $sales_item_id, $operator){
        $latest_bom = $this->get_max_effective_date_bom_id_by_item_id($sales_item_id);
        $this->ci->db->select('bm.bom_id, bid.item_id, bid.quantity, pi.current_item_stock, pi.in_production, pi.effective_stock, pi.uom, pi.reference_qty');
		$this->ci->db->from('bom_master bm');
		$this->ci->db->join('bom_item_details bid', 'bid.bom_id = bm.bom_id', 'left');
		$this->ci->db->join('purchase_project pp', 'pp.project_id = bid.project_id', 'left');
		$this->ci->db->join('purchase_items pi', 'bid.item_id = pi.id', 'left');
        $this->ci->db->where('bm.sales_item_id', $sales_item_id);
        if(!empty($latest_bom)){
            $this->ci->db->where('bm.bom_id', $latest_bom->bom_id);
        }
		$query = $this->ci->db->get();
		$purchase_items = $query->result();
        
        if(!empty($purchase_items)){
            $update_purchase_items = array();
            foreach ($purchase_items as $key => $purchase_item){
                $update_purchase_items[$key]['id'] = $purchase_item->item_id;
                if($operator == '-'){
                    $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock - $purchase_item->quantity;
                    $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock - $purchase_item->quantity;
                    $this->purchase_item_fifo_from_add_challan($inserted_production_id, $challan_id, $purchase_item->item_id, $purchase_item->quantity, $purchase_item->uom, $purchase_item->reference_qty);
                } else if($operator == '+'){
                    $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock + $purchase_item->quantity;
                    $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock + $purchase_item->quantity;
                }
            }
            $this->ci->db->update_batch('purchase_items', $update_purchase_items, 'id');
        }
    }
    
    function update_purchase_stock_by_general_invoice_id($general_invoice_id, $operator){
        $this->ci->db->select('pi.id, gid.quantity, pi.current_item_stock, pi.effective_stock, pi.uom, pi.reference_qty');
		$this->ci->db->from('purchase_items pi');
		$this->ci->db->join('general_invoice_details gid', 'gid.purchase_item_id = pi.id', 'left');
		$this->ci->db->where('gid.general_invoice_id', $general_invoice_id);
		$query = $this->ci->db->get();
		$purchase_items = $query->result();
        
        $update_purchase_items = array();
        foreach ($purchase_items as $key => $purchase_item){
            $update_purchase_items[$key]['id'] = $purchase_item->id;
            if($operator == '-'){
                $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock - $purchase_item->quantity;
                $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock - $purchase_item->quantity;
                $this->purchase_item_fifo_from_add_general_invoice($general_invoice_id, $purchase_item->id, $purchase_item->quantity, $purchase_item->uom, $purchase_item->reference_qty);
            } else if($operator == '+'){
                $update_purchase_items[$key]['current_item_stock'] = $purchase_item->current_item_stock + $purchase_item->quantity;
                $update_purchase_items[$key]['effective_stock'] = $purchase_item->effective_stock + $purchase_item->quantity;
                $this->purchase_item_fifo_from_delete_general_invoice($general_invoice_id, $purchase_item->id, $purchase_item->quantity, $purchase_item->uom, $purchase_item->reference_qty);
            }
        }
        $this->ci->db->update_batch('purchase_items', $update_purchase_items, 'id');
    }
    
    function purchase_item_fifo_from_start_production($production_id, $purchase_item_id, $purchase_item_quantity, $purchase_item_uom, $purchase_item_reference_qty){
        $this->ci->db->select('id, pending_qty, item_data');
        $this->ci->db->from('purchase_invoice_items');
        $this->ci->db->where('item_id', $purchase_item_id);
        $this->ci->db->where('pending_qty != ', 0);
        $pii_query = $this->ci->db->get();
        $pii_result = $pii_query->result();

        foreach ($pii_result as $pii_row){
            $item_data = json_decode($pii_row->item_data);
            if($purchase_item_uom != $item_data->uom_id){
                $purchase_item_quantity = $purchase_item_quantity / $purchase_item_reference_qty;
                $purchase_item_quantity = number_format($purchase_item_quantity, '2', '.', '');
            }
            if(!empty($purchase_item_quantity) && $purchase_item_quantity >= $pii_row->pending_qty){
                $purchase_item_quantity = $purchase_item_quantity - $pii_row->pending_qty;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => 0), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['production_id'] = $production_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $pii_row->pending_qty;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                if($purchase_item_quantity == 0){
                    break;
                }
            } else if(!empty($purchase_item_quantity) && $purchase_item_quantity < $pii_row->pending_qty){
                $now_pending_qty = $pii_row->pending_qty - $purchase_item_quantity;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $now_pending_qty), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['production_id'] = $production_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $purchase_item_quantity;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                $purchase_item_quantity = 0;
                break;
            }
        }
    }
    
    function purchase_item_fifo_from_delete_production($production_id, $purchase_item_id, $purchase_item_quantity, $purchase_item_uom, $purchase_item_reference_qty){
        $this->ci->db->select('id, quantity, pending_qty, item_data');
        $this->ci->db->from('purchase_invoice_items');
        $this->ci->db->where('item_id', $purchase_item_id);
        $this->ci->db->order_by('id', 'desc');
        $pii_query = $this->ci->db->get();
        $pii_result = $pii_query->result();
        
        foreach ($pii_result as $pii_row){
            if($pii_row->quantity > $pii_row->pending_qty){
                $item_data = json_decode($pii_row->item_data);
                $updatable_qty = $pii_row->quantity - $pii_row->pending_qty;
    //                echo $purchase_item_quantity;
                if($purchase_item_uom != $item_data->uom_id){
                    $purchase_item_quantity = $purchase_item_quantity / $purchase_item_reference_qty;
                    $purchase_item_quantity = number_format($purchase_item_quantity, '2', '.', '');
                }
                if(!empty($purchase_item_quantity) && $purchase_item_quantity >= $updatable_qty){
                    $purchase_item_quantity = $purchase_item_quantity - $updatable_qty;
                    $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $pii_row->quantity), array('id' => $pii_row->id));
                    if($purchase_item_quantity == 0){
                        break;
                    }
                } else if(!empty($purchase_item_quantity) && $purchase_item_quantity < $updatable_qty){
                    $now_pending_qty = $pii_row->pending_qty + $purchase_item_quantity;
                    $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $now_pending_qty), array('id' => $pii_row->id));
                    $purchase_item_quantity = 0;
                    break;
                }
            }
        }
        $where_arr = array();
        $where_arr['production_id'] = $production_id;
        $where_arr['purchase_item_id'] = $purchase_item_id;
        $this->ci->db->delete('purchase_item_fifo', $where_arr);
    }
    
    function purchase_item_fifo_from_add_challan($inserted_production_id, $challan_id, $purchase_item_id, $purchase_item_quantity, $purchase_item_uom, $purchase_item_reference_qty){
        $this->ci->db->select('id, pending_qty, item_data');
        $this->ci->db->from('purchase_invoice_items');
        $this->ci->db->where('item_id', $purchase_item_id);
        $this->ci->db->where('pending_qty != ', 0);
        $pii_query = $this->ci->db->get();
        $pii_result = $pii_query->result();

        foreach ($pii_result as $pii_row){
            $item_data = json_decode($pii_row->item_data);
            if($purchase_item_uom != $item_data->uom_id){
                $purchase_item_quantity = $purchase_item_quantity / $purchase_item_reference_qty;
                $purchase_item_quantity = number_format($purchase_item_quantity, '2', '.', '');
            }
            if(!empty($purchase_item_quantity) && $purchase_item_quantity >= $pii_row->pending_qty){
                $purchase_item_quantity = $purchase_item_quantity - $pii_row->pending_qty;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => 0), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['production_id'] = $inserted_production_id;
                $purchase_item_fifo_arr['challan_id'] = $challan_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $pii_row->pending_qty;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                if($purchase_item_quantity == 0){
                    break;
                }
            } else if(!empty($purchase_item_quantity) && $purchase_item_quantity < $pii_row->pending_qty){
                $now_pending_qty = $pii_row->pending_qty - $purchase_item_quantity;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $now_pending_qty), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['production_id'] = $inserted_production_id;
                $purchase_item_fifo_arr['challan_id'] = $challan_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $purchase_item_quantity;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                $purchase_item_quantity = 0;
                break;
            }
        }
    }
    
    function purchase_item_fifo_from_add_challan_and_production_already_started($production_id, $challan_id){
        $this->ci->db->update('purchase_item_fifo', array('challan_id' => $challan_id), array('production_id' => $production_id));
    }
    
    function purchase_item_fifo_from_delete_challan($challan_id){
        $this->ci->db->update('purchase_item_fifo', array('challan_id' => NULL), array('challan_id' => $challan_id));
    }
    
    function purchase_item_fifo_from_add_general_invoice($general_invoice_id, $purchase_item_id, $purchase_item_quantity, $purchase_item_uom, $purchase_item_reference_qty){
        $this->ci->db->select('id, pending_qty, item_data');
        $this->ci->db->from('purchase_invoice_items');
        $this->ci->db->where('item_id', $purchase_item_id);
        $this->ci->db->where('pending_qty != ', 0);
        $pii_query = $this->ci->db->get();
        $pii_result = $pii_query->result();

        foreach ($pii_result as $pii_row){
            $item_data = json_decode($pii_row->item_data);
            if($purchase_item_uom != $item_data->uom_id){
                $purchase_item_quantity = $purchase_item_quantity / $purchase_item_reference_qty;
                $purchase_item_quantity = number_format($purchase_item_quantity, '2', '.', '');
            }
            if(!empty($purchase_item_quantity) && $purchase_item_quantity >= $pii_row->pending_qty){
                $purchase_item_quantity = $purchase_item_quantity - $pii_row->pending_qty;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => 0), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['general_invoice_id'] = $general_invoice_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $pii_row->pending_qty;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                if($purchase_item_quantity == 0){
                    break;
                }
            } else if(!empty($purchase_item_quantity) && $purchase_item_quantity < $pii_row->pending_qty){
                $now_pending_qty = $pii_row->pending_qty - $purchase_item_quantity;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $now_pending_qty), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['general_invoice_id'] = $general_invoice_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $purchase_item_quantity;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                $purchase_item_quantity = 0;
                break;
            }
        }
    }
    
    function purchase_item_fifo_from_edit_general_invoice($general_invoice_id, $purchase_item_id, $purchase_item_quantity, $purchase_item_uom, $purchase_item_reference_qty){
        
        $where_arr = array();
        $where_arr['general_invoice_id'] = $general_invoice_id;
        $where_arr['purchase_item_id'] = $purchase_item_id;
        $this->ci->db->delete('purchase_item_fifo', $where_arr);
        
        $this->ci->db->select('id, pending_qty, item_data');
        $this->ci->db->from('purchase_invoice_items');
        $this->ci->db->where('item_id', $purchase_item_id);
        $this->ci->db->where('pending_qty != ', 0);
        $pii_query = $this->ci->db->get();
        $pii_result = $pii_query->result();

        foreach ($pii_result as $pii_row){
            $item_data = json_decode($pii_row->item_data);
            if($purchase_item_uom != $item_data->uom_id){
                $purchase_item_quantity = $purchase_item_quantity / $purchase_item_reference_qty;
                $purchase_item_quantity = number_format($purchase_item_quantity, '2', '.', '');
            }
            if(!empty($purchase_item_quantity) && $purchase_item_quantity >= $pii_row->pending_qty){
                $purchase_item_quantity = $purchase_item_quantity - $pii_row->pending_qty;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => 0), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['general_invoice_id'] = $general_invoice_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $pii_row->pending_qty;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                if($purchase_item_quantity == 0){
                    break;
                }
            } else if(!empty($purchase_item_quantity) && $purchase_item_quantity < $pii_row->pending_qty){
                $now_pending_qty = $pii_row->pending_qty - $purchase_item_quantity;
                $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $now_pending_qty), array('id' => $pii_row->id));

                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['general_invoice_id'] = $general_invoice_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                $purchase_item_fifo_arr['qty'] = $purchase_item_quantity;
                $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                $purchase_item_quantity = 0;
                break;
            }
        }
    }
    
    function purchase_item_fifo_from_delete_general_invoice($general_invoice_id, $purchase_item_id, $purchase_item_quantity, $purchase_item_uom, $purchase_item_reference_qty){
        $this->ci->db->select('id, quantity, pending_qty, item_data');
        $this->ci->db->from('purchase_invoice_items');
        $this->ci->db->where('item_id', $purchase_item_id);
        $this->ci->db->order_by('id', 'desc');
        $pii_query = $this->ci->db->get();
        $pii_result = $pii_query->result();
        
        foreach ($pii_result as $pii_row){
            if($pii_row->quantity > $pii_row->pending_qty){
                $item_data = json_decode($pii_row->item_data);
                $updatable_qty = $pii_row->quantity - $pii_row->pending_qty;
    //                echo $purchase_item_quantity;
                if($purchase_item_uom != $item_data->uom_id){
                    $purchase_item_quantity = $purchase_item_quantity / $purchase_item_reference_qty;
                    $purchase_item_quantity = number_format($purchase_item_quantity, '2', '.', '');
                }
                if(!empty($purchase_item_quantity) && $purchase_item_quantity >= $updatable_qty){
                    $purchase_item_quantity = $purchase_item_quantity - $updatable_qty;
                    $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $pii_row->quantity), array('id' => $pii_row->id));
                    if($purchase_item_quantity == 0){
                        break;
                    }
                } else if(!empty($purchase_item_quantity) && $purchase_item_quantity < $updatable_qty){
                    $now_pending_qty = $pii_row->pending_qty + $purchase_item_quantity;
                    $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $now_pending_qty), array('id' => $pii_row->id));
                    $purchase_item_quantity = 0;
                    break;
                }
            }
        }
        $where_arr = array();
        $where_arr['general_invoice_id'] = $general_invoice_id;
        $where_arr['purchase_item_id'] = $purchase_item_id;
        $this->ci->db->delete('purchase_item_fifo', $where_arr);
    }
    
    function add_purchase_item_fifo_by_item_id($challan_id, $item_id){
        
        $latest_bom = $this->get_max_effective_date_bom_id_by_item_id($item_id);
        $this->ci->db->select('bm.*,bid.item_id, bid.quantity, pi.rate');
		$this->ci->db->from('bom_master bm');
		$this->ci->db->join('bom_item_details bid', 'bid.bom_id = bm.bom_id', 'left');
		$this->ci->db->join('purchase_items pi', 'bid.item_id = pi.id', 'left');
        $this->ci->db->where('bm.sales_item_id', $item_id);
        if(!empty($latest_bom)){
            $this->ci->db->where('bm.bom_id', $latest_bom->bom_id);
        }
		$bom_query = $this->ci->db->get();
		$bom_result = $bom_query->result();
        $purchase_amount = 0;
        
        foreach ($bom_result as $bom_row){
            
            $amount = 0;
            $purchase_item_id = $bom_row->item_id;
            $purchase_item_quantity = $bom_row->quantity;
            $purchase_item_data = array();
            
            $this->ci->db->select('id, pending_qty, item_data');
            $this->ci->db->from('purchase_invoice_items');
            $this->ci->db->where('item_id', $purchase_item_id);
            $this->ci->db->where('pending_qty != ', 0);
            $pii_query = $this->ci->db->get();
            $pii_result = $pii_query->result();
            
            foreach ($pii_result as $pii_row){
                $item_data = json_decode($pii_row->item_data);
                $item_data->pii_id = $pii_row->id;
                $item_data->pending_qty = $pii_row->pending_qty;
//                echo $purchase_item_quantity;
                if(!empty($purchase_item_quantity) && $purchase_item_quantity >= $item_data->pending_qty){
                    $purchase_item_quantity = $purchase_item_quantity - $item_data->pending_qty;
                    $this->ci->db->update('purchase_invoice_items', array('pending_qty' => 0), array('id' => $pii_row->id));
                   
                    $purchase_item_fifo_arr = array();
                    $purchase_item_fifo_arr['challan_id'] = $challan_id;
                    $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                    $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                    $purchase_item_fifo_arr['qty'] = $item_data->pending_qty;
                    $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                    $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                    $purchase_item_fifo_arr['created_at'] = $this->now_time;
                    $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                   
                } else if(!empty($purchase_item_quantity) && $purchase_item_quantity < $item_data->pending_qty){
                    $now_pending_qty = $item_data->pending_qty - $purchase_item_quantity;
                    $this->ci->db->update('purchase_invoice_items', array('pending_qty' => $now_pending_qty), array('id' => $pii_row->id));
                    
                    $purchase_item_fifo_arr = array();
                    $purchase_item_fifo_arr['challan_id'] = $challan_id;
                    $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                    $purchase_item_fifo_arr['purchase_invoice_item_id'] = $pii_row->id;
                    $purchase_item_fifo_arr['qty'] = $purchase_item_quantity;
                    $purchase_item_fifo_arr['rate'] = $item_data->item_rate;
                    $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                    $purchase_item_fifo_arr['created_at'] = $this->now_time;
                    $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
                    $purchase_item_quantity = 0;
                    break;
                }
            }
            if($purchase_item_quantity > 0){
                $purchase_item_fifo_arr = array();
                $purchase_item_fifo_arr['challan_id'] = $challan_id;
                $purchase_item_fifo_arr['purchase_item_id'] = $purchase_item_id;
                $purchase_item_fifo_arr['purchase_invoice_item_id'] = 0;
                $purchase_item_fifo_arr['qty'] = $purchase_item_quantity;
                $purchase_item_fifo_arr['rate'] = $bom_row->rate;
                $purchase_item_fifo_arr['created_by'] = $this->staff_id;
                $purchase_item_fifo_arr['created_at'] = $this->now_time;
                $this->ci->db->insert('purchase_item_fifo', $purchase_item_fifo_arr);
            }
            
        }
    }
    
    function update_purchase_item_rate($item_id, $item_rate){
        $this->ci->db->update('purchase_items', array('rate' => $item_rate), array('id' => $item_id));
    }
        
    function send_whatsapp_sms_through_twilio($sms){
        
        $dotenv = new Dotenv\Dotenv(__DIR__);
        $dotenv->load();

        // Your Account Sid and Auth Token from twilio.com/console
        $sid    = getenv("TWILIO_ACCOUNT_SID");
        $token  = getenv("TWILIO_AUTH_TOKEN");
        $twilio = new Client($sid, $token);

        $codes = ["chocolate", "vanilla", "strawberry", "mint-chocolate-chip", "cookies-n-cream"];

        $message = $twilio->messages
                    ->create(
                        "whatsapp:+919724887520",
                        array(
        //					"mediaUrl" => array("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"),
//                            "body" => $sms,
                            "body" => 'Quotation Created',
                            "from" => "whatsapp:".getenv("TWILIO_WHATSAPP_NUMBER")
                        )
                    );
    }
    
}
