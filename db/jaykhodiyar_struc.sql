--
-- Table structure for table `agent`
--

CREATE TABLE `agent` (
  `id` int(11) NOT NULL,
  `party_type_1` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `fix_commission` double NOT NULL,
  `agent_name` varchar(100) NOT NULL,
  `commission` double NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `agent`
--
DELIMITER $$
CREATE TRIGGER `agent_add_log` AFTER INSERT ON `agent` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.agent_log
SET
	`id`=new.id,
    `party_type_1`=new.party_type_1,
    `address`=new.address,
    `company_name`=new.company_name,
    `phone_no`=new.phone_no,
    `email`=new.email,
    `note`=new.note,
    `fix_commission`=new.fix_commission,
    `agent_name`=new.agent_name,
    `commission`=new.commission,
    `created_by`=new.created_by,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `agent_delete_log` BEFORE DELETE ON `agent` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.agent_log
SET
`id`=old.id,
`party_type_1`=old.party_type_1,
`address`=old.address,
`company_name`=old.company_name,
`phone_no`=old.phone_no,
`email`=old.email,
`note`=old.note,
`fix_commission`=old.fix_commission,
`agent_name`=old.agent_name,
`commission`=old.commission,
`created_by`=old.created_by,
`created_at`=old.created_at,
`updated_at`=old.updated_at,
`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `agent_edit_log` AFTER UPDATE ON `agent` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.agent_log
SET
	`id`=new.id,
    `party_type_1`=new.party_type_1,
    `address`=new.address,
    `company_name`=new.company_name,
    `phone_no`=new.phone_no,
    `email`=new.email,
    `note`=new.note,
    `fix_commission`=new.fix_commission,
    `agent_name`=new.agent_name,
    `commission`=new.commission,
    `created_by`=new.created_by,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `billing_template`
--

CREATE TABLE `billing_template` (
  `id` int(11) NOT NULL,
  `template` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `billing_template`
--
DELIMITER $$
CREATE TRIGGER `billing_template_add_log` AFTER INSERT ON `billing_template` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_template_log
SET
	`id`=new.id,
    `template`=new.template,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `billing_template_delete_log` AFTER DELETE ON `billing_template` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_template_log
SET
	`id`=old.id,
    `template`=old.template,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `billing_template_edit_log` AFTER UPDATE ON `billing_template` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_template_log
SET
	`id`=new.id,
    `template`=new.template,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `billing_terms`
--

CREATE TABLE `billing_terms` (
  `id` int(11) NOT NULL,
  `billing_terms_name` varchar(255) DEFAULT NULL,
  `billing_terms_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `billing_terms`
--
DELIMITER $$
CREATE TRIGGER `billing_terms_add_log` AFTER INSERT ON `billing_terms` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_log
SET
	`id`=new.id,
    `billing_terms_name`=new.billing_terms_name,
    `billing_terms_date`=new.billing_terms_date,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `billing_terms_delete_log` AFTER DELETE ON `billing_terms` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_log
SET
	`id`=old.id,
    `billing_terms_name`=old.billing_terms_name,
    `billing_terms_date`=old.billing_terms_date,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `billing_terms_edit_log` AFTER UPDATE ON `billing_terms` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_log
SET
	`id`=new.id,
    `billing_terms_name`=new.billing_terms_name,
    `billing_terms_date`=new.billing_terms_date,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `billing_terms_detail`
--

CREATE TABLE `billing_terms_detail` (
  `id` int(11) NOT NULL,
  `billing_terms_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `billing_terms_detail`
--
DELIMITER $$
CREATE TRIGGER `billing_terms_detail_add_log` AFTER INSERT ON `billing_terms_detail` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.billing_terms_detail_log
SET
`id`=new.id,
`billing_terms_id`=new.billing_terms_id,
`cal_code`=new.cal_code,
`narration`=new.narration,
`cal_definition`=new.cal_definition,
`percentage`=new.percentage,
`value`=new.value,
`gl_acc`=new.gl_acc,
`created_at`=new.created_at,
`updated_at`=new.updated_at,
`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `billing_terms_detail_delete_log` AFTER DELETE ON `billing_terms_detail` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_detail_log
SET
	`id`=old.id,
    `billing_terms_id`=old.billing_terms_id,
    `cal_code`=old.cal_code,
    `narration`=old.narration,
    `cal_definition`=old.cal_definition,
    `percentage`=old.percentage,
    `value`=old.value,
    `gl_acc`=old.gl_acc,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `billing_terms_detail_edit_log` AFTER UPDATE ON `billing_terms_detail` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.billing_terms_detail_log
SET
`id`=new.id,
`billing_terms_id`=new.billing_terms_id,
`cal_code`=new.cal_code,
`narration`=new.narration,
`cal_definition`=new.cal_definition,
`percentage`=new.percentage,
`value`=new.value,
`gl_acc`=new.gl_acc,
`created_at`=new.created_at,
`updated_at`=new.updated_at,
`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `branch`
--
DELIMITER $$
CREATE TRIGGER `branch_add_log` AFTER INSERT ON `branch` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.branch_log
SET
	`branch_id`=new.branch_id,
    `branch_code`=new.branch_code,
    `branch`=new.branch,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `branch_delete_log` AFTER DELETE ON `branch` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.branch_log
SET
	`branch_id`=old.branch_id,
    `branch_code`=old.branch_code,
    `branch`=old.branch,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `branch_edit_log` AFTER UPDATE ON `branch` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.branch_log
SET
	`branch_id`=new.branch_id,
    `branch_code`=new.branch_code,
    `branch`=new.branch,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cal_code_definition`
--

CREATE TABLE `cal_code_definition` (
  `id` int(11) NOT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `cal_code_definition`
--
DELIMITER $$
CREATE TRIGGER `cal_code_definition_add_log` AFTER INSERT ON `cal_code_definition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.cal_code_definition_log
SET
	`id`=new.id,
    `billing_terms_detail_id`=new.billing_terms_detail_id,
    `is_first_element`=new.is_first_element,
    `cal_operation`=new.cal_operation,
    `cal_code_id`=new.cal_code_id,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `cal_code_definition_delete_log` AFTER DELETE ON `cal_code_definition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.cal_code_definition_log
SET
	`id`=old.id,
    `billing_terms_detail_id`=old.billing_terms_detail_id,
    `is_first_element`=old.is_first_element,
    `cal_operation`=old.cal_operation,
    `cal_code_id`=old.cal_code_id,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `cal_code_definition_edit_log` AFTER UPDATE ON `cal_code_definition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.cal_code_definition_log
SET
	`id`=new.id,
    `billing_terms_detail_id`=new.billing_terms_detail_id,
    `is_first_element`=new.is_first_element,
    `cal_operation`=new.cal_operation,
    `cal_code_id`=new.cal_code_id,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `challans`
--

CREATE TABLE `challans` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `sales_order_item_id` int(11) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `truck_no` varchar(255) DEFAULT NULL,
  `gear_no_5` varchar(255) DEFAULT NULL,
  `gear_no_3_54` varchar(255) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `lr_date` date DEFAULT NULL,
  `trademark_name` varchar(222) DEFAULT NULL,
  `full_load_current` varchar(222) DEFAULT NULL,
  `serial_no` varchar(111) DEFAULT NULL,
  `serial_no_1` varchar(111) DEFAULT NULL,
  `serial_no_2` varchar(111) DEFAULT NULL,
  `serial_no_3` varchar(111) DEFAULT NULL,
  `serial_no_4` varchar(111) DEFAULT NULL,
  `motor_75_hp` varchar(255) DEFAULT NULL,
  `motor_50_hp` varchar(255) DEFAULT NULL,
  `rated_voltage` varchar(222) DEFAULT NULL,
  `phases_no` varchar(222) DEFAULT NULL,
  `short_circuit_rating` varchar(222) DEFAULT NULL,
  `capacity` varchar(222) DEFAULT NULL,
  `operating_manual_reference_no` varchar(222) DEFAULT NULL,
  `gross_weight` varchar(222) DEFAULT NULL,
  `manufacturing_monthYear` varchar(222) DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `transport_name` varchar(255) DEFAULT NULL,
  `transport_charges` decimal(10,2) DEFAULT NULL,
  `notes` text,
  `kind_attn_id` int(11) DEFAULT NULL,
  `quotation_no` varchar(255) DEFAULT NULL,
  `sales_order_no` varchar(255) DEFAULT NULL,
  `sales_order_date` varchar(255) DEFAULT NULL,
  `purchase_order_no` varchar(255) DEFAULT NULL,
  `purchase_order_date` varchar(255) DEFAULT NULL,
  `loading_at` varchar(255) DEFAULT NULL,
  `port_of_loading` varchar(255) DEFAULT NULL,
  `port_of_discharge` varchar(255) DEFAULT NULL,
  `port_place_of_delivery` varchar(255) DEFAULT NULL,
  `delivery_through_id` int(11) DEFAULT NULL,
  `name_of_shipment` varchar(255) DEFAULT NULL,
  `prepared_by` int(11) DEFAULT NULL,
  `checked_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rounding` varchar(255) DEFAULT NULL,
  `challan_no` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `challans`
--
DELIMITER $$
CREATE TRIGGER `challans_add_log` AFTER INSERT ON `challans` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challans_log
SET
	`id`=new.id,
	`sales_order_id`=new.sales_order_id,
	`sales_to_party_id`=new.sales_to_party_id,
	`truck_no`=new.truck_no,
	`gear_no_5`=new.gear_no_5,
	`gear_no_3_54`=new.gear_no_3_54,
	`lr_no`=new.lr_no,
	`lr_date` =new.lr_date,
	`trademark_name`=new.trademark_name,
	`full_load_current`=new.full_load_current,
	`serial_no`=new.serial_no,
	`serial_no_1`=new.serial_no_1,
	`serial_no_2`=new.serial_no_2,
	`serial_no_3`=new.serial_no_3,
	`serial_no_4`=new.serial_no_4,
	`motor_75_hp`=new.motor_75_hp,
	`motor_50_hp`=new.motor_50_hp,
	`rated_voltage`=new.rated_voltage,
	`phases_no`=new.phases_no,
	`short_circuit_rating`=new.short_circuit_rating,
	`capacity`=new.capacity,
	`operating_manual_reference_no`=new.operating_manual_reference_no,
	`gross_weight`=new.gross_weight,
	`manufacturing_monthYear`=new.manufacturing_monthYear,
	`challan_date` =new.challan_date,
	`transport_name`=new.transport_name,
	`transport_charges`  =new.transport_charges,
	`created_by`=new.created_by,
	`created_at` =new.created_at,
	`updated_by`=new.updated_by,
	`updated_at` =new.updated_at,
	`rounding`=new.rounding,
	`challan_no`=new.challan_no,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `challans_delete_log` AFTER DELETE ON `challans` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challans_log
SET
	`id`=old.id,
	`sales_order_id`=old.sales_order_id,
	`sales_to_party_id`=old.sales_to_party_id,
	`truck_no`=old.truck_no,
	`gear_no_5`=old.gear_no_5,
	`gear_no_3_54`=old.gear_no_3_54,
	`lr_no`=old.lr_no,
	`lr_date` =old.lr_date,
	`trademark_name`=old.trademark_name,
	`full_load_current`=old.full_load_current,
	`serial_no`=old.serial_no,
	`serial_no_1`=old.serial_no_1,
	`serial_no_2`=old.serial_no_2,
	`serial_no_3`=old.serial_no_3,
	`serial_no_4`=old.serial_no_4,
	`motor_75_hp`=old.motor_75_hp,
	`motor_50_hp`=old.motor_50_hp,
	`rated_voltage`=old.rated_voltage,
	`phases_no`=old.phases_no,
	`short_circuit_rating`=old.short_circuit_rating,
	`capacity`=old.capacity,
	`operating_manual_reference_no`=old.operating_manual_reference_no,
	`gross_weight`=old.gross_weight,
	`manufacturing_monthYear`=old.manufacturing_monthYear,
	`challan_date` =old.challan_date,
	`transport_name`=old.transport_name,
	`transport_charges`  =old.transport_charges,
	`created_by`=old.created_by,
	`created_at` =old.created_at,
	`updated_by`=old.updated_by,
	`updated_at` =old.updated_at,
	`rounding`=old.rounding,
	`challan_no`=old.challan_no,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `challans_edit_log` AFTER UPDATE ON `challans` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challans_log
SET
	`id`=new.id,
	`sales_order_id`=new.sales_order_id,
	`sales_to_party_id`=new.sales_to_party_id,
	`truck_no`=new.truck_no,
	`gear_no_5`=new.gear_no_5,
	`gear_no_3_54`=new.gear_no_3_54,
	`lr_no`=new.lr_no,
	`lr_date` =new.lr_date,
	`trademark_name`=new.trademark_name,
	`full_load_current`=new.full_load_current,
	`serial_no`=new.serial_no,
	`serial_no_1`=new.serial_no_1,
	`serial_no_2`=new.serial_no_2,
	`serial_no_3`=new.serial_no_3,
	`serial_no_4`=new.serial_no_4,
	`motor_75_hp`=new.motor_75_hp,
	`motor_50_hp`=new.motor_50_hp,
	`rated_voltage`=new.rated_voltage,
	`phases_no`=new.phases_no,
	`short_circuit_rating`=new.short_circuit_rating,
	`capacity`=new.capacity,
	`operating_manual_reference_no`=new.operating_manual_reference_no,
	`gross_weight`=new.gross_weight,
	`manufacturing_monthYear`=new.manufacturing_monthYear,
	`challan_date` =new.challan_date,
	`transport_name`=new.transport_name,
	`transport_charges`  =new.transport_charges,
	`created_by`=new.created_by,
	`created_at` =new.created_at,
	`updated_by`=new.updated_by,
	`updated_at` =new.updated_at,
	`rounding`=new.rounding,
	`challan_no`=new.challan_no,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `challan_items`
--

CREATE TABLE `challan_items` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `item_description` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `drawing_revision` varchar(255) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `igst` int(11) DEFAULT NULL,
  `igst_amount` int(11) DEFAULT NULL,
  `cgst` int(11) DEFAULT NULL,
  `cgst_amount` int(11) DEFAULT NULL,
  `sgst` int(11) DEFAULT NULL,
  `sgst_amount` int(11) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `item_note` text,
  `quantity` double DEFAULT NULL,
  `disc_per` double DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `disc_value` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `net_amount` double DEFAULT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `cust_part_no` varchar(255) DEFAULT NULL,
  `item_serial_no` varchar(255) DEFAULT NULL,
  `truck_container_no` varchar(255) DEFAULT NULL,
  `lr_bl_no` varchar(255) DEFAULT NULL,
  `lr_date` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `driver_name` varchar(255) DEFAULT NULL,
  `driver_contact_no` varchar(255) DEFAULT NULL,
  `packing_mark` varchar(255) DEFAULT NULL,
  `no_of_packing` varchar(255) DEFAULT NULL,
  `no_of_unit` varchar(255) DEFAULT NULL,
  `item_extra_accessories_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `challan_items`
--
DELIMITER $$
CREATE TRIGGER `challan_items_add_log` AFTER INSERT ON `challan_items` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challan_items_log
SET
	`id`=new.id,
	`item_id`=new.item_id,
	`challan_id`=new.challan_id,
	`item_code`=new.item_code,
	`item_category_id`=new.item_category_id,
	`item_name`=new.item_name,
	`item_desc`=new.item_desc,
	`item_description`=new.item_description,
	`add_description`=new.add_description,
	`detail_description`=new.detail_description,
	`drawing_number`=new.drawing_number,
	`drawing_revision`=new.drawing_revision,
	`uom_id`=new.uom_id,
	`uom`=new.uom,
	`total_amount`=new.total_amount,
	`item_note`=new.item_note,
	`quantity`=new.quantity,
	`disc_per`=new.disc_per,
	`rate`=new.rate,
	`disc_value`=new.disc_value,
	`amount`=new.amount,
	`net_amount`=new.net_amount,
	`item_status_id`=new.item_status_id,
	`item_status`=new.item_status,
	`cust_part_no`=new.cust_part_no,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `challan_items_delete_log` AFTER DELETE ON `challan_items` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challan_items_log
SET
	`id`=old.id,
	`item_id`=old.item_id,
	`challan_id`=old.challan_id,
	`item_code`=old.item_code,
	`item_category_id`=old.item_category_id,
	`item_name`=old.item_name,
	`item_desc`=old.item_desc,
	`item_description`=old.item_description,
	`add_description`=old.add_description,
	`detail_description`=old.detail_description,
	`drawing_number`=old.drawing_number,
	`drawing_revision`=old.drawing_revision,
	`uom_id`=old.uom_id,
	`uom`=old.uom,
	`total_amount`=old.total_amount,
	`item_note`=old.item_note,
	`quantity`=old.quantity,
	`disc_per`=old.disc_per,
	`rate`=old.rate,
	`disc_value`=old.disc_value,
	`amount`=old.amount,
	`net_amount`=old.net_amount,
	`item_status_id`=old.item_status_id,
	`item_status`=old.item_status,
	`cust_part_no`=old.cust_part_no,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `challan_items_edit_log` AFTER UPDATE ON `challan_items` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challan_items_log
SET
	`id`=new.id,
	`item_id`=new.item_id,
	`challan_id`=new.challan_id,
	`item_code`=new.item_code,
	`item_category_id`=new.item_category_id,
	`item_name`=new.item_name,
	`item_desc`=new.item_desc,
	`item_description`=new.item_description,
	`add_description`=new.add_description,
	`detail_description`=new.detail_description,
	`drawing_number`=new.drawing_number,
	`drawing_revision`=new.drawing_revision,
	`uom_id`=new.uom_id,
	`uom`=new.uom,
	`total_amount`=new.total_amount,
	`item_note`=new.item_note,
	`quantity`=new.quantity,
	`disc_per`=new.disc_per,
	`rate`=new.rate,
	`disc_value`=new.disc_value,
	`amount`=new.amount,
	`net_amount`=new.net_amount,
	`item_status_id`=new.item_status_id,
	`item_status`=new.item_status,
	`cust_part_no`=new.cust_part_no,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `challan_items_gearbox_details`
--

CREATE TABLE `challan_items_gearbox_details` (
  `id` int(11) NOT NULL,
  `challan_item_id` int(11) DEFAULT NULL,
  `gearbox_user` int(11) DEFAULT NULL,
  `gearbox_make` int(11) DEFAULT NULL,
  `gearbox_gear_type` int(11) DEFAULT NULL,
  `gearbox_model` int(11) DEFAULT NULL,
  `gearbox_ratio` int(11) DEFAULT NULL,
  `gearbox_serial_no` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_items_motor_details`
--

CREATE TABLE `challan_items_motor_details` (
  `id` int(11) NOT NULL,
  `challan_item_id` int(11) DEFAULT NULL,
  `motor_user` int(11) DEFAULT NULL,
  `motor_make` int(11) DEFAULT NULL,
  `motor_hp` int(11) DEFAULT NULL,
  `motor_kw` int(11) DEFAULT NULL,
  `motor_frequency` int(11) DEFAULT NULL,
  `motor_rpm` int(11) DEFAULT NULL,
  `motor_volts_cycles` int(11) DEFAULT NULL,
  `motor_serial_no` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_frequency`
--

CREATE TABLE `challan_item_frequency` (
  `id` int(11) NOT NULL,
  `frequency_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_gear_type`
--

CREATE TABLE `challan_item_gear_type` (
  `id` int(11) NOT NULL,
  `gear_type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_hp`
--

CREATE TABLE `challan_item_hp` (
  `id` int(11) NOT NULL,
  `hp_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_kw`
--

CREATE TABLE `challan_item_kw` (
  `id` int(11) NOT NULL,
  `kw_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_make`
--

CREATE TABLE `challan_item_make` (
  `id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_model`
--

CREATE TABLE `challan_item_model` (
  `id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_ratio`
--

CREATE TABLE `challan_item_ratio` (
  `id` int(11) NOT NULL,
  `ratio_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_rpm`
--

CREATE TABLE `challan_item_rpm` (
  `id` int(11) NOT NULL,
  `rpm_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_user`
--

CREATE TABLE `challan_item_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_item_volts_cycles`
--

CREATE TABLE `challan_item_volts_cycles` (
  `id` int(11) NOT NULL,
  `volts_cycles_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_roles`
--

CREATE TABLE `chat_roles` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `allowed_staff_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `chat_roles`
--
DELIMITER $$
CREATE TRIGGER `chat_roles_add_log` AFTER INSERT ON `chat_roles` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.chat_roles_log
SET
	`id`=new.id,
    `staff_id`=new.staff_id,
    `allowed_staff_id`=new.allowed_staff_id,
    `created_at`=new.created_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `chat_roles_delete_log` AFTER DELETE ON `chat_roles` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.chat_roles_log
SET
	`id`=old.id,
    `staff_id`=old.staff_id,
    `allowed_staff_id`=old.allowed_staff_id,
    `created_at`=old.created_at,
    `operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `chat_roles_edit_log` AFTER UPDATE ON `chat_roles` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.chat_roles_log
SET
	`id`=new.id,
    `staff_id`=new.staff_id,
    `allowed_staff_id`=new.allowed_staff_id,
    `created_at`=new.created_at,
    `operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `city`
--
DELIMITER $$
CREATE TRIGGER `city_add_log` AFTER INSERT ON `city` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.city_log
SET
	`city_id`=new.city_id,
    `state_id`=new.state_id,
    `country_id`=new.country_id,
    `city`=new.city,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `city_delete_log` AFTER DELETE ON `city` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.city_log
SET
	`city_id`=old.city_id,
    `state_id`=old.state_id,
    `country_id`=old.country_id,
    `city`=old.city,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `city_edit_log` AFTER UPDATE ON `city` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.city_log
SET
	`city_id`=new.city_id,
    `state_id`=new.state_id,
    `country_id`=new.country_id,
    `city`=new.city,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `ceo_name` varchar(225) DEFAULT NULL,
  `m_d_name` varchar(225) DEFAULT NULL,
  `address` text NOT NULL,
  `city` varchar(225) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(15) DEFAULT NULL,
  `country` varchar(225) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `cell_no` varchar(225) DEFAULT NULL,
  `fax_no` varchar(225) DEFAULT NULL,
  `email_id` varchar(225) DEFAULT NULL,
  `gst_no` varchar(255) NOT NULL,
  `tin_no` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) NOT NULL,
  `tan_no` varchar(255) NOT NULL,
  `pf_no` varchar(255) NOT NULL,
  `esic_no` varchar(255) NOT NULL,
  `iec_no` varchar(222) DEFAULT NULL,
  `pt_no` varchar(255) NOT NULL,
  `service_tax_no` varchar(255) NOT NULL,
  `vat_no` varchar(255) NOT NULL,
  `cst_no` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `code` varchar(255) DEFAULT NULL,
  `bank_domestic` varchar(1000) DEFAULT NULL,
  `bank_export` varchar(1000) DEFAULT NULL,
  `margin_left` int(11) DEFAULT NULL,
  `margin_right` int(11) DEFAULT NULL,
  `margin_top` int(11) DEFAULT NULL,
  `margin_bottom` int(11) DEFAULT NULL,
  `invoice_terms` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `company`
--
DELIMITER $$
CREATE TRIGGER `company_add_log` AFTER INSERT ON `company` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.company_log
SET
	`id`=new.id,
	`name`=new.name,
	`ceo_name`=new.ceo_name,
	`m_d_name`=new.m_d_name,
	`address`=new.address,
	`city` =new.city,
	`state` =new.state,
	`pincode` =new.pincode,
	`country` =new.country,
	`contact_no`=new.contact_no,
	`cell_no` =new.cell_no,
	`fax_no` =new.fax_no,
	`email_id` =new.email_id,
	`tin_no` =new.tin_no,
	`pan_no` =new.pan_no,
	`tan_no` =new.tan_no,
	`pf_no` =new.pf_no,
	`esic_no` =new.esic_no,
	`pt_no` =new.pt_no,
	`service_tax_no`=new.service_tax_no, 
	`vat_no` =new.vat_no,
	`cst_no` =new.cst_no,
	`code`=new.code,
	`bank_domestic`=new.bank_domestic,
	`bank_export`=new.bank_export,
	`margin_left`=new.margin_left,
	`margin_right`=new.margin_right,
	`margin_top`=new.margin_top,
	`margin_bottom`=new.margin_bottom,
	`invoice_terms`=new.invoice_terms,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `company_delete_log` AFTER DELETE ON `company` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.company_log
SET
	`id`=old.id,
	`name`=old.name,
	`ceo_name`=old.ceo_name,
	`m_d_name`=old.m_d_name,
	`address`=old.address,
	`city` =old.city,
	`state` =old.state,
	`pincode` =old.pincode,
	`country` =old.country,
	`contact_no`=old.contact_no,
	`cell_no` =old.cell_no,
	`fax_no` =old.fax_no,
	`email_id` =old.email_id,
	`tin_no` =old.tin_no,
	`pan_no` =old.pan_no,
	`tan_no` =old.tan_no,
	`pf_no` =old.pf_no,
	`esic_no` =old.esic_no,
	`pt_no` =old.pt_no,
	`service_tax_no`=old.service_tax_no, 
	`vat_no` =old.vat_no,
	`cst_no` =old.cst_no,
	`code`=old.code,
	`bank_domestic`=old.bank_domestic,
	`bank_export`=old.bank_export,
	`margin_left`=old.margin_left,
	`margin_right`=old.margin_right,
	`margin_top`=old.margin_top,
	`margin_bottom`=old.margin_bottom,
	`invoice_terms`=old.invoice_terms,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `company_edit_log` AFTER UPDATE ON `company` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.company_log
SET
	`id`=new.id,
	`name`=new.name,
	`ceo_name`=new.ceo_name,
	`m_d_name`=new.m_d_name,
	`address`=new.address,
	`city` =new.city,
	`state` =new.state,
	`pincode` =new.pincode,
	`country` =new.country,
	`contact_no`=new.contact_no,
	`cell_no` =new.cell_no,
	`fax_no` =new.fax_no,
	`email_id` =new.email_id,
	`tin_no` =new.tin_no,
	`pan_no` =new.pan_no,
	`tan_no` =new.tan_no,
	`pf_no` =new.pf_no,
	`esic_no` =new.esic_no,
	`pt_no` =new.pt_no,
	`service_tax_no`=new.service_tax_no, 
	`vat_no` =new.vat_no,
	`cst_no` =new.cst_no,
	`code`=new.code,
	`bank_domestic`=new.bank_domestic,
	`bank_export`=new.bank_export,
	`margin_left`=new.margin_left,
	`margin_right`=new.margin_right,
	`margin_top`=new.margin_top,
	`margin_bottom`=new.margin_bottom,
	`invoice_terms`=new.invoice_terms,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `config_key` varchar(255) NOT NULL,
  `config_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `config`
--
DELIMITER $$
CREATE TRIGGER `config_add_log` AFTER INSERT ON `config` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.config_log
SET
	`id`=NEW.id,
	`config_key`=NEW.config_key,
	`config_value`=NEW.config_value,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `config_delete_log` AFTER DELETE ON `config` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.config_log
SET
	
	`id`=OLD.id,
	`config_key`=OLD.config_key,
	`config_value`=OLD.config_value,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `config_edit_log` AFTER UPDATE ON `config` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.config_log
SET
	
	`id`=NEW.id,
	`config_key`=NEW.config_key,
	`config_value`=NEW.config_value,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_person`
--

CREATE TABLE `contact_person` (
  `contact_person_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `phone_no` text,
  `mobile_no` text,
  `fax_no` text,
  `email` varchar(255) NOT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `note` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `contact_person`
--
DELIMITER $$
CREATE TRIGGER `contact_person_add_log` AFTER INSERT ON `contact_person` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_log
SET
	`contact_person_id`=new.contact_person_id,
	`party_id`=new.party_id, 
	`active`=new.active,
	`name`=new.name,
	`phone_no`=new.phone_no,
	`mobile_no`=new.mobile_no,
	`fax_no`=new.fax_no,
	`email`=new.email,
	`designation_id`=new.designation_id,
	`department_id`=new.department_id,
	`note`=new.note,
	`priority`=new.priority,
	`created_by`=new.created_by,
	`updated_by`=new.updated_by,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `contact_person_delete_log` AFTER DELETE ON `contact_person` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_log
SET
	`contact_person_id`=old.contact_person_id,
	`party_id`=old.party_id, 
	`active`=old.active,
	`name`=old.name,
	`phone_no`=old.phone_no,
	`mobile_no`=old.mobile_no,
	`fax_no`=old.fax_no,
	`email`=old.email,
	`designation_id`=old.designation_id,
	`department_id`=old.department_id,
	`note`=old.note,
	`priority`=old.priority,
	`created_by`=old.created_by,
	`updated_by`=old.updated_by,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `contact_person_edit_log` AFTER UPDATE ON `contact_person` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_log
SET
	`contact_person_id`=new.contact_person_id,
	`party_id`=new.party_id, 
	`active`=new.active,
	`name`=new.name,
	`phone_no`=new.phone_no,
	`mobile_no`=new.mobile_no,
	`fax_no`=new.fax_no,
	`email`=new.email,
	`designation_id`=new.designation_id,
	`department_id`=new.department_id,
	`note`=new.note,
	`priority`=new.priority,
	`created_by`=new.created_by,
	`updated_by`=new.updated_by,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_person_log`
--

CREATE TABLE `contact_person_log` (
  `id` int(11) NOT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `mobile_no` varchar(20) DEFAULT NULL,
  `fax_no` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `note` text,
  `priority` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `operation_type` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `country`
--
DELIMITER $$
CREATE TRIGGER `country_add_log` AFTER INSERT ON `country` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.country_log
SET
	`country_id`=new.country_id,
	`country`=new.country,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `country_delete_log` AFTER DELETE ON `country` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.country_log
SET
	`country_id`=old.country_id,
	`country`=old.country,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `country_edit_log` AFTER UPDATE ON `country` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.country_log
SET
	`country_id`=new.country_id,
	`country`=new.country,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `currency`
--
DELIMITER $$
CREATE TRIGGER `currency_add_log` AFTER INSERT ON `currency` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.currency_log
SET
	`id`=new.id,
	`currency`=new.currency,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `currency_delete_log` AFTER DELETE ON `currency` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.currency_log
SET
	`id`=old.id,
	`currency`=old.currency,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `currency_edit_log` AFTER UPDATE ON `currency` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.currency_log
SET
	`id`=new.id,
	`currency`=new.currency,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reminder`
--

CREATE TABLE `customer_reminder` (
  `id` int(11) NOT NULL,
  `topic` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reminder_sent`
--

CREATE TABLE `customer_reminder_sent` (
  `id` int(11) NOT NULL,
  `customer_reminder_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `subject` text,
  `content_data` text,
  `letter_no` int(11) DEFAULT NULL,
  `letter_validity` varchar(255) DEFAULT NULL,
  `pdf_url` varchar(255) DEFAULT NULL,
  `received_payment_detail` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daily_work_entry`
--

CREATE TABLE `daily_work_entry` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `entry_date` date DEFAULT NULL,
  `activity_details` text,
  `created_date` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `daily_work_entry`
--
DELIMITER $$
CREATE TRIGGER `daily_work_entry_add_log` AFTER INSERT ON `daily_work_entry` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.daily_work_entry_log
SET
	`id`=new.id,
	`userid`=new.userid,
	`username`=new.username,
	`entry_date`=new.entry_date,
	`activity_details`=new.activity_details,
	`created_date`=new.created_date,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `daily_work_entry_delete_log` AFTER DELETE ON `daily_work_entry` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.daily_work_entry_log
SET
	`id`=old.id,
	`userid`=old.userid,
	`username`=old.username,
	`entry_date`=old.entry_date,
	`activity_details`=old.activity_details,
	`created_date`=old.created_date,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `daily_work_entry_edit_log` AFTER UPDATE ON `daily_work_entry` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.daily_work_entry_log
SET
	`id`=new.id,
	`userid`=new.userid,
	`username`=new.username,
	`entry_date`=new.entry_date,
	`activity_details`=new.activity_details,
	`created_date`=new.created_date,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `day_id` int(11) NOT NULL,
  `day_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `days`
--
DELIMITER $$
CREATE TRIGGER `days_add_log` AFTER INSERT ON `days` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.days_log
SET
	`day_id`=new.day_id,
	`day_name`=new.day_name,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `days_delete_log` AFTER DELETE ON `days` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.days_log
SET
	`day_id`=old.day_id,
	`day_name`=old.day_name,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `days_edit_log` AFTER UPDATE ON `days` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.days_log
SET
	`day_id`=new.day_id,
	`day_name`=new.day_name,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `default_terms_condition`
--

CREATE TABLE `default_terms_condition` (
  `id` int(11) NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `terms_condition` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `default_terms_condition`
--
DELIMITER $$
CREATE TRIGGER `default_terms_condition_add_log` AFTER INSERT ON `default_terms_condition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.default_terms_condition_log
SET
	`id`=new.id,
	`template_name`=new.template_name,
	`group_name`=new.group_name,
	`terms_condition`=new.terms_condition,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `default_terms_condition_delete_log` AFTER DELETE ON `default_terms_condition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.default_terms_condition_log
SET
	`id`=old.id,
	`template_name`=old.template_name,
	`group_name`=old.group_name,
	`terms_condition`=old.terms_condition,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `default_terms_condition_edit_log` AFTER UPDATE ON `default_terms_condition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.default_terms_condition_log
SET
	`id`=new.id,
	`template_name`=new.template_name,
	`group_name`=new.group_name,
	`terms_condition`=new.terms_condition,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_through`
--

CREATE TABLE `delivery_through` (
  `delivery_through_id` int(11) NOT NULL,
  `delivery_through` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `department`
--
DELIMITER $$
CREATE TRIGGER `department_add_log` AFTER INSERT ON `department` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.department_log
SET
	`department_id`=new.department_id,
	`department`=new.department,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `department_delete_log` AFTER DELETE ON `department` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.department_log
SET
	`department_id`=old.department_id,
	`department`=old.department,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `department_edit_log` AFTER UPDATE ON `department` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.department_log
SET
	`department_id`=new.department_id,
	`department`=new.department,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `designation_id` int(11) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `designation`
--
DELIMITER $$
CREATE TRIGGER `designation_add_log` AFTER INSERT ON `designation` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.designation_log
SET
	`designation_id`=new.designation_id,
	`designation`=new.designation,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `designation_delete_log` AFTER DELETE ON `designation` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.designation_log
SET
	`designation_id`=old.designation_id,
	`designation`=old.designation,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `designation_edit_log` AFTER UPDATE ON `designation` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.designation_log
SET
	`designation_id`=new.designation_id,
	`designation`=new.designation,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `dispatch_followup_history`
--

CREATE TABLE `dispatch_followup_history` (
  `id` int(11) NOT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `followup_date` date DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `dispatch_followup_history`
--
DELIMITER $$
CREATE TRIGGER `dispatch_followup_history_add_log` AFTER INSERT ON `dispatch_followup_history` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.dispatch_followup_history_log
SET
	`id`=new.id,
	`challan_id` =new.challan_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `dispatch_followup_history_delete_log` AFTER DELETE ON `dispatch_followup_history` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.dispatch_followup_history_log
SET
	`id`=old.id,
	`challan_id` =old.challan_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `dispatch_followup_history_edit_log` AFTER UPDATE ON `dispatch_followup_history` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.dispatch_followup_history_log
SET
	`id`=new.id,
	`challan_id` =new.challan_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_number`
--

CREATE TABLE `drawing_number` (
  `id` int(11) NOT NULL,
  `drawing_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `drawing_number`
--
DELIMITER $$
CREATE TRIGGER `drawing_number_add_log` AFTER INSERT ON `drawing_number` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.drawing_number_log
SET
	`id`=new.id,
	`drawing_number`=new.drawing_number,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `drawing_number_delete_log` AFTER DELETE ON `drawing_number` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.drawing_number_log
SET
	`id`=old.id,
	`drawing_number`=old.drawing_number,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `drawing_number_edit_log` AFTER UPDATE ON `drawing_number` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.drawing_number_log
SET
	`id`=new.id,
	`drawing_number`=new.drawing_number,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_leaves`
--

CREATE TABLE `employee_leaves` (
  `leave_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `day` varchar(15) NOT NULL,
  `leave_type` varchar(30) NOT NULL COMMENT 'sl,cl & el/pl',
  `leave_detail` text NOT NULL,
  `leave_status` varchar(15) NOT NULL DEFAULT 'pending' COMMENT 'pending , approve & not approve',
  `leave_note` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `employee_leaves`
--
DELIMITER $$
CREATE TRIGGER `employee_leaves_add_log` AFTER INSERT ON `employee_leaves` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.employee_leaves_log
SET
	`leave_id`=new.leave_id,
	`employee_id`=new.employee_id,
	`from_date`=new.from_date,
	`to_date`=new.to_date,
	`day`=new.day,
	`leave_type`=new.leave_type,
	`leave_detail`=new.leave_detail,
	`leave_status`=new.leave_status,
	`leave_note`=new.leave_note,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `employee_leaves_delete_log` AFTER DELETE ON `employee_leaves` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.employee_leaves_log
SET
	`leave_id`=old.leave_id,
	`employee_id`=old.employee_id,
	`from_date`=old.from_date,
	`to_date`=old.to_date,
	`day`=old.day,
	`leave_type`=old.leave_type,
	`leave_detail`=old.leave_detail,
	`leave_status`=old.leave_status,
	`leave_note`=old.leave_note,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `employee_leaves_edit_log` AFTER UPDATE ON `employee_leaves` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.employee_leaves_log
SET
	`leave_id`=new.leave_id,
	`employee_id`=new.employee_id,
	`from_date`=new.from_date,
	`to_date`=new.to_date,
	`day`=new.day,
	`leave_type`=new.leave_type,
	`leave_detail`=new.leave_detail,
	`leave_status`=new.leave_status,
	`leave_note`=new.leave_note,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `erection_commissioning`
--

CREATE TABLE `erection_commissioning` (
  `id` int(11) NOT NULL,
  `erection_commissioning` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `erection_commissioning`
--
DELIMITER $$
CREATE TRIGGER `erection_commissioning_add_log` AFTER INSERT ON `erection_commissioning` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.erection_commissioning_log
SET
	`id`=new.id,
	`erection_commissioning`=new.erection_commissioning,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `erection_commissioning_delete_log` AFTER DELETE ON `erection_commissioning` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.erection_commissioning_log
SET
	`id`=old.id,
	`erection_commissioning`=old.erection_commissioning,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `erection_commissioning_edit_log` AFTER UPDATE ON `erection_commissioning` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.erection_commissioning_log
SET
	`id`=new.id,
	`erection_commissioning`=new.erection_commissioning,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `excise`
--

CREATE TABLE `excise` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `sub_heading` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `excise`
--
DELIMITER $$
CREATE TRIGGER `excise_add_log` AFTER INSERT ON `excise` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.excise_log
SET
	`id`=new.id,
	`description`=new.description,
	`sub_heading`=new.sub_heading,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `excise_delete_log` AFTER DELETE ON `excise` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.excise_log
SET
	`id`=old.id,
	`description`=old.description,
	`sub_heading`=old.sub_heading,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `excise_edit_log` AFTER UPDATE ON `excise` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.excise_log
SET
	`id`=new.id,
	`description`=new.description,
	`sub_heading`=new.sub_heading,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `followup_category`
--

CREATE TABLE `followup_category` (
  `fc_id` int(11) NOT NULL,
  `fc_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `followup_history`
--

CREATE TABLE `followup_history` (
  `id` int(11) NOT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `reminder_category_id` int(11) DEFAULT NULL,
  `reminder_date` datetime DEFAULT NULL,
  `reminder_message` text,
  `followup_category_id` int(11) DEFAULT NULL,
  `followup_date` datetime DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL,
  `assigned_to` text,
  `is_notified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=not_remind,1= remind'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `followup_history`
--
DELIMITER $$
CREATE TRIGGER `followup_history_add_log` AFTER INSERT ON `followup_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_history_log
SET
	`id`=new.id,
	`quotation_id`=new.quotation_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `followup_history_delete_log` AFTER DELETE ON `followup_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_history_log
SET
	`id`=old.id,
	`quotation_id`=old.quotation_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `followup_history_edit_log` AFTER UPDATE ON `followup_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_history_log
SET
	`id`=new.id,
	`quotation_id`=new.quotation_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `followup_order_history`
--

CREATE TABLE `followup_order_history` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `followup_date` datetime DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `followup_order_history`
--
DELIMITER $$
CREATE TRIGGER `followup_order_history_add_log` AFTER INSERT ON `followup_order_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_order_history_log
SET
	`id`=new.id,
	`order_id`=new.order_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `followup_order_history_delete_log` AFTER DELETE ON `followup_order_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_order_history_log
SET
	`id`=old.id,
	`order_id`=old.order_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `followup_order_history_edit_log` AFTER UPDATE ON `followup_order_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_order_history_log
SET
	`id`=new.id,
	`order_id`=new.order_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `for_required`
--

CREATE TABLE `for_required` (
  `id` int(11) NOT NULL,
  `for_required` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `for_required`
--
DELIMITER $$
CREATE TRIGGER `for_required_add_log` AFTER INSERT ON `for_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.for_required_log
SET
	`id`=new.id,
	`for_required`=new.for_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `for_required_delete_log` AFTER DELETE ON `for_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.for_required_log
SET
	`id`=old.id,
	`for_required`=old.for_required,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `for_required_edit_log` AFTER UPDATE ON `for_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.for_required_log
SET
	`id`=new.id,
	`for_required`=new.for_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `foundation_drawing_required`
--

CREATE TABLE `foundation_drawing_required` (
  `id` int(11) NOT NULL,
  `foundation_drawing_required` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `foundation_drawing_required`
--
DELIMITER $$
CREATE TRIGGER `foundation_drawing_required_add_log` AFTER INSERT ON `foundation_drawing_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.foundation_drawing_required_log
SET
	`id`=new.id,
	`foundation_drawing_required`=new.foundation_drawing_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `foundation_drawing_required_delete_log` AFTER DELETE ON `foundation_drawing_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.foundation_drawing_required_log
SET
	`id`=old.id,
	`foundation_drawing_required`=old.foundation_drawing_required,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `foundation_drawing_required_edit_log` AFTER UPDATE ON `foundation_drawing_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.foundation_drawing_required_log
SET
	`id`=new.id,
	`foundation_drawing_required`=new.foundation_drawing_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `google_sheets`
--

CREATE TABLE `google_sheets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` text,
  `sequence` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `google_sheets`
--
DELIMITER $$
CREATE TRIGGER `google_sheets_add_log` AFTER INSERT ON `google_sheets` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheets_log
SET
	`id`=new.id,
	`name`=new.name,
	`url`=new.url,
	`sequence`=new.sequence,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `google_sheets_delete_log` AFTER DELETE ON `google_sheets` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheets_log
SET
	`id`=old.id,
	`name`=old.name,
	`url`=old.url,
	`sequence`=old.sequence,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `google_sheets_edit_log` AFTER UPDATE ON `google_sheets` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheets_log
SET
	`id`=new.id,
	`name`=new.name,
	`url`=new.url,
	`sequence`=new.sequence,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `google_sheet_staff`
--

CREATE TABLE `google_sheet_staff` (
  `id` int(11) NOT NULL,
  `google_sheet_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `google_sheet_staff`
--
DELIMITER $$
CREATE TRIGGER `google_sheet_staff_add_log` AFTER INSERT ON `google_sheet_staff` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheet_staff_log
SET
	`id`=new.id,
	`google_sheet_id`=new.google_sheet_id,
	`staff_id`=new.staff_id,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `google_sheet_staff_delete_log` AFTER DELETE ON `google_sheet_staff` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheet_staff_log
SET
	`id`=old.id,
	`google_sheet_id`=old.google_sheet_id,
	`staff_id`=old.staff_id,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `google_sheet_staff_edit_log` AFTER UPDATE ON `google_sheet_staff` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheet_staff_log
SET
	`id`=new.id,
	`google_sheet_id`=new.google_sheet_id,
	`staff_id`=new.staff_id,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `grade_id` int(11) NOT NULL,
  `grade` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `grade`
--
DELIMITER $$
CREATE TRIGGER `grade_add_log` AFTER INSERT ON `grade` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.grade_log
SET
`grade_id`=new.grade_id,
`grade`=new.grade,
`created_at`=new.created_at,
`updated_at`=new.updated_at,
`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `grade_delete_log` AFTER DELETE ON `grade` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.grade_log
SET
	`grade_id`=old.grade_id,
	`grade`=old.grade,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `grade_edit_log` AFTER UPDATE ON `grade` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.grade_log
SET
	`grade_id`=new.grade_id,
	`grade`=new.grade,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE `inbox` (
  `mail_id` int(11) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `mail_no` int(11) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `folder_label` varchar(255) DEFAULT NULL,
  `folder_name` varchar(255) DEFAULT NULL,
  `is_sent` tinyint(1) DEFAULT NULL COMMENT '1-Sent 0-Not Sent',
  `is_unread` tinyint(1) DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Starred | 1-Starred',
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_created` tinyint(1) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Updated | 1-Updated',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `staff_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inbox`
--
DELIMITER $$
CREATE TRIGGER `inbox_add_log` AFTER INSERT ON `inbox` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inbox_log
SET
	`mail_id` =new.mail_id,
	`uid` =new.uid,
	`mail_no`=new.mail_no,
	`from_address` =new.from_address,
	`from_name` =new.from_name,
	`to_address` =new.to_address,
	`to_name` =new.to_name,
	`reply_to` =new.reply_to,
	`reply_to_name` =new.reply_to_name,
	`sender_address` =new.sender_address,
	`sender_name` =new.sender_name,
	`cc` =new.cc,
	`bcc` =new.bcc,
	`subject` =new.subject,
	`body` =new.body,
	`attachments` =new.attachments,
	`received_at` =new.received_at,
	`folder_label` =new.folder_label,
	`folder_name` =new.folder_name,
	`is_sent` =new.is_sent,
	`is_unread` =new.is_unread,
	`is_starred` =new.is_starred,
	`is_attachment` =new.is_attachment,
	`is_created` =new.is_created,
	`is_updated` =new.is_updated,
	`is_deleted` =new.is_deleted,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`staff_id` =new.staff_id,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inbox_delete_log` AFTER DELETE ON `inbox` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inbox_log
SET
	`mail_id` =old.mail_id,
	`uid` =old.uid,
	`mail_no`=old.mail_no,
	`from_address` =old.from_address,
	`from_name` =old.from_name,
	`to_address` =old.to_address,
	`to_name` =old.to_name,
	`reply_to` =old.reply_to,
	`reply_to_name` =old.reply_to_name,
	`sender_address` =old.sender_address,
	`sender_name` =old.sender_name,
	`cc` =old.cc,
	`bcc` =old.bcc,
	`subject` =old.subject,
	`body` =old.body,
	`attachments` =old.attachments,
	`received_at` =old.received_at,
	`folder_label` =old.folder_label,
	`folder_name` =old.folder_name,
	`is_sent` =old.is_sent,
	`is_unread` =old.is_unread,
	`is_starred` =old.is_starred,
	`is_attachment` =old.is_attachment,
	`is_created` =old.is_created,
	`is_updated` =old.is_updated,
	`is_deleted` =old.is_deleted,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`staff_id` =old.staff_id,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inbox_edit_log` AFTER UPDATE ON `inbox` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inbox_log
SET
	`mail_id` =new.mail_id,
	`uid` =new.uid,
	`mail_no`=new.mail_no,
	`from_address` =new.from_address,
	`from_name` =new.from_name,
	`to_address` =new.to_address,
	`to_name` =new.to_name,
	`reply_to` =new.reply_to,
	`reply_to_name` =new.reply_to_name,
	`sender_address` =new.sender_address,
	`sender_name` =new.sender_name,
	`cc` =new.cc,
	`bcc` =new.bcc,
	`subject` =new.subject,
	`body` =new.body,
	`attachments` =new.attachments,
	`received_at` =new.received_at,
	`folder_label` =new.folder_label,
	`folder_name` =new.folder_name,
	`is_sent` =new.is_sent,
	`is_unread` =new.is_unread,
	`is_starred` =new.is_starred,
	`is_attachment` =new.is_attachment,
	`is_created` =new.is_created,
	`is_updated` =new.is_updated,
	`is_deleted` =new.is_deleted,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`staff_id` =new.staff_id,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `industry_type`
--

CREATE TABLE `industry_type` (
  `id` int(11) NOT NULL,
  `industry_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `industry_type`
--
DELIMITER $$
CREATE TRIGGER `industry_type_add_log` AFTER INSERT ON `industry_type` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.industry_type_log
SET
	`id` =new.id,
	`industry_type`=new.industry_type,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `industry_type_delete_log` AFTER DELETE ON `industry_type` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.industry_type_log
SET
	`id` =old.id,
	`industry_type`=old.industry_type,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `industry_type_edit_log` AFTER UPDATE ON `industry_type` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.industry_type_log
SET
	`id` =new.id,
	`industry_type`=new.industry_type,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry`
--

CREATE TABLE `inquiry` (
  `inquiry_id` int(11) NOT NULL,
  `inquiry_no` varchar(100) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `reference_date` date DEFAULT NULL,
  `inquiry_date` date DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL COMMENT 'staff_id',
  `follow_up_by_id` int(11) DEFAULT NULL COMMENT 'staff_id',
  `assigned_to_id` int(11) DEFAULT NULL COMMENT 'staff_id',
  `industry_type_id` int(11) DEFAULT NULL,
  `inquiry_stage_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `inquiry_status_id` int(11) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_type_id` int(11) DEFAULT NULL,
  `party_id` int(11) NOT NULL COMMENT 'party_id',
  `received_email` varchar(255) NOT NULL,
  `received_fax` varchar(255) NOT NULL,
  `received_verbal` varchar(255) NOT NULL,
  `is_received_post` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-Yes | 0-No = Checked | Not Checked',
  `send_email` varchar(255) NOT NULL,
  `send_fax` varchar(255) NOT NULL,
  `is_send_post` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-Yes | 0-No = Checked | Not Checked',
  `send_from_post` tinyint(1) DEFAULT '0',
  `send_from_post_name` text,
  `lead_or_inquiry` varchar(222) DEFAULT 'lead',
  `lead_id` int(11) DEFAULT NULL,
  `history` text,
  `created_by` int(11) NOT NULL COMMENT 'user_id , staff_id',
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inquiry`
--
DELIMITER $$
CREATE TRIGGER `inquiry_add_log` AFTER INSERT ON `inquiry` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = new.inquiry_id,
  `inquiry_no` = new.inquiry_no,
  `due_date` = new.due_date,
  `reference_date` = new.reference_date,
  `inquiry_date` = new.inquiry_date,
  `kind_attn_id` = new.kind_attn_id,
  `follow_up_by_id` = new.follow_up_by_id,
  `assigned_to_id` = new.assigned_to_id,
  `industry_type_id` = new.industry_type_id,
  `inquiry_stage_id` = new.inquiry_stage_id,
  `branch_id` = new.branch_id,
  `inquiry_status_id` = new.inquiry_status_id,
  `sales_id` = new.sales_id,
  `sales_type_id` = new.sales_type_id,
  `party_id` = new.party_id,
  `received_email` = new.received_email,
  `received_fax` = new.received_fax,
  `received_verbal` = new.received_verbal,
  `is_received_post` = new.is_received_post,
  `send_email` = new.send_email,
  `send_fax` = new.send_fax,
  `is_send_post` = new.is_send_post,
  `send_from_post_name` = new.send_from_post_name,
  `lead_or_inquiry` = new.lead_or_inquiry,
  `lead_id` = new.lead_id,
  `history` = new.history,
  `created_by` = new.created_by,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `updated_by` = new.updated_by,
  `operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_delete_log` AFTER DELETE ON `inquiry` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = old.inquiry_id,
  `inquiry_no` = old.inquiry_no,
  `due_date` = old.due_date,
  `reference_date` = old.reference_date,
  `inquiry_date` = old.inquiry_date,
  `kind_attn_id` = old.kind_attn_id,
  `follow_up_by_id` = old.follow_up_by_id,
  `assigned_to_id` = old.assigned_to_id,
  `industry_type_id` = old.industry_type_id,
  `inquiry_stage_id` = old.inquiry_stage_id,
  `branch_id` = old.branch_id,
  `inquiry_status_id` = old.inquiry_status_id,
  `sales_id` = old.sales_id,
  `sales_type_id` = old.sales_type_id,
  `party_id` = old.party_id,
  `received_email` = old.received_email,
  `received_fax` = old.received_fax,
  `received_verbal` = old.received_verbal,
  `is_received_post` = old.is_received_post,
  `send_email` = old.send_email,
  `send_fax` = old.send_fax,
  `is_send_post` = old.is_send_post,
  `send_from_post_name` = old.send_from_post_name,
  `lead_or_inquiry` = old.lead_or_inquiry,
  `lead_id` = old.lead_id,
  `history` = old.history,
  `created_by` = old.created_by,
  `created_at` = old.created_at,
  `updated_at` = old.updated_at,
  `updated_by` = old.updated_by,
  `operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_edit_log` AFTER UPDATE ON `inquiry` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = new.inquiry_id,
  `inquiry_no` = new.inquiry_no,
  `due_date` = new.due_date,
  `reference_date` = new.reference_date,
  `inquiry_date` = new.inquiry_date,
  `kind_attn_id` = new.kind_attn_id,
  `follow_up_by_id` = new.follow_up_by_id,
  `assigned_to_id` = new.assigned_to_id,
  `industry_type_id` = new.industry_type_id,
  `inquiry_stage_id` = new.inquiry_stage_id,
  `branch_id` = new.branch_id,
  `inquiry_status_id` = new.inquiry_status_id,
  `sales_id` = new.sales_id,
  `sales_type_id` = new.sales_type_id,
  `party_id` = new.party_id,
  `received_email` = new.received_email,
  `received_fax` = new.received_fax,
  `received_verbal` = new.received_verbal,
  `is_received_post` = new.is_received_post,
  `send_email` = new.send_email,
  `send_fax` = new.send_fax,
  `is_send_post` = new.is_send_post,
  `send_from_post_name` = new.send_from_post_name,
  `lead_or_inquiry` = new.lead_or_inquiry,
  `lead_id` = new.lead_id,
  `history` = new.history,
  `created_by` = new.created_by,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `updated_by` = new.updated_by,
  `operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_confirmation`
--

CREATE TABLE `inquiry_confirmation` (
  `id` int(11) NOT NULL,
  `inquiry_confirmation` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inquiry_confirmation`
--
DELIMITER $$
CREATE TRIGGER `inquiry_confirmation_add_log` AFTER INSERT ON `inquiry_confirmation` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_confirmation_log
SET
	`id` =new.id,
	`inquiry_confirmation`=new.inquiry_confirmation,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_confirmation_delete_log` AFTER DELETE ON `inquiry_confirmation` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_confirmation_log
SET
	`id` =old.id,
	`inquiry_confirmation`=old.inquiry_confirmation,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_confirmation_edit_log` AFTER UPDATE ON `inquiry_confirmation` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_confirmation_log
SET
	`id` =new.id,
	`inquiry_confirmation`=new.inquiry_confirmation,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_followup_history`
--

CREATE TABLE `inquiry_followup_history` (
  `id` int(11) NOT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `followup_date` datetime DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inquiry_followup_history`
--
DELIMITER $$
CREATE TRIGGER `inquiry_followup_history_add_log` AFTER INSERT ON `inquiry_followup_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_followup_history_log
SET
	`id` =new.id,
	`inquiry_id`=new.inquiry_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_followup_history_delete_log` AFTER DELETE ON `inquiry_followup_history` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_followup_history_log
SET
	`id` =old.id,
	`inquiry_id`=old.inquiry_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_followup_history_edit_log` AFTER UPDATE ON `inquiry_followup_history` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_followup_history_log
SET
	`id` =new.id,
	`inquiry_id`=new.inquiry_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_items`
--

CREATE TABLE `inquiry_items` (
  `id` int(11) NOT NULL,
  `inquiry_id` int(11) NOT NULL,
  `item_data` text NOT NULL,
  `inquiry_flag` int(11) DEFAULT '0',
  `quotation_flag` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inquiry_items`
--
DELIMITER $$
CREATE TRIGGER `inquiry_item_add_log` AFTER INSERT ON `inquiry_items` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_log
SET
  `id` = new.id,
  `inquiry_id` = new.inquiry_id,
  `item_data` = new.item_data,
  `inquiry_flag` = new.inquiry_flag,
  `quotation_flag` = new.quotation_flag,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_item_delete_log` AFTER DELETE ON `inquiry_items` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_log
SET
  `id` = old.id,
  `inquiry_id` = old.inquiry_id,
  `item_data` = old.item_data,
  `inquiry_flag` = old.inquiry_flag,
  `quotation_flag` = old.quotation_flag,
  `created_at` = old.created_at,
  `updated_at` = old.updated_at,
  `operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_item_edit_log` AFTER UPDATE ON `inquiry_items` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_log
SET
  `id` = new.id,
  `inquiry_id` = new.inquiry_id,
  `item_data` = new.item_data,
  `inquiry_flag` = new.inquiry_flag,
  `quotation_flag` = new.quotation_flag,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_stage`
--

CREATE TABLE `inquiry_stage` (
  `id` int(11) NOT NULL,
  `inquiry_stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inquiry_stage`
--
DELIMITER $$
CREATE TRIGGER `inquiry_stage_add_log` AFTER INSERT ON `inquiry_stage` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_stage_log
SET
	`id` =new.id,
	`inquiry_stage`=new.inquiry_stage,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_stage_delete_log` AFTER DELETE ON `inquiry_stage` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_stage_log
SET
	`id`=old.id,
	`inquiry_stage`=old.inquiry_stage,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_stage_edit_log` AFTER UPDATE ON `inquiry_stage` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_stage_log
SET
	`id`=new.id,
	`inquiry_stage`=new.inquiry_stage,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_status`
--

CREATE TABLE `inquiry_status` (
  `id` int(11) NOT NULL,
  `inquiry_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inquiry_status`
--
DELIMITER $$
CREATE TRIGGER `inquiry_status_add_log` AFTER INSERT ON `inquiry_status` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_status_log
SET
	`id` =new.id,
	`inquiry_status`=new.inquiry_status,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_status_delete_log` AFTER DELETE ON `inquiry_status` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_status_log
SET
	`id`=old.id,
	`inquiry_status`=old.inquiry_status,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inquiry_status_edit_log` AFTER UPDATE ON `inquiry_status` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_status_log
SET
	`id`=new.id,
	`inquiry_status`=new.inquiry_status,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_required`
--

CREATE TABLE `inspection_required` (
  `id` int(11) NOT NULL,
  `inspection_required` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `inspection_required`
--
DELIMITER $$
CREATE TRIGGER `inspection_required_add_log` AFTER INSERT ON `inspection_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inspection_required_log
SET
	`id` =new.id,
	`inspection_required`=new.inspection_required,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inspection_required_delete_log` AFTER DELETE ON `inspection_required` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inspection_required_log
SET
	`id`=old.id,
	`inspection_required`=old.inspection_required,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `inspection_required_edit_log` AFTER UPDATE ON `inspection_required` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inspection_required_log
SET
	`id`=new.id,
	`inspection_required`=new.inspection_required,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `interview`
--

CREATE TABLE `interview` (
  `interview_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` text,
  `mailbox_email` varchar(255) DEFAULT NULL,
  `mailbox_password` text,
  `image` text NOT NULL,
  `birth_date` date DEFAULT NULL,
  `qualification` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `husband_wife_name` varchar(255) NOT NULL,
  `blood_group` varchar(25) NOT NULL,
  `contact_no` varchar(30) NOT NULL,
  `married_status` varchar(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `interview_review` text NOT NULL,
  `interview_decision` varchar(25) NOT NULL COMMENT '0-Employee Rejcted | 1-Employee Selected | 2-Employee Hold | 3-Employee Pipeline',
  `designation_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL COMMENT 'department',
  `grade_id` int(11) NOT NULL,
  `date_of_joining` date DEFAULT NULL,
  `date_of_leaving` date DEFAULT NULL,
  `allow_pf` varchar(10) NOT NULL,
  `salary` double NOT NULL,
  `basic_pay` double NOT NULL,
  `house_rent` double NOT NULL,
  `traveling_allowance` double NOT NULL,
  `education_allowance` double NOT NULL,
  `pf_amount` double NOT NULL,
  `bonus_amount` double NOT NULL,
  `other_allotment` double NOT NULL,
  `medical_reimbursement` double NOT NULL,
  `professional_tax` double NOT NULL,
  `monthly_gross` double NOT NULL,
  `cost_to_company` double NOT NULL,
  `pf_employee` double NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `esic_no` varchar(255) NOT NULL,
  `emp_id` varchar(255) NOT NULL,
  `pan_no` varchar(255) NOT NULL,
  `bank_acc_no` varchar(255) NOT NULL,
  `uan_id` varchar(255) NOT NULL,
  `pf_no` varchar(255) NOT NULL,
  `emp_no` varchar(255) NOT NULL,
  `esic_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `interview`
--
DELIMITER $$
CREATE TRIGGER `interview_add_log` AFTER INSERT ON `interview` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.interview_log
SET
		`interview_id`=new.interview_id,
		`name`=new.name,
		`email`=new.email,
		`pass`=new.pass,
		`mailbox_email`=new.mailbox_email,
		`mailbox_password`=new.mailbox_password,
		`image`=new.image,
		`birth_date`=new.birth_date,
		`qualification`=new.qualification,
		`father_name`=new.father_name,
		`mother_name`=new.mother_name,
		`husband_wife_name`=new.husband_wife_name,
		`blood_group`=new.blood_group,
		`contact_no`=new.contact_no,
		`married_status`=new.married_status,
		`gender`=new.gender,
		`address`=new.address,
		`permanent_address`=new.permanent_address,
		`interview_review`=new.interview_review,
		`interview_decision`=new.interview_decision,
		`designation_id`=new.designation_id,
		`department_id`=new.department_id,
		`grade_id`=new.grade_id,
		`date_of_joining`=new.date_of_joining,
		`date_of_leaving`=new.date_of_leaving,
		`allow_pf`=new.allow_pf,
		`salary`=new.salary,
		`basic_pay`=new.basic_pay,
		`house_rent`=new.house_rent,
		`traveling_allowance`=new.traveling_allowance,
		`education_allowance`=new.education_allowance,
		`pf_amount`=new.pf_amount,
		`bonus_amount`=new.bonus_amount,
		`other_allotment`=new.other_allotment,
		`medical_reimbursement`=new.medical_reimbursement,
		`professional_tax`=new.professional_tax,
		`monthly_gross`=new.monthly_gross,
		`cost_to_company`=new.cost_to_company,
		`pf_employee`=new.pf_employee,
		`bank_name`=new.bank_name,
		`esic_no`=new.esic_no,
		`emp_id`=new.emp_id,
		`pan_no`=new.pan_no,
		`bank_acc_no`=new.bank_acc_no,
		`uan_id`=new.uan_id,
		`pf_no`=new.pf_no,
		`emp_no`=new.emp_no,
		`esic_id`=new.esic_id,
		`created_at`=new.created_at,
		`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `interview_delete_log` AFTER DELETE ON `interview` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.interview_log
SET
	`interview_id`=old.interview_id,
	`name`=old.name,
	`email`=old.email,
	`pass`=old.pass,
	`mailbox_email`=old.mailbox_email,
	`mailbox_password`=old.mailbox_password,
	`image`=old.image,
	`birth_date`=old.birth_date,
	`qualification`=old.qualification,
	`father_name`=old.father_name,
	`mother_name`=old.mother_name,
	`husband_wife_name`=old.husband_wife_name,
	`blood_group`=old.blood_group,
	`contact_no`=old.contact_no,
	`married_status`=old.married_status,
	`gender`=old.gender,
	`address`=old.address,
	`permanent_address`=old.permanent_address,
	`interview_review`=old.interview_review,
	`interview_decision`=old.interview_decision,
	`designation_id`=old.designation_id,
	`department_id`=old.department_id,
	`grade_id`=old.grade_id,
	`date_of_joining`=old.date_of_joining,
	`date_of_leaving`=old.date_of_leaving,
	`allow_pf`=old.allow_pf,
	`salary`=old.salary,
	`basic_pay`=old.basic_pay,
	`house_rent`=old.house_rent,
	`traveling_allowance`=old.traveling_allowance,
	`education_allowance`=old.education_allowance,
	`pf_amount`=old.pf_amount,
	`bonus_amount`=old.bonus_amount,
	`other_allotment`=old.other_allotment,
	`medical_reimbursement`=old.medical_reimbursement,
	`professional_tax`=old.professional_tax,
	`monthly_gross`=old.monthly_gross,
	`cost_to_company`=old.cost_to_company,
	`pf_employee`=old.pf_employee,
	`bank_name`=old.bank_name,
	`esic_no`=old.esic_no,
	`emp_id`=old.emp_id,
	`pan_no`=old.pan_no,
	`bank_acc_no`=old.bank_acc_no,
	`uan_id`=old.uan_id,
	`pf_no`=old.pf_no,
	`emp_no`=old.emp_no,
	`esic_id`=old.esic_id,
	`created_at`=old.created_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `interview_edit_log` AFTER UPDATE ON `interview` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.interview_log
SET
	`interview_id`=new.interview_id,
	`name`=new.name,
	`email`=new.email,
	`pass`=new.pass,
	`mailbox_email`=new.mailbox_email,
	`mailbox_password`=new.mailbox_password,
	`image`=new.image,
	`birth_date`=new.birth_date,
	`qualification`=new.qualification,
	`father_name`=new.father_name,
	`mother_name`=new.mother_name,
	`husband_wife_name`=new.husband_wife_name,
	`blood_group`=new.blood_group,
	`contact_no`=new.contact_no,
	`married_status`=new.married_status,
	`gender`=new.gender,
	`address`=new.address,
	`permanent_address`=new.permanent_address,
	`interview_review`=new.interview_review,
	`interview_decision`=new.interview_decision,
	`designation_id`=new.designation_id,
	`department_id`=new.department_id,
	`grade_id`=new.grade_id,
	`date_of_joining`=new.date_of_joining,
	`date_of_leaving`=new.date_of_leaving,
	`allow_pf`=new.allow_pf,
	`salary`=new.salary,
	`basic_pay`=new.basic_pay,
	`house_rent`=new.house_rent,
	`traveling_allowance`=new.traveling_allowance,
	`education_allowance`=new.education_allowance,
	`pf_amount`=new.pf_amount,
	`bonus_amount`=new.bonus_amount,
	`other_allotment`=new.other_allotment,
	`medical_reimbursement`=new.medical_reimbursement,
	`professional_tax`=new.professional_tax,
	`monthly_gross`=new.monthly_gross,
	`cost_to_company`=new.cost_to_company,
	`pf_employee`=new.pf_employee,
	`bank_name`=new.bank_name,
	`esic_no`=new.esic_no,
	`emp_id`=new.emp_id,
	`pan_no`=new.pan_no,
	`bank_acc_no`=new.bank_acc_no,
	`uan_id`=new.uan_id,
	`pf_no`=new.pf_no,
	`emp_no`=new.emp_no,
	`esic_id`=new.esic_id,
	`created_at`=new.created_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `invoice_type_id` int(11) DEFAULT NULL,
  `challan_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `total_cmount` double NOT NULL,
  `quatation_id` int(11) NOT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `sales_order_no` varchar(255) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `ship_to_party_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `billing_terms_id` int(11) DEFAULT NULL,
  `sales_order_date` date DEFAULT NULL,
  `committed_date` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `contact_no` varchar(15) NOT NULL,
  `cust_po_no` varchar(50) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `po_date` date NOT NULL,
  `sales_order_pref_id` int(11) DEFAULT NULL,
  `conversation_rate` varchar(50) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_order_stage_id` int(11) DEFAULT NULL,
  `sales_order_status_id` int(11) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `buyer_detail_id` int(11) DEFAULT NULL,
  `note_detail_id` int(11) DEFAULT NULL,
  `login_detail_id` int(11) DEFAULT NULL,
  `transportation_by_id` int(11) DEFAULT NULL,
  `foundation_drawing_required_id` int(11) DEFAULT NULL,
  `loading_by_id` int(11) DEFAULT NULL,
  `inspection_required_id` int(11) DEFAULT NULL,
  `unloading_by_id` int(11) DEFAULT NULL,
  `erection_commissioning_id` int(11) DEFAULT NULL,
  `road_insurance_by_id` int(11) DEFAULT NULL,
  `for_required_id` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `mach_deli_min_weeks` int(11) DEFAULT NULL,
  `mach_deli_max_weeks` int(11) DEFAULT NULL,
  `mach_deli_min_weeks_date` date DEFAULT NULL,
  `mach_deli_max_weeks_date` date DEFAULT NULL,
  `sales_order_validaty` varchar(225) DEFAULT NULL,
  `sales_order_quotation_delivery` varchar(225) DEFAULT NULL,
  `mode_of_shipment_name` varchar(225) DEFAULT NULL,
  `mode_of_shipment_truck_number` varchar(225) DEFAULT NULL,
  `supplier_payment_terms` text,
  `received_payment` varchar(255) DEFAULT NULL,
  `received_payment_date` date DEFAULT NULL,
  `received_payment_type` varchar(255) DEFAULT NULL,
  `received_payment_cheque_no` varchar(255) DEFAULT NULL,
  `terms_condition_purchase` text,
  `isApproved` int(11) DEFAULT '0',
  `pdf_url` text,
  `lr_no` varchar(255) NOT NULL,
  `lr_date` date DEFAULT NULL,
  `vehicle_no` varchar(50) NOT NULL,
  `notes` text,
  `against_form` int(1) NOT NULL,
  `taxes_data` text,
  `loading_at` varchar(255) DEFAULT NULL,
  `port_of_loading` varchar(255) DEFAULT NULL,
  `port_of_discharge` varchar(255) DEFAULT NULL,
  `port_place_of_delivery` varchar(255) DEFAULT NULL,
  `delivery_through_id` int(11) DEFAULT NULL,
  `name_of_shipment` varchar(255) DEFAULT NULL,
  `freight` varchar(255) DEFAULT NULL,
  `freight_cgst` int(11) DEFAULT NULL,
  `freight_sgst` int(11) DEFAULT NULL,
  `freight_igst` int(11) DEFAULT NULL,
  `packing_forwarding` varchar(255) DEFAULT NULL,
  `packing_forwarding_cgst` int(11) DEFAULT NULL,
  `packing_forwarding_sgst` int(11) DEFAULT NULL,
  `packing_forwarding_igst` int(11) DEFAULT NULL,
  `transit_insurance` varchar(255) DEFAULT NULL,
  `transit_insurance_cgst` int(11) DEFAULT NULL,
  `transit_insurance_sgst` int(11) DEFAULT NULL,
  `transit_insurance_igst` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `invoices`
--
DELIMITER $$
CREATE TRIGGER `invoices_add_log` AFTER INSERT ON `invoices` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoices_log
SET
`id`=new.id,
	`invoice_type_id` =NEW.invoice_type_id,
	`sales_order_id` =NEW.sales_order_id,
	`total_cmount` =NEW.total_cmount,
	`quatation_id` =NEW.quatation_id,
	`quotation_id` =NEW.quotation_id,
	`invoice_no` =NEW.invoice_no,
	`sales_order_no` =NEW.sales_order_no,
	`sales_to_party_id` =NEW.sales_to_party_id,
	`ship_to_party_id` =NEW.ship_to_party_id,
	`branch_id` =NEW.branch_id,
	`billing_terms_id` =NEW.billing_terms_id,
	`sales_order_date` =NEW.sales_order_date,
	`committed_date` =NEW.committed_date,
	`currency_id` =NEW.currency_id,
	`project_id` =NEW.project_id,
	`kind_attn_id` =NEW.kind_attn_id,
	`contact_no` =NEW.contact_no,
	`cust_po_no` =NEW.cust_po_no,
	`email_id` =NEW.email_id,
	`po_date` =NEW.po_date,
	`sales_order_pref_id` =NEW.sales_order_pref_id,
	`conversation_rate` =NEW.conversation_rate,
	`sales_id` =NEW.sales_id,
	`sales_order_stage_id` =NEW.sales_order_stage_id,
	`sales_order_status_id` =NEW.sales_order_status_id,
	`order_type` =NEW.order_type,
	`lead_provider_id` =NEW.lead_provider_id,
	`buyer_detail_id` =NEW.buyer_detail_id,
	`note_detail_id` =NEW.note_detail_id,
	`login_detail_id` =NEW.login_detail_id,
	`transportation_by_id` =NEW.transportation_by_id,
	`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
	`loading_by_id` =NEW.loading_by_id,
	`inspection_required_id` =NEW.inspection_required_id,
	`unloading_by_id` =NEW.unloading_by_id,
	`erection_commissioning_id` =NEW.erection_commissioning_id,
	`road_insurance_by_id` =NEW.road_insurance_by_id,
	`for_required_id` =NEW.for_required_id,
	`review_date` =NEW.review_date,
	`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
	`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
	`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
	`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
	`sales_order_validaty` =NEW.sales_order_validaty,
	`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
	`mode_of_shipment_name` =NEW.mode_of_shipment_name,
	`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
	`supplier_payment_terms` =NEW.supplier_payment_terms,
	`received_payment` =NEW.received_payment,
	`received_payment_date` =NEW.received_payment_date,
	`received_payment_type` =NEW.received_payment_type,
	`terms_condition_purchase` =NEW.terms_condition_purchase,
	`isApproved`=NEW.isApproved,
	`pdf_url` =NEW.pdf_url,
	`lr_no` =NEW.lr_no,
	`lr_date` =NEW.lr_date,
	`vehicle_no` =NEW.vehicle_no,
	`against_form`=NEW.against_form,
	`taxes_data` =NEW.taxes_data,
`created_at`=NEW.created_at,
`updated_at`=NEW.updated_at,
	`created_by` =NEW.created_by,
`operation_type` ='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoices_delete_log` AFTER DELETE ON `invoices` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoices_log
SET
`id`=OLD.id,
	`invoice_type_id` =OLD.invoice_type_id,
	`sales_order_id` =OLD.sales_order_id,
	`total_cmount` =OLD.total_cmount,
	`quatation_id` =OLD.quatation_id,
	`quotation_id` =OLD.quotation_id,
	`invoice_no` =OLD.invoice_no,
	`sales_order_no` =OLD.sales_order_no,
	`sales_to_party_id` =OLD.sales_to_party_id,
	`ship_to_party_id` =OLD.ship_to_party_id,
	`branch_id` =OLD.branch_id,
	`billing_terms_id` =OLD.billing_terms_id,
	`sales_order_date` =OLD.sales_order_date,
	`committed_date` =OLD.committed_date,
	`currency_id` =OLD.currency_id,
	`project_id` =OLD.project_id,
	`kind_attn_id` =OLD.kind_attn_id,
	`contact_no` =OLD.contact_no,
	`cust_po_no` =OLD.cust_po_no,
	`email_id` =OLD.email_id,
	`po_date` =OLD.po_date,
	`sales_order_pref_id` =OLD.sales_order_pref_id,
	`conversation_rate` =OLD.conversation_rate,
	`sales_id` =OLD.sales_id,
	`sales_order_stage_id` =OLD.sales_order_stage_id,
	`sales_order_status_id` =OLD.sales_order_status_id,
	`order_type` =OLD.order_type,
	`lead_provider_id` =OLD.lead_provider_id,
	`buyer_detail_id` =OLD.buyer_detail_id,
	`note_detail_id` =OLD.note_detail_id,
	`login_detail_id` =OLD.login_detail_id,
	`transportation_by_id` =OLD.transportation_by_id,
	`foundation_drawing_required_id` =OLD.foundation_drawing_required_id,
	`loading_by_id` =OLD.loading_by_id,
	`inspection_required_id` =OLD.inspection_required_id,
	`unloading_by_id` =OLD.unloading_by_id,
	`erection_commissioning_id` =OLD.erection_commissioning_id,
	`road_insurance_by_id` =OLD.road_insurance_by_id,
	`for_required_id` =OLD.for_required_id,
	`review_date` =OLD.review_date,
	`mach_deli_min_weeks` =OLD.mach_deli_min_weeks,
	`mach_deli_max_weeks` =OLD.mach_deli_max_weeks,
	`mach_deli_min_weeks_date` =OLD.mach_deli_min_weeks_date,
	`mach_deli_max_weeks_date` =OLD.mach_deli_max_weeks_date,
	`sales_order_validaty` =OLD.sales_order_validaty,
	`sales_order_quotation_delivery` =OLD.sales_order_quotation_delivery,
	`mode_of_shipment_name` =OLD.mode_of_shipment_name,
	`mode_of_shipment_truck_number` =OLD.mode_of_shipment_truck_number,
	`supplier_payment_terms` =OLD.supplier_payment_terms,
	`received_payment` =OLD.received_payment,
	`received_payment_date` =OLD.received_payment_date,
	`received_payment_type` =OLD.received_payment_type,
	`terms_condition_purchase` =OLD.terms_condition_purchase,
	`isApproved`=OLD.isApproved,
	`pdf_url` =OLD.pdf_url,
	`lr_no` =OLD.lr_no,
	`lr_date` =OLD.lr_date,
	`vehicle_no` =OLD.vehicle_no,
	`against_form`=OLD.against_form,
	`taxes_data` =OLD.taxes_data,
`created_at`=OLD.created_at,
`updated_at`=OLD.updated_at,
	`created_by` =OLD.created_by,
`operation_type` ='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoices_edit_log` AFTER UPDATE ON `invoices` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoices_log
SET
`id`=new.id,
	`invoice_type_id` =NEW.invoice_type_id,
	`sales_order_id` =NEW.sales_order_id,
	`total_cmount` =NEW.total_cmount,
	`quatation_id` =NEW.quatation_id,
	`quotation_id` =NEW.quotation_id,
	`invoice_no` =NEW.invoice_no,
	`sales_order_no` =NEW.sales_order_no,
	`sales_to_party_id` =NEW.sales_to_party_id,
	`ship_to_party_id` =NEW.ship_to_party_id,
	`branch_id` =NEW.branch_id,
	`billing_terms_id` =NEW.billing_terms_id,
	`sales_order_date` =NEW.sales_order_date,
	`committed_date` =NEW.committed_date,
	`currency_id` =NEW.currency_id,
	`project_id` =NEW.project_id,
	`kind_attn_id` =NEW.kind_attn_id,
	`contact_no` =NEW.contact_no,
	`cust_po_no` =NEW.cust_po_no,
	`email_id` =NEW.email_id,
	`po_date` =NEW.po_date,
	`sales_order_pref_id` =NEW.sales_order_pref_id,
	`conversation_rate` =NEW.conversation_rate,
	`sales_id` =NEW.sales_id,
	`sales_order_stage_id` =NEW.sales_order_stage_id,
	`sales_order_status_id` =NEW.sales_order_status_id,
	`order_type` =NEW.order_type,
	`lead_provider_id` =NEW.lead_provider_id,
	`buyer_detail_id` =NEW.buyer_detail_id,
	`note_detail_id` =NEW.note_detail_id,
	`login_detail_id` =NEW.login_detail_id,
	`transportation_by_id` =NEW.transportation_by_id,
	`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
	`loading_by_id` =NEW.loading_by_id,
	`inspection_required_id` =NEW.inspection_required_id,
	`unloading_by_id` =NEW.unloading_by_id,
	`erection_commissioning_id` =NEW.erection_commissioning_id,
	`road_insurance_by_id` =NEW.road_insurance_by_id,
	`for_required_id` =NEW.for_required_id,
	`review_date` =NEW.review_date,
	`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
	`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
	`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
	`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
	`sales_order_validaty` =NEW.sales_order_validaty,
	`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
	`mode_of_shipment_name` =NEW.mode_of_shipment_name,
	`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
	`supplier_payment_terms` =NEW.supplier_payment_terms,
	`received_payment` =NEW.received_payment,
	`received_payment_date` =NEW.received_payment_date,
	`received_payment_type` =NEW.received_payment_type,
	`terms_condition_purchase` =NEW.terms_condition_purchase,
	`isApproved`=NEW.isApproved,
	`pdf_url` =NEW.pdf_url,
	`lr_no` =NEW.lr_no,
	`lr_date` =NEW.lr_date,
	`vehicle_no` =NEW.vehicle_no,
	`against_form`=NEW.against_form,
	`taxes_data` =NEW.taxes_data,
`created_at`=NEW.created_at,
`updated_at`=NEW.updated_at,
	`created_by` =NEW.created_by,
`operation_type` ='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_billing_terms`
--

CREATE TABLE `invoice_billing_terms` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `invoice_billing_terms`
--
DELIMITER $$
CREATE TRIGGER `invoice_billing_terms_add_log` AFTER INSERT ON `invoice_billing_terms` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_billing_terms_log
SET
	`id`=new.id,
	`invoice_id` =NEW.invoice_id,
	`cal_code` =NEW.cal_code,
	`narration` =NEW.narration,
	`cal_definition` =NEW.cal_definition,
	`percentage` =NEW.percentage,
	`value` =NEW.value,
	`gl_acc` =NEW.gl_acc,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_billing_terms_delete_log` AFTER DELETE ON `invoice_billing_terms` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_billing_terms_log
SET
	`id`=OLD.id,
	`invoice_id` =OLD.invoice_id,
	`cal_code` =OLD.cal_code,
	`narration` =OLD.narration,
	`cal_definition` =OLD.cal_definition,
	`percentage` =OLD.percentage,
	`value` =OLD.value,
	`gl_acc` =OLD.gl_acc,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type` ='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_billing_terms_edit_log` AFTER UPDATE ON `invoice_billing_terms` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_billing_terms_log
SET
	`id`=new.id,
	`invoice_id` =NEW.invoice_id,
	`cal_code` =NEW.cal_code,
	`narration` =NEW.narration,
	`cal_definition` =NEW.cal_definition,
	`percentage` =NEW.percentage,
	`value` =NEW.value,
	`gl_acc` =NEW.gl_acc,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_cal_code_definition`
--

CREATE TABLE `invoice_cal_code_definition` (
  `id` int(11) NOT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `invoice_cal_code_definition`
--
DELIMITER $$
CREATE TRIGGER `invoice_cal_code_definition_add_log` AFTER INSERT ON `invoice_cal_code_definition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_cal_code_definition_log
SET

	`id`=new.id,
	`billing_terms_detail_id` =NEW.billing_terms_detail_id,
	`invoice_id` =NEW.invoice_id,
	`is_first_element` =NEW.is_first_element,
	`cal_operation` =NEW.cal_operation,
	`cal_code_id` =NEW.cal_code_id,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_cal_code_definition_delete_log` AFTER DELETE ON `invoice_cal_code_definition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_cal_code_definition_log
SET

	`id`=OLD.id,
	`billing_terms_detail_id` =OLD.billing_terms_detail_id,
	`invoice_id` =OLD.invoice_id,
	`is_first_element` =OLD.is_first_element,
	`cal_operation` =OLD.cal_operation,
	`cal_code_id` =OLD.cal_code_id,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type` ='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_cal_code_definition_edit_log` AFTER UPDATE ON `invoice_cal_code_definition` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_cal_code_definition_log
SET

	`id`=new.id,
	`billing_terms_detail_id` =NEW.billing_terms_detail_id,
	`invoice_id` =NEW.invoice_id,
	`is_first_element` =NEW.is_first_element,
	`cal_operation` =NEW.cal_operation,
	`cal_code_id` =NEW.cal_code_id,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `item_description` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `drawing_revision` varchar(255) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) NOT NULL,
  `igst` int(11) DEFAULT NULL,
  `igst_amount` int(11) DEFAULT NULL,
  `cgst` int(11) DEFAULT NULL,
  `cgst_amount` int(11) DEFAULT NULL,
  `sgst` int(11) DEFAULT NULL,
  `sgst_amount` int(11) DEFAULT NULL,
  `total_amount` double NOT NULL,
  `item_note` text NOT NULL,
  `quantity` double NOT NULL,
  `disc_per` double NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `rate` double NOT NULL,
  `disc_value` double NOT NULL,
  `amount` double NOT NULL,
  `net_amount` double NOT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) NOT NULL,
  `cust_part_no` varchar(255) NOT NULL,
  `item_serial_no` varchar(255) DEFAULT NULL,
  `truck_container_no` varchar(255) DEFAULT NULL,
  `lr_bl_no` varchar(255) DEFAULT NULL,
  `lr_date` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `driver_name` varchar(255) DEFAULT NULL,
  `driver_contact_no` varchar(255) DEFAULT NULL,
  `item_extra_accessories_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `invoice_items`
--
DELIMITER $$
CREATE TRIGGER `invoice_items_add_log` AFTER INSERT ON `invoice_items` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.invoice_items_log
SET
	`id`=new.id,
	`item_id` =NEW.item_id,
	`invoice_id` =NEW.invoice_id,
	`item_code` =NEW.item_code,
	`item_category_id` =NEW.item_category_id,
	`item_name` =NEW.item_name,
	`item_desc` =NEW.item_desc,
	`item_description` =NEW.item_description,
	`add_description` =NEW.add_description,
	`detail_description` =NEW.detail_description,
	`drawing_number` =NEW.drawing_number,
	`drawing_revision` =NEW.drawing_revision,
	`uom_id` =NEW.uom_id,
	`uom` =NEW.uom,
	`total_amount` =NEW.total_amount,
	`item_note` =NEW.item_note,
	`quantity` =NEW.quantity,
	`disc_per` =NEW.disc_per,
	`rate` =NEW.rate,
	`disc_value` =NEW.disc_value,
	`amount` =NEW.amount,
	`net_amount` =NEW.net_amount,
	`item_status_id` =NEW.item_status_id,
	`item_status` =NEW.item_status,
	`cust_part_no` =NEW.cust_part_no,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_items_delete_log` AFTER DELETE ON `invoice_items` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.invoice_items_log
SET
	`id`=OLD.id,
	`item_id` =OLD.item_id,
	`invoice_id` =OLD.invoice_id,
	`item_code` =OLD.item_code,
	`item_category_id` =OLD.item_category_id,
	`item_name` =OLD.item_name,
	`item_desc` =OLD.item_desc,
	`item_description` =OLD.item_description,
	`add_description` =OLD.add_description,
	`detail_description` =OLD.detail_description,
	`drawing_number` =OLD.drawing_number,
	`drawing_revision` =OLD.drawing_revision,
	`uom_id` =OLD.uom_id,
	`uom` =OLD.uom,
	`total_amount` =OLD.total_amount,
	`item_note` =OLD.item_note,
	`quantity` =OLD.quantity,
	`disc_per` =OLD.disc_per,
	`rate` =OLD.rate,
	`disc_value` =OLD.disc_value,
	`amount` =OLD.amount,
	`net_amount` =OLD.net_amount,
	`item_status_id` =OLD.item_status_id,
	`item_status` =OLD.item_status,
	`cust_part_no` =OLD.cust_part_no,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type` ='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_items_edit_log` AFTER UPDATE ON `invoice_items` FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.invoice_items_log
SET
	`id`=new.id,
	`item_id` =NEW.item_id,
	`invoice_id` =NEW.invoice_id,
	`item_code` =NEW.item_code,
	`item_category_id` =NEW.item_category_id,
	`item_name` =NEW.item_name,
	`item_desc` =NEW.item_desc,
	`item_description` =NEW.item_description,
	`add_description` =NEW.add_description,
	`detail_description` =NEW.detail_description,
	`drawing_number` =NEW.drawing_number,
	`drawing_revision` =NEW.drawing_revision,
	`uom_id` =NEW.uom_id,
	`uom` =NEW.uom,
	`total_amount` =NEW.total_amount,
	`item_note` =NEW.item_note,
	`quantity` =NEW.quantity,
	`disc_per` =NEW.disc_per,
	`rate` =NEW.rate,
	`disc_value` =NEW.disc_value,
	`amount` =NEW.amount,
	`net_amount` =NEW.net_amount,
	`item_status_id` =NEW.item_status_id,
	`item_status` =NEW.item_status,
	`cust_part_no` =NEW.cust_part_no,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_logins`
--

CREATE TABLE `invoice_logins` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_modified_by` varchar(255) NOT NULL,
  `last_modified_by_id` int(11) NOT NULL,
  `modified_date_time` datetime NOT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `prepared_by_id` int(11) NOT NULL,
  `prepared_date_time` datetime NOT NULL,
  `approved_by` varchar(255) NOT NULL,
  `approved_by_id` int(11) NOT NULL,
  `approved_date_time` datetime NOT NULL,
  `stage_id` int(11) NOT NULL,
  `stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `invoice_logins`
--
DELIMITER $$
CREATE TRIGGER `invoice_logins_add_log` AFTER INSERT ON `invoice_logins` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_logins_log
SET
`id` = NEW.id,
`invoice_id` = NEW.invoice_id,
`created_by` = NEW.created_by,
`created_by_id` = NEW.created_by_id,
`created_date_time` = NEW.created_date_time,
`last_modified_by` = NEW.last_modified_by,
`last_modified_by_id` = NEW.last_modified_by_id,
`modified_date_time` = NEW.modified_date_time,
`prepared_by` = NEW.prepared_by,
`prepared_by_id` = NEW.prepared_by_id,
`prepared_date_time` = NEW.prepared_date_time,
`approved_by` = NEW.approved_by,
`approved_by_id` = NEW.approved_by_id,
`approved_date_time` = NEW.approved_date_time,
`stage_id` = NEW.stage_id,
`stage` = NEW.stage,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_logins_delete_log` AFTER DELETE ON `invoice_logins` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_logins_log
SET
`id` = OLD.id,
`invoice_id` = OLD.invoice_id,
`created_by` = OLD.created_by,
`created_by_id` = OLD.created_by_id,
`created_date_time` = OLD.created_date_time,
`last_modified_by` = OLD.last_modified_by,
`last_modified_by_id` = OLD.last_modified_by_id,
`modified_date_time` = OLD.modified_date_time,
`prepared_by` = OLD.prepared_by,
`prepared_by_id` = OLD.prepared_by_id,
`prepared_date_time` = OLD.prepared_date_time,
`approved_by` = OLD.approved_by,
`approved_by_id` = OLD.approved_by_id,
`approved_date_time` = OLD.approved_date_time,
`stage_id` = OLD.stage_id,
`stage` = OLD.stage,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_logins_edit_log` AFTER UPDATE ON `invoice_logins` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_logins_log
SET
`id` = NEW.id,
`invoice_id` = NEW.invoice_id,
`created_by` = NEW.created_by,
`created_by_id` = NEW.created_by_id,
`created_date_time` = NEW.created_date_time,
`last_modified_by` = NEW.last_modified_by,
`last_modified_by_id` = NEW.last_modified_by_id,
`modified_date_time` = NEW.modified_date_time,
`prepared_by` = NEW.prepared_by,
`prepared_by_id` = NEW.prepared_by_id,
`prepared_date_time` = NEW.prepared_date_time,
`approved_by` = NEW.approved_by,
`approved_by_id` = NEW.approved_by_id,
`approved_date_time` = NEW.approved_date_time,
`stage_id` = NEW.stage_id,
`stage` = NEW.stage,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_type`
--

CREATE TABLE `invoice_type` (
  `id` int(11) NOT NULL,
  `invoice_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `invoice_type`
--
DELIMITER $$
CREATE TRIGGER `invoice_type_add_log` AFTER INSERT ON `invoice_type` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_type_log
SET
`id` = NEW.id,
`invoice_type` = NEW.invoice_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_type_delete_log` AFTER DELETE ON `invoice_type` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_type_log
SET
`id` = OLD.id,
`invoice_type` = OLD.invoice_type,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_type_edit_log` AFTER UPDATE ON `invoice_type` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_type_log
SET
`id` = NEW.id,
`invoice_type` = NEW.invoice_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `item_category` varchar(255) NOT NULL,
  `item_code1` varchar(255) NOT NULL,
  `item_code2` varchar(255) NOT NULL,
  `uom` varchar(255) NOT NULL,
  `qty` double NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `document` varchar(100) NOT NULL,
  `hsn_code` varchar(255) NOT NULL,
  `rate` double NOT NULL,
  `cgst` int(11) DEFAULT NULL,
  `sgst` int(11) DEFAULT NULL,
  `igst` int(11) DEFAULT NULL,
  `rate_in` text,
  `item_group` varchar(255) NOT NULL,
  `item_active` int(11) NOT NULL COMMENT '0- Active 1-Deactive',
  `item_name` varchar(255) NOT NULL,
  `cfactor` varchar(255) NOT NULL,
  `conv_uom` varchar(255) NOT NULL,
  `item_mpt` varchar(255) NOT NULL,
  `item_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `items`
--
DELIMITER $$
CREATE TRIGGER `items_add_log` AFTER INSERT ON `items` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.items_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`item_code1` = NEW.item_code1,
`item_code2` = NEW.item_code2,
`uom` = NEW.uom,
`qty` = NEW.qty,
`item_type` = NEW.item_type,
`stock` = NEW.stock,
`document` = NEW.document,
`hsn_code` = NEW.hsn_code,
`item_group` = NEW.item_group,
`item_active` = NEW.item_active,
`item_name` = NEW.item_name,
`cfactor` = NEW.cfactor,
`conv_uom` = NEW.conv_uom,
`item_mpt` = NEW.item_mpt,
`item_status` = NEW.item_status,
`image` = NEW.image,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `items_delete_log` AFTER DELETE ON `items` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.items_log
SET
`id` = OLD.id,
`item_category` = OLD.item_category,
`item_code1` = OLD.item_code1,
`item_code2` = OLD.item_code2,
`uom` = OLD.uom,
`qty` = OLD.qty,
`item_type` = OLD.item_type,
`stock` = OLD.stock,
`document` = OLD.document,
`hsn_code` = OLD.hsn_code,
`item_group` = OLD.item_group,
`item_active` = OLD.item_active,
`item_name` = OLD.item_name,
`cfactor` = OLD.cfactor,
`conv_uom` = OLD.conv_uom,
`item_mpt` = OLD.item_mpt,
`item_status` = OLD.item_status,
`image` = OLD.image,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `items_edit_log` AFTER UPDATE ON `items` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.items_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`item_code1` = NEW.item_code1,
`item_code2` = NEW.item_code2,
`uom` = NEW.uom,
`qty` = NEW.qty,
`item_type` = NEW.item_type,
`stock` = NEW.stock,
`document` = NEW.document,
`hsn_code` = NEW.hsn_code,
`item_group` = NEW.item_group,
`item_active` = NEW.item_active,
`item_name` = NEW.item_name,
`cfactor` = NEW.cfactor,
`conv_uom` = NEW.conv_uom,
`item_mpt` = NEW.item_mpt,
`item_status` = NEW.item_status,
`image` = NEW.image,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_bom`
--

CREATE TABLE `item_bom` (
  `id` int(11) NOT NULL,
  `item_master_id` int(11) DEFAULT NULL COMMENT 'item_id',
  `item_category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_bom`
--
DELIMITER $$
CREATE TRIGGER `item_bom_add_log` AFTER INSERT ON `item_bom` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_bom_log
SET
`id` = NEW.id,
`item_master_id` = NEW.item_master_id,
`item_category_id` = NEW.item_category_id,
`item_id` = NEW.item_id,
`qty` = NEW.qty,
`uom_id` = NEW.uom_id,
`total_amount` = NEW.total_amount,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_bom_delete_log` AFTER DELETE ON `item_bom` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_bom_log
SET
`id` = OLD.id,
`item_master_id` = OLD.item_master_id,
`item_category_id` = OLD.item_category_id,
`item_id` = OLD.item_id,
`qty` = OLD.qty,
`uom_id` = OLD.uom_id,
`total_amount` = OLD.total_amount,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_bom_edit_log` AFTER UPDATE ON `item_bom` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_bom_log
SET
`id` = NEW.id,
`item_master_id` = NEW.item_master_id,
`item_category_id` = NEW.item_category_id,
`item_id` = NEW.item_id,
`qty` = NEW.qty,
`uom_id` = NEW.uom_id,
`total_amount` = NEW.total_amount,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

CREATE TABLE `item_category` (
  `id` int(11) NOT NULL,
  `item_category` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_category`
--
DELIMITER $$
CREATE TRIGGER `item_category_add_log` AFTER INSERT ON `item_category` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_category_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`created_by` = NEW.created_by,
`updated_by` = NEW.updated_by,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_category_delete_log` AFTER DELETE ON `item_category` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_category_log
SET
`id` = OLD.id,
`item_category` = OLD.item_category,
`created_by` = OLD.created_by,
`updated_by` = OLD.updated_by,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_category_edit_log` AFTER UPDATE ON `item_category` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_category_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`created_by` = NEW.created_by,
`updated_by` = NEW.updated_by,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_category_log`
--

CREATE TABLE `item_category_log` (
  `id` int(11) NOT NULL,
  `item_category_id` int(11) NOT NULL,
  `item_category` varchar(255) DEFAULT NULL,
  `operation_type` varchar(32) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_class`
--

CREATE TABLE `item_class` (
  `id` int(11) NOT NULL,
  `item_class` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_class`
--
DELIMITER $$
CREATE TRIGGER `item_class_add_log` AFTER INSERT ON `item_class` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_class_log
SET
`id` = NEW.id,
`item_class` = NEW.item_class,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_class_delete_log` AFTER DELETE ON `item_class` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_class_log
SET
`id` = OLD.id,
`item_class` = OLD.item_class,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_class_edit_log` AFTER UPDATE ON `item_class` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_class_log
SET
`id` = NEW.id,
`item_class` = NEW.item_class,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_details`
--

CREATE TABLE `item_details` (
  `id` int(11) NOT NULL,
  `inquiry_id` int(11) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `item_desc` text NOT NULL,
  `add_description` varchar(255) NOT NULL,
  `detail_description` text NOT NULL,
  `drawing_number` varchar(255) NOT NULL,
  `rev` varchar(255) NOT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `quantity` double NOT NULL,
  `cust_part_no` varchar(255) NOT NULL,
  `item_note` varchar(255) NOT NULL,
  `lead_no1` varchar(255) NOT NULL,
  `lead_no2` varchar(255) NOT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_details`
--
DELIMITER $$
CREATE TRIGGER `item_details_add_log` AFTER INSERT ON `item_details` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_details_log
SET
`id` = NEW.id,
`inquiry_id` =NEW.inquiry_id,
`item_code` =NEW.item_code,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`rev` =NEW.rev,
`uom` =NEW.uom,
`quantity` =NEW.quantity,
`cust_part_no` =NEW.cust_part_no,
`item_note` =NEW.item_note,
`lead_no1` =NEW.lead_no1,
`lead_no2` =NEW.lead_no2,
`item_status` =NEW.item_status,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_details_delete_log` AFTER DELETE ON `item_details` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_details_log
SET
`id` = OLD.id,
`inquiry_id` =OLD.inquiry_id,
`item_code` =OLD.item_code,
`item_description` =OLD.item_description,
`item_desc`=OLD.item_desc,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`rev` =OLD.rev,
`uom` =OLD.uom,
`quantity` =OLD.quantity,
`cust_part_no` =OLD.cust_part_no,
`item_note` =OLD.item_note,
`lead_no1` =OLD.lead_no1,
`lead_no2` =OLD.lead_no2,
`item_status` =OLD.item_status,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_details_edit_log` AFTER UPDATE ON `item_details` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_details_log
SET
`id` = NEW.id,
`inquiry_id` =NEW.inquiry_id,
`item_code` =NEW.item_code,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`rev` =NEW.rev,
`uom` =NEW.uom,
`quantity` =NEW.quantity,
`cust_part_no` =NEW.cust_part_no,
`item_note` =NEW.item_note,
`lead_no1` =NEW.lead_no1,
`lead_no2` =NEW.lead_no2,
`item_status` =NEW.item_status,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_documents`
--

CREATE TABLE `item_documents` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `detail` text,
  `document_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_documents`
--
DELIMITER $$
CREATE TRIGGER `item_documents_add_log` AFTER INSERT ON `item_documents` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_documents_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`detail` =NEW.detail,
`document_type` =NEW.document_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_documents_delete_log` AFTER DELETE ON `item_documents` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_documents_log
SET
`id` = OLD.id,
`item_id` =OLD.item_id,
`detail` =OLD.detail,
`document_type` =OLD.document_type,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_documents_edit_log` AFTER UPDATE ON `item_documents` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_documents_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`detail` =NEW.detail,
`document_type` =NEW.document_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_extra_accessories`
--

CREATE TABLE `item_extra_accessories` (
  `item_extra_accessories_id` int(11) NOT NULL,
  `item_extra_accessories_value` varchar(255) NOT NULL,
  `item_extra_accessories_label` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_files`
--

CREATE TABLE `item_files` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `file_url` text NOT NULL,
  `item_type` varchar(10) NOT NULL COMMENT 'Domestic OR Export',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_files`
--
DELIMITER $$
CREATE TRIGGER `item_files_add_log` AFTER INSERT ON `item_files` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_files_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`file_url` =NEW.file_url,

`created_at` = NEW.created_at,

`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_files_delete_log` AFTER DELETE ON `item_files` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_files_log
SET
`id` = OLD.id,
`item_id` =OLD.item_id,
`file_url` =OLD.file_url,
`created_at` = OLD.created_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_files_edit_log` AFTER UPDATE ON `item_files` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_files_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`file_url` =NEW.file_url,

`created_at` = NEW.created_at,

`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_group_code`
--

CREATE TABLE `item_group_code` (
  `id` int(11) NOT NULL,
  `ig_code` varchar(255) NOT NULL,
  `ig_description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_group_code`
--
DELIMITER $$
CREATE TRIGGER `item_group_code_add_log` AFTER INSERT ON `item_group_code` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_group_code_log
SET
`id` = NEW.id,
`ig_code` =NEW.ig_code,
`ig_description` =NEW.ig_description,

`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_group_code_delete_log` AFTER DELETE ON `item_group_code` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_group_code_log
SET
`id` = OLD.id,
`ig_code` =OLD.ig_code,
`ig_description` =OLD.ig_description,

`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_group_code_edit_log` AFTER UPDATE ON `item_group_code` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_group_code_log
SET
`id` = NEW.id,
`ig_code` =NEW.ig_code,
`ig_description` =NEW.ig_description,

`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_make`
--

CREATE TABLE `item_make` (
  `id` int(11) NOT NULL,
  `item_make` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_make`
--
DELIMITER $$
CREATE TRIGGER `item_make_add_log` AFTER INSERT ON `item_make` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_make_log
SET
`id` = NEW.id,
`item_make` =NEW.item_make,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_make_delete_log` AFTER DELETE ON `item_make` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_make_log
SET
`id` = OLD.id,
`item_make` =OLD.item_make,


`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_make_edit_log` AFTER UPDATE ON `item_make` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_make_log
SET
`id` = NEW.id,
`item_make` =NEW.item_make,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_status`
--

CREATE TABLE `item_status` (
  `id` int(11) NOT NULL,
  `item_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_status`
--
DELIMITER $$
CREATE TRIGGER `item_status_add_log` AFTER INSERT ON `item_status` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_status_log
SET
`id` = NEW.id,
`item_status` =NEW.item_status,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_status_delete_log` AFTER DELETE ON `item_status` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_status_log
SET
`id` = OLD.id,
`item_status` =OLD.item_status,


`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_status_edit_log` AFTER UPDATE ON `item_status` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_status_log
SET
`id` = NEW.id,
`item_status` =NEW.item_status,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_type`
--

CREATE TABLE `item_type` (
  `id` int(11) NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `item_type`
--
DELIMITER $$
CREATE TRIGGER `item_type_add_log` AFTER INSERT ON `item_type` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_type_log
SET
`id` = NEW.id,
`item_type` =NEW.item_type,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_type_delete_log` AFTER DELETE ON `item_type` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_type_log
SET
`id` = OLD.id,
`item_type` =OLD.item_type,


`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `item_type_edit_log` AFTER UPDATE ON `item_type` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_type_log
SET
`id` = NEW.id,
`item_type` =NEW.item_type,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_contact_person`
--

CREATE TABLE `lead_contact_person` (
  `id` int(11) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `phone_no` varchar(15) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `fax_no` varchar(20) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `division` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `lead_contact_person`
--
DELIMITER $$
CREATE TRIGGER `lead_contact_person_add_log` AFTER INSERT ON `lead_contact_person` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.lead_contact_person_log
SET
`id`=NEW.id,
`person_name` =NEW.person_name,
`department` =NEW.department,
`designation` =NEW.designation,
`phone_no` =NEW.phone_no,
`mobile_no` =NEW.mobile_no,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`division` =NEW.division,
`address` =NEW.address,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_contact_person_delete_log` AFTER DELETE ON `lead_contact_person` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.lead_contact_person_log
SET
`id`=OLD.id,
`person_name` =OLD.person_name,
`department` =OLD.department,
`designation` =OLD.designation,
`phone_no` =OLD.phone_no,
`mobile_no` =OLD.mobile_no,
`fax_no` =OLD.fax_no,
`email_id` =OLD.email_id,
`division` =OLD.division,
`address` =OLD.address,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_contact_person_edit_log` AFTER UPDATE ON `lead_contact_person` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.lead_contact_person_log
SET
`id`=NEW.id,
`person_name` =NEW.person_name,
`department` =NEW.department,
`designation` =NEW.designation,
`phone_no` =NEW.phone_no,
`mobile_no` =NEW.mobile_no,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`division` =NEW.division,
`address` =NEW.address,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_details`
--

CREATE TABLE `lead_details` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `lead_no` varchar(255) DEFAULT NULL,
  `lead_title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `lead_detail` text NOT NULL,
  `industry_type_id` int(11) DEFAULT NULL,
  `lead_owner_id` int(11) DEFAULT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `lead_source_id` int(11) DEFAULT NULL,
  `lead_stage_id` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `lead_status_id` int(11) DEFAULT NULL,
  `review_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `lead_details`
--
DELIMITER $$
CREATE TRIGGER `lead_details_add_log` AFTER INSERT ON `lead_details` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_details_log SET
`id`=NEW.id,
`party_id` =NEW.party_id,
`lead_no` =NEW.lead_no,
`lead_title` =NEW.lead_title,
`start_date` =NEW.start_date,
`end_date` =NEW.end_date,
`lead_detail` =NEW.lead_detail,
`industry_type_id` =NEW.industry_type_id,
`lead_owner_id` =NEW.lead_owner_id,
`assigned_to_id` =NEW.assigned_to_id,
`priority_id` =NEW.priority_id,
`lead_source_id` =NEW.lead_source_id,
`lead_stage_id` =NEW.lead_stage_id,
`lead_provider_id` =NEW.lead_provider_id,
`lead_status_id` =NEW.lead_status_id,
`review_date` =NEW.review_date,
`created_by`=NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_details_delete_log` AFTER DELETE ON `lead_details` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_details_log SET
`id`=OLD.id,
`party_id` =OLD.party_id,
`lead_no` =OLD.lead_no,
`lead_title` =OLD.lead_title,
`start_date` =OLD.start_date,
`end_date` =OLD.end_date,
`lead_detail` =OLD.lead_detail,
`industry_type_id` =OLD.industry_type_id,
`lead_owner_id` =OLD.lead_owner_id,
`assigned_to_id` =OLD.assigned_to_id,
`priority_id` =OLD.priority_id,
`lead_source_id` =OLD.lead_source_id,
`lead_stage_id` =OLD.lead_stage_id,
`lead_provider_id` =OLD.lead_provider_id,
`lead_status_id` =OLD.lead_status_id,
`review_date` =OLD.review_date,
`created_by`=OLD.created_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_details_edit_log` AFTER UPDATE ON `lead_details` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_details_log SET
`id`=NEW.id,
`party_id` =NEW.party_id,
`lead_no` =NEW.lead_no,
`lead_title` =NEW.lead_title,
`start_date` =NEW.start_date,
`end_date` =NEW.end_date,
`lead_detail` =NEW.lead_detail,
`industry_type_id` =NEW.industry_type_id,
`lead_owner_id` =NEW.lead_owner_id,
`assigned_to_id` =NEW.assigned_to_id,
`priority_id` =NEW.priority_id,
`lead_source_id` =NEW.lead_source_id,
`lead_stage_id` =NEW.lead_stage_id,
`lead_provider_id` =NEW.lead_provider_id,
`lead_status_id` =NEW.lead_status_id,
`review_date` =NEW.review_date,
`created_by`=NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_provider`
--

CREATE TABLE `lead_provider` (
  `id` int(11) NOT NULL,
  `lead_provider` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `lead_provider`
--
DELIMITER $$
CREATE TRIGGER `lead_provider_add_log` AFTER INSERT ON `lead_provider` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_provider_log SET
`id`=NEW.id,
`lead_provider` =NEW.lead_provider,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_provider_delete_log` AFTER DELETE ON `lead_provider` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_provider_log SET
`id`=OLD.id,
`lead_provider` =OLD.lead_provider,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_provider_edit_log` AFTER UPDATE ON `lead_provider` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_provider_log SET
`id`=NEW.id,
`lead_provider` =NEW.lead_provider,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_source`
--

CREATE TABLE `lead_source` (
  `id` int(11) NOT NULL,
  `lead_source` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `lead_source`
--
DELIMITER $$
CREATE TRIGGER `lead_source_add_log` AFTER INSERT ON `lead_source` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_source_log SET
`id`=NEW.id,
`lead_source` =NEW.lead_source,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_source_delete_log` AFTER DELETE ON `lead_source` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_source_log SET
`id`=OLD.id,
`lead_source` =OLD.lead_source,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_source_edit_log` AFTER UPDATE ON `lead_source` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_source_log SET
`id`=NEW.id,
`lead_source` =NEW.lead_source,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_stage`
--

CREATE TABLE `lead_stage` (
  `id` int(11) NOT NULL,
  `lead_stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `lead_stage`
--
DELIMITER $$
CREATE TRIGGER `lead_stage_add_log` AFTER INSERT ON `lead_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_stage_log SET
`id`=NEW.id,
`lead_stage` =NEW.lead_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_stage_delete_log` AFTER DELETE ON `lead_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_stage_log SET
`id`=OLD.id,
`lead_stage` =OLD.lead_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_stage_edit_log` AFTER UPDATE ON `lead_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_stage_log SET
`id`=NEW.id,
`lead_stage` =NEW.lead_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_status`
--

CREATE TABLE `lead_status` (
  `id` int(11) NOT NULL,
  `lead_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `lead_status`
--
DELIMITER $$
CREATE TRIGGER `lead_status_add_log` AFTER INSERT ON `lead_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_status_log SET
`id`=NEW.id,
`lead_status` =NEW.lead_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_status_delete_log` AFTER DELETE ON `lead_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_status_log SET
`id`=OLD.id,
`lead_status` =OLD.lead_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lead_status_edit_log` AFTER UPDATE ON `lead_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_status_log SET
`id`=NEW.id,
`lead_status` =NEW.lead_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `leave_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `leave_type` varchar(50) NOT NULL,
  `day` varchar(255) NOT NULL,
  `leave_detail` text NOT NULL,
  `leave_count` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `leaves`
--
DELIMITER $$
CREATE TRIGGER `leaves_add_log` AFTER INSERT ON `leaves` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leaves_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`name` =NEW.name,
`email` =NEW.email,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`day` =NEW.day,
`leave_detail` =NEW.leave_detail,
`leave_count` =NEW.leave_count,
`created_at` =NEW.created_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `leaves_delete_log` AFTER DELETE ON `leaves` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leaves_log SET
`leave_id`=OLD.leave_id,
`employee_id` =OLD.employee_id,
`name` =OLD.name,
`email` =OLD.email,
`from_date` =OLD.from_date,
`to_date` =OLD.to_date,
`leave_type` =OLD.leave_type,
`day` =OLD.day,
`leave_detail` =OLD.leave_detail,
`leave_count` =OLD.leave_count,
`created_at` =OLD.created_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `leaves_edit_log` AFTER UPDATE ON `leaves` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leaves_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`name` =NEW.name,
`email` =NEW.email,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`day` =NEW.day,
`leave_detail` =NEW.leave_detail,
`leave_count` =NEW.leave_count,
`created_at` =NEW.created_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_for_employee`
--

CREATE TABLE `leave_for_employee` (
  `id` int(11) NOT NULL,
  `total_sl` int(11) NOT NULL,
  `total_cl` int(11) NOT NULL,
  `total_el_pl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `leave_for_employee`
--
DELIMITER $$
CREATE TRIGGER `leave_for_employee_add_log` AFTER INSERT ON `leave_for_employee` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leave_for_employee_log SET
`id`=NEW.id,
`total_sl` =NEW.total_sl,
`total_cl` =NEW.total_cl,
`total_el_pl` =NEW.total_el_pl,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `leave_for_employee_delete_log` AFTER DELETE ON `leave_for_employee` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leave_for_employee_log SET
`id`=OLD.id,
`total_sl` =OLD.total_sl,
`total_cl` =OLD.total_cl,
`total_el_pl` =OLD.total_el_pl,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `leave_for_employee_edit_log` AFTER UPDATE ON `leave_for_employee` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leave_for_employee_log SET
`id`=NEW.id,
`total_sl` =NEW.total_sl,
`total_cl` =NEW.total_cl,
`total_el_pl` =NEW.total_el_pl,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `letters`
--

CREATE TABLE `letters` (
  `letter_id` int(11) NOT NULL,
  `letter_code` varchar(255) NOT NULL,
  `letter_template` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `letters`
--
DELIMITER $$
CREATE TRIGGER `letters_add_log` AFTER INSERT ON `letters` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.letters_log SET
`letter_id` =NEW.letter_id,
`letter_code` =NEW.letter_code,
`letter_template` =NEW.letter_template,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `letters_delete_log` AFTER DELETE ON `letters` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.letters_log SET
`letter_id` =OLD.letter_id,
`letter_code` =OLD.letter_code,
`letter_template` =OLD.letter_template,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `letters_edit_log` AFTER UPDATE ON `letters` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.letters_log SET
`letter_id` =NEW.letter_id,
`letter_code` =NEW.letter_code,
`letter_template` =NEW.letter_template,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `loading_by`
--

CREATE TABLE `loading_by` (
  `id` int(11) NOT NULL,
  `loading_by` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `loading_by`
--
DELIMITER $$
CREATE TRIGGER `loading_by_add_log` AFTER INSERT ON `loading_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.loading_by_log SET
`id`=NEW.id,
`loading_by` =NEW.loading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `loading_by_delete_log` AFTER DELETE ON `loading_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.loading_by_log SET
`id`=OLD.id,
`loading_by` =OLD.loading_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `loading_by_edit_log` AFTER UPDATE ON `loading_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.loading_by_log SET
`id`=NEW.id,
`loading_by` =NEW.loading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_changes_logs`
--

CREATE TABLE `mailbox_changes_logs` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `mail_id` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `folder_name` varchar(255) DEFAULT NULL,
  `from_folder_name` varchar(255) DEFAULT NULL,
  `from_mail_no` int(11) DEFAULT NULL,
  `to_folder_name` varchar(255) DEFAULT NULL,
  `to_mail_no` int(11) DEFAULT NULL,
  `is_done` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_folders`
--

CREATE TABLE `mailbox_folders` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `folder_label` varchar(255) NOT NULL,
  `folder_name` varchar(255) NOT NULL,
  `old_parent_id` int(11) NOT NULL DEFAULT '0',
  `is_system_folder` tinyint(1) NOT NULL DEFAULT '0',
  `is_created` tinyint(1) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `mailbox_folders`
--
DELIMITER $$
CREATE TRIGGER `mailbox_folders_add_log` AFTER INSERT ON `mailbox_folders` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_folders_log SET
`id`=NEW.id,
`parent_id` =NEW.parent_id,
`folder_label` =NEW.folder_label,
`folder_name` =NEW.folder_name,
`old_parent_id` =NEW.old_parent_id,
`is_system_folder` =NEW.is_system_folder,
`is_created` =NEW.is_created,
`is_updated` =NEW.is_updated,
`is_deleted` =NEW.is_deleted,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`deleted_at` =NEW.deleted_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mailbox_folders_delete_log` AFTER DELETE ON `mailbox_folders` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_folders_log SET
`id`=OLD.id,
`parent_id` =OLD.parent_id,
`folder_label` =OLD.folder_label,
`folder_name` =OLD.folder_name,
`old_parent_id` =OLD.old_parent_id,
`is_system_folder` =OLD.is_system_folder,
`is_created` =OLD.is_created,
`is_updated` =OLD.is_updated,
`is_deleted` =OLD.is_deleted,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`deleted_at` =OLD.deleted_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mailbox_folders_edit_log` AFTER UPDATE ON `mailbox_folders` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_folders_log SET
`id`=NEW.id,
`parent_id` =NEW.parent_id,
`folder_label` =NEW.folder_label,
`folder_name` =NEW.folder_name,
`old_parent_id` =NEW.old_parent_id,
`is_system_folder` =NEW.is_system_folder,
`is_created` =NEW.is_created,
`is_updated` =NEW.is_updated,
`is_deleted` =NEW.is_deleted,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`deleted_at` =NEW.deleted_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_mails`
--

CREATE TABLE `mailbox_mails` (
  `mail_id` int(11) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `mail_no` int(11) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `folder_label` varchar(255) DEFAULT NULL,
  `folder_name` varchar(255) DEFAULT NULL,
  `is_unread` tinyint(1) DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Starred | 1-Starred',
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_created` tinyint(1) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Updated | 1-Updated',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `mailbox_mails`
--
DELIMITER $$
CREATE TRIGGER `mailbox_mails_delete_log` AFTER DELETE ON `mailbox_mails` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_mails_log SET
`mail_id` =OLD.mail_id,
`uid` =OLD.uid,
`folder_id` =OLD.folder_id,
`mail_no` =OLD.mail_no,
`from_address` =OLD.from_address,
`from_name` =OLD.from_name,
`to_address` =OLD.to_address,
`to_name` =OLD.to_name,
`reply_to` =OLD.reply_to,
`reply_to_name` =OLD.reply_to_name,
`sender_address` =OLD.sender_address,
`sender_name` =OLD.sender_name,
`cc` =OLD.cc,
`bcc` =OLD.bcc,
`subject` =OLD.subject,
`body` =OLD.body,
`attachments` =OLD.attachments,
`received_at` =OLD.received_at,
`folder_label` =OLD.folder_label,
`folder_name` =OLD.folder_name,
`is_unread` =OLD.is_unread,
`is_starred` =OLD.is_starred,
`is_attachment` =OLD.is_attachment,
`is_created` =OLD.is_created,
`is_updated` =OLD.is_updated,
`is_deleted` =OLD.is_deleted,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`deleted_at` =OLD.deleted_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mailbox_mails_edit_log` AFTER UPDATE ON `mailbox_mails` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_mails_log SET
`mail_id` =NEW.mail_id,
`uid` =NEW.uid,
`folder_id` =NEW.folder_id,
`mail_no` =NEW.mail_no,
`from_address` =NEW.from_address,
`from_name` =NEW.from_name,
`to_address` =NEW.to_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`folder_label` =NEW.folder_label,
`folder_name` =NEW.folder_name,
`is_unread` =NEW.is_unread,
`is_starred` =NEW.is_starred,
`is_attachment` =NEW.is_attachment,
`is_created` =NEW.is_created,
`is_updated` =NEW.is_updated,
`is_deleted` =NEW.is_deleted,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`deleted_at` =NEW.deleted_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_settings`
--

CREATE TABLE `mailbox_settings` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `setting_key` varchar(255) NOT NULL,
  `setting_value` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `mailbox_settings`
--
DELIMITER $$
CREATE TRIGGER `mailbox_settings_add_log` AFTER INSERT ON `mailbox_settings` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_settings_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`setting_key` =NEW.setting_key,
`setting_value` =NEW.setting_value,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mailbox_settings_delete_log` AFTER DELETE ON `mailbox_settings` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_settings_log SET
`id`=OLD.id,
`staff_id` =OLD.staff_id,
`setting_key` =OLD.setting_key,
`setting_value` =OLD.setting_value,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mailbox_settings_edit_log` AFTER UPDATE ON `mailbox_settings` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_settings_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`setting_key` =NEW.setting_key,
`setting_value` =NEW.setting_value,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mail_system`
--

CREATE TABLE `mail_system` (
  `mail_id` int(11) NOT NULL,
  `mail_uid` varchar(255) DEFAULT NULL,
  `staff_id` int(11) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `body` text CHARACTER SET utf8,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `is_unread` tinyint(1) NOT NULL DEFAULT '0',
  `is_starred` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Starred | 1-Starred',
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_read_receipt_req` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Requested | 1-Requested',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `mail_system`
--
DELIMITER $$
CREATE TRIGGER `mail_system_add_log` AFTER INSERT ON `mail_system` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_log SET
`mail_id` =NEW.mail_id,
`mail_uid` =NEW.mail_uid,
`staff_id` =NEW.staff_id,
`folder` =NEW.folder,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_unread` =NEW.is_unread,
`is_starred` =NEW.is_starred,
`is_attachment` =NEW.is_attachment,
`is_read_receipt_req` =NEW.is_read_receipt_req,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mail_system_delete_log` AFTER DELETE ON `mail_system` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_log SET
`mail_id` =OLD.mail_id,
`mail_uid` =OLD.mail_uid,
`staff_id` =OLD.staff_id,
`folder` =OLD.folder,
`from_name` =OLD.from_name,
`from_address` =OLD.from_address,
`to_name` =OLD.to_name,
`reply_to` =OLD.reply_to,
`reply_to_name` =OLD.reply_to_name,
`sender_address` =OLD.sender_address,
`sender_name` =OLD.sender_name,
`cc` =OLD.cc,
`bcc` =OLD.bcc,
`subject` =OLD.subject,
`body` =OLD.body,
`attachments` =OLD.attachments,
`received_at` =OLD.received_at,
`is_unread` =OLD.is_unread,
`is_starred` =OLD.is_starred,
`is_attachment` =OLD.is_attachment,
`is_read_receipt_req` =OLD.is_read_receipt_req,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mail_system_edit_log` AFTER UPDATE ON `mail_system` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_log SET
`mail_id` =NEW.mail_id,
`mail_uid` =NEW.mail_uid,
`staff_id` =NEW.staff_id,
`folder` =NEW.folder,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_unread` =NEW.is_unread,
`is_starred` =NEW.is_starred,
`is_attachment` =NEW.is_attachment,
`is_read_receipt_req` =NEW.is_read_receipt_req,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mail_system_folder`
--

CREATE TABLE `mail_system_folder` (
  `folder_id` int(11) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `mail_system_folder`
--
DELIMITER $$
CREATE TRIGGER `mail_system_folder_add_log` AFTER INSERT ON `mail_system_folder` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_folder_log SET
`folder_id` =NEW.folder_id,
`folder` =NEW.folder,
`staff_id` =NEW.staff_id,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mail_system_folder_delete_log` AFTER DELETE ON `mail_system_folder` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_folder_log SET
`folder_id` =OLD.folder_id,
`folder` =OLD.folder,
`staff_id` =OLD.staff_id,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `mail_system_folder_edit_log` AFTER UPDATE ON `mail_system_folder` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_folder_log SET
`folder_id` =NEW.folder_id,
`folder` =NEW.folder,
`staff_id` =NEW.staff_id,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `main_group`
--

CREATE TABLE `main_group` (
  `id` int(11) NOT NULL,
  `item_group` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `main_group`
--
DELIMITER $$
CREATE TRIGGER `main_group_add_log` AFTER INSERT ON `main_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.main_group_log SET
`id`=NEW.id,
`item_group`=NEW.item_group,
`code` =NEW.code,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `main_group_delete_log` AFTER DELETE ON `main_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.main_group_log SET
`id`=OLD.id,
`item_group`=OLD.item_group,
`code` =OLD.code,
`description` =OLD.description,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `main_group_edit_log` AFTER UPDATE ON `main_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.main_group_log SET
`id`=NEW.id,
`item_group`=NEW.item_group,
`code` =NEW.code,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `material_process_type`
--

CREATE TABLE `material_process_type` (
  `id` int(11) NOT NULL,
  `material_process_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `material_process_type`
--
DELIMITER $$
CREATE TRIGGER `material_process_type_add_log` AFTER INSERT ON `material_process_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_process_type_log SET
`id`=NEW.id,
`material_process_type` =NEW.material_process_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `material_process_type_delete_log` AFTER DELETE ON `material_process_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_process_type_log SET
`id`=OLD.id,
`material_process_type` =OLD.material_process_type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `material_process_type_edit_log` AFTER UPDATE ON `material_process_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_process_type_log SET
`id`=NEW.id,
`material_process_type` =NEW.material_process_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `material_specification`
--

CREATE TABLE `material_specification` (
  `id` int(11) NOT NULL,
  `material` varchar(255) NOT NULL,
  `material_specification` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `material_specification`
--
DELIMITER $$
CREATE TRIGGER `material_specification_add_log` AFTER INSERT ON `material_specification` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_specification_log SET
`id`=NEW.id,
`material` =NEW.material,
`material_specification` =NEW.material_specification,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `material_specification_delete_log` AFTER DELETE ON `material_specification` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_specification_log SET
`id`=OLD.id,
`material` =OLD.material,
`material_specification` =OLD.material_specification,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `material_specification_edit_log` AFTER UPDATE ON `material_specification` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_specification_log SET
`id`=NEW.id,
`material` =NEW.material,
`material_specification` =NEW.material_specification,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `from_id` int(11) DEFAULT NULL,
  `from_table` varchar(255) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `to_table` varchar(255) DEFAULT NULL,
  `message` text,
  `file` varchar(255) DEFAULT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  `date_time` datetime DEFAULT NULL,
  `msg_email` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module_roles`
--

CREATE TABLE `module_roles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `role_name` varchar(128) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `module_roles`
--
DELIMITER $$
CREATE TRIGGER `module_roles_add_log` AFTER INSERT ON `module_roles` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.module_roles_log SET
`id`=NEW.id,
`title` =NEW.title,
`role_name` =NEW.role_name,
`module_id` =NEW.module_id,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `module_roles_delete_log` AFTER DELETE ON `module_roles` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.module_roles_log SET
`id`=OLD.id,
`title` =OLD.title,
`role_name` =OLD.role_name,
`module_id` =OLD.module_id,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `module_roles_edit_log` AFTER UPDATE ON `module_roles` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.module_roles_log SET
`id`=NEW.id,
`title` =NEW.title,
`role_name` =NEW.role_name,
`module_id` =NEW.module_id,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `order_stage`
--

CREATE TABLE `order_stage` (
  `id` int(11) NOT NULL,
  `order_stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `order_stage`
--
DELIMITER $$
CREATE TRIGGER `order_stage_add_log` AFTER INSERT ON `order_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.order_stage_log SET
`id`=NEW.id,
`order_stage` =NEW.order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `order_stage_delete_log` AFTER DELETE ON `order_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.order_stage_log SET
`id`=OLD.id,
`order_stage` =OLD.order_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `order_stage_edit_log` AFTER UPDATE ON `order_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.order_stage_log SET
`id`=NEW.id,
`order_stage` =NEW.order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE `outbox` (
  `mail_id` int(11) NOT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_sent` tinyint(1) NOT NULL COMMENT '1-Sent 0-Not Sent',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `outbox`
--
DELIMITER $$
CREATE TRIGGER `outbox_add_log` AFTER INSERT ON `outbox` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.outbox_log SET
`mail_id` =NEW.mail_id,
`from_address` =NEW.from_address,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_attachment` =NEW.is_attachment,
`is_sent` =NEW.is_sent,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `outbox_delete_log` AFTER DELETE ON `outbox` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.outbox_log SET
`mail_id` =OLD.mail_id,
`from_address` =OLD.from_address,
`from_name` =OLD.from_name,
`from_address` =OLD.from_address,
`to_name` =OLD.to_name,
`reply_to` =OLD.reply_to,
`reply_to_name` =OLD.reply_to_name,
`sender_address` =OLD.sender_address,
`sender_name` =OLD.sender_name,
`cc` =OLD.cc,
`bcc` =OLD.bcc,
`subject` =OLD.subject,
`body` =OLD.body,
`attachments` =OLD.attachments,
`received_at` =OLD.received_at,
`is_attachment` =OLD.is_attachment,
`is_sent` =OLD.is_sent,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `outbox_edit_log` AFTER UPDATE ON `outbox` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.outbox_log SET
`mail_id` =NEW.mail_id,
`from_address` =NEW.from_address,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_attachment` =NEW.is_attachment,
`is_sent` =NEW.is_sent,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `party`
--

CREATE TABLE `party` (
  `party_id` int(11) NOT NULL,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `reference_description` text NOT NULL,
  `party_type_1` int(11) DEFAULT NULL,
  `party_type_2` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `project` text,
  `address` text NOT NULL,
  `area` text,
  `pincode` varchar(15) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone_no` text NOT NULL,
  `fax_no` text,
  `email_id` text NOT NULL,
  `website` varchar(255) NOT NULL,
  `opening_bal` double NOT NULL,
  `credit_limit` double NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active ,2-Deactive',
  `tin_vat_no` varchar(100) NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `tin_cst_no` varchar(100) NOT NULL,
  `ecc_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `range` varchar(100) NOT NULL,
  `division` varchar(100) NOT NULL,
  `service_tax_no` varchar(100) NOT NULL,
  `utr_no` varchar(225) DEFAULT NULL,
  `address_work` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Yes | 0-No = Work Address same as Party Address ',
  `w_address` text NOT NULL,
  `w_city` int(11) DEFAULT NULL,
  `w_state` int(11) DEFAULT NULL,
  `w_country` int(11) DEFAULT NULL,
  `oldw_pincode` varchar(225) DEFAULT NULL,
  `w_email` text,
  `w_web` text NOT NULL,
  `w_phone1` text,
  `w_phone2` text,
  `w_phone3` text,
  `w_note` text,
  `w_delivery_party_name` varchar(255) DEFAULT NULL,
  `w_pin_code` varchar(15) DEFAULT NULL,
  `com_address` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Yes | 0-No = Work Address same as Party Address',
  `com_address_party_name` varchar(255) DEFAULT NULL,
  `com_address_address` text,
  `com_address_city` int(11) DEFAULT NULL,
  `com_address_pin_code` varchar(15) DEFAULT NULL,
  `com_address_state` int(11) DEFAULT NULL,
  `com_address_country` int(11) DEFAULT NULL,
  `com_address_phone1` text,
  `com_address_email` text,
  `com_address_web` text,
  `com_address_note` text,
  `oldPartyType` varchar(225) DEFAULT NULL,
  `oldHoliday` varchar(225) DEFAULT NULL,
  `oldST_No` varchar(225) DEFAULT NULL,
  `oldVendorCode` varchar(225) DEFAULT NULL,
  `oldw_fax` varchar(225) DEFAULT NULL,
  `oldacct_no` varchar(225) DEFAULT NULL,
  `oldReason` varchar(225) DEFAULT NULL,
  `oldAllowMultipalBill` varchar(225) DEFAULT NULL,
  `oldAddCity` varchar(225) DEFAULT NULL,
  `oldCreatedBy` varchar(225) DEFAULT NULL,
  `oldCreateDate` date DEFAULT NULL,
  `oldModifyBy` varchar(225) DEFAULT NULL,
  `oldModiDate` date DEFAULT NULL,
  `oldPrintFlag` varchar(225) DEFAULT NULL,
  `oldPT` varchar(225) DEFAULT NULL,
  `oldCPerson` varchar(225) DEFAULT NULL,
  `oldOpType` varchar(225) DEFAULT NULL,
  `oldCompProfile` varchar(225) DEFAULT NULL,
  `oldBranchCode` varchar(225) DEFAULT NULL,
  `oldAreaName` varchar(225) DEFAULT NULL,
  `oldCreditLimit` varchar(225) DEFAULT NULL,
  `oldAcctGrpCode` varchar(225) DEFAULT NULL,
  `oldOpBalDr` varchar(225) DEFAULT NULL,
  `oldApprovedBy` varchar(225) DEFAULT NULL,
  `oldApprovedDate` date DEFAULT NULL,
  `oldPStage` varchar(225) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `outstanding_balance` double DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `party_created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `party`
--
DELIMITER $$
CREATE TRIGGER `party_add_log` AFTER INSERT ON `party` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.party_log
SET
	`party_id`=NEW.party_id,
	`party_code` =NEW.party_code,
	`party_name` =NEW.party_name,
	`reference_id`=NEW.reference_id,
	`reference_description`=NEW.reference_description,
	`party_type_1`=NEW.party_type_1,
	`party_type_2`=NEW.party_type_2,
	`branch_id`=NEW.branch_id,
	`project`=NEW.project,
	`address`=NEW.address,
	`area`=NEW.area,
	`pincode`=NEW.pincode,
	`city_id`=NEW.city_id,
	`state_id`=NEW.state_id,
	`country_id`=NEW.country_id,
	`phone_no`=NEW.phone_no,
	`fax_no` =NEW.fax_no,
	`email_id`=NEW.email_id,
	`website` =NEW.website,
	`opening_bal` =NEW.opening_bal,
	`credit_limit` =NEW.credit_limit,
	`active` =NEW.active,
	`tin_vat_no` =NEW.tin_vat_no,
	`gst_no` =NEW.gst_no,
	`tin_cst_no` =NEW.tin_cst_no,
	`ecc_no` =NEW.ecc_no,
	`pan_no` =NEW.pan_no,
	`range` =NEW.range,
	`division` =NEW.division,
	`service_tax_no` =NEW.service_tax_no,
	`utr_no` =NEW.utr_no,
	`address_work` =NEW.address_work,
	`w_address`=NEW.w_address,
	`w_city`=NEW.w_city,
	`w_state`=NEW.w_state,
	`w_country`=NEW.w_country,
	`oldw_pincode` =NEW.oldw_pincode,
	`w_email`=NEW.w_email,
	`w_web`=NEW.w_web,
	`w_phone1`=NEW.w_phone1,
	`w_phone2`=NEW.w_phone2,
	`w_phone3`=NEW.w_phone3,
	`w_note`=NEW.w_note,
	`oldPartyType` =NEW.oldPartyType,
	`oldHoliday` =NEW.oldHoliday,
	`oldST_No` =NEW.oldST_No,
	`oldVendorCode` =NEW.oldVendorCode,
	`oldw_fax` =NEW.oldw_fax,
	`oldacct_no` =NEW.oldacct_no,
	`oldReason` =NEW.oldReason,
	`oldAllowMultipalBill` =NEW.oldAllowMultipalBill,
	`oldAddCity` =NEW.oldAddCity,
	`oldCreatedBy` =NEW.oldCreatedBy,
	`oldCreateDate` =NEW.oldCreateDate,
	`oldModifyBy` =NEW.oldModifyBy,
	`oldModiDate` =NEW.oldModiDate,
	`oldPrintFlag` =NEW.oldPrintFlag,
	`oldPT` =NEW.oldPT,
	`oldCPerson` =NEW.oldCPerson,
	`oldOpType` =NEW.oldOpType,
	`oldCompProfile` =NEW.oldCompProfile,
	`oldBranchCode` =NEW.oldBranchCode,
	`oldAreaName` =NEW.oldAreaName,
	`oldCreditLimit` =NEW.oldCreditLimit,
	`oldAcctGrpCode` =NEW.oldAcctGrpCode,
	`oldOpBalDr` =NEW.oldOpBalDr,
	`oldApprovedBy` =NEW.oldApprovedBy,
	`oldApprovedDate` =NEW.oldApprovedDate,
	`oldPStage` =NEW.oldPStage,
	`agent_id`=NEW.agent_id,
	`outstanding_balance`=NEW.outstanding_balance,
	`created_by`=NEW.created_by,
	`updated_by`=NEW.updated_by,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type`='add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_delete_log` AFTER DELETE ON `party` FOR EACH ROW INSERT INTO
jaykhodiyar_logs.party_log
SET
	
	`party_id`=OLD.party_id,
	`party_code` =OLD.party_code,
	`party_name` =OLD.party_name,
	`reference_id`=OLD.reference_id,
	`reference_description`=OLD.reference_description,
	`party_type_1`=OLD.party_type_1,
	`party_type_2`=OLD.party_type_2,
	`branch_id`=OLD.branch_id,
	`project`=OLD.project,
	`address`=OLD.address,
	`area`=OLD.area,
	`pincode`=OLD.pincode,
	`city_id`=OLD.city_id,
	`state_id`=OLD.state_id,
	`country_id`=OLD.country_id,
	`phone_no`=OLD.phone_no,
	`fax_no` =OLD.fax_no,
	`email_id`=OLD.email_id,
	`website` =OLD.website,
	`opening_bal` =OLD.opening_bal,
	`credit_limit` =OLD.credit_limit,
	`active` =OLD.active,
	`tin_vat_no` =OLD.tin_vat_no,
	`gst_no` =OLD.gst_no,
	`tin_cst_no` =OLD.tin_cst_no,
	`ecc_no` =OLD.ecc_no,
	`pan_no` =OLD.pan_no,
	`range` =OLD.range,
	`division` =OLD.division,
	`service_tax_no` =OLD.service_tax_no,
	`utr_no` =OLD.utr_no,
	`address_work` =OLD.address_work,
	`w_address`=OLD.w_address,
	`w_city`=OLD.w_city,
	`w_state`=OLD.w_state,
	`w_country`=OLD.w_country,
	`oldw_pincode` =OLD.oldw_pincode,
	`w_email`=OLD.w_email,
	`w_web`=OLD.w_web,
	`w_phone1`=OLD.w_phone1,
	`w_phone2`=OLD.w_phone2,
	`w_phone3`=OLD.w_phone3,
	`w_note`=OLD.w_note,
	`oldPartyType` =OLD.oldPartyType,
	`oldHoliday` =OLD.oldHoliday,
	`oldST_No` =OLD.oldST_No,
	`oldVendorCode` =OLD.oldVendorCode,
	`oldw_fax` =OLD.oldw_fax,
	`oldacct_no` =OLD.oldacct_no,
	`oldReason` =OLD.oldReason,
	`oldAllowMultipalBill` =OLD.oldAllowMultipalBill,
	`oldAddCity` =OLD.oldAddCity,
	`oldCreatedBy` =OLD.oldCreatedBy,
	`oldCreateDate` =OLD.oldCreateDate,
	`oldModifyBy` =OLD.oldModifyBy,
	`oldModiDate` =OLD.oldModiDate,
	`oldPrintFlag` =OLD.oldPrintFlag,
	`oldPT` =OLD.oldPT,
	`oldCPerson` =OLD.oldCPerson,
	`oldOpType` =OLD.oldOpType,
	`oldCompProfile` =OLD.oldCompProfile,
	`oldBranchCode` =OLD.oldBranchCode,
	`oldAreaName` =OLD.oldAreaName,
	`oldCreditLimit` =OLD.oldCreditLimit,
	`oldAcctGrpCode` =OLD.oldAcctGrpCode,
	`oldOpBalDr` =OLD.oldOpBalDr,
	`oldApprovedBy` =OLD.oldApprovedBy,
	`oldApprovedDate` =OLD.oldApprovedDate,
	`oldPStage` =OLD.oldPStage,
	`agent_id`=OLD.agent_id,
	`outstanding_balance`=OLD.outstanding_balance,
	`created_by`=OLD.created_by,
	`updated_by`=OLD.updated_by,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type`='delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_edit_log` AFTER UPDATE ON `party` FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.party_log
SET
	
	`party_id`=NEW.party_id,
	`party_code` =NEW.party_code,
	`party_name` =NEW.party_name,
	`reference_id`=NEW.reference_id,
	`reference_description`=NEW.reference_description,
	`party_type_1`=NEW.party_type_1,
	`party_type_2`=NEW.party_type_2,
	`branch_id`=NEW.branch_id,
	`project`=NEW.project,
	`address`=NEW.address,
	`area`=NEW.area,
	`pincode`=NEW.pincode,
	`city_id`=NEW.city_id,
	`state_id`=NEW.state_id,
	`country_id`=NEW.country_id,
	`phone_no`=NEW.phone_no,
	`fax_no` =NEW.fax_no,
	`email_id`=NEW.email_id,
	`website` =NEW.website,
	`opening_bal` =NEW.opening_bal,
	`credit_limit` =NEW.credit_limit,
	`active` =NEW.active,
	`tin_vat_no` =NEW.tin_vat_no,
	`gst_no` =NEW.gst_no,
	`tin_cst_no` =NEW.tin_cst_no,
	`ecc_no` =NEW.ecc_no,
	`pan_no` =NEW.pan_no,
	`range` =NEW.range,
	`division` =NEW.division,
	`service_tax_no` =NEW.service_tax_no,
	`utr_no` =NEW.utr_no,
	`address_work` =NEW.address_work,
	`w_address`=NEW.w_address,
	`w_city`=NEW.w_city,
	`w_state`=NEW.w_state,
	`w_country`=NEW.w_country,
	`oldw_pincode` =NEW.oldw_pincode,
	`w_email`=NEW.w_email,
	`w_web`=NEW.w_web,
	`w_phone1`=NEW.w_phone1,
	`w_phone2`=NEW.w_phone2,
	`w_phone3`=NEW.w_phone3,
	`w_note`=NEW.w_note,
	`oldPartyType` =NEW.oldPartyType,
	`oldHoliday` =NEW.oldHoliday,
	`oldST_No` =NEW.oldST_No,
	`oldVendorCode` =NEW.oldVendorCode,
	`oldw_fax` =NEW.oldw_fax,
	`oldacct_no` =NEW.oldacct_no,
	`oldReason` =NEW.oldReason,
	`oldAllowMultipalBill` =NEW.oldAllowMultipalBill,
	`oldAddCity` =NEW.oldAddCity,
	`oldCreatedBy` =NEW.oldCreatedBy,
	`oldCreateDate` =NEW.oldCreateDate,
	`oldModifyBy` =NEW.oldModifyBy,
	`oldModiDate` =NEW.oldModiDate,
	`oldPrintFlag` =NEW.oldPrintFlag,
	`oldPT` =NEW.oldPT,
	`oldCPerson` =NEW.oldCPerson,
	`oldOpType` =NEW.oldOpType,
	`oldCompProfile` =NEW.oldCompProfile,
	`oldBranchCode` =NEW.oldBranchCode,
	`oldAreaName` =NEW.oldAreaName,
	`oldCreditLimit` =NEW.oldCreditLimit,
	`oldAcctGrpCode` =NEW.oldAcctGrpCode,
	`oldOpBalDr` =NEW.oldOpBalDr,
	`oldApprovedBy` =NEW.oldApprovedBy,
	`oldApprovedDate` =NEW.oldApprovedDate,
	`oldPStage` =NEW.oldPStage,
	`agent_id`=NEW.agent_id,
	`outstanding_balance`=NEW.outstanding_balance,
	`created_by`=NEW.created_by,
	`updated_by`=NEW.updated_by,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type`='edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `party_detail`
--

CREATE TABLE `party_detail` (
  `party_detail_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `party_detail`
--
DELIMITER $$
CREATE TRIGGER `party_detail_add_log` AFTER INSERT ON `party_detail` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_detail_log SET
`party_detail_id` =NEW.party_detail_id,
`party_id` =NEW.party_id,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_detail_delete_log` AFTER DELETE ON `party_detail` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_detail_log SET
`party_detail_id` =OLD.party_detail_id,
`party_id` =OLD.party_id,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_detail_edit_log` AFTER UPDATE ON `party_detail` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_detail_log SET
`party_detail_id` =NEW.party_detail_id,
`party_id` =NEW.party_id,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `party_log`
--

CREATE TABLE `party_log` (
  `id` int(11) NOT NULL,
  `operation_type` varchar(255) NOT NULL,
  `party_id` int(11) NOT NULL,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `reference_description` text NOT NULL,
  `party_type_1` int(11) DEFAULT NULL,
  `party_type_2` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `project` text,
  `address` text NOT NULL,
  `area` text,
  `pincode` varchar(15) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone_no` text NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `email_id` text NOT NULL,
  `website` varchar(255) NOT NULL,
  `opening_bal` double NOT NULL,
  `credit_limit` double NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active ,2-Deactive',
  `tin_vat_no` varchar(100) NOT NULL,
  `tin_cst_no` varchar(100) NOT NULL,
  `ecc_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `range` varchar(100) NOT NULL,
  `division` varchar(100) NOT NULL,
  `service_tax_no` varchar(100) NOT NULL,
  `utr_no` varchar(225) DEFAULT NULL,
  `address_work` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Yes | 0-No = Work Address same as Party Address ',
  `w_address` text NOT NULL,
  `w_city` int(11) DEFAULT NULL,
  `w_state` int(11) DEFAULT NULL,
  `w_country` int(11) DEFAULT NULL,
  `oldw_pincode` varchar(225) DEFAULT NULL,
  `w_email` text,
  `w_web` text NOT NULL,
  `w_phone1` text,
  `w_phone2` text,
  `w_phone3` text,
  `w_note` text,
  `oldPartyType` varchar(225) DEFAULT NULL,
  `oldHoliday` varchar(225) DEFAULT NULL,
  `oldST_No` varchar(225) DEFAULT NULL,
  `oldVendorCode` varchar(225) DEFAULT NULL,
  `oldw_fax` varchar(225) DEFAULT NULL,
  `oldacct_no` varchar(225) DEFAULT NULL,
  `oldReason` varchar(225) DEFAULT NULL,
  `oldAllowMultipalBill` varchar(225) DEFAULT NULL,
  `oldAddCity` varchar(225) DEFAULT NULL,
  `oldCreatedBy` varchar(225) DEFAULT NULL,
  `oldCreateDate` date DEFAULT NULL,
  `oldModifyBy` varchar(225) DEFAULT NULL,
  `oldModiDate` date DEFAULT NULL,
  `oldPrintFlag` varchar(225) DEFAULT NULL,
  `oldPT` varchar(225) DEFAULT NULL,
  `oldCPerson` varchar(225) DEFAULT NULL,
  `oldOpType` varchar(225) DEFAULT NULL,
  `oldCompProfile` varchar(225) DEFAULT NULL,
  `oldBranchCode` varchar(225) DEFAULT NULL,
  `oldAreaName` varchar(225) DEFAULT NULL,
  `oldCreditLimit` varchar(225) DEFAULT NULL,
  `oldAcctGrpCode` varchar(225) DEFAULT NULL,
  `oldOpBalDr` varchar(225) DEFAULT NULL,
  `oldApprovedBy` varchar(225) DEFAULT NULL,
  `oldApprovedDate` date DEFAULT NULL,
  `oldPStage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `party_old`
--

CREATE TABLE `party_old` (
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `fax_no` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `tin_cst_no` varchar(255) NOT NULL,
  `service_tax_no` varchar(255) NOT NULL,
  `ecc_no` varchar(255) NOT NULL,
  `pan_no` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `range` varchar(255) NOT NULL,
  `division` varchar(255) NOT NULL,
  `w_address` varchar(255) NOT NULL,
  `w_phone1` varchar(255) NOT NULL,
  `w_email` varchar(255) NOT NULL,
  `w_web` varchar(255) NOT NULL,
  `opening_bal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `party_old`
--
DELIMITER $$
CREATE TRIGGER `party_old_add_log` AFTER INSERT ON `party_old` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_old_log SET
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`fax_no` =NEW.fax_no,
`website` =NEW.website,
`email_id` =NEW.email_id,
`tin_cst_no` =NEW.tin_cst_no,
`service_tax_no` =NEW.service_tax_no,
`ecc_no` =NEW.ecc_no,
`pan_no` =NEW.pan_no,
`active` =NEW.active,
`range` =NEW.range,
`division` =NEW.division,
`w_address` =NEW.w_address,
`w_phone1` =NEW.w_phone1,
`w_email` =NEW.w_email,
`w_web` =NEW.w_web,
`opening_bal` =NEW.opening_bal,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_old_delete_log` AFTER DELETE ON `party_old` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_old_log SET
`party_code` =OLD.party_code,
`party_name` =OLD.party_name,
`address` =OLD.address,
`pincode` =OLD.pincode,
`phone_no` =OLD.phone_no,
`fax_no` =OLD.fax_no,
`website` =OLD.website,
`email_id` =OLD.email_id,
`tin_cst_no` =OLD.tin_cst_no,
`service_tax_no` =OLD.service_tax_no,
`ecc_no` =OLD.ecc_no,
`pan_no` =OLD.pan_no,
`active` =OLD.active,
`range` =OLD.range,
`division` =OLD.division,
`w_address` =OLD.w_address,
`w_phone1` =OLD.w_phone1,
`w_email` =OLD.w_email,
`w_web` =OLD.w_web,
`opening_bal` =OLD.opening_bal,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_old_edit_log` AFTER UPDATE ON `party_old` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_old_log SET
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`fax_no` =NEW.fax_no,
`website` =NEW.website,
`email_id` =NEW.email_id,
`tin_cst_no` =NEW.tin_cst_no,
`service_tax_no` =NEW.service_tax_no,
`ecc_no` =NEW.ecc_no,
`pan_no` =NEW.pan_no,
`active` =NEW.active,
`range` =NEW.range,
`division` =NEW.division,
`w_address` =NEW.w_address,
`w_phone1` =NEW.w_phone1,
`w_email` =NEW.w_email,
`w_web` =NEW.w_web,
`opening_bal` =NEW.opening_bal,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `party_type_1`
--

CREATE TABLE `party_type_1` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `party_type_1`
--
DELIMITER $$
CREATE TRIGGER `party_type_1_add_log` AFTER INSERT ON `party_type_1` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_1_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_type_1_delete_log` AFTER DELETE ON `party_type_1` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_1_log SET
`id`=OLD.id,
`type` =OLD.type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_type_1_edit_log` AFTER UPDATE ON `party_type_1` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_1_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `party_type_2`
--

CREATE TABLE `party_type_2` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `party_type_2`
--
DELIMITER $$
CREATE TRIGGER `party_type_2_add_log` AFTER INSERT ON `party_type_2` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_2_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_type_2_delete_log` AFTER DELETE ON `party_type_2` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_2_log SET
`id`=OLD.id,
`type` =OLD.type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `party_type_2_edit_log` AFTER UPDATE ON `party_type_2` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_2_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `payment_date` date DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `payment_note` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `payment`
--
DELIMITER $$
CREATE TRIGGER `payment_add_log` AFTER INSERT ON `payment` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.payment_log SET
`payment_id` =NEW.payment_id,
`party_id` =NEW.party_id,
`payment_date` =NEW.payment_date,
`amount` =NEW.amount,
`payment_type` =NEW.payment_type,
`payment_note` =NEW.payment_note,
`created_by` =NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `payment_delete_log` AFTER DELETE ON `payment` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.payment_log SET
`payment_id` =OLD.payment_id,
`party_id` =OLD.party_id,
`payment_date` =OLD.payment_date,
`amount` =OLD.amount,
`payment_type` =OLD.payment_type,
`payment_note` =OLD.payment_note,
`created_by` =OLD.created_by,
`updated_by` =OLD.updated_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `payment_edit_log` AFTER UPDATE ON `payment` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.payment_log SET
`payment_id` =NEW.payment_id,
`party_id` =NEW.party_id,
`payment_date` =NEW.payment_date,
`amount` =NEW.amount,
`payment_type` =NEW.payment_type,
`payment_note` =NEW.payment_note,
`created_by` =NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `id` int(11) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `priority`
--
DELIMITER $$
CREATE TRIGGER `priority_add_log` AFTER INSERT ON `priority` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.priority_log SET
`id`=NEW.id,
`priority` =NEW.priority,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `priority_delete_log` AFTER DELETE ON `priority` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.priority_log SET
`id`=OLD.id,
`priority` =OLD.priority,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `priority_edit_log` AFTER UPDATE ON `priority` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.priority_log SET
`id`=NEW.id,
`priority` =NEW.priority,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `product_group`
--

CREATE TABLE `product_group` (
  `id` int(11) NOT NULL,
  `name` varchar(222) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoices`
--

CREATE TABLE `proforma_invoices` (
  `id` int(11) NOT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `proforma_invoice_no` varchar(255) DEFAULT NULL,
  `quotation_no` varchar(255) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `ship_to_party_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `billing_terms_id` int(11) NOT NULL,
  `sales_order_date` date DEFAULT NULL,
  `committed_date` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `contact_no` varchar(15) NOT NULL,
  `cust_po_no` varchar(50) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `po_date` date NOT NULL,
  `sales_order_pref_id` int(11) DEFAULT NULL,
  `conversation_rate` varchar(50) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_order_stage_id` int(11) DEFAULT NULL,
  `sales_order_status_id` int(11) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `buyer_detail_id` int(11) DEFAULT NULL,
  `note_detail_id` int(11) DEFAULT NULL,
  `login_detail_id` int(11) DEFAULT NULL,
  `transportation_by_id` int(11) DEFAULT NULL,
  `transportation_price` decimal(18,2) DEFAULT NULL,
  `foundation_drawing_required_id` int(11) DEFAULT NULL,
  `loading_by_id` int(11) DEFAULT NULL,
  `inspection_required_id` int(11) DEFAULT NULL,
  `unloading_by_id` int(11) DEFAULT NULL,
  `erection_commissioning_id` int(11) DEFAULT NULL,
  `road_insurance_by_id` int(11) DEFAULT NULL,
  `road_insurance_price` decimal(18,2) DEFAULT NULL,
  `for_required_id` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `mach_deli_min_weeks` int(11) DEFAULT NULL,
  `mach_deli_max_weeks` int(11) DEFAULT NULL,
  `mach_deli_min_weeks_date` date DEFAULT NULL,
  `mach_deli_max_weeks_date` date DEFAULT NULL,
  `sales_order_validaty` varchar(225) DEFAULT NULL,
  `sales_order_quotation_delivery` varchar(225) DEFAULT NULL,
  `mode_of_shipment_name` varchar(225) DEFAULT NULL,
  `mode_of_shipment_truck_number` varchar(225) DEFAULT NULL,
  `supplier_payment_terms` text,
  `received_payment` varchar(255) DEFAULT NULL,
  `received_payment_date` date DEFAULT NULL,
  `received_payment_type` varchar(255) DEFAULT NULL,
  `received_payment_cheque_no` varchar(255) DEFAULT NULL,
  `terms_condition_purchase` text,
  `note` text,
  `isApproved` int(11) DEFAULT '0',
  `pdf_url` text,
  `loading_at` varchar(255) DEFAULT NULL,
  `port_of_discharge` varchar(255) DEFAULT NULL,
  `place_of_delivery` varchar(255) DEFAULT NULL,
  `port_of_loading` varchar(255) DEFAULT NULL,
  `delivery_through_id` int(11) DEFAULT NULL,
  `freight` varchar(255) DEFAULT NULL,
  `freight_cgst` int(11) DEFAULT NULL,
  `freight_sgst` int(11) DEFAULT NULL,
  `freight_igst` int(11) DEFAULT NULL,
  `packing_forwarding` varchar(255) DEFAULT NULL,
  `packing_forwarding_cgst` int(11) DEFAULT NULL,
  `packing_forwarding_sgst` int(11) DEFAULT NULL,
  `packing_forwarding_igst` int(11) DEFAULT NULL,
  `transit_insurance` varchar(255) DEFAULT NULL,
  `transit_insurance_cgst` int(11) DEFAULT NULL,
  `transit_insurance_sgst` int(11) DEFAULT NULL,
  `transit_insurance_igst` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `proforma_invoices`
--
DELIMITER $$
CREATE TRIGGER `proforma_invoices_delete_log` AFTER DELETE ON `proforma_invoices` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoices_log SET
`id`=OLD.id,
`quotation_id` =OLD.quotation_id,
`proforma_invoice_no` =OLD.proforma_invoice_no,
`quotation_no` =OLD.quotation_no,
`sales_to_party_id` =OLD.sales_to_party_id,
`ship_to_party_id` =OLD.ship_to_party_id,
`branch_id` =OLD.branch_id,
`billing_terms_id` =OLD.billing_terms_id,
`sales_order_date` =OLD.sales_order_date,
`committed_date` =OLD.committed_date,
`currency_id` =OLD.currency_id,
`project_id` =OLD.project_id,
`kind_attn_id` =OLD.kind_attn_id,
`contact_no` =OLD.contact_no,
`cust_po_no` =OLD.cust_po_no,
`email_id` =OLD.email_id,
`po_date` =OLD.po_date,
`sales_order_pref_id` =OLD.sales_order_pref_id,
`conversation_rate` =OLD.conversation_rate,
`sales_id` =OLD.sales_id,
`sales_order_stage_id` =OLD.sales_order_stage_id,
`sales_order_status_id` =OLD.sales_order_status_id,
`order_type` =OLD.order_type,
`lead_provider_id` =OLD.lead_provider_id,
`buyer_detail_id` =OLD.buyer_detail_id,
`note_detail_id` =OLD.note_detail_id,
`login_detail_id` =OLD.login_detail_id,
`transportation_by_id` =OLD.transportation_by_id,
`foundation_drawing_required_id` =OLD.foundation_drawing_required_id,
`loading_by_id` =OLD.loading_by_id,
`inspection_required_id` =OLD.inspection_required_id,
`unloading_by_id` =OLD.unloading_by_id,
`erection_commissioning_id` =OLD.erection_commissioning_id,
`road_insurance_by_id` =OLD.road_insurance_by_id,
`for_required_id` =OLD.for_required_id,
`review_date` =OLD.review_date,
`mach_deli_min_weeks` =OLD.mach_deli_min_weeks,
`mach_deli_max_weeks` =OLD.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =OLD.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =OLD.mach_deli_max_weeks_date,
`sales_order_validaty` =OLD.sales_order_validaty,
`sales_order_quotation_delivery` =OLD.sales_order_quotation_delivery,
`mode_of_shipment_name` =OLD.mode_of_shipment_name,
`mode_of_shipment_truck_number` =OLD.mode_of_shipment_truck_number,
`supplier_payment_terms` =OLD.supplier_payment_terms,
`received_payment` =OLD.received_payment,
`received_payment_date` =OLD.received_payment_date,
`received_payment_type` =OLD.received_payment_type,
`terms_condition_purchase` =OLD.terms_condition_purchase,
`note` =OLD.note,
`isApproved` =OLD.isApproved,
`pdf_url` =OLD.pdf_url,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`created_by` =OLD.created_by,
`operation_type` = 'delete'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_billing_terms`
--

CREATE TABLE `proforma_invoice_billing_terms` (
  `id` int(11) NOT NULL,
  `proforma_invoice_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `proforma_invoice_billing_terms`
--
DELIMITER $$
CREATE TRIGGER `proforma_invoice_billing_terms_add_log` AFTER INSERT ON `proforma_invoice_billing_terms` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_billing_terms_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_billing_terms_delete_log` AFTER DELETE ON `proforma_invoice_billing_terms` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_billing_terms_log SET
`id`=OLD.id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`cal_code` =OLD.cal_code,
`narration` =OLD.narration,
`cal_definition` =OLD.cal_definition,
`percentage` =OLD.percentage,
`value` =OLD.value,
`gl_acc` =OLD.gl_acc,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_billing_terms_edit_log` AFTER UPDATE ON `proforma_invoice_billing_terms` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_billing_terms_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_cal_code_definition`
--

CREATE TABLE `proforma_invoice_cal_code_definition` (
  `id` int(11) NOT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `proforma_invoice_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `proforma_invoice_cal_code_definition`
--
DELIMITER $$
CREATE TRIGGER `proforma_invoice_cal_code_definition_add_log` AFTER INSERT ON `proforma_invoice_cal_code_definition` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_cal_code_definition_delete_log` AFTER DELETE ON `proforma_invoice_cal_code_definition` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_cal_code_definition_log SET
`id`=OLD.id,
`billing_terms_detail_id` =OLD.billing_terms_detail_id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`is_first_element` =OLD.is_first_element,
`cal_operation` =OLD.cal_operation,
`cal_code_id` =OLD.cal_code_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_cal_code_definition_edit_log` AFTER UPDATE ON `proforma_invoice_cal_code_definition` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_items`
--

CREATE TABLE `proforma_invoice_items` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `proforma_invoice_id` int(11) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_desc` text,
  `item_description` text NOT NULL,
  `add_description` varchar(255) NOT NULL,
  `detail_description` text NOT NULL,
  `drawing_number` varchar(255) NOT NULL,
  `drawing_revision` varchar(255) NOT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) NOT NULL,
  `igst` int(11) DEFAULT NULL,
  `igst_amount` int(11) DEFAULT NULL,
  `cgst` int(11) DEFAULT NULL,
  `cgst_amount` int(11) DEFAULT NULL,
  `sgst` int(11) DEFAULT NULL,
  `sgst_amount` int(11) DEFAULT NULL,
  `total_amount` double NOT NULL,
  `item_note` text NOT NULL,
  `quantity` double NOT NULL,
  `disc_per` double NOT NULL,
  `rate` double NOT NULL,
  `disc_value` double NOT NULL,
  `amount` double NOT NULL,
  `net_amount` double NOT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) NOT NULL,
  `cust_part_no` varchar(255) NOT NULL,
  `item_extra_accessories_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `proforma_invoice_items`
--
DELIMITER $$
CREATE TRIGGER `proforma_invoice_items_add_log` AFTER INSERT ON `proforma_invoice_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_items_delete_log` AFTER DELETE ON `proforma_invoice_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_items_log SET
`id`=OLD.id,
`item_id` =OLD.item_id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`item_code` =OLD.item_code,
`item_category_id` =OLD.item_category_id,
`item_name` =OLD.item_name,
`item_desc` =OLD.item_desc,
`item_description` =OLD.item_description,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`drawing_revision` =OLD.drawing_revision,
`uom_id`=OLD.uom_id,
`uom` =OLD.uom,
`total_amount` =OLD.total_amount,
`item_note` =OLD.item_note,
`quantity` =OLD.quantity,
`disc_per` =OLD.disc_per,
`rate` =OLD.rate,
`disc_value` =OLD.disc_value,
`amount` =OLD.amount,
`net_amount` =OLD.net_amount,
`item_status_id` =OLD.item_status_id,
`item_status` =OLD.item_status,
`cust_part_no` =OLD.cust_part_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_items_edit_log` AFTER UPDATE ON `proforma_invoice_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_logins`
--

CREATE TABLE `proforma_invoice_logins` (
  `id` int(11) NOT NULL,
  `proforma_invoice_id` int(11) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_modified_by` varchar(255) NOT NULL,
  `last_modified_by_id` int(11) NOT NULL,
  `modified_date_time` datetime NOT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `prepared_by_id` int(11) NOT NULL,
  `prepared_date_time` datetime NOT NULL,
  `approved_by` varchar(255) NOT NULL,
  `approved_by_id` int(11) NOT NULL,
  `approved_date_time` datetime NOT NULL,
  `stage_id` int(11) NOT NULL,
  `stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `proforma_invoice_logins`
--
DELIMITER $$
CREATE TRIGGER `proforma_invoice_logins_add_log` AFTER INSERT ON `proforma_invoice_logins` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_logins_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_logins_delete_log` AFTER DELETE ON `proforma_invoice_logins` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_logins_log SET
`id`=OLD.id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`created_by` =OLD.created_by,
`created_by_id` =OLD.created_by_id,
`created_date_time` =OLD.created_date_time,
`last_modified_by` =OLD.last_modified_by,
`last_modified_by_id` =OLD.last_modified_by_id,
`modified_date_time` =OLD.modified_date_time,
`prepared_by` =OLD.prepared_by,
`prepared_by_id` =OLD.prepared_by_id,
`prepared_date_time` =OLD.prepared_date_time,
`approved_by` =OLD.approved_by,
`approved_by_id` =OLD.approved_by_id,
`approved_date_time` =OLD.approved_date_time,
`stage_id` =OLD.stage_id,
`stage` =OLD.stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `proforma_invoice_logins_edit_log` AFTER UPDATE ON `proforma_invoice_logins` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_logins_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `project` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `project`
--
DELIMITER $$
CREATE TRIGGER `project_add_log` AFTER INSERT ON `project` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.project_log SET
`id`=NEW.id,
`project` =NEW.project,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `project_delete_log` AFTER DELETE ON `project` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.project_log SET
`id`=OLD.id,
`project` =OLD.project,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `project_edit_log` AFTER UPDATE ON `project` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.project_log SET
`id`=NEW.id,
`project` =NEW.project,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_invoice`
--

CREATE TABLE `purchase_invoice` (
  `invoice_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL COMMENT 'supplier_id',
  `address` text NOT NULL,
  `note` text,
  `created_by` int(11) NOT NULL COMMENT 'user_id , staff_id',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `purchase_invoice`
--
DELIMITER $$
CREATE TRIGGER `purchase_invoice_add_log` AFTER INSERT ON `purchase_invoice` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_log SET
`invoice_id` =NEW.invoice_id,
`invoice_no` =NEW.invoice_no,
`invoice_date` =NEW.invoice_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_invoice_delete_log` AFTER DELETE ON `purchase_invoice` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_log SET
`invoice_id` =OLD.invoice_id,
`invoice_no` =OLD.invoice_no,
`invoice_date` =OLD.invoice_date,
`supplier_id` =OLD.supplier_id,
`address` =OLD.address,
`note` =OLD.note,
`created_by` =OLD.created_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_invoice_edit_log` AFTER UPDATE ON `purchase_invoice` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_log SET
`invoice_id` =NEW.invoice_id,
`invoice_no` =NEW.invoice_no,
`invoice_date` =NEW.invoice_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_invoice_items`
--

CREATE TABLE `purchase_invoice_items` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `item_data` text NOT NULL,
  `invoice_flag` int(11) DEFAULT '0',
  `quotation_flag` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `purchase_invoice_items`
--
DELIMITER $$
CREATE TRIGGER `purchase_invoice_items_add_log` AFTER INSERT ON `purchase_invoice_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_items_log SET
`id`=NEW.id,
`invoice_id` =NEW.invoice_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_invoice_items_delete_log` AFTER DELETE ON `purchase_invoice_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_items_log SET
`id`=OLD.id,
`invoice_id` =OLD.invoice_id,
`item_id` =OLD.item_id,
`quantity` =OLD.quantity,
`item_data` =OLD.item_data,
`invoice_flag` =OLD.invoice_flag,
`quotation_flag` =OLD.quotation_flag,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_invoice_items_edit_log` AFTER UPDATE ON `purchase_invoice_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_items_log SET
`id`=NEW.id,
`invoice_id` =NEW.invoice_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_items`
--

CREATE TABLE `purchase_items` (
  `id` int(11) NOT NULL,
  `item_category` varchar(255) NOT NULL,
  `item_code1` int(11) DEFAULT NULL,
  `item_code2` varchar(255) DEFAULT NULL,
  `uom` varchar(255) NOT NULL,
  `qty` double NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `document` varchar(100) NOT NULL,
  `hsn_code` varchar(255) NOT NULL,
  `item_group` varchar(255) NOT NULL,
  `product_group` int(11) DEFAULT NULL,
  `product_code` varchar(222) DEFAULT NULL,
  `item_active` int(11) NOT NULL COMMENT '0- Active 1-Deactive',
  `item_name` varchar(255) NOT NULL,
  `cfactor` varchar(255) NOT NULL,
  `conv_uom` varchar(255) NOT NULL,
  `conv_qty` double NOT NULL,
  `item_mpt` varchar(255) NOT NULL,
  `item_status` varchar(255) NOT NULL,
  `igst` int(11) DEFAULT NULL,
  `cgst` int(11) DEFAULT NULL,
  `sgst` int(11) DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `order_id` int(11) NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL COMMENT 'supplier_id',
  `address` text NOT NULL,
  `note` text,
  `mach_deli_min_weeks_date` date DEFAULT NULL,
  `mach_deli_max_weeks_date` date DEFAULT NULL,
  `pdf_url` text,
  `mach_deli_min_weeks` int(11) DEFAULT NULL,
  `mach_deli_max_weeks` int(11) DEFAULT NULL,
  `purchase_quote_ref` varchar(255) DEFAULT NULL,
  `round_off` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL COMMENT 'user_id , staff_id',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `purchase_order`
--
DELIMITER $$
CREATE TRIGGER `purchase_order_add_log` AFTER INSERT ON `purchase_order` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_log SET
`order_id` =NEW.order_id,
`order_no` =NEW.order_no,
`order_date` =NEW.order_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_order_delete_log` AFTER DELETE ON `purchase_order` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_log SET
`order_id` =OLD.order_id,
`order_no` =OLD.order_no,
`order_date` =OLD.order_date,
`supplier_id` =OLD.supplier_id,
`address` =OLD.address,
`note` =OLD.note,
`created_by` =OLD.created_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_order_edit_log` AFTER UPDATE ON `purchase_order` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_log SET
`order_id` =NEW.order_id,
`order_no` =NEW.order_no,
`order_date` =NEW.order_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items`
--

CREATE TABLE `purchase_order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `item_data` text NOT NULL,
  `invoice_flag` int(11) DEFAULT '0',
  `quotation_flag` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `purchase_order_items`
--
DELIMITER $$
CREATE TRIGGER `purchase_order_items_add_log` AFTER INSERT ON `purchase_order_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_items_log SET
`id`=NEW.id,
`order_id` =NEW.order_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_order_items_delete_log` AFTER DELETE ON `purchase_order_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_items_log SET
`id`=OLD.id,
`order_id` =OLD.order_id,
`item_id` =OLD.item_id,
`quantity` =OLD.quantity,
`item_data` =OLD.item_data,
`invoice_flag` =OLD.invoice_flag,
`quotation_flag` =OLD.quotation_flag,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_order_items_edit_log` AFTER UPDATE ON `purchase_order_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_items_log SET
`id`=NEW.id,
`order_id` =NEW.order_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `quotations`
--

CREATE TABLE `quotations` (
  `id` int(11) NOT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `revised_from_id` int(11) DEFAULT '0',
  `party_id` int(11) DEFAULT NULL,
  `enquiry_no` varchar(255) DEFAULT NULL,
  `quotation_no` varchar(255) DEFAULT NULL,
  `quotation_stage_id` int(11) DEFAULT NULL,
  `quotation_type_id` int(11) DEFAULT NULL,
  `quotation_status_id` int(11) DEFAULT NULL,
  `sales_type_id` int(11) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `rev` double DEFAULT NULL,
  `rev_date` date DEFAULT NULL,
  `quotation_note` text,
  `reason` varchar(255) DEFAULT NULL,
  `reason_remark` varchar(255) DEFAULT NULL,
  `review_date_check` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `enquiry_date` date DEFAULT NULL,
  `quotation_date` date DEFAULT NULL,
  `due_date_check` int(11) DEFAULT NULL,
  `oldEnquiryNo` varchar(225) DEFAULT NULL,
  `oldSubject` varchar(225) DEFAULT NULL,
  `oldPreparedBy` varchar(225) DEFAULT NULL,
  `oldApprovedBy` varchar(225) DEFAULT NULL,
  `oldSrNo` varchar(225) DEFAULT NULL,
  `oldExpr1` varchar(225) DEFAULT NULL,
  `oldExpr2` varchar(225) DEFAULT NULL,
  `oldEnquiryDate` date DEFAULT NULL,
  `oldKindAttn` varchar(225) DEFAULT NULL,
  `oldEnquiryStatus` varchar(225) DEFAULT NULL,
  `oldEnqRef` varchar(225) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `conv_rate` double DEFAULT NULL,
  `reference_date` date DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `party_code` varchar(255) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `reference_id` text,
  `reference_description` varchar(255) DEFAULT NULL,
  `address` text,
  `agent_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `fax_no` varchar(50) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `phone_no` varchar(50) DEFAULT NULL,
  `pincode` varchar(10) DEFAULT NULL,
  `website` varchar(500) DEFAULT NULL,
  `contact_person_name` varchar(255) DEFAULT NULL,
  `contact_person_contact_no` varchar(50) DEFAULT NULL,
  `contact_person_email_id` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `header` text,
  `footer` text,
  `cc_to` varchar(255) DEFAULT NULL,
  `authorised_by` varchar(255) DEFAULT NULL,
  `reference_note` text,
  `item_dec_title` varchar(255) DEFAULT NULL,
  `addon_title` text,
  `specification_1` text,
  `specification_2` text,
  `specification_3` text,
  `send_from_post` tinyint(1) DEFAULT NULL,
  `send_from_post_name` text,
  `pdf_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `quotations`
--
DELIMITER $$
CREATE TRIGGER `quotations_add_log` AFTER INSERT ON `quotations` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotations_log SET
`id`=NEW.id,
`enquiry_id` =NEW.enquiry_id,
`revised_from_id` =NEW.revised_from_id,
`party_id` =NEW.party_id,
`enquiry_no` =NEW.enquiry_no,
`quotation_no` =NEW.quotation_no,
`quotation_stage_id` =NEW.quotation_stage_id,
`quotation_type_id` =NEW.quotation_type_id,
`quotation_status_id` =NEW.quotation_status_id,
`sales_type_id` =NEW.sales_type_id,
`sales_id` =NEW.sales_id,
`action` =NEW.action,
`currency_id` =NEW.currency_id,
`rev` =NEW.rev,
`rev_date` =NEW.rev_date,
`quotation_note` =NEW.quotation_note,
`reason` =NEW.reason,
`reason_remark` =NEW.reason_remark,
`review_date_check` =NEW.review_date_check,
`review_date` =NEW.review_date,
`enquiry_date` =NEW.enquiry_date,
`quotation_date` =NEW.quotation_date,
`due_date_check` =NEW.due_date_check,
`oldEnquiryNo` =NEW.oldEnquiryNo,
`oldSubject` =NEW.oldSubject,
`oldPreparedBy` =NEW.oldPreparedBy,
`oldApprovedBy` =NEW.oldApprovedBy,
`oldSrNo` =NEW.oldSrNo,
`oldExpr1` =NEW.oldExpr1,
`oldExpr2` =NEW.oldExpr2,
`oldEnquiryDate` =NEW.oldEnquiryDate,
`oldKindAttn` =NEW.oldKindAttn,
`oldEnquiryStatus` =NEW.oldEnquiryStatus,
`oldEnqRef` =NEW.oldEnqRef,
`due_date` =NEW.due_date,
`conv_rate` =NEW.conv_rate,
`reference_date` =NEW.reference_date,
`kind_attn_id` =NEW.kind_attn_id,
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`reference_id` =NEW.reference_id,
`reference_description` =NEW.reference_description,
`address` =NEW.address,
`agent_id` =NEW.agent_id,
`city_id` =NEW.city_id,
`state_id` =NEW.state_id,
`country_id` =NEW.country_id,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`project` =NEW.project,
`phone_no` =NEW.phone_no,
`pincode` =NEW.pincode,
`website` =NEW.website,
`contact_person_name` =NEW.contact_person_name,
`contact_person_contact_no` =NEW.contact_person_contact_no,
`contact_person_email_id` =NEW.contact_person_email_id,
`subject` =NEW.subject,
`header` =NEW.header,
`footer` =NEW.footer,
`cc_to` =NEW.cc_to,
`authorised_by` =NEW.authorised_by,
`reference_note` =NEW.reference_note,
`item_dec_title` =NEW.item_dec_title,
`addon_title` =NEW.addon_title,
`specification_1` =NEW.specification_1,
`specification_2` =NEW.specification_2,
`specification_3` =NEW.specification_3,
`send_from_post` =NEW.send_from_post,
`send_from_post_name` =NEW.send_from_post_name,
`pdf_url` =NEW.pdf_url,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`operation_type` = 'add'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_items`
--

CREATE TABLE `quotation_items` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `item_desc` text NOT NULL,
  `add_description` varchar(255) NOT NULL,
  `detail_description` text NOT NULL,
  `drawing_number` varchar(255) NOT NULL,
  `drawing_revision` varchar(255) NOT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) NOT NULL,
  `total_amount` double NOT NULL,
  `item_note` text NOT NULL,
  `quantity` double NOT NULL,
  `disc_per` double NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `rate` double NOT NULL,
  `disc_value` double NOT NULL,
  `amount` double NOT NULL,
  `net_amount` double NOT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) NOT NULL,
  `cust_part_no` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `quotation_items`
--
DELIMITER $$
CREATE TRIGGER `quotation_items_add_log` AFTER INSERT ON `quotation_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`quotation_id` =NEW.quotation_id,
`item_code` =NEW.item_code,
`item_name` =NEW.item_name,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_items_delete_log` AFTER DELETE ON `quotation_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_items_log SET
`id`=OLD.id,
`item_id` =OLD.item_id,
`quotation_id` =OLD.quotation_id,
`item_code` =OLD.item_code,
`item_name` =OLD.item_name,
`item_description` =OLD.item_description,
`item_desc`=OLD.item_desc,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`drawing_revision` =OLD.drawing_revision,
`uom_id`=OLD.uom_id,
`uom` =OLD.uom,
`total_amount` =OLD.total_amount,
`item_note` =OLD.item_note,
`quantity` =OLD.quantity,
`disc_per` =OLD.disc_per,
`rate` =OLD.rate,
`disc_value` =OLD.disc_value,
`amount` =OLD.amount,
`net_amount` =OLD.net_amount,
`item_status_id` =OLD.item_status_id,
`item_status` =OLD.item_status,
`cust_part_no` =OLD.cust_part_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_items_edit_log` AFTER UPDATE ON `quotation_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`quotation_id` =NEW.quotation_id,
`item_code` =NEW.item_code,
`item_name` =NEW.item_name,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_reason`
--

CREATE TABLE `quotation_reason` (
  `id` int(11) NOT NULL,
  `quotation_reason` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `quotation_reason`
--
DELIMITER $$
CREATE TRIGGER `quotation_reason_add_log` AFTER INSERT ON `quotation_reason` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_reason_log SET
`id`=NEW.id,
`quotation_reason` =NEW.quotation_reason,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_reason_delete_log` AFTER DELETE ON `quotation_reason` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_reason_log SET
`id`=OLD.id,
`quotation_reason` =OLD.quotation_reason,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_reason_edit_log` AFTER UPDATE ON `quotation_reason` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_reason_log SET
`id`=NEW.id,
`quotation_reason` =NEW.quotation_reason,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_reminder_roles`
--

CREATE TABLE `quotation_reminder_roles` (
  `qr_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `allowed_staff_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_stage`
--

CREATE TABLE `quotation_stage` (
  `id` int(11) NOT NULL,
  `quotation_stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `quotation_stage`
--
DELIMITER $$
CREATE TRIGGER `quotation_stage_add_log` AFTER INSERT ON `quotation_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_stage_log SET
`id`=NEW.id,
`quotation_stage` =NEW.quotation_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_stage_delete_log` AFTER DELETE ON `quotation_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_stage_log SET
`id`=OLD.id,
`quotation_stage` =OLD.quotation_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_stage_edit_log` AFTER UPDATE ON `quotation_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_stage_log SET
`id`=NEW.id,
`quotation_stage` =NEW.quotation_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_status`
--

CREATE TABLE `quotation_status` (
  `id` int(11) NOT NULL,
  `quotation_status` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `quotation_status`
--
DELIMITER $$
CREATE TRIGGER `quotation_status_add_log` AFTER INSERT ON `quotation_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_status_log SET
`id`=NEW.id,
`quotation_status` =NEW.quotation_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_status_delete_log` AFTER DELETE ON `quotation_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_status_log SET
`id`=OLD.id,
`quotation_status` =OLD.quotation_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_status_edit_log` AFTER UPDATE ON `quotation_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_status_log SET
`id`=NEW.id,
`quotation_status` =NEW.quotation_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_type`
--

CREATE TABLE `quotation_type` (
  `id` int(11) NOT NULL,
  `quotation_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `quotation_type`
--
DELIMITER $$
CREATE TRIGGER `quotation_type_add_log` AFTER INSERT ON `quotation_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_type_log SET
`id`=NEW.id,
`quotation_type` =NEW.quotation_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_type_delete_log` AFTER DELETE ON `quotation_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_type_log SET
`id`=OLD.id,
`quotation_type` =OLD.quotation_type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `quotation_type_edit_log` AFTER UPDATE ON `quotation_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_type_log SET
`id`=NEW.id,
`quotation_type` =NEW.quotation_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `real_leave`
--

CREATE TABLE `real_leave` (
  `leave_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `leave_type` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `real_leave`
--
DELIMITER $$
CREATE TRIGGER `real_leave_add_log` AFTER INSERT ON `real_leave` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.real_leave_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`created_at` =NEW.created_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `real_leave_delete_log` AFTER DELETE ON `real_leave` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.real_leave_log SET
`leave_id`=OLD.leave_id,
`employee_id` =OLD.employee_id,
`from_date` =OLD.from_date,
`to_date` =OLD.to_date,
`leave_type` =OLD.leave_type,
`created_at` =OLD.created_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `real_leave_edit_log` AFTER UPDATE ON `real_leave` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.real_leave_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`created_at` =NEW.created_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `reference`
--

CREATE TABLE `reference` (
  `id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `reference`
--
DELIMITER $$
CREATE TRIGGER `reference_add_log` AFTER INSERT ON `reference` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reference_log SET
`id`=NEW.id,
`reference` =NEW.reference,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `reference_delete_log` AFTER DELETE ON `reference` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reference_log SET
`id`=OLD.id,
`reference` =OLD.reference,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `reference_edit_log` AFTER UPDATE ON `reference` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reference_log SET
`id`=NEW.id,
`reference` =NEW.reference,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `reminder`
--

CREATE TABLE `reminder` (
  `reminder_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `remind` text NOT NULL,
  `reminder_date` datetime NOT NULL,
  `is_notified` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=not_remind,1= remind',
  `assigned_to` int(11) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `reminder`
--
DELIMITER $$
CREATE TRIGGER `reminder_add_log` AFTER INSERT ON `reminder` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reminder_log SET
`reminder_id` =NEW.reminder_id,
`user_id` =NEW.user_id,
`remind` =NEW.remind,
`reminder_date` =NEW.reminder_date,
`is_notified` =NEW.is_notified,
`assigned_to` =NEW.assigned_to,
`status` =NEW.status,
`priority` =NEW.priority,
`created_date` =NEW.created_date,
`last_updated_date` =NEW.last_updated_date,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `reminder_delete_log` AFTER DELETE ON `reminder` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reminder_log SET
`reminder_id` =OLD.reminder_id,
`user_id` =OLD.user_id,
`remind` =OLD.remind,
`reminder_date` =OLD.reminder_date,
`is_notified` =OLD.is_notified,
`assigned_to` =OLD.assigned_to,
`status` =OLD.status,
`priority` =OLD.priority,
`created_date` =OLD.created_date,
`last_updated_date` =OLD.last_updated_date,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `reminder_edit_log` AFTER UPDATE ON `reminder` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reminder_log SET
`reminder_id` =NEW.reminder_id,
`user_id` =NEW.user_id,
`remind` =NEW.remind,
`reminder_date` =NEW.reminder_date,
`is_notified` =NEW.is_notified,
`assigned_to` =NEW.assigned_to,
`status` =NEW.status,
`priority` =NEW.priority,
`created_date` =NEW.created_date,
`last_updated_date` =NEW.last_updated_date,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `road_insurance_by`
--

CREATE TABLE `road_insurance_by` (
  `id` int(11) NOT NULL,
  `road_insurance_by` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `road_insurance_by`
--
DELIMITER $$
CREATE TRIGGER `road_insurance_by_add_log` AFTER INSERT ON `road_insurance_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.road_insurance_by_log SET
`id`=NEW.id,
`road_insurance_by` =NEW.road_insurance_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `road_insurance_by_delete_log` AFTER DELETE ON `road_insurance_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.road_insurance_by_log SET
`id`=OLD.id,
`road_insurance_by` =OLD.road_insurance_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `road_insurance_by_edit_log` AFTER UPDATE ON `road_insurance_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.road_insurance_by_log SET
`id`=NEW.id,
`road_insurance_by` =NEW.road_insurance_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `sales` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales`
--
DELIMITER $$
CREATE TRIGGER `sales_add_log` AFTER INSERT ON `sales` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_log SET
`id`=NEW.id,
`sales` =NEW.sales,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_delete_log` AFTER DELETE ON `sales` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_log SET
`id`=OLD.id,
`sales` =OLD.sales,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_edit_log` AFTER UPDATE ON `sales` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_log SET
`id`=NEW.id,
`sales` =NEW.sales,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_action`
--

CREATE TABLE `sales_action` (
  `id` int(11) NOT NULL,
  `sales_action` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_action`
--
DELIMITER $$
CREATE TRIGGER `sales_action_add_log` AFTER INSERT ON `sales_action` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_action_log SET
`id`=NEW.id,
`sales_action` =NEW.sales_action,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_action_delete_log` AFTER DELETE ON `sales_action` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_action_log SET
`id`=OLD.id,
`sales_action` =OLD.sales_action,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_action_edit_log` AFTER UPDATE ON `sales_action` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_action_log SET
`id`=NEW.id,
`sales_action` =NEW.sales_action,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `id` int(11) NOT NULL,
  `quatation_id` int(11) NOT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `sales_order_no` varchar(255) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `ship_to_party_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `billing_terms_id` int(11) DEFAULT NULL,
  `sales_order_date` date DEFAULT NULL,
  `committed_date` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `contact_no` varchar(15) NOT NULL,
  `cust_po_no` varchar(50) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `po_date` date NOT NULL,
  `sales_order_pref_id` int(11) DEFAULT NULL,
  `conversation_rate` varchar(50) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_order_stage_id` int(11) DEFAULT NULL,
  `sales_order_status_id` int(11) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `buyer_detail_id` int(11) DEFAULT NULL,
  `note_detail_id` int(11) DEFAULT NULL,
  `login_detail_id` int(11) DEFAULT NULL,
  `transportation_by_id` int(11) DEFAULT NULL,
  `transport_amount` varchar(111) DEFAULT NULL,
  `transportation_by_cgst` double DEFAULT NULL,
  `transportation_by_sgst` double DEFAULT NULL,
  `transportation_by_igst` double DEFAULT NULL,
  `foundation_drawing_required_id` int(11) DEFAULT NULL,
  `loading_by_id` int(11) DEFAULT NULL,
  `inspection_required_id` int(11) DEFAULT NULL,
  `unloading_by_id` int(11) DEFAULT NULL,
  `erection_commissioning_id` int(11) DEFAULT NULL,
  `road_insurance_by_id` int(11) DEFAULT NULL,
  `insurance_amount` varchar(255) DEFAULT NULL,
  `road_insurance_by_cgst` double DEFAULT NULL,
  `road_insurance_by_sgst` double DEFAULT NULL,
  `road_insurance_by_igst` double DEFAULT NULL,
  `third_party_inspection_cost` varchar(255) DEFAULT NULL,
  `third_party_inspection_cost_amount` varchar(255) DEFAULT NULL,
  `third_party_inspection_cost_cgst` double DEFAULT NULL,
  `third_party_inspection_cost_sgst` double DEFAULT NULL,
  `third_party_inspection_cost_igst` double DEFAULT NULL,
  `oil_barrel` varchar(255) DEFAULT NULL,
  `oil_barrel_amount` varchar(255) DEFAULT NULL,
  `oil_barrel_cgst` double DEFAULT NULL,
  `oil_barrel_sgst` double DEFAULT NULL,
  `oil_barrel_igst` double DEFAULT NULL,
  `machine_to_all_motor_cable` varchar(255) DEFAULT NULL,
  `machine_to_all_motor_cable_amount` varchar(255) DEFAULT NULL,
  `machine_to_all_motor_cable_cgst` double DEFAULT NULL,
  `machine_to_all_motor_cable_sgst` double DEFAULT NULL,
  `machine_to_all_motor_cable_igst` double DEFAULT NULL,
  `packing_forwarding_amount` varchar(255) DEFAULT NULL,
  `packing_forwarding_cgst` double DEFAULT NULL,
  `packing_forwarding_sgst` double DEFAULT NULL,
  `packing_forwarding_igst` double DEFAULT NULL,
  `for_required_id` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `mach_deli_min_weeks` int(11) DEFAULT NULL,
  `mach_deli_max_weeks` int(11) DEFAULT NULL,
  `mach_deli_min_weeks_date` date DEFAULT NULL,
  `mach_deli_max_weeks_date` date DEFAULT NULL,
  `sales_order_validaty` varchar(225) DEFAULT NULL,
  `sales_order_quotation_delivery` varchar(225) DEFAULT NULL,
  `mode_of_shipment_name` varchar(225) DEFAULT NULL,
  `mode_of_shipment_truck_number` varchar(225) DEFAULT NULL,
  `supplier_payment_terms` text,
  `received_payment` varchar(255) DEFAULT NULL,
  `received_payment_date` date DEFAULT NULL,
  `received_payment_type` varchar(255) DEFAULT NULL,
  `received_payment_cheque_no` varchar(255) DEFAULT NULL,
  `terms_condition_purchase` text,
  `isApproved` int(11) DEFAULT '0',
  `pdf_url` text,
  `taxes_data` text,
  `more_items_data` text NOT NULL,
  `notes` text,
  `work_order_notes` text,
  `due_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order`
--
DELIMITER $$
CREATE TRIGGER `sales_order_add_log` AFTER INSERT ON `sales_order` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_log SET
`id`=NEW.id,
`quatation_id` =NEW.quatation_id,
`quotation_id` =NEW.quotation_id,
`sales_order_no` =NEW.sales_order_no,
`sales_to_party_id` =NEW.sales_to_party_id,
`ship_to_party_id` =NEW.ship_to_party_id,
`branch_id` =NEW.branch_id,
`billing_terms_id` =NEW.billing_terms_id,
`sales_order_date` =NEW.sales_order_date,
`committed_date` =NEW.committed_date,
`currency_id` =NEW.currency_id,
`project_id` =NEW.project_id,
`kind_attn_id` =NEW.kind_attn_id,
`contact_no` =NEW.contact_no,
`cust_po_no` =NEW.cust_po_no,
`email_id` =NEW.email_id,
`po_date` =NEW.po_date,
`sales_order_pref_id` =NEW.sales_order_pref_id,
`conversation_rate` =NEW.conversation_rate,
`sales_id` =NEW.sales_id,
`sales_order_stage_id` =NEW.sales_order_stage_id,
`sales_order_status_id` =NEW.sales_order_status_id,
`order_type` =NEW.order_type,
`lead_provider_id` =NEW.lead_provider_id,
`buyer_detail_id` =NEW.buyer_detail_id,
`note_detail_id` =NEW.note_detail_id,
`login_detail_id` =NEW.login_detail_id,
`transportation_by_id` =NEW.transportation_by_id,
`transport_amount` =NEW.transport_amount,
`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
`loading_by_id` =NEW.loading_by_id,
`inspection_required_id` =NEW.inspection_required_id,
`unloading_by_id` =NEW.unloading_by_id,
`erection_commissioning_id` =NEW.erection_commissioning_id,
`road_insurance_by_id` =NEW.road_insurance_by_id,
`for_required_id` =NEW.for_required_id,
`review_date` =NEW.review_date,
`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
`sales_order_validaty` =NEW.sales_order_validaty,
`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
`mode_of_shipment_name` =NEW.mode_of_shipment_name,
`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
`supplier_payment_terms` =NEW.supplier_payment_terms,
`received_payment` =NEW.received_payment,
`received_payment_date` =NEW.received_payment_date,
`received_payment_type` =NEW.received_payment_type,
`terms_condition_purchase` =NEW.terms_condition_purchase,
`isApproved` =NEW.isApproved,
`pdf_url` =NEW.pdf_url,
`taxes_data` =NEW.taxes_data,
`more_items_data` =NEW.more_items_data,
`due_date` =NEW.due_date,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`created_by` =NEW.created_by,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_delete_log` AFTER DELETE ON `sales_order` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_log SET
`id`=OLD.id,
`quatation_id` =OLD.quatation_id,
`quotation_id` =OLD.quotation_id,
`sales_order_no` =OLD.sales_order_no,
`sales_to_party_id` =OLD.sales_to_party_id,
`ship_to_party_id` =OLD.ship_to_party_id,
`branch_id` =OLD.branch_id,
`billing_terms_id` =OLD.billing_terms_id,
`sales_order_date` =OLD.sales_order_date,
`committed_date` =OLD.committed_date,
`currency_id` =OLD.currency_id,
`project_id` =OLD.project_id,
`kind_attn_id` =OLD.kind_attn_id,
`contact_no` =OLD.contact_no,
`cust_po_no` =OLD.cust_po_no,
`email_id` =OLD.email_id,
`po_date` =OLD.po_date,
`sales_order_pref_id` =OLD.sales_order_pref_id,
`conversation_rate` =OLD.conversation_rate,
`sales_id` =OLD.sales_id,
`sales_order_stage_id` =OLD.sales_order_stage_id,
`sales_order_status_id` =OLD.sales_order_status_id,
`order_type` =OLD.order_type,
`lead_provider_id` =OLD.lead_provider_id,
`buyer_detail_id` =OLD.buyer_detail_id,
`note_detail_id` =OLD.note_detail_id,
`login_detail_id` =OLD.login_detail_id,
`transportation_by_id` =OLD.transportation_by_id,
`transport_amount` =OLD.transport_amount,
`foundation_drawing_required_id` =OLD.foundation_drawing_required_id,
`loading_by_id` =OLD.loading_by_id,
`inspection_required_id` =OLD.inspection_required_id,
`unloading_by_id` =OLD.unloading_by_id,
`erection_commissioning_id` =OLD.erection_commissioning_id,
`road_insurance_by_id` =OLD.road_insurance_by_id,
`for_required_id` =OLD.for_required_id,
`review_date` =OLD.review_date,
`mach_deli_min_weeks` =OLD.mach_deli_min_weeks,
`mach_deli_max_weeks` =OLD.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =OLD.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =OLD.mach_deli_max_weeks_date,
`sales_order_validaty` =OLD.sales_order_validaty,
`sales_order_quotation_delivery` =OLD.sales_order_quotation_delivery,
`mode_of_shipment_name` =OLD.mode_of_shipment_name,
`mode_of_shipment_truck_number` =OLD.mode_of_shipment_truck_number,
`supplier_payment_terms` =OLD.supplier_payment_terms,
`received_payment` =OLD.received_payment,
`received_payment_date` =OLD.received_payment_date,
`received_payment_type` =OLD.received_payment_type,
`terms_condition_purchase` =OLD.terms_condition_purchase,
`isApproved` =OLD.isApproved,
`pdf_url` =OLD.pdf_url,
`taxes_data` =OLD.taxes_data,
`more_items_data` =OLD.more_items_data,
`due_date` =OLD.due_date,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`created_by` =OLD.created_by,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_edit_log` AFTER UPDATE ON `sales_order` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_log SET
`id`=NEW.id,
`quatation_id` =NEW.quatation_id,
`quotation_id` =NEW.quotation_id,
`sales_order_no` =NEW.sales_order_no,
`sales_to_party_id` =NEW.sales_to_party_id,
`ship_to_party_id` =NEW.ship_to_party_id,
`branch_id` =NEW.branch_id,
`billing_terms_id` =NEW.billing_terms_id,
`sales_order_date` =NEW.sales_order_date,
`committed_date` =NEW.committed_date,
`currency_id` =NEW.currency_id,
`project_id` =NEW.project_id,
`kind_attn_id` =NEW.kind_attn_id,
`contact_no` =NEW.contact_no,
`cust_po_no` =NEW.cust_po_no,
`email_id` =NEW.email_id,
`po_date` =NEW.po_date,
`sales_order_pref_id` =NEW.sales_order_pref_id,
`conversation_rate` =NEW.conversation_rate,
`sales_id` =NEW.sales_id,
`sales_order_stage_id` =NEW.sales_order_stage_id,
`sales_order_status_id` =NEW.sales_order_status_id,
`order_type` =NEW.order_type,
`lead_provider_id` =NEW.lead_provider_id,
`buyer_detail_id` =NEW.buyer_detail_id,
`note_detail_id` =NEW.note_detail_id,
`login_detail_id` =NEW.login_detail_id,
`transportation_by_id` =NEW.transportation_by_id,
`transport_amount` =NEW.transport_amount,
`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
`loading_by_id` =NEW.loading_by_id,
`inspection_required_id` =NEW.inspection_required_id,
`unloading_by_id` =NEW.unloading_by_id,
`erection_commissioning_id` =NEW.erection_commissioning_id,
`road_insurance_by_id` =NEW.road_insurance_by_id,
`for_required_id` =NEW.for_required_id,
`review_date` =NEW.review_date,
`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
`sales_order_validaty` =NEW.sales_order_validaty,
`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
`mode_of_shipment_name` =NEW.mode_of_shipment_name,
`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
`supplier_payment_terms` =NEW.supplier_payment_terms,
`received_payment` =NEW.received_payment,
`received_payment_date` =NEW.received_payment_date,
`received_payment_type` =NEW.received_payment_type,
`terms_condition_purchase` =NEW.terms_condition_purchase,
`isApproved` =NEW.isApproved,
`pdf_url` =NEW.pdf_url,
`taxes_data` =NEW.taxes_data,
`more_items_data` =NEW.more_items_data,
`due_date` =NEW.due_date,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`created_by` =NEW.created_by,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_billing_terms`
--

CREATE TABLE `sales_order_billing_terms` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_billing_terms`
--
DELIMITER $$
CREATE TRIGGER `sales_order_billing_terms_add_log` AFTER INSERT ON `sales_order_billing_terms` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_billing_terms_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_billing_terms_delete_log` AFTER DELETE ON `sales_order_billing_terms` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_billing_terms_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`cal_code` =OLD.cal_code,
`narration` =OLD.narration,
`cal_definition` =OLD.cal_definition,
`percentage` =OLD.percentage,
`value` =OLD.value,
`gl_acc` =OLD.gl_acc,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_billing_terms_edit_log` AFTER UPDATE ON `sales_order_billing_terms` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_billing_terms_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_buyers`
--

CREATE TABLE `sales_order_buyers` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `party_code` varchar(255) NOT NULL,
  `party_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `tin_vat_no` varchar(255) NOT NULL,
  `tin_cst_no` varchar(255) NOT NULL,
  `ecc_no` varchar(255) NOT NULL,
  `pincode` varchar(20) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_buyers`
--
DELIMITER $$
CREATE TRIGGER `sales_order_buyers_add_log` AFTER INSERT ON `sales_order_buyers` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_buyers_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_id` =NEW.party_id,
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`city_id` =NEW.city_id,
`state_id` =NEW.state_id,
`country_id` =NEW.country_id,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`gst_no` =NEW.gst_no,
`pan_no` =NEW.pan_no,
`tin_vat_no` =NEW.tin_vat_no,
`tin_cst_no` =NEW.tin_cst_no,
`ecc_no` =NEW.ecc_no,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_buyers_delete_log` AFTER DELETE ON `sales_order_buyers` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_buyers_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`party_id` =OLD.party_id,
`party_code` =OLD.party_code,
`party_name` =OLD.party_name,
`address` =OLD.address,
`city_id` =OLD.city_id,
`state_id` =OLD.state_id,
`country_id` =OLD.country_id,
`fax_no` =OLD.fax_no,
`email_id` =OLD.email_id,
`website` =OLD.website,
`gst_no` =OLD.gst_no,
`pan_no` =OLD.pan_no,
`tin_vat_no` =OLD.tin_vat_no,
`tin_cst_no` =OLD.tin_cst_no,
`ecc_no` =OLD.ecc_no,
`pincode` =OLD.pincode,
`phone_no` =OLD.phone_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_buyers_edit_log` AFTER UPDATE ON `sales_order_buyers` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_buyers_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_id` =NEW.party_id,
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`city_id` =NEW.city_id,
`state_id` =NEW.state_id,
`country_id` =NEW.country_id,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`gst_no` =NEW.gst_no,
`pan_no` =NEW.pan_no,
`tin_vat_no` =NEW.tin_vat_no,
`tin_cst_no` =NEW.tin_cst_no,
`ecc_no` =NEW.ecc_no,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_cal_code_definition`
--

CREATE TABLE `sales_order_cal_code_definition` (
  `id` int(11) NOT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_cal_code_definition`
--
DELIMITER $$
CREATE TRIGGER `sales_order_cal_code_definition_add_log` AFTER INSERT ON `sales_order_cal_code_definition` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`sales_order_id` =NEW.sales_order_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_cal_code_definition_delete_log` AFTER DELETE ON `sales_order_cal_code_definition` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_cal_code_definition_log SET
`id`=OLD.id,
`billing_terms_detail_id` =OLD.billing_terms_detail_id,
`sales_order_id` =OLD.sales_order_id,
`is_first_element` =OLD.is_first_element,
`cal_operation` =OLD.cal_operation,
`cal_code_id` =OLD.cal_code_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_cal_code_definition_edit_log` AFTER UPDATE ON `sales_order_cal_code_definition` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`sales_order_id` =NEW.sales_order_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_item`
--

CREATE TABLE `sales_order_item` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `party_name` int(11) DEFAULT NULL,
  `party_type` int(11) DEFAULT NULL,
  `address` text,
  `city` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `fax_no` varchar(255) DEFAULT NULL,
  `pin_code` varchar(50) DEFAULT NULL,
  `phone_no` varchar(25) DEFAULT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_item`
--
DELIMITER $$
CREATE TRIGGER `sales_order_item_add_log` AFTER INSERT ON `sales_order_item` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_item_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_name` =NEW.party_name,
`party_type` =NEW.party_type,
`address` =NEW.address,
`city` =NEW.city,
`state` =NEW.state,
`country` =NEW.country,
`fax_no` =NEW.fax_no,
`pin_code` =NEW.pin_code,
`phone_no` =NEW.phone_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_item_delete_log` AFTER DELETE ON `sales_order_item` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_item_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`party_name` =OLD.party_name,
`party_type` =OLD.party_type,
`address` =OLD.address,
`city` =OLD.city,
`state` =OLD.state,
`country` =OLD.country,
`fax_no` =OLD.fax_no,
`pin_code` =OLD.pin_code,
`phone_no` =OLD.phone_no,
`email_id` =OLD.email_id,
`website` =OLD.website,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_item_edit_log` AFTER UPDATE ON `sales_order_item` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_item_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_name` =NEW.party_name,
`party_type` =NEW.party_type,
`address` =NEW.address,
`city` =NEW.city,
`state` =NEW.state,
`country` =NEW.country,
`fax_no` =NEW.fax_no,
`pin_code` =NEW.pin_code,
`phone_no` =NEW.phone_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_items`
--

CREATE TABLE `sales_order_items` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_desc` text,
  `item_description` text NOT NULL,
  `add_description` varchar(255) NOT NULL,
  `detail_description` text NOT NULL,
  `drawing_number` varchar(255) NOT NULL,
  `drawing_revision` varchar(255) NOT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) NOT NULL,
  `igst` double DEFAULT NULL,
  `igst_amount` double DEFAULT NULL,
  `cgst` double DEFAULT NULL,
  `cgst_amount` double DEFAULT NULL,
  `sgst` double DEFAULT NULL,
  `sgst_amount` double DEFAULT NULL,
  `total_amount` double NOT NULL,
  `item_note` text NOT NULL,
  `quantity` double NOT NULL,
  `disc_per` double NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `rate` double NOT NULL,
  `disc_value` double NOT NULL,
  `amount` double NOT NULL,
  `net_amount` double NOT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) NOT NULL,
  `cust_part_no` varchar(255) NOT NULL,
  `item_extra_accessories_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_items`
--
DELIMITER $$
CREATE TRIGGER `sales_order_items_add_log` AFTER INSERT ON `sales_order_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`sales_order_id` =NEW.sales_order_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_items_delete_log` AFTER DELETE ON `sales_order_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_items_log SET
`id`=OLD.id,
`item_id` =OLD.item_id,
`sales_order_id` =OLD.sales_order_id,
`item_code` =OLD.item_code,
`item_category_id` =OLD.item_category_id,
`item_name` =OLD.item_name,
`item_desc` =OLD.item_desc,
`item_description` =OLD.item_description,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`drawing_revision` =OLD.drawing_revision,
`uom_id`=OLD.uom_id,
`uom` =OLD.uom,
`total_amount` =OLD.total_amount,
`item_note` =OLD.item_note,
`quantity` =OLD.quantity,
`disc_per` =OLD.disc_per,
`rate` =OLD.rate,
`disc_value` =OLD.disc_value,
`amount` =OLD.amount,
`net_amount` =OLD.net_amount,
`item_status_id` =OLD.item_status_id,
`item_status` =OLD.item_status,
`cust_part_no` =OLD.cust_part_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_items_edit_log` AFTER UPDATE ON `sales_order_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`sales_order_id` =NEW.sales_order_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_logins`
--

CREATE TABLE `sales_order_logins` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) NOT NULL,
  `last_modified_by_id` int(11) DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `prepared_by` varchar(255) DEFAULT NULL,
  `prepared_by_id` int(11) DEFAULT NULL,
  `prepared_date_time` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_by_id` int(11) DEFAULT NULL,
  `approved_date_time` datetime DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_logins`
--
DELIMITER $$
CREATE TRIGGER `sales_order_logins_add_log` AFTER INSERT ON `sales_order_logins` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_logins_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_logins_delete_log` AFTER DELETE ON `sales_order_logins` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_logins_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`created_by` =OLD.created_by,
`created_by_id` =OLD.created_by_id,
`created_date_time` =OLD.created_date_time,
`last_modified_by` =OLD.last_modified_by,
`last_modified_by_id` =OLD.last_modified_by_id,
`modified_date_time` =OLD.modified_date_time,
`prepared_by` =OLD.prepared_by,
`prepared_by_id` =OLD.prepared_by_id,
`prepared_date_time` =OLD.prepared_date_time,
`approved_by` =OLD.approved_by,
`approved_by_id` =OLD.approved_by_id,
`approved_date_time` =OLD.approved_date_time,
`stage_id` =OLD.stage_id,
`stage` =OLD.stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_logins_edit_log` AFTER UPDATE ON `sales_order_logins` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_logins_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_notes`
--

CREATE TABLE `sales_order_notes` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `header` text NOT NULL,
  `note` text NOT NULL,
  `note2` text NOT NULL,
  `internal_note` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_notes`
--
DELIMITER $$
CREATE TRIGGER `sales_order_notes_add_log` AFTER INSERT ON `sales_order_notes` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_notes_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`header` =NEW.header,
`note` =NEW.note,
`note2` =NEW.note2,
`internal_note` =NEW.internal_note,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_notes_delete_log` AFTER DELETE ON `sales_order_notes` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_notes_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`header` =OLD.header,
`note` =OLD.note,
`note2` =OLD.note2,
`internal_note` =OLD.internal_note,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_notes_edit_log` AFTER UPDATE ON `sales_order_notes` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_notes_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`header` =NEW.header,
`note` =NEW.note,
`note2` =NEW.note2,
`internal_note` =NEW.internal_note,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_pref`
--

CREATE TABLE `sales_order_pref` (
  `id` int(11) NOT NULL,
  `sales_order_pref` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_pref`
--
DELIMITER $$
CREATE TRIGGER `sales_order_pref_add_log` AFTER INSERT ON `sales_order_pref` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_pref_log SET
`id`=NEW.id,
`sales_order_pref` =NEW.sales_order_pref,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_pref_delete_log` AFTER DELETE ON `sales_order_pref` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_pref_log SET
`id`=OLD.id,
`sales_order_pref` =OLD.sales_order_pref,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_pref_edit_log` AFTER UPDATE ON `sales_order_pref` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_pref_log SET
`id`=NEW.id,
`sales_order_pref` =NEW.sales_order_pref,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_stage`
--

CREATE TABLE `sales_order_stage` (
  `id` int(11) NOT NULL,
  `sales_order_stage` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_stage`
--
DELIMITER $$
CREATE TRIGGER `sales_order_stage_add_log` AFTER INSERT ON `sales_order_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_stage_log SET
`id`=NEW.id,
`sales_order_stage` =NEW.sales_order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_stage_delete_log` AFTER DELETE ON `sales_order_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_stage_log SET
`id`=OLD.id,
`sales_order_stage` =OLD.sales_order_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_stage_edit_log` AFTER UPDATE ON `sales_order_stage` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_stage_log SET
`id`=NEW.id,
`sales_order_stage` =NEW.sales_order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_status`
--

CREATE TABLE `sales_order_status` (
  `id` int(11) NOT NULL,
  `sales_order_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_order_status`
--
DELIMITER $$
CREATE TRIGGER `sales_order_status_add_log` AFTER INSERT ON `sales_order_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_status_log SET
`id`=NEW.id,
`sales_order_status` =NEW.sales_order_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_status_delete_log` AFTER DELETE ON `sales_order_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_status_log SET
`id`=OLD.id,
`sales_order_status` =OLD.sales_order_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_order_status_edit_log` AFTER UPDATE ON `sales_order_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_status_log SET
`id`=NEW.id,
`sales_order_status` =NEW.sales_order_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_status`
--

CREATE TABLE `sales_status` (
  `id` int(11) NOT NULL,
  `sales_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_status`
--
DELIMITER $$
CREATE TRIGGER `sales_status_add_log` AFTER INSERT ON `sales_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_status_log SET
`id`=NEW.id,
`sales_status` =NEW.sales_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_status_delete_log` AFTER DELETE ON `sales_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_status_log SET
`id`=OLD.id,
`sales_status` =OLD.sales_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_status_edit_log` AFTER UPDATE ON `sales_status` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_status_log SET
`id`=NEW.id,
`sales_status` =NEW.sales_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_type`
--

CREATE TABLE `sales_type` (
  `id` int(11) NOT NULL,
  `sales_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sales_type`
--
DELIMITER $$
CREATE TRIGGER `sales_type_add_log` AFTER INSERT ON `sales_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_type_log SET
`id`=NEW.id,
`sales_type` =NEW.sales_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_type_delete_log` AFTER DELETE ON `sales_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_type_log SET
`id`=OLD.id,
`sales_type` =OLD.sales_type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sales_type_edit_log` AFTER UPDATE ON `sales_type` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_type_log SET
`id`=NEW.id,
`sales_type` =NEW.sales_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `service_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `service_date` date DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `paid_free` varchar(4) NOT NULL DEFAULT 'free',
  `charge` double DEFAULT NULL,
  `service_provide_by` int(11) NOT NULL,
  `service_received_by` int(11) NOT NULL,
  `service_type` varchar(111) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `service`
--
DELIMITER $$
CREATE TRIGGER `service_add_log` AFTER INSERT ON `service` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_log SET
`service_id` =NEW.service_id,
`party_id` =NEW.party_id,
`service_date` =NEW.service_date,
`note` =NEW.note,
`paid_free` =NEW.paid_free,
`charge` =NEW.charge,
`service_provide_by` =NEW.service_provide_by,
`service_received_by` =NEW.service_received_by,
`service_type` =NEW.service_type,
`created_by`=NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `service_delete_log` AFTER DELETE ON `service` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_log SET
`service_id` =OLD.service_id,
`party_id` =OLD.party_id,
`service_date` =OLD.service_date,
`note` =OLD.note,
`paid_free` =OLD.paid_free,
`charge` =OLD.charge,
`service_provide_by` =OLD.service_provide_by,
`service_received_by` =OLD.service_received_by,
`service_type` =OLD.service_type,
`created_by`=OLD.created_by,
`updated_by` =OLD.updated_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `service_edit_log` AFTER UPDATE ON `service` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_log SET
`service_id` =NEW.service_id,
`party_id` =NEW.party_id,
`service_date` =NEW.service_date,
`note` =NEW.note,
`paid_free` =NEW.paid_free,
`charge` =NEW.charge,
`service_provide_by` =NEW.service_provide_by,
`service_received_by` =NEW.service_received_by,
`service_type` =NEW.service_type,
`created_by`=NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `service_followup_history`
--

CREATE TABLE `service_followup_history` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `followup_date` date DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `service_followup_history`
--
DELIMITER $$
CREATE TRIGGER `service_followup_history_add_log` AFTER INSERT ON `service_followup_history` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_followup_history_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`followup_date` =NEW.followup_date,
`history` =NEW.history,
`followup_by` =NEW.followup_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `service_followup_history_delete_log` AFTER DELETE ON `service_followup_history` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_followup_history_log SET
`id`=OLD.id,
`service_id` =OLD.service_id,
`followup_date` =OLD.followup_date,
`history` =OLD.history,
`followup_by` =OLD.followup_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `service_followup_history_edit_log` AFTER UPDATE ON `service_followup_history` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_followup_history_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`followup_date` =NEW.followup_date,
`history` =NEW.history,
`followup_by` =NEW.followup_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `service_items`
--

CREATE TABLE `service_items` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `challan_id` int(11) NOT NULL,
  `challan_item_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `service_items`
--
DELIMITER $$
CREATE TRIGGER `service_items_add_log` AFTER INSERT ON `service_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_items_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`challan_id` =NEW.challan_id,
`challan_item_id` =NEW.challan_item_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `service_items_delete_log` AFTER DELETE ON `service_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_items_log SET
`id`=OLD.id,
`service_id` =OLD.service_id,
`challan_id` =OLD.challan_id,
`challan_item_id` =OLD.challan_item_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `service_items_edit_log` AFTER UPDATE ON `service_items` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_items_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`challan_id` =NEW.challan_id,
`challan_item_id` =NEW.challan_item_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `interview_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) NOT NULL,
  `married_status` varchar(15) NOT NULL,
  `husband_wife_name` varchar(255) NOT NULL,
  `marriage_date` date NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `date_of_joining` date NOT NULL,
  `date_of_leaving` date NOT NULL,
  `salary` double NOT NULL,
  `allow_pf` varchar(10) DEFAULT NULL,
  `basic_pay` double DEFAULT NULL,
  `house_rent` double DEFAULT NULL,
  `traveling_allowance` double DEFAULT NULL,
  `education_allowance` double DEFAULT NULL,
  `pf_amount` double DEFAULT NULL,
  `bonus_amount` double DEFAULT NULL,
  `other_allotment` double DEFAULT NULL,
  `medical_reimbursement` double DEFAULT NULL,
  `professional_tax` double DEFAULT NULL,
  `monthly_gross` double DEFAULT NULL,
  `cost_to_company` double DEFAULT NULL,
  `pf_employee` double DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_no` varchar(255) DEFAULT NULL,
  `esic_no` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `uan_id` varchar(255) DEFAULT NULL,
  `pf_no` varchar(255) DEFAULT NULL,
  `emp_no` varchar(255) DEFAULT NULL,
  `emp_id` varchar(255) NOT NULL,
  `esic_id` varchar(255) DEFAULT NULL,
  `visitor_last_message_id` int(11) NOT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `mailbox_email` varchar(255) DEFAULT NULL,
  `mailbox_password` varchar(255) DEFAULT NULL,
  `pdf_url` text,
  `active` tinyint(1) DEFAULT '1',
  `is_login` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `staff`
--
DELIMITER $$
CREATE TRIGGER `staff_add_log` AFTER INSERT ON `staff` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_log SET
`staff_id` =NEW.staff_id,
`interview_id` =NEW.interview_id,
`name` =NEW.name,
`birth_date` =NEW.birth_date,
`gender` =NEW.gender,
`blood_group` =NEW.blood_group,
`father_name` =NEW.father_name,
`mother_name` =NEW.mother_name,
`married_status` =NEW.married_status,
`husband_wife_name` =NEW.husband_wife_name,
`marriage_date` =NEW.marriage_date,
`contact_no` =NEW.contact_no,
`email` =NEW.email,
`pass`  =NEW.pass,
`image` =NEW.image,
`address` =NEW.address,
`permanent_address` =NEW.permanent_address,
`qualification` =NEW.qualification,
`designation_id` =NEW.designation_id,
`department_id` =NEW.department_id,
`grade_id` =NEW.grade_id,
`date_of_joining` =NEW.date_of_joining,
`date_of_leaving` =NEW.date_of_leaving,
`salary` =NEW.salary,
`allow_pf` =NEW.allow_pf,
`basic_pay` =NEW.basic_pay,
`house_rent` =NEW.house_rent,
`traveling_allowance` =NEW.traveling_allowance,
`education_allowance` =NEW.education_allowance,
`pf_amount` =NEW.pf_amount,
`bonus_amount`  =NEW.bonus_amount,
`other_allotment`  =NEW.other_allotment,
`medical_reimbursement`  =NEW.medical_reimbursement,
`professional_tax`  =NEW.professional_tax,
`monthly_gross`  =NEW.monthly_gross,
`cost_to_company`  =NEW.cost_to_company,
`pf_employee`  =NEW.pf_employee,
`bank_name`  =NEW.bank_name,
`bank_acc_no`  =NEW.bank_acc_no,
`esic_no`  =NEW.esic_no,
`pan_no`  =NEW.pan_no,
`uan_id`  =NEW.uan_id,
`pf_no`  =NEW.pf_no,
`emp_no`  =NEW.emp_no,
`emp_id`  =NEW.emp_id,
`esic_id`  =NEW.esic_id,
`visitor_last_message_id`  =NEW.visitor_last_message_id,
`user_type`  =NEW.user_type,
`mailbox_email`  =NEW.mailbox_email,
`mailbox_password`  =NEW.mailbox_password,
`pdf_url` =NEW.pdf_url,
`active` =NEW.active,
`is_login`  =NEW.is_login,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `staff_edit_log` AFTER UPDATE ON `staff` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_log SET
`staff_id` =NEW.staff_id,
`interview_id` =NEW.interview_id,
`name` =NEW.name,
`birth_date` =NEW.birth_date,
`gender` =NEW.gender,
`blood_group` =NEW.blood_group,
`father_name` =NEW.father_name,
`mother_name` =NEW.mother_name,
`married_status` =NEW.married_status,
`husband_wife_name` =NEW.husband_wife_name,
`marriage_date` =NEW.marriage_date,
`contact_no` =NEW.contact_no,
`email` =NEW.email,
`pass`  =NEW.pass,
`image` =NEW.image,
`address` =NEW.address,
`permanent_address` =NEW.permanent_address,
`qualification` =NEW.qualification,
`designation_id` =NEW.designation_id,
`department_id` =NEW.department_id,
`grade_id` =NEW.grade_id,
`date_of_joining` =NEW.date_of_joining,
`date_of_leaving` =NEW.date_of_leaving,
`salary` =NEW.salary,
`allow_pf` =NEW.allow_pf,
`basic_pay` =NEW.basic_pay,
`house_rent` =NEW.house_rent,
`traveling_allowance` =NEW.traveling_allowance,
`education_allowance` =NEW.education_allowance,
`pf_amount` =NEW.pf_amount,
`bonus_amount`  =NEW.bonus_amount,
`other_allotment`  =NEW.other_allotment,
`medical_reimbursement`  =NEW.medical_reimbursement,
`professional_tax`  =NEW.professional_tax,
`monthly_gross`  =NEW.monthly_gross,
`cost_to_company`  =NEW.cost_to_company,
`pf_employee`  =NEW.pf_employee,
`bank_name`  =NEW.bank_name,
`bank_acc_no`  =NEW.bank_acc_no,
`esic_no`  =NEW.esic_no,
`pan_no`  =NEW.pan_no,
`uan_id`  =NEW.uan_id,
`pf_no`  =NEW.pf_no,
`emp_no`  =NEW.emp_no,
`emp_id`  =NEW.emp_id,
`esic_id`  =NEW.esic_id,
`visitor_last_message_id`  =NEW.visitor_last_message_id,
`user_type`  =NEW.user_type,
`mailbox_email`  =NEW.mailbox_email,
`mailbox_password`  =NEW.mailbox_password,
`pdf_url` =NEW.pdf_url,
`active` =NEW.active,
`is_login`  =NEW.is_login,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles`
--

CREATE TABLE `staff_roles` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `staff_roles`
--
DELIMITER $$
CREATE TRIGGER `staff_roles_add_log` AFTER INSERT ON `staff_roles` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_roles_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`module_id` =NEW.module_id,
`role_id`  =NEW.role_id,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `staff_roles_delete_log` AFTER DELETE ON `staff_roles` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_roles_log SET
`id`=OLD.id,
`staff_id` =OLD.staff_id,
`module_id` =OLD.module_id,
`role_id`  =OLD.role_id,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `staff_roles_edit_log` AFTER UPDATE ON `staff_roles` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_roles_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`module_id` =NEW.module_id,
`role_id`  =NEW.role_id,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `state`
--
DELIMITER $$
CREATE TRIGGER `state_add_log` AFTER INSERT ON `state` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.state_log SET
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`state`  =NEW.state,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `state_delete_log` AFTER DELETE ON `state` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.state_log SET
`state_id`  =OLD.state_id,
`country_id`  =OLD.country_id,
`state`  =OLD.state,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `state_edit_log` AFTER UPDATE ON `state` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.state_log SET
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`state`  =NEW.state,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sub_group`
--

CREATE TABLE `sub_group` (
  `id` int(11) NOT NULL,
  `sub_item_group` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sub_group`
--
DELIMITER $$
CREATE TRIGGER `sub_group_add_log` AFTER INSERT ON `sub_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sub_group_log SET
`id`=NEW.id,
`sub_item_group`  =NEW.sub_item_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sub_group_delete_log` AFTER DELETE ON `sub_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sub_group_log SET
`id`=OLD.id,
`sub_item_group`  =OLD.sub_item_group,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sub_group_edit_log` AFTER UPDATE ON `sub_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sub_group_log SET
`id`=NEW.id,
`sub_item_group`  =NEW.sub_item_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `pincode` varchar(22) DEFAULT NULL,
  `phone_no` text NOT NULL,
  `email_id` text NOT NULL,
  `gstin` varchar(16) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `supplier`
--
DELIMITER $$
CREATE TRIGGER `supplier_add_log` AFTER INSERT ON `supplier` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =NEW.supplier_id,
`supplier_name`  =NEW.supplier_name,
`address` =NEW.address,
`city_id`  =NEW.city_id,
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`phone_no`  =NEW.phone_no,
`email_id`  =NEW.email_id,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `supplier_delete_log` AFTER DELETE ON `supplier` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =OLD.supplier_id,
`supplier_name`  =OLD.supplier_name,
`address` =OLD.address,
`city_id`  =OLD.city_id,
`state_id`  =OLD.state_id,
`country_id`  =OLD.country_id,
`phone_no`  =OLD.phone_no,
`email_id`  =OLD.email_id,
`created_by`=OLD.created_by,
`updated_by`=OLD.updated_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `supplier_edit_log` AFTER UPDATE ON `supplier` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =NEW.supplier_id,
`supplier_name`  =NEW.supplier_name,
`address` =NEW.address,
`city_id`  =NEW.city_id,
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`phone_no`  =NEW.phone_no,
`email_id`  =NEW.email_id,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `terms_and_conditions`
--

CREATE TABLE `terms_and_conditions` (
  `id` int(11) NOT NULL,
  `module` varchar(222) DEFAULT NULL,
  `detail` text,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terms_and_conditions_sub`
--

CREATE TABLE `terms_and_conditions_sub` (
  `id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `detail` text,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_detail`
--

CREATE TABLE `terms_condition_detail` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `group_name1` varchar(255) NOT NULL,
  `group_name2` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `terms_condition_detail`
--
DELIMITER $$
CREATE TRIGGER `terms_condition_detail_add_log` AFTER INSERT ON `terms_condition_detail` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_detail_log SET
`id`=NEW.id,
`code` =NEW.code,
`group_name1`  =NEW.group_name1,
`group_name2`  =NEW.group_name2,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_condition_detail_delete_log` AFTER DELETE ON `terms_condition_detail` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_detail_log SET
`id`=OLD.id,
`code` =OLD.code,
`group_name1`  =OLD.group_name1,
`group_name2`  =OLD.group_name2,
`description` =OLD.description,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_condition_detail_edit_log` AFTER UPDATE ON `terms_condition_detail` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_detail_log SET
`id`=NEW.id,
`code` =NEW.code,
`group_name1`  =NEW.group_name1,
`group_name2`  =NEW.group_name2,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_group`
--

CREATE TABLE `terms_condition_group` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `terms_condition_group`
--
DELIMITER $$
CREATE TRIGGER `terms_condition_group_add_log` AFTER INSERT ON `terms_condition_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_group_log SET
`id`=NEW.id,
`code` =NEW.code,
`description`  =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_condition_group_delete_log` AFTER DELETE ON `terms_condition_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_group_log SET
`id`=OLD.id,
`code` =OLD.code,
`description`  =OLD.description,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_condition_group_edit_log` AFTER UPDATE ON `terms_condition_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_group_log SET
`id`=NEW.id,
`code` =NEW.code,
`description`  =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_template`
--

CREATE TABLE `terms_condition_template` (
  `id` int(11) NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `terms_condition` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `terms_condition_template`
--
DELIMITER $$
CREATE TRIGGER `terms_condition_template_add_log` AFTER INSERT ON `terms_condition_template` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_template_log SET
`id`=NEW.id,
`template_name`  =NEW.template_name,
`group_name`  =NEW.group_name,
`terms_condition`  =NEW.terms_condition,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_condition_template_delete_log` AFTER DELETE ON `terms_condition_template` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_template_log SET
`id`=OLD.id,
`template_name`  =OLD.template_name,
`group_name`  =OLD.group_name,
`terms_condition`  =OLD.terms_condition,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_condition_template_edit_log` AFTER UPDATE ON `terms_condition_template` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_template_log SET
`id`=NEW.id,
`template_name`  =NEW.template_name,
`group_name`  =NEW.group_name,
`terms_condition`  =NEW.terms_condition,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `terms_group`
--

CREATE TABLE `terms_group` (
  `id` int(11) NOT NULL,
  `terms_group` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `terms_group`
--
DELIMITER $$
CREATE TRIGGER `terms_group_add_log` AFTER INSERT ON `terms_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_group_log SET
`id`=NEW.id,
`terms_group`  =NEW.terms_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_group_delete_log` AFTER DELETE ON `terms_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_group_log SET
`id`=OLD.id,
`terms_group`  =OLD.terms_group,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `terms_group_edit_log` AFTER UPDATE ON `terms_group` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_group_log SET
`id`=NEW.id,
`terms_group`  =NEW.terms_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `transportation_by`
--

CREATE TABLE `transportation_by` (
  `id` int(11) NOT NULL,
  `transportation_by` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `transportation_by`
--
DELIMITER $$
CREATE TRIGGER `transportation_by_add_log` AFTER INSERT ON `transportation_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.transportation_by_log SET
`id`=NEW.id,
`transportation_by` =NEW.transportation_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `transportation_by_delete_log` AFTER DELETE ON `transportation_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.transportation_by_log SET
`id`=OLD.id,
`transportation_by` =OLD.transportation_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `transportation_by_edit_log` AFTER UPDATE ON `transportation_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.transportation_by_log SET
`id`=NEW.id,
`transportation_by` =NEW.transportation_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `unloading_by`
--

CREATE TABLE `unloading_by` (
  `id` int(11) NOT NULL,
  `unloading_by` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `unloading_by`
--
DELIMITER $$
CREATE TRIGGER `unloading_by_add_log` AFTER INSERT ON `unloading_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.unloading_by_log SET
`id`=NEW.id,
`unloading_by` =NEW.unloading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `unloading_by_delete_log` AFTER DELETE ON `unloading_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.unloading_by_log SET
`id`=OLD.id,
`unloading_by` =OLD.unloading_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `unloading_by_edit_log` AFTER UPDATE ON `unloading_by` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.unloading_by_log SET
`id`=NEW.id,
`unloading_by` =NEW.unloading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `uom`
--

CREATE TABLE `uom` (
  `id` int(11) NOT NULL,
  `uom` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `uom`
--
DELIMITER $$
CREATE TRIGGER `uom_add_log` AFTER INSERT ON `uom` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.uom_log SET
`id`=NEW.id,
`uom` =NEW.uom,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `uom_delete_log` AFTER DELETE ON `uom` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.uom_log SET
`id`=OLD.id,
`uom` =OLD.uom,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `uom_edit_log` AFTER UPDATE ON `uom` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.uom_log SET
`id`=NEW.id,
`uom` =NEW.uom,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `from_address` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `others` varchar(255) DEFAULT NULL,
  `session_id` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_disconnect_mail_sent` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `visitors`
--
DELIMITER $$
CREATE TRIGGER `visitors_add_log` AFTER INSERT ON `visitors` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.visitors_log SET
`id`=NEW.id,
`name` =NEW.name,
`phone`  =NEW.phone,
`email` =NEW.email,
`ip`  =NEW.ip,
`from_address` =NEW.from_address,
`city`  =NEW.city,
`country`  =NEW.country,
`currency`  =NEW.currency,
`others`  =NEW.others,
`session_id`  =NEW.session_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`is_disconnect_mail_sent` =NEW.is_disconnect_mail_sent,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `visitors_delete_log` AFTER DELETE ON `visitors` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.visitors_log SET
`id`=OLD.id,
`name` =OLD.name,
`phone`  =OLD.phone,
`email` =OLD.email,
`ip`  =OLD.ip,
`from_address` =OLD.from_address,
`city`  =OLD.city,
`country`  =OLD.country,
`currency`  =OLD.currency,
`others`  =OLD.others,
`session_id`  =OLD.session_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`is_disconnect_mail_sent` =OLD.is_disconnect_mail_sent,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `visitors_edit_log` AFTER UPDATE ON `visitors` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.visitors_log SET
`id`=NEW.id,
`name` =NEW.name,
`phone`  =NEW.phone,
`email` =NEW.email,
`ip`  =NEW.ip,
`from_address` =NEW.from_address,
`city`  =NEW.city,
`country`  =NEW.country,
`currency`  =NEW.currency,
`others`  =NEW.others,
`session_id`  =NEW.session_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`is_disconnect_mail_sent` =NEW.is_disconnect_mail_sent,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `website_modules`
--

CREATE TABLE `website_modules` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `main_module` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `website_modules`
--
DELIMITER $$
CREATE TRIGGER `website_modules_add_log` AFTER INSERT ON `website_modules` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.website_modules_log SET
`id`=NEW.id,
`title` =NEW.title,
`table_name`  =NEW.table_name,
`main_module`  =NEW.main_module,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `website_modules_delete_log` AFTER DELETE ON `website_modules` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.website_modules_log SET
`id`=OLD.id,
`title` =OLD.title,
`table_name`  =OLD.table_name,
`main_module`  =OLD.main_module,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `website_modules_edit_log` AFTER UPDATE ON `website_modules` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.website_modules_log SET
`id`=NEW.id,
`title` =NEW.title,
`table_name`  =NEW.table_name,
`main_module`  =NEW.main_module,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `weekly_holiday`
--

CREATE TABLE `weekly_holiday` (
  `id` int(11) NOT NULL,
  `day` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `weekly_holiday`
--
DELIMITER $$
CREATE TRIGGER `weekly_holiday_add_log` AFTER INSERT ON `weekly_holiday` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.weekly_holiday_log SET
`id`=NEW.id,
`day`  =NEW.day,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `weekly_holiday_delete_log` AFTER DELETE ON `weekly_holiday` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.weekly_holiday_log SET
`id`=OLD.id,
`day`  =OLD.day,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `weekly_holiday_edit_log` AFTER UPDATE ON `weekly_holiday` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.weekly_holiday_log SET
`id`=NEW.id,
`day`  =NEW.day,
`operation_type` = 'edit'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `yearly_leaves`
--

CREATE TABLE `yearly_leaves` (
  `leave_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `yearly_leaves`
--
DELIMITER $$
CREATE TRIGGER `yearly_leaves_add_log` AFTER INSERT ON `yearly_leaves` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.yearly_leaves_log SET
`leave_id`=NEW.leave_id,
`user_id`  =NEW.user_id,
`date` =NEW.date,
`operation_type` = 'add'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `yearly_leaves_delete_log` AFTER DELETE ON `yearly_leaves` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.yearly_leaves_log SET
`leave_id`=OLD.leave_id,
`user_id`  =OLD.user_id,
`date` =OLD.date,
`operation_type` = 'delete'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `yearly_leaves_edit_log` AFTER UPDATE ON `yearly_leaves` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.yearly_leaves_log SET
`leave_id`=NEW.leave_id,
`user_id`  =NEW.user_id,
`date` =NEW.date,
`operation_type` = 'edit'
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_template`
--
ALTER TABLE `billing_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_terms`
--
ALTER TABLE `billing_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_terms_detail`
--
ALTER TABLE `billing_terms_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `cal_code_definition`
--
ALTER TABLE `cal_code_definition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challans`
--
ALTER TABLE `challans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_items`
--
ALTER TABLE `challan_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_items_gearbox_details`
--
ALTER TABLE `challan_items_gearbox_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_items_motor_details`
--
ALTER TABLE `challan_items_motor_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_frequency`
--
ALTER TABLE `challan_item_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_gear_type`
--
ALTER TABLE `challan_item_gear_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_hp`
--
ALTER TABLE `challan_item_hp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_kw`
--
ALTER TABLE `challan_item_kw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_make`
--
ALTER TABLE `challan_item_make`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_model`
--
ALTER TABLE `challan_item_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_ratio`
--
ALTER TABLE `challan_item_ratio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_rpm`
--
ALTER TABLE `challan_item_rpm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_user`
--
ALTER TABLE `challan_item_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_item_volts_cycles`
--
ALTER TABLE `challan_item_volts_cycles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_roles`
--
ALTER TABLE `chat_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_person`
--
ALTER TABLE `contact_person`
  ADD PRIMARY KEY (`contact_person_id`);

--
-- Indexes for table `contact_person_log`
--
ALTER TABLE `contact_person_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_reminder`
--
ALTER TABLE `customer_reminder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_reminder_sent`
--
ALTER TABLE `customer_reminder_sent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_work_entry`
--
ALTER TABLE `daily_work_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`day_id`);

--
-- Indexes for table `default_terms_condition`
--
ALTER TABLE `default_terms_condition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_through`
--
ALTER TABLE `delivery_through`
  ADD PRIMARY KEY (`delivery_through_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`designation_id`);

--
-- Indexes for table `dispatch_followup_history`
--
ALTER TABLE `dispatch_followup_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drawing_number`
--
ALTER TABLE `drawing_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_leaves`
--
ALTER TABLE `employee_leaves`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `erection_commissioning`
--
ALTER TABLE `erection_commissioning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excise`
--
ALTER TABLE `excise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followup_category`
--
ALTER TABLE `followup_category`
  ADD PRIMARY KEY (`fc_id`);

--
-- Indexes for table `followup_history`
--
ALTER TABLE `followup_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followup_order_history`
--
ALTER TABLE `followup_order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `for_required`
--
ALTER TABLE `for_required`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foundation_drawing_required`
--
ALTER TABLE `foundation_drawing_required`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `google_sheets`
--
ALTER TABLE `google_sheets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `google_sheet_staff`
--
ALTER TABLE `google_sheet_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`grade_id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `industry_type`
--
ALTER TABLE `industry_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiry`
--
ALTER TABLE `inquiry`
  ADD PRIMARY KEY (`inquiry_id`);

--
-- Indexes for table `inquiry_confirmation`
--
ALTER TABLE `inquiry_confirmation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiry_followup_history`
--
ALTER TABLE `inquiry_followup_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiry_items`
--
ALTER TABLE `inquiry_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiry_stage`
--
ALTER TABLE `inquiry_stage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiry_status`
--
ALTER TABLE `inquiry_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_required`
--
ALTER TABLE `inspection_required`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interview`
--
ALTER TABLE `interview`
  ADD PRIMARY KEY (`interview_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_billing_terms`
--
ALTER TABLE `invoice_billing_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_cal_code_definition`
--
ALTER TABLE `invoice_cal_code_definition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_logins`
--
ALTER TABLE `invoice_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_type`
--
ALTER TABLE `invoice_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_bom`
--
ALTER TABLE `item_bom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_category`
--
ALTER TABLE `item_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_category_log`
--
ALTER TABLE `item_category_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_class`
--
ALTER TABLE `item_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_details`
--
ALTER TABLE `item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_documents`
--
ALTER TABLE `item_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_extra_accessories`
--
ALTER TABLE `item_extra_accessories`
  ADD PRIMARY KEY (`item_extra_accessories_id`);

--
-- Indexes for table `item_files`
--
ALTER TABLE `item_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_group_code`
--
ALTER TABLE `item_group_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_make`
--
ALTER TABLE `item_make`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_status`
--
ALTER TABLE `item_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_type`
--
ALTER TABLE `item_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_contact_person`
--
ALTER TABLE `lead_contact_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_details`
--
ALTER TABLE `lead_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_provider`
--
ALTER TABLE `lead_provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_source`
--
ALTER TABLE `lead_source`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_stage`
--
ALTER TABLE `lead_stage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_status`
--
ALTER TABLE `lead_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `leave_for_employee`
--
ALTER TABLE `leave_for_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `letters`
--
ALTER TABLE `letters`
  ADD PRIMARY KEY (`letter_id`);

--
-- Indexes for table `loading_by`
--
ALTER TABLE `loading_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mailbox_changes_logs`
--
ALTER TABLE `mailbox_changes_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mailbox_folders`
--
ALTER TABLE `mailbox_folders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mailbox_mails`
--
ALTER TABLE `mailbox_mails`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `mailbox_settings`
--
ALTER TABLE `mailbox_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_system`
--
ALTER TABLE `mail_system`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `mail_system_folder`
--
ALTER TABLE `mail_system_folder`
  ADD PRIMARY KEY (`folder_id`);

--
-- Indexes for table `main_group`
--
ALTER TABLE `main_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_process_type`
--
ALTER TABLE `material_process_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_specification`
--
ALTER TABLE `material_specification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_roles`
--
ALTER TABLE `module_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_stage`
--
ALTER TABLE `order_stage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outbox`
--
ALTER TABLE `outbox`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `party`
--
ALTER TABLE `party`
  ADD PRIMARY KEY (`party_id`);

--
-- Indexes for table `party_detail`
--
ALTER TABLE `party_detail`
  ADD PRIMARY KEY (`party_detail_id`);

--
-- Indexes for table `party_log`
--
ALTER TABLE `party_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `party_type_1`
--
ALTER TABLE `party_type_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `party_type_2`
--
ALTER TABLE `party_type_2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_group`
--
ALTER TABLE `product_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proforma_invoices`
--
ALTER TABLE `proforma_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proforma_invoice_billing_terms`
--
ALTER TABLE `proforma_invoice_billing_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proforma_invoice_cal_code_definition`
--
ALTER TABLE `proforma_invoice_cal_code_definition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proforma_invoice_items`
--
ALTER TABLE `proforma_invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proforma_invoice_logins`
--
ALTER TABLE `proforma_invoice_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_invoice`
--
ALTER TABLE `purchase_invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `purchase_invoice_items`
--
ALTER TABLE `purchase_invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_items`
--
ALTER TABLE `purchase_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotations`
--
ALTER TABLE `quotations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_items`
--
ALTER TABLE `quotation_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_reason`
--
ALTER TABLE `quotation_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_reminder_roles`
--
ALTER TABLE `quotation_reminder_roles`
  ADD PRIMARY KEY (`qr_id`);

--
-- Indexes for table `quotation_stage`
--
ALTER TABLE `quotation_stage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_status`
--
ALTER TABLE `quotation_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_type`
--
ALTER TABLE `quotation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `real_leave`
--
ALTER TABLE `real_leave`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `reference`
--
ALTER TABLE `reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reminder`
--
ALTER TABLE `reminder`
  ADD PRIMARY KEY (`reminder_id`);

--
-- Indexes for table `road_insurance_by`
--
ALTER TABLE `road_insurance_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_action`
--
ALTER TABLE `sales_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_billing_terms`
--
ALTER TABLE `sales_order_billing_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_buyers`
--
ALTER TABLE `sales_order_buyers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_cal_code_definition`
--
ALTER TABLE `sales_order_cal_code_definition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_items`
--
ALTER TABLE `sales_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_logins`
--
ALTER TABLE `sales_order_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_notes`
--
ALTER TABLE `sales_order_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_pref`
--
ALTER TABLE `sales_order_pref`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_status`
--
ALTER TABLE `sales_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_type`
--
ALTER TABLE `sales_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `service_followup_history`
--
ALTER TABLE `service_followup_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_items`
--
ALTER TABLE `service_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `sub_group`
--
ALTER TABLE `sub_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_and_conditions_sub`
--
ALTER TABLE `terms_and_conditions_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_condition_detail`
--
ALTER TABLE `terms_condition_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_condition_group`
--
ALTER TABLE `terms_condition_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_condition_template`
--
ALTER TABLE `terms_condition_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_group`
--
ALTER TABLE `terms_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transportation_by`
--
ALTER TABLE `transportation_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unloading_by`
--
ALTER TABLE `unloading_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uom`
--
ALTER TABLE `uom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `website_modules`
--
ALTER TABLE `website_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weekly_holiday`
--
ALTER TABLE `weekly_holiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yearly_leaves`
--
ALTER TABLE `yearly_leaves`
  ADD PRIMARY KEY (`leave_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agent`
--
ALTER TABLE `agent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `billing_template`
--
ALTER TABLE `billing_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing_terms`
--
ALTER TABLE `billing_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `billing_terms_detail`
--
ALTER TABLE `billing_terms_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cal_code_definition`
--
ALTER TABLE `cal_code_definition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `challans`
--
ALTER TABLE `challans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `challan_items`
--
ALTER TABLE `challan_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `challan_items_gearbox_details`
--
ALTER TABLE `challan_items_gearbox_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT for table `challan_items_motor_details`
--
ALTER TABLE `challan_items_motor_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=333;

--
-- AUTO_INCREMENT for table `challan_item_frequency`
--
ALTER TABLE `challan_item_frequency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `challan_item_gear_type`
--
ALTER TABLE `challan_item_gear_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `challan_item_hp`
--
ALTER TABLE `challan_item_hp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `challan_item_kw`
--
ALTER TABLE `challan_item_kw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `challan_item_make`
--
ALTER TABLE `challan_item_make`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `challan_item_model`
--
ALTER TABLE `challan_item_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `challan_item_ratio`
--
ALTER TABLE `challan_item_ratio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `challan_item_rpm`
--
ALTER TABLE `challan_item_rpm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `challan_item_user`
--
ALTER TABLE `challan_item_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `challan_item_volts_cycles`
--
ALTER TABLE `challan_item_volts_cycles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `chat_roles`
--
ALTER TABLE `chat_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=549;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1519;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_person`
--
ALTER TABLE `contact_person`
  MODIFY `contact_person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16222;

--
-- AUTO_INCREMENT for table `contact_person_log`
--
ALTER TABLE `contact_person_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer_reminder`
--
ALTER TABLE `customer_reminder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customer_reminder_sent`
--
ALTER TABLE `customer_reminder_sent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `daily_work_entry`
--
ALTER TABLE `daily_work_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `day_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `default_terms_condition`
--
ALTER TABLE `default_terms_condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `delivery_through`
--
ALTER TABLE `delivery_through`
  MODIFY `delivery_through_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `designation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `dispatch_followup_history`
--
ALTER TABLE `dispatch_followup_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drawing_number`
--
ALTER TABLE `drawing_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_leaves`
--
ALTER TABLE `employee_leaves`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `erection_commissioning`
--
ALTER TABLE `erection_commissioning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `excise`
--
ALTER TABLE `excise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followup_category`
--
ALTER TABLE `followup_category`
  MODIFY `fc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `followup_history`
--
ALTER TABLE `followup_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=508971;

--
-- AUTO_INCREMENT for table `followup_order_history`
--
ALTER TABLE `followup_order_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `for_required`
--
ALTER TABLE `for_required`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `foundation_drawing_required`
--
ALTER TABLE `foundation_drawing_required`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `google_sheets`
--
ALTER TABLE `google_sheets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `google_sheet_staff`
--
ALTER TABLE `google_sheet_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `grade_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `industry_type`
--
ALTER TABLE `industry_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `inquiry`
--
ALTER TABLE `inquiry`
  MODIFY `inquiry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24149;

--
-- AUTO_INCREMENT for table `inquiry_confirmation`
--
ALTER TABLE `inquiry_confirmation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inquiry_followup_history`
--
ALTER TABLE `inquiry_followup_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `inquiry_items`
--
ALTER TABLE `inquiry_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18402;

--
-- AUTO_INCREMENT for table `inquiry_stage`
--
ALTER TABLE `inquiry_stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inquiry_status`
--
ALTER TABLE `inquiry_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `inspection_required`
--
ALTER TABLE `inspection_required`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `interview`
--
ALTER TABLE `interview`
  MODIFY `interview_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_billing_terms`
--
ALTER TABLE `invoice_billing_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_cal_code_definition`
--
ALTER TABLE `invoice_cal_code_definition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_logins`
--
ALTER TABLE `invoice_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_type`
--
ALTER TABLE `invoice_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `item_bom`
--
ALTER TABLE `item_bom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `item_category`
--
ALTER TABLE `item_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `item_category_log`
--
ALTER TABLE `item_category_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item_class`
--
ALTER TABLE `item_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `item_details`
--
ALTER TABLE `item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item_documents`
--
ALTER TABLE `item_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18122;

--
-- AUTO_INCREMENT for table `item_extra_accessories`
--
ALTER TABLE `item_extra_accessories`
  MODIFY `item_extra_accessories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `item_files`
--
ALTER TABLE `item_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `item_group_code`
--
ALTER TABLE `item_group_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `item_make`
--
ALTER TABLE `item_make`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `item_status`
--
ALTER TABLE `item_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `item_type`
--
ALTER TABLE `item_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `lead_contact_person`
--
ALTER TABLE `lead_contact_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_details`
--
ALTER TABLE `lead_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_provider`
--
ALTER TABLE `lead_provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_source`
--
ALTER TABLE `lead_source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lead_stage`
--
ALTER TABLE `lead_stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lead_status`
--
ALTER TABLE `lead_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_for_employee`
--
ALTER TABLE `leave_for_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `letters`
--
ALTER TABLE `letters`
  MODIFY `letter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `loading_by`
--
ALTER TABLE `loading_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mailbox_changes_logs`
--
ALTER TABLE `mailbox_changes_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailbox_folders`
--
ALTER TABLE `mailbox_folders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailbox_mails`
--
ALTER TABLE `mailbox_mails`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailbox_settings`
--
ALTER TABLE `mailbox_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mail_system`
--
ALTER TABLE `mail_system`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80498;

--
-- AUTO_INCREMENT for table `mail_system_folder`
--
ALTER TABLE `mail_system_folder`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `main_group`
--
ALTER TABLE `main_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `material_process_type`
--
ALTER TABLE `material_process_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `material_specification`
--
ALTER TABLE `material_specification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `module_roles`
--
ALTER TABLE `module_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=609;

--
-- AUTO_INCREMENT for table `order_stage`
--
ALTER TABLE `order_stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `outbox`
--
ALTER TABLE `outbox`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `party`
--
ALTER TABLE `party`
  MODIFY `party_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24099;

--
-- AUTO_INCREMENT for table `party_detail`
--
ALTER TABLE `party_detail`
  MODIFY `party_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `party_log`
--
ALTER TABLE `party_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `party_type_1`
--
ALTER TABLE `party_type_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `party_type_2`
--
ALTER TABLE `party_type_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_group`
--
ALTER TABLE `product_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `proforma_invoices`
--
ALTER TABLE `proforma_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `proforma_invoice_billing_terms`
--
ALTER TABLE `proforma_invoice_billing_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proforma_invoice_cal_code_definition`
--
ALTER TABLE `proforma_invoice_cal_code_definition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proforma_invoice_items`
--
ALTER TABLE `proforma_invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `proforma_invoice_logins`
--
ALTER TABLE `proforma_invoice_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_invoice`
--
ALTER TABLE `purchase_invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase_invoice_items`
--
ALTER TABLE `purchase_invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `purchase_items`
--
ALTER TABLE `purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `quotations`
--
ALTER TABLE `quotations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18134;

--
-- AUTO_INCREMENT for table `quotation_items`
--
ALTER TABLE `quotation_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18984;

--
-- AUTO_INCREMENT for table `quotation_reason`
--
ALTER TABLE `quotation_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation_reminder_roles`
--
ALTER TABLE `quotation_reminder_roles`
  MODIFY `qr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `quotation_stage`
--
ALTER TABLE `quotation_stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `quotation_status`
--
ALTER TABLE `quotation_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `quotation_type`
--
ALTER TABLE `quotation_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `real_leave`
--
ALTER TABLE `real_leave`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reference`
--
ALTER TABLE `reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `reminder`
--
ALTER TABLE `reminder`
  MODIFY `reminder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `road_insurance_by`
--
ALTER TABLE `road_insurance_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sales_action`
--
ALTER TABLE `sales_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sales_order`
--
ALTER TABLE `sales_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- AUTO_INCREMENT for table `sales_order_billing_terms`
--
ALTER TABLE `sales_order_billing_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_order_buyers`
--
ALTER TABLE `sales_order_buyers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `sales_order_cal_code_definition`
--
ALTER TABLE `sales_order_cal_code_definition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_order_items`
--
ALTER TABLE `sales_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1070;

--
-- AUTO_INCREMENT for table `sales_order_logins`
--
ALTER TABLE `sales_order_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- AUTO_INCREMENT for table `sales_order_notes`
--
ALTER TABLE `sales_order_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sales_order_pref`
--
ALTER TABLE `sales_order_pref`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sales_status`
--
ALTER TABLE `sales_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sales_type`
--
ALTER TABLE `sales_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `service_followup_history`
--
ALTER TABLE `service_followup_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_items`
--
ALTER TABLE `service_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `staff_roles`
--
ALTER TABLE `staff_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31793;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `sub_group`
--
ALTER TABLE `sub_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `terms_and_conditions_sub`
--
ALTER TABLE `terms_and_conditions_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=364;

--
-- AUTO_INCREMENT for table `terms_condition_detail`
--
ALTER TABLE `terms_condition_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `terms_condition_group`
--
ALTER TABLE `terms_condition_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `terms_condition_template`
--
ALTER TABLE `terms_condition_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `terms_group`
--
ALTER TABLE `terms_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transportation_by`
--
ALTER TABLE `transportation_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `unloading_by`
--
ALTER TABLE `unloading_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `uom`
--
ALTER TABLE `uom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;

--
-- AUTO_INCREMENT for table `website_modules`
--
ALTER TABLE `website_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `weekly_holiday`
--
ALTER TABLE `weekly_holiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `yearly_leaves`
--
ALTER TABLE `yearly_leaves`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT;
