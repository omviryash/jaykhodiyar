-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 07, 2017 at 02:39 PM
-- Server version: 5.7.11-0ubuntu6
-- PHP Version: 7.0.19-1+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jaykhodiyar`
--

-- --------------------------------------------------------

--
-- Table structure for table `website_modules`
--

CREATE TABLE `website_modules` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `main_module` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `website_modules`
--

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES
(1, 'Party', 'party', '5.1'),
(2, 'Party Type', 'party', 'a_master'),
(4, 'Enquiry', 'inquiry', '5.2'),
(5, 'Quotation', 'quotations', '5.3'),
(6, 'Sales Order', 'sales_order', '5.4'),
(9, 'Item', '', '3.2'),
(10, 'Master Agent', '', '3.3'),
(11, 'Master Sales', '', '3.4'),
(15, 'Purchase Menu', '', '4'),
(16, 'Sales Menu', '', '5'),
(19, 'Payment', '', '8.1'),
(21, 'Change Password', '', 'y_admin'),
(22, 'Dashboard Demo', '', '2'),
(23, 'User Dropdown', '', 'y_admin'),
(24, 'Can Approve Sales Order', '', 'z'),
(25, 'Reply Leave', '', '13.2'),
(26, 'Google Sheet', '', '14.1'),
(28, 'Purchase Supplier', '', '4.1'),
(29, 'Purchase Invoice', '', '4.2'),
(30, 'Purchase Order', '', '4.3'),
(31, 'Sales Chart', '', 'y_admin'),
(32, 'Outstanding', '', '5.1.2'),
(33, 'Proforma Invoice', '', '5.5'),
(35, 'Dashboard', '', '1'),
(36, 'Master', '', '3'),
(37, 'Company Detail', '', '3.1'),
(38, 'Master Sales Reference', '', '3.4.1'),
(39, 'Master Sales Inquiry Stage', '', '3.4.2'),
(40, 'Master Sales Industry Type', '', '3.4.3'),
(41, 'Master Sales Inquiry Status', '', '3.4.4'),
(42, 'Master Sales Priority', '', '3.4.5'),
(43, 'Master Sales UOM', '', '3.4.6'),
(44, 'Master Sales Quotation Type', '', '3.4.7'),
(45, 'Master Sales Sales Type', '', '3.4.8'),
(46, 'Master Sales Quotation Reason', '', '3.4.9'),
(47, 'Master Sales Currency', '', '3.4.10'),
(48, 'Master Sales Sales', '', '3.4.11'),
(49, 'Master Sales Action', '', '3.4.12'),
(50, 'Master Sales Quotation Stage', '', '3.4.13'),
(51, 'Master Sales status', '', '3.4.14'),
(52, 'Master Sales Inquiry Confirmation', '', '3.4.15'),
(53, 'Master Sales Order Stage', '', '3.4.16'),
(54, 'Master HR', '', '3.5'),
(55, 'Master HR Day', '', '3.5.1'),
(56, 'Master HR Grade', '', '3.5.2'),
(57, 'Master Terms Condition', '', '3.6'),
(58, 'Master Terms Condition Group', '', '3.6.1'),
(59, 'Master Terms Condition Detail', '', '3.6.2'),
(60, 'Master Default Terms Condition', '', '3.6.3'),
(61, 'Master Terms Condition Template', '', '3.6.4'),
(62, 'Master General Master', '', '3.7'),
(63, 'Master General Master Branch', '', '3.7.1'),
(64, 'Master General Master Project', '', '3.7.2'),
(65, 'Master General Master Billing Template', '', '3.7.3'),
(66, 'Master General Master Country', '', '3.7.4'),
(67, 'Master General Master state', '', '3.7.5'),
(68, 'Master General Master city', '', '3.7.6'),
(69, 'Master General Master reference', '', '3.7.7'),
(70, 'Master General Master designation', '', '3.7.8'),
(71, 'Master General Master department', '', '3.7.9'),
(72, 'Master General Master terms_group', '', '3.7.10'),
(73, 'Master Staff Roles', '', '3.8'),
(74, 'Master Chat Roles', '', '3.9'),
(75, 'Master Reminder', '', '3.10'),
(76, 'Master Billing Terms', '', '3.11'),
(79, 'Ledger', '', '8.2'),
(93, 'Sales Party Log', '', '5.1.1'),
(107, 'Track Sale', '', '5.6'),
(108, 'Dispatch Menu', '', '6'),
(109, 'Challan', '', '6.1'),
(110, 'Our Cliant', '', '6.2'),
(111, 'Invoice', '', '6.3'),
(112, 'Service Menu', '', '7'),
(113, 'Finance Menu', '', '8'),
(114, 'Service', '', '7.1'),
(118, 'Finance New Payment', '', '8.2.1'),
(119, 'Summary Report', '', '9.18'),
(121, 'Report Menu', '', '9'),
(122, 'Report List Of Inquiry', '', '9.1'),
(123, 'Report List Of Quotation', '', '9.2'),
(124, 'Report Sales Lead Summary', '', '9.3'),
(125, 'Report List Of Sales Order', '', '9.4'),
(126, 'Report Delay Quotation Report', '', '9.5'),
(127, 'Report List Of Due Sales Order', '', '9.6'),
(128, 'Report Sales Order Status', '', '9.7'),
(129, 'Report Sales Summary', '', '9.8'),
(130, 'Report Dispatch Summary', '', '9.9'),
(131, 'Report Payment Receipt Details', '', '9.10'),
(132, 'Report Production Schedule', '', '9.11'),
(133, 'Report Stock Management', '', '9.12'),
(134, 'Report Quotation Management', '', '9.13'),
(135, 'Report Enquiry Followup History', '', '9.14'),
(136, 'Report Quotation followup history', '', '9.15'),
(137, 'Report Pending Report', '', '9.16'),
(138, 'Report BOM Info Report', '', '9.17'),
(139, 'General', '', '10'),
(140, 'Daily Work Entry', '', '10.1'),
(141, 'HR', '', '11'),
(142, 'HR Employee Master', '', '11.1'),
(143, 'HR Manage Employee', '', '11.1.1'),
(144, 'HR Calculate Salary', '', '11.1.2'),
(145, 'HR Leave Master', '', '11.2'),
(146, 'HR Total Free Leaves', '', '11.2.1'),
(147, 'HR Weekly Leaves', '', '11.2.2'),
(148, 'HR Yearly Leaves', '', '11.2.3'),
(149, 'HR Expected Interview', '', '11.3'),
(150, 'HR Content Management', '', '11.4'),
(151, 'HR Offer Letter', '', '11.4.1'),
(152, 'HR Appointment Letter', '', '11.4.2'),
(153, 'HR Confirmation Letter', '', '11.4.3'),
(154, 'HR Experience Letter', '', '11.4.4'),
(155, 'HR NOC Letter', '', '11.4.5'),
(156, 'HR Salary Slip Terms', '', '11.4.6'),
(157, 'HR Leave', '', '11.5'),
(158, 'Mail System', '', '12'),
(159, 'Mail System Inbox', '', '12.1'),
(160, 'Mail System Compose', '', '12.2'),
(161, 'Mail System Send Outbox Mails', '', '12.3'),
(162, 'Apply For Leave', '', '13.1'),
(163, 'Google Sheet Menu', '', '14'),
(164, 'Apply For Leave Menu', '', '13'),
(166, 'Cron', '', '15'),
(167, 'Cron Agent Chat', '', '15.1'),
(168, 'Cron Load All Staffs Unread Mails', '', '15.2'),
(169, 'Cron Load Reminders', '', '15.3'),
(170, 'Cron Remove old files', '', '15.4'),
(171, 'Master Item Master', '', '3.2.1'),
(172, 'Master Item Category', '', '3.2.2'),
(173, 'Master Item Group Code', '', '3.2.3'),
(174, 'Master Item Type', '', '3.2.4'),
(175, 'Master Item Status', '', '3.2.5'),
(176, 'Master Item Material Process Type', '', '3.2.6'),
(177, 'Master Item Main Group', '', '3.2.7'),
(178, 'Master Item Sub Group', '', '3.2.8'),
(179, 'Master Item Make', '', '3.2.9'),
(180, 'Master Item Drawing Number', '', '3.2.10'),
(181, 'Master Item Material Specification', '', '3.2.11'),
(182, 'Master Item Class', '', '3.2.12'),
(183, 'Master Item Excise', '', '3.2.13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `website_modules`
--
ALTER TABLE `website_modules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `website_modules`
--
ALTER TABLE `website_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
