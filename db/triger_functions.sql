/*-------- Triggers For Party Table -----------*/

DROP TRIGGER IF EXISTS `party_add_log`;
DELIMITER //
CREATE TRIGGER `party_add_log` AFTER INSERT ON `party`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.party_log
SET
  `party_id`=new.party_id,
  `party_code`=new.party_code,
  `party_name`=new.party_name,
  `reference_id`=new.reference_id,
  `reference_description`=new.reference_description,
  `party_type_1`=new.party_type_1,
  `party_type_2`=new.party_type_2,
  `branch_id`=new.branch_id,
  `project`=new.project,
  `address`=new.address,
  `area`=new.area,
  `pincode`=new.pincode,
  `city_id`=new.city_id,
  `state_id`=new.state_id,
  `country_id`=new.country_id,
  `phone_no`=new.phone_no,
  `fax_no`=new.fax_no,
  `email_id`=new.email_id,
  `website`=new.website,
  `opening_bal`=new.opening_bal,
  `credit_limit`=new.credit_limit,
  `active`=new.active,
  `tin_vat_no`=new.tin_vat_no,
  `tin_cst_no`=new.tin_cst_no,
  `ecc_no`=new.ecc_no,
  `pan_no`=new.pan_no,
  `range`=new.range,
  `division`=new.division,
  `service_tax_no`=new.service_tax_no,
  `utr_no`=new.utr_no,
  `address_work`=new.address_work,
  `w_address`=new.w_address,
  `w_city`=new.w_city,
  `w_state`=new.w_state,
  `w_country`=new.w_country,
  `oldw_pincode`=new.oldw_pincode,
  `w_email`=new.w_email,
  `w_web`=new.w_web,
  `w_phone1`=new.w_phone1,
  `w_phone2`=new.w_phone2,
  `w_phone3`=new.w_phone3,
  `w_note`=new.w_note,
  `oldPartyType`=new.oldPartyType,
  `oldHoliday`=new.oldHoliday,
  `oldST_No`=new.oldST_No,
  `oldVendorCode`=new.oldVendorCode,
  `oldw_fax`=new.oldw_fax,
  `oldacct_no`=new.oldacct_no,
  `oldReason`=new.oldReason,
  `oldAllowMultipalBill`=new.oldAllowMultipalBill,
  `oldAddCity`=new.oldAddCity,
  `oldCreatedBy`=new.oldCreatedBy,
  `oldCreateDate`=new.oldCreateDate,
  `oldModifyBy`=new.oldModifyBy,
  `oldModiDate`=new.oldModiDate,
  `oldPrintFlag`=new.oldPrintFlag,
  `oldPT`=new.oldPT,
  `oldCPerson`=new.oldCPerson,
  `oldOpType`=new.oldOpType,
  `oldCompProfile`=new.oldCompProfile,
  `oldBranchCode`=new.oldBranchCode,
  `oldAreaName`=new.oldAreaName,
  `oldCreditLimit`=new.oldCreditLimit,
  `oldAcctGrpCode`=new.oldAcctGrpCode,
  `oldOpBalDr`=new.oldOpBalDr,
  `oldApprovedBy`=new.oldApprovedBy,
  `oldApprovedDate`=new.oldApprovedDate,
  `oldPStage`=new.oldPStage,
  `agent_id`=new.agent_id,
  `outstanding_balance`=new.outstanding_balance,
  `created_by`=new.created_by,
  `updated_by`=new.updated_by,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type`='add'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `party_edit_log`;
DELIMITER //
CREATE TRIGGER `party_edit_log` AFTER UPDATE ON `party`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.party_log
SET
  `party_id`=new.party_id,
  `party_code`=new.party_code,
  `party_name`=new.party_name,
  `reference_id`=new.reference_id,
  `reference_description`=new.reference_description,
  `party_type_1`=new.party_type_1,
  `party_type_2`=new.party_type_2,
  `branch_id`=new.branch_id,
  `project`=new.project,
  `address`=new.address,
  `area`=new.area,
  `pincode`=new.pincode,
  `city_id`=new.city_id,
  `state_id`=new.state_id,
  `country_id`=new.country_id,
  `phone_no`=new.phone_no,
  `fax_no`=new.fax_no,
  `email_id`=new.email_id,
  `website`=new.website,
  `opening_bal`=new.opening_bal,
  `credit_limit`=new.credit_limit,
  `active`=new.active,
  `tin_vat_no`=new.tin_vat_no,
  `tin_cst_no`=new.tin_cst_no,
  `ecc_no`=new.ecc_no,
  `pan_no`=new.pan_no,
  `range`=new.range,
  `division`=new.division,
  `service_tax_no`=new.service_tax_no,
  `utr_no`=new.utr_no,
  `address_work`=new.address_work,
  `w_address`=new.w_address,
  `w_city`=new.w_city,
  `w_state`=new.w_state,
  `w_country`=new.w_country,
  `oldw_pincode`=new.oldw_pincode,
  `w_email`=new.w_email,
  `w_web`=new.w_web,
  `w_phone1`=new.w_phone1,
  `w_phone2`=new.w_phone2,
  `w_phone3`=new.w_phone3,
  `w_note`=new.w_note,
  `oldPartyType`=new.oldPartyType,
  `oldHoliday`=new.oldHoliday,
  `oldST_No`=new.oldST_No,
  `oldVendorCode`=new.oldVendorCode,
  `oldw_fax`=new.oldw_fax,
  `oldacct_no`=new.oldacct_no,
  `oldReason`=new.oldReason,
  `oldAllowMultipalBill`=new.oldAllowMultipalBill,
  `oldAddCity`=new.oldAddCity,
  `oldCreatedBy`=new.oldCreatedBy,
  `oldCreateDate`=new.oldCreateDate,
  `oldModifyBy`=new.oldModifyBy,
  `oldModiDate`=new.oldModiDate,
  `oldPrintFlag`=new.oldPrintFlag,
  `oldPT`=new.oldPT,
  `oldCPerson`=new.oldCPerson,
  `oldOpType`=new.oldOpType,
  `oldCompProfile`=new.oldCompProfile,
  `oldBranchCode`=new.oldBranchCode,
  `oldAreaName`=new.oldAreaName,
  `oldCreditLimit`=new.oldCreditLimit,
  `oldAcctGrpCode`=new.oldAcctGrpCode,
  `oldOpBalDr`=new.oldOpBalDr,
  `oldApprovedBy`=new.oldApprovedBy,
  `oldApprovedDate`=new.oldApprovedDate,
  `oldPStage`=new.oldPStage,
  `agent_id`=new.agent_id,
  `outstanding_balance`=new.outstanding_balance,
  `created_by`=new.created_by,
  `updated_by`=new.updated_by,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type`='edit'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `party_delete_log`;
DELIMITER //
CREATE TRIGGER `party_delete_log` AFTER DELETE ON `party`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.party_log
SET
  `party_id`=old.party_id,
  `party_code`=old.party_code,
  `party_name`=old.party_name,
  `reference_id`=old.reference_id,
  `reference_description`=old.reference_description,
  `party_type_1`=old.party_type_1,
  `party_type_2`=old.party_type_2,
  `branch_id`=old.branch_id,
  `project`=old.project,
  `address`=old.address,
  `area`=old.area,
  `pincode`=old.pincode,
  `city_id`=old.city_id,
  `state_id`=old.state_id,
  `country_id`=old.country_id,
  `phone_no`=old.phone_no,
  `fax_no`=old.fax_no,
  `email_id`=old.email_id,
  `website`=old.website,
  `opening_bal`=old.opening_bal,
  `credit_limit`=old.credit_limit,
  `active`=old.active,
  `tin_vat_no`=old.tin_vat_no,
  `tin_cst_no`=old.tin_cst_no,
  `ecc_no`=old.ecc_no,
  `pan_no`=old.pan_no,
  `range`=old.range,
  `division`=old.division,
  `service_tax_no`=old.service_tax_no,
  `utr_no`=old.utr_no,
  `address_work`=old.address_work,
  `w_address`=old.w_address,
  `w_city`=old.w_city,
  `w_state`=old.w_state,
  `w_country`=old.w_country,
  `oldw_pincode`=old.oldw_pincode,
  `w_email`=old.w_email,
  `w_web`=old.w_web,
  `w_phone1`=old.w_phone1,
  `w_phone2`=old.w_phone2,
  `w_phone3`=old.w_phone3,
  `w_note`=old.w_note,
  `oldPartyType`=old.oldPartyType,
  `oldHoliday`=old.oldHoliday,
  `oldST_No`=old.oldST_No,
  `oldVendorCode`=old.oldVendorCode,
  `oldw_fax`=old.oldw_fax,
  `oldacct_no`=old.oldacct_no,
  `oldReason`=old.oldReason,
  `oldAllowMultipalBill`=old.oldAllowMultipalBill,
  `oldAddCity`=old.oldAddCity,
  `oldCreatedBy`=old.oldCreatedBy,
  `oldCreateDate`=old.oldCreateDate,
  `oldModifyBy`=old.oldModifyBy,
  `oldModiDate`=old.oldModiDate,
  `oldPrintFlag`=old.oldPrintFlag,
  `oldPT`=old.oldPT,
  `oldCPerson`=old.oldCPerson,
  `oldOpType`=old.oldOpType,
  `oldCompProfile`=old.oldCompProfile,
  `oldBranchCode`=old.oldBranchCode,
  `oldAreaName`=old.oldAreaName,
  `oldCreditLimit`=old.oldCreditLimit,
  `oldAcctGrpCode`=old.oldAcctGrpCode,
  `oldOpBalDr`=old.oldOpBalDr,
  `oldApprovedBy`=old.oldApprovedBy,
  `oldApprovedDate`=old.oldApprovedDate,
  `oldPStage`=old.oldPStage,
  `agent_id`=old.agent_id,
  `outstanding_balance`=old.outstanding_balance,
  `created_by`=old.created_by,
  `updated_by`=old.updated_by,
  `created_at`=old.created_at,
  `updated_at`=old.updated_at,
  `operation_type`='delete'
//
DELIMITER ;

/*-------------- Contact Person Logs -------------*/

DROP TRIGGER IF EXISTS `contact_person_add_log`;
DELIMITER //
CREATE TRIGGER `contact_person_add_log` AFTER INSERT ON `contact_person`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_logs
SET
  `contact_person_id` = new.contact_person_id,
  `party_id` = new.party_id,
  `active` = new.active,
  `name` = new.name,
  `phone_no` = new.phone_no,
  `mobile_no` = new.mobile_no,
  `fax_no` = new.fax_no,
  `email` = new.email,
  `designation_id` = new.designation_id,
  `department_id` = new.department_id,
  `note` = new.note,
  `priority` = new.priority,
  `created_by` = new.created_by,
  `updated_by` = new.updated_by,
  `created_at` = new.created_at,
  `operation_type` = 'add'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `contact_person_edit_log`;
DELIMITER //
CREATE TRIGGER `contact_person_edit_log` AFTER UPDATE ON `contact_person`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_logs
SET
  `contact_person_id` = new.contact_person_id,
  `party_id` = new.party_id,
  `active` = new.active,
  `name` = new.name,
  `phone_no` = new.phone_no,
  `mobile_no` = new.mobile_no,
  `fax_no` = new.fax_no,
  `email` = new.email,
  `designation_id` = new.designation_id,
  `department_id` = new.department_id,
  `note` = new.note,
  `priority` = new.priority,
  `created_by` = new.created_by,
  `updated_by` = new.updated_by,
  `created_at` = new.created_at,
  `operation_type` = 'edit'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `contact_person_delete_log`;
DELIMITER //
CREATE TRIGGER `contact_person_delete_log` AFTER DELETE ON `contact_person`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_logs
SET
  `contact_person_id` = old.contact_person_id,
  `party_id` = old.party_id,
  `active` = old.active,
  `name` = old.name,
  `phone_no` = old.phone_no,
  `mobile_no` = old.mobile_no,
  `fax_no` = old.fax_no,
  `email` = old.email,
  `designation_id` = old.designation_id,
  `department_id` = old.department_id,
  `note` = old.note,
  `priority` = old.priority,
  `created_by` = old.created_by,
  `updated_by` = old.updated_by,
  `created_at` = old.created_at,
  `operation_type` = 'delete'
//
DELIMITER ;


/*-------------- Inquiry Logs -------------*/

DROP TRIGGER IF EXISTS `inquiry_add_log`;
DELIMITER //
CREATE TRIGGER `inquiry_add_log` AFTER INSERT ON `inquiry`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = new.inquiry_id,
  `inquiry_no` = new.inquiry_no,
  `due_date` = new.due_date,
  `reference_date` = new.reference_date,
  `inquiry_date` = new.inquiry_date,
  `kind_attn_id` = new.kind_attn_id,
  `follow_up_by_id` = new.follow_up_by_id,
  `assigned_to_id` = new.assigned_to_id,
  `industry_type_id` = new.industry_type_id,
  `inquiry_stage_id` = new.inquiry_stage_id,
  `branch_id` = new.branch_id,
  `inquiry_status_id` = new.inquiry_status_id,
  `sales_id` = new.sales_id,
  `sales_type_id` = new.sales_type_id,
  `party_id` = new.party_id,
  `received_email` = new.received_email,
  `received_fax` = new.received_fax,
  `received_verbal` = new.received_verbal,
  `is_received_post` = new.is_received_post,
  `send_email` = new.send_email,
  `send_fax` = new.send_fax,
  `is_send_post` = new.is_send_post,
  `send_from_post_name` = new.send_from_post_name,
  `lead_or_inquiry` = new.lead_or_inquiry,
  `lead_id` = new.lead_id,
  `history` = new.history,
  `created_by` = new.created_by,
  `created_at` = new.created_at,
  `created_at` = new.created_at,
  `operation_type` = 'add'
//
DELIMITER ;


DROP TRIGGER IF EXISTS `inquiry_edit_log`;
DELIMITER //
CREATE TRIGGER `inquiry_edit_log` AFTER UPDATE ON `inquiry`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = new.inquiry_id,
  `inquiry_no` = new.inquiry_no,
  `due_date` = new.due_date,
  `reference_date` = new.reference_date,
  `inquiry_date` = new.inquiry_date,
  `kind_attn_id` = new.kind_attn_id,
  `follow_up_by_id` = new.follow_up_by_id,
  `assigned_to_id` = new.assigned_to_id,
  `industry_type_id` = new.industry_type_id,
  `inquiry_stage_id` = new.inquiry_stage_id,
  `branch_id` = new.branch_id,
  `inquiry_status_id` = new.inquiry_status_id,
  `sales_id` = new.sales_id,
  `sales_type_id` = new.sales_type_id,
  `party_id` = new.party_id,
  `received_email` = new.received_email,
  `received_fax` = new.received_fax,
  `received_verbal` = new.received_verbal,
  `is_received_post` = new.is_received_post,
  `send_email` = new.send_email,
  `send_fax` = new.send_fax,
  `is_send_post` = new.is_send_post,
  `send_from_post_name` = new.send_from_post_name,
  `lead_or_inquiry` = new.lead_or_inquiry,
  `lead_id` = new.lead_id,
  `history` = new.history,
  `created_by` = new.created_by,
  `created_at` = new.created_at,
  `created_at` = new.created_at,
  `operation_type` = 'edit'
//
DELIMITER ;


DROP TRIGGER IF EXISTS `inquiry_delete_log`;
DELIMITER //
CREATE TRIGGER `inquiry_delete_log` AFTER DELETE ON `inquiry`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = old.inquiry_id,
  `inquiry_no` = old.inquiry_no,
  `due_date` = old.due_date,
  `reference_date` = old.reference_date,
  `inquiry_date` = old.inquiry_date,
  `kind_attn_id` = old.kind_attn_id,
  `follow_up_by_id` = old.follow_up_by_id,
  `assigned_to_id` = old.assigned_to_id,
  `industry_type_id` = old.industry_type_id,
  `inquiry_stage_id` = old.inquiry_stage_id,
  `branch_id` = oldbranch_id,
  `inquiry_status_id` = old.inquiry_status_id,
  `sales_id` = old.sales_id,
  `sales_type_id` = old.sales_type_id,
  `party_id` = old.party_id,
  `received_email` = old.received_email,
  `received_fax` = old.received_fax,
  `received_verbal` = old.received_verbal,
  `is_received_post` = old.is_received_post,
  `send_email` = old.send_email,
  `send_fax` = old.send_fax,
  `is_send_post` = old.is_send_post,
  `send_from_post_name` = old.send_from_post_name,
  `lead_or_inquiry` = old.lead_or_inquiry,
  `lead_id` = old.lead_id,
  `history` = old.history,
  `created_by` = old.created_by,
  `created_at` = old.created_at,
  `created_at` = old.created_at,
  `operation_type` = 'delete'
//
DELIMITER ;

/*-------------- Inquiry Items Logs -------------*/

DROP TRIGGER IF EXISTS `inquiry_item_add_log`;
DELIMITER //
CREATE TRIGGER `inquiry_item_add_log` AFTER INSERT ON `inquiry_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_logs
SET
  `id` = new.id,
  `inquiry_id` = new.inquiry_id,
  `item_data` = new.item_data,
  `inquiry_flag` = new.inquiry_flag,
  `quotation_flag` = new.quotation_flag,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `operation_type` = 'add'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `inquiry_item_edit_log`;
DELIMITER //
CREATE TRIGGER `inquiry_item_edit_log` AFTER UPDATE ON `inquiry_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_logs
SET
  `id` = new.id,
  `inquiry_id` = new.inquiry_id,
  `item_data` = new.item_data,
  `inquiry_flag` = new.inquiry_flag,
  `quotation_flag` = new.quotation_flag,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `operation_type` = 'edit'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `inquiry_item_delete_log`;
DELIMITER //
CREATE TRIGGER `inquiry_item_delete_log` AFTER DELETE ON `inquiry_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_logs
SET
  `id` = old.id,
  `inquiry_id` = old.inquiry_id,
  `item_data` = old.item_data,
  `inquiry_flag` = old.inquiry_flag,
  `quotation_flag` = old.quotation_flag,
  `created_at` = old.created_at,
  `updated_at` = old.updated_at,
  `operation_type` = 'delete'
//
DELIMITER ;

/*-------------- Quotation Logs -------------*/

DROP TRIGGER IF EXISTS `quotation_add_log`;
DELIMITER //
CREATE TRIGGER `quotation_add_log` AFTER INSERT ON `quotations`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.quotations_logs
SET
  `id`=new.id,
  `enquiry_id`=new.enquiry_id,
  `revised_from_id`=new.revised_from_id,
  `party_id`=new.party_id,
  `enquiry_no`=new.enquiry_no,
  `quotation_no`=new.quotation_no,
  `quotation_stage_id`=new.quotation_stage_id,
  `quotation_type_id`=new.quotation_type_id,
  `quotation_status_id`=new.quotation_status_id,
  `sales_type_id`=new.sales_type_id,
  `sales_id`=new.sales_id,
  `action`=new.action,
  `currency_id`=new.currency_id,
  `rev`=new.rev,
  `rev_date`=new.rev_date,
  `quotation_note`=new.quotation_note,
  `reason`=new.reason,
  `reason_remark`=new.reason_remark,
  `review_date_check`=new.review_date_check,
  `review_date`=new.review_date,
  `enquiry_date`=new.enquiry_date,
  `quotation_date`=new.quotation_date,
  `due_date_check`=new.due_date_check,
  `oldEnquiryNo`=new.oldEnquiryNo,
  `oldSubject`=new.oldSubject,
  `oldPreparedBy`=new.oldPreparedBy,
  `oldApprovedBy`=new.oldApprovedBy,
  `oldSrNo`=new.oldSrNo,
  `oldExpr2`=new.oldExpr2,
  `oldEnquiryDate`=new.oldEnquiryDate,
  `oldKindAttn`=new.oldKindAttn,
  `oldEnquiryStatus`=new.oldEnquiryStatus,
  `oldEnqRef`=new.oldEnqRef,
  `due_date`=new.due_date,
  `conv_rate`=new.conv_rate,
  `reference_date`=new.reference_date,
  `kind_attn_id`=new.kind_attn_id,
  `party_code`=new.party_code,
  `party_name`=new.party_name,
  `reference_id`=new.reference_id,
  `reference_description`=new.reference_description,
  `address`=new.address,
  `agent_id`=new.agent_id,
  `city_id`=new.city_id,
  `state_id`=new.state_id,
  `country_id`=new.country_id,
  `fax_no`=new.fax_no,
  `email_id`=new.email_id,
  `project`=new.project,
  `phone_no`=new.phone_no,
  `pincode`=new.pincode,
  `website`=new.website,
  `contact_person_name`=new.contact_person_name,
  `contact_person_contact_no`=new.contact_person_contact_no,
  `contact_person_email_id`=new.contact_person_email_id,
  `subject`=new.subject,
  `header`=new.header,
  `footer`=new.footer,
  `cc_to`=new.cc_to,
  `authorised_by`=new.authorised_by,
  `reference_note`=new.reference_note,
  `item_dec_title`=new.item_dec_title,
  `addon_title`=new.addon_title,
  `specification_1`=new.specification_1,
  `specification_2`=new.specification_2,
  `specification_3`=new.specification_3,
  `send_from_post`=new.send_from_post,
  `send_from_post_name`=new.send_from_post_name,
  `pdf_url`=new.pdf_url,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `created_by`=new.created_by,
  `operation_type` = 'add'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `quotation_edit_log`;
DELIMITER //
CREATE TRIGGER `quotation_edit_log` AFTER UPDATE ON `quotations`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.quotations_logs
SET
  `id`=new.id,
  `enquiry_id`=new.enquiry_id,
  `revised_from_id`=new.revised_from_id,
  `party_id`=new.party_id,
  `enquiry_no`=new.enquiry_no,
  `quotation_no`=new.quotation_no,
  `quotation_stage_id`=new.quotation_stage_id,
  `quotation_type_id`=new.quotation_type_id,
  `quotation_status_id`=new.quotation_status_id,
  `sales_type_id`=new.sales_type_id,
  `sales_id`=new.sales_id,
  `action`=new.action,
  `currency_id`=new.currency_id,
  `rev`=new.rev,
  `rev_date`=new.rev_date,
  `quotation_note`=new.quotation_note,
  `reason`=new.reason,
  `reason_remark`=new.reason_remark,
  `review_date_check`=new.review_date_check,
  `review_date`=new.review_date,
  `enquiry_date`=new.enquiry_date,
  `quotation_date`=new.quotation_date,
  `due_date_check`=new.due_date_check,
  `oldEnquiryNo`=new.oldEnquiryNo,
  `oldSubject`=new.oldSubject,
  `oldPreparedBy`=new.oldPreparedBy,
  `oldApprovedBy`=new.oldApprovedBy,
  `oldSrNo`=new.oldSrNo,
  `oldExpr2`=new.oldExpr2,
  `oldEnquiryDate`=new.oldEnquiryDate,
  `oldKindAttn`=new.oldKindAttn,
  `oldEnquiryStatus`=new.oldEnquiryStatus,
  `oldEnqRef`=new.oldEnqRef,
  `due_date`=new.due_date,
  `conv_rate`=new.conv_rate,
  `reference_date`=new.reference_date,
  `kind_attn_id`=new.kind_attn_id,
  `party_code`=new.party_code,
  `party_name`=new.party_name,
  `reference_id`=new.reference_id,
  `reference_description`=new.reference_description,
  `address`=new.address,
  `agent_id`=new.agent_id,
  `city_id`=new.city_id,
  `state_id`=new.state_id,
  `country_id`=new.country_id,
  `fax_no`=new.fax_no,
  `email_id`=new.email_id,
  `project`=new.project,
  `phone_no`=new.phone_no,
  `pincode`=new.pincode,
  `website`=new.website,
  `contact_person_name`=new.contact_person_name,
  `contact_person_contact_no`=new.contact_person_contact_no,
  `contact_person_email_id`=new.contact_person_email_id,
  `subject`=new.subject,
  `header`=new.header,
  `footer`=new.footer,
  `cc_to`=new.cc_to,
  `authorised_by`=new.authorised_by,
  `reference_note`=new.reference_note,
  `item_dec_title`=new.item_dec_title,
  `addon_title`=new.addon_title,
  `specification_1`=new.specification_1,
  `specification_2`=new.specification_2,
  `specification_3`=new.specification_3,
  `send_from_post`=new.send_from_post,
  `send_from_post_name`=new.send_from_post_name,
  `pdf_url`=new.pdf_url,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `created_by`=new.created_by,
  `operation_type` = 'edit'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `quotation_delete_log`;
DELIMITER //
CREATE TRIGGER `quotation_delete_log` AFTER DELETE ON `quotations`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.quotations_logs
SET
  `id`=old.id,
  `enquiry_id`=old.enquiry_id,
  `revised_from_id`=old.revised_from_id,
  `party_id`=old.party_id,
  `enquiry_no`=old.enquiry_no,
  `quotation_no`=old.quotation_no,
  `quotation_stage_id`=old.quotation_stage_id,
  `quotation_type_id`=old.quotation_type_id,
  `quotation_status_id`=old.quotation_status_id,
  `sales_type_id`=old.sales_type_id,
  `sales_id`=old.sales_id,
  `action`=old.action,
  `currency_id`=old.currency_id,
  `rev`=old.rev,
  `rev_date`=old.rev_date,
  `quotation_note`=old.quotation_note,
  `reason`=old.reason,
  `reason_remark`=old.reason_remark,
  `review_date_check`=old.review_date_check,
  `review_date`=old.review_date,
  `enquiry_date`=old.enquiry_date,
  `quotation_date`=old.quotation_date,
  `due_date_check`=old.due_date_check,
  `oldEnquiryNo`=old.oldEnquiryNo,
  `oldSubject`=old.oldSubject,
  `oldPreparedBy`=old.oldPreparedBy,
  `oldApprovedBy`=old.oldApprovedBy,
  `oldSrNo`=old.oldSrNo,
  `oldExpr2`=old.oldExpr2,
  `oldEnquiryDate`=old.oldEnquiryDate,
  `oldKindAttn`=old.oldKindAttn,
  `oldEnquiryStatus`=old.oldEnquiryStatus,
  `oldEnqRef`=old.oldEnqRef,
  `due_date`=old.due_date,
  `conv_rate`=old.conv_rate,
  `reference_date`=old.reference_date,
  `kind_attn_id`=old.kind_attn_id,
  `party_code`=old.party_code,
  `party_name`=old.party_name,
  `reference_id`=old.reference_id,
  `reference_description`=old.reference_description,
  `address`=old.address,
  `agent_id`=old.agent_id,
  `city_id`=old.city_id,
  `state_id`=old.state_id,
  `country_id`=old.country_id,
  `fax_no`=old.fax_no,
  `email_id`=old.email_id,
  `project`=old.project,
  `phone_no`=old.phone_no,
  `pincode`=old.pincode,
  `website`=old.website,
  `contact_person_name`=old.contact_person_name,
  `contact_person_contact_no`=old.contact_person_contact_no,
  `contact_person_email_id`=old.contact_person_email_id,
  `subject`=old.subject,
  `header`=old.header,
  `footer`=old.footer,
  `cc_to`=old.cc_to,
  `authorised_by`=old.authorised_by,
  `reference_note`=old.reference_note,
  `item_dec_title`=old.item_dec_title,
  `addon_title`=old.addon_title,
  `specification_1`=old.specification_1,
  `specification_2`=old.specification_2,
  `specification_3`=old.specification_3,
  `send_from_post`=old.send_from_post,
  `send_from_post_name`=old.send_from_post_name,
  `pdf_url`=old.pdf_url,
  `created_at`=old.created_at,
  `updated_at`=old.updated_at,
  `created_by`=old.created_by,
  `operation_type` = 'delete'
//
DELIMITER ;

/*-------------- Quotation Item Logs -------------*/

DROP TRIGGER IF EXISTS `quotation_items_add_log`;
DELIMITER //
CREATE TRIGGER `quotation_items_add_log` AFTER INSERT ON `quotation_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.quotation_items_logs
SET
  `id`=new.id,
  `quotation_id`=new.quotation_id,
  `item_code`=new.item_code,
  `item_name`=new.item_name,
  `item_description`=new.item_description,
  `item_desc`=new.item_desc,
  `add_description`=new.add_description,
  `detail_description`=new.detail_description,
  `drawing_number`=new.drawing_number,
  `drawing_revision`=new.drawing_revision,
  `uom_id`=new.uom_id,
  `uom`=new.uom,
  `total_amount`=new.total_amount,
  `item_note`=new.item_note,
  `quantity`=new.quantity,
  `disc_per`=new.disc_per,
  `rate`=new.rate,
  `disc_value`=new.disc_value,
  `amount`=new.amount,
  `net_amount`=new.net_amount,
  `item_status_id`=new.item_status_id,
  `item_status`=new.item_status,
  `cust_part_no`=new.cust_part_no,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type` = 'add'
//
DELIMITER ;


DROP TRIGGER IF EXISTS `quotation_items_edit_log`;
DELIMITER //
CREATE TRIGGER `quotation_items_edit_log` AFTER UPDATE ON `quotation_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.quotation_items_logs
SET
  `id`=new.id,
  `quotation_id`=new.quotation_id,
  `item_code`=new.item_code,
  `item_name`=new.item_name,
  `item_description`=new.item_description,
  `item_desc`=new.item_desc,
  `add_description`=new.add_description,
  `detail_description`=new.detail_description,
  `drawing_number`=new.drawing_number,
  `drawing_revision`=new.drawing_revision,
  `uom_id`=new.uom_id,
  `uom`=new.uom,
  `total_amount`=new.total_amount,
  `item_note`=new.item_note,
  `quantity`=new.quantity,
  `disc_per`=new.disc_per,
  `rate`=new.rate,
  `disc_value`=new.disc_value,
  `amount`=new.amount,
  `net_amount`=new.net_amount,
  `item_status_id`=new.item_status_id,
  `item_status`=new.item_status,
  `cust_part_no`=new.cust_part_no,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type` = 'edit'
//
DELIMITER ;


DROP TRIGGER IF EXISTS `quotation_items_delete_log`;
DELIMITER //
CREATE TRIGGER `quotation_items_delete_log` AFTER DELETE ON `quotation_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.quotation_items_logs
SET
  `id`=new.id,
  `quotation_id`=new.quotation_id,
  `item_code`=new.item_code,
  `item_name`=new.item_name,
  `item_description`=new.item_description,
  `item_desc`=new.item_desc,
  `add_description`=new.add_description,
  `detail_description`=new.detail_description,
  `drawing_number`=new.drawing_number,
  `drawing_revision`=new.drawing_revision,
  `uom_id`=new.uom_id,
  `uom`=new.uom,
  `total_amount`=new.total_amount,
  `item_note`=new.item_note,
  `quantity`=new.quantity,
  `disc_per`=new.disc_per,
  `rate`=new.rate,
  `disc_value`=new.disc_value,
  `amount`=new.amount,
  `net_amount`=new.net_amount,
  `item_status_id`=new.item_status_id,
  `item_status`=new.item_status,
  `cust_part_no`=new.cust_part_no,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type` = 'delete'
//
DELIMITER ;




/*-------------- Sales Order Logs -------------*/

DROP TRIGGER IF EXISTS `sales_order_add_log`;
DELIMITER //
CREATE TRIGGER `sales_order_add_log` AFTER INSERT ON `sales_order`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.sales_order_logs
SET
  `id`=new.id,
  `quatation_id`=new.quatation_id,
  `sales_order_no`=new.sales_order_no,
  `sales_to_party_id`=new.sales_to_party_id,
  `branch_id`=new.branch_id,
  `billing_terms_id`=new.billing_terms_id,
  `sales_order_date`=new.sales_order_date,
  `committed_date`=new.committed_date,
  `currency_id`=new.currency_id,
  `project_id`=new.project_id,
  `kind_attn_id`=new.kind_attn_id,
  `contact_no`=new.contact_no,
  `cust_po_no`=new.cust_po_no,
  `email_id`=new.email_id,
  `po_date`=new.po_date,
  `sales_order_pref_id`=new.sales_order_pref_id,
  `conversation_rate`=new.conversation_rate,
  `sales_id`=new.sales_id,
  `sales_order_stage_id`=new.sales_order_stage_id,
  `sales_order_status_id`=new.sales_order_status_id,
  `order_type`=new.order_type,
  `lead_provider_id`=new.lead_provider_id,
  `buyer_detail_id`=new.buyer_detail_id,
  `note_detail_id`=new.note_detail_id,
  `login_detail_id`=new.login_detail_id,
  `transportation_by_id`=new.transportation_by_id,
  `transport_amount`=new.transport_amount,
  `foundation_drawing_required_id`=new.foundation_drawing_required_id,
  `loading_by_id`=new.loading_by_id,
  `inspection_required_id`=new.inspection_required_id,
  `unloading_by_id`=new.unloading_by_id,
  `erection_commissioning_id`=new.erection_commissioning_id,
  `road_insurance_by_id`=new.road_insurance_by_id,
  `for_required_id`=new.for_required_id,
  `review_date`=new.review_date,
  `mach_deli_min_weeks`=new.mach_deli_min_weeks,
  `mach_deli_max_weeks`=new.mach_deli_max_weeks,
  `mach_deli_min_weeks_date`=new.mach_deli_min_weeks_date,
  `mach_deli_max_weeks_date`=new.mach_deli_max_weeks_date,
  `sales_order_validaty`=new.sales_order_validaty,
  `sales_order_quotation_delivery`=new.sales_order_quotation_delivery,
  `mode_of_shipment_name`=new.mode_of_shipment_name,
  `mode_of_shipment_truck_number`=new.mode_of_shipment_truck_number,
  `supplier_payment_terms`=new.supplier_payment_terms,
  `received_payment`=new.received_payment,
  `received_payment_date`=new.received_payment_date,
  `received_payment_type`=new.received_payment_type,
  `terms_condition_purchase`=new.terms_condition_purchase,
  `isApproved`=new.isApproved,
  `pdf_url`=new.pdf_url,
  `taxes_data`=new.taxes_data,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `created_by`=new.created_by,
  `operation_type` = 'add'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `sales_order_edit_log`;
DELIMITER //
CREATE TRIGGER `sales_order_edit_log` AFTER UPDATE ON `sales_order`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.sales_order_logs
SET
  `id`=new.id,
  `quatation_id`=new.quatation_id,
  `sales_order_no`=new.sales_order_no,
  `sales_to_party_id`=new.sales_to_party_id,
  `branch_id`=new.branch_id,
  `billing_terms_id`=new.billing_terms_id,
  `sales_order_date`=new.sales_order_date,
  `committed_date`=new.committed_date,
  `currency_id`=new.currency_id,
  `project_id`=new.project_id,
  `kind_attn_id`=new.kind_attn_id,
  `contact_no`=new.contact_no,
  `cust_po_no`=new.cust_po_no,
  `email_id`=new.email_id,
  `po_date`=new.po_date,
  `sales_order_pref_id`=new.sales_order_pref_id,
  `conversation_rate`=new.conversation_rate,
  `sales_id`=new.sales_id,
  `sales_order_stage_id`=new.sales_order_stage_id,
  `sales_order_status_id`=new.sales_order_status_id,
  `order_type`=new.order_type,
  `lead_provider_id`=new.lead_provider_id,
  `buyer_detail_id`=new.buyer_detail_id,
  `note_detail_id`=new.note_detail_id,
  `login_detail_id`=new.login_detail_id,
  `transportation_by_id`=new.transportation_by_id,
  `transport_amount`=new.transport_amount,
  `foundation_drawing_required_id`=new.foundation_drawing_required_id,
  `loading_by_id`=new.loading_by_id,
  `inspection_required_id`=new.inspection_required_id,
  `unloading_by_id`=new.unloading_by_id,
  `erection_commissioning_id`=new.erection_commissioning_id,
  `road_insurance_by_id`=new.road_insurance_by_id,
  `for_required_id`=new.for_required_id,
  `review_date`=new.review_date,
  `mach_deli_min_weeks`=new.mach_deli_min_weeks,
  `mach_deli_max_weeks`=new.mach_deli_max_weeks,
  `mach_deli_min_weeks_date`=new.mach_deli_min_weeks_date,
  `mach_deli_max_weeks_date`=new.mach_deli_max_weeks_date,
  `sales_order_validaty`=new.sales_order_validaty,
  `sales_order_quotation_delivery`=new.sales_order_quotation_delivery,
  `mode_of_shipment_name`=new.mode_of_shipment_name,
  `mode_of_shipment_truck_number`=new.mode_of_shipment_truck_number,
  `supplier_payment_terms`=new.supplier_payment_terms,
  `received_payment`=new.received_payment,
  `received_payment_date`=new.received_payment_date,
  `received_payment_type`=new.received_payment_type,
  `terms_condition_purchase`=new.terms_condition_purchase,
  `isApproved`=new.isApproved,
  `pdf_url`=new.pdf_url,
  `taxes_data`=new.taxes_data,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `created_by`=new.created_by,
  `operation_type` = 'edit'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `sales_order_delete_log`;
DELIMITER //
CREATE TRIGGER `sales_order_delete_log` AFTER DELETE ON `sales_order`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.sales_order_logs
SET
  `id`=old.id,
  `quatation_id`=old.quatation_id,
  `sales_order_no`=old.sales_order_no,
  `sales_to_party_id`=old.sales_to_party_id,
  `branch_id`=old.branch_id,
  `billing_terms_id`=old.billing_terms_id,
  `sales_order_date`=old.sales_order_date,
  `committed_date`=old.committed_date,
  `currency_id`=old.currency_id,
  `project_id`=old.project_id,
  `kind_attn_id`=old.kind_attn_id,
  `contact_no`=old.contact_no,
  `cust_po_no`=old.cust_po_no,
  `email_id`=old.email_id,
  `po_date`=old.po_date,
  `sales_order_pref_id`=old.sales_order_pref_id,
  `conversation_rate`=old.conversation_rate,
  `sales_id`=old.sales_id,
  `sales_order_stage_id`=old.sales_order_stage_id,
  `sales_order_status_id`=old.sales_order_status_id,
  `order_type`=old.order_type,
  `lead_provider_id`=old.lead_provider_id,
  `buyer_detail_id`=old.buyer_detail_id,
  `note_detail_id`=old.note_detail_id,
  `login_detail_id`=old.login_detail_id,
  `transportation_by_id`=old.transportation_by_id,
  `transport_amount`=old.transport_amount,
  `foundation_drawing_required_id`=old.foundation_drawing_required_id,
  `loading_by_id`=old.loading_by_id,
  `inspection_required_id`=old.inspection_required_id,
  `unloading_by_id`=old.unloading_by_id,
  `erection_commissioning_id`=old.erection_commissioning_id,
  `road_insurance_by_id`=old.road_insurance_by_id,
  `for_required_id`=old.for_required_id,
  `review_date`=old.review_date,
  `mach_deli_min_weeks`=old.mach_deli_min_weeks,
  `mach_deli_max_weeks`=old.mach_deli_max_weeks,
  `mach_deli_min_weeks_date`=old.mach_deli_min_weeks_date,
  `mach_deli_max_weeks_date`=old.mach_deli_max_weeks_date,
  `sales_order_validaty`=old.sales_order_validaty,
  `sales_order_quotation_delivery`=old.sales_order_quotation_delivery,
  `mode_of_shipment_name`=old.mode_of_shipment_name,
  `mode_of_shipment_truck_number`=old.mode_of_shipment_truck_number,
  `supplier_payment_terms`=old.supplier_payment_terms,
  `received_payment`=old.received_payment,
  `received_payment_date`=old.received_payment_date,
  `received_payment_type`=old.received_payment_type,
  `terms_condition_purchase`=old.terms_condition_purchase,
  `isApproved`=old.isApproved,
  `pdf_url`=old.pdf_url,
  `taxes_data`=old.taxes_data,
  `created_at`=old.created_at,
  `updated_at`=old.updated_at,
  `created_by`=old.created_by,
  `operation_type` = 'delete'
//
DELIMITER ;


/*-------------- Sales Order Items Logs -------------*/

DROP TRIGGER IF EXISTS `sales_order_items_add_log`;
DELIMITER //
CREATE TRIGGER `sales_order_items_add_log` AFTER INSERT ON `sales_order_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.sales_order_items_logs
SET
  `id`=new.id,
  `item_id`=new.item_id,
  `sales_order_id`=new.sales_order_id,
  `item_code`=new.item_code,
  `item_category_id`=new.item_category_id,
  `item_name`=new.item_name,
  `item_desc`=new.item_desc,
  `item_description`=new.item_description,
  `add_description`=new.add_description,
  `detail_description`=new.detail_description,
  `drawing_number`=new.drawing_number,
  `drawing_revision`=new.drawing_revision,
  `uom_id`=new.uom_id,
  `uom`=new.uom,
  `total_amount`=new.total_amount,
  `item_note`=new.item_note,
  `quantity`=new.quantity,
  `disc_per`=new.disc_per,
  `rate`=new.rate,
  `disc_value`=new.disc_value,
  `amount`=new.amount,
  `net_amount`=new.net_amount,
  `item_status_id`=new.item_status_id,
  `cust_part_no`=new.cust_part_no,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type` = 'add'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `sales_order_items_edit_log`;
DELIMITER //
CREATE TRIGGER `sales_order_items_edit_log` AFTER UPDATE ON `sales_order_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.sales_order_items_logs
SET
  `id`=new.id,
  `item_id`=new.item_id,
  `sales_order_id`=new.sales_order_id,
  `item_code`=new.item_code,
  `item_category_id`=new.item_category_id,
  `item_name`=new.item_name,
  `item_desc`=new.item_desc,
  `item_description`=new.item_description,
  `add_description`=new.add_description,
  `detail_description`=new.detail_description,
  `drawing_number`=new.drawing_number,
  `drawing_revision`=new.drawing_revision,
  `uom_id`=new.uom_id,
  `uom`=new.uom,
  `total_amount`=new.total_amount,
  `item_note`=new.item_note,
  `quantity`=new.quantity,
  `disc_per`=new.disc_per,
  `rate`=new.rate,
  `disc_value`=new.disc_value,
  `amount`=new.amount,
  `net_amount`=new.net_amount,
  `item_status_id`=new.item_status_id,
  `cust_part_no`=new.cust_part_no,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type` = 'edit'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `sales_order_items_delete_log`;
DELIMITER //
CREATE TRIGGER `sales_order_items_delete_log` AFTER UPDATE ON `sales_order_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.sales_order_items_logs
SET
  `id`=new.id,
  `item_id`=new.item_id,
  `sales_order_id`=new.sales_order_id,
  `item_code`=new.item_code,
  `item_category_id`=new.item_category_id,
  `item_name`=new.item_name,
  `item_desc`=new.item_desc,
  `item_description`=new.item_description,
  `add_description`=new.add_description,
  `detail_description`=new.detail_description,
  `drawing_number`=new.drawing_number,
  `drawing_revision`=new.drawing_revision,
  `uom_id`=new.uom_id,
  `uom`=new.uom,
  `total_amount`=new.total_amount,
  `item_note`=new.item_note,
  `quantity`=new.quantity,
  `disc_per`=new.disc_per,
  `rate`=new.rate,
  `disc_value`=new.disc_value,
  `amount`=new.amount,
  `net_amount`=new.net_amount,
  `item_status_id`=new.item_status_id,
  `cust_part_no`=new.cust_part_no,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type` = 'delete'
//
DELIMITER ;

/*-------------- Sales Order Items Logs -------------*/

DROP TRIGGER IF EXISTS `sales_order_items_add_log`;
DELIMITER //
CREATE TRIGGER `sales_order_items_add_log` AFTER INSERT ON `sales_order_items`
FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.sales_order_items_logs
SET
  `id`=new.id,
  `item_id`=new.item_id,
  `sales_order_id`=new.sales_order_id,
  `item_code`=new.item_code,
  `item_category_id`=new.item_category_id,
  `item_name`=new.item_name,
  `item_desc`=new.item_desc,
  `item_description`=new.item_description,
  `add_description`=new.add_description,
  `detail_description`=new.detail_description,
  `drawing_number`=new.drawing_number,
  `drawing_revision`=new.drawing_revision,
  `uom_id`=new.uom_id,
  `uom`=new.uom,
  `total_amount`=new.total_amount,
  `item_note`=new.item_note,
  `quantity`=new.quantity,
  `disc_per`=new.disc_per,
  `rate`=new.rate,
  `disc_value`=new.disc_value,
  `amount`=new.amount,
  `net_amount`=new.net_amount,
  `item_status_id`=new.item_status_id,
  `cust_part_no`=new.cust_part_no,
  `created_at`=new.created_at,
  `updated_at`=new.updated_at,
  `operation_type` = 'add'
//
DELIMITER ;