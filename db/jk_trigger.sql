DROP TRIGGER IF EXISTS `agent_add_log`;
DROP TRIGGER IF EXISTS `agent_delete_log`;
DROP TRIGGER IF EXISTS `agent_edit_log`;
DROP TRIGGER IF EXISTS `billing_template_add_log`;
DROP TRIGGER IF EXISTS `billing_template_delete_log`;
DROP TRIGGER IF EXISTS `billing_template_edit_log`;
DROP TRIGGER IF EXISTS `billing_terms_add_log`;
DROP TRIGGER IF EXISTS `billing_terms_delete_log`;
DROP TRIGGER IF EXISTS `billing_terms_detail_add_log`;
DROP TRIGGER IF EXISTS `billing_terms_detail_delete_log`;
DROP TRIGGER IF EXISTS `billing_terms_detail_edit_log`;
DROP TRIGGER IF EXISTS `billing_terms_edit_log`;
DROP TRIGGER IF EXISTS `branch_add_log`;
DROP TRIGGER IF EXISTS `branch_delete_log`;
DROP TRIGGER IF EXISTS `branch_edit_log`;
DROP TRIGGER IF EXISTS `cal_code_definition_add_log`;
DROP TRIGGER IF EXISTS `cal_code_definition_delete_log`;
DROP TRIGGER IF EXISTS `cal_code_definition_edit_log`;
DROP TRIGGER IF EXISTS `challan_items_add_log`;
DROP TRIGGER IF EXISTS `challan_items_delete_log`;
DROP TRIGGER IF EXISTS `challan_items_edit_log`;
DROP TRIGGER IF EXISTS `challans_add_log`;
DROP TRIGGER IF EXISTS `challans_delete_log`;
DROP TRIGGER IF EXISTS `challans_edit_log`;
DROP TRIGGER IF EXISTS `chat_roles_add_log`;
DROP TRIGGER IF EXISTS `chat_roles_delete_log`;
DROP TRIGGER IF EXISTS `chat_roles_edit_log`;
DROP TRIGGER IF EXISTS `city_add_log`;
DROP TRIGGER IF EXISTS `city_delete_log`;
DROP TRIGGER IF EXISTS `city_edit_log`;
DROP TRIGGER IF EXISTS `company_add_log`;
DROP TRIGGER IF EXISTS `company_delete_log`;
DROP TRIGGER IF EXISTS `company_edit_log`;
DROP TRIGGER IF EXISTS `config_add_log`;
DROP TRIGGER IF EXISTS `config_delete_log`;
DROP TRIGGER IF EXISTS `config_edit_log`;
DROP TRIGGER IF EXISTS `contact_person_add_log`;
DROP TRIGGER IF EXISTS `contact_person_delete_log`;
DROP TRIGGER IF EXISTS `contact_person_edit_log`;
DROP TRIGGER IF EXISTS `country_add_log`;
DROP TRIGGER IF EXISTS `country_delete_log`;
DROP TRIGGER IF EXISTS `country_edit_log`;
DROP TRIGGER IF EXISTS `currency_add_log`;
DROP TRIGGER IF EXISTS `currency_delete_log`;
DROP TRIGGER IF EXISTS `currency_edit_log`;
DROP TRIGGER IF EXISTS `daily_work_entry_add_log`;
DROP TRIGGER IF EXISTS `daily_work_entry_delete_log`;
DROP TRIGGER IF EXISTS `daily_work_entry_edit_log`;
DROP TRIGGER IF EXISTS `days_add_log`;
DROP TRIGGER IF EXISTS `days_delete_log`;
DROP TRIGGER IF EXISTS `days_edit_log`;
DROP TRIGGER IF EXISTS `default_terms_condition_add_log`;
DROP TRIGGER IF EXISTS `default_terms_condition_delete_log`;
DROP TRIGGER IF EXISTS `default_terms_condition_edit_log`;
DROP TRIGGER IF EXISTS `department_add_log`;
DROP TRIGGER IF EXISTS `department_delete_log`;
DROP TRIGGER IF EXISTS `department_edit_log`;
DROP TRIGGER IF EXISTS `designation_add_log`;
DROP TRIGGER IF EXISTS `designation_delete_log`;
DROP TRIGGER IF EXISTS `designation_edit_log`;
DROP TRIGGER IF EXISTS `dispatch_followup_history_add_log`;
DROP TRIGGER IF EXISTS `dispatch_followup_history_delete_log`;
DROP TRIGGER IF EXISTS `dispatch_followup_history_edit_log`;
DROP TRIGGER IF EXISTS `drawing_number_add_log`;
DROP TRIGGER IF EXISTS `drawing_number_delete_log`;
DROP TRIGGER IF EXISTS `drawing_number_edit_log`;
DROP TRIGGER IF EXISTS `employee_leaves_add_log`;
DROP TRIGGER IF EXISTS `employee_leaves_delete_log`;
DROP TRIGGER IF EXISTS `employee_leaves_edit_log`;
DROP TRIGGER IF EXISTS `erection_commissioning_add_log`;
DROP TRIGGER IF EXISTS `erection_commissioning_delete_log`;
DROP TRIGGER IF EXISTS `erection_commissioning_edit_log`;
DROP TRIGGER IF EXISTS `excise_add_log`;
DROP TRIGGER IF EXISTS `excise_delete_log`;
DROP TRIGGER IF EXISTS `excise_edit_log`;
DROP TRIGGER IF EXISTS `followup_history_add_log`;
DROP TRIGGER IF EXISTS `followup_history_delete_log`;
DROP TRIGGER IF EXISTS `followup_history_edit_log`;
DROP TRIGGER IF EXISTS `followup_order_history_add_log`;
DROP TRIGGER IF EXISTS `followup_order_history_delete_log`;
DROP TRIGGER IF EXISTS `followup_order_history_edit_log`;
DROP TRIGGER IF EXISTS `for_required_add_log`;
DROP TRIGGER IF EXISTS `for_required_delete_log`;
DROP TRIGGER IF EXISTS `for_required_edit_log`;
DROP TRIGGER IF EXISTS `foundation_drawing_required_add_log`;
DROP TRIGGER IF EXISTS `foundation_drawing_required_delete_log`;
DROP TRIGGER IF EXISTS `foundation_drawing_required_edit_log`;
DROP TRIGGER IF EXISTS `google_sheet_staff_add_log`;
DROP TRIGGER IF EXISTS `google_sheet_staff_delete_log`;
DROP TRIGGER IF EXISTS `google_sheet_staff_edit_log`;
DROP TRIGGER IF EXISTS `google_sheets_add_log`;
DROP TRIGGER IF EXISTS `google_sheets_delete_log`;
DROP TRIGGER IF EXISTS `google_sheets_edit_log`;
DROP TRIGGER IF EXISTS `grade_add_log`;
DROP TRIGGER IF EXISTS `grade_delete_log`;
DROP TRIGGER IF EXISTS `grade_edit_log`;
DROP TRIGGER IF EXISTS `inbox_add_log`;
DROP TRIGGER IF EXISTS `inbox_delete_log`;
DROP TRIGGER IF EXISTS `inbox_edit_log`;
DROP TRIGGER IF EXISTS `industry_type_add_log`;
DROP TRIGGER IF EXISTS `industry_type_delete_log`;
DROP TRIGGER IF EXISTS `industry_type_edit_log`;
DROP TRIGGER IF EXISTS `inquiry_add_log`;
DROP TRIGGER IF EXISTS `inquiry_confirmation_add_log`;
DROP TRIGGER IF EXISTS `inquiry_confirmation_delete_log`;
DROP TRIGGER IF EXISTS `inquiry_confirmation_edit_log`;
DROP TRIGGER IF EXISTS `inquiry_delete_log`;
DROP TRIGGER IF EXISTS `inquiry_edit_log`;
DROP TRIGGER IF EXISTS `inquiry_followup_history_add_log`;
DROP TRIGGER IF EXISTS `inquiry_followup_history_delete_log`;
DROP TRIGGER IF EXISTS `inquiry_followup_history_edit_log`;
DROP TRIGGER IF EXISTS `inquiry_item_add_log`;
DROP TRIGGER IF EXISTS `inquiry_item_delete_log`;
DROP TRIGGER IF EXISTS `inquiry_item_edit_log`;
DROP TRIGGER IF EXISTS `inquiry_stage_add_log`;
DROP TRIGGER IF EXISTS `inquiry_stage_delete_log`;
DROP TRIGGER IF EXISTS `inquiry_stage_edit_log`;
DROP TRIGGER IF EXISTS `inquiry_status_add_log`;
DROP TRIGGER IF EXISTS `inquiry_status_delete_log`;
DROP TRIGGER IF EXISTS `inquiry_status_edit_log`;
DROP TRIGGER IF EXISTS `inspection_required_add_log`;
DROP TRIGGER IF EXISTS `inspection_required_delete_log`;
DROP TRIGGER IF EXISTS `inspection_required_edit_log`;
DROP TRIGGER IF EXISTS `interview_add_log`;
DROP TRIGGER IF EXISTS `interview_delete_log`;
DROP TRIGGER IF EXISTS `interview_edit_log`;
DROP TRIGGER IF EXISTS `invoice_billing_terms_add_log`;
DROP TRIGGER IF EXISTS `invoice_billing_terms_delete_log`;
DROP TRIGGER IF EXISTS `invoice_billing_terms_edit_log`;
DROP TRIGGER IF EXISTS `invoice_cal_code_definition_add_log`;
DROP TRIGGER IF EXISTS `invoice_cal_code_definition_delete_log`;
DROP TRIGGER IF EXISTS `invoice_cal_code_definition_edit_log`;
DROP TRIGGER IF EXISTS `invoice_items_add_log`;
DROP TRIGGER IF EXISTS `invoice_items_delete_log`;
DROP TRIGGER IF EXISTS `invoice_items_edit_log`;
DROP TRIGGER IF EXISTS `invoice_logins_add_log`;
DROP TRIGGER IF EXISTS `invoice_logins_delete_log`;
DROP TRIGGER IF EXISTS `invoice_logins_edit_log`;
DROP TRIGGER IF EXISTS `invoice_type_add_log`;
DROP TRIGGER IF EXISTS `invoice_type_delete_log`;
DROP TRIGGER IF EXISTS `invoice_type_edit_log`;
DROP TRIGGER IF EXISTS `invoices_add_log`;
DROP TRIGGER IF EXISTS `invoices_delete_log`;
DROP TRIGGER IF EXISTS `invoices_edit_log`;
DROP TRIGGER IF EXISTS `item_bom_add_log`;
DROP TRIGGER IF EXISTS `item_bom_delete_log`;
DROP TRIGGER IF EXISTS `item_bom_edit_log`;
DROP TRIGGER IF EXISTS `item_category_add_log`;
DROP TRIGGER IF EXISTS `item_category_delete_log`;
DROP TRIGGER IF EXISTS `item_category_edit_log`;
DROP TRIGGER IF EXISTS `item_class_add_log`;
DROP TRIGGER IF EXISTS `item_class_delete_log`;
DROP TRIGGER IF EXISTS `item_class_edit_log`;
DROP TRIGGER IF EXISTS `item_details_add_log`;
DROP TRIGGER IF EXISTS `item_details_delete_log`;
DROP TRIGGER IF EXISTS `item_details_edit_log`;
DROP TRIGGER IF EXISTS `item_documents_add_log`;
DROP TRIGGER IF EXISTS `item_documents_delete_log`;
DROP TRIGGER IF EXISTS `item_documents_edit_log`;
DROP TRIGGER IF EXISTS `item_files_add_log`;
DROP TRIGGER IF EXISTS `item_files_delete_log`;
DROP TRIGGER IF EXISTS `item_files_edit_log`;
DROP TRIGGER IF EXISTS `item_group_code_add_log`;
DROP TRIGGER IF EXISTS `item_group_code_delete_log`;
DROP TRIGGER IF EXISTS `item_group_code_edit_log`;
DROP TRIGGER IF EXISTS `item_make_add_log`;
DROP TRIGGER IF EXISTS `item_make_delete_log`;
DROP TRIGGER IF EXISTS `item_make_edit_log`;
DROP TRIGGER IF EXISTS `item_status_add_log`;
DROP TRIGGER IF EXISTS `item_status_delete_log`;
DROP TRIGGER IF EXISTS `item_status_edit_log`;
DROP TRIGGER IF EXISTS `item_type_add_log`;
DROP TRIGGER IF EXISTS `item_type_delete_log`;
DROP TRIGGER IF EXISTS `item_type_edit_log`;
DROP TRIGGER IF EXISTS `items_add_log`;
DROP TRIGGER IF EXISTS `items_delete_log`;
DROP TRIGGER IF EXISTS `items_edit_log`;
DROP TRIGGER IF EXISTS `lead_contact_person_add_log`;
DROP TRIGGER IF EXISTS `lead_contact_person_delete_log`;
DROP TRIGGER IF EXISTS `lead_contact_person_edit_log`;
DROP TRIGGER IF EXISTS `lead_details_add_log`;
DROP TRIGGER IF EXISTS `lead_details_delete_log`;
DROP TRIGGER IF EXISTS `lead_details_edit_log`;
DROP TRIGGER IF EXISTS `lead_provider_add_log`;
DROP TRIGGER IF EXISTS `lead_provider_delete_log`;
DROP TRIGGER IF EXISTS `lead_provider_edit_log`;
DROP TRIGGER IF EXISTS `lead_source_add_log`;
DROP TRIGGER IF EXISTS `lead_source_delete_log`;
DROP TRIGGER IF EXISTS `lead_source_edit_log`;
DROP TRIGGER IF EXISTS `lead_stage_add_log`;
DROP TRIGGER IF EXISTS `lead_stage_delete_log`;
DROP TRIGGER IF EXISTS `lead_stage_edit_log`;
DROP TRIGGER IF EXISTS `lead_status_add_log`;
DROP TRIGGER IF EXISTS `lead_status_delete_log`;
DROP TRIGGER IF EXISTS `lead_status_edit_log`;
DROP TRIGGER IF EXISTS `leave_for_employee_add_log`;
DROP TRIGGER IF EXISTS `leave_for_employee_delete_log`;
DROP TRIGGER IF EXISTS `leave_for_employee_edit_log`;
DROP TRIGGER IF EXISTS `leaves_add_log`;
DROP TRIGGER IF EXISTS `leaves_delete_log`;
DROP TRIGGER IF EXISTS `leaves_edit_log`;
DROP TRIGGER IF EXISTS `letters_add_log`;
DROP TRIGGER IF EXISTS `letters_delete_log`;
DROP TRIGGER IF EXISTS `letters_edit_log`;
DROP TRIGGER IF EXISTS `loading_by_add_log`;
DROP TRIGGER IF EXISTS `loading_by_delete_log`;
DROP TRIGGER IF EXISTS `loading_by_edit_log`;
DROP TRIGGER IF EXISTS `mail_system_add_log`;
DROP TRIGGER IF EXISTS `mail_system_delete_log`;
DROP TRIGGER IF EXISTS `mail_system_edit_log`;
DROP TRIGGER IF EXISTS `mail_system_folder_add_log`;
DROP TRIGGER IF EXISTS `mail_system_folder_delete_log`;
DROP TRIGGER IF EXISTS `mail_system_folder_edit_log`;
DROP TRIGGER IF EXISTS `mailbox_folders_add_log`;
DROP TRIGGER IF EXISTS `mailbox_folders_delete_log`;
DROP TRIGGER IF EXISTS `mailbox_folders_edit_log`;
DROP TRIGGER IF EXISTS `mailbox_mails_delete_log`;
DROP TRIGGER IF EXISTS `mailbox_mails_edit_log`;
DROP TRIGGER IF EXISTS `mailbox_settings_add_log`;
DROP TRIGGER IF EXISTS `mailbox_settings_delete_log`;
DROP TRIGGER IF EXISTS `mailbox_settings_edit_log`;
DROP TRIGGER IF EXISTS `main_group_add_log`;
DROP TRIGGER IF EXISTS `main_group_delete_log`;
DROP TRIGGER IF EXISTS `main_group_edit_log`;
DROP TRIGGER IF EXISTS `material_process_type_add_log`;
DROP TRIGGER IF EXISTS `material_process_type_delete_log`;
DROP TRIGGER IF EXISTS `material_process_type_edit_log`;
DROP TRIGGER IF EXISTS `material_specification_add_log`;
DROP TRIGGER IF EXISTS `material_specification_delete_log`;
DROP TRIGGER IF EXISTS `material_specification_edit_log`;
DROP TRIGGER IF EXISTS `module_roles_add_log`;
DROP TRIGGER IF EXISTS `module_roles_delete_log`;
DROP TRIGGER IF EXISTS `module_roles_edit_log`;
DROP TRIGGER IF EXISTS `order_stage_add_log`;
DROP TRIGGER IF EXISTS `order_stage_delete_log`;
DROP TRIGGER IF EXISTS `order_stage_edit_log`;
DROP TRIGGER IF EXISTS `outbox_add_log`;
DROP TRIGGER IF EXISTS `outbox_delete_log`;
DROP TRIGGER IF EXISTS `outbox_edit_log`;
DROP TRIGGER IF EXISTS `party_add_log`;
DROP TRIGGER IF EXISTS `party_delete_log`;
DROP TRIGGER IF EXISTS `party_detail_add_log`;
DROP TRIGGER IF EXISTS `party_detail_delete_log`;
DROP TRIGGER IF EXISTS `party_detail_edit_log`;
DROP TRIGGER IF EXISTS `party_edit_log`;
DROP TRIGGER IF EXISTS `party_old_add_log`;
DROP TRIGGER IF EXISTS `party_old_delete_log`;
DROP TRIGGER IF EXISTS `party_old_edit_log`;
DROP TRIGGER IF EXISTS `party_type_1_add_log`;
DROP TRIGGER IF EXISTS `party_type_1_delete_log`;
DROP TRIGGER IF EXISTS `party_type_1_edit_log`;
DROP TRIGGER IF EXISTS `party_type_2_add_log`;
DROP TRIGGER IF EXISTS `party_type_2_delete_log`;
DROP TRIGGER IF EXISTS `party_type_2_edit_log`;
DROP TRIGGER IF EXISTS `payment_add_log`;
DROP TRIGGER IF EXISTS `payment_delete_log`;
DROP TRIGGER IF EXISTS `payment_edit_log`;
DROP TRIGGER IF EXISTS `priority_add_log`;
DROP TRIGGER IF EXISTS `priority_delete_log`;
DROP TRIGGER IF EXISTS `priority_edit_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_billing_terms_add_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_billing_terms_delete_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_billing_terms_edit_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_cal_code_definition_add_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_cal_code_definition_delete_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_cal_code_definition_edit_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_items_add_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_items_delete_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_items_edit_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_logins_add_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_logins_delete_log`;
DROP TRIGGER IF EXISTS `proforma_invoice_logins_edit_log`;
DROP TRIGGER IF EXISTS `proforma_invoices_delete_log`;
DROP TRIGGER IF EXISTS `project_add_log`;
DROP TRIGGER IF EXISTS `project_delete_log`;
DROP TRIGGER IF EXISTS `project_edit_log`;
DROP TRIGGER IF EXISTS `purchase_invoice_add_log`;
DROP TRIGGER IF EXISTS `purchase_invoice_delete_log`;
DROP TRIGGER IF EXISTS `purchase_invoice_edit_log`;
DROP TRIGGER IF EXISTS `purchase_invoice_items_add_log`;
DROP TRIGGER IF EXISTS `purchase_invoice_items_delete_log`;
DROP TRIGGER IF EXISTS `purchase_invoice_items_edit_log`;
DROP TRIGGER IF EXISTS `purchase_order_add_log`;
DROP TRIGGER IF EXISTS `purchase_order_delete_log`;
DROP TRIGGER IF EXISTS `purchase_order_edit_log`;
DROP TRIGGER IF EXISTS `purchase_order_items_add_log`;
DROP TRIGGER IF EXISTS `purchase_order_items_delete_log`;
DROP TRIGGER IF EXISTS `purchase_order_items_edit_log`;
DROP TRIGGER IF EXISTS `quotation_items_add_log`;
DROP TRIGGER IF EXISTS `quotation_items_delete_log`;
DROP TRIGGER IF EXISTS `quotation_items_edit_log`;
DROP TRIGGER IF EXISTS `quotation_reason_add_log`;
DROP TRIGGER IF EXISTS `quotation_reason_delete_log`;
DROP TRIGGER IF EXISTS `quotation_reason_edit_log`;
DROP TRIGGER IF EXISTS `quotation_stage_add_log`;
DROP TRIGGER IF EXISTS `quotation_stage_delete_log`;
DROP TRIGGER IF EXISTS `quotation_stage_edit_log`;
DROP TRIGGER IF EXISTS `quotation_status_add_log`;
DROP TRIGGER IF EXISTS `quotation_status_delete_log`;
DROP TRIGGER IF EXISTS `quotation_status_edit_log`;
DROP TRIGGER IF EXISTS `quotation_type_add_log`;
DROP TRIGGER IF EXISTS `quotation_type_delete_log`;
DROP TRIGGER IF EXISTS `quotation_type_edit_log`;
DROP TRIGGER IF EXISTS `quotations_add_log`;
DROP TRIGGER IF EXISTS `real_leave_add_log`;
DROP TRIGGER IF EXISTS `real_leave_delete_log`;
DROP TRIGGER IF EXISTS `real_leave_edit_log`;
DROP TRIGGER IF EXISTS `reference_add_log`;
DROP TRIGGER IF EXISTS `reference_delete_log`;
DROP TRIGGER IF EXISTS `reference_edit_log`;
DROP TRIGGER IF EXISTS `reminder_add_log`;
DROP TRIGGER IF EXISTS `reminder_delete_log`;
DROP TRIGGER IF EXISTS `reminder_edit_log`;
DROP TRIGGER IF EXISTS `road_insurance_by_add_log`;
DROP TRIGGER IF EXISTS `road_insurance_by_delete_log`;
DROP TRIGGER IF EXISTS `road_insurance_by_edit_log`;
DROP TRIGGER IF EXISTS `sales_action_add_log`;
DROP TRIGGER IF EXISTS `sales_action_delete_log`;
DROP TRIGGER IF EXISTS `sales_action_edit_log`;
DROP TRIGGER IF EXISTS `sales_add_log`;
DROP TRIGGER IF EXISTS `sales_delete_log`;
DROP TRIGGER IF EXISTS `sales_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_add_log`;
DROP TRIGGER IF EXISTS `sales_order_billing_terms_add_log`;
DROP TRIGGER IF EXISTS `sales_order_billing_terms_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_billing_terms_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_buyers_add_log`;
DROP TRIGGER IF EXISTS `sales_order_buyers_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_buyers_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_cal_code_definition_add_log`;
DROP TRIGGER IF EXISTS `sales_order_cal_code_definition_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_cal_code_definition_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_item_add_log`;
DROP TRIGGER IF EXISTS `sales_order_item_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_item_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_items_add_log`;
DROP TRIGGER IF EXISTS `sales_order_items_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_items_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_logins_add_log`;
DROP TRIGGER IF EXISTS `sales_order_logins_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_logins_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_notes_add_log`;
DROP TRIGGER IF EXISTS `sales_order_notes_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_notes_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_pref_add_log`;
DROP TRIGGER IF EXISTS `sales_order_pref_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_pref_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_stage_add_log`;
DROP TRIGGER IF EXISTS `sales_order_stage_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_stage_edit_log`;
DROP TRIGGER IF EXISTS `sales_order_status_add_log`;
DROP TRIGGER IF EXISTS `sales_order_status_delete_log`;
DROP TRIGGER IF EXISTS `sales_order_status_edit_log`;
DROP TRIGGER IF EXISTS `sales_status_add_log`;
DROP TRIGGER IF EXISTS `sales_status_delete_log`;
DROP TRIGGER IF EXISTS `sales_status_edit_log`;
DROP TRIGGER IF EXISTS `sales_type_add_log`;
DROP TRIGGER IF EXISTS `sales_type_delete_log`;
DROP TRIGGER IF EXISTS `sales_type_edit_log`;
DROP TRIGGER IF EXISTS `service_add_log`;
DROP TRIGGER IF EXISTS `service_delete_log`;
DROP TRIGGER IF EXISTS `service_edit_log`;
DROP TRIGGER IF EXISTS `service_followup_history_add_log`;
DROP TRIGGER IF EXISTS `service_followup_history_delete_log`;
DROP TRIGGER IF EXISTS `service_followup_history_edit_log`;
DROP TRIGGER IF EXISTS `service_items_add_log`;
DROP TRIGGER IF EXISTS `service_items_delete_log`;
DROP TRIGGER IF EXISTS `service_items_edit_log`;
DROP TRIGGER IF EXISTS `staff_add_log`;
DROP TRIGGER IF EXISTS `staff_edit_log`;
DROP TRIGGER IF EXISTS `staff_roles_add_log`;
DROP TRIGGER IF EXISTS `staff_roles_delete_log`;
DROP TRIGGER IF EXISTS `staff_roles_edit_log`;
DROP TRIGGER IF EXISTS `state_add_log`;
DROP TRIGGER IF EXISTS `state_delete_log`;
DROP TRIGGER IF EXISTS `state_edit_log`;
DROP TRIGGER IF EXISTS `sub_group_add_log`;
DROP TRIGGER IF EXISTS `sub_group_delete_log`;
DROP TRIGGER IF EXISTS `sub_group_edit_log`;
DROP TRIGGER IF EXISTS `supplier_add_log`;
DROP TRIGGER IF EXISTS `supplier_delete_log`;
DROP TRIGGER IF EXISTS `supplier_edit_log`;
DROP TRIGGER IF EXISTS `terms_condition_detail_add_log`;
DROP TRIGGER IF EXISTS `terms_condition_detail_delete_log`;
DROP TRIGGER IF EXISTS `terms_condition_detail_edit_log`;
DROP TRIGGER IF EXISTS `terms_condition_group_add_log`;
DROP TRIGGER IF EXISTS `terms_condition_group_delete_log`;
DROP TRIGGER IF EXISTS `terms_condition_group_edit_log`;
DROP TRIGGER IF EXISTS `terms_condition_template_add_log`;
DROP TRIGGER IF EXISTS `terms_condition_template_delete_log`;
DROP TRIGGER IF EXISTS `terms_condition_template_edit_log`;
DROP TRIGGER IF EXISTS `terms_group_add_log`;
DROP TRIGGER IF EXISTS `terms_group_delete_log`;
DROP TRIGGER IF EXISTS `terms_group_edit_log`;
DROP TRIGGER IF EXISTS `transportation_by_add_log`;
DROP TRIGGER IF EXISTS `transportation_by_delete_log`;
DROP TRIGGER IF EXISTS `transportation_by_edit_log`;
DROP TRIGGER IF EXISTS `unloading_by_add_log`;
DROP TRIGGER IF EXISTS `unloading_by_delete_log`;
DROP TRIGGER IF EXISTS `unloading_by_edit_log`;
DROP TRIGGER IF EXISTS `uom_add_log`;
DROP TRIGGER IF EXISTS `uom_delete_log`;
DROP TRIGGER IF EXISTS `uom_edit_log`;
DROP TRIGGER IF EXISTS `visitors_add_log`;
DROP TRIGGER IF EXISTS `visitors_delete_log`;
DROP TRIGGER IF EXISTS `visitors_edit_log`;
DROP TRIGGER IF EXISTS `website_modules_add_log`;
DROP TRIGGER IF EXISTS `website_modules_delete_log`;
DROP TRIGGER IF EXISTS `website_modules_edit_log`;
DROP TRIGGER IF EXISTS `weekly_holiday_add_log`;
DROP TRIGGER IF EXISTS `weekly_holiday_delete_log`;
DROP TRIGGER IF EXISTS `weekly_holiday_edit_log`;
DROP TRIGGER IF EXISTS `yearly_leaves_add_log`;
DROP TRIGGER IF EXISTS `yearly_leaves_delete_log`;
DROP TRIGGER IF EXISTS `yearly_leaves_edit_log`;

CREATE TRIGGER `agent_add_log` AFTER INSERT ON `agent`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.agent_log
SET
	`id`=new.id,
    `party_type_1`=new.party_type_1,
    `address`=new.address,
    `company_name`=new.company_name,
    `phone_no`=new.phone_no,
    `email`=new.email,
    `note`=new.note,
    `fix_commission`=new.fix_commission,
    `agent_name`=new.agent_name,
    `commission`=new.commission,
    `created_by`=new.created_by,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add';

CREATE TRIGGER `agent_delete_log` BEFORE DELETE ON `agent`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.agent_log
SET
`id`=old.id,
`party_type_1`=old.party_type_1,
`address`=old.address,
`company_name`=old.company_name,
`phone_no`=old.phone_no,
`email`=old.email,
`note`=old.note,
`fix_commission`=old.fix_commission,
`agent_name`=old.agent_name,
`commission`=old.commission,
`created_by`=old.created_by,
`created_at`=old.created_at,
`updated_at`=old.updated_at,
`operation_type`='delete';

CREATE TRIGGER `agent_edit_log` AFTER UPDATE ON `agent`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.agent_log
SET
	`id`=new.id,
    `party_type_1`=new.party_type_1,
    `address`=new.address,
    `company_name`=new.company_name,
    `phone_no`=new.phone_no,
    `email`=new.email,
    `note`=new.note,
    `fix_commission`=new.fix_commission,
    `agent_name`=new.agent_name,
    `commission`=new.commission,
    `created_by`=new.created_by,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit';

CREATE TRIGGER `billing_template_add_log` AFTER INSERT ON `billing_template`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_template_log
SET
	`id`=new.id,
    `template`=new.template,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add';

CREATE TRIGGER `billing_template_edit_log` AFTER UPDATE ON `billing_template`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_template_log
SET
	`id`=new.id,
    `template`=new.template,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit';

CREATE TRIGGER `billing_template_delete_log` AFTER DELETE ON `billing_template`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_template_log
SET
	`id`=old.id,
    `template`=old.template,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='add';

CREATE TRIGGER `billing_terms_add_log` AFTER INSERT ON `billing_terms`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_log
SET
	`id`=new.id,
    `billing_terms_name`=new.billing_terms_name,
    `billing_terms_date`=new.billing_terms_date,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add';

CREATE TRIGGER `billing_terms_delete_log` AFTER DELETE ON `billing_terms`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_log
SET
	`id`=old.id,
    `billing_terms_name`=old.billing_terms_name,
    `billing_terms_date`=old.billing_terms_date,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete';

CREATE TRIGGER `billing_terms_detail_add_log` AFTER INSERT ON `billing_terms_detail`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.billing_terms_detail_log
SET
`id`=new.id,
`billing_terms_id`=new.billing_terms_id,
`cal_code`=new.cal_code,
`narration`=new.narration,
`cal_definition`=new.cal_definition,
`percentage`=new.percentage,
`value`=new.value,
`gl_acc`=new.gl_acc,
`created_at`=new.created_at,
`updated_at`=new.updated_at,
`operation_type`='add';

CREATE TRIGGER `billing_terms_detail_delete_log` AFTER DELETE ON `billing_terms_detail`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_detail_log
SET
	`id`=old.id,
    `billing_terms_id`=old.billing_terms_id,
    `cal_code`=old.cal_code,
    `narration`=old.narration,
    `cal_definition`=old.cal_definition,
    `percentage`=old.percentage,
    `value`=old.value,
    `gl_acc`=old.gl_acc,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete';

CREATE TRIGGER `billing_terms_detail_edit_log` AFTER UPDATE ON `billing_terms_detail`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.billing_terms_detail_log
SET
`id`=new.id,
`billing_terms_id`=new.billing_terms_id,
`cal_code`=new.cal_code,
`narration`=new.narration,
`cal_definition`=new.cal_definition,
`percentage`=new.percentage,
`value`=new.value,
`gl_acc`=new.gl_acc,
`created_at`=new.created_at,
`updated_at`=new.updated_at,
`operation_type`='edit';

CREATE TRIGGER `cal_code_definition_add_log` AFTER INSERT ON `cal_code_definition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.cal_code_definition_log
SET
	`id`=new.id,
    `billing_terms_detail_id`=new.billing_terms_detail_id,
    `is_first_element`=new.is_first_element,
    `cal_operation`=new.cal_operation,
    `cal_code_id`=new.cal_code_id,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add';

CREATE TRIGGER `cal_code_definition_delete_log` AFTER DELETE ON `cal_code_definition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.cal_code_definition_log
SET
	`id`=old.id,
    `billing_terms_detail_id`=old.billing_terms_detail_id,
    `is_first_element`=old.is_first_element,
    `cal_operation`=old.cal_operation,
    `cal_code_id`=old.cal_code_id,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete';

CREATE TRIGGER `billing_terms_edit_log` AFTER UPDATE ON `billing_terms`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.billing_terms_log
SET
	`id`=new.id,
    `billing_terms_name`=new.billing_terms_name,
    `billing_terms_date`=new.billing_terms_date,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit';

CREATE TRIGGER `branch_delete_log` AFTER DELETE ON `branch`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.branch_log
SET
	`branch_id`=old.branch_id,
    `branch_code`=old.branch_code,
    `branch`=old.branch,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete';

CREATE TRIGGER `challan_items_delete_log` AFTER DELETE ON `challan_items`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challan_items_log
SET
	`id`=old.id,
	`item_id`=old.item_id,
	`challan_id`=old.challan_id,
	`item_code`=old.item_code,
	`item_category_id`=old.item_category_id,
	`item_name`=old.item_name,
	`item_desc`=old.item_desc,
	`item_description`=old.item_description,
	`add_description`=old.add_description,
	`detail_description`=old.detail_description,
	`drawing_number`=old.drawing_number,
	`drawing_revision`=old.drawing_revision,
	`uom_id`=old.uom_id,
	`uom`=old.uom,
	`total_amount`=old.total_amount,
	`item_note`=old.item_note,
	`quantity`=old.quantity,
	`disc_per`=old.disc_per,
	`rate`=old.rate,
	`disc_value`=old.disc_value,
	`amount`=old.amount,
	`net_amount`=old.net_amount,
	`item_status_id`=old.item_status_id,
	`item_status`=old.item_status,
	`cust_part_no`=old.cust_part_no,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `challan_items_edit_log` AFTER UPDATE ON `challan_items`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challan_items_log
SET
	`id`=new.id,
	`item_id`=new.item_id,
	`challan_id`=new.challan_id,
	`item_code`=new.item_code,
	`item_category_id`=new.item_category_id,
	`item_name`=new.item_name,
	`item_desc`=new.item_desc,
	`item_description`=new.item_description,
	`add_description`=new.add_description,
	`detail_description`=new.detail_description,
	`drawing_number`=new.drawing_number,
	`drawing_revision`=new.drawing_revision,
	`uom_id`=new.uom_id,
	`uom`=new.uom,
	`total_amount`=new.total_amount,
	`item_note`=new.item_note,
	`quantity`=new.quantity,
	`disc_per`=new.disc_per,
	`rate`=new.rate,
	`disc_value`=new.disc_value,
	`amount`=new.amount,
	`net_amount`=new.net_amount,
	`item_status_id`=new.item_status_id,
	`item_status`=new.item_status,
	`cust_part_no`=new.cust_part_no,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `branch_add_log` AFTER INSERT ON `branch`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.branch_log
SET
	`branch_id`=new.branch_id,
    `branch_code`=new.branch_code,
    `branch`=new.branch,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add';

CREATE TRIGGER `challans_add_log` AFTER INSERT ON `challans`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challans_log
SET
	`id`=new.id,
	`sales_order_id`=new.sales_order_id,
	`sales_to_party_id`=new.sales_to_party_id,
	`truck_no`=new.truck_no,
	`gear_no_5`=new.gear_no_5,
	`gear_no_3_54`=new.gear_no_3_54,
	`lr_no`=new.lr_no,
	`lr_date` =new.lr_date,
	`trademark_name`=new.trademark_name,
	`full_load_current`=new.full_load_current,
	`serial_no`=new.serial_no,
	`serial_no_1`=new.serial_no_1,
	`serial_no_2`=new.serial_no_2,
	`serial_no_3`=new.serial_no_3,
	`serial_no_4`=new.serial_no_4,
	`motor_75_hp`=new.motor_75_hp,
	`motor_50_hp`=new.motor_50_hp,
	`rated_voltage`=new.rated_voltage,
	`phases_no`=new.phases_no,
	`short_circuit_rating`=new.short_circuit_rating,
	`capacity`=new.capacity,
	`operating_manual_reference_no`=new.operating_manual_reference_no,
	`gross_weight`=new.gross_weight,
	`manufacturing_monthYear`=new.manufacturing_monthYear,
	`challan_date` =new.challan_date,
	`transport_name`=new.transport_name,
	`transport_charges`  =new.transport_charges,
	`created_by`=new.created_by,
	`created_at` =new.created_at,
	`updated_by`=new.updated_by,
	`updated_at` =new.updated_at,
	`rounding`=new.rounding,
	`challan_no`=new.challan_no,
	`operation_type`='add';

CREATE TRIGGER `challans_edit_log` AFTER UPDATE ON `challans`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challans_log
SET
	`id`=new.id,
	`sales_order_id`=new.sales_order_id,
	`sales_to_party_id`=new.sales_to_party_id,
	`truck_no`=new.truck_no,
	`gear_no_5`=new.gear_no_5,
	`gear_no_3_54`=new.gear_no_3_54,
	`lr_no`=new.lr_no,
	`lr_date` =new.lr_date,
	`trademark_name`=new.trademark_name,
	`full_load_current`=new.full_load_current,
	`serial_no`=new.serial_no,
	`serial_no_1`=new.serial_no_1,
	`serial_no_2`=new.serial_no_2,
	`serial_no_3`=new.serial_no_3,
	`serial_no_4`=new.serial_no_4,
	`motor_75_hp`=new.motor_75_hp,
	`motor_50_hp`=new.motor_50_hp,
	`rated_voltage`=new.rated_voltage,
	`phases_no`=new.phases_no,
	`short_circuit_rating`=new.short_circuit_rating,
	`capacity`=new.capacity,
	`operating_manual_reference_no`=new.operating_manual_reference_no,
	`gross_weight`=new.gross_weight,
	`manufacturing_monthYear`=new.manufacturing_monthYear,
	`challan_date` =new.challan_date,
	`transport_name`=new.transport_name,
	`transport_charges`  =new.transport_charges,
	`created_by`=new.created_by,
	`created_at` =new.created_at,
	`updated_by`=new.updated_by,
	`updated_at` =new.updated_at,
	`rounding`=new.rounding,
	`challan_no`=new.challan_no,
	`operation_type`='edit';

CREATE TRIGGER `branch_edit_log` AFTER UPDATE ON `branch`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.branch_log
SET
	`branch_id`=new.branch_id,
    `branch_code`=new.branch_code,
    `branch`=new.branch,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit';

CREATE TRIGGER `challan_items_add_log` AFTER INSERT ON `challan_items`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challan_items_log
SET
	`id`=new.id,
	`item_id`=new.item_id,
	`challan_id`=new.challan_id,
	`item_code`=new.item_code,
	`item_category_id`=new.item_category_id,
	`item_name`=new.item_name,
	`item_desc`=new.item_desc,
	`item_description`=new.item_description,
	`add_description`=new.add_description,
	`detail_description`=new.detail_description,
	`drawing_number`=new.drawing_number,
	`drawing_revision`=new.drawing_revision,
	`uom_id`=new.uom_id,
	`uom`=new.uom,
	`total_amount`=new.total_amount,
	`item_note`=new.item_note,
	`quantity`=new.quantity,
	`disc_per`=new.disc_per,
	`rate`=new.rate,
	`disc_value`=new.disc_value,
	`amount`=new.amount,
	`net_amount`=new.net_amount,
	`item_status_id`=new.item_status_id,
	`item_status`=new.item_status,
	`cust_part_no`=new.cust_part_no,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `cal_code_definition_edit_log` AFTER UPDATE ON `cal_code_definition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.cal_code_definition_log
SET
	`id`=new.id,
    `billing_terms_detail_id`=new.billing_terms_detail_id,
    `is_first_element`=new.is_first_element,
    `cal_operation`=new.cal_operation,
    `cal_code_id`=new.cal_code_id,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit';

CREATE TRIGGER `chat_roles_delete_log` AFTER DELETE ON `chat_roles`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.chat_roles_log
SET
	`id`=old.id,
    `staff_id`=old.staff_id,
    `allowed_staff_id`=old.allowed_staff_id,
    `created_at`=old.created_at,
    `operation_type`='delete';

CREATE TRIGGER `chat_roles_add_log` AFTER INSERT ON `chat_roles`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.chat_roles_log
SET
	`id`=new.id,
    `staff_id`=new.staff_id,
    `allowed_staff_id`=new.allowed_staff_id,
    `created_at`=new.created_at,
    `operation_type`='add';

CREATE TRIGGER `challans_delete_log` AFTER DELETE ON `challans`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.challans_log
SET
	`id`=old.id,
	`sales_order_id`=old.sales_order_id,
	`sales_to_party_id`=old.sales_to_party_id,
	`truck_no`=old.truck_no,
	`gear_no_5`=old.gear_no_5,
	`gear_no_3_54`=old.gear_no_3_54,
	`lr_no`=old.lr_no,
	`lr_date` =old.lr_date,
	`trademark_name`=old.trademark_name,
	`full_load_current`=old.full_load_current,
	`serial_no`=old.serial_no,
	`serial_no_1`=old.serial_no_1,
	`serial_no_2`=old.serial_no_2,
	`serial_no_3`=old.serial_no_3,
	`serial_no_4`=old.serial_no_4,
	`motor_75_hp`=old.motor_75_hp,
	`motor_50_hp`=old.motor_50_hp,
	`rated_voltage`=old.rated_voltage,
	`phases_no`=old.phases_no,
	`short_circuit_rating`=old.short_circuit_rating,
	`capacity`=old.capacity,
	`operating_manual_reference_no`=old.operating_manual_reference_no,
	`gross_weight`=old.gross_weight,
	`manufacturing_monthYear`=old.manufacturing_monthYear,
	`challan_date` =old.challan_date,
	`transport_name`=old.transport_name,
	`transport_charges`  =old.transport_charges,
	`created_by`=old.created_by,
	`created_at` =old.created_at,
	`updated_by`=old.updated_by,
	`updated_at` =old.updated_at,
	`rounding`=old.rounding,
	`challan_no`=old.challan_no,
	`operation_type`='delete';

CREATE TRIGGER `city_edit_log` AFTER UPDATE ON `city`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.city_log
SET
	`city_id`=new.city_id,
    `state_id`=new.state_id,
    `country_id`=new.country_id,
    `city`=new.city,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='edit';

CREATE TRIGGER `company_delete_log` AFTER DELETE ON `company`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.company_log
SET
	`id`=old.id,
	`name`=old.name,
	`ceo_name`=old.ceo_name,
	`m_d_name`=old.m_d_name,
	`address`=old.address,
	`city` =old.city,
	`state` =old.state,
	`pincode` =old.pincode,
	`country` =old.country,
	`contact_no`=old.contact_no,
	`cell_no` =old.cell_no,
	`fax_no` =old.fax_no,
	`email_id` =old.email_id,
	`tin_no` =old.tin_no,
	`pan_no` =old.pan_no,
	`tan_no` =old.tan_no,
	`pf_no` =old.pf_no,
	`esic_no` =old.esic_no,
	`pt_no` =old.pt_no,
	`service_tax_no`=old.service_tax_no, 
	`vat_no` =old.vat_no,
	`cst_no` =old.cst_no,
	`code`=old.code,
	`bank_domestic`=old.bank_domestic,
	`bank_export`=old.bank_export,
	`margin_left`=old.margin_left,
	`margin_right`=old.margin_right,
	`margin_top`=old.margin_top,
	`margin_bottom`=old.margin_bottom,
	`invoice_terms`=old.invoice_terms,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `city_add_log` AFTER INSERT ON `city`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.city_log
SET
	`city_id`=new.city_id,
    `state_id`=new.state_id,
    `country_id`=new.country_id,
    `city`=new.city,
    `created_at`=new.created_at,
    `updated_at`=new.updated_at,
    `operation_type`='add';

CREATE TRIGGER `chat_roles_edit_log` AFTER UPDATE ON `chat_roles`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.chat_roles_log
SET
	`id`=new.id,
    `staff_id`=new.staff_id,
    `allowed_staff_id`=new.allowed_staff_id,
    `created_at`=new.created_at,
    `operation_type`='edit';

CREATE TRIGGER `city_delete_log` AFTER DELETE ON `city`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.city_log
SET
	`city_id`=old.city_id,
    `state_id`=old.state_id,
    `country_id`=old.country_id,
    `city`=old.city,
    `created_at`=old.created_at,
    `updated_at`=old.updated_at,
    `operation_type`='delete';

CREATE TRIGGER `company_add_log` AFTER INSERT ON `company`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.company_log
SET
	`id`=new.id,
	`name`=new.name,
	`ceo_name`=new.ceo_name,
	`m_d_name`=new.m_d_name,
	`address`=new.address,
	`city` =new.city,
	`state` =new.state,
	`pincode` =new.pincode,
	`country` =new.country,
	`contact_no`=new.contact_no,
	`cell_no` =new.cell_no,
	`fax_no` =new.fax_no,
	`email_id` =new.email_id,
	`tin_no` =new.tin_no,
	`pan_no` =new.pan_no,
	`tan_no` =new.tan_no,
	`pf_no` =new.pf_no,
	`esic_no` =new.esic_no,
	`pt_no` =new.pt_no,
	`service_tax_no`=new.service_tax_no, 
	`vat_no` =new.vat_no,
	`cst_no` =new.cst_no,
	`code`=new.code,
	`bank_domestic`=new.bank_domestic,
	`bank_export`=new.bank_export,
	`margin_left`=new.margin_left,
	`margin_right`=new.margin_right,
	`margin_top`=new.margin_top,
	`margin_bottom`=new.margin_bottom,
	`invoice_terms`=new.invoice_terms,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `config_add_log` AFTER INSERT ON `config`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.config_log
SET
	`id`=NEW.id,
	`config_key`=NEW.config_key,
	`config_value`=NEW.config_value,
	`operation_type`='add';

CREATE TRIGGER `contact_person_add_log` AFTER INSERT ON `contact_person`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_log
SET
	`contact_person_id`=new.contact_person_id,
	`party_id`=new.party_id, 
	`active`=new.active,
	`name`=new.name,
	`phone_no`=new.phone_no,
	`mobile_no`=new.mobile_no,
	`fax_no`=new.fax_no,
	`email`=new.email,
	`designation_id`=new.designation_id,
	`department_id`=new.department_id,
	`note`=new.note,
	`priority`=new.priority,
	`created_by`=new.created_by,
	`updated_by`=new.updated_by,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `contact_person_delete_log` AFTER DELETE ON `contact_person`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_log
SET
	`contact_person_id`=old.contact_person_id,
	`party_id`=old.party_id, 
	`active`=old.active,
	`name`=old.name,
	`phone_no`=old.phone_no,
	`mobile_no`=old.mobile_no,
	`fax_no`=old.fax_no,
	`email`=old.email,
	`designation_id`=old.designation_id,
	`department_id`=old.department_id,
	`note`=old.note,
	`priority`=old.priority,
	`created_by`=old.created_by,
	`updated_by`=old.updated_by,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `country_add_log` AFTER INSERT ON `country`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.country_log
SET
	`country_id`=new.country_id,
	`country`=new.country,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `config_delete_log` AFTER DELETE ON `config`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.config_log
SET
	
	`id`=OLD.id,
	`config_key`=OLD.config_key,
	`config_value`=OLD.config_value,
	`operation_type`='delete';

CREATE TRIGGER `country_delete_log` AFTER DELETE ON `country`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.country_log
SET
	`country_id`=old.country_id,
	`country`=old.country,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `config_edit_log` AFTER UPDATE ON `config`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.config_log
SET
	
	`id`=NEW.id,
	`config_key`=NEW.config_key,
	`config_value`=NEW.config_value,
	`operation_type`='edit';

CREATE TRIGGER `company_edit_log` AFTER UPDATE ON `company`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.company_log
SET
	`id`=new.id,
	`name`=new.name,
	`ceo_name`=new.ceo_name,
	`m_d_name`=new.m_d_name,
	`address`=new.address,
	`city` =new.city,
	`state` =new.state,
	`pincode` =new.pincode,
	`country` =new.country,
	`contact_no`=new.contact_no,
	`cell_no` =new.cell_no,
	`fax_no` =new.fax_no,
	`email_id` =new.email_id,
	`tin_no` =new.tin_no,
	`pan_no` =new.pan_no,
	`tan_no` =new.tan_no,
	`pf_no` =new.pf_no,
	`esic_no` =new.esic_no,
	`pt_no` =new.pt_no,
	`service_tax_no`=new.service_tax_no, 
	`vat_no` =new.vat_no,
	`cst_no` =new.cst_no,
	`code`=new.code,
	`bank_domestic`=new.bank_domestic,
	`bank_export`=new.bank_export,
	`margin_left`=new.margin_left,
	`margin_right`=new.margin_right,
	`margin_top`=new.margin_top,
	`margin_bottom`=new.margin_bottom,
	`invoice_terms`=new.invoice_terms,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `contact_person_edit_log` AFTER UPDATE ON `contact_person`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.contact_person_log
SET
	`contact_person_id`=new.contact_person_id,
	`party_id`=new.party_id, 
	`active`=new.active,
	`name`=new.name,
	`phone_no`=new.phone_no,
	`mobile_no`=new.mobile_no,
	`fax_no`=new.fax_no,
	`email`=new.email,
	`designation_id`=new.designation_id,
	`department_id`=new.department_id,
	`note`=new.note,
	`priority`=new.priority,
	`created_by`=new.created_by,
	`updated_by`=new.updated_by,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `country_edit_log` AFTER UPDATE ON `country`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.country_log
SET
	`country_id`=new.country_id,
	`country`=new.country,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `currency_delete_log` AFTER DELETE ON `currency`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.currency_log
SET
	`id`=old.id,
	`currency`=old.currency,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `daily_work_entry_add_log` AFTER INSERT ON `daily_work_entry`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.daily_work_entry_log
SET
	`id`=new.id,
	`userid`=new.userid,
	`username`=new.username,
	`entry_date`=new.entry_date,
	`activity_details`=new.activity_details,
	`created_date`=new.created_date,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `daily_work_entry_delete_log` AFTER DELETE ON `daily_work_entry`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.daily_work_entry_log
SET
	`id`=old.id,
	`userid`=old.userid,
	`username`=old.username,
	`entry_date`=old.entry_date,
	`activity_details`=old.activity_details,
	`created_date`=old.created_date,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `currency_edit_log` AFTER UPDATE ON `currency`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.currency_log
SET
	`id`=new.id,
	`currency`=new.currency,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `days_add_log` AFTER INSERT ON `days`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.days_log
SET
	`day_id`=new.day_id,
	`day_name`=new.day_name,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `default_terms_condition_add_log` AFTER INSERT ON `default_terms_condition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.default_terms_condition_log
SET
	`id`=new.id,
	`template_name`=new.template_name,
	`group_name`=new.group_name,
	`terms_condition`=new.terms_condition,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `default_terms_condition_delete_log` AFTER DELETE ON `default_terms_condition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.default_terms_condition_log
SET
	`id`=old.id,
	`template_name`=old.template_name,
	`group_name`=old.group_name,
	`terms_condition`=old.terms_condition,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `daily_work_entry_edit_log` AFTER UPDATE ON `daily_work_entry`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.daily_work_entry_log
SET
	`id`=new.id,
	`userid`=new.userid,
	`username`=new.username,
	`entry_date`=new.entry_date,
	`activity_details`=new.activity_details,
	`created_date`=new.created_date,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `default_terms_condition_edit_log` AFTER UPDATE ON `default_terms_condition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.default_terms_condition_log
SET
	`id`=new.id,
	`template_name`=new.template_name,
	`group_name`=new.group_name,
	`terms_condition`=new.terms_condition,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `days_edit_log` AFTER UPDATE ON `days`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.days_log
SET
	`day_id`=new.day_id,
	`day_name`=new.day_name,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `currency_add_log` AFTER INSERT ON `currency`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.currency_log
SET
	`id`=new.id,
	`currency`=new.currency,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `department_edit_log` AFTER UPDATE ON `department`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.department_log
SET
	`department_id`=new.department_id,
	`department`=new.department,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `days_delete_log` AFTER DELETE ON `days`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.days_log
SET
	`day_id`=old.day_id,
	`day_name`=old.day_name,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `department_delete_log` AFTER DELETE ON `department`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.department_log
SET
	`department_id`=old.department_id,
	`department`=old.department,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `designation_add_log` AFTER INSERT ON `designation`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.designation_log
SET
	`designation_id`=new.designation_id,
	`designation`=new.designation,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `dispatch_followup_history_delete_log` AFTER DELETE ON `dispatch_followup_history`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.dispatch_followup_history_log
SET
	`id`=old.id,
	`challan_id` =old.challan_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete';

CREATE TRIGGER `dispatch_followup_history_edit_log` AFTER UPDATE ON `dispatch_followup_history`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.dispatch_followup_history_log
SET
	`id`=new.id,
	`challan_id` =new.challan_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit';

CREATE TRIGGER `drawing_number_add_log` AFTER INSERT ON `drawing_number`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.drawing_number_log
SET
	`id`=new.id,
	`drawing_number`=new.drawing_number,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `designation_delete_log` AFTER DELETE ON `designation`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.designation_log
SET
	`designation_id`=old.designation_id,
	`designation`=old.designation,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `drawing_number_edit_log` AFTER UPDATE ON `drawing_number`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.drawing_number_log
SET
	`id`=new.id,
	`drawing_number`=new.drawing_number,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `department_add_log` AFTER INSERT ON `department`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.department_log
SET
	`department_id`=new.department_id,
	`department`=new.department,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `employee_leaves_delete_log` AFTER DELETE ON `employee_leaves`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.employee_leaves_log
SET
	`leave_id`=old.leave_id,
	`employee_id`=old.employee_id,
	`from_date`=old.from_date,
	`to_date`=old.to_date,
	`day`=old.day,
	`leave_type`=old.leave_type,
	`leave_detail`=old.leave_detail,
	`leave_status`=old.leave_status,
	`leave_note`=old.leave_note,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `designation_edit_log` AFTER UPDATE ON `designation`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.designation_log
SET
	`designation_id`=new.designation_id,
	`designation`=new.designation,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `employee_leaves_add_log` AFTER INSERT ON `employee_leaves`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.employee_leaves_log
SET
	`leave_id`=new.leave_id,
	`employee_id`=new.employee_id,
	`from_date`=new.from_date,
	`to_date`=new.to_date,
	`day`=new.day,
	`leave_type`=new.leave_type,
	`leave_detail`=new.leave_detail,
	`leave_status`=new.leave_status,
	`leave_note`=new.leave_note,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `erection_commissioning_delete_log` AFTER DELETE ON `erection_commissioning`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.erection_commissioning_log
SET
	`id`=old.id,
	`erection_commissioning`=old.erection_commissioning,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `erection_commissioning_edit_log` AFTER UPDATE ON `erection_commissioning`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.erection_commissioning_log
SET
	`id`=new.id,
	`erection_commissioning`=new.erection_commissioning,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `dispatch_followup_history_add_log` AFTER INSERT ON `dispatch_followup_history`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.dispatch_followup_history_log
SET
	`id`=new.id,
	`challan_id` =new.challan_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add';

CREATE TRIGGER `drawing_number_delete_log` AFTER DELETE ON `drawing_number`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.drawing_number_log
SET
	`id`=old.id,
	`drawing_number`=old.drawing_number,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `excise_delete_log` AFTER DELETE ON `excise`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.excise_log
SET
	`id`=old.id,
	`description`=old.description,
	`sub_heading`=old.sub_heading,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `followup_history_add_log` AFTER INSERT ON `followup_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_history_log
SET
	`id`=new.id,
	`quotation_id`=new.quotation_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add';

CREATE TRIGGER `excise_add_log` AFTER INSERT ON `excise`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.excise_log
SET
	`id`=new.id,
	`description`=new.description,
	`sub_heading`=new.sub_heading,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `excise_edit_log` AFTER UPDATE ON `excise`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.excise_log
SET
	`id`=new.id,
	`description`=new.description,
	`sub_heading`=new.sub_heading,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `employee_leaves_edit_log` AFTER UPDATE ON `employee_leaves`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.employee_leaves_log
SET
	`leave_id`=new.leave_id,
	`employee_id`=new.employee_id,
	`from_date`=new.from_date,
	`to_date`=new.to_date,
	`day`=new.day,
	`leave_type`=new.leave_type,
	`leave_detail`=new.leave_detail,
	`leave_status`=new.leave_status,
	`leave_note`=new.leave_note,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `followup_order_history_delete_log` AFTER DELETE ON `followup_order_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_order_history_log
SET
	`id`=old.id,
	`order_id`=old.order_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete';

CREATE TRIGGER `followup_order_history_edit_log` AFTER UPDATE ON `followup_order_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_order_history_log
SET
	`id`=new.id,
	`order_id`=new.order_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit';

CREATE TRIGGER `for_required_add_log` AFTER INSERT ON `for_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.for_required_log
SET
	`id`=new.id,
	`for_required`=new.for_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `for_required_delete_log` AFTER DELETE ON `for_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.for_required_log
SET
	`id`=old.id,
	`for_required`=old.for_required,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `followup_history_edit_log` AFTER UPDATE ON `followup_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_history_log
SET
	`id`=new.id,
	`quotation_id`=new.quotation_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit';

CREATE TRIGGER `followup_history_delete_log` AFTER DELETE ON `followup_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_history_log
SET
	`id`=old.id,
	`quotation_id`=old.quotation_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete';

CREATE TRIGGER `erection_commissioning_add_log` AFTER INSERT ON `erection_commissioning`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.erection_commissioning_log
SET
	`id`=new.id,
	`erection_commissioning`=new.erection_commissioning,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `followup_order_history_add_log` AFTER INSERT ON `followup_order_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.followup_order_history_log
SET
	`id`=new.id,
	`order_id`=new.order_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add';

CREATE TRIGGER `foundation_drawing_required_delete_log` AFTER DELETE ON `foundation_drawing_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.foundation_drawing_required_log
SET
	`id`=old.id,
	`foundation_drawing_required`=old.foundation_drawing_required,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `for_required_edit_log` AFTER UPDATE ON `for_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.for_required_log
SET
	`id`=new.id,
	`for_required`=new.for_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `foundation_drawing_required_edit_log` AFTER UPDATE ON `foundation_drawing_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.foundation_drawing_required_log
SET
	`id`=new.id,
	`foundation_drawing_required`=new.foundation_drawing_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `foundation_drawing_required_add_log` AFTER INSERT ON `foundation_drawing_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.foundation_drawing_required_log
SET
	`id`=new.id,
	`foundation_drawing_required`=new.foundation_drawing_required,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `google_sheet_staff_add_log` AFTER INSERT ON `google_sheet_staff`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheet_staff_log
SET
	`id`=new.id,
	`google_sheet_id`=new.google_sheet_id,
	`staff_id`=new.staff_id,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `google_sheets_delete_log` AFTER DELETE ON `google_sheets`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheets_log
SET
	`id`=old.id,
	`name`=old.name,
	`url`=old.url,
	`sequence`=old.sequence,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `google_sheet_staff_delete_log` AFTER DELETE ON `google_sheet_staff`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheet_staff_log
SET
	`id`=old.id,
	`google_sheet_id`=old.google_sheet_id,
	`staff_id`=old.staff_id,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `google_sheet_staff_edit_log` AFTER UPDATE ON `google_sheet_staff`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheet_staff_log
SET
	`id`=new.id,
	`google_sheet_id`=new.google_sheet_id,
	`staff_id`=new.staff_id,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `grade_delete_log` AFTER DELETE ON `grade`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.grade_log
SET
	`grade_id`=old.grade_id,
	`grade`=old.grade,
	`created_at`=old.created_at,
	`updated_at`=old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `grade_edit_log` AFTER UPDATE ON `grade`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.grade_log
SET
	`grade_id`=new.grade_id,
	`grade`=new.grade,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `google_sheets_edit_log` AFTER UPDATE ON `google_sheets`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheets_log
SET
	`id`=new.id,
	`name`=new.name,
	`url`=new.url,
	`sequence`=new.sequence,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `google_sheets_add_log` AFTER INSERT ON `google_sheets`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.google_sheets_log
SET
	`id`=new.id,
	`name`=new.name,
	`url`=new.url,
	`sequence`=new.sequence,
	`created_at`=new.created_at,
	`updated_at`=new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `grade_add_log` AFTER INSERT ON `grade`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.grade_log
SET
`grade_id`=new.grade_id,
`grade`=new.grade,
`created_at`=new.created_at,
`updated_at`=new.updated_at,
`operation_type`='add';

CREATE TRIGGER `inbox_delete_log` AFTER DELETE ON `inbox`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inbox_log
SET
	`mail_id` =old.mail_id,
	`uid` =old.uid,
	`mail_no`=old.mail_no,
	`from_address` =old.from_address,
	`from_name` =old.from_name,
	`to_address` =old.to_address,
	`to_name` =old.to_name,
	`reply_to` =old.reply_to,
	`reply_to_name` =old.reply_to_name,
	`sender_address` =old.sender_address,
	`sender_name` =old.sender_name,
	`cc` =old.cc,
	`bcc` =old.bcc,
	`subject` =old.subject,
	`body` =old.body,
	`attachments` =old.attachments,
	`received_at` =old.received_at,
	`folder_label` =old.folder_label,
	`folder_name` =old.folder_name,
	`is_sent` =old.is_sent,
	`is_unread` =old.is_unread,
	`is_starred` =old.is_starred,
	`is_attachment` =old.is_attachment,
	`is_created` =old.is_created,
	`is_updated` =old.is_updated,
	`is_deleted` =old.is_deleted,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`staff_id` =old.staff_id,
	`operation_type`='delete';

CREATE TRIGGER `industry_type_edit_log` AFTER UPDATE ON `industry_type`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.industry_type_log
SET
	`id` =new.id,
	`industry_type`=new.industry_type,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `inquiry_add_log` AFTER INSERT ON `inquiry`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = new.inquiry_id,
  `inquiry_no` = new.inquiry_no,
  `due_date` = new.due_date,
  `reference_date` = new.reference_date,
  `inquiry_date` = new.inquiry_date,
  `kind_attn_id` = new.kind_attn_id,
  `follow_up_by_id` = new.follow_up_by_id,
  `assigned_to_id` = new.assigned_to_id,
  `industry_type_id` = new.industry_type_id,
  `inquiry_stage_id` = new.inquiry_stage_id,
  `branch_id` = new.branch_id,
  `inquiry_status_id` = new.inquiry_status_id,
  `sales_id` = new.sales_id,
  `sales_type_id` = new.sales_type_id,
  `party_id` = new.party_id,
  `received_email` = new.received_email,
  `received_fax` = new.received_fax,
  `received_verbal` = new.received_verbal,
  `is_received_post` = new.is_received_post,
  `send_email` = new.send_email,
  `send_fax` = new.send_fax,
  `is_send_post` = new.is_send_post,
  `send_from_post_name` = new.send_from_post_name,
  `lead_or_inquiry` = new.lead_or_inquiry,
  `lead_id` = new.lead_id,
  `history` = new.history,
  `created_by` = new.created_by,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `updated_by` = new.updated_by,
  `operation_type` = 'add';

CREATE TRIGGER `inquiry_confirmation_add_log` AFTER INSERT ON `inquiry_confirmation`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_confirmation_log
SET
	`id` =new.id,
	`inquiry_confirmation`=new.inquiry_confirmation,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `inquiry_confirmation_delete_log` AFTER DELETE ON `inquiry_confirmation`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_confirmation_log
SET
	`id` =old.id,
	`inquiry_confirmation`=old.inquiry_confirmation,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `inquiry_confirmation_edit_log` AFTER UPDATE ON `inquiry_confirmation`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_confirmation_log
SET
	`id` =new.id,
	`inquiry_confirmation`=new.inquiry_confirmation,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `inquiry_delete_log` AFTER DELETE ON `inquiry`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = old.inquiry_id,
  `inquiry_no` = old.inquiry_no,
  `due_date` = old.due_date,
  `reference_date` = old.reference_date,
  `inquiry_date` = old.inquiry_date,
  `kind_attn_id` = old.kind_attn_id,
  `follow_up_by_id` = old.follow_up_by_id,
  `assigned_to_id` = old.assigned_to_id,
  `industry_type_id` = old.industry_type_id,
  `inquiry_stage_id` = old.inquiry_stage_id,
  `branch_id` = old.branch_id,
  `inquiry_status_id` = old.inquiry_status_id,
  `sales_id` = old.sales_id,
  `sales_type_id` = old.sales_type_id,
  `party_id` = old.party_id,
  `received_email` = old.received_email,
  `received_fax` = old.received_fax,
  `received_verbal` = old.received_verbal,
  `is_received_post` = old.is_received_post,
  `send_email` = old.send_email,
  `send_fax` = old.send_fax,
  `is_send_post` = old.is_send_post,
  `send_from_post_name` = old.send_from_post_name,
  `lead_or_inquiry` = old.lead_or_inquiry,
  `lead_id` = old.lead_id,
  `history` = old.history,
  `created_by` = old.created_by,
  `created_at` = old.created_at,
  `updated_at` = old.updated_at,
  `updated_by` = old.updated_by,
  `operation_type` = 'delete';

CREATE TRIGGER `inbox_add_log` AFTER INSERT ON `inbox`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inbox_log
SET
	`mail_id` =new.mail_id,
	`uid` =new.uid,
	`mail_no`=new.mail_no,
	`from_address` =new.from_address,
	`from_name` =new.from_name,
	`to_address` =new.to_address,
	`to_name` =new.to_name,
	`reply_to` =new.reply_to,
	`reply_to_name` =new.reply_to_name,
	`sender_address` =new.sender_address,
	`sender_name` =new.sender_name,
	`cc` =new.cc,
	`bcc` =new.bcc,
	`subject` =new.subject,
	`body` =new.body,
	`attachments` =new.attachments,
	`received_at` =new.received_at,
	`folder_label` =new.folder_label,
	`folder_name` =new.folder_name,
	`is_sent` =new.is_sent,
	`is_unread` =new.is_unread,
	`is_starred` =new.is_starred,
	`is_attachment` =new.is_attachment,
	`is_created` =new.is_created,
	`is_updated` =new.is_updated,
	`is_deleted` =new.is_deleted,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`staff_id` =new.staff_id,
	`operation_type`='add';

CREATE TRIGGER `inquiry_edit_log` AFTER UPDATE ON `inquiry`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_log
SET
  `inquiry_id` = new.inquiry_id,
  `inquiry_no` = new.inquiry_no,
  `due_date` = new.due_date,
  `reference_date` = new.reference_date,
  `inquiry_date` = new.inquiry_date,
  `kind_attn_id` = new.kind_attn_id,
  `follow_up_by_id` = new.follow_up_by_id,
  `assigned_to_id` = new.assigned_to_id,
  `industry_type_id` = new.industry_type_id,
  `inquiry_stage_id` = new.inquiry_stage_id,
  `branch_id` = new.branch_id,
  `inquiry_status_id` = new.inquiry_status_id,
  `sales_id` = new.sales_id,
  `sales_type_id` = new.sales_type_id,
  `party_id` = new.party_id,
  `received_email` = new.received_email,
  `received_fax` = new.received_fax,
  `received_verbal` = new.received_verbal,
  `is_received_post` = new.is_received_post,
  `send_email` = new.send_email,
  `send_fax` = new.send_fax,
  `is_send_post` = new.is_send_post,
  `send_from_post_name` = new.send_from_post_name,
  `lead_or_inquiry` = new.lead_or_inquiry,
  `lead_id` = new.lead_id,
  `history` = new.history,
  `created_by` = new.created_by,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `updated_by` = new.updated_by,
  `operation_type` = 'edit';

CREATE TRIGGER `inquiry_followup_history_delete_log` AFTER DELETE ON `inquiry_followup_history`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_followup_history_log
SET
	`id` =old.id,
	`inquiry_id`=old.inquiry_id,
	`followup_date`=old.followup_date,
	`history`=old.history,
	`followup_by`=old.followup_by,
	`operation_type`='delete';

CREATE TRIGGER `inquiry_followup_history_edit_log` AFTER UPDATE ON `inquiry_followup_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_followup_history_log
SET
	`id` =new.id,
	`inquiry_id`=new.inquiry_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='edit';

CREATE TRIGGER `inquiry_item_add_log` AFTER INSERT ON `inquiry_items`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_log
SET
  `id` = new.id,
  `inquiry_id` = new.inquiry_id,
  `item_data` = new.item_data,
  `inquiry_flag` = new.inquiry_flag,
  `quotation_flag` = new.quotation_flag,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `operation_type` = 'add';

CREATE TRIGGER `inquiry_item_delete_log` AFTER DELETE ON `inquiry_items`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_log
SET
  `id` = old.id,
  `inquiry_id` = old.inquiry_id,
  `item_data` = old.item_data,
  `inquiry_flag` = old.inquiry_flag,
  `quotation_flag` = old.quotation_flag,
  `created_at` = old.created_at,
  `updated_at` = old.updated_at,
  `operation_type` = 'delete';

CREATE TRIGGER `industry_type_delete_log` AFTER DELETE ON `industry_type`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.industry_type_log
SET
	`id` =old.id,
	`industry_type`=old.industry_type,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `industry_type_add_log` AFTER INSERT ON `industry_type`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.industry_type_log
SET
	`id` =new.id,
	`industry_type`=new.industry_type,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `inquiry_item_edit_log` AFTER UPDATE ON `inquiry_items`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_items_log
SET
  `id` = new.id,
  `inquiry_id` = new.inquiry_id,
  `item_data` = new.item_data,
  `inquiry_flag` = new.inquiry_flag,
  `quotation_flag` = new.quotation_flag,
  `created_at` = new.created_at,
  `updated_at` = new.updated_at,
  `operation_type` = 'edit';

CREATE TRIGGER `inquiry_followup_history_add_log` AFTER INSERT ON `inquiry_followup_history`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_followup_history_log
SET
	`id` =new.id,
	`inquiry_id`=new.inquiry_id,
	`followup_date`=new.followup_date,
	`history`=new.history,
	`followup_by`=new.followup_by,
	`operation_type`='add';

CREATE TRIGGER `inquiry_status_add_log` AFTER INSERT ON `inquiry_status`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_status_log
SET
	`id` =new.id,
	`inquiry_status`=new.inquiry_status,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `inquiry_status_delete_log` AFTER DELETE ON `inquiry_status`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_status_log
SET
	`id`=old.id,
	`inquiry_status`=old.inquiry_status,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `inquiry_status_edit_log` AFTER UPDATE ON `inquiry_status`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_status_log
SET
	`id`=new.id,
	`inquiry_status`=new.inquiry_status,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	
	`operation_type`='edit';

CREATE TRIGGER `inbox_edit_log` AFTER UPDATE ON `inbox`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inbox_log
SET
	`mail_id` =new.mail_id,
	`uid` =new.uid,
	`mail_no`=new.mail_no,
	`from_address` =new.from_address,
	`from_name` =new.from_name,
	`to_address` =new.to_address,
	`to_name` =new.to_name,
	`reply_to` =new.reply_to,
	`reply_to_name` =new.reply_to_name,
	`sender_address` =new.sender_address,
	`sender_name` =new.sender_name,
	`cc` =new.cc,
	`bcc` =new.bcc,
	`subject` =new.subject,
	`body` =new.body,
	`attachments` =new.attachments,
	`received_at` =new.received_at,
	`folder_label` =new.folder_label,
	`folder_name` =new.folder_name,
	`is_sent` =new.is_sent,
	`is_unread` =new.is_unread,
	`is_starred` =new.is_starred,
	`is_attachment` =new.is_attachment,
	`is_created` =new.is_created,
	`is_updated` =new.is_updated,
	`is_deleted` =new.is_deleted,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`staff_id` =new.staff_id,
	`operation_type`='edit';

CREATE TRIGGER `inspection_required_delete_log` AFTER DELETE ON `inspection_required`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inspection_required_log
SET
	`id`=old.id,
	`inspection_required`=old.inspection_required,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `inspection_required_edit_log` AFTER UPDATE ON `inspection_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inspection_required_log
SET
	`id`=new.id,
	`inspection_required`=new.inspection_required,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	
	`operation_type`='edit';

CREATE TRIGGER `inquiry_stage_add_log` AFTER INSERT ON `inquiry_stage`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_stage_log
SET
	`id` =new.id,
	`inquiry_stage`=new.inquiry_stage,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `interview_delete_log` AFTER DELETE ON `interview`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.interview_log
SET
	`interview_id`=old.interview_id,
	`name`=old.name,
	`email`=old.email,
	`pass`=old.pass,
	`mailbox_email`=old.mailbox_email,
	`mailbox_password`=old.mailbox_password,
	`image`=old.image,
	`birth_date`=old.birth_date,
	`qualification`=old.qualification,
	`father_name`=old.father_name,
	`mother_name`=old.mother_name,
	`husband_wife_name`=old.husband_wife_name,
	`blood_group`=old.blood_group,
	`contact_no`=old.contact_no,
	`married_status`=old.married_status,
	`gender`=old.gender,
	`address`=old.address,
	`permanent_address`=old.permanent_address,
	`interview_review`=old.interview_review,
	`interview_decision`=old.interview_decision,
	`designation_id`=old.designation_id,
	`department_id`=old.department_id,
	`grade_id`=old.grade_id,
	`date_of_joining`=old.date_of_joining,
	`date_of_leaving`=old.date_of_leaving,
	`allow_pf`=old.allow_pf,
	`salary`=old.salary,
	`basic_pay`=old.basic_pay,
	`house_rent`=old.house_rent,
	`traveling_allowance`=old.traveling_allowance,
	`education_allowance`=old.education_allowance,
	`pf_amount`=old.pf_amount,
	`bonus_amount`=old.bonus_amount,
	`other_allotment`=old.other_allotment,
	`medical_reimbursement`=old.medical_reimbursement,
	`professional_tax`=old.professional_tax,
	`monthly_gross`=old.monthly_gross,
	`cost_to_company`=old.cost_to_company,
	`pf_employee`=old.pf_employee,
	`bank_name`=old.bank_name,
	`esic_no`=old.esic_no,
	`emp_id`=old.emp_id,
	`pan_no`=old.pan_no,
	`bank_acc_no`=old.bank_acc_no,
	`uan_id`=old.uan_id,
	`pf_no`=old.pf_no,
	`emp_no`=old.emp_no,
	`esic_id`=old.esic_id,
	`created_at`=old.created_at,
	`operation_type`='delete';

CREATE TRIGGER `inquiry_stage_delete_log` AFTER DELETE ON `inquiry_stage`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.inquiry_stage_log
SET
	`id`=old.id,
	`inquiry_stage`=old.inquiry_stage,
	`created_at` =old.created_at,
	`updated_at` =old.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `inspection_required_add_log` AFTER INSERT ON `inspection_required`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inspection_required_log
SET
	`id` =new.id,
	`inspection_required`=new.inspection_required,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	`operation_type`='add';

CREATE TRIGGER `interview_add_log` AFTER INSERT ON `interview`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.interview_log
SET
		`interview_id`=new.interview_id,
		`name`=new.name,
		`email`=new.email,
		`pass`=new.pass,
		`mailbox_email`=new.mailbox_email,
		`mailbox_password`=new.mailbox_password,
		`image`=new.image,
		`birth_date`=new.birth_date,
		`qualification`=new.qualification,
		`father_name`=new.father_name,
		`mother_name`=new.mother_name,
		`husband_wife_name`=new.husband_wife_name,
		`blood_group`=new.blood_group,
		`contact_no`=new.contact_no,
		`married_status`=new.married_status,
		`gender`=new.gender,
		`address`=new.address,
		`permanent_address`=new.permanent_address,
		`interview_review`=new.interview_review,
		`interview_decision`=new.interview_decision,
		`designation_id`=new.designation_id,
		`department_id`=new.department_id,
		`grade_id`=new.grade_id,
		`date_of_joining`=new.date_of_joining,
		`date_of_leaving`=new.date_of_leaving,
		`allow_pf`=new.allow_pf,
		`salary`=new.salary,
		`basic_pay`=new.basic_pay,
		`house_rent`=new.house_rent,
		`traveling_allowance`=new.traveling_allowance,
		`education_allowance`=new.education_allowance,
		`pf_amount`=new.pf_amount,
		`bonus_amount`=new.bonus_amount,
		`other_allotment`=new.other_allotment,
		`medical_reimbursement`=new.medical_reimbursement,
		`professional_tax`=new.professional_tax,
		`monthly_gross`=new.monthly_gross,
		`cost_to_company`=new.cost_to_company,
		`pf_employee`=new.pf_employee,
		`bank_name`=new.bank_name,
		`esic_no`=new.esic_no,
		`emp_id`=new.emp_id,
		`pan_no`=new.pan_no,
		`bank_acc_no`=new.bank_acc_no,
		`uan_id`=new.uan_id,
		`pf_no`=new.pf_no,
		`emp_no`=new.emp_no,
		`esic_id`=new.esic_id,
		`created_at`=new.created_at,
		`operation_type`='add';

CREATE TRIGGER `invoice_billing_terms_edit_log` AFTER UPDATE ON `invoice_billing_terms`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_billing_terms_log
SET
	`id`=new.id,
	`invoice_id` =NEW.invoice_id,
	`cal_code` =NEW.cal_code,
	`narration` =NEW.narration,
	`cal_definition` =NEW.cal_definition,
	`percentage` =NEW.percentage,
	`value` =NEW.value,
	`gl_acc` =NEW.gl_acc,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='edit';

CREATE TRIGGER `invoice_cal_code_definition_add_log` AFTER INSERT ON `invoice_cal_code_definition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_cal_code_definition_log
SET

	`id`=new.id,
	`billing_terms_detail_id` =NEW.billing_terms_detail_id,
	`invoice_id` =NEW.invoice_id,
	`is_first_element` =NEW.is_first_element,
	`cal_operation` =NEW.cal_operation,
	`cal_code_id` =NEW.cal_code_id,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='add';

CREATE TRIGGER `invoice_cal_code_definition_delete_log` AFTER DELETE ON `invoice_cal_code_definition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_cal_code_definition_log
SET

	`id`=OLD.id,
	`billing_terms_detail_id` =OLD.billing_terms_detail_id,
	`invoice_id` =OLD.invoice_id,
	`is_first_element` =OLD.is_first_element,
	`cal_operation` =OLD.cal_operation,
	`cal_code_id` =OLD.cal_code_id,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type` ='delete';

CREATE TRIGGER `interview_edit_log` AFTER UPDATE ON `interview`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.interview_log
SET
	`interview_id`=new.interview_id,
	`name`=new.name,
	`email`=new.email,
	`pass`=new.pass,
	`mailbox_email`=new.mailbox_email,
	`mailbox_password`=new.mailbox_password,
	`image`=new.image,
	`birth_date`=new.birth_date,
	`qualification`=new.qualification,
	`father_name`=new.father_name,
	`mother_name`=new.mother_name,
	`husband_wife_name`=new.husband_wife_name,
	`blood_group`=new.blood_group,
	`contact_no`=new.contact_no,
	`married_status`=new.married_status,
	`gender`=new.gender,
	`address`=new.address,
	`permanent_address`=new.permanent_address,
	`interview_review`=new.interview_review,
	`interview_decision`=new.interview_decision,
	`designation_id`=new.designation_id,
	`department_id`=new.department_id,
	`grade_id`=new.grade_id,
	`date_of_joining`=new.date_of_joining,
	`date_of_leaving`=new.date_of_leaving,
	`allow_pf`=new.allow_pf,
	`salary`=new.salary,
	`basic_pay`=new.basic_pay,
	`house_rent`=new.house_rent,
	`traveling_allowance`=new.traveling_allowance,
	`education_allowance`=new.education_allowance,
	`pf_amount`=new.pf_amount,
	`bonus_amount`=new.bonus_amount,
	`other_allotment`=new.other_allotment,
	`medical_reimbursement`=new.medical_reimbursement,
	`professional_tax`=new.professional_tax,
	`monthly_gross`=new.monthly_gross,
	`cost_to_company`=new.cost_to_company,
	`pf_employee`=new.pf_employee,
	`bank_name`=new.bank_name,
	`esic_no`=new.esic_no,
	`emp_id`=new.emp_id,
	`pan_no`=new.pan_no,
	`bank_acc_no`=new.bank_acc_no,
	`uan_id`=new.uan_id,
	`pf_no`=new.pf_no,
	`emp_no`=new.emp_no,
	`esic_id`=new.esic_id,
	`created_at`=new.created_at,
	`operation_type`='edit';

CREATE TRIGGER `invoice_billing_terms_delete_log` AFTER DELETE ON `invoice_billing_terms`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_billing_terms_log
SET
	`id`=OLD.id,
	`invoice_id` =OLD.invoice_id,
	`cal_code` =OLD.cal_code,
	`narration` =OLD.narration,
	`cal_definition` =OLD.cal_definition,
	`percentage` =OLD.percentage,
	`value` =OLD.value,
	`gl_acc` =OLD.gl_acc,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type` ='delete';

CREATE TRIGGER `inquiry_stage_edit_log` AFTER UPDATE ON `inquiry_stage`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.inquiry_stage_log
SET
	`id`=new.id,
	`inquiry_stage`=new.inquiry_stage,
	`created_at` =new.created_at,
	`updated_at` =new.updated_at,
	
	`operation_type`='edit';

CREATE TRIGGER `invoice_cal_code_definition_edit_log` AFTER UPDATE ON `invoice_cal_code_definition`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_cal_code_definition_log
SET

	`id`=new.id,
	`billing_terms_detail_id` =NEW.billing_terms_detail_id,
	`invoice_id` =NEW.invoice_id,
	`is_first_element` =NEW.is_first_element,
	`cal_operation` =NEW.cal_operation,
	`cal_code_id` =NEW.cal_code_id,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='edit';

CREATE TRIGGER `invoice_billing_terms_add_log` AFTER INSERT ON `invoice_billing_terms`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoice_billing_terms_log
SET
	`id`=new.id,
	`invoice_id` =NEW.invoice_id,
	`cal_code` =NEW.cal_code,
	`narration` =NEW.narration,
	`cal_definition` =NEW.cal_definition,
	`percentage` =NEW.percentage,
	`value` =NEW.value,
	`gl_acc` =NEW.gl_acc,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='add';

CREATE TRIGGER `invoice_items_edit_log` AFTER UPDATE ON `invoice_items`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.invoice_items_log
SET
	`id`=new.id,
	`item_id` =NEW.item_id,
	`invoice_id` =NEW.invoice_id,
	`item_code` =NEW.item_code,
	`item_category_id` =NEW.item_category_id,
	`item_name` =NEW.item_name,
	`item_desc` =NEW.item_desc,
	`item_description` =NEW.item_description,
	`add_description` =NEW.add_description,
	`detail_description` =NEW.detail_description,
	`drawing_number` =NEW.drawing_number,
	`drawing_revision` =NEW.drawing_revision,
	`uom_id` =NEW.uom_id,
	`uom` =NEW.uom,
	`total_amount` =NEW.total_amount,
	`item_note` =NEW.item_note,
	`quantity` =NEW.quantity,
	`disc_per` =NEW.disc_per,
	`rate` =NEW.rate,
	`disc_value` =NEW.disc_value,
	`amount` =NEW.amount,
	`net_amount` =NEW.net_amount,
	`item_status_id` =NEW.item_status_id,
	`item_status` =NEW.item_status,
	`cust_part_no` =NEW.cust_part_no,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='edit';

CREATE TRIGGER `invoice_logins_add_log` AFTER INSERT ON `invoice_logins`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_logins_log
SET
`id` = NEW.id,
`invoice_id` = NEW.invoice_id,
`created_by` = NEW.created_by,
`created_by_id` = NEW.created_by_id,
`created_date_time` = NEW.created_date_time,
`last_modified_by` = NEW.last_modified_by,
`last_modified_by_id` = NEW.last_modified_by_id,
`modified_date_time` = NEW.modified_date_time,
`prepared_by` = NEW.prepared_by,
`prepared_by_id` = NEW.prepared_by_id,
`prepared_date_time` = NEW.prepared_date_time,
`approved_by` = NEW.approved_by,
`approved_by_id` = NEW.approved_by_id,
`approved_date_time` = NEW.approved_date_time,
`stage_id` = NEW.stage_id,
`stage` = NEW.stage,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `invoice_items_delete_log` AFTER DELETE ON `invoice_items`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.invoice_items_log
SET
	`id`=OLD.id,
	`item_id` =OLD.item_id,
	`invoice_id` =OLD.invoice_id,
	`item_code` =OLD.item_code,
	`item_category_id` =OLD.item_category_id,
	`item_name` =OLD.item_name,
	`item_desc` =OLD.item_desc,
	`item_description` =OLD.item_description,
	`add_description` =OLD.add_description,
	`detail_description` =OLD.detail_description,
	`drawing_number` =OLD.drawing_number,
	`drawing_revision` =OLD.drawing_revision,
	`uom_id` =OLD.uom_id,
	`uom` =OLD.uom,
	`total_amount` =OLD.total_amount,
	`item_note` =OLD.item_note,
	`quantity` =OLD.quantity,
	`disc_per` =OLD.disc_per,
	`rate` =OLD.rate,
	`disc_value` =OLD.disc_value,
	`amount` =OLD.amount,
	`net_amount` =OLD.net_amount,
	`item_status_id` =OLD.item_status_id,
	`item_status` =OLD.item_status,
	`cust_part_no` =OLD.cust_part_no,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type` ='delete';

CREATE TRIGGER `invoice_items_add_log` AFTER INSERT ON `invoice_items`
 FOR EACH ROW INSERT INTO
	jaykhodiyar_logs.invoice_items_log
SET
	`id`=new.id,
	`item_id` =NEW.item_id,
	`invoice_id` =NEW.invoice_id,
	`item_code` =NEW.item_code,
	`item_category_id` =NEW.item_category_id,
	`item_name` =NEW.item_name,
	`item_desc` =NEW.item_desc,
	`item_description` =NEW.item_description,
	`add_description` =NEW.add_description,
	`detail_description` =NEW.detail_description,
	`drawing_number` =NEW.drawing_number,
	`drawing_revision` =NEW.drawing_revision,
	`uom_id` =NEW.uom_id,
	`uom` =NEW.uom,
	`total_amount` =NEW.total_amount,
	`item_note` =NEW.item_note,
	`quantity` =NEW.quantity,
	`disc_per` =NEW.disc_per,
	`rate` =NEW.rate,
	`disc_value` =NEW.disc_value,
	`amount` =NEW.amount,
	`net_amount` =NEW.net_amount,
	`item_status_id` =NEW.item_status_id,
	`item_status` =NEW.item_status,
	`cust_part_no` =NEW.cust_part_no,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type` ='add';

CREATE TRIGGER `invoice_logins_edit_log` AFTER UPDATE ON `invoice_logins`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_logins_log
SET
`id` = NEW.id,
`invoice_id` = NEW.invoice_id,
`created_by` = NEW.created_by,
`created_by_id` = NEW.created_by_id,
`created_date_time` = NEW.created_date_time,
`last_modified_by` = NEW.last_modified_by,
`last_modified_by_id` = NEW.last_modified_by_id,
`modified_date_time` = NEW.modified_date_time,
`prepared_by` = NEW.prepared_by,
`prepared_by_id` = NEW.prepared_by_id,
`prepared_date_time` = NEW.prepared_date_time,
`approved_by` = NEW.approved_by,
`approved_by_id` = NEW.approved_by_id,
`approved_date_time` = NEW.approved_date_time,
`stage_id` = NEW.stage_id,
`stage` = NEW.stage,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `invoice_logins_delete_log` AFTER DELETE ON `invoice_logins`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_logins_log
SET
`id` = OLD.id,
`invoice_id` = OLD.invoice_id,
`created_by` = OLD.created_by,
`created_by_id` = OLD.created_by_id,
`created_date_time` = OLD.created_date_time,
`last_modified_by` = OLD.last_modified_by,
`last_modified_by_id` = OLD.last_modified_by_id,
`modified_date_time` = OLD.modified_date_time,
`prepared_by` = OLD.prepared_by,
`prepared_by_id` = OLD.prepared_by_id,
`prepared_date_time` = OLD.prepared_date_time,
`approved_by` = OLD.approved_by,
`approved_by_id` = OLD.approved_by_id,
`approved_date_time` = OLD.approved_date_time,
`stage_id` = OLD.stage_id,
`stage` = OLD.stage,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `invoice_type_add_log` AFTER INSERT ON `invoice_type`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_type_log
SET
`id` = NEW.id,
`invoice_type` = NEW.invoice_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `invoice_type_delete_log` AFTER DELETE ON `invoice_type`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_type_log
SET
`id` = OLD.id,
`invoice_type` = OLD.invoice_type,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `invoice_type_edit_log` AFTER UPDATE ON `invoice_type`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.invoice_type_log
SET
`id` = NEW.id,
`invoice_type` = NEW.invoice_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_bom_add_log` AFTER INSERT ON `item_bom`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_bom_log
SET
`id` = NEW.id,
`item_master_id` = NEW.item_master_id,
`item_category_id` = NEW.item_category_id,
`item_id` = NEW.item_id,
`qty` = NEW.qty,
`uom_id` = NEW.uom_id,
`total_amount` = NEW.total_amount,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `invoices_add_log` AFTER INSERT ON `invoices`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoices_log
SET
`id`=new.id,
	`invoice_type_id` =NEW.invoice_type_id,
	`sales_order_id` =NEW.sales_order_id,
	`total_cmount` =NEW.total_cmount,
	`quatation_id` =NEW.quatation_id,
	`quotation_id` =NEW.quotation_id,
	`invoice_no` =NEW.invoice_no,
	`sales_order_no` =NEW.sales_order_no,
	`sales_to_party_id` =NEW.sales_to_party_id,
	`ship_to_party_id` =NEW.ship_to_party_id,
	`branch_id` =NEW.branch_id,
	`billing_terms_id` =NEW.billing_terms_id,
	`sales_order_date` =NEW.sales_order_date,
	`committed_date` =NEW.committed_date,
	`currency_id` =NEW.currency_id,
	`project_id` =NEW.project_id,
	`kind_attn_id` =NEW.kind_attn_id,
	`contact_no` =NEW.contact_no,
	`cust_po_no` =NEW.cust_po_no,
	`email_id` =NEW.email_id,
	`po_date` =NEW.po_date,
	`sales_order_pref_id` =NEW.sales_order_pref_id,
	`conversation_rate` =NEW.conversation_rate,
	`sales_id` =NEW.sales_id,
	`sales_order_stage_id` =NEW.sales_order_stage_id,
	`sales_order_status_id` =NEW.sales_order_status_id,
	`order_type` =NEW.order_type,
	`lead_provider_id` =NEW.lead_provider_id,
	`buyer_detail_id` =NEW.buyer_detail_id,
	`note_detail_id` =NEW.note_detail_id,
	`login_detail_id` =NEW.login_detail_id,
	`transportation_by_id` =NEW.transportation_by_id,
	`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
	`loading_by_id` =NEW.loading_by_id,
	`inspection_required_id` =NEW.inspection_required_id,
	`unloading_by_id` =NEW.unloading_by_id,
	`erection_commissioning_id` =NEW.erection_commissioning_id,
	`road_insurance_by_id` =NEW.road_insurance_by_id,
	`for_required_id` =NEW.for_required_id,
	`review_date` =NEW.review_date,
	`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
	`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
	`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
	`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
	`sales_order_validaty` =NEW.sales_order_validaty,
	`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
	`mode_of_shipment_name` =NEW.mode_of_shipment_name,
	`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
	`supplier_payment_terms` =NEW.supplier_payment_terms,
	`received_payment` =NEW.received_payment,
	`received_payment_date` =NEW.received_payment_date,
	`received_payment_type` =NEW.received_payment_type,
	`terms_condition_purchase` =NEW.terms_condition_purchase,
	`isApproved`=NEW.isApproved,
	`pdf_url` =NEW.pdf_url,
	`lr_no` =NEW.lr_no,
	`lr_date` =NEW.lr_date,
	`vehicle_no` =NEW.vehicle_no,
	`against_form`=NEW.against_form,
	`taxes_data` =NEW.taxes_data,
`created_at`=NEW.created_at,
`updated_at`=NEW.updated_at,
	`created_by` =NEW.created_by,
`operation_type` ='add';

CREATE TRIGGER `invoices_delete_log` AFTER DELETE ON `invoices`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoices_log
SET
`id`=OLD.id,
	`invoice_type_id` =OLD.invoice_type_id,
	`sales_order_id` =OLD.sales_order_id,
	`total_cmount` =OLD.total_cmount,
	`quatation_id` =OLD.quatation_id,
	`quotation_id` =OLD.quotation_id,
	`invoice_no` =OLD.invoice_no,
	`sales_order_no` =OLD.sales_order_no,
	`sales_to_party_id` =OLD.sales_to_party_id,
	`ship_to_party_id` =OLD.ship_to_party_id,
	`branch_id` =OLD.branch_id,
	`billing_terms_id` =OLD.billing_terms_id,
	`sales_order_date` =OLD.sales_order_date,
	`committed_date` =OLD.committed_date,
	`currency_id` =OLD.currency_id,
	`project_id` =OLD.project_id,
	`kind_attn_id` =OLD.kind_attn_id,
	`contact_no` =OLD.contact_no,
	`cust_po_no` =OLD.cust_po_no,
	`email_id` =OLD.email_id,
	`po_date` =OLD.po_date,
	`sales_order_pref_id` =OLD.sales_order_pref_id,
	`conversation_rate` =OLD.conversation_rate,
	`sales_id` =OLD.sales_id,
	`sales_order_stage_id` =OLD.sales_order_stage_id,
	`sales_order_status_id` =OLD.sales_order_status_id,
	`order_type` =OLD.order_type,
	`lead_provider_id` =OLD.lead_provider_id,
	`buyer_detail_id` =OLD.buyer_detail_id,
	`note_detail_id` =OLD.note_detail_id,
	`login_detail_id` =OLD.login_detail_id,
	`transportation_by_id` =OLD.transportation_by_id,
	`foundation_drawing_required_id` =OLD.foundation_drawing_required_id,
	`loading_by_id` =OLD.loading_by_id,
	`inspection_required_id` =OLD.inspection_required_id,
	`unloading_by_id` =OLD.unloading_by_id,
	`erection_commissioning_id` =OLD.erection_commissioning_id,
	`road_insurance_by_id` =OLD.road_insurance_by_id,
	`for_required_id` =OLD.for_required_id,
	`review_date` =OLD.review_date,
	`mach_deli_min_weeks` =OLD.mach_deli_min_weeks,
	`mach_deli_max_weeks` =OLD.mach_deli_max_weeks,
	`mach_deli_min_weeks_date` =OLD.mach_deli_min_weeks_date,
	`mach_deli_max_weeks_date` =OLD.mach_deli_max_weeks_date,
	`sales_order_validaty` =OLD.sales_order_validaty,
	`sales_order_quotation_delivery` =OLD.sales_order_quotation_delivery,
	`mode_of_shipment_name` =OLD.mode_of_shipment_name,
	`mode_of_shipment_truck_number` =OLD.mode_of_shipment_truck_number,
	`supplier_payment_terms` =OLD.supplier_payment_terms,
	`received_payment` =OLD.received_payment,
	`received_payment_date` =OLD.received_payment_date,
	`received_payment_type` =OLD.received_payment_type,
	`terms_condition_purchase` =OLD.terms_condition_purchase,
	`isApproved`=OLD.isApproved,
	`pdf_url` =OLD.pdf_url,
	`lr_no` =OLD.lr_no,
	`lr_date` =OLD.lr_date,
	`vehicle_no` =OLD.vehicle_no,
	`against_form`=OLD.against_form,
	`taxes_data` =OLD.taxes_data,
`created_at`=OLD.created_at,
`updated_at`=OLD.updated_at,
	`created_by` =OLD.created_by,
`operation_type` ='delete';

CREATE TRIGGER `item_bom_delete_log` AFTER DELETE ON `item_bom`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_bom_log
SET
`id` = OLD.id,
`item_master_id` = OLD.item_master_id,
`item_category_id` = OLD.item_category_id,
`item_id` = OLD.item_id,
`qty` = OLD.qty,
`uom_id` = OLD.uom_id,
`total_amount` = OLD.total_amount,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `invoices_edit_log` AFTER UPDATE ON `invoices`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.invoices_log
SET
`id`=new.id,
	`invoice_type_id` =NEW.invoice_type_id,
	`sales_order_id` =NEW.sales_order_id,
	`total_cmount` =NEW.total_cmount,
	`quatation_id` =NEW.quatation_id,
	`quotation_id` =NEW.quotation_id,
	`invoice_no` =NEW.invoice_no,
	`sales_order_no` =NEW.sales_order_no,
	`sales_to_party_id` =NEW.sales_to_party_id,
	`ship_to_party_id` =NEW.ship_to_party_id,
	`branch_id` =NEW.branch_id,
	`billing_terms_id` =NEW.billing_terms_id,
	`sales_order_date` =NEW.sales_order_date,
	`committed_date` =NEW.committed_date,
	`currency_id` =NEW.currency_id,
	`project_id` =NEW.project_id,
	`kind_attn_id` =NEW.kind_attn_id,
	`contact_no` =NEW.contact_no,
	`cust_po_no` =NEW.cust_po_no,
	`email_id` =NEW.email_id,
	`po_date` =NEW.po_date,
	`sales_order_pref_id` =NEW.sales_order_pref_id,
	`conversation_rate` =NEW.conversation_rate,
	`sales_id` =NEW.sales_id,
	`sales_order_stage_id` =NEW.sales_order_stage_id,
	`sales_order_status_id` =NEW.sales_order_status_id,
	`order_type` =NEW.order_type,
	`lead_provider_id` =NEW.lead_provider_id,
	`buyer_detail_id` =NEW.buyer_detail_id,
	`note_detail_id` =NEW.note_detail_id,
	`login_detail_id` =NEW.login_detail_id,
	`transportation_by_id` =NEW.transportation_by_id,
	`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
	`loading_by_id` =NEW.loading_by_id,
	`inspection_required_id` =NEW.inspection_required_id,
	`unloading_by_id` =NEW.unloading_by_id,
	`erection_commissioning_id` =NEW.erection_commissioning_id,
	`road_insurance_by_id` =NEW.road_insurance_by_id,
	`for_required_id` =NEW.for_required_id,
	`review_date` =NEW.review_date,
	`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
	`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
	`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
	`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
	`sales_order_validaty` =NEW.sales_order_validaty,
	`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
	`mode_of_shipment_name` =NEW.mode_of_shipment_name,
	`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
	`supplier_payment_terms` =NEW.supplier_payment_terms,
	`received_payment` =NEW.received_payment,
	`received_payment_date` =NEW.received_payment_date,
	`received_payment_type` =NEW.received_payment_type,
	`terms_condition_purchase` =NEW.terms_condition_purchase,
	`isApproved`=NEW.isApproved,
	`pdf_url` =NEW.pdf_url,
	`lr_no` =NEW.lr_no,
	`lr_date` =NEW.lr_date,
	`vehicle_no` =NEW.vehicle_no,
	`against_form`=NEW.against_form,
	`taxes_data` =NEW.taxes_data,
`created_at`=NEW.created_at,
`updated_at`=NEW.updated_at,
	`created_by` =NEW.created_by,
`operation_type` ='edit';

CREATE TRIGGER `item_bom_edit_log` AFTER UPDATE ON `item_bom`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_bom_log
SET
`id` = NEW.id,
`item_master_id` = NEW.item_master_id,
`item_category_id` = NEW.item_category_id,
`item_id` = NEW.item_id,
`qty` = NEW.qty,
`uom_id` = NEW.uom_id,
`total_amount` = NEW.total_amount,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_category_add_log` AFTER INSERT ON `item_category`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_category_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`created_by` = NEW.created_by,
`updated_by` = NEW.updated_by,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_category_delete_log` AFTER DELETE ON `item_category`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_category_log
SET
`id` = OLD.id,
`item_category` = OLD.item_category,
`created_by` = OLD.created_by,
`updated_by` = OLD.updated_by,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_class_edit_log` AFTER UPDATE ON `item_class`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_class_log
SET
`id` = NEW.id,
`item_class` = NEW.item_class,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_details_add_log` AFTER INSERT ON `item_details`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_details_log
SET
`id` = NEW.id,
`inquiry_id` =NEW.inquiry_id,
`item_code` =NEW.item_code,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`rev` =NEW.rev,
`uom` =NEW.uom,
`quantity` =NEW.quantity,
`cust_part_no` =NEW.cust_part_no,
`item_note` =NEW.item_note,
`lead_no1` =NEW.lead_no1,
`lead_no2` =NEW.lead_no2,
`item_status` =NEW.item_status,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_category_edit_log` AFTER UPDATE ON `item_category`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_category_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`created_by` = NEW.created_by,
`updated_by` = NEW.updated_by,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_class_delete_log` AFTER DELETE ON `item_class`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_class_log
SET
`id` = OLD.id,
`item_class` = OLD.item_class,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_documents_add_log` AFTER INSERT ON `item_documents`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_documents_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`detail` =NEW.detail,
`document_type` =NEW.document_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_class_add_log` AFTER INSERT ON `item_class`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_class_log
SET
`id` = NEW.id,
`item_class` = NEW.item_class,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_details_delete_log` AFTER DELETE ON `item_details`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_details_log
SET
`id` = OLD.id,
`inquiry_id` =OLD.inquiry_id,
`item_code` =OLD.item_code,
`item_description` =OLD.item_description,
`item_desc`=OLD.item_desc,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`rev` =OLD.rev,
`uom` =OLD.uom,
`quantity` =OLD.quantity,
`cust_part_no` =OLD.cust_part_no,
`item_note` =OLD.item_note,
`lead_no1` =OLD.lead_no1,
`lead_no2` =OLD.lead_no2,
`item_status` =OLD.item_status,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_details_edit_log` AFTER UPDATE ON `item_details`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_details_log
SET
`id` = NEW.id,
`inquiry_id` =NEW.inquiry_id,
`item_code` =NEW.item_code,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`rev` =NEW.rev,
`uom` =NEW.uom,
`quantity` =NEW.quantity,
`cust_part_no` =NEW.cust_part_no,
`item_note` =NEW.item_note,
`lead_no1` =NEW.lead_no1,
`lead_no2` =NEW.lead_no2,
`item_status` =NEW.item_status,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_documents_delete_log` AFTER DELETE ON `item_documents`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_documents_log
SET
`id` = OLD.id,
`item_id` =OLD.item_id,
`detail` =OLD.detail,
`document_type` =OLD.document_type,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_files_add_log` AFTER INSERT ON `item_files`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_files_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`file_url` =NEW.file_url,

`created_at` = NEW.created_at,

`operation_type` = 'add';

CREATE TRIGGER `item_documents_edit_log` AFTER UPDATE ON `item_documents`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_documents_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`detail` =NEW.detail,
`document_type` =NEW.document_type,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_group_code_delete_log` AFTER DELETE ON `item_group_code`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_group_code_log
SET
`id` = OLD.id,
`ig_code` =OLD.ig_code,
`ig_description` =OLD.ig_description,

`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_files_edit_log` AFTER UPDATE ON `item_files`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_files_log
SET
`id` = NEW.id,
`item_id` =NEW.item_id,
`file_url` =NEW.file_url,

`created_at` = NEW.created_at,

`operation_type` = 'edit';

CREATE TRIGGER `item_make_add_log` AFTER INSERT ON `item_make`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_make_log
SET
`id` = NEW.id,
`item_make` =NEW.item_make,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_make_edit_log` AFTER UPDATE ON `item_make`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_make_log
SET
`id` = NEW.id,
`item_make` =NEW.item_make,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_group_code_add_log` AFTER INSERT ON `item_group_code`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_group_code_log
SET
`id` = NEW.id,
`ig_code` =NEW.ig_code,
`ig_description` =NEW.ig_description,

`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_status_add_log` AFTER INSERT ON `item_status`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_status_log
SET
`id` = NEW.id,
`item_status` =NEW.item_status,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_files_delete_log` AFTER DELETE ON `item_files`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_files_log
SET
`id` = OLD.id,
`item_id` =OLD.item_id,
`file_url` =OLD.file_url,
`created_at` = OLD.created_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_make_delete_log` AFTER DELETE ON `item_make`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_make_log
SET
`id` = OLD.id,
`item_make` =OLD.item_make,


`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_type_delete_log` AFTER DELETE ON `item_type`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_type_log
SET
`id` = OLD.id,
`item_type` =OLD.item_type,


`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_type_edit_log` AFTER UPDATE ON `item_type`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_type_log
SET
`id` = NEW.id,
`item_type` =NEW.item_type,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `item_status_edit_log` AFTER UPDATE ON `item_status`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_status_log
SET
`id` = NEW.id,
`item_status` =NEW.item_status,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `items_add_log` AFTER INSERT ON `items`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.items_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`item_code1` = NEW.item_code1,
`item_code2` = NEW.item_code2,
`uom` = NEW.uom,
`qty` = NEW.qty,
`item_type` = NEW.item_type,
`stock` = NEW.stock,
`document` = NEW.document,
`hsn_code` = NEW.hsn_code,
`item_group` = NEW.item_group,
`item_active` = NEW.item_active,
`item_name` = NEW.item_name,
`cfactor` = NEW.cfactor,
`conv_uom` = NEW.conv_uom,
`item_mpt` = NEW.item_mpt,
`item_status` = NEW.item_status,
`image` = NEW.image,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `item_type_add_log` AFTER INSERT ON `item_type`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_type_log
SET
`id` = NEW.id,
`item_type` =NEW.item_type,


`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `items_delete_log` AFTER DELETE ON `items`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.items_log
SET
`id` = OLD.id,
`item_category` = OLD.item_category,
`item_code1` = OLD.item_code1,
`item_code2` = OLD.item_code2,
`uom` = OLD.uom,
`qty` = OLD.qty,
`item_type` = OLD.item_type,
`stock` = OLD.stock,
`document` = OLD.document,
`hsn_code` = OLD.hsn_code,
`item_group` = OLD.item_group,
`item_active` = OLD.item_active,
`item_name` = OLD.item_name,
`cfactor` = OLD.cfactor,
`conv_uom` = OLD.conv_uom,
`item_mpt` = OLD.item_mpt,
`item_status` = OLD.item_status,
`image` = OLD.image,
`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_group_code_edit_log` AFTER UPDATE ON `item_group_code`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_group_code_log
SET
`id` = NEW.id,
`ig_code` =NEW.ig_code,
`ig_description` =NEW.ig_description,

`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `lead_contact_person_delete_log` AFTER DELETE ON `lead_contact_person`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.lead_contact_person_log
SET
`id`=OLD.id,
`person_name` =OLD.person_name,
`department` =OLD.department,
`designation` =OLD.designation,
`phone_no` =OLD.phone_no,
`mobile_no` =OLD.mobile_no,
`fax_no` =OLD.fax_no,
`email_id` =OLD.email_id,
`division` =OLD.division,
`address` =OLD.address,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `item_status_delete_log` AFTER DELETE ON `item_status`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.item_status_log
SET
`id` = OLD.id,
`item_status` =OLD.item_status,


`created_at` = OLD.created_at,
`updated_at` = OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `lead_contact_person_edit_log` AFTER UPDATE ON `lead_contact_person`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.lead_contact_person_log
SET
`id`=NEW.id,
`person_name` =NEW.person_name,
`department` =NEW.department,
`designation` =NEW.designation,
`phone_no` =NEW.phone_no,
`mobile_no` =NEW.mobile_no,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`division` =NEW.division,
`address` =NEW.address,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `lead_details_edit_log` AFTER UPDATE ON `lead_details`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_details_log SET
`id`=NEW.id,
`party_id` =NEW.party_id,
`lead_no` =NEW.lead_no,
`lead_title` =NEW.lead_title,
`start_date` =NEW.start_date,
`end_date` =NEW.end_date,
`lead_detail` =NEW.lead_detail,
`industry_type_id` =NEW.industry_type_id,
`lead_owner_id` =NEW.lead_owner_id,
`assigned_to_id` =NEW.assigned_to_id,
`priority_id` =NEW.priority_id,
`lead_source_id` =NEW.lead_source_id,
`lead_stage_id` =NEW.lead_stage_id,
`lead_provider_id` =NEW.lead_provider_id,
`lead_status_id` =NEW.lead_status_id,
`review_date` =NEW.review_date,
`created_by`=NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `items_edit_log` AFTER UPDATE ON `items`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.items_log
SET
`id` = NEW.id,
`item_category` = NEW.item_category,
`item_code1` = NEW.item_code1,
`item_code2` = NEW.item_code2,
`uom` = NEW.uom,
`qty` = NEW.qty,
`item_type` = NEW.item_type,
`stock` = NEW.stock,
`document` = NEW.document,
`hsn_code` = NEW.hsn_code,
`item_group` = NEW.item_group,
`item_active` = NEW.item_active,
`item_name` = NEW.item_name,
`cfactor` = NEW.cfactor,
`conv_uom` = NEW.conv_uom,
`item_mpt` = NEW.item_mpt,
`item_status` = NEW.item_status,
`image` = NEW.image,
`created_at` = NEW.created_at,
`updated_at` = NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `lead_contact_person_add_log` AFTER INSERT ON `lead_contact_person`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.lead_contact_person_log
SET
`id`=NEW.id,
`person_name` =NEW.person_name,
`department` =NEW.department,
`designation` =NEW.designation,
`phone_no` =NEW.phone_no,
`mobile_no` =NEW.mobile_no,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`division` =NEW.division,
`address` =NEW.address,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `lead_details_delete_log` AFTER DELETE ON `lead_details`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_details_log SET
`id`=OLD.id,
`party_id` =OLD.party_id,
`lead_no` =OLD.lead_no,
`lead_title` =OLD.lead_title,
`start_date` =OLD.start_date,
`end_date` =OLD.end_date,
`lead_detail` =OLD.lead_detail,
`industry_type_id` =OLD.industry_type_id,
`lead_owner_id` =OLD.lead_owner_id,
`assigned_to_id` =OLD.assigned_to_id,
`priority_id` =OLD.priority_id,
`lead_source_id` =OLD.lead_source_id,
`lead_stage_id` =OLD.lead_stage_id,
`lead_provider_id` =OLD.lead_provider_id,
`lead_status_id` =OLD.lead_status_id,
`review_date` =OLD.review_date,
`created_by`=OLD.created_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `lead_provider_edit_log` AFTER UPDATE ON `lead_provider`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_provider_log SET
`id`=NEW.id,
`lead_provider` =NEW.lead_provider,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `lead_provider_add_log` AFTER INSERT ON `lead_provider`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_provider_log SET
`id`=NEW.id,
`lead_provider` =NEW.lead_provider,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `lead_source_delete_log` AFTER DELETE ON `lead_source`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_source_log SET
`id`=OLD.id,
`lead_source` =OLD.lead_source,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `lead_source_edit_log` AFTER UPDATE ON `lead_source`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_source_log SET
`id`=NEW.id,
`lead_source` =NEW.lead_source,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `lead_details_add_log` AFTER INSERT ON `lead_details`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_details_log SET
`id`=NEW.id,
`party_id` =NEW.party_id,
`lead_no` =NEW.lead_no,
`lead_title` =NEW.lead_title,
`start_date` =NEW.start_date,
`end_date` =NEW.end_date,
`lead_detail` =NEW.lead_detail,
`industry_type_id` =NEW.industry_type_id,
`lead_owner_id` =NEW.lead_owner_id,
`assigned_to_id` =NEW.assigned_to_id,
`priority_id` =NEW.priority_id,
`lead_source_id` =NEW.lead_source_id,
`lead_stage_id` =NEW.lead_stage_id,
`lead_provider_id` =NEW.lead_provider_id,
`lead_status_id` =NEW.lead_status_id,
`review_date` =NEW.review_date,
`created_by`=NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `lead_stage_delete_log` AFTER DELETE ON `lead_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_stage_log SET
`id`=OLD.id,
`lead_stage` =OLD.lead_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `lead_provider_delete_log` AFTER DELETE ON `lead_provider`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_provider_log SET
`id`=OLD.id,
`lead_provider` =OLD.lead_provider,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `lead_stage_add_log` AFTER INSERT ON `lead_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_stage_log SET
`id`=NEW.id,
`lead_stage` =NEW.lead_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `lead_status_edit_log` AFTER UPDATE ON `lead_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_status_log SET
`id`=NEW.id,
`lead_status` =NEW.lead_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `leave_for_employee_add_log` AFTER INSERT ON `leave_for_employee`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leave_for_employee_log SET
`id`=NEW.id,
`total_sl` =NEW.total_sl,
`total_cl` =NEW.total_cl,
`total_el_pl` =NEW.total_el_pl,
`operation_type` = 'add';

CREATE TRIGGER `leave_for_employee_delete_log` AFTER DELETE ON `leave_for_employee`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leave_for_employee_log SET
`id`=OLD.id,
`total_sl` =OLD.total_sl,
`total_cl` =OLD.total_cl,
`total_el_pl` =OLD.total_el_pl,
`operation_type` = 'delete';

CREATE TRIGGER `lead_source_add_log` AFTER INSERT ON `lead_source`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_source_log SET
`id`=NEW.id,
`lead_source` =NEW.lead_source,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `lead_status_delete_log` AFTER DELETE ON `lead_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_status_log SET
`id`=OLD.id,
`lead_status` =OLD.lead_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `leaves_delete_log` AFTER DELETE ON `leaves`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leaves_log SET
`leave_id`=OLD.leave_id,
`employee_id` =OLD.employee_id,
`name` =OLD.name,
`email` =OLD.email,
`from_date` =OLD.from_date,
`to_date` =OLD.to_date,
`leave_type` =OLD.leave_type,
`day` =OLD.day,
`leave_detail` =OLD.leave_detail,
`leave_count` =OLD.leave_count,
`created_at` =OLD.created_at,
`operation_type` = 'delete';

CREATE TRIGGER `lead_stage_edit_log` AFTER UPDATE ON `lead_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_stage_log SET
`id`=NEW.id,
`lead_stage` =NEW.lead_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `leave_for_employee_edit_log` AFTER UPDATE ON `leave_for_employee`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leave_for_employee_log SET
`id`=NEW.id,
`total_sl` =NEW.total_sl,
`total_cl` =NEW.total_cl,
`total_el_pl` =NEW.total_el_pl,
`operation_type` = 'edit';

CREATE TRIGGER `letters_delete_log` AFTER DELETE ON `letters`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.letters_log SET
`letter_id` =OLD.letter_id,
`letter_code` =OLD.letter_code,
`letter_template` =OLD.letter_template,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `letters_edit_log` AFTER UPDATE ON `letters`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.letters_log SET
`letter_id` =NEW.letter_id,
`letter_code` =NEW.letter_code,
`letter_template` =NEW.letter_template,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `leaves_add_log` AFTER INSERT ON `leaves`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leaves_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`name` =NEW.name,
`email` =NEW.email,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`day` =NEW.day,
`leave_detail` =NEW.leave_detail,
`leave_count` =NEW.leave_count,
`created_at` =NEW.created_at,
`operation_type` = 'add';

CREATE TRIGGER `leaves_edit_log` AFTER UPDATE ON `leaves`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.leaves_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`name` =NEW.name,
`email` =NEW.email,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`day` =NEW.day,
`leave_detail` =NEW.leave_detail,
`leave_count` =NEW.leave_count,
`created_at` =NEW.created_at,
`operation_type` = 'edit';

CREATE TRIGGER `loading_by_delete_log` AFTER DELETE ON `loading_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.loading_by_log SET
`id`=OLD.id,
`loading_by` =OLD.loading_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `loading_by_add_log` AFTER INSERT ON `loading_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.loading_by_log SET
`id`=NEW.id,
`loading_by` =NEW.loading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `lead_status_add_log` AFTER INSERT ON `lead_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.lead_status_log SET
`id`=NEW.id,
`lead_status` =NEW.lead_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `loading_by_edit_log` AFTER UPDATE ON `loading_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.loading_by_log SET
`id`=NEW.id,
`loading_by` =NEW.loading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `mail_system_folder_add_log` AFTER INSERT ON `mail_system_folder`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_folder_log SET
`folder_id` =NEW.folder_id,
`folder` =NEW.folder,
`staff_id` =NEW.staff_id,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `mail_system_delete_log` AFTER DELETE ON `mail_system`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_log SET
`mail_id` =OLD.mail_id,
`mail_uid` =OLD.mail_uid,
`staff_id` =OLD.staff_id,
`folder` =OLD.folder,
`from_name` =OLD.from_name,
`from_address` =OLD.from_address,
`to_name` =OLD.to_name,
`reply_to` =OLD.reply_to,
`reply_to_name` =OLD.reply_to_name,
`sender_address` =OLD.sender_address,
`sender_name` =OLD.sender_name,
`cc` =OLD.cc,
`bcc` =OLD.bcc,
`subject` =OLD.subject,
`body` =OLD.body,
`attachments` =OLD.attachments,
`received_at` =OLD.received_at,
`is_unread` =OLD.is_unread,
`is_starred` =OLD.is_starred,
`is_attachment` =OLD.is_attachment,
`is_read_receipt_req` =OLD.is_read_receipt_req,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `mail_system_edit_log` AFTER UPDATE ON `mail_system`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_log SET
`mail_id` =NEW.mail_id,
`mail_uid` =NEW.mail_uid,
`staff_id` =NEW.staff_id,
`folder` =NEW.folder,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_unread` =NEW.is_unread,
`is_starred` =NEW.is_starred,
`is_attachment` =NEW.is_attachment,
`is_read_receipt_req` =NEW.is_read_receipt_req,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `mailbox_folders_add_log` AFTER INSERT ON `mailbox_folders`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_folders_log SET
`id`=NEW.id,
`parent_id` =NEW.parent_id,
`folder_label` =NEW.folder_label,
`folder_name` =NEW.folder_name,
`old_parent_id` =NEW.old_parent_id,
`is_system_folder` =NEW.is_system_folder,
`is_created` =NEW.is_created,
`is_updated` =NEW.is_updated,
`is_deleted` =NEW.is_deleted,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`deleted_at` =NEW.deleted_at,
`operation_type` = 'add';

CREATE TRIGGER `mailbox_folders_delete_log` AFTER DELETE ON `mailbox_folders`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_folders_log SET
`id`=OLD.id,
`parent_id` =OLD.parent_id,
`folder_label` =OLD.folder_label,
`folder_name` =OLD.folder_name,
`old_parent_id` =OLD.old_parent_id,
`is_system_folder` =OLD.is_system_folder,
`is_created` =OLD.is_created,
`is_updated` =OLD.is_updated,
`is_deleted` =OLD.is_deleted,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`deleted_at` =OLD.deleted_at,
`operation_type` = 'delete';

CREATE TRIGGER `letters_add_log` AFTER INSERT ON `letters`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.letters_log SET
`letter_id` =NEW.letter_id,
`letter_code` =NEW.letter_code,
`letter_template` =NEW.letter_template,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `mailbox_folders_edit_log` AFTER UPDATE ON `mailbox_folders`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_folders_log SET
`id`=NEW.id,
`parent_id` =NEW.parent_id,
`folder_label` =NEW.folder_label,
`folder_name` =NEW.folder_name,
`old_parent_id` =NEW.old_parent_id,
`is_system_folder` =NEW.is_system_folder,
`is_created` =NEW.is_created,
`is_updated` =NEW.is_updated,
`is_deleted` =NEW.is_deleted,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`deleted_at` =NEW.deleted_at,
`operation_type` = 'edit';

CREATE TRIGGER `mail_system_add_log` AFTER INSERT ON `mail_system`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_log SET
`mail_id` =NEW.mail_id,
`mail_uid` =NEW.mail_uid,
`staff_id` =NEW.staff_id,
`folder` =NEW.folder,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_unread` =NEW.is_unread,
`is_starred` =NEW.is_starred,
`is_attachment` =NEW.is_attachment,
`is_read_receipt_req` =NEW.is_read_receipt_req,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `mail_system_folder_delete_log` AFTER DELETE ON `mail_system_folder`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_folder_log SET
`folder_id` =OLD.folder_id,
`folder` =OLD.folder,
`staff_id` =OLD.staff_id,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `mail_system_folder_edit_log` AFTER UPDATE ON `mail_system_folder`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mail_system_folder_log SET
`folder_id` =NEW.folder_id,
`folder` =NEW.folder,
`staff_id` =NEW.staff_id,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `mailbox_mails_edit_log` AFTER UPDATE ON `mailbox_mails`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_mails_log SET
`mail_id` =NEW.mail_id,
`uid` =NEW.uid,
`folder_id` =NEW.folder_id,
`mail_no` =NEW.mail_no,
`from_address` =NEW.from_address,
`from_name` =NEW.from_name,
`to_address` =NEW.to_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`folder_label` =NEW.folder_label,
`folder_name` =NEW.folder_name,
`is_unread` =NEW.is_unread,
`is_starred` =NEW.is_starred,
`is_attachment` =NEW.is_attachment,
`is_created` =NEW.is_created,
`is_updated` =NEW.is_updated,
`is_deleted` =NEW.is_deleted,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`deleted_at` =NEW.deleted_at,
`operation_type` = 'edit';

CREATE TRIGGER `main_group_add_log` AFTER INSERT ON `main_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.main_group_log SET
`id`=NEW.id,
`item_group`=NEW.item_group,
`code` =NEW.code,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `main_group_delete_log` AFTER DELETE ON `main_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.main_group_log SET
`id`=OLD.id,
`item_group`=OLD.item_group,
`code` =OLD.code,
`description` =OLD.description,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `mailbox_settings_edit_log` AFTER UPDATE ON `mailbox_settings`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_settings_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`setting_key` =NEW.setting_key,
`setting_value` =NEW.setting_value,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `mailbox_settings_delete_log` AFTER DELETE ON `mailbox_settings`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_settings_log SET
`id`=OLD.id,
`staff_id` =OLD.staff_id,
`setting_key` =OLD.setting_key,
`setting_value` =OLD.setting_value,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `material_process_type_delete_log` AFTER DELETE ON `material_process_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_process_type_log SET
`id`=OLD.id,
`material_process_type` =OLD.material_process_type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `material_process_type_edit_log` AFTER UPDATE ON `material_process_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_process_type_log SET
`id`=NEW.id,
`material_process_type` =NEW.material_process_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `material_specification_add_log` AFTER INSERT ON `material_specification`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_specification_log SET
`id`=NEW.id,
`material` =NEW.material,
`material_specification` =NEW.material_specification,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `main_group_edit_log` AFTER UPDATE ON `main_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.main_group_log SET
`id`=NEW.id,
`item_group`=NEW.item_group,
`code` =NEW.code,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `mailbox_settings_add_log` AFTER INSERT ON `mailbox_settings`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_settings_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`setting_key` =NEW.setting_key,
`setting_value` =NEW.setting_value,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `mailbox_mails_delete_log` AFTER DELETE ON `mailbox_mails`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.mailbox_mails_log SET
`mail_id` =OLD.mail_id,
`uid` =OLD.uid,
`folder_id` =OLD.folder_id,
`mail_no` =OLD.mail_no,
`from_address` =OLD.from_address,
`from_name` =OLD.from_name,
`to_address` =OLD.to_address,
`to_name` =OLD.to_name,
`reply_to` =OLD.reply_to,
`reply_to_name` =OLD.reply_to_name,
`sender_address` =OLD.sender_address,
`sender_name` =OLD.sender_name,
`cc` =OLD.cc,
`bcc` =OLD.bcc,
`subject` =OLD.subject,
`body` =OLD.body,
`attachments` =OLD.attachments,
`received_at` =OLD.received_at,
`folder_label` =OLD.folder_label,
`folder_name` =OLD.folder_name,
`is_unread` =OLD.is_unread,
`is_starred` =OLD.is_starred,
`is_attachment` =OLD.is_attachment,
`is_created` =OLD.is_created,
`is_updated` =OLD.is_updated,
`is_deleted` =OLD.is_deleted,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`deleted_at` =OLD.deleted_at,
`operation_type` = 'delete';

CREATE TRIGGER `module_roles_add_log` AFTER INSERT ON `module_roles`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.module_roles_log SET
`id`=NEW.id,
`title` =NEW.title,
`role_name` =NEW.role_name,
`module_id` =NEW.module_id,
`operation_type` = 'add';

CREATE TRIGGER `module_roles_edit_log` AFTER UPDATE ON `module_roles`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.module_roles_log SET
`id`=NEW.id,
`title` =NEW.title,
`role_name` =NEW.role_name,
`module_id` =NEW.module_id,
`operation_type` = 'edit';

CREATE TRIGGER `order_stage_add_log` AFTER INSERT ON `order_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.order_stage_log SET
`id`=NEW.id,
`order_stage` =NEW.order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `material_specification_delete_log` AFTER DELETE ON `material_specification`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_specification_log SET
`id`=OLD.id,
`material` =OLD.material,
`material_specification` =OLD.material_specification,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `order_stage_delete_log` AFTER DELETE ON `order_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.order_stage_log SET
`id`=OLD.id,
`order_stage` =OLD.order_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `outbox_add_log` AFTER INSERT ON `outbox`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.outbox_log SET
`mail_id` =NEW.mail_id,
`from_address` =NEW.from_address,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_attachment` =NEW.is_attachment,
`is_sent` =NEW.is_sent,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `outbox_delete_log` AFTER DELETE ON `outbox`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.outbox_log SET
`mail_id` =OLD.mail_id,
`from_address` =OLD.from_address,
`from_name` =OLD.from_name,
`from_address` =OLD.from_address,
`to_name` =OLD.to_name,
`reply_to` =OLD.reply_to,
`reply_to_name` =OLD.reply_to_name,
`sender_address` =OLD.sender_address,
`sender_name` =OLD.sender_name,
`cc` =OLD.cc,
`bcc` =OLD.bcc,
`subject` =OLD.subject,
`body` =OLD.body,
`attachments` =OLD.attachments,
`received_at` =OLD.received_at,
`is_attachment` =OLD.is_attachment,
`is_sent` =OLD.is_sent,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `outbox_edit_log` AFTER UPDATE ON `outbox`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.outbox_log SET
`mail_id` =NEW.mail_id,
`from_address` =NEW.from_address,
`from_name` =NEW.from_name,
`from_address` =NEW.from_address,
`to_name` =NEW.to_name,
`reply_to` =NEW.reply_to,
`reply_to_name` =NEW.reply_to_name,
`sender_address` =NEW.sender_address,
`sender_name` =NEW.sender_name,
`cc` =NEW.cc,
`bcc` =NEW.bcc,
`subject` =NEW.subject,
`body` =NEW.body,
`attachments` =NEW.attachments,
`received_at` =NEW.received_at,
`is_attachment` =NEW.is_attachment,
`is_sent` =NEW.is_sent,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `material_specification_edit_log` AFTER UPDATE ON `material_specification`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_specification_log SET
`id`=NEW.id,
`material` =NEW.material,
`material_specification` =NEW.material_specification,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `order_stage_edit_log` AFTER UPDATE ON `order_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.order_stage_log SET
`id`=NEW.id,
`order_stage` =NEW.order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `material_process_type_add_log` AFTER INSERT ON `material_process_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.material_process_type_log SET
`id`=NEW.id,
`material_process_type` =NEW.material_process_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `party_delete_log` AFTER DELETE ON `party`
 FOR EACH ROW INSERT INTO
jaykhodiyar_logs.party_log
SET
	
	`party_id`=OLD.party_id,
	`party_code` =OLD.party_code,
	`party_name` =OLD.party_name,
	`reference_id`=OLD.reference_id,
	`reference_description`=OLD.reference_description,
	`party_type_1`=OLD.party_type_1,
	`party_type_2`=OLD.party_type_2,
	`branch_id`=OLD.branch_id,
	`project`=OLD.project,
	`address`=OLD.address,
	`area`=OLD.area,
	`pincode`=OLD.pincode,
	`city_id`=OLD.city_id,
	`state_id`=OLD.state_id,
	`country_id`=OLD.country_id,
	`phone_no`=OLD.phone_no,
	`fax_no` =OLD.fax_no,
	`email_id`=OLD.email_id,
	`website` =OLD.website,
	`opening_bal` =OLD.opening_bal,
	`credit_limit` =OLD.credit_limit,
	`active` =OLD.active,
	`tin_vat_no` =OLD.tin_vat_no,
	`gst_no` =OLD.gst_no,
	`tin_cst_no` =OLD.tin_cst_no,
	`ecc_no` =OLD.ecc_no,
	`pan_no` =OLD.pan_no,
	`range` =OLD.range,
	`division` =OLD.division,
	`service_tax_no` =OLD.service_tax_no,
	`utr_no` =OLD.utr_no,
	`address_work` =OLD.address_work,
	`w_address`=OLD.w_address,
	`w_city`=OLD.w_city,
	`w_state`=OLD.w_state,
	`w_country`=OLD.w_country,
	`oldw_pincode` =OLD.oldw_pincode,
	`w_email`=OLD.w_email,
	`w_web`=OLD.w_web,
	`w_phone1`=OLD.w_phone1,
	`w_phone2`=OLD.w_phone2,
	`w_phone3`=OLD.w_phone3,
	`w_note`=OLD.w_note,
	`oldPartyType` =OLD.oldPartyType,
	`oldHoliday` =OLD.oldHoliday,
	`oldST_No` =OLD.oldST_No,
	`oldVendorCode` =OLD.oldVendorCode,
	`oldw_fax` =OLD.oldw_fax,
	`oldacct_no` =OLD.oldacct_no,
	`oldReason` =OLD.oldReason,
	`oldAllowMultipalBill` =OLD.oldAllowMultipalBill,
	`oldAddCity` =OLD.oldAddCity,
	`oldCreatedBy` =OLD.oldCreatedBy,
	`oldCreateDate` =OLD.oldCreateDate,
	`oldModifyBy` =OLD.oldModifyBy,
	`oldModiDate` =OLD.oldModiDate,
	`oldPrintFlag` =OLD.oldPrintFlag,
	`oldPT` =OLD.oldPT,
	`oldCPerson` =OLD.oldCPerson,
	`oldOpType` =OLD.oldOpType,
	`oldCompProfile` =OLD.oldCompProfile,
	`oldBranchCode` =OLD.oldBranchCode,
	`oldAreaName` =OLD.oldAreaName,
	`oldCreditLimit` =OLD.oldCreditLimit,
	`oldAcctGrpCode` =OLD.oldAcctGrpCode,
	`oldOpBalDr` =OLD.oldOpBalDr,
	`oldApprovedBy` =OLD.oldApprovedBy,
	`oldApprovedDate` =OLD.oldApprovedDate,
	`oldPStage` =OLD.oldPStage,
	`agent_id`=OLD.agent_id,
	`outstanding_balance`=OLD.outstanding_balance,
	`created_by`=OLD.created_by,
	`updated_by`=OLD.updated_by,
	`created_at`=OLD.created_at,
	`updated_at`=OLD.updated_at,
	`operation_type`='delete';

CREATE TRIGGER `party_detail_delete_log` AFTER DELETE ON `party_detail`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_detail_log SET
`party_detail_id` =OLD.party_detail_id,
`party_id` =OLD.party_id,
`operation_type` = 'delete';

CREATE TRIGGER `party_edit_log` AFTER UPDATE ON `party`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.party_log
SET
	
	`party_id`=NEW.party_id,
	`party_code` =NEW.party_code,
	`party_name` =NEW.party_name,
	`reference_id`=NEW.reference_id,
	`reference_description`=NEW.reference_description,
	`party_type_1`=NEW.party_type_1,
	`party_type_2`=NEW.party_type_2,
	`branch_id`=NEW.branch_id,
	`project`=NEW.project,
	`address`=NEW.address,
	`area`=NEW.area,
	`pincode`=NEW.pincode,
	`city_id`=NEW.city_id,
	`state_id`=NEW.state_id,
	`country_id`=NEW.country_id,
	`phone_no`=NEW.phone_no,
	`fax_no` =NEW.fax_no,
	`email_id`=NEW.email_id,
	`website` =NEW.website,
	`opening_bal` =NEW.opening_bal,
	`credit_limit` =NEW.credit_limit,
	`active` =NEW.active,
	`tin_vat_no` =NEW.tin_vat_no,
	`gst_no` =NEW.gst_no,
	`tin_cst_no` =NEW.tin_cst_no,
	`ecc_no` =NEW.ecc_no,
	`pan_no` =NEW.pan_no,
	`range` =NEW.range,
	`division` =NEW.division,
	`service_tax_no` =NEW.service_tax_no,
	`utr_no` =NEW.utr_no,
	`address_work` =NEW.address_work,
	`w_address`=NEW.w_address,
	`w_city`=NEW.w_city,
	`w_state`=NEW.w_state,
	`w_country`=NEW.w_country,
	`oldw_pincode` =NEW.oldw_pincode,
	`w_email`=NEW.w_email,
	`w_web`=NEW.w_web,
	`w_phone1`=NEW.w_phone1,
	`w_phone2`=NEW.w_phone2,
	`w_phone3`=NEW.w_phone3,
	`w_note`=NEW.w_note,
	`oldPartyType` =NEW.oldPartyType,
	`oldHoliday` =NEW.oldHoliday,
	`oldST_No` =NEW.oldST_No,
	`oldVendorCode` =NEW.oldVendorCode,
	`oldw_fax` =NEW.oldw_fax,
	`oldacct_no` =NEW.oldacct_no,
	`oldReason` =NEW.oldReason,
	`oldAllowMultipalBill` =NEW.oldAllowMultipalBill,
	`oldAddCity` =NEW.oldAddCity,
	`oldCreatedBy` =NEW.oldCreatedBy,
	`oldCreateDate` =NEW.oldCreateDate,
	`oldModifyBy` =NEW.oldModifyBy,
	`oldModiDate` =NEW.oldModiDate,
	`oldPrintFlag` =NEW.oldPrintFlag,
	`oldPT` =NEW.oldPT,
	`oldCPerson` =NEW.oldCPerson,
	`oldOpType` =NEW.oldOpType,
	`oldCompProfile` =NEW.oldCompProfile,
	`oldBranchCode` =NEW.oldBranchCode,
	`oldAreaName` =NEW.oldAreaName,
	`oldCreditLimit` =NEW.oldCreditLimit,
	`oldAcctGrpCode` =NEW.oldAcctGrpCode,
	`oldOpBalDr` =NEW.oldOpBalDr,
	`oldApprovedBy` =NEW.oldApprovedBy,
	`oldApprovedDate` =NEW.oldApprovedDate,
	`oldPStage` =NEW.oldPStage,
	`agent_id`=NEW.agent_id,
	`outstanding_balance`=NEW.outstanding_balance,
	`created_by`=NEW.created_by,
	`updated_by`=NEW.updated_by,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type`='edit';

CREATE TRIGGER `party_add_log` AFTER INSERT ON `party`
 FOR EACH ROW INSERT INTO
  jaykhodiyar_logs.party_log
SET
	`party_id`=NEW.party_id,
	`party_code` =NEW.party_code,
	`party_name` =NEW.party_name,
	`reference_id`=NEW.reference_id,
	`reference_description`=NEW.reference_description,
	`party_type_1`=NEW.party_type_1,
	`party_type_2`=NEW.party_type_2,
	`branch_id`=NEW.branch_id,
	`project`=NEW.project,
	`address`=NEW.address,
	`area`=NEW.area,
	`pincode`=NEW.pincode,
	`city_id`=NEW.city_id,
	`state_id`=NEW.state_id,
	`country_id`=NEW.country_id,
	`phone_no`=NEW.phone_no,
	`fax_no` =NEW.fax_no,
	`email_id`=NEW.email_id,
	`website` =NEW.website,
	`opening_bal` =NEW.opening_bal,
	`credit_limit` =NEW.credit_limit,
	`active` =NEW.active,
	`tin_vat_no` =NEW.tin_vat_no,
	`gst_no` =NEW.gst_no,
	`tin_cst_no` =NEW.tin_cst_no,
	`ecc_no` =NEW.ecc_no,
	`pan_no` =NEW.pan_no,
	`range` =NEW.range,
	`division` =NEW.division,
	`service_tax_no` =NEW.service_tax_no,
	`utr_no` =NEW.utr_no,
	`address_work` =NEW.address_work,
	`w_address`=NEW.w_address,
	`w_city`=NEW.w_city,
	`w_state`=NEW.w_state,
	`w_country`=NEW.w_country,
	`oldw_pincode` =NEW.oldw_pincode,
	`w_email`=NEW.w_email,
	`w_web`=NEW.w_web,
	`w_phone1`=NEW.w_phone1,
	`w_phone2`=NEW.w_phone2,
	`w_phone3`=NEW.w_phone3,
	`w_note`=NEW.w_note,
	`oldPartyType` =NEW.oldPartyType,
	`oldHoliday` =NEW.oldHoliday,
	`oldST_No` =NEW.oldST_No,
	`oldVendorCode` =NEW.oldVendorCode,
	`oldw_fax` =NEW.oldw_fax,
	`oldacct_no` =NEW.oldacct_no,
	`oldReason` =NEW.oldReason,
	`oldAllowMultipalBill` =NEW.oldAllowMultipalBill,
	`oldAddCity` =NEW.oldAddCity,
	`oldCreatedBy` =NEW.oldCreatedBy,
	`oldCreateDate` =NEW.oldCreateDate,
	`oldModifyBy` =NEW.oldModifyBy,
	`oldModiDate` =NEW.oldModiDate,
	`oldPrintFlag` =NEW.oldPrintFlag,
	`oldPT` =NEW.oldPT,
	`oldCPerson` =NEW.oldCPerson,
	`oldOpType` =NEW.oldOpType,
	`oldCompProfile` =NEW.oldCompProfile,
	`oldBranchCode` =NEW.oldBranchCode,
	`oldAreaName` =NEW.oldAreaName,
	`oldCreditLimit` =NEW.oldCreditLimit,
	`oldAcctGrpCode` =NEW.oldAcctGrpCode,
	`oldOpBalDr` =NEW.oldOpBalDr,
	`oldApprovedBy` =NEW.oldApprovedBy,
	`oldApprovedDate` =NEW.oldApprovedDate,
	`oldPStage` =NEW.oldPStage,
	`agent_id`=NEW.agent_id,
	`outstanding_balance`=NEW.outstanding_balance,
	`created_by`=NEW.created_by,
	`updated_by`=NEW.updated_by,
	`created_at`=NEW.created_at,
	`updated_at`=NEW.updated_at,
	`operation_type`='add';

CREATE TRIGGER `party_old_delete_log` AFTER DELETE ON `party_old`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_old_log SET
`party_code` =OLD.party_code,
`party_name` =OLD.party_name,
`address` =OLD.address,
`pincode` =OLD.pincode,
`phone_no` =OLD.phone_no,
`fax_no` =OLD.fax_no,
`website` =OLD.website,
`email_id` =OLD.email_id,
`tin_cst_no` =OLD.tin_cst_no,
`service_tax_no` =OLD.service_tax_no,
`ecc_no` =OLD.ecc_no,
`pan_no` =OLD.pan_no,
`active` =OLD.active,
`range` =OLD.range,
`division` =OLD.division,
`w_address` =OLD.w_address,
`w_phone1` =OLD.w_phone1,
`w_email` =OLD.w_email,
`w_web` =OLD.w_web,
`opening_bal` =OLD.opening_bal,
`operation_type` = 'delete';

CREATE TRIGGER `module_roles_delete_log` AFTER DELETE ON `module_roles`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.module_roles_log SET
`id`=OLD.id,
`title` =OLD.title,
`role_name` =OLD.role_name,
`module_id` =OLD.module_id,
`operation_type` = 'delete';

CREATE TRIGGER `party_detail_add_log` AFTER INSERT ON `party_detail`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_detail_log SET
`party_detail_id` =NEW.party_detail_id,
`party_id` =NEW.party_id,
`operation_type` = 'add';

CREATE TRIGGER `party_old_add_log` AFTER INSERT ON `party_old`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_old_log SET
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`fax_no` =NEW.fax_no,
`website` =NEW.website,
`email_id` =NEW.email_id,
`tin_cst_no` =NEW.tin_cst_no,
`service_tax_no` =NEW.service_tax_no,
`ecc_no` =NEW.ecc_no,
`pan_no` =NEW.pan_no,
`active` =NEW.active,
`range` =NEW.range,
`division` =NEW.division,
`w_address` =NEW.w_address,
`w_phone1` =NEW.w_phone1,
`w_email` =NEW.w_email,
`w_web` =NEW.w_web,
`opening_bal` =NEW.opening_bal,
`operation_type` = 'add';

CREATE TRIGGER `party_detail_edit_log` AFTER UPDATE ON `party_detail`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_detail_log SET
`party_detail_id` =NEW.party_detail_id,
`party_id` =NEW.party_id,
`operation_type` = 'edit';

CREATE TRIGGER `party_type_1_add_log` AFTER INSERT ON `party_type_1`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_1_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `party_type_2_add_log` AFTER INSERT ON `party_type_2`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_2_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `party_type_2_edit_log` AFTER UPDATE ON `party_type_2`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_2_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `payment_add_log` AFTER INSERT ON `payment`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.payment_log SET
`payment_id` =NEW.payment_id,
`party_id` =NEW.party_id,
`payment_date` =NEW.payment_date,
`amount` =NEW.amount,
`payment_type` =NEW.payment_type,
`payment_note` =NEW.payment_note,
`created_by` =NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `payment_delete_log` AFTER DELETE ON `payment`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.payment_log SET
`payment_id` =OLD.payment_id,
`party_id` =OLD.party_id,
`payment_date` =OLD.payment_date,
`amount` =OLD.amount,
`payment_type` =OLD.payment_type,
`payment_note` =OLD.payment_note,
`created_by` =OLD.created_by,
`updated_by` =OLD.updated_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `payment_edit_log` AFTER UPDATE ON `payment`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.payment_log SET
`payment_id` =NEW.payment_id,
`party_id` =NEW.party_id,
`payment_date` =NEW.payment_date,
`amount` =NEW.amount,
`payment_type` =NEW.payment_type,
`payment_note` =NEW.payment_note,
`created_by` =NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `party_type_2_delete_log` AFTER DELETE ON `party_type_2`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_2_log SET
`id`=OLD.id,
`type` =OLD.type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `party_type_1_delete_log` AFTER DELETE ON `party_type_1`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_1_log SET
`id`=OLD.id,
`type` =OLD.type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `priority_add_log` AFTER INSERT ON `priority`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.priority_log SET
`id`=NEW.id,
`priority` =NEW.priority,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `party_old_edit_log` AFTER UPDATE ON `party_old`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_old_log SET
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`fax_no` =NEW.fax_no,
`website` =NEW.website,
`email_id` =NEW.email_id,
`tin_cst_no` =NEW.tin_cst_no,
`service_tax_no` =NEW.service_tax_no,
`ecc_no` =NEW.ecc_no,
`pan_no` =NEW.pan_no,
`active` =NEW.active,
`range` =NEW.range,
`division` =NEW.division,
`w_address` =NEW.w_address,
`w_phone1` =NEW.w_phone1,
`w_email` =NEW.w_email,
`w_web` =NEW.w_web,
`opening_bal` =NEW.opening_bal,
`operation_type` = 'edit';

CREATE TRIGGER `priority_delete_log` AFTER DELETE ON `priority`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.priority_log SET
`id`=OLD.id,
`priority` =OLD.priority,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `party_type_1_edit_log` AFTER UPDATE ON `party_type_1`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.party_type_1_log SET
`id`=NEW.id,
`type` =NEW.type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `proforma_invoice_cal_code_definition_add_log` AFTER INSERT ON `proforma_invoice_cal_code_definition`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `proforma_invoice_cal_code_definition_delete_log` AFTER DELETE ON `proforma_invoice_cal_code_definition`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_cal_code_definition_log SET
`id`=OLD.id,
`billing_terms_detail_id` =OLD.billing_terms_detail_id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`is_first_element` =OLD.is_first_element,
`cal_operation` =OLD.cal_operation,
`cal_code_id` =OLD.cal_code_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `proforma_invoice_billing_terms_add_log` AFTER INSERT ON `proforma_invoice_billing_terms`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_billing_terms_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `priority_edit_log` AFTER UPDATE ON `priority`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.priority_log SET
`id`=NEW.id,
`priority` =NEW.priority,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `proforma_invoice_cal_code_definition_edit_log` AFTER UPDATE ON `proforma_invoice_cal_code_definition`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `proforma_invoice_billing_terms_edit_log` AFTER UPDATE ON `proforma_invoice_billing_terms`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_billing_terms_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `proforma_invoice_logins_add_log` AFTER INSERT ON `proforma_invoice_logins`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_logins_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `proforma_invoice_items_edit_log` AFTER UPDATE ON `proforma_invoice_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `proforma_invoice_items_add_log` AFTER INSERT ON `proforma_invoice_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `proforma_invoice_logins_edit_log` AFTER UPDATE ON `proforma_invoice_logins`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_logins_log SET
`id`=NEW.id,
`proforma_invoice_id` =NEW.proforma_invoice_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `project_add_log` AFTER INSERT ON `project`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.project_log SET
`id`=NEW.id,
`project` =NEW.project,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `project_delete_log` AFTER DELETE ON `project`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.project_log SET
`id`=OLD.id,
`project` =OLD.project,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `proforma_invoice_items_delete_log` AFTER DELETE ON `proforma_invoice_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_items_log SET
`id`=OLD.id,
`item_id` =OLD.item_id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`item_code` =OLD.item_code,
`item_category_id` =OLD.item_category_id,
`item_name` =OLD.item_name,
`item_desc` =OLD.item_desc,
`item_description` =OLD.item_description,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`drawing_revision` =OLD.drawing_revision,
`uom_id`=OLD.uom_id,
`uom` =OLD.uom,
`total_amount` =OLD.total_amount,
`item_note` =OLD.item_note,
`quantity` =OLD.quantity,
`disc_per` =OLD.disc_per,
`rate` =OLD.rate,
`disc_value` =OLD.disc_value,
`amount` =OLD.amount,
`net_amount` =OLD.net_amount,
`item_status_id` =OLD.item_status_id,
`item_status` =OLD.item_status,
`cust_part_no` =OLD.cust_part_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `proforma_invoices_delete_log` AFTER DELETE ON `proforma_invoices`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoices_log SET
`id`=OLD.id,
`quotation_id` =OLD.quotation_id,
`proforma_invoice_no` =OLD.proforma_invoice_no,
`quotation_no` =OLD.quotation_no,
`sales_to_party_id` =OLD.sales_to_party_id,
`ship_to_party_id` =OLD.ship_to_party_id,
`branch_id` =OLD.branch_id,
`billing_terms_id` =OLD.billing_terms_id,
`sales_order_date` =OLD.sales_order_date,
`committed_date` =OLD.committed_date,
`currency_id` =OLD.currency_id,
`project_id` =OLD.project_id,
`kind_attn_id` =OLD.kind_attn_id,
`contact_no` =OLD.contact_no,
`cust_po_no` =OLD.cust_po_no,
`email_id` =OLD.email_id,
`po_date` =OLD.po_date,
`sales_order_pref_id` =OLD.sales_order_pref_id,
`conversation_rate` =OLD.conversation_rate,
`sales_id` =OLD.sales_id,
`sales_order_stage_id` =OLD.sales_order_stage_id,
`sales_order_status_id` =OLD.sales_order_status_id,
`order_type` =OLD.order_type,
`lead_provider_id` =OLD.lead_provider_id,
`buyer_detail_id` =OLD.buyer_detail_id,
`note_detail_id` =OLD.note_detail_id,
`login_detail_id` =OLD.login_detail_id,
`transportation_by_id` =OLD.transportation_by_id,
`foundation_drawing_required_id` =OLD.foundation_drawing_required_id,
`loading_by_id` =OLD.loading_by_id,
`inspection_required_id` =OLD.inspection_required_id,
`unloading_by_id` =OLD.unloading_by_id,
`erection_commissioning_id` =OLD.erection_commissioning_id,
`road_insurance_by_id` =OLD.road_insurance_by_id,
`for_required_id` =OLD.for_required_id,
`review_date` =OLD.review_date,
`mach_deli_min_weeks` =OLD.mach_deli_min_weeks,
`mach_deli_max_weeks` =OLD.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =OLD.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =OLD.mach_deli_max_weeks_date,
`sales_order_validaty` =OLD.sales_order_validaty,
`sales_order_quotation_delivery` =OLD.sales_order_quotation_delivery,
`mode_of_shipment_name` =OLD.mode_of_shipment_name,
`mode_of_shipment_truck_number` =OLD.mode_of_shipment_truck_number,
`supplier_payment_terms` =OLD.supplier_payment_terms,
`received_payment` =OLD.received_payment,
`received_payment_date` =OLD.received_payment_date,
`received_payment_type` =OLD.received_payment_type,
`terms_condition_purchase` =OLD.terms_condition_purchase,
`note` =OLD.note,
`isApproved` =OLD.isApproved,
`pdf_url` =OLD.pdf_url,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`created_by` =OLD.created_by,
`operation_type` = 'delete';

CREATE TRIGGER `proforma_invoice_billing_terms_delete_log` AFTER DELETE ON `proforma_invoice_billing_terms`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_billing_terms_log SET
`id`=OLD.id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`cal_code` =OLD.cal_code,
`narration` =OLD.narration,
`cal_definition` =OLD.cal_definition,
`percentage` =OLD.percentage,
`value` =OLD.value,
`gl_acc` =OLD.gl_acc,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `purchase_invoice_delete_log` AFTER DELETE ON `purchase_invoice`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_log SET
`invoice_id` =OLD.invoice_id,
`invoice_no` =OLD.invoice_no,
`invoice_date` =OLD.invoice_date,
`supplier_id` =OLD.supplier_id,
`address` =OLD.address,
`note` =OLD.note,
`created_by` =OLD.created_by,
`created_at` =OLD.created_at,
`operation_type` = 'delete';

CREATE TRIGGER `project_edit_log` AFTER UPDATE ON `project`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.project_log SET
`id`=NEW.id,
`project` =NEW.project,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `purchase_invoice_add_log` AFTER INSERT ON `purchase_invoice`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_log SET
`invoice_id` =NEW.invoice_id,
`invoice_no` =NEW.invoice_no,
`invoice_date` =NEW.invoice_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`operation_type` = 'add';

CREATE TRIGGER `proforma_invoice_logins_delete_log` AFTER DELETE ON `proforma_invoice_logins`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.proforma_invoice_logins_log SET
`id`=OLD.id,
`proforma_invoice_id` =OLD.proforma_invoice_id,
`created_by` =OLD.created_by,
`created_by_id` =OLD.created_by_id,
`created_date_time` =OLD.created_date_time,
`last_modified_by` =OLD.last_modified_by,
`last_modified_by_id` =OLD.last_modified_by_id,
`modified_date_time` =OLD.modified_date_time,
`prepared_by` =OLD.prepared_by,
`prepared_by_id` =OLD.prepared_by_id,
`prepared_date_time` =OLD.prepared_date_time,
`approved_by` =OLD.approved_by,
`approved_by_id` =OLD.approved_by_id,
`approved_date_time` =OLD.approved_date_time,
`stage_id` =OLD.stage_id,
`stage` =OLD.stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `purchase_invoice_items_delete_log` AFTER DELETE ON `purchase_invoice_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_items_log SET
`id`=OLD.id,
`invoice_id` =OLD.invoice_id,
`item_id` =OLD.item_id,
`quantity` =OLD.quantity,
`item_data` =OLD.item_data,
`invoice_flag` =OLD.invoice_flag,
`quotation_flag` =OLD.quotation_flag,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `purchase_invoice_items_add_log` AFTER INSERT ON `purchase_invoice_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_items_log SET
`id`=NEW.id,
`invoice_id` =NEW.invoice_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `purchase_invoice_edit_log` AFTER UPDATE ON `purchase_invoice`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_log SET
`invoice_id` =NEW.invoice_id,
`invoice_no` =NEW.invoice_no,
`invoice_date` =NEW.invoice_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`operation_type` = 'edit';

CREATE TRIGGER `purchase_order_add_log` AFTER INSERT ON `purchase_order`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_log SET
`order_id` =NEW.order_id,
`order_no` =NEW.order_no,
`order_date` =NEW.order_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `purchase_order_delete_log` AFTER DELETE ON `purchase_order`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_log SET
`order_id` =OLD.order_id,
`order_no` =OLD.order_no,
`order_date` =OLD.order_date,
`supplier_id` =OLD.supplier_id,
`address` =OLD.address,
`note` =OLD.note,
`created_by` =OLD.created_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `purchase_invoice_items_edit_log` AFTER UPDATE ON `purchase_invoice_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_invoice_items_log SET
`id`=NEW.id,
`invoice_id` =NEW.invoice_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `purchase_order_items_add_log` AFTER INSERT ON `purchase_order_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_items_log SET
`id`=NEW.id,
`order_id` =NEW.order_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `purchase_order_edit_log` AFTER UPDATE ON `purchase_order`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_log SET
`order_id` =NEW.order_id,
`order_no` =NEW.order_no,
`order_date` =NEW.order_date,
`supplier_id` =NEW.supplier_id,
`address` =NEW.address,
`note` =NEW.note,
`created_by` =NEW.created_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `quotation_items_edit_log` AFTER UPDATE ON `quotation_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`quotation_id` =NEW.quotation_id,
`item_code` =NEW.item_code,
`item_name` =NEW.item_name,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `quotation_reason_add_log` AFTER INSERT ON `quotation_reason`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_reason_log SET
`id`=NEW.id,
`quotation_reason` =NEW.quotation_reason,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `quotation_reason_delete_log` AFTER DELETE ON `quotation_reason`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_reason_log SET
`id`=OLD.id,
`quotation_reason` =OLD.quotation_reason,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `quotation_reason_edit_log` AFTER UPDATE ON `quotation_reason`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_reason_log SET
`id`=NEW.id,
`quotation_reason` =NEW.quotation_reason,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `purchase_order_items_edit_log` AFTER UPDATE ON `purchase_order_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_items_log SET
`id`=NEW.id,
`order_id` =NEW.order_id,
`item_id` =NEW.item_id,
`quantity` =NEW.quantity,
`item_data` =NEW.item_data,
`invoice_flag` =NEW.invoice_flag,
`quotation_flag` =NEW.quotation_flag,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `quotation_items_delete_log` AFTER DELETE ON `quotation_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_items_log SET
`id`=OLD.id,
`item_id` =OLD.item_id,
`quotation_id` =OLD.quotation_id,
`item_code` =OLD.item_code,
`item_name` =OLD.item_name,
`item_description` =OLD.item_description,
`item_desc`=OLD.item_desc,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`drawing_revision` =OLD.drawing_revision,
`uom_id`=OLD.uom_id,
`uom` =OLD.uom,
`total_amount` =OLD.total_amount,
`item_note` =OLD.item_note,
`quantity` =OLD.quantity,
`disc_per` =OLD.disc_per,
`rate` =OLD.rate,
`disc_value` =OLD.disc_value,
`amount` =OLD.amount,
`net_amount` =OLD.net_amount,
`item_status_id` =OLD.item_status_id,
`item_status` =OLD.item_status,
`cust_part_no` =OLD.cust_part_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `purchase_order_items_delete_log` AFTER DELETE ON `purchase_order_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.purchase_order_items_log SET
`id`=OLD.id,
`order_id` =OLD.order_id,
`item_id` =OLD.item_id,
`quantity` =OLD.quantity,
`item_data` =OLD.item_data,
`invoice_flag` =OLD.invoice_flag,
`quotation_flag` =OLD.quotation_flag,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `quotation_status_add_log` AFTER INSERT ON `quotation_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_status_log SET
`id`=NEW.id,
`quotation_status` =NEW.quotation_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `quotation_stage_delete_log` AFTER DELETE ON `quotation_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_stage_log SET
`id`=OLD.id,
`quotation_stage` =OLD.quotation_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `quotation_stage_add_log` AFTER INSERT ON `quotation_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_stage_log SET
`id`=NEW.id,
`quotation_stage` =NEW.quotation_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `quotation_items_add_log` AFTER INSERT ON `quotation_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`quotation_id` =NEW.quotation_id,
`item_code` =NEW.item_code,
`item_name` =NEW.item_name,
`item_description` =NEW.item_description,
`item_desc`=NEW.item_desc,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `quotation_stage_edit_log` AFTER UPDATE ON `quotation_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_stage_log SET
`id`=NEW.id,
`quotation_stage` =NEW.quotation_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `quotation_type_add_log` AFTER INSERT ON `quotation_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_type_log SET
`id`=NEW.id,
`quotation_type` =NEW.quotation_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `quotation_status_edit_log` AFTER UPDATE ON `quotation_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_status_log SET
`id`=NEW.id,
`quotation_status` =NEW.quotation_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `real_leave_add_log` AFTER INSERT ON `real_leave`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.real_leave_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`created_at` =NEW.created_at,
`operation_type` = 'add';

CREATE TRIGGER `real_leave_delete_log` AFTER DELETE ON `real_leave`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.real_leave_log SET
`leave_id`=OLD.leave_id,
`employee_id` =OLD.employee_id,
`from_date` =OLD.from_date,
`to_date` =OLD.to_date,
`leave_type` =OLD.leave_type,
`created_at` =OLD.created_at,
`operation_type` = 'delete';

CREATE TRIGGER `real_leave_edit_log` AFTER UPDATE ON `real_leave`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.real_leave_log SET
`leave_id`=NEW.leave_id,
`employee_id` =NEW.employee_id,
`from_date` =NEW.from_date,
`to_date` =NEW.to_date,
`leave_type` =NEW.leave_type,
`created_at` =NEW.created_at,
`operation_type` = 'edit';

CREATE TRIGGER `reference_add_log` AFTER INSERT ON `reference`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reference_log SET
`id`=NEW.id,
`reference` =NEW.reference,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `quotation_type_delete_log` AFTER DELETE ON `quotation_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_type_log SET
`id`=OLD.id,
`quotation_type` =OLD.quotation_type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `reference_delete_log` AFTER DELETE ON `reference`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reference_log SET
`id`=OLD.id,
`reference` =OLD.reference,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `quotation_type_edit_log` AFTER UPDATE ON `quotation_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_type_log SET
`id`=NEW.id,
`quotation_type` =NEW.quotation_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `quotations_add_log` AFTER INSERT ON `quotations`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotations_log SET
`id`=NEW.id,
`enquiry_id` =NEW.enquiry_id,
`revised_from_id` =NEW.revised_from_id,
`party_id` =NEW.party_id,
`enquiry_no` =NEW.enquiry_no,
`quotation_no` =NEW.quotation_no,
`quotation_stage_id` =NEW.quotation_stage_id,
`quotation_type_id` =NEW.quotation_type_id,
`quotation_status_id` =NEW.quotation_status_id,
`sales_type_id` =NEW.sales_type_id,
`sales_id` =NEW.sales_id,
`action` =NEW.action,
`currency_id` =NEW.currency_id,
`rev` =NEW.rev,
`rev_date` =NEW.rev_date,
`quotation_note` =NEW.quotation_note,
`reason` =NEW.reason,
`reason_remark` =NEW.reason_remark,
`review_date_check` =NEW.review_date_check,
`review_date` =NEW.review_date,
`enquiry_date` =NEW.enquiry_date,
`quotation_date` =NEW.quotation_date,
`due_date_check` =NEW.due_date_check,
`oldEnquiryNo` =NEW.oldEnquiryNo,
`oldSubject` =NEW.oldSubject,
`oldPreparedBy` =NEW.oldPreparedBy,
`oldApprovedBy` =NEW.oldApprovedBy,
`oldSrNo` =NEW.oldSrNo,
`oldExpr1` =NEW.oldExpr1,
`oldExpr2` =NEW.oldExpr2,
`oldEnquiryDate` =NEW.oldEnquiryDate,
`oldKindAttn` =NEW.oldKindAttn,
`oldEnquiryStatus` =NEW.oldEnquiryStatus,
`oldEnqRef` =NEW.oldEnqRef,
`due_date` =NEW.due_date,
`conv_rate` =NEW.conv_rate,
`reference_date` =NEW.reference_date,
`kind_attn_id` =NEW.kind_attn_id,
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`reference_id` =NEW.reference_id,
`reference_description` =NEW.reference_description,
`address` =NEW.address,
`agent_id` =NEW.agent_id,
`city_id` =NEW.city_id,
`state_id` =NEW.state_id,
`country_id` =NEW.country_id,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`project` =NEW.project,
`phone_no` =NEW.phone_no,
`pincode` =NEW.pincode,
`website` =NEW.website,
`contact_person_name` =NEW.contact_person_name,
`contact_person_contact_no` =NEW.contact_person_contact_no,
`contact_person_email_id` =NEW.contact_person_email_id,
`subject` =NEW.subject,
`header` =NEW.header,
`footer` =NEW.footer,
`cc_to` =NEW.cc_to,
`authorised_by` =NEW.authorised_by,
`reference_note` =NEW.reference_note,
`item_dec_title` =NEW.item_dec_title,
`addon_title` =NEW.addon_title,
`specification_1` =NEW.specification_1,
`specification_2` =NEW.specification_2,
`specification_3` =NEW.specification_3,
`send_from_post` =NEW.send_from_post,
`send_from_post_name` =NEW.send_from_post_name,
`pdf_url` =NEW.pdf_url,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`operation_type` = 'add';

CREATE TRIGGER `reference_edit_log` AFTER UPDATE ON `reference`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reference_log SET
`id`=NEW.id,
`reference` =NEW.reference,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `reminder_add_log` AFTER INSERT ON `reminder`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reminder_log SET
`reminder_id` =NEW.reminder_id,
`user_id` =NEW.user_id,
`remind` =NEW.remind,
`reminder_date` =NEW.reminder_date,
`is_notified` =NEW.is_notified,
`assigned_to` =NEW.assigned_to,
`status` =NEW.status,
`priority` =NEW.priority,
`created_date` =NEW.created_date,
`last_updated_date` =NEW.last_updated_date,
`operation_type` = 'add';

CREATE TRIGGER `reminder_delete_log` AFTER DELETE ON `reminder`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reminder_log SET
`reminder_id` =OLD.reminder_id,
`user_id` =OLD.user_id,
`remind` =OLD.remind,
`reminder_date` =OLD.reminder_date,
`is_notified` =OLD.is_notified,
`assigned_to` =OLD.assigned_to,
`status` =OLD.status,
`priority` =OLD.priority,
`created_date` =OLD.created_date,
`last_updated_date` =OLD.last_updated_date,
`operation_type` = 'delete';

CREATE TRIGGER `road_insurance_by_edit_log` AFTER UPDATE ON `road_insurance_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.road_insurance_by_log SET
`id`=NEW.id,
`road_insurance_by` =NEW.road_insurance_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `road_insurance_by_delete_log` AFTER DELETE ON `road_insurance_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.road_insurance_by_log SET
`id`=OLD.id,
`road_insurance_by` =OLD.road_insurance_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `quotation_status_delete_log` AFTER DELETE ON `quotation_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.quotation_status_log SET
`id`=OLD.id,
`quotation_status` =OLD.quotation_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `road_insurance_by_add_log` AFTER INSERT ON `road_insurance_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.road_insurance_by_log SET
`id`=NEW.id,
`road_insurance_by` =NEW.road_insurance_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_action_add_log` AFTER INSERT ON `sales_action`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_action_log SET
`id`=NEW.id,
`sales_action` =NEW.sales_action,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_action_delete_log` AFTER DELETE ON `sales_action`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_action_log SET
`id`=OLD.id,
`sales_action` =OLD.sales_action,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_action_edit_log` AFTER UPDATE ON `sales_action`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_action_log SET
`id`=NEW.id,
`sales_action` =NEW.sales_action,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `reminder_edit_log` AFTER UPDATE ON `reminder`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.reminder_log SET
`reminder_id` =NEW.reminder_id,
`user_id` =NEW.user_id,
`remind` =NEW.remind,
`reminder_date` =NEW.reminder_date,
`is_notified` =NEW.is_notified,
`assigned_to` =NEW.assigned_to,
`status` =NEW.status,
`priority` =NEW.priority,
`created_date` =NEW.created_date,
`last_updated_date` =NEW.last_updated_date,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_billing_terms_add_log` AFTER INSERT ON `sales_order_billing_terms`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_billing_terms_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_delete_log` AFTER DELETE ON `sales`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_log SET
`id`=OLD.id,
`sales` =OLD.sales,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_add_log` AFTER INSERT ON `sales_order`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_log SET
`id`=NEW.id,
`quatation_id` =NEW.quatation_id,
`quotation_id` =NEW.quotation_id,
`sales_order_no` =NEW.sales_order_no,
`sales_to_party_id` =NEW.sales_to_party_id,
`ship_to_party_id` =NEW.ship_to_party_id,
`branch_id` =NEW.branch_id,
`billing_terms_id` =NEW.billing_terms_id,
`sales_order_date` =NEW.sales_order_date,
`committed_date` =NEW.committed_date,
`currency_id` =NEW.currency_id,
`project_id` =NEW.project_id,
`kind_attn_id` =NEW.kind_attn_id,
`contact_no` =NEW.contact_no,
`cust_po_no` =NEW.cust_po_no,
`email_id` =NEW.email_id,
`po_date` =NEW.po_date,
`sales_order_pref_id` =NEW.sales_order_pref_id,
`conversation_rate` =NEW.conversation_rate,
`sales_id` =NEW.sales_id,
`sales_order_stage_id` =NEW.sales_order_stage_id,
`sales_order_status_id` =NEW.sales_order_status_id,
`order_type` =NEW.order_type,
`lead_provider_id` =NEW.lead_provider_id,
`buyer_detail_id` =NEW.buyer_detail_id,
`note_detail_id` =NEW.note_detail_id,
`login_detail_id` =NEW.login_detail_id,
`transportation_by_id` =NEW.transportation_by_id,
`transport_amount` =NEW.transport_amount,
`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
`loading_by_id` =NEW.loading_by_id,
`inspection_required_id` =NEW.inspection_required_id,
`unloading_by_id` =NEW.unloading_by_id,
`erection_commissioning_id` =NEW.erection_commissioning_id,
`road_insurance_by_id` =NEW.road_insurance_by_id,
`for_required_id` =NEW.for_required_id,
`review_date` =NEW.review_date,
`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
`sales_order_validaty` =NEW.sales_order_validaty,
`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
`mode_of_shipment_name` =NEW.mode_of_shipment_name,
`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
`supplier_payment_terms` =NEW.supplier_payment_terms,
`received_payment` =NEW.received_payment,
`received_payment_date` =NEW.received_payment_date,
`received_payment_type` =NEW.received_payment_type,
`terms_condition_purchase` =NEW.terms_condition_purchase,
`isApproved` =NEW.isApproved,
`pdf_url` =NEW.pdf_url,
`taxes_data` =NEW.taxes_data,
`more_items_data` =NEW.more_items_data,
`due_date` =NEW.due_date,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`created_by` =NEW.created_by,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_billing_terms_edit_log` AFTER UPDATE ON `sales_order_billing_terms`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_billing_terms_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`cal_code` =NEW.cal_code,
`narration` =NEW.narration,
`cal_definition` =NEW.cal_definition,
`percentage` =NEW.percentage,
`value` =NEW.value,
`gl_acc` =NEW.gl_acc,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_add_log` AFTER INSERT ON `sales`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_log SET
`id`=NEW.id,
`sales` =NEW.sales,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_buyers_add_log` AFTER INSERT ON `sales_order_buyers`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_buyers_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_id` =NEW.party_id,
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`city_id` =NEW.city_id,
`state_id` =NEW.state_id,
`country_id` =NEW.country_id,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`gst_no` =NEW.gst_no,
`pan_no` =NEW.pan_no,
`tin_vat_no` =NEW.tin_vat_no,
`tin_cst_no` =NEW.tin_cst_no,
`ecc_no` =NEW.ecc_no,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_buyers_edit_log` AFTER UPDATE ON `sales_order_buyers`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_buyers_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_id` =NEW.party_id,
`party_code` =NEW.party_code,
`party_name` =NEW.party_name,
`address` =NEW.address,
`city_id` =NEW.city_id,
`state_id` =NEW.state_id,
`country_id` =NEW.country_id,
`fax_no` =NEW.fax_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`gst_no` =NEW.gst_no,
`pan_no` =NEW.pan_no,
`tin_vat_no` =NEW.tin_vat_no,
`tin_cst_no` =NEW.tin_cst_no,
`ecc_no` =NEW.ecc_no,
`pincode` =NEW.pincode,
`phone_no` =NEW.phone_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_billing_terms_delete_log` AFTER DELETE ON `sales_order_billing_terms`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_billing_terms_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`cal_code` =OLD.cal_code,
`narration` =OLD.narration,
`cal_definition` =OLD.cal_definition,
`percentage` =OLD.percentage,
`value` =OLD.value,
`gl_acc` =OLD.gl_acc,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_buyers_delete_log` AFTER DELETE ON `sales_order_buyers`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_buyers_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`party_id` =OLD.party_id,
`party_code` =OLD.party_code,
`party_name` =OLD.party_name,
`address` =OLD.address,
`city_id` =OLD.city_id,
`state_id` =OLD.state_id,
`country_id` =OLD.country_id,
`fax_no` =OLD.fax_no,
`email_id` =OLD.email_id,
`website` =OLD.website,
`gst_no` =OLD.gst_no,
`pan_no` =OLD.pan_no,
`tin_vat_no` =OLD.tin_vat_no,
`tin_cst_no` =OLD.tin_cst_no,
`ecc_no` =OLD.ecc_no,
`pincode` =OLD.pincode,
`phone_no` =OLD.phone_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_delete_log` AFTER DELETE ON `sales_order`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_log SET
`id`=OLD.id,
`quatation_id` =OLD.quatation_id,
`quotation_id` =OLD.quotation_id,
`sales_order_no` =OLD.sales_order_no,
`sales_to_party_id` =OLD.sales_to_party_id,
`ship_to_party_id` =OLD.ship_to_party_id,
`branch_id` =OLD.branch_id,
`billing_terms_id` =OLD.billing_terms_id,
`sales_order_date` =OLD.sales_order_date,
`committed_date` =OLD.committed_date,
`currency_id` =OLD.currency_id,
`project_id` =OLD.project_id,
`kind_attn_id` =OLD.kind_attn_id,
`contact_no` =OLD.contact_no,
`cust_po_no` =OLD.cust_po_no,
`email_id` =OLD.email_id,
`po_date` =OLD.po_date,
`sales_order_pref_id` =OLD.sales_order_pref_id,
`conversation_rate` =OLD.conversation_rate,
`sales_id` =OLD.sales_id,
`sales_order_stage_id` =OLD.sales_order_stage_id,
`sales_order_status_id` =OLD.sales_order_status_id,
`order_type` =OLD.order_type,
`lead_provider_id` =OLD.lead_provider_id,
`buyer_detail_id` =OLD.buyer_detail_id,
`note_detail_id` =OLD.note_detail_id,
`login_detail_id` =OLD.login_detail_id,
`transportation_by_id` =OLD.transportation_by_id,
`transport_amount` =OLD.transport_amount,
`foundation_drawing_required_id` =OLD.foundation_drawing_required_id,
`loading_by_id` =OLD.loading_by_id,
`inspection_required_id` =OLD.inspection_required_id,
`unloading_by_id` =OLD.unloading_by_id,
`erection_commissioning_id` =OLD.erection_commissioning_id,
`road_insurance_by_id` =OLD.road_insurance_by_id,
`for_required_id` =OLD.for_required_id,
`review_date` =OLD.review_date,
`mach_deli_min_weeks` =OLD.mach_deli_min_weeks,
`mach_deli_max_weeks` =OLD.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =OLD.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =OLD.mach_deli_max_weeks_date,
`sales_order_validaty` =OLD.sales_order_validaty,
`sales_order_quotation_delivery` =OLD.sales_order_quotation_delivery,
`mode_of_shipment_name` =OLD.mode_of_shipment_name,
`mode_of_shipment_truck_number` =OLD.mode_of_shipment_truck_number,
`supplier_payment_terms` =OLD.supplier_payment_terms,
`received_payment` =OLD.received_payment,
`received_payment_date` =OLD.received_payment_date,
`received_payment_type` =OLD.received_payment_type,
`terms_condition_purchase` =OLD.terms_condition_purchase,
`isApproved` =OLD.isApproved,
`pdf_url` =OLD.pdf_url,
`taxes_data` =OLD.taxes_data,
`more_items_data` =OLD.more_items_data,
`due_date` =OLD.due_date,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`created_by` =OLD.created_by,
`operation_type` = 'delete';

CREATE TRIGGER `sales_edit_log` AFTER UPDATE ON `sales`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_log SET
`id`=NEW.id,
`sales` =NEW.sales,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_cal_code_definition_add_log` AFTER INSERT ON `sales_order_cal_code_definition`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`sales_order_id` =NEW.sales_order_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_cal_code_definition_delete_log` AFTER DELETE ON `sales_order_cal_code_definition`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_cal_code_definition_log SET
`id`=OLD.id,
`billing_terms_detail_id` =OLD.billing_terms_detail_id,
`sales_order_id` =OLD.sales_order_id,
`is_first_element` =OLD.is_first_element,
`cal_operation` =OLD.cal_operation,
`cal_code_id` =OLD.cal_code_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_item_edit_log` AFTER UPDATE ON `sales_order_item`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_item_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_name` =NEW.party_name,
`party_type` =NEW.party_type,
`address` =NEW.address,
`city` =NEW.city,
`state` =NEW.state,
`country` =NEW.country,
`fax_no` =NEW.fax_no,
`pin_code` =NEW.pin_code,
`phone_no` =NEW.phone_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_items_add_log` AFTER INSERT ON `sales_order_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`sales_order_id` =NEW.sales_order_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_cal_code_definition_edit_log` AFTER UPDATE ON `sales_order_cal_code_definition`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_cal_code_definition_log SET
`id`=NEW.id,
`billing_terms_detail_id` =NEW.billing_terms_detail_id,
`sales_order_id` =NEW.sales_order_id,
`is_first_element` =NEW.is_first_element,
`cal_operation` =NEW.cal_operation,
`cal_code_id` =NEW.cal_code_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_item_add_log` AFTER INSERT ON `sales_order_item`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_item_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`party_name` =NEW.party_name,
`party_type` =NEW.party_type,
`address` =NEW.address,
`city` =NEW.city,
`state` =NEW.state,
`country` =NEW.country,
`fax_no` =NEW.fax_no,
`pin_code` =NEW.pin_code,
`phone_no` =NEW.phone_no,
`email_id` =NEW.email_id,
`website` =NEW.website,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_logins_add_log` AFTER INSERT ON `sales_order_logins`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_logins_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_item_delete_log` AFTER DELETE ON `sales_order_item`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_item_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`party_name` =OLD.party_name,
`party_type` =OLD.party_type,
`address` =OLD.address,
`city` =OLD.city,
`state` =OLD.state,
`country` =OLD.country,
`fax_no` =OLD.fax_no,
`pin_code` =OLD.pin_code,
`phone_no` =OLD.phone_no,
`email_id` =OLD.email_id,
`website` =OLD.website,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_edit_log` AFTER UPDATE ON `sales_order`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_log SET
`id`=NEW.id,
`quatation_id` =NEW.quatation_id,
`quotation_id` =NEW.quotation_id,
`sales_order_no` =NEW.sales_order_no,
`sales_to_party_id` =NEW.sales_to_party_id,
`ship_to_party_id` =NEW.ship_to_party_id,
`branch_id` =NEW.branch_id,
`billing_terms_id` =NEW.billing_terms_id,
`sales_order_date` =NEW.sales_order_date,
`committed_date` =NEW.committed_date,
`currency_id` =NEW.currency_id,
`project_id` =NEW.project_id,
`kind_attn_id` =NEW.kind_attn_id,
`contact_no` =NEW.contact_no,
`cust_po_no` =NEW.cust_po_no,
`email_id` =NEW.email_id,
`po_date` =NEW.po_date,
`sales_order_pref_id` =NEW.sales_order_pref_id,
`conversation_rate` =NEW.conversation_rate,
`sales_id` =NEW.sales_id,
`sales_order_stage_id` =NEW.sales_order_stage_id,
`sales_order_status_id` =NEW.sales_order_status_id,
`order_type` =NEW.order_type,
`lead_provider_id` =NEW.lead_provider_id,
`buyer_detail_id` =NEW.buyer_detail_id,
`note_detail_id` =NEW.note_detail_id,
`login_detail_id` =NEW.login_detail_id,
`transportation_by_id` =NEW.transportation_by_id,
`transport_amount` =NEW.transport_amount,
`foundation_drawing_required_id` =NEW.foundation_drawing_required_id,
`loading_by_id` =NEW.loading_by_id,
`inspection_required_id` =NEW.inspection_required_id,
`unloading_by_id` =NEW.unloading_by_id,
`erection_commissioning_id` =NEW.erection_commissioning_id,
`road_insurance_by_id` =NEW.road_insurance_by_id,
`for_required_id` =NEW.for_required_id,
`review_date` =NEW.review_date,
`mach_deli_min_weeks` =NEW.mach_deli_min_weeks,
`mach_deli_max_weeks` =NEW.mach_deli_max_weeks,
`mach_deli_min_weeks_date` =NEW.mach_deli_min_weeks_date,
`mach_deli_max_weeks_date` =NEW.mach_deli_max_weeks_date,
`sales_order_validaty` =NEW.sales_order_validaty,
`sales_order_quotation_delivery` =NEW.sales_order_quotation_delivery,
`mode_of_shipment_name` =NEW.mode_of_shipment_name,
`mode_of_shipment_truck_number` =NEW.mode_of_shipment_truck_number,
`supplier_payment_terms` =NEW.supplier_payment_terms,
`received_payment` =NEW.received_payment,
`received_payment_date` =NEW.received_payment_date,
`received_payment_type` =NEW.received_payment_type,
`terms_condition_purchase` =NEW.terms_condition_purchase,
`isApproved` =NEW.isApproved,
`pdf_url` =NEW.pdf_url,
`taxes_data` =NEW.taxes_data,
`more_items_data` =NEW.more_items_data,
`due_date` =NEW.due_date,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`created_by` =NEW.created_by,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_items_delete_log` AFTER DELETE ON `sales_order_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_items_log SET
`id`=OLD.id,
`item_id` =OLD.item_id,
`sales_order_id` =OLD.sales_order_id,
`item_code` =OLD.item_code,
`item_category_id` =OLD.item_category_id,
`item_name` =OLD.item_name,
`item_desc` =OLD.item_desc,
`item_description` =OLD.item_description,
`add_description` =OLD.add_description,
`detail_description` =OLD.detail_description,
`drawing_number` =OLD.drawing_number,
`drawing_revision` =OLD.drawing_revision,
`uom_id`=OLD.uom_id,
`uom` =OLD.uom,
`total_amount` =OLD.total_amount,
`item_note` =OLD.item_note,
`quantity` =OLD.quantity,
`disc_per` =OLD.disc_per,
`rate` =OLD.rate,
`disc_value` =OLD.disc_value,
`amount` =OLD.amount,
`net_amount` =OLD.net_amount,
`item_status_id` =OLD.item_status_id,
`item_status` =OLD.item_status,
`cust_part_no` =OLD.cust_part_no,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_items_edit_log` AFTER UPDATE ON `sales_order_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_items_log SET
`id`=NEW.id,
`item_id` =NEW.item_id,
`sales_order_id` =NEW.sales_order_id,
`item_code` =NEW.item_code,
`item_category_id` =NEW.item_category_id,
`item_name` =NEW.item_name,
`item_desc` =NEW.item_desc,
`item_description` =NEW.item_description,
`add_description` =NEW.add_description,
`detail_description` =NEW.detail_description,
`drawing_number` =NEW.drawing_number,
`drawing_revision` =NEW.drawing_revision,
`uom_id`=NEW.uom_id,
`uom` =NEW.uom,
`total_amount` =NEW.total_amount,
`item_note` =NEW.item_note,
`quantity` =NEW.quantity,
`disc_per` =NEW.disc_per,
`rate` =NEW.rate,
`disc_value` =NEW.disc_value,
`amount` =NEW.amount,
`net_amount` =NEW.net_amount,
`item_status_id` =NEW.item_status_id,
`item_status` =NEW.item_status,
`cust_part_no` =NEW.cust_part_no,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_notes_edit_log` AFTER UPDATE ON `sales_order_notes`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_notes_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`header` =NEW.header,
`note` =NEW.note,
`note2` =NEW.note2,
`internal_note` =NEW.internal_note,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_logins_edit_log` AFTER UPDATE ON `sales_order_logins`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_logins_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`created_by` =NEW.created_by,
`created_by_id` =NEW.created_by_id,
`created_date_time` =NEW.created_date_time,
`last_modified_by` =NEW.last_modified_by,
`last_modified_by_id` =NEW.last_modified_by_id,
`modified_date_time` =NEW.modified_date_time,
`prepared_by` =NEW.prepared_by,
`prepared_by_id` =NEW.prepared_by_id,
`prepared_date_time` =NEW.prepared_date_time,
`approved_by` =NEW.approved_by,
`approved_by_id` =NEW.approved_by_id,
`approved_date_time` =NEW.approved_date_time,
`stage_id` =NEW.stage_id,
`stage` =NEW.stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_logins_delete_log` AFTER DELETE ON `sales_order_logins`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_logins_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`created_by` =OLD.created_by,
`created_by_id` =OLD.created_by_id,
`created_date_time` =OLD.created_date_time,
`last_modified_by` =OLD.last_modified_by,
`last_modified_by_id` =OLD.last_modified_by_id,
`modified_date_time` =OLD.modified_date_time,
`prepared_by` =OLD.prepared_by,
`prepared_by_id` =OLD.prepared_by_id,
`prepared_date_time` =OLD.prepared_date_time,
`approved_by` =OLD.approved_by,
`approved_by_id` =OLD.approved_by_id,
`approved_date_time` =OLD.approved_date_time,
`stage_id` =OLD.stage_id,
`stage` =OLD.stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_notes_add_log` AFTER INSERT ON `sales_order_notes`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_notes_log SET
`id`=NEW.id,
`sales_order_id` =NEW.sales_order_id,
`header` =NEW.header,
`note` =NEW.note,
`note2` =NEW.note2,
`internal_note` =NEW.internal_note,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_pref_add_log` AFTER INSERT ON `sales_order_pref`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_pref_log SET
`id`=NEW.id,
`sales_order_pref` =NEW.sales_order_pref,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_notes_delete_log` AFTER DELETE ON `sales_order_notes`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_notes_log SET
`id`=OLD.id,
`sales_order_id` =OLD.sales_order_id,
`header` =OLD.header,
`note` =OLD.note,
`note2` =OLD.note2,
`internal_note` =OLD.internal_note,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_pref_edit_log` AFTER UPDATE ON `sales_order_pref`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_pref_log SET
`id`=NEW.id,
`sales_order_pref` =NEW.sales_order_pref,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_stage_add_log` AFTER INSERT ON `sales_order_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_stage_log SET
`id`=NEW.id,
`sales_order_stage` =NEW.sales_order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_stage_delete_log` AFTER DELETE ON `sales_order_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_stage_log SET
`id`=OLD.id,
`sales_order_stage` =OLD.sales_order_stage,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_status_edit_log` AFTER UPDATE ON `sales_order_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_status_log SET
`id`=NEW.id,
`sales_order_status` =NEW.sales_order_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_status_add_log` AFTER INSERT ON `sales_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_status_log SET
`id`=NEW.id,
`sales_status` =NEW.sales_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_pref_delete_log` AFTER DELETE ON `sales_order_pref`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_pref_log SET
`id`=OLD.id,
`sales_order_pref` =OLD.sales_order_pref,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_order_status_delete_log` AFTER DELETE ON `sales_order_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_status_log SET
`id`=OLD.id,
`sales_order_status` =OLD.sales_order_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_type_add_log` AFTER INSERT ON `sales_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_type_log SET
`id`=NEW.id,
`sales_type` =NEW.sales_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_order_stage_edit_log` AFTER UPDATE ON `sales_order_stage`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_stage_log SET
`id`=NEW.id,
`sales_order_stage` =NEW.sales_order_stage,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_order_status_add_log` AFTER INSERT ON `sales_order_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_order_status_log SET
`id`=NEW.id,
`sales_order_status` =NEW.sales_order_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_status_delete_log` AFTER DELETE ON `sales_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_status_log SET
`id`=OLD.id,
`sales_status` =OLD.sales_status,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `service_delete_log` AFTER DELETE ON `service`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_log SET
`service_id` =OLD.service_id,
`party_id` =OLD.party_id,
`service_date` =OLD.service_date,
`note` =OLD.note,
`paid_free` =OLD.paid_free,
`charge` =OLD.charge,
`service_provide_by` =OLD.service_provide_by,
`service_received_by` =OLD.service_received_by,
`service_type` =OLD.service_type,
`created_by`=OLD.created_by,
`updated_by` =OLD.updated_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sales_status_edit_log` AFTER UPDATE ON `sales_status`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_status_log SET
`id`=NEW.id,
`sales_status` =NEW.sales_status,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `service_followup_history_add_log` AFTER INSERT ON `service_followup_history`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_followup_history_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`followup_date` =NEW.followup_date,
`history` =NEW.history,
`followup_by` =NEW.followup_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `sales_type_delete_log` AFTER DELETE ON `sales_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_type_log SET
`id`=OLD.id,
`sales_type` =OLD.sales_type,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `service_add_log` AFTER INSERT ON `service`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_log SET
`service_id` =NEW.service_id,
`party_id` =NEW.party_id,
`service_date` =NEW.service_date,
`note` =NEW.note,
`paid_free` =NEW.paid_free,
`charge` =NEW.charge,
`service_provide_by` =NEW.service_provide_by,
`service_received_by` =NEW.service_received_by,
`service_type` =NEW.service_type,
`created_by`=NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `service_followup_history_edit_log` AFTER UPDATE ON `service_followup_history`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_followup_history_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`followup_date` =NEW.followup_date,
`history` =NEW.history,
`followup_by` =NEW.followup_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `service_edit_log` AFTER UPDATE ON `service`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_log SET
`service_id` =NEW.service_id,
`party_id` =NEW.party_id,
`service_date` =NEW.service_date,
`note` =NEW.note,
`paid_free` =NEW.paid_free,
`charge` =NEW.charge,
`service_provide_by` =NEW.service_provide_by,
`service_received_by` =NEW.service_received_by,
`service_type` =NEW.service_type,
`created_by`=NEW.created_by,
`updated_by` =NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `sales_type_edit_log` AFTER UPDATE ON `sales_type`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sales_type_log SET
`id`=NEW.id,
`sales_type` =NEW.sales_type,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `service_items_delete_log` AFTER DELETE ON `service_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_items_log SET
`id`=OLD.id,
`service_id` =OLD.service_id,
`challan_id` =OLD.challan_id,
`challan_item_id` =OLD.challan_item_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `service_items_add_log` AFTER INSERT ON `service_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_items_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`challan_id` =NEW.challan_id,
`challan_item_id` =NEW.challan_item_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `service_followup_history_delete_log` AFTER DELETE ON `service_followup_history`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_followup_history_log SET
`id`=OLD.id,
`service_id` =OLD.service_id,
`followup_date` =OLD.followup_date,
`history` =OLD.history,
`followup_by` =OLD.followup_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `staff_edit_log` AFTER UPDATE ON `staff`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_log SET
`staff_id` =NEW.staff_id,
`interview_id` =NEW.interview_id,
`name` =NEW.name,
`birth_date` =NEW.birth_date,
`gender` =NEW.gender,
`blood_group` =NEW.blood_group,
`father_name` =NEW.father_name,
`mother_name` =NEW.mother_name,
`married_status` =NEW.married_status,
`husband_wife_name` =NEW.husband_wife_name,
`marriage_date` =NEW.marriage_date,
`contact_no` =NEW.contact_no,
`email` =NEW.email,
`pass`  =NEW.pass,
`image` =NEW.image,
`address` =NEW.address,
`permanent_address` =NEW.permanent_address,
`qualification` =NEW.qualification,
`designation_id` =NEW.designation_id,
`department_id` =NEW.department_id,
`grade_id` =NEW.grade_id,
`date_of_joining` =NEW.date_of_joining,
`date_of_leaving` =NEW.date_of_leaving,
`salary` =NEW.salary,
`allow_pf` =NEW.allow_pf,
`basic_pay` =NEW.basic_pay,
`house_rent` =NEW.house_rent,
`traveling_allowance` =NEW.traveling_allowance,
`education_allowance` =NEW.education_allowance,
`pf_amount` =NEW.pf_amount,
`bonus_amount`  =NEW.bonus_amount,
`other_allotment`  =NEW.other_allotment,
`medical_reimbursement`  =NEW.medical_reimbursement,
`professional_tax`  =NEW.professional_tax,
`monthly_gross`  =NEW.monthly_gross,
`cost_to_company`  =NEW.cost_to_company,
`pf_employee`  =NEW.pf_employee,
`bank_name`  =NEW.bank_name,
`bank_acc_no`  =NEW.bank_acc_no,
`esic_no`  =NEW.esic_no,
`pan_no`  =NEW.pan_no,
`uan_id`  =NEW.uan_id,
`pf_no`  =NEW.pf_no,
`emp_no`  =NEW.emp_no,
`emp_id`  =NEW.emp_id,
`esic_id`  =NEW.esic_id,
`visitor_last_message_id`  =NEW.visitor_last_message_id,
`user_type`  =NEW.user_type,
`mailbox_email`  =NEW.mailbox_email,
`mailbox_password`  =NEW.mailbox_password,
`pdf_url` =NEW.pdf_url,
`active` =NEW.active,
`is_login`  =NEW.is_login,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `service_items_edit_log` AFTER UPDATE ON `service_items`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.service_items_log SET
`id`=NEW.id,
`service_id` =NEW.service_id,
`challan_id` =NEW.challan_id,
`challan_item_id` =NEW.challan_item_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `state_add_log` AFTER INSERT ON `state`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.state_log SET
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`state`  =NEW.state,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `state_delete_log` AFTER DELETE ON `state`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.state_log SET
`state_id`  =OLD.state_id,
`country_id`  =OLD.country_id,
`state`  =OLD.state,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `staff_add_log` AFTER INSERT ON `staff`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_log SET
`staff_id` =NEW.staff_id,
`interview_id` =NEW.interview_id,
`name` =NEW.name,
`birth_date` =NEW.birth_date,
`gender` =NEW.gender,
`blood_group` =NEW.blood_group,
`father_name` =NEW.father_name,
`mother_name` =NEW.mother_name,
`married_status` =NEW.married_status,
`husband_wife_name` =NEW.husband_wife_name,
`marriage_date` =NEW.marriage_date,
`contact_no` =NEW.contact_no,
`email` =NEW.email,
`pass`  =NEW.pass,
`image` =NEW.image,
`address` =NEW.address,
`permanent_address` =NEW.permanent_address,
`qualification` =NEW.qualification,
`designation_id` =NEW.designation_id,
`department_id` =NEW.department_id,
`grade_id` =NEW.grade_id,
`date_of_joining` =NEW.date_of_joining,
`date_of_leaving` =NEW.date_of_leaving,
`salary` =NEW.salary,
`allow_pf` =NEW.allow_pf,
`basic_pay` =NEW.basic_pay,
`house_rent` =NEW.house_rent,
`traveling_allowance` =NEW.traveling_allowance,
`education_allowance` =NEW.education_allowance,
`pf_amount` =NEW.pf_amount,
`bonus_amount`  =NEW.bonus_amount,
`other_allotment`  =NEW.other_allotment,
`medical_reimbursement`  =NEW.medical_reimbursement,
`professional_tax`  =NEW.professional_tax,
`monthly_gross`  =NEW.monthly_gross,
`cost_to_company`  =NEW.cost_to_company,
`pf_employee`  =NEW.pf_employee,
`bank_name`  =NEW.bank_name,
`bank_acc_no`  =NEW.bank_acc_no,
`esic_no`  =NEW.esic_no,
`pan_no`  =NEW.pan_no,
`uan_id`  =NEW.uan_id,
`pf_no`  =NEW.pf_no,
`emp_no`  =NEW.emp_no,
`emp_id`  =NEW.emp_id,
`esic_id`  =NEW.esic_id,
`visitor_last_message_id`  =NEW.visitor_last_message_id,
`user_type`  =NEW.user_type,
`mailbox_email`  =NEW.mailbox_email,
`mailbox_password`  =NEW.mailbox_password,
`pdf_url` =NEW.pdf_url,
`active` =NEW.active,
`is_login`  =NEW.is_login,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `staff_roles_delete_log` AFTER DELETE ON `staff_roles`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_roles_log SET
`id`=OLD.id,
`staff_id` =OLD.staff_id,
`module_id` =OLD.module_id,
`role_id`  =OLD.role_id,
`operation_type` = 'delete';

CREATE TRIGGER `sub_group_delete_log` AFTER DELETE ON `sub_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sub_group_log SET
`id`=OLD.id,
`sub_item_group`  =OLD.sub_item_group,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `sub_group_edit_log` AFTER UPDATE ON `sub_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sub_group_log SET
`id`=NEW.id,
`sub_item_group`  =NEW.sub_item_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `supplier_add_log` AFTER INSERT ON `supplier`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =NEW.supplier_id,
`supplier_name`  =NEW.supplier_name,
`address` =NEW.address,
`city_id`  =NEW.city_id,
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`tel_no`  =NEW.tel_no,
`email_id`  =NEW.email_id,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `supplier_delete_log` AFTER DELETE ON `supplier`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =OLD.supplier_id,
`supplier_name`  =OLD.supplier_name,
`address` =OLD.address,
`city_id`  =OLD.city_id,
`state_id`  =OLD.state_id,
`country_id`  =OLD.country_id,
`tel_no`  =OLD.tel_no,
`email_id`  =OLD.email_id,
`created_by`=OLD.created_by,
`updated_by`=OLD.updated_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `staff_roles_add_log` AFTER INSERT ON `staff_roles`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_roles_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`module_id` =NEW.module_id,
`role_id`  =NEW.role_id,
`operation_type` = 'add';

CREATE TRIGGER `terms_condition_detail_add_log` AFTER INSERT ON `terms_condition_detail`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_detail_log SET
`id`=NEW.id,
`code` =NEW.code,
`group_name1`  =NEW.group_name1,
`group_name2`  =NEW.group_name2,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `terms_condition_detail_delete_log` AFTER DELETE ON `terms_condition_detail`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_detail_log SET
`id`=OLD.id,
`code` =OLD.code,
`group_name1`  =OLD.group_name1,
`group_name2`  =OLD.group_name2,
`description` =OLD.description,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `state_edit_log` AFTER UPDATE ON `state`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.state_log SET
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`state`  =NEW.state,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `staff_roles_edit_log` AFTER UPDATE ON `staff_roles`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_roles_log SET
`id`=NEW.id,
`staff_id` =NEW.staff_id,
`module_id` =NEW.module_id,
`role_id`  =NEW.role_id,
`operation_type` = 'edit';

CREATE TRIGGER `sub_group_add_log` AFTER INSERT ON `sub_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.sub_group_log SET
`id`=NEW.id,
`sub_item_group`  =NEW.sub_item_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `terms_condition_detail_edit_log` AFTER UPDATE ON `terms_condition_detail`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_detail_log SET
`id`=NEW.id,
`code` =NEW.code,
`group_name1`  =NEW.group_name1,
`group_name2`  =NEW.group_name2,
`description` =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `terms_condition_group_delete_log` AFTER DELETE ON `terms_condition_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_group_log SET
`id`=OLD.id,
`code` =OLD.code,
`description`  =OLD.description,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `supplier_edit_log` AFTER UPDATE ON `supplier`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =NEW.supplier_id,
`supplier_name`  =NEW.supplier_name,
`address` =NEW.address,
`city_id`  =NEW.city_id,
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`tel_no`  =NEW.tel_no,
`email_id`  =NEW.email_id,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `terms_condition_template_add_log` AFTER INSERT ON `terms_condition_template`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_template_log SET
`id`=NEW.id,
`template_name`  =NEW.template_name,
`group_name`  =NEW.group_name,
`terms_condition`  =NEW.terms_condition,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `terms_condition_group_add_log` AFTER INSERT ON `terms_condition_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_group_log SET
`id`=NEW.id,
`code` =NEW.code,
`description`  =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `terms_condition_template_edit_log` AFTER UPDATE ON `terms_condition_template`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_template_log SET
`id`=NEW.id,
`template_name`  =NEW.template_name,
`group_name`  =NEW.group_name,
`terms_condition`  =NEW.terms_condition,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `terms_group_add_log` AFTER INSERT ON `terms_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_group_log SET
`id`=NEW.id,
`terms_group`  =NEW.terms_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `terms_group_edit_log` AFTER UPDATE ON `terms_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_group_log SET
`id`=NEW.id,
`terms_group`  =NEW.terms_group,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `terms_group_delete_log` AFTER DELETE ON `terms_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_group_log SET
`id`=OLD.id,
`terms_group`  =OLD.terms_group,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `terms_condition_template_delete_log` AFTER DELETE ON `terms_condition_template`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_template_log SET
`id`=OLD.id,
`template_name`  =OLD.template_name,
`group_name`  =OLD.group_name,
`terms_condition`  =OLD.terms_condition,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `terms_condition_group_edit_log` AFTER UPDATE ON `terms_condition_group`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.terms_condition_group_log SET
`id`=NEW.id,
`code` =NEW.code,
`description`  =NEW.description,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `unloading_by_add_log` AFTER INSERT ON `unloading_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.unloading_by_log SET
`id`=NEW.id,
`unloading_by` =NEW.unloading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `unloading_by_delete_log` AFTER DELETE ON `unloading_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.unloading_by_log SET
`id`=OLD.id,
`unloading_by` =OLD.unloading_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `transportation_by_add_log` AFTER INSERT ON `transportation_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.transportation_by_log SET
`id`=NEW.id,
`transportation_by` =NEW.transportation_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `transportation_by_delete_log` AFTER DELETE ON `transportation_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.transportation_by_log SET
`id`=OLD.id,
`transportation_by` =OLD.transportation_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `uom_edit_log` AFTER UPDATE ON `uom`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.uom_log SET
`id`=NEW.id,
`uom` =NEW.uom,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `visitors_add_log` AFTER INSERT ON `visitors`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.visitors_log SET
`id`=NEW.id,
`name` =NEW.name,
`phone`  =NEW.phone,
`email` =NEW.email,
`ip`  =NEW.ip,
`from_address` =NEW.from_address,
`city`  =NEW.city,
`country`  =NEW.country,
`currency`  =NEW.currency,
`others`  =NEW.others,
`session_id`  =NEW.session_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`is_disconnect_mail_sent` =NEW.is_disconnect_mail_sent,
`operation_type` = 'add';

CREATE TRIGGER `visitors_delete_log` AFTER DELETE ON `visitors`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.visitors_log SET
`id`=OLD.id,
`name` =OLD.name,
`phone`  =OLD.phone,
`email` =OLD.email,
`ip`  =OLD.ip,
`from_address` =OLD.from_address,
`city`  =OLD.city,
`country`  =OLD.country,
`currency`  =OLD.currency,
`others`  =OLD.others,
`session_id`  =OLD.session_id,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`is_disconnect_mail_sent` =OLD.is_disconnect_mail_sent,
`operation_type` = 'delete';

CREATE TRIGGER `unloading_by_edit_log` AFTER UPDATE ON `unloading_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.unloading_by_log SET
`id`=NEW.id,
`unloading_by` =NEW.unloading_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `transportation_by_edit_log` AFTER UPDATE ON `transportation_by`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.transportation_by_log SET
`id`=NEW.id,
`transportation_by` =NEW.transportation_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

CREATE TRIGGER `uom_delete_log` AFTER DELETE ON `uom`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.uom_log SET
`id`=OLD.id,
`uom` =OLD.uom,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

CREATE TRIGGER `uom_add_log` AFTER INSERT ON `uom`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.uom_log SET
`id`=NEW.id,
`uom` =NEW.uom,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

CREATE TRIGGER `visitors_edit_log` AFTER UPDATE ON `visitors`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.visitors_log SET
`id`=NEW.id,
`name` =NEW.name,
`phone`  =NEW.phone,
`email` =NEW.email,
`ip`  =NEW.ip,
`from_address` =NEW.from_address,
`city`  =NEW.city,
`country`  =NEW.country,
`currency`  =NEW.currency,
`others`  =NEW.others,
`session_id`  =NEW.session_id,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`is_disconnect_mail_sent` =NEW.is_disconnect_mail_sent,
`operation_type` = 'edit';

CREATE TRIGGER `website_modules_add_log` AFTER INSERT ON `website_modules`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.website_modules_log SET
`id`=NEW.id,
`title` =NEW.title,
`table_name`  =NEW.table_name,
`main_module`  =NEW.main_module,
`operation_type` = 'add';

CREATE TRIGGER `website_modules_edit_log` AFTER UPDATE ON `website_modules`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.website_modules_log SET
`id`=NEW.id,
`title` =NEW.title,
`table_name`  =NEW.table_name,
`main_module`  =NEW.main_module,
`operation_type` = 'edit';

CREATE TRIGGER `weekly_holiday_edit_log` AFTER UPDATE ON `weekly_holiday`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.weekly_holiday_log SET
`id`=NEW.id,
`day`  =NEW.day,
`operation_type` = 'edit';

CREATE TRIGGER `website_modules_delete_log` AFTER DELETE ON `website_modules`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.website_modules_log SET
`id`=OLD.id,
`title` =OLD.title,
`table_name`  =OLD.table_name,
`main_module`  =OLD.main_module,
`operation_type` = 'delete';

CREATE TRIGGER `weekly_holiday_add_log` AFTER INSERT ON `weekly_holiday`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.weekly_holiday_log SET
`id`=NEW.id,
`day`  =NEW.day,
`operation_type` = 'add';

CREATE TRIGGER `yearly_leaves_add_log` AFTER INSERT ON `yearly_leaves`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.yearly_leaves_log SET
`leave_id`=NEW.leave_id,
`user_id`  =NEW.user_id,
`date` =NEW.date,
`operation_type` = 'add';

CREATE TRIGGER `yearly_leaves_delete_log` AFTER DELETE ON `yearly_leaves`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.yearly_leaves_log SET
`leave_id`=OLD.leave_id,
`user_id`  =OLD.user_id,
`date` =OLD.date,
`operation_type` = 'delete';

CREATE TRIGGER `yearly_leaves_edit_log` AFTER UPDATE ON `yearly_leaves`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.yearly_leaves_log SET
`leave_id`=NEW.leave_id,
`user_id`  =NEW.user_id,
`date` =NEW.date,
`operation_type` = 'edit';

CREATE TRIGGER `weekly_holiday_delete_log` AFTER DELETE ON `weekly_holiday`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.weekly_holiday_log SET
`id`=OLD.id,
`day`  =OLD.day,
`operation_type` = 'delete';
