-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 07, 2017 at 03:32 PM
-- Server version: 5.6.16-1~exp1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jaykhodiyar_logs`
--

-- --------------------------------------------------------

--
-- Table structure for table `agent_log`
--

CREATE TABLE `agent_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `party_type_1` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `fix_commission` double DEFAULT NULL,
  `agent_name` varchar(100) DEFAULT NULL,
  `commission` double DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_template_log`
--

CREATE TABLE `billing_template_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_terms_detail_log`
--

CREATE TABLE `billing_terms_detail_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `billing_terms_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_terms_log`
--

CREATE TABLE `billing_terms_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `billing_terms_name` varchar(255) DEFAULT NULL,
  `billing_terms_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branch_log`
--

CREATE TABLE `branch_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `branch_code` varchar(100) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cal_code_definition_log`
--

CREATE TABLE `cal_code_definition_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challans_log`
--

CREATE TABLE `challans_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `truck_no` varchar(255) DEFAULT NULL,
  `gear_no_5` varchar(255) DEFAULT NULL,
  `gear_no_3_54` varchar(255) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `lr_date` date DEFAULT NULL,
  `trademark_name` varchar(222) DEFAULT NULL,
  `full_load_current` varchar(222) DEFAULT NULL,
  `serial_no` varchar(111) DEFAULT NULL,
  `serial_no_1` varchar(111) DEFAULT NULL,
  `serial_no_2` varchar(111) DEFAULT NULL,
  `serial_no_3` varchar(111) DEFAULT NULL,
  `serial_no_4` varchar(111) DEFAULT NULL,
  `motor_75_hp` varchar(255) DEFAULT NULL,
  `motor_50_hp` varchar(255) DEFAULT NULL,
  `rated_voltage` varchar(222) DEFAULT NULL,
  `phases_no` varchar(222) DEFAULT NULL,
  `short_circuit_rating` varchar(222) DEFAULT NULL,
  `capacity` varchar(222) DEFAULT NULL,
  `operating_manual_reference_no` varchar(222) DEFAULT NULL,
  `gross_weight` varchar(222) DEFAULT NULL,
  `manufacturing_monthYear` varchar(222) DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `transport_name` varchar(255) DEFAULT NULL,
  `transport_charges` decimal(10,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `rounding` varchar(255) DEFAULT NULL,
  `challan_no` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challan_items_log`
--

CREATE TABLE `challan_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `item_description` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `drawing_revision` varchar(255) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `item_note` text,
  `quantity` double DEFAULT NULL,
  `disc_per` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `disc_value` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `net_amount` double DEFAULT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `cust_part_no` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_roles_log`
--

CREATE TABLE `chat_roles_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `allowed_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `city_log`
--

CREATE TABLE `city_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_log`
--

CREATE TABLE `company_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `ceo_name` varchar(225) DEFAULT NULL,
  `m_d_name` varchar(225) DEFAULT NULL,
  `address` text,
  `city` varchar(225) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(15) DEFAULT NULL,
  `country` varchar(225) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `cell_no` varchar(225) DEFAULT NULL,
  `fax_no` varchar(225) DEFAULT NULL,
  `email_id` varchar(225) DEFAULT NULL,
  `tin_no` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `tan_no` varchar(255) DEFAULT NULL,
  `pf_no` varchar(255) DEFAULT NULL,
  `esic_no` varchar(255) DEFAULT NULL,
  `pt_no` varchar(255) DEFAULT NULL,
  `service_tax_no` varchar(255) DEFAULT NULL,
  `vat_no` varchar(255) DEFAULT NULL,
  `cst_no` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `bank_domestic` varchar(1000) DEFAULT NULL,
  `bank_export` varchar(1000) DEFAULT NULL,
  `margin_left` int(11) DEFAULT NULL,
  `margin_right` int(11) DEFAULT NULL,
  `margin_top` int(11) DEFAULT NULL,
  `margin_bottom` int(11) DEFAULT NULL,
  `invoice_terms` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config_log`
--

CREATE TABLE `config_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `config_key` varchar(255) DEFAULT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_person_log`
--

CREATE TABLE `contact_person_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `mobile_no` varchar(20) DEFAULT NULL,
  `fax_no` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `note` text,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country_log`
--

CREATE TABLE `country_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency_log`
--

CREATE TABLE `currency_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daily_work_entry_log`
--

CREATE TABLE `daily_work_entry_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `entry_date` date DEFAULT NULL,
  `activity_details` text,
  `created_date` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `days_log`
--

CREATE TABLE `days_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `day_id` int(11) DEFAULT NULL,
  `day_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_terms_condition_log`
--

CREATE TABLE `default_terms_condition_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `terms_condition` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department_log`
--

CREATE TABLE `department_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `designation_log`
--

CREATE TABLE `designation_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dispatch_followup_history_log`
--

CREATE TABLE `dispatch_followup_history_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `followup_date` date DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_number_log`
--

CREATE TABLE `drawing_number_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `drawing_number` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_leaves_log`
--

CREATE TABLE `employee_leaves_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `leave_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `day` varchar(15) DEFAULT NULL,
  `leave_type` varchar(30) NOT NULL COMMENT 'sl,cl & el/pl',
  `leave_detail` text,
  `leave_status` varchar(15) NOT NULL DEFAULT 'pending' COMMENT 'pending , approve & not approve',
  `leave_note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erection_commissioning_log`
--

CREATE TABLE `erection_commissioning_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `erection_commissioning` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `excise_log`
--

CREATE TABLE `excise_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `sub_heading` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `followup_history_log`
--

CREATE TABLE `followup_history_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `followup_date` date DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `followup_order_history_log`
--

CREATE TABLE `followup_order_history_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `followup_date` date DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `for_required_log`
--

CREATE TABLE `for_required_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `for_required` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foundation_drawing_required_log`
--

CREATE TABLE `foundation_drawing_required_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `foundation_drawing_required` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `google_sheets_log`
--

CREATE TABLE `google_sheets_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` text,
  `sequence` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `google_sheet_staff_log`
--

CREATE TABLE `google_sheet_staff_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `google_sheet_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grade_log`
--

CREATE TABLE `grade_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inbox_log`
--

CREATE TABLE `inbox_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `mail_id` int(11) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `mail_no` int(11) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `folder_label` varchar(255) DEFAULT NULL,
  `folder_name` varchar(255) DEFAULT NULL,
  `is_sent` tinyint(1) DEFAULT NULL,
  `is_unread` tinyint(1) DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Starred | 1-Starred',
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_created` tinyint(1) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Updated | 1-Updated',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `industry_type_log`
--

CREATE TABLE `industry_type_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `industry_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_confirmation_log`
--

CREATE TABLE `inquiry_confirmation_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `inquiry_confirmation` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_followup_history_log`
--

CREATE TABLE `inquiry_followup_history_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `followup_date` date DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_items_log`
--

CREATE TABLE `inquiry_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `item_data` text,
  `inquiry_flag` int(11) DEFAULT '0',
  `quotation_flag` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_log`
--

CREATE TABLE `inquiry_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `inquiry_no` varchar(100) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `reference_date` date DEFAULT NULL,
  `inquiry_date` date DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL COMMENT 'staff_id',
  `follow_up_by_id` int(11) DEFAULT NULL COMMENT 'staff_id',
  `assigned_to_id` int(11) DEFAULT NULL COMMENT 'staff_id',
  `industry_type_id` int(11) DEFAULT NULL,
  `inquiry_stage_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `inquiry_status_id` int(11) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_type_id` int(11) DEFAULT NULL,
  `party_id` int(11) NOT NULL COMMENT 'party_id',
  `received_email` varchar(255) DEFAULT NULL,
  `received_fax` varchar(255) DEFAULT NULL,
  `received_verbal` varchar(255) DEFAULT NULL,
  `is_received_post` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-Yes | 0-No = Checked | Not Checked',
  `send_email` varchar(255) DEFAULT NULL,
  `send_fax` varchar(255) DEFAULT NULL,
  `is_send_post` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-Yes | 0-No = Checked | Not Checked',
  `send_from_post` tinyint(1) DEFAULT '0',
  `send_from_post_name` text,
  `lead_or_inquiry` varchar(222) DEFAULT 'lead',
  `lead_id` int(11) DEFAULT NULL,
  `history` text,
  `created_by` int(11) NOT NULL COMMENT 'user_id , staff_id',
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_stage_log`
--

CREATE TABLE `inquiry_stage_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `inquiry_stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_status_log`
--

CREATE TABLE `inquiry_status_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `inquiry_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_required_log`
--

CREATE TABLE `inspection_required_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `inspection_required` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `interview_log`
--

CREATE TABLE `interview_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `interview_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pass` text,
  `mailbox_email` varchar(255) DEFAULT NULL,
  `mailbox_password` text,
  `image` text,
  `birth_date` date DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `husband_wife_name` varchar(255) DEFAULT NULL,
  `blood_group` varchar(25) DEFAULT NULL,
  `contact_no` varchar(30) DEFAULT NULL,
  `married_status` varchar(10) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` text,
  `permanent_address` text,
  `interview_review` text,
  `interview_decision` varchar(25) NOT NULL COMMENT '0-Employee Rejcted | 1-Employee Selected | 2-Employee Hold | 3-Employee Pipeline',
  `designation_id` int(11) DEFAULT NULL,
  `department_id` int(11) NOT NULL COMMENT 'department',
  `grade_id` int(11) DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `date_of_leaving` date DEFAULT NULL,
  `allow_pf` varchar(10) DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `basic_pay` double DEFAULT NULL,
  `house_rent` double DEFAULT NULL,
  `traveling_allowance` double DEFAULT NULL,
  `education_allowance` double DEFAULT NULL,
  `pf_amount` double DEFAULT NULL,
  `bonus_amount` double DEFAULT NULL,
  `other_allotment` double DEFAULT NULL,
  `medical_reimbursement` double DEFAULT NULL,
  `professional_tax` double DEFAULT NULL,
  `monthly_gross` double DEFAULT NULL,
  `cost_to_company` double DEFAULT NULL,
  `pf_employee` double DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `esic_no` varchar(255) DEFAULT NULL,
  `emp_id` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `bank_acc_no` varchar(255) DEFAULT NULL,
  `uan_id` varchar(255) DEFAULT NULL,
  `pf_no` varchar(255) DEFAULT NULL,
  `emp_no` varchar(255) DEFAULT NULL,
  `esic_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoices_log`
--

CREATE TABLE `invoices_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `invoice_type_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `total_cmount` double DEFAULT NULL,
  `quatation_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(255) DEFAULT NULL,
  `sales_order_no` varchar(255) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `ship_to_party_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `billing_terms_id` int(11) DEFAULT NULL,
  `sales_order_date` date DEFAULT NULL,
  `committed_date` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `cust_po_no` varchar(50) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `sales_order_pref_id` int(11) DEFAULT NULL,
  `conversation_rate` varchar(50) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_order_stage_id` int(11) DEFAULT NULL,
  `sales_order_status_id` int(11) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `buyer_detail_id` int(11) DEFAULT NULL,
  `note_detail_id` int(11) DEFAULT NULL,
  `login_detail_id` int(11) DEFAULT NULL,
  `transportation_by_id` int(11) DEFAULT NULL,
  `foundation_drawing_required_id` int(11) DEFAULT NULL,
  `loading_by_id` int(11) DEFAULT NULL,
  `inspection_required_id` int(11) DEFAULT NULL,
  `unloading_by_id` int(11) DEFAULT NULL,
  `erection_commissioning_id` int(11) DEFAULT NULL,
  `road_insurance_by_id` int(11) DEFAULT NULL,
  `for_required_id` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `mach_deli_min_weeks` int(11) DEFAULT NULL,
  `mach_deli_max_weeks` int(11) DEFAULT NULL,
  `mach_deli_min_weeks_date` date DEFAULT NULL,
  `mach_deli_max_weeks_date` date DEFAULT NULL,
  `sales_order_validaty` varchar(225) DEFAULT NULL,
  `sales_order_quotation_delivery` varchar(225) DEFAULT NULL,
  `mode_of_shipment_name` varchar(225) DEFAULT NULL,
  `mode_of_shipment_truck_number` varchar(225) DEFAULT NULL,
  `supplier_payment_terms` text,
  `received_payment` varchar(255) DEFAULT NULL,
  `received_payment_date` date DEFAULT NULL,
  `received_payment_type` varchar(255) DEFAULT NULL,
  `terms_condition_purchase` text,
  `isApproved` int(11) DEFAULT '0',
  `pdf_url` text,
  `lr_no` varchar(255) DEFAULT NULL,
  `lr_date` date DEFAULT NULL,
  `vehicle_no` varchar(50) DEFAULT NULL,
  `against_form` int(1) DEFAULT NULL,
  `taxes_data` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_billing_terms_log`
--

CREATE TABLE `invoice_billing_terms_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_cal_code_definition_log`
--

CREATE TABLE `invoice_cal_code_definition_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items_log`
--

CREATE TABLE `invoice_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `item_description` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `drawing_revision` varchar(255) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `item_note` text,
  `quantity` double DEFAULT NULL,
  `disc_per` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `disc_value` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `net_amount` double DEFAULT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `cust_part_no` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_logins_log`
--

CREATE TABLE `invoice_logins_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_by_id` int(11) DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `prepared_by` varchar(255) DEFAULT NULL,
  `prepared_by_id` int(11) DEFAULT NULL,
  `prepared_date_time` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_by_id` int(11) DEFAULT NULL,
  `approved_date_time` datetime DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_type_log`
--

CREATE TABLE `invoice_type_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `invoice_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `items_log`
--

CREATE TABLE `items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_category` varchar(255) DEFAULT NULL,
  `item_code1` varchar(255) DEFAULT NULL,
  `item_code2` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `item_type` varchar(255) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL,
  `hsn_code` varchar(255) DEFAULT NULL,
  `item_group` varchar(255) DEFAULT NULL,
  `item_active` int(11) NOT NULL COMMENT '0- Active 1-Deactive',
  `item_name` varchar(255) DEFAULT NULL,
  `cfactor` varchar(255) DEFAULT NULL,
  `conv_uom` varchar(255) DEFAULT NULL,
  `item_mpt` varchar(255) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_bom_log`
--

CREATE TABLE `item_bom_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_master_id` int(11) DEFAULT NULL COMMENT 'item_id',
  `item_category_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_category_log`
--

CREATE TABLE `item_category_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_category` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_class_log`
--

CREATE TABLE `item_class_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_class` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_details_log`
--

CREATE TABLE `item_details_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `rev` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `cust_part_no` varchar(255) DEFAULT NULL,
  `item_note` varchar(255) DEFAULT NULL,
  `lead_no1` varchar(255) DEFAULT NULL,
  `lead_no2` varchar(255) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_documents_log`
--

CREATE TABLE `item_documents_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `detail` text,
  `document_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_files_log`
--

CREATE TABLE `item_files_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `file_url` text,
  `item_type` varchar(10) NOT NULL COMMENT 'Domestic OR Export',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_group_code_log`
--

CREATE TABLE `item_group_code_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `ig_code` varchar(255) DEFAULT NULL,
  `ig_description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_make_log`
--

CREATE TABLE `item_make_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_make` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_status_log`
--

CREATE TABLE `item_status_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_type_log`
--

CREATE TABLE `item_type_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_contact_person_log`
--

CREATE TABLE `lead_contact_person_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `person_name` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone_no` varchar(15) DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `fax_no` varchar(20) DEFAULT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `division` varchar(100) DEFAULT NULL,
  `address` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_details_log`
--

CREATE TABLE `lead_details_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `lead_no` varchar(255) DEFAULT NULL,
  `lead_title` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `lead_detail` text,
  `industry_type_id` int(11) DEFAULT NULL,
  `lead_owner_id` int(11) DEFAULT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `lead_source_id` int(11) DEFAULT NULL,
  `lead_stage_id` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `lead_status_id` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_provider_log`
--

CREATE TABLE `lead_provider_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `lead_provider` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_source_log`
--

CREATE TABLE `lead_source_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `lead_source` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_stage_log`
--

CREATE TABLE `lead_stage_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `lead_stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_status_log`
--

CREATE TABLE `lead_status_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `lead_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leaves_log`
--

CREATE TABLE `leaves_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `leave_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `leave_type` varchar(50) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `leave_detail` text,
  `leave_count` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leave_for_employee_log`
--

CREATE TABLE `leave_for_employee_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `total_sl` int(11) DEFAULT NULL,
  `total_cl` int(11) DEFAULT NULL,
  `total_el_pl` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `letters_log`
--

CREATE TABLE `letters_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `letter_id` int(11) DEFAULT NULL,
  `letter_code` varchar(255) DEFAULT NULL,
  `letter_template` text,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loading_by_log`
--

CREATE TABLE `loading_by_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `loading_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_folders_log`
--

CREATE TABLE `mailbox_folders_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `folder_label` varchar(255) DEFAULT NULL,
  `folder_name` varchar(255) DEFAULT NULL,
  `old_parent_id` int(11) NOT NULL DEFAULT '0',
  `is_system_folder` tinyint(1) NOT NULL DEFAULT '0',
  `is_created` tinyint(1) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_mails_log`
--

CREATE TABLE `mailbox_mails_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `mail_id` int(11) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `mail_no` int(11) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `folder_label` varchar(255) DEFAULT NULL,
  `folder_name` varchar(255) DEFAULT NULL,
  `is_unread` tinyint(1) DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Starred | 1-Starred',
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_created` tinyint(1) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Updated | 1-Updated',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_settings_log`
--

CREATE TABLE `mailbox_settings_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `setting_key` varchar(255) DEFAULT NULL,
  `setting_value` text,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mail_system_folder_log`
--

CREATE TABLE `mail_system_folder_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mail_system_log`
--

CREATE TABLE `mail_system_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `mail_id` int(11) DEFAULT NULL,
  `mail_uid` varchar(255) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `body` text CHARACTER SET utf8,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `is_unread` tinyint(1) NOT NULL DEFAULT '0',
  `is_starred` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Starred | 1-Starred',
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_read_receipt_req` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Requested | 1-Requested',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_group_log`
--

CREATE TABLE `main_group_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_group` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `material_process_type_log`
--

CREATE TABLE `material_process_type_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `material_process_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `material_specification_log`
--

CREATE TABLE `material_specification_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `material_specification` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module_roles_log`
--

CREATE TABLE `module_roles_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `role_name` varchar(128) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_stage_log`
--

CREATE TABLE `order_stage_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `order_stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outbox_log`
--

CREATE TABLE `outbox_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `mail_id` int(11) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `reply_to_name` varchar(255) DEFAULT NULL,
  `sender_address` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `attachments` text,
  `received_at` varchar(255) DEFAULT NULL,
  `is_attachment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No Attachment | 1-Attachment',
  `is_sent` tinyint(1) NOT NULL COMMENT '1-Sent 0-Not Sent',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `party_detail_log`
--

CREATE TABLE `party_detail_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `party_detail_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `party_log`
--

CREATE TABLE `party_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `party_code` varchar(255) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `reference_description` text,
  `party_type_1` int(11) DEFAULT NULL,
  `party_type_2` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `project` text,
  `address` text,
  `area` text,
  `pincode` varchar(15) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone_no` text,
  `fax_no` varchar(50) DEFAULT NULL,
  `email_id` text,
  `website` varchar(255) DEFAULT NULL,
  `opening_bal` double DEFAULT NULL,
  `credit_limit` double DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active ,2-Deactive',
  `tin_vat_no` varchar(100) DEFAULT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `tin_cst_no` varchar(100) DEFAULT NULL,
  `ecc_no` varchar(100) DEFAULT NULL,
  `pan_no` varchar(100) DEFAULT NULL,
  `range` varchar(100) DEFAULT NULL,
  `division` varchar(100) DEFAULT NULL,
  `service_tax_no` varchar(100) DEFAULT NULL,
  `utr_no` varchar(225) DEFAULT NULL,
  `address_work` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Yes | 0-No = Work Address same as Party Address ',
  `w_address` text,
  `w_city` int(11) DEFAULT NULL,
  `w_state` int(11) DEFAULT NULL,
  `w_country` int(11) DEFAULT NULL,
  `oldw_pincode` varchar(225) DEFAULT NULL,
  `w_email` text,
  `w_web` text,
  `w_phone1` text,
  `w_phone2` text,
  `w_phone3` text,
  `w_note` text,
  `oldPartyType` varchar(225) DEFAULT NULL,
  `oldHoliday` varchar(225) DEFAULT NULL,
  `oldST_No` varchar(225) DEFAULT NULL,
  `oldVendorCode` varchar(225) DEFAULT NULL,
  `oldw_fax` varchar(225) DEFAULT NULL,
  `oldacct_no` varchar(225) DEFAULT NULL,
  `oldReason` varchar(225) DEFAULT NULL,
  `oldAllowMultipalBill` varchar(225) DEFAULT NULL,
  `oldAddCity` varchar(225) DEFAULT NULL,
  `oldCreatedBy` varchar(225) DEFAULT NULL,
  `oldCreateDate` date DEFAULT NULL,
  `oldModifyBy` varchar(225) DEFAULT NULL,
  `oldModiDate` date DEFAULT NULL,
  `oldPrintFlag` varchar(225) DEFAULT NULL,
  `oldPT` varchar(225) DEFAULT NULL,
  `oldCPerson` varchar(225) DEFAULT NULL,
  `oldOpType` varchar(225) DEFAULT NULL,
  `oldCompProfile` varchar(225) DEFAULT NULL,
  `oldBranchCode` varchar(225) DEFAULT NULL,
  `oldAreaName` varchar(225) DEFAULT NULL,
  `oldCreditLimit` varchar(225) DEFAULT NULL,
  `oldAcctGrpCode` varchar(225) DEFAULT NULL,
  `oldOpBalDr` varchar(225) DEFAULT NULL,
  `oldApprovedBy` varchar(225) DEFAULT NULL,
  `oldApprovedDate` date DEFAULT NULL,
  `oldPStage` varchar(225) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `outstanding_balance` double DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `party_old_log`
--

CREATE TABLE `party_old_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `party_code` varchar(255) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `pincode` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `fax_no` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `tin_cst_no` varchar(255) DEFAULT NULL,
  `service_tax_no` varchar(255) DEFAULT NULL,
  `ecc_no` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `range` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `w_address` varchar(255) DEFAULT NULL,
  `w_phone1` varchar(255) DEFAULT NULL,
  `w_email` varchar(255) DEFAULT NULL,
  `w_web` varchar(255) DEFAULT NULL,
  `opening_bal` double DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `party_type_1_log`
--

CREATE TABLE `party_type_1_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `party_type_2_log`
--

CREATE TABLE `party_type_2_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_log`
--

CREATE TABLE `payment_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `payment_type` varchar(50) DEFAULT NULL,
  `payment_note` text,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `priority_log`
--

CREATE TABLE `priority_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoices_log`
--

CREATE TABLE `proforma_invoices_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `proforma_invoice_no` varchar(255) DEFAULT NULL,
  `quotation_no` varchar(255) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `ship_to_party_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `billing_terms_id` int(11) DEFAULT NULL,
  `sales_order_date` date DEFAULT NULL,
  `committed_date` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `cust_po_no` varchar(50) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `sales_order_pref_id` int(11) DEFAULT NULL,
  `conversation_rate` varchar(50) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_order_stage_id` int(11) DEFAULT NULL,
  `sales_order_status_id` int(11) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `buyer_detail_id` int(11) DEFAULT NULL,
  `note_detail_id` int(11) DEFAULT NULL,
  `login_detail_id` int(11) DEFAULT NULL,
  `transportation_by_id` int(11) DEFAULT NULL,
  `foundation_drawing_required_id` int(11) DEFAULT NULL,
  `loading_by_id` int(11) DEFAULT NULL,
  `inspection_required_id` int(11) DEFAULT NULL,
  `unloading_by_id` int(11) DEFAULT NULL,
  `erection_commissioning_id` int(11) DEFAULT NULL,
  `road_insurance_by_id` int(11) DEFAULT NULL,
  `for_required_id` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `mach_deli_min_weeks` int(11) DEFAULT NULL,
  `mach_deli_max_weeks` int(11) DEFAULT NULL,
  `mach_deli_min_weeks_date` date DEFAULT NULL,
  `mach_deli_max_weeks_date` date DEFAULT NULL,
  `sales_order_validaty` varchar(225) DEFAULT NULL,
  `sales_order_quotation_delivery` varchar(225) DEFAULT NULL,
  `mode_of_shipment_name` varchar(225) DEFAULT NULL,
  `mode_of_shipment_truck_number` varchar(225) DEFAULT NULL,
  `supplier_payment_terms` text,
  `received_payment` varchar(255) DEFAULT NULL,
  `received_payment_date` date DEFAULT NULL,
  `received_payment_type` varchar(255) DEFAULT NULL,
  `terms_condition_purchase` text,
  `note` text,
  `isApproved` int(11) DEFAULT '0',
  `pdf_url` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_billing_terms_log`
--

CREATE TABLE `proforma_invoice_billing_terms_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `proforma_invoice_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_cal_code_definition_log`
--

CREATE TABLE `proforma_invoice_cal_code_definition_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `proforma_invoice_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_items_log`
--

CREATE TABLE `proforma_invoice_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `proforma_invoice_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `item_description` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `drawing_revision` varchar(255) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `item_note` text,
  `quantity` double DEFAULT NULL,
  `disc_per` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `disc_value` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `net_amount` double DEFAULT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `cust_part_no` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proforma_invoice_logins_log`
--

CREATE TABLE `proforma_invoice_logins_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `proforma_invoice_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_by_id` int(11) DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `prepared_by` varchar(255) DEFAULT NULL,
  `prepared_by_id` int(11) DEFAULT NULL,
  `prepared_date_time` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_by_id` int(11) DEFAULT NULL,
  `approved_date_time` datetime DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_log`
--

CREATE TABLE `project_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_invoice_items_log`
--

CREATE TABLE `purchase_invoice_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `item_data` text,
  `invoice_flag` int(11) DEFAULT '0',
  `quotation_flag` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_invoice_log`
--

CREATE TABLE `purchase_invoice_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL COMMENT 'supplier_id',
  `address` text,
  `note` text,
  `created_by` int(11) NOT NULL COMMENT 'user_id , staff_id',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items_log`
--

CREATE TABLE `purchase_order_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `item_data` text,
  `invoice_flag` int(11) DEFAULT '0',
  `quotation_flag` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_log`
--

CREATE TABLE `purchase_order_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_no` varchar(100) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL COMMENT 'supplier_id',
  `address` text,
  `note` text,
  `created_by` int(11) NOT NULL COMMENT 'user_id , staff_id',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotations_log`
--

CREATE TABLE `quotations_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `revised_from_id` int(11) DEFAULT '0',
  `party_id` int(11) DEFAULT NULL,
  `enquiry_no` varchar(255) DEFAULT NULL,
  `quotation_no` varchar(255) DEFAULT NULL,
  `quotation_stage_id` int(11) DEFAULT NULL,
  `quotation_type_id` int(11) DEFAULT NULL,
  `quotation_status_id` int(11) DEFAULT NULL,
  `sales_type_id` int(11) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `rev` double DEFAULT NULL,
  `rev_date` date DEFAULT NULL,
  `quotation_note` text,
  `reason` varchar(255) DEFAULT NULL,
  `reason_remark` varchar(255) DEFAULT NULL,
  `review_date_check` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `enquiry_date` date DEFAULT NULL,
  `quotation_date` date DEFAULT NULL,
  `due_date_check` int(11) DEFAULT NULL,
  `oldEnquiryNo` varchar(225) DEFAULT NULL,
  `oldSubject` varchar(225) DEFAULT NULL,
  `oldPreparedBy` varchar(225) DEFAULT NULL,
  `oldApprovedBy` varchar(225) DEFAULT NULL,
  `oldSrNo` varchar(225) DEFAULT NULL,
  `oldExpr1` varchar(225) DEFAULT NULL,
  `oldExpr2` varchar(225) DEFAULT NULL,
  `oldEnquiryDate` date DEFAULT NULL,
  `oldKindAttn` varchar(225) DEFAULT NULL,
  `oldEnquiryStatus` varchar(225) DEFAULT NULL,
  `oldEnqRef` varchar(225) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `conv_rate` double DEFAULT NULL,
  `reference_date` date DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `party_code` varchar(255) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `reference_id` text,
  `reference_description` varchar(255) DEFAULT NULL,
  `address` text,
  `agent_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `fax_no` varchar(50) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `phone_no` varchar(50) DEFAULT NULL,
  `pincode` varchar(10) DEFAULT NULL,
  `website` varchar(500) DEFAULT NULL,
  `contact_person_name` varchar(255) DEFAULT NULL,
  `contact_person_contact_no` varchar(50) DEFAULT NULL,
  `contact_person_email_id` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `header` text,
  `footer` text,
  `cc_to` varchar(255) DEFAULT NULL,
  `authorised_by` varchar(255) DEFAULT NULL,
  `reference_note` text,
  `item_dec_title` varchar(255) DEFAULT NULL,
  `addon_title` text,
  `specification_1` text,
  `specification_2` text,
  `specification_3` text,
  `send_from_post` tinyint(1) DEFAULT NULL,
  `send_from_post_name` text,
  `pdf_url` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_items_log`
--

CREATE TABLE `quotation_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `drawing_revision` varchar(255) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `item_note` text,
  `quantity` double DEFAULT NULL,
  `disc_per` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `disc_value` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `net_amount` double DEFAULT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `cust_part_no` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_reason_log`
--

CREATE TABLE `quotation_reason_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `quotation_reason` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_stage_log`
--

CREATE TABLE `quotation_stage_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `quotation_stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_status_log`
--

CREATE TABLE `quotation_status_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `quotation_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_type_log`
--

CREATE TABLE `quotation_type_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `quotation_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `real_leave_log`
--

CREATE TABLE `real_leave_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `leave_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `leave_type` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reference_log`
--

CREATE TABLE `reference_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reminder_log`
--

CREATE TABLE `reminder_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `reminder_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `remind` text,
  `reminder_date` datetime DEFAULT NULL,
  `is_notified` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=not_remind,1= remind',
  `assigned_to` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `road_insurance_by_log`
--

CREATE TABLE `road_insurance_by_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `road_insurance_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_action_log`
--

CREATE TABLE `sales_action_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_action` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_log`
--

CREATE TABLE `sales_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_billing_terms_log`
--

CREATE TABLE `sales_order_billing_terms_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `cal_code` varchar(255) DEFAULT NULL,
  `narration` varchar(5000) DEFAULT NULL,
  `cal_definition` text,
  `percentage` decimal(10,2) DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  `gl_acc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_buyers_log`
--

CREATE TABLE `sales_order_buyers_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `party_code` varchar(255) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `address` text,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `fax_no` varchar(50) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `pan_no` varchar(100) DEFAULT NULL,
  `tin_vat_no` varchar(255) DEFAULT NULL,
  `tin_cst_no` varchar(255) DEFAULT NULL,
  `ecc_no` varchar(255) DEFAULT NULL,
  `pincode` varchar(20) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_cal_code_definition_log`
--

CREATE TABLE `sales_order_cal_code_definition_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `billing_terms_detail_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `is_first_element` tinyint(2) DEFAULT '1',
  `cal_operation` varchar(20) DEFAULT NULL,
  `cal_code_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_items_log`
--

CREATE TABLE `sales_order_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `item_description` text,
  `add_description` varchar(255) DEFAULT NULL,
  `detail_description` text,
  `drawing_number` varchar(255) DEFAULT NULL,
  `drawing_revision` varchar(255) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `item_note` text,
  `quantity` double DEFAULT NULL,
  `disc_per` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `disc_value` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `net_amount` double DEFAULT NULL,
  `item_status_id` int(11) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `cust_part_no` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_item_log`
--

CREATE TABLE `sales_order_item_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `party_name` int(11) DEFAULT NULL,
  `party_type` int(11) DEFAULT NULL,
  `address` text,
  `city` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `fax_no` varchar(255) DEFAULT NULL,
  `pin_code` varchar(50) DEFAULT NULL,
  `phone_no` varchar(25) DEFAULT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_log`
--

CREATE TABLE `sales_order_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `quatation_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `sales_order_no` varchar(255) DEFAULT NULL,
  `sales_to_party_id` int(11) DEFAULT NULL,
  `ship_to_party_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `billing_terms_id` int(11) DEFAULT NULL,
  `sales_order_date` date DEFAULT NULL,
  `committed_date` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `cust_po_no` varchar(50) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `sales_order_pref_id` int(11) DEFAULT NULL,
  `conversation_rate` varchar(50) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `sales_order_stage_id` int(11) DEFAULT NULL,
  `sales_order_status_id` int(11) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `lead_provider_id` int(11) DEFAULT NULL,
  `buyer_detail_id` int(11) DEFAULT NULL,
  `note_detail_id` int(11) DEFAULT NULL,
  `login_detail_id` int(11) DEFAULT NULL,
  `transportation_by_id` int(11) DEFAULT NULL,
  `transport_amount` varchar(111) DEFAULT NULL,
  `foundation_drawing_required_id` int(11) DEFAULT NULL,
  `loading_by_id` int(11) DEFAULT NULL,
  `inspection_required_id` int(11) DEFAULT NULL,
  `unloading_by_id` int(11) DEFAULT NULL,
  `erection_commissioning_id` int(11) DEFAULT NULL,
  `road_insurance_by_id` int(11) DEFAULT NULL,
  `for_required_id` int(11) DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `mach_deli_min_weeks` int(11) DEFAULT NULL,
  `mach_deli_max_weeks` int(11) DEFAULT NULL,
  `mach_deli_min_weeks_date` date DEFAULT NULL,
  `mach_deli_max_weeks_date` date DEFAULT NULL,
  `sales_order_validaty` varchar(225) DEFAULT NULL,
  `sales_order_quotation_delivery` varchar(225) DEFAULT NULL,
  `mode_of_shipment_name` varchar(225) DEFAULT NULL,
  `mode_of_shipment_truck_number` varchar(225) DEFAULT NULL,
  `supplier_payment_terms` text,
  `received_payment` varchar(255) DEFAULT NULL,
  `received_payment_date` date DEFAULT NULL,
  `received_payment_type` varchar(255) DEFAULT NULL,
  `terms_condition_purchase` text,
  `isApproved` int(11) DEFAULT '0',
  `pdf_url` text,
  `taxes_data` text,
  `more_items_data` text,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_logins_log`
--

CREATE TABLE `sales_order_logins_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_by_id` int(11) DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `prepared_by` varchar(255) DEFAULT NULL,
  `prepared_by_id` int(11) DEFAULT NULL,
  `prepared_date_time` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_by_id` int(11) DEFAULT NULL,
  `approved_date_time` datetime DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_notes_log`
--

CREATE TABLE `sales_order_notes_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `header` text,
  `note` text,
  `note2` text,
  `internal_note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_pref_log`
--

CREATE TABLE `sales_order_pref_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_pref` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_stage_log`
--

CREATE TABLE `sales_order_stage_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_stage` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_status_log`
--

CREATE TABLE `sales_order_status_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_order_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_status_log`
--

CREATE TABLE `sales_status_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_type_log`
--

CREATE TABLE `sales_type_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sales_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_followup_history_log`
--

CREATE TABLE `service_followup_history_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `followup_date` date DEFAULT NULL,
  `history` text,
  `followup_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_items_log`
--

CREATE TABLE `service_items_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `challan_item_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_log`
--

CREATE TABLE `service_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `service_date` date DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `paid_free` varchar(4) NOT NULL DEFAULT 'free',
  `charge` double DEFAULT NULL,
  `service_provide_by` int(11) DEFAULT NULL,
  `service_received_by` int(11) DEFAULT NULL,
  `service_type` varchar(111) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_log`
--

CREATE TABLE `staff_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `interview_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `married_status` varchar(15) DEFAULT NULL,
  `husband_wife_name` varchar(255) DEFAULT NULL,
  `marriage_date` date DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `address` text,
  `permanent_address` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `date_of_leaving` date DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `allow_pf` varchar(10) DEFAULT NULL,
  `basic_pay` double DEFAULT NULL,
  `house_rent` double DEFAULT NULL,
  `traveling_allowance` double DEFAULT NULL,
  `education_allowance` double DEFAULT NULL,
  `pf_amount` double DEFAULT NULL,
  `bonus_amount` double DEFAULT NULL,
  `other_allotment` double DEFAULT NULL,
  `medical_reimbursement` double DEFAULT NULL,
  `professional_tax` double DEFAULT NULL,
  `monthly_gross` double DEFAULT NULL,
  `cost_to_company` double DEFAULT NULL,
  `pf_employee` double DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_no` varchar(255) DEFAULT NULL,
  `esic_no` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `uan_id` varchar(255) DEFAULT NULL,
  `pf_no` varchar(255) DEFAULT NULL,
  `emp_no` varchar(255) DEFAULT NULL,
  `emp_id` varchar(255) DEFAULT NULL,
  `esic_id` varchar(255) DEFAULT NULL,
  `visitor_last_message_id` int(11) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `mailbox_email` varchar(255) DEFAULT NULL,
  `mailbox_password` varchar(255) DEFAULT NULL,
  `pdf_url` text,
  `active` tinyint(1) DEFAULT '1',
  `is_login` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles_log`
--

CREATE TABLE `staff_roles_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `state_log`
--

CREATE TABLE `state_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_group_log`
--

CREATE TABLE `sub_group_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `sub_item_group` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_log`
--

CREATE TABLE `supplier_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `address` text,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone_no` text,
  `email_id` text,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_detail_log`
--

CREATE TABLE `terms_condition_detail_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `group_name1` varchar(255) DEFAULT NULL,
  `group_name2` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_group_log`
--

CREATE TABLE `terms_condition_group_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition_template_log`
--

CREATE TABLE `terms_condition_template_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `terms_condition` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terms_group_log`
--

CREATE TABLE `terms_group_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `terms_group` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transportation_by_log`
--

CREATE TABLE `transportation_by_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `transportation_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unloading_by_log`
--

CREATE TABLE `unloading_by_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `unloading_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `uom_log`
--

CREATE TABLE `uom_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `uom` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visitors_log`
--

CREATE TABLE `visitors_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `from_address` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `others` varchar(255) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `is_disconnect_mail_sent` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `website_modules_log`
--

CREATE TABLE `website_modules_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `main_module` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `weekly_holiday_log`
--

CREATE TABLE `weekly_holiday_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `day` varchar(20) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `yearly_leaves_log`
--

CREATE TABLE `yearly_leaves_log` (
  `log_id` int(11) NOT NULL,
  `operation_type` varchar(33) NOT NULL,
  `leave_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agent_log`
--
ALTER TABLE `agent_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `billing_template_log`
--
ALTER TABLE `billing_template_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `billing_terms_detail_log`
--
ALTER TABLE `billing_terms_detail_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `billing_terms_log`
--
ALTER TABLE `billing_terms_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `branch_log`
--
ALTER TABLE `branch_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `cal_code_definition_log`
--
ALTER TABLE `cal_code_definition_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `challans_log`
--
ALTER TABLE `challans_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `challan_items_log`
--
ALTER TABLE `challan_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `chat_roles_log`
--
ALTER TABLE `chat_roles_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `city_log`
--
ALTER TABLE `city_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `company_log`
--
ALTER TABLE `company_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `config_log`
--
ALTER TABLE `config_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `contact_person_log`
--
ALTER TABLE `contact_person_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `country_log`
--
ALTER TABLE `country_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `currency_log`
--
ALTER TABLE `currency_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `daily_work_entry_log`
--
ALTER TABLE `daily_work_entry_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `days_log`
--
ALTER TABLE `days_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `default_terms_condition_log`
--
ALTER TABLE `default_terms_condition_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `department_log`
--
ALTER TABLE `department_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `designation_log`
--
ALTER TABLE `designation_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `dispatch_followup_history_log`
--
ALTER TABLE `dispatch_followup_history_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `drawing_number_log`
--
ALTER TABLE `drawing_number_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `employee_leaves_log`
--
ALTER TABLE `employee_leaves_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `erection_commissioning_log`
--
ALTER TABLE `erection_commissioning_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `excise_log`
--
ALTER TABLE `excise_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `followup_history_log`
--
ALTER TABLE `followup_history_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `followup_order_history_log`
--
ALTER TABLE `followup_order_history_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `for_required_log`
--
ALTER TABLE `for_required_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `foundation_drawing_required_log`
--
ALTER TABLE `foundation_drawing_required_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `google_sheets_log`
--
ALTER TABLE `google_sheets_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `google_sheet_staff_log`
--
ALTER TABLE `google_sheet_staff_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `grade_log`
--
ALTER TABLE `grade_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inbox_log`
--
ALTER TABLE `inbox_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `industry_type_log`
--
ALTER TABLE `industry_type_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inquiry_confirmation_log`
--
ALTER TABLE `inquiry_confirmation_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inquiry_followup_history_log`
--
ALTER TABLE `inquiry_followup_history_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inquiry_items_log`
--
ALTER TABLE `inquiry_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inquiry_log`
--
ALTER TABLE `inquiry_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inquiry_stage_log`
--
ALTER TABLE `inquiry_stage_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inquiry_status_log`
--
ALTER TABLE `inquiry_status_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `inspection_required_log`
--
ALTER TABLE `inspection_required_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `interview_log`
--
ALTER TABLE `interview_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `invoices_log`
--
ALTER TABLE `invoices_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `invoice_billing_terms_log`
--
ALTER TABLE `invoice_billing_terms_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `invoice_cal_code_definition_log`
--
ALTER TABLE `invoice_cal_code_definition_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `invoice_items_log`
--
ALTER TABLE `invoice_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `invoice_logins_log`
--
ALTER TABLE `invoice_logins_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `invoice_type_log`
--
ALTER TABLE `invoice_type_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `items_log`
--
ALTER TABLE `items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_bom_log`
--
ALTER TABLE `item_bom_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_category_log`
--
ALTER TABLE `item_category_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_class_log`
--
ALTER TABLE `item_class_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_details_log`
--
ALTER TABLE `item_details_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_documents_log`
--
ALTER TABLE `item_documents_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_files_log`
--
ALTER TABLE `item_files_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_group_code_log`
--
ALTER TABLE `item_group_code_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_make_log`
--
ALTER TABLE `item_make_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_status_log`
--
ALTER TABLE `item_status_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `item_type_log`
--
ALTER TABLE `item_type_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `lead_contact_person_log`
--
ALTER TABLE `lead_contact_person_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `lead_details_log`
--
ALTER TABLE `lead_details_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `lead_provider_log`
--
ALTER TABLE `lead_provider_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `lead_source_log`
--
ALTER TABLE `lead_source_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `lead_stage_log`
--
ALTER TABLE `lead_stage_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `lead_status_log`
--
ALTER TABLE `lead_status_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `leaves_log`
--
ALTER TABLE `leaves_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `leave_for_employee_log`
--
ALTER TABLE `leave_for_employee_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `letters_log`
--
ALTER TABLE `letters_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `loading_by_log`
--
ALTER TABLE `loading_by_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `mailbox_folders_log`
--
ALTER TABLE `mailbox_folders_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `mailbox_mails_log`
--
ALTER TABLE `mailbox_mails_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `mailbox_settings_log`
--
ALTER TABLE `mailbox_settings_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `mail_system_folder_log`
--
ALTER TABLE `mail_system_folder_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `mail_system_log`
--
ALTER TABLE `mail_system_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `main_group_log`
--
ALTER TABLE `main_group_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `material_process_type_log`
--
ALTER TABLE `material_process_type_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `material_specification_log`
--
ALTER TABLE `material_specification_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `module_roles_log`
--
ALTER TABLE `module_roles_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `order_stage_log`
--
ALTER TABLE `order_stage_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `outbox_log`
--
ALTER TABLE `outbox_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `party_detail_log`
--
ALTER TABLE `party_detail_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `party_log`
--
ALTER TABLE `party_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `party_old_log`
--
ALTER TABLE `party_old_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `party_type_1_log`
--
ALTER TABLE `party_type_1_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `party_type_2_log`
--
ALTER TABLE `party_type_2_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `payment_log`
--
ALTER TABLE `payment_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `priority_log`
--
ALTER TABLE `priority_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `proforma_invoices_log`
--
ALTER TABLE `proforma_invoices_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `proforma_invoice_billing_terms_log`
--
ALTER TABLE `proforma_invoice_billing_terms_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `proforma_invoice_cal_code_definition_log`
--
ALTER TABLE `proforma_invoice_cal_code_definition_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `proforma_invoice_items_log`
--
ALTER TABLE `proforma_invoice_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `proforma_invoice_logins_log`
--
ALTER TABLE `proforma_invoice_logins_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `project_log`
--
ALTER TABLE `project_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `purchase_invoice_items_log`
--
ALTER TABLE `purchase_invoice_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `purchase_invoice_log`
--
ALTER TABLE `purchase_invoice_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `purchase_order_items_log`
--
ALTER TABLE `purchase_order_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `purchase_order_log`
--
ALTER TABLE `purchase_order_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `quotations_log`
--
ALTER TABLE `quotations_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `quotation_items_log`
--
ALTER TABLE `quotation_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `quotation_reason_log`
--
ALTER TABLE `quotation_reason_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `quotation_stage_log`
--
ALTER TABLE `quotation_stage_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `quotation_status_log`
--
ALTER TABLE `quotation_status_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `quotation_type_log`
--
ALTER TABLE `quotation_type_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `real_leave_log`
--
ALTER TABLE `real_leave_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `reference_log`
--
ALTER TABLE `reference_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `reminder_log`
--
ALTER TABLE `reminder_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `road_insurance_by_log`
--
ALTER TABLE `road_insurance_by_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_action_log`
--
ALTER TABLE `sales_action_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_log`
--
ALTER TABLE `sales_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_billing_terms_log`
--
ALTER TABLE `sales_order_billing_terms_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_buyers_log`
--
ALTER TABLE `sales_order_buyers_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_cal_code_definition_log`
--
ALTER TABLE `sales_order_cal_code_definition_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_items_log`
--
ALTER TABLE `sales_order_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_item_log`
--
ALTER TABLE `sales_order_item_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_log`
--
ALTER TABLE `sales_order_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_logins_log`
--
ALTER TABLE `sales_order_logins_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_notes_log`
--
ALTER TABLE `sales_order_notes_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_pref_log`
--
ALTER TABLE `sales_order_pref_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_stage_log`
--
ALTER TABLE `sales_order_stage_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_order_status_log`
--
ALTER TABLE `sales_order_status_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_status_log`
--
ALTER TABLE `sales_status_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sales_type_log`
--
ALTER TABLE `sales_type_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `service_followup_history_log`
--
ALTER TABLE `service_followup_history_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `service_items_log`
--
ALTER TABLE `service_items_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `service_log`
--
ALTER TABLE `service_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `staff_log`
--
ALTER TABLE `staff_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `staff_roles_log`
--
ALTER TABLE `staff_roles_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `state_log`
--
ALTER TABLE `state_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sub_group_log`
--
ALTER TABLE `sub_group_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `supplier_log`
--
ALTER TABLE `supplier_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `terms_condition_detail_log`
--
ALTER TABLE `terms_condition_detail_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `terms_condition_group_log`
--
ALTER TABLE `terms_condition_group_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `terms_condition_template_log`
--
ALTER TABLE `terms_condition_template_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `terms_group_log`
--
ALTER TABLE `terms_group_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `transportation_by_log`
--
ALTER TABLE `transportation_by_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `unloading_by_log`
--
ALTER TABLE `unloading_by_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `uom_log`
--
ALTER TABLE `uom_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `visitors_log`
--
ALTER TABLE `visitors_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `website_modules_log`
--
ALTER TABLE `website_modules_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `weekly_holiday_log`
--
ALTER TABLE `weekly_holiday_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `yearly_leaves_log`
--
ALTER TABLE `yearly_leaves_log`
  ADD PRIMARY KEY (`log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agent_log`
--
ALTER TABLE `agent_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `billing_template_log`
--
ALTER TABLE `billing_template_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing_terms_detail_log`
--
ALTER TABLE `billing_terms_detail_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `billing_terms_log`
--
ALTER TABLE `billing_terms_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `branch_log`
--
ALTER TABLE `branch_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cal_code_definition_log`
--
ALTER TABLE `cal_code_definition_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `challans_log`
--
ALTER TABLE `challans_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1133;

--
-- AUTO_INCREMENT for table `challan_items_log`
--
ALTER TABLE `challan_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1013;

--
-- AUTO_INCREMENT for table `chat_roles_log`
--
ALTER TABLE `chat_roles_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2269;

--
-- AUTO_INCREMENT for table `city_log`
--
ALTER TABLE `city_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13391;

--
-- AUTO_INCREMENT for table `company_log`
--
ALTER TABLE `company_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `config_log`
--
ALTER TABLE `config_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_person_log`
--
ALTER TABLE `contact_person_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148147;

--
-- AUTO_INCREMENT for table `country_log`
--
ALTER TABLE `country_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1622;

--
-- AUTO_INCREMENT for table `currency_log`
--
ALTER TABLE `currency_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `daily_work_entry_log`
--
ALTER TABLE `daily_work_entry_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `days_log`
--
ALTER TABLE `days_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `default_terms_condition_log`
--
ALTER TABLE `default_terms_condition_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `department_log`
--
ALTER TABLE `department_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `designation_log`
--
ALTER TABLE `designation_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `dispatch_followup_history_log`
--
ALTER TABLE `dispatch_followup_history_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drawing_number_log`
--
ALTER TABLE `drawing_number_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_leaves_log`
--
ALTER TABLE `employee_leaves_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `erection_commissioning_log`
--
ALTER TABLE `erection_commissioning_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `excise_log`
--
ALTER TABLE `excise_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followup_history_log`
--
ALTER TABLE `followup_history_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5143683;

--
-- AUTO_INCREMENT for table `followup_order_history_log`
--
ALTER TABLE `followup_order_history_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `for_required_log`
--
ALTER TABLE `for_required_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `foundation_drawing_required_log`
--
ALTER TABLE `foundation_drawing_required_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `google_sheets_log`
--
ALTER TABLE `google_sheets_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `google_sheet_staff_log`
--
ALTER TABLE `google_sheet_staff_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=334;

--
-- AUTO_INCREMENT for table `grade_log`
--
ALTER TABLE `grade_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `inbox_log`
--
ALTER TABLE `inbox_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `industry_type_log`
--
ALTER TABLE `industry_type_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `inquiry_confirmation_log`
--
ALTER TABLE `inquiry_confirmation_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inquiry_followup_history_log`
--
ALTER TABLE `inquiry_followup_history_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `inquiry_items_log`
--
ALTER TABLE `inquiry_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140047;

--
-- AUTO_INCREMENT for table `inquiry_log`
--
ALTER TABLE `inquiry_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161265;

--
-- AUTO_INCREMENT for table `inquiry_stage_log`
--
ALTER TABLE `inquiry_stage_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `inquiry_status_log`
--
ALTER TABLE `inquiry_status_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `inspection_required_log`
--
ALTER TABLE `inspection_required_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `interview_log`
--
ALTER TABLE `interview_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices_log`
--
ALTER TABLE `invoices_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_billing_terms_log`
--
ALTER TABLE `invoice_billing_terms_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_cal_code_definition_log`
--
ALTER TABLE `invoice_cal_code_definition_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_items_log`
--
ALTER TABLE `invoice_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_logins_log`
--
ALTER TABLE `invoice_logins_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_type_log`
--
ALTER TABLE `invoice_type_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `items_log`
--
ALTER TABLE `items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `item_bom_log`
--
ALTER TABLE `item_bom_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `item_category_log`
--
ALTER TABLE `item_category_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `item_class_log`
--
ALTER TABLE `item_class_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `item_details_log`
--
ALTER TABLE `item_details_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item_documents_log`
--
ALTER TABLE `item_documents_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1823;

--
-- AUTO_INCREMENT for table `item_files_log`
--
ALTER TABLE `item_files_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `item_group_code_log`
--
ALTER TABLE `item_group_code_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `item_make_log`
--
ALTER TABLE `item_make_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `item_status_log`
--
ALTER TABLE `item_status_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `item_type_log`
--
ALTER TABLE `item_type_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `lead_contact_person_log`
--
ALTER TABLE `lead_contact_person_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_details_log`
--
ALTER TABLE `lead_details_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_provider_log`
--
ALTER TABLE `lead_provider_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_source_log`
--
ALTER TABLE `lead_source_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lead_stage_log`
--
ALTER TABLE `lead_stage_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lead_status_log`
--
ALTER TABLE `lead_status_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `leaves_log`
--
ALTER TABLE `leaves_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_for_employee_log`
--
ALTER TABLE `leave_for_employee_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `letters_log`
--
ALTER TABLE `letters_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `loading_by_log`
--
ALTER TABLE `loading_by_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mailbox_folders_log`
--
ALTER TABLE `mailbox_folders_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailbox_mails_log`
--
ALTER TABLE `mailbox_mails_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailbox_settings_log`
--
ALTER TABLE `mailbox_settings_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mail_system_folder_log`
--
ALTER TABLE `mail_system_folder_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mail_system_log`
--
ALTER TABLE `mail_system_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182856;

--
-- AUTO_INCREMENT for table `main_group_log`
--
ALTER TABLE `main_group_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `material_process_type_log`
--
ALTER TABLE `material_process_type_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `material_specification_log`
--
ALTER TABLE `material_specification_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `module_roles_log`
--
ALTER TABLE `module_roles_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1474;

--
-- AUTO_INCREMENT for table `order_stage_log`
--
ALTER TABLE `order_stage_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `outbox_log`
--
ALTER TABLE `outbox_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `party_detail_log`
--
ALTER TABLE `party_detail_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `party_log`
--
ALTER TABLE `party_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38682;

--
-- AUTO_INCREMENT for table `party_old_log`
--
ALTER TABLE `party_old_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `party_type_1_log`
--
ALTER TABLE `party_type_1_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `party_type_2_log`
--
ALTER TABLE `party_type_2_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_log`
--
ALTER TABLE `payment_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `priority_log`
--
ALTER TABLE `priority_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `proforma_invoices_log`
--
ALTER TABLE `proforma_invoices_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proforma_invoice_billing_terms_log`
--
ALTER TABLE `proforma_invoice_billing_terms_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proforma_invoice_cal_code_definition_log`
--
ALTER TABLE `proforma_invoice_cal_code_definition_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proforma_invoice_items_log`
--
ALTER TABLE `proforma_invoice_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `proforma_invoice_logins_log`
--
ALTER TABLE `proforma_invoice_logins_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project_log`
--
ALTER TABLE `project_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_invoice_items_log`
--
ALTER TABLE `purchase_invoice_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT for table `purchase_invoice_log`
--
ALTER TABLE `purchase_invoice_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `purchase_order_items_log`
--
ALTER TABLE `purchase_order_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `purchase_order_log`
--
ALTER TABLE `purchase_order_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `quotations_log`
--
ALTER TABLE `quotations_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36270;

--
-- AUTO_INCREMENT for table `quotation_items_log`
--
ALTER TABLE `quotation_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37151;

--
-- AUTO_INCREMENT for table `quotation_reason_log`
--
ALTER TABLE `quotation_reason_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation_stage_log`
--
ALTER TABLE `quotation_stage_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `quotation_status_log`
--
ALTER TABLE `quotation_status_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `quotation_type_log`
--
ALTER TABLE `quotation_type_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `real_leave_log`
--
ALTER TABLE `real_leave_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reference_log`
--
ALTER TABLE `reference_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `reminder_log`
--
ALTER TABLE `reminder_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `road_insurance_by_log`
--
ALTER TABLE `road_insurance_by_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales_action_log`
--
ALTER TABLE `sales_action_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sales_log`
--
ALTER TABLE `sales_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales_order_billing_terms_log`
--
ALTER TABLE `sales_order_billing_terms_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_order_buyers_log`
--
ALTER TABLE `sales_order_buyers_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `sales_order_cal_code_definition_log`
--
ALTER TABLE `sales_order_cal_code_definition_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_order_items_log`
--
ALTER TABLE `sales_order_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2129;

--
-- AUTO_INCREMENT for table `sales_order_item_log`
--
ALTER TABLE `sales_order_item_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_order_log`
--
ALTER TABLE `sales_order_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1928;

--
-- AUTO_INCREMENT for table `sales_order_logins_log`
--
ALTER TABLE `sales_order_logins_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1927;

--
-- AUTO_INCREMENT for table `sales_order_notes_log`
--
ALTER TABLE `sales_order_notes_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sales_order_pref_log`
--
ALTER TABLE `sales_order_pref_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sales_order_stage_log`
--
ALTER TABLE `sales_order_stage_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_order_status_log`
--
ALTER TABLE `sales_order_status_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_status_log`
--
ALTER TABLE `sales_status_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales_type_log`
--
ALTER TABLE `sales_type_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `service_followup_history_log`
--
ALTER TABLE `service_followup_history_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_items_log`
--
ALTER TABLE `service_items_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `service_log`
--
ALTER TABLE `service_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff_log`
--
ALTER TABLE `staff_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=862;

--
-- AUTO_INCREMENT for table `staff_roles_log`
--
ALTER TABLE `staff_roles_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10704;

--
-- AUTO_INCREMENT for table `state_log`
--
ALTER TABLE `state_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `sub_group_log`
--
ALTER TABLE `sub_group_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier_log`
--
ALTER TABLE `supplier_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `terms_condition_detail_log`
--
ALTER TABLE `terms_condition_detail_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `terms_condition_group_log`
--
ALTER TABLE `terms_condition_group_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `terms_condition_template_log`
--
ALTER TABLE `terms_condition_template_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `terms_group_log`
--
ALTER TABLE `terms_group_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transportation_by_log`
--
ALTER TABLE `transportation_by_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `unloading_by_log`
--
ALTER TABLE `unloading_by_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `uom_log`
--
ALTER TABLE `uom_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `visitors_log`
--
ALTER TABLE `visitors_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=351;

--
-- AUTO_INCREMENT for table `website_modules_log`
--
ALTER TABLE `website_modules_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `weekly_holiday_log`
--
ALTER TABLE `weekly_holiday_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `yearly_leaves_log`
--
ALTER TABLE `yearly_leaves_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
