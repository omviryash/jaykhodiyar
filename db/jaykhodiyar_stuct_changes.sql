--
-- Chirag : 2017_12_07
--
ALTER TABLE `supplier` ADD `contact_no` TEXT NULL DEFAULT NULL AFTER `gstin`, ADD `tin_no` VARCHAR(255) NULL DEFAULT NULL AFTER `contact_no`, ADD `cst_no` VARCHAR(255) NULL DEFAULT NULL AFTER `tin_no`, ADD `pan_no` VARCHAR(255) NULL DEFAULT NULL AFTER `cst_no`, ADD `aadhar_no` VARCHAR(255) NULL DEFAULT NULL AFTER `pan_no`, ADD `cont_person` VARCHAR(255) NULL DEFAULT NULL AFTER `aadhar_no`, ADD `note` TEXT NULL DEFAULT NULL AFTER `cont_person`;
ALTER TABLE `supplier` CHANGE `supplier_name` `supplier_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address` `address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `phone_no` `tel_no` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email_id` `email_id` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;  

--
-- Avinash : 2017_12_07
--
ALTER TABLE `supplier` CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
DROP TRIGGER IF EXISTS `supplier_add_log`;
CREATE DEFINER=`root`@`localhost` TRIGGER `supplier_add_log` AFTER INSERT ON `supplier` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =NEW.supplier_id,
`supplier_name`  =NEW.supplier_name,
`address` =NEW.address,
`city_id`  =NEW.city_id,
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`tel_no`  =NEW.tel_no,
`email_id`  =NEW.email_id,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add';

DROP TRIGGER IF EXISTS `supplier_delete_log`;CREATE DEFINER=`root`@`localhost` TRIGGER `supplier_delete_log` AFTER DELETE ON `supplier` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =OLD.supplier_id,
`supplier_name`  =OLD.supplier_name,
`address` =OLD.address,
`city_id`  =OLD.city_id,
`state_id`  =OLD.state_id,
`country_id`  =OLD.country_id,
`tel_no`  =OLD.tel_no,
`email_id`  =OLD.email_id,
`created_by`=OLD.created_by,
`updated_by`=OLD.updated_by,
`created_at` =OLD.created_at,
`updated_at` =OLD.updated_at,
`operation_type` = 'delete';

DROP TRIGGER IF EXISTS `supplier_edit_log`;CREATE DEFINER=`root`@`localhost` TRIGGER `supplier_edit_log` AFTER UPDATE ON `supplier` FOR EACH ROW INSERT INTO  jaykhodiyar_logs.supplier_log SET
`supplier_id`  =NEW.supplier_id,
`supplier_name`  =NEW.supplier_name,
`address` =NEW.address,
`city_id`  =NEW.city_id,
`state_id`  =NEW.state_id,
`country_id`  =NEW.country_id,
`tel_no`  =NEW.tel_no,
`email_id`  =NEW.email_id,
`created_by`=NEW.created_by,
`updated_by`=NEW.updated_by,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit';

--
-- Table structure for table `purchase_project`
--

CREATE TABLE `purchase_project` (
	`project_id` int(11) NOT NULL,
	`project_name` varchar(255) DEFAULT NULL,
	`project_code` varchar(50) DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Avinash : 2017_12_08
--
--
-- Table structure for table `purchase_project_items`
--

CREATE TABLE `purchase_project_items` (
	`ppi_id` int(11) NOT NULL,
	`project_id` int(11) DEFAULT NULL,
	`item_id` int(11) DEFAULT NULL,
	`qty` double DEFAULT NULL,
	`uom_id` int(11) DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `purchase_project`
--
ALTER TABLE `purchase_project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `purchase_project_items`
--
ALTER TABLE `purchase_project_items`
  ADD PRIMARY KEY (`ppi_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchase_project`
--
ALTER TABLE `purchase_project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_project_items`
--
ALTER TABLE `purchase_project_items`
  MODIFY `ppi_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `purchase_order` ADD `terms_conditions` TEXT NULL DEFAULT NULL AFTER `round_off`;
ALTER TABLE `purchase_order` ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `created_at`;
ALTER TABLE `purchase_order` CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `purchase_order` ADD `is_approved` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Disapproved, 1 = Approved' AFTER `updated_at`, ADD `approved_by` INT(11) NULL DEFAULT NULL AFTER `is_approved`, ADD `approved_at` DATETIME NULL DEFAULT NULL AFTER `approved_by`;


UPDATE `terms_and_conditions` SET `module` = 'purchase_order_general_terms' WHERE `terms_and_conditions`.`id` = 1;
INSERT INTO `terms_and_conditions` (`id`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'purchase_order_electrict_components_terms', 'Purchase Order Electrict Components Terms', '1', '2017-12-09 00:00:00');
ALTER TABLE `purchase_order` ADD `terms_conditions_type` TINYINT(1) NOT NULL DEFAULT '1' AFTER `round_off`;


INSERT INTO `purchase_project` (`project_id`, `project_name`, `project_code`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, 'Project 1', 'p1', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00'), (NULL, 'Project 2', 'p2', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00');
INSERT INTO `purchase_project_items` (`ppi_id`, `project_id`, `item_id`, `qty`, `uom_id`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '1', '1', '100', '7', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00'), (NULL, '1', '2', '200', '7', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00'), (NULL, '1', '3', '250', '7', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00'), (NULL, '1', '4', '300', '7', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00');
INSERT INTO `purchase_project_items` (`ppi_id`, `project_id`, `item_id`, `qty`, `uom_id`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '2', '1', '200', '7', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00'), (NULL, '2', '2', '400', '7', '1', '2017-12-09 00:00:00', '1', '2017-12-09 00:00:00');


--
-- Avinash : 2017_12_11
--
ALTER TABLE `purchase_items` DROP `item_code1`, DROP `item_code2`, DROP `product_group`;
ALTER TABLE `purchase_items` CHANGE `product_code` `item_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
-- ALTER TABLE `purchase_project` DROP `product_code`;
ALTER TABLE `purchase_project_items` ADD `item_group_id` INT(11) NULL DEFAULT NULL AFTER `item_id`;
ALTER TABLE `purchase_project_items` ADD `item_name` VARCHAR(255) NULL DEFAULT NULL AFTER `item_id`;

-- 
-- Chirag : 2017_12_12
-- 
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('197', 'Master Purchase Item', '', '3.2.15');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '197'), (NULL, 'add', 'add', '197'), (NULL, 'edit', 'edit', '197'), (NULL, 'delete', 'delete', '197');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('198', 'Master Purchase Project', '', '3.2.16');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '198'), (NULL, 'add', 'add', '198'), (NULL, 'edit', 'edit', '198'), (NULL, 'delete', 'delete', '198');

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('199', 'Can Approve Purchase Order ', '', 'z');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '199');

ALTER TABLE `purchase_items` DROP `qty`, DROP `item_type`, DROP `conv_uom`, DROP `conv_qty`;

--
-- Avinash : 2017_12_12
--
ALTER TABLE `purchase_order` ADD `purchase_project_id` INT(11) NULL DEFAULT NULL AFTER `round_off`, ADD `purchase_project_qty` INT(11) NULL DEFAULT NULL AFTER `purchase_project_id`;
ALTER TABLE `purchase_items` ADD `item_make_id` INT(11) NULL DEFAULT NULL AFTER `item_code`;

--
-- Jignesh : 2017_12_12
-- Table structure for table `company_banks`
--
CREATE TABLE `company_banks` (
	`id` int(11) NOT NULL,
	`detail` text NOT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` datetime DEFAULT NULL,
	`updated_by` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_banks`
--

INSERT INTO `company_banks` (`id`, `detail`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Indusind Bank, The Imperial Heights, 150 Feet Ring Road Branch,  Rajkot (GUJ), India. Current  Account No. 250000006328, IFSC CODE: INDB 0000079', '2017-12-12 19:33:19', '2017-12-12 19:33:19', '2017-12-12 00:00:00', '2017-12-12 00:00:00'),
(2, 'State Bank Of India, Bhakti Nagar Branch, Gondal Road, Rajkot (GUJ) India Account No. 30211476817 \r\n(IFS CODE: SBIN0001851)\r\n', '2017-12-12 19:33:19', '2017-12-12 19:33:19', '2017-12-12 00:00:00', '2017-12-12 00:00:00');

--
-- Indexes for table `company_banks`
--
ALTER TABLE `company_banks`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `company_banks`
--
ALTER TABLE `company_banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `sales_order` ADD `loading_at` VARCHAR(55) NULL DEFAULT NULL AFTER `terms_condition_purchase`, ADD `port_of_loading_city` VARCHAR(55) NULL DEFAULT NULL AFTER `loading_at`, ADD `port_of_loading_country` VARCHAR(55) NULL DEFAULT NULL AFTER `port_of_loading_city`, ADD `delivery_thourgh` VARCHAR(222) NULL DEFAULT NULL AFTER `port_of_loading_country`, ADD `port_of_discharge_city` VARCHAR(55) NULL DEFAULT NULL AFTER `delivery_thourgh`, ADD `port_of_discharge_country` VARCHAR(55) NULL DEFAULT NULL AFTER `port_of_discharge_city`, ADD `place_of_delivery_city` VARCHAR(55) NULL DEFAULT NULL AFTER `port_of_discharge_country`, ADD `place_of_delivery_country` VARCHAR(55) NULL DEFAULT NULL AFTER `place_of_delivery_city`, ADD `shipping_line_name` VARCHAR(222) NULL DEFAULT NULL AFTER `place_of_delivery_country`, ADD `sea_freight` DOUBLE NULL DEFAULT NULL AFTER `shipping_line_name`, ADD `bank_detail` INT NULL DEFAULT NULL AFTER `sea_freight`;

--
-- Jignesh : 2017_12_13
--

ALTER TABLE `sales_order` ADD `export_order_type` VARCHAR(22) NULL DEFAULT NULL AFTER `bank_detail`;
ALTER TABLE `sales_order` ADD `ex_factory` DOUBLE NULL DEFAULT NULL AFTER `export_order_type`;

--
-- Added By : Jignesh
-- Generation Time: Dec 14, 2017 at 11:45 AM
--
--

UPDATE `terms_and_conditions` SET `module` = 'sales_order_terms_and_conditions_for_contract' WHERE `terms_and_conditions`.`module` = 'sales_order_terms_and_conditions_for_export';
INSERT INTO `terms_and_conditions` (`module`) VALUES ('sales_order_terms_and_conditions_for_export_order');

ALTER TABLE `sales_order` ADD `port_of_loading_country_id` INT NULL DEFAULT NULL AFTER `place_of_delivery_country`, ADD `port_of_discharge_country_id` INT NULL DEFAULT NULL AFTER `port_of_loading_country_id`, ADD `place_of_delivery_country_id` INT NULL DEFAULT NULL AFTER `port_of_discharge_country_id`;

--
-- Table structure for table `sea_freight_type`
--

CREATE TABLE `sea_freight_type` (
	`id` int(11) NOT NULL,
	`name` varchar(33) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` datetime DEFAULT NULL,
	`updated_by` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sea_freight_type`
--

INSERT INTO `sea_freight_type` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'CIF', '2017-12-11 00:00:00', NULL, NULL, NULL),
(2, 'C&F', '2017-12-11 00:00:00', NULL, NULL, NULL),
(3, 'FOB', '2017-12-11 00:00:00', NULL, NULL, NULL);

--
-- Indexes for table `sea_freight_type`
--
ALTER TABLE `sea_freight_type`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `sea_freight_type`
--
ALTER TABLE `sea_freight_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `sales_order` ADD `sea_freight_type` INT NULL DEFAULT NULL AFTER `ex_factory`;

--
-- Avinash : 2017_12_14
--
ALTER TABLE `purchase_items` CHANGE `uom` `uom` INT(11) NULL DEFAULT NULL, CHANGE `igst` `igst` DOUBLE NULL DEFAULT NULL, CHANGE `cgst` `cgst` DOUBLE NULL DEFAULT NULL, CHANGE `sgst` `sgst` DOUBLE NULL DEFAULT NULL, CHANGE `discount` `discount` DOUBLE NULL DEFAULT NULL;
ALTER TABLE `purchase_items` ADD `reference_qty` INT(11) NULL DEFAULT NULL AFTER `discount`, ADD `reference_qty_uom` INT(11) NULL DEFAULT NULL AFTER `reference_qty`;
ALTER TABLE `purchase_project_items` ADD `item_code` VARCHAR(255) NULL DEFAULT NULL AFTER `item_name`;
ALTER TABLE `purchase_project_items` ADD `project_reference_qty` DOUBLE NULL DEFAULT NULL AFTER `uom_id`, ADD `project_reference_qty_uom` INT(11) NULL DEFAULT NULL AFTER `project_reference_qty`;

ALTER TABLE `purchase_project_items` ADD `item_group` VARCHAR(255) NULL DEFAULT NULL AFTER `item_group_id`;
ALTER TABLE `purchase_project_items` ADD `uom_name` VARCHAR(255) NULL DEFAULT NULL AFTER `uom_id`;
ALTER TABLE `purchase_project_items` ADD `project_reference_qty_uom_name` VARCHAR(255) NULL DEFAULT NULL AFTER `project_reference_qty_uom`;

--
-- Jignesh : 2017_12_14
--

ALTER TABLE `proforma_invoices` ADD `agent_id` INT NOT NULL AFTER `branch_id`;
ALTER TABLE `proforma_invoice_items` ADD `currency_id` INT NOT NULL AFTER `rate`;

ALTER TABLE `proforma_invoices` ADD `port_of_loading_city` VARCHAR(55) NOT NULL AFTER `transit_insurance_igst`, 
ADD `port_of_loading_country` VARCHAR(55) NULL DEFAULT NULL  AFTER `port_of_loading_city`, 
ADD `delivery_thourgh` VARCHAR(55) NULL DEFAULT NULL AFTER `port_of_loading_country`, 
ADD `port_of_discharge_city` VARCHAR(55) NULL DEFAULT NULL AFTER `delivery_thourgh`, 
ADD `place_of_delivery_city` VARCHAR(55) NULL DEFAULT NULL AFTER `port_of_discharge_city`, 
ADD `place_of_delivery_country` VARCHAR(55) NULL DEFAULT NULL AFTER `place_of_delivery_city`, 
ADD `port_of_loading_country_id` INT NULL DEFAULT NULL AFTER `place_of_delivery_country`, 
ADD `port_of_discharge_country_id` INT NULL DEFAULT NULL AFTER `port_of_loading_country_id`, 
ADD `place_of_delivery_country_id` INT NULL DEFAULT NULL AFTER `port_of_discharge_country_id`, 
ADD `shipping_line_name` VARCHAR(222) NULL DEFAULT NULL AFTER `place_of_delivery_country_id`;

ALTER TABLE `proforma_invoices` ADD `sea_freight` DOUBLE NULL DEFAULT NULL AFTER `shipping_line_name`,
ADD `bank_detail` INT NULL DEFAULT NULL AFTER `sea_freight`, 
ADD `export_order_type` INT NULL DEFAULT NULL AFTER `bank_detail`,
ADD `ex_factory` DOUBLE NULL DEFAULT NULL AFTER `export_order_type`, 
ADD `sea_freight_type` INT NULL DEFAULT NULL AFTER `ex_factory`;

ALTER TABLE `proforma_invoices` ADD `port_of_discharge_country` VARCHAR(22) NULL DEFAULT NULL AFTER `port_of_discharge_country_id`;
ALTER TABLE `proforma_invoices` CHANGE `export_order_type` `export_order_type` VARCHAR(22) NULL DEFAULT NULL;

--
-- Avinash : 2017_12_14
--
ALTER TABLE `sales_order` DROP `ex_factory`;
ALTER TABLE `proforma_invoices` DROP `ex_factory`;

-- Avinash : 2017_12_15
UPDATE `company_banks` SET `detail` = '<b>Indusind Bank</b>, The Imperial Heights, 150 Feet Ring Road Branch, Rajkot (GUJ), India. <br />Current Account No. <b>250000006328</b>, IFSC CODE: <b>INDB0000079</b>' WHERE `company_banks`.`id` = 1;
UPDATE `company_banks` SET `detail` = '<b>State Bank Of India</b>, Bhakti Nagar Branch, Gondal Road, Rajkot (GUJ) India<br/> Account No. <b>30211476817</b> (IFS CODE: <b>SBIN0001851</b>)' WHERE `company_banks`.`id` = 2;

-- Jignesh : 2017_12_15
ALTER TABLE `proforma_invoices` CHANGE `agent_id` `agent_id` INT(11) NULL DEFAULT NULL, CHANGE `billing_terms_id` `billing_terms_id` INT(11) NULL DEFAULT NULL, CHANGE `contact_no` `contact_no` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cust_po_no` `cust_po_no` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email_id` `email_id` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `port_of_loading_city` `port_of_loading_city` VARCHAR(55) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Jignesh : 2017_12_18
ALTER TABLE `proforma_invoice_items` CHANGE `item_code` `item_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_name` `item_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_description` `item_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `add_description` `add_description` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `detail_description` `detail_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `drawing_number` `drawing_number` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `drawing_revision` `drawing_revision` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uom` `uom` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `total_amount` `total_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_note` `item_note` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `quantity` `quantity` DOUBLE NULL DEFAULT NULL, CHANGE `disc_per` `disc_per` DOUBLE NULL DEFAULT NULL, CHANGE `rate` `rate` DOUBLE NULL DEFAULT NULL, CHANGE `currency_id` `currency_id` INT(11) NULL DEFAULT NULL, CHANGE `disc_value` `disc_value` DOUBLE NULL DEFAULT NULL, CHANGE `amount` `amount` DOUBLE NULL DEFAULT NULL, CHANGE `net_amount` `net_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_status` `item_status` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cust_part_no` `cust_part_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Avinash : 2017_12_18
ALTER TABLE `sales_order` ADD `is_dispatched` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Pending Dispatch, 1 = Dispatched' AFTER `isApproved`;
ALTER TABLE `sales_order_items` ADD `dispatched_qty` INT(11) NOT NULL DEFAULT '0' AFTER `item_extra_accessories_id`;

-- Jignesh : 2017_12_19
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Quotation Management', '', 'y_admin'), (NULL, 'Sales Order Management', '', 'y_admin'), (NULL, 'Proforma Management', '', 'y_admin'), (NULL, 'Challan Management', '', 'y_admin');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '200'), (NULL, 'allow', 'allow', '201'), (NULL, 'allow', 'allow', '202'), (NULL, 'allow', 'allow', '203');

-- Avinash : 2017_12_19
ALTER TABLE `sales_order` ADD `delete_not_allow` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Delete Allow, 1 = Delete Not Allow' AFTER `is_dispatched`;

-- Jignesh : 2017_12_19
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Purchase Order Items', '', '4.3.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'add', 'add', '204'), (NULL, 'edit', 'edit', '204'), (NULL, 'delete', 'delete', '204');

-- Jignesh : 2017_12_21
ALTER TABLE `quotations` ADD `is_proforma_created` INT NULL DEFAULT '0' AFTER `pdf_url`;
ALTER TABLE `quotations` CHANGE `is_proforma_created` `is_proforma_created` INT(11) NULL DEFAULT '0' COMMENT '0 = proforma invoice not created, 1 = proforma invoice created.';

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Quotation Followup', 'Quotation Followup', '5.3.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'add', 'add', '205'), (NULL, 'edit', 'edit', '205'), (NULL, 'delete', 'delete', '205');

ALTER TABLE `proforma_invoice_logins` CHANGE `approved_date_time` `approved_date_time` DATETIME NULL;

-- Avinash : 2017_12_21
ALTER TABLE `challans` ADD `is_invoice_created` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Invoice not created, 1 = Invoice Created' AFTER `challan_no`;

ALTER TABLE `invoice_items` CHANGE `igst` `igst` DOUBLE NULL DEFAULT NULL, CHANGE `igst_amount` `igst_amount` DOUBLE NULL DEFAULT NULL, CHANGE `cgst` `cgst` DOUBLE NULL DEFAULT NULL, CHANGE `cgst_amount` `cgst_amount` DOUBLE NULL DEFAULT NULL, CHANGE `sgst` `sgst` DOUBLE NULL DEFAULT NULL, CHANGE `sgst_amount` `sgst_amount` DOUBLE NULL DEFAULT NULL;

-- Jignesh : 2017_12_22
ALTER TABLE `payment` ADD `referance_from` VARCHAR(33) NULL DEFAULT NULL AFTER `payment_note`, ADD `referance_no` INT NULL DEFAULT NULL AFTER `referance_from`, ADD `payment_bank` VARCHAR(66) NULL DEFAULT NULL AFTER `referance_no`, ADD `payment_by` INT NULL DEFAULT NULL COMMENT 'id from payment_by master' AFTER `payment_bank`, ADD `payment_by_no` INT NULL DEFAULT NULL AFTER `payment_by`, ADD `receipt_date_2` DATE NULL DEFAULT NULL AFTER `payment_by_no`, ADD `receipt_no` INT NULL DEFAULT NULL AFTER `receipt_date_2`;

-- 
-- Generation Time: Dec 22, 2017 at 11:28 AM
-- Table structure for table `payment_by`
--

CREATE TABLE `payment_by` (
	`id` int(11) NOT NULL,
	`name` varchar(55) NOT NULL,
	`created_at` datetime NOT NULL,
	`updated_at` datetime NOT NULL,
	`created_by` int(11) NOT NULL,
	`updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_by`
--
INSERT INTO `payment_by` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Cash', '2017-12-22 03:00:00', '2017-12-22 03:00:00', 1, 0),
(2, 'RTGS', '2017-12-22 03:00:00', '2017-12-22 03:00:00', 1, 0),
(3, 'Cheque', '2017-12-22 03:00:00', '2017-12-22 03:00:00', 1, 0),
(4, 'NEFT', '2017-12-22 03:00:00', '2017-12-22 03:00:00', 1, 0),
(5, 'IMPS', '2017-12-22 03:00:00', '2017-12-22 03:00:00', 1, 0);

--
-- Indexes for table `payment_by`
--
ALTER TABLE `payment_by`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `payment_by`
--
ALTER TABLE `payment_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

-- Jignesh : 2017_12_23 10:40
DELETE FROM `website_modules` WHERE `website_modules`.`id` = 203;
DELETE FROM `module_roles` WHERE `module_roles`.`module_id` = 203;

-- Avinash : 2017_12_23
DROP TABLE `website_modules`;

--
-- Table structure for table `website_modules`
--
CREATE TABLE `website_modules` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `main_module` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `website_modules`
--
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES
(1, 'Party', 'party', '5.1'),
(2, 'Party Type', 'party', '1.1'),
(4, 'Enquiry', 'inquiry', '5.2'),
(5, 'Quotation', 'quotations', '5.3'),
(6, 'Sales Order', 'sales_order', '5.4'),
(9, 'Master >> Item', '', '3.2'),
(10, 'Agent', '', '3.3'),
(11, 'Sales', '', '3.4'),
(15, 'Purchase Menu', '', '4'),
(16, 'Sales Menu', '', '5'),
(19, 'Payment', '', '8.1'),
(21, 'Top Right Menu >> Profile >> Change Password', '', '11.1.11'),
(22, 'Dashboard Demo', '', '91'),
(23, 'User Dropdown', '', '1.3'),
(24, 'Can Approve Sales Order', '', '5.4.2'),
(25, 'Apply For Leave >> Reply Leave', '', '13.2'),
(26, 'Google Sheet', '', '14.1'),
(28, 'Supplier', '', '4.1'),
(29, 'Purchase Invoice', '', '4.3'),
(30, 'Purchase Order', '', '4.2'),
(31, 'Sales Chart', '', '9.11'),
(32, 'Outstanding', '', '8.2.2'),
(33, 'Proforma Invoice', '', '5.5'),
(35, 'Dashboard', '', '1'),
(36, 'Master', '', '3'),
(37, 'Master >> Company Detail', '', '3.1'),
(38, 'Master >> Sales >> Reference', '', '3.4.1'),
(39, 'Master >> Sales >> Enquiry Stage', '', '3.4.2'),
(40, 'Master >> Sales >> Industry Type', '', '3.4.3'),
(41, 'Master >> Sales >> Enquiry Status', '', '3.4.4'),
(42, 'Master >> Sales >> Priority', '', '3.4.5'),
(43, 'Master >> Sales >> UOM', '', '3.4.6'),
(44, 'Master >> Sales >> Quotation Type', '', '3.4.7'),
(45, 'Master >> Sales >> Sales Type', '', '3.4.8'),
(46, 'Master >> Sales >> Reason (Quotation)', '', '3.4.9'),
(47, 'Master >> Sales >> Currency', '', '3.4.10'),
(48, 'Master >> Sales >> Sales', '', '3.4.11'),
(49, 'Master >> Sales >> Action', '', '3.4.12'),
(50, 'Master >> Sales >> Quot Stage', '', '3.4.13'),
(51, 'Master >> Sales >> Status', '', '3.4.14'),
(52, 'Master >> Sales >> Enquiry Confirmation', '', '3.4.15'),
(53, 'Master >> Sales >> Order Stage', '', '3.4.16'),
(54, 'Master >> HR', '', '3.5'),
(55, 'Master >> HR Days', '', '3.5.1'),
(56, 'Master >> HR Grade', '', '3.5.2'),
(57, 'Master >> Terms & Condition', '', '3.6'),
(58, 'Master >> Terms & Condition Group', '', '3.6.2'),
(59, 'Master >> Terms & Condition Details', '', '3.6.3'),
(60, 'Master >> Default Terms & Condition', '', '3.6.4'),
(61, 'Master >> Terms & Condition Template', '', '3.6.5'),
(62, 'Master >> General Master', '', '3.7'),
(63, 'Master >> General Master >> Branch', '', '3.7.1'),
(64, 'Master >> General Master >> Project', '', '3.7.2'),
(65, 'Master >> General Master >> Billing Template', '', '3.7.3'),
(66, 'Master >> General Master >> Country', '', '3.7.4'),
(67, 'Master >> General Master >> State', '', '3.7.5'),
(68, 'Master >> General Master >> City', '', '3.7.6'),
(69, 'Master >> General Master >> Reference', '', '3.7.7'),
(70, 'Master >> General Master >> Designation', '', '3.7.8'),
(71, 'Master >> General Master >> Department', '', '3.7.9'),
(72, 'Master >> General Master >> Terms Group', '', '3.7.10'),
(73, 'Master >> Staff Roles', '', '3.8'),
(74, 'Master >> Chat Roles', '', '3.9'),
(75, 'Master >> Reminder', '', '3.92'),
(76, 'Master >> Billing Terms', '', '3.95'),
(79, 'Ledger', '', '8.2.3'),
(93, 'Sales >> Party >> Log', '', '51.1'),
(107, 'Track Sale', '', '9.12'),
(108, 'Dispatch Menu', '', '6'),
(109, 'Challan', '', '6.1'),
(110, 'Customer List', '', '6.4'),
(111, 'Invoice', '', '6.3'),
(112, 'Service Menu', '', '7'),
(113, 'Finance Menu', '', '8'),
(114, 'Service', '', '7.1'),
(118, 'Finance New Payment', '', '8.2.1'),
(119, 'Summary Report', '', '9.13'),
(121, 'Report Menu', '', '9'),
(122, 'Report List Of Enquiry', '', '9.14'),
(123, 'Report List Of Quotation', '', '9.15'),
(124, 'Report Sales Lead Summary', '', '9.16'),
(125, 'Report List Of Sales Order', '', '9.17'),
(126, 'Report Delay Quotation Report', '', '9.18'),
(127, 'Report List Of Due Sales Order', '', '9.19'),
(128, 'Report Sales Order Status', '', '9.20'),
(129, 'Report Sales Summary', '', '9.21'),
(130, 'Report Dispatch Summary', '', '9.22'),
(131, 'Report Payment Receipt Details', '', '9.23'),
(132, 'Report Production Schedule', '', '9.24'),
(133, 'Report Stock Management', '', '9.25'),
(134, 'Report Quotation Management', '', '9.26'),
(135, 'Report Enquiry Followup History', '', '9.27'),
(136, 'Report Quotation followup history', '', '9.28'),
(137, 'Report Pending Report', '', '9.29'),
(138, 'Report BOM Info Report', '', '9.30'),
(139, 'General', '', '10'),
(140, 'Daily Work Entry', '', '10.1'),
(141, 'HR', '', '11'),
(142, 'HR >> Employee Master', '', '11.1'),
(143, 'HR >> Employee Master >> Manage Employee', '', '11.1.1'),
(144, 'HR >> Employee Master >> Calculate Salary', '', '11.1.2'),
(145, 'HR >> Leave Master', '', '11.2'),
(146, 'HR >> Leave Master >> Total Free Leaves', '', '11.2.1'),
(147, 'HR >> Leave Master >> Weekly Leaves', '', '11.2.2'),
(148, 'HR >> Leave Master >> Yearly Leaves', '', '11.2.3'),
(149, 'HR >> Expected Interview', '', '11.3'),
(150, 'HR >> Content Management', '', '11.4'),
(151, 'HR >> Content Management >> Offer Letter', '', '11.4.1'),
(152, 'HR >> Content Management >> Appointment Letter', '', '11.4.2'),
(153, 'HR >> Content Management >> Confirmation Letter', '', '11.4.3'),
(154, 'HR >> Content Management >> Experience Letter', '', '11.4.4'),
(155, 'HR >> Content Management >> NOC Letter', '', '11.4.5'),
(156, 'HR >> Content Management >> Salary Slip Terms', '', '11.4.6'),
(157, 'HR >> Leave', '', '11.5'),
(158, 'Mail System', '', '12'),
(159, 'Mail System >> Inbox', '', '12.1'),
(160, 'Mail System >> Compose', '', '12.2'),
(161, 'Mail System >> Send Outbox Mails', '', '12.3'),
(162, 'Apply For Leave', '', '13.1'),
(163, 'Google Sheet Menu', '', '14'),
(164, 'Apply For Leave Menu', '', '13'),
(166, 'Cron', '', '15'),
(167, 'Cron Agent Chat', '', '15.1'),
(168, 'Cron Load All Staffs Unread Mails', '', '15.2'),
(169, 'Cron Load Reminders', '', '15.3'),
(170, 'Cron Remove old files', '', '15.4'),
(171, 'Master >> Item >> Sales Item', '', '3.2.1'),
(172, 'Master >> Item >> Item Category', '', '3.2.21'),
(173, 'Master >> Item >> Item Group Code', '', '3.2.22'),
(174, 'Master >> Item >> Item Type', '', '3.2.23'),
(175, 'Master >> Item >> Item Status', '', '3.2.24'),
(176, 'Master >> Item >> Material Process Type', '', '3.2.25'),
(177, 'Master >> Item >> Main Group', '', '3.2.26'),
(178, 'Master >> Item >> Sub Group', '', '3.2.27'),
(179, 'Master >> Item >> Make', '', '3.2.28'),
(180, 'Master >> Item >> Drawing Number', '', '3.2.29'),
(181, 'Master >> Item >> Material Specification', '', '3.2.30'),
(182, 'Master >> Item >> Class', '', '3.2.31'),
(183, 'Master >> Item >> Excise', '', '3.2.32'),
(184, 'Logs', '', '51'),
(186, 'Master >> Terms & Condition >> Terms & Conditions', '', '3.6.1'),
(188, 'Can Approve Proforma Invoice', '', '5.5.2'),
(189, 'Master >> Customer Reminder', '', '3.93'),
(190, 'Customer Reminder Send ( Module Wise)', '', '3.94'),
(191, 'Master Product Group', '', '3.2.96'),
(192, 'Master >> Sales >> Followup Category', '', '3.4.97'),
(193, 'Master >> Quotation Reminder Roles', '', '3.91'),
(194, 'Master >> Sales >> Purchase Order Ref', '', '3.4.17'),
(195, 'Master >> Sales >> Quatation Status', '', '3.4.18'),
(196, 'Management', '', '1.2'),
(197, 'Master >> Item >> Purchase Item', '', '3.2.11'),
(198, 'Master >> Item >> Purchase Project', '', '3.2.12'),
(199, 'Can Approve Purchase Order ', '', '4.2.2'),
(200, 'Quotation Management', '', '5.3.2'),
(201, 'Sales Order Management', '', '5.4.1'),
(202, 'Proforma Management', '', '5.5.1'),
(204, 'Purchase Order >> Items', '', '4.2.1'),
(205, 'Quotation Followup', 'Quotation Followup', '5.3.1');

--
-- Indexes for table `website_modules`
--
ALTER TABLE `website_modules`
  ADD PRIMARY KEY (`id`);


-- AUTO_INCREMENT for table `website_modules`
--
ALTER TABLE `website_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

-- Jignesh : 2017_12_23 12:24
ALTER TABLE `payment` ADD `is_approved` TINYINT NULL DEFAULT '0' AFTER `receipt_no`, ADD `approved_by` INT NULL DEFAULT NULL AFTER `is_approved`, ADD `approved_at` DATETIME NULL DEFAULT NULL AFTER `approved_by`;

-- Jignesh : 2017_12_23 03:33
DELETE FROM `website_modules` WHERE `website_modules`.`title` = 'Finance New Payment';
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Can Approve Payment Receipt', '', '8.1.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '206');
UPDATE `website_modules` SET `title` = 'Account Menu' WHERE `website_modules`.`title` = 'Finance Menu';

-- Avinash : 2017_12_23
ALTER TABLE `invoice_items` ADD `challan_no` INT(11) NULL DEFAULT NULL AFTER `invoice_id`;
ALTER TABLE `invoice_logins` CHANGE `approved_date_time` `approved_date_time` DATETIME NULL DEFAULT NULL;
ALTER TABLE `invoices` CHANGE `invoice_no` `invoice_no` INT(4) UNSIGNED ZEROFILL NULL DEFAULT NULL;
ALTER TABLE `invoices` ADD `bank_detail` TEXT NULL DEFAULT NULL AFTER `transit_insurance_igst`;
ALTER TABLE `invoices` ADD `sea_freight_type` INT(11) NULL DEFAULT NULL AFTER `bank_detail`;

-- Jignesh : 2017_12_23 07:33
INSERT INTO `terms_and_conditions` (`id`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'payment_declaration', NULL, NULL, NULL);
ALTER TABLE `payment` ADD `reciver_id` INT NULL DEFAULT NULL AFTER `receipt_no`;
ALTER TABLE `payment` CHANGE `receipt_no` `receipt_no` INT(7) ZEROFILL NULL DEFAULT NULL;


ALTER TABLE `staff` ADD `signature` VARCHAR(222) NULL DEFAULT NULL AFTER `image`;

-- Avinash : 2017_12_25
ALTER TABLE `invoices` ADD `contact_person` VARCHAR(50) NULL DEFAULT NULL AFTER `pdf_url`, ADD `contact_person_no` VARCHAR(50) NULL DEFAULT NULL AFTER `contact_person`, ADD `driver_name` VARCHAR(50) NULL DEFAULT NULL AFTER `contact_person_no`, ADD `driver_contact_no` VARCHAR(50) NULL DEFAULT NULL AFTER `driver_name`;

-- Chirag : 2017_12_25
ALTER TABLE `company` ADD `lut` VARCHAR(255) NULL DEFAULT NULL AFTER `gst_no`, ADD `end_use_code` VARCHAR(255) NULL DEFAULT NULL AFTER `lut`;

-- Avinash : 2017_12_26
DELETE FROM `terms_and_conditions` WHERE `terms_and_conditions`.`id` = 8;
DELETE FROM `terms_and_conditions` WHERE `terms_and_conditions`.`id` = 15;

ALTER TABLE `followup_history` ADD `followup_at` DATETIME NULL DEFAULT NULL AFTER `followup_by`, ADD `updated_by` INT NULL DEFAULT NULL AFTER `followup_at`, ADD `updated_at` DATETIME NULL DEFAULT NULL AFTER `updated_by`;

-- Avinash : 2017_12_27
ALTER TABLE `invoice_items` ADD `packing_mark` VARCHAR(100) NULL DEFAULT NULL AFTER `item_extra_accessories_id`;
ALTER TABLE `invoice_items` ADD `no_of_packing` VARCHAR(100) NULL DEFAULT NULL AFTER `packing_mark`;
ALTER TABLE `invoices` ADD `lc_no` VARCHAR(100) NULL DEFAULT NULL AFTER `sea_freight_type`, ADD `lc_date` DATE NULL DEFAULT NULL AFTER `lc_no`, ADD `consignee_info` TEXT NULL DEFAULT NULL AFTER `lc_date`;
ALTER TABLE `invoices` ADD `port_of_loading_country_id` INT(11) NULL DEFAULT NULL AFTER `port_place_of_delivery`, ADD `port_of_discharge_country_id` INT(11) NULL DEFAULT NULL AFTER `port_of_loading_country_id`, ADD `place_of_delivery_country_id` INT(11) NULL DEFAULT NULL AFTER `port_of_discharge_country_id`;
ALTER TABLE `invoices` ADD `sea_freight` VARCHAR(100) NULL DEFAULT NULL AFTER `bank_detail`;
ALTER TABLE `invoices` ADD `origin_of_goods_country_id` INT(11) NULL DEFAULT NULL AFTER `sea_freight_type`, ADD `final_destination_country_id` INT(11) NULL DEFAULT NULL AFTER `origin_of_goods_country_id`, ADD `line_seal_no` VARCHAR(100) NULL DEFAULT NULL AFTER `final_destination_country_id`, ADD `exporter_seal_no` VARCHAR(100) NULL DEFAULT NULL AFTER `line_seal_no`;
ALTER TABLE `invoice_items` ADD `gross_weight` VARCHAR(50) NULL DEFAULT NULL AFTER `no_of_packing`, ADD `gross_weight_uom_id` INT(11) NULL DEFAULT NULL AFTER `gross_weight`, ADD `net_weight` VARCHAR(50) NULL DEFAULT NULL AFTER `gross_weight_uom_id`, ADD `net_weight_uom_id` INT(11) NULL DEFAULT NULL AFTER `net_weight`;

-- Avinash : 2017_12_27
INSERT INTO `terms_and_conditions` (`id`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'sales_invoice_payment_terms_for_export', 'sales_invoice_payment_terms_for_export', '1', NULL), (NULL, 'sales_invoice_declaration_for_export', 'sales_invoice_declaration_for_export', '1', NULL);

-- Chirag : 2017_12_27
ALTER TABLE `payment` CHANGE `payment_by_no` `payment_by_no` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `payment` CHANGE `created_by` `created_by` INT(11) NULL DEFAULT NULL, CHANGE `updated_by` `updated_by` INT(11) NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- Avinash : 2017_12_28
ALTER TABLE `challans` CHANGE `challan_no` `challan_no` INT(255) NULL DEFAULT NULL;

-- Chirag : 2017_12_29
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Can Approve Invoice', '', '6.3.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '207');


--
-- Jignesh : 2017_12_29 2:34
-- Table staructure for `testing_report` table
--

CREATE TABLE `testing_report` (
  `testing_report_id` int(11) NOT NULL,
  `challan_id` int(11) NOT NULL,
  `testing_report_no` varchar(33) DEFAULT NULL,
  `testing_report_date` date DEFAULT NULL,
  `quotation_no` varchar(33) DEFAULT NULL,
  `purchase_order_no` varchar(33) DEFAULT NULL,
  `purchase_order_date` date DEFAULT NULL,
  `sales_order_no` varchar(33) DEFAULT NULL,
  `sales_order_date` date DEFAULT NULL,
  `manufacturing_m_y` varchar(33) DEFAULT NULL,
  `assembling_equipment` date DEFAULT NULL,
  `quality_checked_by` int(11) DEFAULT NULL,
  `equipment_testing_date` date DEFAULT NULL,
  `equipment_testing_by` int(11) DEFAULT NULL,
  `equipment_inspection_by` int(11) DEFAULT NULL,
  `delivery_of_equipment` date DEFAULT NULL,
  `installation_commissioning_by` int(11) DEFAULT NULL,
  `installation_commissioning_by_text` varchar(88) DEFAULT NULL,
  `equipment_full_load_operating_by` varchar(66) DEFAULT NULL,
  `our_site_incharge_person` int(11) DEFAULT NULL,
  `our_site_incharge_person_text` varchar(66) DEFAULT NULL,
  `project_training_provide_by` varchar(66) DEFAULT NULL,
  `your_site_incharge_person_name` varchar(66) DEFAULT NULL,
  `your_site_incharge_person_contect_no` varchar(66) DEFAULT NULL,
  `your_technical_operator_person` varchar(66) DEFAULT NULL,
  `party_name` varchar(66) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `kind_attn_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(66) DEFAULT NULL,
  `jk_testing_normal_capacity_hrs` varchar(66) DEFAULT NULL,
  `jk_testing_normal_test_raw_material` varchar(66) DEFAULT NULL,
  `jk_testing_normal_moisture_raw_material` varchar(66) DEFAULT NULL,
  `jk_testing_normal_after_procces_moisture` varchar(66) DEFAULT NULL,
  `jk_testing_full_capacity_hrs` varchar(66) DEFAULT NULL,
  `jk_testing_full_test_raw_material` varchar(66) DEFAULT NULL,
  `jk_testing_full_moisture_raw_material` varchar(66) DEFAULT NULL,
  `jk_testing_full_after_procces_moisture` varchar(66) DEFAULT NULL,
  `jk_temp_oli` varchar(66) DEFAULT NULL,
  `jk_temp_eqipment` varchar(66) DEFAULT NULL,
  `jk_temp_head` varchar(66) DEFAULT NULL,
  `jk_temp_kiln` varchar(66) DEFAULT NULL,
  `jk_temp_hot_air` varchar(66) DEFAULT NULL,
  `jk_temp_weather` varchar(66) DEFAULT NULL,
  `c_testing_normal_capacity_hrs` varchar(66) DEFAULT NULL,
  `c_testing_normal_test_raw_material` varchar(66) DEFAULT NULL,
  `c_testing_normal_moisture_raw_material` varchar(66) DEFAULT NULL,
  `c_testing_normal_after_procces_moisture` varchar(66) DEFAULT NULL,
  `c_testing_full_capacity_hrs` varchar(66) DEFAULT NULL,
  `c_testing_full_test_raw_material` varchar(66) DEFAULT NULL,
  `c_testing_full_moisture_raw_material` varchar(66) DEFAULT NULL,
  `c_testing_full_after_procces_moisture` varchar(66) DEFAULT NULL,
  `c_temp_oli` varchar(66) DEFAULT NULL,
  `c_temp_eqipment` varchar(66) DEFAULT NULL,
  `c_temp_head` varchar(66) DEFAULT NULL,
  `c_temp_kiln` varchar(66) DEFAULT NULL,
  `c_temp_hot_air` varchar(66) DEFAULT NULL,
  `c_temp_weather` varchar(66) DEFAULT NULL,
  `test_specification` text,
  `test_detail` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `testing_report`
--
ALTER TABLE `testing_report`
  ADD PRIMARY KEY (`testing_report_id`);
--
-- AUTO_INCREMENT for table `testing_report`
--
ALTER TABLE `testing_report`
  MODIFY `testing_report_id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `challans` ADD `is_testing_report_created` TINYINT NOT NULL DEFAULT '0' AFTER `is_invoice_created`;

-- Jignesh : 29/12/2017 06:24 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Testing Report', '', '6.2');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '208'), (NULL, 'add', 'add', '208'), (NULL, 'edit', 'edit', '208'), (NULL, 'delete', 'delete', '208');

ALTER TABLE `challan_items_motor_details` ADD `normal_amps_jk` varchar(66) DEFAULT NULL AFTER `motor_serial_no`;
ALTER TABLE `challan_items_motor_details` ADD `working_amps_jk` varchar(66) DEFAULT NULL AFTER `normal_amps_jk`;
ALTER TABLE `challan_items_motor_details` ADD `equipment_rpm_jk` varchar(66) DEFAULT NULL AFTER `working_amps_jk`;
ALTER TABLE `challan_items_motor_details` ADD `total_workinghours_jk` varchar(66) DEFAULT NULL AFTER `equipment_rpm_jk`;
ALTER TABLE `challan_items_motor_details` ADD `normal_amps_c` varchar(66) DEFAULT NULL AFTER `total_workinghours_jk`;
ALTER TABLE `challan_items_motor_details` ADD `working_amps_c` varchar(66) DEFAULT NULL AFTER `normal_amps_c`;
ALTER TABLE `challan_items_motor_details` ADD `equipment_rpm_c` varchar(66) DEFAULT NULL AFTER `working_amps_c`;
ALTER TABLE `challan_items_motor_details` ADD `total_workinghours_c` varchar(66) DEFAULT NULL AFTER `equipment_rpm_c`;

-- Jignesh : 30/12/2017 06:42 PM
ALTER TABLE `testing_report` ADD `our_site_incharge_person_contact` VARCHAR(66) NULL DEFAULT NULL AFTER `our_site_incharge_person_text`;

-- Avinash : 2018_01_01
ALTER TABLE `sales_order` ADD `packing_forwarding_by` TINYINT(1) NULL DEFAULT '1' COMMENT '1 = Excluding, 2 = Including' AFTER `machine_to_all_motor_cable_igst`;
ALTER TABLE `sales_order` ADD `sea_freight_by` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1 = Excluding, 2 = Including' AFTER `shipping_line_name`;
ALTER TABLE `proforma_invoices` ADD `packing_forwarding_by` TINYINT(1) NULL DEFAULT '1' COMMENT '1 = Excluding, 2 = Including' AFTER `freight_igst`;
ALTER TABLE `proforma_invoices` ADD `sea_freight_by` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1 = Excluding, 2 = Including' AFTER `shipping_line_name`;

ALTER TABLE `sales_order` DROP `delivery_thourgh`;
ALTER TABLE `sales_order` ADD `delivery_through_id` INT(11) NULL DEFAULT NULL AFTER `place_of_delivery_country_id`;
ALTER TABLE `challans` ADD `port_of_loading_country_id` INT(11) NULL DEFAULT NULL AFTER `port_place_of_delivery`, ADD `port_of_discharge_country_id` INT(11) NULL DEFAULT NULL AFTER `port_of_loading_country_id`, ADD `place_of_delivery_country_id` INT(11) NULL DEFAULT NULL AFTER `port_of_discharge_country_id`;

-- Jignesh : 02/01/2018
ALTER TABLE `department` ADD `is_deleted` INT NOT NULL DEFAULT '1' AFTER `department`;

-- Avinash : 2018_01_02
ALTER TABLE `proforma_invoice_items` ADD `dispatched_qty` INT(11) NOT NULL DEFAULT '0' AFTER `quantity`;
ALTER TABLE `challans` ADD `proforma_invoice_id` INT(11) NULL DEFAULT NULL AFTER `sales_order_item_id`, ADD `proforma_invoice_item_id` INT(11) NULL DEFAULT NULL AFTER `proforma_invoice_id`;
ALTER TABLE `proforma_invoices` ADD `is_dispatched` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Pending Dispatch, 1 = Dispatched' AFTER `isApproved`, ADD `delete_not_allow` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Delete Allow, 1 = Delete Not Allow' AFTER `is_dispatched`;

-- Jignesh : 02/01/2018 7:15 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES 
(NULL, 'Challan', '', '3.10'), 
(NULL, 'Master >> Challan >> Item User', '', '3.10.1'), 
(NULL, 'Master >> Challan >> Item Make', '', '3.10.2'), 
(NULL, 'Master >> Challan >> Item HP', '', '3.10.3'), 
(NULL, 'Master >> Challan >> Item KW', '', '3.10.4'), 
(NULL, 'Master >> Challan >> Item Frequency', '', '3.10.5'), 
(NULL, 'Master >> Challan >> Item RPM', '', '3.10.6'), 
(NULL, 'Master >> Challan >> Item Volts', '', '3.10.7'), 
(NULL, 'Master >> Challan >> Item Gear Type', '', '3.10.8'), 
(NULL, 'Master >> Challan >> Item Model', '', '3.10.9'), 
(NULL, 'Master >> Challan >> Item Ratio', '', '3.10.10');

INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES 
(NULL, 'view', 'view', '209'),

(NULL, 'view', 'view', '210'),
(NULL, 'add', 'add', '210'), 
(NULL, 'edit', 'edit', '210'), 
(NULL, 'delete', 'delete', '210'),
(NULL, 'view', 'view', '211'),
(NULL, 'add', 'add', '211'), 
(NULL, 'edit', 'edit', '211'), 
(NULL, 'delete', 'delete', '211'),
(NULL, 'view', 'view', '212'),
(NULL, 'add', 'add', '212'), 
(NULL, 'edit', 'edit', '212'), 
(NULL, 'delete', 'delete', '212'),
(NULL, 'view', 'view', '213'),
(NULL, 'add', 'add', '213'), 
(NULL, 'edit', 'edit', '213'), 
(NULL, 'delete', 'delete', '213'),
(NULL, 'view', 'view', '214'),
(NULL, 'add', 'add', '214'), 
(NULL, 'edit', 'edit', '214'), 
(NULL, 'delete', 'delete', '214'),
(NULL, 'view', 'view', '215'),
(NULL, 'add', 'add', '215'), 
(NULL, 'edit', 'edit', '215'), 
(NULL, 'delete', 'delete', '215'),
(NULL, 'view', 'view', '216'),
(NULL, 'add', 'add', '216'), 
(NULL, 'edit', 'edit', '216'), 
(NULL, 'delete', 'delete', '216'),
(NULL, 'view', 'view', '217'),
(NULL, 'add', 'add', '217'), 
(NULL, 'edit', 'edit', '217'), 
(NULL, 'delete', 'delete', '217'),
(NULL, 'view', 'view', '218'),
(NULL, 'add', 'add', '218'), 
(NULL, 'edit', 'edit', '218'), 
(NULL, 'delete', 'delete', '218'),
(NULL, 'view', 'view', '219'),
(NULL, 'add', 'add', '219'), 
(NULL, 'edit', 'edit', '219'), 
(NULL, 'delete', 'delete', '219');

-- Jignesh : 02/01/2018 8:15 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Can Assigne Enquiry', '', '5.2.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '220');

-- Avinash : 2018_01_03
UPDATE `website_modules` SET `title` = 'Can Assigne Enquiry to Other' WHERE `website_modules`.`id` = 220;
ALTER TABLE `department` DROP `is_deleted`;
ALTER TABLE `department` ADD `delete_not_allow` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Delete Allow, 1 = Delete Not Allow' AFTER `department`;
UPDATE `department` SET `delete_not_allow` = '1' WHERE `department`.`department_id` = 12;

ALTER TABLE `invoices` ADD `packing_forwarding_by` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1 = Excluding, 2 = Including' AFTER `freight_igst`;
ALTER TABLE `invoices` ADD `sea_freight_by` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1 = Excluding, 2 = Including' AFTER `bank_detail`;

-- Chirag : 2018_01_04
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES (NULL, 'Master >> Sales >> Sea Freight Type', '', '3.4.20');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '221'), (NULL, 'add', 'add', '221'), (NULL, 'edit', 'edit', '221'), (NULL, 'delete', 'delete', '221');

-- Jignesh : 2018_01_04 03:24 PM
INSERT INTO `terms_and_conditions` (`id`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'testing_declaration', 'Testing Declaration text', NULL, NULL);

-- Jignesh : 2018_01_04 06:12 PM
ALTER TABLE `terms_and_conditions` ADD `label` VARCHAR(99) NULL DEFAULT NULL AFTER `id`;

-- Jignesh : 2018_01_04 06:51 PM
UPDATE `terms_and_conditions` SET `label` = 'Purchase Order >> Genral Terms' WHERE `terms_and_conditions`.`id` = 1;
UPDATE `terms_and_conditions` SET `label` = 'Purchase Invoice' WHERE `terms_and_conditions`.`id` = 2;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Sales Order >> Payment Terms' WHERE `terms_and_conditions`.`id` = 3;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Sales Order >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 5;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Proforma Invoice >> Payment Terms' WHERE `terms_and_conditions`.`id` = 6;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Proforma Invoice >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 7;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Sales Invoice >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 9;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Challan Declaration' WHERE `terms_and_conditions`.`id` = 10;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Sales Order >> Payment Terms' WHERE `terms_and_conditions`.`id` = 11;
UPDATE `terms_and_conditions` SET `label` = 'Contract >> Sales Order >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 12;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Proforma Invoice >> Payment Terms' WHERE `terms_and_conditions`.`id` = 13;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Proforma Invoice >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 14;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Sales Invoice >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 16;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Challan Declaration' WHERE `terms_and_conditions`.`id` = 17;
UPDATE `terms_and_conditions` SET `label` = 'Purchase Order >> Electrict Components Terms' WHERE `terms_and_conditions`.`id` = 18;
UPDATE `terms_and_conditions` SET `label` = 'Export Order >> Sales Order >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 19;
UPDATE `terms_and_conditions` SET `label` = 'Payment >> Declaration' WHERE `terms_and_conditions`.`id` = 20;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Sales Invoice >> Payment Terms' WHERE `terms_and_conditions`.`id` = 21;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Sales Invoice >> Declaration' WHERE `terms_and_conditions`.`id` = 22;
UPDATE `terms_and_conditions` SET `label` = 'Testing Report >> Declaration' WHERE `terms_and_conditions`.`id` = 23;

-- Jignesh : 2018_01_05 : 04:29
ALTER TABLE `invoices` CHANGE `sales_order_id` `sales_order_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `invoices` CHANGE `challan_id` `challan_id` INT(11) NULL DEFAULT NULL,
CHANGE `total_cmount` `total_cmount` DOUBLE NULL DEFAULT NULL,
CHANGE `quatation_id` `quatation_id` INT(11) NULL DEFAULT NULL,
CHANGE `contact_no` `contact_no` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
CHANGE `cust_po_no` `cust_po_no` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
CHANGE `email_id` `email_id` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
CHANGE `po_date` `po_date` DATE NULL DEFAULT NULL,
CHANGE `lr_no` `lr_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
CHANGE `vehicle_no` `vehicle_no` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
CHANGE `against_form` `against_form` INT(1) NULL DEFAULT NULL;

ALTER TABLE `invoice_logins` CHANGE `invoice_id` `invoice_id` INT(11) NULL DEFAULT NULL, CHANGE `created_by` `created_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by_id` `created_by_id` INT(11) NULL DEFAULT NULL, CHANGE `created_date_time` `created_date_time` DATETIME NULL DEFAULT NULL, CHANGE `last_modified_by` `last_modified_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `last_modified_by_id` `last_modified_by_id` INT(11) NULL DEFAULT NULL, CHANGE `modified_date_time` `modified_date_time` DATETIME NULL DEFAULT NULL, CHANGE `prepared_by` `prepared_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `prepared_by_id` `prepared_by_id` INT(11) NULL DEFAULT NULL, CHANGE `prepared_date_time` `prepared_date_time` DATETIME NULL DEFAULT NULL, CHANGE `approved_by` `approved_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `approved_by_id` `approved_by_id` INT(11) NULL DEFAULT NULL, CHANGE `approved_date_time` `approved_date_time` DATETIME NULL DEFAULT NULL, CHANGE `stage_id` `stage_id` INT(11) NULL DEFAULT NULL, CHANGE `stage` `stage` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `invoice_items` CHANGE `invoice_id` `invoice_id` INT(11) NULL DEFAULT NULL, CHANGE `item_code` `item_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uom` `uom` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `total_amount` `total_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_note` `item_note` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `quantity` `quantity` DOUBLE NULL DEFAULT NULL, CHANGE `disc_per` `disc_per` DOUBLE NULL DEFAULT NULL, CHANGE `rate` `rate` DOUBLE NULL DEFAULT NULL, CHANGE `disc_value` `disc_value` DOUBLE NULL DEFAULT NULL, CHANGE `amount` `amount` DOUBLE NULL DEFAULT NULL, CHANGE `net_amount` `net_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_status` `item_status` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cust_part_no` `cust_part_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;

-- Avinash : 2018_01_06 : 09:45 AM
ALTER TABLE `terms_and_conditions_sub` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- Jignesh : 2018_01_09 : 07:34 AM
ALTER TABLE `challan_items_gearbox_details` ADD `normal_amps_jk` VARCHAR(66) NULL DEFAULT NULL AFTER `gearbox_serial_no`, ADD `working_amps_jk` VARCHAR(66) NULL DEFAULT NULL AFTER `normal_amps_jk`, ADD `equipment_rpm_jk` VARCHAR(66) NULL DEFAULT NULL AFTER `working_amps_jk`, ADD `total_workinghours_jk` VARCHAR(66) NULL DEFAULT NULL AFTER `equipment_rpm_jk`, ADD `normal_amps_c` VARCHAR(66) NULL DEFAULT NULL AFTER `total_workinghours_jk`, ADD `working_amps_c` VARCHAR(66) NULL DEFAULT NULL AFTER `normal_amps_c`, ADD `equipment_rpm_c` VARCHAR(66) NULL DEFAULT NULL AFTER `working_amps_c`, ADD `total_workinghours_c` VARCHAR(66) NULL DEFAULT NULL AFTER `equipment_rpm_c`;
ALTER TABLE `testing_report` ADD `your_technical_operator_person_contect_no` VARCHAR(66) NULL DEFAULT NULL AFTER `your_technical_operator_person`, ADD `project_training_provide_by_contect_no` VARCHAR(66) NULL DEFAULT NULL AFTER `your_technical_operator_person_contect_no`;

-- Avinash : 2018_01_10 : 12:05 AM
UPDATE `party` SET `party_created_date`='1994-01-01' WHERE `party_created_date`='1970-01-01' OR `party_created_date`='0000-00-00' OR `party_created_date` IS NULL;
UPDATE `inquiry` SET `reference_date`='1994-01-01' WHERE `reference_date`='1970-01-01' OR `reference_date`='0000-00-00' OR `reference_date` IS NULL;
UPDATE `inquiry` SET `due_date`='1994-01-01' WHERE `due_date`='1970-01-01' OR `due_date`='0000-00-00' OR `due_date` IS NULL;
UPDATE `inquiry` SET `inquiry_date`='1994-01-01' WHERE `inquiry_date`='1970-01-01' OR `inquiry_date`='0000-00-00' OR `inquiry_date` IS NULL;
UPDATE `quotations` SET `quotation_date`='1994-01-01' WHERE `quotation_date`='1970-01-01' OR `quotation_date`='0000-00-00' OR `quotation_date` IS NULL;
UPDATE `followup_history` SET `followup_date`='1994-01-01 00:00:00' WHERE `followup_date`='1970-01-01 00:00:00' OR `followup_date`='0000-00-00 00:00:00' OR `followup_date` IS NULL;
UPDATE `followup_history` SET `reminder_date`= null WHERE `reminder_date`='1970-01-01 00:00:00' OR `reminder_date`='0000-00-00 00:00:00' OR `reminder_date` IS NULL;

-- Avinash : 2018_01_10 : 01:00 AM
ALTER TABLE `grade` CHANGE `grade_id` `grade_id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `grade` `grade` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
UPDATE `grade` SET `updated_at`='1994-01-01 00:00:00' WHERE `updated_at`='0000-00-00 00:00:00';
UPDATE `invoice_logins` SET `prepared_date_time`='1994-01-01 00:00:00' WHERE `prepared_date_time`='0000-00-00 00:00:00';
UPDATE `invoice_logins` SET `approved_date_time`='1994-01-01 00:00:00' WHERE `approved_date_time`='0000-00-00 00:00:00';
UPDATE `invoices` SET `po_date`='1994-01-01' WHERE `po_date`='0000-00-00';
ALTER TABLE `proforma_invoice_logins` CHANGE `proforma_invoice_id` `proforma_invoice_id` INT(11) NULL DEFAULT NULL, CHANGE `created_by` `created_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by_id` `created_by_id` INT(11) NULL DEFAULT NULL, CHANGE `created_date_time` `created_date_time` DATETIME NULL DEFAULT NULL, CHANGE `last_modified_by` `last_modified_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `last_modified_by_id` `last_modified_by_id` INT(11) NULL DEFAULT NULL, CHANGE `modified_date_time` `modified_date_time` DATETIME NULL DEFAULT NULL, CHANGE `prepared_by` `prepared_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `prepared_by_id` `prepared_by_id` INT(11) NULL DEFAULT NULL, CHANGE `prepared_date_time` `prepared_date_time` DATETIME NULL DEFAULT NULL, CHANGE `approved_by` `approved_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `approved_by_id` `approved_by_id` INT(11) NULL DEFAULT NULL, CHANGE `approved_date_time` `approved_date_time` DATETIME NULL DEFAULT NULL, CHANGE `stage_id` `stage_id` INT(11) NULL DEFAULT NULL, CHANGE `stage` `stage` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
UPDATE `proforma_invoice_logins` SET `prepared_date_time`='1994-01-01 00:00:00' WHERE `prepared_date_time`='0000-00-00 00:00:00';
UPDATE `proforma_invoice_logins` SET `approved_date_time`='1994-01-01 00:00:00' WHERE `approved_date_time`='0000-00-00 00:00:00';
UPDATE `staff` SET `created_at`='1994-01-01 00:00:00' WHERE `created_at`='0000-00-00 00:00:00';
ALTER TABLE `staff` CHANGE `marriage_date` `marriage_date` DATE NULL DEFAULT NULL;
UPDATE `staff` SET `marriage_date`= null WHERE `marriage_date`='0000-00-00';
ALTER TABLE `staff` CHANGE `birth_date` `birth_date` DATE NULL DEFAULT NULL, CHANGE `date_of_joining` `date_of_joining` DATE NULL DEFAULT NULL, CHANGE `date_of_leaving` `date_of_leaving` DATE NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;
UPDATE `staff` SET `date_of_joining`= null WHERE `date_of_joining`='0000-00-00';
UPDATE `staff` SET `date_of_leaving`= null WHERE `date_of_leaving`='0000-00-00';
UPDATE `staff` SET `birth_date`= null WHERE `birth_date`='0000-00-00';
UPDATE `followup_history` SET `reminder_date`= null WHERE `reminder_date`='1970-01-01 05:30:00';

-- Chirag : 2018_01_10 : 03:00 PM
UPDATE `item_extra_accessories` SET `item_extra_accessories_label` = 'Blank' WHERE `item_extra_accessories`.`item_extra_accessories_id` = 3;

-- Avinash : 2018_01_12 : 02:15 PM
ALTER TABLE `testing_report`
  DROP `quotation_no`,
  DROP `purchase_order_no`,
  DROP `purchase_order_date`,
  DROP `sales_order_no`,
  DROP `sales_order_date`;

-- Avinash : 2018_01_12 : 06:50 PM
ALTER TABLE `testing_report` ADD `is_installation_created` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Instalation not created, 1 = Instalation Created' AFTER `test_detail`;

-- Avinash : 2018_01_13 : 09:00 PM
ALTER TABLE `sea_freight_type` CHANGE `created_by` `created_by` INT(11) NULL DEFAULT NULL, CHANGE `updated_by` `updated_by` INT(11) NULL DEFAULT NULL;
ALTER TABLE `company_banks` CHANGE `created_by` `created_by` INT(11) NULL DEFAULT NULL, CHANGE `updated_by` `updated_by` INT(11) NULL DEFAULT NULL;
ALTER TABLE `payment_by` CHANGE `name` `name` VARCHAR(55) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `payment_by` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NULL DEFAULT NULL, CHANGE `updated_by` `updated_by` INT(11) NULL DEFAULT NULL;

--
-- Table structure for table `electric_power`
--
CREATE TABLE `electric_power` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `electric_power`
--
INSERT INTO `electric_power` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Available', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(2, 'Applied', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(3, 'Not Applied', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1);

--
-- Indexes for table `electric_power`
--
ALTER TABLE `electric_power`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `electric_power`
--
ALTER TABLE `electric_power`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Table structure for table `available_or_not`
--
CREATE TABLE `available_or_not` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `available_or_not`
--
INSERT INTO `available_or_not` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Available', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(2, 'Not Available', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1);

--
-- Indexes for table `available_or_not`
--
ALTER TABLE `available_or_not`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `available_or_not`
--
ALTER TABLE `available_or_not`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
  
--
-- Table structure for table `water_tank_or_chiller_system`
--
CREATE TABLE `water_tank_or_chiller_system` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `water_tank_or_chiller_system`
--
INSERT INTO `water_tank_or_chiller_system` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Available', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(2, 'Under Work', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(3, 'Not Ready', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1);

--
-- Indexes for table `water_tank_or_chiller_system`
--
ALTER TABLE `water_tank_or_chiller_system`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `water_tank_or_chiller_system`
--
ALTER TABLE `water_tank_or_chiller_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
  
--
-- Table structure for table `water_plumbing_or_chiller_conn`
--
CREATE TABLE `water_plumbing_or_chiller_conn` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `water_plumbing_or_chiller_conn`
--
INSERT INTO `water_plumbing_or_chiller_conn` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Completed', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(2, 'Under Work', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1);

--
-- Indexes for table `water_plumbing_or_chiller_conn`
--
ALTER TABLE `water_plumbing_or_chiller_conn`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `water_plumbing_or_chiller_conn`
--
ALTER TABLE `water_plumbing_or_chiller_conn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Table structure for table `lubrication_gear_oil`
--
CREATE TABLE `lubrication_gear_oil` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lubrication_gear_oil`
--
INSERT INTO `lubrication_gear_oil` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Available', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(2, 'Not Available', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(3, '320', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(4, '220', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(5, '150', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1);

--
-- Indexes for table `lubrication_gear_oil`
--
ALTER TABLE `lubrication_gear_oil`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `lubrication_gear_oil`
--
ALTER TABLE `lubrication_gear_oil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Table structure for table `customer_feedback`
--
CREATE TABLE `customer_feedback` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_feedback`
--
INSERT INTO `customer_feedback` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Excellent', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(2, 'Very Good', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(3, 'Good', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(4, 'Average', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1),
(5, 'Poor', '2018-01-13 00:00:00', '2018-01-13 00:00:00', 1, 1);

--
-- Indexes for table `customer_feedback`
--
ALTER TABLE `customer_feedback`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `customer_feedback`
--
ALTER TABLE `customer_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

-- Avinash : 2018_01_15 : 11:00 AM
ALTER TABLE `testing_report` DROP `equipment_full_load_operating_by`;
ALTER TABLE `testing_report`
  DROP `your_technical_operator_person`,
  DROP `your_technical_operator_person_contect_no`;
ALTER TABLE `testing_report`
  DROP `installation_commissioning_by`,
  DROP `installation_commissioning_by_text`;
ALTER TABLE `testing_report`
  DROP `project_training_provide_by`,
  DROP `project_training_provide_by_contect_no`;
ALTER TABLE `testing_report`
  DROP `your_site_incharge_person_name`,
  DROP `your_site_incharge_person_contect_no`;
  
-- Avinash : 2018_01_15 : 04:30 PM
ALTER TABLE `testing_report`
  DROP `our_site_incharge_person_text`,
  DROP `our_site_incharge_person_contact`;

-- Avinash : 2018_01_15 : 06:50 PM
--
-- Table structure for table `problem_resolutions`
--
CREATE TABLE `problem_resolutions` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `problem_resolutions`
--
INSERT INTO `problem_resolutions` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Problem', '2018-01-15 00:00:00', '2018-01-15 00:00:00', 1, 1),
(2, 'Resolution', '2018-01-15 00:00:00', '2018-01-15 00:00:00', 1, 1);

--
-- Indexes for table `problem_resolutions`
--
ALTER TABLE `problem_resolutions`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `problem_resolutions`
--
ALTER TABLE `problem_resolutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
  
-- Avinash : 2018_01_16 : 08:25 PM
-- 
-- Table structure for table `installation`
-- 
CREATE TABLE `installation` (
	`installation_id` int(11) NOT NULL,
	`testing_report_id` int(11) DEFAULT NULL,
	`challan_id` int(11) DEFAULT NULL,
	`party_id` int(11) DEFAULT NULL,
	`installation_no` varchar(11) DEFAULT NULL,
	`installation_date` date DEFAULT NULL,
	`installation_commissioning_by` int(11) DEFAULT NULL,
	`installation_commissioning_by_name` varchar(50) DEFAULT NULL,
	`installation_commissioning_by_contact` varchar(50) DEFAULT NULL,
	`incharge_person` int(11) DEFAULT NULL,
	`incharge_person_name` varchar(50) DEFAULT NULL,
	`incharge_person_contact` varchar(50) DEFAULT NULL,
	`project_training_provide_by` int(11) DEFAULT NULL,
	`project_training_provide_by_name` varchar(50) DEFAULT NULL,
	`project_training_provide_by_contact` varchar(50) DEFAULT NULL,
	`customer_technical_operator_person` varchar(50) DEFAULT NULL,
	`customer_technical_operator_person_contact` varchar(50) DEFAULT NULL,
	`customer_site_quality_checked_by_name` varchar(50) DEFAULT NULL,
	`customer_site_quality_checked_by_contact` varchar(50) DEFAULT NULL,
	`customer_site_equipment_inspection_by_name` varchar(50) DEFAULT NULL,
	`customer_site_equipment_inspection_by_contact` varchar(50) DEFAULT NULL,
	`kind_attn_id` int(11) DEFAULT NULL,
	`land_building_factory_area` varchar(50) DEFAULT NULL,
	`plant_room_size` varchar(50) DEFAULT NULL,
	`prepared_foundation` varchar(50) DEFAULT NULL,
	`foundation_marking_by` varchar(50) DEFAULT NULL,
	`no_of_foundation` varchar(50) DEFAULT NULL,
	`unloading_date` date DEFAULT NULL,
	`filling_of_foundation_boxes` date DEFAULT NULL, 
	`installation_start` date DEFAULT NULL,
	`installation_finished` date DEFAULT NULL,
	`electric_power` int(11) DEFAULT NULL,
	`electric_connection` varchar(50) DEFAULT NULL,	
	`elec_rm2_panel_board_cable` int(11) DEFAULT NULL,
	`connection_cable_1` int(11) DEFAULT NULL,
	`connection_cable_2` varchar(50) DEFAULT NULL,
	`water_tank_chiller_system_label` int(11) DEFAULT NULL,
	`water_tank_chiller_system_1` int(11) DEFAULT NULL,
	`water_tank_chiller_system_2` varchar(50) DEFAULT NULL,
	`water_plumbing_chiller_conn_label` int(11) DEFAULT NULL,
	`water_plumbing_chiller_conn_1` int(11) DEFAULT NULL,
	`water_plumbing_chiller_conn_2` varchar(50) DEFAULT NULL,
	`lubrication_gear_oil_1` int(11) DEFAULT NULL,
	`lubrication_gear_oil_2` varchar(50) DEFAULT NULL,
	`gear_box_oil_1` int(11) DEFAULT NULL,
	`gear_box_oil_2` varchar(50) DEFAULT NULL,
	`raw_material_1` int(11) DEFAULT NULL,
	`raw_material_2` varchar(50) DEFAULT NULL,
	`run_start_date` date DEFAULT NULL,
	`run_hrs` varchar(50) DEFAULT NULL,
	`production_start` date DEFAULT NULL,
	`name_of_raw_material` varchar(50) DEFAULT NULL,
	`moisture_of_raw_material` varchar(50) DEFAULT NULL,
	`after_drying_moisture_content`  varchar(50) DEFAULT NULL,
	`approx_production_per_hr`  varchar(50) DEFAULT NULL,
	`product_quality` int(11) DEFAULT NULL,
	`electric_consumption_unit_hr`  varchar(50) DEFAULT NULL,
	`electricity_rate_per_unit`  varchar(50) DEFAULT NULL,
	`unskilled_labour`  varchar(22) DEFAULT NULL,
	`skilled_labour`  varchar(22) DEFAULT NULL,
	`num_of_equipments` varchar(11) DEFAULT NULL,
	`technician_of_jk_installed` int(11) DEFAULT NULL,
	`technician_from_jk_accompanied` int(11) DEFAULT NULL,
	`technician_from_jk_helped` int(11) DEFAULT NULL,
	`performance_of_our_equipment` int(11) DEFAULT NULL,
	`overall_work_desc` TEXT NULL DEFAULT NULL,
	`quantity_of_production_desc` TEXT NULL DEFAULT NULL,
	`satisfaction_level_desc` TEXT NULL DEFAULT NULL,
	`query_suggestion_desc` TEXT NULL DEFAULT NULL,
	`feedback_for_quality` TEXT NULL DEFAULT NULL,
	`feedback_for_team` TEXT NULL DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `installation`
--
ALTER TABLE `installation`
 ADD PRIMARY KEY (`installation_id`);

--
-- AUTO_INCREMENT for table `installation`
--
ALTER TABLE `installation`
 MODIFY `installation_id` int(11) NOT NULL AUTO_INCREMENT;
-- --------------------------------------------------------

--
-- Table structure for table `installation_daily_working_report`
--

CREATE TABLE `installation_daily_working_report` (
	`id` int(11) NOT NULL,
	`installation_id` int(11) DEFAULT NULL,
	`work_date` date DEFAULT NULL,
	`oper_person` int(11) DEFAULT NULL,
	`dwr_incharge_person` int(11) DEFAULT NULL,
	`use_raw_mater` int(11) DEFAULT NULL,
	`moisture` int(11) DEFAULT NULL,
	`produ_hour` varchar(255) DEFAULT NULL,
	`after_mois` varchar(66) DEFAULT NULL,
	`working_hour` varchar(66) DEFAULT NULL,
	`remark` varchar(66) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `installation_daily_working_report`
--
ALTER TABLE `installation_daily_working_report`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `installation_daily_working_report`
--
ALTER TABLE `installation_daily_working_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- --------------------------------------------------------

--
-- Table structure for table `installation_problem_resolutions`
--

CREATE TABLE `installation_problem_resolutions` (
	`id` int(11) NOT NULL,
	`installation_id` int(11) DEFAULT NULL,
	`problem_resolution_option` int(11) DEFAULT NULL,
	`problem_resolution_text` TEXT NULL DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `installation_problem_resolutions`
--
ALTER TABLE `installation_problem_resolutions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `installation_problem_resolutions`
--
ALTER TABLE `installation_problem_resolutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag : 2018_01_16 : 11:04 AM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('222', 'Master >> General Master >> Company Banks', '', '3.7.11');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '222'), (NULL, 'add', 'add', '222'), (NULL, 'edit', 'edit', '222'), (NULL, 'delete', 'delete', '222');
UPDATE `website_modules` SET `title` = 'Master >> Hr >> Department', `main_module` = '3.5.3' WHERE `website_modules`.`id` = 71;

-- Avinash : 2018_01_16 : 03:45 AM
ALTER TABLE `installation` CHANGE `incharge_person` `jk_technical_person` INT(11) NULL DEFAULT NULL, CHANGE `incharge_person_name` `jk_technical_person_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `incharge_person_contact` `jk_technical_person_contact` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `installation` ADD `customer_site_incharge_person` VARCHAR(50) NULL DEFAULT NULL AFTER `project_training_provide_by_contact`, ADD `customer_site_incharge_person_contact` VARCHAR(50) NULL DEFAULT NULL AFTER `customer_site_incharge_person`;

-- Avinash : 2018_01_16 : 06:55 AM
ALTER TABLE `installation_daily_working_report` CHANGE `oper_person` `oper_person` VARCHAR(11) NULL DEFAULT NULL, CHANGE `dwr_incharge_person` `dwr_incharge_person` VARCHAR(11) NULL DEFAULT NULL, CHANGE `use_raw_mater` `use_raw_mater` VARCHAR(11) NULL DEFAULT NULL, CHANGE `moisture` `moisture` VARCHAR(11) NULL DEFAULT NULL;

-- Avinash : 2018_01_17 : 10:50 AM
ALTER TABLE `installation` CHANGE `water_tank_chiller_system_label` `water_tank_chiller_system_label` INT(11) NULL DEFAULT NULL COMMENT '1 = Water Tank, 2 = Chiller System', CHANGE `water_plumbing_chiller_conn_label` `water_plumbing_chiller_conn_label` INT(11) NULL DEFAULT NULL COMMENT '1 = Water Plumbing, 2 = Chiller Conn';

-- Avinash : 2018_01_17 : 11:30 AM
ALTER TABLE `installation_daily_working_report` CHANGE `oper_person` `oper_person` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `dwr_incharge_person` `dwr_incharge_person` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `use_raw_mater` `use_raw_mater` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `moisture` `moisture` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `produ_hour` `produ_hour` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `after_mois` `after_mois` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `working_hour` `working_hour` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `remark` `remark` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Avinash : 2018_01_17 : 04:25 PM
ALTER TABLE `testing_report` ADD `jk_et_equipment_rpm` VARCHAR(100) NULL DEFAULT NULL AFTER `jk_temp_weather`, ADD `jk_et_testing` VARCHAR(100) NULL DEFAULT NULL AFTER `jk_et_equipment_rpm`, ADD `jk_et_stop_run_time` VARCHAR(100) NULL DEFAULT NULL AFTER `jk_et_testing`, ADD `jk_cpb_testing` VARCHAR(100) NULL DEFAULT NULL AFTER `jk_et_stop_run_time`, ADD `jk_cpb_working_hours` VARCHAR(100) NULL DEFAULT NULL AFTER `jk_cpb_testing`;

-- Avinash : 2018_01_17 : 05:00 PM
ALTER TABLE `testing_report` ADD `c_et_equipment_rpm` VARCHAR(100) NULL DEFAULT NULL AFTER `c_temp_weather`, ADD `c_et_testing` VARCHAR(100) NULL DEFAULT NULL AFTER `c_et_equipment_rpm`, ADD `c_et_stop_run_time` VARCHAR(100) NULL DEFAULT NULL AFTER `c_et_testing`, ADD `c_cpb_testing` VARCHAR(100) NULL DEFAULT NULL AFTER `c_et_stop_run_time`, ADD `c_cpb_working_hours` VARCHAR(100) NULL DEFAULT NULL AFTER `c_cpb_testing`;
ALTER TABLE `installation` ADD `tractor_loader` TINYINT(1) NULL DEFAULT NULL COMMENT '1 = Yes, 2 = No' AFTER `customer_site_equipment_inspection_by_contact`;

-- Avinash : 2018_01_18 : 10:30 PM
INSERT INTO `terms_and_conditions` (`id`, `label`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'Installation >> Declaration', 'installation_declaration', 'Installation >> Declaration sdzf dfs sd', '1', '2018-01-18 00:00:00');

-- Chirag : 2018_01_18 : 3:20 PM
UPDATE `website_modules` SET `main_module` = '7.2' WHERE `website_modules`.`id` = 114;
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('223', 'Installation', '', '7.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '223'), (NULL, 'add', 'add', '223'), (NULL, 'edit', 'edit', '223'), (NULL, 'delete', 'delete', '223');

CREATE TABLE `sms_topic` (
`id` INT(11) NOT NULL AUTO_INCREMENT ,
 `topic_name` VARCHAR(255) NOT NULL ,
 `created_at` DATETIME NULL DEFAULT NULL ,
 `updated_at` DATETIME NULL DEFAULT NULL ,
 `created_by` INT(11) NULL DEFAULT NULL ,
 `updated_by` INT(11) NULL DEFAULT NULL ,
 PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('224', 'Master >> General Master >> Sms Topics', '', '3.7.12');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '224'), (NULL, 'add', 'add', '224'), (NULL, 'edit', 'edit', '224'), (NULL, 'delete', 'delete', '224');

-- Chirag : 2018_01_20 : 11:30 AM
ALTER TABLE `sms_topic` ADD `message` TEXT NULL DEFAULT NULL AFTER `topic_name`;

-- Chirag : 2018_01_25 : 06:00 PM
ALTER TABLE `installation` ADD `pdf_url` VARCHAR(222) NULL DEFAULT NULL AFTER `feedback_for_team`;

-- Avinash : 2018_02_01 : 01:10 PM
UPDATE `quotation_items` 
JOIN `quotations` ON `quotations`.`id` = `quotation_items`.`quotation_id`
JOIN `party` ON `party`.`party_id` = `quotations`.`party_id`
SET `quotation_items`.`currency_id` = 4
WHERE `party`.`party_type_1` = 5 AND `quotation_items`.`currency_id` IS NULL;

UPDATE `quotation_items` 
JOIN `quotations` ON `quotations`.`id` = `quotation_items`.`quotation_id`
JOIN `party` ON `party`.`party_id` = `quotations`.`party_id`
SET `quotation_items`.`currency_id` = 3
WHERE `party`.`party_type_1` = 6 AND `quotation_items`.`currency_id` IS NULL;

--
-- Avinash : 2018_02_02 : 10:45 PM
-- Table structure for table `staff`
--
DROP TABLE staff;

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `interview_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) NOT NULL,
  `mother_mobile` varchar(100) DEFAULT NULL,
  `incentive_fix` varchar(100) DEFAULT NULL,
  `father_mobile` varchar(100) DEFAULT NULL,
  `husband_name` varchar(100) DEFAULT NULL,
  `husband_mobile` varchar(100) DEFAULT NULL,
  `wife_name` varchar(100) DEFAULT NULL,
  `wife_mobile` varchar(100) DEFAULT NULL,
  `reference_person` varchar(100) DEFAULT NULL,
  `ref_per_mobile_no` varchar(100) DEFAULT NULL,
  `company_contact` varchar(100) DEFAULT NULL,
  `person_mobile_no` varchar(100) DEFAULT NULL,
  `driving_licence_no` varchar(100) DEFAULT NULL,
  `whatsapp_no` varchar(100) DEFAULT NULL,
  `personal_email` varchar(100) DEFAULT NULL,
  `passport_no` varchar(100) DEFAULT NULL,
  `passport_issue_by` varchar(100) DEFAULT NULL,
  `passport_issue_date` varchar(100) DEFAULT NULL,
  `passport_expired_date` varchar(100) DEFAULT NULL,
  `aadhaar_no` varchar(100) DEFAULT NULL,
  `incentive` varchar(100) DEFAULT NULL,
  `bank_branch` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `uan_no` varchar(100) DEFAULT NULL,
  `married_status` varchar(15) NOT NULL,
  `husband_wife_name` varchar(255) NOT NULL,
  `marriage_date` date DEFAULT NULL,
  `contact_no` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `signature` varchar(222) DEFAULT NULL,
  `address` text NOT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `date_of_joining` date DEFAULT NULL,
  `date_of_leaving` date DEFAULT NULL,
  `salary` double NOT NULL,
  `allow_pf` varchar(10) DEFAULT NULL,
  `basic_pay` double DEFAULT NULL,
  `house_rent` double DEFAULT NULL,
  `traveling_allowance` double DEFAULT NULL,
  `education_allowance` double DEFAULT NULL,
  `pf_amount` double DEFAULT NULL,
  `bonus_amount` double DEFAULT NULL,
  `other_allotment` double DEFAULT NULL,
  `medical_reimbursement` double DEFAULT NULL,
  `professional_tax` double DEFAULT NULL,
  `monthly_gross` double DEFAULT NULL,
  `cost_to_company` double DEFAULT NULL,
  `pf_employee` double DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_no` varchar(255) DEFAULT NULL,
  `esic_no` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `uan_id` varchar(255) DEFAULT NULL,
  `pf_no` varchar(255) DEFAULT NULL,
  `emp_no` varchar(255) DEFAULT NULL,
  `emp_id` varchar(255) NOT NULL,
  `esic_id` varchar(255) DEFAULT NULL,
  `visitor_last_message_id` int(11) NOT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `mailbox_email` varchar(255) DEFAULT NULL,
  `mailbox_password` varchar(255) DEFAULT NULL,
  `pdf_url` text,
  `active` tinyint(1) DEFAULT '1',
  `is_login` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Triggers `staff`
--
DROP TRIGGER IF EXISTS `staff_add_log`;
DELIMITER //
CREATE TRIGGER `staff_add_log` AFTER INSERT ON `staff`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_log SET
`staff_id` =NEW.staff_id,
`interview_id` =NEW.interview_id,
`name` =NEW.name,
`birth_date` =NEW.birth_date,
`gender` =NEW.gender,
`blood_group` =NEW.blood_group,
`father_name` =NEW.father_name,
`mother_name` =NEW.mother_name,
`married_status` =NEW.married_status,
`husband_wife_name` =NEW.husband_wife_name,
`marriage_date` =NEW.marriage_date,
`contact_no` =NEW.contact_no,
`email` =NEW.email,
`pass`  =NEW.pass,
`image` =NEW.image,
`address` =NEW.address,
`permanent_address` =NEW.permanent_address,
`qualification` =NEW.qualification,
`designation_id` =NEW.designation_id,
`department_id` =NEW.department_id,
`grade_id` =NEW.grade_id,
`date_of_joining` =NEW.date_of_joining,
`date_of_leaving` =NEW.date_of_leaving,
`salary` =NEW.salary,
`allow_pf` =NEW.allow_pf,
`basic_pay` =NEW.basic_pay,
`house_rent` =NEW.house_rent,
`traveling_allowance` =NEW.traveling_allowance,
`education_allowance` =NEW.education_allowance,
`pf_amount` =NEW.pf_amount,
`bonus_amount`  =NEW.bonus_amount,
`other_allotment`  =NEW.other_allotment,
`medical_reimbursement`  =NEW.medical_reimbursement,
`professional_tax`  =NEW.professional_tax,
`monthly_gross`  =NEW.monthly_gross,
`cost_to_company`  =NEW.cost_to_company,
`pf_employee`  =NEW.pf_employee,
`bank_name`  =NEW.bank_name,
`bank_acc_no`  =NEW.bank_acc_no,
`esic_no`  =NEW.esic_no,
`pan_no`  =NEW.pan_no,
`uan_id`  =NEW.uan_id,
`pf_no`  =NEW.pf_no,
`emp_no`  =NEW.emp_no,
`emp_id`  =NEW.emp_id,
`esic_id`  =NEW.esic_id,
`visitor_last_message_id`  =NEW.visitor_last_message_id,
`user_type`  =NEW.user_type,
`mailbox_email`  =NEW.mailbox_email,
`mailbox_password`  =NEW.mailbox_password,
`pdf_url` =NEW.pdf_url,
`active` =NEW.active,
`is_login`  =NEW.is_login,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'add'
//
DELIMITER ;
DROP TRIGGER IF EXISTS `staff_edit_log`;
DELIMITER //
CREATE TRIGGER `staff_edit_log` AFTER UPDATE ON `staff`
 FOR EACH ROW INSERT INTO  jaykhodiyar_logs.staff_log SET
`staff_id` =NEW.staff_id,
`interview_id` =NEW.interview_id,
`name` =NEW.name,
`birth_date` =NEW.birth_date,
`gender` =NEW.gender,
`blood_group` =NEW.blood_group,
`father_name` =NEW.father_name,
`mother_name` =NEW.mother_name,
`married_status` =NEW.married_status,
`husband_wife_name` =NEW.husband_wife_name,
`marriage_date` =NEW.marriage_date,
`contact_no` =NEW.contact_no,
`email` =NEW.email,
`pass`  =NEW.pass,
`image` =NEW.image,
`address` =NEW.address,
`permanent_address` =NEW.permanent_address,
`qualification` =NEW.qualification,
`designation_id` =NEW.designation_id,
`department_id` =NEW.department_id,
`grade_id` =NEW.grade_id,
`date_of_joining` =NEW.date_of_joining,
`date_of_leaving` =NEW.date_of_leaving,
`salary` =NEW.salary,
`allow_pf` =NEW.allow_pf,
`basic_pay` =NEW.basic_pay,
`house_rent` =NEW.house_rent,
`traveling_allowance` =NEW.traveling_allowance,
`education_allowance` =NEW.education_allowance,
`pf_amount` =NEW.pf_amount,
`bonus_amount`  =NEW.bonus_amount,
`other_allotment`  =NEW.other_allotment,
`medical_reimbursement`  =NEW.medical_reimbursement,
`professional_tax`  =NEW.professional_tax,
`monthly_gross`  =NEW.monthly_gross,
`cost_to_company`  =NEW.cost_to_company,
`pf_employee`  =NEW.pf_employee,
`bank_name`  =NEW.bank_name,
`bank_acc_no`  =NEW.bank_acc_no,
`esic_no`  =NEW.esic_no,
`pan_no`  =NEW.pan_no,
`uan_id`  =NEW.uan_id,
`pf_no`  =NEW.pf_no,
`emp_no`  =NEW.emp_no,
`emp_id`  =NEW.emp_id,
`esic_id`  =NEW.esic_id,
`visitor_last_message_id`  =NEW.visitor_last_message_id,
`user_type`  =NEW.user_type,
`mailbox_email`  =NEW.mailbox_email,
`mailbox_password`  =NEW.mailbox_password,
`pdf_url` =NEW.pdf_url,
`active` =NEW.active,
`is_login`  =NEW.is_login,
`created_at` =NEW.created_at,
`updated_at` =NEW.updated_at,
`operation_type` = 'edit'
//
DELIMITER ;

--
-- Avinash : 2018_02_02 : 04:15 PM
-- Table structure for table `service_type`
--
CREATE TABLE `service_type` (
	`id` int(11) NOT NULL,
	`label` varchar(50) DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`updated_at` datetime DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_type`
--
INSERT INTO `service_type` (`id`, `label`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Free', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 1, 1),
(2, 'Paid', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 1, 1);

--
-- Indexes for table `service_type`
--
ALTER TABLE `service_type`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `service_type`
--
ALTER TABLE `service_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Table structure for table `complain`
--
CREATE TABLE `complain` (
  `complain_id` int(11) NOT NULL,
  `complain_no` int(11) DEFAULT NULL,
  `complain_date` date DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `complaint_by` varchar(100) DEFAULT NULL,
  `received_by` int(11) DEFAULT NULL,
  `received_by_details` varchar(200) DEFAULT NULL,
  `complain_call_received_by` int(11) DEFAULT NULL,
  `complain_call_received_by_contact` varchar(100) DEFAULT NULL,
  `technical_support_by` int(11) DEFAULT NULL,
  `technical_support_by_contact` varchar(100) DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `complain`
--
ALTER TABLE `complain`
  ADD PRIMARY KEY (`complain_id`);

--
-- AUTO_INCREMENT for table `complain`
--
ALTER TABLE `complain`
  MODIFY `complain_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `complain_type_details`
--
CREATE TABLE `complain_type_details` (
  `complain_type_id` int(11) NOT NULL,
  `complain_id` int(11) DEFAULT NULL,
  `complain_type_date` int(11) DEFAULT NULL,
  `type_of_complain` int(11) DEFAULT NULL,
  `complain_box` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `complain_type_details`
--
ALTER TABLE `complain_type_details`
  ADD PRIMARY KEY (`complain_type_id`);

--
-- AUTO_INCREMENT for table `complain_type_details`
--
ALTER TABLE `complain_type_details`
  MODIFY `complain_type_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `complain_type_details` CHANGE `complain_type_date` `complain_type_date` DATE NULL DEFAULT NULL;

-- Chirag : 03_02_2018 10:05 AM

--
-- Table structure for table `letter_for`
--

CREATE TABLE `letter_for` (
  `id` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `letter_for`
--

INSERT INTO `letter_for` (`id`, `label`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Staff', '2018-02-02 00:00:00', 1, '2018-02-02 00:00:00', 1),
(2, 'Interview Candidate', '2018-02-02 00:00:00', 1, '2018-02-02 00:00:00', 1);

--
-- Indexes for table `letter_for`
--
ALTER TABLE `letter_for`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `letter_for`
--
ALTER TABLE `letter_for`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
  
  ALTER TABLE `letters`
  ADD `letter_for` INT(11) NULL DEFAULT NULL AFTER `letter_id`,
  ADD `letter_topic` VARCHAR(255) NULL DEFAULT NULL AFTER `letter_for`,
  ADD `created_at` DATETIME NULL DEFAULT NULL AFTER `letter_template`,
  ADD `created_by` INT(11) NULL DEFAULT NULL AFTER `created_at`,
  ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `updated_at`;
  
  ALTER TABLE `letters`
  CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
  
  ALTER TABLE `letters`
  CHANGE `letter_code` `letter_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  CHANGE `letter_template` `letter_template` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
  
--
-- Avinash : 2018_02_03 : 12:30 PM
-- Table structure for table `complain_items`
--
CREATE TABLE `complain_items` (
  `id` int(11) NOT NULL,
  `complain_id` int(11) DEFAULT NULL,
  `installation_id` int(11) DEFAULT NULL,
  `testing_report_id` int(11) DEFAULT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `complain_items`
--
ALTER TABLE `complain_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `complain_items`
--
ALTER TABLE `complain_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `complain_type_details` CHANGE `type_of_complain` `type_of_complain` TEXT NULL DEFAULT NULL, CHANGE `complain_box` `complain_box` TEXT NULL DEFAULT NULL;

-- Avinash : 2018_02_03 : 09:45 PM
ALTER TABLE `complain`
  DROP `complain_call_received_by`,
  DROP `complain_call_received_by_contact`;

ALTER TABLE `complain` ADD `service_charge` INT(11) NULL DEFAULT NULL AFTER `service_type`;
ALTER TABLE `complain` CHANGE `complaint_by` `complain_by` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Avinash : 2018_02_05 : 04:30 PM
ALTER TABLE `complain` ADD `pdf_url` VARCHAR(255) NULL DEFAULT NULL AFTER `service_charge`;

-- Chirag : 06_02_2018 02:27 PM
--
-- Table structure for table `print_letter`
--

CREATE TABLE `print_letter` (
  `id` int(11) NOT NULL,
  `letter_for_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `letter_topic_id` int(11) DEFAULT NULL,
  `letter_template` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `print_letter`
--
ALTER TABLE `print_letter`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `print_letter`
--
ALTER TABLE `print_letter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Avinash : 2018_02_06 : 07:00 PM
-- Table structure for table `sent_sms`
--

CREATE TABLE `sent_sms` (
  `id` int(11) NOT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `send_to` text,
  `sms_topic_id` int(11) DEFAULT NULL,
  `message` text,
  `delivery_report_id` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `sent_sms`
--
ALTER TABLE `sent_sms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `sent_sms`
--
ALTER TABLE `sent_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Jignesh : 2018_02_07 : 05:00 PM
--
ALTER TABLE `print_letter` ADD `pdf_url` VARCHAR(255) NULL DEFAULT NULL AFTER `letter_template`;

--
-- Avinash : 2018_02_08 : 10:30 PM
-- Table structure for table `resolve_complain`
--
CREATE TABLE `resolve_complain` (
  `resolve_complain_id` int(11) NOT NULL,
  `resolve_complain_no` int(11) DEFAULT NULL,
  `resolve_complain_date` date DEFAULT NULL,
  `complain_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `service_charge` int(11) DEFAULT NULL,
  `num_of_equipments` int(11) DEFAULT NULL,
  `rank_of_providing_service` int(11) DEFAULT NULL,
  `technician_of_jk_accompanied` int(11) DEFAULT NULL,
  `rank_our_technician_kbs` int(11) DEFAULT NULL,
  `quantity_of_production_desc` text,
  `satisfactory_services` text,
  `query_suggestion_desc` text,
  `feedback_for_quality` text,
  `feedback_for_technical_team` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `resolve_complain`
--
ALTER TABLE `resolve_complain`
  ADD PRIMARY KEY (`resolve_complain_id`);

--
-- AUTO_INCREMENT for table `resolve_complain`
--
ALTER TABLE `resolve_complain`
  MODIFY `resolve_complain_id` int(11) NOT NULL AUTO_INCREMENT;


--
-- Table structure for table `reason_customer_complain_details`
--
CREATE TABLE `reason_customer_complain_details` (
  `reason_customer_complain_id` int(11) NOT NULL,
  `resolve_complain_id` int(11) DEFAULT NULL,
  `reason_for_customer_complain_text` text,
  `reason_checking_person` int(11) DEFAULT NULL,
  `complain_start_date` date DEFAULT NULL,
  `complain_end_date` date DEFAULT NULL,
  `complain_hours` varchar(15) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `reason_customer_complain_details`
--
ALTER TABLE `reason_customer_complain_details`
  ADD PRIMARY KEY (`reason_customer_complain_id`);

--
-- AUTO_INCREMENT for table `reason_customer_complain_details`
--
ALTER TABLE `reason_customer_complain_details`
  MODIFY `reason_customer_complain_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `resolving_complain_details`
--
CREATE TABLE `resolving_complain_details` (
  `resolving_complain_id` int(11) NOT NULL,
  `resolve_complain_id` int(11) DEFAULT NULL,
  `resolving_complain_text` text,
  `resolving_person` int(11) DEFAULT NULL,
  `resolving_start_date` date DEFAULT NULL,
  `resolving_end_date` date DEFAULT NULL,
  `resolving_hours` varchar(15) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `resolving_complain_details`
--
ALTER TABLE `resolving_complain_details`
  ADD PRIMARY KEY (`resolving_complain_id`);

--
-- AUTO_INCREMENT for table `resolving_complain_details`
--
ALTER TABLE `resolving_complain_details`
  MODIFY `resolving_complain_id` int(11) NOT NULL AUTO_INCREMENT;

-- 
-- Added By : jignesh
-- Table structure for table `print_letter_roles`
-- Generation Time: Feb 08, 2018 at 11:13 AM
--

CREATE TABLE `print_letter_roles` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `letter_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Indexes for table `print_letter_roles`
--
ALTER TABLE `print_letter_roles`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `print_letter_roles`
--
ALTER TABLE `print_letter_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('225', 'Master >> General Master >> Print Letter Roles', '', '3.15');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '225'), (NULL, 'edit', 'edit', '225');


-- 
-- Added By : jignesh
-- Time: Feb 08, 2018 at 06:34 PM
--
DELETE FROM `website_modules` WHERE `website_modules`.`id` BETWEEN 151 AND 156;
DELETE FROM `module_roles` WHERE `module_id` BETWEEN 151 AND 156;
UPDATE `website_modules` SET `title` = 'HR >> Letter Management' WHERE `website_modules`.`id` = 150;

-- Avinash : 2018_02_08 : 07:00 PM
INSERT INTO `sms_topic` (`id`, `topic_name`, `message`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 'Resolve Complain SMS', 'We have received your inquiry. Thank you for your inquiry. We will send offer and other details via email soon.', '2018-02-08 18:37:21', '2018-02-08 18:37:21', 1, 1);

-- Avinash : 2018_02_09 : 12:30 PM
TRUNCATE `sms_topic`;
INSERT INTO `sms_topic` (`id`, `topic_name`, `message`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'COMPLAIN SMS', 'We have registered your complaint. Our technical team will give you technical support soon.\r\nThank you for your precious time.\r\nJay Khodiyar Team.', '2018-02-06 11:39:14', '2018-02-09 12:01:19', 1, 1),
(2, 'RESOLVE COMPLAIN SMS', 'Your complaint has been taken by Mr. Jayrambhai whose Contact No is  +91-9727786693 has solved your technical problem.\r\nThank you for your precious time.\r\nJay Khodiyar Team.', '2018-02-08 18:37:21', '2018-02-09 12:01:43', 1, 1),
(3, 'ENQUIRY SMS', 'Thank you for your inquiry with our company. Your inquiry is very important for us for which we shall assist you in all possible ways.\r\nThank You for your precious time.\r\nJay Khodiyar Team.', '2018-02-09 11:57:09', '2018-02-09 11:57:09', 1, 1),
(4, 'QUOTATION SMS', 'Thank you for your inquiry. You shall receive the quotation of our product in your email.  Kindly check your inbox in next 10 minutes.\r\nJay Khodiyar Team.', '2018-02-09 11:57:22', '2018-02-09 11:57:22', 1, 1),
(5, 'SALES ORDER SMS', 'Thank you for your order confirmation. Your sales order is been generated in our system and you shall receive the copy of the same in your email in few minutes. Your order is very important for us.\r\nJay Khodiyar Team.', '2018-02-09 11:57:40', '2018-02-09 11:57:40', 1, 1),
(6, 'PAYMENT RECEIPT SMS', 'Thank you for sending payment to jay Khodiyar Group. Your payment is credited in our bank account. The receipt of your payment will be sent to you on your email in sometime. Thank you for the payment.\r\nJay Khodiyar Team.', '2018-02-09 11:57:57', '2018-02-09 11:57:57', 1, 1),
(7, 'DISPATCH CHALLAN & INVOICE SMS', 'We are dispatching the material according to your order from our company. These are the Challan No : {{challan_no}} Transport No: {{transport_no}} Vehicle No: {{vehicle_no}} and Mobile No: {{mobile_no}}  We are dispatching your material today itself  for which you were waiting.\r\nJay Khodiyar Team.', '2018-02-09 11:59:19', '2018-02-09 12:20:21', 1, 1),
(8, 'INSTALLION SMS', 'Our technician Mr {{technician_name}} having Mobile No {{technician_mobile_no}} has left from our premises to install your project and will complete your work.\r\nThank you for your precious time.\r\nJay Khodiyar Team.', '2018-02-09 12:00:48', '2018-02-09 12:00:48', 1, 1);

-- Chirag 2018_02_09 : 12:30 PM
ALTER TABLE `staff`
CHANGE `married_status` `married_status` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `staff` CHANGE `married_status` `married_status` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '1 = Married, 0 = Unmarried';

-- Chirag 2018_02_09 : 03:50 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('226', 'HR >> Letter Management >> Add Letter', '', '11.4.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '226'), (NULL, 'add', 'add', '226'), (NULL, 'edit', 'edit', '226'), (NULL, 'delete', 'delete', '226');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('227', 'HR >> Letter Management >> Print Letter', '', '11.4.2');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'print', 'print', '227');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('228', 'HR >> Letter Management >> Issued Letter', '', '11.4.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '228');

-- Avinash : 2018_02_09 : 06:30 PM
ALTER TABLE `resolve_complain` ADD `pdf_url` VARCHAR(255) NULL DEFAULT NULL AFTER `feedback_for_technical_team`;

-- Chirag 2018_02_10 : 01:20 PM
UPDATE `website_modules` SET `main_module` = '7.4' WHERE `website_modules`.`id` = 114;
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('229', 'Complain', '', '7.2');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '229'), (NULL, 'add', 'add', '229'), (NULL, 'edit', 'edit', '229'), (NULL, 'delete', 'delete', '229');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('230', 'Resolve Complain', '', '7.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '230'), (NULL, 'add', 'add', '230'), (NULL, 'edit', 'edit', '230'), (NULL, 'delete', 'delete', '230');

--
-- Added By : Jignesh
-- Generation Time: Feb 12, 2018 at 11:49 AM
-- Table structure for table `text_suggestion`
--
CREATE TABLE `text_suggestion` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `text_suggestion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `text_suggestion`
--
ALTER TABLE `text_suggestion`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `text_suggestion`
--
ALTER TABLE `text_suggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
-- Avinash : 2018_02_12 : 05:15 PM  
ALTER TABLE `invoices` CHANGE `invoice_no` `invoice_no` VARCHAR(40) NULL DEFAULT NULL;

-- Chirag : 2018_02_13 : 04:15 PM  
ALTER TABLE `print_letter`
 ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `created_at`, ADD `updated_at` DATETIME NULL DEFAULT NULL AFTER `updated_by`;

INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'edit', 'edit', '228'), (NULL, 'delete', 'delete', '228');

-- Avinash : 2018_02_14 : 05:06 PM
ALTER TABLE `complain` ADD `is_resolve_complain_created` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Resolve Complain not created, 1 = Resolve Complain Created' AFTER `pdf_url`;

-- Avinash : 2018_02_14 : 07:00 PM
ALTER TABLE `challans` CHANGE `challan_no` `challan_no` VARCHAR(40) NULL DEFAULT NULL;

-- Jignesh : 2018_02_16 : 12:20 PM
ALTER TABLE `installation` ADD `installation_no_year` VARCHAR(55) NULL DEFAULT NULL AFTER `installation_no`;
ALTER TABLE `testing_report` ADD `testing_report_no_year` VARCHAR(55) NULL DEFAULT NULL AFTER `testing_report_no`;
ALTER TABLE `challan_items_motor_details` DROP `equipment_rpm_jk`, DROP `equipment_rpm_c`;
ALTER TABLE `challan_items_gearbox_details` DROP `equipment_rpm_jk`, DROP `equipment_rpm_c`;

-- Avinash : 2018_02_16 : 10:30 PM
ALTER TABLE `complain_items`  ADD `c_testing_normal_capacity_hrs` VARCHAR(255) NULL DEFAULT NULL  AFTER `challan_id`,  
ADD `c_testing_normal_test_raw_material` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_normal_capacity_hrs`,  
ADD `c_testing_normal_moisture_raw_material` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_normal_test_raw_material`,  
ADD `c_testing_normal_after_procces_moisture` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_normal_moisture_raw_material`,  
ADD `c_testing_full_capacity_hrs` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_normal_after_procces_moisture`,  
ADD `c_testing_full_test_raw_material` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_full_capacity_hrs`,  
ADD `c_testing_full_moisture_raw_material` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_full_test_raw_material`,  
ADD `c_testing_full_after_procces_moisture` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_full_moisture_raw_material`,  
ADD `c_temp_oli` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_testing_full_after_procces_moisture`,  
ADD `c_temp_eqipment` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_temp_oli`,  
ADD `c_temp_head` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_temp_eqipment`,  
ADD `c_temp_kiln` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_temp_head`,  
ADD `c_temp_hot_air` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_temp_kiln`,  
ADD `c_temp_weather` VARCHAR(255) NULL DEFAULT NULL  AFTER `c_temp_hot_air`;

-- Avinash : 2018_02_16 : 03:30 PM
ALTER TABLE `testing_report` CHANGE `testing_report_no` `testing_report_no` INT(11) NULL DEFAULT NULL;
ALTER TABLE `installation` CHANGE `installation_no` `installation_no` INT(11) NULL DEFAULT NULL;

-- Avinash : 2018_02_16 : 05:40 PM
ALTER TABLE `resolve_complain` DROP `service_charge`;
ALTER TABLE `resolve_complain` ADD `rc_jk_technical_person` INT(11) NULL DEFAULT NULL AFTER `party_id`, 
ADD `rc_jk_technical_person_contact` VARCHAR(50) NULL DEFAULT NULL AFTER `rc_jk_technical_person`, 
ADD `customer_site_service_checked` VARCHAR(50) NULL DEFAULT NULL AFTER `rc_jk_technical_person_contact`, 
ADD `customer_site_service_checked_contact` VARCHAR(50) NULL DEFAULT NULL AFTER `customer_site_service_checked`, 
ADD `customer_technical_person` VARCHAR(50) NULL DEFAULT NULL AFTER `customer_site_service_checked_contact`, 
ADD `customer_technical_person_contact` VARCHAR(50) NULL DEFAULT NULL AFTER `customer_technical_person`;

-- Chirag : 2018_02_17 : 01:00 PM
CREATE TABLE `parts` (
  `id` int(11) NOT NULL,
  `part_name` varchar(255) DEFAULT NULL,
  `part_code` varchar(255) DEFAULT NULL,
  `rate_in` text,
  `uom` int(11) DEFAULT NULL,
  `hsn_code` varchar(255) DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `igst` double DEFAULT NULL,
  `cgst` double DEFAULT NULL,
  `sgst` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `parts`
ALTER TABLE `parts`
  ADD PRIMARY KEY (`id`);
-- AUTO_INCREMENT for table `parts`
ALTER TABLE `parts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Avinash : 2018_02_19 : 11:20 AM
ALTER TABLE `complain` ADD `complain_no_year` VARCHAR(55) NULL DEFAULT NULL AFTER `complain_no`;
ALTER TABLE `resolve_complain` ADD `resolve_complain_no_year` VARCHAR(55) NULL DEFAULT NULL AFTER `resolve_complain_no`;

-- Chirag : 2018_02_17 : 11:30 AM
ALTER TABLE `parts`
  DROP `discount`;

UPDATE `website_modules` SET `main_module` = '7.5' WHERE `website_modules`.`id` = 114;

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('231', 'Parts', '', '7.4');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '231'), (NULL, 'add', 'add', '231'), (NULL, 'edit', 'edit', '231'), (NULL, 'delete', 'delete', '231');

-- Chirag : 2018_02_17 : 12:30 PM
DELETE FROM `website_modules` WHERE `website_modules`.`id` = 114;
DELETE FROM `module_roles` WHERE `module_roles`.`id` = 559;
DELETE FROM `module_roles` WHERE `module_roles`.`id` = 558;
DELETE FROM `module_roles` WHERE `module_roles`.`id` = 557;
DELETE FROM `module_roles` WHERE `module_roles`.`id` = 556;

--
-- Avinash : 2018_02_19 : 06:15 PM
-- Table structure for table `resolve_complain_items`
--
CREATE TABLE `resolve_complain_items` (
  `id` int(11) NOT NULL,
  `resolve_complain_id` int(11) DEFAULT NULL,
  `complain_id` int(11) DEFAULT NULL,
  `installation_id` int(11) DEFAULT NULL,
  `testing_report_id` int(11) DEFAULT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `resolve_complain_items`
--
ALTER TABLE `resolve_complain_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `resolve_complain_items`
--
ALTER TABLE `resolve_complain_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Avinash : 2018_02_20 : 12:00 PM
-- Table structure for table `feedback`
--
CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL,
  `feedback_no` int(11) DEFAULT NULL,
  `feedback_no_year` varchar(50) DEFAULT NULL,
  `feedback_date` date DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `feedback_call_by` int(11) DEFAULT NULL,
  `feedback_call_by_contact` varchar(100) DEFAULT NULL,
  `feedback_call_received_by` varchar(100) DEFAULT NULL,
  `feedback_call_received_by_contact` varchar(100) DEFAULT NULL,
  `technical_operator_name` varchar(100) DEFAULT NULL,
  `technical_operator_contact` varchar(100) DEFAULT NULL,
  `how_are_you` varchar(100) DEFAULT NULL,
  `can_we_talk` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No, 3 = After',
  `satisfied_with_quality` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `delivery_on_time` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `technician_come_on_time` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `technician_properly_explain` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `ask_money_for_install` varchar(100) DEFAULT NULL,
  `install_machine_properly` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `raw_material_using` varchar(100) DEFAULT NULL,
  `give_proper_production` varchar(100) DEFAULT NULL,
  `properly_done_service` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `visit_on_time` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `were_problems_arised` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `idea_regarding_problem` varchar(100) DEFAULT NULL,
  `behaviour_of_technician` int(11) DEFAULT NULL,
  `ask_for_facility` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `ask_for_money` varchar(100) DEFAULT NULL,
  `pick_up_facility` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `work_perfomance` int(11) DEFAULT NULL,
  `when_technician_came` varchar(100) DEFAULT NULL,
  `support_of_staff` int(11) DEFAULT NULL,
  `understand_technical_problem` int(11) DEFAULT NULL,
  `able_to_solve` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `satisfied_our_product` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `business_with_jk` tinyint(1) DEFAULT NULL COMMENT '1 = Yes, 2 = No',
  `say_something_for_product` varchar(100) DEFAULT NULL,
  `say_something_for_team` varchar(100) DEFAULT NULL,
  `behaviour_of_jk` int(11) DEFAULT NULL,
  `any_suggestion` text DEFAULT NULL,
  `pdf_url` VARCHAR(222) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `feedback_items`
--
CREATE TABLE `feedback_items` (
  `id` int(11) NOT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  `installation_id` int(11) DEFAULT NULL,
  `testing_report_id` int(11) DEFAULT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `feedback_items`
--
ALTER TABLE `feedback_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `feedback_items`
--
ALTER TABLE `feedback_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Avinash : 2018_02_20 : 04:15 PM
INSERT INTO `sms_topic` (`id`, `topic_name`, `message`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(9, 'FEEDBACK SMS', 'We have Recieved your Feedback.', '2018-02-20 16:12:38', '2018-02-20 16:12:38', 1, 1);

-- Chirag : 2018_02_21 : 11:08 AM
UPDATE `website_modules` SET `main_module` = '7.3' WHERE `website_modules`.`id` = 229;
UPDATE `website_modules` SET `main_module` = '7.4' WHERE `website_modules`.`id` = 230;
UPDATE `website_modules` SET `main_module` = '7.5' WHERE `website_modules`.`id` = 231;

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('232', 'Feedback', '', '7.2');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '232'), (NULL, 'add', 'add', '232'), (NULL, 'edit', 'edit', '232'), (NULL, 'delete', 'delete', '232');

-- Chirag : 2018_02_21 : 02:15 PM
UPDATE `website_modules` SET `main_module` = '7.4' WHERE `website_modules`.`id` = 229;
UPDATE `website_modules` SET `main_module` = '7.5' WHERE `website_modules`.`id` = 230;
UPDATE `website_modules` SET `main_module` = '7.6' WHERE `website_modules`.`id` = 231;

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('233', 'Complain Menu', '', '7.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '233');

-- Chirag : 2018_02_21 : 06:30 PM
ALTER TABLE `staff`
 CHANGE `blood_group` `blood_group` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
 CHANGE `mother_name` `mother_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
 CHANGE `contact_no` `contact_no` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
 CHANGE `image` `image` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
 CHANGE `address` `address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
 CHANGE `qualification` `qualification` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
 CHANGE `designation_id` `designation_id` INT(11) NULL DEFAULT NULL,
 CHANGE `department_id` `department_id` INT(11) NULL DEFAULT NULL,
 CHANGE `grade_id` `grade_id` INT(11) NULL DEFAULT NULL,
 CHANGE `salary` `salary` DOUBLE NULL DEFAULT NULL;

-- Chirag : 2018_02_21 : 06:45 PM
UPDATE `website_modules` SET `title` = 'Service >> Installation' WHERE `website_modules`.`id` = 223;
UPDATE `website_modules` SET `title` = 'Service >> Feedback' WHERE `website_modules`.`id` = 232;
UPDATE `website_modules` SET `title` = 'Service >> Complain Menu' WHERE `website_modules`.`id` = 233;
UPDATE `website_modules` SET `title` = 'Service >> Complain >> Complain' WHERE `website_modules`.`id` = 229;
UPDATE `website_modules` SET `title` = 'Service >> Complain >> Resolve Complain' WHERE `website_modules`.`id` = 230;

-- Avinash : 2018_02_22 : 10:30 AM
INSERT INTO `terms_and_conditions` (`id`, `label`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'General Invoice From Complain >> Terms And Conditions', 'general_invoice_from_complain_terms_and_conditions', 'general_invoice_from_complain_terms_and_conditions', '1', '2018-02-22 10:30:00');
INSERT INTO `terms_and_conditions` (`id`, `label`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'General Invoice From Party >> Terms And Conditions', 'general_invoice_from_party_terms_and_conditions', 'general_invoice_from_party_terms_and_conditions', '1', '2018-02-22 10:30:00');


--
-- Avinash : 2018_02_22 : 07:00 PM
-- Table structure for table `general_invoice`
--
CREATE TABLE `general_invoice` (
  `general_invoice_id` int(11) NOT NULL,
  `general_invoice_no` varchar(40) DEFAULT NULL,
  `general_invoice_date` date DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `purchase_order_no` varchar(100) DEFAULT NULL,
  `purchase_order_date` date DEFAULT NULL,
  `sales_order_no` varchar(100) DEFAULT NULL,
  `sales_order_date` date DEFAULT NULL,
  `note` text,
  `loading_at` varchar(100) DEFAULT NULL,
  `port_of_discharge` varchar(100) DEFAULT NULL,
  `delivery_through_id` int(11) DEFAULT NULL,
  `place_of_delivery` varchar(100) DEFAULT NULL,
  `name_of_shipment` varchar(100) DEFAULT NULL,
  `truck_container_no` varchar(100) DEFAULT NULL,
  `lr_no` varchar(100) DEFAULT NULL,
  `lr_date` date DEFAULT NULL,
  `transport` int(11) DEFAULT NULL,
  `p_and_f` int(11) DEFAULT NULL,
  `insurance` int(11) DEFAULT NULL,
  `terms_conditions` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `general_invoice`
--
ALTER TABLE `general_invoice`
  ADD PRIMARY KEY (`general_invoice_id`);

--
-- AUTO_INCREMENT for table `general_invoice`
--
ALTER TABLE `general_invoice`
  MODIFY `general_invoice_id` int(11) NOT NULL AUTO_INCREMENT;
  
--
-- Table structure for table `general_invoice_details`
--
CREATE TABLE `general_invoice_details` (
  `id` int(11) NOT NULL,
  `general_invoice_id` int(11) DEFAULT NULL,
  `complain_id` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `item_rate` double DEFAULT NULL,
  `disc_per` double DEFAULT NULL,
  `disc_value` double DEFAULT NULL,
  `cgst` double DEFAULT NULL,
  `cgst_amount` double DEFAULT NULL,
  `sgst` double DEFAULT NULL,
  `sgst_amount` double DEFAULT NULL,
  `igst` double DEFAULT NULL,
  `igst_amount` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `net_amount` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `general_invoice_details`
--
ALTER TABLE `general_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `general_invoice_details`
--
ALTER TABLE `general_invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Avinash : 2018_02_23 : 04:00 PM
ALTER TABLE `general_invoice` ADD `is_approved` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Disapproved, 1 = Approved' AFTER `terms_conditions`;
ALTER TABLE `general_invoice` ADD `approved_by` INT(11) NULL DEFAULT NULL AFTER `updated_at`, ADD `approved_at` DATETIME NULL DEFAULT NULL AFTER `approved_by`;

-- Avinash : 2018_02_23 : 06:45 PM
ALTER TABLE `general_invoice` ADD `pdf_url` VARCHAR(222) NULL DEFAULT NULL AFTER `terms_conditions`;

-- Avinash : 2018_02_23 : 04:35 PM
ALTER TABLE `invoice_items` CHANGE `challan_no` `challan_id` INT(11) NULL DEFAULT NULL;

-- Avinash : 2018_02_26 : 09:50 AM
ALTER TABLE `general_invoice` ADD `created_from` TINYINT(1) NULL DEFAULT NULL COMMENT '1 = Created From Complain, 2 = Created From Dispatch Party' AFTER `general_invoice_date`;
ALTER TABLE `general_invoice_details` ADD `part_id` INT(11) NULL DEFAULT NULL AFTER `complain_id`, ADD `uom_id` INT(11) NULL DEFAULT NULL AFTER `part_id`;

-- Chirag : 2018_02_26 : 04:15 PM
UPDATE `website_modules` SET `main_module` = '7.7' WHERE `website_modules`.`id` = 231;
UPDATE `website_modules` SET `title` = 'Service >> General Invoice >> Parts' WHERE `website_modules`.`id` = 231;
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('234', 'Service >> General Invoice Menu', '', '7.6');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '234');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('235', 'Service >> General Invoice >> General Invoice', '', '7.8');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '235'), (NULL, 'add', 'add', '235'), (NULL, 'edit', 'edit', '235'), (NULL, 'delete', 'delete', '235');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('236', 'Can Approve General Invoice', '', '7.9');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '236');

-- Chirag : 2018_03_01 : 10:15 PM
ALTER TABLE `feedback` ADD `can_we_talk_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `can_we_talk`;
ALTER TABLE `feedback` ADD `satisfied_with_quality_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `satisfied_with_quality`;
ALTER TABLE `feedback` ADD `delivery_on_time_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `delivery_on_time`;
ALTER TABLE `feedback` ADD `technician_come_on_time_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `technician_come_on_time`;
ALTER TABLE `feedback` ADD `technician_properly_explain_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `technician_properly_explain`;
ALTER TABLE `feedback` ADD `install_machine_properly_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `install_machine_properly`;
ALTER TABLE `feedback` ADD `properly_done_service_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `properly_done_service`;
ALTER TABLE `feedback` ADD `visit_on_time_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `visit_on_time`;
ALTER TABLE `feedback` ADD `were_problems_arised_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `were_problems_arised`;
ALTER TABLE `feedback` ADD `ask_for_facility_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `ask_for_facility`;
ALTER TABLE `feedback` ADD `pick_up_facility_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `pick_up_facility`;
ALTER TABLE `feedback` ADD `able_to_solve_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `able_to_solve`;
ALTER TABLE `feedback` ADD `satisfied_our_product_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `satisfied_our_product`;
ALTER TABLE `feedback` ADD `business_with_jk_remark` VARCHAR(255) NULL DEFAULT NULL AFTER `business_with_jk`;

-- Jignesh : 2018_03_13 : 08:17 PM
ALTER TABLE `sales_order` ADD `cancel_reason` TEXT NULL DEFAULT NULL AFTER `work_order_notes`;

-- Jignesh : 2018_03_14 : 11:36 AM
ALTER TABLE `proforma_invoices` ADD `cancel_reason` TEXT NULL DEFAULT NULL AFTER `sales_order_stage_id`;

INSERT INTO `module_roles` (`title`, `role_name`, `module_id`) VALUES ('isManagement', 'isManagement', '75');

-- Chirag : 2018_03_15 : 07:27 AM
ALTER TABLE `installation`
 ADD `is_feedback_created` TINYINT(1) NOT NULL COMMENT '0 = feedback not created, 1 = feedback created' AFTER `pdf_url`,
 ADD `is_complain_created` TINYINT(1) NOT NULL COMMENT '0 = complain not created, 1 = complain created' AFTER `is_feedback_created`;
 
 -- Jignesh : 2018_03_15 : 09:00 PM
 ALTER TABLE `general_invoice` 
 ADD `sea_freight_by` TINYINT NULL DEFAULT NULL AFTER `p_and_f`, 
 ADD `sea_freight` VARCHAR(100) NULL DEFAULT NULL AFTER `sea_freight_by`, 
 ADD `sea_freight_type` INT NULL DEFAULT NULL AFTER `sea_freight`, 
 ADD `lc_no` VARCHAR(111) NULL DEFAULT NULL AFTER `sea_freight_type`, 
 ADD `lc_date` DATE NULL DEFAULT NULL AFTER `lc_no`, 
 ADD `port_of_loading` VARCHAR(111) NULL DEFAULT NULL AFTER `lc_date`, 
 ADD `port_of_loading_country_id` INT NULL DEFAULT NULL AFTER `port_of_loading`, 
 ADD `origin_of_goods_country_id` INT NULL DEFAULT NULL AFTER `port_of_loading_country_id`, 
 ADD `final_destination_country_id` INT NULL DEFAULT NULL AFTER `origin_of_goods_country_id`, 
 ADD `line_seal_no` VARCHAR(111) NULL DEFAULT NULL AFTER `final_destination_country_id`,
 ADD `exporter_seal_no` VARCHAR(111) NULL DEFAULT NULL AFTER `line_seal_no`,
 ADD `bank_detail` TEXT NULL DEFAULT NULL AFTER `exporter_seal_no`;

-- Jignesh : 2018_03_16 : 04:00 PM
INSERT INTO `terms_and_conditions` (`label`, `module`, `detail`) VALUES ('Export >> General Invoice >> Declaration', 'general_invoice_declaration_for_export', NULL);

-- Chirag : 2018_03_19 : 03:42 PM
INSERT INTO `terms_and_conditions` (`id`, `label`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'Domestic >> Sales Order >> Cancel Order Letter', 'domestic_sales_order_cancel_order_letter', NULL, NULL, NULL), (NULL, 'Export >> Sales Order >> Cancel Order Letter', 'export_sales_order_cancel_order_letter', NULL, NULL, NULL);

ALTER TABLE `sales_order` ADD `proof_of_cancellation` VARCHAR(255) NULL DEFAULT NULL AFTER `cancel_reason`, ADD `cancel_letter` TEXT NULL DEFAULT NULL AFTER `proof_of_cancellation`;

-- Chirag : 2018_03_19 : 05:35 PM
INSERT INTO `terms_and_conditions` (`id`, `label`, `module`, `detail`, `updated_by`, `updated_at`) VALUES (NULL, 'Domestic >> Proforma Invoice >> Cancel Order Letter', 'domestic_proforma_invoice_cancel_order_letter', NULL, NULL, NULL), (NULL, 'Export >> Proforma Invoice >> Cancel Order Letter', 'export_proforma_invoice_cancel_order_letter', NULL, NULL, NULL);

ALTER TABLE `proforma_invoices` ADD `proof_of_cancellation` VARCHAR(255) NULL DEFAULT NULL AFTER `cancel_reason`, ADD `cancel_letter` TEXT NULL DEFAULT NULL AFTER `proof_of_cancellation`;

-- Avinash : 2018_03_21 : 04:45 PM
ALTER TABLE `sales_order` CHANGE `is_dispatched` `is_dispatched` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Pending Dispatch, 1 = Dispatched, 2 = Canceled Order';
ALTER TABLE `proforma_invoices` CHANGE `is_dispatched` `is_dispatched` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Pending Dispatch, 1 = Dispatched, 2 = Canceled';

-- Chirag : 2018_03_22 : 04:52 PM
ALTER TABLE `item_documents` ADD `allow_to_edit` TINYINT(1) NULL DEFAULT NULL COMMENT '0 = Not Allow To Edit, 1 = Allow To Edit' AFTER `document_type`;

-- Avinash : 2018_03_23 : 05:30 PM
ALTER TABLE `party` CHANGE `party_code` `party_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `party_name` `party_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `reference_description` `reference_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `address` `address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `pincode` `pincode` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `phone_no` `phone_no` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `email_id` `email_id` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `website` `website` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `opening_bal` `opening_bal` DOUBLE NULL DEFAULT NULL, CHANGE `credit_limit` `credit_limit` DOUBLE NULL DEFAULT NULL, 
CHANGE `tin_vat_no` `tin_vat_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `gst_no` `gst_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `tin_cst_no` `tin_cst_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `ecc_no` `ecc_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `pan_no` `pan_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `range` `range` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `division` `division` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `service_tax_no` `service_tax_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `w_address` `w_address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `w_web` `w_web` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;

ALTER TABLE `contact_person` CHANGE `party_id` `party_id` INT(11) NULL DEFAULT NULL, 
CHANGE `name` `name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `email` `email` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `note` `note` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, 
CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;

-- Ishita :  2018_03_28 : 11:30 PM
ALTER TABLE `agent` ADD `city` INT(11) NULL DEFAULT NULL AFTER `address`;
ALTER TABLE `agent` ADD `state` INT(11) NULL DEFAULT NULL AFTER `city`;
ALTER TABLE `agent` ADD `country` INT(11) NULL DEFAULT NULL AFTER `state`;

-- Jignesh : 2018_03_28 : 12:49 PM
ALTER TABLE `item_documents` ADD `sequence` INT NULL DEFAULT NULL AFTER `allow_to_edit`;

-- Ishita :  2018_03_29 : 4:30 PM
-- Table structure for table `default_challan_details`
--

CREATE TABLE `default_challan_details` (
  `id` int(11) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `default_challan_details`
--

INSERT INTO `default_challan_details` (`id`, `meta_key`, `meta_value`) VALUES
(1, 'm_user_1', '1'),
(2, 'm_make_1', '1'),
(3, 'm_hp_1', '4'),
(4, 'm_kw_1', '4'),
(5, 'm_frequency_1', '1'),
(6, 'm_rpm_1', '1'),
(7, 'm_volts_1', '1'),
(8, 'm_user_2', '2'),
(9, 'm_make_2', '1'),
(10, 'm_hp_2', '15'),
(11, 'm_kw_2', '15'),
(12, 'm_frequency_2', '1'),
(13, 'm_rpm_2', '1'),
(14, 'm_volts_2', '1'),
(15, 'm_user_3', '3'),
(16, 'm_make_3', '1'),
(17, 'm_hp_3', '7'),
(18, 'm_kw_3', '7'),
(19, 'm_frequency_3', '1'),
(20, 'm_rpm_3', '1'),
(21, 'm_volts_3', '1'),
(22, 'm_user_4', '4'),
(23, 'm_make_4', '1'),
(24, 'm_hp_4', '4'),
(25, 'm_kw_4', '4'),
(26, 'm_frequency_4', '1'),
(27, 'm_rpm_4', '1'),
(28, 'm_volts_4', '1'),
(29, 'g_user_1', '3'),
(30, 'g_make_1', '5'),
(31, 'g_gear_type_1', '1'),
(32, 'g_model_1', '2'),
(33, 'g_ratio_1', '2'),
(34, 'g_user_2', '4'),
(35, 'g_make_2', '5'),
(36, 'g_gear_type_2', '1'),
(37, 'g_model_2', '1'),
(38, 'g_ratio_2', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `default_challan_details`
--
ALTER TABLE `default_challan_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `default_challan_details`
--
ALTER TABLE `default_challan_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

-- Chirag :  2018_03_30 : 4:30 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('237', 'Master >> Challan >> Default Challan Details', '', '3.10.11');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '237'), (NULL, 'edit', 'edit', '237');

-- Avinash : 2018_03_30 : 01:00 PM
ALTER TABLE `quotation_items` ADD `item_documents` TEXT NULL DEFAULT NULL AFTER `cust_part_no`;

-- Jignesh : 2018_03_31 : 9:55 AM
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'isManagement', 'isManagement', '231');

-- Avinash : 2018_04_05 : 3:00 PM
ALTER TABLE `visitors` ADD `unsubscribe` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Subscribe, 1 = Unsubscribe' AFTER `others`, ADD `unsubscribe_code` VARCHAR(20) NULL DEFAULT NULL AFTER `unsubscribe`;

-- Chirag :  2018_04_05 : 05:42 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('238', 'Display Send SMS Checkbox', '', '10.2');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '238');

-- Avinash : 2018_04_07 : 11:10 AM
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'is_management', 'is_management', '1');

--
-- Avinash : 2018_04_09 : 01:25 PM
-- Table structure for table `party_transfer_history`
--

CREATE TABLE `party_transfer_history` (
  `id` int(11) NOT NULL,
  `module_table_name` varchar(50) DEFAULT NULL,
  `module_table_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `transfer_from` int(11) DEFAULT NULL,
  `transfer_to` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `party_transfer_history`
--
ALTER TABLE `party_transfer_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `party_transfer_history`
--
ALTER TABLE `party_transfer_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag :  2018_04_10 : 02:20 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('239', 'General >> SMS', '', '10.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '239'), (NULL, 'add', 'add', '239');

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('240', 'General >> SMS >> Allow To Send', '', '10.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'party', 'party', '240'), (NULL, 'supplier', 'Supplier', '240'), (NULL, 'staff', 'staff', '240');

ALTER TABLE `sent_sms`
 ADD `party_id` INT(11) NULL DEFAULT NULL AFTER `send_to`,
 ADD `supplier_id` INT(11) NULL DEFAULT NULL AFTER `party_id`,
 ADD `staff_id` INT(11) NULL DEFAULT NULL AFTER `supplier_id`;

-- Avinash :  2018_04_23 : 03:45 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('241', 'Visitor Chat Notification', '', '1.4');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '241');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('242', 'Visitor Tab', '', '1.5');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '242');

-- Avinash :  2018_04_26 : 09:55 AM
ALTER TABLE `visitors` ADD `http_host_name` VARCHAR(255) NULL DEFAULT NULL AFTER `email`;

-- Ishita :  2018_04_26 : 06:00 AM
ALTER TABLE `staff` ADD `chat_title` VARCHAR(225) NULL DEFAULT NULL AFTER `is_login`;

-- Avinash :  2018_04_27 : 05:30 AM
ALTER TABLE `staff` ADD `accepted_visitors` INT(11) NOT NULL DEFAULT '0' AFTER `chat_title`;

-- Avinash :  2018_04_30 : 03:40 AM
ALTER TABLE `visitors` ADD `state` VARCHAR(255) NULL DEFAULT NULL AFTER `city`;
ALTER TABLE `visitors` ADD `location` VARCHAR(255) NULL DEFAULT NULL AFTER `currency`;
ALTER TABLE `visitors` ADD `timezone` VARCHAR(255) NULL DEFAULT NULL AFTER `location`;

--
-- Avinash :  2018_05_01 : 06:00 AM
-- Table structure for table `visitor_response`
--

CREATE TABLE `visitor_response` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `visitor_id` int(11) DEFAULT NULL,
  `response_time` time DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `visitor_response`
--
ALTER TABLE `visitor_response`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `visitor_response`
--
ALTER TABLE `visitor_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Avinash :  2018_05_02 : 03:40 PM
ALTER TABLE `staff` ADD `socket_id` VARCHAR(255) NULL DEFAULT NULL AFTER `accepted_visitors`;

-- Avinash :  2018_05_02 : 10:30 AM
ALTER TABLE `visitors` ADD `socket_id` VARCHAR(255) NULL DEFAULT NULL AFTER `session_id`;
ALTER TABLE `visitors` ADD `is_login` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Is not login, 1 = Is login' AFTER `session_id`;

-- Avinash :  2018_05_10 : 04:50 PM
UPDATE `module_roles` SET `title` = 'is_management' WHERE `module_roles`.`id` = 728;
UPDATE `module_roles` SET `role_name` = 'is_management' WHERE `module_roles`.`id` = 728;
ALTER TABLE `general_invoice_details` ADD `purchase_item_id` INT(11) NULL DEFAULT NULL AFTER `part_id`;
ALTER TABLE `general_invoice` CHANGE `created_from` `created_from` TINYINT(1) NULL DEFAULT NULL COMMENT '1 = Created From Complain, 2 = Created From Dispatch Party, 3 = Created From Dispatch Party ( Purchase Item )';

--
-- Avinash :  2018_05_11 : 06:00 PM
-- Table structure for table `bom_item_details`
--

CREATE TABLE `bom_item_details` (
  `id` int(11) NOT NULL,
  `bom_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bom_master`
--

CREATE TABLE `bom_master` (
  `bom_id` int(11) NOT NULL,
  `sales_item_id` int(11) DEFAULT NULL,
  `effective_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bom_projects`
--

CREATE TABLE `bom_projects` (
  `bom_project_id` int(11) NOT NULL,
  `bom_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_qty` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `bom_item_details`
--
ALTER TABLE `bom_item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bom_master`
--
ALTER TABLE `bom_master`
  ADD PRIMARY KEY (`bom_id`);

--
-- Indexes for table `bom_projects`
--
ALTER TABLE `bom_projects`
  ADD PRIMARY KEY (`bom_project_id`);

--
-- AUTO_INCREMENT for table `bom_item_details`
--
ALTER TABLE `bom_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bom_master`
--
ALTER TABLE `bom_master`
  MODIFY `bom_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bom_projects`
--
ALTER TABLE `bom_projects`
  MODIFY `bom_project_id` int(11) NOT NULL AUTO_INCREMENT;

-- Avinash :  2018_05_12 : 10:00 PM
ALTER TABLE `bom_item_details` ADD `basic_qty` DOUBLE NULL DEFAULT NULL AFTER `item_id`;
ALTER TABLE `quotations` ADD `latest_revision` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 = Not Latest Revision, 1 = Is Latest Revision' AFTER `rev_date`;

--
-- Avinash :  2018_05_15 : 11:30 AM
-- Table structure for table `bom_item_expenses`
--
CREATE TABLE `bom_item_expenses` (
  `bom_item_expense_id` int(11) NOT NULL,
  `bom_id` int(11) DEFAULT NULL,
  `item_expense` varchar(255) DEFAULT NULL,
  `expense` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `bom_item_expenses`
--
ALTER TABLE `bom_item_expenses`
  ADD PRIMARY KEY (`bom_item_expense_id`);

--
-- AUTO_INCREMENT for table `bom_item_expenses`
--
ALTER TABLE `bom_item_expenses`
  MODIFY `bom_item_expense_id` int(11) NOT NULL AUTO_INCREMENT;

-- Avinash : 2018_05_17 : 12:15 PM
ALTER TABLE `bom_item_details` ADD `project_id` INT(11) NULL DEFAULT NULL AFTER `item_id`;
ALTER TABLE `bom_item_details` ADD `project_qty` DOUBLE NULL DEFAULT NULL AFTER `basic_qty`;

-- Avinash : 2018_05_18 : 12:00 PM
ALTER TABLE `purchase_items` CHANGE `stock` `stock` INT(11) NOT NULL COMMENT 'Batch Wise Stock >> 0 = Yes, 1 = No.';
ALTER TABLE `purchase_items` ADD `current_item_stock` DOUBLE NOT NULL DEFAULT '0' AFTER `stock`;

-- Avinash : 2018_05_19 : 12:30 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('243', 'Master >> BOM', '', '3.2.99');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '243'), (NULL, 'add', 'add', '243'), (NULL, 'edit', 'edit', '243'), (NULL, 'delete', 'delete', '243');

-- Avinash : 2018_05_19 : 02:45 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('244', 'Report >> Item Diff Report', '', '9.40');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '244');

-- Avinash : 2018_05_22 : 10:30 AM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('245', 'Report >> Purchase Item Stock Report', '', '9.41');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '245');

-- Avinash : 2018_05_23 : 10:00 AM
ALTER TABLE `purchase_invoice` DROP `updated_at`;
ALTER TABLE `purchase_invoice` ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `created_at`, ADD `updated_at` DATETIME NULL DEFAULT NULL AFTER `updated_by`;

-- Avinash : 2018_05_24 : 9:20 AM
ALTER TABLE `purchase_invoice_items` ADD `pending_qty` DOUBLE NULL DEFAULT NULL COMMENT 'Pending Qty for use in Sales ( FIFO )' AFTER `quantity`;

-- Avinash : 2018_05_24 : 10:40 AM
ALTER TABLE `purchase_items` ADD `sales_rate` DOUBLE NULL DEFAULT NULL AFTER `reference_qty_uom`;

-- Ishita : 2018_05_18 : 7:25 PM
ALTER TABLE `currency` ADD `inr_amt` INT(11) NULL DEFAULT NULL AFTER `currency`;

--
-- Avinash : 2018_06_01 : 2:10 PM
-- Table structure for table `purchase_item_fifo`
--

CREATE TABLE `purchase_item_fifo` (
  `fifo_id` int(11) NOT NULL,
  `challan_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `purchase_invoice_item_id` int(11) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `purchase_item_fifo`
--
ALTER TABLE `purchase_item_fifo`
  ADD PRIMARY KEY (`fifo_id`);

--
-- AUTO_INCREMENT for table `purchase_item_fifo`
--
ALTER TABLE `purchase_item_fifo`
  MODIFY `fifo_id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag : 2018_06_06 : 12:10 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('246', 'Sales >> Production Start', '', '5.5.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '246');

-- Pooja : 2018_06_07 : 08:11 PM

CREATE TABLE `production` (
  `production_id` int(11) NOT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `proforma_invoice_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`production_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `production_id` int(11) NOT NULL AUTO_INCREMENT;

-- Ishita : 2018_06_11 02:40 PM
ALTER TABLE `production` ADD `is_finished` INT(11) NULL DEFAULT NULL COMMENT '1=production,2=fiinished' AFTER `item_id`;

-- Avinash : 2018_06_15 : 09:30 AM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('247', 'Can Allow Transfer Party', '', '5.1');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '247');

-- Avinash : 2018_06_15 : 10:10 AM
ALTER TABLE `sales_order_items` ADD `pro_started_qty` INT(11) NOT NULL DEFAULT '0' AFTER `dispatched_qty`, ADD `pro_ended_qty` INT(11) NOT NULL DEFAULT '0' AFTER `pro_started_qty`;
ALTER TABLE `proforma_invoice_items` ADD `pro_started_qty` INT(11) NOT NULL DEFAULT '0' AFTER `dispatched_qty`, ADD `pro_ended_qty` INT(11) NOT NULL DEFAULT '0' AFTER `pro_started_qty`;
ALTER TABLE `production` CHANGE `is_finished` `pro_status` TINYINT(1) NULL DEFAULT '1' COMMENT '1 = In Production, 2 = Finished';
ALTER TABLE `production` ADD `finished_at` DATETIME NULL DEFAULT NULL AFTER `updated_by`, ADD `finished_by` INT(11) NULL DEFAULT NULL AFTER `finished_at`;

-- Avinash : 2018_07_10 : 10:30 AM
ALTER TABLE `purchase_items` ADD `in_production` DOUBLE NOT NULL DEFAULT '0' AFTER `current_item_stock`, ADD `effective_stock` DOUBLE NOT NULL DEFAULT '0' AFTER `in_production`;

-- Avinash : 2018_07_14 : 01:50 AM
ALTER TABLE `purchase_item_fifo` ADD `general_invoice_id` INT(11) NULL DEFAULT NULL AFTER `challan_id`;

-- Avinash : 2018_07_16 : 11:50 AM
ALTER TABLE `purchase_item_fifo` ADD `production_id` INT(11) NULL DEFAULT NULL AFTER `fifo_id`;

--
-- Avinash : 2018_07_27 : 04:30 PM
-- Table structure for table `couriers`
--
CREATE TABLE `couriers` (
  `courier_id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `address_type` tinyint(1) DEFAULT NULL COMMENT '1 = Party Details Address, 2 = Delivery Address, 3 = Communication Address',
  `courier_name` varchar(255) DEFAULT NULL,
  `courier_date` date DEFAULT NULL,
  `courier_time` time DEFAULT NULL,
  `docket_no` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`courier_id`);

--
-- AUTO_INCREMENT for table `couriers`
--
ALTER TABLE `couriers`
  MODIFY `courier_id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('248', 'General >> Courier', '', '10.5');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '248'), (NULL, 'add', 'add', '248'), (NULL, 'edit', 'edit', '248'), (NULL, 'delete', 'delete', '248');

-- Chirag : 2018_07_30 : 07:55 PM
ALTER TABLE `items` ADD `item_for_domestic` TINYINT(1) NOT NULL DEFAULT '0' AFTER `igst`, ADD `item_for_export` TINYINT(1) NOT NULL DEFAULT '0' AFTER `item_for_domestic`;

-- Chirag : 2018_07_31 : 10:21 AM
ALTER TABLE `items` CHANGE `item_code2` `item_code2` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Chirag : 2018_08_08 : 11:21 AM
ALTER TABLE `couriers` ADD `party_name` VARCHAR(255) NULL DEFAULT NULL AFTER `address_type`, ADD `party_address` TEXT NULL DEFAULT NULL AFTER `party_name`, ADD `contact_person_name` VARCHAR(255) NULL DEFAULT NULL AFTER `party_address`, ADD `contact_person_no` VARCHAR(255) NULL DEFAULT NULL AFTER `contact_person_name`;
ALTER TABLE `couriers` ADD `party_contact_no` VARCHAR(255) NULL DEFAULT NULL AFTER `party_name`;

-- Chirag : 2018_08_08 : 06:21 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('249', 'General >> Party Frontend', '', '10.6');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'View', 'View', '249');

-- Chirag : 2018_08_09 : 10:05 AM
CREATE TABLE `party_frontend_files` (
  `file_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type_id` tinyint(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `party_frontend_files`
  ADD PRIMARY KEY (`file_id`);

ALTER TABLE `party_frontend_files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag : 2018_08_09 : 11:05 AM
ALTER TABLE `party` ADD `username` VARCHAR(255) NULL DEFAULT NULL AFTER `outstanding_balance`, ADD `password` VARCHAR(255) NULL DEFAULT NULL AFTER `username`;

-- Chirag : 2018_08_09 : 02:58 PM
ALTER TABLE `party_frontend_files` ADD `party_id` INT NULL DEFAULT NULL AFTER `file_type_id`;
ALTER TABLE `party_frontend_files` CHANGE `file_type_id` `file_type_id` TINYINT(1) NULL DEFAULT NULL COMMENT '1 = Files, 2 = Videos, 3 = Photos';

-- Chirag : 2018_08_14 : 03:55 PM
ALTER TABLE `company_banks` ADD `for_domestic` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = No, 1 = Yes' AFTER `id`, ADD `for_export` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = No, 1 = Yes' AFTER `for_domestic`;

-- Chirag : 2018_08_16 : 06:15 PM
ALTER TABLE `couriers` ADD `parcel_details` TEXT NULL DEFAULT NULL AFTER `docket_no`;

-- Avinash : 2018_08_23 : 10:00 AM
UPDATE `party` SET `username` = `email_id`;
UPDATE `party` SET `username` = `phone_no` WHERE `username` = '';
UPDATE `party` SET `password` = CONCAT(
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25))),
  CHAR( FLOOR(65 + (RAND() * 25)))
  );

-- Riddhi : 2018_08_30 : 11:30 AM
ALTER TABLE  `party_frontend_files` CHANGE  `file_type_id`  `file_type_id` TINYINT( 1 ) NULL DEFAULT NULL COMMENT  '1 = General Files, 2 = Videos, 3 = Photos, 4=Foundation Detail';

-- Avinash : 2018_08_30 : 05:00 PM
ALTER TABLE `purchase_project` ADD `item_group_id` INT(11) NULL DEFAULT NULL AFTER `project_name`;

-- Chirag : 2018_10_27 : 12:00 PM
ALTER TABLE `proforma_invoices` ADD `freight_note` VARCHAR(255) NULL DEFAULT NULL AFTER `freight`;
ALTER TABLE `proforma_invoices` ADD `packing_forwarding_note` VARCHAR(255) NULL DEFAULT NULL AFTER `packing_forwarding`;
ALTER TABLE `proforma_invoices` ADD `transit_insurance_note` VARCHAR(255) NULL DEFAULT NULL AFTER `transit_insurance`;

-- Chirag : 2018_11_22 : 02:54 PM
ALTER TABLE `company` ADD `authorized_signatory_name` VARCHAR(255) NULL DEFAULT NULL AFTER `invoice_terms`, ADD `authorized_signatory_image` TEXT NULL DEFAULT NULL AFTER `authorized_signatory_name`;

-- Ishita : 2018_12_19 : 01:30 PM
-- Table structure for table `user_feedback`
--
CREATE TABLE `user_feedback` (
  `feedback_id` int(11) NOT NULL,
  `feedback_created` int(11) DEFAULT NULL,
  `feedback_date` date DEFAULT NULL,
  `note` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `user_feedback`
  ADD PRIMARY KEY (`feedback_id`);

ALTER TABLE `user_feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT;

-- Ishita : 2018_12_19 : 01:30 PM
-- Table structure for table `reply`
--
CREATE TABLE `reply` (
  `reply_id` int(11) NOT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  `assign_to_id` int(11) DEFAULT NULL,
  `reply_date` date DEFAULT NULL,
  `note` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `reply`
  ADD PRIMARY KEY (`reply_id`);

ALTER TABLE `reply`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT;

-- Ishita : 2018_12_31 02:20 PM
ALTER TABLE `user_feedback` ADD `feedback_from` TINYINT(1) NULL DEFAULT NULL COMMENT '1 = Jay Khodiyar, 2 = JK India' AFTER `note`;
ALTER TABLE `reply` ADD `reply_from` TINYINT(1) NULL DEFAULT NULL COMMENT '1 = Jay Khodiyar, 2 = JK India' AFTER `note`;

-- Ishita : 2018_12_31 05:40 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('250', 'General >> Feedback', '', '10.7');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '250'), (NULL, 'add', 'add', '250'), (NULL, 'edit', 'edit', '250'), (NULL, 'delete', 'delete', '250');

-- Avinash : 2019_01_25 10:30 AM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('251', 'Report >> Chart >> Employee Performance Chart', '', '9.11');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '251');
UPDATE `website_modules` SET `title` = 'Report >> Chart >> Sales Chart' WHERE `website_modules`.`id` = 31;

-- Avinash : 2019_02_21 04:00 PM
ALTER TABLE `party` ADD `whatsapp_no` VARCHAR(255) NULL DEFAULT NULL AFTER `phone_no`;
UPDATE `party` SET `whatsapp_no` = `phone_no`;
ALTER TABLE `quotation_items` CHANGE `item_description` `item_description` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `quotation_items` CHANGE `item_desc` `item_desc` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `quotation_items` CHANGE `quotation_id` `quotation_id` INT(11) NULL DEFAULT NULL, CHANGE `item_code` `item_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_name` `item_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `add_description` `add_description` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `detail_description` `detail_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `drawing_number` `drawing_number` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `drawing_revision` `drawing_revision` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uom` `uom` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `total_amount` `total_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_note` `item_note` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `quantity` `quantity` DOUBLE NULL DEFAULT NULL, CHANGE `disc_per` `disc_per` DOUBLE NULL DEFAULT NULL, CHANGE `rate` `rate` DOUBLE NULL DEFAULT NULL, CHANGE `disc_value` `disc_value` DOUBLE NULL DEFAULT NULL, CHANGE `amount` `amount` DOUBLE NULL DEFAULT NULL, CHANGE `net_amount` `net_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_status` `item_status` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cust_part_no` `cust_part_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;

-- Avinash : 2019_03_02 10:30 PM
ALTER TABLE `sales_order` CHANGE `is_dispatched` `is_dispatched` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Pending Dispatch, 1 = Dispatched, 2 = Canceled Order, 4 = Challan Created from Proforma Invoice';
ALTER TABLE `proforma_invoices` CHANGE `is_dispatched` `is_dispatched` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 = Pending Dispatch, 1 = Dispatched, 2 = Canceled, 3 = Challan Created from Sales Order';

-- Ishita : 2019_03_13 11:50 AM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('252', 'Allow to change Status : Cancel to Pending', '', '5.4.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '252');

-- Avinash : 2019_03_14 11:15 PM
ALTER TABLE `sales_order_items` CHANGE `sales_order_id` `sales_order_id` INT(11) NULL DEFAULT NULL, CHANGE `item_code` `item_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_name` `item_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_description` `item_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `add_description` `add_description` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `detail_description` `detail_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `drawing_number` `drawing_number` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `drawing_revision` `drawing_revision` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uom` `uom` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `total_amount` `total_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_note` `item_note` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `quantity` `quantity` DOUBLE NULL DEFAULT NULL, CHANGE `disc_per` `disc_per` DOUBLE NULL DEFAULT NULL, CHANGE `rate` `rate` DOUBLE NULL DEFAULT NULL, CHANGE `disc_value` `disc_value` DOUBLE NULL DEFAULT NULL, CHANGE `amount` `amount` DOUBLE NULL DEFAULT NULL, CHANGE `net_amount` `net_amount` DOUBLE NULL DEFAULT NULL, CHANGE `item_status` `item_status` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cust_part_no` `cust_part_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `company` CHANGE `name` `name` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address` `address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `gst_no` `gst_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pan_no` `pan_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `tan_no` `tan_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pf_no` `pf_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `esic_no` `esic_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pt_no` `pt_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `service_tax_no` `service_tax_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `vat_no` `vat_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cst_no` `cst_no` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `invoice_terms` `invoice_terms` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Avinash : 2019_03_15 12:00 PM
ALTER TABLE `sales_order` CHANGE `quatation_id` `quatation_id` INT(11) NULL DEFAULT NULL, CHANGE `contact_no` `contact_no` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cust_po_no` `cust_po_no` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email_id` `email_id` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `po_date` `po_date` DATE NULL DEFAULT NULL, CHANGE `more_items_data` `more_items_data` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `sales_order_logins` CHANGE `last_modified_by` `last_modified_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;

-- Avinash : 2019_03_19 10:50 AM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('253', 'Enquiry Allow edit after Quotation', '', '5.2.2');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '253');
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('254', 'Quotation Allow edit after Sales Order', '', '5.3.3');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'allow', 'allow', '254');

-- Ishita : 2019_03_19 05:50 PM
ALTER TABLE `contact_person` ADD INDEX(`party_id`);

-- Avinash : 2019_03_25 09:40 AM
ALTER TABLE `inquiry` CHANGE `party_id` `party_id` INT(11) NULL DEFAULT NULL COMMENT 'party_id', CHANGE `received_email` `received_email` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `received_fax` `received_fax` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `received_verbal` `received_verbal` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `send_email` `send_email` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `send_fax` `send_fax` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NULL DEFAULT NULL COMMENT 'user_id , staff_id', CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_by` `updated_by` INT(11) NULL DEFAULT NULL;

-- Avinash : 2019_03_25 12:10 PM
ALTER TABLE `party` ADD `current_party_staff_id` INT(11) NULL DEFAULT NULL AFTER `party_created_date`;
UPDATE `party` SET `current_party_staff_id` = `created_by`;

-- Avinash : 2019_03_25 06:05 PM
ALTER TABLE `proforma_invoices` CHANGE `agent_id` `agent_id` INT(11) NULL DEFAULT NULL, CHANGE `billing_terms_id` `billing_terms_id` INT(11) NULL DEFAULT NULL, CHANGE `contact_no` `contact_no` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cust_po_no` `cust_po_no` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email_id` `email_id` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `po_date` `po_date` DATE NULL DEFAULT NULL, CHANGE `port_of_loading_city` `port_of_loading_city` VARCHAR(55) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Avinash : 2019_04_06 06:05 PM
ALTER TABLE `purchase_invoice` CHANGE `supplier_id` `supplier_id` INT(11) NULL DEFAULT NULL COMMENT 'supplier_id', CHANGE `address` `address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NULL DEFAULT NULL COMMENT 'user_id , staff_id', CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `purchase_invoice_items` CHANGE `invoice_id` `invoice_id` INT(11) NULL DEFAULT NULL, CHANGE `item_id` `item_id` INT(11) NULL DEFAULT NULL, CHANGE `item_data` `item_data` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `purchase_items` CHANGE `item_category` `item_category` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `stock` `stock` INT(11) NULL DEFAULT NULL COMMENT 'Batch Wise Stock >> 0 = Yes, 1 = No.', CHANGE `document` `document` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `hsn_code` `hsn_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_group` `item_group` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_active` `item_active` INT(11) NULL DEFAULT NULL COMMENT '0- Active 1-Deactive', CHANGE `item_name` `item_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cfactor` `cfactor` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_mpt` `item_mpt` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_status` `item_status` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `image` `image` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `purchase_order` CHANGE `supplier_id` `supplier_id` INT(11) NULL DEFAULT NULL COMMENT 'supplier_id', CHANGE `address` `address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NULL DEFAULT NULL COMMENT 'user_id , staff_id', CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `purchase_order_items` CHANGE `order_id` `order_id` INT(11) NULL DEFAULT NULL, CHANGE `item_id` `item_id` INT(11) NULL DEFAULT NULL, CHANGE `item_data` `item_data` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;

-- Avinash : 2019_04_08 06:20 PM
ALTER TABLE `followup_order_history` ADD `reminder_category_id` INT(11) NULL DEFAULT NULL AFTER `order_id`, ADD `reminder_date` DATETIME NULL DEFAULT NULL AFTER `reminder_category_id`, ADD `reminder_message` TEXT NULL DEFAULT NULL AFTER `reminder_date`, ADD `followup_category_id` INT(11) NULL DEFAULT NULL AFTER `reminder_message`;
ALTER TABLE `followup_order_history` ADD `followup_at` DATETIME NULL DEFAULT NULL AFTER `followup_by`, ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `followup_at`, ADD `updated_at` DATETIME NULL DEFAULT NULL AFTER `updated_by`, ADD `assigned_to` TEXT NULL DEFAULT NULL AFTER `updated_at`, ADD `is_notified` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0=not_remind,1= remind ' AFTER `assigned_to`;

--
-- Avinash : 2019_04_09 10:40 AM
-- Table structure for table `raw_material_group`
--

CREATE TABLE `raw_material_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `group_desc` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `raw_material_group`
--
ALTER TABLE `raw_material_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `raw_material_group`
--
ALTER TABLE `raw_material_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `purchase_items` ADD `raw_material_group_id` INT(11) NULL DEFAULT NULL AFTER `image`;

-- Avinash : 2019_04_09 06:45 PM
ALTER TABLE `item_group_code` CHANGE `ig_code` `ig_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ig_description` `ig_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `item_category` CHANGE `item_category` `item_category` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `uom` CHANGE `uom` `uom` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `item_make` CHANGE `item_make` `item_make` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `material_process_type` CHANGE `material_process_type` `material_process_type` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `item_status` CHANGE `item_status` `item_status` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `challan_item_make` CHANGE `make_name` `make_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Pooja : 2019_04_09 08:02 PM
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '1');

-- Ishita : 2019_04_12 05:30 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES
(255, 'Master >> Item >> Raw Material Group', '', '3.2.33');

INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES
(NULL, 'view', 'view', '255'),
(NULL, 'add', 'add', '255'),
(NULL, 'edit', 'edit', '255'),
(NULL, 'delete', 'delete', '255');

-- Ishita : 2019_04_13 10:35 AM
ALTER TABLE `purchase_items` ADD `qty_2` INT(11) NULL DEFAULT NULL AFTER `reference_qty_uom`, ADD `uom_2` INT(11) NULL DEFAULT NULL AFTER `qty_2`;

ALTER TABLE `bom_item_details` ADD `uom_id` INT(11) NULL DEFAULT NULL AFTER `quantity`, ADD `loss` DOUBLE NULL DEFAULT NULL AFTER `uom_id`;

-- Ishita : 2019_04_13 03:50 PM
ALTER TABLE `staff` CHANGE `name` `name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `gender` `gender` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `married_status` `married_status` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `husband_wife_name` `husband_wife_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pass` `pass` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `emp_id` `emp_id` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `visitor_last_message_id` `visitor_last_message_id` INT(11) NULL DEFAULT NULL;

-- Ishita : 2019_04_12 07:30 PM
ALTER TABLE `staff` ADD `employee_code` INT(11) NULL DEFAULT NULL AFTER `socket_id`;

-- Ishita : 2019_04_13 04:33 PM
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '4');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '5');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '6');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '33');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '246');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '109');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '208');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_allow', 'export_allow', '111');

-- Ishita : 2019_04_15 10:16 AM
-- For Updated Employee Code in Staff
SET @rank:=100;
update `staff`
set `employee_code`=@rank:=@rank+1;

-- Ishita : 2019_04_15 06:10 PM
UPDATE `website_modules` SET `title` = 'Master >> Company Banks', `main_module` = '3.2' WHERE `website_modules`.`id` = 222;
UPDATE `website_modules` SET `main_module` = '3.3' WHERE `website_modules`.`id` = 9;
UPDATE `website_modules` SET `main_module` = '3.3.1' WHERE `website_modules`.`id` = 171;
UPDATE `website_modules` SET `main_module` = '3.3.2' WHERE `website_modules`.`id` = 197;
UPDATE `website_modules` SET `main_module` = '3.3.3' WHERE `website_modules`.`id` = 198;
UPDATE `website_modules` SET `main_module` = '3.3.4' WHERE `website_modules`.`id` = 172;
UPDATE `website_modules` SET `main_module` = '3.3.5' WHERE `website_modules`.`id` = 173;
UPDATE `website_modules` SET `main_module` = '3.3.6' WHERE `website_modules`.`id` = 255;
UPDATE `website_modules` SET `main_module` = '3.3.7' WHERE `website_modules`.`id` = 174;
UPDATE `website_modules` SET `main_module` = '3.3.8' WHERE `website_modules`.`id` = 175;
UPDATE `website_modules` SET `main_module` = '3.3.9' WHERE `website_modules`.`id` = 176;
UPDATE `website_modules` SET `main_module` = '3.3.10' WHERE `website_modules`.`id` = 177;
UPDATE `website_modules` SET `main_module` = '3.3.11' WHERE `website_modules`.`id` = 178;
UPDATE `website_modules` SET `main_module` = '3.3.12' WHERE `website_modules`.`id` = 179;
UPDATE `website_modules` SET `main_module` = '3.3.13' WHERE `website_modules`.`id` = 180;
UPDATE `website_modules` SET `main_module` = '3.3.14' WHERE `website_modules`.`id` = 181;
UPDATE `website_modules` SET `main_module` = '3.3.15' WHERE `website_modules`.`id` = 182;
UPDATE `website_modules` SET `main_module` = '3.3.16' WHERE `website_modules`.`id` = 183;
UPDATE `website_modules` SET `main_module` = '3.4' WHERE `website_modules`.`id` = 243;
UPDATE `website_modules` SET `main_module` = '3.5' WHERE `website_modules`.`id` = 10;
UPDATE `website_modules` SET `title` = 'Master >> Sales', `main_module` = '3.6' WHERE `website_modules`.`id` = 11;
UPDATE `website_modules` SET `main_module` = '3.6.1' WHERE `website_modules`.`id` = 38;
UPDATE `website_modules` SET `main_module` = '3.6.2' WHERE `website_modules`.`id` = 39;
UPDATE `website_modules` SET `main_module` = '3.6.3' WHERE `website_modules`.`id` = 40;
UPDATE `website_modules` SET `main_module` = '3.6.4' WHERE `website_modules`.`id` = 41;
UPDATE `website_modules` SET `main_module` = '3.6.5' WHERE `website_modules`.`id` = 42;
UPDATE `website_modules` SET `main_module` = '3.6.6' WHERE `website_modules`.`id` = 43;
UPDATE `website_modules` SET `main_module` = '3.6.7' WHERE `website_modules`.`id` = 44;
UPDATE `website_modules` SET `main_module` = '3.6.8' WHERE `website_modules`.`id` = 45;
UPDATE `website_modules` SET `main_module` = '3.6.9' WHERE `website_modules`.`id` = 46;
UPDATE `website_modules` SET `main_module` = '3.6.10' WHERE `website_modules`.`id` = 47;
UPDATE `website_modules` SET `main_module` = '3.6.11' WHERE `website_modules`.`id` = 48;
UPDATE `website_modules` SET `main_module` = '3.6.12' WHERE `website_modules`.`id` = 49;
UPDATE `website_modules` SET `main_module` = '3.6.13' WHERE `website_modules`.`id` = 50;
UPDATE `website_modules` SET `main_module` = '3.6.14' WHERE `website_modules`.`id` = 51;
UPDATE `website_modules` SET `main_module` = '3.6.15' WHERE `website_modules`.`id` = 52;
UPDATE `website_modules` SET `main_module` = '3.6.16' WHERE `website_modules`.`id` = 53;
UPDATE `website_modules` SET `main_module` = '3.6.17' WHERE `website_modules`.`id` = 192;
UPDATE `website_modules` SET `main_module` = '3.6.18' WHERE `website_modules`.`id` = 194;
UPDATE `website_modules` SET `main_module` = '3.6.19' WHERE `website_modules`.`id` = 195;
UPDATE `website_modules` SET `main_module` = '3.6.20' WHERE `website_modules`.`id` = 221;
UPDATE `website_modules` SET `title` = 'Master >> Challan', `main_module` = '3.7' WHERE `website_modules`.`id` = 209;
UPDATE `website_modules` SET `main_module` = '3.7.1' WHERE `website_modules`.`id` = 210;
UPDATE `website_modules` SET `main_module` = '3.7.2' WHERE `website_modules`.`id` = 211;
UPDATE `website_modules` SET `main_module` = '3.7.3' WHERE `website_modules`.`id` = 212;
UPDATE `website_modules` SET `main_module` = '3.7.4' WHERE `website_modules`.`id` = 213;
UPDATE `website_modules` SET `main_module` = '3.7.5' WHERE `website_modules`.`id` = 214;
UPDATE `website_modules` SET `main_module` = '3.7.6' WHERE `website_modules`.`id` = 215;
UPDATE `website_modules` SET `main_module` = '3.7.7' WHERE `website_modules`.`id` = 216;
UPDATE `website_modules` SET `main_module` = '3.7.8' WHERE `website_modules`.`id` = 217;
UPDATE `website_modules` SET `main_module` = '3.7.9' WHERE `website_modules`.`id` = 218;
UPDATE `website_modules` SET `main_module` = '3.7.10' WHERE `website_modules`.`id` = 219;
UPDATE `website_modules` SET `main_module` = '3.7.11' WHERE `website_modules`.`id` = 237;
UPDATE `website_modules` SET `main_module` = '3.8' WHERE `website_modules`.`id` = 54;
UPDATE `website_modules` SET `main_module` = '3.8.1' WHERE `website_modules`.`id` = 55;
UPDATE `website_modules` SET `main_module` = '3.8.2' WHERE `website_modules`.`id` = 56;
UPDATE `website_modules` SET `main_module` = '3.8.3' WHERE `website_modules`.`id` = 71;
UPDATE `website_modules` SET `main_module` = '3.9' WHERE `website_modules`.`id` = 57;
UPDATE `website_modules` SET `main_module` = '3.9.1' WHERE `website_modules`.`id` = 186;
UPDATE `website_modules` SET `main_module` = '3.9.2' WHERE `website_modules`.`id` = 58;
UPDATE `website_modules` SET `main_module` = '3.9.3' WHERE `website_modules`.`id` = 59;
UPDATE `website_modules` SET `main_module` = '3.9.4' WHERE `website_modules`.`id` = 60;
UPDATE `website_modules` SET `main_module` = '3.9.5' WHERE `website_modules`.`id` = 61;
UPDATE `website_modules` SET `main_module` = '3.10' WHERE `website_modules`.`id` = 62;
UPDATE `website_modules` SET `main_module` = '3.10.1' WHERE `website_modules`.`id` = 63;
UPDATE `website_modules` SET `main_module` = '3.10.2' WHERE `website_modules`.`id` = 64;
UPDATE `website_modules` SET `main_module` = '3.10.3' WHERE `website_modules`.`id` = 65;
UPDATE `website_modules` SET `main_module` = '3.10.4' WHERE `website_modules`.`id` = 66;
UPDATE `website_modules` SET `main_module` = '3.10.5' WHERE `website_modules`.`id` = 67;
UPDATE `website_modules` SET `main_module` = '3.10.6' WHERE `website_modules`.`id` = 68;
UPDATE `website_modules` SET `main_module` = '3.10.7' WHERE `website_modules`.`id` = 69;
UPDATE `website_modules` SET `main_module` = '3.10.8' WHERE `website_modules`.`id` = 70;
UPDATE `website_modules` SET `main_module` = '3.10.9' WHERE `website_modules`.`id` = 72;
UPDATE `website_modules` SET `main_module` = '3.10.10' WHERE `website_modules`.`id` = 224;
UPDATE `website_modules` SET `main_module` = '3.11' WHERE `website_modules`.`id` = 73;
UPDATE `website_modules` SET `main_module` = '3.12' WHERE `website_modules`.`id` = 74;
UPDATE `website_modules` SET `main_module` = '3.13' WHERE `website_modules`.`id` = 193;
UPDATE `website_modules` SET `main_module` = '3.14' WHERE `website_modules`.`id` = 225;
UPDATE `website_modules` SET `main_module` = '3.15' WHERE `website_modules`.`id` = 75;
UPDATE `website_modules` SET `main_module` = '3.16' WHERE `website_modules`.`id` = 189;
UPDATE `website_modules` SET `main_module` = '3.17' WHERE `website_modules`.`id` = 76;
UPDATE `website_modules` SET `main_module` = '5.5.4' WHERE `website_modules`.`id` = 107;
UPDATE `website_modules` SET `main_module` = '9.1' WHERE `website_modules`.`id` = 122;
UPDATE `website_modules` SET `main_module` = '9.2' WHERE `website_modules`.`id` = 123;
UPDATE `website_modules` SET `main_module` = '9.3' WHERE `website_modules`.`id` = 125;
UPDATE `website_modules` SET `main_module` = '9.4' WHERE `website_modules`.`id` = 126;
UPDATE `website_modules` SET `main_module` = '9.5' WHERE `website_modules`.`id` = 127;
UPDATE `website_modules` SET `main_module` = '9.6' WHERE `website_modules`.`id` = 128;
UPDATE `website_modules` SET `main_module` = '9.7' WHERE `website_modules`.`id` = 134;
UPDATE `website_modules` SET `main_module` = '9.8' WHERE `website_modules`.`id` = 135;
UPDATE `website_modules` SET `main_module` = '9.9' WHERE `website_modules`.`id` = 136;
UPDATE `website_modules` SET `main_module` = '9.10' WHERE `website_modules`.`id` = 124;
UPDATE `website_modules` SET `main_module` = '9.11' WHERE `website_modules`.`id` = 129;
UPDATE `website_modules` SET `main_module` = '9.12' WHERE `website_modules`.`id` = 132;
UPDATE `website_modules` SET `main_module` = '9.13' WHERE `website_modules`.`id` = 133;
UPDATE `website_modules` SET `main_module` = '9.14' WHERE `website_modules`.`id` = 137;
UPDATE `website_modules` SET `main_module` = '9.15' WHERE `website_modules`.`id` = 138;
UPDATE `website_modules` SET `main_module` = '9.16' WHERE `website_modules`.`id` = 119;
UPDATE `website_modules` SET `main_module` = '9.17' WHERE `website_modules`.`id` = 251;
UPDATE `website_modules` SET `main_module` = '9.18' WHERE `website_modules`.`id` = 31;
UPDATE `website_modules` SET `main_module` = '9.19' WHERE `website_modules`.`id` = 244;
UPDATE `website_modules` SET `main_module` = '9.20' WHERE `website_modules`.`id` = 245;

-- Ishita : 2019_04_16 02:00 PM
ALTER TABLE `purchase_items` CHANGE `qty_2` `required_qty` INT(11) NULL DEFAULT NULL;
ALTER TABLE `purchase_items` CHANGE `uom_2` `required_uom` INT(11) NULL DEFAULT NULL;

-- Ishita : 2019_04_16 04:00 PM
ALTER TABLE `bom_item_details` ADD `job_expense` DOUBLE NULL DEFAULT NULL AFTER `loss`;

-- Ishita : 2019_04_18 03:00 PM
UPDATE `website_modules` SET `title` = 'Production Start', `main_module` = '5.5.4' WHERE `website_modules`.`id` = 246;
UPDATE `website_modules` SET `main_module` = '5.5.3' WHERE `website_modules`.`id` = 107;

-- Ishita : 2019_05_06 04:26 PM
ALTER TABLE `items` CHANGE `item_category` `item_category` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_code1` `item_code1` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uom` `uom` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `qty` `qty` DOUBLE NULL DEFAULT NULL, CHANGE `item_type` `item_type` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `stock` `stock` INT(11) NULL DEFAULT NULL, CHANGE `document` `document` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `hsn_code` `hsn_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `rate` `rate` DOUBLE NULL DEFAULT NULL, CHANGE `item_group` `item_group` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_active` `item_active` INT(11) NULL DEFAULT NULL COMMENT '0- Active 1-Deactive', CHANGE `item_name` `item_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cfactor` `cfactor` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `conv_uom` `conv_uom` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_mpt` `item_mpt` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `item_status` `item_status` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `image` `image` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;

-- Ishita : 2019_05_08 02:00 PM
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 759;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 764;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 765;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 766;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 767;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 768;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 769;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 770;
UPDATE `module_roles` SET `title` = 'export_data', `role_name` = 'export_data' WHERE `module_roles`.`id` = 771;

-- Ishita : 2019_05_08 02:18 PM
ALTER TABLE `supplier` ADD `cin_no` VARCHAR(255) NULL DEFAULT NULL AFTER `contact_no`;

-- Ishita : 2019_05_13 03:12 PM
ALTER TABLE `purchase_project` ADD `item_make_id` VARCHAR(255) NULL DEFAULT NULL AFTER `item_group_id`;
ALTER TABLE `purchase_project` ADD `raw_material_group_id` VARCHAR(255) NULL DEFAULT NULL AFTER `item_make_id`;

--
-- Avinash : 2019_05_22 09:50 AM
-- Table structure for table `supplier_make`
--

CREATE TABLE `supplier_make` (
  `id` int(11) NOT NULL,
  `supplier_make_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `supplier_make`
--
ALTER TABLE `supplier_make`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `supplier_make`
--
ALTER TABLE `supplier_make`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `purchase_items` ADD `supplier_make_id` INT(11) NULL DEFAULT NULL AFTER `item_code`;

-- Avinash : 2019_05_22 11:30 AM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES
(256, 'Master >> Item >> Supplier Make', '', '3.3.6');

INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES
(NULL, 'view', 'view', '256'),
(NULL, 'add', 'add', '256'),
(NULL, 'edit', 'edit', '256'),
(NULL, 'delete', 'delete', '256');

-- Ishita : 2019_05_22 11:30 AM
ALTER TABLE  `purchase_project_items` ADD INDEX ( `item_id` ) ;
DELETE FROM `purchase_project_items` WHERE `purchase_project_items`.`item_id` NOT IN (SELECT `purchase_items`.`id` FROM `purchase_items`);
ALTER TABLE  `purchase_project_items` ADD CONSTRAINT  `FK_item_id` FOREIGN KEY ( `item_id` ) REFERENCES  `purchase_items` (`id`) ON DELETE RESTRICT;

ALTER TABLE  `bom_item_details` ADD INDEX ( `item_id` ) ;
DELETE FROM `bom_item_details` WHERE `bom_item_details`.`item_id` NOT IN (SELECT `purchase_items`.`id` FROM `purchase_items`);
ALTER TABLE  `bom_item_details` ADD CONSTRAINT  `FK_bom_item_id` FOREIGN KEY ( `item_id` ) REFERENCES  `purchase_items` (`id`) ON DELETE RESTRICT;

ALTER TABLE  `purchase_order_items` ADD INDEX ( `item_id` ) ;
DELETE FROM `purchase_order_items` WHERE `purchase_order_items`.`item_id` NOT IN (SELECT `purchase_items`.`id` FROM `purchase_items`);
ALTER TABLE  `purchase_order_items` ADD CONSTRAINT  `FK_purchase_order_item_id` FOREIGN KEY ( `item_id` ) REFERENCES  `purchase_items` (`id`) ON DELETE RESTRICT;

ALTER TABLE  `purchase_invoice_items` ADD INDEX ( `item_id` ) ;
DELETE FROM `purchase_invoice_items` WHERE `purchase_invoice_items`.`item_id` NOT IN (SELECT purchase_items.id FROM purchase_items);
ALTER TABLE  `purchase_invoice_items` ADD CONSTRAINT  `FK_purchase_invoice_item_id` FOREIGN KEY ( `item_id` ) REFERENCES  `purchase_items` (`id`) ON DELETE RESTRICT;

-- Ishita : 2019_05_23 02:60 AM
ALTER TABLE `purchase_project` ADD `supplier_make_id` INT(11) NULL DEFAULT NULL AFTER `raw_material_group_id`;

-- Avinash : 2019_06_11 06:30 PM
ALTER TABLE `installation` CHANGE `is_feedback_created` `is_feedback_created` TINYINT(1) NULL DEFAULT NULL COMMENT '0 = feedback not created, 1 = feedback created', CHANGE `is_complain_created` `is_complain_created` TINYINT(1) NULL DEFAULT NULL COMMENT '0 = complain not created, 1 = complain created';

-- Avinash : 2019_06_14 11:15 AM
ALTER TABLE `purchase_items` CHANGE `rate` `rate` DOUBLE NULL DEFAULT NULL;

-- Avinash : 2019_06_18 03:15 PM
ALTER TABLE `staff` ADD `intensive_per` DOUBLE NULL DEFAULT NULL AFTER `pf_employee`, ADD `esi_amount` DOUBLE NULL DEFAULT NULL AFTER `intensive_per`;

-- Avinash : 2019_06_19 06:40 PM
ALTER TABLE `purchase_items` CHANGE `reference_qty` `reference_qty` DOUBLE NULL DEFAULT NULL, CHANGE `required_qty` `required_qty` DOUBLE NULL DEFAULT NULL;

-- Ishita : 2019_06_22 02:04 PM
ALTER TABLE `purchase_invoice` ADD `debit_cash` TINYINT(1) NULL DEFAULT NULL COMMENT '1 = Debit, 2 = Cash' AFTER `invoice_date`;

-- Ishita : 2019_06_22 03:40 PM
ALTER TABLE `purchase_order_items` ADD `invoice_qty` DOUBLE NOT NULL DEFAULT '0' AFTER `quantity`;

-- Ishita : 2019_06_24 01:20 PM
ALTER TABLE `purchase_invoice` ADD `pf_amount` DOUBLE NULL DEFAULT NULL AFTER `note`, ADD `pf_cgst` DOUBLE NULL DEFAULT NULL AFTER `pf_amount`, ADD `pf_sgst` DOUBLE NULL DEFAULT NULL AFTER `pf_cgst`, ADD `pf_igst` DOUBLE NULL DEFAULT NULL AFTER `pf_sgst`, ADD `pf_amount_with_gst` DOUBLE NULL DEFAULT NULL AFTER `pf_igst`, ADD `t_amount` DOUBLE NULL DEFAULT NULL AFTER `pf_amount_with_gst`, ADD `t_cgst` DOUBLE NULL DEFAULT NULL AFTER `t_amount`, ADD `t_sgst` DOUBLE NULL DEFAULT NULL AFTER `t_cgst`, ADD `t_igst` DOUBLE NULL DEFAULT NULL AFTER `t_sgst`, ADD `t_amount_with_gst` DOUBLE NULL DEFAULT NULL AFTER `t_igst`, ADD `grand_total` DOUBLE NULL DEFAULT NULL AFTER `t_amount_with_gst`;

-- Avinash : 2019_06_25 06:00 PM
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'only_current_date_entry_allow', 'only_current_date_entry_allow', '6');

-- Ishita : 2019_06_26 05:08 PM
ALTER TABLE  `bom_projects` ADD INDEX ( `project_id` ) ;
DELETE FROM `bom_projects` WHERE `bom_projects`.`project_id` NOT IN (SELECT purchase_project.project_id FROM purchase_project);
ALTER TABLE  `bom_projects` ADD CONSTRAINT  `FK_bom_project_id` FOREIGN KEY ( `project_id` ) REFERENCES  `purchase_project` (`project_id`) ON DELETE RESTRICT;

ALTER TABLE  `purchase_invoice` ADD INDEX ( `order_id` ) ;
DELETE FROM `purchase_invoice` WHERE `purchase_invoice`.`order_id` NOT IN (SELECT `purchase_order`.`order_id` FROM `purchase_order`);
ALTER TABLE  `purchase_invoice` ADD CONSTRAINT  `FK_invoice_order_id` FOREIGN KEY ( `order_id` ) REFERENCES  `purchase_order` (`order_id`) ON DELETE RESTRICT;

--
-- Avinash : 2019_06_26 08:00 PM
-- Table structure for table `production_projects`
--

CREATE TABLE `production_projects` (
  `id` int(11) NOT NULL,
  `production_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_qty` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `production_projects`
--
ALTER TABLE `production_projects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `production_projects`
--
ALTER TABLE `production_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `production_item_expenses`
--

CREATE TABLE `production_item_expenses` (
  `id` int(11) NOT NULL,
  `production_id` int(11) DEFAULT NULL,
  `item_expense` varchar(255) DEFAULT NULL,
  `expense` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `production_item_expenses`
--
ALTER TABLE `production_item_expenses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `production_item_expenses`
--
ALTER TABLE `production_item_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `production_item_details`
--

CREATE TABLE `production_item_details` (
  `id` int(11) NOT NULL,
  `production_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `basic_qty` double DEFAULT NULL,
  `project_qty` double DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `loss` double DEFAULT NULL,
  `job_expense` double DEFAULT NULL,
  `inout_stock` tinyint(1) DEFAULT NULL COMMENT '1 = In Stock, 2 = Out of Stock',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `production_item_details`
--
ALTER TABLE `production_item_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`purchase_item_id`);

--
-- AUTO_INCREMENT for table `production_item_details`
--
ALTER TABLE `production_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Ishita : 2019_06_27 06:06 PM
ALTER TABLE `production` ADD `bom_id` INT(11) NULL DEFAULT NULL AFTER `item_id`;
ALTER TABLE  `production` ADD INDEX ( `bom_id` ) ;
DELETE FROM `production` WHERE `production`.`bom_id` NOT IN (SELECT bom_master.bom_id FROM bom_master);
ALTER TABLE `production` ADD CONSTRAINT  `FK_production_bom_id` FOREIGN KEY ( `bom_id` ) REFERENCES  `bom_master` (`bom_id`) ON DELETE RESTRICT;

-- Avinash : 2019_07_01 12:30 PM
UPDATE `module_roles` SET `title` = 'change_date_allow', `role_name` = 'change_date_allow' WHERE `module_roles`.`id` = 776;

-- Ishita : 2019_07_01 12:30 PM
-- Table structure for table `employee_wise_incentive`
--
CREATE TABLE `employee_wise_incentive` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `no_of_item` int(11) DEFAULT NULL,
  `incentive` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `employee_wise_incentive`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `employee_wise_incentive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Ishita : 2019_07_01 12:30 PM
ALTER TABLE `staff` ADD `pf_per` DOUBLE NULL DEFAULT NULL AFTER `education_allowance`;
ALTER TABLE `staff` ADD `esi_per` DOUBLE NULL DEFAULT NULL AFTER `intensive_per`;

-- Ishita : 2019_07_03 03:30 PM
ALTER TABLE `company` ADD `tds_per` DOUBLE NULL DEFAULT NULL AFTER `authorized_signatory_image`;

-- Ishita : 2019_07_03 05:50 PM
ALTER TABLE `staff` ADD `bank_acc_name` VARCHAR(255) NULL DEFAULT NULL AFTER `bank_acc_no`;
ALTER TABLE `staff` ADD `allow_esi` VARCHAR(10) NULL DEFAULT NULL AFTER `allow_pf`;

-- Ishita : 2019_07_04 07:15 PM
-- Table structure for table `employee_salary`
--

CREATE TABLE `employee_salary` (
  `employee_salary_id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `month` tinyint(2) DEFAULT NULL,
  `year` smallint(4) DEFAULT NULL,
  `actual_salary` double DEFAULT NULL,
  `basic_salary` double DEFAULT NULL,
  `ctc_pf` double DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `esi` double DEFAULT NULL,
  `pf` double DEFAULT NULL,
  `professional_tax` double DEFAULT NULL,
  `other_deduction` double DEFAULT NULL,
  `late_coming_charge` double DEFAULT NULL,
  `hra` double DEFAULT NULL,
  `conveyance` double DEFAULT NULL,
  `mediclaim` double DEFAULT NULL,
  `incentive` double DEFAULT NULL,
  `tds_on_incentive` double DEFAULT NULL,
  `net_pay` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `employee_salary`
  ADD PRIMARY KEY (`employee_salary_id`);

ALTER TABLE `employee_salary`
  MODIFY `employee_salary_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

-- Ishita : 2019_07_06 04:50 PM
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '126');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '127');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '134');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '135');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '136');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '124');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '132');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '138');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'export_data', 'export_data', '245');

-- Ishita : 2019_07_08 05:50 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('257', 'Report >> Item Costing Report', '', '9.18');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '257');

INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'manage', 'manage', '144');

-- Avinash : 2019_07_11 08:50 PM
DELETE FROM `module_roles` WHERE `module_roles`.`id` = 305;
DELETE FROM `website_modules` WHERE `website_modules`.`id` = 128;

-- Ishita : 2019_07_12 12:21 PM
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('258', 'Master >> Hr >> Employee Wise Incentive', '', '3.8.4');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '258');

-- Chirag : 2019_07_13 04:44 PM
ALTER TABLE `employee_salary` ADD `narration` TEXT NULL DEFAULT NULL AFTER `net_pay`;

-- Avinash : 2019_07_24 02:00 PM
ALTER TABLE `employee_salary` ADD `pay_mode` INT(11) NULL DEFAULT NULL AFTER `narration`;
INSERT INTO `website_modules` (`id`, `title`, `table_name`, `main_module`) VALUES ('259', 'Master >> Hr >> Payment Mode', '', '3.8.5');
INSERT INTO `module_roles` (`id`, `title`, `role_name`, `module_id`) VALUES (NULL, 'view', 'view', '259'), (NULL, 'add', 'add', '259'), (NULL, 'edit', 'edit', '259'), (NULL, 'delete', 'delete', '259');

-- Avinash : 2019_08_07 06:30 PM
ALTER TABLE `company` ADD `c_pf_per` DOUBLE NULL DEFAULT NULL AFTER `tds_per`;
ALTER TABLE `company` ADD `c_esi_per` DOUBLE NULL DEFAULT NULL AFTER `c_pf_per`;

-- Avinash : 2019_08_08 11:30 AM
ALTER TABLE `employee_salary` ADD `pf_per` DOUBLE NULL DEFAULT NULL AFTER `salary`;
ALTER TABLE `employee_salary` ADD `esi_per` DOUBLE NULL DEFAULT NULL AFTER `pf_per`;
ALTER TABLE `employee_salary` ADD `allow_pf` varchar(10) NULL DEFAULT NULL AFTER `salary`;
ALTER TABLE `employee_salary` ADD `allow_esi` varchar(10) NULL DEFAULT NULL AFTER `allow_pf`;

-- Avinash : 2019_08_09 10:15 AM
ALTER TABLE `purchase_order_items` CHANGE `quantity` `quantity` DOUBLE NOT NULL DEFAULT '0';
ALTER TABLE `purchase_invoice_items` CHANGE `quantity` `quantity` DOUBLE NOT NULL DEFAULT '0';

--
-- Avinash : 2019_11_08 : 12:25 PM
-- Table structure for table `letterpad_content`
--

CREATE TABLE `letterpad_content` (
  `id` int(11) NOT NULL,
  `module_label` varchar(255) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `header_logo` varchar(255) DEFAULT NULL,
  `footer_detail` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `letterpad_content`
--

INSERT INTO `letterpad_content` (`id`, `module_label`, `module_name`, `header_logo`, `footer_detail`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Quotation >> Letter pad Content', 'quotation_letter_pad_content', null, 'quotation_letter_pad_content', '1', '2019-11-08 12:25:00', '1', '2019-11-08 12:25:00');

--
-- Indexes for table `letterpad_content`
--
ALTER TABLE `letterpad_content`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `letterpad_content`
--
ALTER TABLE `letterpad_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

-- Shailesh : 2019_12_25 : 04:35 PM
ALTER TABLE `terms_and_conditions` 
ADD `sort_1` INT(11) NULL AFTER `id`, 
ADD `sort_2` INT(11) NULL AFTER `sort_1`;

UPDATE `terms_and_conditions` SET `sort_1` ='1',`sort_2`='1' WHERE `id`=1;
UPDATE `terms_and_conditions` SET `sort_1` ='1',`sort_2`='2' WHERE `id`=18;
UPDATE `terms_and_conditions` SET `sort_1` ='2',`sort_2`='1' WHERE `id`=2;
UPDATE `terms_and_conditions` SET `sort_1` ='3',`sort_2`='1' WHERE `id`=3;
UPDATE `terms_and_conditions` SET `sort_1` ='3',`sort_2`='2' WHERE `id`=5;
UPDATE `terms_and_conditions` SET `sort_1` ='3',`sort_2`='3' WHERE `id`=11;
UPDATE `terms_and_conditions` SET `sort_1` ='3',`sort_2`='4' WHERE `id`=12;
UPDATE `terms_and_conditions` SET `sort_1` ='3',`sort_2`='5' WHERE `id`=19;
UPDATE `terms_and_conditions` SET `sort_1` ='3',`sort_2`='6' WHERE `id`=28;
UPDATE `terms_and_conditions` SET `sort_1` ='3',`sort_2`='7' WHERE `id`=29;
UPDATE `terms_and_conditions` SET `sort_1` ='4',`sort_2`='1' WHERE `id`=6;
UPDATE `terms_and_conditions` SET `sort_1` ='4',`sort_2`='2' WHERE `id`=7;
UPDATE `terms_and_conditions` SET `sort_1` ='4',`sort_2`='3' WHERE `id`=13;
UPDATE `terms_and_conditions` SET `sort_1` ='4',`sort_2`='4' WHERE `id`=14;
UPDATE `terms_and_conditions` SET `sort_1` ='4',`sort_2`='5' WHERE `id`=30;
UPDATE `terms_and_conditions` SET `sort_1` ='4',`sort_2`='6' WHERE `id`=31;
UPDATE `terms_and_conditions` SET `sort_1` ='5',`sort_2`='1' WHERE `id`=10;
UPDATE `terms_and_conditions` SET `sort_1` ='5',`sort_2`='2' WHERE `id`=17;
UPDATE `terms_and_conditions` SET `sort_1` ='6',`sort_2`='1' WHERE `id`=9;
UPDATE `terms_and_conditions` SET `sort_1` ='6',`sort_2`='2' WHERE `id`=16;
UPDATE `terms_and_conditions` SET `sort_1` ='6',`sort_2`='3' WHERE `id`=21;
UPDATE `terms_and_conditions` SET `sort_1` ='6',`sort_2`='4' WHERE `id`=22;
UPDATE `terms_and_conditions` SET `sort_1` ='6',`sort_2`='5' WHERE `id`=25;
UPDATE `terms_and_conditions` SET `sort_1` ='6',`sort_2`='6' WHERE `id`=26;
UPDATE `terms_and_conditions` SET `sort_1` ='6',`sort_2`='7' WHERE `id`=27;
UPDATE `terms_and_conditions` SET `sort_1` ='7',`sort_2`='1' WHERE `id`=20;
UPDATE `terms_and_conditions` SET `sort_1` ='8',`sort_2`='1' WHERE `id`=23;
UPDATE `terms_and_conditions` SET `sort_1` ='9',`sort_2`='1' WHERE `id`=24;

--
-- Avinash : 2019_12_26 : 10:30 AM
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `settings_key` varchar(255) DEFAULT NULL,
  `settings_label` varchar(255) DEFAULT NULL,
  `settings_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `settings` (`settings_id`, `settings_key`, `settings_label`, `settings_value`) VALUES ('1', 'sidebar_logo', 'Sidebar Logo', NULL);
INSERT INTO `settings` (`settings_id`, `settings_key`, `settings_label`, `settings_value`) VALUES ('2', 'theme_color', 'Theme Color Picker', NULL);

-- Avinash : 2019_12_27 : 05:00 PM
ALTER TABLE `letterpad_content` ADD `header_logo_alignment` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1 = Left, 2 = Center, 3 = Right' AFTER `header_logo`;
UPDATE `letterpad_content` SET `module_label` = 'Domestic' WHERE `letterpad_content`.`id` = 1;
UPDATE `letterpad_content` SET `module_name` = 'domestic_letter_pad_content' WHERE `letterpad_content`.`id` = 1;

INSERT INTO `letterpad_content` (`id`, `module_label`, `module_name`, `header_logo`, `header_logo_alignment`, `footer_detail`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(2, 'Export', 'export_letter_pad_content', null, 2, null, 1, '2019-11-08 12:25:00', 1, '2019-12-27 17:18:29');

ALTER TABLE `letterpad_content` CHANGE `header_logo_alignment` `header_logo_alignment` VARCHAR(10) NOT NULL DEFAULT 'left';
UPDATE `letterpad_content` SET `header_logo_alignment` = 'right' WHERE `letterpad_content`.`id` = 1;
UPDATE `letterpad_content` SET `header_logo_alignment` = 'right' WHERE `letterpad_content`.`id` = 2;

-- Avinash : 2019_12_29 : 11:30 AM
ALTER TABLE `terms_and_conditions` ADD `module_name` VARCHAR(100) NULL DEFAULT NULL AFTER `sort_2`;
UPDATE `terms_and_conditions` SET `module_name`= 'Purchase Order' WHERE `sort_1` = 1;
UPDATE `terms_and_conditions` SET `module_name`= 'Purchase Invoice' WHERE `sort_1` = 2;
UPDATE `terms_and_conditions` SET `module_name`= 'Sales Order' WHERE `sort_1` = 3;
UPDATE `terms_and_conditions` SET `module_name`= 'Proforma Invoice' WHERE `sort_1` = 4;
UPDATE `terms_and_conditions` SET `module_name`= 'Challan' WHERE `sort_1` = 5;
UPDATE `terms_and_conditions` SET `module_name`= 'Sales Invoice' WHERE `sort_1` = 6;

UPDATE `terms_and_conditions` SET `sort_1` = '7', `sort_2` = '1' WHERE `terms_and_conditions`.`id` = 25;
UPDATE `terms_and_conditions` SET `sort_1` = '7', `sort_2` = '2' WHERE `terms_and_conditions`.`id` = 26;
UPDATE `terms_and_conditions` SET `sort_1` = '7', `sort_2` = '3' WHERE `terms_and_conditions`.`id` = 27;
UPDATE `terms_and_conditions` SET `sort_1` = '8' WHERE `terms_and_conditions`.`id` = 20;
UPDATE `terms_and_conditions` SET `sort_1` = '9' WHERE `terms_and_conditions`.`id` = 23;
UPDATE `terms_and_conditions` SET `sort_1` = '10' WHERE `terms_and_conditions`.`id` = 24;

UPDATE `terms_and_conditions` SET `module_name`= 'General Invoice' WHERE `sort_1` = 7;
UPDATE `terms_and_conditions` SET `module_name`= 'Payment' WHERE `sort_1` = 8;
UPDATE `terms_and_conditions` SET `module_name`= 'Testing Report' WHERE `sort_1` = 9;
UPDATE `terms_and_conditions` SET `module_name`= 'Installation' WHERE `sort_1` = 10;

UPDATE `terms_and_conditions` SET `label` = 'General Terms' WHERE `terms_and_conditions`.`id` = 1;
UPDATE `terms_and_conditions` SET `label` = 'Electrict Components Terms' WHERE `terms_and_conditions`.`id` = 18;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Payment Terms' WHERE `terms_and_conditions`.`id` = 3;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 5;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Payment Terms' WHERE `terms_and_conditions`.`id` = 11;
UPDATE `terms_and_conditions` SET `label` = 'Contract >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 12;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 19;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Cancel Order Letter' WHERE `terms_and_conditions`.`id` = 28;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Cancel Order Letter' WHERE `terms_and_conditions`.`id` = 29;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Payment Terms' WHERE `terms_and_conditions`.`id` = 6;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 7;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Payment Terms' WHERE `terms_and_conditions`.`id` = 13;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 14;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Cancel Order Letter' WHERE `terms_and_conditions`.`id` = 30;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Cancel Order Letter' WHERE `terms_and_conditions`.`id` = 31;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Declaration' WHERE `terms_and_conditions`.`id` = 10;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Declaration' WHERE `terms_and_conditions`.`id` = 17;
UPDATE `terms_and_conditions` SET `label` = 'Domestic >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 9;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 16;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Payment Terms' WHERE `terms_and_conditions`.`id` = 21;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Declaration' WHERE `terms_and_conditions`.`id` = 22;
UPDATE `terms_and_conditions` SET `label` = 'From Complain >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 25;
UPDATE `terms_and_conditions` SET `label` = 'From Party >> Terms And Conditions' WHERE `terms_and_conditions`.`id` = 26;
UPDATE `terms_and_conditions` SET `label` = 'Export >> Declaration' WHERE `terms_and_conditions`.`id` = 27;
UPDATE `terms_and_conditions` SET `label` = 'Declaration' WHERE `terms_and_conditions`.`id` = 20;
UPDATE `terms_and_conditions` SET `label` = 'Declaration' WHERE `terms_and_conditions`.`id` = 23;
UPDATE `terms_and_conditions` SET `label` = 'Declaration' WHERE `terms_and_conditions`.`id` = 24;

UPDATE `terms_and_conditions` SET `sort_2` = '1' WHERE `terms_and_conditions`.`id` = 5;
UPDATE `terms_and_conditions` SET `sort_2` = '2' WHERE `terms_and_conditions`.`id` = 19;
UPDATE `terms_and_conditions` SET `sort_2` = '3' WHERE `terms_and_conditions`.`id` = 12;
UPDATE `terms_and_conditions` SET `sort_2` = '4' WHERE `terms_and_conditions`.`id` = 3;
UPDATE `terms_and_conditions` SET `sort_2` = '5' WHERE `terms_and_conditions`.`id` = 11;
UPDATE `terms_and_conditions` SET `sort_2` = '1' WHERE `terms_and_conditions`.`id` = 7;
UPDATE `terms_and_conditions` SET `sort_2` = '2' WHERE `terms_and_conditions`.`id` = 14;
UPDATE `terms_and_conditions` SET `sort_2` = '3' WHERE `terms_and_conditions`.`id` = 6;
UPDATE `terms_and_conditions` SET `sort_2` = '4' WHERE `terms_and_conditions`.`id` = 13;

--
-- Avinash : 2020_10_12 : 06:25 PM
-- Table structure for table `twilio_webhook_demo`
--

CREATE TABLE `twilio_webhook_demo` (
  `id` int(11) NOT NULL,
  `webhook_type` varchar(255) DEFAULT NULL,
  `webhook_content` text CHARACTER SET utf8,
  `message_from` varchar(100) DEFAULT NULL,
  `message_to` varchar(100) DEFAULT NULL,
  `message_body` text CHARACTER SET utf8,
  `message_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `twilio_webhook_demo`
--
ALTER TABLE `twilio_webhook_demo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `twilio_webhook_demo`
--
ALTER TABLE `twilio_webhook_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Avinash : 2020_10_14 : 02:15 PM
ALTER TABLE `visitors` ADD `chat_through` INT(1) NOT NULL DEFAULT '1' COMMENT '1 = Chat Box, 2 = WhatsApp' AFTER `email`;
