DROP TRIGGER IF EXISTS `delete_item_category_log`;
DELIMITER //
CREATE TRIGGER `delete_item_category_log` AFTER DELETE ON `item_category`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.item_category_log
SET
item_category_id = old.id,
item_category = old.item_category,
operation_type = 'delete',
created_by = old.created_by,
updated_by = old.updated_by,
created_at = NOW()
//


DELIMITER ;
DROP TRIGGER IF EXISTS `insert_item_category_log`;
DELIMITER //
CREATE TRIGGER `insert_item_category_log` AFTER INSERT ON `item_category`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.item_category_log
SET
item_category_id = NEW.id,
item_category = NEW.item_category,
operation_type = 'insert',
created_by = NEW.created_by,
updated_by = NEW.updated_by,
created_at = NOW()
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_item_category_log`;
DELIMITER //
CREATE TRIGGER `update_item_category_log` AFTER UPDATE ON `item_category`
 FOR EACH ROW INSERT INTO jaykhodiyar.item_category_log
SET
item_category_id = NEW.id,
item_category = NEW.item_category,
operation_type = 'update',
created_by = NEW.created_by,
updated_by = NEW.updated_by,
created_at = NOW()
//
DELIMITER ;

/* trigger for log "party" table */
DROP TRIGGER IF EXISTS `add_party_log`;
DELIMITER //
CREATE TRIGGER `add_party_log` AFTER INSERT ON `party`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.party_log
SET
party_id = NEW.party_id,
party_code = NEW.party_code,
party_name = NEW.party_name,
reference_id=NEW.reference_id,
reference_description=NEW.reference_description,
party_type_1=NEW.party_type_1,
party_type_2=NEW.party_type_2,
branch_id=NEW.branch_id,
project=NEW.project,
address=NEW.address,
area=NEW.area,
pincode=NEW.pincode,
city_id=NEW.city_id,
state_id=NEW.state_id,
country_id=NEW.country_id,
phone_no=NEW.phone_no,
fax_no=NEW.fax_no,
email_id=NEW.email_id,
website=NEW.website,
opening_bal=NEW.opening_bal,
credit_limit=NEW.credit_limit,
active=NEW.active,
tin_vat_no=NEW.tin_vat_no,
tin_cst_no=NEW.tin_cst_no,
ecc_no=NEW.ecc_no,
pan_no=NEW.pan_no,
`range`=NEW.range,
division=NEW.division,
service_tax_no=NEW.service_tax_no,
utr_no=NEW.utr_no,
address_work=NEW.address_work,
w_address=NEW.w_address,
w_city=NEW.w_city,
w_state=NEW.w_state,
w_country=NEW.w_country,
oldw_pincode=NEW.oldw_pincode,
w_email=NEW.w_email,
w_web=NEW.w_web,
w_phone1=NEW.w_phone1,
w_phone2=NEW.w_phone2,
w_phone3=NEW.w_phone3,
w_note=NEW.w_note,
oldPartyType=NEW.oldPartyType,
oldHoliday=NEW.oldHoliday,
oldST_No=NEW.oldST_No,
oldVendorCode=NEW.oldVendorCode,
oldw_fax=NEW.oldw_fax,
oldacct_no=NEW.oldacct_no,
oldReason=NEW.oldReason,
oldAllowMultipalBill=NEW.oldAllowMultipalBill,
oldAddCity=NEW.oldAddCity,
oldCreatedBy=NEW.oldCreatedBy,
oldCreateDate=NEW.oldCreateDate,
oldModifyBy=NEW.oldModifyBy,
oldModiDate=NEW.oldModiDate,
oldPrintFlag=NEW.oldPrintFlag,
oldPT=NEW.oldPT,
oldCPerson=NEW.oldCPerson,
oldOpType=NEW.oldOpType,
oldCompProfile=NEW.oldCompProfile,
oldBranchCode=NEW.oldBranchCode,
oldAreaName=NEW.oldAreaName,
oldCreditLimit=NEW.oldCreditLimit,
oldAcctGrpCode=NEW.oldAcctGrpCode,
oldOpBalDr=NEW.oldOpBalDr,
oldApprovedBy=NEW.oldApprovedBy,
oldApprovedDate=NEW.oldApprovedDate,
oldPStage=NEW.oldPStage,
created_by=NEW.created_by,
operation_type='insert',
created_at=NOW()
//
DELIMITER ;

DROP TRIGGER IF EXISTS `delete_party_log`;
DELIMITER //
CREATE TRIGGER `delete_party_log` AFTER DELETE ON `party`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.party_log
SET
party_id = OLD.party_id,
party_code = OLD.party_code,
party_name = OLD.party_name,
reference_id=OLD.reference_id,
reference_description=OLD.reference_description,
party_type_1=OLD.party_type_1,
party_type_2=OLD.party_type_2,
branch_id=OLD.branch_id,
project=OLD.project,
address=OLD.address,
area=OLD.area,
pincode=OLD.pincode,
city_id=OLD.city_id,
state_id=OLD.state_id,
country_id=OLD.country_id,
phone_no=OLD.phone_no,
fax_no=OLD.fax_no,
email_id=OLD.email_id,
website=OLD.website,
opening_bal=OLD.opening_bal,
credit_limit=OLD.credit_limit,
active=OLD.active,
tin_vat_no=OLD.tin_vat_no,
tin_cst_no=OLD.tin_cst_no,
ecc_no=OLD.ecc_no,
pan_no=OLD.pan_no,
`range`=OLD.range,
division=OLD.division,
service_tax_no=OLD.service_tax_no,
utr_no=OLD.utr_no,
address_work=OLD.address_work,
w_address=OLD.w_address,
w_city=OLD.w_city,
w_state=OLD.w_state,
w_country=OLD.w_country,
oldw_pincode=OLD.oldw_pincode,
w_email=OLD.w_email,
w_web=OLD.w_web,
w_phone1=OLD.w_phone1,
w_phone2=OLD.w_phone2,
w_phone3=OLD.w_phone3,
w_note=OLD.w_note,
oldPartyType=OLD.oldPartyType,
oldHoliday=OLD.oldHoliday,
oldST_No=OLD.oldST_No,
oldVendorCode=OLD.oldVendorCode,
oldw_fax=OLD.oldw_fax,
oldacct_no=OLD.oldacct_no,
oldReason=OLD.oldReason,
oldAllowMultipalBill=OLD.oldAllowMultipalBill,
oldAddCity=OLD.oldAddCity,
oldCreatedBy=OLD.oldCreatedBy,
oldCreateDate=OLD.oldCreateDate,
oldModifyBy=OLD.oldModifyBy,
oldModiDate=OLD.oldModiDate,
oldPrintFlag=OLD.oldPrintFlag,
oldPT=OLD.oldPT,
oldCPerson=OLD.oldCPerson,
oldOpType=OLD.oldOpType,
oldCompProfile=OLD.oldCompProfile,
oldBranchCode=OLD.oldBranchCode,
oldAreaName=OLD.oldAreaName,
oldCreditLimit=OLD.oldCreditLimit,
oldAcctGrpCode=OLD.oldAcctGrpCode,
oldOpBalDr=OLD.oldOpBalDr,
oldApprovedBy=OLD.oldApprovedBy,
oldApprovedDate=OLD.oldApprovedDate,
oldPStage=OLD.oldPStage,
created_by=OLD.created_by,
operation_type='delete',
created_at=NOW()
//
DELIMITER ;

DROP TRIGGER IF EXISTS `edit_party_log`;
DELIMITER //
CREATE TRIGGER `edit_party_log` AFTER UPDATE ON `party`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.party_log
SET
party_id = NEW.party_id,
party_code = NEW.party_code,
party_name = NEW.party_name,
reference_id=NEW.reference_id,
reference_description=NEW.reference_description,
party_type_1=NEW.party_type_1,
party_type_2=NEW.party_type_2,
branch_id=NEW.branch_id,
project=NEW.project,
address=NEW.address,
area=NEW.area,
pincode=NEW.pincode,
city_id=NEW.city_id,
state_id=NEW.state_id,
country_id=NEW.country_id,
phone_no=NEW.phone_no,
fax_no=NEW.fax_no,
email_id=NEW.email_id,
website=NEW.website,
opening_bal=NEW.opening_bal,
credit_limit=NEW.credit_limit,
active=NEW.active,
tin_vat_no=NEW.tin_vat_no,
tin_cst_no=NEW.tin_cst_no,
ecc_no=NEW.ecc_no,
pan_no=NEW.pan_no,
`range`=NEW.range,
division=NEW.division,
service_tax_no=NEW.service_tax_no,
utr_no=NEW.utr_no,
address_work=NEW.address_work,
w_address=NEW.w_address,
w_city=NEW.w_city,
w_state=NEW.w_state,
w_country=NEW.w_country,
oldw_pincode=NEW.oldw_pincode,
w_email=NEW.w_email,
w_web=NEW.w_web,
w_phone1=NEW.w_phone1,
w_phone2=NEW.w_phone2,
w_phone3=NEW.w_phone3,
w_note=NEW.w_note,
oldPartyType=NEW.oldPartyType,
oldHoliday=NEW.oldHoliday,
oldST_No=NEW.oldST_No,
oldVendorCode=NEW.oldVendorCode,
oldw_fax=NEW.oldw_fax,
oldacct_no=NEW.oldacct_no,
oldReason=NEW.oldReason,
oldAllowMultipalBill=NEW.oldAllowMultipalBill,
oldAddCity=NEW.oldAddCity,
oldCreatedBy=NEW.oldCreatedBy,
oldCreateDate=NEW.oldCreateDate,
oldModifyBy=NEW.oldModifyBy,
oldModiDate=NEW.oldModiDate,
oldPrintFlag=NEW.oldPrintFlag,
oldPT=NEW.oldPT,
oldCPerson=NEW.oldCPerson,
oldOpType=NEW.oldOpType,
oldCompProfile=NEW.oldCompProfile,
oldBranchCode=NEW.oldBranchCode,
oldAreaName=NEW.oldAreaName,
oldCreditLimit=NEW.oldCreditLimit,
oldAcctGrpCode=NEW.oldAcctGrpCode,
oldOpBalDr=NEW.oldOpBalDr,
oldApprovedBy=NEW.oldApprovedBy,
oldApprovedDate=NEW.oldApprovedDate,
oldPStage=NEW.oldPStage,
created_by=NEW.created_by,
operation_type='update',
created_at=NOW()
//
DELIMITER ;

/* trigger for log "contact_person" table */

DROP TRIGGER IF EXISTS `delete_contact_person_log`;
DELIMITER //
CREATE TRIGGER `delete_contact_person_log` AFTER DELETE ON `contact_person`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.contact_person_log
SET
contact_person_id = OLD.contact_person_id,
party_id = OLD.party_id,
name = OLD.name,
phone_no = OLD.phone_no,
mobile_no = OLD.mobile_no,
fax_no = OLD.fax_no,
email = OLD.email,
designation_id = OLD.designation_id,
department_id = OLD.department_id,
note = OLD.note,
created_by = OLD.updated_by,
created_at = NOW(),
operation_type = 'delete'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `insert_contact_person_log`;
DELIMITER //
CREATE TRIGGER `insert_contact_person_log` AFTER INSERT ON `contact_person`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.contact_person_log
SET
contact_person_id = NEW.contact_person_id,
party_id = NEW.party_id,
name = NEW.name,
phone_no = NEW.phone_no,
mobile_no = NEW.mobile_no,
fax_no = NEW.fax_no,
email = NEW.email,
designation_id = NEW.designation_id,
department_id = NEW.department_id,
note = NEW.note,
created_by = NEW.created_by,
created_at = NOW(),
operation_type = 'insert'
//
DELIMITER ;

DROP TRIGGER IF EXISTS `update_contact_person_log`;
DELIMITER //
CREATE TRIGGER `update_contact_person_log` AFTER UPDATE ON `contact_person`
 FOR EACH ROW INSERT INTO
	jaykhodiyar.contact_person_log
SET
contact_person_id = NEW.contact_person_id,
party_id = NEW.party_id,
name = NEW.name,
phone_no = NEW.phone_no,
mobile_no = NEW.mobile_no,
fax_no = NEW.fax_no,
email = NEW.email,
designation_id = NEW.designation_id,
department_id = NEW.department_id,
note = NEW.note,
created_by = NEW.updated_by,
created_at = NOW(),
operation_type = 'update'
//
DELIMITER ;