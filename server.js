var socket  = require( 'socket.io' );
var express = require('express');
var app     = express();
var server  = require('http').createServer(app);
var io      = socket.listen( server );
var port    = process.env.PORT || 3000;

const twilio_accountSid = 'ACf264cbf611e68202fffa1075d7c1b3d7'; // omviravinash twilio account
const twilio_authToken = 'f1f31a7d2d7befec11aac888497f7fbd';
const twilio_client = require('twilio')(twilio_accountSid, twilio_authToken);
const twilio_MessagingResponse = require('twilio').twiml.MessagingResponse;

var mysql = require('mysql');
var connection = {
  host: "localhost",
  user: "root",
  password: "root",
  database: "jaykhodiyar"
};

var con_mysql;

function handleDisconnect() {
  con_mysql = mysql.createConnection(connection); 
  con_mysql.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
     
  con_mysql.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      handleDisconnect();
    } else {
        var indianTimeZoneVal = new Date().toLocaleString('en-US', {timeZone: 'Asia/Calcutta'});
        var currentdate = new Date(indianTimeZoneVal);
        var datetime = currentdate.getFullYear() + "-"
                + (currentdate.getMonth() + 1) + "-"
                + currentdate.getDate() + " "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();
        console.log(datetime);
      throw err;
    }
  });
}

handleDisconnect();

//when_a_twilio_sendbox_message_comes();

server.listen(port, function () {
	console.log('Server listening at port %d', port);
        var indianTimeZoneVal = new Date().toLocaleString('en-US', {timeZone: 'Asia/Calcutta'});
        var currentdate = new Date(indianTimeZoneVal);
        var datetime = currentdate.getFullYear() + "-"
            + (currentdate.getMonth() + 1) + "-"
            + currentdate.getDate() + " "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
        console.log(datetime);
        get_reminder();
});

    function get_reminder(){
        setTimeout(function () {
            console.log('timeout completed'); 
            get_reminder_data();
        }, 100);
    }
 
    function get_reminder_data(){
        var indianTimeZoneVal = new Date().toLocaleString('en-US', {timeZone: 'Asia/Calcutta'});
            var currentdate = new Date(indianTimeZoneVal);
            var current_datetime = currentdate.getFullYear() + "-"
                + (currentdate.getMonth() + 1) + "-"
                + currentdate.getDate() + " "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();
//            console.log(current_datetime);
        var indianTimeZoneVal = new Date().toLocaleString('en-US', {timeZone: 'Asia/Calcutta'});
            var currentdate = new Date(indianTimeZoneVal);
            var pre_datetime = currentdate.getFullYear() + "-"
                + (currentdate.getMonth() + 1) + "-"
                + currentdate.getDate() + " "
                + currentdate.getHours() + ":"
                + (currentdate.getMinutes() - 1) + ":"
                + currentdate.getSeconds();
//            console.log(pre_datetime);

        var sql = "SELECT r.*, s.socket_id, s.name as assign_to_name, assigner.name as assign_by FROM reminder r LEFT JOIN staff s ON s.staff_id = r.assigned_to LEFT JOIN staff assigner ON assigner.staff_id = r.user_id WHERE r.reminder_date >= '"+pre_datetime+"' AND r.reminder_date <= '"+current_datetime+"' GROUP BY r.assigned_to ";
        con_mysql.query(sql, function (err, result) {
            if (err)
                throw err;
            if (result != '') {
                result.forEach(function (remi_data) {
                    io.to(remi_data.socket_id).emit('reminder_notify', { });
                });
            }
        });
        var quo_sql = "SELECT fh.assigned_to AS ids, p.current_party_staff_id  FROM followup_history fh LEFT JOIN quotations qo ON qo.id = fh.quotation_id LEFT JOIN party p ON p.party_id = qo.party_id WHERE fh.reminder_date >= '"+pre_datetime+"' AND fh.reminder_date <= '"+current_datetime+"' ";
        ids_arr = [];
        con_mysql.query(quo_sql, function (err, quo_result) {
            if (err)
                throw err;
            if (quo_result != '') {
    //                console.log(quo_result);
                quo_result.forEach(function (remi_data) {
                    var ids = remi_data.ids.split(',');
                    for (var i = 0; i < ids.length; i++) {
                        ids_arr.push(ids[i]);
                    }
                });
                quo_result.forEach(function (o_data) {
                    ids_arr.push(o_data.current_party_staff_id);
                });
                id_uniqueArray = ids_arr.filter(function (item, pos) {
                    return ids_arr.indexOf(item) == pos;
                });
                var filtered = id_uniqueArray.filter(function (el) {
                    return el != '';
                });
                var arrayOfNumbers = filtered.map(Number);
                id_uniqueArray = arrayOfNumbers.filter(function (item, pos) {
                    return arrayOfNumbers.indexOf(item) == pos;
                });
                var staff_ids = id_uniqueArray.toString();
//                console.log(staff_ids);
                var staff_sql = "SELECT staff_id,socket_id FROM staff WHERE staff_id IN (" + staff_ids + ") ";
                con_mysql.query(staff_sql, function (err, staff_result) {
                    if (err)
                        throw err;
                    if (staff_result != '') {
                        staff_result.forEach(function (remi_data) {
                            io.to(remi_data.socket_id).emit('reminder_notify', {});
                        });
                    }
                });
            }
        });
        var order_sql = "SELECT fh.assigned_to AS ids, p.current_party_staff_id  FROM followup_order_history fh LEFT JOIN sales_order so ON so.id = fh.order_id LEFT JOIN party p ON p.party_id = so.sales_to_party_id WHERE fh.reminder_date >= '"+pre_datetime+"' AND fh.reminder_date <= '"+current_datetime+"' ";
        ids_arr = [];
        con_mysql.query(order_sql, function (err, order_result) {
            if (err)
                throw err;
            if (order_result != '') {
                //                console.log(order_result);
                order_result.forEach(function (remi_data) {
                    var idss = remi_data.ids.split(',');
                    for (var i = 0; i < idss.length; i++) {
                        ids_arr.push(idss[i]);
                    }
                });
                order_result.forEach(function (o_data) {
                    ids_arr.push(o_data.current_party_staff_id);
                });
                id_uniqueArray = ids_arr.filter(function (item, pos) {
                    return ids_arr.indexOf(item) == pos;
                });
                var filteredd = id_uniqueArray.filter(function (el) {
                    return el != '';
                });
                var arrayOfNumbers = filteredd.map(Number);
                id_uniqueArray = arrayOfNumbers.filter(function (item, pos) {
                    return arrayOfNumbers.indexOf(item) == pos;
                });
                var staff_ids = id_uniqueArray.toString();
//                console.log(staff_ids);
                var staff_sql = "SELECT staff_id,socket_id FROM staff WHERE staff_id IN (" + staff_ids + ") ";
                con_mysql.query(staff_sql, function (err, staff_result) {
                    if (err)
                        throw err;
                    if (staff_result != '') {
                        staff_result.forEach(function (remi_data) {
                            io.to(remi_data.socket_id).emit('reminder_notify', {});
                        });
                    }
                });
            }
        });
        setTimeout(function () {
            get_reminder_data();
        }, 60000);
    }

io.on('connection', function (socket) {

	socket.on( 'ping1', function( data ) {
		io.sockets.emit( 'ping1', { 
			beat: data.beat,
			sender_id: data.sender_id
		});
	});

	socket.on( 'pong1', function( data ) {
		io.sockets.emit( 'pong1', { 
			beat: data.beat
		});
	});

	socket.on( 'new_count_message', function( data ) {
		io.sockets.emit( 'new_count_message', { 
			new_count_message: data.new_count_message
		});
	});


	socket.on( 'update_count_message', function( data ) {
		io.sockets.emit( 'update_count_message', {
			update_count_message: data.update_count_message 
		});
	});

	socket.on( 'new_message', function( data ) {
		io.sockets.emit( 'new_message', {
			name: data.name,
			email: data.email,
			subject: data.subject,
			created_at: data.created_at,
			id: data.id
		});
	});

	/*
    * Admin to Admin chat
    * */
	socket.on( 'new_message_client', function( data ) {
		io.sockets.emit( 'new_message_client', {
			to_id: data.to_id,
			to_table: data.to_table,
			to_name: data.to_name,
			to_image: data.to_image,
			from_id: data.from_id,
			from_table: data.from_table,
			from_name: data.from_name,
			from_image: data.from_image,
			msg: data.msg,
			date_time: data.date_time
		});
	});

	/*
    * Visitor to Admin chat
    * */
	socket.on( 'new_message_visitor_to_client', function( data ) {
		io.sockets.emit( 'new_message_visitor_to_client', {
			to_id: data.to_id,
			from_id: data.from_id,
			from_session_id: data.from_session_id,
			from_table: data.from_table,
			from_name: data.from_name,
			from_image: data.from_image,
			msg: data.msg,
			date_time: data.date_time,
			chat_html: data.chat_html
		});
	});

	/*
    * Visitor Update Details
    * */
	socket.on( 'add_visitor_data', function( data ) {
		io.sockets.emit( 'add_visitor_data', {
			name: data.name,
			phone: data.phone,
			email: data.email,
			ip: data.ip,
			currency: data.currency,
			country: data.country,
			city: data.city,
			others: data.others,
			session_id: data.session_id,
			date: data.date,
		});
	});

	/*
    * Admin to Visitor chat
    * */
	socket.on( 'new_message_client_to_visitor', function( data ) {
		io.sockets.emit( 'new_message_client_to_visitor', {
			to_session_id: data.to_session_id,
			from_id: data.from_id,
			to_name: data.to_name,
			to_image: data.to_image,
			other_agent_chat_html: data.other_agent_chat_html,
			chat_html: data.chat_html,
			date_time: data.date_time,
			msg: data.msg,
			to_id: data.to_id
		});
	});

	/*
    * Admin to Admin file send
    * */
	socket.on( 'new_file_client_to_client', function( data ) {
		io.sockets.emit( 'new_file_client_to_client', {
			from_id: data.from_id,
			from_table: data.from_table,
			from_name: data.from_name,
			from_image: data.from_image,
			to_id: data.to_id,
			to_table: data.to_table,
			to_name: data.to_name,
			to_image: data.to_image,
			file: data.file,
			date_time: data.date_time
		});
	});

	/*
    * Visitor to Admin file send
    * */
	socket.on( 'new_file_visitor_to_client', function( data ) {
		io.sockets.emit( 'new_file_visitor_to_client', {
			from_id: data.from_id,
			from_session_id: data.from_session_id,
			from_table: data.from_table,
			from_name: data.from_name,
			from_image: data.from_image,
			to_id: data.to_id,
			to_table: data.to_table,
			to_name: data.to_name,
			file: data.file,
			date_time: data.date_time
		});
	});

	/*
    * Admin to Visitor file send
    * */
	socket.on( 'new_file_client_to_visitor', function( data ) {
		io.sockets.emit( 'new_file_client_to_visitor', {
			to_session_id: data.to_session_id,
			from_id: data.from_id,
			to_name: data.to_name,
			to_image: data.to_image,
			other_agent_chat_html: data.other_agent_chat_html,
			chat_html: data.chat_html,
			date_time: data.date_time,
			file: data.file
		});
	});


	/*
    * Staff to notify when new mail received by running cron in background
    * */
	socket.on( 'cron_mail_notification', function( data ) {
		io.sockets.emit( 'cron_mail_notification', {
			staff_id: data.staff_id,
			count_mail: data.count_mail    	
		});
	});

	/*
    * new visitor come to site, open that visitor chat popup in admin side
    * */
	socket.on( 'new_visitor_come', function( data ) {
		io.sockets.emit( 'new_visitor_come', {
			session_id: data.session_id,
			visitor_name: data.visitor_name    	
		});
	});
	
	/*
   * Added by Jignesh for close other agnets popoup after send msg 
   * */
	socket.on( 'close_visitor_popup', function( data ) {
		io.sockets.emit( 'close_visitor_popup', {
			to_session_id: data.to_session_id,
			from_id: data.from_id    	
		});
	});

	/*
   * Added by Naitik for reminder chron which runs manually
   * */
	socket.on( 'reminder_to_user', function( data ) {
		io.sockets.emit( 'reminder_to_user', {
			user_id: data.user_id,
			reminder: data.reminder,
			reminder_id: data.reminder_id
		});
	});


	/*
    * Notify Sales Order For Approve
    * */
	socket.on( 'notify_sales_order_for_approve', function( data ) {
		io.sockets.emit( 'notify_sales_order_for_approve', {});
	});

	/*
    * Notify Quotation Reminder
    * */
	socket.on( 'notify_quotation_reminder', function( data ) {
		io.sockets.emit( 'notify_quotation_reminder', {
			user_id: data.user_id,
			message: data.message,
			followup_by: data.followup_by,
			quotation_no: data.quotation_no,
			party_name: data.party_name,
			fh_id: data.fh_id
		});
	});
    
    /*
    * Transfer Chat to other
    * */
	socket.on( 'transfer_chat_to_other', function( data ) {
		io.sockets.emit( 'transfer_chat_to_other', {
			from_staff_id: data.from_staff_id,
			from_staff_name: data.from_staff_name,
			to_staff_id: data.to_staff_id,
			visitor_session_id: data.visitor_session_id,
			visitor_name: data.visitor_name,
		});
	});
	socket.on( 'transfer_chat_to_other_response', function( data ) {
		io.sockets.emit( 'transfer_chat_to_other_response', {
			from_staff_id: data.from_staff_id,
			to_staff_name: data.to_staff_name,
			to_staff_id: data.to_staff_id,
			visitor_session_id: data.visitor_session_id,
			visitor_name: data.visitor_name,
			response: data.response,
		});
	});

    socket.on('disconnect', function() {
        
        var sql = "SELECT * FROM `visitors` WHERE `socket_id` = '"+ socket.id + "'";
        con_mysql.query(sql, function (err, result) {
            if (err)
                throw err;
            if(result != ''){
//                console.log(result[0].session_id);
                io.sockets.emit( 'visitor_is_left', {
                    name: result[0].name,
                    session_id: result[0].session_id,
                });
                
                var sql = "UPDATE `visitors` SET `is_login` = '0' WHERE `socket_id` = '"+ socket.id + "'";
                con_mysql.query(sql, function (err, result) {
                    if (err)
                        throw err;
//                    console.log(result.affectedRows + " record(s) updated");
                });
                
                var request = require('request');
                var curl = require('curlrequest');
                curl.request({ url: 'http://localhost/jaykhodiyar/frontend/send_chat_mail_on_visitor_left', data: {id:result[0].id}}, function (err, stdout, meta) {
                });
                
            } else {
                
                var sql = "SELECT * FROM `staff` WHERE `socket_id` = '"+ socket.id + "'";
                con_mysql.query(sql, function (err, result) {
                    if (err)
                        throw err;
                    if(result != ''){
        //                console.log(result[0].session_id);
                        io.sockets.emit( 'staff_is_left', {
                            name: result[0].name,
                            staff_id: result[0].staff_id,
                        });
                        
                        var sql = "UPDATE `staff` SET `is_login` = '0' WHERE `socket_id` = '"+ socket.id + "'";
                        con_mysql.query(sql, function (err, result) {
                            if (err)
                                throw err;
        //                    console.log(result.affectedRows + " record(s) updated");
                        });
                        
                        var request = require('request');
                        var curl = require('curlrequest');
                        curl.request({ url: 'http://localhost/jaykhodiyar/frontend/send_chat_mail_on_admin_left', data: {staff_id:result[0].staff_id}}, function (err, stdout, meta) {
                        });
                        
                    }
                });
            }
        });
        
    });
    
    socket.on( 'staff_is_back', function( data ) {
        io.sockets.emit( 'staff_is_back', {
            name: data.name,
            staff_id: data.staff_id,
        });
    });
    
    socket.on( 'visitor_is_back', function( data ) {
        io.sockets.emit( 'visitor_is_back', {
            name: data.name,
            session_id: data.session_id,
        });
    });

    const publicIp = require('public-ip');
    publicIp.v4().then(ip => {
        io.sockets.emit( 'visitor_public_ip', {
            ip: ip,
        });
    });

    socket.on( 'send_custom_twilio_whatsapp_message', function( data ) {
        twilio_client.messages 
            .create({ 
               body: data.msg_text, 
               from: 'whatsapp:' + data.from_number,       
               to: 'whatsapp:' + data.to_number 
             }) 
            .then(
                message => 
                io.sockets.emit( 'send_custom_twilio_whatsapp_message', {
                    conversation_radio: data.to_number,
                    response: message,
                })
            ) 
            .done();
	});
    
    socket.on( 'get_twilio_whatsapp_conversation_list', function( data ) {
        twilio_client.conversations.conversations
        .list({limit: 20})
        .then(
            conversations => 
            io.sockets.emit( 'get_twilio_whatsapp_conversation_list', {
                response: conversations,
            })
        );
	});
    
    socket.on( 'get_twilio_messages_in_conversation', function( data ) {
        if(data.from_number == ''){
            twilio_client.messages 
                .list() 
                .then(
                    message => 
                    io.sockets.emit( 'get_twilio_messages_in_conversation', {
                        from_number: data.from_number,
                        response: message,
                    })
                ) 
                .done();
        } else {
            twilio_client.messages 
                .list({limit: 20}) 
                .then(
                    message => 
                    io.sockets.emit( 'get_twilio_messages_in_conversation', {
                        from_number: data.from_number,
                        response: message,
                    })
                ) 
                .done();
        }
	});
    
    socket.on( 'get_twilio_messages_media', function( data ) {
        twilio_client.messages(data.sid)
            .media
            .list()
            .then(
                media => 
                io.sockets.emit( 'get_twilio_messages_media', {
                    response: media,
                })
            ) 
            .done();
	});
    
    socket.on( 'read_twilio_messages', function( data ) {
        var message_sql = "SELECT * FROM `twilio_webhook_demo` WHERE `webhook_type` = 'message_comes' AND `message_from` = '"+ data.from_number +"' ORDER BY `id` DESC  LIMIT 0,20";
        con_mysql.query(message_sql, function (err, result) {
            io.sockets.emit( 'read_twilio_messages', {
                from_number: data.from_number,
                user_id: data.user_id,
                result: result,
                err: err,
            });
            var update_message_status_sql = "UPDATE `twilio_webhook_demo` SET `message_status` = '1' WHERE `webhook_type` = 'message_comes' AND `message_from` = '"+ data.from_number +"' ";
            con_mysql.query(update_message_status_sql, function (update_message_status_err, update_message_status_result) {});
        });
    });

    socket.on( 'message_comes_in_webhook', function( data ) {
        io.sockets.emit( 'message_comes_in_webhook', {});
    });

    socket.on( 'send_message_chat_to_twilio_whatsapp', function( data ) {
        twilio_client.messages
            .create({
               body: data.msg_text, 
               from: 'whatsapp:' + data.from_number, 
               to: 'whatsapp:' + data.to_number
             })
            .done();
        io.sockets.emit( 'new_message_client_to_visitor', {
                to_session_id: data.to_session_id,
                from_id: data.from_id,
                to_name: data.to_name,
                to_image: data.to_image,
                other_agent_chat_html: data.other_agent_chat_html,
                chat_html: data.chat_html,
                date_time: data.date_time,
                msg: data.msg,
                to_id: data.to_id
        });
    });

    socket.on( 'send_file_chat_to_twilio_whatsapp', function( data ) {
        twilio_client.messages
            .create({
               mediaUrl: [data.mediaUrl], 
               body: data.msg_text, 
               from: 'whatsapp:' + data.from_number, 
               to: 'whatsapp:' + data.to_number
             })
            .done();
        io.sockets.emit( 'new_file_client_to_visitor', {
                to_session_id: data.to_session_id,
                from_id: data.from_id,
                to_name: data.to_name,
                to_image: data.to_image,
                other_agent_chat_html: data.other_agent_chat_html,
                chat_html: data.chat_html,
                date_time: data.date_time,
                file: data.file
        });
    });

});

function when_a_twilio_sendbox_message_comes() {
    setTimeout(function () {
        var message_sql = "SELECT * FROM `twilio_webhook_demo` WHERE `webhook_type` = 'message_comes' AND `message_status` = '0' ORDER BY `id` DESC  LIMIT 0,5";
        con_mysql.query(message_sql, function (err, result) {
            if (err)
                throw err;
            if(result != ''){
                io.sockets.emit( 'new_twilio_sendbox_message_comes', {});
            }
        });
        when_a_twilio_sendbox_message_comes();
        // 1/2 Second
    }, 500);
}